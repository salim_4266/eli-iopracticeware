﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments;
using IO.Practiceware.Presentation.ViewServices.Financials.Common;
using IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Domain;
using Soaf.Linq;
using Soaf.Collections;
using BillingServiceTransactionStatus = IO.Practiceware.Model.BillingServiceTransactionStatus;
using FinancialSourceType = IO.Practiceware.Model.FinancialSourceType;
using MethodSent = IO.Practiceware.Model.MethodSent;

[assembly: Component(typeof(PostAdjustmentsService), typeof(IPostAdjustmentsService))]

namespace IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments
{
    [SupportsTransactionScopes]
    [SupportsUnitOfWork]
    public interface IPostAdjustmentsService
    {
        PostAdjustmentsUtilities Utilities { get; }

        PaymentInstrumentViewModel FindPaymentInstrumentDuplicate(PaymentInstrumentViewModel paymentInstrument, bool matchByInsurerId);

        ICollection<BalanceForwardSuggestionInformation> Suggest(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts);

        /// <summary>
        /// NOTE: we expect that activeAccounts represent only accounts with active billing services and that they are filtered
        /// </summary>
        /// <param name="paymentInstrument"></param>
        /// <param name="activeAccounts"></param>
        /// <param name="deletedTransactions"></param>
        IDictionary<int, ValidationMessage[]> Post(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts, ICollection<FinancialTransactionViewModel> deletedTransactions);

        /// <summary>
        /// Saves new or updates existing payment instrument.
        /// </summary>
        /// <param name="paymentInstrument">The payment instrument.</param>
        /// <returns></returns>
        FinancialBatch SavePaymentInstrument(PaymentInstrumentViewModel paymentInstrument);

        void InsertInvoiceReceivable(int invoiceId);
    }

    public enum PostBillingTransactionAction
    {
        DoNothing,
        Close, // Close, do not create new
        UpdateAmount,
        Recreate // Close and create new
    }

    public enum SuggestedBillingAction
    {
        Bill,
        Wait
    }

    public enum BalanceForwardingTarget
    {
        BilledParty,
        NextPayer,
        Patient
    }

    public class PostBillingContext
    {
        public BillingServiceTransaction ActiveTransaction { get; set; }
        public InvoiceReceivable BilledParty { get; set; }
        public PostBillingTransactionAction Action { get; set; }
    }

    public class BillingSuggestion
    {
        public BillingParty To { get; set; }
        public BillingActionType Action { get; set; }
    }

    public class BillingParty
    {
        public PatientInsurance Insurance { get; set; }
        public Patient Patient { get; set; }

        public bool IsPatient()
        {
            return Insurance == null && Patient != null;
        }

        public bool IsInsurer()
        {
            return Insurance != null && Patient == null;
        }
    }

    public class PostAdjustmentsService : IPostAdjustmentsService
    {
        private readonly IRepositoryService _repositoryService;
        private readonly IUnitOfWorkProvider _uowProvider;
        private readonly InvoiceValidationUtility _validationUtility;
        private readonly Lazy<DateTime?> _closeDateTime;

        private Lazy<AdjustmentType> _reversalAdjustmentType;
        private Lazy<AdjustmentType> _paymentAdjustmentType;
        private Lazy<AdjustmentType> _contractualAdjustmentType;

        public IPracticeRepository PracticeRepository { get; set; }

        public PostAdjustmentsUtilities Utilities { get; private set; }

        public PostAdjustmentsService(IPracticeRepository practiceRepository,
            IRepositoryService repositoryService,
            IUnitOfWorkProvider uowProvider,
            PostAdjustmentsUtilities utilities,
            InvoiceValidationUtility validationUtility)
        {
            _repositoryService = repositoryService;
            _uowProvider = uowProvider;
            _validationUtility = validationUtility;

            Utilities = utilities;
            PracticeRepository = practiceRepository;

            _closeDateTime = new Lazy<DateTime?>(() => ApplicationSettings.Cached.GetSetting<DateTime>(ApplicationSetting.CloseDateTime)
                .IfNotNull(s => (DateTime?)s.Value));

            Initialize();
        }

        public virtual ICollection<BalanceForwardSuggestionInformation> Suggest(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts)
        {
            var result = new List<BalanceForwardSuggestionInformation>();

            foreach (var account in activeAccounts)
            {
                foreach (var billingService in account.BillingServices)
                {
                    ValidateOrSetInvoiceReceivables(billingService.Transactions, paymentInstrument, account, billingService);

                    var suggestion = SuggestBalanceForwarding(account.PatientId, billingService);

                    // Skip when there is no suggestion
                    if (suggestion == null) continue;

                    // Prepare server reply
                    var balanceForward = new BalanceForwardSuggestionInformation
                    {
                        AccountId = account.PatientId,
                        BillingServiceId = billingService.Id.GetValueOrDefault(),
                        Action = PostAdjustmentsUtilities.BillingActionTypeMapper.Map(suggestion.Action),
                        To = suggestion.To.IsInsurer()
                            ? PostAdjustmentsUtilities.InsurerAsNextPayerMapper.Map(suggestion.To.Insurance)
                            : PostAdjustmentsUtilities.PatientAsNextPayerMapper.Map(suggestion.To.Patient)
                    };
                    result.Add(balanceForward);
                }
            }

            return result;
        }

        public virtual PaymentInstrumentViewModel FindPaymentInstrumentDuplicate(PaymentInstrumentViewModel paymentInstrument, bool matchByInsurerId)
        {
            var isPatientSource = paymentInstrument.FinancialSourceType == ViewModels.Financials.Common.Transaction.FinancialSourceType.Patient;
            var payerId = paymentInstrument.Payer.IfNotNull(p => (int?)p.Id);
            var paymentMethodId = paymentInstrument.PaymentMethod.IfNotNull(p => (int?)p.Id);

            // If the payer is the patient or the office and the Reference # is blank/null, do not perform the duplicate check
            if (string.IsNullOrWhiteSpace(paymentInstrument.ReferenceNumber)
                && (paymentInstrument.FinancialSourceType == ViewModels.Financials.Common.Transaction.FinancialSourceType.Patient
                    || paymentInstrument.FinancialSourceType == ViewModels.Financials.Common.Transaction.FinancialSourceType.BillingOrganization))
            {
                return null;
            }

            // If payment method is cash do not perform duplicate check
            if (paymentInstrument.PaymentMethod != null
                && string.Equals(paymentInstrument.PaymentMethod.Name, PaymentMethod.Cash, StringComparison.OrdinalIgnoreCase))
            {
                return null;
            }

            // Find all matching batches
            var matchingBatches = PracticeRepository.FinancialBatches
                .Include(fb => fb.Patient, fb => fb.Insurer, fb => fb.PaymentMethod)
                .Where(fb => (int)fb.FinancialSourceType == (int)paymentInstrument.FinancialSourceType
                                      && fb.Amount == paymentInstrument.PaymentAmount
                                      && fb.ExplanationOfBenefitsDateTime == paymentInstrument.CheckDate
                                      && fb.CheckCode == paymentInstrument.ReferenceNumber
                                      && (!isPatientSource || fb.PaymentMethodId == paymentMethodId)
                                      && (!matchByInsurerId || fb.InsurerId == payerId))
                .OrderByDescending(fb => fb.PaymentDateTime)
                .ToList();

            FinancialBatch financialBatch = null;

            // If amount found batches we have batch specifically for specified patient payer -> prefer it
            if (isPatientSource)
            {
                financialBatch = matchingBatches.FirstOrDefault(fb => fb.PatientId == payerId);
            }

            // Take latest one amount found
            financialBatch = financialBatch ?? matchingBatches.FirstOrDefault();

            var result = financialBatch.IfNotNull(PostAdjustmentsUtilities.FinancialBatchMapper.Map);
            return result;
        }

        public virtual FinancialBatch SavePaymentInstrument(PaymentInstrumentViewModel paymentInstrument)
        {
            FinancialBatch financialBatch;

            if (paymentInstrument.Id.HasValue)
            {
                financialBatch = PracticeRepository.FinancialBatches.First(fb => fb.Id == paymentInstrument.Id.Value);
            }
            else
            {
                financialBatch = new FinancialBatch();
                financialBatch.PaymentDateTime = paymentInstrument.Date;
                financialBatch.ExplanationOfBenefitsDateTime = paymentInstrument.CheckDate;
                financialBatch.FinancialSourceType = (FinancialSourceType)(int)paymentInstrument.FinancialSourceType;

                switch (financialBatch.FinancialSourceType)
                {
                    case FinancialSourceType.Insurer:
                        financialBatch.InsurerId = paymentInstrument.Payer.Id;
                        break;
                    case FinancialSourceType.Patient:
                        financialBatch.PatientId = paymentInstrument.Payer.Id;
                        break;
                }
            }

            // Round 1 allows editing only payment method, amount and check code
            financialBatch.PaymentMethodId = paymentInstrument.PaymentMethod.IfNotNull(pm => (int?)pm.Id);
            financialBatch.Amount = paymentInstrument.PaymentAmount;
            financialBatch.CheckCode = paymentInstrument.ReferenceNumber;

            PracticeRepository.Save(financialBatch);

            return financialBatch;
        }

        public virtual void InsertInvoiceReceivable(int invoiceId)
        {
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                var query ="INSERT INTO model.InvoiceReceivables(InvoiceId,PatientInsuranceId,PatientInsuranceAuthorizationId,PatientInsuranceReferralId,OpenForReview)" +
                    " SELECT " + invoiceId + ", pIn.Id, pia.Id, pir.Id, 0 FROM model.PatientInsurances pIn JOIN model.InvoiceReceivables ir ON ir.PatientInsuranceId IS NOT NULL " +
                    " AND NOT EXISTS (SELECT 1 FROM model.InvoiceReceivables WHERE PatientInsuranceId = pIn.Id) JOIN model.Invoices i ON i.id = ir.InvoiceId JOIN model.Encounters e ON e.Id = i.EncounterId" +
                    " LEFT JOIN model.InsurancePolicies ip ON pIn.InsurancePolicyId = ip.Id" +
                    " LEFT JOIN model.PatientInsuranceAuthorizations pia ON pia.EncounterId = e.Id AND pia.PatientInsuranceId = pIn.Id  AND pia.IsArchived = 0 AND pia.AuthorizationDateTime >=ip.StartDateTime" +
                    " LEFT JOIN model.PatientInsuranceReferrals pir ON pir.PatientInsuranceId = pIn.Id " +
                    " WHERE Ir.InvoiceId = " + invoiceId + " AND pin.InsuredPatientId = e.PatientId AND pIn.IsDeleted = 0 AND (pin.EndDateTime is null OR pin.EndDateTime > GetDate())";

                dbConnection.Execute(query);                
            }           
        }

        [UnitOfWork]
        public virtual IDictionary<int, ValidationMessage[]> Post(PaymentInstrumentViewModel paymentInstrument, ICollection<AccountViewModel> activeAccounts, ICollection<FinancialTransactionViewModel> deletedTransactions)
        {
            // Ensure payment instrument is saved first
            var financialBatch = SavePaymentInstrument(paymentInstrument);

            // Update invoice receivables
            foreach (var account in activeAccounts)
            {
                foreach (var billingService in account.BillingServices)
                {
                    ValidateOrSetInvoiceReceivables(billingService.Transactions, paymentInstrument, account, billingService);
                }

                ValidateOrSetInvoiceReceivables(account.OnAccountTransactions, paymentInstrument, account, null);
            }

            Dictionary<int, ValidationMessage[]> validationResult;
            using (_uowProvider.Create()) // New unit of work for validation to ensure it doesn't interfere with our unit of work
            {
                // Only perform validation for billed services' invoices
                var billActionTypes = new[] { BillingActionType.QueueClaimElectronic, BillingActionType.QueueClaimPaper };

                // Prepare for validation
                var invoicesWithReceivables = activeAccounts
                    .SelectMany(a => a.BillingServices)
                    .Where(bs => bs.BillingAction != null
                                 && billActionTypes.Contains((BillingActionType)bs.BillingAction.Id))
                    .SelectMany(bs => bs.Transactions)
                    .GroupBy(bs => bs.InvoiceId)
                    .ToDictionary(k => k.Key, v => v
                        .Where(i => i.InvoiceReceivableId != null)
                        .Select(i => i.InvoiceReceivableId.EnsureNotDefault().GetValueOrDefault())
                        .Distinct()
                        .ToList());

                var invoices = PracticeRepository.Invoices
                    .Where(i => invoicesWithReceivables.Keys.Contains(i.Id))
                    .ToList();
                _validationUtility.LoadInvoiceRelations(invoices);

                // Validate invoices we are about to bill
                var validationErrors = _validationUtility.ValidateInvoices(invoices, ir =>
                    invoicesWithReceivables.ContainsKey(ir.InvoiceId) && invoicesWithReceivables[ir.InvoiceId].Contains(ir.Id));

                // Check result to evaluate whether we should continue billing
                validationResult = validationErrors.ToDictionary(i => i.Key.Id, v => v.Value.ToArray());
                if (validationResult.SelectMany(e => e.Value).Any())
                {
                    return validationResult;
                }
            }

            // Remove all deleted financial transactions
            RemoveTransactions(deletedTransactions);

            foreach (var account in activeAccounts)
            {
                var billingContexts = new Dictionary<BillingServiceViewModel, PostBillingContext>();

                // Update on-account transactions
                foreach (var transaction in account.OnAccountTransactions)
                {
                    UpdateOrCreateFinancialTransaction(transaction, account, financialBatch);
                }

                foreach (var billingService in account.BillingServices)
                {
                    var billingContext = EvaluateBillingSteps(billingService);
                    billingContexts[billingService] = billingContext;

                    // Close transactions when needed
                    CloseBillingServiceTransactions(billingService, billingContext);

                    UpdateBillingService(billingService);

                    foreach (var transaction in billingService.Transactions)
                    {
                        UpdateOrCreateFinancialTransaction(transaction, account, financialBatch);
                    }

                    // Create active billing transaction for remaining balance
                    CreateActiveBillingTransaction(billingService, billingContext);
                }
            }

            // Save (the fast way)
            _repositoryService.Persist(_uowProvider.Current.ObjectStateEntries, typeof(IPracticeRepository).FullName);

            // Update payment instrument id if it was new
            paymentInstrument.Id = financialBatch.Id;

            return validationResult;
        }

        /// <summary>
        /// Validate that invoice receivables are properly set for each financial transaction
        /// </summary>
        /// <param name="transactions"></param>
        /// <param name="paymentInstrument"></param>
        /// <param name="account"></param>
        /// <param name="billingService"></param>
        private void ValidateOrSetInvoiceReceivables(IEnumerable<FinancialTransactionViewModel> transactions, PaymentInstrumentViewModel paymentInstrument, AccountViewModel account, BillingServiceViewModel billingService)
        {
            var resolvedInvoiceReceivable = new Lazy<int?>(() =>
            {
                // On-account?
                if (billingService == null) return (int?)null;

                int? patientInsuranceId = null;

                // Resolve patient insurance for insurer financial source
                if (paymentInstrument.FinancialSourceType == ViewModels.Financials.Common.Transaction.FinancialSourceType.Insurer)
                {
                    patientInsuranceId = Utilities.ResolvePatientInsuranceForSelectedPayer(account.PatientId,
                        account.PayerOverride ?? paymentInstrument.Payer, billingService);
                }

                // Resolve invoice receivable for insurance and invoice
                var invoiceReceivableId = Utilities.GetOrCreateInvoiceReceivableId(
                    billingService.InvoiceId, patientInsuranceId, billingService.Service.Id);
                return invoiceReceivableId;
            });

            // Update transaction properties (if moved from different billing service or created new)
            foreach (var transaction in transactions)
            {
                // Verify provided invoice receivable
                if (transaction.InvoiceReceivableId != null)
                {
                    var invoiceReceivable = Utilities.GetInvoiceReceivable(transaction.InvoiceReceivableId.GetValueOrDefault());
                    if (invoiceReceivable.InvoiceId != transaction.InvoiceId)
                    {
                        transaction.InvoiceReceivableId = resolvedInvoiceReceivable.Value;
                    }
                }
                else
                {
                    transaction.InvoiceReceivableId = resolvedInvoiceReceivable.Value;
                }
            }
        }

        private void RemoveTransactions(ICollection<FinancialTransactionViewModel> deletedTransactions)
        {
            // Remove adjustments
            var adjustmentTransactionIds = deletedTransactions
                .Where(t => t.FinancialTransactionType is AdjustmentTypeViewModel)
                .Select(t => t.Id)
                .Distinct()
                .ToArray();

            var adjustmentsToDelete = PracticeRepository.Adjustments
                .Where(a => adjustmentTransactionIds.Contains(a.Id)
                    && (_closeDateTime.Value == null || a.PostedDateTime > _closeDateTime.Value))
                .ToList();

            adjustmentsToDelete.ForEach(PracticeRepository.Delete);

            // Remove financial informations
            var financialInformationIds = deletedTransactions
                .Where(t => t.FinancialTransactionType is FinancialInformationTypeViewModel)
                .Select(t => t.Id)
                .Distinct()
                .ToArray();

            var financialInformationsToDelete = PracticeRepository.FinancialInformations
                .Where(a => financialInformationIds.Contains(a.Id)
                    && (_closeDateTime.Value == null || a.PostedDateTime > _closeDateTime.Value))
                .ToList();

            financialInformationsToDelete.ForEach(PracticeRepository.Delete);
        }

        private void CreateActiveBillingTransaction(BillingServiceViewModel billingService, PostBillingContext postBillingContext)
        {
            // Create new transaction only if requested to recreate (previous active is already closed)
            if (postBillingContext.Action != PostBillingTransactionAction.Recreate) return;

            BillingServiceTransactionStatus transactionStatus;
            MethodSent methodSent;
            GetTransactionStatusAndMethodByAction((BillingActionType)billingService.BillingAction.Id,
                out transactionStatus, out methodSent);

            // Get invoice receivable for this new billing transaction
            var patientInsuranceId = billingService.NextPayer.Payer is PatientInsurancePayerViewModel
                ? billingService.NextPayer.Payer.CastTo<PatientInsurancePayerViewModel>().PatientInsuranceId
                : (long?)null;
            var invoiceReceivableId = Utilities.GetOrCreateInvoiceReceivableId(billingService.InvoiceId, patientInsuranceId, billingService.Service.Id);

            var transaction = new BillingServiceTransaction();
            transaction.DateTime = DateTime.Now.ToClientTime();
            transaction.AmountSent = billingService.Balance;
            transaction.BillingServiceTransactionStatus = transactionStatus;
            transaction.MethodSent = methodSent;
            transaction.BillingServiceId = billingService.Id.GetValueOrDefault();
            transaction.InvoiceReceivable = Utilities.GetInvoiceReceivable(invoiceReceivableId);

            // Ensure invoice receivable for the invoice is no longer open for review, since we billed it
            var invoiceReceivables = Utilities.GetCachedInvoiceReceivables(transaction.InvoiceReceivable.InvoiceId);
            invoiceReceivables.ForEach(x => x.OpenForReview = false);

            // For electronic claims -> set claim file receiver id
            if (methodSent == MethodSent.Electronic
                && transaction.InvoiceReceivable.PatientInsuranceId != null)
            {
                transaction.ClaimFileReceiverId = transaction.InvoiceReceivable.PatientInsurance.InsurancePolicy
                    .Insurer.ClaimFileReceiverId;
            }
        }

        private void UpdateOrCreateFinancialTransaction(FinancialTransactionViewModel transaction, AccountViewModel account, FinancialBatch financialBatch)
        {
            IAdjustmentOrFinancialInformation transactionRecord;

            if (transaction.FinancialTransactionType is AdjustmentTypeViewModel)
            {
                transactionRecord = transaction.Id == null
                    ? new Adjustment()
                    : PracticeRepository.Adjustments.First(a => a.Id == transaction.Id);
            }
            else if (transaction.FinancialTransactionType is FinancialInformationTypeViewModel)
            {
                transactionRecord = transaction.Id == null
                    ? new FinancialInformation()
                    : PracticeRepository.FinancialInformations.First(a => a.Id == transaction.Id);
            }
            else
            {
                throw new ArgumentException(string.Format("Financial transaction type {0} not supported", transaction.FinancialTransactionType));
            }

            // Associate with current billing service
            transactionRecord.BillingServiceId = transaction.ServiceId;

            // Update other properties only if it doesn't fall into closed period yet
            if (_closeDateTime.Value == null
                || transaction.Id == null
                || transactionRecord.PostedDateTime > _closeDateTime.Value)
            {
                // Copy information from financial batch
                transactionRecord.FinancialBatch = financialBatch;
                transactionRecord.FinancialSourceType = financialBatch.FinancialSourceType;

                // Update properties user could have changed
                transactionRecord.InvoiceReceivable = transaction.InvoiceReceivableId == null
                    ? null
                    : Utilities.GetInvoiceReceivable(transaction.InvoiceReceivableId.Value);
                transactionRecord.PatientId = null;
                transactionRecord.InsurerId = null;
                transactionRecord.PostedDateTime = transaction.Date;
                transactionRecord.Amount = transaction.Amount;
                transactionRecord.Comment = transaction.Comment;
                transactionRecord.IncludeCommentOnStatement = transaction.IncludeCommentOnStatement;
                transactionRecord.ClaimAdjustmentGroupCodeId = transaction.FinancialTransactionType.GroupCode.IfNotNull(gc => gc.Id, (int?)null);
                transactionRecord.ClaimAdjustmentReasonCodeId = transaction.FinancialTransactionType.ReasonCode.IfNotNull(rc => rc.Id, (int?)null);

                // Perform special updates by record type
                if (transactionRecord is Adjustment)
                {
                    if (transaction.ServiceId == null && transaction.InvoiceReceivableId == null)
                    {
                        // On-account payment adjustment
                        transactionRecord.Patient = Utilities.GetPatientWithInsurances(account.PatientId);
                    }

                    // Set type
                    transactionRecord.CastTo<Adjustment>().AdjustmentTypeId = transaction.FinancialTransactionType.Id;
                }
                else
                {
                    // Set type
                    transactionRecord.CastTo<FinancialInformation>().FinancialInformationTypeId = transaction.FinancialTransactionType.Id;
                }
            }

            // Save changes
            PracticeRepository.Save(transactionRecord);
        }

        private void UpdateBillingService(BillingServiceViewModel billingService)
        {
            var billingServiceRecord = Utilities.GetBillingService(billingService.Id.GetValueOrDefault());
            billingServiceRecord.UnitAllowableExpense = billingService.Allowed;
            PracticeRepository.Save(billingServiceRecord);
        }

        private void CloseBillingServiceTransactions(BillingServiceViewModel billingService, PostBillingContext postBillingContext)
        {
            if (postBillingContext.Action == PostBillingTransactionAction.DoNothing) return;

            var activeTransactions = Utilities.GetActiveBillingTransactions(billingService.Id.GetValueOrDefault());

            // Close by updating transaction status
            foreach (var activeTransaction in activeTransactions)
            {
                switch (postBillingContext.Action)
                {
                    case PostBillingTransactionAction.Close:
                    case PostBillingTransactionAction.Recreate:
                        activeTransaction.BillingServiceTransactionStatus = PostAdjustmentsUtilities.ActiveToClosedTransactionStatus[activeTransaction.BillingServiceTransactionStatus];
                        break;
                    case PostBillingTransactionAction.UpdateAmount:
                        // Update only 1 transaction
                        if (postBillingContext.ActiveTransaction == activeTransaction)
                        {
                            activeTransaction.AmountSent = billingService.Balance;
                        }
                        break;
                }

                PracticeRepository.Save(activeTransaction);
            }
        }

        private BillingSuggestion SuggestBalanceForwarding(int accountId, BillingServiceViewModel billingService)
        {
            // Fetch patient information
            var patient = Utilities.GetPatientWithInsurances(accountId);

            // Get primary transaction and thus current billed party
            var primaryTransaction = Utilities.GetActivePrimaryTransaction(billingService.Id.GetValueOrDefault());

            // No current active transaction -> no suggestion
            // #16 & 17
            if (primaryTransaction == null)
            {
                return null;
            }

            BillingSuggestion result = null;

            // #18 & 19
            if (MatchesCriteria(billingService, primaryTransaction,
                IsCrOrPiDenial, new[]
                {
                    ContainingTransaction(IsPayment, IsZeroAmount, IsAnyPayer),
                    ContainingTransaction(IsContractualAdjustment, IsZeroAmount, IsAnyPayer),
                    ContainingTransaction(IsCopayOrCoinsurance, IsZeroAmount, IsAnyPayer),
                    ContainingTransaction(IsDeductible, IsZeroAmount, IsAnyPayer)
                }))
            {
                result = CreateResult(billingService, primaryTransaction, patient,
                    BalanceForwardingTarget.BilledParty,
                    primaryTransaction.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued
                        ? SuggestedBillingAction.Bill
                        : SuggestedBillingAction.Wait);
            }
            // #20
            else if (MatchesCriteria(billingService, primaryTransaction,
                IsAnything, new[]
                {
                    ContainingTransaction(IsPayment, IsZeroAmount, IsBilledPartyPayer),
                    ContainingTransaction(IsContractualAdjustment, IsNonZeroAmount, IsBilledPartyPayer),
                    ContainingTransaction(IsCopayOrCoinsurance, IsNonZeroAmount, IsBilledPartyPayer),
                    ContainingTransaction(IsDeductible, IsNonZeroAmount, IsBilledPartyPayer)
                }))
            {
                result = CreateResult(billingService, primaryTransaction, patient,
                    BalanceForwardingTarget.NextPayer, SuggestedBillingAction.Bill);
            }
            // #21
            else if (MatchesCriteria(billingService, primaryTransaction,
                IsAnything, new[]
                {
                    ContainingTransaction(IsPayment, IsZeroAmount, IsNonBilledPartyPayer),
                    ContainingTransaction(IsContractualAdjustment, IsNonZeroAmount, IsNonBilledPartyPayer),
                    ContainingTransaction(IsCopayOrCoinsurance, IsNonZeroAmount, IsNonBilledPartyPayer),
                    ContainingTransaction(IsDeductible, IsNonZeroAmount, IsNonBilledPartyPayer)
                }))
            {
                result = CreateResult(billingService, primaryTransaction, patient,
                    BalanceForwardingTarget.BilledParty, SuggestedBillingAction.Wait);
            }
            // #22 & 23
            else if (MatchesCriteria(billingService, primaryTransaction,
                IsNonDenial, new[]
                {
                    ContainingTransaction(IsPayment, IsGreaterThanZeroAmount, IsNonBilledPartyPayer)
                }))
            {
                result = CreateResult(billingService, primaryTransaction, patient,
                    BalanceForwardingTarget.BilledParty,
                    primaryTransaction.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued
                        ? SuggestedBillingAction.Bill
                        : SuggestedBillingAction.Wait);
            }
            // #24
            else if (MatchesCriteria(billingService, primaryTransaction,
                IsNonDenial, new[]
                {
                    ContainingTransaction(IsPayment, IsGreaterThanZeroAmount, IsBilledPartyPayer)
                }))
            {
                result = CreateResult(billingService, primaryTransaction, patient,
                    BalanceForwardingTarget.NextPayer, SuggestedBillingAction.Bill);
            }
            // #25 & 26
            else if (MatchesCriteria(billingService, primaryTransaction,
                IsDenial, null))
            {
                result = CreateResult(billingService, primaryTransaction, patient,
                    BalanceForwardingTarget.BilledParty,
                    primaryTransaction.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued
                        ? SuggestedBillingAction.Bill
                        : SuggestedBillingAction.Wait);
            }
            // #27 & 28
            else if (MatchesCriteria(billingService, primaryTransaction,
                IsPtRespDenial, new[]
                {
                    ContainingTransaction(IsOtherFinancialTransaction, IsGreaterThanZeroAmount, IsNonBilledPartyPayer)
                }))
            {
                result = CreateResult(billingService, primaryTransaction, patient,
                    BalanceForwardingTarget.BilledParty,
                    primaryTransaction.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued
                        ? SuggestedBillingAction.Bill
                        : SuggestedBillingAction.Wait);
            }
            // #29
            else if (MatchesCriteria(billingService, primaryTransaction,
                IsPtRespDenial, new[]
                {
                    ContainingTransaction(IsOtherFinancialTransaction, IsGreaterThanZeroAmount, IsBilledPartyPayer)
                }))
            {
                result = CreateResult(billingService, primaryTransaction, patient,
                    BalanceForwardingTarget.NextPayer, SuggestedBillingAction.Bill);
            }
            // #30
            else if (MatchesCriteria(billingService, primaryTransaction,
                IsOtherDenial, new[]
                {
                    ContainingTransaction(IsOtherFinancialTransaction, IsGreaterThanZeroAmount, IsAnyPayer)
                }))
            {
                result = CreateResult(billingService, primaryTransaction, patient,
                    BalanceForwardingTarget.Patient, SuggestedBillingAction.Bill);
            }
            // #31
            else if (MatchesCriteria(billingService, primaryTransaction,
                IsReversal, new[]
                {
                    ContainingTransaction(IsOtherFinancialTransaction, IsGreaterThanZeroAmount, IsAnyPayer)
                }))
            {
                // No suggestion
                //result = null;
            }
            else
            {
                // No suggestion made
                Trace.TraceWarning("Could not suggest next action for billing service {0}. Patient: {1}, Active Transaction: {2}",
                    billingService.Id, accountId, primaryTransaction.Id);
            }

            return result;
        }

        private PostBillingContext EvaluateBillingSteps(BillingServiceViewModel billingService)
        {
            var primaryTransaction = Utilities.GetActivePrimaryTransaction(billingService.Id.GetValueOrDefault());
            var result = new PostBillingContext();

            // No active transaction yet -> either bill next payer or do nothing
            if (primaryTransaction == null)
            {
                result.Action = (billingService.NextPayer != null
                    && billingService.BillingAction != null
                    && billingService.BillingAction.Id != (int)BillingActionType.LeaveUnbilled)
                    ? PostBillingTransactionAction.Recreate
                    : PostBillingTransactionAction.DoNothing;
                return result;
            }

            // Record current active transaction before we do anything to it
            result.ActiveTransaction = primaryTransaction;
            result.BilledParty = primaryTransaction.InvoiceReceivable;

            // See how should we handle it

            // Always close when balance reaches 0
            if (billingService.Balance <= 0)
            {
                result.Action = PostBillingTransactionAction.Close;
                return result;
            }

            // If payer and action not set -> Check balance change due to added transactions
            if (billingService.NextPayer == null || billingService.BillingAction == null)
            {
                result.Action = billingService.Balance != primaryTransaction.AmountSent
                    ? PostBillingTransactionAction.UpdateAmount
                    : PostBillingTransactionAction.DoNothing;
                return result;
            }

            // Requested leave unbilled? -> close
            if (billingService.BillingAction.Id == (int)BillingActionType.LeaveUnbilled)
            {
                result.Action = PostBillingTransactionAction.Close;
                return result;
            }

            // If we are about to wait for money from same party and the amount is same -> do nothing
            if ((IsSameParty(result.BilledParty, billingService.NextPayer.Payer)
                 && PostAdjustmentsUtilities.WaitBillingActions.Any(a => (int)a == billingService.BillingAction.Id))
                && billingService.Balance == primaryTransaction.AmountSent)
            {
                result.Action = PostBillingTransactionAction.DoNothing;
                return result;
            }

            // Otherwise -> close current active transactions and create new one
            result.Action = PostBillingTransactionAction.Recreate;
            return result;
        }

        private void GetTransactionStatusAndMethodByAction(BillingActionType billingActionType, out BillingServiceTransactionStatus transactionStatus, out MethodSent methodSent)
        {
            switch (billingActionType)
            {
                case BillingActionType.QueueClaimElectronic:
                    transactionStatus = BillingServiceTransactionStatus.Queued;
                    methodSent = MethodSent.Electronic;
                    break;
                case BillingActionType.QueueClaimPaper:
                    transactionStatus = BillingServiceTransactionStatus.Queued;
                    methodSent = MethodSent.Paper;
                    break;
                case BillingActionType.CrossoverClaim:
                    transactionStatus = BillingServiceTransactionStatus.SentCrossOver;
                    methodSent = MethodSent.Electronic;
                    break;
                case BillingActionType.ClaimWait:
                    transactionStatus = BillingServiceTransactionStatus.Wait;
                    methodSent = MethodSent.Electronic;
                    break;
                case BillingActionType.QueueStatement:
                    transactionStatus = BillingServiceTransactionStatus.Queued;
                    methodSent = MethodSent.Paper;
                    break;
                case BillingActionType.StatementWait:
                    transactionStatus = BillingServiceTransactionStatus.Wait;
                    methodSent = MethodSent.Paper;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("billingActionType");
            }
        }

        private BillingSuggestion CreateResult(BillingServiceViewModel billingService, BillingServiceTransaction activeTransaction, Patient patient, BalanceForwardingTarget forwardTo, SuggestedBillingAction suggestedAction)
        {
            var result = new BillingSuggestion();
            var activeInsurancesOnEncounterDate = Utilities.GetPatientInsurances(patient.Id)
                .Where(pi => pi.IsActiveForDateTime(billingService.Date))
                .OrderBy(pi => (int)pi.InsuranceType)
                .ThenBy(pi => pi.OrdinalId)
                .ToList();

            // Set To
            var billedParty = activeTransaction.InvoiceReceivable.PatientInsuranceId == null
                ? new BillingParty { Patient = patient }
                : new BillingParty { Insurance = activeTransaction.InvoiceReceivable.PatientInsurance };
            switch (forwardTo)
            {
                case BalanceForwardingTarget.BilledParty:
                    result.To = billedParty;
                    break;
                case BalanceForwardingTarget.NextPayer:
                    result.To = EvaluateNextPayer(activeInsurancesOnEncounterDate, billedParty, patient);
                    break;
                case BalanceForwardingTarget.Patient:
                    result.To = new BillingParty { Patient = patient };
                    break;
                default:
                    throw new ArgumentOutOfRangeException("forwardTo");
            }

            // Set Action
            if (result.To.IsPatient())
            {
                switch (suggestedAction)
                {
                    case SuggestedBillingAction.Bill:
                        result.Action = BillingActionType.QueueStatement;
                        break;
                    case SuggestedBillingAction.Wait:
                        result.Action = BillingActionType.StatementWait;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("suggestedAction");
                }
            }
            else if (result.To.IsInsurer())
            {
                switch (suggestedAction)
                {
                    case SuggestedBillingAction.Bill:
                        var primaryInsurance = activeInsurancesOnEncounterDate.First(i => i.InsuranceType == result.To.Insurance.InsuranceType);

                        // Check whether it will cross over
                        var willCrossOver = false;
                        var forceSendPaper = false;
                        if (primaryInsurance.Id != result.To.Insurance.Id)
                        {
                            willCrossOver = PracticeRepository.ClaimCrossOvers
                                .Any(cco => cco.SendingInsurerId == primaryInsurance.InsurancePolicy.InsurerId
                                            && cco.ReceivingInsurerId == result.To.Insurance.InsurancePolicy.InsurerId);

                            // Check whether we can send electronically
                            if (!willCrossOver)
                            {
                                forceSendPaper = result.To.Insurance.InsurancePolicy.Insurer.IsSecondaryClaimPaper;
                            }
                        }

                        // Set action
                        if (willCrossOver)
                        {
                            result.Action = BillingActionType.CrossoverClaim;
                        }
                        else
                        {
                            result.Action = !forceSendPaper && CanSendClaimElectronic(result.To.Insurance.InsurancePolicy.Insurer)
                                ? BillingActionType.QueueClaimElectronic
                                : BillingActionType.QueueClaimPaper;
                        }

                        break;
                    case SuggestedBillingAction.Wait:
                        result.Action = BillingActionType.ClaimWait;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("suggestedAction");
                }
            }

            return result;
        }

        private static bool IsSameParty(InvoiceReceivable billedParty, PayerViewModel payer)
        {
            // Insurer payer for the same patient insurance as billed party?
            var insurerPayer = payer as PatientInsurancePayerViewModel;
            if (insurerPayer != null
                && billedParty.PatientInsuranceId != null
                && billedParty.PatientInsuranceId == insurerPayer.PatientInsuranceId)
            {
                return true;
            }

            // Non-insurer payer in both cases?
            if (insurerPayer == null && billedParty.PatientInsuranceId == null)
            {
                return true;
            }

            // Not the same
            return false;
        }

        private bool MatchesCriteria(BillingServiceViewModel billingService,
            BillingServiceTransaction primaryActiveTransaction,
            Func<BillingServiceViewModel, bool> denialTypeCheck,
            Func<FinancialTransactionViewModel, int, bool>[] transactionPropertiesCheck)
        {
            var result = denialTypeCheck(billingService)
                && (transactionPropertiesCheck == null
                    || transactionPropertiesCheck.Length == 0
                    || billingService.Transactions
                        .Any(t => transactionPropertiesCheck
                            .Any(tpc => tpc(t, primaryActiveTransaction.InvoiceReceivableId)))
                   );
            return result;
        }

        private BillingParty EvaluateNextPayer(IList<PatientInsurance> activeInsurancesOnEncounterDate, BillingParty billedParty, Patient patient)
        {
            // Patient is last billed party
            if (billedParty.IsPatient()) return billedParty;

            // Next payer is only suggested having same insurance type
            var insuranceSelection = activeInsurancesOnEncounterDate
                .Where(pi => pi.InsuranceType == billedParty.Insurance.InsuranceType)
                .OrderBy(pi => pi.OrdinalId)
                .ToList();

            // Get position of current billed party
            var position = insuranceSelection.IndexOf(i => i.Id == billedParty.Insurance.Id);

            // Last insurance -> to patient; otherwise -> next insurance
            return position == insuranceSelection.Count - 1
                ? new BillingParty { Patient = patient }
                : new BillingParty { Insurance = insuranceSelection[position + 1] };
        }

        private bool CanSendClaimElectronic(Insurer insurer)
        {
            var result = !string.IsNullOrEmpty(insurer.PayerCode) && insurer.ClaimFileReceiverId != null;
            return result;
        }

        #region Transaction checks

        private Func<FinancialTransactionViewModel, int, bool> ContainingTransaction(Func<FinancialTransactionViewModel, bool> transactionTypeCheck, Func<decimal, bool> amountCheck, Func<int, int, bool> payerCheck)
        {
            return (ft, billedPartyId) =>
            {
                // Check whether it is transaction of given type
                var result = transactionTypeCheck(ft);
                if (!result) return false;

                result = amountCheck(ft.Amount) && payerCheck(billedPartyId, ft.InvoiceReceivableId.GetValueOrDefault());
                return result;
            };
        }

        private bool IsOtherFinancialTransaction(FinancialTransactionViewModel transaction)
        {
            var result = !IsPayment(transaction)
                && !IsContractualAdjustment(transaction)
                && !IsCopayOrCoinsurance(transaction)
                && !IsDeductible(transaction);

            return result;
        }

        private bool IsPayment(FinancialTransactionViewModel transaction)
        {
            if (_paymentAdjustmentType.Value == null) return false;

            return transaction.FinancialTransactionType is AdjustmentTypeViewModel
                && transaction.FinancialTransactionType.Id == _paymentAdjustmentType.Value.Id;
        }

        private bool IsContractualAdjustment(FinancialTransactionViewModel transaction)
        {
            if (_contractualAdjustmentType.Value == null) return false;

            return transaction.FinancialTransactionType is AdjustmentTypeViewModel
                && transaction.FinancialTransactionType.Id == _contractualAdjustmentType.Value.Id;
        }

        private bool IsCopayOrCoinsurance(FinancialTransactionViewModel transaction)
        {
            // TODO: a better way to identify these?
            var reasonCode = transaction.FinancialTransactionType.ReasonCode;
            var result = reasonCode != null
                         && (reasonCode.Name == "2"
                             || reasonCode.Name == "3" );
            return result;
        }

        private bool IsDeductible(FinancialTransactionViewModel transaction)
        {
            // TODO: a better way to identify these?
            var reasonCode = transaction.FinancialTransactionType.ReasonCode;
            var result = reasonCode != null
                         && reasonCode.Name == "1";
            return result;
        }

        private bool IsNonBilledPartyPayer(int billedPartyId, int transactionPayerId)
        {
            return billedPartyId != transactionPayerId;
        }

        private bool IsBilledPartyPayer(int billedPartyId, int transactionPayerId)
        {
            return billedPartyId == transactionPayerId;
        }

        private bool IsAnyPayer(int billedPartyId, int transactionPayerId)
        {
            return true;
        }

        private bool IsGreaterThanZeroAmount(decimal amount)
        {
            return amount > 0;
        }

        private bool IsZeroAmount(decimal amount)
        {
            return amount == 0;
        }

        private bool IsNonZeroAmount(decimal amount)
        {
            return amount != 0;
        }

        #endregion

        #region Denials

        private bool IsAnything(BillingServiceViewModel billingService)
        {
            return true;
        }

        private bool IsNonDenial(BillingServiceViewModel billingService)
        {
            return !IsDenial(billingService)
                && !IsOtherDenial(billingService)
                && !IsPtRespDenial(billingService)
                && !IsCrOrPiDenial(billingService);
        }

        private bool IsCrOrPiDenial(BillingServiceViewModel billingService)
        {
            var result = billingService.Transactions
                .Where(t => t.FinancialTransactionType.GroupCode != null)
                .Any(t => t.FinancialTransactionType.GroupCode.Id == (int)ClaimAdjustmentGroupCode.CorrectionReversal
                          || t.FinancialTransactionType.GroupCode.Id == (int)ClaimAdjustmentGroupCode.PayerInitiatedReduction);
            return result;
        }

        private bool IsDenial(BillingServiceViewModel billingService)
        {
            // TODO: a better way to identify denials?
            var result = billingService.Transactions
                .Any(t => t.FinancialTransactionType.Name.ToLower().Contains("denial"));
            return result;
        }

        private bool IsPtRespDenial(BillingServiceViewModel billingService)
        {
            // TODO: a better way to identify denials?
            var result = billingService.Transactions
                .Any(t => t.FinancialTransactionType.Name.ToLower().Replace(" ", string.Empty).Contains("ptresp"));
            return result;
        }

        private bool IsOtherDenial(BillingServiceViewModel billingService)
        {
            // TODO: how should we identify other types of denials?
            return false;
        }

        private bool IsReversal(BillingServiceViewModel billingService)
        {
            if (_reversalAdjustmentType.Value == null) return false;

            var result = billingService.Transactions
                .Any(t => t.FinancialTransactionType.Id == _reversalAdjustmentType.Value.Id);
            return result;
        }

        #endregion

        private void Initialize()
        {
            _reversalAdjustmentType = new Lazy<AdjustmentType>(() => PracticeRepository.AdjustmentTypes
                .FirstOrDefault(at => at.Id == (int)AdjustmentTypeId.Reversal));
            _paymentAdjustmentType = new Lazy<AdjustmentType>(() => PracticeRepository.AdjustmentTypes
                .FirstOrDefault(at => at.Id == (int)AdjustmentTypeId.Payment));
            _contractualAdjustmentType = new Lazy<AdjustmentType>(() => PracticeRepository.AdjustmentTypes
                .FirstOrDefault(at => at.Id == (int)AdjustmentTypeId.ContractualAdjustment));
        }
    }
}
