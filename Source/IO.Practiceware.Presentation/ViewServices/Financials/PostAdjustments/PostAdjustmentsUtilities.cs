﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Linq;
using Soaf.Collections;
using BillingServiceTransactionStatus = IO.Practiceware.Model.BillingServiceTransactionStatus;
using FinancialSourceType = IO.Practiceware.Model.FinancialSourceType;

namespace IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments
{
    /// <summary>
    /// Provide common mappers and querying tools used for posting adjustments. Server also as a caching layer
    /// </summary>
    public class PostAdjustmentsUtilities
    {
        public static IMapper<BillingActionType, NamedViewModel> BillingActionTypeMapper { get; private set; }
        public static IMapper<PatientInsurance, NextPayerViewModel> InsurerAsNextPayerMapper { get; private set; }
        public static IMapper<Patient, NextPayerViewModel> PatientAsNextPayerMapper { get; private set; }
        public static IMapper<FinancialSourceType, NamedViewModel> FinancialSourceTypeMapper { get; private set; }
        public static IMapper<Insurer, InsurerPayerViewModel> InsurerAsPayerMapper { get; private set; }
        public static IMapper<Patient, PatientPayerViewModel> PatientAsPayerMapper { get; private set; }
        public static IMapper<ClaimAdjustmentGroupCode, NameAndAbbreviationViewModel> ClaimAdjustmentGroupCodeMapper { get; private set; }
        public static IMapper<ClaimAdjustmentReasonCode, NameAndAbbreviationAndDetailViewModel> ClaimAdjustmentReasonCodeMapper { get; private set; }
        public static IMapper<PaymentMethod, PaymentMethodViewModel> PaymentMethodMapper { get; private set; }
        public static IMapper<EncounterService, ServiceViewModel> EncounterServiceMapper { get; private set; }
        public static IMapper<BillingService, BillingServiceViewModel> BillingServiceMapper { get; private set; }
        public static IMapper<AdjustmentType, AdjustmentTypeViewModel> AdjustmentTypeMapper { get; private set; }
        public static IMapper<FinancialInformationType, FinancialInformationTypeViewModel> FinancialInformationTypeMapper { get; private set; }
        public static IMapper<FinancialBatch, PaymentInstrumentViewModel> FinancialBatchMapper { get; private set; }

        public static Dictionary<ClaimAdjustmentGroupCode, string> ClaimAdjustmentGroupCodeMappings { get; private set; }
        public static IDictionary<BillingServiceTransactionStatus, BillingServiceTransactionStatus> ActiveToClosedTransactionStatus { get; private set; }
        public static int[] ActiveStatuses { get; private set; }
        public static BillingActionType[] WaitBillingActions { get; private set; }

        // Caches
        private IDictionary<int, List<BillingServiceTransaction>> _activeBillingServiceTransactions;
        private IDictionary<int, InvoiceReceivable> _invoiceReceivablesCache;
        private IDictionary<int, BillingService> _billingServicesCache;
        private IDictionary<int, Patient> _patientsWithInsurancesCache;
        private IDictionary<int, bool> _patientInsuranceEverBilledCache;

        public IPracticeRepository PracticeRepository { get; set; }

        static PostAdjustmentsUtilities()
        {
            StaticInit();
        }

        public PostAdjustmentsUtilities(IPracticeRepository practiceRepository)
        {
            PracticeRepository = practiceRepository;
            ClearCache();
        }

        public void ClearCache()
        {
            _activeBillingServiceTransactions = new Dictionary<int, List<BillingServiceTransaction>>();
            _invoiceReceivablesCache = new Dictionary<int, InvoiceReceivable>();
            _billingServicesCache = new Dictionary<int, BillingService>();
            _patientsWithInsurancesCache = new Dictionary<int, Patient>();
            _patientInsuranceEverBilledCache = new Dictionary<int, bool>();
        }

        public void CacheForAccounts(ICollection<AccountViewModel> accounts)
        {
            var queryableFactory = PracticeRepository.AsQueryableFactory();

            int[] patientIds = accounts.Select(a => a.PatientId).ToArray();
            int[] invoiceIds = accounts
                .SelectMany(ac => ac.BillingServices.Select(b => b.InvoiceId))
                .ToArray();
            int[] billingServiceIds = accounts
                .SelectMany(a => a.BillingServices)
                .Where(bs => bs.Id != null)
                .Select(bs => bs.Id.Value)
                .ToArray();

            _invoiceReceivablesCache = PracticeRepository.InvoiceReceivables
                .Include(ir => ir.PatientInsurance.InsurancePolicy.Insurer)
                .Where(ir => invoiceIds.Contains(ir.InvoiceId))
                .ToList()
                .ToDictionary(ir => ir.Id, ir => ir);

            _activeBillingServiceTransactions = PracticeRepository.BillingServiceTransactions
                .Include(bst => bst.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer)
                .Where(bst => billingServiceIds.Contains(bst.BillingServiceId)
                    && ActiveStatuses.Contains((int)bst.BillingServiceTransactionStatus))
                .ToList()
                .GroupBy(at => at.BillingServiceId)
                .ToDictionary(p => p.Key, p => p.ToList());

            _billingServicesCache = PracticeRepository.BillingServices
                .Where(p => billingServiceIds.Contains(p.Id))
                .ToList()
                .ToDictionary(p => p.Id, p => p);

            _patientsWithInsurancesCache = PracticeRepository.Patients
                .Where(p => patientIds.Contains(p.Id))
                .ToList()
                .ToDictionary(p => p.Id, p => p);

            // Load patient relations
            queryableFactory.Load(_patientsWithInsurancesCache.Values,
                x => x.PatientInsurances.Select(y => new
                {
                    Item1 = y.InsurancePolicy.Insurer.InsurerAddresses.Select(ia => ia.StateOrProvince.Country),
                    Item2 = y.InsurancePolicy.Insurer.IncomingPayerCodes
                }),
                x => x.PatientPhoneNumbers,
                x => x.PatientAddresses.Select(y => y.StateOrProvince),
                x => x.PatientEmailAddresses);

            _patientInsuranceEverBilledCache = PracticeRepository.InvoiceReceivables
                .Where(ir => ir.PatientInsurance != null && patientIds.Contains(ir.PatientInsurance.InsuredPatientId))
                .Select(ir => new
                {
                    PatientInsuranceId = ir.PatientInsuranceId.Value,
                    EverBilled = ir.BillingServiceTransactions.Any()
                })
                .GroupBy(p => p.PatientInsuranceId)
                .ToDictionary(k => k.Key, v => v.Max(p => p.EverBilled));
        }

        public Patient GetPatientWithInsurances(int patientId)
        {
            return _patientsWithInsurancesCache.GetValue(patientId, () =>
            {
                var patient = PracticeRepository.Patients
                    .Include(
                        x => x.PatientInsurances.Select(y => new 
                        { 
                            Item1 = y.InsurancePolicy.Insurer.InsurerAddresses.Select(ia => ia.StateOrProvince.Country),
                            Item2 = y.InsurancePolicy.Insurer.IncomingPayerCodes
                        }),
                        x => x.PatientPhoneNumbers,
                        x => x.PatientAddresses.Select(y => y.StateOrProvince),
                        x => x.PatientEmailAddresses)
                    .First(ir => ir.Id == patientId);
                return patient;
            });
        }

        public BillingService GetBillingService(int billingServiceId)
        {
            return _billingServicesCache.GetValue(billingServiceId, () =>
            {
                var billingService = PracticeRepository.BillingServices
                        .First(ir => ir.Id == billingServiceId);
                return billingService;
            });
        }

        public InvoiceReceivable GetInvoiceReceivable(int invoiceReceivableId)
        {
            return _invoiceReceivablesCache.GetValue(invoiceReceivableId, () =>
            {
                var invoiceReceivable = PracticeRepository.InvoiceReceivables
                    .Include(ir => ir.PatientInsurance.InsurancePolicy.Insurer)
                    .First(ir => ir.Id == invoiceReceivableId);
                return invoiceReceivable;
            });
        }

        /// <summary>
        /// Return all the invoice receivables attached to the given invoice id
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public List<InvoiceReceivable> GetCachedInvoiceReceivables(int invoiceId)
        {
            lock (_invoiceReceivablesCache)
            {
                var invoiceReceivables = _invoiceReceivablesCache.Where(x => x.Value.InvoiceId == invoiceId).Select(x => x.Value).ToList();
                return invoiceReceivables;
            }

        }

        public bool HasEverBeenBilled(int patientInsuranceId)
        {
            return _patientInsuranceEverBilledCache.GetValue(patientInsuranceId, () =>
            {
                var everBilled = PracticeRepository.InvoiceReceivables
                    .Where(ir => ir.PatientInsuranceId == patientInsuranceId)
                    .Select(ir => ir.BillingServiceTransactions.Any())
                    .FirstOrDefault();
                return everBilled;
            });
        }

        public int GetOrCreateInvoiceReceivableId(int invoiceId, long? patientInsuranceId, int encounterId)
        {
            lock (_invoiceReceivablesCache)
            {
                // Try find it in cache
                var invoiceReceivableId = _invoiceReceivablesCache
                    .Where(ir => ir.Value.InvoiceId == invoiceId && ir.Value.PatientInsuranceId == patientInsuranceId)
                    .Select(ir => (int?)ir.Key)
                    .FirstOrDefault();

                if (invoiceReceivableId != null)
                {
                    return invoiceReceivableId.Value;
                }

                // Get from database
                var invoiceReceivables = PracticeRepository.InvoiceReceivables
                    .Include(ir => ir.PatientInsurance.InsurancePolicy.Insurer)
                    .Where(ir => ir.InvoiceId == invoiceId).ToArray();
                invoiceReceivables.ForEach(x => _invoiceReceivablesCache.GetValue(x.Id, () => x));

                invoiceReceivableId = _invoiceReceivablesCache.Where(ir => ir.Value.InvoiceId == invoiceId && ir.Value.PatientInsuranceId == patientInsuranceId).Select(x => (int?)x.Key).FirstOrDefault();
                if (invoiceReceivableId.HasValue)
                {
                    return invoiceReceivableId.Value;
                }

                // Create new if not found
                var patientInsurance = patientInsuranceId == null
                    ? null
                    : PracticeRepository.PatientInsurances
                        .Include(pi => pi.InsurancePolicy.Insurer)
                        .First(pi => pi.Id == patientInsuranceId);

                var authorizationId = PracticeRepository.PatientInsuranceAuthorizations
                    .Where(pia => pia.PatientInsuranceId == patientInsuranceId
                                  && pia.EncounterId == encounterId
                                  && !pia.IsArchived)
                    .Select(pia => pia.Id)
                    .FirstOrDefault();

                var referralId = PracticeRepository.PatientInsuranceReferrals
                    .Where(pir => pir.PatientInsuranceId == patientInsuranceId
                                  && pir.Encounters.Any(e => e.Id == encounterId)
                                  && !pir.IsArchived)
                    .Select(pia => pia.Id)
                    .FirstOrDefault();

                var invoiceReceivable = new InvoiceReceivable
                 {
                     InvoiceId = invoiceId,
                     PatientInsurance = patientInsurance,
                     OpenForReview = false,
                     PatientInsuranceAuthorizationId = authorizationId == 0 ? new int?() : authorizationId,
                     PatientInsuranceReferralId = referralId == 0 ? new int?() : referralId,

                     // TODO: not supported at the moment
                     //PayerClaimControlNumber = 
                 };

                PracticeRepository.Save(invoiceReceivable);

                // Create a temporary Id we can use until invoice receivable is actually saved
                var id = int.MaxValue - _invoiceReceivablesCache.Count;
                _invoiceReceivablesCache.Add(id, invoiceReceivable);
                return id;
            }
        }

        public IList<BillingServiceTransaction> GetActiveBillingTransactions(int billingServiceId)
        {
            lock (_activeBillingServiceTransactions)
            {
                // Get active transactions
                List<BillingServiceTransaction> activeTransactions;
                if (_activeBillingServiceTransactions.ContainsKey(billingServiceId))
                {
                    activeTransactions = _activeBillingServiceTransactions[billingServiceId];
                }
                else
                {
                    activeTransactions = PracticeRepository.BillingServiceTransactions
                    .Include(bst => bst.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer)
                    .Where(bst => bst.BillingServiceId == billingServiceId
                                  && ActiveStatuses.Contains((int)bst.BillingServiceTransactionStatus))
                    .ToList();

                    // Cache
                    _activeBillingServiceTransactions.Add(billingServiceId, activeTransactions);
                }

                return activeTransactions;
            }
        }

        public BillingServiceTransaction GetActivePrimaryTransaction(int billingServiceId)
        {
            // Get active transactions
            var activeTransactions = GetActiveBillingTransactions(billingServiceId);

            // Evaluate primary one
            var primaryTransaction = activeTransactions
                .Where(bst => bst.InvoiceReceivable.PatientInsuranceId != null) // To Insurer
                .OrderByDescending(bst => bst.DateTime)
                .FirstOrDefault()
                ?? activeTransactions
                    .Where(bst => bst.InvoiceReceivable.PatientInsuranceId == null) // Otherwise to Patient
                    .OrderByDescending(bst => bst.DateTime)
                    .FirstOrDefault();
            return primaryTransaction;
        }

        /// <summary>
        /// List patient all insurances (except deleted ones)
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public List<PatientInsurance> GetPatientInsurances(int patientId)
        {
            var patientRecord = GetPatientWithInsurances(patientId);

            var insurances = patientRecord.PatientInsurances
                .Where(p => p.InsuredPatientId == patientId && !p.IsDeleted)
                .OrderBy(pi => (int)pi.InsuranceType)
                .ThenBy(pi => pi.OrdinalId)
                .ToList();
            return insurances;
        }


        /// <summary>
        /// List patient all insurances whose date exists for the invoice.
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public List<PatientInsurance> GetPatientInsurancesForInvoiceId(int patientId, DateTime invoiceDate)
        {
            var patientRecord = GetPatientWithInsurances(patientId);

            var insurances = patientRecord.PatientInsurances
                .Where(p => p.InsuredPatientId == patientId && ( p.EndDateTime == null || p.EndDateTime >= invoiceDate) && p.InsurancePolicy.StartDateTime <= invoiceDate)
                .OrderBy(pi => (int)pi.InsuranceType)
                .ThenBy(pi => pi.OrdinalId)
                .ToList();
            return insurances;
        }

        public int? ResolvePatientInsuranceForSelectedPayer(int patientId, PayerViewModel payer, BillingServiceViewModel billingService)
        {
            //var insurancesForPayer = GetPatientInsurancesForInvoiceId(patientId, billingService.Date);
            var insurancesForPayer = GetPatientInsurances(patientId);
            
            if (payer is InsurerPayerLookupViewModel)
            {
                var payerIdentifiers = payer.CastTo<InsurerPayerLookupViewModel>().PayerIdentifiers;
                insurancesForPayer = insurancesForPayer
                    .Where(pi => pi.InsurancePolicy.Insurer.IncomingPayerCodes.Any(ipc => payerIdentifiers.Contains(ipc.Value, StringComparer.OrdinalIgnoreCase))
                    || payerIdentifiers.Contains(pi.InsurancePolicy.Insurer.PayerCode, StringComparer.OrdinalIgnoreCase))
                    .ToList();
            }
            else
            {
                // Retrieve all patient insurances with same insurer name
                insurancesForPayer = insurancesForPayer
                    .Where(pi => string.Equals(pi.InsurancePolicy.Insurer.Name, payer.Name, StringComparison.OrdinalIgnoreCase))
                    .ToList();
            }

            // Shouldn't happen since override is always provided, but just in case
            if (insurancesForPayer.Count <= 0)
            {
                throw new InvalidOperationException(string.Format("Patient {0} has no insurance with {1} and service date {2}.", patientId, payer, billingService.Date));
            }
           
            // Only 1 insurance? -> Use it
            if (insurancesForPayer.Count == 1)
            {
                return insurancesForPayer.First().Id;
            }

            // Take patient insurance of active BST
            var billedParty = GetActivePrimaryTransaction(billingService.Id.GetValueOrDefault());
            if (billedParty != null)
            {
                var currentBilledInsurance = insurancesForPayer
                    .FirstOrDefault(pi => pi.Id == billedParty.InvoiceReceivable.PatientInsuranceId);

                // Found it? -> return
                if (currentBilledInsurance != null)
                {
                    return currentBilledInsurance.Id;
                }
            }

            // Try active insurance for specified insurer on encounter date
            var activeInsurances = insurancesForPayer
                .Where(pi => pi.IsActiveForDateTime(billingService.Date))
                .ToList();
            if (activeInsurances.Count > 0)
            {
                // Limit further search by active insurances
                insurancesForPayer = activeInsurances;
            }

            // Prefer insurance which was ever billed
            var everBilledInsurances = insurancesForPayer
                .Where(i => HasEverBeenBilled(i.Id))
                .ToList();
            if (everBilledInsurances.Count > 0)
            {
                insurancesForPayer = everBilledInsurances;
            }

            // Finally prioritize by last created patient insurance
            var patientInsuranceId = insurancesForPayer.Max(pi => pi.Id);
            return patientInsuranceId;
        }


        private static void StaticInit()
        {
            ActiveToClosedTransactionStatus = new Dictionary<BillingServiceTransactionStatus, BillingServiceTransactionStatus>
            {
                { BillingServiceTransactionStatus.Queued, BillingServiceTransactionStatus.ClosedNotSent },
                { BillingServiceTransactionStatus.Sent, BillingServiceTransactionStatus.ClosedSent },
                { BillingServiceTransactionStatus.Wait, BillingServiceTransactionStatus.ClosedNotSent },
                { BillingServiceTransactionStatus.SentCrossOver, BillingServiceTransactionStatus.ClosedCrossOver }
            };
            ClaimAdjustmentGroupCodeMappings = new Dictionary<ClaimAdjustmentGroupCode, string>
            {
                {ClaimAdjustmentGroupCode.ContractualObligation, "CO"},
                {ClaimAdjustmentGroupCode.CorrectionReversal, "CR"},
                {ClaimAdjustmentGroupCode.OtherAdjustment, "OA"},
                {ClaimAdjustmentGroupCode.PatientResponsibility, "PR"},
                {ClaimAdjustmentGroupCode.PayerInitiatedReduction, "PI"}
            };
            ActiveStatuses = ActiveToClosedTransactionStatus.Keys.Distinct().Select(s => (int)s).ToArray();
            WaitBillingActions = new[] { BillingActionType.ClaimWait, BillingActionType.StatementWait };

            InitMappers();
        }

        static void InitMappers()
        {
            var mapperFactory = Mapper.Factory;

            var addressMapper = mapperFactory.CreateAddressViewModelMapper<InsurerAddress>();
            var policyHolderRelationshipMapper = mapperFactory.CreateNamedViewModelMapper<PolicyHolderRelationshipType>();
            FinancialSourceTypeMapper = mapperFactory.CreateNamedViewModelMapper<FinancialSourceType>();
            BillingActionTypeMapper = mapperFactory.CreateNamedViewModelMapper<BillingActionType>();

            ClaimAdjustmentReasonCodeMapper = mapperFactory.CreateMapper<ClaimAdjustmentReasonCode, NameAndAbbreviationAndDetailViewModel>(
                item => new NameAndAbbreviationAndDetailViewModel
                {
                    Id = item.Id,
                    Name = item.Code,
                    Abbreviation = item.Code,
                    Detail = item.Name
                }, false);

            AdjustmentTypeMapper = mapperFactory.CreateMapper<AdjustmentType, AdjustmentTypeViewModel>(
                item => new AdjustmentTypeViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    IsCash = item.IsCash,
                    IsDebit = item.IsDebit,
                    OrdinalId = item.OrdinalId,

                    // Set separately
                    //GroupCode,
                    //ReasonCode
                }, false);

            FinancialInformationTypeMapper = mapperFactory.CreateMapper<FinancialInformationType, FinancialInformationTypeViewModel>(
                item => new FinancialInformationTypeViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    OrdinalId = item.OrdinalId,

                    // Set separately
                    //GroupCode,
                    //ReasonCode
                }, false);

            PaymentMethodMapper = mapperFactory.CreateMapper<PaymentMethod, PaymentMethodViewModel>(
                item => new PaymentMethodViewModel
                {
                    Id = item.Id,
                    Name = item.Name.Trim(), // TODO: contains extra spaces in the end. Maybe change view?
                    IsReferenceRequired = item.IsReferenceRequired,
                    OrdinalId = item.OrdinalId
                }, false);

            InsurerAsPayerMapper = mapperFactory.CreateMapper<Insurer, InsurerPayerViewModel>(
                item => new InsurerPayerViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    PlanName = item.PlanName,
                    Address = item.InsurerAddresses == null ? null : item.InsurerAddresses
                        // Prefer claims address when showing insurers address
                        .OrderBy(a => a.InsurerAddressTypeId == (int)InsurerAddressTypeId.Claims)
                        .Select(a => addressMapper.Map(a).ToString())
                        .FirstOrDefault()
                }, false);

            PatientAsPayerMapper = mapperFactory.CreateMapper<Patient, PatientPayerViewModel>(
                item => new PatientPayerViewModel
                {
                    Id = item.Id,
                    Name = item.DisplayName
                }, false);

            FinancialBatchMapper = mapperFactory.CreateMapper<FinancialBatch, PaymentInstrumentViewModel>(
                item => new PaymentInstrumentViewModel
                {
                    Id = item.Id,
                    ReferenceNumber = item.CheckCode,
                    FinancialSourceType = item.FinancialSourceType.CastTo<int>().CastTo<ViewModels.Financials.Common.Transaction.FinancialSourceType>(),
                    Payer = item.FinancialSourceType == FinancialSourceType.Insurer
                        ? InsurerAsPayerMapper.Map(item.Insurer)
                        : item.FinancialSourceType == FinancialSourceType.Patient
                        ? PatientAsPayerMapper.Map(item.Patient)
                        : (PayerViewModel)null,
                    Date = item.PaymentDateTime,
                    CheckDate = item.ExplanationOfBenefitsDateTime,
                    PaymentAmount = item.Amount,
                    PaymentMethod = item.PaymentMethod.IfNotNull(PaymentMethodMapper.Map)
                }, false);

            ClaimAdjustmentGroupCodeMapper = mapperFactory.CreateMapper<ClaimAdjustmentGroupCode, NameAndAbbreviationViewModel>(
                item => new NameAndAbbreviationViewModel
                {
                    Id = (int)item,
                    Name = item.GetDisplayName(),
                    Abbreviation = ClaimAdjustmentGroupCodeMappings[item]
                }, false);

            var billingServiceModifierMapper = mapperFactory.CreateMapper<BillingServiceModifier, NameAndAbbreviationAndDetailViewModel>(
                item => new NameAndAbbreviationAndDetailViewModel
                {
                    Id = item.ServiceModifier.Id,
                    Detail = item.ServiceModifier.Description,
                    Abbreviation = item.ServiceModifier.Code
                }, false);

            EncounterServiceMapper = mapperFactory.CreateMapper<EncounterService, ServiceViewModel>(
                item => new ServiceViewModel
                {
                    Id = item.Id,
                    Code = item.Code,
                    Description = item.Description
                }, false);

            BillingServiceMapper = mapperFactory.CreateMapper<BillingService, BillingServiceViewModel>(
                item => new BillingServiceViewModel
                {
                    Id = item.Id,
                    InvoiceId = item.InvoiceId.GetValueOrDefault(),
                    Date = item.Invoice.Encounter.StartDateTime,
                    Allowed = item.UnitAllowableExpense,
                    Charge = item.UnitCharge.GetValueOrDefault(),
                    Units = item.Unit.GetValueOrDefault(),
                    Modifiers = item.BillingServiceModifiers
                        .OrderBy(bsm => bsm.OrdinalId)
                        .Select(billingServiceModifierMapper.Map)
                        .ToObservableCollection(),
                    Service = EncounterServiceMapper.Map(item.EncounterService),

                    // Populated separately
                    //Transactions
                }, false);

            InsurerAsNextPayerMapper = mapperFactory.CreateMapper<PatientInsurance, NextPayerViewModel>(
                item => new NextPayerViewModel
                {
                    Payer = new PatientInsurancePayerViewModel
                    {
                        Id = item.InsurancePolicy.InsurerId,
                        Name = item.InsurancePolicy.Insurer.Name,
                        PlanName = item.InsurancePolicy.Insurer.PlanName,
                        Address = item.InsurancePolicy.Insurer.InsurerAddresses == null ? null
                            : item.InsurancePolicy.Insurer.InsurerAddresses
                            // Prefer claims address when showing insurers address
                                .OrderBy(a => a.InsurerAddressTypeId == (int)InsurerAddressTypeId.Claims)
                                .Select(a => addressMapper.Map(a).ToString())
                                .FirstOrDefault(),

                        Abbreviation = InsurancePolicyToAbbreviation(item),

                        PatientInsuranceId = item.Id,
                        OrdinalId = item.OrdinalId,
                        InsurancePolicyId = item.InsurancePolicy.Id,
                        InsuranceType = new NameAndAbbreviationViewModel
                        {
                            Id = (int)item.InsuranceType,
                            Name = item.InsuranceType.GetDisplayName(),
                            Abbreviation = item.InsuranceType.GetAttribute<DisplayAttribute>().ShortName
                        },
                        PolicyStartDate = item.InsurancePolicy.StartDateTime,
                        PolicyEndDate = item.EndDateTime,
                    },
                    Relationship = policyHolderRelationshipMapper.Map(item.PolicyholderRelationshipType)
                }, false);

            PatientAsNextPayerMapper = mapperFactory.CreateMapper<Patient, NextPayerViewModel>(
                item => new NextPayerViewModel
                {
                    Payer = new PatientPayerViewModel
                    {
                        Id = item.Id,
                        Name = item.DisplayName,
                        Abbreviation = "P"
                    },
                    Relationship = policyHolderRelationshipMapper.Map(PolicyHolderRelationshipType.Self)
                }, false);
        }

        /// <summary>
        /// Converts insurance policy type and it's ordinal Id to following representation: M1, M2, V1, W1 and etc.
        /// </summary>
        /// <returns></returns>
        private static string InsurancePolicyToAbbreviation(PatientInsurance insurance)
        {
            var result = insurance.InsuranceType.GetAttribute<DisplayAttribute>().ShortName + insurance.OrdinalId;
            return result;
        }
    }
}
