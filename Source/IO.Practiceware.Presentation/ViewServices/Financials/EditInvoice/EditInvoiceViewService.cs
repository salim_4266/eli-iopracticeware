﻿using System.ComponentModel.DataAnnotations;
using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.Financials.Common;
using IO.Practiceware.Presentation.ViewServices.Financials.EditInvoice;
using IO.Practiceware.Services.Documents;
using Soaf;
using IO.Practiceware.Data;
using IO.Practiceware.Services.Icd;
using Soaf.Data;
using System.Data;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Encounter = IO.Practiceware.Model.Encounter;
using EncounterServiceViewModel = IO.Practiceware.Presentation.ViewModels.Financials.EditInvoice.EncounterServiceViewModel;
using ReferralViewModel = IO.Practiceware.Presentation.ViewModels.PatientReferralPopup.ReferralViewModel;
using IO.Practiceware.Services.ApplicationSettings;
using System.Globalization;

[assembly: Component(typeof(EditInvoiceViewService), typeof(EditInvoiceViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Financials.EditInvoice
{
    public interface IEditInvoiceViewService
    {
        EditInvoiceViewModel LoadInfo(int? invoiceId, int? patientId, int diagnosisTypeId, bool loadDataList = true, bool? isIcd10 = null);
        int SaveInvoice(InvoiceViewModel invoice);
        bool IsDefaultDiagnosisTypeIcd10(int patientId);
        void BillInovice(InvoiceViewModel invoice);
        ObservableCollection<ReferralViewModel> GetPatientExistingReferralInformation(int patientId);
        IList<ValidationMessage> ValidateBill(int id, ObservableCollection<EncounterServiceViewModel> encounterServices, int? referralPatientInsuranceId, int renderingProviderId);
        ObservableCollection<DiagnosisCodeViewModel> LoadDiagnoses(bool isIcd10, string filterText);
        EncounterServiceViewModel LoadServiceDetails(int id);
        ProvidersDataListsViewModel LoadProviders();
        List<HtmlDocument> LoadClaim(int invoiceId);
        DiagnosisCodeViewModel FetchDiagnosisDescription(DiagnosisViewModel diagnosisViewModel);
        ObservableCollection<DiagnosisCodeViewModel> GetIcd10Codes(string icd9Code);
        string GetAdjustmentRate(int insurerId, string code);
        decimal GetServiceCharge(int insurerId, string code);
    }

    public class EditInvoiceViewService : BaseViewService, IEditInvoiceViewService
    {
        private readonly Func<EditInvoiceViewModel> _editInvoiceViewModel;
        private readonly InvoiceValidationUtility _validationUtility;
        private readonly IMapper<EncounterService, ServiceViewModel> _encounterServiceMapper;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly IMapper<Invoice, CareTeamViewModel> _careTeamMapper;
        private readonly IMapper<BillingDiagnosis, DiagnosisViewModel> _diagnosisMapper;

        private IMapper<Patient, PatientViewModel> _patientViewModelMapper;
        private IMapper<PatientInsuranceReferral, ReferralViewModel> _referralMapper;
        private IMapper<Tuple<Invoice, Model.PatientInsuranceAuthorization>, PatientInsuranceAuthorizationViewModel> _authMapper;
        private IMapper<Invoice, InvoiceViewModel> _invoiceMapper;
        private IMapper<PatientInsurance, NextPayerViewModel> _nextInsurancePayMapper;
        private IMapper<Invoice, NextPayerViewModel> _nextPatientPayMapper;
        private IMapper<BillingService, EncounterServiceViewModel> _billingServiceServiceMapper;
        private IMapper<EncounterService, EncounterServiceViewModel> _billingServiceServiceDetailsMapper;
        private IMapper<BillingServiceModifier, NameAndAbbreviationAndDetailViewModel> _billingModifierMapper;
        private IMapper<Doctor, ResourceViewModel> _doctorMapper;
        private IMapper<Patient, NamedViewModel> _patientNamedViewModelMapper;
        private IMapper<PatientInsurance, InsuranceViewModel> _patientInsuranceMapper;
        private IMapper<ServiceLocation, ColoredViewModel> _locationMapper;
        private IMapper<ServiceLocation, ServiceLocationViewModel> _locationNamedViewMapper;
        private IMapper<ExternalProvider, NameAndAbbreviationAndDetailViewModel> _externalProviderNamedViewMapper;
        private IMapper<ExternalProvider, CareTeamProviderViewModel> _orderingProviderMapper;
        private IMapper<Provider, CareTeamProviderViewModel> _supervisingProviderMapper;

        private IMapper<InvoiceType, NamedViewModel> _invoiceTypeMapper;
        private IMapper<BillingActionType, NamedViewModel> _billingActionMapper;
        private IMapper<PolicyHolderRelationshipType, NamedViewModel> _policyHolderRelationshipTypeMapper;
        private readonly PaperClaimsMessageProducer _paperClaimsMessageProducer;
        private readonly PaperClaimGenerationUtilities _paperClaimGenerationUtilities;
        private readonly Func<ObservableCollection<DiagnosisViewModel>> _createDiagnosisViewModelCollection;
        private readonly Func<DiagnosisViewModel> _createDiagnosisViewModel;
        private readonly Func<DiagnosisCodeViewModel> _createDiagnosisCodeViewModel;
        private readonly IIcd9To10Service _icd9To10Service;
        private readonly IIcd10Service _icd10Service;
        private readonly Func<ObservableCollection<DiagnosisCodeViewModel>> _createDiagnosisCodeViewModelCollection;
        private readonly Func<ObservableCollection<DiagnosisLinkPointerViewModel>> _createLinkedDiagnosisViewModelCollection;
        private readonly String[] _diagnosisLetters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" };

        private bool? _isIcd10;
        //private int _diagnosisTypeId;        
        private ObservableCollection<DiagnosisViewModel> _diagnosisViewModel;

        public EditInvoiceViewService(Func<EditInvoiceViewModel> editInvoiceViewModel,
            IMapper<Invoice, CareTeamViewModel> careTeamMapper,
            IMapper<BillingDiagnosis, DiagnosisViewModel> diagnosisMapper,
            IMapper<EncounterService, ServiceViewModel> encounterServiceMapper,
            IUnitOfWorkProvider unitOfWorkProvider,
            InvoiceValidationUtility validationUtility,
            PaperClaimsMessageProducer paperClaimsMessageProducer,
            PaperClaimGenerationUtilities paperClaimGenerationUtilities,
            Func<ObservableCollection<DiagnosisViewModel>> createDiagnosisViewModelCollection,
            Func<DiagnosisViewModel> createDiagnosisViewModel,
            Func<DiagnosisCodeViewModel> createDiagnosisCodeViewModel,
            Func<ObservableCollection<DiagnosisCodeViewModel>> createDiagnosisCodeViewModelCollection,
            Func<ObservableCollection<DiagnosisLinkPointerViewModel>> createLinkedDiagnosisViewModelCollection,
            IIcd9To10Service icd9To10Service,
            IIcd10Service icd10Service)
        {
            _editInvoiceViewModel = editInvoiceViewModel;
            _careTeamMapper = careTeamMapper;
            _diagnosisMapper = diagnosisMapper;
            _encounterServiceMapper = encounterServiceMapper;
            _unitOfWorkProvider = unitOfWorkProvider;
            _validationUtility = validationUtility;
            _paperClaimsMessageProducer = paperClaimsMessageProducer;
            _paperClaimGenerationUtilities = paperClaimGenerationUtilities;
            _createDiagnosisViewModelCollection = createDiagnosisViewModelCollection;
            _createDiagnosisViewModel = createDiagnosisViewModel;
            _createDiagnosisCodeViewModel = createDiagnosisCodeViewModel;
            _createDiagnosisCodeViewModelCollection = createDiagnosisCodeViewModelCollection;
            _createLinkedDiagnosisViewModelCollection = createLinkedDiagnosisViewModelCollection;
            _icd9To10Service = icd9To10Service;
            _icd10Service = icd10Service;

            InitMappers();



        }

        private static List<BillingDiagnosisCode> MapBillingDiagnosis(DataTable dt)
        {
            var billingDiagnosisCodes = new List<BillingDiagnosisCode>();
            foreach (DataRow item in dt.Rows)
            {
                billingDiagnosisCodes.Add(new BillingDiagnosisCode
                {
                    Code = item["DiagnosisCode"].ToString(),
                    OrdinalId = Convert.ToInt32(item["OrdinalId"]),
                    DiagnosisId = Convert.ToInt32(item["Id"]),
                    Description = Convert.ToString(item["DiagnosisDescription"])
                });
            }
            return billingDiagnosisCodes;
        }

        private static List<BillingServiceBillingDiagnosis> MapBillingServiceBillingDiagnosis(DataTable dt)
        {
            var billingServiceBillingDiagnosis = new List<BillingServiceBillingDiagnosis>();
            foreach (DataRow item in dt.Rows)
            {
                billingServiceBillingDiagnosis.Add(new BillingServiceBillingDiagnosis
                {
                    BillingDiagnosisId = Convert.ToInt32(item["BillingDiagnosisId"]),
                    OrdinalId = Convert.ToInt32(item["OrdinalId"]),
                    BillingServiceId = Convert.ToInt32(item["BillingServiceId"])
                });
            }
            return billingServiceBillingDiagnosis;
        }

        public bool IsDefaultDiagnosisTypeIcd10(int patientId)
        {
            Insurer insurer = PracticeRepository.PatientInsurances.Where(pi => pi.InsuredPatientId == patientId && pi.OrdinalId == 1 && pi.IsDeleted == false).Select(pi => pi.InsurancePolicy.Insurer).FirstOrDefault();
            bool isDiagnosisTypeIcd10;
            if (insurer != null && insurer.DiagnosisTypeId != null)
            {
                isDiagnosisTypeIcd10 = (insurer.DiagnosisTypeId == (int)DiagnosisTypeId.Icd10);
            }
            else
            {
                isDiagnosisTypeIcd10 = (ApplicationSettings.Cached.GetSetting<string>(ApplicationSetting.DefaultDiagnosisTypeId).Value == Convert.ToString((int)DiagnosisTypeId.Icd10));
            }
            return isDiagnosisTypeIcd10;
        }

        public EditInvoiceViewModel LoadInfo(int? invoiceId, int? patientId, int diagnosisTypeId, bool loadDataList = true, bool? isIcd10 = null)
        {
            if (isIcd10 == null)
                _isIcd10 = diagnosisTypeId == 2; // Manual Invoice

            var editInvoiceViewModel = _editInvoiceViewModel();

            if (invoiceId.HasValue && invoiceId > 0)
            {
                DataTable diagTable;
                var invoice = LoadInvoice(invoiceId);
                _isIcd10 = invoice.DiagnosisTypeId == 2;
                string icd10DiagQuery = _icd10Service.GetIcd10DiagnosisCodesQuery(invoiceId.Value);

                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    diagTable = dbConnection.Execute<DataTable>(icd10DiagQuery);
                }
                if (_isIcd10.Value && isIcd10 == null || (_isIcd10.Value && isIcd10.HasValue && isIcd10.Value))
                {

                    if (diagTable.Rows.Count > 0)
                    {
                        _diagnosisViewModel = GetDiagnosis(diagTable);
                    }
                    else {
                        _diagnosisViewModel = null;
                    }
                }
                else if (!_isIcd10.Value && isIcd10 == null || (!_isIcd10.Value && isIcd10.HasValue && !isIcd10.Value))
                {
                    _diagnosisViewModel = invoice.BillingDiagnoses.OrderBy(x => x.OrdinalId).Select(x => _diagnosisMapper.Map(x)).ToExtendedObservableCollection();
                }
                else
                {
                    _isIcd10 = null;
                    if (isIcd10.HasValue && isIcd10.Value)
                        invoice.DiagnosisTypeId = 2;
                    else
                        invoice.DiagnosisTypeId = 1;

                    _diagnosisViewModel = _createDiagnosisViewModelCollection();
                }
                editInvoiceViewModel.Invoice = _invoiceMapper.Map(invoice);
                //_isIcd10 = (DiagnosisTypeId) Enum.Parse(typeof (DiagnosisTypeId), editInvoiceViewModel.Invoice.CodesetType.ToString()) == DiagnosisTypeId.Icd10;
                editInvoiceViewModel.Invoice.NextPayers = editInvoiceViewModel.Invoice.NextPayers.FixOrdinalIds().ToExtendedObservableCollection();
                //Synch diagnosis link with invoice diagnosis so that when reordering main diagnosis it should reflect the encounter service as well 
                editInvoiceViewModel.Invoice.EncounterServices
                    .ForEachWithSinglePropertyChangedNotification(x => x.LinkedDiagnoses = x.LinkedDiagnoses
                        .Where(ed => editInvoiceViewModel.Invoice.EncounterDiagnosisLinks.Select(ld => ld.PointerLetter).Contains(ed.PointerLetter))
                        .Select(ed => ed).ToExtendedObservableCollection());
                //Adding patient pay mapper as it could not be assigned at initial.
                editInvoiceViewModel.Invoice.NextPayers.Add(_nextPatientPayMapper.Map(invoice));
                if (editInvoiceViewModel.Invoice.PatientInsuranceAuthorization != null && editInvoiceViewModel.Invoice.PatientInsuranceAuthorization.Location != null && invoice.Encounter.ServiceLocation != null)
                {
                    editInvoiceViewModel.Invoice.PatientInsuranceAuthorization.Location = _locationMapper.Map(invoice.Encounter.ServiceLocation);
                }
                editInvoiceViewModel.Invoice.CareTeam.IsNotManualClaim = invoice.Encounter.Appointments.OfType<UserAppointment>().Any();
            }
            else
            {
                var patient = LoadPatient(patientId);
                diagnosisTypeId = (int)GetDiagnosisCodeSetToCreateClaim(patient.Id, diagnosisTypeId);

                editInvoiceViewModel.Invoice.Patient = _patientViewModelMapper.Map(patient);
                editInvoiceViewModel.Invoice.Date = DateTime.Now.ToClientTime();
                editInvoiceViewModel.Invoice.NextPayers = patient.PatientInsurances
                                                          .Where(x => x.IsActiveForDateTime(editInvoiceViewModel.Invoice.Date))
                                                          .OrderBy(x => x.InsuranceType.GetAttribute<DisplayAttribute>().Order).ThenBy(p => p.OrdinalId)
                    .Select(x => _nextInsurancePayMapper.Map(x))
                                                          .FixOrdinalIds()
                                                          .ToObservableCollection();

                editInvoiceViewModel.Invoice.NextPayers.Add(new NextPayerViewModel
                  {
                      Payer = new PatientPayerViewModel
                      {
                          Id = patient.Id,
                          Name = patient.DisplayName,
                          Abbreviation = "P"
                      },
                      Relationship = _policyHolderRelationshipTypeMapper.Map(PolicyHolderRelationshipType.Self),
                  });
                editInvoiceViewModel.Invoice.CareTeam.IsNotManualClaim = false;
                editInvoiceViewModel.Invoice.InvoiceSupplemental = new InvoiceSupplementalViewModel();
                editInvoiceViewModel.Invoice.CodesetType = new NamedViewModel
                {
                    Id = diagnosisTypeId,
                    Name = Enum.GetName(typeof(DiagnosisTypeId), diagnosisTypeId)
                };
            }
            if (loadDataList)
            {
                editInvoiceViewModel.DataLists = new DataListsViewModel();
                new Action[]
                {
                    () => { editInvoiceViewModel.DataLists.Providers = PracticeRepository.Users.Where(x => !x.IsArchived && x.IsSchedulable).Select(x => new NamedViewModel {Id = x.Id, Name = x.UserName}).ToExtendedObservableCollection(); },
                        () => { editInvoiceViewModel.DataLists.Modifiers = PracticeRepository.ServiceModifiers.OrderBy(x => x.BillingOrdinalId).ThenBy(x => x.Code).Select(x => new NameAndAbbreviationAndDetailViewModel {Abbreviation = x.Code, Name = x.Description, Id = x.Id}).ToObservableCollection(); },
                        () => { editInvoiceViewModel.DataLists.Services = PracticeRepository.EncounterServices.Where(x => !x.IsArchived).OrderBy(x => x.Code).Select(x => new ServiceViewModel {Code = x.Code, Description = x.Description, Id = x.Id, UnitFee = x.UnitFee, IsZeroChargeAllowedOnClaim = x.IsZeroChargeAllowedOnClaim}).ToObservableCollection(); },
                        () => { editInvoiceViewModel.DataLists.AttributeToLocations = PracticeRepository.ServiceLocations.Select(x => new ServiceLocationViewModel {Id = x.Id, Name = x.ShortName}).ToObservableCollection(); },
                        () => { editInvoiceViewModel.DataLists.ServiceLocations = PracticeRepository.ServiceLocations.Select(x => new ServiceLocationViewModel {Id = x.Id, Name = x.ShortName, CodeId = x.ServiceLocationCodeId}).ToObservableCollection(); },
                        () =>
                        {
                            //Until we build support for facility invoices, hiding "Facility" as a choice under InvoiceType.
                            editInvoiceViewModel.DataLists.InvoiceTypes = Enum.GetValues(typeof (InvoiceType)).Cast<InvoiceType>().Where(x => x != InvoiceType.Facility).Select(x => new NamedViewModel {Id = (int) x, Name = x.GetDisplayName()}).ToObservableCollection();
                            if (editInvoiceViewModel.Invoice.InvoiceType == null)
                            {
                                editInvoiceViewModel.Invoice.InvoiceType = editInvoiceViewModel.DataLists.InvoiceTypes.FirstOrDefault(x => x.Id == (int) InvoiceType.Professional);
                            }
                        },
                        () => { editInvoiceViewModel.DataLists.BillingActions = Enums.GetValues<BillingActionType>().Select(_billingActionMapper.Map).ToObservableCollection(); },
                        () =>
                        {
                            var userProviders = PracticeRepository.Providers.Where(x => x.User != null && !x.User.IsArchived && x.IsBillable).Distinct()
                                                                                                            .OrderBy(x => x.User.OrdinalId)
                                                                                                            .ThenBy(x => x.Id)
                                                                                                            .Select(x => new ProviderViewModel
                                                                                                            {
                                                                                                                Provider = new NamedViewModel
                                                                                                                {
                                                                                                                    Id = (int)x.UserId,                                                                                                        
                                                                                                                    Name = x.User.UserName
                                                                                                                },
                                                                                                                BillingOrganization = new NamedViewModel {Id = x.BillingOrganization.Id, Name = x.BillingOrganization.ShortName},
                                                                                                                IsProviderUser = true, Id = (int)x.Id

                                                                                                            }).Distinct().ToExtendedObservableCollection();
                            var serviceLocationProviders = PracticeRepository.Providers.Where(x => x.User == null && x.ServiceLocation != null)
                                                                                                            .OrderBy(x => x.ServiceLocation.ShortName)
                                                                                                            .Select(x => new ProviderViewModel
                                                                                                            {
                                                                                                                Provider = new NamedViewModel
                                                                                                                {
                                                                                                                    Id = x.ServiceLocation.Id,
                                                                                                                    Name = x.ServiceLocation.ShortName
                                                                                                                },
                                                                                                                BillingOrganization = new NamedViewModel {Id = x.BillingOrganization.Id, Name = x.BillingOrganization.ShortName},
                                                                                                                IsProviderUser = false, Id = x.Id
                                                                                                            })
                                                                                                            .ToExtendedObservableCollection();
                            editInvoiceViewModel.DataLists.RenderingProviders = userProviders.AddRange(serviceLocationProviders).ToExtendedObservableCollection();
                            var supervisingProviderList = PracticeRepository.Providers.Where(x => (x.User != null && !x.User.IsArchived && x.IsBillable) || (x.ServiceLocation != null)).ToList();
                            PracticeRepository.AsQueryableFactory().Load(supervisingProviderList,
                                x => x.User,
                                x => x.ServiceLocation,
                                x => x.IdentifierCodes);
                            editInvoiceViewModel.DataLists.SupervisingProviders = supervisingProviderList.Select(x => _supervisingProviderMapper.Map(x)).ToExtendedObservableCollection();
                        },
                    () => editInvoiceViewModel.DataLists.PlacesOfService = PracticeRepository.ServiceLocationCodes.Select(sl => new NameAndAbbreviationAndDetailViewModel {Id = sl.Id, Name = sl.Name, Abbreviation = sl.PlaceOfServiceCode, Detail = sl.PlaceOfServiceCode + " - " + sl.Name }).OrderBy(sl => sl.Abbreviation).ToObservableCollection(),
                    () => editInvoiceViewModel.DataLists.ClaimFrequencyTypeCodes = Enum.GetValues(typeof (ClaimFrequencyTypeCode)).Cast<ClaimFrequencyTypeCode>().Select(x => new NamedViewModel{ Id = (int)x, Name = (int)x + " - " + x.GetDisplayName() }).OrderBy(x => x.Id).ToObservableCollection(),
                    () => editInvoiceViewModel.DataLists.StatesOrProvinces = PracticeRepository.StateOrProvinces.Select(x => new NameAndAbbreviationViewModel{ Id = x.Id, Abbreviation = x.Abbreviation, Name = x.Name }).OrderBy(x => x.Abbreviation).ToObservableCollection(),
                    () => editInvoiceViewModel.DataLists.ClaimDelayReasonCodes = Enum.GetValues(typeof (ClaimDelayReason)).Cast<ClaimDelayReason>().Select(x => new NamedViewModel{ Id = (int)x, Name = x.GetDisplayName()}).OrderBy(x => x.Id).ToObservableCollection()
                }.ForAllInParallel(a => a());
            }

            return editInvoiceViewModel;
        }

        private DiagnosisTypeId GetDiagnosisCodeSetToCreateClaim(int patientId, int defaultCodeSet)
        {
            int diagnosisCodeType = defaultCodeSet;
            Insurer insurer = PracticeRepository.PatientInsurances.Where(pi => pi.InsuredPatientId == patientId && pi.OrdinalId == 1 && pi.IsDeleted == false && ((pi.EndDateTime != null && pi.EndDateTime > DateTime.Now) || (pi.EndDateTime == null)) ).Select(pi => pi.InsurancePolicy.Insurer).FirstOrDefault(x => !x.IsArchived);


            if (insurer != null && insurer.DiagnosisTypeId.HasValue)
            {
                if (defaultCodeSet != insurer.DiagnosisTypeId.Value)
                {
                    diagnosisCodeType = insurer.DiagnosisTypeId.Value;
                }
            }
            return (DiagnosisTypeId)diagnosisCodeType;
        }

        private Patient LoadPatient(int? id)
        {
            var patient = PracticeRepository.Patients.FirstOrDefault(x => x.Id == id);
            PracticeRepository.AsQueryableFactory().Load(patient,
                p => p.PatientInsurances.Select(x => x.InvoiceReceivables),
                p => p.PatientInsurances.Select(x => x.InsurancePolicy.Insurer));
            return patient;
        }

        //For validation at the time of billing.
        protected virtual Invoice LoadInvoice(int? id)
        {
            var invoice = PracticeRepository.Invoices.Where(x => x.Id == id).Select(x => x).Select(inv => inv).FirstOrDefault();
            PracticeRepository.AsQueryableFactory().Load(invoice,
                inv => inv.ReferringExternalProvider.PatientExternalProviders.Select(x => x.ExternalProvider),
                inv => inv.ReferringExternalProvider.IdentifierCodes.Select(x => x.ExternalProvider),
                inv => inv.ReferringExternalProvider.ExternalContactAddresses.Select(eca => eca.StateOrProvince),
                inv => inv.ReferringExternalProvider.ExternalContactPhoneNumbers,
                inv => inv.BillingDiagnoses.Select(x => x.ExternalSystemEntityMapping.ExternalSystemEntity),
                inv => inv.DiagnosisType,
                inv => inv.InvoiceComments,
                inv => inv.InvoiceReceivables.Select(x => new
                {
                    PatientInsurance = x.PatientInsurance.InsurancePolicy.Insurer.InsurerAddresses,
                    Invoice = x.Invoice.ReferringExternalProvider.PatientExternalProviders.Select(y => y.ExternalProvider),
                    x.PatientInsuranceReferral
                }),
                inv => inv.BillingServices.Select(x => new
                {
                    BillingServiceBillingDiagnoses = x.BillingServiceBillingDiagnoses.Select(y => y.BillingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntity),
                    BillingServiceModifiers = x.BillingServiceModifiers.Select(y => y.ServiceModifier),
                    x.EncounterService,
                    FacilityEncounterService = x.FacilityEncounterService.RevenueCode,
                    Adjustments = x.Adjustments.Select(y => y.AdjustmentType),
                    x.BillingServiceTransactions,
                    x.FinancialInformations,
                    x.OrderingExternalProvider
                }),
                inv => inv.BillingProvider.BillingOrganization.IdentifierCodes,
                inv => inv.BillingProvider.BillingOrganization.BillingOrganizationAddresses,
                inv => inv.BillingProvider.User,
                inv => inv.BillingProvider.IdentifierCodes,
                inv => inv.Encounter.ServiceLocation,
                inv => inv.Encounter.ServiceLocation.ServiceLocationAddresses,
                inv => inv.Encounter.ServiceLocation.IdentifierCodes,
                inv => inv.Encounter.ServiceLocation.ServiceLocationCode,
                inv => inv.Encounter.Appointments.OfType<UserAppointment>().Select(x => x.User),
                inv => inv.Encounter.Patient.PatientAddresses.Select(y => y.StateOrProvince),
                inv => inv.Encounter.PatientInsuranceAuthorizations,
                inv => inv.Encounter.PatientInsuranceReferrals,
                inv => inv.Encounter.Patient.PatientInsurances.Select(x => new
                {
                    InsurancePolicy = x.InsurancePolicy.Insurer,
                    x.InvoiceReceivables
                }),
                inv => inv.Encounter.Patient.PatientInsurances.Select(y => y.InsurancePolicy.Insurer.InsurerAddresses),
                inv => inv.Encounter.Patient.PatientInsurances.Select(y => y.InsurancePolicy.PolicyholderPatient),
                inv => inv.AttributeToServiceLocation,
                inv => inv.InvoiceSupplemental.AccidentStateOrProvince,
                inv => inv.InvoiceSupplemental.OrderingProvider,
                inv => inv.InvoiceSupplemental.OrderingProvider.IdentifierCodes,
                inv => inv.InvoiceSupplemental.SupervisingProvider,
                inv => inv.InvoiceSupplemental.SupervisingProvider.IdentifierCodes,
                inv => inv.InvoiceSupplemental.SupervisingProvider.ServiceLocation,
                inv => inv.InvoiceSupplemental.SupervisingProvider.User,
                inv => inv.ServiceLocationCode);

            return invoice;
        }

        public IList<ValidationMessage> ValidateBill(int id, ObservableCollection<EncounterServiceViewModel> encounterServices, int? referralPatientInsuranceId, int renderingProviderId)
        {
            var invoice = PracticeRepository.Invoices.Single(x => x.Id == id);
            var invoices = new[] { invoice };
            _validationUtility.LoadInvoiceRelations(invoices);

            // PatientInsurance :
            var insurance = encounterServices
                .Where(x => x.NextPayer != null
                    && x.NextPayer.Payer as PatientInsurancePayerViewModel != null
                    && ValidServiceForTransaction(x))
                .Select(x => ((PatientInsurancePayerViewModel)x.NextPayer.Payer)).FirstOrDefault();

            var insuranceId = insurance != null ? (int?)insurance.PatientInsuranceId : null;
            var insuranceTypeId = insurance != null ? (int?)insurance.InsuranceType.Id : null;

            var warningList = _validationUtility.ValidateInvoices(invoices, null, insuranceTypeId, insuranceId, referralPatientInsuranceId)
                .Select(v => v.Value)
                .FirstOrDefault()
                ?? new List<ValidationMessage>();

            var renderingProvider = PracticeRepository.Providers.Single(x => x.Id == renderingProviderId);
            if (!renderingProvider.IsBillable)
            {
                warningList.Add(ValidationMessage.BillableRenderingProviderRequired);
            }

            return warningList;
        }

        //Initialize members.
        private void InitMappers()
        {
            var mapperFactory = Mapper.Factory;
            //diagnosisViewModel = GetDiagnosis();
            _billingActionMapper = mapperFactory.CreateNamedViewModelMapper<BillingActionType>();
            _policyHolderRelationshipTypeMapper = mapperFactory.CreateNamedViewModelMapper<PolicyHolderRelationshipType>();

            //Used by _invoiceMapper
            _invoiceTypeMapper = mapperFactory.CreateNamedViewModelMapper<InvoiceType>();

            _patientViewModelMapper = mapperFactory.CreateMapper<Patient, PatientViewModel>(patient => new PatientViewModel
            {
                DateOfBirth = patient.DateOfBirth.HasValue ? patient.DateOfBirth : null,
                Id = patient.Id,
                Name = patient.DisplayName
            });

            _locationNamedViewMapper = mapperFactory.CreateMapper<ServiceLocation, ServiceLocationViewModel>(location => new ServiceLocationViewModel
            {
                Name = location.ShortName,
                Id = location.Id,
                CodeId = location.ServiceLocationCodeId
            });
            //Used by referral mapper
            _doctorMapper = mapperFactory.CreateMapper<Doctor, ResourceViewModel>(doc => new ResourceViewModel
            {
                Id = doc.Id,
                FirstName = doc.FirstName,
                LastName = doc.LastName,
                DisplayName = doc.DisplayName,
                UserName = doc.UserName
            });
            _patientNamedViewModelMapper = mapperFactory.CreateMapper<Patient, NamedViewModel>(patient => new NamedViewModel
            {
                Name = patient.DisplayName,
                Id = patient.Id
            });

            _patientInsuranceMapper = mapperFactory.CreateMapper<PatientInsurance, InsuranceViewModel>(patientInsurance => new InsuranceViewModel
            {

                Id = patientInsurance.Id,
                PolicyId = patientInsurance.InsurancePolicy.Id,
                PolicyCode = patientInsurance.InsurancePolicy.PolicyCode,
                GroupCode = patientInsurance.InsurancePolicy.GroupCode,
                StartDateTime = patientInsurance.InsurancePolicy.StartDateTime,
                EndDateTime = patientInsurance.EndDateTime.HasValue ? patientInsurance.EndDateTime.Value : new DateTime?(),
                InsurerId = patientInsurance.InsurancePolicy.Insurer.Id,
                InsurerName = patientInsurance.InsurancePolicy.Insurer.Name
            });

            //Used by auth mapper
            _locationMapper = mapperFactory.CreateMapper<ServiceLocation, ColoredViewModel>(service => new ColoredViewModel
            {
                Color = service.Color,
                Id = service.Id,
                Name = service.Name
            });

            //Used by Referral Helper method.
            _referralMapper = mapperFactory.CreateMapper<PatientInsuranceReferral, ReferralViewModel>(referral => new ReferralViewModel
            {
                Id = referral.Id,
                StartDateTime = referral.StartDateTime.GetValueOrDefault(),
                EncounterIds = referral.Encounters.Select(x => x.Id).ToList(),
                InvoiceReceivableIds = referral.InvoiceReceivables.Select(x => x.Id).ToList(),
                EndDateTime = referral.EndDateTime.HasValue ? referral.EndDateTime.Value : new DateTime?(),
                ReferralCode = referral.ReferralCode,
                TotalEncountersCovered = referral.TotalEncountersCovered.GetValueOrDefault(),
                EncountersRemaining = referral.TotalEncountersCovered.GetValueOrDefault() - referral.Encounters.Count(e => !(e.EncounterStatus.IsCancelledStatus())),
                Resource = _doctorMapper.Map(referral.Doctor),
                Patient = _patientNamedViewModelMapper.Map(referral.Patient),
                Insurance = _patientInsuranceMapper.Map(referral.PatientInsurance)
            });
            _nextInsurancePayMapper = mapperFactory.CreateMapper<PatientInsurance, NextPayerViewModel>(patientInsuranceTuple => new NextPayerViewModel
            {
                Payer = new PatientInsurancePayerViewModel
                {
                    Id = patientInsuranceTuple.InsurancePolicy.InsurerId,
                    Name = patientInsuranceTuple.InsurancePolicy.Insurer.Name,
                    Abbreviation = InsurancePolicyToAbbreviation(patientInsuranceTuple),
                    PatientInsuranceId = patientInsuranceTuple.Id,
                    InsurancePolicyId = patientInsuranceTuple.InsurancePolicy.Id,
                    PolicyStartDate = patientInsuranceTuple.InsurancePolicy.StartDateTime,
                    PolicyEndDate = patientInsuranceTuple.EndDateTime,
                    ClaimFileReceiverId = patientInsuranceTuple.InsurancePolicy.Insurer.ClaimFileReceiverId,
                    IsSecondaryClaimPaper = patientInsuranceTuple.InsurancePolicy.Insurer.IsSecondaryClaimPaper,
                    SendZeroCharge = patientInsuranceTuple.InsurancePolicy.Insurer.SendZeroCharge,
                    InsuranceType = new NameAndAbbreviationViewModel
                        {
                            Id = (int)patientInsuranceTuple.InsuranceType,
                            Name = patientInsuranceTuple.InsuranceType.GetDisplayName(),
                            Abbreviation = patientInsuranceTuple.InsuranceType.GetAttribute<DisplayAttribute>().ShortName
                        }
                },
                Relationship = Mapper.Factory.CreateNamedViewModelMapper<PolicyHolderRelationshipType>().Map(patientInsuranceTuple.PolicyholderRelationshipType)
            });

            _nextPatientPayMapper = mapperFactory.CreateMapper<Invoice, NextPayerViewModel>(invoice => new NextPayerViewModel
            {
                Payer = new PatientPayerViewModel
                {
                    Id = invoice.Encounter.Patient.Id,
                    Name = invoice.Encounter.Patient.DisplayName,
                    Abbreviation = "P"
                },
                Relationship = mapperFactory.CreateNamedViewModelMapper<PolicyHolderRelationshipType>().Map(PolicyHolderRelationshipType.Self),
            });
            //PatientAuthorzation helper mapper
            _authMapper = mapperFactory.CreateMapper<Tuple<Invoice, Model.PatientInsuranceAuthorization>, PatientInsuranceAuthorizationViewModel>(item => new PatientInsuranceAuthorizationViewModel
            {
                Code = item.Item2.AuthorizationCode,
                EncounterId = (int)item.Item2.EncounterId,
                PatientInsuranceId = item.Item2.PatientInsuranceId,
                Comments = item.Item2.Comment,
                Location = (item.Item2.Encounter != null || (item.Item2.Encounter != null && item.Item2.Encounter.ServiceLocation != null)) ? _locationMapper.Map(item.Item2.Encounter.ServiceLocation) : null,
                Id = item.Item2.Id,
                Date = item.Item2.AuthorizationDateTime,
                InvoiceId = item.Item1.Id
            });
            _billingModifierMapper = mapperFactory.CreateMapper<BillingServiceModifier, NameAndAbbreviationAndDetailViewModel>(billingServiceModifier => new NameAndAbbreviationAndDetailViewModel
            {
                Name = billingServiceModifier.ServiceModifier.Description,
                Abbreviation = billingServiceModifier.ServiceModifier.Code,
                Id = billingServiceModifier.ServiceModifier.Id
            });
            var billingActionMapper = mapperFactory.CreateNamedViewModelMapper<BillingActionType>();
            //Used by Invoice mapper.
            _billingServiceServiceMapper = mapperFactory.CreateMapper<BillingService, EncounterServiceViewModel>(billingService => new EncounterServiceViewModel
            {
                Service = _encounterServiceMapper.Map(billingService.EncounterService),
                Id = billingService.Id,
                Modifiers = billingService.BillingServiceModifiers.IfNotNull(bsm => bsm.OrderBy(ord => ord.OrdinalId).Select(x => _billingModifierMapper.Map(x))).ToExtendedObservableCollection(),
                LinkedDiagnoses = _isIcd10 == null ? _createLinkedDiagnosisViewModelCollection() : (bool)_isIcd10 ? billingService.BillingServiceBillingDiagnoses.Where(x => x.IsIcd10).OrderBy(x => x.OrdinalId).Select(x => _diagnosisViewModel.Where(j => j.ActiveDiagnosisCode.Id == x.BillingDiagnosisId).Select(i => i.DiagnosisLinkPointer).FirstOrDefault()).ToExtendedObservableCollection() : billingService.BillingServiceBillingDiagnoses.Where(x => x.BillingDiagnosis != null && !x.IsIcd10).OrderBy(x => x.OrdinalId).Select(x => _diagnosisMapper.Map(x.BillingDiagnosis).DiagnosisLinkPointer).ToExtendedObservableCollection(),
                Units = billingService.Unit.HasValue ? billingService.Unit.Value : 1,
                Charge = billingService.UnitCharge.GetValueOrDefault(),
                Allowed = billingService.Unit * billingService.UnitAllowableExpense,
                AdjustmentAmount = billingService.Adjustments.Where(x => x.AdjustmentType != null).Sum(a => a.AdjustmentType.IsDebit ? a.Amount * -1 : a.Amount),
                AllBillingActions = Enums.GetValues<BillingActionType>().Select(billingActionMapper.Map).ToObservableCollection(), // Collection to get the valid list of actions from.
                PatientBillDate = billingService.PatientBillDate.HasValue ? billingService.PatientBillDate.Value : (DateTime?)null,
                NdcCode = billingService.Ndc.IsNotNullOrEmpty()
                            ? billingService.Ndc
                            : billingService.EncounterService.Ndc,
                IsEmg = billingService.IsEmg,
                IsEpsdt = billingService.IsEpsdt,
                UnclassifiedServiceDescription = billingService.UnclassifiedServiceDescription.IsNotNullOrEmpty()
                            ? billingService.UnclassifiedServiceDescription
                            : billingService.EncounterService.UnclassifiedServiceDescription,
                FreehandNote = billingService.ClaimComment,
                TransactionsExist = billingService.BillingServiceTransactions.Any()
            });
            // Used By LoadServiceDetails
            _billingServiceServiceDetailsMapper = mapperFactory.CreateMapper<EncounterService, EncounterServiceViewModel>(encounterService => new EncounterServiceViewModel
            {
                Units = encounterService.DefaultUnit,
                NdcCode = encounterService.Ndc,
                UnclassifiedServiceDescription = encounterService.UnclassifiedServiceDescription
            });

            //used by invoice mapper.
            _externalProviderNamedViewMapper = mapperFactory.CreateMapper<ExternalProvider, NameAndAbbreviationAndDetailViewModel>(ep => new NameAndAbbreviationAndDetailViewModel
            {
                Id = ep.Id,
                Name = ep.GetFormattedName() + ep.GetFormattedNpi(),
                Abbreviation = ep.LastNameOrEntityName,
                Detail = ep.GetFormattedAddressAndPhoneNumber(),
            });

            _supervisingProviderMapper = mapperFactory.CreateMapper<Provider, CareTeamProviderViewModel>(x => new CareTeamProviderViewModel
            {
                Id = x.Id,
                NPI = x.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi).ToStringIfNotNull("NPI not documented"),
                FirstName = x.User.IfNotNull(y => y.FirstName),
                MiddleName = x.User.IfNotNull(y => y.MiddleName),
                LastName = x.User != null ? x.User.LastName : x.ServiceLocation.Name,
                Suffix = x.User.IfNotNull(y => y.Suffix),
                DisplayName = x.User != null ? x.User.DisplayName : null
            });

            _orderingProviderMapper = mapperFactory.CreateMapper<ExternalProvider, CareTeamProviderViewModel>(x => new CareTeamProviderViewModel
            {
                Id = x.Id,
                NPI = x.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi).ToStringIfNotNull("NPI not documented"),
                FirstName = x.FirstName,
                MiddleName = x.MiddleName,
                LastName = x.LastNameOrEntityName,
                Suffix = x.Suffix,
                DisplayName = x.DisplayName
            });

            _invoiceMapper = mapperFactory.CreateMapper<Invoice, InvoiceViewModel>(invoice => new InvoiceViewModel
            {
                Patient = _patientViewModelMapper.Map(invoice.Encounter.Patient),
                Date = invoice.Encounter.StartDateTime,
                CareTeam = _careTeamMapper.Map(invoice),
                ReferringProvider = invoice.ReferringExternalProvider.IfNotNull(_externalProviderNamedViewMapper.Map),
                InvoiceType = _invoiceTypeMapper.Map(invoice.InvoiceType),
                Id = invoice.Id,
                EncounterId = invoice.EncounterId,
                ServiceLocation = invoice.Encounter.ServiceLocation == null ? null : _locationNamedViewMapper.Map(invoice.Encounter.ServiceLocation),
                CodesetType = new NamedViewModel
                {
                    Id = (int)invoice.DiagnosisTypeId,
                    Name = Enum.GetName(typeof(DiagnosisTypeId), (int)invoice.DiagnosisTypeId)
                },
                AttributeTo = invoice.AttributeToServiceLocation == null ? null : _locationNamedViewMapper.Map(invoice.AttributeToServiceLocation),
                ClaimNote = invoice.ClaimComment,
                NextPayers = invoice.Encounter.Patient.PatientInsurances.Where(x => x.IsActiveForDateTime(invoice.Encounter.StartDateTime))
                                                                       .OrderBy(x => x.InsuranceType)
                                                                       .ThenBy(x => x.OrdinalId)
                    .Select(x => _nextInsurancePayMapper.Map(x)).ToObservableCollection(),
                EncounterServices = invoice.BillingServices.OrderBy(x => x.OrdinalId).Select(x => _billingServiceServiceMapper.Map(x)).ToExtendedObservableCollection(),
                EncounterDiagnoses = _diagnosisViewModel,
                InvoiceReceivables = invoice.InvoiceReceivables.Select(x => x.Id).ToObservableCollection(),
                PatientInsuranceAuthorization = invoice.Encounter.PatientInsuranceAuthorizations.Select(x => _authMapper.Map(Tuple.Create(invoice, x))).FirstOrDefault(),
                AppointmentStartDateTime = invoice.Encounter.Appointments.Any() ? invoice.Encounter.Appointments.OfType<UserAppointment>().Select(x => x.DateTime).FirstOrDefault() : invoice.Encounter.StartDateTime,
                InvoiceSupplemental = new InvoiceSupplementalViewModel
                {
                    PlaceOfService = invoice.ServiceLocationCode.IfNotNull(x =>
                        new NameAndAbbreviationAndDetailViewModel
                        {
                            Id = x.Id,
                            Abbreviation = x.PlaceOfServiceCode,
                            Name = x.Name,
                            Detail = string.Format("{0} - {1}", x.PlaceOfServiceCode, x.Name)
                        }, new NameAndAbbreviationAndDetailViewModel
                        {
                            Id = invoice.Encounter.ServiceLocation.ServiceLocationCode.Id,
                            Abbreviation = invoice.Encounter.ServiceLocation.ServiceLocationCode.PlaceOfServiceCode,
                            Name = invoice.Encounter.ServiceLocation.ServiceLocationCode.Name,
                            Detail = string.Format("{0} - {1}", invoice.Encounter.ServiceLocation.ServiceLocationCode.PlaceOfServiceCode, invoice.Encounter.ServiceLocation.ServiceLocationCode.Name)
                        }),
                    RelatedCause1Id = invoice.InvoiceSupplemental.IfNotNull(x => x.RelatedCause1Id),
                    RelatedCause2Id = invoice.InvoiceSupplemental.IfNotNull(x => x.RelatedCause2Id),
                    RelatedCause3Id = invoice.InvoiceSupplemental.IfNotNull(x => x.RelatedCause3Id),
                    AccidentDate = invoice.InvoiceSupplemental.IfNotNull(x => x.AccidentDateTime),
                    StateOrProvince = invoice.InvoiceSupplemental != null ? new NameAndAbbreviationViewModel { Id = invoice.InvoiceSupplemental.AccidentStateOrProvince.IfNotNull(asp => asp.Id), Name = invoice.InvoiceSupplemental.AccidentStateOrProvince.IfNotNull(asp => asp.Name), Abbreviation = invoice.InvoiceSupplemental.AccidentStateOrProvince.IfNotNull(asp => asp.Abbreviation) } : null,
                    ClaimDelayReasonCode = invoice.InvoiceSupplemental.IfNotNull(x => x.ClaimDelayReason.HasValue ? new NamedViewModel { Id = (int)x.ClaimDelayReason, Name = x.ClaimDelayReason.GetDisplayName() } : null),
                    Resubmissions = invoice.InvoiceReceivables.Where(x => x.PatientInsurance != null).Select(x => new ResubmissionViewModel
                    {
                        PayerClaimControlNumber = x.PayerClaimControlNumber,
                        ClaimFrequencyTypeCode = x.ClaimFrequencyTypeCodeId.HasValue ? new NamedViewModel { Id = x.ClaimFrequencyTypeCodeId.Value, Name = string.Format("{0} - {1}", x.ClaimFrequencyTypeCodeId.Value, ((ClaimFrequencyTypeCode)x.ClaimFrequencyTypeCodeId).GetDisplayName()) } : null,
                        NextPayer = _nextInsurancePayMapper.Map(x.PatientInsurance)
                    }).ToExtendedObservableCollection(),
                    HospitalizationRelatedToServicesStartDate = invoice.InvoiceSupplemental.IfNotNull(x => x.AdmissionDateTime),
                    OrderingProvider = invoice.InvoiceSupplemental.IfNotNull(x => x.OrderingProvider.IfNotNull(_orderingProviderMapper.Map)),
                    SupervisingProvider = invoice.InvoiceSupplemental.IfNotNull(x => x.SupervisingProvider.IfNotNull(_supervisingProviderMapper.Map))
                }
            });
        }

        private ObservableCollection<DiagnosisViewModel> GetDiagnosis(DataTable _diagTable)
        {
            var diagnosisViewModelCollection = _createDiagnosisViewModelCollection();
            foreach (DataRow row in _diagTable.Rows)
            {
                var diagnosisViewModel = _createDiagnosisViewModel();
                diagnosisViewModel.ActiveDiagnosisCode = _createDiagnosisCodeViewModel();
                diagnosisViewModel.ActiveDiagnosisCode.Id = Convert.ToInt32(row["Id"]);
                diagnosisViewModel.ActiveDiagnosisCode.Code = row["DiagnosisCode"].ToString();
                diagnosisViewModel.ActiveDiagnosisCode.Description = Convert.ToString(row["DiagnosisDescription"]);
                diagnosisViewModel.ActiveDiagnosisCode.CodeSet = DiagnosisCodeSet.Icd10;
                diagnosisViewModel.IsLinked = Convert.ToBoolean(row["IsLinked"]);

                diagnosisViewModel.DiagnosisLinkPointer = GetLinkedDiagnosis(Convert.ToInt32(row["OrdinalId"]));

                diagnosisViewModelCollection.Add(diagnosisViewModel);
            }
            return diagnosisViewModelCollection;
        }


        private static DiagnosisLinkPointerViewModel GetLinkedDiagnosis(int ordinalId)
        {
            var linkedDiag = new DiagnosisLinkPointerViewModel();

            if (ordinalId < 1 || ordinalId > 12)
            {
                return null;
            }

            var bill = new BillingDiagnosis();
            bill.OrdinalId = ordinalId;
            linkedDiag.PointerLetter = DiagnosisMap.GetDiagnosisLetter(bill);
            linkedDiag.DiagnosisColor = DiagnosisMap.GetDiagnosisHighlightColor(bill);

            return linkedDiag;
        }

        public ObservableCollection<ReferralViewModel> GetPatientExistingReferralInformation(int patientId)
        {
            var fromDatabaseReferrals = PracticeRepository.PatientInsuranceReferrals.Where(r => r.PatientId == patientId).ToList();

            PracticeRepository.AsQueryableFactory().Load(fromDatabaseReferrals,
                r => r.Encounters,
                r => r.InvoiceReceivables,
                r => r.Doctor,
                r => r.Patient,
                r => r.PatientInsurance,
                r => r.PatientInsurance.InsurancePolicy,
                r => r.PatientInsurance.InsurancePolicy.Insurer);

            return fromDatabaseReferrals.Select(r => _referralMapper.Map(r)).ToExtendedObservableCollection();
        }

        public ProvidersDataListsViewModel LoadProviders()
        {
            // prepate external provider list
            var externalProviders = PracticeRepository.ExternalContacts.OfType<ExternalProvider>()
                .Where(x => !x.IsArchived).ToList();

            PracticeRepository.AsQueryableFactory().Load(externalProviders,
            e => e.ExternalContactAddresses.Select(z => new
            {
                z.ExternalContactAddressType,
                z.StateOrProvince
            }),
            e => e.ExternalContactPhoneNumbers.Select(x => new
            {
                x.ExternalContact,
                x.ExternalContactPhoneNumberType
            }),
            e => e.IdentifierCodes.Select(x => x.ExternalProvider)
            );
            var externalProviderList = externalProviders.Select(x => x.IfNotNull(_externalProviderNamedViewMapper.Map)).Distinct().OrderBy(x => x.Abbreviation).ToObservableCollection();

            // prepare ordering provider list
            var orderingProviderList = new ObservableCollection<CareTeamProviderViewModel>();
            externalProviders.ForEach(x => x.IfNotNull(ep => orderingProviderList.Add(_orderingProviderMapper.Map(ep))));
            orderingProviderList = orderingProviderList.Distinct().OrderBy(x => x.LastName).ToObservableCollection();

            var providerListViewModel = new ProvidersDataListsViewModel
            {
                ExternalProviders = externalProviderList,
                OrderingProviders = orderingProviderList
            };
            return providerListViewModel;
        }

        private static BillingServiceTransactionStatus MapBillingServiceTransaction(BillingActionType action)
        {
            switch (action)
            {
                case BillingActionType.ClaimWait:
                case BillingActionType.StatementWait:
                    return BillingServiceTransactionStatus.Wait;
                case BillingActionType.CrossoverClaim:
                    return BillingServiceTransactionStatus.SentCrossOver;
                default:
                    return BillingServiceTransactionStatus.Queued;
            }
        }

        private static MethodSent MapBillingMethodSent(BillingActionType action)
        {
            switch (action)
            {
                case BillingActionType.QueueClaimPaper:
                case BillingActionType.QueueStatement:
                    return MethodSent.Paper;
                default:
                    return MethodSent.Electronic;
            }
        }

        private static void CloseTransaction(BillingServiceTransaction transaction)
        {
            switch (transaction.BillingServiceTransactionStatus)
            {
                case BillingServiceTransactionStatus.Sent:
                    transaction.BillingServiceTransactionStatus = BillingServiceTransactionStatus.ClosedSent;
                    break;
                case BillingServiceTransactionStatus.SentCrossOver:
                    transaction.BillingServiceTransactionStatus = BillingServiceTransactionStatus.ClosedCrossOver;
                    break;
                default:
                    transaction.BillingServiceTransactionStatus = BillingServiceTransactionStatus.ClosedNotSent;
                    break;
            }
        }

        private static bool ValidServiceForTransaction(EncounterServiceViewModel service)
        {
            //We should not bill a service for the following condition, 1: If Action type is LeaveUnbilled, 2 : if charge is 0 and service with insurer is not marked as send bill as zero and, 3:Service balance is 0 or less and nothing is defined as action.
            if ((service.Balance <= 0 && service.BillingAction == null)
                || (service.BillingAction != null && service.BillingAction.Id == (int)BillingActionType.LeaveUnbilled)
                || (service.Charge <= 0 && (!service.Service.IsZeroChargeAllowedOnClaim && service.NextPayer != null && (service.NextPayer.Payer as PatientInsurancePayerViewModel == null || !(service.NextPayer.Payer as PatientInsurancePayerViewModel).SendZeroCharge)))
            )
            {
                return false;
            }
            return true;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void BillInovice(InvoiceViewModel invoice)
        {
            var existingInvoice = LoadInvoice(invoice.Id);

            //Close all existing billing service before billing new.
            var billingServiceTransactions = existingInvoice.BillingServices
                .SelectMany(x => x.BillingServiceTransactions)
                .ToList();

            billingServiceTransactions
                .Where(x => x.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent &&
                x.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedSent &&
                            x.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedCrossOver)
                .ForEach(CloseTransaction);

            // Create new billing service transaction for each encounter service of the invoice
            var resolvedInvoiceReceivables = new List<InvoiceReceivable>();
            foreach (var service in invoice.EncounterServices)
            {
                if (service.Id == null || !ValidServiceForTransaction(service)) continue;

                var payer = service.NextPayer.Payer as PatientInsurancePayerViewModel;
                var invoiceReceivable = GetOrCreateInvoiceReceivable(invoice, payer.IfNotNull(p => (long?)p.PatientInsuranceId), resolvedInvoiceReceivables);

                // Update referral and authorization
                invoiceReceivable.PatientInsuranceReferralId = invoice.Referral != null
                    && invoice.Referral.Insurance != null
                    && invoice.Referral.Insurance.Id == invoiceReceivable.PatientInsuranceId
                    ? invoice.Referral.Id
                    : invoiceReceivable.PatientInsuranceReferralId;

                invoiceReceivable.PatientInsuranceAuthorizationId = invoice.PatientInsuranceAuthorization != null
                    && invoice.PatientInsuranceAuthorization.PatientInsuranceId == invoiceReceivable.PatientInsuranceId
                    ? invoice.PatientInsuranceAuthorization.Id
                    : invoiceReceivable.PatientInsuranceAuthorizationId;

                // Create new billing service transaction
                var transaction = new BillingServiceTransaction
                {
                    DateTime = DateTime.Now.ToClientTime(),
                    InvoiceReceivable = invoiceReceivable,
                    AmountSent = service.Balance,
                    ClaimFileReceiverId = payer.IfNotNull(x => x.ClaimFileReceiverId),
                    BillingServiceTransactionStatus = MapBillingServiceTransaction((BillingActionType)service.BillingAction.Id),
                    MethodSent = MapBillingMethodSent((BillingActionType)service.BillingAction.Id),
                    BillingServiceId = service.Id.Value,
                };
                //After billing open for review should be false for attached invoice receivable.
                invoiceReceivable.OpenForReview = false;
                billingServiceTransactions.Add(transaction);
            }

            // Ensure invoice receivables are no longer open for review, since we billed invoice
            existingInvoice.InvoiceReceivables.ForEach(x => x.OpenForReview = false);

            PracticeRepository.Save(billingServiceTransactions);
        }

        public virtual int SaveInvoice(InvoiceViewModel invoice)
        {
            return !invoice.Id.HasValue ? CreateInvoice(invoice) : EditInvoice(invoice);
        }

        protected virtual int CreateInvoice(InvoiceViewModel invoice)
        {
            using (var work = _unitOfWorkProvider.Create())
            {
                var ordinalId = 0;
                var insuranceType = invoice.InvoiceType.Id == (int)InvoiceType.Vision ? InsuranceType.Vision : InsuranceType.MajorMedical;
                var encounter = new Encounter
                {
                    EncounterStatusId = (int)EncounterStatus.Discharged,
                    EncounterTypeId = invoice.InvoiceType.Id == (int)InvoiceType.Vision
                        ? (int)EncounterTypeId.VisionEncounter
                        : (int)EncounterTypeId.ClinicianEncounter,
                    InsuranceType = insuranceType,
                    PatientId = invoice.Patient.Id,
                    ServiceLocationId = invoice.ServiceLocation.Id,
                    StartDateTime = invoice.Date
                };
                if (invoice.Referral != null)
                {
                    var referral = PracticeRepository.PatientInsuranceReferrals.FirstOrDefault(x => x.Id == invoice.Referral.Id);
                    encounter.PatientInsuranceReferrals = new ExtendedObservableCollection<PatientInsuranceReferral> { referral };
                }
                if (invoice.PatientInsuranceAuthorization != null)
                {
                    var patientInsuranceAuthorization = PracticeRepository.PatientInsuranceAuthorizations.FirstOrDefault(x => x.Id == invoice.PatientInsuranceAuthorization.Id);
                    encounter.PatientInsuranceAuthorizations = new ExtendedObservableCollection<Model.PatientInsuranceAuthorization> { patientInsuranceAuthorization };
                }

                var serviceLocationCodeId = invoice.ServiceLocation.CodeId;
                if (InvoiceType.Vision == (InvoiceType)invoice.InvoiceType.Id)
                {
                    ServiceLocationCode serviceLocationForVisionType = PracticeRepository.ServiceLocationCodes.FirstOrDefault(x => x.Name.ToLower() == "home");
                    if (serviceLocationForVisionType != null)
                    {
                        serviceLocationCodeId = serviceLocationForVisionType.Id;
                    }
                }

                var dbInvoice = new Invoice
                {
                    ServiceLocationCodeId = invoice.InvoiceSupplemental.IfNotNull(x => x.PlaceOfService.IfNotNull(pos => pos.Id, serviceLocationCodeId), serviceLocationCodeId),
                    InvoiceType = (InvoiceType)invoice.InvoiceType.Id,
                    BillingProviderId = invoice.CareTeam.RenderingProvider.Id,
                    AttributeToServiceLocationId = invoice.AttributeTo.Id,
                    ReferringExternalProviderId = invoice.ReferringProvider != null ? invoice.ReferringProvider.Id : (int?)null,
                    Encounter = encounter,
                    DiagnosisTypeId = invoice.CodesetType.Id
                };

                if (!invoice.InvoiceSupplemental.IsEmpty)
                {
                    dbInvoice.InvoiceSupplemental = new InvoiceSupplemental
                    {
                        AccidentDateTime = invoice.InvoiceSupplemental.AccidentDate,
                        AccidentStateOrProvinceId = invoice.InvoiceSupplemental.StateOrProvince.IfNotNull(x => x.Id, null),
                        ClaimDelayReason = invoice.InvoiceSupplemental.ClaimDelayReasonCode != null ? (ClaimDelayReason?)invoice.InvoiceSupplemental.ClaimDelayReasonCode.IfNotNull(x => x.Id) : null,
                        RelatedCause1Id = invoice.InvoiceSupplemental.RelatedCause1Id,
                        RelatedCause2Id = invoice.InvoiceSupplemental.RelatedCause2Id,
                        RelatedCause3Id = invoice.InvoiceSupplemental.RelatedCause3Id,
                        AdmissionDateTime = invoice.InvoiceSupplemental.HospitalizationRelatedToServicesStartDate,
                        OrderingProviderId = (invoice.InvoiceSupplemental.OrderingProvider != null ? invoice.InvoiceSupplemental.OrderingProvider.Id : (int?)null),
                        SupervisingProviderId = invoice.InvoiceSupplemental.SupervisingProvider != null ? invoice.InvoiceSupplemental.SupervisingProvider.Id : (int?)null
                    };
                }

                if (invoice.ClaimNote.IsNotNullOrEmpty())
                {
                    dbInvoice.ClaimComment = invoice.ClaimNote;
                }
                if (invoice.CodesetType.Id == 1)
                {
                    foreach (var diagnoses in invoice.EncounterDiagnoses)
                    {
                        ordinalId += 1;
                        var billingDiagnosis = new BillingDiagnosis
                        {
                            OrdinalId = ordinalId,
                            ExternalSystemEntityMappingId = diagnoses.ActiveDiagnosisCode.Id
                        };
                        dbInvoice.BillingDiagnoses.Add(billingDiagnosis);
                    }
                }

                ordinalId = 0;


                var modifiersIds = invoice.EncounterServices
                    .SelectMany(x => x.Modifiers.Select(z => z.Id))
                    .ToArray();

                //Retrieve only used modifiers in Invoice. // modified here x.ServiceModifierId
                var cachedModifiers = PracticeRepository.BillingServiceModifiers
                   .Where(x => modifiersIds.Contains(x.ServiceModifierId)).ToList();


                foreach (var service in invoice.EncounterServices)
                {
                    ordinalId += 1;
                    var newService = new BillingService
                    {
                        Unit = service.Units,
                        UnitCharge = service.Charge,
                        OrdinalId = ordinalId,
                        EncounterServiceId = service.Service.Id,
                        BillingServiceBillingDiagnoses = new ExtendedObservableCollection<BillingServiceBillingDiagnosis>(),
                        BillingServiceModifiers = new ExtendedObservableCollection<BillingServiceModifier>(),
                        IsEmg = service.IsEmg,
                        IsEpsdt = service.IsEpsdt,
                        UnclassifiedServiceDescription = service.UnclassifiedServiceDescription,
                        ClaimComment = service.FreehandNote,
                        Ndc = service.NdcCode,
                        PatientBillDate = service.PatientBillDate,
                        UnitAllowableExpense = service.Allowed
                        //UnitAllowableExpense = ( service.Service.IsZeroChargeAllowedOnClaim && service.Charge > 0) ? service.Charge : 0
                    };
                    //If there are no diagnosis attached to the service, it should assign all the diagnosis from invoice
                    if (!service.LinkedDiagnoses.Any())
                    {
                        AttachDiagnosis(service, invoice.EncounterDiagnoses);
                    }
                    var linkedDianosisOrndinalId = 0;
                    if (invoice.CodesetType.Id == 1)
                        service.LinkedDiagnoses.ForEach(x => newService.BillingServiceBillingDiagnoses.Add(new BillingServiceBillingDiagnosis { BillingDiagnosis = dbInvoice.BillingDiagnoses.FirstOrDefault(y => y.ExternalSystemEntityMappingId == invoice.EncounterDiagnoses.Single(z => z.DiagnosisLinkPointer.PointerLetter == x.PointerLetter).ActiveDiagnosisCode.Id), OrdinalId = linkedDianosisOrndinalId += 1 }));
                    var linkedModiferOrndinalId = 0;
                    //modified here if null then X.ID
                    service.Modifiers.ForEach(x => newService.BillingServiceModifiers.Add(new BillingServiceModifier { ServiceModifierId = cachedModifiers.FirstOrDefault(y => y.ServiceModifierId == x.Id) == null ? x.Id : cachedModifiers.FirstOrDefault(y => y.ServiceModifierId == x.Id).ServiceModifierId, OrdinalId = linkedModiferOrndinalId += 1 }));


                    dbInvoice.BillingServices.Add(newService);
                    service.OrdinalId = ordinalId;
                }
                var patientInsurances = GetInsurancesForInvoiceReceivable(invoice, encounter);
                // Generating the invoice receivable for all insurances.
                foreach (var patientInsurance in patientInsurances)
                {
                    var invoiceReceivable = CreateInvoiceReceivable(invoice, patientInsurance, patientInsurances.IndexOf(patientInsurance) == 0);
                    dbInvoice.InvoiceReceivables.Add(invoiceReceivable);
                }
                // Generate invoice receivable for the patient, If patient has the insurance then open for review has been handled in statement above else only patient will have open for review always true.
                var patientInvoiceReceivable = CreateInvoiceReceivable(invoice, null, !patientInsurances.Any());
                dbInvoice.InvoiceReceivables.Add(patientInvoiceReceivable);


                PracticeRepository.Save(dbInvoice);
                work.AcceptChanges();

                if (invoice.CodesetType.Id == 2)
                {
                    ordinalId = 0;
                    foreach (var diagnoses in invoice.EncounterDiagnoses)
                    {
                        ordinalId += 1;
                        var query = "INSERT INTO Model.BillingDiagnosisIcd10 (InvoiceId, OrdinalId, DiagnosisCode, DiagnosisDescription) SELECT TOP 1 " + dbInvoice.Id + "," + ordinalId + ",'" + diagnoses.ActiveDiagnosisCode.Code + "','" + (diagnoses.ActiveDiagnosisCode.Description ?? "").Replace("'", "''") + "'" +
                                    " FROM Model.BillingDiagnosisIcd10 WHERE NOT EXISTS (SELECT 1 FROM Model.BillingDiagnosisIcd10 diag WHERE diag.InvoiceId = " + dbInvoice.Id + " AND diag.DiagnosisCode = '" + diagnoses.ActiveDiagnosisCode.Code + "')";
                        using (var dbConnection = DbConnectionFactory.PracticeRepository)
                        {
                            dbConnection.Execute(query);
                        }
                    }
                    foreach (var service in invoice.EncounterServices)
                    {
                        var dbService = dbInvoice.BillingServices.First(x => x.EncounterServiceId == service.Service.Id && x.OrdinalId == service.OrdinalId);

                        var diagCodes = invoice.EncounterDiagnoses.Select(i => i.ActiveDiagnosisCode.Code).ToArray().Join("','");
                        var diagIdQuery = "Select Distinct Id, DiagnosisCode, OrdinalId, DiagnosisDescription from Model.BillingDiagnosisIcd10(NOLOCK) where invoiceId = " + dbInvoice.Id + " And DiagnosisCode in ( '" + diagCodes + "' )";
                        using (var dbConnection = DbConnectionFactory.PracticeRepository)
                        {
                            var diagCodeTable = dbConnection.Execute<DataTable>(diagIdQuery);
                            var billingDiagnosisCodes = MapBillingDiagnosis(diagCodeTable);
                            var diagnosis = new List<BillingDiagnosisCode>();
                            service.LinkedDiagnoses.ForEach(x =>
                            {
                                diagnosis.Add(billingDiagnosisCodes.FirstOrDefault(i => i.OrdinalId == _diagnosisLetters.IndexOf(x.PointerLetter) + 1));

                            });

                            int index = 1;
                            var newBillingServiceItems = new List<BillingServiceBillingDiagnosis>();

                            diagnosis.ForEach(x =>
                            {
                                newBillingServiceItems.Add(new BillingServiceBillingDiagnosis
                                {
                                    BillingDiagnosisId = x.DiagnosisId,
                                    OrdinalId = index,
                                    BillingServiceId = dbService.Id
                                });

                                index += 1;
                            });

                            foreach (var servdiag in newBillingServiceItems)
                            {
                                var query = "Insert Into Model.BillingServiceBillingDiagnosis (BillingServiceId, BillingDiagnosisId, OrdinalId, IsIcd10) Values (" + servdiag.BillingServiceId + "," + servdiag.BillingDiagnosisId + "," + servdiag.OrdinalId + ", 1)";
                                dbConnection.Execute(query);
                            }
                        }
                    }
                }

                return dbInvoice.Id;
            }
        }

        private static void AttachDiagnosis(EncounterServiceViewModel service, IEnumerable<DiagnosisViewModel> encounterDiagnoses)
        {
            encounterDiagnoses.ForEach(x =>
            {
                switch (x.DiagnosisLinkPointer.PointerLetter)
                {
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                        service.LinkedDiagnoses.Add(x.DiagnosisLinkPointer);
                        break;
                }

            });
        }

        private List<PatientInsurance> GetInsurancesForInvoiceReceivable(InvoiceViewModel invoice, Encounter encounter)
        {
            var patientInsurances = PracticeRepository.PatientInsurances
                .Where(x => x.InsuredPatientId == invoice.Patient.Id
                            && x.InsurancePolicy != null && encounter.StartDateTime.Date >= x.InsurancePolicy.StartDateTime
                            && (encounter.EndDateTime.Date <= x.EndDateTime || !x.EndDateTime.HasValue)
                ).ToList();

            var majorMedical = patientInsurances.Where(x => x.InsuranceType == InsuranceType.MajorMedical).OrderBy(x => x.OrdinalId).ToList();
            var vision = patientInsurances.Where(x => x.InsuranceType == InsuranceType.Vision).OrderBy(x => x.OrdinalId).ToList();
            var other = patientInsurances.Where(x => x.InsuranceType == InsuranceType.Ambulatory).OrderBy(x => x.OrdinalId).ToList();
            var workersComp = patientInsurances.Where(x => x.InsuranceType == InsuranceType.WorkersComp).OrderBy(x => x.OrdinalId).ToList();

            switch (invoice.InvoiceType.Id)
            {
                case (int)InvoiceType.Professional:
                    return majorMedical.Any() ? majorMedical : vision.Any() ? vision : other.Any() ? other : workersComp.Any() ? workersComp : new List<PatientInsurance>();
                case (int)InvoiceType.Vision:
                    return vision.Any() ? vision : majorMedical.Any() ? majorMedical : new List<PatientInsurance>();
                case (int)InvoiceType.Facility:
                    return majorMedical.Any() ? majorMedical : other.Any() ? other : workersComp.Any() ? workersComp : new List<PatientInsurance>();
            }

            return new List<PatientInsurance>();
        }
        public InvoiceReceivable GetOrCreateInvoiceReceivable(InvoiceViewModel invoice, long? patientInsuranceId, List<InvoiceReceivable> resolvedInvoiceReceivables)
        {
            // See if it has already been resolved
            var invoiceReceivable = resolvedInvoiceReceivables
                .FirstOrDefault(ir => ir.InvoiceId == invoice.Id && ir.PatientInsuranceId == patientInsuranceId);
            if (invoiceReceivable != null)
            {
                return invoiceReceivable;
            }

            // Get from database
            invoiceReceivable = PracticeRepository.InvoiceReceivables
                .Include(ir => ir.PatientInsurance.InsurancePolicy.Insurer)
                .FirstOrDefault(ir => ir.InvoiceId == invoice.Id && ir.PatientInsuranceId == patientInsuranceId);
            if (invoiceReceivable != null)
            {
                resolvedInvoiceReceivables.Add(invoiceReceivable);
                return invoiceReceivable;
            }

            var patientInsurance = patientInsuranceId == null
                ? null
                : PracticeRepository.PatientInsurances
                    .Include(pi => pi.InsurancePolicy.Insurer)
                    .First(pi => pi.Id == patientInsuranceId);

            // Create new if not found.Open for review will always be false as this is at the time of billing .
            invoiceReceivable = CreateInvoiceReceivable(invoice, patientInsurance, false);

            resolvedInvoiceReceivables.Add(invoiceReceivable);
            return invoiceReceivable;
        }

        private static InvoiceReceivable CreateInvoiceReceivable(InvoiceViewModel invoice, PatientInsurance patientInsurance, bool openForeReview)
        {
            var invoiceReceivable = new InvoiceReceivable
            {
                InvoiceId = invoice.Id.GetValueOrDefault(),
                PatientInsurance = patientInsurance,
                OpenForReview = openForeReview,
                PatientInsuranceReferralId = patientInsurance != null
                                             && invoice.Referral != null
                                             && invoice.Referral.Insurance != null
                                             && invoice.Referral.Insurance.Id == patientInsurance.Id
                    ? invoice.Referral.Id : (int?)null,
                PatientInsuranceAuthorizationId = patientInsurance != null
                                                  && invoice.PatientInsuranceAuthorization != null
                                                  && invoice.PatientInsuranceAuthorization.PatientInsuranceId == patientInsurance.Id
                    ? invoice.PatientInsuranceAuthorization.Id : null,
            };
            return invoiceReceivable;
        }

        protected virtual int EditInvoice(InvoiceViewModel invoice)
        {
            using (var work = _unitOfWorkProvider.Create())
            {
                var ordinalId = 0;
                var existingInvoice = LoadInvoice(invoice.Id);
                existingInvoice.AttributeToServiceLocationId = invoice.AttributeTo.Id;
                existingInvoice.BillingProviderId = invoice.CareTeam.RenderingProvider.Id;
                existingInvoice.ServiceLocationCodeId = invoice.InvoiceSupplemental.IfNotNull(x => x.PlaceOfService.IfNotNull(pos => pos.Id, invoice.ServiceLocation.CodeId), invoice.ServiceLocation.CodeId);
                existingInvoice.InvoiceType = (InvoiceType)invoice.InvoiceType.Id;
                existingInvoice.Encounter.InsuranceType = invoice.InvoiceType.Id == (int)InvoiceType.Vision ? InsuranceType.Vision : InsuranceType.MajorMedical;
                existingInvoice.Encounter.ServiceLocationId = invoice.ServiceLocation.Id;
                existingInvoice.Encounter.StartDateTime = invoice.Date;
                existingInvoice.ClaimComment = invoice.ClaimNote;
                existingInvoice.DiagnosisTypeId = invoice.CodesetType.Id;
                existingInvoice.ReferringExternalProviderId = invoice.ReferringProvider != null ? invoice.ReferringProvider.Id : (int?)null;
                if (existingInvoice.InvoiceSupplemental == null && !invoice.InvoiceSupplemental.IsEmpty)
                {
                    existingInvoice.InvoiceSupplemental = new InvoiceSupplemental();
                }
                if (existingInvoice.InvoiceSupplemental != null && !invoice.InvoiceSupplemental.IsEmpty)
                {
                    existingInvoice.InvoiceSupplemental.RelatedCause1Id = invoice.InvoiceSupplemental.RelatedCause1Id;
                    existingInvoice.InvoiceSupplemental.RelatedCause2Id = invoice.InvoiceSupplemental.RelatedCause2Id;
                    existingInvoice.InvoiceSupplemental.RelatedCause3Id = invoice.InvoiceSupplemental.RelatedCause3Id;
                    existingInvoice.InvoiceSupplemental.AccidentStateOrProvinceId = invoice.InvoiceSupplemental.StateOrProvince != null && invoice.InvoiceSupplemental.StateOrProvince.Id > 0 ? invoice.InvoiceSupplemental.StateOrProvince.Id : (int?)null;
                    existingInvoice.InvoiceSupplemental.AccidentDateTime = invoice.InvoiceSupplemental.AccidentDate;
                    existingInvoice.InvoiceSupplemental.ClaimDelayReason = ((ClaimDelayReason?)invoice.InvoiceSupplemental.ClaimDelayReasonCode.IfNotNull(x => x.Id));
                    existingInvoice.ServiceLocationCodeId = invoice.InvoiceSupplemental.PlaceOfService.Id;
                    existingInvoice.InvoiceSupplemental.AdmissionDateTime = invoice.InvoiceSupplemental.HospitalizationRelatedToServicesStartDate;
                    existingInvoice.InvoiceSupplemental.OrderingProviderId = invoice.InvoiceSupplemental.OrderingProvider != null ? invoice.InvoiceSupplemental.OrderingProvider.Id : (int?)null;
                    existingInvoice.InvoiceSupplemental.SupervisingProviderId = invoice.InvoiceSupplemental.SupervisingProvider != null ? invoice.InvoiceSupplemental.SupervisingProvider.Id : (int?)null;
                }

                if (invoice.CareTeam.IsNotManualClaim)
                {
                    var user = existingInvoice.Encounter.Appointments.OfType<UserAppointment>()
                        .Where(x => x.UserId == invoice.CareTeam.ScheduledProvider.Id)
                        .Select(x => x.User)
                        .FirstOrDefault();

                    var newUser = PracticeRepository.Users
                        .Where(x => x.Id == invoice.CareTeam.ScheduledProvider.Id)
                        .Select(x => x).FirstOrDefault();

                    if (user == null && newUser != null &&
                        existingInvoice.Encounter.Appointments.OfType<UserAppointment>().Any())
                    {
                        // ReSharper disable once PossibleNullReferenceException
                        existingInvoice.Encounter.Appointments.OfType<UserAppointment>().FirstOrDefault().UserId = newUser.Id;
                    }
                }
                var diagnosisOrdinalId = 0;
                if (invoice.CodesetType.Id == 1)
                {
                    PracticeRepository.SyncDependencyCollection(invoice.EncounterDiagnoses, existingInvoice.BillingDiagnoses,
                        (source, destination) => source.ActiveDiagnosisCode != null
                                                 && destination.ExternalSystemEntityMapping != null
                                                 && source.ActiveDiagnosisCode.Code.IsNotNullOrEmpty()
                                                 && destination.ExternalSystemEntityMapping.ExternalSystemEntityKey == source.ActiveDiagnosisCode.Code,
                        (source, destination) =>
                        {
                            destination.ExternalSystemEntityMappingId = source.ActiveDiagnosisCode.Id;
                            destination.OrdinalId = ++diagnosisOrdinalId;
                        });
                }


                //set the ordinal id
                foreach (var encounterService in invoice.EncounterServices)
                {
                    existingInvoice.BillingServices.FirstOrDefault(x => x.Id == encounterService.Id &&
                                                                        (x.Unit != encounterService.Units ||
                                                                        x.UnitCharge != encounterService.Charge))
                                                    .IfNotNull(bs => bs.BillingServiceTransactions.Where(x => x.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent &&
                                                                    x.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedSent &&
                                                                    x.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedCrossOver)
                                                    .ForEach(CloseTransaction));
                    encounterService.OrdinalId = ordinalId + 1;
                    ordinalId++;
                }
                //Delete the removed service
                PracticeRepository.SyncDependencyCollection(invoice.EncounterServices, existingInvoice.BillingServices,
                   (source, destination) => source.Id != 0
                                            && destination.Id == source.Id,
                   (source, destination) =>
                   {
                       destination.EncounterServiceId = source.Service.Id;
                       destination.Unit = source.Units;
                       destination.UnitCharge = source.Charge;
                       destination.OrdinalId = source.OrdinalId;
                       destination.UnitAllowableExpense = source.Allowed;
                       destination.PatientBillDate = source.PatientBillDate.HasValue ? source.PatientBillDate.Value : (DateTime?)null;
                       destination.Ndc = source.NdcCode;
                       destination.IsEmg = source.IsEmg;
                       destination.IsEpsdt = source.IsEpsdt;
                       destination.UnclassifiedServiceDescription = source.UnclassifiedServiceDescription;
                       destination.ClaimComment = source.FreehandNote;
                   });

                foreach (var service in invoice.EncounterServices)
                {
                    ordinalId = 0;
                    //Multiple services can have the same encounter service id and the service data also newly added services will not have any id, so reference which will satisfy this condition will have to be updated.
                    var existingService = existingInvoice.BillingServices.First(x => x.EncounterServiceId == service.Service.Id &&
                                                                                        x.OrdinalId == service.OrdinalId &&
                                                                                           x.Id == service.Id.GetValueOrDefault());

                    PracticeRepository.SyncDependencyCollection(service.Modifiers, existingService.BillingServiceModifiers,
                        (source, destination) => source.Id != 0
                                                 && destination.ServiceModifierId == source.Id,
                        (source, destination) => { destination.ServiceModifierId = source.Id; });
                    //set the ordinal id
                    foreach (var modifiers in existingService.BillingServiceModifiers)
                    {
                        modifiers.OrdinalId = ordinalId + 1;
                        ordinalId++;
                    }
                    //If there are no diagnosis attached to the service, it should assign all the diagnosis from invoice
                    if (!service.LinkedDiagnoses.Any())
                    {
                        AttachDiagnosis(service, invoice.EncounterDiagnoses);
                    }

                    var billingServiceBillingDiagnosisOrdinalId = 0;
                    var diagnoses = service.LinkedDiagnoses.Select(x => x.DiagnosisColor);
                    // Select using diagnoses so it keeps the diagnoses order

                    if (invoice.CodesetType.Id == 1)
                    {

                        var billingServicesBillingDiagnosis = diagnoses.SelectMany(d => invoice.EncounterDiagnoses.Where(ed => ed.DiagnosisLinkPointer.DiagnosisColor.Equals(d))).ToArray();

                        PracticeRepository.SyncDependencyCollection(billingServicesBillingDiagnosis, existingService.BillingServiceBillingDiagnoses,
                            (source, destination) => source.AlternateDiagnosisCode != null && source.ActiveDiagnosisCode.Id != 0
                                                     && destination.BillingDiagnosis.ExternalSystemEntityMappingId == source.ActiveDiagnosisCode.Id,
                            (source, destination) =>
                            {
                                destination.BillingDiagnosis = existingInvoice.BillingDiagnoses.FirstOrDefault(x => x.ExternalSystemEntityMappingId == source.ActiveDiagnosisCode.Id);
                                destination.OrdinalId = ++billingServiceBillingDiagnosisOrdinalId;
                            });
                    }
                }


                invoice.InvoiceSupplemental.Resubmissions.ForEach(x =>
                {
                    var receivable = existingInvoice.InvoiceReceivables.FirstOrDefault(i => i.PatientInsurance != null
                                                                                            && i.PatientInsurance.InsurancePolicy.Insurer.Id == x.NextPayer.Payer.Id);
                    if (receivable != null)
                    {
                        receivable.PayerClaimControlNumber = x.PayerClaimControlNumber;
                        if (x.ClaimFrequencyTypeCode != null && x.ClaimFrequencyTypeCode.Id > 0)
                        {
                            receivable.ClaimFrequencyTypeCodeId = x.ClaimFrequencyTypeCode.Id;
                        }
                    }
                });

                PracticeRepository.Save(existingInvoice);

                work.AcceptChanges();

                if (invoice.CodesetType.Id == 2)
                {
                    var oldBillingDiagnosisCodes = new List<BillingDiagnosisCode>();
                    DataTable editDiagTable;
                    string icd10DiagQuery = _icd10Service.GetIcd10DiagnosisCodesQuery(invoice.Id.Value);

                    using (var dbConnection = DbConnectionFactory.PracticeRepository)
                    {
                        editDiagTable = dbConnection.Execute<DataTable>(icd10DiagQuery);
                    }
                    if (editDiagTable != null)
                    {
                        oldBillingDiagnosisCodes = MapBillingDiagnosis(editDiagTable);
                    }

                    var newBillingDiagItems = new List<BillingDiagnosisCode>();
                    var modifiedBillingDiagItems = new List<BillingDiagnosisCode>();
                    var deletedBillingDiagItems = new List<BillingDiagnosisCode>();
                    //var existingItems = new 

                    invoice.EncounterDiagnoses.ForEach(x =>
                    {
                        // for new items
                        if (!(oldBillingDiagnosisCodes.Any(code => code.Code == x.ActiveDiagnosisCode.Code)))
                        {
                            newBillingDiagItems.Add(new BillingDiagnosisCode
                            {
                                Code = x.ActiveDiagnosisCode.Code,
                                OrdinalId = _diagnosisLetters.IndexOf(x.DiagnosisLinkPointer.PointerLetter) + 1,
                                Description = x.ActiveDiagnosisCode.Description
                            });
                        }


                        // for modified items
                        var existing = oldBillingDiagnosisCodes.FirstOrDefault(code => code.Code == x.ActiveDiagnosisCode.Code);
                        if (existing != null)
                        {
                            modifiedBillingDiagItems.Add(new BillingDiagnosisCode
                            {
                                Code = x.ActiveDiagnosisCode.Code,
                                OrdinalId = _diagnosisLetters.IndexOf(x.DiagnosisLinkPointer.PointerLetter) + 1
                            });
                        }
                    });

                    // for deleted items
                    oldBillingDiagnosisCodes.ForEach(x =>
                    {
                        var existing = invoice.EncounterDiagnoses.FirstOrDefault(code => code.ActiveDiagnosisCode.Code == x.Code);
                        if (existing == null)
                        {
                            deletedBillingDiagItems.Add(new BillingDiagnosisCode
                            {
                                Code = x.Code,
                                OrdinalId = x.OrdinalId
                            });
                        }
                    });

                    foreach (var billingDiag in newBillingDiagItems)
                    {
                        var query = "INSERT INTO Model.BillingDiagnosisIcd10 (InvoiceId, OrdinalId, DiagnosisCode, DiagnosisDescription) SELECT TOP 1 " + invoice.Id + "," + billingDiag.OrdinalId + ",'" + billingDiag.Code + "','" + (billingDiag.Description ?? "").Replace("'", "''") + "'" +
                                    " FROM Model.BillingDiagnosisIcd10 WITH(NOLOCK) WHERE NOT EXISTS (SELECT 1 FROM Model.BillingDiagnosisIcd10 diag WITH(NOLOCK) WHERE diag.InvoiceId = " + invoice.Id + " AND diag.DiagnosisCode = '" + billingDiag.Code + "')";
                        using (var dbConnection = DbConnectionFactory.PracticeRepository)
                        {
                            dbConnection.Execute(query);
                        }
                    }

                    foreach (var billingDiag in modifiedBillingDiagItems)
                    {
                        var query = "Update Model.BillingDiagnosisIcd10 Set OrdinalId = " + billingDiag.OrdinalId + " Where InvoiceId = " + invoice.Id + " And DiagnosisCode = '" + billingDiag.Code + "'";
                        using (var dbConnection = DbConnectionFactory.PracticeRepository)
                        {
                            dbConnection.Execute(query);
                        }
                    }

                    foreach (var billingDiag in deletedBillingDiagItems)
                    {
                        var query = "Delete from Model.BillingDiagnosisIcd10 Where InvoiceId = " + invoice.Id + " And DiagnosisCode = '" + billingDiag.Code + "'";
                        using (var dbConnection = DbConnectionFactory.PracticeRepository)
                        {
                            dbConnection.Execute(query);
                        }
                    }

                    foreach (var service in invoice.EncounterServices)
                    {
                        var existingService = existingInvoice.BillingServices.First(x => x.EncounterServiceId == service.Service.Id &&
                        x.OrdinalId == service.OrdinalId
                           );

                        var diagCodes = invoice.EncounterDiagnoses.Select(i => i.ActiveDiagnosisCode.Code).ToArray().Join("','");
                        var diagIdQuery = "Select Distinct Id, DiagnosisCode, OrdinalId, DiagnosisDescription from Model.BillingDiagnosisIcd10(NOLOCK) where invoiceId = " + invoice.Id + " And DiagnosisCode in ( '" + diagCodes + "' )";
                        var diagServQuery = "Select Distinct BillingServiceId, BillingDiagnosisId, OrdinalId From model.BillingServiceBillingDiagnosis(NOLOCK) Where BillingServiceId = " + existingService.Id + " AND isICd10 = 1";
                        using (var dbConnection = DbConnectionFactory.PracticeRepository)
                        {
                            var diagCodeTable = dbConnection.Execute<DataTable>(diagIdQuery);
                            var diagServTable = dbConnection.Execute<DataTable>(diagServQuery);

                            var billingDiagnosisCodes = MapBillingDiagnosis(diagCodeTable);
                            var oldBillingServiceBillingDiagnosis = MapBillingServiceBillingDiagnosis(diagServTable);

                            var diagnosis = new List<BillingDiagnosisCode>();
                            service.LinkedDiagnoses.ForEach(x =>
                            {
                                diagnosis.Add(billingDiagnosisCodes.FirstOrDefault(i => i.OrdinalId == _diagnosisLetters.IndexOf(x.PointerLetter) + 1));

                            });

                            var newBillingServiceItems = new List<BillingServiceBillingDiagnosis>();
                            var modifiedBillingServiceItems = new List<BillingServiceBillingDiagnosis>();
                            var deletedBillingServiceItems = new List<BillingServiceBillingDiagnosis>();
                            //var existingItems = new 
                            int index = 1;
                            //int totCount = diagnosis.Count;
                            diagnosis.ForEach(x =>
                            {
                                // for new items
                                if (!(oldBillingServiceBillingDiagnosis.Any(code => code.BillingDiagnosisId == x.DiagnosisId)))
                                {
                                    newBillingServiceItems.Add(new BillingServiceBillingDiagnosis
                                    {
                                        BillingDiagnosisId = x.DiagnosisId,
                                        OrdinalId = index,
                                        BillingServiceId = existingService.Id
                                    });

                                    index += 1;
                                }


                                // for modified items
                                var existing = oldBillingServiceBillingDiagnosis.FirstOrDefault(code => code.BillingDiagnosisId == x.DiagnosisId);
                                if (existing != null)
                                {
                                    modifiedBillingServiceItems.Add(new BillingServiceBillingDiagnosis
                                    {
                                        BillingDiagnosisId = x.DiagnosisId,
                                        OrdinalId = index,
                                        BillingServiceId = existingService.Id
                                    });
                                    index += 1;
                                }
                            });

                            // for deleted items
                            oldBillingServiceBillingDiagnosis.ForEach(x =>
                            {
                                var existing = diagnosis.FirstOrDefault(code => code.DiagnosisId == x.BillingDiagnosisId);
                                if (existing == null)
                                {
                                    deletedBillingServiceItems.Add(new BillingServiceBillingDiagnosis
                                    {
                                        BillingDiagnosisId = x.BillingDiagnosisId,
                                        OrdinalId = x.OrdinalId,
                                        BillingServiceId = existingService.Id
                                    });
                                }
                            });

                            foreach (var servdiag in newBillingServiceItems)
                            {
                                var query = "Insert Into Model.BillingServiceBillingDiagnosis (BillingServiceId, BillingDiagnosisId, OrdinalId, IsIcd10) Values (" + servdiag.BillingServiceId + "," + servdiag.BillingDiagnosisId + "," + servdiag.OrdinalId + ", 1)";
                                dbConnection.Execute(query);
                            }

                            foreach (var servdiag in modifiedBillingServiceItems)
                            {
                                var query = "Update Model.BillingServiceBillingDiagnosis Set OrdinalId = " + servdiag.OrdinalId + " Where BillingServiceId = " + servdiag.BillingServiceId + " And BillingDiagnosisId = " + servdiag.BillingDiagnosisId + " And IsIcd10 = 1";
                                dbConnection.Execute(query);
                            }

                            foreach (var servdiag in deletedBillingServiceItems)
                            {
                                var query = "Delete From Model.BillingServiceBillingDiagnosis Where BillingServiceId = " + servdiag.BillingServiceId + " And BillingDiagnosisId = " + servdiag.BillingDiagnosisId + " And IsIcd10 = 1";
                                dbConnection.Execute(query);
                            }

                        }

                    }
                }

                //work.AcceptChanges()

                return existingInvoice.Id;
            }
        }

        private static string InsurancePolicyToAbbreviation(PatientInsurance insurance)
        {
            var result = insurance.InsuranceType.GetAttribute<DisplayAttribute>().ShortName + insurance.OrdinalId;
            return result;
        }

        public ObservableCollection<DiagnosisCodeViewModel> GetIcd10Codes(string icd9Code)
        {
            var icd10Codes = _createDiagnosisCodeViewModelCollection();
            //var icd10Code = _createDiagnosisCodeViewModel();
            var icd9 = new IcdModel();
            icd9.Code = icd9Code;

            var icd9Models = new ObservableCollection<IcdModel>();
            icd9Models.Add(icd9);
            ObservableCollection<Icd9To10Model> icd9To10Mappings = _icd9To10Service.GetIcd9To10Models(icd9Models).ToObservableCollection();
            if (icd9To10Mappings.Any())
                icd10Codes = LoadDiagnosisCodes(icd9To10Mappings.First(icd => icd.Icd9 == icd9).Icd10Mappings.ToList()).ToObservableCollection();

            return icd10Codes;

        }

        public ObservableCollection<DiagnosisCodeViewModel> LoadDiagnoses(bool isIcd10, string searchText)
        {
            if (isIcd10)
            {

                ObservableCollection<DiagnosisCodeViewModel> icdCodes = _createDiagnosisCodeViewModelCollection();
                ObservableCollection<IcdModel> icds = _icd9To10Service.GetIcd10Codes(searchText).ToObservableCollection();
                if (icds.Any())
                {
                    foreach (var icd in icds)
                    {
                        var diagnosisCode = _createDiagnosisCodeViewModel();
                        diagnosisCode.Code = icd.Code;
                        diagnosisCode.Description = icd.Description;
                        icdCodes.Add(diagnosisCode);
                    }
                }
                return icdCodes;
            }
            else
            {
                ObservableCollection<DiagnosisCodeViewModel> icdCodes = _createDiagnosisCodeViewModelCollection();
                ObservableCollection<IcdModel> icds = _icd9To10Service.GetIcd9Codes().ToObservableCollection();

                if(icds.Any())
                {
                    foreach(var icd in icds)
                    {
                        var diagnosisCode = _createDiagnosisCodeViewModel();
                        diagnosisCode.Code = icd.Code;
                        diagnosisCode.Description = icd.Description;
                        diagnosisCode.Id = icd.Id;
                        diagnosisCode.Key = icd.key;
                        diagnosisCode.CodeSet = DiagnosisCodeSet.Icd9;
                        icdCodes.Add(diagnosisCode);
                    }
                }
                return icdCodes;



            }
        }

        public EncounterServiceViewModel LoadServiceDetails(int id)
        {
            var service = PracticeRepository.EncounterServices.WithId(id);
            return _billingServiceServiceDetailsMapper.Map(service);
        }



        public virtual List<HtmlDocument> LoadClaim(int invoiceId)
        {
            using (var work = _unitOfWorkProvider.Create())
            {
                var invoice = PracticeRepository.Invoices.FirstOrDefault(x => x.Id == invoiceId &&
                                                                              x.BillingServices.Any(bs => bs.BillingServiceTransactions.Any(bst => bst.MethodSent == MethodSent.Paper
                                                                                                                                                   && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedCrossOver
                                                                                                                                                   && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent
                                                                                                                                                   && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedSent)));
                var documents = new List<HtmlDocument>();
                if (invoice != null)
                {
                    PracticeRepository.AsQueryableFactory().Load(invoice,
                        inv => inv.InvoiceReceivables.Select(x =>
                            new
                            {
                                BillingServiceTransaction = x.BillingServiceTransactions,
                                PatientInsurance = x.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Select(icf => icf.ClaimFormType)
                            })
                        );

                    var generationClaimInfo = _paperClaimGenerationUtilities.GetPaperClaimInformation(invoice);

                    foreach (var paperClaimGenerationInfo in generationClaimInfo)
                    {
                        var claimGenerationInfo = _paperClaimGenerationUtilities.GetPaperClaimGenerationInfo(paperClaimGenerationInfo);

                        var document = (HtmlDocument)_paperClaimsMessageProducer.ProduceMessage(claimGenerationInfo.BillingServiceTransactionIds.ToList(), claimGenerationInfo.ClaimFormTypeId, true);
                        documents.Add(document);
                    }

                    if (invoice.DiagnosisTypeId == 1)
                    {

                        invoice.InvoiceReceivables.ForEach(x => x.BillingServiceTransactions.Where(bst => bst.MethodSent == MethodSent.Paper
                                                                                                          && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedCrossOver
                                                                                                          && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent
                                                                                                          && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedSent)
                            .ForEach(bst => bst.BillingServiceTransactionStatus = BillingServiceTransactionStatus.Sent));

                        work.AcceptChanges();

                    }
                    else
                    {
                        string update = "Update model.BillingServiceTransactions Set BillingServiceTransactionStatusId = " + (int)BillingServiceTransactionStatus.Sent + " Where Id = '{0}' ";
                        invoice.InvoiceReceivables.ForEach(x => x.BillingServiceTransactions.Where(bst => bst.MethodSent == MethodSent.Paper
                                                                                                          && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedCrossOver
                                                                                                          && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent
                                                                                                          && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedSent)
                            .ForEach(bst =>
                            {
                                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                                {
                                    dbConnection.Execute(string.Format(update, bst.Id));
                                }
                            }));

                    }

                }


                return documents;
            }
        }


        public DiagnosisCodeViewModel FetchDiagnosisDescription(DiagnosisViewModel diagnosisViewModel)
        {
            var description = PracticeRepository.ClinicalConditions.Where(cc => cc.Id == diagnosisViewModel.ActiveDiagnosisCode.Key.ToInt()).Select(cc => cc.Name).FirstOrDefault();
            diagnosisViewModel.ActiveDiagnosisCode.Description = description;
            return diagnosisViewModel.ActiveDiagnosisCode;
        }

        private IEnumerable<DiagnosisCodeViewModel> LoadDiagnosisCodes(IEnumerable<IcdModel> icd10List)
        {
            ObservableCollection<DiagnosisCodeViewModel> diagnosisCodes = _createDiagnosisCodeViewModelCollection();

            foreach (IcdModel icd10 in icd10List)
            {
                DiagnosisCodeViewModel diagnosisCode = _createDiagnosisCodeViewModel();
                diagnosisCode.Code = icd10.Code;
                diagnosisCode.Description = icd10.Description;
                diagnosisCodes.Add(diagnosisCode);
            }

            return diagnosisCodes.ToObservableCollection();
        }

        public string GetAdjustmentRate(int insurerId, string code)
        {
            string AdjustmentRate = string.Empty;
            using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(IO.Practiceware.Configuration.ConfigurationManager.ConnectionStrings["PracticeRepository"].ConnectionString))
            {
                con.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("select AdjustmentAllowed from dbo.PracticeFeeSchedules where PlanId =" + insurerId + " and CPT ='" + code + "'and (EndDate > GetDate() or EndDate = '')", con))
                using (System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        AdjustmentRate = reader[0].ToString();
                    }
                }
            }
            return AdjustmentRate;
        }

        public decimal GetServiceCharge(int insurerId, string code)
        {
            int docId = 0;
            int locId = 0;
            decimal serviceCharge = 0;
            decimal adjustmentAllowed = 0;
            decimal fee = 0;
            decimal facilityFee = 0;
            string startDate, endDate;
            startDate = endDate = string.Empty;

            using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(IO.Practiceware.Configuration.ConfigurationManager.ConnectionStrings["PracticeRepository"].ConnectionString))
            {
                con.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("select * from dbo.PracticeFeeSchedules where PlanId =" + insurerId + " and CPT ='" + code + "' and (EndDate > GetDate() or EndDate = '')"  , con))
                using (System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        docId = int.Parse(reader["DoctorId"].ToString());
                        fee = decimal.Parse(reader["Fee"].ToString());
                        startDate = reader["StartDate"].ToString();
                        endDate = reader["EndDate"].ToString();
                        adjustmentAllowed = decimal.Parse(reader["AdjustmentAllowed"].ToStringIfNotNull());
                        locId = int.Parse(reader["LocId"].ToString());
                        facilityFee = decimal.Parse(reader["FacilityFee"].ToString());
                        if (!endDate.IsNotNullOrEmpty() || (DateTime.ParseExact(endDate, "yyyyMMdd", CultureInfo.InvariantCulture) >= DateTime.UtcNow))
                        {
                            if (adjustmentAllowed <= 0)
                            {
                                serviceCharge = fee;
                            }
                            else
                            {
                                serviceCharge = adjustmentAllowed;
                            }
                            if (Convert.ToInt16(locId) > 0 && Convert.ToInt16(locId) < 1000)
                            {
                                serviceCharge = facilityFee > 0 ? facilityFee : (adjustmentAllowed != fee ? fee : adjustmentAllowed);
                            }
                            else
                            {
                                serviceCharge = adjustmentAllowed != fee ? fee : adjustmentAllowed;
                            }
                        }
                    }
                }
            }
            return serviceCharge;
        }

    }
}



