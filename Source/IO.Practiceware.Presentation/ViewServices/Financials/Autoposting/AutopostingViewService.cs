﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using IO.Practiceware.Integration.X12;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Autoposting;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.ViewModels.Financials.PostAdjustments;
using IO.Practiceware.Presentation.ViewServices.Financials.Autoposting;
using IO.Practiceware.Presentation.ViewServices.Financials.PostAdjustments;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using FinancialSourceType = IO.Practiceware.Model.FinancialSourceType;

[assembly: Component(typeof(AutopostingViewService), typeof(AutopostingViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Financials.Autoposting
{
    public interface IAutopostingViewService
    {
        /// <summary>
        /// Loads PracticeRepository.ExternalSystemMessages where the ExternalSystemId = Payer &amp; ExternalSystemMessageTypeId = X12_835 
        /// from the database as remittance messages.
        /// </summary>
        RemittanceMessagesLoadInformation LoadRemittanceMessages(bool includeProcessed = false);

        /// <summary>
        /// Uploads a collection of remittance message view models' metadata to the database as ExternalSystemMessages.
        /// </summary>
        ICollection<RemittanceMessageViewModel> Upload(ICollection<RemittanceMessageUploadArgument> uploadInformations);

        /// <summary>
        /// Loads the remittance message with advices.
        /// </summary>
        /// <param name="remittanceMessageId">The remittance message identifier.</param>
        /// <returns></returns>
        RemittanceMessageViewModel LoadRemittanceMessageWithAdvices(Guid remittanceMessageId);

        /// <summary>
        /// Parse a remittance message and posts it's remittance advices.
        /// </summary>
        /// <param name="remittanceMessageId">The ExternalSystemMessage identifier.</param>
        /// <param name="selectedPaymentDate">The selected payment date from the UI.</param>
        /// <param name="selectedPaymentMethod">The selected payment method from the UI.</param>
        /// <returns></returns>
        RemittanceMessageViewModel PostRemittanceMessage(Guid remittanceMessageId, DateTime? selectedPaymentDate, PaymentMethodViewModel selectedPaymentMethod);
    }

    public class AutopostingViewService : BaseViewService, IAutopostingViewService
    {
        private readonly RemittanceMessageParser _remittanceMessageParser;
        private readonly IPostAdjustmentsService _postAdjustmentsService;
        private readonly IUnitOfWorkProvider _uowProvider;
        private readonly Func<AutopostingUserContext> _createAutopostingUserContext;
        private IMapper<ExternalSystemMessage, RemittanceMessageViewModel> _remittanceMessageFromExternalMessageMapper;
        private IMapper<RemittanceMessage.RemittanceAdvice, RemittanceAdviceViewModel> _remittanceAdviceMapper;
        private readonly Lazy<int> _externalSystemExternalSystemMessageTypeId;
        private IMapper<Tuple<ExternalSystemMessage, RemittanceMessage>, RemittanceMessageViewModel> _remittanceMessageMapper;

        public AutopostingViewService(RemittanceMessageParser remittanceMessageParser,
            IPostAdjustmentsService postAdjustmentsService,
            IUnitOfWorkProvider uowProvider,
            Func<AutopostingUserContext> createAutopostingUserContext)
        {
            _remittanceMessageParser = remittanceMessageParser;
            _postAdjustmentsService = postAdjustmentsService;
            _uowProvider = uowProvider;
            _createAutopostingUserContext = createAutopostingUserContext;

            _externalSystemExternalSystemMessageTypeId = new Lazy<int>(() =>
            {
                var cachedExternalSystemExternalSystemMessageTypeId = PracticeRepository.ExternalSystemExternalSystemMessageTypes
                    .Where(esesmt => esesmt.ExternalSystemId == (int) ExternalSystemId.Payer &&
                                     esesmt.ExternalSystemMessageTypeId == (int) ExternalSystemMessageTypeId.X12_835_Inbound)
                    .Select(esesmt => esesmt.Id).Single();
                return cachedExternalSystemExternalSystemMessageTypeId;
            });

            InitMappers();
        }

        private void InitMappers()
        {
            var mapperFactory = Mapper.Factory;

            _remittanceMessageFromExternalMessageMapper = mapperFactory.CreateMapper<ExternalSystemMessage, RemittanceMessageViewModel>(
                item => new RemittanceMessageViewModel
                {
                    Id = item.Id,
                    IsProcessed = item.ExternalSystemMessageProcessingStateId != (int)ExternalSystemMessageProcessingStateId.Unprocessed,
                    FileName = item.Description,
                    PostingDate = item.CreatedDateTime,
                    Errors = item.Error
                });

            _remittanceAdviceMapper = mapperFactory.CreateMapper<RemittanceMessage.RemittanceAdvice, RemittanceAdviceViewModel>(
                a => new RemittanceAdviceViewModel
                {
                    InsurerName = a.ResolvedInsurerName,
                    Errors = a.Errors
                        .Concat(a.ClaimPayments
                            .SelectMany(cp => cp.Errors))
                        .Concat(a.ClaimPayments
                            .SelectMany(cp => cp.ServicePayments)
                            .SelectMany(cp => cp.Errors))
                        .Concat(a.ClaimPayments
                            .SelectMany(cp => cp.ServicePayments)
                            .SelectMany(cp => cp.ServiceAdjustments)
                            .SelectMany(sa => sa.Errors))
                        .Join(Environment.NewLine, true),
                    Informations = a.Informations.Join(Environment.NewLine, true),
                    RemittanceDate = a.PostingDate,
                    RemittanceCode = a.Code,
                    TotalAmount = a.TotalAmount
                });

            _remittanceMessageMapper = mapperFactory.CreateMapper<Tuple<ExternalSystemMessage, RemittanceMessage>, RemittanceMessageViewModel>(
                item => new RemittanceMessageViewModel
                {
                    Id = item.Item1.Id,
                    IsProcessed = item.Item1.ExternalSystemMessageProcessingStateId != (int)ExternalSystemMessageProcessingStateId.Unprocessed,
                    FileName = item.Item1.Description,
                    PostingDate = item.Item1.CreatedDateTime,
                    Errors = item.Item2.CriticalErrors.Join(Environment.NewLine, true),
                    AreRemittanceAdvicesLoaded = true,
                    RemittanceAdvices = item.Item2.Advices
                        .Select(_remittanceAdviceMapper.Map)
                        .ToList()
                });
        }

        public virtual RemittanceMessagesLoadInformation LoadRemittanceMessages(bool includeProcessed)
        {
            var result = new RemittanceMessagesLoadInformation();

            var messagesQuery = PracticeRepository.ExternalSystemMessages
                .Where(esm => esm.ExternalSystemExternalSystemMessageTypeId == _externalSystemExternalSystemMessageTypeId.Value);

            // Skip processed if selected
            if (!includeProcessed)
            {
                messagesQuery = messagesQuery.Where(m => m.ExternalSystemMessageProcessingStateId != (int) ExternalSystemMessageProcessingStateId.Processed);
            }

            result.Remittances = messagesQuery
                .Select(esm => new
                {
                    esm.Id, 
                    esm.CreatedDateTime, 
                    // Don't load full error message when loading the whole list to improve performance
                    Error = string.IsNullOrEmpty(esm.Error) ? null : "Has errors", 
                    esm.ExternalSystemMessageProcessingStateId,
                    esm.Description
                    // We want to exclude "Value" column
                })
                .ToList()
                .Select(esm => new ExternalSystemMessage
                {
                    Id = esm.Id,
                    CreatedDateTime = esm.CreatedDateTime,
                    Error = esm.Error,
                    ExternalSystemMessageProcessingStateId = esm.ExternalSystemMessageProcessingStateId,
                    Description = esm.Description
                })
                .Select(_remittanceMessageFromExternalMessageMapper.Map)
                .ToExtendedObservableCollection();

            result.PaymentMethods = PracticeRepository.PaymentMethods
                .Where(i => !i.IsArchived)
                .Select(PostAdjustmentsUtilities.PaymentMethodMapper.Map)
                .OrderBy(p => p.OrdinalId).ThenBy(p => p.Name)
                .ToExtendedObservableCollection();

            return result;
        }

        public virtual ICollection<RemittanceMessageViewModel> Upload(ICollection<RemittanceMessageUploadArgument> uploadInformations)
        {
            var externalSystemMessages = new List<ExternalSystemMessage>();

            foreach (var uploadInformation in uploadInformations)
            {
                var message = _remittanceMessageParser.Parse(uploadInformation.Message);
                var hasCriticalErrors = message.CriticalErrors.Any();

                // Create message and mark it as processed if it's with critical errors
                externalSystemMessages.Add(new ExternalSystemMessage
                {
                    Description = uploadInformation.FileName,
                    Value = message.ToXml(null, false),
                    ExternalSystemExternalSystemMessageTypeId = _externalSystemExternalSystemMessageTypeId.Value,
                    ExternalSystemMessageProcessingStateId = hasCriticalErrors
                        ? (int) ExternalSystemMessageProcessingStateId.Processed
                        : (int) ExternalSystemMessageProcessingStateId.Unprocessed,
                    CreatedBy = "Autoposting",
                    ProcessingAttemptCount = hasCriticalErrors ? 1 : 0,
                    CreatedDateTime = uploadInformation.PostingDate,
                    UpdatedDateTime = uploadInformation.PostingDate,
                    Error = hasCriticalErrors ? message.CriticalErrors.Join(Environment.NewLine) : null
                });
            }

            if (externalSystemMessages.Any())
            {
                PracticeRepository.Save(externalSystemMessages);
            }

            var result = externalSystemMessages.Select(_remittanceMessageFromExternalMessageMapper.Map).ToList();
            return result;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual RemittanceMessageViewModel LoadRemittanceMessageWithAdvices(Guid remittanceMessageId)
        {
            var externalSystemMessage = PracticeRepository.ExternalSystemMessages.WithId(remittanceMessageId);
            var message = LoadStoredRemittanceMessage(externalSystemMessage);

            // If not processed yet -> add hints for missing insurers and clean up to not show all errors
            if (externalSystemMessage.ExternalSystemMessageProcessingStateId != (int) ExternalSystemMessageProcessingStateId.Processed
                && !message.CriticalErrors.Any()
                && message.Advices.Any())
            {
                const string missingPayer = @"The payers listed in the file is not mapped to an insurance. Please do the following: 
- Open the Setup Insurers Screen
- Click Remittance Mapping
- Enter any one of the following values: ""{0}"" as the Incoming Payer Id 
- Enter the correct Outgoing Payer Id (See the PayerId column of the Setup Insurers Screen for the insurer)";

                foreach (var remittanceAdvice in message.Advices)
                {
                    // Only insurer mapping warnings and remittance advice level errors should be shown to user until file is posted
                    remittanceAdvice.Informations.Clear();
                    remittanceAdvice.ClaimPayments
                        .ForEach(cp =>
                        {
                            cp.Errors.Clear();
                            cp.ServicePayments.ForEach(sp =>
                            {
                                sp.Errors.Clear();
                                sp.ServiceAdjustments.ForEach(sa => sa.Errors.Clear());
                            });
                        });
                }

                // Resolve insurers for advices
                ResolveInsurers(message.Advices, missingPayer);
            }

            var result = _remittanceMessageMapper.Map(new Tuple<ExternalSystemMessage, RemittanceMessage>(externalSystemMessage, message));
            return result;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual RemittanceMessageViewModel PostRemittanceMessage(Guid remittanceMessageId, DateTime? selectedPaymentDate, PaymentMethodViewModel selectedPaymentMethod)
        {
            var externalSystemMessage = PracticeRepository.ExternalSystemMessages.WithId(remittanceMessageId);
            var message = LoadStoredRemittanceMessage(externalSystemMessage);
            var hasCriticalErrors = message.CriticalErrors.Any();

            if (!hasCriticalErrors)
            {
                PostRemittanceAdvices(message.Advices, selectedPaymentMethod, selectedPaymentDate);
            }
            
            // Mark external system message as processed
            externalSystemMessage.Value = message.ToXml(null, false); // Update message along with posting errors
            externalSystemMessage.ProcessingAttemptCount = externalSystemMessage.ProcessingAttemptCount + 1;
            externalSystemMessage.UpdatedDateTime = DateTime.UtcNow;
            externalSystemMessage.ExternalSystemMessageProcessingStateId = (int) ExternalSystemMessageProcessingStateId.Processed;
            externalSystemMessage.Error = hasCriticalErrors ? message.CriticalErrors.Join(Environment.NewLine) : null;

            PracticeRepository.Save(externalSystemMessage);

            var result = _remittanceMessageMapper.Map(new Tuple<ExternalSystemMessage, RemittanceMessage>(
                externalSystemMessage, message));
            return result;
        }

        private void PostRemittanceAdvices(List<RemittanceMessage.RemittanceAdvice> remittanceAdvices, PaymentMethodViewModel paymentMethod, DateTime? paymentDate)
        {
            // Resolve insurers for each remittance advice
            var remittanceAdvicesMappedToInsurers = ResolveInsurers(remittanceAdvices, "Couldn't resolve insurer by payer identifier: {0}");

            // Map remittance advice to financial batch/payment instrument
            var remittanceAdvicesMappedToPaymentInstruments = MapRemittanceAdvicesToPaymentInstruments(paymentMethod, remittanceAdvicesMappedToInsurers);

            // Perform duplicates check
            foreach (var adviceToPaymentInstrument in remittanceAdvicesMappedToPaymentInstruments)
            {
                var duplicateBatch = _postAdjustmentsService.FindPaymentInstrumentDuplicate(adviceToPaymentInstrument.Value, true);
                if (duplicateBatch != null)
                {
                    adviceToPaymentInstrument.Key.Errors.Add(@"The financial batch (check) has already been posted for insurer id: {0}, check code {1}, with an explanation of benefits date of {2}, and an amount of ${3}."
                        .FormatWith(adviceToPaymentInstrument.Value.Payer.Id, adviceToPaymentInstrument.Value.ReferenceNumber, adviceToPaymentInstrument.Value.CheckDate.ToMMDDYYString(), adviceToPaymentInstrument.Value.PaymentAmount));
                }
            }

            // Post
            var adjustmentInformations = GetServiceAdjustmentsToPost(remittanceAdvices);
            PostServiceAdjustments(paymentDate, adjustmentInformations, remittanceAdvicesMappedToPaymentInstruments);
        }

        private static Dictionary<RemittanceMessage.RemittanceAdvice, PaymentInstrumentViewModel> MapRemittanceAdvicesToPaymentInstruments(PaymentMethodViewModel selectedPaymentMethod, Dictionary<RemittanceMessage.RemittanceAdvice, Insurer> remittanceAdvicesMappedToInsurers)
        {
            var remittanceAdvicesToPaymentInstruments = remittanceAdvicesMappedToInsurers
                .Where(p => p.Value != null)
                .ToDictionary(p => p.Key, p => new PaymentInstrumentViewModel
                {
                    FinancialSourceType = ViewModels.Financials.Common.Transaction.FinancialSourceType.Insurer,
                    Payer = PostAdjustmentsUtilities.InsurerAsPayerMapper.Map(p.Value),
                    Date = DateTime.Now.ToClientTime(),
                    CheckDate = p.Key.PostingDate,
                    PaymentAmount = p.Key.TotalAmount,
                    ReferenceNumber = p.Key.Code,
                    PaymentMethod = selectedPaymentMethod
                });
            return remittanceAdvicesToPaymentInstruments;
        }

        /// <summary>
        /// Resolves insurers for every given advice. Value can be null if no match found
        /// </summary>
        /// <param name="remittanceAdvices">The remittance advices.</param>
        /// <param name="notMatchedErrorFormat">Error to add to remittance advice when we couldn't match insurer.</param>
        /// <returns></returns>
        private Dictionary<RemittanceMessage.RemittanceAdvice, Insurer> ResolveInsurers(IList<RemittanceMessage.RemittanceAdvice> remittanceAdvices, string notMatchedErrorFormat)
        {
            // Cache all insurers that would match payer identifiers mentioned in every advice
            var payerIdentifiers = remittanceAdvices
                .SelectMany(a => a.PayerIdentifiers)
                .Distinct()
                .ToArray();

            var insurersCache = PracticeRepository.Insurers
                .Where(i => payerIdentifiers.Contains(i.PayerCode))
                .Union(PracticeRepository.IncomingPayerCodes
                    .Where(ipc => payerIdentifiers.Contains(ipc.Value))
                    .SelectMany(ipc => ipc.Insurers))
                    .ToArray();

            if (insurersCache.Any())
            {
                PracticeRepository.AsQueryableFactory().Load(insurersCache, i => i.IncomingPayerCodes);
            }

            // Perform actual mapping
            var mapping = remittanceAdvices
                .ToDictionary(a => a, a =>
                {
                    // Match insurer from cache
                    var insurer = insurersCache.FirstOrDefault(i => 
                        a.PayerIdentifiers.Contains(i.PayerCode, StringComparer.OrdinalIgnoreCase)
                        || i.IncomingPayerCodes
                            .Any(ipc => a.PayerIdentifiers
                                .Contains(ipc.Value, StringComparer.OrdinalIgnoreCase)));

                    if (insurer != null)
                    {
                        a.ResolvedInsurerName = insurer.Name;
                    }
                    else if (!string.IsNullOrEmpty(notMatchedErrorFormat))
                    {
                        a.Errors.Add(string.Format(notMatchedErrorFormat, a.PayerIdentifiers.Join()));
                    }
                    return insurer;
                });

            return mapping;
        }

        private IList<ServiceAdjustmentInformation> GetServiceAdjustmentsToPost(IEnumerable<RemittanceMessage.RemittanceAdvice> remittanceAdvices)
        {
            var validRemittanceAdvices = remittanceAdvices.Where(ra => !ra.Errors.Any()).ToArray();

            // Prefetch billing services
            var cache = CacheBillingServicesAndPatients(validRemittanceAdvices);
            var billingServicesCache = cache.Item1;
            var patientsCache = cache.Item2;

            // Cache all known reason codes
            var codesCache = PracticeRepository.ClaimAdjustmentReasonCodes
                .Include(c => c.ClaimAdjustmentReasonAdjustmentTypes).ToArray();

            var result = new List<ServiceAdjustmentInformation>();
            foreach (var remittanceAdvice in validRemittanceAdvices)
            {
                foreach (var claimPayment in remittanceAdvice.ClaimPayments)
                {
                    // Skip invalid claim payments
                    if (claimPayment.Errors.Any()) continue;

                    // Verify patient insurance can be resolved
                    if (!CanMatchPatientInsurance(claimPayment.PatientId.GetValueOrDefault(), remittanceAdvice.PayerIdentifiers, patientsCache))
                    {
                        claimPayment.Errors.Add(string.Format("Can't find a matching insurance policy for patient id {0}.", claimPayment.PatientId));
                        continue;
                    }

                    foreach (var servicePayment in claimPayment.ServicePayments)
                    {
                        // Skip invalid service payments
                        if (servicePayment.Errors.Any()) continue;

                        // Try match billing service
                        var billingServiceLookupErrors = new List<string>();
                        var matchedBillingService = GetMatchingBillingService(servicePayment, claimPayment, false, billingServiceLookupErrors, billingServicesCache)
                            ?? GetMatchingBillingService(servicePayment, claimPayment, true, billingServiceLookupErrors, billingServicesCache);

                        // If no billing service matched -> output all collected errors and skip this payment
                        if (matchedBillingService == null)
                        {
                            servicePayment.Errors.AddRange(billingServiceLookupErrors);
                            continue;
                        }

                        foreach (var serviceAdjustment in servicePayment.ServiceAdjustments)
                        {
                            // Skip invalid adjustments
                            if (serviceAdjustment.Errors.Any()) continue;

                            var information = new ServiceAdjustmentInformation
                            {
                                RemittanceAdvice = remittanceAdvice,
                                ClaimPayment = claimPayment,
                                ServicePayment = servicePayment,
                                ServiceAdjustment = serviceAdjustment,

                                BillingService = matchedBillingService
                            };

                            ResolveFinancialTransactionType(information, codesCache);

                            result.Add(information);
                        }
                    }
                }
            }

            return result;
        }

        private static void ResolveFinancialTransactionType(ServiceAdjustmentInformation information, IEnumerable<ClaimAdjustmentReasonCode> codesCache)
        {
            // Resolve group code
            information.ClaimAdjustmentGroupCode = PostAdjustmentsUtilities.ClaimAdjustmentGroupCodeMappings
                .Where(m => m.Value == information.ServiceAdjustment.GroupCode)
                .Select(m => (ClaimAdjustmentGroupCode?) m.Key)
                .FirstOrDefault();

            // Resolve reason code
            information.ClaimAdjustmentReasonCode = codesCache.FirstOrDefault(c => c.Code == information.ServiceAdjustment.ReasonCode);

            if (information.ClaimAdjustmentReasonCode != null)
            {
                // Get list of transaction types which match reason and group code
                var typesByReasonCode = information.ClaimAdjustmentReasonCode == null
                    ? new ClaimAdjustmentReasonAdjustmentType[0]
                    : information.ClaimAdjustmentReasonCode.ClaimAdjustmentReasonAdjustmentTypes
                        .Where(carat => carat.ClaimAdjustmentReasonCode == information.ClaimAdjustmentReasonCode
                                        && carat.ClaimAdjustmentGroupCode == information.ClaimAdjustmentGroupCode).ToArray();

                // Try to resolve using db mappings
                information.AdjustmentTypeId = typesByReasonCode
                    .Select(carat => (AdjustmentTypeId?) carat.AdjustmentTypeId)
                    .FirstOrDefault();

                if (information.AdjustmentTypeId == null)
                {
                    var financialInformationTypeId = typesByReasonCode
                        .Select(carat => (FinancialInformationTypeId?) carat.FinancialInformationTypeId)
                        .FirstOrDefault();

                    information.FinancialInformationTypeId =
                        financialInformationTypeId ?? FinancialInformationTypeId.Denial;
                }

                // Force Info type for information status codes
                var informationClaimStatusCodes = new[]
                {
                    RemittanceMessage.KnownClaimStatusCode.ProcessedSecondary,
                    RemittanceMessage.KnownClaimStatusCode.ProcessedTertiary,
                    RemittanceMessage.KnownClaimStatusCode.ProcessedSecondaryAutoCrossed,
                    RemittanceMessage.KnownClaimStatusCode.ProcessedTertiaryAutoCrossed
                };
                if (informationClaimStatusCodes.Contains(information.ClaimPayment.ResolvedClaimStatusCode) 
                    && information.AdjustmentTypeId != AdjustmentTypeId.Payment)
                {
                    information.AdjustmentTypeId = null;
                    information.FinancialInformationTypeId = FinancialInformationTypeId.Information;
                }
            }
            else if (information.ServiceAdjustment.IsPrimaryPayment)
            {
                information.AdjustmentTypeId = AdjustmentTypeId.Payment;
            }
            else
            {
                information.FinancialInformationTypeId = FinancialInformationTypeId.Information;
            }
        }

        private void PostServiceAdjustments(DateTime? selectedPaymentDate, IList<ServiceAdjustmentInformation> serviceAdjustmentInformations, Dictionary<RemittanceMessage.RemittanceAdvice, PaymentInstrumentViewModel> remittanceAdvicesToPaymentInstruments)
        {
            // Fetch adjustment types information
            var adjustmentTypeIds = serviceAdjustmentInformations
                .Where(s => s.AdjustmentTypeId != null)
                .Select(s => (int) s.AdjustmentTypeId.GetValueOrDefault())
                .Distinct()
                .ToArray();
            var adjustmentTypes = PracticeRepository.AdjustmentTypes
                .Where(p => adjustmentTypeIds.Contains(p.Id))
                .ToDictionary(p => (AdjustmentTypeId) p.Id, p => p);

            // Post adjustments for every remittance advice
            foreach (var remittanceAdvicesGroup in serviceAdjustmentInformations.GroupBy(sai => sai.RemittanceAdvice))
            {
                var paymentInstrument = remittanceAdvicesToPaymentInstruments[remittanceAdvicesGroup.Key];
                var accounts = new List<AccountViewModel>();
                foreach (var patientsGroup in remittanceAdvicesGroup.GroupBy(g => g.ClaimPayment.PatientId.GetValueOrDefault()))
                {
                    // Map into account record for every patient
                    var account = new AccountViewModel
                    {
                        PatientId = patientsGroup.Key,
                        PayerOverride = new InsurerPayerLookupViewModel {PayerIdentifiers = remittanceAdvicesGroup.Key.PayerIdentifiers},
                        BillingServices = new ExtendedObservableCollection<BillingServiceViewModel>(),
                        OnAccountTransactions = new ExtendedObservableCollection<FinancialTransactionViewModel>()
                    };
                    accounts.Add(account);

                    foreach (var billingServicesGroup in patientsGroup.GroupBy(g => g.BillingService))
                    {
                        // Map into billing service for all billing services of a patient
                        var billingServiceModel = billingServicesGroup.Key;
                        var billingService = PostAdjustmentsUtilities.BillingServiceMapper
                            .Map(billingServiceModel)
                            .Modify(bs =>
                            {
                                bs.Transactions = new ExtendedObservableCollection<FinancialTransactionViewModel>();
                                bs.FilteredAdjustmentsTotal = billingServiceModel.Adjustments.CalculateTotalAmount();
                            });

                        account.BillingServices.Add(billingService);

                        foreach (var adjustmentInformation in billingServicesGroup.ToList())
                        {
                            // Map adjustment/information into financial transaction
                            var information = adjustmentInformation;
                            var transaction = new FinancialTransactionViewModel
                            {
                                FinancialTransactionType = information.AdjustmentTypeId != null
                                    ? (FinancialTransactionTypeViewModel) PostAdjustmentsUtilities.AdjustmentTypeMapper
                                        .Map(adjustmentTypes[(AdjustmentTypeId) information.AdjustmentTypeId])
                                        .Modify(at =>
                                        {
                                            at.GroupCode = information.ClaimAdjustmentGroupCode == null ? null : PostAdjustmentsUtilities.ClaimAdjustmentGroupCodeMapper.Map(information.ClaimAdjustmentGroupCode.Value);
                                            at.ReasonCode = information.ClaimAdjustmentReasonCode.IfNotNull(PostAdjustmentsUtilities.ClaimAdjustmentReasonCodeMapper.Map);
                                        })
                                    : new FinancialInformationTypeViewModel
                                    {
                                        Id = (int) information.FinancialInformationTypeId.GetValueOrDefault(),
                                        GroupCode = information.ClaimAdjustmentGroupCode == null ? null : PostAdjustmentsUtilities.ClaimAdjustmentGroupCodeMapper.Map(information.ClaimAdjustmentGroupCode.Value),
                                        ReasonCode = information.ClaimAdjustmentReasonCode.IfNotNull(PostAdjustmentsUtilities.ClaimAdjustmentReasonCodeMapper.Map),
                                        Name = information.FinancialInformationTypeId.GetValueOrDefault().GetDisplayName()
                                    },
                                Amount = information.ServiceAdjustment.Amount,
                                IncludeCommentOnStatement = false,
                                Date = selectedPaymentDate ?? DateTime.Now.ToClientTime(),
                                InvoiceId = billingService.InvoiceId,
                                InvoiceReceivableId = null, // Resolved via payer
                                ServiceId = billingService.Id,
                                Source = PostAdjustmentsUtilities.FinancialSourceTypeMapper.Map(FinancialSourceType.Insurer)
                            };

                            billingService.Transactions.Add(transaction);
                        }
                    }
                }

                // Create UoW, so that caching, suggest and post share same object context
                using (_uowProvider.Create())
                {
                    // Optimize performance and cache first
                    _postAdjustmentsService.Utilities.CacheForAccounts(accounts);

                    // Take suggestions on billing action
                    var suggestions = _postAdjustmentsService.Suggest(paymentInstrument, accounts);

                    foreach (var p in accounts.SelectMany(a => a.BillingServices.Select(
                        bs => new {Account = a, BillingService = bs})))
                    {
                        // Find service to apply suggestion to
                        var suggestion = suggestions.FirstOrDefault(s => s.AccountId == p.Account.PatientId
                                                                         && s.BillingServiceId == p.BillingService.Id);

                        // Apply suggestion or add error
                        if (suggestion != null && suggestion.To != null && suggestion.Action != null)
                        {
                            p.BillingService.NextPayer = suggestion.To;
                            p.BillingService.BillingAction = suggestion.Action;
                        }
                        else
                        {
                            remittanceAdvicesGroup.Key.Errors.Add(string.Format("Could not bill next party for patient {0} and date of service {1}.", p.Account.PatientId, p.BillingService.Date.ToMMDDYYString()));
                        }
                    }

                    // Post
                    IDictionary<int, ValidationMessage[]> errors;
                    // Separate auto-posted data from manual posting
                    using (_createAutopostingUserContext())
                    {
                        errors = _postAdjustmentsService.Post(paymentInstrument, accounts, new Collection<FinancialTransactionViewModel>());
                    }
                    if (errors.SelectMany(v => v.Value).Any())
                    {
                        // If we couldn't post -> record errors
                        var warnings = errors
                            .Select(x => string.Format("Invoice {0}:{1}{2}",
                                x.Key, Environment.NewLine, x.Value
                                    .Select(v => v.GetDisplayName())
                                    .Join(Environment.NewLine)))
                            .Join(Environment.NewLine);

                        remittanceAdvicesGroup.Key.Errors.Add(warnings);
                    }
                }
            }
        }

        private static bool CanMatchPatientInsurance(int patientId, IEnumerable<string> payerIdentifiers, IEnumerable<Patient> patientsCache)
        {
            // Resolve patient from cache
            var patient = patientsCache.FirstOrDefault(p => p.Id == patientId);
            if (patient == null)
            {
                return false;
            }

            // Verify patient insurance exists
            var canMatch = patient.PatientInsurances.Any(pi =>
                pi.InsurancePolicy.Insurer.IncomingPayerCodes.Any(ipc => payerIdentifiers.Contains(ipc.Value, StringComparer.OrdinalIgnoreCase))
                    || payerIdentifiers.Contains(pi.InsurancePolicy.Insurer.PayerCode, StringComparer.OrdinalIgnoreCase));
            return canMatch;
        }

        private static BillingService GetMatchingBillingService(RemittanceMessage.RemittanceAdviceServicePayment servicePayment, RemittanceMessage.RemittanceAdviceClaimPayment claimPayment, bool bySecondary, List<string> errors, BillingService[] billingServicesCache)
        {
            var procedureIdentifier = bySecondary 
                ? servicePayment.SecondaryProcedureIdentifier 
                : servicePayment.ProcedureIdentifier;

            // No match can occur without procedure identifier or patient id
            if (string.IsNullOrEmpty(procedureIdentifier) 
                || claimPayment.PatientId == null
                || servicePayment.ServiceDate == DateTime.MinValue)
            {
                return null;
            }

            var modifiers = bySecondary
                ? servicePayment.SecondaryProcedureModifiers
                : servicePayment.ProcedureModifiers;

            // Match by metdata
            var matchedBillingServices = billingServicesCache
                .Where(bs => bs.Invoice.Encounter.StartDateTime.Date == servicePayment.ServiceDate
                    && bs.Invoice.Encounter.PatientId == claimPayment.PatientId
                    // We include only the first 5 characters of any EncounterService.Code to be sent to the insurance company
                    && bs.EncounterService.Code.Truncate(5) == procedureIdentifier.Truncate(5)
                    && bs.BillingServiceModifiers.All(bsm => modifiers.Contains(bsm.ServiceModifier.Code)))
                .ToArray();

            // Attempt to additionally make a definitive lookup by billing service id
            var result = matchedBillingServices.FirstOrDefault(mbs => mbs.Id == servicePayment.BillingServiceId);
            if (result == null)
            {
                // We only allow single billing service to match, so output errors in other scenarios 
                if (matchedBillingServices.Length > 1)
                {
                    errors.Add("More than one match found for patient id {0}, service {1} with modifiers {2} for date of service {3}."
                        .FormatWith(claimPayment.PatientId, procedureIdentifier, string.Join(":", modifiers), servicePayment.ServiceDate.ToMMDDYYString()));
                }
                else if (matchedBillingServices.Length == 0)
                {
                    errors.Add("There were no possible match for patientid {0}, service {1} with modifiers {2} for date of service {3}."
                        .FormatWith(claimPayment.PatientId, procedureIdentifier, string.Join(":", modifiers), servicePayment.ServiceDate.ToMMDDYYString()));
                }
                else
                {
                    result = matchedBillingServices.FirstOrDefault();
                }
            }

            return result;
        }

        private Tuple<BillingService[], Patient[]> CacheBillingServicesAndPatients(IList<RemittanceMessage.RemittanceAdvice> remittanceAdvices)
        {
            var receivedBillingServiceIds = remittanceAdvices
                .SelectMany(ra => ra.ClaimPayments.SelectMany(cp => cp.ServicePayments))
                .Where(sp => sp.BillingServiceId != null)
                .Select(sp => sp.BillingServiceId.GetValueOrDefault())
                .Distinct()
                .ToArray();

            var patientIds = remittanceAdvices.SelectMany(a => a.ClaimPayments)
                .Where(cp => cp.PatientId != null)
                .Select(cp => cp.PatientId.GetValueOrDefault())
                .Distinct()
                .ToArray();

            var serviceDates = remittanceAdvices.SelectMany(a => a.ClaimPayments)
                .SelectMany(cp => cp.ServicePayments)
                .Where(sp => sp.ServiceDate != DateTime.MinValue)
                .Select(sp => sp.ServiceDate)
                .Distinct()
                .ToArray();

            var billingServices = PracticeRepository.BillingServices
                .Where(bs => (patientIds.Contains(bs.Invoice.Encounter.PatientId) 
                    && serviceDates.Contains(CommonQueries.GetDatePart(bs.Invoice.Encounter.StartDateTime))
                    && bs.Invoice.InvoiceType != InvoiceType.Facility
                    )
                    || (receivedBillingServiceIds.Contains(bs.Id)
                    && bs.Invoice.InvoiceType != InvoiceType.Facility)
                    )
                .ToArray();
            var patients = PracticeRepository.Patients
                .Where(p => patientIds.Contains(p.Id))
                .ToArray();

            // Prefetch relations
            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(billingServices,
                bs => bs.BillingServiceModifiers.Select(bsm => bsm.ServiceModifier),
                bs => bs.EncounterService,
                bs => bs.Invoice.Encounter,
                bs => bs.Adjustments.Select(a => a.AdjustmentType));
            queryableFactory.Load(patients,
                p => p.PatientInsurances.Select(pi => pi.InsurancePolicy.Insurer.IncomingPayerCodes));

            return new Tuple<BillingService[],Patient[]>(billingServices, patients);
        }
        
        private RemittanceMessage LoadStoredRemittanceMessage(ExternalSystemMessage storedMessage)
        {
            RemittanceMessage message;
            try
            {
                message = storedMessage.Value.FromXml<RemittanceMessage>();
            }
            catch (Exception ex)
            {
                var wrapperException = new ArgumentException("Stored remittance message format is not valid. Ensure you have migrated database to the latest.", ex);
                message = new RemittanceMessage();
                message.SourceX12Message = storedMessage.Value;
                message.CriticalErrors.AddRange(wrapperException.GetExceptions().Select(e => e.Message));
            }
            return message;
        }
 
        private class ServiceAdjustmentInformation
        {
            public RemittanceMessage.RemittanceAdvice RemittanceAdvice { get; set; }
            public RemittanceMessage.RemittanceAdviceClaimPayment ClaimPayment { get; set; }
            public RemittanceMessage.RemittanceAdviceServicePayment ServicePayment { get; set; }
            public RemittanceMessage.RemittanceAdviceServiceAdjustment ServiceAdjustment { get; set; }
            
            public BillingService BillingService { get; set; }

            public ClaimAdjustmentGroupCode? ClaimAdjustmentGroupCode { get; set; }
            public ClaimAdjustmentReasonCode ClaimAdjustmentReasonCode { get; set; }
            public AdjustmentTypeId? AdjustmentTypeId { get; set; }
            public FinancialInformationTypeId? FinancialInformationTypeId { get; set; }
        }
    }
}
