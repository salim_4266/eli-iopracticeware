using System;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Security;
using User = IO.Practiceware.Model.User;

namespace IO.Practiceware.Presentation.ViewServices.Financials.Autoposting
{
    public class AutopostingUserContext : IDisposable
    {
        public const string AutopostingUserNameSuffix = "-Auto";
        private readonly UserPrincipal _originalPrincipal;

        public AutopostingUserContext(IPracticeRepository repository, 
            IUserRepository userRepository,
            IUnitOfWorkProvider unitOfWorkProvider)
        {
            const string currentUserCheckError = "User must be authenticated";

            // Retrieve and validate original principal
            _originalPrincipal = PrincipalContext
                .Current.EnsureNotDefault(currentUserCheckError)
                .Principal.As<UserPrincipal>().EnsureNotDefault(currentUserCheckError);

            // Verify user
            var userDetails = (_originalPrincipal.Identity.User.Tag as UserDetails).EnsureNotDefault(currentUserCheckError);
            if (userDetails.UserName.EndsWith(AutopostingUserNameSuffix))
            {
                throw new InvalidOperationException(string.Format("An autoposting user context is already establish which is unexpected. User Id: {0}, User Name: {1}", userDetails.Id, userDetails.UserName));
            }

            // Get auto posting user
            var autopostingUserName = string.Concat(userDetails.UserName, AutopostingUserNameSuffix)
                .Truncate(16, true); // User name is maximum 16 characters
            var autopostingUser = repository.Users.FirstOrDefault(u => u.UserName == autopostingUserName);
            if (autopostingUser == null)
            {
                // Create user in a separate unit of work (in case we are already inside UoW)
                using (var unitOfWork = unitOfWorkProvider.Create())
                {
                    autopostingUser = new User
                    {
                        UserName = autopostingUserName,
                        IsArchived = true,
                        FirstName = userDetails.FirstName,
                        LastName = (userDetails.LastName + AutopostingUserNameSuffix).Truncate(35, true),
                        DisplayName = (userDetails.DisplayName + AutopostingUserNameSuffix).Truncate(64, true)
                    };
                    repository.Save(autopostingUser);
                    unitOfWork.AcceptChanges();
                }
            }

            // Fetch user for principal (can only be found by Id, since Pid for such user is null)
            var user = userRepository.Users.First(u => u.Id == autopostingUser.Id.ToString());

            // Set new principal
            PrincipalContext.Current.Principal = new UserPrincipal(new UserIdentity(user, true, string.Empty));
        }

        public void Dispose()
        {
            // Restore principal
            PrincipalContext.Current.Principal = _originalPrincipal;
        }
    }
}