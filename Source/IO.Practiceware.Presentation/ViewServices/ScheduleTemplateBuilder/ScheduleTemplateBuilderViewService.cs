﻿using System.Threading;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ScheduleTemplateBuilder;
using IO.Practiceware.Presentation.ViewServices.Common.Configuration;
using IO.Practiceware.Presentation.ViewServices.ScheduleTemplateBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Domain;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using Soaf.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using Enumerable = System.Linq.Enumerable;

[assembly: Component(typeof(ScheduleTemplateBuilderViewService), typeof(IScheduleTemplateBuilderViewService))]

namespace IO.Practiceware.Presentation.ViewServices.ScheduleTemplateBuilder
{
    /// <summary>
    ///   Flag to indicate what action to take when schedule blocks conflict
    /// </summary>
    [DataContract]
    public enum ScheduleBlockConflictResolutionMode
    {
        [EnumMember]
        Abort,
        [EnumMember]
        Merge,
        [EnumMember]
        Replace
    }

    public interface IScheduleTemplateBuilderViewService
    {
        /// <summary>
        ///   Get list of service locations
        ///   Gets the information necessary to load the schedule template builder
        /// </summary>
        /// <returns> List of service locations </returns>
        ObservableCollection<ColoredViewModel> LoadLocations();

        /// <summary>
        ///   Get list of categories
        /// </summary>
        /// <returns> List of categories </returns>
        ObservableCollection<CategoryViewModel> LoadCatogories();

        /// <summary>
        ///   Get list of schedule templates
        /// </summary>
        /// <returns> List of schedule templates </returns>
        ObservableCollection<ScheduleTemplateViewModel> LoadTemplates();

        /// <summary>
        /// Get list of resources
        /// </summary>
        /// <returns>List of resources</returns>
        ObservableCollection<Tuple<string, int>> LoadResources();

        /// <summary>
        ///   Gets the information necessary to load the schedule template builder in parallel
        /// </summary>
        /// <returns> </returns>
        ScheduleTemplateBuilderLoadInformation LoadInformation(ScheduleTemplateBuilderLoadTypes loadTypes);

        /// <summary>
        ///   Get series of items from database composting a schedule template
        /// </summary>
        /// <param name="templateName"> Name of template </param>
        /// <returns> </returns>
        List<ScheduleBlockViewModel> GetTemplate(string templateName);

        /// <summary>
        ///   Save the template to the database using the workspace
        /// </summary>
        /// <param name="templateName"> Name of template </param>
        /// <param name="scheduleBlocks"> The template loaded on screen </param>
        void SaveTemplate(string templateName, List<ScheduleBlockViewModel> scheduleBlocks);

        /// <summary>
        ///   Delete the template from the database - Nuke the template id on existing schedule blocks created using the template
        /// </summary>
        /// <param name="templateName"> Name of template </param>
        void DeleteTemplate(string templateName);

        /// <summary>
        ///   Generate schedule blocks using a template
        /// </summary>
        /// <param name="scheduleBlocks">The template loaded on screen</param>
        /// <param name="mode">Flag to indicate what action to take when schedule blocks conflict</param>
        /// <param name="templateName">Name of template</param>
        /// <param name="dates">Dates that schedule blocks should be created for</param>
        /// <param name="resourceId">Which resource schedule blocks are assigned to</param>
        /// <returns>Flag to trigger request for end-user decision</returns>
        bool TryApplyTemplate(List<ScheduleBlockViewModel> scheduleBlocks, ScheduleBlockConflictResolutionMode mode, string templateName, List<DateTime> dates, int resourceId);
    }

    internal class ScheduleTemplateBuilderViewService : BaseViewService, IScheduleTemplateBuilderViewService
    {
        private readonly Func<ScheduleBlockViewModel> _createScheduleBlockViewModel;
        private readonly Func<ScheduleTemplateViewModel> _createScheduleTemplateViewModel;
        private readonly IRepositoryService _repositoryService;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly ISchedulingConfigurationViewService _schedulingConfigurationViewService;

        public ScheduleTemplateBuilderViewService(ISchedulingConfigurationViewService schedulingConfigurationViewService,
                                                  Func<ScheduleBlockViewModel> createScheduleBlockViewModel,
                                                  Func<ScheduleTemplateViewModel> createScheduleTemplateViewModel,
                                                  IRepositoryService repositoryService,
                                                  IUnitOfWorkProvider unitOfWorkProvider)
        {
            _schedulingConfigurationViewService = schedulingConfigurationViewService;
            _createScheduleBlockViewModel = createScheduleBlockViewModel;
            _createScheduleTemplateViewModel = createScheduleTemplateViewModel;
            _repositoryService = repositoryService;
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        #region IScheduleTemplateBuilderViewService

        public ScheduleTemplateBuilderLoadInformation LoadInformation(ScheduleTemplateBuilderLoadTypes loadTypes)
        {
            ObservableCollection<ColoredViewModel> locations = null;
            ObservableCollection<CategoryViewModel> categories = null;
            ObservableCollection<ScheduleTemplateViewModel> templates = null;
            ObservableCollection<Tuple<string, int>> resources = null;

            var parallelLoadActions = new List<Action>();
            if (loadTypes.HasFlag(ScheduleTemplateBuilderLoadTypes.Categories))
            {
                parallelLoadActions.Add((() => categories = LoadCatogories()));
            }
            if (loadTypes.HasFlag(ScheduleTemplateBuilderLoadTypes.Locations))
            {
                parallelLoadActions.Add((() => locations = LoadLocations()));
            }
            if (loadTypes.HasFlag(ScheduleTemplateBuilderLoadTypes.Resources))
            {
                parallelLoadActions.Add((() => resources = LoadResources()));
            }
            if (loadTypes.HasFlag(ScheduleTemplateBuilderLoadTypes.Templates))
            {
                parallelLoadActions.Add((() => templates = LoadTemplates()));
            }

            if (parallelLoadActions.Any())
            {
                parallelLoadActions.ForAllInParallel(a => a());
                return new ScheduleTemplateBuilderLoadInformation
                {
                    Locations = locations,
                    Categories = categories,
                    Templates = templates,
                    Resources = resources
                };
            }

            return default(ScheduleTemplateBuilderLoadInformation);
        }

        public ObservableCollection<ColoredViewModel> LoadLocations()
        {
            IEnumerable<ColoredViewModel> locationList = from serviceLocation in PracticeRepository.ServiceLocations.ToArray().OrderBy(i => i.Name)
                                                         select new ColoredViewModel
                                                         {
                                                             Id = serviceLocation.Id,
                                                             Name = serviceLocation.ShortName,
                                                             Color = serviceLocation.Color
                                                         };
            return locationList.ToExtendedObservableCollection();
        }

        public ObservableCollection<CategoryViewModel> LoadCatogories()
        {
            IEnumerable<CategoryViewModel> categoryList = from appointmentCategory in PracticeRepository.AppointmentCategories.Where(c => !c.IsArchived).ToArray().OrderBy(i => i.Name)
                                                          select new CategoryViewModel
                                                          {
                                                              Id = appointmentCategory.Id,
                                                              Name = appointmentCategory.Name,
                                                              Color = appointmentCategory.Color
                                                          };
            return categoryList.ToExtendedObservableCollection();
        }

        public ObservableCollection<ScheduleTemplateViewModel> LoadTemplates()
        {
            var templateList = (from template in PracticeRepository.ScheduleTemplates.OrderBy(i => i.Name)
                                select new
                                {
                                    template.Name,
                                    template.Id,
                                }).ToList();

            var newTemplate = _createScheduleTemplateViewModel();
            newTemplate.Id = 0;
            newTemplate.Name = "Create New";
            newTemplate.AcceptChanges();
            var result = new ExtendedObservableCollection<ScheduleTemplateViewModel> { newTemplate };

            foreach (var template in templateList)
            {
                var viewModel = _createScheduleTemplateViewModel();
                viewModel.Name = template.Name;
                viewModel.Id = template.Id;
                viewModel.AcceptChanges();
                result.Add(viewModel);
            }
            return result;
        }

        public ObservableCollection<Tuple<string, int>> LoadResources()
        {
            List<User> userList = PracticeRepository.Users.Where(u => !u.IsArchived && u.IsSchedulable).OrderBy(u => u.UserName).ToList();
            return userList.Select(r => Tuple.Create(r.UserName, r.Id)).ToExtendedObservableCollection();
        }

        public List<ScheduleBlockViewModel> GetTemplate(string templateName)
        {
            if (String.IsNullOrWhiteSpace(templateName)) throw new ApplicationException();

            ScheduleTemplate scheduleTemplate = GetModelTemplate(templateName);

            if (scheduleTemplate == null || !scheduleTemplate.StartTime.HasValue) throw new ApplicationException();

            TimeSpan startTime = scheduleTemplate.StartTime.Value.TimeOfDay;
            var incrementMinutes = new TimeSpan(0, _schedulingConfigurationViewService.GetSchedulingConfiguration().SpanSize.Minutes, 0);

            // Compute appointments
            var scheduleBlocks = new List<ScheduleBlockViewModel>();
            foreach (ScheduleTemplateBlock block in scheduleTemplate.ScheduleTemplateBlocks)
            {
                ScheduleBlockViewModel scheduleBlock = _createScheduleBlockViewModel();
                scheduleBlock.StartTime = startTime;
                scheduleBlock.EndTime = startTime.Add(incrementMinutes);
                scheduleBlock.Comment = block.Comment;
                if (block.ServiceLocation != null)
                {
                    scheduleBlock.Location = new ColoredViewModel { Id = block.ServiceLocation.Id, Name = block.ServiceLocation.ShortName, Color = block.ServiceLocation.Color };
                }

                scheduleBlock.Categories =
                    (block.IsUnavailable ? new[] { new UnavailableCategoryViewModel() }
                         : block.ScheduleTemplateBlockAppointmentCategories.SelectMany(c => Enumerable.Range(0, c.Count).Select(i =>
                                                                                                                                new CategoryViewModel
                                                                                                                                {
                                                                                                                                    Id = c.AppointmentCategoryId,
                                                                                                                                    Name = c.AppointmentCategory.Name,
                                                                                                                                    Color = c.AppointmentCategory.Color
                                                                                                                                }))).ToExtendedObservableCollection();

                scheduleBlocks.Add(scheduleBlock);
                startTime = startTime.Add(incrementMinutes);

                scheduleBlock.CastTo<IChangeTracking>().AcceptChanges();
            }
            return scheduleBlocks;
        }

        [UnitOfWork]
        public virtual void SaveTemplate(string templateName, List<ScheduleBlockViewModel> scheduleBlocks)
        {
            if (scheduleBlocks == null || String.IsNullOrWhiteSpace(templateName)) throw new ApplicationException();

            DeleteTemplateInternal(templateName);

            ScheduleTemplate scheduleTemplate = ToTemplate(templateName, scheduleBlocks);

            PracticeRepository.Save(scheduleTemplate);

            _repositoryService.Persist(_unitOfWorkProvider.Current.ObjectStateEntries, typeof(IPracticeRepository).FullName);
        }

        [UnitOfWork]
        public virtual void DeleteTemplate(string templateName)
        {
            DeleteTemplateInternal(templateName);

            _repositoryService.Persist(_unitOfWorkProvider.Current.ObjectStateEntries, typeof(IPracticeRepository).FullName);
        }

        private void DeleteTemplateInternal(string templateName)
        {
            if (String.IsNullOrWhiteSpace(templateName)) throw new ApplicationException();

            ScheduleTemplate scheduleTemplate = GetModelTemplate(templateName, false);
            if (scheduleTemplate == null) return;

            PracticeRepository.Delete(scheduleTemplate);
        }

        [UnitOfWork]
        public virtual bool TryApplyTemplate(List<ScheduleBlockViewModel> scheduleBlocks, ScheduleBlockConflictResolutionMode mode, string templateName, List<DateTime> dates, int userId)
        {
            using (new TimedScope(s => Debug.WriteLine("Applied template in {0}".FormatWith(s))))
            {
                if (scheduleBlocks == null || String.IsNullOrWhiteSpace(templateName) || dates == null || dates.Count == 0 || userId == 0) throw new ApplicationException();

                var incrementMinutes = _schedulingConfigurationViewService.GetSchedulingConfiguration().SpanSize.Minutes;

                ScheduleTemplate template = ToTemplate(templateName, scheduleBlocks);
                ScheduleTemplate modelTemplate = GetModelTemplate(templateName);
                if (modelTemplate != null) template.Id = modelTemplate.Id;

                List<ScheduleBlock> existingScheduleBlocks = QueryScheduleBlocksForDates(dates)
                    .Include(b => b.ScheduleBlockAppointmentCategories)
                    .OrderBy(b => b.StartDateTime)
                    .OfType<UserScheduleBlock>().Where(u => u.UserId == userId).OfType<ScheduleBlock>().ToList();

                if (mode == ScheduleBlockConflictResolutionMode.Abort && existingScheduleBlocks.Any())
                {
                    return false;
                }
                if (existingScheduleBlocks.Any() && mode == ScheduleBlockConflictResolutionMode.Replace)
                {
                    DeleteFromScheduleWhereNotInTemplate(template, existingScheduleBlocks);
                }

                var result = new List<UserScheduleBlock>();
                foreach (DateTime d in dates)
                {
                    DateTime date = d;
                    result.AddRangeWithSinglePropertyChangedNotification(ToScheduleBlocks(template, existingScheduleBlocks.Where(b => b.StartDateTime.Date == date.Date).ToArray(), date, userId, incrementMinutes));
                }

                ReconcileOrphanAppointments(result.SelectMany(b => b.ScheduleBlockAppointmentCategories).Where(c => c.AppointmentId == null).ToArray());

                PracticeRepository.Save(result);

                using (new SuppressAuditingScope())
                {
                    _repositoryService.Persist(_unitOfWorkProvider.Current.ObjectStateEntries, typeof(IPracticeRepository).FullName);
                }

                return true;
            }
        }

        /// <summary>
        ///   Attempts to assing orphaned appointments to matching schedule blocks.
        /// </summary>
        /// <param name="scheduleBlockAppointmentCategories"> The schedule blocks. </param>
        private void ReconcileOrphanAppointments(ScheduleBlockAppointmentCategory[] scheduleBlockAppointmentCategories)
        {
            Appointment[] appointments;
            // run on separate UnitOfWork to avoid attaching objects to UnitOfWork.
            using (_unitOfWorkProvider.Create())
            {
                appointments = GetOrphanedAppointments(scheduleBlockAppointmentCategories);
            }

            foreach (Appointment a in appointments.Where(a => a.ScheduleBlockAppointmentCategories.Count == 0 &&
                !a.Encounter.EncounterStatus.IsCancelledStatus()))
            {
                Appointment appointment = a;

                ScheduleBlockAppointmentCategory eligibleCategory = scheduleBlockAppointmentCategories
                    .FirstOrDefault(c => c.ScheduleBlock.StartDateTime == appointment.DateTime
                                         && c.ScheduleBlock.ServiceLocationId == a.Encounter.ServiceLocationId
                                         && (c.ScheduleBlock.Is<UserScheduleBlock>() && appointment.Is<UserAppointment>()
                                         && c.ScheduleBlock.CastTo<UserScheduleBlock>().UserId == appointment.CastTo<UserAppointment>().UserId)
                                         && c.AppointmentId == null
                                         && appointment.AppointmentType.AppointmentCategoryId == c.AppointmentCategoryId);

                if (eligibleCategory != null) eligibleCategory.AppointmentId = appointment.Id;
            }
        }

        private Appointment[] GetOrphanedAppointments(IEnumerable<ScheduleBlockAppointmentCategory> scheduleBlockAppointmentCategories)
        {
            List<Expression<Func<Appointment, bool>>> appointmentsPredicates = PracticeRepository.Appointments.CreatePredicateList();
            foreach (var g in scheduleBlockAppointmentCategories
                .GroupBy(i => new { i.ScheduleBlock, i.AppointmentCategoryId }))
            {
                var group = g;
                appointmentsPredicates.Add(a =>
                                           a.DateTime == group.Key.ScheduleBlock.StartDateTime
                                           && a.AppointmentType.AppointmentCategoryId == group.Key.AppointmentCategoryId);
            }

            if (appointmentsPredicates.Count == 0) return new Appointment[0];

            var appointmentsQueue = new BlockingCollection<Appointment>();

            using (new TimedScope(s => Debug.WriteLine("Queried orphaned appointments in {0}".FormatWith(s))))
            using (var scope = new DbViewQueryingOptimizationScope())
            {
                // Limiting degree of parallelism to 2 to prevent halting the server down 
                // as sql query issued well parallelizes itself on Sql Server (will utilize all cores)
                appointmentsPredicates.Batch(200).ForAllInParallel(batch =>
                {
                    // Enable scope on worker thread
                    scope.AttachScopeToThread(Thread.CurrentThread);

                    var temp = PracticeRepository.Appointments
                        .Where(batch.Aggregate((x, y) => x.Or(y)))
                        .ToArray();
                    temp.ForEachWithSinglePropertyChangedNotification(appointmentsQueue.Add);

                    // Detach optimization scope from worker thread
                    scope.DetachScopeFromThread(Thread.CurrentThread);
                }, degreeOfParallelism: 2);
            }

            var appointments = appointmentsQueue.Distinct(EqualityComparer.For<Appointment>((x, y) => x.Id == y.Id)).ToArray();

            PracticeRepository.AsQueryableFactory().Load(appointments,
                                                          a => a.ScheduleBlockAppointmentCategories,
                                                          a => a.AppointmentType,
                                                          a => a.Encounter);
            return appointments;
        }

        private ScheduleTemplate GetModelTemplate(string templateName, bool includeServiceLocation = true)
        {
            IQueryable<ScheduleTemplate> templates = PracticeRepository.ScheduleTemplates
                            .Include(t => t.ScheduleTemplateBlocks.Select(b => b.ScheduleTemplateBlockAppointmentCategories.Select(c => c.AppointmentCategory)));
            if (includeServiceLocation)
            {
                templates = templates.Include(t => t.ScheduleTemplateBlocks.Select(b => b.ServiceLocation));
            }
            return templates.FirstOrDefault(t => t.Name == templateName);
        }

        private static ScheduleTemplate ToTemplate(string templateName, List<ScheduleBlockViewModel> scheduleBlocks)
        {
            DateTime scheduleTemplateStartTime = DateTime.Today;
            if (scheduleBlocks.Count > 0) scheduleTemplateStartTime += scheduleBlocks.Min(s => s.StartTime);

            var scheduleTemplate = new ScheduleTemplate
            {
                Name = templateName,
                StartTime = scheduleTemplateStartTime,
            };

            int counter = 1;
            foreach (ScheduleBlockViewModel scheduleBlock in scheduleBlocks.OrderBy(b => b.StartTime))
            {
                var scheduleTemplateBlock = new ScheduleTemplateBlock
                {
                    OrdinalId = counter++,
                    ScheduleTemplate = scheduleTemplate,
                    IsUnavailable = scheduleBlock.IsUnavailable,
                    ServiceLocationId = scheduleBlock.Location.Id,
                    Comment = scheduleBlock.Comment
                };
                if (!scheduleBlock.IsUnavailable)
                {
                    foreach (IGrouping<int, CategoryViewModel> category in scheduleBlock.Categories.GroupBy(i => i.Id))
                    {
                        scheduleTemplateBlock.ScheduleTemplateBlockAppointmentCategories.Add(
                            new ScheduleTemplateBlockAppointmentCategory
                            {
                                AppointmentCategoryId = category.Key,
                                Count = category.Count()
                            });
                    }
                }
            }
            return scheduleTemplate;
        }

        private IEnumerable<ScheduleBlock> ToScheduleBlocks(ScheduleTemplate template, ScheduleBlock[] existingScheduleBlocks, DateTime date, int userId, int incrementMinutes)
        {
            var scheduleBlocks = new List<ScheduleBlock>();

            foreach (ScheduleTemplateBlock templateBlock in template.ScheduleTemplateBlocks)
            {
                ScheduleBlock currentScheduleBlock = null;

                if (existingScheduleBlocks != null)
                {
                    ScheduleBlock matchingScheduleBlock = existingScheduleBlocks.FirstOrDefault(b => IsMatch(b, templateBlock, incrementMinutes));

                    if (matchingScheduleBlock != null)
                    {
                        // reuse existing
                        currentScheduleBlock = matchingScheduleBlock;
                    }
                    else
                    {
                        var existingScheduleBlockWithSameTime = existingScheduleBlocks.FirstOrDefault(b => templateBlock.ScheduleTemplate.GetStartTime(templateBlock, incrementMinutes).TimeOfDay == b.StartDateTime.TimeOfDay);
                        if (existingScheduleBlockWithSameTime != null) PracticeRepository.Delete(existingScheduleBlockWithSameTime);
                    }
                }
                if (currentScheduleBlock == null)
                {
                    currentScheduleBlock = new UserScheduleBlock
                    {
                        Comment = templateBlock.Comment,
                        IsUnavailable = templateBlock.IsUnavailable,
                        UserId = userId,
                        ServiceLocationId = templateBlock.ServiceLocationId,
                        ScheduleTemplateId = template.Id,
                        StartDateTime = date + template.GetStartTime(templateBlock, incrementMinutes).TimeOfDay,
                    };
                }

                DeleteFromScheduleWhereNotInTemplate(templateBlock, currentScheduleBlock);

                foreach (ScheduleTemplateBlockAppointmentCategory tc in templateBlock.ScheduleTemplateBlockAppointmentCategories)
                {
                    ScheduleTemplateBlockAppointmentCategory templateCategory = tc;

                    while (currentScheduleBlock.ScheduleBlockAppointmentCategories.Count(c => c.AppointmentCategoryId == templateCategory.AppointmentCategoryId) < templateCategory.Count)
                    {
                        currentScheduleBlock.ScheduleBlockAppointmentCategories.Add(new ScheduleBlockAppointmentCategory { AppointmentCategoryId = templateCategory.AppointmentCategoryId });
                    }
                }

                scheduleBlocks.Add(currentScheduleBlock);
            }

            return scheduleBlocks;
        }


        private void DeleteFromScheduleWhereNotInTemplate(ScheduleTemplate template, List<ScheduleBlock> scheduleBlocks)
        {
            var incrementMinutes = _schedulingConfigurationViewService.GetSchedulingConfiguration().SpanSize.Minutes;

            foreach (var b in scheduleBlocks.ToArray())
            {
                var scheduleBlock = b;

                ScheduleTemplateBlock matchingTemplateBlock = template.ScheduleTemplateBlocks.FirstOrDefault(tb => IsMatch(scheduleBlock, tb, incrementMinutes));

                if (matchingTemplateBlock == null)
                {
                    PracticeRepository.Delete(scheduleBlock);
                    scheduleBlocks.Remove(b);
                }
                else
                {
                    scheduleBlock.ScheduleTemplateId = template.Id;
                    // try to reuse block
                    DeleteFromScheduleWhereNotInTemplate(matchingTemplateBlock, scheduleBlock);
                }
            }
        }

        private void DeleteFromScheduleWhereNotInTemplate(ScheduleTemplateBlock templateBlock, ScheduleBlock scheduleBlock)
        {
            foreach (IGrouping<int, ScheduleBlockAppointmentCategory> sbc in scheduleBlock.ScheduleBlockAppointmentCategories.GroupBy(c => c.AppointmentCategoryId))
            {
                IGrouping<int, ScheduleBlockAppointmentCategory> scheduleBlockCategories = sbc;
                ScheduleTemplateBlockAppointmentCategory matchingTemplateCategory = templateBlock
                    .ScheduleTemplateBlockAppointmentCategories
                    .FirstOrDefault(c =>
                                    c.AppointmentCategoryId == scheduleBlockCategories.Key);


                var toDelete = new List<ScheduleBlockAppointmentCategory>();

                if (matchingTemplateCategory == null)
                {
                    toDelete.AddRange(scheduleBlockCategories);
                }
                else if (matchingTemplateCategory.Count < scheduleBlockCategories.Count())
                {
                    // too many for new template...remove extraneous ones
                    toDelete.AddRange(scheduleBlockCategories.Except(scheduleBlockCategories.Take(matchingTemplateCategory.Count)));
                }

                foreach (ScheduleBlockAppointmentCategory item in toDelete)
                {
                    item.ScheduleBlock = null;
                    item.AppointmentId = null;
                    PracticeRepository.Delete(item);
                }
            }
        }

        private static bool IsMatch(ScheduleBlock scheduleBlock, ScheduleTemplateBlock templateBlock, int incrementMinutes)
        {
            return templateBlock.ScheduleTemplate.GetStartTime(templateBlock, incrementMinutes).TimeOfDay == scheduleBlock.StartDateTime.TimeOfDay
                   && templateBlock.IsUnavailable == scheduleBlock.IsUnavailable && templateBlock.ServiceLocationId == scheduleBlock.ServiceLocationId
                   && templateBlock.Comment == scheduleBlock.Comment;
        }

        private IQueryable<ScheduleBlock> QueryScheduleBlocksForDates(IEnumerable<DateTime> dates)
        {
            List<Expression<Func<ScheduleBlock, bool>>> predicates = PracticeRepository.ScheduleBlocks.CreatePredicateList();
            foreach (DateTime d in dates)
            {
                DateTime date = d;
                predicates.Add(b => Queryables.IsOnDate.Invoke(b.StartDateTime, date));
            }
            Expression<Func<ScheduleBlock, bool>> predicate = predicates.Aggregate((x, y) => x.Or(y));
            return PracticeRepository.ScheduleBlocks.Where(predicate);
        }

        #endregion
    }

    [DataContract]
    public class ScheduleTemplateBuilderLoadInformation
    {
        [DataMember]
        [Dependency]
        public ObservableCollection<ColoredViewModel> Locations { get; set; }

        [DataMember]
        [Dependency]
        public ObservableCollection<CategoryViewModel> Categories { get; set; }

        [DataMember]
        [Dependency]
        public ObservableCollection<ScheduleTemplateViewModel> Templates { get; set; }

        [DataMember]
        [Dependency]
        public ObservableCollection<Tuple<string, int>> Resources { get; set; }
    }

    [DataContract]
    [Flags]
    public enum ScheduleTemplateBuilderLoadTypes
    {
        [EnumMember]
        Locations = 1,
        [EnumMember]
        Categories = 2,
        [EnumMember]
        Templates = 4,
        [EnumMember]
        Resources = 8
    }
}