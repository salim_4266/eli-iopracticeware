﻿using IO.Practiceware.Configuration;
using IO.Practiceware.Presentation.ViewServices.Login;
using Soaf;
using Soaf.ComponentModel;
using System;
using System.Data.SqlClient;
using System.Data;

[assembly: Component(typeof(LoginViewService), typeof(ILoginViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Login
{
    public interface ILoginViewService
    {
        /// <summary>
        /// Attempt to open a connection for the current PracticeRepositoryConnectionString in the ConfigurationManager.
        /// Forwards any exception throws from sqlConnection.Open().  This is mainly to test if a connection is good by setting a short timeout (15 seconds)
        /// and throwing any exceptions that occur.
        /// </summary>
        void TryOpenCurrentSqlConnection();
        
        int RunReplicatinScripts();
    }

    public class LoginViewService : BaseViewService, ILoginViewService
    {
        #region ILoginViewService Members

        public int RunReplicatinScripts()
        {
            int state = 0;
            var distributionCheckConnectionString = new SqlConnectionStringBuilder
            {
                ConnectionString = ConfigurationManager.PracticeRepositoryConnectionString
            };

            const string distributionDatabaseExistenceCheckQuery = @"SELECT COUNT(*) FROM master.dbo.sysdatabases WHERE NAME = 'distribution'";

            bool distributionDatabaseExists;

            using (var distributionCheckConnection = new SqlConnection(distributionCheckConnectionString.ToString()))
            {
                distributionCheckConnection.Open();

                using (var sqlCmd = new SqlCommand(distributionDatabaseExistenceCheckQuery, distributionCheckConnection))
                {
                    var result = (int)sqlCmd.ExecuteScalar();

                    distributionDatabaseExists = result > 0;
                }

                distributionCheckConnection.Close();
            }


            if (!distributionDatabaseExists)
            {
                using (var connection = new SqlConnection(ConfigurationManager.PracticeRepositoryConnectionString))
                {
                    try
                    {
                        connection.Open();
                        var replicationCheck = new DataSet();
                        string SQlQuery = string.Empty;
                        SQlQuery = @"Declare @SQLQUERY nvarchar(max)" + Environment.NewLine;
                        SQlQuery += "Declare @publisherName nvarchar(200)" + Environment.NewLine;
                        SQlQuery += "select @publisherName= publisher FROM MSreplication_subscriptions" + Environment.NewLine;
                        SQlQuery += " set @sqlquery= 'EXEC ['+@publisherName+'].distribution..sp_replmonitorhelppublication '''+@publisherName+''',''PracticeRepository''' " + Environment.NewLine;
                        SQlQuery += "exec (@sqlquery)";
                        var dataAdapter = new SqlDataAdapter(SQlQuery, connection);
                        dataAdapter.Fill(replicationCheck);
                        var status = replicationCheck.Tables[0].Rows[0]["status"].ToString();
                        var warning = replicationCheck.Tables[0].Rows[0]["warning"].ToString();
                        if (((status != "1") || (status != "3") || (status != "4")) && warning != "0")
                        {
                            state = 1;
                        }
                        if ((status == "5") || (status == "6"))
                        {
                            state = 1;
                        }
                    }

                    catch
                    {
                        state = 0;
                    }
                }
            }

            return state;
        }
        public void TryOpenCurrentSqlConnection()
        {
            if (ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled || ConfigurationManager.PracticeRepositoryConnectionString.IsNullOrEmpty() || (ConfigurationManager.PracticeRepositoryConnectionString == ConfigurationManager.DefaultPracticeRepositoryConnectionStringKey))
            {
                throw new InvalidOperationException("Configuration is set for application server connections, not direct sql connections");
            }

            var builder = new SqlConnectionStringBuilder();
            builder.ConnectionString = ConfigurationManager.PracticeRepositoryConnectionString; // throws if invalid connection string

            // manually set the timout
            builder.ConnectTimeout = 15;

            using (var conn = new SqlConnection(builder.ToString()))
            {
                conn.Open(); // throws if invalid
            }
        }

        #endregion
      
    }
}