﻿using System.Collections.Generic;
using IO.Practiceware.Integration.Cda;

namespace IO.Practiceware.Presentation.ViewServices.ClinicalInfo
{
    /// <summary>
    /// A source of mapping medication allergy to view model using IMapper
    /// </summary>
    internal class CdaMedicationAllergyMappingSource
    {
        public int PatientId { get; set; }

        public PracticeEntityIdResolutionRequest ReactionCode { get; set; }
        public PracticeEntityIdResolutionRequest MedicationCode { get; set; }
        public PracticeEntityIdResolutionRequest SeverityCode { get; set; }

        public POCD_MT000040Act DrugAllergyAct { get; set; }

        /// <summary>
        /// An entity id resolution cache
        /// </summary>
        public IDictionary<PracticeEntityIdResolutionRequest, int> ResolvedPracticeEntities { get; set; }
    }
}