﻿using System.Collections.Generic;
using IO.Practiceware.Integration.Cda;

namespace IO.Practiceware.Presentation.ViewServices.ClinicalInfo
{
    /// <summary>
    /// A source of mapping patient problem to view model using IMapper
    /// </summary>
    internal class CdaProblemMappingSource
    {
        public int PatientId { get; set; }

        public PracticeEntityIdResolutionRequest ClinicalCondition { get; set; }

        public POCD_MT000040Act ProblemAct { get; set; }

        /// <summary>
        /// An entity id resolution cache
        /// </summary>
        public IDictionary<PracticeEntityIdResolutionRequest, int> ResolvedPracticeEntities { get; set; }
    }
}