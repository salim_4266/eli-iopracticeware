﻿using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Model;

namespace IO.Practiceware.Presentation.ViewServices.ClinicalInfo
{
    /// <summary>
    /// A request to resolve practice entity id
    /// </summary>
    public class PracticeEntityIdResolutionRequest
    {
        private PracticeEntityIdResolutionRequest()
        {}

        public string ExternalEntityKey { get; protected set; }

        /// <summary>
        /// A lowercased version of external system name
        /// </summary>
        public string ExternalSystemNameLowercase { get; protected set; }

        public PracticeRepositoryEntityId PracticeRepositoryEntityId { get; protected set; }

        /// <summary>
        /// A display name for the entity
        /// Note: we use it from CDA file to avoid additional queries to resolve entity names in DB
        /// </summary>
        public string EntityDisplayName { get; protected set; }

        protected bool Equals(PracticeEntityIdResolutionRequest other)
        {
            return string.Equals(ExternalEntityKey, other.ExternalEntityKey)
                && PracticeRepositoryEntityId == other.PracticeRepositoryEntityId
                && string.Equals(ExternalSystemNameLowercase, other.ExternalSystemNameLowercase);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PracticeEntityIdResolutionRequest)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = ExternalEntityKey.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)PracticeRepositoryEntityId;
                hashCode = (hashCode * 397) ^ ExternalSystemNameLowercase.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(PracticeEntityIdResolutionRequest left, PracticeEntityIdResolutionRequest right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PracticeEntityIdResolutionRequest left, PracticeEntityIdResolutionRequest right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Creates request from CDA code
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="cdaCode"></param>
        /// <returns></returns>
        public static PracticeEntityIdResolutionRequest FromCdaCode(PracticeRepositoryEntityId entityId, CD cdaCode)
        {
            return cdaCode == null || string.IsNullOrWhiteSpace(cdaCode.code)
                ? null
                : new PracticeEntityIdResolutionRequest
                {
                    PracticeRepositoryEntityId = entityId,
                    ExternalEntityKey = cdaCode.code,
                    // External system names shouldn't have spaces or dashes
                    ExternalSystemNameLowercase = cdaCode.GetCodeSystemNameWithLookup()
                        .Replace(" ", string.Empty).Replace("-", string.Empty)
                        .ToLower(),

                    // Provide display name for use
                    EntityDisplayName = cdaCode.displayName
                };
        }
    }
}
