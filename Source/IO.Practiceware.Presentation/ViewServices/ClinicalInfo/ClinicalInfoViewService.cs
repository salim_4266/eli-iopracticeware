﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Auditing;
using IO.Practiceware.Presentation.ViewModels.ClinicalInfo;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.ClinicalInfo;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;

[assembly: Component(typeof(ClinicalInfoViewService), typeof(IClinicalInfoViewService))]

namespace IO.Practiceware.Presentation.ViewServices.ClinicalInfo
{
    public interface IClinicalInfoViewService
    {
        PatientProblemsListLoadInformation LoadEditPatientProblemsList(int patientId);
        ICollection<PatientProblemViewModel> LoadPatientProblemsList(int patientId);
        void SavePatientProblems(ICollection<PatientProblemViewModel> problems);
        ICollection<PatientProblemViewModel> LoadProblemsFromCda(ImportedCdaViewModel cdaFileToImport);
        ICollection<ImportedCdaViewModel> LoadImportedCdaList(int patientId);
        ICollection<PatientMedicationAllergyViewModel> LoadPatientMedicationAllergies(int patientId);
        ICollection<PatientMedicationAllergyViewModel> LoadMedicationAllergiesFromCda(ImportedCdaViewModel cdaFileToImport);
        void SavePatientMedicationAllergies(ICollection<PatientMedicationAllergyViewModel> medicationAllergies);
        ICollection<NamedViewModel> LookupClinicalProblemNames(string clinicalProblemNameSearchText);
        bool HasDischargedAppointments(int patientId);
    }

    [SupportsUnitOfWork]
    public class ClinicalInfoViewService : BaseViewService, IClinicalInfoViewService
    {
        readonly IAuditRepository _auditRepository;
        readonly IUnitOfWorkProvider _unitOfWorkProvider;
        IMapper<ClinicalConditionStatus, NamedViewModel> _clinicalConditionStatusMapper;
        IMapper<Laterality, NamedViewModel> _lateralityMapper;
        IMapper<BodyLocation, NamedViewModel> _bodyLocationMapper;
        IMapper<ClinicalCondition, NamedViewModel> _clinicalConditionMapper;
        IMapper<PatientDiagnosisDetail, PatientProblemViewModel> _patientProblemMapper;
        IMapper<PatientAllergenDetail, PatientMedicationAllergyViewModel> _medicationAllergyMapper;
        IMapper<ExternalSystemMessage, ImportedCdaViewModel> _importedCdaFileMapper;
        IMapper<ClinicalDataSourceType, NamedViewModel> _sourceTypeMapper;
        IMapper<User, NamedViewModel> _userMapper;
        IMapper<Allergen, NamedViewModel> _allergenMapper;
        IMapper<ClinicalQualifier, NamedViewModel> _clinicalQualifierMapper;
        IMapper<CdaMedicationAllergyMappingSource, PatientMedicationAllergyViewModel> _medicationAllergyFromCdaMapper;
        IMapper<CdaProblemMappingSource, PatientProblemViewModel> _problemFromCdaMapper;
        IMapper<AllergenReactionType, NamedViewModel> _allergenReactionTypeMapper;

        public ClinicalInfoViewService(
            IAuditRepository auditRepository,
            IUnitOfWorkProvider unitOfWorkProvider)
        {
            _auditRepository = auditRepository;
            _unitOfWorkProvider = unitOfWorkProvider;
            InitMappers();
        }

        public virtual PatientProblemsListLoadInformation LoadEditPatientProblemsList(int patientId)
        {
            var statusValues = Enums.GetValues<ClinicalConditionStatus>()
                .Select(_clinicalConditionStatusMapper.Map)
                .ToList();

            // Chronicity qualifiers list
            var chronicityValues = PracticeRepository.ClinicalQualifiers
                .Where(c => c.ClinicalQualifierCategory.Name == ClinicalQualifierCategory.Chronicity)
                .ToList()
                .Select(_clinicalQualifierMapper.Map)
                .ToList();

            // Severity qualifiers list
            var severityValues = PracticeRepository.ClinicalQualifiers
                .Where(c => c.ClinicalQualifierCategory.Name == ClinicalQualifierCategory.Severity)
                .ToList()
                .Select(_clinicalQualifierMapper.Map)
                .ToList();

            var lateralityValues = Enums.GetValues<Laterality>()
                .Select(_lateralityMapper.Map)
                .ToList();

            // Fetch list of body locations
            var locationValues = PracticeRepository.BodyLocations
                .Select(_bodyLocationMapper.Map)
                .ToList();

            // Get the list of problems
            var problems = LoadPatientProblemsList(patientId);

            // Prepare result for handover
            var result = new PatientProblemsListLoadInformation
                {
                    Problems = problems.ToList(),
                    ChronicityValues = chronicityValues,
                    SeverityValues = severityValues,
                    LateralityValues = lateralityValues,
                    LocationValues = locationValues,
                    StatusValues = statusValues
                };
            return result;
        }

        public virtual ICollection<PatientProblemViewModel> LoadPatientProblemsList(int patientId)
        {
            var diagnosisDetails = PracticeRepository.PatientDiagnosisDetails
                                             .Where(pdd => pdd.PatientDiagnosis.PatientId == patientId
                                                && pdd.IsOnProblemList)
                                             .Include(
                                                pdd => pdd.PatientDiagnosis.ClinicalCondition,
                                                pdd => pdd.BodyLocation,
                                                pdd => pdd.User,
                                                pdd => pdd.PatientDiagnosis.ClinicalDataSourceType,
                                                pdd => pdd.PatientDiagnosisDetailAxisQualifiers,
                                                pdd => pdd.PatientDiagnosisDetailQualifiers
                                                    .Select(pcq => pcq.ClinicalQualifier.ClinicalQualifierCategory))
                                             .ToList();

            // Convert to view model
            var viewModels = diagnosisDetails.Select(_patientProblemMapper.Map).ToList();

            // Match and update last modified
            var latestChanges = GetLastModifiedStamps<PatientDiagnosisDetail>(
                diagnosisDetails.Select(dd => dd.Id).ToArray());
            foreach (var viewModel in viewModels)
            {
                var auditInfo = latestChanges.FirstOrDefault(p => p.EntityId == viewModel.Id);
                if (auditInfo == null) continue;

                viewModel.LastModifiedDate = auditInfo.Timestamp.ToLocalTime().ToClientTime();
                viewModel.LastModifiedBy = auditInfo.User.IfNotNull(_userMapper.Map);
            }

            return viewModels;
        }

        public virtual ICollection<PatientProblemViewModel> LoadProblemsFromCda(ImportedCdaViewModel cdaFileToImport)
        {
            // Load the file
            var cdaFileSource = PracticeRepository.ExternalSystemMessages
                .Where(m => m.Id == cdaFileToImport.Id)
                .Select(m => m.Value)
                .First();

            // Parse it
            var cdaFile = cdaFileSource.FromXml<POCD_MT000040ClinicalDocument>();

            // Load and process problems
            var mappingSource = new List<CdaProblemMappingSource>();
            foreach (var problem in cdaFile.Problems())
            {
                var observation = problem.PrimaryProblemObservation();
                if (observation == null) continue;

                // Create mapping item
                var mappingItem = new CdaProblemMappingSource
                                      {
                                          PatientId = cdaFileToImport.PatientId,
                                          ProblemAct = problem,

                                          ClinicalCondition = PracticeEntityIdResolutionRequest.FromCdaCode(
                                              PracticeRepositoryEntityId.ClinicalCondition, problem.ClinicalConditionCode())
                                      };

                mappingSource.Add(mappingItem);
            }

            // Resolve practice entities
            var resolutions = ResolvePracticeEntities(mappingSource,
                source => source.ClinicalCondition);
            // Set cache on every mapping source
            mappingSource.ForEach(ms => ms.ResolvedPracticeEntities = resolutions);

            // Map to view models
            var result = mappingSource
                .Select(_problemFromCdaMapper.Map)
                .ToList();

            return result;
        }

        public virtual ICollection<ImportedCdaViewModel> LoadImportedCdaList(int patientId)
        {
            var patientCdaFiles = (from esm in PracticeRepository.ExternalSystemMessages
                                   where
                                       // It's a message of PatientTransitionOfCareCCDA_Inbound type
                                     esm.ExternalSystemExternalSystemMessageType.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Inbound
                                       // The message hasn't yet been fully processed
                                       // TODO: is this correct?
                                     && esm.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Unprocessed
                                       // One of the entities is Patient with supplied Id
                                     && esm.ExternalSystemMessagePracticeRepositoryEntities.Any(pre =>
                                         pre.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Patient
                                         && pre.PracticeRepositoryEntityKey == patientId.ToString())
                                   select new { esm.Id, esm.CreatedDateTime, esm.Description })
                                  .ToList()
                                  .Select(esm => new ExternalSystemMessage
                                      {
                                          Id = esm.Id,
                                          CreatedDateTime = esm.CreatedDateTime,
                                          Description = esm.Description
                                      })
                                  .ToList();

            // Map to view models
            var result = patientCdaFiles
                .Select(_importedCdaFileMapper.Map)
                .ToList();

            // Set patient Ids
            result.ForEach(vm => vm.PatientId = patientId);

            return result;
        }

        public virtual ICollection<PatientMedicationAllergyViewModel> LoadPatientMedicationAllergies(int patientId)
        {
            // Fetch from database
            var reactionDetails = PracticeRepository.PatientAllergenDetails
                .Where(par => par.PatientAllergen.PatientId == patientId
                                && par.PatientAllergen.Allergen.IsAdverseDrugReaction)
                .Include(
                    par => par.PatientAllergen.Allergen,
                    par => par.PatientAllergen.PatientAllergenAllergenReactionTypes.Select(paart => paart.AllergenReactionType),
                    par => par.PatientAllergen.ClinicalDataSourceType,
                    par => par.ClinicalQualifiers.Select(cq => cq.ClinicalQualifierCategory),
                    par => par.User)
                .ToList();

            // Convert to view models
            var viewModels = reactionDetails
                .Select(_medicationAllergyMapper.Map)
                .ToList();

            // Match and update last modified
            var latestChanges = GetLastModifiedStamps<PatientAllergenDetail>(
                reactionDetails.Select(dd => dd.Id).ToArray());
            foreach (var viewModel in viewModels)
            {
                var auditInfo = latestChanges.FirstOrDefault(p => p.EntityId == viewModel.Id);
                if (auditInfo == null)
                {
                    viewModel.LastModifiedDate = viewModel.LoggedDate;
                    viewModel.LastModifiedBy = viewModel.LoggedBy;
                }
                else
                {
                    viewModel.LastModifiedDate = auditInfo.Timestamp;
                    viewModel.LastModifiedBy = auditInfo.User.IfNotNull(_userMapper.Map);
                }
            }

            return viewModels;
        }

        public virtual ICollection<PatientMedicationAllergyViewModel> LoadMedicationAllergiesFromCda(ImportedCdaViewModel cdaFileToImport)
        {
            // Load the file
            var cdaFileSource = PracticeRepository.ExternalSystemMessages
                .Where(m => m.Id == cdaFileToImport.Id)
                .Select(m => m.Value)
                .First();
            
            // Parse it
            var cdaFile = cdaFileSource.FromXml<POCD_MT000040ClinicalDocument>();

            // Load and process medication allergies
            var mappingSource = new List<CdaMedicationAllergyMappingSource>();
            foreach (var drugAllergy in cdaFile.Allergens().DrugAllergies())
            {
                var allergyObservation = drugAllergy.PrimaryAllergyObservation();
                if (allergyObservation == null) continue;

                // Create mapping item
                var mappingItem = new CdaMedicationAllergyMappingSource
                    {
                        PatientId = cdaFileToImport.PatientId,
                        DrugAllergyAct = drugAllergy,

                        MedicationCode = PracticeEntityIdResolutionRequest.FromCdaCode(
                            PracticeRepositoryEntityId.Allergen, drugAllergy.AllergyMedicationCode()),
                        ReactionCode = PracticeEntityIdResolutionRequest.FromCdaCode(
                            PracticeRepositoryEntityId.AllergenReactionType, drugAllergy.AllergenObservationCode()),
                        SeverityCode = PracticeEntityIdResolutionRequest.FromCdaCode(
                            PracticeRepositoryEntityId.ClinicalQualifier, drugAllergy.SeverityObservationOfAllergenCode())
                    };

                mappingSource.Add(mappingItem);
            }

            // Resolve practice entities
            var resolutions = ResolvePracticeEntities(mappingSource,
                source => source.MedicationCode,
                source => source.ReactionCode,
                source => source.SeverityCode);
            // Set cache on every mapping source
            mappingSource.ForEach(ms => ms.ResolvedPracticeEntities = resolutions);

            // Map to view models
            var result = mappingSource.Where(m=> m.MedicationCode != null ||  m.ReactionCode != null ||  m.SeverityCode != null)
                .Select(_medicationAllergyFromCdaMapper.Map)
                .ToList();

            return result;
        }

        public virtual ICollection<NamedViewModel> LookupClinicalProblemNames(string clinicalProblemNameSearchText)
        {
            // Search in clinical conditions in Name column
            var matches = PracticeRepository
                .ClinicalConditions
                .Where(cc => cc.Name.Contains(clinicalProblemNameSearchText))
                .OrderBy(cc => cc.Name)
                .ToList();

            // Map and return results
            var result = matches
                .Select(_clinicalConditionMapper.Map)
                .ToList();

            return result;
        }

        public bool HasDischargedAppointments(int patientId)
        {
            return PracticeRepository.Patients.Include(p => p.Encounters).WithId(patientId).Encounters.Any(e => e.EncounterStatusId == (int)EncounterStatus.Discharged);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SavePatientMedicationAllergies(ICollection<PatientMedicationAllergyViewModel> medicationAllergies)
        {
            var toUpdate = new Dictionary<PatientMedicationAllergyViewModel, PatientAllergenDetail>(medicationAllergies.Count);

            // Fetch all qualifiers mentioned (so that we can set them later)
            var qualifierIds = medicationAllergies
                .Where(ma => ma.Severity != null)
                .Select(ma => ma.Severity.Id)
                .ToList();
            var qualifiers = PracticeRepository.ClinicalQualifiers
                .Where(cq => qualifierIds.Contains(cq.Id))
                .ToList();

            // Prepare records for new adverse reactions
            foreach (var viewModel in medicationAllergies.Where(p => !p.Id.HasValue))
            {
                var reactionDetail = new PatientAllergenDetail();
                reactionDetail.EnteredDateTime = viewModel.LoggedDate;
                reactionDetail.UserId = viewModel.LoggedBy.Id;

                reactionDetail.PatientAllergen = new PatientAllergen();
                reactionDetail.PatientAllergen.PatientId = viewModel.PatientId;
                reactionDetail.PatientAllergen.ClinicalDataSourceTypeId = viewModel.SourceType == null
                    ? (int)ClinicalDataSourceTypeId.PracticeProvider
                    : viewModel.SourceType.Id;

                // Add to update list
                toUpdate.Add(viewModel, reactionDetail);
            }

            // Fetch existing reactions
            var existingReactionIds = medicationAllergies
                .Where(p => p.Id.HasValue)
                .Select(p => p.Id.EnsureNotDefault().Value)
                .ToArray();
            var existingReactions = PracticeRepository.PatientAllergenDetails
                .Where(p => existingReactionIds.Contains(p.Id))

                // Include pre loadings
                .Include(
                    par => par.PatientAllergen.PatientAllergenAllergenReactionTypes,
                    par => par.ClinicalQualifiers.Select(cq => cq.ClinicalQualifierCategory))

                .ToList();

            // Add existing to update list
            foreach (var reaction in existingReactions)
            {
                toUpdate.Add(medicationAllergies.First(p => p.Id == reaction.Id), reaction);
            }

            // Perform actual update
            foreach (var detail in toUpdate)
            {
                var source = detail.Key;
                var destination = detail.Value;

                destination.PatientAllergen.AllergenId = source.Medication.Id;
                destination.StartDateTime = source.OnsetDate;
                destination.EndDateTime = source.ResolutionDate;
                destination.IsDeactivated = source.IsDeactivated;
                destination.ClinicalConditionStatus = (ClinicalConditionStatus)source.Status.Id;

                // Set reaction type
                SetAllergenReactionType(destination.PatientAllergen.PatientAllergenAllergenReactionTypes, source.ReactionType);

                // Update qualifiers
                UpdateClinicalQualifier(ClinicalQualifierCategory.Severity, source.Severity,
                                        destination.ClinicalQualifiers, qualifiers);
            }

            // Commit all pending changes
            PracticeRepository.Save(toUpdate.Values);

            // End unit of work to force all pending changes to be saved
            _unitOfWorkProvider.Current.AcceptChanges();
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SavePatientProblems(ICollection<PatientProblemViewModel> problems)
        {
            var toUpdate = new Dictionary<PatientProblemViewModel, PatientDiagnosisDetail>(problems.Count);

            // Prepare records for new diagnosis details
            foreach (var viewModel in problems.Where(p => !p.Id.HasValue))
            {
                var diagnoseDetail = new PatientDiagnosisDetail();
                // Settings this upon creation only
                diagnoseDetail.IsOnProblemList = true;
                diagnoseDetail.IsEncounterDiagnosis = viewModel.IsEncounterDiagnosis;
                diagnoseDetail.EnteredDateTime = viewModel.LoggedDate;
                diagnoseDetail.UserId = viewModel.LoggedBy.Id;

                diagnoseDetail.PatientDiagnosis = new PatientDiagnosis();
                diagnoseDetail.PatientDiagnosis.PatientId = viewModel.PatientId;
                diagnoseDetail.PatientDiagnosis.ClinicalDataSourceTypeId = viewModel.SourceType == null
                    ? (int)ClinicalDataSourceTypeId.PracticeProvider
                    : viewModel.SourceType.Id;

                // Add to update list
                toUpdate.Add(viewModel, diagnoseDetail);
            }

            // Fetch existing diagnoses
            var existingDiagnosisIds = problems
                .Where(p => p.Id.HasValue)
                .Select(p => p.Id.EnsureNotDefault().Value)
                .ToArray();
            var existingDiagnoses = PracticeRepository.PatientDiagnosisDetails
                .Where(p => existingDiagnosisIds.Contains(p.Id))
                // Include pre loadings
                .Include(
                    p => p.PatientDiagnosis,
                    p => p.PatientDiagnosisDetailQualifiers
                        .Select(pcq => pcq.ClinicalQualifier.ClinicalQualifierCategory))
                .ToList();

            // Add existing to update list
            foreach (var diagnosis in existingDiagnoses)
            {
                toUpdate.Add(problems.First(p => p.Id == diagnosis.Id), diagnosis);
            }

            // Fetch default attribute id (it must be set when adding qualifiers)
            var defaultAttributeId = PracticeRepository.ClinicalAttributes.Select(a => a.Id).FirstOrDefault();

            // Perform actual update
            foreach (var detail in toUpdate)
            {
                var source = detail.Key;
                var destination = detail.Value;

                destination.StartDateTime = source.OnsetDate;
                destination.EndDateTime = source.ResolutionDate;
                destination.IsDeactivated = source.IsDeactivated;

                destination.IsEncounterDiagnosis = source.IsEncounterDiagnosis;

                destination.ClinicalConditionStatus = source.Status == null
                    ? ClinicalConditionStatus.Active
                    : (ClinicalConditionStatus)source.Status.Id;
                destination.PatientDiagnosis.Laterality = source.Laterality == null
                    ? null
                    : (Laterality?)source.Laterality.Id;
                destination.PatientDiagnosis.ClinicalConditionId = source.Description.IfNotNull(d => (int?)d.Id);
                destination.BodyLocationId = source.Location.IfNotNull(l => (int?)l.Id);

                // Update qualifiers
                UpdateDiagnosisClinicalQualifier(ClinicalQualifierCategory.Severity, source.Severity,
                        destination.PatientDiagnosisDetailQualifiers, defaultAttributeId);
                UpdateDiagnosisClinicalQualifier(ClinicalQualifierCategory.Chronicity, source.Chronicity,
                        destination.PatientDiagnosisDetailQualifiers, defaultAttributeId);
            }

            // Commit all pending changes
            PracticeRepository.Save(toUpdate.Values);

            // End unit of work to force all pending changes to be saved
            _unitOfWorkProvider.Current.AcceptChanges();
        }

        private IDictionary<PracticeEntityIdResolutionRequest, int> ResolvePracticeEntities<TMappingSource>(IList<TMappingSource> mappingSource, params Func<TMappingSource, PracticeEntityIdResolutionRequest>[] resolutionRequestGetters)
        {
            // Collect all resolution requests
            var resolutionRequests = mappingSource
                .SelectMany(ms => resolutionRequestGetters.Select(rrg => rrg(ms)))
                .Where(rr => rr != null)
                .Distinct()
                .ToList();

            var result = new ConcurrentDictionary<PracticeEntityIdResolutionRequest, int>();
            var fetchActions = new List<Action>();

            // Prepare resolution actions (resolve by system and entity)
            foreach (var resolutionRequest in resolutionRequests
                .GroupBy(rr => new
                    {
                        rr.ExternalSystemNameLowercase,
                        PracticeRepositoryEntityId = (int)rr.PracticeRepositoryEntityId
                    }))
            {
                var request = resolutionRequest;
                fetchActions.Add(() =>
                    {
                        // Collect all ids
                        var allExternalIds = request
                            .Select(rr => rr.ExternalEntityKey)
                            .ToList();

                        // Resolve in DB
                        var resolvedEntities = PracticeRepository.ExternalSystemEntityMappings
                            .Where(esem =>
                                esem.PracticeRepositoryEntityId == request.Key.PracticeRepositoryEntityId
                                && esem.ExternalSystemEntity.ExternalSystem.Name.ToLower() == request.Key.ExternalSystemNameLowercase
                                && allExternalIds.Contains(esem.ExternalSystemEntityKey))
                            .Select(esem => new
                                {
                                    esem.PracticeRepositoryEntityId,
                                    esem.PracticeRepositoryEntityKey,
                                    ExternalSystemNameLowercase = esem.ExternalSystemEntity.ExternalSystem.Name.ToLower(),
                                    esem.ExternalSystemEntityKey
                                })
                            .ToList();

                        // Record in result
                        foreach (var resolvedEntity in resolvedEntities)
                        {
                            // Locate request
                            var idResolutionRequest = resolutionRequests.First(rr =>
                                (int)rr.PracticeRepositoryEntityId == resolvedEntity.PracticeRepositoryEntityId
                                && rr.ExternalSystemNameLowercase == resolvedEntity.ExternalSystemNameLowercase
                                && rr.ExternalEntityKey == resolvedEntity.ExternalSystemEntityKey);

                            // Add mapping
                            result[idResolutionRequest] = int.Parse(resolvedEntity.PracticeRepositoryEntityKey);
                        }
                    });
            }

            // Execute all fetch operations
            fetchActions.ForAllInParallel(a => a());

            return result;
        }

        /// <summary>
        /// Fetches last modified information for specified Ids
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entityIds"></param>
        /// <returns></returns>
        List<LastModifiedEntry> GetLastModifiedStamps<TEntity>(IEnumerable<int> entityIds)
        {
            // Prepare basic query
            var changesSourceQuery = _auditRepository
                .AuditEntriesFor<TEntity>(entityIds);

            // Locate latest entry per every record
            var latestChangeRecords = (from c in changesSourceQuery
                                       group c by c.KeyValueNumeric
                                           into cGrouped
                                           where cGrouped.Any()
                                           select new
                                           {
                                               Id = cGrouped.Key,
                                               LatestChange = cGrouped
                                                   .OrderByDescending(change => change.AuditDateTime)
                                                   .FirstOrDefault()
                                           }).ToList();

            // Prepare result
            var latestChanges = (from lc in latestChangeRecords
                                 select new LastModifiedEntry
                                 {
                                     EntityId = lc.Id.GetValueOrDefault(),
                                     Timestamp = lc.LatestChange.AuditDateTime,
                                     User = new User
                                         {
                                             Id = lc.LatestChange.UserId.GetValueOrDefault()
                                         }
                                 }).ToList();

            // Resolve user records
            var latestChangesUserIds = latestChanges.Select(p => p.User.Id).ToList();
            var userRecords = PracticeRepository.Users
                .Where(u => latestChangesUserIds.Contains(u.Id))
                .ToDictionary(k => k.Id, v => v);
            foreach (var change in latestChanges)
            {
                User userRecord;
                if (userRecords.TryGetValue(change.User.Id, out userRecord))
                {
                    change.User = userRecord;
                }
            }

            return latestChanges;
        }

        void SetAllergenReactionType(ICollection<PatientAllergenAllergenReactionType> allergenReactionTypes, NamedViewModel reactionType)
        {
            // No reaction type?
            if (reactionType == null)
            {
                allergenReactionTypes.ToArray().ForEachWithSinglePropertyChangedNotification(PracticeRepository.Delete);
                return;
            }

            // Check if it is already set
            var reactionTypeCurrent = allergenReactionTypes.FirstOrDefault(als => als.Id == reactionType.Id);
            if (reactionTypeCurrent == null)
            {
                // No - remove existing items
                allergenReactionTypes.ToArray().ForEachWithSinglePropertyChangedNotification(PracticeRepository.Delete);

                // Add reaction type
                allergenReactionTypes.Add(new PatientAllergenAllergenReactionType
                {
                    AllergenReactionTypeId = reactionType.Id
                });
            }
        }

        /// <summary>
        /// Updates qualifier value in qualifiers collection
        /// </summary>
        /// <param name="qualifierCategory"></param>
        /// <param name="newQualifierValue"></param>
        /// <param name="entityQualifiers"></param>
        /// <param name="qualifiersCache"></param>
        static void UpdateClinicalQualifier(string qualifierCategory, NamedViewModel newQualifierValue, ICollection<ClinicalQualifier> entityQualifiers, IEnumerable<ClinicalQualifier> qualifiersCache)
        {
            var clinicalQualifier = entityQualifiers
                .FirstOrDefault(cq =>
                    // Clinical qualifier category can be null for newly added qualifiers
                    cq.ClinicalQualifierCategory != null

                    && cq.ClinicalQualifierCategory.Name == qualifierCategory);

            // No qualifier set and need -> quit
            if (clinicalQualifier == null && newQualifierValue == null) return;

            // Qualifier is set, but not needed -> quit
            if (clinicalQualifier != null && newQualifierValue == null)
            {
                entityQualifiers.Remove(clinicalQualifier);
                return;
            }

            // Qualifier not set, but should be
            if (clinicalQualifier == null)
            {
                entityQualifiers.Add(qualifiersCache.First(q => q.Id == newQualifierValue.Id));
                return;
            }

            // Same qualifier is already set
            if (clinicalQualifier.Id == newQualifierValue.Id) return;

            // Set new qualifier
            entityQualifiers.Remove(clinicalQualifier);
            entityQualifiers.Add(qualifiersCache.First(q => q.Id == newQualifierValue.Id));
        }

        static void UpdateDiagnosisClinicalQualifier(string qualifierCategory, NamedViewModel newQualifierValue, ICollection<PatientDiagnosisDetailQualifier> entityQualifiers, int defaultClinicalAttributeId)
        {
            var clinicalQualifier = entityQualifiers
                .FirstOrDefault(cq =>
                    // Clinical qualifier can be null for newly added qualifiers
                    cq.ClinicalQualifier != null
                    && cq.ClinicalQualifier.ClinicalQualifierCategory != null

                    && cq.ClinicalQualifier.ClinicalQualifierCategory.Name == qualifierCategory);

            // No qualifier set and need -> quit
            if (clinicalQualifier == null && newQualifierValue == null) return;

            // Qualifier is set, but not needed -> quit
            if (clinicalQualifier != null && newQualifierValue == null)
            {
                entityQualifiers.Remove(clinicalQualifier);
                return;
            }

            // Qualifier not set, but should be
            if (clinicalQualifier == null)
            {
                entityQualifiers.Add(new PatientDiagnosisDetailQualifier
                {
                    ClinicalQualifierId = newQualifierValue.Id,
                    ClinicalAttributeId = defaultClinicalAttributeId,
                    OrdinalId = 0,
                });
                return;
            }

            // Set new qualifier
            clinicalQualifier.ClinicalQualifierId = newQualifierValue.Id;
        }

        void InitMappers()
        {
            _clinicalConditionStatusMapper = Mapper.Factory.CreateNamedViewModelMapper<ClinicalConditionStatus>();
            _lateralityMapper = Mapper.Factory.CreateNamedViewModelMapper<Laterality>();

            _bodyLocationMapper = Mapper.Factory.CreateMapper<BodyLocation, NamedViewModel>(
                location => new NamedViewModel
                {
                    Id = location.Id,
                    Name = location.Name
                });
            _clinicalConditionMapper = Mapper.Factory.CreateMapper<ClinicalCondition, NamedViewModel>(
                condition => new NamedViewModel
                    {
                        Id = condition.Id,
                        Name = condition.Name
                    });
            _clinicalQualifierMapper = Mapper.Factory.CreateMapper<ClinicalQualifier, NamedViewModel>(
                qualifier => new NamedViewModel
                {
                    Id = qualifier.Id,
                    Name = qualifier.Name
                });
            _sourceTypeMapper = Mapper.Factory.CreateMapper<ClinicalDataSourceType, NamedViewModel>(
                sourceType => new NamedViewModel
                    {
                        Id = sourceType.Id,
                        Name = sourceType.Name
                    });
            _userMapper = Mapper.Factory.CreateMapper<User, NamedViewModel>(
                user => new NamedViewModel
                    {
                        Id = user.Id,
                        Name = user.DisplayName
                    });
            _allergenMapper = Mapper.Factory.CreateMapper<Allergen, NamedViewModel>(
                user => new NamedViewModel
                {
                    Id = user.Id,
                    Name = user.Name
                });
            _allergenReactionTypeMapper = Mapper.Factory.CreateMapper<AllergenReactionType, NamedViewModel>(
                reactionType => new NamedViewModel
                    {
                        Id = reactionType.Id,
                        Name = reactionType.Name
                    });

            _problemFromCdaMapper = Mapper.Factory.CreateMapper<CdaProblemMappingSource, PatientProblemViewModel>(
                mapping => new PatientProblemViewModel
                               {
                                   Id = null,
                                   PatientId = mapping.PatientId,
                                   OnsetDate = mapping.ProblemAct.PrimaryProblemObservation().StartDate(),
                                   ResolutionDate = mapping.ProblemAct.PrimaryProblemObservation().EndDate(),

                                   Description = mapping.ClinicalCondition != null && mapping.ResolvedPracticeEntities.ContainsKey(mapping.ClinicalCondition)
                                                     ? new NamedViewModel(mapping.ResolvedPracticeEntities[mapping.ClinicalCondition], mapping.ClinicalCondition.EntityDisplayName)
                                                     : null,

                                   // Constants
                                   Laterality = null,
                                   IsDeactivated = false,
                                   LastModifiedBy = new NamedViewModel(UserContext.Current.UserDetails.Id, UserContext.Current.UserDetails.DisplayName),
                                   LastModifiedDate = mapping.ProblemAct.PrimaryProblemObservation().StartDate().HasValue ? mapping.ProblemAct.PrimaryProblemObservation().StartDate().Value : DateTime.Now.ToClientTime(),
                                   LoggedBy = new NamedViewModel(UserContext.Current.UserDetails.Id, UserContext.Current.UserDetails.DisplayName),
                                   LoggedDate = DateTime.Now.ToClientTime(),
                                   SourceType = new NamedViewModel(
                                       (int)ClinicalDataSourceTypeId.ExternalProvider,
                                       ClinicalDataSourceTypeId.ExternalProvider.GetDisplayName()),
                                   Status = _clinicalConditionStatusMapper.Map(ConvertClinicalConditionStatus(mapping.ProblemAct.ClinicalConditionStatus()))
                               });

            _medicationAllergyFromCdaMapper = Mapper.Factory.CreateMapper<CdaMedicationAllergyMappingSource, PatientMedicationAllergyViewModel>(
                mapping => new PatientMedicationAllergyViewModel
                    {
                        Id = null,
                        PatientId = mapping.PatientId,
                        OnsetDate = mapping.DrugAllergyAct.PrimaryAllergyObservation().StartDate(),
                        ResolutionDate = mapping.DrugAllergyAct.PrimaryAllergyObservation().EndDate(),

                        Medication = mapping.MedicationCode != null && mapping.ResolvedPracticeEntities.ContainsKey(mapping.MedicationCode)
                            ? new NamedViewModel(mapping.ResolvedPracticeEntities[mapping.MedicationCode], mapping.MedicationCode.EntityDisplayName)
                            : null,
                        ReactionType = mapping.ReactionCode != null && mapping.ResolvedPracticeEntities.ContainsKey(mapping.ReactionCode)
                            ? new NamedViewModel(mapping.ResolvedPracticeEntities[mapping.ReactionCode], mapping.ReactionCode.EntityDisplayName)
                            : null,
                        Severity = mapping.SeverityCode != null && mapping.ResolvedPracticeEntities.ContainsKey(mapping.SeverityCode)
                            ? new NamedViewModel(mapping.ResolvedPracticeEntities[mapping.SeverityCode], mapping.SeverityCode.EntityDisplayName)
                            : null,

                        // Constants
                        IsDeactivated = false,
                        LastModifiedBy = new NamedViewModel(UserContext.Current.UserDetails.Id, UserContext.Current.UserDetails.DisplayName),
                        LastModifiedDate = mapping.DrugAllergyAct.PrimaryAllergyObservation().StartDate().HasValue ? mapping.DrugAllergyAct.PrimaryAllergyObservation().StartDate() : DateTime.Now.ToClientTime(),
                        LoggedBy = new NamedViewModel(UserContext.Current.UserDetails.Id, UserContext.Current.UserDetails.DisplayName),
                        LoggedDate = DateTime.Now.ToClientTime(),
                        SourceType = new NamedViewModel(
                            (int)ClinicalDataSourceTypeId.ExternalProvider,
                            ClinicalDataSourceTypeId.ExternalProvider.GetDisplayName()),
                        Status = _clinicalConditionStatusMapper.Map(ConvertClinicalConditionStatus(mapping.DrugAllergyAct.DrugAllergyConditionStatus()))
                    });

            _patientProblemMapper = Mapper.Factory.CreateMapper<PatientDiagnosisDetail, PatientProblemViewModel>(
                problem => new PatientProblemViewModel
                    {
                        Id = problem.Id,
                        PatientId = problem.PatientDiagnosis.PatientId,
                        SourceType = _sourceTypeMapper.Map(problem.PatientDiagnosis.ClinicalDataSourceType),
                        Description = problem.PatientDiagnosis.ClinicalCondition.IfNotNull(_clinicalConditionMapper.Map),
                        Laterality = problem.PatientDiagnosis.Laterality == null
                            ? null
                            : _lateralityMapper.Map((Laterality)problem.PatientDiagnosis.Laterality),
                        OnsetDate = problem.StartDateTime,
                        ResolutionDate = problem.EndDateTime,
                        IsDeactivated = problem.IsDeactivated,
                        LoggedDate = problem.EnteredDateTime,
                        LoggedBy = problem.User.IfNotNull(_userMapper.Map),
                        Status = _clinicalConditionStatusMapper.Map(problem.ClinicalConditionStatus),
                        Location = problem.BodyLocation.IfNotNull(_bodyLocationMapper.Map),
                        AxisQualifier = String.Join(", ", problem.PatientDiagnosisDetailAxisQualifiers),

                        // Fetch qualifier values
                        Chronicity = problem.PatientDiagnosisDetailQualifiers
                            .Select(q => q.ClinicalQualifier)
                            .FirstOrDefault(q => q.ClinicalQualifierCategory.Name == ClinicalQualifierCategory.Chronicity)
                            .IfNotNull(_clinicalQualifierMapper.Map),
                        Severity = problem.PatientDiagnosisDetailQualifiers
                            .Select(q => q.ClinicalQualifier)
                            .FirstOrDefault(q => q.ClinicalQualifierCategory.Name == ClinicalQualifierCategory.Severity)
                            .IfNotNull(_clinicalQualifierMapper.Map),
                        IsEncounterDiagnosis = problem.IsEncounterDiagnosis
                    });

            // Medication allergies related
            _medicationAllergyMapper = Mapper.Factory.CreateMapper<PatientAllergenDetail, PatientMedicationAllergyViewModel>(
                detail => new PatientMedicationAllergyViewModel
                    {
                        Id = detail.Id,
                        PatientId = detail.PatientAllergen.PatientId,
                        // We support only one reaction type per medication allergy
                        ReactionType = detail.PatientAllergen.PatientAllergenAllergenReactionTypes
                            .Select(p => p.AllergenReactionType)
                            .FirstOrDefault()
                            .IfNotNull(_allergenReactionTypeMapper.Map),

                        Medication = detail.PatientAllergen.Allergen.IfNotNull(_allergenMapper.Map),
                        IsDeactivated = detail.IsDeactivated,
                        LoggedBy = detail.User.IfNotNull(_userMapper.Map),
                        LoggedDate = detail.EnteredDateTime,
                        OnsetDate = detail.StartDateTime,
                        ResolutionDate = detail.EndDateTime,
                        SourceType = detail.PatientAllergen.ClinicalDataSourceType.IfNotNull(_sourceTypeMapper.Map),
                        Status = _clinicalConditionStatusMapper.Map(detail.ClinicalConditionStatus),

                        // Fetch qualifier values
                        Severity = detail.ClinicalQualifiers
                            .FirstOrDefault(q => q.ClinicalQualifierCategory.Name == ClinicalQualifierCategory.Severity)
                            .IfNotNull(_clinicalQualifierMapper.Map),
                    });

            _importedCdaFileMapper = Mapper.Factory.CreateMapper<ExternalSystemMessage, ImportedCdaViewModel>(
                message => new ImportedCdaViewModel
                    {
                        Id = message.Id,
                        Date = message.CreatedDateTime,
                        // Note: We expect Description to be CDA display description
                        Doctor = message.Description
                    });
        }

        /// <summary>
        /// Checks for clinicalConditionstatus code and returns an enum value.
        /// </summary>
        /// <param name="clinicalConditionStatus"></param>
        /// <returns></returns>
        private static ClinicalConditionStatus ConvertClinicalConditionStatus(CD clinicalConditionStatus)
        {
            var status = ClinicalConditionStatus.Active;

            if (clinicalConditionStatus == null || clinicalConditionStatus.codeSystem != CodeSystem.SnomedCt.Oid()) return status;

            switch (clinicalConditionStatus.code)
            {
                case "55561003":
                    status = ClinicalConditionStatus.Active;
                    break;
                case "413322009":
                    status = ClinicalConditionStatus.Resolved;
                    break;
                case "73425007":
                    status = ClinicalConditionStatus.Inactive;
                    break;
            }

            return status;
        }
        private class LastModifiedEntry
        {
            public long EntityId { get; set; }
            public User User { get; set; }
            public DateTime Timestamp { get; set; }
        }
    }
}
