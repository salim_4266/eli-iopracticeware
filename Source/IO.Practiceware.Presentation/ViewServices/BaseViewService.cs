﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewServices.Common;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Web.Client;

namespace IO.Practiceware.Presentation.ViewServices
{
    [RemoteService]
    [ReturnsAopActivatedInstances]
    [SupportsTransactionScopes]
    [SupportsUnitOfWork]
    public abstract class BaseViewService 
    {
        [Dependency]
        protected IPracticeRepository PracticeRepository { get; set; }
    }
}
