﻿using System.Collections.ObjectModel;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Setup.Administrative;
using IO.Practiceware.Presentation.ViewServices.Setup.Administrative;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Linq;
using Soaf.Domain;

[assembly: Component(typeof(AdminsitrativeSetupViewService), typeof(IAdminsitrativeSetupViewService))]


namespace IO.Practiceware.Presentation.ViewServices.Setup.Administrative
{

    public interface IAdminsitrativeSetupViewService
    {
        ObservableCollection<AdministrativeDetails> GetAministrativeSetupData(string selectedItem);
        void SaveIsLoadedItems(ExtendedObservableCollection<AdministrativeSetupListViewModel> isLoadedItems);
    }


    internal class AdminsitrativeSetupViewService : BaseViewService, IAdminsitrativeSetupViewService
    {
        private readonly IRepositoryService _repositoryService;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;

        public AdminsitrativeSetupViewService(IRepositoryService repositoryService,
            IUnitOfWorkProvider unitOfWorkProvider)
        {
            _repositoryService = repositoryService;
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public virtual ObservableCollection<AdministrativeDetails> GetAministrativeSetupData(string selectedItem)
        {            
            IQueryable query = null;
            var detailscollection = new ExtendedObservableCollection<AdministrativeDetails>();
                if (selectedItem == "Insurer Plan Types")
                {
                    query = from data in PracticeRepository.InsurerPlanTypes
                        select data;

                }
                else if (selectedItem == "Claim File Receivers")
                {
                    query = from data in PracticeRepository.ClaimFileReceivers
                        select data;

                }
                else if (selectedItem == "Insurer Business Classes")
                {
                    query = from data in PracticeRepository.InsurerBusinessClasses
                        select data;
                }
                else if (selectedItem == "External Organizations")
                {
                    query = from data in PracticeRepository.ExternalOrganizations
                        select data;
                }
                else if (selectedItem == "Encounter Status Change Reasons")
                {
                    query = from data in PracticeRepository.EncounterStatusChangeReasons
                        select data;
                }
                else if (selectedItem == "Service Locations")
                {
                    query = from data in PracticeRepository.ServiceLocations
                        select data;
                }
                if (query != null)
                {                   
                    foreach (var tableData in query)
                    {
                        if (tableData != null)
                        {
                            var details = new AdministrativeDetails();
                            var stringCollection = new ExtendedObservableCollection<StringAndIdViewModel>();
                            var booleanCollection = new ExtendedObservableCollection<BoolAndIdViewModel>();
                            var foreignKeysCollection = new ExtendedObservableCollection<CollectionAndSelectedItemAndIdViewModel>();
                            foreach (var propinfo in tableData.GetType().GetProperties())
                            {
                                if (propinfo.Name == "Id" || propinfo.Name == "HexColor")
                                {
                                    continue;
                                }
                               if (propinfo.Name == "Name")
                                {
                                    details.Name = Convert.ToString(propinfo.GetValue(tableData, null));
                                    details.CanDelete = true;
                                }
                                if(selectedItem == "Encounter Status Change Reasons" && propinfo.Name == "Value")
                                {
                                    details.Name = Convert.ToString(propinfo.GetValue(tableData, null));
                                    details.CanDelete = true;
                                }


                                if (propinfo.PropertyType == typeof (Boolean))
                                {
                                    var model = new BoolAndIdViewModel();
                                    model.Name = propinfo.Name;
                                    model.Value = Convert.ToBoolean(propinfo.GetValue(tableData, null));
                                    booleanCollection.Add(model);
                                }
                                else if (propinfo.Name == "ExternalOrganizationTypeId" || propinfo.Name == "EncounterStatusId" || propinfo.Name == "ServiceLocationCodeId")
                                {
                                    var foreignKeys = new CollectionAndSelectedItemAndIdViewModel();
                                    var namedCollection = new ExtendedObservableCollection<NamedViewModel>();
                                    IQueryable orgTypeQuery = null;
                                    if (propinfo.Name == "ExternalOrganizationTypeId")
                                    {
                                        orgTypeQuery = from data in PracticeRepository.ExternalOrganizationTypes
                                            select data;
                                    }
                                    else if (propinfo.Name == "EncounterStatusId")
                                    {
                                        orgTypeQuery = from data in PracticeRepository.EncounterStatusConfigurations
                                            select data;
                                    }
                                    else if (propinfo.Name == "ServiceLocationCodeId")
                                    {
                                        orgTypeQuery = from data in PracticeRepository.ServiceLocationCodes
                                            select data;
                                    }

                                    if (orgTypeQuery != null)
                                    {
                                        foreach (var orgTypeData in orgTypeQuery)
                                        {
                                            if (orgTypeData != null)
                                            {
                                                var namedModel = new NamedViewModel();
                                                foreach (var prop in orgTypeData.GetType().GetProperties())
                                                {
                                                    if (prop.Name == "Name")
                                                    {
                                                        namedModel.Name = Convert.ToString(prop.GetValue(orgTypeData, null));
                                                    }
                                                    if (prop.Name == "Id")
                                                    {
                                                        namedModel.Id = Convert.ToInt32(prop.GetValue(orgTypeData, null));
                                                    }
                                                }
                                                namedCollection.Add(namedModel);
                                            }
                                        }

                                        foreignKeys.Collection = namedCollection;
                                        foreignKeys.SelectedItem = namedCollection[0];
                                        foreignKeys.Name = propinfo.Name;
                                        foreignKeysCollection.Add(foreignKeys);

                                    }
                                }
                                else if (IsPrimitive(propinfo.PropertyType))
                                {
                                    var model = new StringAndIdViewModel();
                                    model.Name = propinfo.Name;
                                    model.Value = Convert.ToString(propinfo.GetValue(tableData, null));
                                    stringCollection.Add(model);
                                }
                            }
                            details.StringValues = stringCollection;
                            details.BooleanValues = booleanCollection;
                            details.ForeignKeyValues = foreignKeysCollection;
                            detailscollection.Add(details);
                        }
                    }
                   
                }

                return detailscollection;
        }
       
        
        [UnitOfWork]
        public virtual void SaveIsLoadedItems(ExtendedObservableCollection<AdministrativeSetupListViewModel> isLoadedItems)
        {           
            isLoadedItems.ForEach(i =>
            {
                if (i.Name == "Insurer Plan Types")
                {
                    i.List.ForEach(j =>
                    {
                        var insurerPlanType = PracticeRepository.InsurerPlanTypes.FirstOrDefault(k => k.Name == j.Name);
                        if (insurerPlanType != null)
                        {
                            insurerPlanType.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                        }
                        else
                        {
                            insurerPlanType = new InsurerPlanType();
                            insurerPlanType.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                        }

                        PracticeRepository.Save(insurerPlanType);                        
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(k =>
                    {
                        var insurerPlanType = PracticeRepository.InsurerPlanTypes.FirstOrDefault(l => l.Name == k);
                        if (insurerPlanType != null)
                        {
                            PracticeRepository.Delete(insurerPlanType);
                        }
                    }));

                }
                else if (i.Name == "Claim File Receivers")
                {
                    i.List.ForEach(j =>
                    {
                        var claimFileReceiver = PracticeRepository.ClaimFileReceivers.FirstOrDefault(k => k.Name == j.Name);
                        if (claimFileReceiver != null)
                        {
                            claimFileReceiver.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            claimFileReceiver.GSApplicationReceiverCode = j.StringValues.Where(l => l.Name == "GSApplicationReceiverCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.GSApplicationSenderCode = j.StringValues.Where(l => l.Name == "GSApplicationSenderCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeControlVersionCode = j.StringValues.Where(l => l.Name == "InterchangeControlVersionCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeReceiverCode = j.StringValues.Where(l => l.Name == "InterchangeReceiverCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeReceiverCodeQualifier = j.StringValues.Where(l => l.Name == "InterchangeReceiverCodeQualifier").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeSenderCode = j.StringValues.Where(l => l.Name == "InterchangeSenderCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeSenderCodeQualifier = j.StringValues.Where(l => l.Name == "InterchangeSenderCodeQualifier").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeUsageIndicator = j.StringValues.Where(l => l.Name == "InterchangeUsageIndicator").Select(l => l).First().Value.ToString();
                            claimFileReceiver.Loop1000ASubmitterCode = j.StringValues.Where(l => l.Name == "Loop1000ASubmitterCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.Loop1000BRecipientCode = j.StringValues.Where(l => l.Name == "Loop1000BRecipientCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.ComponentElementSeparator = j.StringValues.Where(l => l.Name == "ComponentElementSeparator").Select(l => l).First().Value.ToString();
                            claimFileReceiver.RepetitionSeparator = j.StringValues.Where(l => l.Name == "RepetitionSeparator").Select(l => l).First().Value.ToString();
                            claimFileReceiver.WebsiteUrl = j.StringValues.Where(l => l.Name == "WebsiteUrl").Select(l => l).First().Value.ToString();                            
                        }
                        else
                        {
                            claimFileReceiver = new ClaimFileReceiver();
                            claimFileReceiver.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            claimFileReceiver.GSApplicationReceiverCode = j.StringValues.Where(l => l.Name == "GSApplicationReceiverCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.GSApplicationSenderCode = j.StringValues.Where(l => l.Name == "GSApplicationSenderCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeControlVersionCode = j.StringValues.Where(l => l.Name == "InterchangeControlVersionCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeReceiverCode = j.StringValues.Where(l => l.Name == "InterchangeReceiverCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeReceiverCodeQualifier = j.StringValues.Where(l => l.Name == "InterchangeReceiverCodeQualifier").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeSenderCode = j.StringValues.Where(l => l.Name == "InterchangeSenderCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeSenderCodeQualifier = j.StringValues.Where(l => l.Name == "InterchangeSenderCodeQualifier").Select(l => l).First().Value.ToString();
                            claimFileReceiver.InterchangeUsageIndicator = j.StringValues.Where(l => l.Name == "InterchangeUsageIndicator").Select(l => l).First().Value.ToString();
                            claimFileReceiver.Loop1000ASubmitterCode = j.StringValues.Where(l => l.Name == "Loop1000ASubmitterCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.Loop1000BRecipientCode = j.StringValues.Where(l => l.Name == "Loop1000BRecipientCode").Select(l => l).First().Value.ToString();
                            claimFileReceiver.ComponentElementSeparator = j.StringValues.Where(l => l.Name == "ComponentElementSeparator").Select(l => l).First().Value.ToString();
                            claimFileReceiver.RepetitionSeparator = j.StringValues.Where(l => l.Name == "RepetitionSeparator").Select(l => l).First().Value.ToString();
                            claimFileReceiver.WebsiteUrl = j.StringValues.Where(l => l.Name == "WebsiteUrl").Select(l => l).First().Value.ToString();   
                        }

                        PracticeRepository.Save(claimFileReceiver);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(k =>
                    {
                        var claimFileReceiver = PracticeRepository.ClaimFileReceivers.FirstOrDefault(l => l.Name == k);
                        if (claimFileReceiver != null)
                        {
                            PracticeRepository.Delete(claimFileReceiver);
                        }
                    }));                   

                }
                else if (i.Name == "Insurer Business Classes")
                {
                    i.List.ForEach(j =>
                    {
                        var insurerBusinessClass = PracticeRepository.InsurerBusinessClasses.FirstOrDefault(k => k.Name == j.Name);
                        if (insurerBusinessClass != null)
                        {
                            insurerBusinessClass.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            insurerBusinessClass.IsArchived = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                        }
                        else
                        {
                             insurerBusinessClass = new InsurerBusinessClass();
                             insurerBusinessClass.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                             insurerBusinessClass.IsArchived = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                        }
                        PracticeRepository.Save(insurerBusinessClass);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(k =>
                    {
                        var insurerBusinessClass = PracticeRepository.InsurerBusinessClasses.FirstOrDefault(l => l.Name == k);
                        if (insurerBusinessClass != null)
                        {
                            PracticeRepository.Delete(insurerBusinessClass);
                        }
                    }));                       
                }
                else if (i.Name == "External Organizations")
                {
                    i.List.ForEach(j =>
                    {
                        var externalOrganization = PracticeRepository.ExternalOrganizations.FirstOrDefault(k => k.Name == j.Name);
                        if (externalOrganization != null)
                        {
                            externalOrganization.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            externalOrganization.DisplayName = j.StringValues.Where(l => l.Name == "DisplayName").Select(l => l).First().Value.ToString();
                            externalOrganization.ExternalOrganizationTypeId = Convert.ToInt32(j.ForeignKeyValues.FirstOrDefault(l => l.Name == "ExternalOrganizationTypeId").IfNotNull(k=>k.SelectedItem.Id));
                            externalOrganization.IsArchived = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                        }
                        else
                        {
                            externalOrganization = new ExternalOrganization();
                            externalOrganization.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            externalOrganization.DisplayName = j.StringValues.Where(l => l.Name == "DisplayName").Select(l => l).First().Value.ToString();
                            externalOrganization.ExternalOrganizationTypeId = Convert.ToInt32(j.ForeignKeyValues.FirstOrDefault(l => l.Name == "ExternalOrganizationTypeId").IfNotNull(k => k.SelectedItem.Id));
                            externalOrganization.IsArchived = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                        }
                        PracticeRepository.Save(externalOrganization);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(k =>
                    {
                        var externalOrganization = PracticeRepository.ExternalOrganizations.FirstOrDefault(l => l.Name == k);
                        if (externalOrganization != null)
                        {
                            PracticeRepository.Delete(externalOrganization);
                        }
                    }));                     
                }
                else if (i.Name == "Encounter Status Change Reasons")
                {
                    i.List.ForEach(j =>
                    {
                        var encounterStatusChangeReason = PracticeRepository.EncounterStatusChangeReasons.FirstOrDefault(k => k.Value == j.Name);
                        if (encounterStatusChangeReason != null)
                        {
                            encounterStatusChangeReason.Value = j.StringValues.Where(l => l.Name == "Value").Select(l => l).First().Value.ToString();
                            encounterStatusChangeReason.EncounterStatusId = Convert.ToInt32(j.ForeignKeyValues.FirstOrDefault(l => l.Name == "EncounterStatusId").IfNotNull(k => k.SelectedItem.Id));
                        }
                        else
                        {
                            encounterStatusChangeReason = new EncounterStatusChangeReason();
                            encounterStatusChangeReason.Value = j.StringValues.Where(l => l.Name == "Value").Select(l => l).First().Value.ToString();
                            encounterStatusChangeReason.EncounterStatusId = Convert.ToInt32(j.ForeignKeyValues.FirstOrDefault(l => l.Name == "EncounterStatusId").IfNotNull(k => k.SelectedItem.Id));
                        }
                        PracticeRepository.Save(encounterStatusChangeReason);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(k =>
                    {
                        var encounterStatusChangeReason = PracticeRepository.EncounterStatusChangeReasons.FirstOrDefault(l => l.Value == k);
                        if (encounterStatusChangeReason != null)
                        {
                            PracticeRepository.Delete(encounterStatusChangeReason);
                        }
                    }));                     
                }
                else if (i.Name == "Service Locations")
                {
                    i.List.ForEach(j =>
                    {
                        var serviceLocation = PracticeRepository.ServiceLocations.FirstOrDefault(k => k.Name == j.Name);
                        if (serviceLocation != null)
                        {
                            serviceLocation.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            serviceLocation.IsArchived = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                            serviceLocation.IsExternal = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsExternal").Select(l => l).First().Value);
                            serviceLocation.ServiceLocationCodeId = Convert.ToInt32(j.ForeignKeyValues.FirstOrDefault(l => l.Name == "ServiceLocationCodeId").IfNotNull(k => k.SelectedItem.Id));
                            serviceLocation.ShortName = j.StringValues.Where(l => l.Name == "ShortName").Select(l => l).First().Value.ToString();                                                      
                        }
                        else
                        {
                            serviceLocation = new ServiceLocation();
                            serviceLocation.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            serviceLocation.IsArchived = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                            serviceLocation.IsExternal = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsExternal").Select(l => l).First().Value);
                            serviceLocation.ServiceLocationCodeId = Convert.ToInt32(j.ForeignKeyValues.FirstOrDefault(l => l.Name == "ServiceLocationCodeId").IfNotNull(k => k.SelectedItem.Id));
                            serviceLocation.ShortName = j.StringValues.Where(l => l.Name == "ShortName").Select(l => l).First().Value.ToString();                         
                        }
                        PracticeRepository.Save(serviceLocation);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(k =>
                    {
                        var serviceLocation = PracticeRepository.ServiceLocations.FirstOrDefault(l => l.Name == k);
                        if (serviceLocation != null)
                        {
                            PracticeRepository.Delete(serviceLocation);
                        }
                    }));                     
                }
                
            });

            _repositoryService.Persist(_unitOfWorkProvider.Current.ObjectStateEntries, typeof(IPracticeRepository).FullName);
        }

        private static bool IsPrimitive(Type t)
        {
            // TODO: put any type here that you consider as primitive as I didn't
            // quite understand what your definition of primitive type is
            return new[] { 
            typeof(string), 
            typeof(char),
            typeof(byte),
            typeof(sbyte),
            typeof(ushort),
            typeof(short),
            typeof(uint),
            typeof(int),
            typeof(int?),
            typeof(ulong),
            typeof(long),
            typeof(float),
            typeof(double),
            typeof(decimal),           
            typeof(DateTime)
        }.Contains(t);
        }
    }
}
