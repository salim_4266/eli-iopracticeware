﻿using System.Collections.ObjectModel;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Setup.Clinical;
using IO.Practiceware.Presentation.ViewServices.Setup.ClinicalSetup;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Soaf.Domain;
using IO.Practiceware.Data;

[assembly: Component(typeof(ClinicalSetupViewService), typeof(IClinicalSetupViewService))]


namespace IO.Practiceware.Presentation.ViewServices.Setup.ClinicalSetup
{

    public interface IClinicalSetupViewService
    {
        ObservableCollection<ClinicalDetails> GetClinicalSetupData(string selectedItem);
        void SaveIsLoadedItems(ExtendedObservableCollection<ClinicalSetupListViewModel> isLoadedItems);
    }


    public class ClinicalSetupViewService : BaseViewService, IClinicalSetupViewService
    {
        private readonly IRepositoryService _repositoryService;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;

        public ClinicalSetupViewService(IRepositoryService repositoryService,
            IUnitOfWorkProvider unitOfWorkProvider)
        {
            _repositoryService = repositoryService;
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public virtual ObservableCollection<ClinicalDetails> GetClinicalSetupData(string selectedItem)
        {
            IQueryable query = null;
            var detailscollection = new ExtendedObservableCollection<ClinicalDetails>();

            if (selectedItem == "Clinical Specialty Types")
            {
                query = from data in PracticeRepository.ClinicalSpecialtyTypes
                        select data;


            }
            else if (selectedItem == "Service Modifiers")
            {
                query = from data in PracticeRepository.ServiceModifiers
                        select new
                        {
                            code = data.Code + "-" + data.Description,
                            data.Code,
                            data.Description,
                            data.IsArchived
                        };
            }

            if (query != null)
            {
                foreach (var tableData in query)
                {
                    if (tableData != null)
                    {
                        var details = new ClinicalDetails();
                        var stringCollection = new ExtendedObservableCollection<StringAndIdViewModel>();
                        var booleanCollection = new ExtendedObservableCollection<BoolAndIdViewModel>();
                        var foreignKeysCollection = new ExtendedObservableCollection<CollectionAndSelectedItemAndIdViewModel>();
                        foreach (var propinfo in tableData.GetType().GetProperties())
                        {
                            if (propinfo.Name == "Id" || propinfo.Name == "HexColor")
                            {
                                continue;
                            }
                            if (propinfo.Name == "Name")
                            {
                                details.Name = Convert.ToString(propinfo.GetValue(tableData, null));
                                details.CanDelete = true;
                            }
                            if (propinfo.Name == "code")
                            {
                                details.Name = Convert.ToString(propinfo.GetValue(tableData, null));
                                details.CanDelete = true;
                            }
                            if (propinfo.PropertyType == typeof(Boolean))
                            {
                                var model = new BoolAndIdViewModel();
                                model.Name = propinfo.Name;
                                model.Value = Convert.ToBoolean(propinfo.GetValue(tableData, null));
                                booleanCollection.Add(model);
                            }
                            else if (IsPrimitive(propinfo.PropertyType))
                            {
                                if (propinfo.Name != "code")
                                {
                                    var model = new StringAndIdViewModel();
                                    model.Name = propinfo.Name;
                                    model.Value = Convert.ToString(propinfo.GetValue(tableData, null));
                                    stringCollection.Add(model);
                                }
                            }
                        }
                        details.StringValues = stringCollection;
                        details.BooleanValues = booleanCollection;
                        detailscollection.Add(details);
                    }
                }
            }

            return detailscollection;
        }

        [UnitOfWork]
        public virtual void SaveIsLoadedItems(ExtendedObservableCollection<ClinicalSetupListViewModel> isLoadedItems)
        {
            isLoadedItems.ForEach(i =>
            {
                if (i.Name == "Clinical Specialty Types")
                {
                    i.List.ForEach(j =>
                    {
                        var ClinicalSpecialtyTypes = PracticeRepository.ClinicalSpecialtyTypes.FirstOrDefault(k => k.Name == j.Name);
                        if (ClinicalSpecialtyTypes != null )
                        {
                            string name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            int isArchived = Convert.ToInt32(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                            var dbConnection = DbConnectionFactory.PracticeRepository;
                            dbConnection.Open();
                            var dbCommand = dbConnection.CreateCommand();
                            dbCommand.CommandText = string.Format("UPDATE model.ClinicalSpecialtyTypes SET Name='{0}', IsArchived={1} WHERE Name='{2}' ", name, isArchived, j.Name.ToString());
                            dbCommand.ExecuteNonQuery();
                            dbConnection.Close();
                        }
                        else
                        {
                            ClinicalSpecialtyTypes = new ClinicalSpecialtyType();
                            ClinicalSpecialtyTypes.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            ClinicalSpecialtyTypes.IsArchived = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                        }

                        PracticeRepository.Save(ClinicalSpecialtyTypes);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(k =>
                    {
                        var ClinicalSpecialtyTypes = PracticeRepository.ClinicalSpecialtyTypes.FirstOrDefault(l => l.Name == k);
                        if (ClinicalSpecialtyTypes != null)
                        {
                            PracticeRepository.Delete(ClinicalSpecialtyTypes);
                        }
                    }));

                }

                else if (i.Name == "Service Modifiers")
                {
                    i.List.ForEach(j =>
                    {
                        var ServiceModifiers = PracticeRepository.ServiceModifiers.FirstOrDefault(k => k.Code == j.StringValues[0].Value);
                        if (ServiceModifiers != null)
                        {
                            ServiceModifiers.Code = j.StringValues.Where(l => l.Name == "Code").Select(l => l).First().Value.ToString();
                            ServiceModifiers.Description = j.StringValues.Where(l => l.Name == "Description").Select(l => l).First().Value.ToString();
                            ServiceModifiers.IsArchived = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                        }
                        else
                        {
                            ServiceModifiers = new ServiceModifier();
                            ServiceModifiers.Code = j.StringValues.Where(l => l.Name == "Code").Select(l => l).First().Value.ToString();
                            ServiceModifiers.Description = j.StringValues.Where(l => l.Name == "Description").Select(l => l).First().Value.ToString();
                            ServiceModifiers.IsArchived = Convert.ToBoolean(j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value);
                        }
                        PracticeRepository.Save(ServiceModifiers);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(k =>
                    {
                        var ServiceModifiers = PracticeRepository.ServiceModifiers.FirstOrDefault(l => l.Code == k);
                        if (ServiceModifiers != null)
                        {
                            PracticeRepository.Delete(ServiceModifiers);
                        }
                    }));
                }


            });

            _repositoryService.Persist(_unitOfWorkProvider.Current.ObjectStateEntries, typeof(IPracticeRepository).FullName);
        }

        private static bool IsPrimitive(Type t)
        {
            // TODO: put any type here that you consider as primitive as I didn't
            // quite understand what your definition of primitive type is
            return new[] { 
            typeof(string), 
            typeof(char),
            typeof(byte),
            typeof(sbyte),
            typeof(ushort),
            typeof(short),
            typeof(uint),
            typeof(int),
            typeof(int?),
            typeof(ulong),
            typeof(long),
            typeof(float),
            typeof(double),
            typeof(decimal),           
            typeof(DateTime)
        }.Contains(t);

        }



    }
}