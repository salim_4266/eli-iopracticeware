﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Setup.Billing;
using IO.Practiceware.Presentation.ViewServices.Setup.Billing;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System.Windows.Media;
using Soaf.Domain;
using Soaf;



[assembly: Component(typeof(BillingSetupViewService), typeof(IBillingSetupViewService))]
namespace IO.Practiceware.Presentation.ViewServices.Setup.Billing
{
    public interface IBillingSetupViewService
    {
        void SaveOrModifyApplicationSettings(DateTime? closeDateTime, NamedViewModel openBalanceType, int diagnosisCodeSet);
        ObservableCollection<BillingDetails> GetBillingSetupData(string selectedItem);
        void SaveIsLoadedItems(ExtendedObservableCollection<BillingSetupListViewModel> isLoadedItems);
    }

    [SupportsUnitOfWork]
    public class BillingSetupViewService : BaseViewService, IBillingSetupViewService
    {
        private readonly IRepositoryService _repositoryService;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly IApplicationSettingsService _applicationSettingsService;

        public BillingSetupViewService(IRepositoryService repositoryService, IUnitOfWorkProvider unitOfWorkProvider, IApplicationSettingsService applicationSettingsService)
        {
            _repositoryService = repositoryService;
            _unitOfWorkProvider = unitOfWorkProvider;
            _applicationSettingsService = applicationSettingsService;
        }


        public void SaveOrModifyApplicationSettings(DateTime? closeDateTime, NamedViewModel openBalanceType, int diagnosisCodeSet)
        {
            if (!closeDateTime.HasValue)
            {
                DeleteApplicationSettings();
            }
            else
            {
                var closeDateSettings = ApplicationSettings.Cached.GetSetting<DateTime>(ApplicationSetting.CloseDateTime) ?? new ApplicationSettingContainer<DateTime>
                                                                                                                             {
                                                                                                                                 Id = null,
                                                                                                                                 Name = ApplicationSetting.CloseDateTime,
                                                                                                                             };

                closeDateSettings.Value = closeDateTime.Value;

                _applicationSettingsService.SetSetting(closeDateSettings);
            }

            //OpenBalanceDeterminer Settings
            var openBalanceTypeSettings = ApplicationSettings.Cached.GetSetting<NamedViewModel>(ApplicationSetting.OpenBalanceDeterminer) ?? new ApplicationSettingContainer<NamedViewModel>
                                          {
                                              Id = null,
                                              Name = ApplicationSetting.OpenBalanceDeterminer,
                                          };

            openBalanceTypeSettings.Value = new NamedViewModel { Id = openBalanceType.Id, Name = openBalanceType.Name };

            _applicationSettingsService.SetSetting(openBalanceTypeSettings);

            //DiagnosisCodeSet
            var diagnosisCodeSetSettings = ApplicationSettings.Cached.GetSetting<string>(ApplicationSetting.DefaultDiagnosisTypeId) ?? new ApplicationSettingContainer<string>
            {
                Id = null,
                Name = ApplicationSetting.DefaultDiagnosisTypeId,
            };

            diagnosisCodeSetSettings.Value = diagnosisCodeSet.ToString();

            _applicationSettingsService.SetSetting(diagnosisCodeSetSettings);
        }

        private void DeleteApplicationSettings()
        {
            var closeDateSettings = ApplicationSettings.Cached.GetSetting<DateTime>(ApplicationSetting.CloseDateTime);

            //Remove the CloseDate Setting if already exists.
            if (closeDateSettings != null) _applicationSettingsService.DeleteSettings(new[] { closeDateSettings.Id.GetValueOrDefault() });
        }
        public virtual ObservableCollection<BillingDetails> GetBillingSetupData(string selectedItem)
        {
            IQueryable query = null;
            var detailscollection = new ExtendedObservableCollection<BillingDetails>();

            if (selectedItem == "Adjustment Types")
            {
                query = from data in PracticeRepository.AdjustmentTypes
                        select data;
            }
            else if (selectedItem == "Financial Information Types")
            {
                query = from data in PracticeRepository.FinancialInformationTypes
                        select data;
            }
            else if (selectedItem == "Payment Methods")
            {
                query = from data in PracticeRepository.PaymentMethods
                        select data;
            }
            else if (selectedItem == "Claim Adjustment Reason Codes")
            {
                query = from data in PracticeRepository.ClaimAdjustmentReasonCodes
                        select data;
            }
            else if (selectedItem == "Billing Organization")
            {
                query = from data in PracticeRepository.BillingOrganizations.OfType<BillingOrganization>()
                        select data;
            }

            else if (selectedItem == "Person Billing Organization")
            {
                query = from data in PracticeRepository.BillingOrganizations.OfType<PersonBillingOrganization>() //.Where(i => i.__EntityType__ == "PersonBillingOrganization")
                        select data;
            }
            else if (selectedItem == "Claim Statuses")
            {
                query = from data in PracticeRepository.ClaimStatuses
                        select data;
            }

            if (query != null)
            {
                foreach (var tableData in query)
                {
                    if (tableData != null)
                    {
                        var details = new BillingDetails();
                        var stringCollection = new ExtendedObservableCollection<StringAndIdViewModel>();
                        var booleanCollection = new ExtendedObservableCollection<BoolAndIdViewModel>();
                        var foreignKeysCollection = new ExtendedObservableCollection<CollectionAndSelectedItemAndIdViewModel>();
                        foreach (var propinfo in tableData.GetType().GetProperties())
                        {
                            if (propinfo.Name == "Id" || propinfo.Name == "HexColor")
                            {
                                continue;
                            }
                            if (propinfo.Name == "Name")
                            {
                                details.Name = Convert.ToString(propinfo.GetValue(tableData, null));
                                details.CanDelete = true;
                            }

                            if (propinfo.PropertyType == typeof(Boolean))
                            {
                                var model = new BoolAndIdViewModel();
                                model.Name = propinfo.Name;
                                model.Value = Convert.ToBoolean(propinfo.GetValue(tableData, null));
                                booleanCollection.Add(model);
                            }
                            else if (IsPrimitive(propinfo.PropertyType))
                            {
                                var model = new StringAndIdViewModel();
                                model.Name = propinfo.Name;
                                model.Value = Convert.ToString(propinfo.GetValue(tableData, null));
                                stringCollection.Add(model);

                            }
                        }
                        details.StringValues = stringCollection;
                        details.BooleanValues = booleanCollection;
                        detailscollection.Add(details);
                    }
                }
            }

            return detailscollection;
        }
        [UnitOfWork]
        public virtual void SaveIsLoadedItems(ExtendedObservableCollection<BillingSetupListViewModel> isLoadedItems)
        {
            isLoadedItems.ForEach(i =>
            {
                if (i.Name == "Adjustment Types")
                {
                    i.List.ForEach(j =>
                    {
                        var AdjustmentTypes = PracticeRepository.AdjustmentTypes.FirstOrDefault(k => k.Name == j.Name);
                        if (AdjustmentTypes != null)
                        {
                            AdjustmentTypes.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            AdjustmentTypes.ShortName = j.StringValues.Where(l => l.Name == "ShortName").Select(l => l).First().Value.ToString();
                            string val = j.StringValues.Where(l => l.Name == "FinancialTypeGroupId").Select(l => l).First().Value.ToString();
                            AdjustmentTypes.FinancialTypeGroupId = Int32.Parse(val == "" ? "0" : val);
                            AdjustmentTypes.IsCash = j.BooleanValues.Where(l => l.Name == "IsCash").Select(l => l).First().Value;
                            AdjustmentTypes.IsDebit = j.BooleanValues.Where(l => l.Name == "IsDebit").Select(l => l).First().Value;
                            AdjustmentTypes.IsPrintOnStatement = j.BooleanValues.Where(l => l.Name == "IsPrintOnStatement").Select(l => l).First().Value;
                            AdjustmentTypes.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;
                            AdjustmentTypes.OrdinalId = Convert.ToInt32(j.StringValues.Where(l => l.Name == "OrdinalId").Select(l => l).First().Value);


                        }

                        else
                        {
                            AdjustmentTypes = new AdjustmentType();
                            AdjustmentTypes.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            AdjustmentTypes.ShortName = j.StringValues.Where(l => l.Name == "ShortName").Select(l => l).First().Value.ToString();
                            string str = j.StringValues.Where(l => l.Name == "FinancialTypeGroupId").Select(l => l).First().Value.ToString();
                            AdjustmentTypes.FinancialTypeGroupId = Int32.Parse(str == "" ? "0" : str);
                            AdjustmentTypes.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;
                            AdjustmentTypes.IsCash = j.BooleanValues.Where(l => l.Name == "IsCash").Select(l => l).First().Value;
                            AdjustmentTypes.IsDebit = j.BooleanValues.Where(l => l.Name == "IsDebit").Select(l => l).First().Value;
                            AdjustmentTypes.IsPrintOnStatement = j.BooleanValues.Where(l => l.Name == "IsPrintOnStatement").Select(l => l).First().Value;
                            AdjustmentTypes.OrdinalId = Convert.ToInt32(j.StringValues.Where(l => l.Name == "OrdinalId").Select(l => l).First().Value);
                        }

                        PracticeRepository.Save(AdjustmentTypes);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(K =>
                        {
                            var AdjustmentTypes = PracticeRepository.AdjustmentTypes.FirstOrDefault(l => l.Name == K);
                            if (AdjustmentTypes != null)
                            {
                                PracticeRepository.Delete(AdjustmentTypes);
                            }
                        }));
                }


                if (i.Name == "Financial Information Types")
                {
                    i.List.ForEach(j =>
                    {
                        var FinancialInformationTypes = PracticeRepository.FinancialInformationTypes.FirstOrDefault(k => k.Name == j.Name);
                        if (FinancialInformationTypes != null)
                        {
                            FinancialInformationTypes.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            FinancialInformationTypes.ShortName = j.StringValues.Where(l => l.Name == "ShortName").Select(l => l).First().Value.ToString();
                            string val = j.StringValues.Where(l => l.Name == "FinancialTypeGroupId").Select(l => l).First().Value.ToString();
                            FinancialInformationTypes.FinancialTypeGroupId = Int32.Parse(val == "" ? "0" : val);
                            FinancialInformationTypes.IsPrintOnStatement = j.BooleanValues.Where(l => l.Name == "IsPrintOnStatement").Select(l => l).First().Value;
                            FinancialInformationTypes.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;
                            FinancialInformationTypes.OrdinalId = Convert.ToInt32(j.StringValues.Where(l => l.Name == "OrdinalId").Select(l => l).First().Value);
                        }

                        else
                        {
                            FinancialInformationTypes = new FinancialInformationType();
                            FinancialInformationTypes.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            FinancialInformationTypes.ShortName = j.StringValues.Where(l => l.Name == "ShortName").Select(l => l).First().Value.ToString();
                            string str = j.StringValues.Where(l => l.Name == "FinancialTypeGroupId").Select(l => l).First().Value.ToString();
                            FinancialInformationTypes.FinancialTypeGroupId = Int32.Parse(str == "" ? "0" : str);
                            FinancialInformationTypes.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;
                            FinancialInformationTypes.IsPrintOnStatement = j.BooleanValues.Where(l => l.Name == "IsPrintOnStatement").Select(l => l).First().Value;
                            FinancialInformationTypes.OrdinalId = Convert.ToInt32(j.StringValues.Where(l => l.Name == "OrdinalId").Select(l => l).First().Value);
                        }

                        PracticeRepository.Save(FinancialInformationTypes);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(K =>
                    {
                        var FinancialInformationTypes = PracticeRepository.FinancialInformationTypes.FirstOrDefault(l => l.Name == K);
                        if (FinancialInformationTypes != null)
                        {
                            PracticeRepository.Delete(FinancialInformationTypes);
                        }
                    }));
                }

                if (i.Name == "Payment Methods")
                {
                    i.List.ForEach(j =>
                    {
                        var PaymentMethods = PracticeRepository.PaymentMethods.FirstOrDefault(k => k.Name == j.Name);
                        if (PaymentMethods != null)
                        {
                            PaymentMethods.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            PaymentMethods.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;
                            PaymentMethods.OrdinalId = Convert.ToInt32(j.StringValues.Where(l => l.Name == "OrdinalId").Select(l => l).First().Value);
                        }

                        else
                        {
                            PaymentMethods = new PaymentMethod();
                            PaymentMethods.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            PaymentMethods.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;
                            PaymentMethods.OrdinalId = Convert.ToInt32(j.StringValues.Where(l => l.Name == "OrdinalId").Select(l => l).First().Value);
                        }

                        PracticeRepository.Save(PaymentMethods);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(K =>
                    {
                        var PaymentMethods = PracticeRepository.PaymentMethods.FirstOrDefault(l => l.Name == K);
                        if (PaymentMethods != null)
                        {
                            PracticeRepository.Delete(PaymentMethods);
                        }
                    }));
                }

                if (i.Name == "Claim Adjustment Reason Codes")
                {
                    i.List.ForEach(j =>
                    {
                        var ClaimAdjustmentReasonCodes = PracticeRepository.ClaimAdjustmentReasonCodes.FirstOrDefault(k => k.Name == j.Name);
                        if (ClaimAdjustmentReasonCodes != null)
                        {
                            ClaimAdjustmentReasonCodes.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            ClaimAdjustmentReasonCodes.Code = j.StringValues.Where(l => l.Name == "Code").Select(l => l).First().Value.ToString();
                            ClaimAdjustmentReasonCodes.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;
                            //ClaimAdjustmentReasonCodes.ExcludeFromSecondaryClaims =  j.BooleanValues.Where(l => l.Name == "ExcludeFromSecondaryClaims").Select(l => l).First().Value;
                        }

                        else
                        {
                            ClaimAdjustmentReasonCodes = new ClaimAdjustmentReasonCode();
                            ClaimAdjustmentReasonCodes.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            ClaimAdjustmentReasonCodes.Code = j.StringValues.Where(l => l.Name == "Code").Select(l => l).First().Value.ToString();
                            ClaimAdjustmentReasonCodes.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;
                            //ClaimAdjustmentReasonCodes.ExcludeFromSecondaryClaims =  j.BooleanValues.Where(l => l.Name == "ExcludeFromSecondaryClaims").Select(l => l).First().Value;
                        }

                        PracticeRepository.Save(ClaimAdjustmentReasonCodes);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(K =>
                    {
                        var ClaimAdjustmentReasonCodes = PracticeRepository.ClaimAdjustmentReasonCodes.FirstOrDefault(l => l.Name == K);
                        if (ClaimAdjustmentReasonCodes != null)
                        {
                            PracticeRepository.Delete(ClaimAdjustmentReasonCodes);
                        }
                    }));
                }

                if (i.Name == "Person Billing Organization")
                {
                    i.List.ForEach(j =>
                    {
                        var PersonBillingOrganization = PracticeRepository.BillingOrganizations.OfType<PersonBillingOrganization>().FirstOrDefault(k => k.Name == j.Name);
                        if (PersonBillingOrganization != null)
                        {
                            PersonBillingOrganization.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.ShortName = j.StringValues.Where(l => l.Name == "ShortName").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.FirstName = j.StringValues.Where(l => l.Name == "FirstName").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.MiddleName = j.StringValues.Where(l => l.Name == "MiddleName").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.LastName = j.StringValues.Where(l => l.Name == "LastName").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.Suffix = j.StringValues.Where(l => l.Name == "Suffix").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;

                        }

                        else
                        {
                            PersonBillingOrganization = new PersonBillingOrganization();
                            PersonBillingOrganization.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.ShortName = j.StringValues.Where(l => l.Name == "ShortName").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.FirstName = j.StringValues.Where(l => l.Name == "FirstName").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.MiddleName = j.StringValues.Where(l => l.Name == "MiddleName").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.LastName = j.StringValues.Where(l => l.Name == "LastName").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.Suffix = j.StringValues.Where(l => l.Name == "Suffix").Select(l => l).First().Value.ToString();
                            PersonBillingOrganization.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;

                        }

                        PracticeRepository.Save(PersonBillingOrganization);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(K =>
                    {
                        var PersonBillingOrganization = PracticeRepository.BillingOrganizations.OfType<PersonBillingOrganization>().FirstOrDefault(l => l.Name == K);
                        if (PersonBillingOrganization != null)
                        {
                            PracticeRepository.Delete(PersonBillingOrganization);
                        }
                    }));
                }

                if (i.Name == "Claim Statuses")
                {
                    i.List.ForEach(j =>
                    {
                        var ClaimStatuses = PracticeRepository.ClaimStatuses.FirstOrDefault(k => k.Name == j.Name);
                        if (ClaimStatuses != null)
                        {
                            ClaimStatuses.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            ClaimStatuses.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;

                        }

                        else
                        {
                            ClaimStatuses = new ClaimStatuses();
                            ClaimStatuses.Name = j.StringValues.Where(l => l.Name == "Name").Select(l => l).First().Value.ToString();
                            ClaimStatuses.IsArchived = j.BooleanValues.Where(l => l.Name == "IsArchived").Select(l => l).First().Value;

                        }

                        PracticeRepository.Save(ClaimStatuses);
                    });

                    i.DeletedItems.IfNotNull(j => j.ForEach(K =>
                    {
                        var ClaimStatuses = PracticeRepository.ClaimStatuses.FirstOrDefault(l => l.Name == K);
                        if (ClaimStatuses != null)
                        {
                            PracticeRepository.Delete(ClaimStatuses);
                        }
                    }));
                }

            });

            _repositoryService.Persist(_unitOfWorkProvider.Current.ObjectStateEntries, typeof(IPracticeRepository).FullName);
        }
        private static bool IsPrimitive(Type t)
        {
            // TODO: put any type here that you consider as primitive as I didn't
            // quite understand what your definition of primitive type is
            return new[] { 
            typeof(string), 
            typeof(char),
            typeof(byte),
            typeof(sbyte),
            typeof(ushort),
            typeof(short),
            typeof(uint),
            typeof(int),
            typeof(int?),
            typeof(ulong),
            typeof(long),
            typeof(float),
            typeof(double),
            typeof(decimal),           
            typeof(DateTime)
        }.Contains(t);

        }
    }
}
