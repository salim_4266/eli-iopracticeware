﻿using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Home;
using IO.Practiceware.Presentation.ViewServices.Home;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;

[assembly: Component(typeof(TimeSheetViewService), typeof(ITimeSheetViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Home
{
    public interface ITimeSheetViewService
    {
        /// <summary>
        /// Saves Employee Clock In and Out time information
        /// </summary>
        int? UpdateTimesheet(TimeSheetTimeBlockViewModel timeBlock);

        int? IsClockedIn();
    }

    public class TimeSheetViewService : BaseViewService, ITimeSheetViewService
    {
        #region ITimeSheetViewService Members

        public int? UpdateTimesheet(TimeSheetTimeBlockViewModel timeBlock)
        {
            EmployeeHour employeeHour;
            if (timeBlock.Id.HasValue)
            {
                employeeHour = PracticeRepository.EmployeeHours.WithId(timeBlock.Id.Value);
                employeeHour.EndDateTime = DateTime.Now.ToClientTime();
                PracticeRepository.Save(employeeHour);
                return null;
            }
            employeeHour = new EmployeeHour();
            employeeHour.StartDateTime = DateTime.Now.ToClientTime();
            employeeHour.UserId = UserContext.Current.UserDetails.Id;
            PracticeRepository.Save(employeeHour);
            return employeeHour.Id;
        }

        public int? IsClockedIn()
        {
            EmployeeHour employeeHour = PracticeRepository.EmployeeHours.OrderByDescending(i => i.Id).FirstOrDefault(i => i.UserId == UserContext.Current.UserDetails.Id && i.StartDateTime != null && i.EndDateTime == null);
            if (employeeHour != null && employeeHour.StartDateTime.Date < DateTime.Now.ToClientTime().Date)
            {
                employeeHour.EndDateTime = employeeHour.StartDateTime.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                PracticeRepository.Save(employeeHour);
                return null;
            }

            return employeeHour == null ? (int?)null : employeeHour.Id;
        }

        #endregion
    }
}