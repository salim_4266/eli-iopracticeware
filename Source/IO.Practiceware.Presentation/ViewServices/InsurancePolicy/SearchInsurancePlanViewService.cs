﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.SearchInsurancePlan;
using IO.Practiceware.Presentation.ViewServices.InsurancePolicy;
using IO.Practiceware.Services.PatientSearch;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;

[assembly: Component(typeof(SearchInsurancePlanViewService), typeof(ISearchInsurancePlanViewService))]
[assembly: Component(typeof(InsurerViewModelMap), AddAllServices = true)]

namespace IO.Practiceware.Presentation.ViewServices.InsurancePolicy
{
    public interface ISearchInsurancePlanViewService
    {
        ObservableCollection<InsurerViewModel> Search(TextSearchMode textSearchMode, string searchText);
    }

    public class SearchInsurancePlanViewService : BaseViewService, ISearchInsurancePlanViewService
    {
        private readonly IMapper<Insurer, InsurerViewModel> _insurerMapper;

        public SearchInsurancePlanViewService(IMapper<Insurer, InsurerViewModel> insurerMapper)
        {
            _insurerMapper = insurerMapper;
        }

        public ObservableCollection<InsurerViewModel> Search(TextSearchMode textSearchMode, string searchText)
        {
            var insurers = PracticeRepository.Insurers.Where(i => !i.IsArchived).Include(i => i.InsurerAddresses.Select(a => a.StateOrProvince),
                                                               i => i.InsurerPhoneNumbers,
                                                               i => i.InsurerPlanType);

            var filterCondition = insurers.GetSearchPredicate(textSearchMode, searchText);
            var filteredInsurers = insurers.Where(filterCondition).ToArray().AsQueryable();

            var exactMatchCondition = filteredInsurers.GetExactMatchPredicate(searchText);
            var exactMatches = filteredInsurers.Where(exactMatchCondition).OrderBy(i => i.Name.ToLowerInvariant()).ThenBy(i => i.PlanName.ToStringIfNotNull(String.Empty).ToLowerInvariant()).SortByAddress();
            var partialMatches = filteredInsurers.Where(i => !exactMatches.Contains(i)).OrderBy(i => i.Name.ToLowerInvariant()).ThenBy(i => i.PlanName.ToStringIfNotNull(String.Empty).ToLowerInvariant()).SortByAddress();

            var viewModels = exactMatches.Concat(partialMatches).Select(_insurerMapper.Map);
            return viewModels.ToExtendedObservableCollection();
        }
    }

    public static class InsurerSearchExtensions
    {

        public static Expression<Func<Insurer, bool>> GetExactMatchPredicate(this IQueryable<Insurer> source, string searchText)
        {
            var predicates = source.CreatePredicateList();
            foreach (var searchTerm in GetSearchTerms(searchText))
            {
                var localSearchTerm = searchTerm;
                predicates.Add(source.CreatePredicate(i => i.Name != null && i.Name.Equals(localSearchTerm, StringComparison.InvariantCultureIgnoreCase)));
                predicates.Add(source.CreatePredicate(i => i.PlanName != null && i.PlanName.Equals(localSearchTerm, StringComparison.InvariantCultureIgnoreCase)));
                predicates.Add(source.CreatePredicate(i => i.PayerCode != null && i.PayerCode.Equals(localSearchTerm, StringComparison.InvariantCultureIgnoreCase)));
                predicates.Add(source.CreatePredicate(i => i.InsurerPhoneNumbers
                                                                           .Select(pn => (pn.CountryCode + pn.AreaCode + pn.ExchangeAndSuffix + pn.Extension).Replace("-", string.Empty)
                                                                                             .Replace("(", string.Empty)
                                                                                             .Replace(")", string.Empty))
                                                                           .Any(val => val.Equals(localSearchTerm, StringComparison.InvariantCultureIgnoreCase))));
            }
            return predicates.Any() ? predicates.Aggregate((x, y) => x.Or(y)) : null;
        }

        public static Expression<Func<Insurer, bool>> GetSearchPredicate(this IQueryable<Insurer> source, TextSearchMode textSearchMode, string searchText)
        {
            var predicates = source.CreatePredicateList();
            foreach (var searchTerm in GetSearchTerms(searchText))
            {
                var localSearchTerm = searchTerm;
                switch (textSearchMode)
                {
                    case TextSearchMode.BeginsWith:
                        {
                            predicates.Add(source.CreatePredicate(i => i.Name != null && i.Name.StartsWithIgnoreCase(localSearchTerm)));
                            predicates.Add(source.CreatePredicate(i => i.PlanName != null && i.PlanName.StartsWithIgnoreCase(localSearchTerm)));
                            predicates.Add(source.CreatePredicate(i => i.PayerCode != null && i.PayerCode.StartsWithIgnoreCase(localSearchTerm)));
                            predicates.Add(source.CreatePredicate(i => i.InsurerPhoneNumbers
                                                                           .Select(pn => (pn.CountryCode + pn.AreaCode + pn.ExchangeAndSuffix + pn.Extension).Replace("-", string.Empty)
                                                                                             .Replace("(", string.Empty)
                                                                                             .Replace(")", string.Empty))
                                                                           .Any(val => val.StartsWithIgnoreCase(localSearchTerm))));
                            break;
                        }
                    case TextSearchMode.Contains:
                        {
                            predicates.Add(source.CreatePredicate(i => i.Name != null && i.Name.ContainsIgnoreCase(localSearchTerm)));
                            predicates.Add(source.CreatePredicate(i => i.PlanName != null && i.PlanName.ContainsIgnoreCase(localSearchTerm)));
                            predicates.Add(source.CreatePredicate(i => i.PayerCode != null && i.PayerCode.ContainsIgnoreCase(localSearchTerm)));
                            predicates.Add(source.CreatePredicate(i => i.InsurerPhoneNumbers
                                                                           .Select(pn => (pn.CountryCode + pn.AreaCode + pn.ExchangeAndSuffix + pn.Extension).Replace("-", string.Empty)
                                                                                             .Replace("(", string.Empty)
                                                                                             .Replace(")", string.Empty))
                                                                           .Any(val => val.ContainsIgnoreCase(localSearchTerm))));
                            break;
                        }
                    default:
                        throw new Exception("{0} not supported".FormatWith(textSearchMode));
                }
            }
            return predicates.Any() ? predicates.Aggregate((x, y) => x.Or(y)) : null;
        }

        private static IEnumerable<string> GetSearchTerms(string searchText)
        {
            #region characters to split and remove

            var charactersToSplit = new[] 
                        {
                            ',', 
                            ' '
                        };
            var charactersToRemove = new[]
                        {
                            "-", 
                            "(", 
                            ")", 
                            "[", 
                            "]"
                        };

            #endregion

            foreach (var splitText in searchText.Split(charactersToSplit))
            {
                var localText = charactersToRemove.Aggregate(splitText, (current, charToRemove) => current.Replace(charToRemove, string.Empty));
                yield return localText;
            }
        }

        public static IOrderedQueryable<Insurer> SortByAddress(this IOrderedQueryable<Insurer> query)
        {
            ////Then Address
            query = query.ThenBy(i => i.InsurerAddresses.Select(a => a.AllLines()).Join(Environment.NewLine, true));

            return query;
        }

    }

    public class InsurerViewModelMap : IMap<Insurer, InsurerViewModel>
    {
        public InsurerViewModelMap(ICustomMapperFactory customMapperFactory)
        {
            var addressMapper = customMapperFactory.CreateAddressViewModelMapper<InsurerAddress>();

            Members = this.CreateMembers(i => new InsurerViewModel
                {
                    Address = i.InsurerAddresses.Any() ? addressMapper.Map(i.InsurerAddresses.First()) : null,
                    Comments = i.Comment,
                    Id = i.Id,
                    IsReferralRequired = i.IsReferralRequired,
                    Name = i.Name,
                    PayerCode = i.PayerCode,
                    PhoneNumber = i.InsurerPhoneNumbers.Any() ? i.InsurerPhoneNumbers.First().ToString() : string.Empty,
                    PlanName = i.PlanName,
                    PlanType = i.InsurerPlanType.IfNotNull(x => x.Name, string.Empty),
                }).ToArray();
        }

        public IEnumerable<IMapMember<Insurer, InsurerViewModel>> Members { get; private set; }
    }

}
