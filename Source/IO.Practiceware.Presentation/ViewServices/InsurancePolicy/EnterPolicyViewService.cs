﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.EnterPolicyDetails;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance;
using IO.Practiceware.Presentation.ViewServices.InsurancePolicy;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

[assembly: Component(typeof(EnterPolicyDetailsViewService), typeof(IEnterPolicyDetailsViewService))]
[assembly: Component(typeof(InsurancePolicyViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(BasicPolicyholderViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(BasicInsurerViewModelMap), AddAllServices = true)]

namespace IO.Practiceware.Presentation.ViewServices.InsurancePolicy
{
    [DataContract]
    [KnownType(typeof(PersonViewModel))]
    [KnownType(typeof(InsurancePolicyViewModel))]
    [KnownType(typeof(BasicInsurerViewModel))]
    public class EnterPolicyLoadInformation
    {
        [DataMember]
        public virtual PersonViewModel Policyholder { get; set; }

        [DataMember]
        public virtual InsurancePolicyViewModel InsurancePolicy { get; set; }

        [DataMember]
        public virtual BasicInsurerViewModel Insurer { get; set; }
    }

    public interface IEnterPolicyDetailsViewService
    {
        void SaveNewInsurancePolicy(InsurancePolicyViewModel insurancePolicy, int patientId, InsuranceType insuranceType, IEnumerable<byte[]> scannedDocuments, PolicyHolderRelationshipType relationshipType);
        void SaveExistingInsurancePolicy(InsurancePolicyViewModel insurancePolicy);
        EnterPolicyLoadInformation GetLoadInformation(int policyholderId, int insurerId, int? insurancePolicyId = null);
        PersonViewModel GetPolicyholder(int policyholderId);

        /// <summary>
        /// Validate this new policy against the existing ones for the patient to make sure it can be added.
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="insurancePolicyViewModel"></param>
        /// <param name="insuranceType"></param>
        /// <returns></returns>
        string ValidateCanInsertNewPolicyForPatient(int patientId, InsurancePolicyViewModel insurancePolicyViewModel, InsuranceType insuranceType);
    }

    public class EnterPolicyDetailsViewService : BaseViewService, IEnterPolicyDetailsViewService
    {
        private readonly IMapper<Model.InsurancePolicy, InsurancePolicyViewModel> _insurancePolicyMapper;
        private readonly IMapper<Patient, PersonViewModel> _policyholderMapper;
        private readonly IMapper<Insurer, BasicInsurerViewModel> _insurerMapper;
        private readonly Func<InsurancePolicyViewModel> _createInsurancePolicyViewModel;

        public EnterPolicyDetailsViewService(IMapper<Model.InsurancePolicy, InsurancePolicyViewModel> insurancePolicyMapper,
                                             IMapper<Patient, PersonViewModel> policyholderMapper,
                                             IMapper<Insurer, BasicInsurerViewModel> insurerMapper,
                                             Func<InsurancePolicyViewModel> createInsurancePolicyViewModel)
        {
            _insurancePolicyMapper = insurancePolicyMapper;
            _policyholderMapper = policyholderMapper;
            _insurerMapper = insurerMapper;
            _createInsurancePolicyViewModel = createInsurancePolicyViewModel;
        }

        private int GetMaxOrdinalIdByPatientIdAndInsuranceType(int patientId, InsuranceType it)
        {
            return PracticeRepository.PatientInsurances.Where(pi => pi.InsuredPatientId == patientId 
                && pi.InsuranceType == it 
                && !pi.IsDeleted)
                .ToList()
                .Select(pi => pi.OrdinalId)
                .DefaultIfEmpty(0)
                .Max();
        }

        public void SaveNewInsurancePolicy(InsurancePolicyViewModel viewModel, int patientId, InsuranceType insuranceType, IEnumerable<byte[]> scannedDocuments, PolicyHolderRelationshipType relationshipType)
        {
            // create the new insurance policy
            var insurancePolicy = new Model.InsurancePolicy
                                  {
                                      InsurerId = viewModel.InsurerId,
                                      PolicyholderPatientId = viewModel.PolicyholderId,
                                      Copay = viewModel.CoPay,
                                      Deductible = viewModel.Deductible,
                                      GroupCode = viewModel.GroupCode,
                                      PolicyCode = viewModel.PolicyCode,
                                      StartDateTime = viewModel.StartDate.GetValueOrDefault()
                                  };

            int nextOrdinalId;

            // is the user trying to create a linked policy?
            var patientIsNotThePolicyHolder = patientId != viewModel.PolicyholderId;
            if (patientIsNotThePolicyHolder)
            {
                // find the correct ordinalId based on the existing insurances (if any)
                nextOrdinalId = GetMaxOrdinalIdByPatientIdAndInsuranceType(patientId, insuranceType);

                // now we create a patient insurance for the intended patient                
                insurancePolicy.PatientInsurances.Add(new PatientInsurance
                {
                    EndDateTime = viewModel.EndDate,
                    InsuranceType = insuranceType,
                    PolicyholderRelationshipType = relationshipType,
                    InsuredPatientId = patientId,
                    OrdinalId = ++nextOrdinalId
                });

                PracticeRepository.Save(insurancePolicy);
            }

            // find the correct ordinalId based on the existing insurances (if any)
            nextOrdinalId = GetMaxOrdinalIdByPatientIdAndInsuranceType(viewModel.PolicyholderId, insuranceType);

            // we need to create a default patient insurance for this new policy                               
            insurancePolicy.PatientInsurances.Add(new PatientInsurance
                                                  {
                                                      EndDateTime = viewModel.EndDate,
                                                      InsuranceType = insuranceType,
                                                      PolicyholderRelationshipType = PolicyHolderRelationshipType.Self,
                                                      InsuredPatientId = viewModel.PolicyholderId,
                                                      OrdinalId = ++nextOrdinalId
                                                  });

            PracticeRepository.Save(insurancePolicy);

            if (scannedDocuments != null)
            {
                var localScannedDocuments = scannedDocuments.ToArray();
                foreach (var patientInsurance in insurancePolicy.PatientInsurances)
                {
                    var newDocuments = localScannedDocuments.Select(d => new PatientInsuranceDocument { Content = d });
                    patientInsurance.PatientInsuranceDocuments = patientInsurance.PatientInsuranceDocuments.Union(newDocuments).ToArray();
                }

                PracticeRepository.Save(insurancePolicy);
            }
        }

        public string ValidateCanInsertNewPolicyForPatient(int patientId, InsurancePolicyViewModel insurancePolicyViewModel, InsuranceType insuranceType)
        {
            var patient = PracticeRepository.Patients
                .Include(x => x.PatientInsurances.Select(y => y.InsurancePolicy.Insurer))
                .Include(x => x.PatientInsurances.Select(y => y.InsurancePolicy.PolicyholderPatient))
                .Single(x => x.Id == patientId);

            var newIncomingInsurancePolicy = new List<PatientInsurance>
                                                 {
                                                     new PatientInsurance
                                                         {
                                                             InsuranceType = insuranceType,
                                                             EndDateTime = insurancePolicyViewModel.EndDate,
                                                             InsurancePolicy = new Model.InsurancePolicy
                                                                                   {
                                                                                       InsurerId = insurancePolicyViewModel.InsurerId,
                                                                                       PolicyholderPatientId = insurancePolicyViewModel.PolicyholderId,
                                                                                       StartDateTime = insurancePolicyViewModel.StartDate.GetValueOrDefault(),
                                                                                       PolicyholderPatient = PracticeRepository.Patients.Single(x => x.Id == insurancePolicyViewModel.PolicyholderId)
                                                                                   }
                                                         }
                                                 };

            if (patientId != insurancePolicyViewModel.PolicyholderId)
            {
                // check the insurer to see if it allows dependents
                var insurer = PracticeRepository.Insurers.Single(i => i.Id == insurancePolicyViewModel.InsurerId);
                if (!insurer.AllowDependents) return "We apologize but the selected insurer \"{0}\" does not allow dependents on policies".FormatWith(insurer.Name);
            }

            var existingPolicies = patient.PatientInsurances.ToPolicyViewModels();
            var newPolicy = newIncomingInsurancePolicy.ToPolicyViewModels().First();

            var maxOrdinalId = existingPolicies.GetMaxRank(insuranceType);
            newPolicy.PolicyRank = ++maxOrdinalId;

            var validationResultForConflictingDateRanges = newPolicy.ValidatePolicyRankOrderForIntersectingDateRanges(existingPolicies);
            if (validationResultForConflictingDateRanges.IsNotSuccessful()) { return validationResultForConflictingDateRanges.ErrorMessage; }

            return null;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SaveExistingInsurancePolicy(InsurancePolicyViewModel viewModel)
        {
            var insurancePolicy = PracticeRepository.InsurancePolicies.Include(ip => ip.PatientInsurances).Single(ip => ip.Id == viewModel.Id);
            insurancePolicy.InsurerId = viewModel.InsurerId;
            insurancePolicy.PolicyholderPatientId = viewModel.PolicyholderId;
            insurancePolicy.Copay = viewModel.CoPay;
            insurancePolicy.Deductible = viewModel.Deductible;
            insurancePolicy.GroupCode = viewModel.GroupCode;
            insurancePolicy.PolicyCode = viewModel.PolicyCode;
            insurancePolicy.StartDateTime = viewModel.StartDate.GetValueOrDefault();

            PracticeRepository.Save(insurancePolicy);
        }

        public EnterPolicyLoadInformation GetLoadInformation(int policyholderId, int insurerId, int? insurancePolicyId = null)
        {
            var loadInformation = new EnterPolicyLoadInformation();
            var parallelLoadActions = new List<Action>();

            if (insurancePolicyId.HasValue)
            {
                parallelLoadActions.Add(() => loadInformation.InsurancePolicy = GetInsurancePolicy(insurancePolicyId.Value));
            }
            else
            {
                loadInformation.InsurancePolicy = _createInsurancePolicyViewModel();
                loadInformation.InsurancePolicy.PolicyholderId = policyholderId;
                loadInformation.InsurancePolicy.InsurerId = insurerId;
            }

            parallelLoadActions.Add(() => loadInformation.Policyholder = GetPolicyholder(policyholderId));
            parallelLoadActions.Add(() => loadInformation.Insurer = GetInsurer(insurerId));
            parallelLoadActions.ForAllInParallel(a => a());

            return loadInformation;
        }

        private InsurancePolicyViewModel GetInsurancePolicy(int insurancePolicyId)
        {
            return _insurancePolicyMapper.Map(PracticeRepository.InsurancePolicies.Include(ip => ip.Insurer).Include(ip => ip.PatientInsurances).WithId(insurancePolicyId));
        }

        public PersonViewModel GetPolicyholder(int policyholderId)
        {
            return _policyholderMapper.Map(PracticeRepository.Patients.WithId(policyholderId));
        }

        private BasicInsurerViewModel GetInsurer(int insurerId)
        {
            return _insurerMapper.Map(PracticeRepository.Insurers.WithId(insurerId));
        }
    }

    public class InsurancePolicyViewModelMap : IMap<Model.InsurancePolicy, InsurancePolicyViewModel>
    {
        public InsurancePolicyViewModelMap()
        {
            Members = this.CreateMembers(i => new InsurancePolicyViewModel
                {
                    Id = i.Id,
                    CoPay = i.Copay,
                    Deductible = i.Deductible,
                    GroupCode = i.GroupCode,
                    PolicyCode = i.PolicyCode,
                    PolicyNumberFormat = i.Insurer.PolicyNumberFormat,
                    StartDate = i.StartDateTime,
                    InsurerId = i.InsurerId,
                    PolicyholderId = i.PolicyholderPatientId,
                    EndDate = i.PatientInsuranceForPolicyholder.IfNotNull(pi => pi.EndDateTime)
                }).ToArray();
        }

        public IEnumerable<IMapMember<Model.InsurancePolicy, InsurancePolicyViewModel>> Members { get; private set; }
    }

    public class BasicPolicyholderViewModelMap : IMap<Patient, PersonViewModel>
    {
        public BasicPolicyholderViewModelMap()
        {
            Members = this.CreateMembers(i => new PersonViewModel
                {
                    FirstName = i.FirstName,
                    Id = i.Id,
                    LastName = i.LastName
                }).ToArray();
        }

        public IEnumerable<IMapMember<Patient, PersonViewModel>> Members { get; private set; }
    }

    public class BasicInsurerViewModelMap : IMap<Insurer, BasicInsurerViewModel>
    {
        public BasicInsurerViewModelMap()
        {
            Members = this.CreateMembers(i => new BasicInsurerViewModel
                {
                    Id = i.Id,
                    Name = i.Name,
                    PlanName = i.PlanName
                }).ToArray();
        }

        public IEnumerable<IMapMember<Insurer, BasicInsurerViewModel>> Members { get; private set; }
    }
}
