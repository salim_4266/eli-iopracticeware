﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.SelectPolicies;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientCommunicationPreferences;
using IO.Practiceware.Presentation.ViewServices.InsurancePolicy;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;

[assembly: Component(typeof(SelectPoliciesViewService), typeof(ISelectPoliciesViewService))]
[assembly: Component(typeof(SelectInsurancePolicyViewModelMap), AddAllServices = true)]

namespace IO.Practiceware.Presentation.ViewServices.InsurancePolicy
{
    [DataContract]
    [KnownType(typeof(PersonViewModel))]
    [KnownType(typeof(ExtendedObservableCollection<PatientInsuranceViewModel>))]
    public class SelectPoliciesLoadInformation
    {
        [DataMember]
        [Dependency]
        public ObservableCollection<PatientInsuranceViewModel> PatientInsurances { get; set; }
        [DataMember]
        public PersonViewModel Policyholder { get; set; }
    }

    public interface ISelectPoliciesViewService
    {
        SelectPoliciesLoadInformation GetLoadInformation(int policyholderId, int patientId, InsuranceType insuranceType);       

        /// <summary>
        /// Unlinks a patient's PatientInsurances given a collection of policyholder patient insurance ids.
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="insurancePolicyIdsToUnlink"></param>
        /// <returns></returns>
        string UnlinkPolicies(int patientId, IEnumerable<int> insurancePolicyIdsToUnlink);

        /// <summary>
        /// Links a patient's PatientInsurances given a collection of policyholder patient insurance ids.
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="relationshipToPolicyholder"></param>
        /// <param name="insurancePolicyIdsToLink"></param>
        /// <returns></returns>
        string LinkPolicies(int patientId, PolicyHolderRelationshipType relationshipToPolicyholder, IEnumerable<int> insurancePolicyIdsToLink);

        /// <summary>
        /// Links a patient's PatientInsurances given a collection of policyholder patient insurance ids.
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="relationshipToPolicyholder"></param>
        /// <param name="insurancePolicyIdsToLink"></param>
        /// <param name="scannedDocuments"></param>
        /// <returns></returns>
        string LinkPolicies(int patientId, PolicyHolderRelationshipType relationshipToPolicyholder, IEnumerable<int> insurancePolicyIdsToLink, IEnumerable<byte[]> scannedDocuments);
    }

    public class SelectPoliciesViewService : BaseViewService, ISelectPoliciesViewService
    {
        private readonly IEnterPolicyDetailsViewService _enterPolicyDetailsViewService;
        private readonly Func<bool, IMapper<PatientInsurance, PatientInsuranceViewModel>> _createPatientInsuranceMapper;
        private readonly IPatientCommunicationPreferencesViewService _patientCommunicationsPreferencesViewService;

        public SelectPoliciesViewService(IEnterPolicyDetailsViewService enterPolicyDetailsViewService,
                                        Func<bool, IMapper<PatientInsurance, PatientInsuranceViewModel>> createPatientInsuranceMapper,
                                        IPatientCommunicationPreferencesViewService patientCommunicationsPreferencesViewService)
        {
            _enterPolicyDetailsViewService = enterPolicyDetailsViewService;
            _createPatientInsuranceMapper = createPatientInsuranceMapper;
            _patientCommunicationsPreferencesViewService = patientCommunicationsPreferencesViewService;
        }

        public string UnlinkPolicies(int patientId,  IEnumerable<int> insurancePolicyIdsToUnlink)
        {
            return AlterLinkedPolicies(patientId, PolicyHolderRelationshipType.Unknown, new List<int>(), insurancePolicyIdsToUnlink, new List<byte[]>());
        }

        public string LinkPolicies(int patientId, PolicyHolderRelationshipType relationshipToPolicyholder, IEnumerable<int> insurancePolicyIdsToLink)
        {
            return AlterLinkedPolicies(patientId, relationshipToPolicyholder, insurancePolicyIdsToLink, new List<int>(), new List<byte[]>());
        }

        public string LinkPolicies(int patientId, PolicyHolderRelationshipType relationshipToPolicyholder, IEnumerable<int> insurancePolicyIdsToLink, IEnumerable<byte[]> scannedDocuments)
        {
            return AlterLinkedPolicies(patientId, relationshipToPolicyholder, insurancePolicyIdsToLink, new List<int>(), scannedDocuments);
        }

        private string AlterLinkedPolicies(int patientId, PolicyHolderRelationshipType relationshipToPolicyholder, IEnumerable<int> insurancePolicyIdsToLink, IEnumerable<int> insurancePolicyIdsToUnlink, IEnumerable<byte[]> scannedDocuments)
        {
            var idsToLink = insurancePolicyIdsToLink.ToList();
            var policyholderInsurancesToLink = PracticeRepository.PatientInsurances
                                                   .Include(pi => pi.InsurancePolicy.Insurer)
                                                   .Where(pi => idsToLink.Contains(pi.InsurancePolicyId)
                                                                && !pi.IsDeleted 
                                                                && pi.InsuredPatientId == pi.InsurancePolicy.PolicyholderPatientId 
                                                                && pi.InsuredPatientId != patientId)
                                                   .ToList();

            string userMessage = null;
            var policyholderInsurancesNotLinkable = policyholderInsurancesToLink.Where(p => !p.InsurancePolicy.Insurer.AllowDependents).ToList();            
            if (policyholderInsurancesNotLinkable.Any())
            {
                policyholderInsurancesToLink = policyholderInsurancesToLink.Except(policyholderInsurancesNotLinkable).ToList();
                userMessage = "We apologize but the following policies could not be linked because the plan does not support dependents: ";
                userMessage += policyholderInsurancesNotLinkable
                                    .Select(p => string.Format("{0}{1}", p.InsurancePolicy.Insurer.Name, p.InsurancePolicy.Insurer.PlanName.IsNotNullOrEmpty() ? " - " + p.InsurancePolicy.Insurer.PlanName : string.Empty))
                                    .Join();
            }
            idsToLink = policyholderInsurancesToLink.Select(p => p.InsurancePolicyId).ToList();

            var patientsExistingInsurances = PracticeRepository.PatientInsurances.Include(pi => pi.InsurancePolicy).Where(pi => pi.InsuredPatientId == patientId).ToList();

            // try to see if this patient already has a patientInsurance linked to the policies.  if there is a link and it's deleted, we just re-enable it.
            var insurancesToReenable = patientsExistingInsurances
                                                .Where(pi =>  idsToLink.Contains(pi.InsurancePolicyId))
                                                .GroupBy(pi => pi.InsurancePolicyId)
                                                .Where(g => g.Any() && g.All(p => p.IsDeleted))
                                                .Select(g => g.First())
                                                .ToList();                                                
            

            // create new patientInsurances for the patient where he doesn't already have a link
            var insurancesToAdd = policyholderInsurancesToLink
                .Where(pi => patientsExistingInsurances.All(pi2 => pi.InsurancePolicyId != pi2.InsurancePolicyId))
                .GroupBy(i => i.InsuranceType)
                .SelectMany(g => g
                    .Select((pi, index) => new PatientInsurance
                    {
                        EndDateTime = pi.EndDateTime,
                        InsurancePolicyId = pi.InsurancePolicyId,
                        InsuranceType = pi.InsuranceType,
                        InsuredPatientId = patientId,
                        OrdinalId = patientsExistingInsurances.GetMaxOrdinalId(patientId, pi.InsuranceType) + 1 + index,
                        PolicyholderRelationshipType = relationshipToPolicyholder
                    })).ToList();

            var insurancesToDelete = insurancePolicyIdsToUnlink.IsNotNullOrEmpty() ? patientsExistingInsurances.Where(pi => !pi.IsDeleted && pi.InsuredPatientId == patientId && insurancePolicyIdsToUnlink.Contains(pi.InsurancePolicyId)) : new List<PatientInsurance>();
            insurancesToDelete.ForEachWithSinglePropertyChangedNotification(i => i.IsDeleted = true);

            var allInsurancesToSave = new List<PatientInsurance>().Union(insurancesToReenable).Union(insurancesToAdd).Union(insurancesToDelete);
            var docs = scannedDocuments.IsNotNullOrEmpty() ? scannedDocuments.Select(d => new PatientInsuranceDocument { Content = d }).ToList() : new List<PatientInsuranceDocument>();
            docs.ForEach(doc => allInsurancesToSave.ForEachWithSinglePropertyChangedNotification(i =>
                                                            {
                                                                i.PatientInsuranceDocuments = i.PatientInsuranceDocuments ?? new List<PatientInsuranceDocument>();
                                                                i.PatientInsuranceDocuments.Add(doc);
                                                            }));

            if (insurancesToAdd.Count >= 1)
            {

                var insurancePolicy = insurancesToAdd.FirstOrDefault();
                var policy = PracticeRepository.InsurancePolicies.FirstOrDefault(i => i.Id == insurancePolicy.InsurancePolicyId);
                if (policy != null)
                {
                    var policyHolderPatientId = policy.PolicyholderPatientId;
                    var existingRelationshipFromPolicyHolderToPatient = PracticeRepository.PatientRelationships.FirstOrDefault(i => i.ToPatientId == patientId && i.FromPatientId == policyHolderPatientId);
                    var existingRelationshipFromPatientToPolicyHolder = PracticeRepository.PatientRelationships.FirstOrDefault(i => i.ToPatientId == policyHolderPatientId && i.FromPatientId == patientId);
                    if (existingRelationshipFromPatientToPolicyHolder == null)
                        _patientCommunicationsPreferencesViewService.AddContactToPatient(patientId, policyHolderPatientId, relationshipToPolicyholder.ToString());
                    if (existingRelationshipFromPolicyHolderToPatient == null)
                        _patientCommunicationsPreferencesViewService.AddContactToPatient(policyHolderPatientId, patientId);

                    if (existingRelationshipFromPatientToPolicyHolder != null && existingRelationshipFromPatientToPolicyHolder.PatientRelationshipDescription.IsNullOrEmpty())
                    {
                        existingRelationshipFromPatientToPolicyHolder.PatientRelationshipDescription = relationshipToPolicyholder.ToString();
                        PracticeRepository.Save(existingRelationshipFromPatientToPolicyHolder);
                    }
                }
        }

            allInsurancesToSave.ForEachWithSinglePropertyChangedNotification(PracticeRepository.Save);

            return userMessage;
        }       

        public SelectPoliciesLoadInformation GetLoadInformation(int policyholderId, int patientId, InsuranceType insuranceType)
        {
            var loadInformation = new SelectPoliciesLoadInformation();
            var parallelLoadActions = new List<Action>();

            parallelLoadActions.Add(() => loadInformation.PatientInsurances = GetPatientInsurances(policyholderId, patientId, insuranceType));
            parallelLoadActions.Add(() => loadInformation.Policyholder = _enterPolicyDetailsViewService.GetPolicyholder(policyholderId));
            parallelLoadActions.ForAllInParallel(a => a());

            return loadInformation;
        }

        public string ValidateAreValidToAdd(int patientId, InsuranceType insuranceType, IEnumerable<PatientInsuranceViewModel> policyholderPatientInsurances)
        {
            var patient = PracticeRepository.Patients.Include(x => x.PatientInsurances.Select(y => y.InsurancePolicy.Insurer)).Single(x => x.Id == patientId);

            var ids = policyholderPatientInsurances.Select(x => x.Id);

            var newInsurances = PracticeRepository.PatientInsurances
                .Include(x => x.InsurancePolicy.Insurer)
                .Include(x => x.InsurancePolicy.PolicyholderPatient)
                .Where(x => ids.Contains(x.Id))
                .ToList();

            if (policyholderPatientInsurances.Count() == 1)
            {
                // check the insurer to see if it allows dependents
                var insurancepolicy = PracticeRepository.InsurancePolicies.Include(ip => ip.Insurer).Single(ip => ip.Id == policyholderPatientInsurances.First().InsurancePolicyId);
                if (!insurancepolicy.Insurer.AllowDependents) return "We apologize but the insurer \"{0}\" for the selected policy does not allow dependents.".FormatWith(insurancepolicy.Insurer.Name);     
            }
           

            var existingPolicies = patient.PatientInsurances.ToPolicyViewModels();
            var newPolicies = newInsurances.ToPolicyViewModels();

            var nextRank = existingPolicies.GetMaxRank(insuranceType);
            newPolicies.ForEach(x => x.PolicyRank = ++nextRank);

            var validationResultForIntersectingDateRanges = existingPolicies.Union(newPolicies).ValidatePolicyRankOrderForIntersectingDateRanges();
            if (validationResultForIntersectingDateRanges.IsNotSuccessful()) { return validationResultForIntersectingDateRanges.ErrorMessage; }

            return null;
        }

        private ObservableCollection<PatientInsuranceViewModel> GetPatientInsurances(int policyholderId, int patientId, InsuranceType insuranceType)
        {
            var piQuery = PracticeRepository.PatientInsurances.Where(pi => pi.InsurancePolicy.PolicyholderPatientId == policyholderId && pi.InsuranceType == insuranceType);

            var allpolicyholderPatientInsurances = piQuery.Include(pi => pi.InsurancePolicy.Insurer.InsurerPlanType).Where(pi => pi.InsurancePolicy.Insurer.AllowDependents).ToArray();

            var policyholderPatientInsurances = allpolicyholderPatientInsurances.Where(pi => pi.InsuredPatientId == policyholderId).ToArray();

            // Do not include deleted patient insurances for given patient.
            // If included (shouldn't be), then it may match with deleted policy holder patient insurances noted above.
            var patientPatientInsurances = allpolicyholderPatientInsurances.Where(pi => pi.InsuredPatientId == patientId && !pi.IsDeleted).ToArray();

            var comparer = new PatientInsuranceComparer();
            var linkedPolicyholderPatientInsurances = policyholderPatientInsurances.Where(pi => patientPatientInsurances.Contains(pi, comparer));
            var unlinkedPolicyholderPatientInsurances = policyholderPatientInsurances.Where(pi => !patientPatientInsurances.Contains(pi, comparer));

            var linkedMapper = _createPatientInsuranceMapper(true);
            var linkedViewModels = linkedPolicyholderPatientInsurances.Select(linkedMapper.Map);

            var unlinkedMapper = _createPatientInsuranceMapper(false);
            var unlinkedViewModels = unlinkedPolicyholderPatientInsurances.Select(unlinkedMapper.Map);

            return linkedViewModels.Concat(unlinkedViewModels)
                    .OrderBy(pi => pi.InsuranceType)
                        .ThenBy(pi => pi.EndDate)
                        .ThenBy(pi => pi.Rank)
                    .ToExtendedObservableCollection();
        }

        /// <summary>
        /// Used to determine if a policyholder's PatientInsurance is 'linked' 
        /// (EndDateTime, InsurancePolicyId and InsuranceType being equal).
        /// </summary>
        private class PatientInsuranceComparer : IEqualityComparer<PatientInsurance>
        {
            public bool Equals(PatientInsurance x, PatientInsurance y)
            {
                return
                       x.InsurancePolicyId == y.InsurancePolicyId &&
                       x.InsuranceType == y.InsuranceType;
            }

            public int GetHashCode(PatientInsurance obj)
            {
                return Objects.GetHashCodeWithNullCheck(obj.EndDateTime) ^
                       obj.InsurancePolicyId.GetHashCode() ^
                       obj.InsuranceType.GetHashCode();
            }
        }
    }

    public class SelectInsurancePolicyViewModelMap : IMap<PatientInsurance, PatientInsuranceViewModel>
    {
        public SelectInsurancePolicyViewModelMap(bool isLinked)
        {
            Members = this.CreateMembers(i => new PatientInsuranceViewModel
                {
                    CoPay = i.InsurancePolicy.Copay,
                    Deductible = i.InsurancePolicy.Deductible,
                    GroupNumber = i.InsurancePolicy.GroupCode,
                    Id = i.Id,
                    InsurancePlan = "{0}, {1}".FormatWith(i.InsurancePolicy.Insurer.Name, i.InsurancePolicy.Insurer.PlanName),
                    InsuranceType = i.InsuranceType,
                    EndDate = i.EndDateTime,
                    PlanType = i.InsurancePolicy.Insurer.InsurerPlanType.IfNotNull(x => x.Name),
                    PolicyNumber = i.InsurancePolicy.PolicyCode,
                    StartDate = i.InsurancePolicy.StartDateTime,
                    Rank = i.OrdinalId,
                    IsLinked = isLinked,
                    IsDeleted = i.IsDeleted,
                    InsurancePolicyId = i.InsurancePolicyId
                }).ToArray();
        }

        public IEnumerable<IMapMember<PatientInsurance, PatientInsuranceViewModel>> Members { get; private set; }
    }

}
