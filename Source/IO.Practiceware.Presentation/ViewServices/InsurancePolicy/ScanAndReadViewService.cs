﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePolicy.ScanAndRead;
using IO.Practiceware.Presentation.ViewServices.InsurancePolicy;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;


[assembly: Component(typeof(ScanAndReadViewService), typeof(IScanAndReadViewService))]

namespace IO.Practiceware.Presentation.ViewServices.InsurancePolicy
{
    public class ScanAndReadLoadInformation
    {
        public ObservableCollection<NamedViewModel> PolicyholderRelationshipTypes { get; set; }
        public ObservableCollection<NamedViewModel> PlanTypes { get; set; }
    }

    public interface IScanAndReadViewService
    {
        OcrInsurancePolicyViewModel GetViewModel(IEnumerable<CardScannerDataModel> ocrScannerValues);
        ScanAndReadLoadInformation GetLoadInformation();
    }

    public class ScanAndReadViewService : BaseViewService, IScanAndReadViewService
    {
        public OcrInsurancePolicyViewModel GetViewModel(IEnumerable<CardScannerDataModel> ocrScannerValues)
        {
            if (ocrScannerValues == null) ocrScannerValues = new CardScannerDataModel[] { };
            var localScannerValues = ocrScannerValues.ToArray();
            var viewModel = new OcrInsurancePolicyViewModel
                {
                    CoPay = localScannerValues.GetValueAsDecimal(CardScannerDataField.CopaySpecialist) ?? localScannerValues.GetValueAsDecimal(CardScannerDataField.CopayOfficeVisit),
                    Deductible = localScannerValues.GetValueAsDecimal(CardScannerDataField.Deductible),
                    StartDate = localScannerValues.GetValueAsDateTime(CardScannerDataField.EffectiveDate),
                    EndDate = localScannerValues.GetValueAsDateTime(CardScannerDataField.ExpirationDate),
                    GroupCode = localScannerValues.GetValueAsString(CardScannerDataField.GroupNumber),
                    InsuranceName = localScannerValues.GetValueAsString(CardScannerDataField.PlanProvider),
                    PlanName = localScannerValues.GetValueAsString(CardScannerDataField.GroupName),
                    PolicyCode = localScannerValues.GetValueAsString(CardScannerDataField.MemberId),
                    PolicyholderName = "{1} {0}".FormatWith(localScannerValues.GetValueAsString(CardScannerDataField.LastName),
                                                            localScannerValues.GetValueAsString(CardScannerDataField.FirstName).IsNotNullOrEmpty()
                                                                ? localScannerValues.GetValueAsString(CardScannerDataField.FirstName)[0].ToString()
                                                                : string.Empty),
                };

            var planType = localScannerValues.GetValueAsString(CardScannerDataField.PlanType);
            if (planType != null)
            {
                viewModel.PlanType = PracticeRepository.InsurerPlanTypes.Select(pt => new NamedViewModel { Id = pt.Id, Name = pt.Name }).FirstOrDefault(pt => planType == pt.Name);
            }

            return viewModel;
        }

        public ScanAndReadLoadInformation GetLoadInformation()
        {
            var loadInformation = new ScanAndReadLoadInformation();

            new Action[]
                {
                    () => loadInformation.PolicyholderRelationshipTypes = GetPolicyholderRelationshipTypes(),
                    () => loadInformation.PlanTypes = GetPlanTypes()
                }.ForAllInParallel(a => a());

            return loadInformation;
        }

        private static ObservableCollection<NamedViewModel> GetPolicyholderRelationshipTypes()
        {
            var result = new ExtendedObservableCollection<NamedViewModel>();
            foreach (PolicyHolderRelationshipType relationshipType in Enum.GetValues(typeof(PolicyHolderRelationshipType)))
            {
                result.Add(new NamedViewModel { Name = relationshipType.GetDisplayName(), Id = (int)relationshipType });
            }
            return result;
        }

        private ObservableCollection<NamedViewModel> GetPlanTypes()
        {
            return PracticeRepository.InsurerPlanTypes.Select(pt => new NamedViewModel { Id = pt.Id, Name = pt.Name }).ToExtendedObservableCollection();
        }
    }

    public static class CardScannerDataFieldExtensions
    {
        public static string GetValueAsString(this IEnumerable<CardScannerDataModel> source, CardScannerDataField dataField)
        {
            var kvp = source.FirstOrDefault(i => i.Key == dataField);
            if (kvp == null) { return default(string); }

            return kvp.Value as string;
        }

        public static decimal? GetValueAsDecimal(this IEnumerable<CardScannerDataModel> source, CardScannerDataField dataField)
        {
            var kvp = source.FirstOrDefault(i => i.Key == dataField);
            if (kvp == null) { return default(decimal); }

            decimal value;
            return decimal.TryParse(kvp.Value.ToString(), out value) ? value : default(decimal);
        }

        public static DateTime? GetValueAsDateTime(this IEnumerable<CardScannerDataModel> source, CardScannerDataField dataField)
        {
            var kvp = source.FirstOrDefault(i => i.Key == dataField);
            if (kvp == null) { return null; }

            DateTime value;
            return DateTime.TryParse(kvp.Value.ToString(), out value) ? value : (DateTime?)null;
        }

        public static object GetValue(this IEnumerable<CardScannerDataModel> source, CardScannerDataField dataField, Type valueType)
        {
            if (valueType == typeof(string)) { return GetValueAsString(source, dataField); }
            if (valueType == typeof(decimal?)) { return GetValueAsDecimal(source, dataField); }
            if (valueType == typeof(DateTime?)) { return GetValueAsDateTime(source, dataField); }

            throw new Exception("GetValue for type {0} not implemented.".FormatWith(valueType.Name));
        }
    }
}
