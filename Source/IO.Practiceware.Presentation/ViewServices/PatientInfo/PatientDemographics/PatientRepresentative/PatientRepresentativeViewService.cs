﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using IO.Practiceware.Configuration;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.PatientRepresentative;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using IO.Practiceware.Presentation.Common;
using IO.Practiceware.Data;
using Soaf.Data;

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics.PatientRepresentative
{
    public class PatientRepresentativeViewService : BaseViewService
    {
        private IMapper<StateOrProvince, StateOrProvinceViewModel> _stateOrProvinceMapper;
        private IMapper<IO.Practiceware.Model.PatientAddress, AddressViewModel> _addressMapper;
        /// <constructor>
        /// Initialise Connection
        /// </constructor>
        public PatientRepresentativeViewService()
        {
            InitMappers();
        }


        private void InitMappers()
        {
            _addressMapper = Mapper.Factory.CreateAddressViewModelMapper<IO.Practiceware.Model.PatientAddress>();

            _stateOrProvinceMapper = Mapper.Factory.CreateStateOrProvinceViewModelMapper();

        }
        public PatientRepresentativeViewModel GetPatientRepresentativeDetails(int _patientId)
        {
            NameAndAbbreviationViewModel countrydescription = new NameAndAbbreviationViewModel();
            DataTable dtPatientRep = new DataTable();
            PatientRepresentativeViewModel PatRepresViewModel = new PatientRepresentativeViewModel();
            string _SelectQry = "Select Top(1) * From model.PatientRepresentatives where PatientId = " + _patientId;
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _SelectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtPatientRep.Load(reader);
                    }
                }
                dbConnection.Close();
            }
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                string slctqry = "Select Top(1) c.Id as CountryId, c.Abbreviation3Letters as CountryName,* From model.PatientRepresentatives pr inner join model.Countries c on pr.Country=c.Abbreviation3Letters where PatientId = " + _patientId;
                DataTable dataTable = dbConnection.Execute<DataTable>(slctqry);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    countrydescription.Id = int.Parse(dataTable.Rows[0]["CountryId"].ToString());
                    countrydescription.Abbreviation= dataTable.Rows[0]["CountryName"].ToString();
                }
            }
            if (dtPatientRep.Rows.Count == 0)
            {
                string _insertQry = "Insert into model.PatientRepresentatives(PatientId) Values (@PatientId)";
                using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                {
                    dbConnection.Open();
                    using (IDbCommand cmd = dbConnection.CreateCommand())
                    {
                        IDbDataParameter PatientId = cmd.CreateParameter();
                        PatientId.Direction = ParameterDirection.Input;
                        PatientId.Value = _patientId;
                        PatientId.ParameterName = "@PatientId";
                        PatientId.DbType = DbType.Int32;
                        cmd.Parameters.Add(PatientId);

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = _insertQry;
                        int Status = cmd.ExecuteNonQuery();
                        if (Status == 1)
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = _SelectQry;
                            using (var reader = cmd.ExecuteReader())
                            {
                                dtPatientRep.Load(reader);
                            }
                        }
                    }
                    dbConnection.Close();
                }
            }
            foreach (DataRow row in dtPatientRep.Rows)
            {
                PatRepresViewModel = new PatientRepresentativeViewModel();
                PatRepresViewModel.Country = new NameAndAbbreviationViewModel();  
                PatRepresViewModel.Addresses = new ViewModels.Common.AddressViewModel();
                PatRepresViewModel.Addresses.StateOrProvince = new ViewModels.Common.StateOrProvinceViewModel();
                PatRepresViewModel.Title = row["Salutation"].ToString();
                PatRepresViewModel.FirstName = row["FirstName"].ToString();
                PatRepresViewModel.MiddleName = row["MidleName"].ToString();
                PatRepresViewModel.LastName = row["LastName"].ToString();
                PatRepresViewModel.Relationship = row["Relationship"].ToString();
                PatRepresViewModel.RelationshipCode = row["RelationshipCode"].ToString();
                PatRepresViewModel.Addresses.Line1 = row["Addressline1"].ToString();
                PatRepresViewModel.Addresses.Line2 = row["Addressline2"].ToString();
                PatRepresViewModel.Addresses.City = row["City"].ToString();
                PatRepresViewModel.Zip = row["Zip"].ToString();
                PatRepresViewModel.State = row["State"].ToString();
                PatRepresViewModel.County = row["County"].ToString();
                PatRepresViewModel.Country.Id = countrydescription.Id;
                PatRepresViewModel.Country.Abbreviation = countrydescription.Abbreviation;
                PatRepresViewModel.HomePhone = row["HomePhone"].ToString();
                PatRepresViewModel.WorkPhone = row["WorkPhone"].ToString();
                PatRepresViewModel.CellPhone = row["CellPhone"].ToString();
                PatRepresViewModel.DateOfBirth = row["DateOfBirth"].ToString();
                PatRepresViewModel.Email = Convert.ToString(row["Email"]);
            }
            return PatRepresViewModel;
        }

        public ObservableCollection<NameAndAbbreviationViewModel> GetStateOrProvince()
        {

            var stateOrProvince = PracticeRepository.StateOrProvinces.Select(x => new NameAndAbbreviationViewModel { Id = x.Id, Abbreviation = x.Abbreviation, Name = x.Name }).OrderBy(x => x.Abbreviation).ToObservableCollection();

            return stateOrProvince;
        }

        public ObservableCollection<StateOrProvinceViewModel> GetStateOrProvinces()
        {
            var stateOrProvinces = PracticeRepository.StateOrProvinces.Include(i => i.Country)
                    .Select(_stateOrProvinceMapper.Map)
                    .ToObservableCollection();
            return stateOrProvinces;
        }

        public ObservableCollection<NameAndAbbreviationViewModel> GetZipCodesAndCity()
        {
            ObservableCollection<NameAndAbbreviationViewModel> ZipCodes = new ObservableCollection<NameAndAbbreviationViewModel>();
            NameAndAbbreviationViewModel model;
            DataTable datatable = new DataTable();
            string _SelectQry = "Select * from dbo.zipcodes";
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _SelectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        datatable.Load(reader);
                    }
                }
                dbConnection.Close();
            }
            foreach (DataRow row in datatable.Rows)
            {
                model = new NameAndAbbreviationViewModel()
                {
                    Id = (int)row["Id"],
                    Name = row["CityName"].ToString(),
                    Abbreviation = row["ZIPCode"].ToString(),
                };
                ZipCodes.Add(model);
            }
            return ZipCodes;
        }

        public int UpdatePatientRepersentativeDetails(PatientRepresentativeViewModel dtPatientRep)
        {
            int patientId;
            string _updateQry = "Update model.PatientRepresentatives Set Salutation = @Title, FirstName = @FirstName, LastName = @LastName, MidleName= @MiddleName, Relationship = @Relationship, RelationshipCode = @RelationshipCode,Addressline1 = @AddressLine1, Addressline2 = @AddressLine2,City = @City, State= @State, Zip =@Zip, County = @County, Country = @Country,  HomePhone= @HomePhone, WorkPhone= @WorkPhone, CellPhone= @CellPhone, DateofBirth = @DateOfBirth, Status =@Status, Email=@Email  where PatientId = " + dtPatientRep.PatientId;
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                using (IDbCommand cmd = dbConnection.CreateCommand())
                {
                    IDbDataParameter PatientId = cmd.CreateParameter();
                    PatientId.Direction = ParameterDirection.Input;
                    PatientId.Value = dtPatientRep.PatientId.ToString();
                    PatientId.ParameterName = "@PatientId";
                    PatientId.DbType = DbType.Int32;
                    cmd.Parameters.Add(PatientId);

                    IDbDataParameter Title = cmd.CreateParameter();
                    Title.Direction = ParameterDirection.Input;
                    Title.Value = dtPatientRep.Title.ToString(); //Title
                    Title.ParameterName = "@Title";
                    Title.DbType = DbType.String;
                    cmd.Parameters.Add(Title);

                    IDbDataParameter FirstName = cmd.CreateParameter();
                    FirstName.Direction = ParameterDirection.Input;
                    FirstName.Value = dtPatientRep.FirstName.ToString();
                    FirstName.ParameterName = "@FirstName";
                    FirstName.DbType = DbType.String;
                    cmd.Parameters.Add(FirstName);

                    IDbDataParameter MiddleName = cmd.CreateParameter();
                    MiddleName.Direction = ParameterDirection.Input;
                    MiddleName.Value = dtPatientRep.MiddleName.ToString();
                    MiddleName.ParameterName = "@MiddleName";
                    MiddleName.DbType = DbType.String;
                    cmd.Parameters.Add(MiddleName);


                    IDbDataParameter LastName = cmd.CreateParameter();
                    LastName.Direction = ParameterDirection.Input;
                    LastName.Value = dtPatientRep.LastName.ToString();
                    LastName.ParameterName = "@LastName";
                    LastName.DbType = DbType.String;
                    cmd.Parameters.Add(LastName);

                    IDbDataParameter AddressLine1 = cmd.CreateParameter();
                    AddressLine1.Direction = ParameterDirection.Input;
                    AddressLine1.Value = dtPatientRep.Addresses.Line1.ToString();
                    AddressLine1.ParameterName = "@AddressLine1";
                    AddressLine1.DbType = DbType.String;
                    cmd.Parameters.Add(AddressLine1);

                    IDbDataParameter AddressLine2 = cmd.CreateParameter();
                    AddressLine2.Direction = ParameterDirection.Input;
                    AddressLine2.Value = dtPatientRep.Addresses.Line2.ToString();
                    AddressLine2.ParameterName = "@AddressLine2";
                    AddressLine2.DbType = DbType.String;
                    cmd.Parameters.Add(AddressLine2);

                    IDbDataParameter City = cmd.CreateParameter();
                    City.Direction = ParameterDirection.Input;
                    City.Value = dtPatientRep.Addresses.City.ToString();
                    City.ParameterName = "@City";
                    City.DbType = DbType.String;
                    cmd.Parameters.Add(City);

                    IDbDataParameter State = cmd.CreateParameter();
                    State.Direction = ParameterDirection.Input;
                    State.Value = dtPatientRep.State.ToString();
                    State.ParameterName = "@State";
                    State.DbType = DbType.String;
                    cmd.Parameters.Add(State);

                    IDbDataParameter Zip = cmd.CreateParameter();
                    Zip.Direction = ParameterDirection.Input;
                    Zip.Value = dtPatientRep.Zip.ToString();
                    Zip.ParameterName = "@Zip";
                    Zip.DbType = DbType.String;
                    cmd.Parameters.Add(Zip);

                    IDbDataParameter HomePhone = cmd.CreateParameter();
                    HomePhone.Direction = ParameterDirection.Input;
                    HomePhone.Value = dtPatientRep.HomePhone.ToString();
                    HomePhone.ParameterName = "@HomePhone";
                    HomePhone.DbType = DbType.String;
                    cmd.Parameters.Add(HomePhone);

                    IDbDataParameter WorkPhone = cmd.CreateParameter();
                    WorkPhone.Direction = ParameterDirection.Input;
                    WorkPhone.Value = dtPatientRep.WorkPhone.ToString();
                    WorkPhone.ParameterName = "@WorkPhone";
                    WorkPhone.DbType = DbType.String;
                    cmd.Parameters.Add(WorkPhone);

                    IDbDataParameter CellPhone = cmd.CreateParameter();
                    CellPhone.Direction = ParameterDirection.Input;
                    CellPhone.Value = dtPatientRep.CellPhone.ToString();
                    CellPhone.ParameterName = "@CellPhone";
                    CellPhone.DbType = DbType.String;
                    cmd.Parameters.Add(CellPhone);

                    IDbDataParameter Relationship = cmd.CreateParameter();
                    Relationship.Direction = ParameterDirection.Input;
                    Relationship.Value = dtPatientRep.Relationship.ToString();
                    Relationship.ParameterName = "@Relationship";
                    Relationship.DbType = DbType.String;
                    cmd.Parameters.Add(Relationship);

                    IDbDataParameter RelationshipCode = cmd.CreateParameter();
                    RelationshipCode.Direction = ParameterDirection.Input;
                    RelationshipCode.Value = dtPatientRep.RelationshipCode.ToString();
                    RelationshipCode.ParameterName = "@RelationshipCode";
                    RelationshipCode.DbType = DbType.String;
                    cmd.Parameters.Add(RelationshipCode);

                    IDbDataParameter County = cmd.CreateParameter();
                    County.Direction = ParameterDirection.Input;
                    County.Value = dtPatientRep.Country.ToString();
                    County.ParameterName = "@County";
                    County.DbType = DbType.String;
                    cmd.Parameters.Add(County);

                    //IDbDataParameter CountryCode = cmd.CreateParameter();
                    //CountryCode.Direction = ParameterDirection.Input;
                    //CountryCode.Value = dtPatientRep.CountryCode.ToString();
                    //CountryCode.ParameterName = "@CountryCode";
                    //CountryCode.DbType = DbType.String;
                    //cmd.Parameters.Add(CountryCode);

                    IDbDataParameter Country = cmd.CreateParameter();
                    Country.Direction = ParameterDirection.Input;
                    Country.Value = dtPatientRep.Country.ToString();
                    Country.ParameterName = "@Country";
                    Country.DbType = DbType.String;
                    cmd.Parameters.Add(Country);


                    IDbDataParameter DateOfBirth = cmd.CreateParameter();
                    DateOfBirth.Direction = ParameterDirection.Input;
                    DateOfBirth.Value = dtPatientRep.DateOfBirth.ToString();
                    DateOfBirth.ParameterName = "@DateOfBirth";
                    DateOfBirth.DbType = DbType.String;
                    cmd.Parameters.Add(DateOfBirth);

                    IDbDataParameter Status = cmd.CreateParameter();
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = dtPatientRep.PatientStatusId.ToString();
                    Status.ParameterName = "@Status";
                    Status.DbType = DbType.String;
                    cmd.Parameters.Add(Status);

                    IDbDataParameter Email = cmd.CreateParameter();
                    Email.Direction = ParameterDirection.Input;
                    Email.Value = Convert.ToString(dtPatientRep.Email);
                    Email.ParameterName = "@Email";
                    Email.DbType = DbType.String;
                    cmd.Parameters.Add(Email);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _updateQry;
                    cmd.ExecuteNonQuery();
                    patientId = dtPatientRep.PatientId;
                }
            }
            return patientId;
        }


    }
}
