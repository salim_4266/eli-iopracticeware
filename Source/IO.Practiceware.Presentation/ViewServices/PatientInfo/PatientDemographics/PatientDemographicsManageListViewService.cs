﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

[assembly: Component(typeof (PatientDemographicsManageListViewService), typeof (IPatientDemographicsManageListViewService))]

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics
{
    public interface IPatientDemographicsManageListViewService
    {
        LoadPatientDemographicsFieldsListsInformation LoadPatientDemographicsFieldsListsInformation();

        /// <summary>
        /// Updates PatientDemographicsFieldConfigurations
        /// </summary>
        /// <param name="patientDemographicsConfiguredFieldGroups"></param>
        void UpdatePatientDemographicsFieldConfigurations(IEnumerable<PatientDemographicsConfigureFieldGroupViewModel> patientDemographicsConfiguredFieldGroups);

        /// <summary>
        /// Updates PatientDemographicsDefaultValueConfigurations
        /// </summary>
        /// <param name="patientDemographicsConfiguredDefaultValues"></param>
        void UpdatePatientDemographicsDefaultValueConfigurations(IEnumerable<PatientDemographicsConfigureDefaultValueViewModel> patientDemographicsConfiguredDefaultValues);

        /// <summary>
        /// Updates all list's OrdinalIds used in Patient Demographics
        /// </summary>
        /// <param name="patientDemographicsLists"></param>
        void UpdatePatientDemographicsListsInformation(IEnumerable<PatientDemographicsListViewModel> patientDemographicsLists);
    }

    internal class PatientDemographicsManageListViewService : BaseViewService, IPatientDemographicsManageListViewService
    {
        private readonly Func<PatientDemographicsConfigureDefaultValueViewModel> _createPatientDemographicsConfigureDefaultValueViewModel;
        private readonly Func<PatientDemographicsConfigureFieldViewModel> _createPatientDemographicsConfigureFieldViewModel;
        private readonly Func<PatientDemographicsListViewModel> _createPatientDemographicsListViewModel;

        public PatientDemographicsManageListViewService(Func<PatientDemographicsConfigureFieldViewModel> createPatientDemographicsConfigureFieldViewModel, Func<PatientDemographicsConfigureDefaultValueViewModel> createPatientDemographicsConfigureDefaultValueViewModel, Func<PatientDemographicsListViewModel> createPatientDemographicsListViewModel)
        {
            _createPatientDemographicsConfigureFieldViewModel = createPatientDemographicsConfigureFieldViewModel;
            _createPatientDemographicsConfigureDefaultValueViewModel = createPatientDemographicsConfigureDefaultValueViewModel;
            _createPatientDemographicsListViewModel = createPatientDemographicsListViewModel;
        }

        #region IPatientDemographicsManageListViewService Members

        public LoadPatientDemographicsFieldsListsInformation LoadPatientDemographicsFieldsListsInformation()
        {
            List<PatientDemographicsConfigureFieldViewModel> fieldConfigurations = null;
            List<PatientDemographicsConfigureDefaultValueViewModel> defaultCheckBoxValueConfigurations = null;
            List<PatientDemographicsConfigureDefaultValueViewModel> defaultComboBoxValueConfigurations = null;
            List<PatientDemographicsListViewModel> lists = null;

            var patientDemographicsFieldConfigurations = LoadAllPatientDemographicsFieldConfigurations().ToList();
            var patientDemographicsDefaultValueConfigurations = LoadPatientDemographicsDefaultValueConfigurations(patientDemographicsFieldConfigurations).ToList();

            var a1 = new Action(() => fieldConfigurations = LoadPatientDemographicsFieldConfigurations(patientDemographicsFieldConfigurations).ToList());
            var a2 = new Action(() => defaultCheckBoxValueConfigurations = patientDemographicsDefaultValueConfigurations.Where(c => c.Id == (int)PatientDemographicsFieldConfigurationId.HasHipaaConsent).ToList());
            var a3 = new Action(() => defaultComboBoxValueConfigurations = patientDemographicsDefaultValueConfigurations.Where(c => c.Id == (int)PatientDemographicsFieldConfigurationId.AssignBenefitsToProviderId || c.Id == (int)PatientDemographicsFieldConfigurationId.ReleaseMedicalInformationId).ToList());
            var a4 = new Action(() => lists = LoadPatientDemographicsLists().ToList());
            new[] {a1, a2, a3, a4}.ForAllInParallel(a => a());

            return new LoadPatientDemographicsFieldsListsInformation
                {
                    PatientDemographicsFieldConfigurations = fieldConfigurations,
                    PatientDemographicsDefaultCheckBoxValueConfigurations = defaultCheckBoxValueConfigurations,
                    PatientDemographicsDefaultComboBoxValueConfigurations = defaultComboBoxValueConfigurations,
                    PatientDemographicsLists = lists
                };
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void UpdatePatientDemographicsFieldConfigurations(IEnumerable<PatientDemographicsConfigureFieldGroupViewModel> patientDemographicsConfiguredFieldGroups)
        {
            if (patientDemographicsConfiguredFieldGroups != null)
            {
                PatientDemographicsFieldConfiguration[] patientDemographicsFieldConfigurations = PracticeRepository.PatientDemographicsFieldConfigurations.ToArray();
                foreach (PatientDemographicsConfigureFieldGroupViewModel patientDemographicsConfiguredFieldGroup in patientDemographicsConfiguredFieldGroups)
                {
                    PatientDemographicsConfigureFieldViewModel[] patientDemographicsConfiguredFields = patientDemographicsConfiguredFieldGroup.ConfigureFields.ToArray();
                    foreach (PatientDemographicsConfigureFieldViewModel patientDemographicsConfiguredField in patientDemographicsConfiguredFields)
                    {
                        PatientDemographicsFieldConfiguration patientDemographicsFieldConfiguration = patientDemographicsFieldConfigurations.First(d => d.Id == patientDemographicsConfiguredField.Id);
                        patientDemographicsFieldConfiguration.IsVisible = patientDemographicsConfiguredField.IsVisible;
                        patientDemographicsFieldConfiguration.IsRequired = patientDemographicsConfiguredField.IsRequired;
                    }
                }
                PracticeRepository.Save(patientDemographicsFieldConfigurations);
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void UpdatePatientDemographicsDefaultValueConfigurations(IEnumerable<PatientDemographicsConfigureDefaultValueViewModel> patientDemographicsConfiguredDefaultValues)
        {
            if (patientDemographicsConfiguredDefaultValues != null)
            {
                PatientDemographicsConfigureDefaultValueViewModel[] patientDemographicsConfigureDefaultValueViewModels = patientDemographicsConfiguredDefaultValues as PatientDemographicsConfigureDefaultValueViewModel[] ?? patientDemographicsConfiguredDefaultValues.ToArray();
                List<int> ids = patientDemographicsConfigureDefaultValueViewModels.Select(d => d.Id).ToList();
                PatientDemographicsFieldConfiguration[] patientDemographicsDefaultCheckBoxConfigurations = PracticeRepository.PatientDemographicsFieldConfigurations.Where(p => ids.Contains(p.Id)).ToArray();
                foreach (PatientDemographicsConfigureDefaultValueViewModel patientDemographicsConfiguredDefaultValue in patientDemographicsConfigureDefaultValueViewModels.ToList())
                {
                    PatientDemographicsFieldConfiguration patientDemographicsDefaultCheckboxConfiguration = patientDemographicsDefaultCheckBoxConfigurations.First(d => d.Id == patientDemographicsConfiguredDefaultValue.Id);
                    patientDemographicsDefaultCheckboxConfiguration.DefaultValue = patientDemographicsConfiguredDefaultValue.DefaultValue;
                }
                PracticeRepository.Save(patientDemographicsDefaultCheckBoxConfigurations);
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void UpdatePatientDemographicsListsInformation(IEnumerable<PatientDemographicsListViewModel> patientDemographicsLists)
        {
            if (patientDemographicsLists != null)
            {
                foreach (PatientDemographicsListViewModel patientDemographicsList in patientDemographicsLists)
                {
                    UpdatePatientDemographicsLists(patientDemographicsList);
                }
            }
        }

        #endregion

        private IEnumerable<PatientDemographicsFieldConfiguration> LoadAllPatientDemographicsFieldConfigurations()
        {
            return PracticeRepository.PatientDemographicsFieldConfigurations.ToList();
        }
        
        private IEnumerable<PatientDemographicsConfigureFieldViewModel> LoadPatientDemographicsFieldConfigurations(IEnumerable<PatientDemographicsFieldConfiguration> patientDemographicsFieldConfigurations)
        {
            return patientDemographicsFieldConfigurations.ToList().Select(d => _createPatientDemographicsConfigureFieldViewModel()
                .Modify(vm => vm.Id = d.Id)
                .Modify(vm => vm.Name = ((PatientDemographicsFieldConfigurationId)d.Id).GetDisplayName())
                .Modify(vm => vm.GroupName = ((PatientDemographicsFieldConfigurationId)d.Id).GetAttribute<DisplayAttribute>().GroupName)
                .Modify(vm => vm.IsVisibleConfigurable = d.IsVisibilityConfigurable)
                .Modify(vm => vm.IsVisible = d.IsVisible)
                .Modify(vm => vm.IsRequiredConfigurable = d.IsRequiredConfigurable)
                .Modify(vm => vm.IsRequired = d.IsRequired)).ToList();
        }

        private IEnumerable<PatientDemographicsConfigureDefaultValueViewModel> LoadPatientDemographicsDefaultValueConfigurations(IEnumerable<PatientDemographicsFieldConfiguration> patientDemographicsFieldConfigurations)
        {
            var patientDemographicsConfigureDefaultValueFields = patientDemographicsFieldConfigurations.Where(p => p.Id == (int) PatientDemographicsFieldConfigurationId.HasHipaaConsent
                    || p.Id == (int) PatientDemographicsFieldConfigurationId.ReleaseMedicalInformationId 
                    || p.Id == (int) PatientDemographicsFieldConfigurationId.AssignBenefitsToProviderId).ToList()
                    .Select(d => _createPatientDemographicsConfigureDefaultValueViewModel()
                        .Modify(vm => vm.Id = d.Id)
                        .Modify(vm => vm.Name = ((PatientDemographicsFieldConfigurationId) d.Id).GetDisplayName())
                        .Modify(vm => vm.DefaultValue = d.DefaultValue)).ToList();

            foreach (var field in patientDemographicsConfigureDefaultValueFields)
            {
                if (field.Id  == (int) PatientDemographicsFieldConfigurationId.ReleaseMedicalInformationId )
                    field.Items = NamedViewModelExtensions.ToNamedViewModel<ReleaseOfInformationCode>().OrderBy(x => x.Name).ToObservableCollection();
                else if (field.Id  == (int) PatientDemographicsFieldConfigurationId.AssignBenefitsToProviderId)
                    field.Items = NamedViewModelExtensions.ToNamedViewModel<PaymentOfBenefitsToProvider>().OrderBy(x => x.Name).ToObservableCollection();
            }
            return patientDemographicsConfigureDefaultValueFields;
        }

        private IEnumerable<PatientDemographicsListViewModel> LoadPatientDemographicsLists()
        {
            var loadInformation = new ExtendedObservableCollection<PatientDemographicsListViewModel>();
            foreach (PatientDemographicsList listName in Enum.GetValues(typeof (PatientDemographicsList)))
            {
                NamedViewModel[] allItems = GetAllItems(listName).ToArray();
                string patientDemographicsListName = listName.GetDisplayName();
                PatientDemographicsListViewModel patientDemographicsListViewModel = _createPatientDemographicsListViewModel();
                patientDemographicsListViewModel.Id = (int) listName;
                patientDemographicsListViewModel.Name = patientDemographicsListName;
                patientDemographicsListViewModel.FavoriteItems = allItems.Take(5).ToExtendedObservableCollection();
                patientDemographicsListViewModel.NonFavoriteItems = allItems.Skip(5).ToExtendedObservableCollection();
                loadInformation.Add(patientDemographicsListViewModel);
            }

            return loadInformation;
        }

        private IEnumerable<NamedViewModel> GetAllItems(PatientDemographicsList listName)
        {
            switch (listName)
            {
                case PatientDemographicsList.ExternalProviders:
                    return PracticeRepository.ExternalContacts.OfType<ExternalProvider>().Where(x => !x.IsArchived).OrderBy(p => p.OrdinalId).ToList().Select(p => new NamedViewModel {Id = p.Id, Name = p.DisplayName}).ToList();
                case PatientDemographicsList.Language:
                    return PracticeRepository.Languages.OrderBy(p => p.OrdinalId).ToList().Select(p => new NamedViewModel {Id = p.Id, Name = p.Name}).ToList();
                case PatientDemographicsList.Physicians:
                    return PracticeRepository.Users.OfType<Doctor>().OrderBy(p => p.OrdinalId).ToList().Select(p => new NamedViewModel {Id = p.Id, Name = p.DisplayName}).ToList();
                case PatientDemographicsList.ReferringCategory:
                    return PracticeRepository.PatientReferralSourceTypes.OrderBy(p => p.OrdinalId).ToList().Select(p => new NamedViewModel {Id = p.Id, Name = p.Name}).ToList();
                default:
                    return new ExtendedObservableCollection<NamedViewModel>();
            }
        }

        private void UpdatePatientDemographicsLists(PatientDemographicsListViewModel patientDemographicsList)
        {
            var allItems = new List<NamedViewModel>();
            if (patientDemographicsList.FavoriteItems != null) allItems = allItems.Union(patientDemographicsList.FavoriteItems).ToList();
            if (patientDemographicsList.NonFavoriteItems != null) allItems = allItems.Union(patientDemographicsList.NonFavoriteItems).ToList();
            if (allItems.Count == 0) return;
            List<int> allIds = allItems.Select(p => p.Id).ToList();
            switch ((PatientDemographicsList) patientDemographicsList.Id)
            {
                case PatientDemographicsList.ExternalProviders:
                    {
                        ExternalProvider[] externalProviders = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().Where(p => allIds.Contains(p.Id)).ToArray();
                        PracticeRepository.SyncDependencyCollection(allItems, externalProviders,
                                                                    (m, e) => m.Id == e.Id, (viewModel, mapping) =>
                                                                        {
                                                                            mapping.Id = viewModel.Id;
                                                                            mapping.OrdinalId = allItems.IndexOf(viewModel);
                                                                        });
                        PracticeRepository.Save(externalProviders);
                        break;
                    }
                case PatientDemographicsList.Language:
                    {
                        Language[] patientLanguages = PracticeRepository.Languages.Where(p => allIds.Contains(p.Id)).ToArray();
                        PracticeRepository.SyncDependencyCollection(allItems, patientLanguages,
                                                                    (m, e) => m.Id == e.Id, (viewModel, mapping) =>
                                                                        {
                                                                            mapping.Id = viewModel.Id;
                                                                            mapping.OrdinalId = allItems.IndexOf(viewModel);
                                                                        });
                        PracticeRepository.Save(patientLanguages);
                        break;
                    }
                case PatientDemographicsList.Physicians:
                    {
                        Doctor[] doctors = PracticeRepository.Users.OfType<Doctor>().Where(p => allIds.Contains(p.Id)).ToArray();
                        PracticeRepository.SyncDependencyCollection(allItems, doctors,
                                                                    (m, e) => m.Id == e.Id, (viewModel, mapping) =>
                                                                        {
                                                                            mapping.Id = viewModel.Id;
                                                                            mapping.OrdinalId = allItems.IndexOf(viewModel);
                                                                        });
                        PracticeRepository.Save(doctors);
                        break;
                    }
                case PatientDemographicsList.ReferringCategory:
                    {
                        PatientReferralSourceType[] patientReferralSourceTypes = PracticeRepository.PatientReferralSourceTypes.Where(p => allIds.Contains(p.Id)).ToArray();
                        PracticeRepository.SyncDependencyCollection(allItems, patientReferralSourceTypes,
                                                                    (m, e) => m.Id == e.Id, (viewModel, mapping) =>
                                                                        {
                                                                            mapping.Id = viewModel.Id;
                                                                            mapping.OrdinalId = allItems.IndexOf(viewModel);
                                                                        });
                        PracticeRepository.Save(patientReferralSourceTypes);
                        break;
                    }
            }
        }
    }
}