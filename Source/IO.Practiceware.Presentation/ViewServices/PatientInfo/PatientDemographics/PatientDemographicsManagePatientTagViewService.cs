﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;

[assembly: Component(typeof(PatientDemographicsManagePatientTagViewService), typeof(IPatientDemographicsManagePatientTagViewService))]

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics
{
    public interface IPatientDemographicsManagePatientTagViewService
    {
        LoadPatientDemographicsTagsInformation LoadPatientDemographicsTagsInformation();

        IEnumerable<PatientDemographicsPatientTagViewModel> LoadArchivedTags();

        /// <summary>
        /// Saves a new Patient Tag and returns the Id of the newly created Patient Tag
        /// </summary>
        /// <param name="patientTag"></param>
        /// <returns></returns>
        int SavePatientTag(PatientDemographicsPatientTagViewModel patientTag);

        /// <summary>
        /// Archives Patient Tag 
        /// </summary>
        /// <param name="patientTagId"></param>
        /// <returns></returns>
        PatientDemographicsPatientTagViewModel ArchivePatientTag(int patientTagId);

        /// <summary>
        /// Restores Patient Tag 
        /// </summary>
        /// <param name="patientTagId"></param>
        /// <returns></returns>
        PatientDemographicsPatientTagViewModel RestorePatientTag(int patientTagId);
    }

    internal class PatientDemographicsManagePatientTagViewService : BaseViewService, IPatientDemographicsManagePatientTagViewService
    {
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly Func<PatientDemographicsPatientTagViewModel> _createPatientDemographicsPatientTagViewModel;

        public PatientDemographicsManagePatientTagViewService(IUnitOfWorkProvider unitOfWorkProvider, Func<PatientDemographicsPatientTagViewModel> createPatientDemographicsPatientTagViewModel)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
            _createPatientDemographicsPatientTagViewModel = createPatientDemographicsPatientTagViewModel;
        }

        public LoadPatientDemographicsTagsInformation LoadPatientDemographicsTagsInformation()
        {
            IList<PatientDemographicsPatientTagViewModel> existingTags = null;
            IList<PatientDemographicsPatientTagViewModel> archivedTags = null;

            var a1 = new Action(() => existingTags = LoadExistingTags().ToArray());
            var a2 = new Action(() => archivedTags = LoadArchivedTags().ToArray());
            new[] { a1, a2 }.ForAllInParallel(a => a());

            return new LoadPatientDemographicsTagsInformation
            {
                ExistingTags = existingTags.ToList(),
                ArchivedTags = archivedTags.ToList()
            };
        }
        
        private IEnumerable<PatientDemographicsPatientTagViewModel> LoadExistingTags()
        {
            var tags = PracticeRepository.Tags.Where(t => !t.IsArchived && t.TagType == TagType.PatientCategory).OrderBy(t => t.Name).ToList();
            return MapTags(tags);
        }

        public IEnumerable<PatientDemographicsPatientTagViewModel> LoadArchivedTags()
        {
            var tags = PracticeRepository.Tags.Where(t => t.IsArchived && t.TagType == TagType.PatientCategory).OrderBy(t => t.Name).ToList();
            return MapTags(tags);
        }

        public int SavePatientTag(PatientDemographicsPatientTagViewModel patientTag)
        {
            Tag databaseTag = patientTag.Id > 0 ? PracticeRepository.Tags.WithId(patientTag.Id) : new Tag();
            databaseTag.HexColor = ToHexColor(patientTag.Color);
            databaseTag.DisplayName = patientTag.Name;
            databaseTag.Name = patientTag.Description ?? string.Empty;
            databaseTag.IsArchived = false;
            databaseTag.TagType = TagType.PatientCategory;
            PracticeRepository.Save(databaseTag);
            int newId = databaseTag.Id;
            return newId;
        }

        public PatientDemographicsPatientTagViewModel ArchivePatientTag(int tagId)
        {
            Tag databaseTag = PracticeRepository.Tags.WithId(tagId);
            if (databaseTag == null) throw new Exception("Could not find tag whose Id is {0}".FormatWith(tagId));
            databaseTag.IsArchived = true;

            using (var unitOfWork = _unitOfWorkProvider.Create())
            {
                databaseTag.PatientTags.ToList().ForEach(PracticeRepository.Delete);
                PracticeRepository.Save(databaseTag);
                unitOfWork.AcceptChanges();
            }

            PracticeRepository.Save(databaseTag);
            return MapPatientTag(databaseTag);
        }

        public PatientDemographicsPatientTagViewModel RestorePatientTag(int patientTagId)
        {
            Tag databasePatientTag = PracticeRepository.Tags.WithId(patientTagId);

            databasePatientTag.IsArchived = false;
            PracticeRepository.Save(databasePatientTag);

            return MapPatientTag(databasePatientTag);
        }

        private static string ToHexColor(Color color)
        {
            return String.Format("#{0}{1}{2}",
                                 color.R.ToString("X2"),
                                 color.G.ToString("X2"),
                                 color.B.ToString("X2"));
        }

        private IEnumerable<PatientDemographicsPatientTagViewModel> MapTags(IEnumerable<Tag> tags)
        {
            var result = new List<PatientDemographicsPatientTagViewModel>();

            tags.ForEachWithSinglePropertyChangedNotification(p => result.Add(MapPatientTag(p)));

            return result;
        }

        private PatientDemographicsPatientTagViewModel MapPatientTag(Tag patientTag)
        {
            if (patientTag == null) return null;
            var viewModel = _createPatientDemographicsPatientTagViewModel();
                viewModel.Color = patientTag.Color;
                viewModel.Description = patientTag.Name;
                viewModel.Id = patientTag.Id;
                viewModel.Name = patientTag.DisplayName;
                return viewModel;
        }
    }
}