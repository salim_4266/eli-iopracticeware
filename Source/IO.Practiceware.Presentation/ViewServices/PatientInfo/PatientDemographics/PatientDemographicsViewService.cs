﻿using System.Data;
using System.Xml;
using System.Xml.Schema;
using IO.Practiceware.Configuration;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.Common;
using Soaf;
using Soaf.Data;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
[assembly: Component(typeof(PatientDemographicsViewService), typeof(IPatientDemographicsViewService))]
[assembly: Component(typeof(PatientViewModelMap), typeof(IMap<Patient, PatientViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics
{
    public interface IPatientDemographicsViewService
    {
        /// <summary>
        /// Load the demographic information for this patient
        /// </summary>
        /// <returns></returns>
        PatientDemographicsLoadInformation LoadPatientDemographicsInformation(int? patientId);

        /// <summary>
        /// Saves the demographic information for this patient. Returns the id of the patient whether or not it is a new patient or an existing one.
        /// </summary>
        /// <param name="patientData"></param>
        int SavePatientDemographicsInformation(PatientViewModel patientData);

        /// <summary>
        /// Loads the select lists used for choosing a pre-defined set of values for certain patient demographic information
        /// </summary>
        /// <returns></returns>
        PatientDemographicsDataListsViewModel LoadPatientDemographicsLists();

        /// <summary>
        /// Gets the id of the state or province based on the name.
        /// </summary>
        /// <param name="stateOrProvinceName"></param>
        /// <returns></returns>
        StateOrProvinceViewModel GetStateOrProvince(string stateOrProvinceName);

        /// <summary>
        /// Searches for a service location by <see cref="serviceLocationShortName"/> and returns the id of the service location. 
        /// </summary>
        /// <param name="serviceLocationShortName">The short name of the service location to search for</param>
        /// <returns>An Id value if service location was found, else null.</returns>
        int? GetServiceLocationId(string serviceLocationShortName);

        /// <summary>
        /// Returns All External Providers
        /// </summary>
        /// <returns>All External Providers</returns>
        IEnumerable<NameAndAbbreviationAndDetailViewModel> LoadExternalProviders();

        IEnumerable<PatientViewModel> GetPatientsBySocialSecurity(string socialSecurityNumber);

        IEnumerable<PatientViewModel> GetPatientsByLastNameAndDob(string lastName, DateTime? birthDate);

        string PatientActivation(int patientId,bool IsPatientExists);
        bool IsPortalPatientExist(int patientId);
    }

    [SupportsUnitOfWork]
    public class PatientDemographicsViewService : BaseViewService, IPatientDemographicsViewService
    {
        private readonly Func<PatientViewModel> _createPatientViewModel;
        private readonly Func<PatientDemographicsDataListsViewModel> _createPatientDemographicsDataListsViewModel;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;

        private readonly IMapper<Patient, PatientViewModel> _patientViewModelMap;
        private IMapper<PatientEmailAddress, EmailAddressViewModel> _emailAddressMapper;
        private IMapper<IO.Practiceware.Model.PatientAddress, AddressViewModel> _addressMapper;
        private IMapper<IO.Practiceware.Model.PatientPhoneNumber, PhoneNumberViewModel> _phoneNumberMapper;
        private IMapper<PatientExternalProvider, PhysicianInformationViewModel> _physicianMapper;
        private IMapper<PatientReferralSource, PatientReferralSourceViewModel> _patientReferralSourceMapper;
        private IMapper<PatientTag, PatientTagViewModel> _patientTagMapper;
        private IMapper<StateOrProvince, StateOrProvinceViewModel> _stateOrProvinceMapper;

        public PatientDemographicsViewService(
            IUnitOfWorkProvider unitOfWorkProvider,
            IMapper<Patient, PatientViewModel> patientViewModelMap,
            Func<PatientViewModel> createPatientViewModel, Func<PatientDemographicsDataListsViewModel> createPatientDemographicsDataListsViewModel)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
            _patientViewModelMap = patientViewModelMap;
            _createPatientViewModel = createPatientViewModel;
            _createPatientDemographicsDataListsViewModel = createPatientDemographicsDataListsViewModel;

            InitMappers();
        }

        private void InitMappers()
        {
            _emailAddressMapper = Mapper.Factory.CreateMapper<PatientEmailAddress, EmailAddressViewModel>(emailAddress => new EmailAddressViewModel
            {
                Id = emailAddress.Id,
                EmailAddressType = new NamedViewModel { Id = (int)emailAddress.EmailAddressType, Name = emailAddress.EmailAddressType.GetDisplayName() },
                PatientId = emailAddress.PatientId,
                IsPrimary = emailAddress == emailAddress.Patient.PatientEmailAddresses.OrderBy(i => i.OrdinalId).First(),
                Value = emailAddress.Value
            });

            _addressMapper = Mapper.Factory.CreateAddressViewModelMapper<IO.Practiceware.Model.PatientAddress>();

            _stateOrProvinceMapper = Mapper.Factory.CreateStateOrProvinceViewModelMapper();

            _phoneNumberMapper = Mapper.Factory.CreatePhoneViewModelMapper<IO.Practiceware.Model.PatientPhoneNumber>();

            _patientReferralSourceMapper = Mapper.Factory.CreateMapper<PatientReferralSource, PatientReferralSourceViewModel>(rs => new PatientReferralSourceViewModel
            {
                Id = rs.Id,
                Category = new NamedViewModel { Id = rs.ReferralSourceTypeId, Name = rs.PatientReferralSourceType.Name },
                ReferralSourceContactName = rs.Comment
            });

            _physicianMapper = Mapper.Factory.CreateMapper<PatientExternalProvider, PhysicianInformationViewModel>(provider => new PhysicianInformationViewModel
            {
                Id = provider.Id,
                Physician = new NameAndAbbreviationAndDetailViewModel
                {
                    Id = provider.ExternalProviderId,
                    Name = provider.ExternalProvider.GetFormattedNameAndClinicalSpecialtyType(),
                    Abbreviation = provider.ExternalProvider.LastNameOrEntityName + provider.ExternalProvider.GetFormattedClinicalSpecialtyType(),
                    Detail = provider.ExternalProvider.GetFormattedAddressAndPhoneNumber().ToStringIfNotNull("Address not documented")
                },
                IsPcp = provider.IsPrimaryCarePhysician,
                IsReferring = provider.IsReferringPhysician
            });

            _patientTagMapper = Mapper.Factory.CreateMapper<PatientTag, PatientTagViewModel>(pt => new PatientTagViewModel
            {
                Id = pt.Id,
                Name = pt.Tag.DisplayName,
                IsPrimary = pt == pt.Patient.PatientTags.OrderBy(i => i.OrdinalId).First(),
                TagId = pt.TagId,
                HexColor = pt.Tag.HexColor
            });
        }

        public int? GetServiceLocationId(string serviceLocationShortName)
        {
            if (serviceLocationShortName.IsNotNullOrEmpty())
            {
                // shortname was specified so try to find a match in the client's database.
                var serviceLocation = PracticeRepository.ServiceLocations.FirstOrDefault(sl => !sl.IsExternal && !sl.IsArchived && sl.ShortName == serviceLocationShortName);
                if (serviceLocation != null) return serviceLocation.Id;
            }

            return null;
        }

        public IEnumerable<PatientViewModel> GetPatientsBySocialSecurity(string socialSecurityNumber)
        {
            var databasePatients = PracticeRepository
               .Patients.Where(p => p.SocialSecurityNumber.Equals(socialSecurityNumber)).ToList();

            if (!databasePatients.Any()) return new List<PatientViewModel>();

            IEnumerable<PatientViewModel> patientsWithSocialSecurityConflict = _patientViewModelMap.MapAll(databasePatients);

            return patientsWithSocialSecurityConflict;
        }

        public IEnumerable<PatientViewModel> GetPatientsByLastNameAndDob(string lastName, DateTime? birthDate)
        {
            var databasePatients = PracticeRepository
               .Patients.Where(p => p.LastName.Equals(lastName, StringComparison.OrdinalIgnoreCase) && p.DateOfBirth == birthDate).ToList();

            if (!databasePatients.Any()) return new List<PatientViewModel>();

            IEnumerable<PatientViewModel> patientsWithLastNameAndDobConflict = _patientViewModelMap.MapAll(databasePatients);

            return patientsWithLastNameAndDobConflict;
        }

        public PatientDemographicsLoadInformation LoadPatientDemographicsInformation(int? patientId)
        {
            var loadInfo = new PatientDemographicsLoadInformation();
            // create the patient view model.
            PatientViewModel patientViewModel = patientId.HasValue ? LoadPatientData(patientId.Value) : _createPatientViewModel();

            // load the field configuration for all fields on the demographics view
            IEnumerable<PatientDemographicsFieldConfiguration> patientDemographicsFieldConfigurations = PracticeRepository.PatientDemographicsFieldConfigurations
    .Where(p => p.Id != ((int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsSocialSecurityNumber)
            && p.Id != (int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsDateOfBirth).ToList();
            // map field configurations to PropertyConfigurations on the PatientViewModel
            patientViewModel.PropertyConfigurations = patientDemographicsFieldConfigurations.Select(i =>
                new PropertyConfiguration
                {
                    Kind = i.IsRequired ? PropertyConfigurationKind.Required : !i.IsVisible ? PropertyConfigurationKind.Hidden : PropertyConfigurationKind.Visible,
                    PropertyName = ((PatientDemographicsFieldConfigurationId)i.Id).ToString(),
                    DefaultValue = i.DefaultValue
                }).ToObservableCollection();

            if (!patientId.HasValue)
            {
                patientViewModel.LoadDefaultValues();
                var today = DateTime.Now.ToClientTime();
                patientViewModel.ReleaseSignatureDate = today;
                patientViewModel.LastUpdated = today;
                patientViewModel.PatientStatus = PatientStatus.Active;
                var mainBillingOrganization = PracticeRepository.BillingOrganizations.FirstOrDefault(x => !x.IsArchived && x.IsMain);
                if (mainBillingOrganization != null)
                {
                    patientViewModel.DefaultBillingOrganizationId = mainBillingOrganization.Id;
                }
            }

            loadInfo.PatientViewModel = patientViewModel;
            loadInfo.PatientDemographicsDataLists = LoadPatientDemographicsLists();
            return loadInfo;
        }


        public PatientDemographicsDataListsViewModel LoadPatientDemographicsLists()
        {
            var patientDemographicsViewModel = _createPatientDemographicsDataListsViewModel();

            new Action[]
            {
                () => patientDemographicsViewModel.AddressTypes = NamedViewModelExtensions.ToNamedViewModel<PatientAddressTypeId>().OrderBy(x => x.Name).ToObservableCollection(),

                () => patientDemographicsViewModel.EmailAddressTypes = NamedViewModelExtensions.ToNamedViewModel<EmailAddressType>().OrderBy(x => x.Name).ToObservableCollection(),

                //() => patientDemographicsViewModel.Genders = NamedViewModelExtensions.ToNamedViewModel<Gender>(false).OrderBy(x => x.Name).ToObservableCollection(),

                () => patientDemographicsViewModel.Genders = GenderViewModel.GenderList.ToObservableCollection(),

                () => patientDemographicsViewModel.Sexes = NamedViewModelExtensions.ToNamedViewModel<Sex>(false).OrderBy(x => x.Name).ToObservableCollection(),

                () => patientDemographicsViewModel.SexualOrientations = SexualOrientationViewModel.SexualOrientationList.ToObservableCollection(),

                () => patientDemographicsViewModel.MaritalStatuses = NamedViewModelExtensions.ToNamedViewModel<MaritalStatus>(false, true).ToObservableCollection(),

                () => patientDemographicsViewModel.PhoneTypes = NamedViewModelExtensions.ToNamedViewModel<PatientPhoneNumberType>().OrderBy(x => x.Name).ToObservableCollection(),

                () => patientDemographicsViewModel.NameSuffixes = NamedViewModelExtensions.ToNamedViewModel<PersonNameSuffix>().OrderBy(x => x.Name).ToObservableCollection(),

                () => patientDemographicsViewModel.PaymentOfBenefitsToProviderOptions = NamedViewModelExtensions.ToNamedViewModel<PaymentOfBenefitsToProvider>().OrderBy(x => x.Name).ToObservableCollection(),

                () => patientDemographicsViewModel.ReleaseOfMedicalInformationOptions = NamedViewModelExtensions.ToNamedViewModel<ReleaseOfInformationCode>().OrderBy(x => x.Name).ToObservableCollection(),

                () => patientDemographicsViewModel.PatientStatuses = NamedViewModelExtensions.ToNamedViewModel<PatientStatus>().OrderBy(x => x.Name).ToObservableCollection(),

                () => patientDemographicsViewModel.Ethnicities = PracticeRepository.Ethnicities.Where(x => !x.IsArchived).OrderBy(x => x.Name)
                    .Select(x => new NamedViewModel
                    {
                        Id = x.Id, 
                        Name = x.Name
                    }).ToObservableCollection(),

                () => patientDemographicsViewModel.Races = PracticeRepository.Races.Where(x => !x.IsArchived)
                                                                    .Select(x => new PatientRaceViewModel
                                                                    {
                                                                        Id = x.Id,
                                                                        Name = x.Name + (x.IsArchived ? " (Archived)" : string.Empty),
                                                                        IsArchived = x.IsArchived
                                                                    })
                                                                    .OrderBy(x => x.Name)
                                                                    .ToObservableCollection(),
        
                () => patientDemographicsViewModel.RacesAndEthnicities = RacesAndEthnicitiesViewModel.RacesAndEthnicitiesList.ToObservableCollection(),
                                                                        

                () => patientDemographicsViewModel.Languages = PracticeRepository.Languages.Where(x => !x.IsArchived)
                    .OrderBy(x => x.OrdinalId)
                    .Select(x => new NamedViewModel
                    {
                        Id = x.Id, 
                        Name = x.Name
                    }).ToObservableCollection(),

                () => patientDemographicsViewModel.StateOrProvinces = PracticeRepository.StateOrProvinces.Include(i => i.Country)
                    .Select(_stateOrProvinceMapper.Map)
                    .ToObservableCollection(),

                () => patientDemographicsViewModel.BillingOrganizations = PracticeRepository.BillingOrganizations.Where(x => !x.IsArchived)
                    .Select(x => new NamedViewModel
                    {
                        Id = x.Id, 
                        Name = x.ShortName
                    }).OrderBy(x => x.Name).ToObservableCollection(), 

                () => patientDemographicsViewModel.Physicians = PracticeRepository.Users.OfType<Doctor>().Where(x => !x.IsArchived)
                    .OrderBy(x => x.OrdinalId)
                    .Select(x => new NameAndAbbreviationViewModel
                    {
                        Id = x.Id, 
                        Name = x.UserName, 
                        Abbreviation = x.LastName
                    }).ToObservableCollection(),

                () => patientDemographicsViewModel.ServiceLocations = PracticeRepository.ServiceLocations.Where(l => !l.IsExternal)
                    .OrderBy(i => i.ShortName)
                    .Select(l => new NamedViewModel
                    {
                        Id = l.Id, 
                        Name = l.ShortName
                    })
                    .ToObservableCollection(),

                () => patientDemographicsViewModel.TagsList = PracticeRepository.Tags.Where(x => !x.IsArchived)
                    .WithTagTypePatientCategory()
                    .Select(x => new PatientTagViewModel
                    {
                        Id = x.Id, 
                        Name = x.DisplayName, 
                        HexColor = x.HexColor
                    }).ToObservableCollection(),

                () => patientDemographicsViewModel.PatientReferralSourceTypes = PracticeRepository.PatientReferralSourceTypes.Where(x => !x.IsArchived)
                    .OrderBy(x => x.OrdinalId)
                    .Select(x => new NamedViewModel
                    {
                        Id = x.Id, 
                        Name = x.Name
                    }).ToObservableCollection(),

                () => patientDemographicsViewModel.EmploymentStatuses = PracticeRepository.PatientEmploymentStatus
                    .Select(es => new NamedViewModel
                    {
                        Id = es.Id,
                        Name = es.Name
                    }).ToObservableCollection()
            }.ForAllInParallel(a => a());

            return patientDemographicsViewModel;
        }

        public StateOrProvinceViewModel GetStateOrProvince(string stateOrProvinceName)
        {
            var stateOrProvince = PracticeRepository
                                    .StateOrProvinces.FirstOrDefault(s => (s.Abbreviation.ToLower() == stateOrProvinceName.ToLower())
                                             || (s.Name.ToLower() == stateOrProvinceName.ToLower()));

            return stateOrProvince != null ? _stateOrProvinceMapper.Map(stateOrProvince) : null;
        }


        private PatientViewModel LoadPatientData(int patientId)
        {
            var patient = LoadPatient(patientId);

            var patientViewModel = _patientViewModelMap.MapAll(new[] { patient }).FirstOrDefault();
            if (patientViewModel == null)
            {
                return null;
            }
            new Action[]
            {
                () => patientViewModel.Addresses = _addressMapper.MapAll(patient.PatientAddresses
                    .OrderBy(x => x.OrdinalId))
                    .ToExtendedObservableCollection(),
                () => patientViewModel.EmailAddresses = _emailAddressMapper.MapAll(patient.PatientEmailAddresses
                    .OrderBy(x => x.OrdinalId))
                    .ToExtendedObservableCollection(),
                () => patientViewModel.PhoneNumbers = _phoneNumberMapper.MapAll(patient.PatientPhoneNumbers
                    .OrderBy(x => x.OrdinalId))
                    .ToExtendedObservableCollection(),
                () => patientViewModel.ReferralSources = _patientReferralSourceMapper.MapAll(patient.PatientReferralSources)
                    .ToExtendedObservableCollection(),
                () => patientViewModel.ExternalPhysicians = _physicianMapper.MapAll(patient.ExternalProviders
                    .OrderBy(i => i.IsPrimaryCarePhysician ? 0 : 1))
                    .ToExtendedObservableCollection(),
                () => patientViewModel.Tags = _patientTagMapper.MapAll(patient.PatientTags.Where(x => !x.Tag.IsArchived)
                    .OrderBy(x => x.OrdinalId))
                    .ToExtendedObservableCollection(),
                () => patientViewModel.PatientRaces = patient.Races.Select(p => new PatientRaceViewModel
                {
                    Id = p.Id,
                    Name = p.Name + (p.IsArchived ? " (Archived)" : string.Empty),
                    IsArchived = p.IsArchived
                })
                .ToExtendedObservableCollection()
            }.ForAllInParallel(a => a());
            return patientViewModel;
        }

        private Patient LoadPatient(int patientId)
        {
            var patient = PracticeRepository.Patients
                .First(x => x.Id == patientId);

            PracticeRepository.AsQueryableFactory().Load(patient,
                p => p.PatientEmailAddresses,
                p => p.PatientAddresses.Select(y => new
                {
                    y.PatientAddressType,
                    y.StateOrProvince.Country
                }),
                p => p.PatientPhoneNumbers,
                p => p.PatientReferralSources.Select(rs => new
                {
                    rs.PatientReferralSourceType
                }),
                p => p.ExternalProviders.Select(e => new
                {
                    e.ExternalProvider.ExternalContactPhoneNumbers
                }),
                p => p.ExternalProviders.Select(e => new
                {
                    Addresses = e.ExternalProvider.ExternalContactAddresses.Select(a => new
                    {
                        a.StateOrProvince
                    })
                }),
                p => p.ExternalProviders.Select(e => new
                {
                    SpecialtyTypes = e.ExternalProvider.ExternalProviderClinicalSpecialtyTypes.Select(cst => new
                    {
                        cst.ClinicalSpecialtyType
                    })
                }),
                p => p.PatientComments,
                p => p.PatientTags.Select(t => new { t.Tag }),
                p => p.Races
                );    
            return patient;                 
        }

        #region Saving Changes
        public virtual int SavePatientDemographicsInformation(PatientViewModel patientData)
        {
            using (var work = _unitOfWorkProvider.Create())
            {
                Patient patient = patientData.Id > 0 ? LoadPatient(patientData.Id) : new Patient();

                MapPatientViewModelToPatientModel(patientData, patient);

                MapPatientRelationships(patientData, patient);

                // we may be saving a non-clinical patient that's been converted to clinical for the first time
                patient.IsClinical = true;

                PracticeRepository.Save(patient);

                work.AcceptChanges();

                return patient.Id;
            }
        }

        private void MapPatientViewModelToPatientModel(PatientViewModel patientData, Patient patient)
        {
            patient.FirstName = patientData.FirstName;
            patient.Honorific = patientData.Honorific;
            patient.LastName = patientData.LastName;
            patient.MiddleName = patientData.MiddleName;
            patient.NickName = patientData.Nickname;
            patient.PriorFirstName = patientData.PriorFirstName;
            patient.PriorLastName = patientData.PriorLastName;
            patient.Suffix = patientData.Suffix;
            patient.Prefix = patientData.Title;
            patient.DateOfBirth = patientData.DateOfBirth;
            patient.Gender = patientData.Gender;
            patient.MaritalStatus = patientData.MaritalStatus;
            patient.EthnicityId = patientData.EthnicityId;
            patient.LanguageId = patientData.LanguageId;
            patient.SocialSecurityNumber = patientData.SocialSecurityNumber;
            patient.PriorPatientCode = patientData.PriorId;
            patient.Occupation = patientData.Occupation;
            patient.EmployerName = patientData.CompanyName;
            patient.PatientReligionId = patientData.ReligionId;
            patient.IsHipaaConsentSigned = patientData.HasHipaaConsent;
            patient.PaymentOfBenefitsToProviderId = patientData.AssignBenefitsToProviderId;
            patient.ReleaseOfInformationCodeId = patientData.ReleaseMedicalInformationId;
            patient.ReleaseOfInformationDateTime = patientData.ReleaseSignatureDate;
            patient.PatientStatus = patientData.PatientStatus.HasValue ? patientData.PatientStatus.Value : PatientStatus.Active;
            patient.DateOfDeath = patientData.DateOfDeath;
            patient.DefaultUserId = patientData.PreferredPhysicianId;
            patient.UserDefinedLastUpdated = patientData.LastUpdated;
            patient.PatientEmploymentStatusId = patientData.EmploymentStatusId;
            patient.PreferredServiceLocationId = patientData.PreferredServiceLocationId;
            patient.BillingOrganizationId = patientData.DefaultBillingOrganizationId;
            patient.SexId = patientData.SexId;
            patient.SexualOrientationId = patientData.SexualOrientationId;
            patient.RaceAndEthnicityId = patientData.RaceAndEthnicityId;

            //MapPatientRaceIdsToPatient(patient, patientData);
        }

        private void MapPatientRelationships(PatientViewModel patientData, Patient patient)
        {
            var comment = patient.PatientComments.FirstOrDefault(x => x.PatientCommentType == PatientCommentType.PatientDemographicSummary);
            if (comment != null)
            {
                comment.Value = patientData.Comments;
            }
            else
            {
                if (patientData.Comments != null && patientData.Comments.Trim() != string.Empty)
                {
                    patient.PatientComments.Add(new PatientComment { PatientId = patient.Id, PatientCommentType = PatientCommentType.PatientDemographicSummary, Value = patientData.Comments });
                }
            }


            // Sync External Providers (sync takes care of inserting, updating and deleting)
            PracticeRepository.SyncDependencyCollection(patientData.ExternalPhysicians, patient.ExternalProviders,
                                                      (source, destination) => source.Id != 0 && destination.Id == source.Id, // func to test for equality
                                                       (source, destination) =>
                                                       {
                                                           if (source.Physician != null)
                                                           {
                                                               destination.IsPrimaryCarePhysician = source.IsPcp;
                                                               destination.ExternalProviderId = source.Physician.Id;
                                                               destination.IsReferringPhysician = source.IsReferring;
                                                           }
                                                       }); // action to map viewmodel to model


            // Sync Patient Tags
            PracticeRepository.SyncDependencyCollection(patientData.Tags, patient.PatientTags,
                                                       (source, destination) => patientData.Id == destination.PatientId && source.TagId == destination.TagId, // func to test for equality
                                                        (source, destination) =>
                                                        {
                                                            destination.TagId = source.TagId;
                                                            if (source.IsPrimary)
                                                            {
                                                                destination.OrdinalId = 1;
                                                            }

                                                        }); // action to map viewmodel to model

            var defaultAddressType = PracticeRepository.PatientAddressTypes.First();
            // Sync Patient Addresses
            PracticeRepository.SyncAddresses(patientData.Addresses, patient.PatientAddresses,
                                                    address => address.PatientAddressTypeId, (address, i) => address.PatientAddressTypeId = i,
                                                    defaultAddressType.Id);

            // Sync Patient Addresses
            PracticeRepository.SyncEmailAddresses(patientData.EmailAddresses, patient.PatientEmailAddresses,
                                                    email => email.EmailAddressTypeId, (email, i) => email.EmailAddressTypeId = i);

            // Sync Phone Numbers
            PracticeRepository.SyncDependencyCollection(patientData.PhoneNumbers, patient.PatientPhoneNumbers,
                                                       (source, destination) => source.Id != 0 && source.Id == destination.Id, // func to test for equality
                                                       MapPhoneNumberViewModelToPhoneNumberModel); // action to map viewmodel to model

            // Sync Referral Sources
            PracticeRepository.SyncDependencyCollection(patientData.ReferralSources, patient.PatientReferralSources,
                                                        (source, destination) => source.Id != 0 && source.Id == destination.Id, // func to test for equality
                                                        MapReferringViewModelToReferringModel); // action to map viewmodel to model                
        }


        //private void MapPatientRaceIdsToPatient(Patient patient, PatientViewModel patientViewModel)
        //{
        //    // remove all of the existing Patient's PatientRaces where they don't appear in the viewModel's list of Races
        //    var racesToRemove = patient.Races.Where(pr => patientViewModel.PatientRaces.All(ppr => ppr.Id != pr.Id)).ToList();
        //    racesToRemove.ForEach(r => patient.Races.Remove(r));

        //    // find the new id's.  (i.e the ones in the viewModel that are not in the current db model)            
        //    var raceIdsToAdd = patientViewModel.PatientRaces.Where(pr => patient.Races.All(p => p.Id != pr.Id)).Select(pr => pr.Id).ToList();

        //    // grab the new ones from the database and add them to the patient
        //    var racesToAdd = PracticeRepository.Races.Where(pr => raceIdsToAdd.Contains(pr.Id)).ToList();
        //    racesToAdd.ForEach(patient.Races.Add);
        //}

        private static void MapReferringViewModelToReferringModel(PatientReferralSourceViewModel patientReferralSourceViewModel, PatientReferralSource patientReferralSource)
        {
            patientReferralSource.ReferralSourceTypeId = patientReferralSourceViewModel.Category.Id;
            patientReferralSource.Comment = patientReferralSourceViewModel.ReferralSourceContactName;
        }

        private static void MapPhoneNumberViewModelToPhoneNumberModel(PhoneNumberViewModel phoneNumberViewModel, IO.Practiceware.Model.PatientPhoneNumber patientPhoneNumber)
        {
            patientPhoneNumber.AreaCode = phoneNumberViewModel.AreaCode;
            patientPhoneNumber.CountryCode = phoneNumberViewModel.CountryCode;
            patientPhoneNumber.ExchangeAndSuffix = phoneNumberViewModel.ExchangeAndSuffix;
            patientPhoneNumber.Extension = phoneNumberViewModel.Extension;
            patientPhoneNumber.IsInternational = phoneNumberViewModel.IsInternational;
            if (phoneNumberViewModel.IsPrimary)
            {
                patientPhoneNumber.OrdinalId = 1;
            }
            patientPhoneNumber.PatientPhoneNumberType = phoneNumberViewModel.PhoneType != null ? phoneNumberViewModel.PhoneType.Id : (int)PatientPhoneNumberType.Unknown;
        }

        #endregion


        public IEnumerable<NameAndAbbreviationAndDetailViewModel> LoadExternalProviders()
        {
            var externalProviders = PracticeRepository.ExternalContacts.OfType<ExternalProvider>()
                .Where(x => !x.IsArchived).ToList();

            PracticeRepository.AsQueryableFactory().Load(externalProviders,
                e => e.ExternalProviderClinicalSpecialtyTypes.Select(cst => cst.ClinicalSpecialtyType),
                e => e.ExternalContactAddresses.Select(z => new
                {
                    z.ExternalContactAddressType,
                    z.StateOrProvince
                }),
                e => e.ExternalContactPhoneNumbers.Select(x => new
                {
                    x.ExternalContact,
                    x.ExternalContactPhoneNumberType
                })
            );

            return externalProviders.Select(x => new NameAndAbbreviationAndDetailViewModel
            {
                Id = x.Id,
                Name = x.GetFormattedNameAndClinicalSpecialtyType(),
                Abbreviation = x.LastNameOrEntityName + x.GetFormattedClinicalSpecialtyType(),
                Detail = x.GetFormattedAddressAndPhoneNumber(),
            }).Distinct().OrderBy(x => x.Abbreviation).ToList();
        }

        public string PatientActivation(int patientId, bool isPatientExists)
        {
            string retStatus = string.Empty;
            string _practiceToken = string.Empty;
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                var cmdQuery = String.Format(@"Select Cast(Value as nvarchar(100)) From model.ApplicationSettings Where Name = 'PracticeAuthToken'");
                _practiceToken = dbConnection.Execute<string>(cmdQuery);
            }

            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                var cmdQuery = String.Format(@"Select Top(1) P.Id as ExternalId, FirstName, ISNULL(MiddleName, '') as MiddleName, LastName, ISNULL(Suffix, '') as Suffix, ISNULL(Prefix,'') Prefix, '' as CompanyName, '' as JobTitle, PEA.Value as EmailAddresses, IsNull(Line1,'') as Address1, Isnull(Line2, '') as  Address2, Isnull(City, '') as  City, Isnull(StateOrProvinceId, '') as  State, '' as Country, Isnull(PostalCode, '') as Zip, Cast(DateOfBirth as Date) as Birthdate, '' as Notes, ISNULL((Select top(1) Name From model.languages Where Id = P.LanguageId),'English') as LanguageName, ISNULL(P.PreferredServiceLocationId, (Select Top(1) Id From model.ServiceLocations)) as LocationId, ISNULL(PPN.CountryCode + PPn.AreaCode + PPn.ExchangeAndSuffix,' ') as PhoneNumbers From model.Patients P inner join model.PatientEmailAddresses PEA on P.Id = PEA.PatientId and p.LastName not like '%Test%' and p.FirstName not like '%Test%' and p.DateOfBirth is not null and p.GenderId is not null Left join model.PatientAddresses PA on PA.PatientId = P.Id LEFT Join model.PatientPhoneNumbers PPN on P.Id = PPN.PatientId Where P.Id  = {0}", patientId);

                var dtable = dbConnection.Execute<DataTable>(cmdQuery);
                if (dtable.Rows.Count > 0)
                {
                    var parameters = new PatientEntity
                    {
                        PracticeToken = _practiceToken,
                        ExternalId = dtable.Rows[0]["ExternalId"].ToString(),
                        FirstName = dtable.Rows[0]["FirstName"].ToString(),
                        LastName = dtable.Rows[0]["LastName"].ToString(),
                        MiddleName = dtable.Rows[0]["MiddleName"].ToString(),
                        Suffix = dtable.Rows[0]["Suffix"].ToString(),
                        Prefix = dtable.Rows[0]["Prefix"].ToString(),
                        LocationID = dtable.Rows[0]["LocationID"].ToString(),
                        EmailAddresses = dtable.Rows[0]["EmailAddresses"].ToString(),
                        PhoneNumbers = dtable.Rows[0]["PhoneNumbers"].ToString(),
                        Address1 = dtable.Rows[0]["Address1"].ToString(),
                        Address2 = dtable.Rows[0]["Address2"].ToString(),
                        City = dtable.Rows[0]["LocationID"].ToString(),
                        State = dtable.Rows[0]["State"].ToString(),
                        Country = dtable.Rows[0]["Country"].ToString(),
                        Zip = dtable.Rows[0]["Zip"].ToString(),
                        BirthDate = dtable.Rows[0]["BirthDate"].ToString(),
                        Notes = dtable.Rows[0]["Notes"].ToString(),
                        LanguageName = dtable.Rows[0]["LanguageName"].ToString()
                    };

                    //if (parameters.ExternalId != "")
                    //{
                    //    cmdQuery = String.Format("Select PatientRepresentativeId AS RepresentativeId, FirstName, LastName from model.PatientRepresentatives Where PatientId = {0}", patientId);

                    //    dtable = dbConnection.Execute<DataTable>(cmdQuery);
                    //    if (dtable.Rows.Count > 0)
                    //    {
                    //        PatientRepresentativeDetail pr = new PatientRepresentativeDetail()
                    //        {
                    //            FirstName = dtable.Rows[0]["FirstName"].ToString(),
                    //            LastName = dtable.Rows[0]["LastName"].ToString(),
                    //            RepresentativeId = dtable.Rows[0]["RepresentativeId"].ToString()
                    //        };
                    //        parameters.PatientRepresentative = pr;
                    //    }
                    //}

                    var portalSvc = new PortalServices();
                    if (isPatientExists)
                    {
                        retStatus = portalSvc.UpdateDemographics(parameters);
                    }
                    else
                    {
                        retStatus = portalSvc.RegisterPatient(parameters);
                    }
                    dtable.Dispose();
                }
                else
                {
                    retStatus = "Information is missing";
                }
            }
            return retStatus;
        }

        public bool IsPortalPatientExist(int patientId)
        {
            bool patientExists = false;
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                var cmdQuery = String.Format(@"Select * From MVE.PP_PortalQueueResponse PQD INNER JOIN model.Patients P on Cast(P.Id as nvarchar) = PQD.PatientNo and PQD.ActionTypeLookupId = 97 and P.Id = '{0}'", patientId);

                var dtable = dbConnection.Execute<DataTable>(cmdQuery);
                if (dtable.Rows.Count > 0)
                {
                    patientExists = true;
                    dtable.Dispose();
                }
            }
            return patientExists;
        }

    }

    public class PatientViewModelMap : IMap<Patient, PatientViewModel>
    {
        private readonly IEnumerable<IMapMember<Patient, PatientViewModel>> _members;

        public PatientViewModelMap()
        {
            _members = this.CreateMembers(patient => new PatientViewModel
            {
                Id = patient.Id,
                FirstName = patient.FirstName,
                Honorific = patient.Honorific,
                LastName = patient.LastName,
                MiddleName = patient.MiddleName,
                Nickname = patient.NickName,
                PriorFirstName = patient.PriorFirstName,
                PriorLastName = patient.PriorLastName,
                Suffix = patient.Suffix,
                Title = patient.Prefix,
                DateOfBirth = patient.DateOfBirth,
                GenderId = patient.Gender.HasValue ? (int)patient.Gender : new int?(),
                MaritalStatusId = patient.MaritalStatus.HasValue ? (int)patient.MaritalStatus : new int?(),
                EthnicityId = patient.EthnicityId,
                LanguageId = patient.LanguageId,
                SocialSecurityNumber = patient.SocialSecurityNumber,
                PriorId = patient.PriorPatientCode,
                Occupation = patient.Occupation,
                CompanyName = patient.EmployerName,
                ReligionId = patient.PatientReligionId,
                HasHipaaConsent = patient.IsHipaaConsentSigned,
                AssignBenefitsToProviderId = patient.PaymentOfBenefitsToProviderId,
                ReleaseMedicalInformationId = patient.ReleaseOfInformationCodeId.HasValue
                        ? patient.ReleaseOfInformationCodeId.Value
                        : (int)ReleaseOfInformationCode.I,
                ReleaseSignatureDate = patient.ReleaseOfInformationDateTime,
                PatientStatusId = (int)patient.PatientStatus,
                DateOfDeath = patient.DateOfDeath,
                Comments = patient.PatientComments.Where(i => i.PatientCommentType == PatientCommentType.PatientDemographicSummary).Select(x => x.Value).Join(", ", false),
                PreferredPhysicianId = patient.DefaultUserId,
                LastUpdated = patient.UserDefinedLastUpdated,
                EmploymentStatusId = patient.PatientEmploymentStatusId,
                PreferredServiceLocationId = patient.PreferredServiceLocationId,
                DefaultBillingOrganizationId = patient.BillingOrganizationId,
                ActivationCode = patient.ActivationCode,
                SexId = patient.SexId,
                SexualOrientationId = patient.SexualOrientationId,
                RaceAndEthnicityId = patient.RaceAndEthnicityId
            }).ToArray();
        }

        public IEnumerable<IMapMember<Patient, PatientViewModel>> Members
        {
            get { return _members; }
        }


    }
}