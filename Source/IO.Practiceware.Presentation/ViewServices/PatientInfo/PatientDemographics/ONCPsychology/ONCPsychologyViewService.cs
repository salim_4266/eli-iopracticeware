﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using IO.Practiceware.Configuration;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics.ONCPsychology;
using System.Collections.ObjectModel;
using Soaf.Data;

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientDemographics.ONCPsychology
{
    public class ONCPsychologyViewService
    {
        /// <constructor>
        /// Initialise Connection
        /// </constructor>
        public ONCPsychologyViewService()
        {
        }
        /// <method>
        /// Select Query
        /// </method>
        public void GetAnswers(ref ObservableCollection<ONCPsychologicalAnswersViewModel>[] answers)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("AnswerId", typeof(int));
            dataTable.Columns.Add("Answerdescription", typeof(string));
            dataTable.Columns.Add("QuestionId", typeof(int));

            string _query = " Select * from Model.PsychologicalAnswers ";
            try
            {
                using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                {
                    dataTable = dbConnection.Execute<DataTable>(_query);
                }
                for (int idx = 1; idx <= answers.Length; idx++)
                {
                    ObservableCollection<ONCPsychologicalAnswersViewModel> a1 = new ObservableCollection<ONCPsychologicalAnswersViewModel>();
                    string filter = "QuestionId=" + idx.ToString();
                    DataRow[] options = dataTable.Select(filter);
                    FillAnswers(options, ref answers, idx, a1);
                }
            }
            catch (SqlException)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Populate individual feedback based on questions
        /// </summary>
        /// <param name="arrRow"></param>
        /// <param name="answers"></param>
        /// <param name="index"></param>
        /// <param name="a1"></param>
        private void FillAnswers(DataRow[] arrRow, ref ObservableCollection<ONCPsychologicalAnswersViewModel>[] answers, int index, ObservableCollection<ONCPsychologicalAnswersViewModel> a1)
        {
            if (arrRow != null && arrRow.Length > 0)
            {
                ONCPsychologicalAnswersViewModel obj1 = new ONCPsychologicalAnswersViewModel
                {
                    AnswerDescription = "Select",
                    AnswerId = 0,
                    QuestionId = index,
                    QuestionIdAnswerId = index + "~0",
                };
                a1.Add(obj1);

                foreach (var row in arrRow)
                {
                    ONCPsychologicalAnswersViewModel obj = new ONCPsychologicalAnswersViewModel
                    {
                        QuestionIdAnswerId = row["QuestionId"].ToString() + "~" + row["AnswerId"].ToString(),
                        AnswerDescription = row["AnswerDescription"].ToString(),
                        QuestionId = int.Parse(row["AnswerId"].ToString()),
                        AnswerId = int.Parse(row["QuestionId"].ToString())
                    };
                    a1.Add(obj);
                }
                answers[--index] = a1;
            }
        }

        public ObservableCollection<ONCPsychologyServiceLocationsViewModel> GetLocations()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("ShortName", typeof(string));
            ObservableCollection<ONCPsychologyServiceLocationsViewModel> locations = new ObservableCollection<ONCPsychologyServiceLocationsViewModel>();

            string _query = " Select Id, ShortName from Model.ServiceLocations ";
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                {
                    dataTable = dbConnection.Execute<DataTable>(_query);
                }
                foreach (DataRow _dataRow in dataTable.Rows)
                {
                    locations.Add(new ONCPsychologyServiceLocationsViewModel
                    {
                        Id = int.Parse(_dataRow["Id"].ToString()),
                        ShortName = _dataRow["ShortName"].ToString()
                    });
                }
            }
            catch (SqlException)
            {
            }
            finally
            {
            }
            return locations;
        }

        public ObservableCollection<ONCPsychologicalAnswersViewModel> GetPatientFeedBackById(int patientId, ref int locn)
        {
            ObservableCollection<ONCPsychologicalAnswersViewModel> objAnswersVM = new ObservableCollection<ONCPsychologicalAnswersViewModel>();
            Dictionary<string, object> param = new Dictionary<string, object>();
            try
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("LocationId", typeof(int));
                dataTable.Columns.Add("AnswerId", typeof(int));
                dataTable.Columns.Add("Answerdescription", typeof(string));
                dataTable.Columns.Add("QuestionIdAnswerId", typeof(string));
                dataTable.Columns.Add("QuestionId", typeof(int));

                string _query = "  Select * from [Model].PsychologicalAnswers pa ";
                _query += "inner join [model].[PsychologicalFeedback] pf ";
                _query += "on pa.answerId = pf.answerId ";
                _query += "and pf.patientid = @Val1";
                param.Add("@Val1", patientId);

                using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                {
                    dataTable = dbConnection.Execute<DataTable>(_query, param);
                }

                foreach (DataRow _dataRow in dataTable.Rows)
                {
                    locn = int.Parse(_dataRow["LocationId"].ToString());
                    objAnswersVM.Add(new ONCPsychologicalAnswersViewModel
                    {
                        AnswerDescription = _dataRow["AnswerDescription"].ToString(),
                        AnswerId = int.Parse(_dataRow["AnswerId"].ToString()),
                        QuestionId = int.Parse(_dataRow["QuestionId"].ToString()),
                        QuestionIdAnswerId = _dataRow["QuestionId"].ToString() + "~" + _dataRow["AnswerId"].ToString()
                    });
                }
                return objAnswersVM;
            }
            catch (SqlException)
            {
            }
            finally
            {
            }
            return objAnswersVM;
        }
        /// <method>
        /// Insert Query
        /// </method>
        public bool executeInsertQuery(ONCPsychologicalFeedBackViewModel oncFeedback)
        {
            DataTable dtExists = new DataTable();
            Dictionary<string, object> paramExists = new Dictionary<string, object>();
            int rec = 0;

            string _query = " INSERT INTO [model].[PsychologicalFeedback] (QuestionId,AnswerId,PatientId,LocationId,LastUpdatedDate,CreatedDate) ";
            _query += " VALUES (@Val1,@Val2,@Val3,@Val4,@Val5,@Val6) ";
            string _qryQuesExists = " select Count(Id) as Id from  [model].[PsychologicalFeedback] where patientId=@Val1 and questionid=@Val2";
            string _qryUpdate = "  Update [model].[PsychologicalFeedback] set AnswerId=@Val4, LastUpdatedDate=@Val3,LocationId=@val5 where patientId=@Val1 and QuestionId=@Val2";

            oncFeedback.LastUpdatedDate = DateTime.Now;
            oncFeedback.CreatedDate = DateTime.Now;

            try
            {
                //Checking for Existing Feedback
                paramExists.Add("@Val1", oncFeedback.PatientId);
                paramExists.Add("@Val2", oncFeedback.QuestionId);
                paramExists.Add("@Val3", oncFeedback.LastUpdatedDate);

                using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                {
                    dtExists = dbConnection.Execute<DataTable>(_qryQuesExists, paramExists);
                }

                if (dtExists.Rows.Count > 0)
                    rec = int.Parse(dtExists.Rows[0]["Id"].ToString());
                int aid = oncFeedback.AnswerId;
                if (rec > 0)
                {
                    if (aid == 0)
                    {
                        executeDeleteQuery(oncFeedback);
                    }
                    else
                    {
                        //Updation if exists 
                        Dictionary<string, object> paramUpdate = new Dictionary<string, object>();
                        paramUpdate.Add("@Val1", oncFeedback.PatientId);
                        paramUpdate.Add("@Val2", oncFeedback.QuestionId);
                        paramUpdate.Add("@Val3", oncFeedback.LastUpdatedDate);
                        paramUpdate.Add("@Val4", oncFeedback.AnswerId);
                        paramUpdate.Add("@Val5", oncFeedback.LocationId);

                        using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                        {
                            dbConnection.Execute(_qryUpdate, paramUpdate);
                        }
                    }
                }
                else if (rec == 0 && aid == 0)
                {
                }
                else
                {
                    // Insert if new Feedback             
                    Dictionary<string, object> paramInsert = new Dictionary<string, object>();
                    paramInsert.Add("@Val1", oncFeedback.QuestionId);
                    paramInsert.Add("@Val2", oncFeedback.AnswerId);
                    paramInsert.Add("@Val3", oncFeedback.PatientId);
                    paramInsert.Add("@Val4", oncFeedback.LocationId);
                    paramInsert.Add("@Val5", oncFeedback.LastUpdatedDate);
                    paramInsert.Add("@Val6", oncFeedback.CreatedDate);

                    using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                    {
                        dbConnection.Execute(_query, paramInsert);
                    }
                }
            }
            catch (SqlException)
            {
                return false;
            }
            finally
            {
            }
            return true;
        }

        /// <method>
        /// Update Query
        /// </method>
        public bool executeDeleteQuery(ONCPsychologicalFeedBackViewModel feedback)
        {
            Dictionary<string, object> paramDelete = new Dictionary<string, object>();
            String _query = "Delete from model.PsychologicalFeedback where patientid=@val1 and questionid=@val2";
            try
            {
                paramDelete.Add("@Val1", feedback.PatientId);
                paramDelete.Add("@Val2", feedback.QuestionId);

                using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                {
                    dbConnection.Execute(_query, paramDelete);
                }
            }
            catch (SqlException)
            {
                return false;
            }
            finally
            {
            }
            return true;
        }
    }
}
