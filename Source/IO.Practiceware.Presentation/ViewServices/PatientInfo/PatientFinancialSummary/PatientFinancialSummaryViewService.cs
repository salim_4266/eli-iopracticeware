﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using IO.Practiceware.Data;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Service;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientFinancialSummary;
using IO.Practiceware.Presentation.ViewServices.Financials.Common;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientFinancialSummary;
using IO.Practiceware.Services.Documents;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using EncounterServiceViewModel = IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientFinancialSummary.EncounterServiceViewModel;
using BillingServiceTransactionStatus = IO.Practiceware.Model.BillingServiceTransactionStatus;
using MethodSent = IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction.MethodSent;
using BillingTransactionViewModel = IO.Practiceware.Presentation.ViewModels.Financials.Common.Transaction.BillingTransactionViewModel;
using DiagnosisViewModel = IO.Practiceware.Presentation.ViewModels.Financials.Common.Diagnosis.DiagnosisViewModel;
using FinancialSourceType = IO.Practiceware.Model.FinancialSourceType;
using IO.Practiceware.Services.Icd;

[assembly: Component(typeof(PatientFinancialSummaryViewService), typeof(IPatientFinancialSummaryViewService))]

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientFinancialSummary
{

    public interface IPatientFinancialSummaryViewService
    {
        PatientAccountViewModel LoadPatientFinancialSummaryInformation(int patientId, FilterSelectionViewModel invoiceFilterSelected, List<int> invoicesWithDetailsIds);
        InvoiceViewModel LoadFullInvoiceViewModel(InvoiceViewModel invoice);
        HtmlDocument LoadClaim(PaperClaimGenerationInfo claimViewModel);
        DiagnosisCodeViewModel FetchDiagnosisDescription(DiagnosisViewModel diagnosisViewModel);
        void DeleteComment(TransactionalNoteViewModel transactionalNoteViewModel);
    }

    class PatientFinancialSummaryViewService : BaseViewService, IPatientFinancialSummaryViewService
    {
        private IMapper<Invoice, InvoiceViewModel> _initialInvoiceMapper;
        private IMapper<Adjustment, FinancialTransactionViewModel> _initialAdjustmentToTransactionviewModelMapper;
        private IMapper<Adjustment, AdjustmentTypeViewModel> _initalAdjustmentTypeToAdjustmentTypeViewModelMapper;
        private IMapper<BillingServiceTransaction, BillingTransactionViewModel> _billingServiceTransactionMapper;
        private IMapper<BillingDiagnosis, DiagnosisViewModel> _billingDiagnosisMapper;
        private IMapper<BillingServiceModifier, NameAndAbbreviationAndDetailViewModel> _billingServiceModifierMapper;
        private IMapper<EncounterService, ServiceViewModel> _encounterServiceMapper;
        private IMapper<FinancialInformation, FinancialTransactionViewModel> _financialInformationToTransactionviewModelMapper;
        private IMapper<Adjustment, FinancialTransactionViewModel> _adjustmentToTransactionviewModelMapper;
        private IMapper<BillingDiagnosis, DiagnosisLinkPointerViewModel> _billingDiagnosisToDiagnosisLinkPointerViewModelMapper;
        private IMapper<Adjustment, AdjustmentTypeViewModel> _adjustmentTypeToAdjustmentTypeViewModelMapper;
        private readonly IPracticeRepository _practiceRepository;
        private readonly PaperClaimsMessageProducer _paperClaimsMessageProducer;
        private readonly PaperClaimGenerationUtilities _paperClaimGenerationUtilities;
        private readonly Func<ObservableCollection<DiagnosisViewModel>> _createDiagnosisViewModelCollection;
        private readonly Func<DiagnosisViewModel> _createDiagnosisViewModel;
        private readonly Func<DiagnosisCodeViewModel> _createDiagnosisCodeViewModel;
        private readonly IIcd10Service _icd10Service;

        private readonly Dictionary<ClaimAdjustmentGroupCode, string> _claimAdjustmentGroupCodeMappings = new Dictionary<ClaimAdjustmentGroupCode, string>
                                                                                                          {
                                                                                                              {ClaimAdjustmentGroupCode.ContractualObligation, "CO"},
                                                                                                              {ClaimAdjustmentGroupCode.CorrectionReversal, "CR"},
                                                                                                              {ClaimAdjustmentGroupCode.OtherAdjustment, "OA"},
                                                                                                              {ClaimAdjustmentGroupCode.PatientResponsibility, "PR"},
                                                                                                              {ClaimAdjustmentGroupCode.PayerInitiatedReduction, "PI"}
                                                                                                          };

        public PatientFinancialSummaryViewService(IPracticeRepository practiceRepository, PaperClaimsMessageProducer paperClaimsMessageProducer, PaperClaimGenerationUtilities paperClaimGenerationUtilities, Func<ObservableCollection<DiagnosisViewModel>> createDiagnosisViewModelCollection, Func<DiagnosisViewModel> createDiagnosisViewModel, Func<DiagnosisCodeViewModel> createDiagnosisCodeViewModel, IIcd10Service icd10Service)
        {
            _practiceRepository = practiceRepository;
            _paperClaimsMessageProducer = paperClaimsMessageProducer;
            _paperClaimGenerationUtilities = paperClaimGenerationUtilities;
            _createDiagnosisViewModelCollection = createDiagnosisViewModelCollection;
            _createDiagnosisViewModel = createDiagnosisViewModel;
            _createDiagnosisCodeViewModel = createDiagnosisCodeViewModel;
            _icd10Service = icd10Service;
            InitMapper();
        }


        public PatientAccountViewModel LoadPatientFinancialSummaryInformation(int patientId, FilterSelectionViewModel invoiceFilterSelected, List<int> invoicesWithDetailsIds)
        {
            Patient patient = null;
            Invoice[] invoices = null;
            Adjustment[] adjustments = null;

            var patientInvoices = _practiceRepository.Invoices
                .Where(inv => inv.Encounter.Patient.Id == patientId)
                .Select(x => new
                             {
                                 InvoiceId = x.Id,
                                 TotalCharge = Math.Round((x.BillingServices.Sum(bs => bs.Unit * bs.UnitCharge) ?? 0), 2),
                                 TotalPaid = Math.Round((x.InvoiceReceivables.SelectMany(ir => ir.Adjustments)
                                     .Sum(a => a.AdjustmentType.IsDebit ? (decimal?)a.Amount * -1 : (decimal?)a.Amount) ?? 0), 2),

                             })
                .ToArray();

            int[] invoicesFilter = patientInvoices.Select(i => i.InvoiceId).ToArray();

            if (invoiceFilterSelected.Id == 0)
            {
                invoicesFilter = patientInvoices
                   .Where(i => i.TotalCharge - i.TotalPaid != 0)
                    .Select(i => i.InvoiceId)
                    .ToArray();
            }

            new Action[]
            {
                () => patient = _practiceRepository.Patients.First(p => p.Id == patientId),

                () => invoices = _practiceRepository.Invoices.Where(inv => inv.Encounter.Patient.Id == patientId && invoicesFilter.Contains(inv.Id))
                    .ToArray(),

                () => adjustments = _practiceRepository.Adjustments
                    .Where(adj => adj.Patient.Id == patientId).ToArray()

            }.ForAllInParallel(a => a());

            _practiceRepository.AsQueryableFactory().Load(invoices,
                inv => inv.AttributeToServiceLocation,
                inv => inv.BillingProvider.BillingOrganization,
                inv => inv.BillingProvider.User,
                inv => inv.Encounter.ServiceLocation,
                inv => inv.ReferringExternalProvider,
                inv => inv.InvoiceReceivables.Select(ir => new
                                                           {
                                                               Adjustments = ir.Adjustments.Select(adj => new
                                                                                                          {
                                                                                                              adj.AdjustmentType
                                                                                                          }),
                                                               InsurerClaimForms = ir.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Select(icf => icf.ClaimFormType),
                                                               ir.PatientInsuranceAuthorization,
                                                               ir.PatientInsuranceReferral.ExternalProvider,
                                                               ir.PatientInsurance.PatientInsuranceReferrals,
                                                               Bst = ir.BillingServiceTransactions.Select(bst => new
                                                                                                                 {
                                                                                                                     bst.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer
                                                                                                                 })
                                                           }),
                inv => inv.BillingServices.Select(bs => new
                                                        {
                                                            Adjustments = bs.Adjustments.Select(a => a.AdjustmentType),
                                                            bs.BillingServiceTransactions
                                                        }),
                inv => inv.Encounter.Patient.Adjustments
                );
            _practiceRepository.AsQueryableFactory().Load(adjustments,
                adj => adj.AdjustmentType,
                adj => adj.ClaimAdjustmentReasonCode,
                adj => adj.BillingService.Invoice.Encounter.Patient,
                adj => adj.InvoiceReceivable,
                adj => adj.Insurer,
                adj => adj.Patient,
                adj => adj.FinancialBatch.Insurer,
                adj => adj.FinancialBatch.Patient
                );
          
            var completedInvoices = _initialInvoiceMapper.MapAll(invoices.Where(inv => inv.Encounter.Patient.Id == patient.Id)
                .OrderByDescending(inv => inv.Encounter.StartDateTime))
                .ToObservableCollection();
            completedInvoices.Where(x => invoicesWithDetailsIds.Contains(x.Id)).ForEachWithSinglePropertyChangedNotification(x => LoadFullInvoiceViewModel(x) );

            if (invoiceFilterSelected.Id == 0)
            {
                completedInvoices = completedInvoices.Where(x => x.OpenBalance != 0).ToObservableCollection();
            }

            return new PatientAccountViewModel
                   {
                       Invoices = completedInvoices,
                       OnAccountTransactions = GetOnAccountTransactions(adjustments),
                       FilterByIsOpen = invoiceFilterSelected.Id == 0
                   };
        }


        public InvoiceViewModel LoadFullInvoiceViewModel(InvoiceViewModel invoice)
        {
            var databaseInvoice = _practiceRepository.Invoices.Single(inv => inv.Id == invoice.Id);

            #region PracticeRepository database invoice loading data
            invoice.CodesetType = (DiagnosisTypeId)Enum.Parse(typeof(DiagnosisTypeId), databaseInvoice.DiagnosisTypeId.ToString());
            _practiceRepository.AsQueryableFactory().Load(databaseInvoice,
                inv => inv.InvoiceComments,
                inv => inv.BillingDiagnoses.Select(bd => new
                                                         {
                                                             bd.BillingServiceBillingDiagnoses,
                                                             bd.ExternalSystemEntityMapping
                                                         }),
                inv => inv.InvoiceReceivables.Select(ir => new
                                                           {  
                                                               FI = ir.FinancialInformations.Select(fi => new
                                                                                                          {
                                                                                                              fi.Insurer,
                                                                                                              fi.BillingService,
                                                                                                              fi.Patient,
                                                                                                              InvoiceReceivableInsurer = fi.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer,
                                                                                                              InvoiceReceivablePatient = fi.InvoiceReceivable.Invoice.Encounter.Patient,
                                                                                                              FinancialBatchPatient = fi.FinancialBatch.Patient,
                                                                                                              FinancialBatchInsurer = fi.FinancialBatch.Insurer,
                                                                                                              fi.FinancialInformationType,
                                                                                                              fi.ClaimAdjustmentReasonCode
                                                                                                          }),
                                                               Bst = ir.BillingServiceTransactions.Select(bst => new
                                                                                                                 {
                                                                                                                     bst.InvoiceReceivable.Invoice.Encounter.Patient,
                                                                                                                     bst.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer
                                                                                                                 }),
                                                               Adj = ir.Adjustments.Select(adj => new
                                                                                                  {
                                                                                                      adj.Insurer,
                                                                                                      adj.BillingService,
                                                                                                      adj.Patient,
                                                                                                      FinancialBatchPatient = adj.FinancialBatch.Patient,
                                                                                                      FinancialBatchInsurer = adj.FinancialBatch.Insurer,
                                                                                                      adj.InvoiceReceivable.Invoice,
                                                                                                      InvoiceReceivableInsurer = adj.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer,
                                                                                                      InvoiceReceivablePatient = adj.InvoiceReceivable.Invoice.Encounter.Patient,
                                                                                                      adj.ClaimAdjustmentReasonCode,
                                                                                                      adj.AdjustmentType

                                                                                                  })
                                                           }),
                inv => inv.BillingServices.Select(bs => new
                                                        {
                                                            bs.BillingServiceComments,
                                                            Bsm = bs.BillingServiceModifiers.Select(bsm => new
                                                                                                           {
                                                                                                               bsm.ServiceModifier
                                                                                                           }),
                                                            bs.Invoice.Encounter.Patient.PatientInsurances,
                                                            bs.BillingServiceBillingDiagnoses,
                                                            bs.EncounterService
                                                        })
                );

            #endregion

            // ReSharper disable once RedundantArgumentDefaultValue
            var isIcd10 = invoice.CodesetType == DiagnosisTypeId.Icd10;

            if (isIcd10)
            {
                string icd10DiagQuery = _icd10Service.GetIcd10DiagnosisCodesQuery(invoice.Id);

                using (var dbConnection = DbConnectionFactory.PracticeRepository)
                {
                    var clinicalTable = dbConnection.Execute<DataTable>(icd10DiagQuery);
                    if (clinicalTable.Rows.Count > 0)
                    {
                        var diagnosisViewModelCollection = _createDiagnosisViewModelCollection();
                        foreach (DataRow row in clinicalTable.Rows)
                        {
                            var diagnosisViewModel = _createDiagnosisViewModel();

                            diagnosisViewModel.ActiveDiagnosisCode = _createDiagnosisCodeViewModel();
                            diagnosisViewModel.ActiveDiagnosisCode.Id = Convert.ToInt32(row["Id"]);
                            diagnosisViewModel.ActiveDiagnosisCode.Code = row["DiagnosisCode"].ToString();
                            diagnosisViewModel.ActiveDiagnosisCode.Description = Convert.ToString(row["DiagnosisDescription"]);
                            diagnosisViewModel.ActiveDiagnosisCode.CodeSet = DiagnosisCodeSet.Icd10;
                            diagnosisViewModel.IsLinked = Convert.ToBoolean(row["IsLinked"]);

                            diagnosisViewModel.DiagnosisLinkPointer = GetLinkedDiagnosis(Convert.ToInt32(row["OrdinalId"]));

                            diagnosisViewModelCollection.Add(diagnosisViewModel);
                        }
                        invoice.Diagnoses = diagnosisViewModelCollection;

                    }
                }
            }
            else
            {
                invoice.Diagnoses = _billingDiagnosisMapper.MapAll(databaseInvoice.BillingDiagnoses.OrderBy(x => x.OrdinalId))
                    .ToObservableCollection();
            }        
            
            invoice.UnassignedTransactions = GetUnassignedTransactions(databaseInvoice);
            invoice.Notes = databaseInvoice.InvoiceComments
                .OrderByDescending(ic => ic.DateTime)
                .Select(ic => new TransactionalNoteViewModel { Content = ic.Value, Id = ic.Id, Date = ic.DateTime.GetValueOrDefault(), IsArchived = ic.IsArchived, InvoiceId = ic.InvoiceId }).ToObservableCollection();

            var transactions = GetTransactions(databaseInvoice).ToList();
            foreach (var billingService in invoice.EncounterServices)
            {
                var databaseBillingService = databaseInvoice.BillingServices.Single(bs => bs.Id == billingService.Id);
                billingService.Transactions = transactions.Where(bs => bs.ServiceId == databaseBillingService.Id).ToObservableCollection();
                var modifers = databaseBillingService.BillingServiceModifiers.IfNotNull(bsm => bsm.OrderBy(ord => ord.OrdinalId));
                // ReSharper disable once RedundantArgumentDefaultValue
                billingService.Modifiers = _billingServiceModifierMapper.MapAll(modifers).ToObservableCollection();
                // ReSharper disable once PossibleInvalidOperationException
                billingService.Allowed = databaseBillingService.UnitAllowableExpense.HasValue ? databaseBillingService.UnitAllowableExpense.Value : 0;
                billingService.ClaimNote = databaseBillingService.ClaimComment;
                billingService.Date = databaseBillingService.Invoice.Encounter.StartDateTime;
                // ReSharper disable once RedundantArgumentDefaultValue
                if (!isIcd10)
                {
                    billingService.LinkedDiagnoses = databaseBillingService.BillingServiceBillingDiagnoses
                        .Where(x => x.BillingDiagnosis != null && !x.IsIcd10)
                        .OrderBy(x => x.OrdinalId)
                        .Select(x => _billingDiagnosisMapper.Map(x.BillingDiagnosis).DiagnosisLinkPointer)
                        .ToExtendedObservableCollection(); // Diagnosis letters and colors, ordered by ordinal ID
                }
                else
                {
                    billingService.LinkedDiagnoses = databaseBillingService.BillingServiceBillingDiagnoses
                        .Where(x=> x.IsIcd10)
                        .OrderBy(x => x.OrdinalId)
                        .Select(x => invoice.Diagnoses.Where(j=>j.ActiveDiagnosisCode.Id == x.BillingDiagnosisId).Select(i=>i.DiagnosisLinkPointer).FirstOrDefault()).ToExtendedObservableCollection();                        
                }
                billingService.Service = databaseBillingService.EncounterService.IfNotNull(_encounterServiceMapper.Map);
            }
            

            return invoice;
        }

        private static DiagnosisLinkPointerViewModel GetLinkedDiagnosis(int ordinalId)
        {
            var linkedDiag = new DiagnosisLinkPointerViewModel();
           
            if (ordinalId < 1 || ordinalId > 12)
            {
                return null;
            }
            
            var bill = new BillingDiagnosis();
            bill.OrdinalId = ordinalId;
            linkedDiag.PointerLetter = DiagnosisMap.GetDiagnosisLetter(bill);
            linkedDiag.DiagnosisColor = DiagnosisMap.GetDiagnosisHighlightColor(bill);

            return linkedDiag;
        }

        public HtmlDocument LoadClaim(PaperClaimGenerationInfo claimGenerationInfo)
        {
            claimGenerationInfo = _paperClaimGenerationUtilities.GetPaperClaimGenerationInfo(claimGenerationInfo);

            return (HtmlDocument)_paperClaimsMessageProducer.ProduceMessage(claimGenerationInfo.BillingServiceTransactionIds.ToList(), claimGenerationInfo.ClaimFormTypeId, true);
        }

        /// <summary>
        /// Called by the initializer for the view service
        /// </summary>
        private void InitMapper()
        {
            var financialSourceTypeMapper = Mapper.Factory.CreateNamedViewModelMapper<FinancialSourceType>();

            #region

            // used by _billingDiagnosisMapper
            _billingDiagnosisToDiagnosisLinkPointerViewModelMapper = Mapper.Factory.CreateMapper<BillingDiagnosis, DiagnosisLinkPointerViewModel>(billingDiagnosis => new DiagnosisLinkPointerViewModel
                                                                                                                                                                      {
                                                                                                                                                                          PointerLetter = DiagnosisMap.GetDiagnosisLetter(billingDiagnosis),
                                                                                                                                                                          DiagnosisColor = DiagnosisMap.GetDiagnosisHighlightColor(billingDiagnosis)
                                                                                                                                                                      });

            // used by LoadFullInvoiceViewModel
            _encounterServiceMapper = Mapper.Factory.CreateMapper<EncounterService, ServiceViewModel>(encounterService => new ServiceViewModel
                                                                                                                          {
                                                                                                                              Code = encounterService.Code ?? "",
                                                                                                                              Description = encounterService.Description ?? ""
                                                                                                                          });

            // used by LoadFullInvoiceViewModel
            _billingServiceModifierMapper = Mapper.Factory.CreateMapper<BillingServiceModifier, NameAndAbbreviationAndDetailViewModel>(billingServiceModifier => new NameAndAbbreviationAndDetailViewModel
                                                                                                                                                                 {
                                                                                                                                                                     Detail = billingServiceModifier.ServiceModifier != null ? billingServiceModifier.ServiceModifier.Description ?? "" : "",
                                                                                                                                                                     Abbreviation = billingServiceModifier.ServiceModifier != null ? billingServiceModifier.ServiceModifier.Code ?? "" : "",
                                                                                                                                                                 });

            // used by _adjustmentToTransactionviewModelMapper
            _adjustmentTypeToAdjustmentTypeViewModelMapper = Mapper.Factory.CreateMapper<Adjustment, AdjustmentTypeViewModel>(adjustment => new AdjustmentTypeViewModel
                                                                                                                                            {
                                                                                                                                                Id = adjustment.AdjustmentType.Id,
                                                                                                                                                Name = adjustment.AdjustmentType.Name ?? "",
                                                                                                                                                ReasonCode = adjustment.ClaimAdjustmentReasonCode != null ? new NamedViewModel(adjustment.ClaimAdjustmentReasonCode.Id, adjustment.ClaimAdjustmentReasonCode.Code) : null,
                                                                                                                                                GroupCode = adjustment.ClaimAdjustmentGroupCode != null ? new NamedViewModel((int)adjustment.ClaimAdjustmentGroupCode.Value, _claimAdjustmentGroupCodeMappings[adjustment.ClaimAdjustmentGroupCode.Value]) : null,
                                                                                                                                                IsCash = adjustment.AdjustmentType.IsCash,
                                                                                                                                                IsDebit = adjustment.AdjustmentType.IsDebit
                                                                                                                                            });

            // used by LoadFullInvoiceViewModel
            _billingDiagnosisMapper = Mapper.Factory.CreateMapper<BillingDiagnosis, DiagnosisViewModel>(billingDiagnosis => new DiagnosisViewModel
                                                                                                                            {
                                                                                                                                ActiveDiagnosisCode = new DiagnosisCodeViewModel
                                                                                                                                                      {
                                                                                                                                                          //TODO: get description for icd9 codes: Note this cannot be done until a column for Description is actually added to the database
                                                                                                                                                          Code = billingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey ?? "",
                                                                                                                                                          CodeSet = DiagnosisCodeSet.Icd9,
                                                                                                                                                          Key = billingDiagnosis.ExternalSystemEntityMapping.PracticeRepositoryEntityKey
                                                                                                                                                          //Description =  "Active Diagnosis Description goes here"
                                                                                                                                                      }, // ICD9
                                                                                                                                AlternateDiagnosisCode = new DiagnosisCodeViewModel
                                                                                                                                                         {
                                                                                                                                                             //TODO: get the codes and descriptions for Icd10: Note As of 9/4/2014 it will still be a few months until this is needed. It is currently not in the database anywhere.
                                                                                                                                                             //Code = billingDiagnosis,
                                                                                                                                                             CodeSet = DiagnosisCodeSet.Icd10,
                                                                                                                                                             //Description = "alternateDiagnosis Description goes here"
                                                                                                                                                         }, //ICD10
                                                                                                                                DiagnosisLinkPointer = _billingDiagnosisToDiagnosisLinkPointerViewModelMapper.Map(billingDiagnosis, false),
                                                                                                                                IsLinked = billingDiagnosis.BillingServiceBillingDiagnoses != null, //billing diagnoses billing service billing diagnoses if it's not linked to a service,
                                                                                                                            });

            // used by GetTransactions function which is called by _invoiceMapper
            _adjustmentToTransactionviewModelMapper = Mapper.Factory.CreateMapper<Adjustment, FinancialTransactionViewModel>(adjustment => new FinancialTransactionViewModel
                                                                                                                                           {
                                                                                                                                               Id = adjustment.Id,
                                                                                                                                               FinancialTransactionType = adjustment.AdjustmentType != null ? _adjustmentTypeToAdjustmentTypeViewModelMapper.Map(adjustment) : null,
                                                                                                                                               Amount = !adjustment.AdjustmentType.IsDebit ? adjustment.Amount : -adjustment.Amount,
                                                                                                                                               Comment = adjustment.Comment,
                                                                                                                                               Reference = adjustment.FinancialBatch != null ? adjustment.FinancialBatch.CheckCode ?? "" : "",
                                                                                                                                               Source = GetFinancialSourceNamedViewModel(adjustment, financialSourceTypeMapper),
                                                                                                                                               // ReSharper disable once ConstantNullCoalescingCondition
                                                                                                                                               InvoiceReceivableId = adjustment.InvoiceReceivableId,
                                                                                                                                               Date = adjustment.PostedDateTime,
                                                                                                                                               InvoiceId = adjustment.InvoiceReceivable.IfNotNull(ir => (int?)ir.InvoiceId),
                                                                                                                                               ServiceId = adjustment.BillingServiceId
                                                                                                                                           });


            // used by GetTransactions function which is called by _invoiceMapper
            _financialInformationToTransactionviewModelMapper = Mapper.Factory.CreateMapper<FinancialInformation, FinancialTransactionViewModel>(financialInformation => new FinancialTransactionViewModel
                                                                                                                                                                         {
                                                                                                                                                                             Id = financialInformation.Id,
                                                                                                                                                                             InvoiceId = financialInformation.InvoiceReceivable.IfNotNull(ir => (int?)ir.InvoiceId),
                                                                                                                                                                             ServiceId = financialInformation.BillingServiceId,
                                                                                                                                                                             Amount = financialInformation.Amount,
                                                                                                                                                                             Comment = financialInformation.Comment,
                                                                                                                                                                             Date = financialInformation.PostedDateTime,
                                                                                                                                                                             Reference = financialInformation.FinancialBatch != null ? financialInformation.FinancialBatch.CheckCode ?? "" : "",
                                                                                                                                                                             FinancialTransactionType = new FinancialInformationTypeViewModel
                                                                                                                                                                                                        {
                                                                                                                                                                                                            GroupCode = new NamedViewModel
                                                                                                                                                                                                                        {
                                                                                                                                                                                                                            Name = financialInformation.ClaimAdjustmentGroupCode.HasValue ? _claimAdjustmentGroupCodeMappings[financialInformation.ClaimAdjustmentGroupCode.Value] : ""
                                                                                                                                                                                                                        },
                                                                                                                                                                                                            Name = financialInformation.FinancialInformationType.Name,
                                                                                                                                                                                                            ReasonCode = new NamedViewModel
                                                                                                                                                                                                                         {
                                                                                                                                                                                                                             Name = financialInformation.ClaimAdjustmentReasonCode != null ? financialInformation.ClaimAdjustmentReasonCode.Code ?? "" : ""
                                                                                                                                                                                                                         }

                                                                                                                                                                                                        },
                                                                                                                                                                             Source = GetFinancialSourceNamedViewModel(financialInformation, financialSourceTypeMapper)
                                                                                                                                                                         });


            // used by GetTransactions function which is called by _invoiceMapper
            _billingServiceTransactionMapper = Mapper.Factory.CreateMapper<BillingServiceTransaction, BillingTransactionViewModel>(billingServiceTransaction => new BillingTransactionViewModel
                                                                                                                                                                {
                                                                                                                                                                    Date = billingServiceTransaction.DateTime,
                                                                                                                                                                    Amount = billingServiceTransaction.AmountSent,
                                                                                                                                                                    InvoiceId = billingServiceTransaction.InvoiceReceivable.IfNotNull(ir => (int?)ir.InvoiceId),
                                                                                                                                                                    ServiceId = billingServiceTransaction.BillingServiceId,
                                                                                                                                                                    Reference = billingServiceTransaction.InvoiceReceivable.PatientInsuranceReferral != null ? billingServiceTransaction.InvoiceReceivable.PatientInsuranceReferral.ReferralCode : "",
                                                                                                                                                                    Status = (ViewModels.Financials.Common.Transaction.BillingServiceTransactionStatus)((int)billingServiceTransaction.BillingServiceTransactionStatus),
                                                                                                                                                                    Method = (MethodSent)((int)billingServiceTransaction.MethodSent),
                                                                                                                                                                    Receiver = GetReceiver(billingServiceTransaction)
                                                                                                                                                                });

            #endregion


            // used by _adjustmentToTransactionviewModelMapper
            _initalAdjustmentTypeToAdjustmentTypeViewModelMapper = Mapper.Factory.CreateMapper<Adjustment, AdjustmentTypeViewModel>(adjustment => new AdjustmentTypeViewModel
                                                                                                                                                  {
                                                                                                                                                      Id = adjustment.AdjustmentType.Id,
                                                                                                                                                      IsCash = adjustment.AdjustmentType.IsCash,
                                                                                                                                                      IsDebit = adjustment.AdjustmentType.IsDebit
                                                                                                                                                  });

            _initialAdjustmentToTransactionviewModelMapper = Mapper.Factory.CreateMapper<Adjustment, FinancialTransactionViewModel>(adjustment => new FinancialTransactionViewModel
                                                                                                                                                  {
                                                                                                                                                      Id = adjustment.Id,
                                                                                                                                                      FinancialTransactionType = adjustment.AdjustmentType != null ? _initalAdjustmentTypeToAdjustmentTypeViewModelMapper.Map(adjustment) : null,
                                                                                                                                                      Amount = !adjustment.AdjustmentType.IsDebit ? adjustment.Amount : -adjustment.Amount,                                                                                                                                                      
                                                                                                                                                      InvoiceReceivableId = adjustment.InvoiceReceivableId,
                                                                                                                                                      InvoiceId = adjustment.InvoiceReceivable.IfNotNull(ir => (int?)ir.InvoiceId),
                                                                                                                                                      ServiceId = adjustment.BillingServiceId
                                                                                                                                                  });

            // called By LoadPatientFinancialSummaryInformation
            _initialInvoiceMapper = Mapper.Factory.CreateMapper<Invoice, InvoiceViewModel>(invoice => new InvoiceViewModel
                                                                                                      {
                                                                                                          Id = invoice.Id,
                                                                                                          InvoiceType = (ViewModels.PatientInfo.PatientFinancialSummary.InvoiceType)invoice.InvoiceType,
                                                                                                          PrintingOptions = _paperClaimGenerationUtilities.GetPaperClaimInformation(invoice),
                                                                                                          InsurerBilled = GetInsurerBilled(invoice),
                                                                                                          PatientBilled = GetPatientBilled(invoice),
                                                                                                          Date = invoice.Encounter.StartDateTime,
                                                                                                          EncounterId = invoice.Encounter.Id,
                                                                                                          ReferringProviders = GetReferrals(invoice),
                                                                                                          CodesetType = (DiagnosisTypeId)Enum.Parse(typeof(DiagnosisTypeId), invoice.DiagnosisTypeId.ToString()),

                                                                                                          Referral = invoice.InvoiceReceivables
                                                                                                              .Where(pir => pir.PatientInsuranceReferral != null && pir.PatientInsuranceReferral.ReferralCode.IsNotNullOrEmpty())
                                                                                                              .Select(x => new
                                                                                                                           {
                                                                                                                               x.PatientInsurance.OrdinalId,
                                                                                                                               x.PatientInsuranceReferral.ReferralCode
                                                                                                                           })
                                                                                                              .Distinct()
                                                                                                              .OrderBy(ir => ir.OrdinalId)
                                                                                                              .Select(x => x.ReferralCode)
                                                                                                              .Join(", ", true),

                                                                                                          AuthorizationCode = invoice.InvoiceReceivables
                                                                                                              .Where(pins => pins.PatientInsuranceAuthorization != null && pins.PatientInsuranceAuthorization.AuthorizationCode.IsNotNullOrEmpty())
                                                                                                              .Select(ir => ir.PatientInsuranceAuthorization)
                                                                                                              .OrderBy(pins => pins.PatientInsurance != null ? pins.PatientInsurance.OrdinalId : 1)
                                                                                                              .Select(pia => pia.AuthorizationCode).Join(", ", true),
                                                                                                          ServiceLocation = invoice.Encounter.ServiceLocation
                                                                                                                            != null ? new NamedViewModel
                                                                                                                                      {
                                                                                                                                          Name = invoice.Encounter.ServiceLocation.ShortName
                                                                                                                                      } : null,
                                                                                                          CareTeam = new CareTeamViewModel
                                                                                                                     {
                                                                                                                         RenderingProvider = new ProviderViewModel
                                                                                                                                             {
                                                                                                                                                 Provider = invoice.IfNotNull(inv => inv.BillingProvider).IfNotNull(bp => bp.User) != null ? new NamedViewModel { Name = invoice.BillingProvider.User.UserName } : null,
                                                                                                                                                 BillingOrganization = invoice.IfNotNull(inv => inv.BillingProvider).IfNotNull(inv => inv.BillingOrganization) != null ? new NamedViewModel { Name = invoice.BillingProvider.BillingOrganization.Name } : null
                                                                                                                                             }
                                                                                                                     },
                                                                                                          AttributeTo = invoice.AttributeToServiceLocation != null ? new NamedViewModel { Name = invoice.AttributeToServiceLocation.ShortName } : null,
                                                                                                          BillingOrganization = invoice.BillingProvider.IfNotNull(inv => inv.BillingOrganization) != null ? new NamedViewModel { Name = invoice.BillingProvider.BillingOrganization.ShortName } : null,
                                                                                                          EncounterServices = invoice.BillingServices
                                                                                                              .OrderByDescending(bs => bs.Invoice.Encounter.StartDateTime).ThenBy(b => b.OrdinalId).Select(billingService =>
                                                                                                              new EncounterServiceViewModel
                                                                                                              {
                                                                                                                  Id = billingService.Id,
                                                                                                                  BilledTotal = Math.Round((billingService.BillingServiceTransactions.Where(bst => bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedCrossOver
                                                                                                                                                                                       && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent
                                                                                                                                                                                       && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedSent).Sum(bst => bst.AmountSent)), 2),
                                                                                                                  // ReSharper disable once RedundantArgumentDefaultValue
                                                                                                                  Adjustments = _initialAdjustmentToTransactionviewModelMapper.MapAll(billingService.Adjustments).ToObservableCollection(),
                                                                                                                  InvoiceId = invoice.Id,
                                                                                                                  Units = billingService.Unit.GetValueOrDefault(),
                                                                                                                  Charge = billingService.UnitCharge.GetValueOrDefault()
                                                                                                              }).ToObservableCollection(),
                                                                                                          ClaimNote = invoice.ClaimComment ?? "",
                                                                                                          Adjustments = _initialAdjustmentToTransactionviewModelMapper.MapAll(invoice.InvoiceReceivables.SelectMany(ir => ir.Adjustments))
                                                                                                              .OrderByDescending(a => a.Date)
                                                                                                              .ToObservableCollection()
                                                                                                      });

        }

        public NamedViewModel GetFinancialSourceNamedViewModel(IAdjustmentOrFinancialInformation adjustmentOrFinancialInfo, IMapper<FinancialSourceType, NamedViewModel> financialSourceTypeMapper)
        {
            var name = string.Empty;
            switch (adjustmentOrFinancialInfo.FinancialSourceType)
            {
                case FinancialSourceType.Insurer:
                    if (adjustmentOrFinancialInfo.InvoiceReceivable != null
                        && adjustmentOrFinancialInfo.InvoiceReceivable.PatientInsurance != null)
                    {
                        name = adjustmentOrFinancialInfo.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.Name;
                    }
                    else if (adjustmentOrFinancialInfo.FinancialBatch != null
                             && adjustmentOrFinancialInfo.FinancialBatch.Insurer != null)
                    {
                        name = adjustmentOrFinancialInfo.FinancialBatch.Insurer.Name;
                    }
                    else if (adjustmentOrFinancialInfo.Insurer != null)
                    {
                        name = adjustmentOrFinancialInfo.Insurer.Name;
                    }
                    break;
                case FinancialSourceType.Patient:
                    if (adjustmentOrFinancialInfo.FinancialBatch != null
                        && adjustmentOrFinancialInfo.FinancialBatch.Patient != null)
                    {
                        name = adjustmentOrFinancialInfo.FinancialBatch.Patient.DisplayName;
                    }
                    else if (adjustmentOrFinancialInfo.InvoiceReceivable != null
                             && adjustmentOrFinancialInfo.InvoiceReceivable.Invoice.Encounter.Patient != null)
                    {
                        name = adjustmentOrFinancialInfo.InvoiceReceivable.Invoice.Encounter.Patient.DisplayName;
                    }
                    else if (adjustmentOrFinancialInfo.Patient != null)
                    {
                        name = adjustmentOrFinancialInfo.Patient.DisplayName;
                    }
                    break;
                case FinancialSourceType.BillingOrganization:
                    name = "Office";
                    break;
                default:
                    var result = financialSourceTypeMapper.Map(adjustmentOrFinancialInfo.FinancialSourceType);
                    name = result.Name;
                    break;
            }
            return new NamedViewModel
                   {
                       Id = (int)adjustmentOrFinancialInfo.FinancialSourceType,
                       Name = name
                   };
        }


        public ObservableCollection<NamedViewModel> GetReferrals(Invoice invoice)
        {
            var results = invoice.InvoiceReceivables
                .Where(rp => rp.PatientInsuranceReferral != null && rp.PatientInsuranceReferral.ExternalProvider != null)
                .Select(rp => new NamedViewModel
                              {
                                  Id = rp.PatientInsuranceReferral.ExternalProvider.Id,
                                  Name = rp.PatientInsuranceReferral.ExternalProvider.DisplayName
                              })
                .ToObservableCollection();
            if (results.Count == 0 && invoice.ReferringExternalProvider != null)
            {
                results.Add(new NamedViewModel(invoice.ReferringExternalProvider.Id, invoice.ReferringExternalProvider.DisplayName));
            }
            return results;
        }

        /// <summary>
        /// Called by _invoiceMapper Get's the amount the insurer was billed
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        private static Decimal GetInsurerBilled(Invoice invoice)
        {
            return invoice.InvoiceReceivables.Where(ir => ir.PatientInsurance != null).SelectMany(bs => bs.BillingServiceTransactions
                .Where(bst => bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedCrossOver
                              && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent
                              && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedSent)
                .Select(bst => bst.AmountSent)).Sum();
        }

        /// <summary>
        /// called by _invoiceMapper Get's the amount the patient was billed
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        private static Decimal GetPatientBilled(Invoice invoice)
        {
            return invoice.InvoiceReceivables.Where(ir => ir.PatientInsurance == null).SelectMany(bs => bs.BillingServiceTransactions
                .Where(bst => bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedCrossOver
                              && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent
                              && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedSent)
                .Select(bst => bst.AmountSent)).Sum();
        }

        /// <summary>
        /// Called by _billingServiceTransactionMapper, used to indicate receiver name for who receives the bill for a billing service transaction
        /// </summary>
        /// <param name="billingServiceTransaction"></param>
        /// <returns></returns>
        private static NamedViewModel GetReceiver(BillingServiceTransaction billingServiceTransaction)
        {
            if (billingServiceTransaction.InvoiceReceivable.PatientInsurance != null
                && billingServiceTransaction.InvoiceReceivable.PatientInsurance.InsurancePolicy != null
                && billingServiceTransaction.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer != null)
            {
                return new NamedViewModel
                       {
                           Name = billingServiceTransaction.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.Name,
                           Id = billingServiceTransaction.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId
                       };
            }
            if (billingServiceTransaction.InvoiceReceivable.Invoice != null)
            {
                return new NamedViewModel
                       {
                           Name = billingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.DisplayName
                       };
            }
            return new NamedViewModel();
        }

        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        /// <summary>
        /// Called By _invoiceMapper and GetTransactions returns the Invoice receivable billing service transactions
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        private ObservableCollection<FinancialTransactionViewModel> GetAdjustmentTransactionsForUnbilled(Invoice invoice)
        {
            var transactions = new ObservableCollection<FinancialTransactionViewModel>();
            transactions.AddRangeWithSinglePropertyChangedNotification(_adjustmentToTransactionviewModelMapper.MapAll(invoice.InvoiceReceivables.SelectMany(ir => ir.Adjustments).ToList()));
            return transactions.OrderByDescending(pt => pt.Date).ToObservableCollection();
        }

        /// <summary>
        /// Called by LoadFullInvoiceViewModel, is used to get all transactions (Adjustments, Financial Informations, Billing Service Transactions) related to the invoice provided
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        private IEnumerable<TransactionViewModel> GetTransactions(Invoice invoice)
        {
            var transactions = new List<TransactionViewModel>();
            if (PermissionId.ViewPatientComments.PrincipalContextHasPermission())
            {
                transactions.AddRange(invoice.BillingServices.SelectMany(bs => bs.BillingServiceComments
                    .Select(bsc => new TransactionalNoteViewModel
                                   {
                                       ServiceId = bsc.BillingService.Id,
                                       InvoiceId = bsc.BillingService.Invoice.Id,
                                       Content = bsc.Value,
                                       Id = (int)bsc.Id,
                                       Date = bsc.DateTime.GetValueOrDefault(),
                                       IsArchived = bsc.IsArchived
                                   })).ToObservableCollection());
            }
            transactions.AddRange(_billingServiceTransactionMapper.MapAll(invoice.InvoiceReceivables.SelectMany(ir => ir.BillingServiceTransactions).ToList()));
            transactions.AddRange(GetAdjustmentTransactionsForUnbilled(invoice));
            transactions.AddRange(_financialInformationToTransactionviewModelMapper.MapAll(invoice.InvoiceReceivables.SelectMany(ir => ir.FinancialInformations).ToList()));
            return transactions.OrderByDescending(pt => pt.Date).ToList();
        }

        /// <summary>
        /// called by _invoiceMapper, this method gets all unassigned transactions ie invoice receivable adjustments where the billing service is null
        /// and invoice receivable financial informations where the billing service is null
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        private ObservableCollection<TransactionViewModel> GetUnassignedTransactions(Invoice invoice)
        {
            var transactions = new ObservableCollection<TransactionViewModel>();
            transactions.AddRangeWithSinglePropertyChangedNotification(_adjustmentToTransactionviewModelMapper.MapAll(invoice.InvoiceReceivables.SelectMany(ir => ir.Adjustments.Where(adj => adj.BillingService == null))));
            transactions.AddRangeWithSinglePropertyChangedNotification(_financialInformationToTransactionviewModelMapper.MapAll(invoice.InvoiceReceivables.SelectMany(ir => ir.FinancialInformations.Where(adj => adj.BillingService == null))));
            return transactions.OrderByDescending(t => t.Date).ToObservableCollection();
        }

        /// <summary>
        /// Called by LoadPatientFinancialSummaryInformation returns transaction view models for financial informations and adjustments where the billing service and invoice receivables are null
        /// </summary>
        /// <param name="adjustments"></param>
        /// <returns></returns>
        private ObservableCollection<TransactionViewModel> GetOnAccountTransactions(IEnumerable<Adjustment> adjustments)
        {
            var transactions = new ObservableCollection<TransactionViewModel>();
            transactions.AddRangeWithSinglePropertyChangedNotification(_adjustmentToTransactionviewModelMapper.MapAll(adjustments.Where(adj => adj.AdjustmentTypeId == (int)AdjustmentTypeId.Payment && adj.InvoiceReceivable == null)));
            return transactions.OrderByDescending(t => t.Date).ToObservableCollection();
        }

        /// <summary>
        /// Fetch the diagnosis description
        /// </summary>
        /// <param name="diagnosisViewModel"></param>
        /// <returns></returns>
        public DiagnosisCodeViewModel FetchDiagnosisDescription(DiagnosisViewModel diagnosisViewModel)
        {
            var description = PracticeRepository.ClinicalConditions.Where(cc => cc.Id == diagnosisViewModel.ActiveDiagnosisCode.Key.ToInt()).Select(cc => cc.Name).FirstOrDefault();
            diagnosisViewModel.ActiveDiagnosisCode.Description = description;
            return diagnosisViewModel.ActiveDiagnosisCode;
        }

        public void DeleteComment(TransactionalNoteViewModel noteViewModelToDelete)
        {
            BillingServiceComment billingServiceComment = PracticeRepository.BillingServiceComments.Include(x => x.Alerts).FirstOrDefault(i => i.BillingServiceId == noteViewModelToDelete.ServiceId && i.Id == noteViewModelToDelete.Id);
            if (billingServiceComment != null)
            {
                PracticeRepository.Delete(billingServiceComment);
            }
            InvoiceComment invoicecomment = PracticeRepository.InvoiceComments.Include(x => x.Alerts).FirstOrDefault(i => i.InvoiceId == noteViewModelToDelete.InvoiceId && i.Id == noteViewModelToDelete.Id);
            if (invoicecomment != null)
            {
                PracticeRepository.Delete(invoicecomment);
            }
        }
    }
}
