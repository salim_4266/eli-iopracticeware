﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientInsurance;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

[assembly: Component(typeof(PatientInsuranceViewService), typeof(IPatientInsuranceViewService))]
[assembly: Component(typeof(PolicyDetailViewModelMap), typeof(IMap<PatientInsurance, PolicyDetailsViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientInsurance
{
    public interface IPatientInsuranceViewService
    {
        PatientInsuranceLoadInformation LoadInformation(int patientId);
        void SavePolicyRankAndInsuranceType(IEnumerable<PolicyRankDetailViewModel> policies);
        void SavePolicy(PolicyViewModel policy);
        void SavePolicy(PolicyViewModel policy, bool resetBilledPolicyHolderToInsuredPatient);
        void DeletePolicy(long patientInsuranceId);
        void EndPolicy(long patientInsuranceId);
        void SetEndDateForAllPolicies(long patientInsuranceId, DateTime endDate);
        void ReactivatePolicy(long patientInsuranceId);
        bool HasAdjustments(long patientInsuranceId);
        bool IsPolicyHolderPolicyDeleted(int policyHolderId);
        int SaveImage(int patientInsuranceId, byte[] image);
        void DeleteImage(int imageId);
        void SaveMedicareSecondaryReason(int insurancePolicyId, int? reasonCode);

        /// <summary>
        /// Resets the communication preference of the patient for 'Statements' back to the address of the patient themself (Instead of whoever else was set to receive statements)
        /// </summary>
        /// <param name="patientId"></param>
        void ResetBillPolicyHolderToPatient(int patientId);
    }

    public class PatientInsuranceViewService : BaseViewService, IPatientInsuranceViewService
    {
        private readonly IMapper<Model.PatientInsurance, PolicyDetailsViewModel> _policyDetailViewModelMapper;

        public PatientInsuranceViewService(IMapper<Model.PatientInsurance, PolicyDetailsViewModel> policyDetailViewModelMapper)
        {
            _policyDetailViewModelMapper = policyDetailViewModelMapper;
        }

        public PatientInsuranceLoadInformation LoadInformation(int patientId)
        {
            var viewModel = new PatientInsuranceLoadInformation();

            viewModel.MedicareSecondaryReasonCodes = Enum.GetValues(typeof(MedicareSecondaryReasonCode)).Cast<MedicareSecondaryReasonCode>().Select(x => new NameAndAbbreviationViewModel { Id = (int)x, Name = x.GetDisplayName(), Abbreviation = x.GetDescription() }).ToExtendedObservableCollection();
            viewModel.Relationships = Enum.GetValues(typeof(PolicyHolderRelationshipType)).Cast<PolicyHolderRelationshipType>().Where(x => x != PolicyHolderRelationshipType.Self).Select(x => new NamedViewModel { Id = (int)x, Name = x.GetDisplayName() }).ToExtendedObservableCollection();

            var policyViewModels = GetPolicyViewModels(patientId);

            viewModel.Policies = policyViewModels.ToExtendedObservableCollection();

            return viewModel;
        }

        public void SavePolicyRankAndInsuranceType(IEnumerable<PolicyRankDetailViewModel> policies)
        {
            var policiesToRerank = policies.ToList();
            var insuranceIds = policiesToRerank.Select(p => p.PatientInsuranceId).ToList();

            var patientInsurances = PracticeRepository.PatientInsurances.Where(x => insuranceIds.Contains(x.Id)).ToList();

            foreach (var patientInsurance in patientInsurances)
            {
                var policy = policiesToRerank.Single(p => p.PatientInsuranceId == patientInsurance.Id);
                patientInsurance.OrdinalId = policy.PolicyRank;
                patientInsurance.InsuranceType = policy.InsuranceType;
                PracticeRepository.Save(patientInsurance);
            }
        }

        public void SavePolicy(PolicyViewModel policy)
        {
            SavePolicy(policy, false);
        }

        public void SavePolicy(PolicyViewModel policy, bool resetBilledPolicyHolderToInsuredPatient)
        {
            var insurance = PracticeRepository.PatientInsurances.Include(x => x.InsurancePolicy).SingleOrDefault(x => x.Id == policy.PatientInsuranceId);

            if (insurance == null) { throw new InvalidOperationException("No record for was found for this insurance policy: {0}".FormatWith(policy.PatientInsuranceId)); }

            MapPolicyViewModelToPatientInsurance(policy, insurance);

            ValidateEndDateForLinkedPolicies(insurance, policy.EndDate);

            PracticeRepository.Save(insurance);

            // set the Bill Policy Holder
            if (resetBilledPolicyHolderToInsuredPatient)
            {
                ResetBillPolicyHolderToPatient(policy.InsuredPatientId);
            }
            else if (policy.IsBilledPolicyHolder)
            {
                SetBillPolicyHolder(policy);
            }
        }

        public void DeletePolicy(long patientInsuranceId)
        {
            var insurance = PracticeRepository.PatientInsurances.SingleOrDefault(x => x.Id == patientInsuranceId && !x.IsDeleted);

            // check to see if this insurance is self. If so then must delete for all linked policies

            if (insurance == null) return;

            if (insurance.PolicyholderRelationshipType == PolicyHolderRelationshipType.Self)
            {
                var linkedInsurances = PracticeRepository.PatientInsurances.Where(x => x.InsurancePolicyId == insurance.InsurancePolicyId);

                foreach (var linkedInsurance in linkedInsurances)
                {
                    linkedInsurance.IsDeleted = true;
                    if (!linkedInsurance.EndDateTime.HasValue)
                    {
                        linkedInsurance.EndDateTime = DateTime.Now.ToClientTime().AddDays(-1);
                    }

                    PracticeRepository.Save(linkedInsurance);
                }
                return;
            }


            insurance.IsDeleted = true;

            if (!insurance.EndDateTime.HasValue)
            {
                insurance.EndDateTime = DateTime.Now.ToClientTime().AddDays(-1);
            }

            PracticeRepository.Save(insurance);
        }

        public void EndPolicy(long patientInsuranceId)
        {
            var insurance = PracticeRepository.PatientInsurances.Single(x => x.Id == patientInsuranceId);

            if (insurance.EndDateTime.HasValue)
            {
                throw new ValidationException("The policy you attempted to end already has an end date");
            }

            insurance.EndDateTime = DateTime.Now.ToClientTime().AddDays(-1);
            PracticeRepository.Save(insurance);
        }

        public void SetEndDateForAllPolicies(long patientInsuranceId, DateTime endDate)
        {
            var patientInsurance = PracticeRepository.PatientInsurances
                .Include(x => x.InsurancePolicy)
                .Single(x => x.Id == patientInsuranceId);

            // grab all the patient insurances that have the same insurance policy and are not deleted and not ended
            var allInsurancesLinkedToPolicy = PracticeRepository.PatientInsurances
                .Include(x => x.InsurancePolicy)
                .Where(x => x.InsurancePolicyId == patientInsurance.InsurancePolicyId && !x.IsDeleted);

            foreach (var p in allInsurancesLinkedToPolicy)
            {
                if (patientInsurance.PolicyholderRelationshipType == PolicyHolderRelationshipType.Self && p.Id == patientInsurance.Id)
                {
                    p.EndDateTime = endDate;
                    PracticeRepository.Save(p);
                }
                else
                {
                    if ((p.EndDateTime.HasValue && p.EndDateTime > endDate) || (!p.EndDateTime.HasValue))
                    {
                        p.EndDateTime = endDate;
                        PracticeRepository.Save(p);
                    }
                }
            }
        }

        private void ValidateEndDateForLinkedPolicies(Model.PatientInsurance patientInsurance, DateTime? newEndDate)
        {
            if (patientInsurance.PolicyholderRelationshipType != PolicyHolderRelationshipType.Self)
            {
                // get the policy holder insurance
                var policyHoldersMainInsurance = PracticeRepository.PatientInsurances
                                                .Include(x => x.InsurancePolicy)
                                                .FirstOrDefault(x => x.InsurancePolicyId == patientInsurance.InsurancePolicyId
                                                    && x.InsuredPatientId == patientInsurance.InsurancePolicy.PolicyholderPatientId
                                                    && !x.IsDeleted);

                if (policyHoldersMainInsurance == null)
                {
                    throw new ValidationException("No policy holder insurance can be found");
                }

                // in theory this should never be true because at this point we already know it's a linked policy
                if (policyHoldersMainInsurance.Id == patientInsurance.Id) return;

                if (!newEndDate.HasValue && policyHoldersMainInsurance.EndDateTime.HasValue)
                {
                    throw new ValidationException("Uh-oh, you can't remove the End Date here because the policyholder's policy still has an End Date. Remove the End Date on the policyholder's policy first");
                }

                if (policyHoldersMainInsurance.EndDateTime.HasValue && newEndDate > policyHoldersMainInsurance.EndDateTime)
                {
                    throw new ValidationException("Whoops, the End Date you are trying to enter is after the policyholder's policy End Date. Update the End Date of the policyholder's policy first");
                }
            }
        }

        public void ReactivatePolicy(long patientInsuranceId)
        {
            var insurance = PracticeRepository.PatientInsurances.Include(x => x.InsurancePolicy).SingleOrDefault(x => x.Id == patientInsuranceId);

            if (insurance == null) return;

            if (insurance.PolicyholderRelationshipType == PolicyHolderRelationshipType.Self)
            {
                insurance.IsDeleted = false;
                PracticeRepository.Save(insurance);
            }
            else
            {
                var policyHolderInsurance = PracticeRepository.PatientInsurances.Single(x => x.InsurancePolicyId == insurance.InsurancePolicyId && x.PolicyholderRelationshipType == PolicyHolderRelationshipType.Self && !x.IsDeleted);

                if (!policyHolderInsurance.IsDeleted)
                {
                    insurance.IsDeleted = false;
                    PracticeRepository.Save(insurance);
                }
                else
                {
                    throw new ValidationException("You cannot reactivate this policy when the policy holder's insurance is also deleted for this policy");
                }
            }

        }

        public bool HasAdjustments(long patientInsuranceId)
        {
            var hasAdjustments = PracticeRepository.PatientInsurances.Any(x => x.Id == patientInsuranceId && x.InvoiceReceivables.Any(y => y.Adjustments.Any()));

            return hasAdjustments;
        }

        public bool IsPolicyHolderPolicyDeleted(int insurancePolicyId)
        {
            var policyHolderInsurance = PracticeRepository.PatientInsurances.Single(x => x.InsurancePolicyId == insurancePolicyId && x.PolicyholderRelationshipType == PolicyHolderRelationshipType.Self);

            if (policyHolderInsurance != null)
            {
                return policyHolderInsurance.IsDeleted;
            }

            return false;
        }

        public int SaveImage(int patientInsuranceId, byte[] image)
        {
            var imageDocument = new PatientInsuranceDocument
                                    {
                                        Content = image,
                                        PatientInsuranceId = patientInsuranceId
                                    };

            PracticeRepository.Save(imageDocument);

            return imageDocument.Id;
        }

        public void DeleteImage(int imageId)
        {
            var imageDocument = PracticeRepository.PatientInsuranceDocuments.Single(x => x.Id == imageId);
            PracticeRepository.Delete(imageDocument);
        }

        public void SaveMedicareSecondaryReason(int insurancePolicyId, int? reasonCode)
        {
            var insurancePolicy = PracticeRepository.InsurancePolicies.Single(x => x.Id == insurancePolicyId);

            var reason = reasonCode.HasValue ? (MedicareSecondaryReasonCode)reasonCode.Value : default(MedicareSecondaryReasonCode?);

            insurancePolicy.MedicareSecondaryReasonCode = reason;

            PracticeRepository.Save(insurancePolicy);
        }

        public void ResetBillPolicyHolderToPatient(int patientId)
        {
            SetBillPolicyHolder(new PolicyViewModel { InsuredPatientId = patientId }, true);
        }

        /// <summary>
        /// Sets the policy holder of the policy as the preferred communication for sending statement bills
        /// </summary>
        /// <param name="policy">The patient of this policy will have their communication preference for statements be sent to the policy holder of the policy</param>
        /// <param name="resetToPatient">Set to true if you want the communication preference set back to the patient themself regardless of who the policy holder is.</param>
        private void SetBillPolicyHolder(PolicyViewModel policy, bool resetToPatient = false)
        {
            var patientCommunicationPreferences = PracticeRepository.PatientCommunicationPreferences
                                                                   .Where(x => x.PatientId == policy.InsuredPatientId && x.PatientCommunicationType == PatientCommunicationType.Statement)
                                                                   .OfType<PatientAddressCommunicationPreference>();

            var preference = patientCommunicationPreferences.OrderBy(x => x.Id).FirstOrDefault() ?? new PatientAddressCommunicationPreference
                                                                                                    {
                                                                                                        IsAddressedToRecipient = false,
                                                                                                        PatientId = policy.InsuredPatientId,
                                                                                                        PatientCommunicationType = PatientCommunicationType.Statement
                                                                                                    };

            var patientIdOfAddressToPull = resetToPatient ? policy.InsuredPatientId : policy.PolicyHolder.Id;

            var address = PracticeRepository.PatientAddresses.Where(x => x.PatientId == patientIdOfAddressToPull)
                                               .OrderBy(x => x.PatientAddressTypeId).ThenBy(x => x.Id)
                                               .FirstOrDefault();

            if (address != null)
            {
                preference.PatientAddressId = address.Id;
                PracticeRepository.Save(preference);
            }
        }

        private IEnumerable<PolicyViewModel> GetPolicyViewModels(int patientId)
        {
            var patientInsurances = PracticeRepository.PatientInsurances.Where(x => x.InsuredPatientId == patientId).ToList();

            PracticeRepository.AsQueryableFactory().Load(patientInsurances,
                                                         x => x.PatientInsuranceDocuments,
                                                         x => x.InsurancePolicy.Insurer.InsurerPlanType,
                                                         x => x.InsurancePolicy.Insurer.InsurerAddresses,
                                                         x => x.InsurancePolicy.Insurer.InsurerPhoneNumbers,
                                                         x => x.InsurancePolicy.PolicyholderPatient.PatientAddresses.Select(y => y.StateOrProvince),
                                                         x => x.InsurancePolicy.PolicyholderPatient.PatientPhoneNumbers,
                                                         x => x.InsurancePolicy.PolicyholderPatient.ExternalProviders.Select(y => y.ExternalProvider),
                                                         x => x.InsurancePolicy.PolicyholderPatient.PatientTags.Select(y => y.Tag),
                                                         x => x.InsurancePolicy.PolicyholderPatient.DefaultUser,
                                                         x => x.InsurancePolicy.PatientInsurances.Select(y => y.InsuredPatient));

            var policyViewModels = patientInsurances.ToPolicyViewModels();

            var patientCommunicationPreferences = PracticeRepository.PatientCommunicationPreferences
                                                                    .OfType<PatientAddressCommunicationPreference>()
                                                                    .Include(x => x.PatientAddress)
                                                                    .Where(x => x.PatientId == patientId && x.PatientCommunicationType == PatientCommunicationType.Statement)
                                                                    .ToList();

            var billPolicyHolderSet = false;
            foreach (var policyViewModel in policyViewModels)
            {
                policyViewModel.PolicyDetails = _policyDetailViewModelMapper.Map(patientInsurances.Single(x => x.Id == policyViewModel.PatientInsuranceId));

                if (policyViewModel.PolicyDetails.LinkedPatients.Count == 1 && policyViewModel.PolicyHolderIsSelf)
                {
                    policyViewModel.PolicyDetails.LinkedPatients.First().Id = -1;
                    policyViewModel.PolicyDetails.LinkedPatients.First().Name = "None";
                    policyViewModel.PolicyDetails.LinkedPatients.First().IsPolicyHolder = false;
                }

                if (policyViewModel.PolicyDetails.AttachedImages == null)
                {
                    policyViewModel.PolicyDetails.AttachedImages = new List<AttachedImage>().ToExtendedObservableCollection();
                }

                if (!billPolicyHolderSet && policyViewModel.PolicyStatus != PolicyRowStatus.Ended && policyViewModel.PolicyStatus != PolicyRowStatus.Deleted && patientCommunicationPreferences.Any(x => x.PatientAddress.IfNotNull(y => y.PatientId, -1) == policyViewModel.PolicyHolder.Id))
                {
                    policyViewModel.IsBilledPolicyHolder = true;
                    billPolicyHolderSet = true;
                }
            }

            return policyViewModels;
        }

        private static void MapPolicyViewModelToPatientInsurance(PolicyViewModel viewModel, Model.PatientInsurance patientInsurance)
        {
            if (patientInsurance.PolicyholderRelationshipType == PolicyHolderRelationshipType.Self)
            {
                patientInsurance.InsurancePolicy.Copay = viewModel.Copay;
                patientInsurance.InsurancePolicy.Deductible = viewModel.Deductible;
                patientInsurance.InsurancePolicy.GroupCode = viewModel.GroupCode;
                if (viewModel.StartDate.HasValue) patientInsurance.InsurancePolicy.StartDateTime = viewModel.StartDate.Value;
                patientInsurance.InsurancePolicy.PolicyCode = viewModel.PolicyCode;
                patientInsurance.InsurancePolicy.MedicareSecondaryReasonCode = viewModel.MedicareSecondaryReasonCode;
            }

            patientInsurance.InsuranceType = viewModel.InsuranceType;
            patientInsurance.EndDateTime = viewModel.EndDate;
            patientInsurance.PolicyholderRelationshipType = viewModel.Relationship;
        }
    }

    public class PolicyDetailViewModelMap : IMap<Model.PatientInsurance, PolicyDetailsViewModel>
    {
        private readonly IEnumerable<IMapMember<Model.PatientInsurance, PolicyDetailsViewModel>> _members;

        public PolicyDetailViewModelMap()
        {
            _members = this.CreateMembers(insurance => new PolicyDetailsViewModel
                                                           {
                                                               PolicyHoldersMainInsuranceEndDate = insurance.InsurancePolicy.PatientInsuranceForPolicyholder.IfNotNull(pi => pi.EndDateTime),
                                                               LinkedPatients = insurance.InsurancePolicy.PatientInsurances.Where(x => !x.IsDeleted || x.PolicyholderRelationshipType == PolicyHolderRelationshipType.Self).Select(x => new LinkedPatient { Id = x.InsuredPatient.Id, Name = x.InsuredPatient.DisplayName, IsPolicyHolder = insurance.InsurancePolicy.PolicyholderPatientId == x.InsuredPatientId }).ToExtendedObservableCollection(),
                                                               AttachedImages = insurance.PatientInsuranceDocuments.Select(x =>  x.Content != null ? x.Content != new byte[] { } ?  new AttachedImage { Id = x.Id,  Image = x.Content } : null : null ).OrderByDescending(x => x.Id).ToExtendedObservableCollection()
                                                           });
        }

        public IEnumerable<IMapMember<Model.PatientInsurance, PolicyDetailsViewModel>> Members
        {
            get { return _members; }
        }
    }


}
