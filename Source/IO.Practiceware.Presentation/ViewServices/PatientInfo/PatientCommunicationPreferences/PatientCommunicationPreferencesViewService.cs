﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientCommunicationPreferences;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientCommunicationPreferences;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

[assembly: Component(typeof(PatientCommunicationPreferencesViewService), typeof(IPatientCommunicationPreferencesViewService))]

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientCommunicationPreferences
{
    public interface IPatientCommunicationPreferencesViewService
    {
        /// <summary>
        /// Returns the necessary data (Contacts, Preferences) to load up the screen.
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        PatientCommunicationPreferencesLoadInformation LoadPreferences(int patientId);

        /// <summary>
        /// Adds a new relationship between both patient's.  Returns a ContactViewModel for the new toPatient
        /// </summary>
        /// <param name="fromPatientId"></param>
        /// <param name="toPatientId"></param>
        /// <param name="relationshipDescription"></param>
        /// <returns></returns>
        PatientContactViewModel AddContactToPatient(int fromPatientId, int toPatientId, string relationshipDescription = null);

        /// <summary>
        /// Deletes the Patient Relationship
        /// </summary>
        /// <param name="patientRelationshipId"></param>
        /// <exception cref="System.ComponentModel.DataAnnotations.ValidationException">Thrown when the contact being unlinked is referenced in one or more patient communication preferences</exception>
        void UnlinkContactFromPatient(int patientRelationshipId);

        /// <summary>
        /// Saves the list of patientCommunicationPreferences for the specified patientId
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="patientCommunicationPreferences"></param>
        /// <param name="comments"></param>
        void SaveAllPatientCommunicationPreferences(int patientId, IEnumerable<PatientCommunicationPreferenceViewModel> patientCommunicationPreferences, string comments);

        /// <summary>
        /// Deletes the specified preference
        /// </summary>
        /// <param name="patientCommunicationPreferenceId"></param>
        void RemovePreference(int patientCommunicationPreferenceId);

        /// <summary>
        /// Saves the contact information specified to the PatientRelationships table        
        /// </summary>        
        /// <param name="contacts"></param>
        void SaveAllContactInfo(IEnumerable<PatientContactViewModel> contacts);
    }

    public class PatientCommunicationPreferencesViewService : BaseViewService, IPatientCommunicationPreferencesViewService
    {
        private readonly Func<PatientCommunicationPreferencesLoadInformation> _createPatientCommunicationPreferencesLoadInformation;
        private readonly ICustomMapperFactory _mapperFactory;

        private IMapper<PatientRelationship, PatientContactViewModel> _contactMapper;
        private IMapper<PatientEmailAddress, EmailAddressViewModel> _emailAddressMapper;
        private IMapper<PatientAddress, AddressViewModel> _addressMapper;
        private IMapper<PatientPhoneNumber, PhoneNumberViewModel> _phoneNumberMapper;
        private IMapper<PatientCommunicationPreference, PatientCommunicationPreferenceViewModel> _communicationPreferenceMapper;


        public PatientCommunicationPreferencesViewService(Func<PatientCommunicationPreferencesLoadInformation> createPatientCommunicationPreferencesLoadInformation,
            ICustomMapperFactory mapperFactory)
        {
            _createPatientCommunicationPreferencesLoadInformation = createPatientCommunicationPreferencesLoadInformation;
            _mapperFactory = mapperFactory;
            InitializeMappers();
        }

        public void InitializeMappers()
        {
            _contactMapper = Mapper.Factory.CreateMapper<PatientRelationship, PatientContactViewModel>(patientRelationship => new PatientContactViewModel
                                                                                                   {
                                                                                                       FullName = patientRelationship.ToPatient.DisplayName,
                                                                                                       Hippa = patientRelationship.HasSignedHipaa,
                                                                                                       PatientRelationshipId = patientRelationship.Id,
                                                                                                       PatientId = patientRelationship.ToPatientId,
                                                                                                       RelationshipDescription = patientRelationship.PatientRelationshipDescription
                                                                                                   });

            _emailAddressMapper = Mapper.Factory.CreateMapper<PatientEmailAddress, EmailAddressViewModel>(emailAddress => new EmailAddressViewModel
                                                                                                                         {
                                                                                                                             Id = emailAddress.Id,
                                                                                                                             EmailAddressType = new NamedViewModel { Id = (int)emailAddress.EmailAddressType, Name = emailAddress.EmailAddressType.GetDisplayName() },
                                                                                                                             OrdinalId = emailAddress.OrdinalId,
                                                                                                                             PatientId = emailAddress.PatientId,
                                                                                                                             Value = emailAddress.Value
                                                                                                                         });

            _addressMapper = Mapper.Factory.CreateAddressViewModelMapper<PatientAddress>();

            _phoneNumberMapper = _mapperFactory.CreatePhoneViewModelMapper<PatientPhoneNumber>();

            _communicationPreferenceMapper = _mapperFactory.CreateMapper<PatientCommunicationPreference, PatientCommunicationPreferenceViewModel>(pcp => new PatientCommunicationPreferenceViewModel
                                                                                                                                                             {
                                                                                                                                                                 CommunicationType = pcp.PatientCommunicationType,
                                                                                                                                                                 DestinationId = pcp.DestinationId,
                                                                                                                                                                 Id = pcp.Id,
                                                                                                                                                                 MethodId = pcp.CommunicationMethodTypeId,
                                                                                                                                                                 PatientContact = new PatientContactViewModel { PatientId = pcp.SelectedContactPatientId },
                                                                                                                                                                 IsOptOut = pcp.IsOptOut
                                                                                                                                                             });


        }

        public PatientCommunicationPreferencesLoadInformation LoadPreferences(int patientId)
        {
            var relationships = PracticeRepository.PatientRelationships.Where(x => x.FromPatientId == patientId).ToList();

            // manually add a reference to self if it does not exist
            if (!relationships.Any(x => x.FromPatientId == patientId && x.ToPatientId == patientId))
            {
                relationships.Add(new PatientRelationship { FromPatientId = patientId, ToPatientId = patientId, PatientRelationshipDescription = "Self" });
            }

            // load all data for relationships
            LoadRelationships(relationships);

            var contactViewModels = _contactMapper.MapAll(relationships);

            foreach (var r in relationships)
            {
                var correspondingViewModel = contactViewModels.Single(x => x.PatientRelationshipId == r.Id);

                correspondingViewModel.EmailAddresses = _emailAddressMapper.MapAll(r.ToPatient.PatientEmailAddresses).ToExtendedObservableCollection();
                correspondingViewModel.Addresses = _addressMapper.MapAll(r.ToPatient.PatientAddresses).ToExtendedObservableCollection();
                correspondingViewModel.PhoneNumbers = _phoneNumberMapper.MapAll(r.ToPatient.PatientPhoneNumbers).ToExtendedObservableCollection();
            }

            // load the preferences
            var preferences = PracticeRepository.PatientCommunicationPreferences.Where(x => x.PatientId == patientId).ToList();
            LoadPreferences(preferences);

            // map the preferences
            var preferenceViewModels = _communicationPreferenceMapper.MapAll(preferences);
            foreach (var p in preferenceViewModels)
            {
                // set the correct contact
                p.PatientContact = contactViewModels.SingleOrDefault(x => x.PatientId == p.PatientContact.PatientId);
            }

            var comments = PracticeRepository.PatientComments.Where(x => x.PatientId == patientId && x.PatientCommentType == PatientCommentType.PatientCommunicationPreferenceNotes);
            var commentValues = comments.Join(x => x.Value);

            var loadInfo = _createPatientCommunicationPreferencesLoadInformation();
            loadInfo.Contacts = contactViewModels.ToList();
            loadInfo.PatientCommunicationPreferences = preferenceViewModels.ToList();
            loadInfo.PatientCommunicationPreferencesNotes = commentValues;

            return loadInfo;
        }

        private void LoadPreferences(IEnumerable<PatientCommunicationPreference> preferences)
        {
            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(preferences.OfType<PatientAddressCommunicationPreference>(), p => p.PatientAddress);
            queryableFactory.Load(preferences.OfType<PatientEmailAddressCommunicationPreference>(), p => p.PatientEmailAddress);
            queryableFactory.Load(preferences.OfType<PatientPhoneNumberCommunicationPreference>(), p => p.PatientPhoneNumber);
        }

        // Loads all foreign key objects to PatientRelationships
        private void LoadRelationships(IEnumerable<PatientRelationship> relationships)
        {
            PracticeRepository.AsQueryableFactory().Load(relationships,
                                                         r => r.FromPatient,
                                                         r => r.ToPatient,
                                                         r => r.ToPatient.PatientEmailAddresses,
                                                         r => r.ToPatient.PatientAddresses.Select(x => x.PatientAddressType),
                                                         r => r.ToPatient.PatientAddresses.Select(x => x.StateOrProvince),
                                                         r => r.ToPatient.PatientPhoneNumbers);
        }

        public PatientContactViewModel AddContactToPatient(int fromPatientId, int toPatientId, string relationshipDescription = null)
        {
            var newRelationship = new PatientRelationship
                                      {
                                          FromPatientId = fromPatientId,
                                          ToPatientId = toPatientId,
                                          PatientRelationshipDescription = relationshipDescription
                                      };

            PracticeRepository.Save(newRelationship);

            // load all data for this object
            LoadRelationships(new[] { newRelationship });

            var contactViewModel = _contactMapper.Map(newRelationship);
            contactViewModel.EmailAddresses = _emailAddressMapper.MapAll(newRelationship.ToPatient.PatientEmailAddresses).ToExtendedObservableCollection();
            contactViewModel.Addresses = _addressMapper.MapAll(newRelationship.ToPatient.PatientAddresses).ToExtendedObservableCollection();
            contactViewModel.PhoneNumbers = _phoneNumberMapper.MapAll(newRelationship.ToPatient.PatientPhoneNumbers).ToExtendedObservableCollection();

            return contactViewModel;
        }

        public void UnlinkContactFromPatient(int patientRelationshipId)
        {
            if (patientRelationshipId <= 0) return;

            var patientRelationship = PracticeRepository.PatientRelationships.Single(x => x.Id == patientRelationshipId);

            var existingPreferences = PracticeRepository.PatientCommunicationPreferences.Where(x => x.PatientId == patientRelationship.FromPatientId).ToList();
            LoadPreferences(existingPreferences);

            if (existingPreferences.Any(x => x.SelectedContactPatientId == patientRelationship.ToPatientId))
            {
                throw new ValidationException("Cannot unlink contact because it is being referenced in one or more Patient Communication Preferences");
            }

            PracticeRepository.Delete(patientRelationship);
        }


        public void SaveAllPatientCommunicationPreferences(int patientId, IEnumerable<PatientCommunicationPreferenceViewModel> patientCommunicationPreferences, string comments)
        {
            foreach (var viewModelPreference in patientCommunicationPreferences)
            {
                // make sure new entities have the required fields
                if ((!viewModelPreference.Id.HasValue) && (viewModelPreference.PatientContact == null || viewModelPreference.Method == null || viewModelPreference.DestinationId == null)) continue;

                PatientCommunicationPreference preference;

                // logic for existing entities
                if (viewModelPreference.Id.HasValue && viewModelPreference.Id.Value > 0)
                {
                    preference = PracticeRepository.PatientCommunicationPreferences.SingleOrDefault(pcp => pcp.Id == viewModelPreference.Id.Value);

                    // make sure the type does not change for existing preference
                    if (preference == null || preference.PatientCommunicationType != viewModelPreference.CommunicationType)
                    {
                        continue;
                    }

                    if (viewModelPreference.PatientContact == null || viewModelPreference.MethodId == null || viewModelPreference.DestinationId == null)
                    {
                        // delete this preference if user set all fields to null
                        PracticeRepository.Delete(preference);
                        continue;
                    }

                    // check to see if the type has changed
                    if (PatientCommunicationMappings.CommunicationMethodToPatientCommunicationPreferenceMapping[preference.CommunicationMethodType] != PatientCommunicationMappings.CommunicationMethodToPatientCommunicationPreferenceMapping[viewModelPreference.Method.EnsureNotDefault().Value])
                    {
                        // the destination type has changed, therefore we must delete this preference and create a new one in its place
                        PracticeRepository.Delete(preference);
                        preference = PatientCommunicationPreferenceFactory.CreatePreference(viewModelPreference.DestinationType);
                        preference.PatientCommunicationType = viewModelPreference.CommunicationType;
                    }
                }
                else
                {
                    preference = PatientCommunicationPreferenceFactory.CreatePreference(viewModelPreference.DestinationType);
                    preference.PatientCommunicationType = viewModelPreference.CommunicationType;
                }

                preference.DestinationId = (int)viewModelPreference.DestinationId.EnsureNotDefault().Value;
                preference.PatientId = patientId;
                preference.CommunicationMethodType = viewModelPreference.Method.EnsureNotDefault().Value;
                preference.IsOptOut = viewModelPreference.IsOptOut;

                PracticeRepository.Save(preference);
            }

            var patientComment = PracticeRepository.PatientComments.FirstOrDefault(pc => pc.PatientId == patientId && pc.PatientCommentType == PatientCommentType.PatientCommunicationPreferenceNotes);
            if (patientComment != null)
            {
                PracticeRepository.Delete(patientComment);
            }

            if (comments.IsNotNullOrEmpty())
            {
                patientComment = new PatientComment();
                patientComment.PatientId = patientId;
                patientComment.PatientCommentType = PatientCommentType.PatientCommunicationPreferenceNotes;
                patientComment.Value = comments;
                PracticeRepository.Save(patientComment);
            }
        }


        public void RemovePreference(int patientCommunicationPreferenceId)
        {
            if (patientCommunicationPreferenceId <= 0) return;

            var preference = PracticeRepository.PatientCommunicationPreferences.Single(x => x.Id == patientCommunicationPreferenceId);

            PracticeRepository.Delete(preference);
        }


        public void SaveAllContactInfo(IEnumerable<PatientContactViewModel> contacts)
        {
            if (contacts.IsNullOrEmpty()) return;

            foreach (var c in contacts)
            {
                if (c.PatientRelationshipId > 0)
                {
                    var relationship = PracticeRepository.PatientRelationships.Single(x => x.Id == c.PatientRelationshipId);
                    relationship.HasSignedHipaa = c.Hippa;
                    relationship.PatientRelationshipDescription = c.RelationshipDescription;

                    PracticeRepository.Save(relationship);
                }
            }
        }
    }
}
