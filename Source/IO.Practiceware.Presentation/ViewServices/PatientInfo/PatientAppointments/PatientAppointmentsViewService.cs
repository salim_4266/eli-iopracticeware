﻿using IO.Practiceware.Model;
using IO.Practiceware.Model.Auditing;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientAppointments;
using IO.Practiceware.Presentation.ViewServices.PatientInsuranceAuthorization;
using IO.Practiceware.Presentation.ViewServices.PatientReferralPopup;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using Soaf.Reflection;
using Soaf.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using IO.Practiceware.Presentation.ViewModels.PatientReferralPopup;

[assembly: Component(typeof(PatientAppointmentsViewService), typeof(IPatientAppointmentsViewService))]
[assembly: Component(typeof(PatientAppointmentViewModelMap), typeof(IMap<UserAppointment, PatientAppointmentViewModel>))]
[assembly: Component(typeof(PatientInsuranceAuthorizationViewModelMap), typeof(IMap<PatientInsuranceAuthorization, PatientInsuranceAuthorizationViewModel>))]
[assembly: Component(typeof(ReferralViewModelMap), typeof(IMap<PatientInsuranceReferral, ReferralViewModel>))]
[assembly: Component(typeof(AppointmentChangeViewModelMap), typeof(IMap<KeyValuePair<Appointment, IEnumerable<AuditEntry>>, List<AppointmentChangeViewModel>>))]

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo.PatientAppointments
{
    [DataContract]
    public class AppointmentSearchLoadInformation
    {
        [Dependency]
        [DataMember]
        public List<PatientAppointmentViewModel> PatientAppointments { get; set; }

        [Dependency]
        [DataMember]
        public List<AppointmentChangeViewModel> AppointmentChanges { get; set; }

        [Dependency]
        [DataMember]
        public List<PatientInsuranceAuthorizationViewModel> PatientInsuranceAuthorizations { get; set; }

        [Dependency]
        [DataMember]
        public List<ReferralViewModel> Referrals { get; set; }
    }

    public interface IPatientAppointmentsViewService
    {
        IEnumerable<PatientAppointmentViewModel> LoadPatientAppointments(int patientId);
        void SaveAppointmentComment(int appointmentId, string comment);
        IEnumerable<AppointmentChangeViewModel> LoadAppointmentChanges(IEnumerable<int> appointmentIds);
        IEnumerable<PatientInsuranceAuthorizationViewModel> LoadPatientInsuranceAuthorizations(IEnumerable<int> appointmentIds);
        void RemoveReferralFromEncounter(int encounterId, int referralId);
        void AddReferralToEncounter(int encounterId, int referralId);

        IEnumerable<ReferralViewModel> GetPatientExistingReferralInformation(int patientId);
        ReferralViewModel GetReferral(int referralId);
        PatientInsuranceAuthorizationViewModel GetAuthorization(int id);

        /// <summary>
        /// Gets the patient info load information necessary to load the PatientInfo view
        /// </summary>
        /// <returns>Returns a new instance of AppointmentSearchLoadInformation</returns>
        AppointmentSearchLoadInformation LoadInformation(int patientId);

        void DeleteAppointmentComment(int appointmentId);
    }


    [SupportsUnitOfWork]
    public class PatientAppointmentsViewService : BaseViewService, IPatientAppointmentsViewService
    {
        private readonly Func<IEnumerable<EncounterStatusConfiguration>, IMapper<UserAppointment, PatientAppointmentViewModel>> _createPatientAppointmentMapper;
        private readonly IMapper<List<Model.PatientInsuranceAuthorization>, List<PatientInsuranceAuthorizationViewModel>> _authorizationMapper;
        private readonly IMapper<PatientInsuranceReferral, ReferralViewModel> _referralMapper;
        private readonly Func<IEnumerable<User>, IEnumerable<ServiceLocation>, IEnumerable<EncounterStatusChangeReason>, IEnumerable<AppointmentType>, IMapper<KeyValuePair<Appointment, IEnumerable<AuditEntry>>, List<AppointmentChangeViewModel>>> _createAppointmentChangeMapper;
        private readonly IAuditRepository _auditRepository;
        private readonly IPracticeRepository _practiceRepository;

        public PatientAppointmentsViewService(
            Func<IEnumerable<EncounterStatusConfiguration>, IMapper<UserAppointment, PatientAppointmentViewModel>> createPatientAppointmentMapper,
            IMapper<List<Model.PatientInsuranceAuthorization>, List<PatientInsuranceAuthorizationViewModel>> authorizationMapper,
            IMapper<PatientInsuranceReferral, ReferralViewModel> referralMapper,
            Func<IEnumerable<User>, IEnumerable<ServiceLocation>, IEnumerable<EncounterStatusChangeReason>, IEnumerable<AppointmentType>, IMapper<KeyValuePair<Appointment, IEnumerable<AuditEntry>>, List<AppointmentChangeViewModel>>> createAppointmentChangeMapper,
            IAuditRepository auditRepository,
            IPracticeRepository practiceRepository)
        {
            _createPatientAppointmentMapper = createPatientAppointmentMapper;
            _authorizationMapper = authorizationMapper;
            _referralMapper = referralMapper;
            _createAppointmentChangeMapper = createAppointmentChangeMapper;
            _auditRepository = auditRepository;
            _practiceRepository = practiceRepository;
        }

        public IEnumerable<PatientAppointmentViewModel> LoadPatientAppointments(int patientId)
        {
            var appointments = PracticeRepository.Appointments.OfType<UserAppointment>().Where(x => x.Encounter.PatientId == patientId).ToList();

            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(appointments,
                                  x => x.AppointmentType,
                                  x => x.User,
                                  x => x.Encounter.ConfirmationStatus,
                                  x => x.Encounter.ServiceLocation,
                                  x => x.Encounter.Patient.PatientInsurances.Select(y => y.InsurancePolicy.Insurer));

            EncounterStatusConfiguration[] encounterStatusConfigurations = _practiceRepository.EncounterStatusConfigurations.ToArray();
            var mapper = _createPatientAppointmentMapper(encounterStatusConfigurations);
            return appointments.Select(mapper.Map).ToArray();
        }

        public IEnumerable<AppointmentChangeViewModel> LoadAppointmentChanges(IEnumerable<int> appointmentIds)
        {
            var localAppointmentIds = appointmentIds.ToArray();

            if (localAppointmentIds.IsNullOrEmpty()) return new List<AppointmentChangeViewModel>();

            var getEncounterAuditEntries = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => _auditRepository.AuditEntriesFor<Encounter>(localAppointmentIds).Include(i => i.AuditEntryChanges).ToArray());
            var getAppointmentAuditEntries = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => _auditRepository.AuditEntriesFor<Appointment>(localAppointmentIds).Include(i => i.AuditEntryChanges).ToArray());
            var getAppointments = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => PracticeRepository.Appointments.Where(i => localAppointmentIds.Contains(i.Id)).Include(i => i.Encounter).ToArray());
            var getUsers = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => PracticeRepository.Users.ToArray());
            var getAppointmentTypes = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => PracticeRepository.AppointmentTypes.Where(x => !x.IsArchived).ToArray());
            var getLocations = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => PracticeRepository.ServiceLocations.ToArray());
            var getChangeReasons = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => PracticeRepository.EncounterStatusChangeReasons.ToArray());

            var encounterAuditEntries = getEncounterAuditEntries.Result;
            var appointmentAuditEntries = getAppointmentAuditEntries.Result;
            var appointmentsById = getAppointments.Result.ToDictionary(i => i.Id);

            var mapper = _createAppointmentChangeMapper(getUsers.Result, getLocations.Result, getChangeReasons.Result, getAppointmentTypes.Result);

            var results = new List<AppointmentChangeViewModel>();

            foreach (var g in encounterAuditEntries.GroupBy(i => i.KeyValueNumeric))
            {
                var group = g;
                var currentAppointment = appointmentsById[Convert.ToInt32(group.Key)];

                results.AddRange(mapper.Map(
                    new KeyValuePair<Appointment, IEnumerable<AuditEntry>>(
                        currentAppointment, group.OrderBy(i => i.AuditDateTime).ToArray())));
            }

            foreach (var g in appointmentAuditEntries.GroupBy(i => i.KeyValueNumeric))
            {
                var group = g;
                var currentAppointment = appointmentsById[Convert.ToInt32(group.Key)];

                results.AddRange(mapper.Map(
                    new KeyValuePair<Appointment, IEnumerable<AuditEntry>>(
                        currentAppointment, group.OrderBy(i => i.AuditDateTime).ToArray())));
            }

            return results.OrderByDescending(i => i.DateTime).ToList();
        }

        public IEnumerable<PatientInsuranceAuthorizationViewModel> LoadPatientInsuranceAuthorizations(IEnumerable<int> appointmentIds)
        {
            var patientAuthorizations = PracticeRepository.PatientInsuranceAuthorizations.Where(x => appointmentIds.Contains(x.EncounterId.Value)).Include(x => x.Encounter.ServiceLocation).ToList();

            var viewModels = _authorizationMapper.Map(patientAuthorizations);

            return viewModels;
        }

        public void SaveAppointmentComment(int appointmentId, string comment)
        {
            var appointment = PracticeRepository.Appointments.OfType<UserAppointment>().Single(x => x.Id == appointmentId);

            appointment.Comment = comment;

            PracticeRepository.Save(appointment);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void RemoveReferralFromEncounter(int encounterId, int referralId)
        {
            var encounter = PracticeRepository.Encounters.Include(x => x.PatientInsuranceReferrals).Single(x => x.Id == encounterId);

            var referralToRemove = encounter.PatientInsuranceReferrals.Single(x => x.Id == referralId);
            encounter.PatientInsuranceReferrals.Remove(referralToRemove);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void AddReferralToEncounter(int encounterId, int referralId)
        {
            var encounter = PracticeRepository.Encounters.Include(x => x.PatientInsuranceReferrals).Single(x => x.Id == encounterId);
            var referral = PracticeRepository.PatientInsuranceReferrals.Single(x => x.Id == referralId);
            encounter.PatientInsuranceReferrals.Add(referral);
        }

        public IEnumerable<ReferralViewModel> GetPatientExistingReferralInformation(int patientId)
        {
            var fromDatabaseReferrals = PracticeRepository.PatientInsuranceReferrals.Where(r => r.PatientId == patientId).ToList();

            PracticeRepository.AsQueryableFactory().Load(fromDatabaseReferrals,
                                                          r => r.Encounters,
                                                          r => r.Doctor,
                                                          r => r.Patient,
                                                          r => r.PatientInsurance,
                                                          r => r.PatientInsurance.InsurancePolicy,
                                                          r => r.PatientInsurance.InsurancePolicy.Insurer);
            return fromDatabaseReferrals.Select(r => _referralMapper.Map(r)).ToExtendedObservableCollection();
        }

        public ReferralViewModel GetReferral(int referralId)
        {
            var fromDatabaseReferral = PracticeRepository.PatientInsuranceReferrals.WithId(referralId);

            PracticeRepository.AsQueryableFactory().Load(fromDatabaseReferral,
                                                          r => r.Encounters,
                                                          r => r.Doctor,
                                                          r => r.Patient,
                                                          r => r.PatientInsurance,
                                                          r => r.PatientInsurance.InsurancePolicy,
                                                          r => r.PatientInsurance.InsurancePolicy.Insurer);

            return _referralMapper.Map(fromDatabaseReferral);
        }

        public PatientInsuranceAuthorizationViewModel GetAuthorization(int id)
        {
            var fromDatabaseAuthorization = PracticeRepository.PatientInsuranceAuthorizations.Where(pia => pia.Id == id).Include(p => p.Encounter.ServiceLocation).First();
            var location = new ColoredViewModel { Id = fromDatabaseAuthorization.Encounter.ServiceLocation.Id, Name = fromDatabaseAuthorization.Encounter.ServiceLocation.Name, Color = fromDatabaseAuthorization.Encounter.ServiceLocation.Color };
            var authorization = new PatientInsuranceAuthorizationViewModel { Id = fromDatabaseAuthorization.Id, Code = fromDatabaseAuthorization.AuthorizationCode, Location = location };
            authorization.Date = fromDatabaseAuthorization.AuthorizationDateTime;
            return authorization;
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public virtual AppointmentSearchLoadInformation LoadInformation(int patientId)
        {
            var patientAppointments = LoadPatientAppointments(patientId).ToArray();
            var getAppointmentChanges = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => LoadAppointmentChanges(patientAppointments.Select(x => x.AppointmentId)));
            var getReferrals = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => GetPatientExistingReferralInformation(patientId));
            var getPatientInsuranceAuthorizations = System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => LoadPatientInsuranceAuthorizations(patientAppointments.Select(x => x.AppointmentId)));

            return new AppointmentSearchLoadInformation
                       {
                           AppointmentChanges = getAppointmentChanges.Result.ToList(),
                           PatientAppointments = patientAppointments.ToList(),
                           PatientInsuranceAuthorizations = getPatientInsuranceAuthorizations.Result.ToList(),
                           Referrals = getReferrals.Result.ToList()
                       };
        }

        public void DeleteAppointmentComment(int appointmentId)
        {
            var appointment = PracticeRepository.Appointments.WithId(appointmentId);
            appointment.Comment = null;
            PracticeRepository.Save(appointment);
        }

        public bool GetIsReferralInformationRequired(int patientId, InsuranceType insuranceType, DateTime dateTime)
        {
            return PracticeRepository.PatientInsurances.Any(i => i.InsuredPatientId == patientId && i.InsuranceType == insuranceType && InsuranceQueries.IsActiveForDateTime.Invoke(i, dateTime) && i.InsurancePolicy.Insurer.IsReferralRequired);
        }
    }

    public class AppointmentChangeViewModelMap : IMap<KeyValuePair<Appointment, IEnumerable<AuditEntry>>, List<AppointmentChangeViewModel>>
    {
        private readonly IEnumerable<IMapMember<KeyValuePair<Appointment, IEnumerable<AuditEntry>>, List<AppointmentChangeViewModel>>> _members;
        private readonly Dictionary<int, User> _users;
        private readonly Dictionary<int, ServiceLocation> _serviceLocations;
        private readonly Dictionary<int, EncounterStatusChangeReason> _changeReasons;
        private readonly Dictionary<int, AppointmentType> _appointmentTypes;

        public IEnumerable<IMapMember<KeyValuePair<Appointment, IEnumerable<AuditEntry>>, List<AppointmentChangeViewModel>>> Members
        {
            get { return _members; }
        }

        public AppointmentChangeViewModelMap(IEnumerable<User> users, IEnumerable<ServiceLocation> serviceLocations, IEnumerable<EncounterStatusChangeReason> changeReasons,
            IEnumerable<AppointmentType> appointmentTypes)
        {
            _changeReasons = changeReasons.ToDictionary(i => i.Id);
            _users = users.ToDictionary(i => i.Id);
            _serviceLocations = serviceLocations.ToDictionary(i => i.Id);
            _appointmentTypes = appointmentTypes.ToDictionary(i => i.Id);
            _members = this.CreateMembers((source, target) => CreateAppointmentChangeViewModels(source.Key, source.Value.ToArray(), target), "AppointmentChangeViewModel");
        }


        private void CreateAppointmentChangeViewModels(Appointment appointment, AuditEntry[] auditEntries, List<AppointmentChangeViewModel> results)
        {
            #region helper func getDisplayValue - Calls Enum.GetDisplayName or ToString

            Func<object, string> getDisplayValue = value => (value is Enum) ? ((Enum)value).GetDisplayName() : value.ToString();

            #endregion

            var encounterAuditEntries = auditEntries.Where(a => a.IsAuditEntryFor(typeof(Encounter))).ToArray();

            var changes = auditEntries.Where(a => a.ChangeType == AuditEntryChangeType.Update).SelectMany(a => a.AuditEntryChanges).ToArray();

            var insertAuditEntry = encounterAuditEntries.FirstOrDefault(e => e.ChangeType == AuditEntryChangeType.Insert);
            if (insertAuditEntry != null)
            {
                var insertViewModel = new AppointmentChangeViewModel
                {
                    AppointmentId = appointment.Id,
                    IsInsertChange = true,
                    UserName = _users.GetValue(insertAuditEntry.UserId.GetValueOrDefault()).IfNotNull(u => u.UserName),
                    DateTime = insertAuditEntry.AuditDateTime.ToClientTime()
                };
                results.Add(insertViewModel);
            }

            foreach (var change in changes)
            {
                var target = change.AuditEntry.IsAuditEntryFor(typeof(Appointment)) ? appointment as object : appointment.Encounter;

                if (!IsTracked(change)) continue;

                var viewModel = new AppointmentChangeViewModel
                {
                    AppointmentId = appointment.Id,
                    UserName = _users.GetValue(change.AuditEntry.UserId.GetValueOrDefault()).IfNotNull(u => u.UserName),
                    DateTime = change.AuditEntry.AuditDateTime.ToClientTime(),
                    Field = change.GetDisplayName(),
                    ChangedFrom = GetDisplayValue(change.ColumnName, change.OldValue, true) ?? change.GetOldValue(target).IfNotNull(getDisplayValue, () => ""),
                    ChangedTo = GetDisplayValue(change.ColumnName, change.GetNewValue(changes, target).IfNotNull(getDisplayValue, () => null), false) ?? change.GetNewValue(changes, target).IfNotNull(getDisplayValue, () => "")
                };

                if (!(viewModel.ChangedTo == "" && viewModel.ChangedFrom.IsNullOrEmpty()) || !(viewModel.ChangedTo.IsNullOrEmpty() && viewModel.ChangedFrom == ""))
                    results.Add(viewModel);
            }
        }

        private string GetDisplayValue(string column, string value, bool getOldValue)
        {
            if (value == null) return string.Empty;

            if (column == Reflector.MemberPath<Appointment>(a => a.DateTime))
            {
                return DateTime.Parse(value).ToString("M/d/yyyy h:mm tt");
            }

            if (column == Reflector.MemberPath<UserAppointment>(a => a.UserId))
            {
                return _users.GetValue(value.ToInt().GetValueOrDefault()).As<User>().IfNotNull(u => u.UserName);
            }

            if (column == Reflector.MemberPath<Encounter>(a => a.ServiceLocationId))
            {
                return _serviceLocations.GetValue(value.ToInt().GetValueOrDefault()).IfNotNull(sl => sl.ShortName);
            }

            if (column == Reflector.MemberPath<Encounter>(a => a.EncounterStatusChangeReasonId))
            {
                return getOldValue ? string.Empty : _changeReasons.GetValue(value.ToInt().GetValueOrDefault()).IfNotNull(i => i.Value);
            }

            if (column == Reflector.MemberPath<Appointment>(a => a.AppointmentTypeId))
            {
                return _appointmentTypes.GetValue(value.ToInt().GetValueOrDefault()).IfNotNull(at => at.Name);
            }

            return null;
        }

        private static readonly string[] TrackedColumns =
            {
                Reflector.MemberPath<Appointment>(a => a.DateTime), Reflector.MemberPath<UserAppointment>(a => a.UserId), Reflector.MemberPath<Encounter>(a => a.ServiceLocationId),
                Reflector.MemberPath<Encounter>(a => a.EncounterStatusId), Reflector.MemberPath<Encounter>(a => a.EncounterStatusChangeReasonId), Reflector.MemberPath<Encounter>(a => a.EncounterStatusChangeComment),
                Reflector.MemberPath<Appointment>(a => a.AppointmentTypeId), Reflector.MemberPath<Encounter>(a => a.InsuranceType) + "Id", Reflector.MemberPath<Appointment>(a => a.Comment)
            };

        private static bool IsTracked(AuditEntryChange change)
        {
            return TrackedColumns.Contains(change.ColumnName);
        }
    }


    public class PatientAppointmentViewModelMap : IMap<UserAppointment, PatientAppointmentViewModel>
    {
        private readonly IEnumerable<IMapMember<UserAppointment, PatientAppointmentViewModel>> _members;

        public PatientAppointmentViewModelMap(IEnumerable<EncounterStatusConfiguration> encounterStatusConfigurations)
        {
            Dictionary<int, EncounterStatusConfiguration> statusLookup = encounterStatusConfigurations.ToDictionary(c => c.Id);
            _members = this.CreateMembers(a => new PatientAppointmentViewModel
            {
                AppointmentId = a.Id,
                DateTime = a.DateTime,
                EncounterId = a.EncounterId,
                EncounterStatus = new ColoredViewModel
                    {
                        Id = a.Encounter.EncounterStatusId,
                        Color = statusLookup.GetValue((int)a.Encounter.EncounterStatus).IfNotNull(s => s.Color),
                        Name = statusLookup.GetValue((int)a.Encounter.EncounterStatus).IfNotNull(s => s.Name)
                    },
                AppointmentInsuranceType = a.Encounter.InsuranceType,
                AppointmentLocation = new NamedViewModel<int> { Id = a.Encounter.ServiceLocation.Id, Name = a.Encounter.ServiceLocation.ShortName },
                Comments = a.Comment,
                Resource = a.User.UserName,
                ResourceId = a.ResourceId,
                AppointmentType = new AppointmentTypeViewModel { Id = a.AppointmentType.Id, Name = a.AppointmentType.Name, CategoryId = a.AppointmentType.AppointmentCategoryId },
                IsReferralInformationRequired = a.Encounter.Patient.PatientInsurances.Any(i => i.InsuranceType == a.Encounter.InsuranceType && i.IsActiveForDateTime(a.DateTime) && i.InsurancePolicy.Insurer.IsReferralRequired),
                ConfirmationStatus = (a.Encounter.ConfirmationStatusId.HasValue) ? new NamedViewModel { Id = a.Encounter.ConfirmationStatus.Id, Name = a.Encounter.ConfirmationStatus.Description } : null
            }).ToArray();
        }

        public IEnumerable<IMapMember<UserAppointment, PatientAppointmentViewModel>> Members
        {
            get { return _members; }
        }
    }


}


