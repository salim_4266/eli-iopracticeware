﻿using System.ComponentModel.DataAnnotations;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientDemographics;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance;
using IO.Practiceware.Presentation.ViewServices.PatientInfo;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

[assembly: Component(typeof(PatientInfoViewService), typeof(IPatientInfoViewService))]
[assembly: Component(typeof(PatientInfoViewModelMap), typeof(IMap<Patient, PatientInfoViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.PatientInfo
{
    public interface IPatientInfoViewService
    {
        PatientInfoViewModel LoadPatientInfo(int patientId);

        bool IsClinical(int patientId);

        /// <summary>
        /// Checks for pending alerts for specified patient
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="alertType"></param>
        /// <returns></returns>
        bool CheckHasPendingAlert(int patientId, int alertType);
    }

    public class PatientInfoViewService : BaseViewService, IPatientInfoViewService
    {
        private readonly IMapper<Patient, PatientInfoViewModel> _patientInfoMapper;

        private static readonly IMapper<Model.PatientInsurance, PolicyViewModel> InsuranceMapper =
            Mapper.Factory.CreateMapper<Model.PatientInsurance, PolicyViewModel>(pi => new PolicyViewModel
            {
                InsuranceType = pi.InsuranceType,
                Copay = pi.InsurancePolicy.Copay,
                InsurancePlan = new InsurancePlanViewModel
                {
                    InsuranceName = string.Format("{0}{1}", pi.InsurancePolicy.Insurer.Name, pi.InsurancePolicy.Insurer.PlanName.IsNullOrWhiteSpace() || pi.InsurancePolicy.Insurer.PlanName.Equals(pi.InsurancePolicy.Insurer.Name, StringComparison.OrdinalIgnoreCase) ? string.Empty : " - " + pi.InsurancePolicy.Insurer.PlanName),
                    PlanType = pi.InsurancePolicy.Insurer.InsurerPlanType.IfNotNull(x => x.Name),
                    DiagnosisType = pi.InsurancePolicy.Insurer.DiagnosisTypeId == null ? ((DiagnosisTypeId)Convert.ToInt32(ApplicationSettings.Cached.GetSetting<string>(ApplicationSetting.DefaultDiagnosisTypeId).IfNotNull(x => x.Value))).GetDisplayName(): ((DiagnosisTypeId)pi.InsurancePolicy.Insurer.DiagnosisTypeId).GetDisplayName(),
                    Id = pi.InsurancePolicy.Insurer.Id
                },
                PolicyRank = pi.OrdinalId,
                InsurerClaimForm = pi.InsurancePolicy.Insurer.InsurerClaimForms.Select(x=> new InsurerClaimFormViewModel
                {
                    InvoiceType = new NamedViewModel{
                       Id = (int)x.InvoiceType,
                       Name = x.InvoiceType.GetDisplayName()
                    },
                    InsurerId = x.InsurerId                    
                }).ToObservableCollection(),
                
                Relationship = pi.PolicyholderRelationshipType
            });


        public PatientInfoViewService(IMapper<Patient, PatientInfoViewModel> patientInfoMapper)
        {
            _patientInfoMapper = patientInfoMapper;
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public virtual PatientInfoViewModel LoadPatientInfo(int patientId)
        {
            var patient = PracticeRepository.Patients.Single(x => x.Id == patientId);

            PracticeRepository.AsQueryableFactory().Load(patient,
                x => x.PatientTags.Select(y => y.Tag),
                x => x.PatientPhoneNumbers,
                x => x.PatientComments,
                x => x.Encounters.SelectMany(y => y.Invoices.SelectMany(z => z.InvoiceReceivables).SelectMany(t => t.BillingServiceTransactions)),
                x => x.PatientInsurances.Where(y => !y.IsDeleted).Select(z => z.InsurancePolicy.Insurer.InsurerPlanType),
                x => x.PatientInsurances.Where(y => !y.IsDeleted).Select(z => z.InsurancePolicy.Insurer.InsurerClaimForms)
                );


            patient.PatientInsurances = patient.PatientInsurances.Where(x => x.IsActiveForDateTime(DateTime.Now.ToClientTime())).ToList();

            var viewModel = _patientInfoMapper.Map(patient);           
            viewModel.Insurances = InsuranceMapper.MapAll(patient.PatientInsurances).OrderBy(p => p.InsuranceType.GetAttribute<DisplayAttribute>().Order).ThenBy(p => p.PolicyRank).ToObservableCollection();
            viewModel.Insurances.OrderByInsuranceTypeThenRank().ApplyIndexToRank();

            return viewModel;
        }

        public bool IsClinical(int patientId)
        {
            return PracticeRepository.Patients.WithId(patientId).IfNotNull(p => p.IsClinical);
        }

        public virtual bool CheckHasPendingAlert(int patientId, int alertType)
        {
            var hasPendingAlert = PracticeRepository.CheckPatientHasPendingAlert(patientId, alertType);
            return hasPendingAlert.Any(hasAlert => hasAlert.GetValueOrDefault(false));
        }
    }

    public class PatientInfoViewModelMap : IMap<Patient, PatientInfoViewModel>
    {
        private readonly IEnumerable<IMapMember<Patient, PatientInfoViewModel>> _members;

        public PatientInfoViewModelMap()
        {
            _members = this.CreateMembers(patient => new PatientInfoViewModel
            {
                PatientId = patient.Id,
                Name = patient.DisplayName,
                BirthDate = patient.DateOfBirth,
                Gender = patient.Gender.ToString(),
                Tags = patient.PatientTags.Select(t => new PatientTagViewModel { Id = t.Id, Name = t.Tag.DisplayName, TagId = t.TagId, HexColor = t.Tag.HexColor, IsPrimary = t == t.Patient.PatientTags.OrderBy(i => i.OrdinalId).First() }).ToObservableCollection(),
                CoPay = patient.PatientInsurances.OrderBy(x => x.InsuranceType).ThenBy(x => x.OrdinalId).FirstOrDefault().IfNotNull(y => y.InsurancePolicy.Copay, () => null),
                Comments = patient.PatientComments.Where(c => c.PatientCommentType == PatientCommentType.PatientDemographicSummary).Select(c => c.Value).Join(Environment.NewLine, true) ?? string.Empty,
                WorkPhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business, NotFoundBehavior.None) != null ? patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business, NotFoundBehavior.None).ToString() : string.Empty,
                CellPhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell, NotFoundBehavior.None) != null ? patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell, NotFoundBehavior.None).ToString() : string.Empty,
                HomePhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home, NotFoundBehavior.None) != null ? patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home, NotFoundBehavior.None).ToString() : string.Empty,
                Balance = patient.Encounters.SelectMany(x => x.Invoices).SelectMany(x => x.InvoiceReceivables).GetPatientBalance(),
                IsClinical = patient.IsClinical
            }).ToArray();
        }

        public IEnumerable<IMapMember<Patient, PatientInfoViewModel>> Members
        {
            get { return _members; }
        }
    }

}
