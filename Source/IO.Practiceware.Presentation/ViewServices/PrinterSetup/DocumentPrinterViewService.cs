﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PrinterSetup;
using IO.Practiceware.Presentation.ViewServices.PrinterSetup;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

[assembly: Component(typeof(DocumentPrinterViewService), typeof(IDocumentPrinterViewService))]

namespace IO.Practiceware.Presentation.ViewServices.PrinterSetup
{
    public interface IDocumentPrinterViewService
    {
        /// <summary>
        /// Retrieves all the Document Types
        /// </summary>
        /// <returns></returns>
        IEnumerable<NamedViewModel> GetDocumentTypes();

        /// <summary>
        /// Gets Templates of specified Document Type 
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentViewModel> GetTemplates(int? documentTypeId = null);

        /// <summary>
        /// Retrieves IO Printers
        /// </summary>
        /// <returns></returns>
        IEnumerable<PrinterViewModel> GetIoPrinters();


        /// <summary>
        /// Saves the document printers.
        /// </summary>
        /// <param name="documentTypeId">The document type id.</param>
        /// <param name="documentId">The document id.</param>
        /// <param name="printerId">The printer id.</param>
        /// <param name="serviceLocationShortName">Short name of the site.</param>
        void SaveDocumentPrinters(int documentTypeId, int documentId, int printerId, string serviceLocationShortName);
    }

    public class DocumentPrinterViewService : BaseViewService, IDocumentPrinterViewService
    {        
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;

        public DocumentPrinterViewService( IUnitOfWorkProvider unitOfWorkProvider)
        {
            
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        #region IDocumentPrinterViewService Members

        public IEnumerable<NamedViewModel> GetDocumentTypes()
        {
            return
                Enums.GetValues<TemplateDocumentCategory>().ToList().Select(
                    d =>
                    new NamedViewModel { Id = (int)d, Name = d.GetAttribute<DisplayAttribute>().Name }).Where(
                            d => d.Name != null).OrderBy(d => d.Name).ToArray();
        }

        public IEnumerable<DocumentViewModel> GetTemplates(int? documentTypeId)
        {
            IEnumerable<TemplateDocument> templates =
                PracticeRepository.TemplateDocuments.Where(d => documentTypeId == null || d.TemplateDocumentCategory == (TemplateDocumentCategory)documentTypeId.Value).OrderBy(d => d.Name).ToList();
            if (!templates.Any()) return new List<DocumentViewModel>();
            IEnumerable<DocumentViewModel> templateViewModels =
                new List<DocumentViewModel> { new DocumentViewModel { Id = 0, Name = "Apply to All Templates", DisplayName = "Apply to All Templates" } }.
                    Union(templates
                              .Select(t => new DocumentViewModel { Id = t.Id, Name = t.Name, DisplayName = t.DisplayName }).Where(
                                  t => t.Name != null).
                              ToArray()).ToArray();

            return templateViewModels;
        }

        public IEnumerable<PrinterViewModel> GetIoPrinters()
        {
            IEnumerable<Printer> printers = PracticeRepository.Printers.Include(p => p.ServiceLocation).OrderBy(p => p.Name).ToList();
            IEnumerable<PrinterViewModel> ioPrinters =
                new List<PrinterViewModel> { new PrinterViewModel { Id = 0, Name = "Remove associated printer", UncPath ="", SiteName ="" } }.
                    Union(printers
                              .Select(p => new PrinterViewModel { Id = p.Id, Name = p.Name, UncPath = p.UncPath, SiteName = p.ServiceLocation.ShortName }).Where(
                                  p => p.Name != null).
                              ToArray()).ToArray();

            return ioPrinters;
        }


        public void SaveDocumentPrinters(int documentTypeId, int documentId, int printerId, string serviceLocationShortName)
        {
            if (documentId > 0)
            {
                if (printerId > 0) AddDocumentPrinter(documentId, printerId);
                else RemoveDocumentPrinters(documentId, serviceLocationShortName);
            }
            else
            {
                IEnumerable<TemplateDocument> documents =
                    PracticeRepository.TemplateDocuments.Where(d => d.TemplateDocumentCategory == (TemplateDocumentCategory)documentTypeId).OrderBy(d => d.Name).
                        ToList();
                if (printerId > 0)
                    foreach (TemplateDocument document in documents)
                        AddDocumentPrinter(document.Id, printerId);
                else
                    foreach (TemplateDocument document in documents)
                        RemoveDocumentPrinters(document.Id, serviceLocationShortName);
            }
        }

        #endregion

        private void RemoveDocumentPrinters(int documentId, string serviceLocationShortName)
        {
            using (IUnitOfWork work = _unitOfWorkProvider.Create())
            {
                TemplateDocument document =
                    PracticeRepository.TemplateDocuments.Include(
                        d => d.Printers.Select(p => p.ServiceLocation)).WithId(documentId);
                if (document != null && document.Printers.Count > 0)
                {
                    document.Printers.Where(p => p.ServiceLocation.ShortName.Equals(serviceLocationShortName, System.StringComparison.OrdinalIgnoreCase)).ToList().ForEach(
                        p => document.Printers.Remove(p));
                }
                work.AcceptChanges();
            }
        }

        private void RemoveExistingDocumentPrinter(int documentId, int printerId)
        {
            using (IUnitOfWork work = _unitOfWorkProvider.Create())
            {
                TemplateDocument document =
                    PracticeRepository.TemplateDocuments.Include(
                        d => d.Printers).WithId(documentId);
                Printer printer =
                    PracticeRepository.Printers.Include(p => p.TemplateDocuments).WithId(printerId);
                if (document != null && printer != null && document.Printers.Count > 0)
                {
                    document.Printers.Where(p => p.ServiceLocationId == printer.ServiceLocationId).ToList().ForEach(
                        p => document.Printers.Remove(p));
                }
                work.AcceptChanges();
            }
        }

        private void AddDocumentPrinter(int documentId, int printerId)
        {
            RemoveExistingDocumentPrinter(documentId, printerId);
            using (IUnitOfWork work = _unitOfWorkProvider.Create())
            {
                TemplateDocument document = PracticeRepository.TemplateDocuments.WithId(documentId);
                Printer printer =
                    PracticeRepository.Printers.Include(p => p.TemplateDocuments).WithId(printerId);
                if (printer != null && document != null)
                {
                    printer.TemplateDocuments.Add(document);
                    PracticeRepository.Save(printer);
                }
                work.AcceptChanges();
            }
        }
    }
}