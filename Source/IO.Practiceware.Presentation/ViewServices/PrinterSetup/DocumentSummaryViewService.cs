﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PrinterSetup;
using IO.Practiceware.Presentation.ViewServices.PrinterSetup;
using Soaf;
using Soaf.ComponentModel;
using System.Collections.Generic;
using System.Linq;

[assembly: Component(typeof(DocumentSummaryViewService), typeof(IDocumentSummaryViewService))]

namespace IO.Practiceware.Presentation.ViewServices.PrinterSetup
{
    public interface IDocumentSummaryViewService
    {
        /// <summary>
        /// Gets all documents which have associated printer
        /// </summary>
        /// <returns>Document Printers</returns>
        IEnumerable<DocumentPrinterViewModel> GetDocumentPrinters(string serviceLocationShortName);
    }

    internal class DocumentSummaryViewService : BaseViewService, IDocumentSummaryViewService
    {         
        #region IDocumentSummaryViewService Members

        public IEnumerable<DocumentPrinterViewModel> GetDocumentPrinters(string serviceLocationShortName)
        {
            if (serviceLocationShortName.IsNullOrEmpty()) return new DocumentPrinterViewModel[0];

            var results = (from document in PracticeRepository.TemplateDocuments
                           from printer in document.Printers
                           where printer.ServiceLocation.ShortName == serviceLocationShortName || serviceLocationShortName == null || serviceLocationShortName == ""
                           select new
                                      {
                                          document.TemplateDocumentCategoryId,
                                          DocumentName = document.Name,
                                          PrinterName = printer.Name
                                      }).AsEnumerable()
                .Select(i => new DocumentPrinterViewModel
                                 {
                                     DocumentTypeName = ((TemplateDocumentCategory)i.TemplateDocumentCategoryId).GetDisplayName(),
                                     DocumentName = i.DocumentName,
                                     PrinterName = i.PrinterName
                                 }).OrderBy(i => i.DocumentTypeName).ThenBy(i => i.DocumentName).ToArray();

            return results;
        }

        #endregion
    }
}