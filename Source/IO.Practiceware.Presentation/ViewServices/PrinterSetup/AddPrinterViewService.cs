﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PrinterSetup;
using IO.Practiceware.Presentation.ViewServices.PrinterSetup;
using Soaf.ComponentModel;
using System.Collections.Generic;
using System.Linq;

[assembly: Component(typeof (AddPrinterViewService), typeof (IAddPrinterViewService))]

namespace IO.Practiceware.Presentation.ViewServices.PrinterSetup
{
    public interface IAddPrinterViewService
    {
        /// <summary>
        /// Gets the sites.
        /// </summary>
        /// <returns></returns>
        IEnumerable<NamedViewModel> GetServiceLocations();

        /// <summary>
        /// Saving Printer Details
        /// </summary>
        /// <param name="printerName"> IO printer name</param>
        /// <param name="uncPath">Printer physical path</param>
        /// <param name="serviceLocationId">Service Location Id</param>
        void SavePrinterDetails(string printerName, string uncPath, int serviceLocationId);

        /// <summary>
        /// Gets the IO Printers.
        /// </summary>
        /// <returns></returns>
        IEnumerable<PrinterViewModel> GetIoPrinters();
    }

    public class AddPrinterViewService : BaseViewService, IAddPrinterViewService
    {        
        #region IAddPrinterViewService Members

        public IEnumerable<NamedViewModel> GetServiceLocations()
        {
            return PracticeRepository.ServiceLocations.Select(l => new NamedViewModel {Id = l.Id, Name = l.ShortName}).ToArray();
        }

        /// <summary>
        /// Saving Printer Details
        /// </summary>
        /// <param name="printerName"></param>
        /// <param name="uncPath"></param>
        /// <param name="serviceLocationId"></param>
        /// <returns></returns>
        public void SavePrinterDetails(string printerName, string uncPath, int serviceLocationId)
        {
            var printer = new IO.Practiceware.Model.Printer {ServiceLocationId = serviceLocationId, Name = printerName, UncPath = uncPath};
            PracticeRepository.Save(printer);
        }

        public IEnumerable<PrinterViewModel> GetIoPrinters()
        {
            return PracticeRepository.Printers.Select(p => new PrinterViewModel {Id = p.Id, Name = p.Name, UncPath = p.UncPath}).ToArray();
        }

        #endregion
    }
}