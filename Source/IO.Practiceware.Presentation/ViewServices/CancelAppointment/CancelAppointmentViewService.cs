﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.CancelAppointment;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewServices.CancelAppointment;
using IO.Practiceware.Presentation.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

[assembly: Component(typeof(CancelAppointmentViewService), typeof(ICancelAppointmentViewService))]
[assembly: Component(typeof(CancelAppointmentViewModelMap), typeof(IMap<UserAppointment, CancelAppointmentViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.CancelAppointment
{
    public interface ICancelAppointmentViewService
    {
        List<EncounterStatusChangeReasonViewModel> LoadEncounterStatusChangeReasons();

        /// <summary>
        /// Loads the appointment cancellation types.
        /// </summary>
        /// <returns></returns>
        ObservableCollection<NamedViewModel> LoadAppointmentCancellationTypes();

        /// <summary>
        /// Cancels the specified appointments.
        /// </summary>
        /// <param name="appointmentIds">The appointment ids.</param>
        /// <param name="appointmentCancellationTypeId">The appointment cancellation type id.</param>
        /// <param name="changeReason">The change reason.</param>
        /// <param name="comment">The comment.</param>
        void CancelAppointments(List<int> appointmentIds, int appointmentCancellationTypeId, EncounterStatusChangeReasonViewModel changeReason, string comment);

        /// <summary>
        /// Retrieve the specified appointments
        /// </summary>
        /// <param name="appointmentIds">Ids of appointments to get</param>
        /// <returns>List of appointments to be canceled</returns>
        ObservableCollection<CancelAppointmentViewModel> GetAppointments(List<int> appointmentIds);
    }

    [SupportsUnitOfWork]
    public class CancelAppointmentViewService : BaseViewService, ICancelAppointmentViewService
    {
        private readonly IMapper<List<UserAppointment>, List<CancelAppointmentViewModel>> _appointmentToCancelAppointmentMapper;

        public CancelAppointmentViewService(IMapper<List<UserAppointment>, List<CancelAppointmentViewModel>> appointmentToCancelAppointmentMapper)
        {

            _appointmentToCancelAppointmentMapper = appointmentToCancelAppointmentMapper;
        }

        [UnitOfWork(AcceptChanges = true)]
        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public virtual void CancelAppointments(List<int> appointmentIds, int appointmentCancellationTypeId, EncounterStatusChangeReasonViewModel changeReason, string comment)
        {
            Appointment[] appointments = PracticeRepository.Appointments.Include(a => a.Encounter.PatientInsuranceReferrals, a => a.ScheduleBlockAppointmentCategories).Where(a => appointmentIds.Contains(a.Id)).ToArray();

            foreach (Appointment appointment in appointments)
            {
                appointment.Encounter.EncounterStatusChangeComment = comment;
                appointment.Encounter.EncounterStatusChangeReasonId = changeReason != null ? changeReason.Id : new int?();
                appointment.Encounter.EncounterStatus = (EncounterStatus)appointmentCancellationTypeId;
                appointment.ScheduleBlockAppointmentCategories.Clear();
                appointment.Encounter.PatientInsuranceReferrals.Clear();

                try
                {
                    var isPortalActive = PracticeRepository.ApplicationSettings.Where(a => a.Name == "IsPortalActive").SingleOrDefault().Value.ToString();
                    if (isPortalActive.ToLower() == "true")
                    {
                        var prtSrv = new PortalServices();
                        AppointmentEntity objTemp = new AppointmentEntity()
                        {
                            PracticeToken = PracticeRepository.ApplicationSettings.Where(a => a.Name == "PracticeAuthToken").SingleOrDefault().Value.ToString(),
                            PatientExternalId = appointment.Encounter.PatientId.ToString(),
                            LocationExternalId = appointment.Encounter.ServiceLocationId.ToString(),
                            DoctorExternalId = appointment.ResourceId.ToString(),
                            AppointmentStatusExternalId = appointment.Encounter.EncounterStatusId.ToString(),
                            Start = appointment.Encounter.StartDateTime,
                            End = appointment.Encounter.EndDateTime,
                            ExternalId = appointment.Id.ToString()
                        };
                        prtSrv.DeleteAppointment(objTemp);
                    }
                }
                catch { }
            }
        }

        public List<EncounterStatusChangeReasonViewModel> LoadEncounterStatusChangeReasons()
        {
            return PracticeRepository.EncounterStatusChangeReasons.Select(r => new EncounterStatusChangeReasonViewModel {Id = r.Id, Value = r.Value, EncounterStatusId = r.EncounterStatusId}).ToList();
        }

        public ObservableCollection<NamedViewModel> LoadAppointmentCancellationTypes()
        {
            return Enums.GetValues<EncounterStatus>().Where(i => (int) i >= (int) EncounterStatus.CancelOffice && (int) i <= (int) EncounterStatus.CancelOther).Select(i => new NamedViewModel((int) i, i.GetDisplayName())).ToExtendedObservableCollection();
        }

        public ObservableCollection<CancelAppointmentViewModel> GetAppointments(List<int> appointmentIds)
        {
            List<UserAppointment> appointments = PracticeRepository.Appointments.OfType<UserAppointment>().Where(a => appointmentIds.Contains(a.Id)).ToList();

            PracticeRepository.AsQueryableFactory().Load(appointments, a => a.Encounter.Patient);

            return _appointmentToCancelAppointmentMapper.Map(appointments).ToExtendedObservableCollection();
        }
    }

    public class CancelAppointmentViewModelMap : IMap<UserAppointment, CancelAppointmentViewModel>
    {
        private readonly IEnumerable<IMapMember<UserAppointment, CancelAppointmentViewModel>> _members;

        public CancelAppointmentViewModelMap()
        {

            _members = this.CreateMembers(appointment => new CancelAppointmentViewModel
                {
                    Id = appointment.Id,
                    StartDateTime = appointment.DateTime,
                    PatientName = appointment.Encounter.Patient.DisplayName,
                    IsCancelled = appointment.Encounter.EncounterStatus.IsCancelledStatus(),
                    EncounterStatusId = (int)appointment.Encounter.EncounterStatus,
                    EncounterStatusChangedReasonId = appointment.Encounter.EncounterStatusChangeReasonId,
                    EncounterStatusChangedComment = appointment.Encounter.EncounterStatusChangeComment
                }).ToArray();
        }

        #region IMap<Appointment,AppointmentViewModel> Members

        public IEnumerable<IMapMember<UserAppointment, CancelAppointmentViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }
}