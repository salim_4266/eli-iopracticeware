﻿using System.ComponentModel.DataAnnotations;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorization;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.ViewServices.PatientInsuranceAuthorization;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

[assembly: Component(typeof(PatientInsuranceAuthorizationsViewService), typeof(IPatientInsuranceAuthorizationsViewService))]
[assembly: Component(typeof(PatientInsuranceAuthorizationViewModelMap), typeof(IMap<PatientInsuranceAuthorization, PatientInsuranceAuthorizationViewModel>))]
namespace IO.Practiceware.Presentation.ViewServices.PatientInsuranceAuthorization
{
    public class PatientInsuranceAuthorizationsViewService : BaseViewService, IPatientInsuranceAuthorizationsViewService
    {
        private readonly IMapper<Model.PatientInsuranceAuthorization, PatientInsuranceAuthorizationViewModel> _mapper;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;

        public PatientInsuranceAuthorizationsViewService(IMapper<Model.PatientInsuranceAuthorization, PatientInsuranceAuthorizationViewModel> mapper,
            IUnitOfWorkProvider unitOfWorkProvider)
        {

            _mapper = mapper;
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        public PatientAppointmentInfo LoadPatientAppointmentInfo(int? encounterId)
        {
            var encounter = PracticeRepository.Encounters.WithId(encounterId);

            if (encounter == null)
            {
                return null;
            }

            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(encounter,
                                  x => x.Patient,
                                  x => x.Patient.PatientTags.Select(y => y.Tag),
                                  x => x.Patient.PatientPhoneNumbers,
                                  x => x.Patient.PatientInsurances.Select(a => a.InsurancePolicy.Insurer),
                                  x => x.EncounterType,
                                  x => x.ServiceLocation,
                                  x => x.Appointments.OfType<UserAppointment>().Select(a => a.User),
                                  x => x.Appointments.Select(a => a.AppointmentType));

            var insurances = new ObservableCollection<NamedViewModel<int>>();
            if (encounter.Patient.PatientInsurances.Any())
            {
                insurances = new ObservableCollection<NamedViewModel<int>>(encounter.Patient.PatientInsurances.Where(x => x.IsActiveForDateTime(encounter.StartDateTime)).OrderBy(x => x.InsuranceType.GetAttribute<DisplayAttribute>().Order).Select(x => new NamedViewModel<int> { Id = x.Id, Name = x.InsurancePolicy.Insurer.Name }));
            }

            var viewModel = new PatientAppointmentInfo
                            {
                                PatientId = encounter.PatientId.ToString(),
                                PatientType = encounter.Patient.PatientTags.Any() ? encounter.Patient.PatientTags.Join(x => x.Tag.DisplayName) : string.Empty,
                                PatientName = encounter.Patient.DisplayName,
                                DateOfBirth = encounter.Patient.DateOfBirth.ToMMDDYYYYString(),
                                WorkPhone = encounter.Patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business) != null ? encounter.Patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business).ToString() : string.Empty,
                                CellPhone = encounter.Patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell) != null ? encounter.Patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell).ToString() : string.Empty,
                                HomePhone = encounter.Patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home) != null ? encounter.Patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home).ToString() : string.Empty,
                                AppointmentDate = encounter.Appointments.Any() ? encounter.Appointments.First().DateTime.ToMMDDYYYYString() : string.Empty,
                                AppointmentType = encounter.Appointments.Any() ? encounter.Appointments.First().AppointmentType.Name : string.Empty,
                                Resource = encounter.Appointments.Any() ? encounter.Appointments.First().ResourceName : string.Empty,
                                Location = encounter.ServiceLocation.IfNotNull(x => x.ShortName, ""),
                                Insurances = insurances
                            };

            return viewModel;
        }

        public PatientInsuranceAuthorizationViewModel LoadPatientInsuranceAuthorization(long patientInsuranceAuthorizationId)
        {
            var authorization = PracticeRepository.PatientInsuranceAuthorizations
                .WithId(patientInsuranceAuthorizationId);

            var viewModel = _mapper.Map(authorization);

            return viewModel;
        }

        public int Save(PatientInsuranceAuthorizationViewModel viewModel)
        {
            using (var work = _unitOfWorkProvider.Create())
            {
                Model.PatientInsuranceAuthorization patientInsuranceAuthorization;
                if (viewModel.Id.HasValue)
                {
                    patientInsuranceAuthorization = PracticeRepository.PatientInsuranceAuthorizations.Include(i => i.InvoiceReceivables)
                                                                                                      .WithId(viewModel.Id.Value);
                }
                else
                {
                    patientInsuranceAuthorization = new Model.PatientInsuranceAuthorization();
                }

                MapViewModelToPatientInsuranceAuthorization(viewModel, patientInsuranceAuthorization);

                if (viewModel.InvoiceId.HasValue)
                {
                    var invoiceReceivableIdsOnAuth = patientInsuranceAuthorization.InvoiceReceivables.Select(x => x.Id).ToList();
                    var invoiceReceivables = PracticeRepository.InvoiceReceivables.Where(x => x.InvoiceId == viewModel.InvoiceId)
                                                                .Include(x => x.PatientInsuranceAuthorization).ToList();

                    if (invoiceReceivables.All(x => x.PatientInsuranceId != patientInsuranceAuthorization.PatientInsuranceId))
                    {
                        var patientInsurance = PracticeRepository.PatientInsurances.First(x => x.Id == patientInsuranceAuthorization.PatientInsuranceId);
                        var invoiceReceivable = CreateInvoiceReceivable(viewModel.InvoiceId.Value, patientInsurance, patientInsuranceAuthorization);
                        invoiceReceivables.Add(invoiceReceivable);
                    }

                    invoiceReceivables.ForEach(x =>
                        {
                            if (x.PatientInsuranceId == patientInsuranceAuthorization.PatientInsuranceId)
                            {
                                var existingAuth = x.PatientInsuranceAuthorization;
                                if (!invoiceReceivableIdsOnAuth.Contains(x.Id))
                                {
                                    patientInsuranceAuthorization.InvoiceReceivables.Add(x);
                                }
                                if (existingAuth == null)
                                {
                                    x.PatientInsuranceAuthorization = patientInsuranceAuthorization;
                                }
                            }
                            else if (x.PatientInsuranceAuthorizationId == patientInsuranceAuthorization.Id)
                            {
                                x.PatientInsuranceAuthorizationId = null;
                            }
                        });
                }
                else // incase patient does not have invoice, no object state entry has been changed and we have to call save explicitly.
                {
                    PracticeRepository.Save(patientInsuranceAuthorization);
                }
                work.AcceptChanges();

                return patientInsuranceAuthorization.Id;
            }
        }

        private static InvoiceReceivable CreateInvoiceReceivable(int invoiceId, PatientInsurance patientInsurance, Model.PatientInsuranceAuthorization patientInsuranceAuthorization)
        {
            var invoiceReceivable = new InvoiceReceivable
            {
                InvoiceId = invoiceId,
                PatientInsurance = patientInsurance,
                OpenForReview = false,//Open for review will always be false here as invoice already has open for review to true for one of the invoice receivable already attached to it.
                PatientInsuranceAuthorization = patientInsuranceAuthorization
            };
            return invoiceReceivable;
        }

        private static void MapViewModelToPatientInsuranceAuthorization(PatientInsuranceAuthorizationViewModel source, Model.PatientInsuranceAuthorization destination)
        {
            destination.AuthorizationCode = source.Code;
            destination.EncounterId = source.EncounterId;
            destination.Comment = source.Comments;
            if (source.PatientInsuranceId != 0) destination.PatientInsuranceId = source.PatientInsuranceId;
        }
    }

    public interface IPatientInsuranceAuthorizationsViewService
    {
        /// <summary>
        /// Gets the Patient and Appointment information for the authorization display
        /// </summary>
        /// <param name="encounterId">The Id of the encounter to load</param>        
        /// <returns>View model with patient information populated</returns>
        PatientAppointmentInfo LoadPatientAppointmentInfo(int? encounterId);

        /// <summary>
        /// Gets the Patient Insurance Authorization data for editing an existing PatientInsuranceAuthorization
        /// </summary>
        /// <param name="patientInsuranceAuthorizationId">The Id of a pre authorization to load</param>
        /// <returns></returns>
        PatientInsuranceAuthorizationViewModel LoadPatientInsuranceAuthorization(long patientInsuranceAuthorizationId);

        int Save(PatientInsuranceAuthorizationViewModel viewModel);
    }


    public class PatientInsuranceAuthorizationViewModelMap : IMap<Model.PatientInsuranceAuthorization, PatientInsuranceAuthorizationViewModel>
    {
        private readonly IEnumerable<IMapMember<Model.PatientInsuranceAuthorization, PatientInsuranceAuthorizationViewModel>> _members;

        public PatientInsuranceAuthorizationViewModelMap()
        {
            _members = this.CreateMembers(authorization => new PatientInsuranceAuthorizationViewModel
                                                               {
                                                                   Id = authorization.Id,
                                                                   PatientInsuranceId = authorization.PatientInsuranceId,
                                                                   EncounterId = authorization.EncounterId.Value,
                                                                   Date = authorization.AuthorizationDateTime,
                                                                   Code = authorization.AuthorizationCode,
                                                                   Comments = authorization.Comment,
                                                                   Location = authorization.Encounter != null ? new ColoredViewModel
                                                                   {
                                                                       Id = authorization.Encounter.ServiceLocation.Id,
                                                                       Color = authorization.Encounter.ServiceLocation.Color,
                                                                       Name = authorization.Encounter.ServiceLocation.Name,
                                                                   } : new ColoredViewModel(),
                                                               }).ToArray();
        }

        public IEnumerable<IMapMember<Model.PatientInsuranceAuthorization, PatientInsuranceAuthorizationViewModel>> Members
        {
            get { return _members; }
        }
    }
}

