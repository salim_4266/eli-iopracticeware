﻿using IO.Practiceware.Model.Legacy;
using IO.Practiceware.Presentation.ViewModels.PatientImageImporter;
using IO.Practiceware.Presentation.ViewServices.PatientImageImporter;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Threading;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

[assembly: Component(typeof(PatientImageImporterViewService), typeof(IPatientImageImporterViewService))]

namespace IO.Practiceware.Presentation.ViewServices.PatientImageImporter
{
    /// <summary>
    ///   Provides data and actions for the patient image importer.
    /// </summary>
    public interface IPatientImageImporterViewService
    {
        /// <summary>
        ///   Imports using the specified criteria and index type.
        /// </summary>
        /// <param name="criteria"> The criteria. </param>
        /// <param name="indexType"> Type of the index. </param>
        /// <returns> A token </returns>
        Guid Import(PatientImageImporterCriteriaViewModel criteria, PatientImageImporterIndexTypeViewModel indexType);

        /// <summary>
        ///   Gets information for the specified job.
        /// </summary>
        /// <param name="id"> The id. </param>
        /// <returns> </returns>
        PatientImageImporterJobViewModel GetJobStatus(Guid id);

        /// <summary>
        ///   Cancels the specified job.
        /// </summary>
        /// <param name="id"> The id. </param>
        void Cancel(Guid id);

        /// <summary>
        ///   Gets the available index types.
        /// </summary>
        /// <returns> </returns>
        IEnumerable<PatientImageImporterIndexTypeViewModel> GetIndexTypes();
    }

    internal class PatientImageImporterViewService : BaseViewService, IPatientImageImporterViewService
    {
        private static readonly IDictionary<Guid, PatientImageImporterJobViewModel> Jobs = new Dictionary<Guid, PatientImageImporterJobViewModel>().Synchronized();
        private readonly Func<FlatFilePatientImageImporterIndexTypeViewModel> _createFlatFileIndexType;
        private readonly Func<PatientImageImporterJobViewModel> _createJob;
        private readonly IDictionary<string, Delegate> _filterCache = new Dictionary<string, Delegate>();
        private readonly IPatientImageImporterService _importerService;
        private readonly ILegacyPracticeRepository _legacyPracticeRepository;

        public PatientImageImporterViewService(
            IPatientImageImporterService importerService, 
            ILegacyPracticeRepository legacyPracticeRepository, 
            Func<FlatFilePatientImageImporterIndexTypeViewModel> createFlatFileIndexType, 
            Func<PatientImageImporterJobViewModel> createJob)
        {
            _importerService = importerService;
            _legacyPracticeRepository = legacyPracticeRepository;
            _createFlatFileIndexType = createFlatFileIndexType;
            _createJob = createJob;
        }

        #region IPatientImageImporterViewService Members

        public Guid Import(PatientImageImporterCriteriaViewModel criteria, PatientImageImporterIndexTypeViewModel indexType)
        {
            Guid jobId = Guid.NewGuid();

            PatientImageImporterJobViewModel job = _createJob();
            job.Id = jobId;
            job.Status = PatientImageImporterJobStatus.Running;
            job.Started = DateTime.Now.ToClientTime();

            Jobs[jobId] = job;

            Task.Factory.StartNewWithCurrentTransaction(() => BeginImport(criteria, indexType, job));

            return jobId;
        }

        public PatientImageImporterJobViewModel GetJobStatus(Guid id)
        {
            PatientImageImporterJobViewModel job;
            if (!Jobs.TryGetValue(id, out job))
            {
                throw new InvalidOperationException("Job {0} not found.".FormatWith(id));
            }
            return job;
        }

        public void Cancel(Guid id)
        {
            PatientImageImporterJobViewModel job;
            if (!Jobs.TryGetValue(id, out job))
            {
                throw new InvalidOperationException("Job {0} not found.".FormatWith(id));
            }
            if (job.Status == PatientImageImporterJobStatus.Running) job.Status = PatientImageImporterJobStatus.CancellationPending;
            while (job.Status == PatientImageImporterJobStatus.CancellationPending) Thread.Sleep(50);
        }

        public IEnumerable<PatientImageImporterIndexTypeViewModel> GetIndexTypes()
        {
            return new[] { _createFlatFileIndexType() };
        }

        #endregion

        private void BeginImport(PatientImageImporterCriteriaViewModel criteria, PatientImageImporterIndexTypeViewModel indexType, PatientImageImporterJobViewModel job)
        {
            IEnumerable<PatientDocument> source = null;
            try
            {
                using (var logger = new StreamWriter("{0}.log".FormatWith(job.Id), true))
                {
                    switch (indexType.IndexType)
                    {
                        case PatientImageImporterIndexType.FlatFile:
                            source = CreateFlatFileSource(criteria, (FlatFilePatientImageImporterIndexTypeViewModel)indexType, job, logger);
                            break;
                        default:
                            throw new NotSupportedException();
                    }

                    _importerService.Import(source, criteria.MoveFiles, criteria.ConvertToPdf, criteria.DestinationPath, e => OnImport(criteria, e, job, logger), e => e.Cancel = OnError(e.Message, job, logger));
                }

                if (job.Status != PatientImageImporterJobStatus.Cancelled) job.Status = PatientImageImporterJobStatus.Completed;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                lock (((ICollection)job.Errors).SyncRoot)
                {
                    job.Errors.Add(ex.ToString());
                }
                job.Status = PatientImageImporterJobStatus.Error;
            }
            finally
            {
                var disposable = source as IDisposable;
                if (disposable != null) disposable.Dispose();
            }
        }

        private IEnumerable<PatientDocument> CreateFlatFileSource(PatientImageImporterCriteriaViewModel criteria, FlatFilePatientImageImporterIndexTypeViewModel indexType, PatientImageImporterJobViewModel job, StreamWriter logger)
        {
            var result = new FlatFilePatientDocumentSource(
                indexType.IndexFilePath,
                criteria.SourcePath,
                new Regex(indexType.RegularExpression),
                _legacyPracticeRepository.PatientDemographics.ByPriorPatientCode(),
                indexType.DateFormat
                );

            result.Error += (sender, e) => e.Cancel = OnError(e.Message, job, logger);

            return result;
        }


        private static bool OnError(string message, PatientImageImporterJobViewModel job, StreamWriter logger)
        {
            job.FailedFileCount++;
            logger.WriteLine(message);

            lock (((ICollection)job.Errors).SyncRoot)
            {
                while (job.Errors.Count > 200) job.Errors.RemoveAt(0);

                job.Errors.Add(string.Empty);
                job.Errors.Add(message);
            }

            if (job.Status == PatientImageImporterJobStatus.CancellationPending)
            {
                job.Status = PatientImageImporterJobStatus.Cancelled;
                return true;
            }

            return false;
        }


        /// <summary>
        ///   Handles file import event. Applies filters, ignores and cancellations as necessary. Updates log.
        /// </summary>
        /// <param name="criteria"> The criteria. </param>
        /// <param name="args"> The instance containing the event data. </param>
        /// <param name="job"> The job. </param>
        /// <param name="logger"> The logger. </param>
        private void OnImport(PatientImageImporterCriteriaViewModel criteria, PatientImageImportEventArgs args, PatientImageImporterJobViewModel job, StreamWriter logger)
        {
            if (job.Status == PatientImageImporterJobStatus.CancellationPending)
            {
                logger.WriteLine("Cancelled manually.");
                job.Status = PatientImageImporterJobStatus.Cancelled;
                args.Cancel = true;
                return;
            }

            if (criteria.MaximumFilesToImport != null && job.ImportedFileCount >= criteria.MaximumFilesToImport)
            {
                logger.WriteLine("Cancelled after {0} files.".FormatWith(criteria.MaximumFilesToImport));
                job.Status = PatientImageImporterJobStatus.Cancelled;
                args.Cancel = true;
                return;
            }

            if (criteria.MaximumRunMinutes != null && (DateTime.Now.ToClientTime() - job.Started).TotalMinutes > criteria.MaximumRunMinutes)
            {
                logger.WriteLine("Cancelled after {0} minutes.".FormatWith(criteria.MaximumRunMinutes));
                job.Status = PatientImageImporterJobStatus.Cancelled;
                args.Cancel = true;
                return;
            }

            if ((criteria.CreatedAfter != null && criteria.CreatedAfter > args.Document.DateTime)
                ||
                criteria.CreatedBefore != null && criteria.CreatedBefore < args.Document.DateTime)
            {
                args.Ignore = true;
                job.IgnoredFileCount++;

                logger.WriteLine("Ignoring file because of date: {0}.".FormatWith(args.Document.Pages.First()));

                return;
            }

            if (criteria.Filter.IsNotNullOrEmpty())
            {
                Delegate predicate = _filterCache.GetValue(
                    "{0}_{1}".FormatWith(args.Document.GetType(), criteria.Filter),
                    () => DynamicExpression.ParseLambda(args.Document.GetType(), typeof(bool), criteria.Filter).Compile()
                    );

                var result = (bool)predicate.DynamicInvoke(args.Document);

                if (!result)
                {
                    args.Ignore = true;
                    job.IgnoredFileCount++;
                    logger.WriteLine("Ignoring file because of filter: {0}.".FormatWith(args.Document.Pages.First()));

                    return;
                }
            }

            job.ImportedFileCount++;

            logger.WriteLine("Processing file with {0} pages: {1}.".FormatWith(args.Document.Pages.Count, args.Document.Pages.First()));
        }
    }
}