﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.AppointmentSearch;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.AppointmentSearch;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using Soaf.Threading;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Media;
using Task = System.Threading.Tasks.Task;

[assembly: Component(typeof(AppointmentSearchViewService), typeof(IAppointmentSearchViewService))]

namespace IO.Practiceware.Presentation.ViewServices.AppointmentSearch
{
    public interface IAppointmentSearchViewService
    {
        /// <summary>
        /// Gets the appointment search load information necessary to load the AppointmentSearch view
        /// </summary>
        /// <returns>Returns a new instance of AppointmentSearchLoadInformation</returns>
        AppointmentSearchLoadInformation LoadInformation(int patientId, DateTime activeDate, AppointmentSearchLoadTypes loadTypes = AppointmentSearchLoadTypes.All);

        /// <summary>
        /// Search for available appointments
        /// </summary>
        /// <param name="filter">Search options</param>
        /// <param name="dateFilter">Date search options</param>
        /// <returns>Collection of view models with available appointment data populated</returns>
        ObservableCollection<AppointmentSearchResultsViewModel> FindAppointments(AppointmentSearchFilterViewModel filter, AppointmentSearchDateTimeFilterViewModel dateFilter);

        /// <summary>
        /// Get the appointment history for a patient - Used to refresh the history list as appointments are schedule or reschedule
        /// </summary>
        /// <param name="patientId">The patient</param>
        /// <returns>The history</returns>
        ObservableCollection<AppointmentSearchAppointmentHistoryViewModel> LoadPatientAppointmentHistory(int patientId);

        /// <summary>
        /// Gets a flag that indicates whether the patient is active
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        bool IsPatientActive(int patientId);
    }

    [SupportsDataErrorInfo]
    [DataContract]
    public class AppointmentSearchLoadInformation
    {
        [DataMember]
        public AppointmentSearchPatientInfoViewModel PatientInfo { get; set; }

        [DataMember]
        [Dependency]
        public ObservableCollection<AppointmentSearchAppointmentHistoryViewModel> PatientAppointmentHistory { get; set; }

        [DataMember]
        public AppointmentSearchPatientCommentsViewModel PatientComments { get; set; }

        [DataMember]
        [Dependency]
        public ObservableCollection<AppointmentSearchReferralsViewModel> PatientReferrals { get; set; }

        [DataMember]
        public AppointmentSearchFilterViewModel SearchFilter { get; set; }
    }

    internal class AppointmentSearchViewService : BaseViewService, IAppointmentSearchViewService
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly Func<AppointmentSearchFilterViewModel> _createSearchFilter;

        public AppointmentSearchViewService(IPracticeRepository practiceRepository,
                                            Func<AppointmentSearchFilterViewModel> createSearchFilter)
        {
            _practiceRepository = practiceRepository;
            _createSearchFilter = createSearchFilter;
        }

        public bool IsPatientActive(int patientId)
        {
            return PracticeRepository.Patients.WithId(patientId).PatientStatus == PatientStatus.Active;
        }

        public AppointmentSearchLoadInformation LoadInformation(int patientId, DateTime activeDate, AppointmentSearchLoadTypes loadTypes)
        {
            ObservableCollection<AppointmentSearchAppointmentHistoryViewModel> patientAppointmentHistory = null;
            AppointmentSearchPatientCommentsViewModel patientComments = null;
            AppointmentSearchPatientInfoViewModel patientInfo = null;
            ObservableCollection<AppointmentSearchReferralsViewModel> patientReferrals = null;
            AppointmentSearchFilterViewModel searchFilter = null;
            var parallelLoadActions = new List<Action>();

            if (loadTypes.HasFlag(AppointmentSearchLoadTypes.PatientAppointmentHistory))
            {
                parallelLoadActions.Add(() => patientAppointmentHistory = LoadPatientAppointmentHistory(patientId));
            }
            if (loadTypes.HasFlag(AppointmentSearchLoadTypes.PatientComments))
            {
                parallelLoadActions.Add(() => patientComments = LoadPatientComments(patientId));
            }
            if (loadTypes.HasFlag(AppointmentSearchLoadTypes.PatientInfo))
            {
                parallelLoadActions.Add((() => patientInfo = LoadPatientInfo(patientId)));
            }
            if (loadTypes.HasFlag(AppointmentSearchLoadTypes.PatientReferrals))
            {
                parallelLoadActions.Add(() => patientReferrals = LoadPatientReferrals(patientId, activeDate));
            }
            if (loadTypes.HasFlag(AppointmentSearchLoadTypes.SearchFilter))
            {
                parallelLoadActions.Add(() => searchFilter = LoadSearchFilter());
            }

            if (parallelLoadActions.Any())
            {
                parallelLoadActions.ForAllInParallel(a => a());
                return new AppointmentSearchLoadInformation
                    {
                        PatientAppointmentHistory = patientAppointmentHistory,
                        PatientComments = patientComments,
                        PatientInfo = patientInfo,
                        PatientReferrals = patientReferrals,
                        SearchFilter = searchFilter
                    };
            }
            return default(AppointmentSearchLoadInformation);
        }

        #region Patient Information

        private AppointmentSearchPatientInfoViewModel LoadPatientInfo(int patientId)
        {

            IEnumerable<Encounter> encounters = null;
            Patient patient = null;

            var a1 = new Action(() => patient = PracticeRepository.Patients.Where(p => p.Id == patientId).Include(p => p.PatientTags.Select(y => y.Tag)).Include(p => p.PatientPhoneNumbers).First());
            var a2 = new Action(() =>
                                    {
                                        encounters = PracticeRepository.Encounters.Where(e => e.PatientId == patientId).ToArray();
                                        PracticeRepository.AsQueryableFactory().Load(encounters, e => e.Invoices.Select(i => i.InvoiceReceivables.Select(ir => ir.BillingServiceTransactions)));
                                    });

            new[] { a1, a2 }.ForAllInParallel(a => a());

            var viewModel = new AppointmentSearchPatientInfoViewModel
                {
                    Id = patient.Id,
                    Type = patient.PatientTags.Any() ? AggregatePatientTypesToString(patient.PatientTags.Select(y => y.Tag).WithTagTypePatientCategory()) : string.Empty,
                    Name = patient.DisplayName,
                    DefaultResourceId = patient.DefaultUserId,
                    WorkPhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business) != null ? patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business).ToString() : string.Empty,
                    CellPhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell) != null ? patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell).ToString() : string.Empty,
                    HomePhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home) != null ? patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home).ToString() : string.Empty,
                    Balance = encounters.SelectMany(e => e.Invoices).SelectMany(i => i.InvoiceReceivables).GetPatientBalance(),
                    IsPatientRemoved = patient.PatientStatus != PatientStatus.Active
                };

            return viewModel;
        }

        public ObservableCollection<AppointmentSearchAppointmentHistoryViewModel> LoadPatientAppointmentHistory(int patientId)
        {
            var viewModels = new ObservableCollection<AppointmentSearchAppointmentHistoryViewModel>();

            var encounters = PracticeRepository.Encounters.Where(e => e.PatientId == patientId).ToArray();
            encounters.LoadForAppointmentSearch(PracticeRepository);

            var configurations = _practiceRepository.EncounterStatusConfigurations.ToDictionary(i => i.Id);

            foreach (var appointment in GetValidAppointmentsInDescendingOrder(encounters))
            {
                int encounterStatusId = (int)appointment.Encounter.EncounterStatus;
                var encounterStatus = configurations[encounterStatusId];
                var viewModel = new AppointmentSearchAppointmentHistoryViewModel
                    {
                        Id = appointment.Id,
                        AppointmentDateTime = appointment.DateTime,
                        AppointmentStatus = new AppointmentStatusViewModel { Id = encounterStatus.Id, Status = encounterStatus.Name, StatusCode = encounterStatus.Name[0], StatusColor = encounterStatus.Color },
                        AppointmentType = new NamedViewModel { Id = appointment.AppointmentType.Id, Name = appointment.AppointmentType.Name },
                        Location = new NamedViewModel { Id = appointment.Encounter.ServiceLocationId, Name = appointment.Encounter.ServiceLocation.ShortName },
                        Comments = appointment.Comment,
                        ReferralNumbers = new ObservableCollection<string>(GetReferralsInDescendingOrder(appointment).Select(pir => pir.ReferralCode))
                    };
                // ReSharper disable PossibleInvalidOperationException
                // Appointment should always have a resource
                viewModel.Resource = new NamedViewModel { Name = appointment.ResourceName, Id = appointment.ResourceId.Value };
                // ReSharper restore PossibleInvalidOperationException

                viewModels.Add(viewModel);
            }

            return viewModels;
        }

        private AppointmentSearchPatientCommentsViewModel LoadPatientComments(int patientId)
        {
            var viewModel = new AppointmentSearchPatientCommentsViewModel();
            viewModel.Comments = PracticeRepository.PatientComments
                                                   .Where(c => c.PatientId == patientId && c.PatientCommentType == PatientCommentType.PatientDemographicSummary)
                                                   .Select(c => c.Value)
                                                   .Join(". ");
            return viewModel;
        }

        private ObservableCollection<AppointmentSearchReferralsViewModel> LoadPatientReferrals(int patientId, DateTime activeDate)
        {
            var databaseReferrals = PracticeRepository.PatientInsuranceReferrals.Where(r => !r.IsArchived && r.PatientId == patientId && r.StartDateTime <= activeDate && r.EndDateTime >= activeDate).OrderBy(r => r.StartDateTime).ToList();
            PracticeRepository.AsQueryableFactory().Load(databaseReferrals, r => r.Encounters,
                                                                            r => r.PatientInsurance.InsurancePolicy.Insurer,
                                                                            r => r.Doctor);

            var viewModels = new ExtendedObservableCollection<AppointmentSearchReferralsViewModel>();

            foreach (var databaseReferral in databaseReferrals)
            {
                // Display referrals that have more then 0 visits remaining
                var encountersUsed = databaseReferral.Encounters.Count(e => !e.EncounterStatus.IsCancelledStatus());
                var totalEncountersCovered = databaseReferral.TotalEncountersCovered.GetValueOrDefault();
                if (encountersUsed >= totalEncountersCovered) continue;

                var viewModel = new AppointmentSearchReferralsViewModel();
                viewModel.EncountersUsed = encountersUsed;
                viewModel.Insurer = new NamedViewModel { Id = databaseReferral.PatientInsurance.InsurancePolicy.InsurerId, Name = databaseReferral.PatientInsurance.InsurancePolicy.Insurer.Name };
                viewModel.ReferralExpirationDate = databaseReferral.EndDateTime;
                viewModel.ReferralNumber = databaseReferral.ReferralCode;
                viewModel.ReferralReason = databaseReferral.Comment;
                viewModel.ReferredTo = new NamedViewModel { Id = databaseReferral.DoctorId, Name = databaseReferral.Doctor.IfNotNull(u => u.UserName) };
                viewModel.TotalEncountersCovered = totalEncountersCovered;
                viewModels.Add(viewModel);
            }
            return viewModels;
        }

        #endregion

        private AppointmentSearchFilterViewModel LoadSearchFilter()
        {
            var viewModel = _createSearchFilter();

            ObservableCollection<AppointmentTypeViewModel> appointmentTypes = null;
            ObservableCollection<NamedViewModel> locations = null;
            ObservableCollection<NamedViewModel> resources = null;

            var a1 = new Action(() => appointmentTypes = PracticeRepository.AppointmentTypes.Where(at => at.AppointmentCategoryId != null && !at.IsArchived).Include(at => at.AppointmentCategory).OrderBy(i => i.OrdinalId).ThenBy(i => i.Name).Select(at => new AppointmentTypeViewModel { Id = at.Id, Name = at.Name, AppointmentCategory = new NamedViewModel { Id = at.AppointmentCategory.Id, Name = at.AppointmentCategory.Name } }).ToExtendedObservableCollection());
            var a3 = new Action(() => locations = PracticeRepository.ServiceLocations.OrderBy(l => l.IsExternal).ThenBy(l => l.ShortName).Select(sl => new NamedViewModel { Id = sl.Id, Name = sl.ShortName, }).ToExtendedObservableCollection());
            var a4 = new Action(() => resources = PracticeRepository.Users.OrderBy(i => i.UserName).Where(u => u.IsSchedulable).Select(u => new NamedViewModel { Id = u.Id, Name = u.UserName }).ToExtendedObservableCollection());
            new[] { a1, a3, a4 }.ForAllInParallel(a => a());

            viewModel.AppointmentTypes = appointmentTypes;
            viewModel.Locations = locations;
            viewModel.Resources = resources;

            return viewModel;
        }

        #region Search Results

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public virtual ObservableCollection<AppointmentSearchResultsViewModel> FindAppointments(AppointmentSearchFilterViewModel filter, AppointmentSearchDateTimeFilterViewModel dateFilter)
        {
            var results = new List<AppointmentSearchResultsViewModel>();

            var availableBlocks = PracticeRepository.ScheduleBlocks.OfType<UserScheduleBlock>()
                .Include(b => b.ScheduleBlockAppointmentCategories,
                         b => b.ServiceLocation,
                         b => b.User)
                .Where(sb => !sb.IsUnavailable)
                .OrderBy(sb => sb.StartDateTime);

            var predicates = new List<Expression<Func<UserScheduleBlock, bool>>>();
            predicates.Add(GetSearchPredicateByAppointmentType(availableBlocks, filter));
            predicates.Add(GetSearchPredicateByDoctor(availableBlocks, filter));
            predicates.Add(GetSearchPredicateByLocation(availableBlocks, filter));
            predicates.AddRange(GetSearchPredicatesByDateFilter(availableBlocks, dateFilter.SelectedDateOption));
            predicates.AddRange(GetSearchPredicatesByTimeFilter(availableBlocks, dateFilter.SelectedTimeOption));

            Expression<Func<UserScheduleBlock, bool>> filterCondition = predicates.First();
            foreach (var predicate in predicates.Skip(1)) { filterCondition = filterCondition.And(predicate); }

            Task<List<UserScheduleBlock>> loadBlocksTask = Task.Factory.StartNewWithCurrentTransaction(() => availableBlocks.Where(filterCondition).Take(100).ToArray().GroupBy(i => new { i.StartDateTime, i.UserId }).Select(i => i.First()).ToList());
            var blocks = loadBlocksTask.Result;
            var days = dateFilter.SelectedDateOption.SelectedDays.Select(sd => sd.ToString());
            blocks = blocks.Where(sb => days.Contains(AppointmentSearchQueries.GetDayOfWeek(sb))).ToList();

            foreach (var block in blocks)
            {
                var userScheduleBlock = block;

                var viewModel = new AppointmentSearchResultsViewModel
                {
                    AppointmentType = filter.SelectedAppointmentType,
                    Date = block.StartDateTime.Date,
                    Time = block.StartDateTime.TimeOfDay,
                    Location = new NamedViewModel { Id = userScheduleBlock.ServiceLocationId, Name = userScheduleBlock.ServiceLocation.ShortName },
                    Resource = new NamedViewModel { Id = userScheduleBlock.UserId, Name = userScheduleBlock.User.UserName },
                    Color = Color.FromArgb(userScheduleBlock.User.Color.A, userScheduleBlock.User.Color.R, userScheduleBlock.User.Color.G, userScheduleBlock.User.Color.B)
                };
                viewModel.ScheduleBlockCategoryIds = block.ScheduleBlockAppointmentCategories.Where(c => c.AppointmentId == null && c.AppointmentCategoryId == filter.SelectedAppointmentType.AppointmentCategory.Id).Select(c => c.Id).ToExtendedObservableCollection();

                if (!viewModel.ScheduleBlockCategoryIds.Any()) continue;

                results.Add(viewModel);
            }

            if (results.Any())
            {

                var resultDates = results.Select(r => r.Date).Distinct().ToArray();
                var resultResources = results.Select(r => r.Resource.Id).Distinct().ToArray();

                var orphanedAppointmentsTask = Task.Factory.StartNewWithCurrentTransaction(() => PracticeRepository.Appointments.OfType<UserAppointment>()
                                                                                                     .Where(a => a.ScheduleBlockAppointmentCategories.Count == 0)
                                                                                                     .Where(Queries.IsOnDates<UserAppointment>(a => a.DateTime, resultDates))
                                                                                                     .Where(a => resultResources.Contains(a.UserId))
                                                                                                     .Where(a => !EncounterQueries.IsCancelled.Invoke(a.Encounter)).ToArray()
                                                                                                     .GroupBy(a => new { a.DateTime.Date, a.UserId })
                                                                                                     .ToDictionary(i => new { i.Key.Date, ResourceId = i.Key.UserId }));

                var workloadByDayAndResource = (from block in PracticeRepository.ScheduleBlocks.OfType<UserScheduleBlock>()
                                                from blockCategory in block.ScheduleBlockAppointmentCategories
                                                where block != null && resultDates.Contains(MultiCalendarQueries.GetUserScheduleBlockDate(block)) && resultResources.Contains(block.UserId)
                                                group blockCategory by new { Date = MultiCalendarQueries.GetUserScheduleBlockDate(block), block.UserId }
                                                    into g
                                                    select new
                                                        {
                                                            g.Key.Date,
                                                            ResourceId = g.Key.UserId,
                                                            AppointmentCount = g.Count(i => i.AppointmentId != null),
                                                            TotalBlockCategories = g.Count()
                                                        }).ToDictionary(i => new { i.Date, i.ResourceId });

                var orphanedAppointmentsByDayAndResource = orphanedAppointmentsTask.Result;

                foreach (var result in results)
                {
                    var workload = workloadByDayAndResource.GetValue(new { result.Date, ResourceId = result.Resource.Id });
                    var orphanCount = orphanedAppointmentsByDayAndResource.GetValue(new { result.Date, ResourceId = result.Resource.Id });
                    if (workload != null)
                    {
                        result.Workload = orphanCount != null ? "{0}/{1}".FormatWith(workload.AppointmentCount + orphanCount.Count(), workload.TotalBlockCategories)
                                              : "{0}/{1}".FormatWith(workload.AppointmentCount, workload.TotalBlockCategories);
                    }
                }
            }

            return results.ToExtendedObservableCollection();
        }

        private static Expression<Func<UserScheduleBlock, bool>> GetSearchPredicateByDoctor(IEnumerable<UserScheduleBlock> scheduleEntries, AppointmentSearchFilterViewModel filter)
        {
            var selectedResourceIds = filter.SelectedResources.Select(r => r.Id).ToArray();
            return scheduleEntries.CreatePredicate(se => selectedResourceIds.Contains(se.UserId));
        }

        private static Expression<Func<UserScheduleBlock, bool>> GetSearchPredicateByLocation(IEnumerable<UserScheduleBlock> scheduleEntries, AppointmentSearchFilterViewModel filter)
        {
            var selectedLocationIds = filter.SelectedLocations.Select(l => l.Id).ToArray();
            return scheduleEntries.CreatePredicate(se => selectedLocationIds.Contains(se.ServiceLocationId));
        }

        private static Expression<Func<UserScheduleBlock, bool>> GetSearchPredicateByAppointmentType(IEnumerable<UserScheduleBlock> scheduleEntries, AppointmentSearchFilterViewModel filter)
        {
            var selectedAppointmentCategoryId = filter.SelectedAppointmentType.AppointmentCategory.Id;
            return scheduleEntries.CreatePredicate(se => se.ScheduleBlockAppointmentCategories.Any(sbac => sbac.AppointmentCategoryId == selectedAppointmentCategoryId && sbac.AppointmentId == null));
        }

        private static IEnumerable<Expression<Func<UserScheduleBlock, bool>>> GetSearchPredicatesByDateFilter(IQueryable<UserScheduleBlock> scheduleEntries, DateSelectionViewModel filter)
        {
            var predicates = new List<Expression<Func<UserScheduleBlock, bool>>>();

            // TODO Make AppointmentSearchQueries.GetDayOfWeek queryable
            //var days = filter.SelectedDays.Select(sd => sd.ToString());
            //predicates.Add(scheduleEntries.CreatePredicate(se => days.Contains(AppointmentSearchQueries.GetDayOfWeek(se))));

            if (filter.BeginDate.HasValue) { predicates.Add(scheduleEntries.CreatePredicate(se => se.StartDateTime >= filter.BeginDate.Value)); }
            if (filter.EndDate.HasValue) { predicates.Add(scheduleEntries.CreatePredicate(se => se.StartDateTime < filter.EndDate.Value)); }

            return predicates;
        }

        private static IEnumerable<Expression<Func<UserScheduleBlock, bool>>> GetSearchPredicatesByTimeFilter(IEnumerable<UserScheduleBlock> scheduleEntries, TimeSelectionViewModel filter)
        {
            Func<double, Expression<Func<UserScheduleBlock, bool>>> afterTime = timeInSeconds => scheduleEntries.CreatePredicate(sb => AppointmentSearchQueries.GetTimeOfDayInSeconds(sb) >= timeInSeconds);
            Func<double, Expression<Func<UserScheduleBlock, bool>>> beforeTime = timeInSeconds => scheduleEntries.CreatePredicate(sb => AppointmentSearchQueries.GetTimeOfDayInSeconds(sb) < timeInSeconds);
            Func<double, Expression<Func<UserScheduleBlock, bool>>> atTime = timeInSeconds => scheduleEntries.CreatePredicate(sb => AppointmentSearchQueries.GetTimeOfDayInSeconds(sb).Equals((int)timeInSeconds));

            var predicates = new List<Expression<Func<UserScheduleBlock, bool>>>();

            var beginTime = filter.BeginTime;
            var endTime = filter.EndTime;

            // ReSharper disable PossibleInvalidOperationException
            switch (filter.TimeSelectionType)
            {
                case TimeSelectionType.Before:
                    predicates.Add(beforeTime(beginTime.Value.TotalSeconds));
                    break;
                case TimeSelectionType.Exact:
                    predicates.Add(atTime(beginTime.Value.TotalSeconds));
                    break;
                case TimeSelectionType.After:
                    predicates.Add(afterTime(beginTime.Value.TotalSeconds));
                    break;
                case TimeSelectionType.AllDay:
                case TimeSelectionType.Am:
                case TimeSelectionType.Pm:
                case TimeSelectionType.Range:
                    if (beginTime.Value == endTime.Value)
                    {
                        predicates.Add(atTime(beginTime.Value.TotalSeconds));
                    }
                    else
                    {
                        predicates.Add(afterTime(beginTime.Value.TotalSeconds));
                        predicates.Add(beforeTime(endTime.Value.TotalSeconds));
                    }
                    break;
            }
            // ReSharper restore PossibleInvalidOperationException
            return predicates;
        }

        #endregion

        #region Helper Functions

        private static string AggregatePatientTypesToString(IEnumerable<Tag> patientTypes)
        {
            return patientTypes.Select(pt => pt.DisplayName).Join();
        }

        private static IEnumerable<Appointment> GetValidAppointmentsInDescendingOrder(IEnumerable<Encounter> encounters)
        {
            return encounters.SelectMany(e => e.Appointments).Where(a => a.ResourceId != null).OrderByDescending(a => a.DateTime);
        }

        private static IEnumerable<PatientInsuranceReferral> GetReferralsInDescendingOrder(Appointment appointment)
        {
            return appointment.Encounter.PatientInsuranceReferrals.OrderByDescending(pir => pir.StartDateTime);
        }

        #endregion
    }

    [Flags]
    public enum AppointmentSearchLoadTypes
    {
        PatientAppointmentHistory = 1,
        PatientComments = 2,
        PatientInfo = 4,
        PatientReferrals = 8,
        SearchFilter = 16,
        All = PatientAppointmentHistory | PatientComments | PatientInfo | PatientReferrals | SearchFilter
    }

    public static class DayOfWeekExtensions
    {
        public static int GetValue(this DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Sunday: return 1;
                case DayOfWeek.Monday: return 2;
                case DayOfWeek.Tuesday: return 3;
                case DayOfWeek.Wednesday: return 4;
                case DayOfWeek.Thursday: return 5;
                case DayOfWeek.Friday: return 6;
                default: return 7;
            }
        }
    }
}