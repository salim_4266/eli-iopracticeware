﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.Common;
using Soaf;
using Soaf.ComponentModel;
using Soaf.Data;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;

[assembly: Component(typeof(PostOperativePeriodService), typeof(IPostOperativePeriodService))]

namespace IO.Practiceware.Presentation.ViewServices.Common
{
    public interface IPostOperativePeriodService
    {
        PostOperativeViewModel GetLatestPostOperativePeriodByPatientId(int patientId);
    }

    public class PostOperativePeriodService : BaseViewService, IPostOperativePeriodService
    {
        #region IPostOperativePeriodService Members

        [TransactionScope(TransactionScopeOption.Suppress, IsolationLevel.ReadUncommitted, TimeoutSeconds = 10)]
        public virtual PostOperativeViewModel GetLatestPostOperativePeriodByPatientId(int patientId)
        {
            return GetLatestPostOperativePeriodByPatientIdQuery(patientId);
        }

        #endregion

        private PostOperativeViewModel GetLatestPostOperativePeriodByPatientIdQuery(int patientId)
        {
            try
            {
            DateTime? dateTime = PracticeRepository.Invoices
                .Where(i => i.Encounter.PatientId == patientId)
                .SelectMany(i => i.BillingServices.Where(bs => bs.EncounterService != null)
                                     .SelectMany(bs => bs.EncounterService.InsurerEncounterServices
                                                           .Where(ies => i.InvoiceReceivables.Any(ir => ir.PatientInsurance != null && ir.PatientInsurance.InsurancePolicy.InsurerId == ies.InsurerId))
                                                           .Select(ies => CommonQueries.AddDays(i.Encounter.StartDateTime, ies.PostOperativePeriod))))
                .OrderByDescending(i => i).FirstOrDefault();


                return dateTime != null ? new PostOperativeViewModel { EndDate = dateTime.Value, PatientId = patientId } : null;
            }
            catch (Exception ex)
            {
                // to be removed when timing out isn't a concern for this query
                var sqlException = ex.SearchFor<SqlException>();
                if (sqlException != null && sqlException.Message.StartsWith("timeout expired", StringComparison.InvariantCultureIgnoreCase))
                {
                    return null;
                }

                // Still need to throw other exceptions
                throw;
            }
        }
    }
}