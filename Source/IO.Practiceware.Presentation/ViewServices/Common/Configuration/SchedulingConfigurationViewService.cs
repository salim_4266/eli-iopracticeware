using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Common.Configuration;
using IO.Practiceware.Presentation.ViewServices.Common.Configuration;
using Soaf.ComponentModel;
using Soaf.Data;
using System.Linq;
using System.Transactions;

[assembly: Component(typeof(SchedulingConfigurationViewService), typeof(ISchedulingConfigurationViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Common.Configuration
{
    public interface ISchedulingConfigurationViewService
    {
        /// <summary>
        ///   Get the initial informational parameters for the schedule views
        /// </summary>
        ScheduleConfigurationViewModel GetSchedulingConfiguration();
    }

    internal class SchedulingConfigurationViewService : BaseViewService, ISchedulingConfigurationViewService
    {
        private readonly IPracticeRepository _practiceRepository;

        public SchedulingConfigurationViewService(IPracticeRepository practiceRepository)
        {
            _practiceRepository = practiceRepository;
        }

        #region ISchedulingConfigurationViewService Members

        [TransactionScope(TransactionScopeOption.Suppress, IsolationLevel.ReadUncommitted)]
        public virtual ScheduleConfigurationViewModel GetSchedulingConfiguration()
        {
            var viewModel = new ScheduleConfigurationViewModel();

            ScheduleTemplateBuilderConfiguration configuration = _practiceRepository.ScheduleTemplateBuilderConfigurations.FirstOrDefault();
            if (configuration == null) return viewModel;

            viewModel.DefaultLocation = PracticeRepository.ServiceLocations
                                            .Where(sl => sl.Id == configuration.DefaultLocationId)
                                            .ToArray()
                                            .Select(sl =>
                                                    new ColoredViewModel
                                                        {
                                                            Id = configuration.DefaultLocationId,
                                                            Color = sl.Color,
                                                            Name = sl.ShortName
                                                        }).FirstOrDefault() ?? viewModel.DefaultLocation;

            viewModel.SpanSize = configuration.SpanSize.TimeOfDay;
            viewModel.AreaStart = configuration.AreaStart.TimeOfDay;
            viewModel.AreaEnd = configuration.AreaEnd.TimeOfDay;
            viewModel.MaximumApplyTemplateMonths = configuration.MaximumApplyTemplateMonths;

            return viewModel;
        }

        #endregion
    }
}