using System.Linq;
using System.Runtime.Remoting.Messaging;
using IO.Practiceware.Application;
using IO.Practiceware.Configuration;
using Soaf.Collections;
using Soaf.ComponentModel;

namespace IO.Practiceware.Presentation.ViewServices.Common
{
    /// <summary>
    /// Ensures that view service returns AOP activated view models
    /// </summary>
    public class ReturnsAopActivatedInstancesAttribute : ConcernAttribute
    {
        public ReturnsAopActivatedInstancesAttribute()
            : base(typeof(ReturnsAopActivatedInstancesBehavior), -900) // RemoteService has -1000, so execute before it executes
        {
        }
    }

    public class ReturnsAopActivatedInstancesBehavior : IInterceptor
    {
        private const string ReturnsAopActivatedInstancesCallContextKey = "__ReturnsAopActivatedInstances__";

        private readonly ICloner _cloner;

        public ReturnsAopActivatedInstancesBehavior(ICloner cloner)
        {
            _cloner = cloner;
        }

        public void Intercept(IInvocation invocation)
        {
            // Skip if call travels over the wire or we are running on server's end
            if (ConfigurationManager.ApplicationServerClientConfiguration.IsEnabled
                || ApplicationContext.Current.IsServerSide)
            {
                invocation.Proceed();
                return;
            }

            // Check whether we are already within the AOP scope
            var scope = CallContext.LogicalGetData(ReturnsAopActivatedInstancesCallContextKey);
            if (scope != null)
            {
                invocation.Proceed();
                return;
            }

            using (new DisposableScope<object>(new object(),
                o => CallContext.LogicalSetData(ReturnsAopActivatedInstancesCallContextKey, o),
                o => CallContext.FreeNamedDataSlot(ReturnsAopActivatedInstancesCallContextKey)))
            {
                // Resolve parameters
                var parameters = invocation.Method.GetParameters()
                    .Select((p, i) => new
                    {
                        Index = i,
                        ParameterType = p.ParameterType.IsByRef
                            ? p.ParameterType.GetElementType()
                            : p.ParameterType,
                        IsOut = p.ParameterType.IsByRef // Handles both scenarios
                    })
                    .ToArray();

                // Ensure inputs are copied as well (so that server updates to parameters do not influence the view)
                parameters
                    .ForEach(x => invocation.Arguments[x.Index] = _cloner.Clone(
                        invocation.Arguments[x.Index], x.ParameterType, x.ParameterType));

                // We apply behavior after execution completes
                invocation.Proceed();

                // Clone every Ref/Out parameter and return value to ensure Castle's dynamic proxies are created
                // This naturally occurs when view service is called over WCF
                parameters
                    .Where(x => x.IsOut)
                    .ForEach(x => invocation.Arguments[x.Index] = _cloner.Clone(
                        invocation.Arguments[x.Index], x.ParameterType, x.ParameterType));

                invocation.ReturnValue = _cloner.Clone(invocation.ReturnValue, invocation.Method.ReturnType, invocation.Method.ReturnType);
            }
        }
    }
}