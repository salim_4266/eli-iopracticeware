﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using Soaf;
using Soaf.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IO.Practiceware.Presentation.ViewServices.Common
{
    public static class ViewServiceExtensions
    {
        /// <summary>
        /// Syncs phone numbers collection between view model and model records.
        /// </summary>
        /// <typeparam name="TPhoneNumber">The type of the phone number.</typeparam>
        /// <param name="repository">The repository.</param>
        /// <param name="phoneNumberViewModels">The phone number view models.</param>
        /// <param name="modelPhoneNumbers">The model phone numbers.</param>
        /// <param name="getPhoneNumberTypeId">The get phone number type id.</param>
        /// <param name="setPhoneNumberTypeId">The set phone number type id.</param>
        /// <param name="defaultPhoneNumberTypeId">The default phone number type id.</param>
        public static void SyncPhoneNumbers<TPhoneNumber>(this IPracticeRepository repository,
            ICollection<PhoneNumberViewModel> phoneNumberViewModels, ICollection<TPhoneNumber> modelPhoneNumbers,
            Func<TPhoneNumber, int> getPhoneNumberTypeId, Action<TPhoneNumber, int> setPhoneNumberTypeId, int defaultPhoneNumberTypeId = 0)
            where TPhoneNumber : class, IPhoneNumber, new()
        {
            // Won't sync not initialized collections
            if (phoneNumberViewModels == null || modelPhoneNumbers == null) return;

            phoneNumberViewModels = phoneNumberViewModels.Where(p => !p.IsEmpty).ToList();

            // Step 1: ensure primary phone number is first in the view models collection
            var viewModelsSource = phoneNumberViewModels.ToList();
            int currentIndex = viewModelsSource.IndexOf(p => p.IsPrimary);
            // Not first or missing item? -> Fix it
            if (currentIndex > 0)
            {
                var item = viewModelsSource[currentIndex];
                // Move to first position
                viewModelsSource.RemoveAt(currentIndex);
                viewModelsSource.Insert(0, item);
            }

            // Step 2: synchronize collections (the order of items in view model collection will be carried over as well)
            SyncDependencyCollection(repository, viewModelsSource, modelPhoneNumbers,
                (viewModel, model) =>
                    (viewModel.Id == model.Id)
                    || // Match either by Id or values of all fields
                    // TODO: match by Id only when automated mechanism of passing ObjectIds for newly added records back to ViewModel is implemented
                    (viewModel.PhoneNumber == model.GetParsedNationalNumber()
                     && viewModel.CountryCode == model.CountryCode
                     && viewModel.Extension == model.Extension
                     && viewModel.PhoneType.IfNotNull(p => p.Id) == getPhoneNumberTypeId(model)),
                (viewModel, model) =>
                {
                    model.AreaCode = viewModel.AreaCode;
                    model.CountryCode = viewModel.CountryCode;
                    model.ExchangeAndSuffix = viewModel.ExchangeAndSuffix;
                    model.IsInternational = viewModel.IsInternational;
                    model.Extension = viewModel.Extension;
                    setPhoneNumberTypeId(model, viewModel.PhoneType.IfNotNull(p => p.Id, () => defaultPhoneNumberTypeId));
                });

            // Step 3: Set new OrdinalId sequence (correct or confirms current one)
            modelPhoneNumbers.EnsureOrdinalIdSequential();
        }

        /// <summary>
        /// Syncs address collection between view model and model records.
        /// </summary>
        /// <typeparam name="TAddress">The type of the address.</typeparam>
        /// <param name="repository">The repository.</param>
        /// <param name="addressViewModels">The address models.</param>
        /// <param name="modelAddresses">The model addresses.</param>
        /// <param name="getAddressTypeId">The get address type id.</param>
        /// <param name="setAddressTypeId">The set address type id.</param>
        /// <param name="defaultAddressTypeId">The default address type id.</param>
        public static void SyncAddresses<TAddress>(this IPracticeRepository repository,
            ICollection<AddressViewModel> addressViewModels, ICollection<TAddress> modelAddresses,
            Func<TAddress, int> getAddressTypeId, Action<TAddress, int> setAddressTypeId, int defaultAddressTypeId = 0)
            where TAddress : class, IAddress, new()
        {
            // Won't sync not initialized collections
            if (addressViewModels == null || modelAddresses == null) return;

            addressViewModels = addressViewModels.Where(p => p != null && !p.IsEmpty).ToList();

            // Step 1: ensure primary address is first in the view models collection
            var viewModelsSource = addressViewModels.ToList();
            int currentIndex = viewModelsSource.IndexOf(p => p.IsPrimary);
            // Not first or missing item? -> Fix it
            if (currentIndex > 0)
            {
                var item = viewModelsSource[currentIndex];
                // Move to first position
                viewModelsSource.RemoveAt(currentIndex);
                viewModelsSource.Insert(0, item);
            }

            // Step 2: synchronize collections (the order of items in view model collection will be carried over as well)
            SyncDependencyCollection(repository, viewModelsSource, modelAddresses,
                (viewModel, model) =>
                    (viewModel.Id == model.Id)
                    || // Match either by Id or values of all fields
                    // TODO: match by Id only when automated mechanism of passing ObjectIds for newly added records back to ViewModel is implemented
                    (viewModel.Line1 == model.Line1
                    && viewModel.Line2 == model.Line2
                    && viewModel.Line3 == model.Line3
                    && viewModel.PostalCode == model.PostalCode
                    && viewModel.City == model.City
                    && viewModel.StateOrProvince.Id == model.StateOrProvince.Id
                    && viewModel.AddressType.IfNotNull(p => p.Id) == getAddressTypeId(model)),
                (viewModel, model) =>
                {
                    model.Line1 = viewModel.Line1;
                    model.Line2 = viewModel.Line2;
                    model.Line3 = viewModel.Line3;
                    model.City = viewModel.City;
                    model.PostalCode = viewModel.PostalCode;
                    model.StateOrProvinceId = viewModel.StateOrProvince.Id;
                    setAddressTypeId(model, viewModel.AddressType.IfNotNull(p => p.Id, () => defaultAddressTypeId));
                });

            // Step 3: Set new OrdinalId sequence (correct or confirms current one)
            modelAddresses.OfType<IHasOrdinalId>().ToList().EnsureOrdinalIdSequential();
        }

        /// <summary>
        /// Syncs email address collection between view model and model records.
        /// </summary>
        /// <typeparam name="TEmailAddress">The type of the email address.</typeparam>
        /// <param name="repository">The repository.</param>
        /// <param name="emailAddressViewModels">The email address models.</param>
        /// <param name="modelEmailAddresses">The model email addresses.</param>
        /// <param name="getEmailAddressTypeId">The get email address type id.</param>
        /// <param name="setEmailAddressTypeId">The set email address type id.</param>
        public static void SyncEmailAddresses<TEmailAddress>(this IPracticeRepository repository,
            ICollection<EmailAddressViewModel> emailAddressViewModels, ICollection<TEmailAddress> modelEmailAddresses,
            Func<TEmailAddress, int> getEmailAddressTypeId, Action<TEmailAddress, int> setEmailAddressTypeId)
            where TEmailAddress : class, IEmailAddress, new()
        {
            // Won't sync not initialized collections
            if (emailAddressViewModels == null || modelEmailAddresses == null) return;

            emailAddressViewModels = emailAddressViewModels.Where(p => !p.IsEmpty).ToList();

            // Step 1: ensure primary address is first in the view models collection
            var viewModelsSource = emailAddressViewModels.ToList();
            int currentIndex = viewModelsSource.IndexOf(p => p.IsPrimary);
            // Not first or missing item? -> Fix it
            if (currentIndex > 0)
            {
                var item = viewModelsSource[currentIndex];
                // Move to first position
                viewModelsSource.RemoveAt(currentIndex);
                viewModelsSource.Insert(0, item);
            }

            // Step 2: synchronize collections (the order of items in view model collection will be carried over as well)
            SyncDependencyCollection(repository, viewModelsSource, modelEmailAddresses,
                (viewModel, model) =>
                    (viewModel.Id == model.Id)
                    || // Match either by Id or values of all fields
                    // TODO: match by Id only when automated mechanism of passing ObjectIds for newly added records back to ViewModel is implemented
                    (viewModel.Value == model.Value
                    && viewModel.EmailAddressType.IfNotNull(p => p.Id) == getEmailAddressTypeId(model)),
                (viewModel, model) =>
                {
                    model.Value = viewModel.Value;
                    setEmailAddressTypeId(model, viewModel.EmailAddressType.IfNotNull(p => p.Id, () => (int)EmailAddressType.Business));
                });

            // Step 3: Set new OrdinalId sequence (correct or confirms current one)
            modelEmailAddresses.OfType<IHasOrdinalId>().ToList().EnsureOrdinalIdSequential();
        }

        /// <summary>
        /// Syncs view model's collection with model's
        /// </summary>
        /// <typeparam name="TSourceItem">The type of the source item.</typeparam>
        /// <typeparam name="TDestinationItem">The type of the destination item.</typeparam>
        /// <param name="repository">The repository.</param>
        /// <param name="sourceItems">The source items.</param>
        /// <param name="destinationItems">The destination items.</param>
        /// <param name="isSameItem">The is same item.</param>
        /// <param name="updateDestinationItem">The update destination item.</param>
        /// <param name="markAsArchived"><c>null</c> when dropped items should be deleted, otherwise - function to mark as archived</param>
        public static void SyncDependencyCollection<TSourceItem, TDestinationItem>(
            this IPracticeRepository repository,
            ICollection<TSourceItem> sourceItems, ICollection<TDestinationItem> destinationItems,
            Func<TSourceItem, TDestinationItem, bool> isSameItem,
            Action<TSourceItem, TDestinationItem> updateDestinationItem,
            Action<TDestinationItem> markAsArchived = null)
            where TDestinationItem : class, new()
        {
            sourceItems.SyncToTargetCollection(destinationItems, isSameItem, updateDestinationItem, (itemToDelete, collection) =>
                                                                                                    {
                                                                                                        if (markAsArchived == null)
                                                                                                        {
                                                                                                            // Remove it from collection first
                                                                                                            collection.Remove(itemToDelete);

                                                                                                            // Remark: EF cannot detect change of item being removed from collection, because it doesn't treat it as identifyable relation
                                                                                                            // See: http://weblogs.asp.net/zeeshanhirani/archive/2010/07/23/removing-entity-from-a-related-collection.aspx
                                                                                                            repository.Delete(itemToDelete);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            markAsArchived(itemToDelete);
                                                                                                        }
                                                                                                    });
        }
    }
}
