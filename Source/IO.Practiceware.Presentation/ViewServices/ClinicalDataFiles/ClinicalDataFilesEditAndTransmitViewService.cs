﻿using System;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using Soaf.ComponentModel;
using Soaf.Linq;
using System.Collections.Generic;
using IO.Practiceware.Presentation.Common;


[assembly: Component(typeof(ClinicalDataFilesEditAndTransmitViewService), typeof(IClinicalDataFilesEditAndTransmitViewService))]
[assembly: Component(typeof(SelectedAppointmentViewModelMap), typeof(IMap<UserAppointment, SelectedAppointmentViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles
{
    public interface IClinicalDataFilesEditAndTransmitViewService
    {
        /// <summary>
        /// Gets the Appointment table for rows which match the appointment Ids
        /// </summary>
        /// <param name="appointmentIds"></param>
        /// <returns></returns>
        IEnumerable<SelectedAppointmentViewModel> GetAppointments(List<int> appointmentIds = null);

        /// <summary>
        /// Gets VisitSummary Xml for the selected Id
        /// </summary>
        /// <param name="selectedAppointmentViewModel"></param>
        /// <returns></returns>
        CdaViewModel GetEncounterClinicalSummaryCda(SelectedAppointmentViewModel selectedAppointmentViewModel);

        /// <summary>
        /// Gets TransitionOfCare Xml for the selected Id
        /// </summary>
        /// <param name="selectedAppointmentViewModel"></param>
        /// <returns></returns>
        CdaViewModel GetPatientTransitionOfCareCda(SelectedAppointmentViewModel selectedAppointmentViewModel);


        /// <summary>
        /// Save the CCDA file to the ExternalSystemMessages table for electronic transmission.
        /// </summary>
        /// <param name="cdaViewModel">The cda view model.</param>
        void TransmitClinicalDataFile(CdaViewModel cdaViewModel);

        /// <summary>
        /// Save the CCDA file to the ExternalSystemMessages table as having been printed.
        /// </summary>
        /// <param name="cdaViewModel">The cda view model.</param>
        void RecordPrintingOfClinicalDataFile(CdaViewModel cdaViewModel);

        /// <summary>
        /// Save the CCDA file to the ExternalSystemMessages table as having been saved.
        /// </summary>
        /// <param name="cdaViewModel">The cda view model.</param>
        void RecordSavingOfClinicalDataFile(CdaViewModel cdaViewModel);

        string PostHealthSummary2Portal(CdaViewModel cdaViewModel);
    }

    internal class ClinicalDataFilesEditAndTransmitViewService : BaseViewService, IClinicalDataFilesEditAndTransmitViewService
    {
        private readonly IMapper<UserAppointment, SelectedAppointmentViewModel>
            _selectedAppointmentViewModelMapper;

        private readonly CdaMessageProducer _cdaMessageProducer;

        public ClinicalDataFilesEditAndTransmitViewService(IMapper<UserAppointment, SelectedAppointmentViewModel> selectedAppointmentViewModelMapper,
                                                           CdaMessageProducer cdaMessageProducer)
        {
            _selectedAppointmentViewModelMapper = selectedAppointmentViewModelMapper;
            _cdaMessageProducer = cdaMessageProducer;
        }

        #region IClinicalDataFilesEditAndTransmitViewService Members

        public IEnumerable<SelectedAppointmentViewModel> GetAppointments(List<int> appointmentIds = null)
        {
            IQueryable<UserAppointment> appointments = PracticeRepository.Appointments.OfType<UserAppointment>()
                .Include(ap => ap.Encounter.Patient)
                .Include(ap => ap.User)
                .Include(ap => ap.AppointmentType);

            if (appointments != null)
            {
                if (appointmentIds != null)
                {
                    appointments = appointments.Where(ap => appointmentIds.Contains(ap.Id));
                }
                UserAppointment[] results = appointments.Where(ap => ap.Encounter.EncounterStatusId == (int) EncounterStatus.Discharged || ap.Encounter.EncounterStatusId <= (int)EncounterStatus.Checkout).ToArray();

                PracticeRepository.AsQueryableFactory().Load(results,
                                                             ap => ap.Encounter
                                                             ,
                                                             ap =>
                                                             ap.Encounter.Patient.ExternalProviders.Select(
                                                                 ep => ep.ExternalProvider)
                                                             , ap => ap.Encounter.ServiceLocation);

                SelectedAppointmentViewModel[] viewModel =
                    results.Select(app => _selectedAppointmentViewModelMapper.Map(app)).OrderBy(ap => ap.LastName).ToArray();
                return viewModel;
            }
            return null;
        }

        public CdaViewModel GetEncounterClinicalSummaryCda(SelectedAppointmentViewModel selectedAppointmentViewModel)
        {
            if (selectedAppointmentViewModel.Id > 0)
            {
                var externalSystemMessage = CreateTestExternalSystemMessage(ExternalSystemMessageTypeId.EncounterClinicalSummaryCCDA_Outbound, PracticeRepositoryEntityId.Appointment, selectedAppointmentViewModel.Id.ToString());
                var request = new ProduceMessageRequest { ExternalSystemMessage = externalSystemMessage };
                _cdaMessageProducer.ProduceMessage(request);
                var viewModel = new CdaViewModel
                    {
                        Id = selectedAppointmentViewModel.Id,
                        CdaXml = request.ProducedMessage.ToString(),
                        IsClinicalSummary = true,
                        ServiceLocation = selectedAppointmentViewModel.ServiceLocation,
                        Resource = selectedAppointmentViewModel.ScheduledDoctor
                    };
                return viewModel;
            }
            return null;
        }

        public CdaViewModel GetPatientTransitionOfCareCda(SelectedAppointmentViewModel selectedAppointmentViewModel)
        {
            if (selectedAppointmentViewModel.Id > 0)
            {
                var externalSystemMessage = CreateTestExternalSystemMessage(ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Outbound, PracticeRepositoryEntityId.Patient, selectedAppointmentViewModel.PatientId.ToString());
                var request = new ProduceMessageRequest { ExternalSystemMessage = externalSystemMessage };
                _cdaMessageProducer.ProduceMessage(request);
                var viewModel = new CdaViewModel
                    {
                        Id = selectedAppointmentViewModel.PatientId,
                        CdaXml = request.ProducedMessage.ToString(),
                        IsClinicalSummary = false,
                        ServiceLocation = selectedAppointmentViewModel.ServiceLocation,
                        Resource = selectedAppointmentViewModel.ScheduledDoctor
                    };
                return viewModel;
            }
            return null;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void RecordPrintingOfClinicalDataFile(CdaViewModel cdaViewModel)
        {
            var externalSystemMessageType = cdaViewModel.IsClinicalSummary ? ExternalSystemMessageTypeId.EncounterClinicalSummaryCCDA_Outbound : ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Outbound;

            var externalSystemExternalSystemMessageTypeId =
                PracticeRepository.ExternalSystemExternalSystemMessageTypes
                    .Where(e => e.ExternalSystemMessageTypeId == (int) externalSystemMessageType
                        // only printer destinations
                                && e.ExternalSystemId == (int) ExternalSystemId.Printer)
                    .Select(e => e.Id).Single();

            CreateExternalSystemMessage(cdaViewModel, DateTime.UtcNow, externalSystemMessageType, externalSystemExternalSystemMessageTypeId);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void RecordSavingOfClinicalDataFile(CdaViewModel cdaViewModel)
        {
            var externalSystemMessageType = cdaViewModel.IsClinicalSummary ? ExternalSystemMessageTypeId.EncounterClinicalSummaryCCDA_Outbound : ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Outbound;

            var externalSystemExternalSystemMessageTypeId =
                PracticeRepository.ExternalSystemExternalSystemMessageTypes
                    .Where(e => e.ExternalSystemMessageTypeId == (int)externalSystemMessageType
                        // only saved destinations
                                && e.ExternalSystemId == (int)ExternalSystemId.Saved)
                    .Select(e => e.Id).Single();

            CreateExternalSystemMessage(cdaViewModel, DateTime.UtcNow, externalSystemMessageType, externalSystemExternalSystemMessageTypeId);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void TransmitClinicalDataFile(CdaViewModel cdaViewModel)
        {
            var externalSystemMessageType = cdaViewModel.IsClinicalSummary ? ExternalSystemMessageTypeId.EncounterClinicalSummaryCCDA_Outbound : ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Outbound;
            var dateTime = DateTime.UtcNow;

            List<int> externalSystemExternalSystemMessageTypeIds =
                PracticeRepository.ExternalSystemExternalSystemMessageTypes
                .Where(e => e.ExternalSystemMessageTypeId == (int)externalSystemMessageType
                    // only electronic transmission destinations
                    && e.ExternalSystemId != (int) ExternalSystemId.Printer && e.ExternalSystemId != (int) ExternalSystemId.Saved)
                .Select(e => e.Id).ToList();

            foreach (int externalSystemExternalSystemMessageTypeId in externalSystemExternalSystemMessageTypeIds)
            {
                CreateExternalSystemMessage(cdaViewModel, dateTime, externalSystemMessageType, externalSystemExternalSystemMessageTypeId);
            }
        }

        private void CreateExternalSystemMessage(CdaViewModel cdaViewModel, DateTime dateTime, ExternalSystemMessageTypeId externalSystemMessageType, int externalSystemExternalSystemMessageTypeId)
        {
            var message = new ExternalSystemMessage
                          {
                              CreatedBy = UserContext.Current.UserDetails.DisplayName,
                              CreatedDateTime = dateTime,
                              UpdatedDateTime = dateTime,
                              Description = externalSystemMessageType.ToString(),
                              ExternalSystemExternalSystemMessageTypeId = externalSystemExternalSystemMessageTypeId,
                              ExternalSystemMessageProcessingStateId = (int)ExternalSystemMessageProcessingStateId.Unprocessed,
                              Value = cdaViewModel.CdaXml
                          };
            message.ExternalSystemMessagePracticeRepositoryEntities.Add(new ExternalSystemMessagePracticeRepositoryEntity
                                                                        {
                                                                            PracticeRepositoryEntityId = (int)(cdaViewModel.IsClinicalSummary ? PracticeRepositoryEntityId.Appointment : PracticeRepositoryEntityId.Patient),
                                                                            PracticeRepositoryEntityKey = cdaViewModel.Id.ToString()
                                                                        });
            PracticeRepository.Save(message);
        }

        private static ExternalSystemMessage CreateTestExternalSystemMessage(ExternalSystemMessageTypeId externalSystemMessageTypeId, PracticeRepositoryEntityId practiceRepositoryEntityId, string practiceRepositoryEntityKey)
        {
            var externalSystemMessagePracticeRepositoryEntities = new List<ExternalSystemMessagePracticeRepositoryEntity>();
            var externalSystemMessagePracticeRepositoryEntity = new ExternalSystemMessagePracticeRepositoryEntity { PracticeRepositoryEntityId = (int)practiceRepositoryEntityId, PracticeRepositoryEntityKey = practiceRepositoryEntityKey };
            externalSystemMessagePracticeRepositoryEntities.Add(externalSystemMessagePracticeRepositoryEntity);
            var externalSystemExternalSystemMessageType = new ExternalSystemExternalSystemMessageType { ExternalSystemMessageTypeId = (int)externalSystemMessageTypeId };
            var externalSystemMessage = new ExternalSystemMessage { ExternalSystemExternalSystemMessageType = externalSystemExternalSystemMessageType, ExternalSystemMessagePracticeRepositoryEntities = externalSystemMessagePracticeRepositoryEntities };
            return externalSystemMessage;
        }

        public string PostHealthSummary2Portal(CdaViewModel cdaViewModel)
        {
            string retStatus = string.Empty;
            string _practiceToken = PracticeRepository.ApplicationSettings.Where(a => a.Name == "PracticeAuthToken").FirstOrDefault().Value;
            string _messageId = PracticeRepository.ExternalSystemMessages.Where(a => a.Description == "PatientTransitionOfCareCCDA_Outbound").OrderByDescending(a => a.CreatedDateTime).FirstOrDefault().Id.ToString();
            string _locationId = PracticeRepository.ServiceLocations.FirstOrDefault(a => a.ShortName == cdaViewModel.ServiceLocation).Id.ToString();
            string _resourceId = PracticeRepository.Users.FirstOrDefault(a => a.DisplayName == cdaViewModel.Resource).Id.ToString();

            var parameters = new CDAMessage()
            {
                PracticeToken = _practiceToken,
                ExternalId = _messageId,
                CDAxml = cdaViewModel.CdaXml,
                PatientExternalId = cdaViewModel.Id.ToString(),
                LocationExternalId =_locationId,
                DoctorExternalId = _resourceId
            };
            var portalSvc = new PortalServices();
            retStatus = portalSvc.PostHealthSummary(parameters);
            return retStatus;
        }

        #endregion
    }

    /// <summary>
    ///   Maps a Appointments to a SelectedAppointmentViewModel.
    /// </summary>
    internal class SelectedAppointmentViewModelMap : IMap<UserAppointment, SelectedAppointmentViewModel>
    {
        private readonly IMapMember<UserAppointment, SelectedAppointmentViewModel>[] _members;

        public SelectedAppointmentViewModelMap()
        {
            _members = this
                .CreateMembers(source =>
                               new SelectedAppointmentViewModel
                               {
                                   Id = source.Id,
                                   LastName = source.Encounter.Patient.LastName,
                                   FirstName = source.Encounter.Patient.FirstName,
                                   PatientId = source.Encounter.PatientId,
                                   AppointmentDate = source.DateTime,
                                   ScheduledDoctor = source.User.DisplayName,
                                   ServiceLocation = source.Encounter.ServiceLocation.ShortName,
                                   AppointmentType = source.AppointmentType.Name,
                                   ReferringDoctor =
                                       source.Encounter.Patient.ExternalProviders.Where(e => e.IsReferringPhysician).Select(
                                           e =>
                                           e.ExternalProvider != null
                                               ? e.ExternalProvider.DisplayName
                                               : string.Empty).FirstOrDefault(),
                                   PrimaryCarePhysician = source.Encounter.Patient.ExternalProviders.Where(e => e.IsPrimaryCarePhysician).Select(
                                       e =>
                                       e.ExternalProvider != null
                                           ? e.ExternalProvider.DisplayName
                                           : string.Empty).FirstOrDefault()
                               }).ToArray();
        }

        #region IMap<UserAppointment,SelectedAppointmentViewModel> Members

        public IEnumerable<IMapMember<UserAppointment, SelectedAppointmentViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }
}