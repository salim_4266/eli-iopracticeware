﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Integration.Messaging;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using Soaf.Collections;
using Soaf.ComponentModel;

[assembly: Component(typeof(GenerateQrdaViewService), typeof(IGenerateQrdaViewService))]

namespace IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles
{
    public interface IGenerateQrdaViewService
    {
        GenerateQrdaViewLoadInformation GetLoadInformation();
        /// <summary>
        /// Finds the patients that qualify for QRDA Category I files based on input criteria.
        /// </summary>
        /// <param name="measureId">The measure identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        ICollection<NamedViewModel> FindPatients(int measureId, int userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Generates QRDA Category I files.
        /// </summary>
        /// <param name="patientIds">The patient ids.</param>
        /// <param name="measureId">The measure identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        ICollection<QrdaCategory1ViewModel> GenerateQrdaCategory1Files(int[] patientIds, int measureId, int userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Generates the QRDA Category III report.
        /// </summary>
        /// <param name="patientIds">The patient ids.</param>
        /// <param name="measureIds">The measure ids.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        QrdaCategory3ViewModel GenerateQrdaCategory3Report(int[] patientIds, int[] measureIds, int userId, DateTime startDate, DateTime endDate);
    }

    public class GenerateQrdaViewService : BaseViewService, IGenerateQrdaViewService
    {
        private readonly QrdaCategory1MessageProducer _qrdaCategory1MessageProducer;
        private readonly QrdaCategory3MessageProducer _qrdaCategory3MessageProducer;
        private IMapper<Tuple<int, IMessage>, QrdaCategory1ViewModel> _qrdaCategory1Mapper;
        private IMapper<Tuple<IMessage, ICollection<QrdaCategory1ViewModel>>, QrdaCategory3ViewModel> _qrdaCategory3Mapper;
        private IMapper<IO.Practiceware.Model.Patient, NamedViewModel> _patientMapper;

        public GenerateQrdaViewService(QrdaCategory1MessageProducer qrdaCategory1MessageProducer,
            QrdaCategory3MessageProducer qrdaCategory3MessageProducer)
        {
            _qrdaCategory1MessageProducer = qrdaCategory1MessageProducer;
            _qrdaCategory3MessageProducer = qrdaCategory3MessageProducer;

            InitMappers();
        }

        private void InitMappers()
        {
            _qrdaCategory1Mapper = Mapper.Factory.CreateMapper<Tuple<int, IMessage>, QrdaCategory1ViewModel>(m => new QrdaCategory1ViewModel
            {
                PatientId = m.Item1,
                QrdaXml = m.Item2.ToString()
            });
            _qrdaCategory3Mapper = Mapper.Factory.CreateMapper<Tuple<IMessage, ICollection<QrdaCategory1ViewModel>>, QrdaCategory3ViewModel>(m => new QrdaCategory3ViewModel
            {
                QrdaXml = m.Item1.ToString(),
                PatientsSource = m.Item2.ToExtendedObservableCollection()
            });
            _patientMapper = Mapper.Factory.CreateMapper<IO.Practiceware.Model.Patient, NamedViewModel>(p => new NamedViewModel
            {
                Id = p.Id,
                Name = p.DisplayName
            });
        }

        public GenerateQrdaViewLoadInformation GetLoadInformation()
        {
            var result = new GenerateQrdaViewLoadInformation
                         {
                             Users = PracticeRepository.Providers.Where(p => p.User != null)
                                 .Select(p => p.User).Distinct().Select(u => new NamedViewModel {Id = u.Id, Name = u.UserName}).OrderBy(i => i.Name).ToObservableCollection(),
                             Measures = NamedViewModelExtensions.ToNamedViewModel<ElectronicClinicalQualityMeasure>().ToObservableCollection()
                         };

            return result;
        }

        public ICollection<NamedViewModel> FindPatients(int measureId, int userId, DateTime startDate, DateTime endDate)
        {
            var patientIds = _qrdaCategory1MessageProducer.FindPatients(measureId, userId, startDate, endDate);

            return PracticeRepository.Patients.Where(p => patientIds.Contains(p.Id))
                .ToArray()
                .Select(_patientMapper.Map)
                .ToExtendedObservableCollection();
        }

        public ICollection<QrdaCategory1ViewModel> GenerateQrdaCategory1Files(int[] patientIds, int measureId, int userId, DateTime startDate, DateTime endDate)
        {
            var results = new ExtendedObservableCollection<QrdaCategory1ViewModel>();
            foreach (var id in patientIds)
            {
                var message = _qrdaCategory1MessageProducer.ProduceMessage(id, measureId, userId, startDate, endDate);

                var viewModel = _qrdaCategory1Mapper.Map(new Tuple<int, IMessage>(id, message));
                results.Add(viewModel);
            }

            return results.ToExtendedObservableCollection();
        }

        public QrdaCategory3ViewModel GenerateQrdaCategory3Report(int[] patientIds, int[] measureIds, int userId, DateTime startDate, DateTime endDate)
        {
            // Export Cat 1 files
            var category1ViewModels = new List<QrdaCategory1ViewModel>();
            var category1Messages = new List<IMessage>();
            var measures = measureIds.Select(i => (ElectronicClinicalQualityMeasure) i).ToArray();
            foreach (var id in patientIds)
            {
                var message = _qrdaCategory1MessageProducer.ProduceMessage(id, measures, userId, startDate, endDate);
                category1Messages.Add(message);

                var viewModel = _qrdaCategory1Mapper.Map(new Tuple<int, IMessage>(id, message));
                category1ViewModels.Add(viewModel);
            }

            // Export report
            var report = _qrdaCategory3MessageProducer.ProduceMessage(userId, startDate, endDate, measures, category1Messages);

            // Map to view model and return
            var result = _qrdaCategory3Mapper.Map(new Tuple<IMessage, ICollection<QrdaCategory1ViewModel>>(report, category1ViewModels));
            return result;
        }
    }
}