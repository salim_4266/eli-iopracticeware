﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Application;
using IO.Practiceware.Integration.Cda;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf;
using System.Xml;
using System.Text.RegularExpressions;
using System.Text;

[assembly: Component(typeof(ClinicalDataFilesLinkFilesViewService), typeof(IClinicalDataFilesLinkFilesViewService))]

namespace IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles
{
    public interface IClinicalDataFilesLinkFilesViewService
    {
        /// <summary>
        /// Saves a set of cda xml files in the external system messages table as an outbound message.
        /// </summary>
        /// <param name="clinicalDataFiles"></param>
        void Save(IEnumerable<ImportAndLinkClinicalDataFileViewModel> clinicalDataFiles);
    }

    public class ClinicalDataFilesLinkFilesViewService : BaseViewService, IClinicalDataFilesLinkFilesViewService
    {
        private readonly IPatientImageImporterService _patientImageImporter;

        public ClinicalDataFilesLinkFilesViewService(IPatientImageImporterService patientImageImporter)
        {
            _patientImageImporter = patientImageImporter;
        }

        public void Save(IEnumerable<ImportAndLinkClinicalDataFileViewModel> clinicalDataFiles)
        {
            var listOfClinicalFiles = clinicalDataFiles.Where(cdf => cdf.PatientId.HasValue).ToList();

            // save pdf versions of all clinical files
            var listOfViewModelsWithPdf = listOfClinicalFiles.Where(cdf => cdf.PdfDocument.IsNotNullOrEmpty());
            foreach (var viewModelWithPdf in listOfViewModelsWithPdf)
            {
                _patientImageImporter.Import(new[]
                                                        {
                                                            new PatientDocument
                                                                {                                                                    
                                                                    FileType = viewModelWithPdf.ClinicalDataFileType.GetDisplayName(),
                                                                    PatientId = viewModelWithPdf.PatientId.EnsureNotDefault("Patient Id has not been set.").Value,
                                                                    FileExtension = ".pdf",
                                                                    DateTime = DateTime.Now.ToClientTime(),
                                                                    Pages = {new PatientDocumentBinaryPage(viewModelWithPdf.PdfDocument)}
                                                                }
                                                        }, true, false, onError: e => { throw new InvalidOperationException(e.Message); });
            }

            // only save c-cda type files to the external system messages
            var externalMessagesToCreate = listOfClinicalFiles
                .Where(cda => cda.ClinicalDataFileType == ClinicalDataFileType.Ccda && !cda.XmlContent.IsNullOrEmpty())
                .Select(CreateExternalSystemMessageInbound).ToArray();

            // save the external system messages
            externalMessagesToCreate.ForEachWithSinglePropertyChangedNotification(PracticeRepository.Save);
        }

        private ExternalSystemMessage CreateExternalSystemMessageInbound(ImportAndLinkClinicalDataFileViewModel viewModel)
        {
            return CreateExternalSystemMessage(viewModel, ExternalSystemMessageTypeId.PatientTransitionOfCareCCDA_Inbound);
        }

        private ExternalSystemMessage CreateExternalSystemMessage(ImportAndLinkClinicalDataFileViewModel viewModel, ExternalSystemMessageTypeId externalSystemMessageTypeId)
        {
            var externalSystemMessagePracticeRepositoryEntities = new List<ExternalSystemMessagePracticeRepositoryEntity>();
            var externalSystemMessagePracticeRepositoryEntity = new ExternalSystemMessagePracticeRepositoryEntity { PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.Patient, PracticeRepositoryEntityKey = viewModel.PatientId.ToString() };
            externalSystemMessagePracticeRepositoryEntities.Add(externalSystemMessagePracticeRepositoryEntity);

            // get the type
            var externalSystemExternalSystemMessageType = PracticeRepository.ExternalSystemExternalSystemMessageTypes
                .FirstOrDefault(e => e.ExternalSystemId == (int)ExternalSystemId.IOPracticeware
                    && e.ExternalSystemMessageTypeId == (int)externalSystemMessageTypeId
                    && !e.IsDisabled);

            if (externalSystemExternalSystemMessageType == null)
            {
                throw new InvalidOperationException(string.Format("Message type {0} is not registered with IO Practiceware system or is disabled", externalSystemMessageTypeId));
            }

            // Get description from C-CDA
            //var cdaFile = viewModel.XmlContent.FromXml<POCD_MT000040ClinicalDocument>();
            //var cdaFile = viewModel.XmlContent.FromXml<ClinicalDocument.ClinicalDocument>();
            
            // create the message and set the required properties
            var externalSystemMessage = new ExternalSystemMessage
            {
                ExternalSystemExternalSystemMessageType = externalSystemExternalSystemMessageType,
                ExternalSystemMessagePracticeRepositoryEntities = externalSystemMessagePracticeRepositoryEntities,
                Value = viewModel.XmlContent,
                Description = string.Empty,//doc.SelectSingleNode("//ClinicalDocument/recordTarget/patientRole/providerOrganization").InnerText,
                CreatedBy = UserContext.Current.UserDetails.Id.ToString(),
                CreatedDateTime = DateTime.Now.ToClientTime(),
                UpdatedDateTime = DateTime.Now.ToClientTime(),
                ExternalSystemMessageProcessingStateId = (int)ExternalSystemMessageProcessingStateId.Unprocessed
            };
            return externalSystemMessage;
        }   
    }
}
