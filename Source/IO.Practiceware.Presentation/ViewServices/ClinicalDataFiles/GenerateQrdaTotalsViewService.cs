﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.ClinicalDataFiles;
using IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;

[assembly: Component(typeof(GenerateQrdaTotalsViewService), typeof(IGenerateQrdaTotalsViewService))]

namespace IO.Practiceware.Presentation.ViewServices.ClinicalDataFiles
{
    public interface IGenerateQrdaTotalsViewService
    {
        /// <summary>
        /// Finds the patients that qualify for QRDA Category I files based on input criteria.
        /// </summary>
        /// <param name="electronicClinicalQualityMeasure">The measure identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        ICollection<QrdaSearchResultViewModel> FindResults(ElectronicClinicalQualityMeasure electronicClinicalQualityMeasure, int userId, DateTime startDate, DateTime endDate);

    }

    public class GenerateQrdaTotalsViewService : BaseViewService, IGenerateQrdaTotalsViewService
    {
        public ICollection<QrdaSearchResultViewModel> FindResults(ElectronicClinicalQualityMeasure electronicClinicalQualityMeasure, int userId, DateTime startDate, DateTime endDate)
        {
            Type t = typeof(QrdaSearchResultViewModel);
            var dataTable = new DataTable(t.Name);
            foreach (PropertyInfo pi in t.GetProperties())
            {
                dataTable.Columns.Add(new DataColumn(pi.Name, pi.GetType()));
            }
            

            var parameters = new Dictionary<string, object>();
            parameters.Add("@StartDate", startDate);
            parameters.Add("@EndDate", endDate);
            parameters.Add("@UserId", userId);

            var createCommand = new Func<IDbCommand>(() =>
            {
                var c = DbConnectionFactory.PracticeRepository.CreateCommand();
                c.CommandTimeout = 1200;
                c.CommandType = CommandType.StoredProcedure;
                return c;
            });

            if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.BmiPop1)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_BMIPop1_CMS069v3", parameters, false, createCommand);
            else if(electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.BmiPop2)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_BMIPop2_CMS069v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.TobaccoScreening)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_Tobacco_CMS138v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DiabetesEyeExam)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_DiabetesEyeExam_CMS131v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DiabeticRetinopathyDocumentation)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_DRDocumentation_CMS167v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DiabeticRetinopathyCommunication)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_DRCommunication_CMS142v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.Poag)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_POAG_CMS143v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.DocumentationOfMedications)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_CurrentMeds_CMS068v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.HighRiskMedications1)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_HighRiskMeds_CMS156v3_Numerator1", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.HighRiskMedications2)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_HighRiskMeds_CMS156v3_Numerator2", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.ControllingHighBloodPressure)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_HighBloodPressure_CMS165v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.CloseTheReferralLoop)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_ReferralLoop_CMS050v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.CataractComplications)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_CataractsComplications_CMS132v3", parameters, false, createCommand);
            else if (electronicClinicalQualityMeasure == ElectronicClinicalQualityMeasure.CataractSurgeryVisualAcuity)
                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("qrda.QRDA_CataractsVA_CMS133v3", parameters, false, createCommand);
            dataTable.Columns.Add("NumeratorExclusions", typeof(System.Boolean));
            foreach (DataRow dr in dataTable.Rows)
            {
                dr["NumeratorExclusions"] = false;
            }
            var results = new ExtendedObservableCollection<QrdaSearchResultViewModel>();
            if(dataTable.Rows.Count > 0)
                results = dataTable.AsEnumerable().Select(i => i.MapTo<QrdaSearchResultViewModel>()).ToExtendedObservableCollection();
            return results;
        }
    }
}
