﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.NonClinicalPatient;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.NonClinicalPatient;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;

[assembly: Component(typeof(NonClinicalPatientViewService), typeof(INonClinicalPatientViewService))]
[assembly: Component(typeof(NonClinicalPatientViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(EmployerViewModelMap), AddAllServices = true)]

namespace IO.Practiceware.Presentation.ViewServices.NonClinicalPatient
{
    [DataContract]
    [KnownType(typeof(ExtendedObservableCollection<StateViewModel>))]
    [KnownType(typeof(ExtendedObservableCollection<NamedViewModel>))]
    [KnownType(typeof(NonClinicalPatientViewModel))]
    public class NonClinicalPatientLoadInformation
    {
        [DataMember]
        public NonClinicalPatientViewModel NonClinicalPatient { get; set; }

        [Dependency]
        [DataMember]
        public ObservableCollection<StateOrProvinceViewModel> StateOrProvinces { get; set; }

        [Dependency]
        [DataMember]
        public ObservableCollection<NamedViewModel> PhoneNumberTypes { get; set; }

        [Dependency]
        [DataMember]
        public ObservableCollection<NamedViewModel> EmailAddressTypes { get; set; }

        [Dependency]
        [DataMember]
        public ObservableCollection<NamedViewModel> EmploymentStatuses { get; set; }

        [Dependency]
        [DataMember]
        public ObservableCollection<NamedViewModel> GenderTypes { get; set; }

    }

    public interface INonClinicalPatientViewService
    {
        NonClinicalPatientLoadInformation GetLoadInformation(int? nonClinicalPatientId);
        AddressViewModel GetPatientAddress(int patientId);
        int SaveNonClinicalPatient(NonClinicalPatientViewModel nonClinicalPatient);
        NonClinicalPatientViewModel GetPatientBySocialSecurity(string socialSecurity);
    }

    [SupportsUnitOfWork]
    public class NonClinicalPatientViewService : BaseViewService, INonClinicalPatientViewService
    {
        private readonly IMapper<Patient, NonClinicalPatientViewModel> _nonClinicalPatientMapper;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly IMapper<PatientAddress, AddressViewModel> _addressMapper;
        private readonly Func<NamedViewModel> _createNamedViewModel;
        private readonly IMapper<StateOrProvince, StateOrProvinceViewModel> _stateOrProvinceMapper;

        public NonClinicalPatientViewService(IUnitOfWorkProvider unitOfWorkProvider, ICustomMapperFactory mapperFactory,
                                              Func<IMapper<PatientPhoneNumber, PhoneNumberViewModel>, IMapper<Patient, NonClinicalPatientViewModel>> createNonClinicalPatientMapper,
                                              Func<NamedViewModel> createNamedViewModel)
        {
            var phoneNumberMapper = mapperFactory.CreatePhoneViewModelMapper<PatientPhoneNumber>();
            _nonClinicalPatientMapper = createNonClinicalPatientMapper(phoneNumberMapper);

            _unitOfWorkProvider = unitOfWorkProvider;
            _addressMapper = mapperFactory.CreateAddressViewModelMapper<PatientAddress>();
            _stateOrProvinceMapper = mapperFactory.CreateStateOrProvinceViewModelMapper();
            _createNamedViewModel = createNamedViewModel;
        }


        public virtual int SaveNonClinicalPatient(NonClinicalPatientViewModel nonClinicalPatient)
        {
            Patient databasePatient;

            using (var scope = _unitOfWorkProvider.GetUnitOfWorkScope())
            {
                databasePatient = nonClinicalPatient.Id.HasValue ? PracticeRepository
                    .Patients.Include(p => p.PatientAddresses.Select(a => a.StateOrProvince),
                        p => p.PatientEmploymentStatus,
                        p => p.PatientPhoneNumbers,
                        p => p.PatientEmailAddresses)
                    .WithId(nonClinicalPatient.Id.Value)
                    : new Patient();

                var defaultPhoneNumberType = (int)PatientPhoneNumberType.Home;

                var addressViewModel = nonClinicalPatient.Address;
                var databasePatientAddress = databasePatient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.Add);
                if (addressViewModel != null && !addressViewModel.IsEmpty)
                {
                        databasePatientAddress.City = addressViewModel.City;
                        databasePatientAddress.Line1 = addressViewModel.Line1;
                        databasePatientAddress.Line2 = addressViewModel.Line2;
                        databasePatientAddress.Line3 = addressViewModel.Line3;
                        databasePatientAddress.PostalCode = addressViewModel.PostalCode;
                        databasePatientAddress.StateOrProvinceId = addressViewModel.StateOrProvince.Id;
                }
                else
                {
                    databasePatient.PatientAddresses.Remove(databasePatientAddress);
                    PracticeRepository.Delete(databasePatientAddress);
                }
                var databaseEmployerAddress = databasePatient.PatientAddresses.WithAddressType(PatientAddressTypeId.Business, NotFoundBehavior.Add);
                if (nonClinicalPatient.Employer != null && nonClinicalPatient.Employer.Address != null && !nonClinicalPatient.Employer.Address.IsEmpty)
                {
                 
                        var employerAddressViewModel = nonClinicalPatient.Employer.Address;

                        databaseEmployerAddress.City = employerAddressViewModel.City;
                        databaseEmployerAddress.Line1 = employerAddressViewModel.Line1;
                        databaseEmployerAddress.Line2 = employerAddressViewModel.Line2;
                        databaseEmployerAddress.Line3 = employerAddressViewModel.Line3;
                        databaseEmployerAddress.PostalCode = employerAddressViewModel.PostalCode;
                        databaseEmployerAddress.StateOrProvinceId = employerAddressViewModel.StateOrProvince.Id;
                }
                else
                {
                    databasePatient.PatientAddresses.Remove(databaseEmployerAddress);
                    PracticeRepository.Delete(databaseEmployerAddress);
                }

                // Sync phone numbers
                // Main PhoneNumber can be representing an empty one, so check that it has value
                var viewModelPhoneNumbers = new List<PhoneNumberViewModel>();
                if (nonClinicalPatient.PhoneNumber != null && !nonClinicalPatient.PhoneNumber.IsEmpty)
                {
                    viewModelPhoneNumbers.Add(nonClinicalPatient.PhoneNumber);
                }

                PracticeRepository.SyncPhoneNumbers(viewModelPhoneNumbers, databasePatient.PatientPhoneNumbers,
                    number => (int)number.PatientPhoneNumberType, (number, i) => number.PatientPhoneNumberType = i,
                    defaultPhoneNumberType);

                databasePatient.DateOfBirth = nonClinicalPatient.DateOfBirth;
                databasePatient.EmployerName = nonClinicalPatient.Employer != null ? nonClinicalPatient.Employer.Name : null;
                databasePatient.FirstName = nonClinicalPatient.FirstName;
                databasePatient.LastName = nonClinicalPatient.LastName;
                databasePatient.MiddleName = nonClinicalPatient.MiddleName;
                databasePatient.Occupation = nonClinicalPatient.Occupation;
                databasePatient.PatientEmploymentStatusId = nonClinicalPatient.EmploymentStatus == null ? (int?)null : nonClinicalPatient.EmploymentStatus.Id;
                databasePatient.SocialSecurityNumber = nonClinicalPatient.SocialSecurity;
                databasePatient.Suffix = nonClinicalPatient.Suffix;
                if (nonClinicalPatient.SelectedGender != null)
                {
                    databasePatient.Gender = (Gender)nonClinicalPatient.SelectedGender.Id; 
                }

                if (nonClinicalPatient.EmailAddress == null || nonClinicalPatient.EmailAddress.IsEmpty)
                {
                    databasePatient.PatientEmailAddresses.ToArray().ForEach(PracticeRepository.Delete);
                    databasePatient.PatientEmailAddresses.Clear();
                }
                else
                {
                    var emailAddress = databasePatient.PatientEmailAddresses.FirstOrDefault();
                    if (emailAddress == null)
                    {
                        emailAddress = new PatientEmailAddress();
                        databasePatient.PatientEmailAddresses.Add(emailAddress);
                    }
                    emailAddress.OrdinalId = 1;
                    emailAddress.Value = nonClinicalPatient.EmailAddress.Value;
                    emailAddress.EmailAddressTypeId = nonClinicalPatient.EmailAddress.EmailAddressType.Id;
                }
                databasePatient.PatientStatus = PatientStatus.Active;
                var mainBillingOrganization = PracticeRepository.BillingOrganizations.FirstOrDefault(x => !x.IsArchived && x.IsMain);
                if (mainBillingOrganization != null) databasePatient.BillingOrganizationId = mainBillingOrganization.Id;

                // don't revert clinical patients to non-clinical - they may just be editing a clinical patient in the non-clinical view
                if (databasePatient.Id == 0) databasePatient.IsClinical = false;

                PracticeRepository.Save(databasePatient);

                scope.UnitOfWork.AcceptChanges();
            }

            return databasePatient.Id;
        }

        public AddressViewModel GetPatientAddress(int patientId)
        {
            var databasePatient = PracticeRepository.Patients
                .Include(p => p.PatientAddresses.Select(pa => new { pa.StateOrProvince.Country, pa.PatientAddressType }))
                .WithId(patientId);

            var databaseAddress = databasePatient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home);
            return databaseAddress == null ? null : _addressMapper.Map(databaseAddress);
        }

        public NonClinicalPatientViewModel GetPatientBySocialSecurity(string socialSecurity)
        {
            var databasePatient = PracticeRepository
                .Patients.Include(p => p.PatientAddresses.Select(a => a.StateOrProvince),
                                  p => p.PatientEmploymentStatus,
                                  p => p.PatientPhoneNumbers)
                .FirstOrDefault(p => p.SocialSecurityNumber.Equals(socialSecurity));

            if (databasePatient == null) return null;

            NonClinicalPatientViewModel nonClinicalPatient = _nonClinicalPatientMapper.Map(databasePatient);
            var nonClinicalFieldConfigurations = PracticeRepository.PatientDemographicsFieldConfigurations.Where(p => p.Id == ((int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsSocialSecurityNumber) || p.Id == (int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsDateOfBirth).ToArray();
            var ssnFieldConfiguration = nonClinicalFieldConfigurations.FirstOrDefault(c => c.Id == (int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsSocialSecurityNumber);
            if (ssnFieldConfiguration != null) nonClinicalPatient.IsSocialSecurityRequired = ssnFieldConfiguration.IsRequired;
            var dobFieldConfiguration = nonClinicalFieldConfigurations.FirstOrDefault(c => c.Id == (int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsDateOfBirth);
            if (dobFieldConfiguration != null) nonClinicalPatient.IsDateOfBirthRequired = dobFieldConfiguration.IsRequired;
            return nonClinicalPatient;
        }

        public NonClinicalPatientLoadInformation GetLoadInformation(int? nonClinicalPatientId)
        {
            var loadInformation = new NonClinicalPatientLoadInformation();
            var parallelLoadActions = new List<Action>();

            parallelLoadActions.Add(() => loadInformation.EmploymentStatuses = GetEmploymentStatuses());
            parallelLoadActions.Add(() => loadInformation.PhoneNumberTypes = GetPhoneNumberTypes());
            parallelLoadActions.Add(() => loadInformation.EmailAddressTypes = GetEmailAddressTypes());
            parallelLoadActions.Add(() => loadInformation.GenderTypes = GetGenderTypes());
            parallelLoadActions.Add(() => loadInformation.NonClinicalPatient = GetNonClinicalPatient(nonClinicalPatientId));
            parallelLoadActions.Add(() => loadInformation.StateOrProvinces = PracticeRepository.StateOrProvinces.Include(i => i.Country).Select(_stateOrProvinceMapper.Map).ToObservableCollection());
            parallelLoadActions.ForAllInParallel(a => a());

            return loadInformation;
        }

        private NonClinicalPatientViewModel GetNonClinicalPatient(int? nonClinicalPatientId)
        {
            var databasePatient = nonClinicalPatientId.HasValue
                ? PracticeRepository.Patients.Include(p => p.PatientAddresses.Select(a => new { a.StateOrProvince.Country, a.PatientAddressType }),
                                                      p => p.PatientEmploymentStatus,
                                                      p => p.PatientPhoneNumbers,
                                                      p => p.PatientEmailAddresses)
                                                      .WithId(nonClinicalPatientId)
                : new Patient();

            NonClinicalPatientViewModel nonClinicalPatient = databasePatient != null ? _nonClinicalPatientMapper.Map(databasePatient) : new NonClinicalPatientViewModel();
            var nonClinicalFieldConfigurations = PracticeRepository.PatientDemographicsFieldConfigurations.Where(p => p.Id == ((int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsSocialSecurityNumber) || p.Id == (int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsDateOfBirth).ToArray();
            var ssnFieldConfiguration = nonClinicalFieldConfigurations.FirstOrDefault(c => c.Id == (int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsSocialSecurityNumber);
            if (ssnFieldConfiguration != null) nonClinicalPatient.IsSocialSecurityRequired = ssnFieldConfiguration.IsRequired;
            var dobFieldConfiguration = nonClinicalFieldConfigurations.FirstOrDefault(c => c.Id == (int)PatientDemographicsFieldConfigurationId.NonClinicalPatientsDateOfBirth);
            if (dobFieldConfiguration != null) nonClinicalPatient.IsDateOfBirthRequired = dobFieldConfiguration.IsRequired;
            return nonClinicalPatient;
        }

        private ObservableCollection<NamedViewModel> GetPhoneNumberTypes()
        {
            return NamedViewModelExtensions.ToNamedViewModel<PatientPhoneNumberType>().OrderBy(x => x.Name).ToExtendedObservableCollection();
        }

        private ObservableCollection<NamedViewModel> GetEmailAddressTypes()
        {
            return NamedViewModelExtensions.ToNamedViewModel<EmailAddressType>().OrderBy(x => x.Name).ToExtendedObservableCollection();
        }

        private ObservableCollection<NamedViewModel> GetGenderTypes()
        {
            return NamedViewModelExtensions.ToNamedViewModel<Gender>().OrderBy(x => x.Id).ToExtendedObservableCollection();
        }

        private ObservableCollection<NamedViewModel> GetEmploymentStatuses()
        {
            var result = new List<NamedViewModel>();
            var patientEmploymentStatuses = PracticeRepository.PatientEmploymentStatus.ToArray();
            foreach (var patientEmploymentStatus in patientEmploymentStatuses)
            {
                var viewModel = _createNamedViewModel();
                viewModel.Id = patientEmploymentStatus.Id;
                viewModel.Name = patientEmploymentStatus.Name;
                result.Add(viewModel);
            }

            return result.ToExtendedObservableCollection();
        }


    }

    public class NonClinicalPatientViewModelMap : IMap<Patient, NonClinicalPatientViewModel>
    {
        public NonClinicalPatientViewModelMap(ICustomMapperFactory customMapperFactory,
                                        IMapper<Patient, EmployerViewModel> employerMapper,
                                        IMapper<PatientPhoneNumber, PhoneNumberViewModel> phoneNumberMapper)
        {
            var addressMapper = customMapperFactory.CreateAddressViewModelMapper<PatientAddress>();

            Members = this.CreateMembers(i => new NonClinicalPatientViewModel
                {
                    Id = i.Id == 0 ? (int?)null : i.Id,
                    Address = i.PatientAddresses.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.None).IfNotNull(addressMapper.Map),
                    DateOfBirth = i.DateOfBirth.HasValue ? i.DateOfBirth.Value : new DateTime?(),
                    Employer = employerMapper.Map(i),
                    FirstName = i.FirstName,
                    LastName = i.LastName,
                    MiddleName = i.MiddleName,
                    Occupation = i.Occupation,
                    EmploymentStatus = i.PatientEmploymentStatus != null
                                                ? new NamedViewModel { Id = i.PatientEmploymentStatus.Id, Name = i.PatientEmploymentStatus.Name }
                                                : null,
                    PhoneNumber = i.PatientPhoneNumbers.Select(pn => phoneNumberMapper.Map(pn)).FirstOrDefault(),
                    SocialSecurity = i.SocialSecurityNumber,
                    Suffix = i.Suffix,
                    EmailAddress = i.PatientEmailAddresses.OrderBy(pea => pea.OrdinalId).Select(pea => new EmailAddressViewModel
                    {
                        Id = pea.Id,
                        Value = pea.Value,
                        EmailAddressType = new NamedViewModel { Id = (int)pea.EmailAddressType, Name = pea.EmailAddressType.GetDisplayName() },
                        PatientId = pea.PatientId,
                        IsPrimary = true,
                    }).FirstOrDefault(),
                    SelectedGender = i.Gender != null ? new NamedViewModel
                    {
                        Id = (int)i.Gender,
                        Name = i.Gender.GetDisplayName()
                    } : null
                }).ToArray();
        }

        public IEnumerable<IMapMember<Patient, NonClinicalPatientViewModel>> Members { get; private set; }
    }

    public class EmployerViewModelMap : IMap<Patient, EmployerViewModel>
    {
        public EmployerViewModelMap(ICustomMapperFactory customMapperFactory)
        {
            var addressMapper = customMapperFactory.CreateAddressViewModelMapper<PatientAddress>();

            Members = this.CreateMembers(i => new EmployerViewModel
                {
                    Name = i.EmployerName,
                    Address = i.PatientAddresses.Where(a => a.PatientAddressTypeId == (int)PatientAddressTypeId.Business).Select(addressMapper.Map).FirstOrDefault()
                }).ToArray();
        }

        public IEnumerable<IMapMember<Patient, EmployerViewModel>> Members { get; private set; }
    }

}
