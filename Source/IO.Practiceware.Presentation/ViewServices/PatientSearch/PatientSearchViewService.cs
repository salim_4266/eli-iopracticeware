using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.PatientSearch;
using IO.Practiceware.Presentation.ViewServices.PatientSearch;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Services.PatientSearch;
using Soaf.Collections;
using Soaf.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

[assembly: Component(typeof(PatientSearchViewService), typeof(IPatientSearchViewService))]
[assembly: Component(typeof(PatientSearchViewModelMap), typeof(IMap<PatientSearchResult, PatientSearchPatientViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.PatientSearch
{
    public interface IPatientSearchViewService
    {
        IEnumerable<PatientSearchPatientViewModel> Search(TextSearchMode textSearchMode, SearchFields searchFields, string searchText);

        IEnumerable<PatientSearchPatientViewModel> GetPatients(IEnumerable<int> patientIds);

        IEnumerable<PatientSearchPatientViewModel> SuggestRecentPatients(int userId, string userComputerName);

        void RecordPatientAccessed(int patientId, int userId, string userComputerName);
    }

    internal class PatientSearchViewService : BaseViewService, IPatientSearchViewService
    {
        private readonly IPatientSearchService _patientSearchService;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private readonly IMapper<PatientSearchResult, PatientSearchPatientViewModel> _patientViewModelMap;

        public PatientSearchViewService(IPatientSearchService patientSearchService,
            IApplicationSettingsService applicationSettingsService,
            IMapper<PatientSearchResult, PatientSearchPatientViewModel> patientViewModelMap)
        {
            _patientSearchService = patientSearchService;
            _applicationSettingsService = applicationSettingsService;
            _patientViewModelMap = patientViewModelMap;
        }

        public IEnumerable<PatientSearchPatientViewModel> Search(TextSearchMode textSearchMode, SearchFields searchFields, string searchText)
        {
            var results = _patientSearchService.Search(textSearchMode, searchFields, searchText, IncludedResultFields.PatientPhoneNumbers |
                                                                                                IncludedResultFields.PatientAddresses |
                                                                                                IncludedResultFields.DefaultUser |
                                                                                                IncludedResultFields.ExternalProviders |
                                                                                                IncludedResultFields.PatientTags).ToArray();
            return results.Select(_patientViewModelMap.Map).ToList();
        }

        public IEnumerable<PatientSearchPatientViewModel> GetPatients(IEnumerable<int> patientIds)
        {
            var results = _patientSearchService.GetPatients(patientIds).ToList();
            return results.Select(_patientViewModelMap.Map).ToList();
        }

        public IEnumerable<PatientSearchPatientViewModel> SuggestRecentPatients(int userId, string userComputerName)
        {
            var setting = _applicationSettingsService.GetSetting<List<int>>(
                ApplicationSetting.RecentlyAccessedPatients, userComputerName, userId);

            if (setting == null) return new Collection<PatientSearchPatientViewModel>();

            // Load top 5 recently accessed patients
            var patientIds = setting.Value.Take(5).ToList();
            var result = GetPatients(patientIds);

            // Re-order models accordingly
            result = result
                .OrderBy(p => p.Id, GenericComparer<int>.With((x, y) => patientIds.IndexOf(x) - patientIds.IndexOf(y)))
                .ToList();

            return result;
        }

        public void RecordPatientAccessed(int patientId, int userId, string userComputerName)
        {
            // Load existing record or prepare new
            var recentlyAccessedPatients = _applicationSettingsService.GetSetting<List<int>>(
                ApplicationSetting.RecentlyAccessedPatients, userComputerName, userId);

            if (recentlyAccessedPatients == null)
            {
                recentlyAccessedPatients = new ApplicationSettingContainer<List<int>>
                {
                    MachineName = userComputerName,
                    Name = ApplicationSetting.RecentlyAccessedPatients,
                    Value = new List<int>(),
                    Id = null,
                    UserId = userId
                };
            }

            // Record as latest accessed patient
            recentlyAccessedPatients.Value.Remove(patientId);
            recentlyAccessedPatients.Value.Insert(0, patientId);
            // Cut recent list to last 50 patients
            recentlyAccessedPatients.Value = recentlyAccessedPatients.Value.Take(50).ToList();

            // Save it
            _applicationSettingsService.SetSetting(recentlyAccessedPatients);
        }
    }

    public class PatientSearchViewModelMap : IMap<PatientSearchResult, PatientSearchPatientViewModel>
    {
        private readonly IEnumerable<IMapMember<PatientSearchResult, PatientSearchPatientViewModel>> _members;

        public PatientSearchViewModelMap()
        {
            _members = this.CreateMembers(pm => new PatientSearchPatientViewModel
            {
                CellPhone = pm.CellPhone,
                DateOfBirth = pm.DateOfBirth,
                DisplayName = pm.DisplayName,
                FirstName = pm.FirstName,
                HomePhone = pm.HomePhone,
                Id = pm.Id,
                LastAppointmentDate = pm.LastAppointmentDate,
                LastName = pm.LastName,
                ListOfTypeName = pm.ListOfTypeName.ToList(),
                MiddleName = pm.MiddleName,
                NextAppointmentDate = pm.NextAppointmentDate,
                PracticeDoctor = pm.PracticeDoctor,
                Prefix = pm.Prefix,
                PrimaryAddress = pm.PrimaryAddress,
                PrimaryCareDoctor = pm.PrimaryCareDoctor,
                PriorId = pm.PriorId,
                ReferringDoctor = pm.ReferringDoctor,
                Suffix = pm.Suffix,
                WorkPhone = pm.WorkPhone,
                IsClinical = pm.IsClinical
            }).ToArray();
        }

        public IEnumerable<IMapMember<PatientSearchResult, PatientSearchPatientViewModel>> Members
        {
            get { return _members; }
        }
    }
}