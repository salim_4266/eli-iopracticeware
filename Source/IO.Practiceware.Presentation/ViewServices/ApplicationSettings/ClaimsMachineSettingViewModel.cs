﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewServices.ApplicationSettings;
using Soaf.ComponentModel;
using Soaf.Presentation;

[assembly: Component(typeof(ClaimViewModelToClaimSettingMap), typeof(IMap<ClaimsMachineSettingViewModel, ApplicationSettingContainer>))]
[assembly: Component(typeof(ClaimSettingToClaimViewModelMap), typeof(IMap<ApplicationSettingContainer, ClaimsMachineSettingViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.ApplicationSettings
{
    [DataContract]
    public class ClaimsMachineSettingViewModel : IViewModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string MachineName { get; set; }

        [DataMember]
        public string ElectronicClaimsFileDirectory { get; set; }

        [DataMember]
        public string PaperClaimsFileDirectory { get; set; }
    }


    internal class ClaimSettingToClaimViewModelMap : IMap<ApplicationSettingContainer, ClaimsMachineSettingViewModel>
    {
        private readonly IEnumerable<IMapMember<ApplicationSettingContainer, ClaimsMachineSettingViewModel>> _members;

        public ClaimSettingToClaimViewModelMap()
        {
            _members = this.CreateMembers(Map, "ClaimSettingToPaperClaimViewModelMap");
        }

        public void Map(ApplicationSettingContainer source, ClaimsMachineSettingViewModel destination)
        {
            var claimSetting = (ClaimsConfigurationApplicationSetting)source.Value;

            destination.Id = source.Id.Value;
            destination.MachineName = source.MachineName;
            destination.ElectronicClaimsFileDirectory = claimSetting.ElectronicClaimsFileDirectory;
            destination.PaperClaimsFileDirectory = claimSetting.PaperClaimsFileDirectory;
        }

        public IEnumerable<IMapMember<ApplicationSettingContainer, ClaimsMachineSettingViewModel>> Members
        {
            get { return _members; }
        }
    }




    internal class ClaimViewModelToClaimSettingMap : IMap<ClaimsMachineSettingViewModel, ApplicationSettingContainer>
    {
        private readonly IEnumerable<IMapMember<ClaimsMachineSettingViewModel, ApplicationSettingContainer>> _members;

        private const string SettingName = "ClaimsDirectory";

        public ClaimViewModelToClaimSettingMap()
        {
            _members = this.CreateMembers(Map, "ClaimViewModelToPaperClaimSettingMap");
        }

        public void Map(ClaimsMachineSettingViewModel source, ApplicationSettingContainer destination)
        {
            destination.Id = source.Id;
            destination.MachineName = source.MachineName;
            destination.Name = SettingName;
            destination.Type = ApplicationSettingType.ClaimsConfigurationApplicationSetting;
            var claimsConfigurationModelSetting = new ClaimsConfigurationApplicationSetting();
            claimsConfigurationModelSetting.PaperClaimsFileDirectory = source.PaperClaimsFileDirectory;
            claimsConfigurationModelSetting.ElectronicClaimsFileDirectory = source.ElectronicClaimsFileDirectory;

            destination.Value = claimsConfigurationModelSetting;
        }

        public IEnumerable<IMapMember<ClaimsMachineSettingViewModel, ApplicationSettingContainer>> Members
        {
            get { return _members; }
        }
    }
}
