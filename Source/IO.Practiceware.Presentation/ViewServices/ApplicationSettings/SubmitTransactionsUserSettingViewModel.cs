﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewServices.ApplicationSettings;
using IO.Practiceware.Presentation.ViewServices.SubmitTransactions;
using Soaf.ComponentModel;
using Soaf.Presentation;


[assembly: Component(typeof(UserSettingToUserSettingViewModelMap), typeof(IMap<ApplicationSettingContainer, SubmitTransactionsUserSettingViewModel>))]
[assembly: Component(typeof(UserSettingViewModelToUserSettingMap), typeof(IMap<SubmitTransactionsUserSettingViewModel, ApplicationSettingContainer>))]

namespace IO.Practiceware.Presentation.ViewServices.ApplicationSettings
{
    [DataContract]
    public class SubmitTransactionsUserSettingViewModel : IViewModel
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int ClaimType { get; set; }

        [DataMember]
        public int? PaperClaimFormId { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public List<int> ElectronicClaimFormIds { get; set; }

        [DataMember]
        public virtual List<int> SelectedInsurers { get; set; }

        [DataMember]
        public virtual List<int> SelectedBillingDoctors { get; set; }

        [DataMember]
        public virtual List<int> SelectedScheduledDoctors { get; set; }

        [DataMember]
        public virtual List<int> SelectedLocations { get; set; }

        [DataMember]
        public virtual List<int> SelectedBillingOrganizations { get; set; }
    }


    internal class UserSettingViewModelToUserSettingMap : IMap<SubmitTransactionsUserSettingViewModel, ApplicationSettingContainer>
    {
        private readonly IEnumerable<IMapMember<SubmitTransactionsUserSettingViewModel, ApplicationSettingContainer>> _members;

        public UserSettingViewModelToUserSettingMap()
        {
            _members = this.CreateMembers(Map, "UserSettingViewModelToUserSettingMap");
        }

        public void Map(SubmitTransactionsUserSettingViewModel source, ApplicationSettingContainer destination)
        {
            destination.Id = source.Id;
            destination.UserId = source.UserId;
            destination.Name = source.Name;
            destination.Type = ApplicationSettingType.SubmitTransactionsApplicationSetting;
            var submitTransactionsUserSetting = new SubmitTransactionsApplicationSetting
            {
                ClaimType = source.ClaimType,
                StartDate = source.StartDate,
                EndDate = source.EndDate,
                PaperClaimFormId = source.PaperClaimFormId,
                SelectedBillingDoctors = source.SelectedBillingDoctors,
                SelectedBillingOrganizations = source.SelectedBillingOrganizations,
                SelectedInsurers = source.SelectedInsurers,
                ElectronicClaimFormIds = source.ElectronicClaimFormIds,
                SelectedLocations = source.SelectedLocations,
                SelectedScheduledDoctors = source.SelectedScheduledDoctors
            };

            destination.Value = submitTransactionsUserSetting;
        }

        public IEnumerable<IMapMember<SubmitTransactionsUserSettingViewModel, ApplicationSettingContainer>> Members
        {
            get { return _members; }
        }
    }

    internal class UserSettingToUserSettingViewModelMap : IMap<ApplicationSettingContainer, SubmitTransactionsUserSettingViewModel>
    {
        private readonly IEnumerable<IMapMember<ApplicationSettingContainer, SubmitTransactionsUserSettingViewModel>> _members;

        public UserSettingToUserSettingViewModelMap()
        {
            _members = this.CreateMembers(Map, "UserSettingToUserSettingViewModelMap");
        }

        public void Map(ApplicationSettingContainer source, SubmitTransactionsUserSettingViewModel destination)
        {
            var submitTransactionSetting = (SubmitTransactionsApplicationSetting)source.Value;

            destination.Id = source.Id;
            destination.UserId = source.UserId.Value;
            destination.Name = source.Name;
            destination.ClaimType = submitTransactionSetting.ClaimType;
            destination.StartDate = submitTransactionSetting.StartDate;
            destination.EndDate = submitTransactionSetting.EndDate;
            destination.PaperClaimFormId = submitTransactionSetting.PaperClaimFormId;
            destination.SelectedBillingDoctors = submitTransactionSetting.SelectedBillingDoctors;
            destination.SelectedBillingOrganizations = submitTransactionSetting.SelectedBillingOrganizations;
            destination.SelectedInsurers = submitTransactionSetting.SelectedInsurers;
            destination.ElectronicClaimFormIds = submitTransactionSetting.ElectronicClaimFormIds;
            destination.SelectedLocations = submitTransactionSetting.SelectedLocations;
            destination.SelectedScheduledDoctors = submitTransactionSetting.SelectedScheduledDoctors;
        }

        public IEnumerable<IMapMember<ApplicationSettingContainer, SubmitTransactionsUserSettingViewModel>> Members
        {
            get { return _members; }
        }
    }
}
