﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.MultiCalendar;
using IO.Practiceware.Presentation.ViewServices.Common.Configuration;
using IO.Practiceware.Presentation.ViewServices.MultiCalendar;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using Soaf.Presentation;
using Soaf.Threading;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Media;
using Task = System.Threading.Tasks.Task;

[assembly: Component(typeof(MultiCalendarViewService), typeof(IMultiCalendarViewService))]
[assembly: Component(typeof(BlockViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(LocationViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(ResourceViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(AppointmentTypeViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(BlockCategoryViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(AppointmentViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(PatientViewModelMap), AddAllServices = true)]

namespace IO.Practiceware.Presentation.ViewServices.MultiCalendar
{
    public interface IMultiCalendarViewService
    {
        /// <summary>
        ///   Get list of service locations
        /// </summary>
        /// <returns> List of service locations </returns>
        ObservableCollection<ColoredViewModel> LoadLocations();

        /// <summary>
        ///   Get list of categories
        /// </summary>
        /// <returns> List of categories </returns>
        ObservableCollection<ColoredViewModel> LoadCategories();

        /// <summary>
        ///   Get list of providers
        /// </summary>
        /// <returns> List of providers </returns>
        ObservableCollection<ResourceViewModel> LoadResources();

        /// <summary>
        /// Loads the appointment cancellation types.
        /// </summary>
        /// <returns></returns>
        ObservableCollection<NamedViewModel> LoadAppointmentCancellationTypes();

        /// <summary>
        ///   Gets all schedule blocks and appointments for the specified providers and dates.
        /// </summary>
        /// <param name="resourceIds"> The resource ids. </param>
        /// <param name="dates"> The dates. </param>
        /// <returns> </returns>        
        GetScheduleBlockResults GetScheduleBlocks(IEnumerable<int> resourceIds, IEnumerable<DateTime> dates);

        /// <summary>
        ///   Adds categories to the specified blocks.
        /// </summary>
        /// <param name="blockIds"> The block ids. </param>
        /// <param name="categoryIds"> The category ids. </param>
        IEnumerable<BlockCategoryViewModel> AddScheduleBlockCategories(IEnumerable<int> blockIds, IEnumerable<int> categoryIds);

        /// <summary>
        /// Sets the comment for an unavailable block
        /// </summary>
        /// <param name="blockId">The block id.</param>
        /// <param name="comment">The comment to set.</param>
        void SetUnavailableBlockComment(int blockId, string comment);

        /// <summary>
        /// Removes categories specified.
        /// </summary>
        /// <param name="blockCategoryIds">The block category ids.</param>
        void RemoveScheduleBlockCategories(IEnumerable<Guid> blockCategoryIds);

        /// <summary>
        ///   Clears all categories from the schedule block.
        /// </summary>
        /// <param name="blockIds"> The block ids. </param>
        /// <param name="isUnavailable"> </param>
        void SetBlocksIsUnavailable(IEnumerable<int> blockIds, bool isUnavailable);

        /// <summary>
        ///   Sets the location for all the schedule blocks.
        /// </summary>
        /// <param name="locationIdsKeyedByBlockId"> The location ids keyed by block id. </param>
        void SetScheduleBlockLocations(Dictionary<int, int> locationIdsKeyedByBlockId);

        /// <summary>
        ///   Sets the location for all specified appointments.
        /// </summary>
        /// <param name="appointmentIds"> The appointment ids. </param>
        /// <param name="locationId"> The location id. </param>
        void SetAppointmentsLocation(IEnumerable<int> appointmentIds, int locationId);

        /// <summary>
        ///   Assigns the appointment to the specified block and category (if specified).
        /// </summary>
        /// <param name="appointmentId"> The appointment id. </param>
        /// <param name="blockId"> The block id. </param>
        /// <param name="blockCategoryId"> The block category id. </param>
        /// <param name="useNewLocation">Flag to indicate whether the appointment will use the target block's location</param>
        void ReassignAppointment(int appointmentId, int blockId, Guid? blockCategoryId, bool useNewLocation = true);

        /// <summary>
        ///  Saves a new schedule block category to the specified schedule block with the specified appointment category
        /// </summary>
        /// <param name="blockId">The block id.</param>
        /// <param name="appointmentCategoryId">The appointmentCategory id.</param>
        /// <returns>The new block category.</returns>
        BlockCategoryViewModel SaveNewScheduleBlockCategory(int blockId, int? appointmentCategoryId);

        /// <summary>
        ///   Loads the encounter status change reasons.
        /// </summary>
        /// <returns> </returns>
        ObservableCollection<EncounterStatusChangeReasonViewModel> LoadEncounterStatusChangeReasons();

        /// <summary>
        /// Cancels the specified appointments.
        /// </summary>
        /// <param name="appointmentIds">The appointment ids.</param>
        /// <param name="appointmentCancellationTypeId">The appointment cancellation type id.</param>
        /// <param name="changeReason">The change reason.</param>
        /// <param name="comment">The comment.</param>
        void CancelAppointments(List<int> appointmentIds, int appointmentCancellationTypeId, EncounterStatusChangeReasonViewModel changeReason, string comment);

        /// <summary>
        /// Save either a global or resource comment
        /// </summary>
        /// <param name="calendarCommentViewModel">Contains comment to be saved as global or resource comment</param>
        void SaveCalendarComment(CalendarCommentViewModel calendarCommentViewModel);

        /// <summary>
        /// Edit either a global or resource comment
        /// </summary>
        /// <param name="calendarCommentViewModel">Contains comment to be edited</param>
        void EditCalendarComment(CalendarCommentViewModel calendarCommentViewModel);

        /// <summary>
        /// Delete either a global or resource comment
        /// </summary>
        /// <param name="calendarCommentViewModel">Contains comment to be deleted</param>
        void DeleteCalendarComment(CalendarCommentViewModel calendarCommentViewModel);

        /// <summary>
        /// Returns the default resource id for the patient
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        int? GetPatientDefaultResourceId(int patientId);

        /// <summary>
        /// Makes appointments orphaned
        /// </summary>
        /// <param name="appointmentIds"></param>
        void MakeAppointmentsOrphaned(IList<int> appointmentIds);

        /// <summary>
        /// Gets whether the given patient is active
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns>Returns a flag indicating whether the given patient is active</returns>
        bool IsPatientActive(int patientId);

        /// <summary>
        /// Saves new user schedule blocks.
        /// </summary>
        /// <param name="blocks">The block view models to save</param>
        /// <returns>Returns an ordered enumeration of user schedule block ids.</returns>
        IEnumerable<int> SaveNewScheduleBlocks(IEnumerable<BlockViewModel> blocks);
    }

    [SupportsUnitOfWork]
    internal class MultiCalendarViewService : BaseViewService, IMultiCalendarViewService
    {
        private readonly IPracticeRepository _practiceRepository;
        private readonly Func<TimeSpan, IEnumerable<EncounterStatusConfiguration>, IMapper<Appointment, AppointmentViewModel>> _createAppointmentMapper;
        private readonly Func<TimeSpan, IEnumerable<EncounterStatusConfiguration>, IMapper<ScheduleBlockAppointmentCategory, BlockCategoryViewModel>> _createBlockCategoryViewModelMapper;
        private readonly Func<CalendarCommentViewModel> _createCalendarComment;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly Func<TimeSpan, IEnumerable<EncounterStatusConfiguration>, IMapper<ScheduleBlock, BlockViewModel>> _createBlockMapper;
        private readonly IMapper<User, ResourceViewModel> _resourceMapper;
        private readonly ISchedulingConfigurationViewService _schedulingConfigurationViewService;

        public MultiCalendarViewService(IPracticeRepository practiceRepository,
                                        ISchedulingConfigurationViewService schedulingConfigurationViewService,
                                        IMapper<User, ResourceViewModel> resourceMapper,
                                        Func<TimeSpan, IEnumerable<EncounterStatusConfiguration>, IMapper<ScheduleBlock, BlockViewModel>> createBlockMapper,
                                        Func<TimeSpan, IEnumerable<EncounterStatusConfiguration>, IMapper<Appointment, AppointmentViewModel>> createAppointmentMapper,
                                        Func<TimeSpan, IEnumerable<EncounterStatusConfiguration>, IMapper<ScheduleBlockAppointmentCategory, BlockCategoryViewModel>> createBlockCategoryViewModelMapper,
                                        Func<CalendarCommentViewModel> createCalendarComment,
                                        IUnitOfWorkProvider unitOfWorkProvider)
        {
            _practiceRepository = practiceRepository;
            _schedulingConfigurationViewService = schedulingConfigurationViewService;
            _resourceMapper = resourceMapper;
            _createBlockMapper = createBlockMapper;
            _createAppointmentMapper = createAppointmentMapper;
            _createBlockCategoryViewModelMapper = createBlockCategoryViewModelMapper;
            _createCalendarComment = createCalendarComment;
            _unitOfWorkProvider = unitOfWorkProvider;
        }

        #region IMultiCalendarViewService Members

        public ObservableCollection<ColoredViewModel> LoadLocations()
        {
            return (from serviceLocation in PracticeRepository.ServiceLocations.ToArray()
                    orderby serviceLocation.ShortName
                    select new ColoredViewModel
                        {
                            Id = serviceLocation.Id,
                            Name = serviceLocation.ShortName,
                            Color = serviceLocation.Color
                        }).ToExtendedObservableCollection();
        }

        public ObservableCollection<ColoredViewModel> LoadCategories()
        {
            var categories = (from appointmentCategory in PracticeRepository.AppointmentCategories.Where(ac => !ac.IsArchived).ToArray()
                              orderby appointmentCategory.Name
                              select new ColoredViewModel
                                  {
                                      Id = appointmentCategory.Id,
                                      Name = appointmentCategory.Name,
                                      Color = appointmentCategory.Color
                                  }).ToExtendedObservableCollection();

            categories.Insert(0, new UnavailableCategoryViewModel());
            return categories;
        }

        public ObservableCollection<ResourceViewModel> LoadResources()
        {
            return (from u in PracticeRepository.Users.Where(u => !u.IsArchived && u.IsSchedulable).ToArray()
                    orderby u.UserName
                    select _resourceMapper.Map(u)).ToExtendedObservableCollection();
        }

        public ObservableCollection<NamedViewModel> LoadAppointmentCancellationTypes()
        {
            return Enums.GetValues<EncounterStatus>().Where(i => (int)i >= (int)EncounterStatus.CancelOffice && (int)i <= (int)EncounterStatus.CancelOther).Select(i => new NamedViewModel((int)i, i.GetDisplayName())).ToExtendedObservableCollection();
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public virtual GetScheduleBlockResults GetScheduleBlocks(IEnumerable<int> resourceIds, IEnumerable<DateTime> dates)
        {
            var results = new GetScheduleBlockResults();
            var localDates = dates.ToArray();

            EncounterStatusConfiguration[] encounterStatusConfigurations = _practiceRepository.EncounterStatusConfigurations.ToArray();
            TimeSpan schedulingIncrement = _schedulingConfigurationViewService.GetSchedulingConfiguration().SpanSize;

            Task<IEnumerable<AppointmentViewModel>> loadOrphanedAppointmentsTask = Task.Factory.StartNewWithCurrentTransaction(() => GetOrphanedAppointments(resourceIds, localDates, schedulingIncrement, encounterStatusConfigurations));

            Task<Tuple<IEnumerable<CalendarCommentViewModel>, IEnumerable<CalendarCommentViewModel>>> loadCalendarCommentsTask = Task.Factory.StartNewWithCurrentTransaction(() => GetCalendarComments(resourceIds, localDates));

            UserScheduleBlock[] scheduleBlocks = PracticeRepository.ScheduleBlocks.OfType<UserScheduleBlock>()
                .Where(Queries.IsOnDates<UserScheduleBlock>(b => b.StartDateTime, localDates))
                .Where(b => resourceIds.Contains(b.UserId))
                .ToArray();


            PracticeRepository.AsQueryableFactory().Load(scheduleBlocks,
                                                         b => b.User,
                                                         b => b.ServiceLocation,
                                                         b => b.ScheduleBlockAppointmentCategories
                                                                  .Select(c => new object[]
                                                                      {
                                                                          c.AppointmentCategory.AppointmentTypes,
                                                                          c.Appointment.Encounter.Patient.PatientInsurances.Select(pi => pi.InsurancePolicy.Insurer),
                                                                          c.Appointment.Encounter.Patient.PatientPhoneNumbers,
                                                                          c.Appointment.Encounter.ServiceLocation,
                                                                          c.Appointment.Encounter.PatientInsuranceReferrals,
                                                                          c.Appointment.AppointmentType,
                                                                          ((UserAppointment) c.Appointment).User
                                                                      }));

            var duplicateBlockAppointments = new List<AppointmentViewModel>();

            var mapper = _createBlockMapper(schedulingIncrement, encounterStatusConfigurations);
            var blocks = mapper.MapAll(scheduleBlocks)            
                // only take one block with a distinct datetime and resource
                // appointments from other conflicting blocks are considered orphans 
                // this could occur if the scheduling increment changes without reapplying templates
                .GroupBy(b => new { b.StartDateTime, b.Resource.Id }).Select(g =>
                    {
                        duplicateBlockAppointments.AddRange(g.Skip(1).SelectMany(b => b.Categories.Select(c => c.Appointment)).WhereNotDefault());
                        return g.First();
                    })
                .ToArray();

            results.OrphanedAppointments = loadOrphanedAppointmentsTask.Result.Concat(duplicateBlockAppointments).ToExtendedObservableCollection();
            results.ResourceComments = loadCalendarCommentsTask.Result.Item1.ToExtendedObservableCollection();
            results.GlobalComments = loadCalendarCommentsTask.Result.Item2.ToExtendedObservableCollection();
            results.Blocks = blocks.ToExtendedObservableCollection();
            results.QueriedDateTimes = localDates;
            results.QueriedResourceIds = resourceIds.ToArray();

            return results;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual IEnumerable<int> SaveNewScheduleBlocks(IEnumerable<BlockViewModel> blocks)
        {
            var createdDatabaseBlocks = new List<UserScheduleBlock>();
            var localBlocks = blocks.ToList();
            foreach (var block in localBlocks)
            {
                var databaseBlock = new UserScheduleBlock
                {
                    Comment = block.Comment,
                    IsUnavailable = false,
                    ServiceLocationId = block.Location.Id,
                    StartDateTime = block.StartDateTime,
                    UserId = block.Resource.Id,
                };
                PracticeRepository.Save(databaseBlock);
                createdDatabaseBlocks.Add(databaseBlock);
            }
            return createdDatabaseBlocks.Select(b => b.Id);
        }

        public virtual IEnumerable<BlockCategoryViewModel> AddScheduleBlockCategories(IEnumerable<int> blockIds, IEnumerable<int> categoryIds)
        {
            using (var work = _unitOfWorkProvider.Create())
            {
                IQueryable<ScheduleBlock> blocks = PracticeRepository.ScheduleBlocks.Where(b => blockIds.Contains(b.Id));

                var categories = PracticeRepository.AppointmentCategories.Where(c => categoryIds.Contains(c.Id)).ToDictionary(i => i.Id);

                var saved = new List<ScheduleBlockAppointmentCategory>();

                blocks.ForEachWithSinglePropertyChangedNotification(b => categoryIds.ForEachWithSinglePropertyChangedNotification(c => saved.Add(new ScheduleBlockAppointmentCategory
                    {
                        AppointmentCategory = categories[c],
                        ScheduleBlock = b,
                    })));


                blocks.ForEachWithSinglePropertyChangedNotification(b =>
                    {
                        if (b.IsUnavailable)
                        {
                            b.IsUnavailable = false;
                            b.Comment = null;
                        }
                    });

                work.AcceptChanges();

                EncounterStatusConfiguration[] encounterStatusConfigurations = _practiceRepository.EncounterStatusConfigurations.ToArray();
                TimeSpan schedulingIncrement = _schedulingConfigurationViewService.GetSchedulingConfiguration().SpanSize;

                var mapper = _createBlockCategoryViewModelMapper(schedulingIncrement, encounterStatusConfigurations);

                return mapper.MapAll(saved);                
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void RemoveScheduleBlockCategories(IEnumerable<Guid> blockCategoryIds)
        {
            PracticeRepository.ScheduleBlockAppointmentCategories
                .Where(c => blockCategoryIds.Contains(c.Id)).ToList().ForEach(PracticeRepository.Delete);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SetBlocksIsUnavailable(IEnumerable<int> blockIds, bool isUnavailable)
        {
            var blocks = PracticeRepository.ScheduleBlocks.Include(b => b.ScheduleBlockAppointmentCategories).Where(b => blockIds.Contains(b.Id));
            foreach (var block in blocks)
            {
                if (isUnavailable)
                {
                    block.ScheduleBlockAppointmentCategories.ToList().ForEach(PracticeRepository.Delete);
                }
                else
                {
                    block.Comment = null;
                }
                block.IsUnavailable = isUnavailable;
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SetScheduleBlockLocations(Dictionary<int, int> locationIdsKeyedByBlockId)
        {
            PracticeRepository.ScheduleBlocks.Where(b => locationIdsKeyedByBlockId.Keys.Contains(b.Id)).ToArray()
                .ForEachWithSinglePropertyChangedNotification(b => b.ServiceLocationId = locationIdsKeyedByBlockId[b.Id]);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SetAppointmentsLocation(IEnumerable<int> appointmentIds, int locationId)
        {
            PracticeRepository.Appointments.Include(a => a.Encounter).Where(a => appointmentIds.Contains(a.Id)).ToArray()
                .ForEachWithSinglePropertyChangedNotification(a => a.Encounter.ServiceLocationId = locationId);
        }

        public virtual void ReassignAppointment(int appointmentId, int blockId, Guid? blockCategoryId, bool useNewLocation = true)
        {
            var block = PracticeRepository.ScheduleBlocks.WithId(blockId);
            Appointment appointment = PracticeRepository.Appointments.Include(a => a.Encounter, a => a.ScheduleBlockAppointmentCategories).WithId(appointmentId);
            appointment.DateTime = block.StartDateTime;

            if (useNewLocation)
            {
                appointment.Encounter.ServiceLocationId = block.ServiceLocationId;
                PracticeRepository.Save(appointment);
            }
            appointment.As<UserAppointment>().IfNotNull(a => a.UserId = block.As<UserScheduleBlock>().IfNotNull(b => b.UserId));

            // dis-associate previous ScheduleBlockAppointmentCategories
            var oldCategory = appointment.ScheduleBlockAppointmentCategories.FirstOrDefault();
            appointment.ScheduleBlockAppointmentCategories.Clear();
            PracticeRepository.Save(appointment);
            if (oldCategory != null) PracticeRepository.Save(oldCategory);

            if (blockCategoryId != null)
            {
                var blockCategory = PracticeRepository.ScheduleBlockAppointmentCategories.WithId(blockCategoryId);
                if (blockCategory != null) appointment.ScheduleBlockAppointmentCategories.Add(blockCategory);
                PracticeRepository.Save(appointment);
            }
        }

        public BlockCategoryViewModel SaveNewScheduleBlockCategory(int blockId, int? appointmentCategoryId)
        {
            var databaseCategory = new ScheduleBlockAppointmentCategory { ScheduleBlockId = blockId };
            if (appointmentCategoryId.HasValue) databaseCategory.AppointmentCategoryId = appointmentCategoryId.Value;
            PracticeRepository.Save(databaseCategory);

            databaseCategory = PracticeRepository.ScheduleBlockAppointmentCategories.Include(c => c.AppointmentCategory).WithId(databaseCategory.Id);

            EncounterStatusConfiguration[] encounterStatusConfigurations = _practiceRepository.EncounterStatusConfigurations.ToArray();
            TimeSpan schedulingIncrement = _schedulingConfigurationViewService.GetSchedulingConfiguration().SpanSize;
            var mapper = _createBlockCategoryViewModelMapper(schedulingIncrement, encounterStatusConfigurations);
            return mapper.Map(databaseCategory);
        }

        public ObservableCollection<EncounterStatusChangeReasonViewModel> LoadEncounterStatusChangeReasons()
        {
            return (from r in PracticeRepository.EncounterStatusChangeReasons
                    orderby r.Value
                    select new EncounterStatusChangeReasonViewModel
                        {
                            Id = r.Id,
                            EncounterStatusId = r.EncounterStatusId,
                            Value = r.Value
                        }).ToExtendedObservableCollection();
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void CancelAppointments(List<int> appointmentIds, int appointmentCancellationTypeId, EncounterStatusChangeReasonViewModel changeReason, string comment)
        {
            foreach (Appointment appointment in PracticeRepository.Appointments.Include(a => a.Encounter, a => a.ScheduleBlockAppointmentCategories).Where(a => appointmentIds.Contains(a.Id)).ToArray())
            {
                appointment.Encounter.EncounterStatusChangeComment = comment;
                if (changeReason != null) appointment.Encounter.EncounterStatusChangeReasonId = changeReason.Id;

                appointment.Encounter.EncounterStatus = (EncounterStatus)appointmentCancellationTypeId;
                appointment.ScheduleBlockAppointmentCategories.Clear();
            }
        }

        public void SaveCalendarComment(CalendarCommentViewModel calendarCommentViewModel)
        {
            var calendarComment = new CalendarComment { Date = calendarCommentViewModel.Date, Value = calendarCommentViewModel.Value };
            if (calendarCommentViewModel.UserId.HasValue) calendarComment.UserId = calendarCommentViewModel.UserId.Value;
            PracticeRepository.Save(calendarComment);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void EditCalendarComment(CalendarCommentViewModel calendarCommentViewModel)
        {
            List<CalendarComment> fromDatabaseCalendarComments = calendarCommentViewModel.UserId.HasValue
                                                                     ? PracticeRepository.CalendarComments.Where(c => c.UserId == calendarCommentViewModel.UserId.Value && c.Date == calendarCommentViewModel.Date).ToList()
                                                                     : PracticeRepository.CalendarComments.Where(c => !c.UserId.HasValue && c.Date == calendarCommentViewModel.Date).ToList();
            foreach (var comment in fromDatabaseCalendarComments)
            {
                comment.Value = calendarCommentViewModel.Value;
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void DeleteCalendarComment(CalendarCommentViewModel calendarCommentViewModel)
        {
            List<CalendarComment> fromDatabaseCalendarComments = calendarCommentViewModel.UserId.HasValue
                                                                     ? PracticeRepository.CalendarComments.Where(c => c.UserId == calendarCommentViewModel.UserId.Value && c.Date == calendarCommentViewModel.Date).ToList()
                                                                     : PracticeRepository.CalendarComments.Where(c => !c.UserId.HasValue && c.Date == calendarCommentViewModel.Date).ToList();
            foreach (var comment in fromDatabaseCalendarComments)
            {
                PracticeRepository.Delete(comment);
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SetUnavailableBlockComment(int blockId, string comment)
        {
            var databaseUnavailableBlock = PracticeRepository.ScheduleBlocks.Where(sb => sb.IsUnavailable).WithId(blockId);
            databaseUnavailableBlock.Comment = comment;
            PracticeRepository.Save(databaseUnavailableBlock);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void MakeAppointmentsOrphaned(IList<int> appointmentIds)
        {
            var databaseAppointments = PracticeRepository.Appointments.Where(a => appointmentIds.Contains(a.Id)).Include(a => a.ScheduleBlockAppointmentCategories);
            foreach (var appointment in databaseAppointments)
            {
                appointment.ScheduleBlockAppointmentCategories.Clear();
            }
        }

        public bool IsPatientActive(int patientId)
        {
            var databasePatient = PracticeRepository.Patients.WithId(patientId);
            if (databasePatient == null) throw new Exception("Patient with Id {0} could not be found.".FormatWith(patientId));
            return databasePatient.PatientStatus == PatientStatus.Active;
        }

        #endregion

        private IEnumerable<AppointmentViewModel> GetOrphanedAppointments(IEnumerable<int> resourceIds, IEnumerable<DateTime> dates, TimeSpan schedulingIncrement, IEnumerable<EncounterStatusConfiguration> encounterStatusConfigurations)
        {
            UserAppointment[] orphanedAppointments = PracticeRepository.Appointments.OfType<UserAppointment>()
                .Where(a => a.ScheduleBlockAppointmentCategories.Count == 0)
                .Where(Queries.IsOnDates<UserAppointment>(a => a.DateTime, dates))
                .Where(a => resourceIds.Contains(a.UserId))
                .Where(a => !EncounterQueries.IsCancelled.Invoke(a.Encounter))
                .Where(a => a.DisplayOnCalendar)
                .ToArray();

            PracticeRepository.AsQueryableFactory().Load(orphanedAppointments,
                                                         a => new object[]
                                                             {                                                                 
                                                                 a.Encounter.Patient.PatientInsurances.Select(pi => pi.InsurancePolicy.Insurer),
                                                                 a.Encounter.Patient.PatientPhoneNumbers,
                                                                 a.Encounter.PatientInsuranceReferrals,
                                                                 a.Encounter.ServiceLocation,
                                                                 a.AppointmentType,
                                                                 a.User
                                                             });

            var mapper = _createAppointmentMapper(schedulingIncrement, encounterStatusConfigurations);
            return mapper.MapAll(orphanedAppointments).ToArray();            
        }

        /// <summary>
        /// Gets a tuple of calendar comment view models where the first item is resource comments and the second item is global comments.
        /// </summary>
        /// <param name="resourceIds"></param>
        /// <param name="dates"></param>
        /// <returns></returns>
        private Tuple<IEnumerable<CalendarCommentViewModel>,IEnumerable<CalendarCommentViewModel>>  GetCalendarComments(IEnumerable<int> resourceIds, IEnumerable<DateTime> dates)
        {
            var localResourceIds = resourceIds.ToArray();
            var localDates = dates.ToArray();
            var fromDatabaseCalendarComments = PracticeRepository.CalendarComments;
            var wherePredicates = new List<Expression<Func<CalendarComment, bool>>>();
            wherePredicates.AddRange(GetCalendarCommentQueryUserId(fromDatabaseCalendarComments, localResourceIds));
            wherePredicates.AddRange(GetCalendarCommentQueryDate(fromDatabaseCalendarComments, localDates));
            var whereCondition = wherePredicates.FirstOrDefault();
            if (whereCondition == null) return null;
            if (wherePredicates.Count > 1) whereCondition = wherePredicates.Skip(1).Aggregate(whereCondition, (current, predicate) => current.Or(predicate));
            var fromDatabaseResults = fromDatabaseCalendarComments.Where(whereCondition).ToList();

            var resourceComments = new List<CalendarCommentViewModel>();
            var globalComments = new List<CalendarCommentViewModel>();

            foreach (var resourceId in localResourceIds)
            {
                var localResourceId = resourceId;
                foreach (var date in localDates)
                {
                    var localDate = date;
                    var comment = fromDatabaseResults.Where(i => i.UserId.HasValue).FirstOrDefault(i => i.Date == localDate && i.UserId == localResourceId).IfNull(() =>
                    {
                        var newComment = new CalendarComment();
                        newComment.UserId = localResourceId;
                        newComment.Date = localDate;
                        return newComment;
                    });

                    var viewModel = _createCalendarComment();
                    viewModel.Date = comment.Date;
                    viewModel.Value = comment.Value;
                    viewModel.UserId = comment.UserId;
                    resourceComments.Add(viewModel);
                }
            }

            foreach (var d in localDates)
            {
                var date = d;
                var comment = fromDatabaseResults.Where(i => !i.UserId.HasValue).FirstOrDefault(i => i.Date == date).IfNull(() =>
                {
                    var newComment = new CalendarComment();
                    newComment.Date = date;
                    return newComment;
                });

                var viewModel = _createCalendarComment();
                viewModel.Date = comment.Date;
                viewModel.Value = comment.Value;
                globalComments.Add(viewModel);
            }

            return Tuple.Create<IEnumerable<CalendarCommentViewModel>, IEnumerable<CalendarCommentViewModel>>(resourceComments, globalComments);
        }

        private static IEnumerable<Expression<Func<CalendarComment, bool>>> GetCalendarCommentQueryUserId(IQueryable<CalendarComment> calendarComments, IEnumerable<int> resourceIds)
        {
            var queries = new List<Expression<Func<CalendarComment, bool>>>();
            foreach (var resourceId in resourceIds)
            {
                var id = resourceId;
                var query = calendarComments.CreatePredicate(c => c.UserId == id);
                queries.Add(query);
            }
            return queries;
        }

        private static IEnumerable<Expression<Func<CalendarComment, bool>>> GetCalendarCommentQueryDate(IQueryable<CalendarComment> calendarComments, IEnumerable<DateTime> dates)
        {
            var queries = new List<Expression<Func<CalendarComment, bool>>>();
            foreach (var date in dates)
            {
                var d = date;
                var query = calendarComments.CreatePredicate(c => c.Date == d);
                queries.Add(query);
            }
            return queries;
        }


        public int? GetPatientDefaultResourceId(int patientId)
        {
            return PracticeRepository.Patients.Single(x => x.Id == patientId).DefaultUserId;
        }
    }

    public class BlockViewModelMap : IMap<ScheduleBlock, BlockViewModel>
    {
        private readonly IEnumerable<IMapMember<ScheduleBlock, BlockViewModel>> _members;

        public BlockViewModelMap(TimeSpan schedulingIncrement,
                                 IMapper<ServiceLocation, ColoredViewModel> locationMapper,
                                 IMapper<User, ResourceViewModel> resourceMapper,
                                 IMapper<ScheduleBlockAppointmentCategory, BlockCategoryViewModel> categoryMapper)
        {
            _members = this.CreateMembers(i =>
                                          new BlockViewModel
                                              {
                                                  Id = i.Id,
                                                  Comment = i.Comment,
                                                  StartDateTime = i.StartDateTime.RoundToNearest(schedulingIncrement),
                                                  EndDateTime = i.StartDateTime.RoundToNearest(schedulingIncrement) + schedulingIncrement,
                                                  Location = locationMapper.Map(i.ServiceLocation),
                                                  Resource = !i.Is<UserScheduleBlock>() ? null : resourceMapper.Map(i.CastTo<UserScheduleBlock>().User),
                                                  Categories = i.IsUnavailable
                                                                   ? new[] { new UnavailableBlockCategoryViewModel { BlockId = i.Id } as BlockCategoryViewModel }.ToExtendedObservableCollection()
                                                                   : i.ScheduleBlockAppointmentCategories.Select(categoryMapper.Map).ToExtendedObservableCollection()
                                              }).ToArray();
        }

        #region IMap<ScheduleBlock,BlockViewModel> Members

        public IEnumerable<IMapMember<ScheduleBlock, BlockViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    public class LocationViewModelMap : IMap<ServiceLocation, ColoredViewModel>
    {
        private readonly IEnumerable<IMapMember<ServiceLocation, ColoredViewModel>> _members;

        public LocationViewModelMap()
        {
            _members = this.CreateMembers(i =>
                                          new ColoredViewModel
                                              {
                                                  Id = i.Id,
                                                  Name = i.ShortName,
                                                  Color = i.Color
                                              }).ToArray();
        }

        #region IMap<ServiceLocation,ColoredViewModel> Members

        public IEnumerable<IMapMember<ServiceLocation, ColoredViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    public class AppointmentTypeViewModelMap : IMap<AppointmentType, ColoredViewModel>
    {
        private readonly IEnumerable<IMapMember<AppointmentType, ColoredViewModel>> _members;

        public AppointmentTypeViewModelMap()
        {
            _members = this.CreateMembers(i =>
                                          new ColoredViewModel
                                          {
                                              Id = i.Id,
                                              Name = i.Name,
                                              Color = i.Color
                                          }).ToArray();
        }

        #region IMap<AppointmentType,ColoredViewModel> Members

        public IEnumerable<IMapMember<AppointmentType, ColoredViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    public class ResourceViewModelMap : IMap<User, ResourceViewModel>
    {
        private readonly IEnumerable<IMapMember<User, ResourceViewModel>> _members;

        public ResourceViewModelMap()
        {
            _members = this.CreateMembers(i => new ResourceViewModel
                {
                    Id = i.Id,
                    DisplayName = i.UserName,
                    FirstName = i.FirstName,
                    LastName = i.LastName,
                    UserName = i.UserName
                }).ToArray();
        }

        #region IMap<User,ResourceViewModel> Members

        public IEnumerable<IMapMember<User, ResourceViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    public class BlockCategoryViewModelMap : IMap<ScheduleBlockAppointmentCategory, BlockCategoryViewModel>
    {
        private readonly IEnumerable<IMapMember<ScheduleBlockAppointmentCategory, BlockCategoryViewModel>> _members;

        public BlockCategoryViewModelMap(IMapper<Appointment, AppointmentViewModel> appointmentMapper)
        {
            _members = this.CreateMembers(i => new BlockCategoryViewModel
                {
                    Id = i.Id,
                    BlockId = i.ScheduleBlockId,
                    Category = new ColoredViewModel
                        {
                            Id = i.AppointmentCategoryId,
                            Name = i.AppointmentCategory.Name,
                            Color = i.AppointmentCategory.Color
                        },
                    Appointment = (i.Appointment != null && i.Appointment.DisplayOnCalendar) ? appointmentMapper.Map(i.Appointment) : null
                }).ToArray();
        }

        #region IMap<ScheduleBlockAppointmentCategory,BlockCategoryViewModel> Members

        public IEnumerable<IMapMember<ScheduleBlockAppointmentCategory, BlockCategoryViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    public class UnavailableBlockCategoryViewModelMap : IMap<ScheduleBlock, UnavailableBlockCategoryViewModel>
    {
        private readonly IEnumerable<IMapMember<ScheduleBlock, UnavailableBlockCategoryViewModel>> _members;

        public UnavailableBlockCategoryViewModelMap()
        {
            _members = this.CreateMembers(i => new UnavailableBlockCategoryViewModel
                {
                    Id = Guid.Empty,
                    Category = new ColoredViewModel
                        {
                            Name = "Unavailable",
                            Color = Colors.DarkSlateGray
                        },
                }).ToArray();
        }

        #region IMap<ScheduleBlockAppointmentCategory,BlockCategoryViewModel> Members

        public IEnumerable<IMapMember<ScheduleBlock, UnavailableBlockCategoryViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    public class AppointmentViewModelMap : IMap<Appointment, AppointmentViewModel>
    {
        private readonly IEnumerable<IMapMember<Appointment, AppointmentViewModel>> _members;

        public AppointmentViewModelMap(IEnumerable<EncounterStatusConfiguration> encounterStatusConfigurations,
                                       TimeSpan schedulingIncrement,
                                       IMapper<ServiceLocation, ColoredViewModel> locationMapper,
                                       IMapper<AppointmentType, ColoredViewModel> appointmentTypeMapper,
                                       IMapper<Tuple<Patient, InsuranceType, DateTime>, PatientViewModel> patientMapper,
                                       IMapper<User, ResourceViewModel> resourceMapper)
        {
            Dictionary<int, EncounterStatusConfiguration> statusLookup = encounterStatusConfigurations.ToDictionary(c => c.Id);

            _members = this.CreateMembers(i => new AppointmentViewModel
            {
                Id = i.Id,
                EncounterStatusColor = statusLookup.GetValue((int)i.Encounter.EncounterStatus).IfNotNull(s => s.Color),
                EncounterStatusId = (int)i.Encounter.EncounterStatus,
                EncounterStatusName = statusLookup.GetValue((int)i.Encounter.EncounterStatus).IfNotNull(s => s.Name),
                InsuranceTypeId = i.Encounter.InsuranceType,
                AppointmentCategoryId = i.AppointmentType.AppointmentCategoryId,
                AppointmentType = appointmentTypeMapper.Map(i.AppointmentType),
                Comments = i.Comment,
                IsCancelled = i.Encounter.EncounterStatus.IsCancelledStatus(),
                IsOrphaned = i.ScheduleBlockAppointmentCategories.Count == 0,
                Location = locationMapper.Map(i.Encounter.ServiceLocation),
                Patient = patientMapper.Map(Tuple.Create(i.Encounter.Patient, i.Encounter.InsuranceType, i.DateTime)),
                Resource = i is UserAppointment ? resourceMapper.Map(i.CastTo<UserAppointment>().User) : null,
                StartDateTime = i.DateTime.RoundToNearest(schedulingIncrement),
                EndDateTime = i.DateTime.RoundToNearest(schedulingIncrement) + schedulingIncrement,
                IsReferralRequired = i.PatientInsurance.IfNotNull(pi => pi.InsurancePolicy.IfNotNull(ip => ip.Insurer.IfNotNull(j => j.IsReferralRequired)))
            }).ToArray();
        }

        #region IMap<Appointment,AppointmentViewModel> Members

        public IEnumerable<IMapMember<Appointment, AppointmentViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    public class PatientViewModelMap : IMap<Tuple<Patient, InsuranceType, DateTime>, PatientViewModel>
    {
        private readonly IEnumerable<IMapMember<Tuple<Patient, InsuranceType, DateTime>, PatientViewModel>> _members;

        public PatientViewModelMap()
        {
            _members = this.CreateMembers(i => new PatientViewModel
                {
                    Id = i.Item1.Id,
                    DisplayName = i.Item1.DisplayName,
                    FirstName = i.Item1.FirstName,
                    LastName = i.Item1.LastName,
                    MiddleName = i.Item1.MiddleName,
                    InsurerName = i.Item1.PatientInsurances
                                                   .Where(pi => pi.InsuranceType == i.Item2 && pi.InsurancePolicy != null
                                                                && pi.IsActiveForDateTime(i.Item3))
                                                   .OrderBy(pi => pi.OrdinalId)
                                                                .Select(pi => pi.InsurancePolicy.Insurer.IfNotNull(y => y.Name)).FirstOrDefault(z => z.IsNotNullOrEmpty()),
                    CellPhone = i.Item1.CellPhoneNumber.IfNotNull(x => x.ToString(), string.Empty),
                    HomePhone = i.Item1.HomePhoneNumber.IfNotNull(x => x.ToString(), string.Empty),
                    BusinessPhone = i.Item1.BusinessPhoneNumber.IfNotNull(x => x.ToString(), string.Empty),
                    BirthDate = i.Item1.DateOfBirth,
                    PatientStatus = i.Item1.PatientStatus
                }).ToArray();
        }

        #region IMap<Patient,PatientViewModel> Members

        public IEnumerable<IMapMember<Tuple<Patient, InsuranceType, DateTime>, PatientViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    [DataContract]
    [KnownType(typeof(ExtendedObservableCollection<BlockViewModel>))]
    [KnownType(typeof(ExtendedObservableCollection<AppointmentViewModel>))]
    [KnownType(typeof(ExtendedObservableCollection<CalendarCommentViewModel>))]
    public class GetScheduleBlockResults : IViewModel
    {
        [DataMember]
        [Dependency]
        public ObservableCollection<BlockViewModel> Blocks { get; set; }

        [DataMember]
        [Dependency]
        public ObservableCollection<AppointmentViewModel> OrphanedAppointments { get; set; }

        [DataMember]
        [Dependency]
        public ObservableCollection<CalendarCommentViewModel> ResourceComments { get; set; }

        [DataMember]
        [Dependency]
        public ObservableCollection<CalendarCommentViewModel> GlobalComments { get; set; }

        [DataMember]
        public int[] QueriedResourceIds { get; set; }

        [DataMember]
        public DateTime[] QueriedDateTimes { get; set; }
    }
}