﻿using System.ComponentModel.DataAnnotations;
using IO.Practiceware.Application;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Permissions;
using IO.Practiceware.Presentation.ViewServices.Permissions;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using User = IO.Practiceware.Model.User;

[assembly: Component(typeof(PermissionsViewService), typeof(IPermissionsViewService))]
[assembly: Component(typeof(PermissionViewModelMap), typeof(IMap<Permission, UserPermissionViewModel>))]
[assembly: Component(typeof(UserViewModelMap), typeof(IMap<User, NamedViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.Permissions
{
    [DataContract]
    public class PermissionLoadInformation
    {
        [DataMember]
        public NamedViewModel User { get; set; }

        [DataMember]
        [Dependency]
        public ObservableCollection<UserPermissionViewModel> UserPermissions { get; set; }
    }

    public interface IPermissionsViewService
    {
        PermissionLoadInformation GetLoadInformation(int userId);
        void SavePermission(UserPermissionViewModel userPermission);
    }

    public class PermissionsViewService : BaseViewService, IPermissionsViewService
    {
        private readonly Func<int, IMapper<Permission, UserPermissionViewModel>> _createPermissionMapper;
        private readonly IMapper<User, NamedViewModel> _userMapper;

        public PermissionsViewService(Func<int, IMapper<Permission, UserPermissionViewModel>> createPermissionMapper,
                                      IMapper<User, NamedViewModel> userMapper)
        {
            _createPermissionMapper = createPermissionMapper;
            _userMapper = userMapper;
        }

        public PermissionLoadInformation GetLoadInformation(int userId)
        {
            PermissionLoadInformation result;
            var permissionMapper = _createPermissionMapper(userId);
            var user = PracticeRepository.Users.FirstOrDefault(u => u.Id == userId);

            if (user!= null)
            { 
                var currentUserViewModel = _userMapper.Map(user);
                var permissions = PracticeRepository.Permissions.Include(p => p.UserPermissions);
                var permissionViewModels = permissions.Select(permissionMapper.Map).ToExtendedObservableCollection();

                result = new PermissionLoadInformation{
                User = currentUserViewModel,
                UserPermissions = permissionViewModels};
            }
            else
            {
                result = new PermissionLoadInformation{
                User = null,
                UserPermissions = null};
            }
            return result;
        }

        public void SavePermission(UserPermissionViewModel userPermission)
        {
            var databaseUserPermission = PracticeRepository.UserPermissions.FirstOrDefault(up => up.UserId == userPermission.UserId && up.PermissionId == userPermission.PermissionId) ?? new UserPermission { PermissionId = userPermission.PermissionId, UserId = userPermission.UserId };
            databaseUserPermission.IsDenied = !userPermission.IsSelected;
            PracticeRepository.Save(databaseUserPermission);
            UserContext.Current.RefreshUser();
        }
    }

    public class PermissionViewModelMap : IMap<Permission, UserPermissionViewModel>
    {
        private readonly IEnumerable<IMapMember<Permission, UserPermissionViewModel>> _members;

        public PermissionViewModelMap(int userId)
        {
            _members = this.CreateMembers(i => new UserPermissionViewModel
                                                   {
                                                       IsSelected = (i.UserPermissions.Any(up => up.UserId == userId)) && !i.UserPermissions.First(up => up.UserId == userId).IsDenied,
                                                       Name = i.Name,
                                                       PermissionId = i.Id,
                                                       UserId = userId,
                                                       Category = GetPermissionCategory(i.PermissionCategory, i.Id).GetDisplayName() 
                                                   });
        }

        private PermissionCategory GetPermissionCategory(PermissionCategory? permissionCategory, int permissionId)
        {
            if (permissionCategory.HasValue) return permissionCategory.Value;

            if ((PermissionId) permissionId != default(PermissionId))
            {
                var a = ((PermissionId) permissionId).GetAttribute<DisplayAttribute>();
                if (a != null && a.GroupName.IsNotNullOrEmpty())
                {
                    return a.GroupName.ToEnum<PermissionCategory>();
                }
            }
            return PermissionCategory.Other;
        }

        public IEnumerable<IMapMember<Permission, UserPermissionViewModel>> Members
        {
            get { return _members; }
        }
    }

    public class UserPermissionViewModelMap : IMap<UserPermission, UserPermissionViewModel>
    {
        private readonly IEnumerable<IMapMember<UserPermission, UserPermissionViewModel>> _members;

        public UserPermissionViewModelMap()
        {
            _members = this.CreateMembers(i =>
                                          new UserPermissionViewModel
                                          {
                                              Name = ((PermissionId)i.PermissionId).GetDisplayName(),
                                              IsSelected = !i.IsDenied,
                                              PermissionId = i.PermissionId,
                                              UserId = i.UserId
                                          }).ToArray();
        }

        #region IMap<ServiceLocation,ColoredViewModel> Members

        public IEnumerable<IMapMember<UserPermission, UserPermissionViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    public class UserViewModelMap : IMap<User, NamedViewModel>
    {
        private readonly IEnumerable<IMapMember<User, NamedViewModel>> _members;

        public UserViewModelMap()
        {
            _members = this.CreateMembers(i =>
                                          new NamedViewModel
                                          {
                                              Id = i.Id,
                                              Name = i.DisplayName,
                                          }).ToArray();
        }

        #region IMap<ServiceLocation,ColoredViewModel> Members

        public IEnumerable<IMapMember<User, NamedViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }
}
