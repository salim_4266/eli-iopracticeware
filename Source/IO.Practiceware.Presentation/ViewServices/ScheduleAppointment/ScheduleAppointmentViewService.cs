﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientReferralPopup;
using IO.Practiceware.Presentation.ViewModels.ScheduleAppointment;
using IO.Practiceware.Presentation.ViewServices.Common.Configuration;
using IO.Practiceware.Presentation.ViewServices.PatientReferralPopup;
using IO.Practiceware.Presentation.ViewServices.ScheduleAppointment;
using IO.Practiceware.Presentation.Common;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using AppointmentTypeViewModel = IO.Practiceware.Presentation.ViewModels.ScheduleAppointment.AppointmentTypeViewModel;
using PatientInsuranceAuthorizationViewModel = IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup.PatientInsuranceAuthorizationViewModel;

[assembly: Component(typeof(ScheduleAppointmentViewService), typeof(IScheduleAppointmentViewService))]
[assembly: Component(typeof(AppointmentViewModelMap), AddAllServices = true)]
[assembly: Component(typeof(ReferralViewModelMap), AddAllServices = true)]

namespace IO.Practiceware.Presentation.ViewServices.ScheduleAppointment
{
    public interface IScheduleAppointmentViewService
    {
        /// <summary>
        /// Get the schedule block that the appointment will be created for
        /// </summary>
        /// <param name="scheduleBlockId">The block id</param>
        /// <param name="scheduleBlockAppointmentCategoryId">The block category id</param>
        /// <returns>The schedule block</returns>
        ScheduleBlockViewModel GetScheduleBlock(int? scheduleBlockId, Guid? scheduleBlockAppointmentCategoryId);

        /// <summary>
        /// Get the appointment that will be updated
        /// </summary>
        /// <param name="appointmentId">The appointment id</param>
        /// <returns>The appointment</returns>
        AppointmentViewModel GetAppointment(int appointmentId);

        /// <summary>
        /// Get this existing referral information
        /// </summary>
        /// <param name="patientId">The patient</param>
        /// <returns>List of referrals</returns>
        ObservableCollection<ReferralViewModel> GetPatientExistingReferralInformation(int patientId);

        /// <summary>
        /// Get flag that indicates if a referral is required
        /// </summary>
        /// <param name="patientId">The patient</param>
        /// <param name="insuranceType">The insurance type</param>
        /// <param name="dateTime">The effective date for when the referral is valid</param>
        /// <returns>Flag</returns>
        bool GetIsReferralInformationRequired(int patientId, InsuranceType insuranceType, DateTime dateTime);

        /// <summary>
        /// List of various appointment types by a given category
        /// </summary>
        /// <param name="categoryId">The category</param>
        /// <returns>List of appointment types</returns>
        ObservableCollection<AppointmentTypeViewModel> LoadAppointmentTypes(int? categoryId);

        /// <summary>
        /// Creates or Updates an appointment in the database 
        /// </summary>
        /// <param name="scheduleBlockViewModel">The appointment will be created from this schedule block</param>
        /// <param name="appointmentViewModel">The appointment to be updated</param>
        /// <param name="appointmentTypeViewModel">The appointment type</param>
        /// <param name="insuranceType">The insurance type</param>
        /// <param name="patientViewModel">The patient the appointment is for</param>
        /// <param name="referrals">The referrals</param>
        /// <param name="authorizationId">The authorization</param>
        /// <param name="reasonForVisit">A pass through datapoint for vb 6 screen</param>
        /// <param name="comment">A comment about the appointment</param>
        /// <param name="orderDate">An optional value for the appointment order date</param>
        /// <returns></returns>
        AppointmentViewModel SaveAppointment(ScheduleBlockViewModel scheduleBlockViewModel, AppointmentViewModel appointmentViewModel, AppointmentTypeViewModel appointmentTypeViewModel, InsuranceType insuranceType, NamedViewModel patientViewModel, List<ReferralViewModel> referrals, int? authorizationId, string reasonForVisit, string comment, DateTime? orderDate);

        /// <summary>
        /// Delete an appointment from the system if the user abandon the creation process
        /// </summary>
        /// <param name="appointmentId">The appintment to delete</param>
        void DeleteAppointment(int appointmentId);

        /// <summary>
        /// Get a patient
        /// </summary>
        /// <param name="patientId">The patient id</param>
        /// <returns>Patient</returns>
        NamedViewModel GetPatient(int patientId);

        /// <summary>
        /// Gets a flag that indicates whether the patient is active
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        bool IsPatientActive(int patientId);

        /// <summary>
        /// Flag indicating if there are pending recalls
        /// </summary>
        /// <param name="patientId">The patient id</param>
        /// <returns>Flag</returns>
        bool HasPendingRecalls(int patientId);
    }

    [SupportsUnitOfWork]
    internal class ScheduleAppointmentViewService : BaseViewService, IScheduleAppointmentViewService
    {
        private readonly ISchedulingConfigurationViewService _schedulingConfigurationViewService;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly Func<TimeSpan, IMapper<UserAppointment, AppointmentViewModel>> _createAppointmentMapper;
        private readonly IMapper<PatientInsuranceReferral, ReferralViewModel> _referralMapper;

        public ScheduleAppointmentViewService(ISchedulingConfigurationViewService schedulingConfigurationViewService, IUnitOfWorkProvider unitOfWorkProvider,
            Func<TimeSpan, IMapper<UserAppointment, AppointmentViewModel>> createAppointmentMapper, IMapper<PatientInsuranceReferral, ReferralViewModel> referralMapper)
        {

            _schedulingConfigurationViewService = schedulingConfigurationViewService;
            _unitOfWorkProvider = unitOfWorkProvider;
            _createAppointmentMapper = createAppointmentMapper;
            _referralMapper = referralMapper;
        }

        public ScheduleBlockViewModel GetScheduleBlock(int? scheduleBlockId, Guid? scheduleBlockAppointmentCategoryId)
        {
            var fromDatabaseScheduleBlock = scheduleBlockAppointmentCategoryId.HasValue
                                            ? PracticeRepository.ScheduleBlocks.OfType<UserScheduleBlock>().First(b => b.ScheduleBlockAppointmentCategories.Any(c => c.Id == scheduleBlockAppointmentCategoryId))
                                            : PracticeRepository.ScheduleBlocks.OfType<UserScheduleBlock>().WithId(scheduleBlockId);

            PracticeRepository.AsQueryableFactory().Load(fromDatabaseScheduleBlock,
                                                          b => b.ScheduleBlockAppointmentCategories.Select(c => c.AppointmentCategory),
                                                          b => b.ServiceLocation,
                                                          b => b.User);

            var block = new ScheduleBlockViewModel
                            {
                                Id = fromDatabaseScheduleBlock.Id,
                                StartDateTime = fromDatabaseScheduleBlock.StartDateTime,
                                EndDateTime = fromDatabaseScheduleBlock.StartDateTime + _schedulingConfigurationViewService.GetSchedulingConfiguration().SpanSize,
                                Location = new ColoredViewModel
                                               {
                                                   Id = fromDatabaseScheduleBlock.ServiceLocation.Id,
                                                   Name = fromDatabaseScheduleBlock.ServiceLocation.ShortName,
                                                   Color = fromDatabaseScheduleBlock.ServiceLocation.Color
                                               },
                                Resource = new ResourceViewModel
                                               {
                                                   Id = fromDatabaseScheduleBlock.User.Id,
                                                   FirstName = fromDatabaseScheduleBlock.User.FirstName,
                                                   LastName = fromDatabaseScheduleBlock.User.LastName,
                                                   DisplayName = fromDatabaseScheduleBlock.User.UserName
                                               },
                                Category = fromDatabaseScheduleBlock.ScheduleBlockAppointmentCategories
                                    .Where(c => c.Id == scheduleBlockAppointmentCategoryId)
                                    .Select(i => new NamedViewModel<Guid>
                                                     {
                                                         Id = i.Id,
                                                         Name = i.AppointmentCategory.Name
                                                     }).FirstOrDefault()
                            };

            return block;
        }

        public AppointmentViewModel GetAppointment(int appointmentId)
        {
            var appointment = PracticeRepository.Appointments.OfType<UserAppointment>().First(a => a.Id == appointmentId);

            PracticeRepository.AsQueryableFactory().Load(appointment,
                                                          a => a.AppointmentType,
                                                          a => a.User,
                                                          a => a.Encounter,
                                                          a => a.Encounter.Patient,
                                                          a => a.Encounter.ServiceLocation,
                                                          a => a.Encounter.PatientInsuranceReferrals.Select(r => r.Encounters),
                                                          a => a.Encounter.PatientInsuranceReferrals.Select(pir => pir.Doctor),
                                                          a => a.Encounter.PatientInsuranceReferrals.Select(pir => pir.Patient),
                                                          a => a.Encounter.PatientInsuranceReferrals.Select(pir => pir.PatientInsurance),
                                                          a => a.Encounter.PatientInsuranceReferrals.Select(pir => pir.PatientInsurance.InsurancePolicy),
                                                          a => a.Encounter.PatientInsuranceReferrals.Select(pir => pir.PatientInsurance.InsurancePolicy.Insurer),
                                                         a => a.Encounter.PatientInsuranceAuthorizations);

            return _createAppointmentMapper(_schedulingConfigurationViewService.GetSchedulingConfiguration().SpanSize).Map(appointment);
        }

        public ObservableCollection<ReferralViewModel> GetPatientExistingReferralInformation(int patientId)
        {
            var fromDatabaseReferrals = PracticeRepository.PatientInsuranceReferrals.Where(r => r.PatientId == patientId).ToList();

            PracticeRepository.AsQueryableFactory().Load(fromDatabaseReferrals,
                                                          r => r.Encounters,
                                                          r => r.Doctor,
                                                          r => r.Patient,
                                                          r => r.PatientInsurance,
                                                          r => r.PatientInsurance.InsurancePolicy,
                                                          r => r.PatientInsurance.InsurancePolicy.Insurer);

            return fromDatabaseReferrals.Select(r => _referralMapper.Map(r)).ToExtendedObservableCollection();
        }

        public bool GetIsReferralInformationRequired(int patientId, InsuranceType insuranceType, DateTime dateTime)
        {
            return PracticeRepository.PatientInsurances.Any(i => i.InsuredPatientId == patientId && i.InsuranceType == insuranceType && InsuranceQueries.IsActiveForDateTime.Invoke(i, dateTime) && i.InsurancePolicy.Insurer.IsReferralRequired);
        }

        public ObservableCollection<AppointmentTypeViewModel> LoadAppointmentTypes(int? categoryId)
        {
            var fromDatabaseTypes = categoryId == null ? (from type in PracticeRepository.AppointmentTypes where !type.IsArchived select type).ToList() : (from type in PracticeRepository.AppointmentTypes where type.AppointmentCategoryId == categoryId && !type.IsArchived select type).ToList();
            return fromDatabaseTypes.OrderBy(i => i.OrdinalId).ThenBy(i => i.Name).Select(t => new AppointmentTypeViewModel { Id = t.Id, Name = t.Name, CategoryId = t.AppointmentCategoryId.GetValueOrDefault(), IsOrderRequired = t.IsOrderRequired }).ToExtendedObservableCollection();
        }

        [TransactionScope(TransactionScopeOption.Suppress, IsolationLevel.ReadUncommitted)]
        public virtual AppointmentViewModel SaveAppointment(ScheduleBlockViewModel scheduleBlockViewModel, AppointmentViewModel appointmentViewModel, AppointmentTypeViewModel appointmentTypeViewModel, InsuranceType insuranceType, NamedViewModel patientViewModel, List<ReferralViewModel> referrals, int? authorizationId, string reasonForVisit, string comment, DateTime? orderDate)
        {
            using (var unitOfWork = _unitOfWorkProvider.Create())
            {
                var userAppointment = SaveAppointmentHelper(scheduleBlockViewModel, appointmentViewModel, appointmentTypeViewModel, insuranceType, patientViewModel, referrals, authorizationId, comment, orderDate);
                unitOfWork.AcceptChanges();

                try
                {
                    var isPortalActive = PracticeRepository.ApplicationSettings.Where(a => a.Name == "IsPortalActive").SingleOrDefault().Value.ToString();
                    if (isPortalActive.ToLower() == "true")
                    {
                        var prtSrv = new PortalServices();
                        AppointmentEntity objTemp = new AppointmentEntity()
                        {
                            PracticeToken = PracticeRepository.ApplicationSettings.Where(a => a.Name == "PracticeAuthToken").SingleOrDefault().Value.ToString(),
                            PatientExternalId = patientViewModel.Id.ToString(),
                            LocationExternalId = scheduleBlockViewModel.Location.Id.ToString(),
                            DoctorExternalId = scheduleBlockViewModel.Resource.Id.ToString(),
                            AppointmentStatusExternalId = userAppointment.Encounter.EncounterStatusId.ToString(),
                            Start = scheduleBlockViewModel.StartDateTime,
                            End = scheduleBlockViewModel.EndDateTime,
                            ExternalId = userAppointment.Id.ToString()
                        };
                        prtSrv.NewAppointment(objTemp);
                    }
                }
                catch
                {

                }
                return _createAppointmentMapper(_schedulingConfigurationViewService.GetSchedulingConfiguration().SpanSize).Map(userAppointment);
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void DeleteAppointment(int appointmentId)
        {
            var encounter = PracticeRepository.Encounters
                .FirstOrDefault(e => e.Appointments.Any(a => a.Id == appointmentId));

            var scheduleBlockAppointmentCategories = PracticeRepository.ScheduleBlockAppointmentCategories.Where(c => c.AppointmentId == appointmentId);
            scheduleBlockAppointmentCategories.ForEachWithSinglePropertyChangedNotification(c => c.AppointmentId = null);

            PracticeRepository.Delete(encounter);
        }

        public NamedViewModel GetPatient(int patientId)
        {
            return PracticeRepository.Patients.Where(p => p.Id == patientId).ToArray()
                .Select(p => new NamedViewModel { Id = patientId, Name = p.DisplayName }).FirstOrDefault();
        }

        public bool IsPatientActive(int patientId)
        {
            return PracticeRepository.Patients.WithId(patientId).PatientStatus == PatientStatus.Active;
        }

        public bool HasPendingRecalls(int patientId)
        {
            var dates = PracticeRepository.PatientRecalls.Where(x => x.PatientId == patientId && x.RecallStatus == RecallStatus.Pending).Select(x => x.DueDateTime).ToList();
            return dates.Any(x => x.Date >= DateTime.Now.ToClientTime().Date);
        }

        #region SaveAppointmentHelpers

        private UserAppointment SaveAppointmentHelper(ScheduleBlockViewModel scheduleBlockViewModel, AppointmentViewModel appointmentViewModel, AppointmentTypeViewModel appointmentTypeViewModel, InsuranceType insuranceType, NamedViewModel patientViewModel, List<ReferralViewModel> referrals, int? authorizationId, string comment, DateTime? orderDate)
        {
            var referralIds = referrals.Select(r => r.Id).ToList();
            var appointment = appointmentViewModel == null ? CreateNewAppointment(scheduleBlockViewModel, appointmentTypeViewModel.Id, insuranceType, patientViewModel.Id, referralIds, authorizationId, comment, orderDate) : UpdateExistingAppointment(appointmentViewModel, appointmentTypeViewModel.Id, insuranceType, referralIds, authorizationId, comment, orderDate);
            PracticeRepository.Save(appointment);

            return appointment;
        }

        private UserAppointment CreateNewAppointment(ScheduleBlockViewModel scheduleBlock, int appointmentType, InsuranceType insuranceType, int patientId, List<int> referrals, int? authorization, string comment, DateTime? orderDate)
        {
            var appointment = new UserAppointment();
            appointment.DateTime = scheduleBlock.StartDateTime;
            appointment.UserId = scheduleBlock.Resource.Id;

            var encounter = new Encounter();
            encounter.EncounterStatus = EncounterStatus.Pending;
            encounter.PatientId = patientId;
            encounter.ServiceLocationId = scheduleBlock.Location.Id;

            appointment.Encounter = encounter;

            UpdateAppointment(appointmentType, insuranceType, referrals, authorization, comment, appointment, scheduleBlock.Location.Id, patientId, scheduleBlock.Resource.Id, orderDate);

            if (scheduleBlock.Category != null)
            {
                appointment.ScheduleBlockAppointmentCategories.Add(PracticeRepository.ScheduleBlockAppointmentCategories.WithId(scheduleBlock.Category.Id));
            }

            return appointment;
        }

        private UserAppointment UpdateExistingAppointment(AppointmentViewModel appointment, int appointmentType, InsuranceType insuranceType, List<int> referrals, int? authorization, string comment, DateTime? orderDate)
        {
            var fromDatabaseAppointment = PracticeRepository.Appointments.OfType<UserAppointment>().Where(a => a.Id == appointment.Id)
                .Include(a => a.User)
                .Include(a => a.Encounter.Patient)
                .Include(a => a.Encounter.ServiceLocation)
                .Include(a => a.Encounter.PatientInsuranceReferrals.Select(r => new { r.Doctor, r.PatientInsurance.InsurancePolicy.Insurer }))
                .Include(a => a.Encounter.PatientInsuranceAuthorizations)
                .First();

            UpdateAppointment(appointmentType, insuranceType, referrals, authorization, comment, fromDatabaseAppointment, null, null, null, orderDate);

            return fromDatabaseAppointment;
        }

        private void UpdateAppointment(int appointmentType, InsuranceType insuranceType, List<int> referrals, int? authorization, string comment, UserAppointment fromDatabaseAppointment,
                                       int? locationId, int? patientId, int? resourceId, DateTime? orderDate)
        {
            fromDatabaseAppointment.OrderDate = orderDate;
            fromDatabaseAppointment.AppointmentType = PracticeRepository.AppointmentTypes.WithId(appointmentType);
            fromDatabaseAppointment.Encounter.EncounterTypeId = fromDatabaseAppointment.AppointmentType.EncounterTypeId.GetValueOrDefault();
            fromDatabaseAppointment.Encounter.EncounterTypeId = PracticeRepository.AppointmentTypes.WithId(appointmentType).EncounterTypeId.GetValueOrDefault();
            fromDatabaseAppointment.Encounter.InsuranceType = insuranceType;

            if (locationId.HasValue) fromDatabaseAppointment.Encounter.ServiceLocation = PracticeRepository.ServiceLocations.WithId(locationId.Value);

            if (patientId.HasValue) fromDatabaseAppointment.Encounter.Patient = PracticeRepository.Patients.WithId(patientId.Value);

            if (resourceId.HasValue) fromDatabaseAppointment.User = PracticeRepository.Users.WithId(resourceId);

            if (referrals.Count == 0)
                fromDatabaseAppointment.Encounter.PatientInsuranceReferrals.Clear();
            else
                fromDatabaseAppointment.Encounter.PatientInsuranceReferrals = PracticeRepository.PatientInsuranceReferrals
                     .Include(r => new { r.Doctor, r.PatientInsurance.InsurancePolicy.Insurer })
                    .Where(pir => referrals.Contains(pir.Id)).ToList();

            fromDatabaseAppointment.Comment = comment;

            fromDatabaseAppointment.Encounter.PatientInsuranceAuthorizations.Clear();
            if (authorization.HasValue)
            {
                fromDatabaseAppointment.Encounter.PatientInsuranceAuthorizations.Add(PracticeRepository.PatientInsuranceAuthorizations.WithId(authorization));
            }
        }

        #endregion

    }

    internal class AppointmentViewModelMap : IMap<UserAppointment, AppointmentViewModel>
    {
        private readonly IEnumerable<IMapMember<UserAppointment, AppointmentViewModel>> _members;

        public AppointmentViewModelMap(TimeSpan schedulingIncrement, IMapper<PatientInsuranceReferral, ReferralViewModel> referralMapper)
        {
            _members = this.CreateMembers(a =>
                                          new AppointmentViewModel
                                              {
                                                  Id = a.Id,
                                                  StartDateTime = a.DateTime,
                                                  EndDateTime = a.DateTime + schedulingIncrement,
                                                  Type = new AppointmentTypeViewModel
                                                      {
                                                          Id = a.AppointmentTypeId,
                                                          Name = a.AppointmentType.Name,
                                                          CategoryId = a.AppointmentType.AppointmentCategoryId.GetValueOrDefault(),
                                                      },
                                                  InsuranceTypeId = (int)a.Encounter.InsuranceType,
                                                  Resource = new ResourceViewModel
                                                                 {
                                                                     Id = a.User.Id,
                                                                     FirstName = a.User.FirstName,
                                                                     LastName = a.User.LastName,
                                                                     DisplayName = a.User.UserName
                                                                 },
                                                  Patient = new NamedViewModel { Id = a.Encounter.Patient.Id, Name = a.Encounter.Patient.DisplayName },
                                                  Location = new ColoredViewModel { Id = a.Encounter.ServiceLocationId, Name = a.Encounter.ServiceLocation.ShortName, Color = a.Encounter.ServiceLocation.Color },
                                                  Encounter = new EncounterViewModel
                                                                  {
                                                                      Id = a.EncounterId,
                                                                      Authorization = CreateAuthorization(a.Encounter.PatientInsuranceAuthorizations.LastOrDefault()),
                                                                      Comment = a.Comment,
                                                                      Referrals = a.Encounter.PatientInsuranceReferrals.Select(referralMapper.Map).ToExtendedObservableCollection()
                                                                  }
                                              });
        }

        private static PatientInsuranceAuthorizationViewModel CreateAuthorization(Model.PatientInsuranceAuthorization auth)
        {

            if (auth == null) return null;
            return new PatientInsuranceAuthorizationViewModel
                       {
                           Id = auth.Id,
                           Code = auth.AuthorizationCode,
                           Date = auth.AuthorizationDateTime,
                           PatientInsuranceId = auth.PatientInsuranceId,
                           EncounterId = auth.EncounterId,
                           Comments = auth.Comment,
                           Location = new ColoredViewModel
                                          {
                                              Id = auth.Encounter.ServiceLocationId,
                                              Name = auth.Encounter.ServiceLocation.ShortName,
                                              Color = auth.Encounter.ServiceLocation.Color
                                          }
                       };
        }

        public IEnumerable<IMapMember<UserAppointment, AppointmentViewModel>> Members
        {
            get { return _members; }
        }
    }

}