﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using IO.Practiceware.Presentation.ViewServices.BatchBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

[assembly: Component(typeof (BatchBuilderFiltersViewService), typeof (IBatchBuilderFiltersViewService))]

namespace IO.Practiceware.Presentation.ViewServices.BatchBuilder
{
    /// <summary>
    /// </summary>
    public interface IBatchBuilderFiltersViewService
    {
        ObservableCollection<BatchBuilderDropDownFilterViewModel> GetAllDropDownFilterChoices(BatchBuilderSearchType searchType);
    }

    internal class BatchBuilderFiltersViewService : BaseViewService, IBatchBuilderFiltersViewService
    {
        #region IBatchBuilderFiltersViewService Members

        /// <summary>
        ///   Retrieves the drop down filter Choices.
        /// </summary>
        /// <returns> </returns>
        public ObservableCollection<BatchBuilderDropDownFilterViewModel> GetAllDropDownFilterChoices(BatchBuilderSearchType searchType)
        {
            ObservableCollection<BatchBuilderDropDownFilterViewModel> filters = null;
            if (searchType == BatchBuilderSearchType.Patient)
                filters = new BatchBuilderPatientFilterSource().Filters
                    .Where(f => f.GetType() == typeof (BatchBuilderDropDownFilterViewModel)).Select(g => new BatchBuilderDropDownFilterViewModel(g.Name, g.GroupName)).ToExtendedObservableCollection();
            else if (searchType == BatchBuilderSearchType.Appointment)
                filters = new BatchBuilderAppointmentFilterSource().Filters
                    .Where(f => f.GetType() == typeof (BatchBuilderDropDownFilterViewModel)).Select(g => new BatchBuilderDropDownFilterViewModel(g.Name, g.GroupName)).ToExtendedObservableCollection();
            if (filters != null)
                foreach (BatchBuilderDropDownFilterViewModel filter in filters)
                {
                    switch (filter.Name)
                    {
                        case BatchBuilderFilterNames.AppointmentType:
                            {
                                filter.Choices = PracticeRepository.AppointmentTypes.Select(at => at.Name).ToExtendedObservableCollection();
                            }
                            break;
                        case BatchBuilderFilterNames.AppointmentStatus:
                            {
                                IEnumerable<EncounterStatus> encounterStatuses = Enums.GetValues<EncounterStatus>();
                                filter.Choices = encounterStatuses.Select(es => es.GetDisplayName()).ToExtendedObservableCollection();
                            }
                            break;
                        case BatchBuilderFilterNames.ScheduledDoctor:
                            {
                                filter.Choices = PracticeRepository.Users.OfType<Doctor>().Select(p => p.DisplayName).ToExtendedObservableCollection();
                            }
                            break;
                        case BatchBuilderFilterNames.ScheduledLocation:
                            {
                                filter.Choices = PracticeRepository.ServiceLocations.Select(s => s.Name).ToExtendedObservableCollection();
                            }
                            break;
                        case BatchBuilderFilterNames.PatientType:
                            {
                                filter.Choices = PracticeRepository.Tags.Select(p => p.DisplayName).ToExtendedObservableCollection();
                            }
                            break;
                        case BatchBuilderFilterNames.ReferringDoctor:
                            {
                                filter.Choices = PracticeRepository.PatientExternalProviders.Where(p => p.IsReferringPhysician).Select(p => p.ExternalProvider.DisplayName).ToExtendedObservableCollection();
                            }
                            break;
                        case BatchBuilderFilterNames.InsuranceType:
                            {
                                filter.Choices = Enums.GetValues<InsuranceType>().Select(es => es.GetAttribute<DisplayAttribute>().Name).ToExtendedObservableCollection();
                            }
                            break;
                        case BatchBuilderFilterNames.InsurerPlanType:
                            {
                                filter.Choices = PracticeRepository.InsurerPlanTypes.Select(p => p.Name).ToExtendedObservableCollection();
                            }
                            break;
                    }
                }

            if (filters != null)
            {
                foreach (var filter in filters)
                {

                    filter.Choices = filter.Choices.Where(c => c != null).ToExtendedObservableCollection();
                }
            }
            return filters;
        }

        #endregion
    }
}