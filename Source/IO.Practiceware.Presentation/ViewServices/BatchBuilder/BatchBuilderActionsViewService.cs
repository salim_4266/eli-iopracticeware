﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using IO.Practiceware.Presentation.ViewServices.BatchBuilder;
using IO.Practiceware.Services.ExternalSystems;
using Soaf.Collections;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

[assembly: Component(typeof (BatchBuilderActionsViewService), typeof (IBatchBuilderActionsViewService))]

namespace IO.Practiceware.Presentation.ViewServices.BatchBuilder
{
    public interface IBatchBuilderActionsViewService
    {
        /// <summary>
        ///   Retrieves actions.
        /// </summary>
        /// <returns> </returns>
        IEnumerable<BatchBuilderActionsGroupViewModel> GetPatientActions();


        /// <summary>
        ///   Retrieves actions.
        /// </summary>
        /// <returns> </returns>
        IEnumerable<BatchBuilderActionsGroupViewModel> GetAppointmentActions();

        /// <summary>
        ///   Performs the specified action.
        /// </summary>
        /// <param name="patientIds"> The patient ids. </param>
        /// <param name="action"> The action. </param>
        void PerformPatientAction(IEnumerable<int> patientIds, BatchBuilderActionViewModel action);


        /// <summary>
        ///   Performs the specified action.
        /// </summary>
        /// <param name="appointmentIds"> The appointment ids. </param>
        /// <param name="action"> The action. </param>
        void PerformAppointmentAction(IEnumerable<int> appointmentIds, BatchBuilderActionViewModel action);
    }

    internal class BatchBuilderActionsViewService : BaseViewService, IBatchBuilderActionsViewService
    {
        private readonly Func<BatchBuilderConfirmationActionViewModel> _createConfirmationAction;
        private readonly Func<BatchBuilderConfirmationsActionsGroupViewModel> _createConfirmationsGroup;
        private readonly IExternalSystemService _externalSystemService;        

        public BatchBuilderActionsViewService(
                                              IExternalSystemService externalSystemService,
                                              Func<BatchBuilderConfirmationActionViewModel> createConfirmationAction,
                                              Func<BatchBuilderConfirmationsActionsGroupViewModel>
                                                  createConfirmationsGroup)
        {            
            _externalSystemService = externalSystemService;
            _createConfirmationAction = createConfirmationAction;
            _createConfirmationsGroup = createConfirmationsGroup;
        }

        #region IBatchBuilderActionsViewService Members

        public IEnumerable<BatchBuilderActionsGroupViewModel> GetPatientActions()
        {
            return new BatchBuilderActionsGroupViewModel[]
                       {
                           new BatchBuilderEligibilityActionsGroupViewModel(),
                           new BatchBuilderRecallsActionsGroupViewModel()
                       };
        }

        public IEnumerable<BatchBuilderActionsGroupViewModel> GetAppointmentActions()
        {
            return new[]
                       {
                           CreateConfirmationsGroup(),
                           new BatchBuilderLettersAndFormsActionsGroupViewModel()
                       };
        }

        public void PerformPatientAction(IEnumerable<int> patientIds, BatchBuilderActionViewModel action)
        {
            var batchBuilderConfirmationActionViewModel = action as BatchBuilderConfirmationActionViewModel;
            if (batchBuilderConfirmationActionViewModel != null)
            {
                QueueConfirmation(patientIds, batchBuilderConfirmationActionViewModel,
                                  PracticeRepositoryEntityId.Patient);
            }
        }

        public void PerformAppointmentAction(IEnumerable<int> appointmentIds, BatchBuilderActionViewModel action)
        {
            var batchBuilderConfirmationActionViewModel = action as BatchBuilderConfirmationActionViewModel;
            if (batchBuilderConfirmationActionViewModel != null)
            {
                QueueConfirmation(appointmentIds, batchBuilderConfirmationActionViewModel,
                                  PracticeRepositoryEntityId.Appointment);
            }
        }

        #endregion

        private BatchBuilderActionsGroupViewModel CreateConfirmationsGroup()
        {
            BatchBuilderConfirmationsActionsGroupViewModel group = _createConfirmationsGroup();
            group.
                EmailAction = CreateConfirmationAction(BatchBuilderConfirmationType.Email,
                                                       GetTemplates(ExternalSystemId.Email));
            group.TextMessageAction = CreateConfirmationAction(BatchBuilderConfirmationType.TextMessage,
                                                               GetTemplates(ExternalSystemId.TextMessage));

            return group;
        }

        private BatchBuilderConfirmationActionViewModel CreateConfirmationAction(BatchBuilderConfirmationType type,
                                                                                 ObservableCollection<string> templates)
        {
            BatchBuilderConfirmationActionViewModel action = _createConfirmationAction();
            action.ConfirmationType = type;
            action.Templates = templates;
            return action;
        }

        private ObservableCollection<string> GetTemplates(ExternalSystemId externalSystem)
        {
            return
                PracticeRepository.ExternalSystemExternalSystemMessageTypes.Where(
                    e => e.ExternalSystemId == (int) externalSystem).Select(
                        e => e.TemplateFile).ToArray().ToExtendedObservableCollection();
        }

        private void QueueConfirmation(IEnumerable<int> ids, BatchBuilderConfirmationActionViewModel action,
                                       PracticeRepositoryEntityId practiceRepositoryEntityId)
        {
            ExternalSystemMessage message = _externalSystemService.CreateMessage(
                new Dictionary<PracticeRepositoryEntityId, IEnumerable<object>>
                    {{practiceRepositoryEntityId, ids.OfType<object>().ToArray()}},
                action.ConfirmationType.ToString(),
                ExternalSystemMessageTypeId.Confirmation_Outbound, action.SelectedTemplate);
            message.CreatedBy = action.ConfirmationType == BatchBuilderConfirmationType.Email
                                    ? "SendMail"
                                    : "SendTextMessage";
            message.Description = action.ConfirmationType.ToString();
            message.Value = string.Empty;
            message.ExternalSystemMessageProcessingStateId = (int) ExternalSystemMessageProcessingStateId.Unprocessed;
            message.CreatedDateTime = DateTime.UtcNow;
            message.UpdatedDateTime = DateTime.UtcNow;
            PracticeRepository.Save(message);
        }
    }
}