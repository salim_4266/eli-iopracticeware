﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using IO.Practiceware.Presentation.ViewServices.BatchBuilder;
using Soaf.Collections;
using Soaf.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

[assembly: Component(typeof(BatchBuilderTemplatesViewService), typeof(IBatchBuilderTemplatesViewService))]

namespace IO.Practiceware.Presentation.ViewServices.BatchBuilder
{
    public interface IBatchBuilderTemplatesViewService
    {
        /// <summary>
        /// Gets all the batch builder templates.
        /// </summary>
        /// <returns></returns>
        IEnumerable<BatchBuilderTemplateViewModel> GetBatchBuilderTemplates();

        /// <summary>
        /// Saves the batch builder template.
        /// </summary>
        /// <param name="templateViewModel">The template view model.</param>
        BatchBuilderTemplateViewModel SaveBatchBuilderTemplate(BatchBuilderTemplateViewModel templateViewModel);

        /// <summary>
        /// Removes the batch builder template.
        /// </summary>
        /// <param name="templateViewModel">The template view model.</param>
        void RemoveBatchBuilderTemplate(BatchBuilderTemplateViewModel templateViewModel);
    }

    internal class BatchBuilderTemplatesViewService : BaseViewService, IBatchBuilderTemplatesViewService
    {        
        #region IBatchBuilderTemplatesViewService Members

        public IEnumerable<BatchBuilderTemplateViewModel> GetBatchBuilderTemplates()
        {
            IEnumerable<BatchBuilderTemplate> batchBuilderTemplates = PracticeRepository.BatchBuilderTemplates.ToList();

            var templateViewModels = batchBuilderTemplates
                .Select(i => new { Entity = i, ViewModel = TryDeserialize(i.TemplateXml) })
                .Where(i => i.ViewModel != null)
                .ToArray()
                .ForEachWithSinglePropertyChangedNotification(i => i.ViewModel.Id = i.Entity.Id)
                .Select(i => i.ViewModel).OrderBy(i => i.Name)
                .ToList();

            return templateViewModels;
        }

        [DebuggerNonUserCode]
        private static BatchBuilderTemplateViewModel TryDeserialize(string templateXml)
        {
            try
            {
                return templateXml.FromXml<BatchBuilderTemplateViewModel>();
            }
            catch
            {
                return null;
            }
        }

        public BatchBuilderTemplateViewModel SaveBatchBuilderTemplate(BatchBuilderTemplateViewModel templateViewModel)
        {
            BatchBuilderTemplate batchBuilderTemplate =
                templateViewModel.Id == 0
                    ? new BatchBuilderTemplate() :
                                                     PracticeRepository.BatchBuilderTemplates.Single(t => t.Id == templateViewModel.Id);

            batchBuilderTemplate.TemplateXml = templateViewModel.ToXml();

            PracticeRepository.Save(batchBuilderTemplate);

            templateViewModel.Id = batchBuilderTemplate.Id;

            return templateViewModel;
        }

        public void RemoveBatchBuilderTemplate(BatchBuilderTemplateViewModel templateViewModel)
        {
            BatchBuilderTemplate batchBuilderTemplate = PracticeRepository.BatchBuilderTemplates.Single(t => t.Id == templateViewModel.Id);
            PracticeRepository.Delete(batchBuilderTemplate);
        }
        #endregion
    }

}