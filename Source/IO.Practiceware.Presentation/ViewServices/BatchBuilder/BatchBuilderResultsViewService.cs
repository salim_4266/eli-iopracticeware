﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.BatchBuilder;
using IO.Practiceware.Presentation.ViewServices.BatchBuilder;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

[assembly: Component(typeof (BatchBuilderResultsViewService), typeof (IBatchBuilderResultsViewService))]
[assembly:
    Component(typeof (BatchBuilderAppointmentResultViewModelMap),
        typeof (IMap<UserAppointment, BatchBuilderAppointmentResultViewModel>))]
[assembly:
    Component(typeof (BatchBuilderPatientResultViewModelMap), typeof (IMap<Patient, BatchBuilderPatientResultViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.BatchBuilder
{
    public interface IBatchBuilderResultsViewService
    {
        /// <summary>
        ///   Retrieves BatchResults.
        /// </summary>
        /// <returns> </returns>
        IEnumerable<BatchBuilderResultViewModel> GetBatchResults(
            IEnumerable<BatchBuilderFilterViewModel> filters, BatchBuilderSearchType searchType);
    }

    internal class BatchBuilderResultsViewService : BaseViewService, IBatchBuilderResultsViewService
    {
        private readonly IMapper<UserAppointment, BatchBuilderAppointmentResultViewModel>
            _batchBuilderAppointmentResultViewModelMapper;

        private readonly IMapper<Patient, BatchBuilderPatientResultViewModel> _batchBuilderPatientResultViewModelMapper;        

        public BatchBuilderResultsViewService(
                                              IMapper<UserAppointment, BatchBuilderAppointmentResultViewModel>
                                                  batchBuilderAppointmentResultViewModelMapper,
                                              IMapper<Patient, BatchBuilderPatientResultViewModel>
                                                  batchBuilderPatientResultViewModelMapper)
        {            
            _batchBuilderAppointmentResultViewModelMapper = batchBuilderAppointmentResultViewModelMapper;
            _batchBuilderPatientResultViewModelMapper = batchBuilderPatientResultViewModelMapper;
        }

        #region IBatchBuilderResultsViewService Members

        /// <summary>
        ///   Retrieves all the Results.
        /// </summary>
        /// <returns> </returns>
        public IEnumerable<BatchBuilderResultViewModel> GetBatchResults(
            IEnumerable<BatchBuilderFilterViewModel> filters, BatchBuilderSearchType searchType)
        {
            switch (searchType)
            {
                case BatchBuilderSearchType.Patient:
                    return GetPatientBatchResults(filters);
                case BatchBuilderSearchType.Appointment:
                    return GetAppointmentBatchResults(filters);
            }
            return null;
        }

        #endregion

        private IEnumerable<BatchBuilderPatientResultViewModel> GetPatientBatchResults(
            IEnumerable<BatchBuilderFilterViewModel> filters)
        {
            IQueryable<Patient> patients =
                PracticeRepository.Patients.Include(p => p.PatientTags.Select(y => y.Tag));

            foreach (BatchBuilderFilterViewModel filter in filters)
            {
                switch (filter.Name)
                {
                    case BatchBuilderFilterNames.PatientLastName:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && patients != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<Patient, bool>> predicate = textFilter.Text.Split(';').Select(f =>
                                                                                                                  patients.CreatePredicate(p => p.LastName.ToLower().StartsWith(f.ToLower())))
                                        .Aggregate((x, y) => x.Or(y));
                                    patients = patients.Where(predicate);
                                }
                                else
                                    patients = patients.Where(
                                        p => p.LastName.StartsWith(textFilter.Text.Trim().ToUpper()));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.PatientFirstName:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && patients != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<Patient, bool>> predicate = textFilter.Text.Split(';').Select(f =>
                                                                                                                  patients.CreatePredicate(p => p.FirstName.ToLower().StartsWith(f.ToLower())))
                                        .Aggregate((x, y) => x.Or(y));
                                    patients = patients.Where(predicate);
                                }
                                else
                                    patients =
                                        patients.Where(p => p.FirstName.StartsWith(textFilter.Text.Trim().ToUpper()));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.PatientId:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && patients != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<Patient, bool>> predicate = textFilter.Text.Split(';').Select(f => f.IsNotNullOrEmpty() && f.Trim().IsNumeric() ? patients.CreatePredicate(p => p.Id.Equals(Convert.ToInt32(f.Trim()))) : patients.CreatePredicate(p => p.Id.Equals(0)))
                                        .Aggregate((x, y) => x.Or(y));
                                    patients = patients.Where(predicate);
                                }
                                else if (textFilter.Text.IsNumeric())
                                    patients = patients.Where(p => p.Id == Convert.ToInt32(textFilter.Text));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.PatientType:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && patients != null)
                                patients =
                                    patients.Where(
                                        p =>
                                        p.PatientTags.Any(pt => dropDownFilter.SelectedChoices.Contains(pt.Tag.DisplayName)));
                        }
                        break;
                    case BatchBuilderFilterNames.PatientAge:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && patients != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<Patient, bool>> predicate = textFilter.Text.Split(';').Select(f => f.IsNotNullOrEmpty()
                                                                                                                       && f.Trim().IsNumeric() ? patients.CreatePredicate(p => p.DateOfBirth != null && PatientQueries.GetAge(p) == Convert.ToInt32(f.Trim())) : patients.CreatePredicate(p => p.DateOfBirth != null && PatientQueries.GetAge(p) == 0))
                                        .Aggregate((x, y) => x.Or(y));
                                    patients = patients.Where(predicate);
                                }
                                else if (textFilter.Text.IsNumeric())
                                {
                                    int age = Convert.ToInt32(textFilter.Text);
                                    patients =
                                        patients.Where(p => p.DateOfBirth != null && PatientQueries.GetAge(p) == age);
                                }
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.PatientDob:
                        {
                            var dateRangeFilter = (BatchBuilderDateRangeFilterViewModel) filter;
                            if (dateRangeFilter.DateRanges !=null)
                            {
                                Expression<Func<Patient, bool>> predicate = dateRangeFilter.DateRanges.Select(f => (f.Start != null || f.End != null) ?
                                                                                                                                                          patients.CreatePredicate(p => (f.Start != null && f.End != null) ? p.DateOfBirth >= f.Start && p.DateOfBirth <= f.End
                                                                                                                                                                                            : p.DateOfBirth == f.Start || p.DateOfBirth == f.End) : null)
                                    .Aggregate((x, y) => x.Or(y));
                                if (patients != null && predicate != null) patients = patients.Where(predicate);
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.AppointmentsDateRange:
                    case BatchBuilderFilterNames.AppointmentsToday:
                    case BatchBuilderFilterNames.AppointmentsTommorrow:
                    case BatchBuilderFilterNames.AppointmentsWeek:
                    case BatchBuilderFilterNames.AppointmentsMonth:
                        {
                            var dateRangeFilter = (BatchBuilderDateRangeFilterViewModel) filter;
                            if (dateRangeFilter.DateRanges !=null)
                            {
                                Expression<Func<Patient, bool>> predicate = dateRangeFilter.DateRanges.Select(f => (f.Start != null || f.End != null) ?
                                                                                                                                                      patients.CreatePredicate(p =>
                                                                                                                                                                               p.Encounters.Any(
                                                                                                                                                                                   e =>
                                                                                                                                                                                   e.Appointments.Any(
                                                                                                                                                                                       a => (f.Start != null && f.End != null) ? a.DateTime >= f.Start && a.DateTime <= f.End
                                                                                                                                                                                                : a.DateTime == f.Start || a.DateTime == f.End))) : null)
                                .Aggregate((x, y) => x.Or(y));
                                if (patients != null && predicate != null) patients = patients.Where(predicate);
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.AppointmentType:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && patients != null)
                                patients =
                                    patients.Where(
                                        p =>
                                        p.Encounters.Any(
                                            e =>
                                            e.Appointments.Any(
                                                a => dropDownFilter.SelectedChoices.Contains(a.AppointmentType.Name))));
                        }
                        break;
                    case BatchBuilderFilterNames.AppointmentStatus:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && patients != null)
                            {
                                IEnumerable<int> ids =
                                    dropDownFilter.SelectedChoices.Select(
                                        i => (int) i.ToEnumFromDisplayName<EncounterStatus>());
                                patients =
                                    patients.Where(
                                        p =>
                                        p.Encounters.Any(
                                            e => ids.
                                                     Contains(
                                                         (int)e.EncounterStatus)));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.ScheduledDoctor:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && patients != null)
                                patients =
                                    patients.Where(
                                        p =>
                                        p.Encounters.Any(
                                            e =>
                                            e.Appointments.OfType<UserAppointment>().Any(
                                                a => dropDownFilter.SelectedChoices.Contains(a.User.DisplayName))));
                        }
                        break;
                    case BatchBuilderFilterNames.ScheduledLocation:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && patients != null)
                                patients =
                                    patients.Where(
                                        p =>
                                        p.Encounters.Any(
                                            e => dropDownFilter.SelectedChoices.Contains(e.ServiceLocation.Name)));
                        }
                        break;

                    case BatchBuilderFilterNames.ReferringDoctor:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && patients != null)
                                patients =
                                    patients.Where(
                                        p =>
                                        p.ExternalProviders.Any(
                                            ep =>
                                            dropDownFilter.SelectedChoices.Contains(ep.ExternalProvider.DisplayName)));
                        }
                        break;
                    case BatchBuilderFilterNames.Diagnosis:
                        {
                        }
                        break;
                    case BatchBuilderFilterNames.InsurerName:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && patients != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<Patient, bool>> predicate = textFilter.Text.Split(';').Select(f =>
                                                                                                                  patients.CreatePredicate(p => p.PatientInsurances.Any(pi => pi.InsurancePolicy != null && pi.InsurancePolicy.Insurer != null && pi.InsurancePolicy.Insurer.Name.ToLower().StartsWith(f.ToLower()))))
                                        .Aggregate((x, y) => x.Or(y));
                                    patients = patients.Where(predicate);
                                }
                                else
                                    patients =
                                        patients.Where(
                                            p =>
                                            p.PatientInsurances.Any(
                                                pi =>
                                                pi.InsurancePolicy != null && pi.InsurancePolicy.Insurer != null &&
                                                pi.InsurancePolicy.Insurer.Name.StartsWith(
                                                    textFilter.Text.Trim().ToUpper())));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.InsuranceType:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && patients != null)
                            {
                                IEnumerable<int> ids =
                                    dropDownFilter.SelectedChoices.Select(
                                        i => (int) i.ToEnumFromDisplayName<InsuranceType>());
                                patients =
                                    patients.Where(
                                        p =>
                                        p.PatientInsurances.Any(
                                            i => ids.
                                                     Contains(
                                                         (int) i.InsuranceType)));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.InsurerPlanType:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && patients != null)
                                patients =
                                    patients.Where(
                                        p =>
                                        p.PatientInsurances.Any(i => dropDownFilter.SelectedChoices.Contains(
                                            i.InsurancePolicy.Insurer.InsurerPlanType.Name)));
                        }
                        break;
                    case BatchBuilderFilterNames.PatientPhone:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && patients != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<Patient, bool>> predicate = textFilter.Text.Split(';').Select(f =>
                                                                                                                  patients.CreatePredicate(p => p.PatientPhoneNumbers.Any(pi => pi.ExchangeAndSuffix != null && pi.ExchangeAndSuffix.ToLower().StartsWith(f.Trim().ToLower()))))
                                        .Aggregate((x, y) => x.Or(y));
                                    patients = patients.Where(predicate);
                                }
                                else
                                    patients =
                                        patients.Where(
                                            p =>
                                            p.PatientPhoneNumbers.Any(
                                                pp =>
                                                pp.ExchangeAndSuffix != null && pp.ExchangeAndSuffix.StartsWith(
                                                    textFilter.Text.Trim().ToUpper())));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.PrimaryInsuranceName:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && patients != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<Patient, bool>> predicate = textFilter.Text.Split(';').Select(f =>
                                                                                                                  patients.CreatePredicate(p => p.PatientInsurances.Where(pi => pi.OrdinalId == p.PatientInsurances.OrderBy(ppi => ppi.OrdinalId).Select(ppi => ppi.OrdinalId).FirstOrDefault()).Any(pi => pi.InsurancePolicy != null && pi.InsurancePolicy.Insurer != null && pi.InsurancePolicy.Insurer.Name.ToLower().StartsWith(f.Trim().ToLower()))))
                                        .Aggregate((x, y) => x.Or(y));
                                    patients = patients.Where(predicate);
                                }
                                else
                                    patients =
                                        patients.Where(
                                            p =>
                                            p.PatientInsurances.Where(pi => pi.OrdinalId == p.PatientInsurances.OrderBy(ppi => ppi.OrdinalId).Select(ppi => ppi.OrdinalId).FirstOrDefault()).Any(
                                                pi =>
                                                pi.InsurancePolicy != null && pi.InsurancePolicy.Insurer != null && pi.InsurancePolicy.Insurer.Name.StartsWith(
                                                    textFilter.Text.Trim().ToUpper())));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.NonPrimaryInsuranceName:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && patients != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<Patient, bool>> predicate = textFilter.Text.Split(';').Select(f =>
                                                                                                                  patients.CreatePredicate(p => p.PatientInsurances.Where(pi => pi.OrdinalId != p.PatientInsurances.OrderBy(ppi => ppi.OrdinalId).Select(ppi => ppi.OrdinalId).FirstOrDefault()).Any(pi => pi.InsurancePolicy != null && pi.InsurancePolicy.PolicyCode.ToLower().StartsWith(f.Trim().ToLower()))))
                                        .Aggregate((x, y) => x.Or(y));
                                    patients = patients.Where(predicate);
                                }
                                else
                                    patients =
                                        patients.Where(
                                            p =>
                                            p.PatientInsurances.Where(pi => pi.OrdinalId != p.PatientInsurances.OrderBy(ppi => ppi.OrdinalId).Select(ppi => ppi.OrdinalId).FirstOrDefault()).Any(
                                                pi =>
                                                pi.InsurancePolicy != null && pi.InsurancePolicy.PolicyCode.StartsWith(
                                                    textFilter.Text.Trim().ToUpper())));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.ActiveDateRange:
                        {
                            var dateRangeFilter = (BatchBuilderDateRangeFilterViewModel) filter;
                            if (dateRangeFilter.DateRanges != null)
                            {
                                Expression<Func<Patient, bool>> predicate = dateRangeFilter.DateRanges.Select(f => (f.Start != null || f.End != null) ?
                                                                                                                                                      patients.CreatePredicate(p =>
                                                                                                                                                                               p.PatientInsurances.Any(
                                                                                                                                                                                   pi =>
                                                                                                                                                                                   pi.InsurancePolicy != null && !pi.IsDeleted && (
                                                                                                                                                                                                                                                      (f.Start != null && f.End != null) ? (pi.InsurancePolicy.StartDateTime <= f.Start && (pi.EndDateTime >= f.Start || pi.EndDateTime == null)) || pi.InsurancePolicy.StartDateTime <= f.End && (pi.EndDateTime >= f.End || pi.EndDateTime == null)
                                                                                                                                                                                                                                                          : (f.Start >= pi.InsurancePolicy.StartDateTime && pi.EndDateTime == null) || (f.End >= pi.InsurancePolicy.StartDateTime && pi.EndDateTime == null)))) : null)
                                .Aggregate((x, y) => x.Or(y));
                                if (patients != null && predicate != null) patients = patients.Where(predicate);
                            }
                        }
                        break;

                        //Eligibility responses filters
                    case BatchBuilderFilterNames.ResponseDateRange:
                    case BatchBuilderFilterNames.ResponsesYesterday:
                    case BatchBuilderFilterNames.ResponsesToday:
                    case BatchBuilderFilterNames.ResponsesLastWeek:
                        {
                            //var dateRangeFilter = (BatchBuilderDateRangeFilterViewModel) filter;
                            //if (dateRangeFilter.DateRanges != null)
                            // {
                            //Expression<Func<Patient, bool>> predicate = dateRangeFilter.DateRanges.Select(f => (f.Start != null || f.End != null) ?
                            //                                                                                                                          patients.CreatePredicate(p =>
                            //                                                                                                                                                   p.X12EligibilityPatient.Any(
                            //                                                                                                                                                       x =>
                            //                                                                                                                                                       x.PatientDateOfBirth != null && ((f.Start != null && f.End != null) ? p.DateOfBirth >= f.Start && p.DateOfBirth <= f.End
                            //                                                                                                                                                                                            : p.DateOfBirth == f.Start || p.DateOfBirth == f.End))) : null)
                            //    .Aggregate((x, y) => x.Or(y));
                            //if (patients != null  && predicate != null) patients = patients.Where(predicate);
                            //}
                        }
                        break;
                }
            }

            if (patients != null)
            {
                Patient[] results = patients.ToArray();

                PracticeRepository.AsQueryableFactory().Load(results,
                                                              p => p.Encounters,
                                                              p =>
                                                              p.Encounters.Select(
                                                                  e =>
                                                                  e.Appointments.Select(
                                                                      a => a.AppointmentType))
                                                              , p => p.Encounters.Select(e => e.EncounterType)
                                                              , p => p.Encounters.Select(e => e.ServiceLocation)
                                                              , p =>
                                                                p.PatientInsurances.Select(
                                                                    pi => pi.InsurancePolicy.Insurer.InsurerPlanType)
                                                              ,
                                                              p => p.ExternalProviders.Select(ep => ep.ExternalProvider));

                BatchBuilderPatientResultViewModel[] viewModel =
                    results.Select(patient => _batchBuilderPatientResultViewModelMapper.Map(patient)).ToArray();
                return viewModel;
            }
            return null;
        }

        private IEnumerable<BatchBuilderAppointmentResultViewModel> GetAppointmentBatchResults(
            IEnumerable<BatchBuilderFilterViewModel> filters)
        {
            IQueryable<UserAppointment> appointments = PracticeRepository.Appointments.OfType<UserAppointment>()
                .Include(ap => ap.Encounter.EncounterType)
                .Include(ap => ap.Encounter.Patient.PatientTags.Select(y => y.Tag))
                .Include(ap => ap.User)
                .Include(ap => ap.AppointmentType);

            foreach (BatchBuilderFilterViewModel filter in filters)
            {
                switch (filter.Name)
                {
                    case BatchBuilderFilterNames.PatientLastName:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && appointments != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<UserAppointment, bool>> predicate = textFilter.Text.Split(';').Select(f =>
                                                                                                                          appointments.CreatePredicate(ap => ap.Encounter.Patient.LastName.ToLower().StartsWith(f.ToLower())))
                                        .Aggregate((x, y) => x.Or(y));
                                    appointments = appointments.Where(predicate);
                                }
                                else
                                    appointments =
                                        appointments.Where(
                                            ap =>
                                            ap.Encounter.Patient.LastName.StartsWith(textFilter.Text.Trim().ToUpper()));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.PatientFirstName:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && appointments != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<UserAppointment, bool>> predicate = textFilter.Text.Split(';').Select(f =>
                                                                                                                          appointments.CreatePredicate(ap => ap.Encounter.Patient.FirstName.ToLower().StartsWith(f.ToLower())))
                                        .Aggregate((x, y) => x.Or(y));
                                    appointments = appointments.Where(predicate);
                                }
                                else
                                    appointments =
                                        appointments.Where(
                                            a =>
                                            a.Encounter.Patient.FirstName.StartsWith(textFilter.Text.Trim().ToUpper()));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.PatientId:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && appointments != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<UserAppointment, bool>> predicate = textFilter.Text.Split(';').
                                        Select(f => f.IsNotNullOrEmpty() && f.Trim().IsNumeric() ? appointments.CreatePredicate(ap => ap.Encounter.PatientId.Equals(Convert.ToInt32(f.Trim()))) : appointments.CreatePredicate(ap => ap.Encounter.PatientId.Equals(0)))
                                        .Aggregate((x, y) => x.Or(y));
                                    appointments = appointments.Where(predicate);
                                }
                                else if (textFilter.Text.IsNumeric())
                                    appointments =
                                        appointments.Where(
                                            a => a.Encounter.Patient.Id == Convert.ToInt32(textFilter.Text));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.PatientType:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && appointments != null)
                                appointments = appointments.Where(a =>
                                                                  a.Encounter.Patient.PatientTags.Any(
                                                                      p =>
                                                                      dropDownFilter.SelectedChoices.Contains(p.Tag.DisplayName)));
                        }
                        break;
                    case BatchBuilderFilterNames.PatientAge:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && appointments != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<UserAppointment, bool>> predicate = textFilter.Text.Split(';').
                                        Select(f => f.IsNotNullOrEmpty() && f.Trim().IsNumeric() ?
                                                                                                     appointments.CreatePredicate(ap => ap.Encounter.Patient.DateOfBirth != null && PatientQueries.GetAge(ap.Encounter.Patient).Equals(Convert.ToInt32(f.Trim()))) : appointments.CreatePredicate(ap => ap.Encounter.Patient.DateOfBirth != null && PatientQueries.GetAge(ap.Encounter.Patient).Equals(0)))
                                        .Aggregate((x, y) => x.Or(y));
                                    appointments = appointments.Where(predicate);

                                    IEnumerable<int> textFilters =
                                        textFilter.Text.Split(Convert.ToChar(";")).Select(
                                            t =>
                                            t.IsNotNullOrEmpty() && t.Trim().IsNumeric() ? Convert.ToInt32(t.Trim()) : 0)
                                            .
                                            ToArray();
                                    appointments =
                                        appointments.Where(
                                            ap =>
                                            ap.Encounter.Patient.DateOfBirth != null &&
                                            textFilters.Contains(PatientQueries.GetAge(ap.Encounter.Patient)));
                                }
                                else if (textFilter.Text.IsNumeric())
                                {
                                    int age = Convert.ToInt32(textFilter.Text);
                                    appointments =
                                        appointments.Where(
                                            ap =>
                                            ap.Encounter.Patient.DateOfBirth != null &&
                                            PatientQueries.GetAge(ap.Encounter.Patient) == age);
                                }
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.PatientDob:
                        {
                            var dateRangeFilter = (BatchBuilderDateRangeFilterViewModel) filter;
                            if (dateRangeFilter.DateRanges != null)
                            {
                                Expression<Func<UserAppointment, bool>> predicate =
                                    dateRangeFilter.DateRanges.Select(f => (f.Start != null || f.End != null) ?
                                                                                                                  appointments.CreatePredicate(ap => (f.Start != null && f.End != null) ? ap.Encounter.Patient.DateOfBirth >= f.Start && ap.Encounter.Patient.DateOfBirth <= f.End
                                                                                                                                                         : ap.Encounter.Patient.DateOfBirth == f.Start || ap.Encounter.Patient.DateOfBirth == f.End) : null)
                                        .Aggregate((x, y) => x.Or(y));
                                if (appointments != null && predicate != null) appointments = appointments.Where(predicate);
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.AppointmentsDateRange:
                    case BatchBuilderFilterNames.AppointmentsToday:
                    case BatchBuilderFilterNames.AppointmentsTommorrow:
                    case BatchBuilderFilterNames.AppointmentsWeek:
                    case BatchBuilderFilterNames.AppointmentsMonth:
                        {
                            var dateRangeFilter = (BatchBuilderDateRangeFilterViewModel) filter;
                            if (dateRangeFilter.DateRanges != null)
                            {
                                Expression<Func<UserAppointment, bool>> predicate = dateRangeFilter.DateRanges.Select(f => (f.Start != null || f.End != null) ?
                                                                                                                                                                  appointments.CreatePredicate(ap =>
                                                                                                                                                                                               (f.Start != null && f.End != null) ? ap.DateTime >= f.Start && ap.DateTime <= f.End
                                                                                                                                                                                                   : ap.DateTime == f.Start || ap.DateTime == f.End) : null)
                                    .Aggregate((x, y) => x.Or(y));
                                if (appointments != null && predicate != null) appointments = appointments.Where(predicate);
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.AppointmentType:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && appointments != null)
                                appointments =
                                    appointments.Where(
                                        a => dropDownFilter.SelectedChoices.Contains(a.AppointmentType.Name));
                        }
                        break;
                    case BatchBuilderFilterNames.AppointmentStatus:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && appointments != null)
                            {
                                IEnumerable<int> ids =
                                    dropDownFilter.SelectedChoices.Select(
                                        i => (int) i.ToEnumFromDisplayName<EncounterStatus>());
                                appointments =
                                    appointments.Where(
                                        a =>
                                        ids.Contains((int)a.Encounter.EncounterStatus));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.ScheduledDoctor:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && appointments != null)
                                appointments =
                                    appointments.Where(
                                        a => dropDownFilter.SelectedChoices.Contains(a.User.DisplayName));
                        }
                        break;
                    case BatchBuilderFilterNames.ScheduledLocation:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && appointments != null)
                                appointments =
                                    appointments.Where(
                                        a =>
                                        dropDownFilter.SelectedChoices.Contains(a.Encounter.ServiceLocation.Name));
                        }
                        break;

                    case BatchBuilderFilterNames.ReferringDoctor:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && appointments != null)
                                appointments = appointments.Where(a => a.Encounter.Patient.ExternalProviders.Any(
                                    e => dropDownFilter.SelectedChoices.Contains(e.ExternalProvider.DisplayName)));
                        }
                        break;
                    case BatchBuilderFilterNames.Diagnosis:
                        {
                        }
                        break;
                    case BatchBuilderFilterNames.InsurerName:
                        {
                            var textFilter = (BatchBuilderTextFilterViewModel) filter;
                            if (textFilter.Text.IsNotNullOrEmpty() && appointments != null)
                            {
                                if (textFilter.Text.IndexOf(";") > 0)
                                {
                                    Expression<Func<UserAppointment, bool>> predicate = textFilter.Text.Split(';').Select(f =>
                                                                                                                          appointments.CreatePredicate(ap => ap.Encounter.Patient.PatientInsurances.Any(pi =>
                                                                                                                                                                                                        pi.InsurancePolicy != null && pi.InsurancePolicy.Insurer != null && pi.InsurancePolicy.Insurer.Name.ToLower().StartsWith(f.ToLower()))))
                                        .Aggregate((x, y) => x.Or(y));
                                    appointments = appointments.Where(predicate);
                                }
                                else
                                    appointments =
                                        appointments.Where(
                                            a =>
                                            a.Encounter.Patient.PatientInsurances.Any(
                                                pi =>
                                                pi.InsurancePolicy != null && pi.InsurancePolicy.Insurer != null &&
                                                pi.InsurancePolicy.Insurer.Name.StartsWith(
                                                    textFilter.Text.Trim().ToUpper())));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.InsuranceType:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && appointments != null)
                            {
                                IEnumerable<int> ids =
                                    dropDownFilter.SelectedChoices.Select(
                                        i => (int) i.ToEnumFromDisplayName<InsuranceType>());
                                appointments = appointments.Where(a =>
                                                                  a.Encounter.Patient.PatientInsurances.Any(
                                                                      i => ids.
                                                                               Contains(
                                                                                   (int) i.InsuranceType)));
                            }
                        }
                        break;
                    case BatchBuilderFilterNames.InsurerPlanType:
                        {
                            var dropDownFilter = (BatchBuilderDropDownFilterViewModel) filter;
                            if (dropDownFilter.SelectedChoices.Count > 0 && appointments != null)
                                appointments = appointments.Where(a =>
                                                                  a.Encounter.Patient.PatientInsurances.Any(
                                                                      i =>
                                                                      dropDownFilter.SelectedChoices.Contains(
                                                                          i.InsurancePolicy.Insurer.InsurerPlanType.
                                                                              Name)));
                        }
                        break;
                    //Eligibility responses filters
                    case BatchBuilderFilterNames.ResponseDateRange:
                    case BatchBuilderFilterNames.ResponsesYesterday:
                    case BatchBuilderFilterNames.ResponsesToday:
                    case BatchBuilderFilterNames.ResponsesLastWeek:
                        {
                            //var dateRangeFilter = (BatchBuilderDateRangeFilterViewModel)filter;
                            //if (dateRangeFilter.DateRanges != null)
                            //{
                            //Expression<Func<UserAppointment, bool>> predicate = dateRangeFilter.DateRanges.Select(f => (f.Start != null || f.End != null) ?
                            //                                                                                                                          appointments.CreatePredicate(
                            //                                                                                                                          ap =>
                            //                                                                                                                                                   ap.Encounter.Patient.X12EligibilityPatient.Any(
                            //                                                                                                                                                       x =>
                            //                                                                                                                                                       x.PatientDateOfBirth != null && ((f.Start != null && f.End != null) ? ap.Encounter.Patient.DateOfBirth >= f.Start && ap.Encounter.Patient.DateOfBirth <= f.End
                            //                                                                                                                                                                                            : ap.Encounter.Patient.DateOfBirth == f.Start || ap.Encounter.Patient.DateOfBirth == f.End))) : null)
                            //    .Aggregate((x, y) => x.Or(y));
                            //if (appointments != null && predicate != null) appointments = appointments.Where(predicate);
                            //}
                        }
                        break;
                }
            }
            if (appointments != null)
            {
                UserAppointment[] results = appointments.ToArray();

                PracticeRepository.AsQueryableFactory().Load(results,
                                                              ap => ap.Encounter
                                                              ,
                                                              ap =>
                                                              ap.Encounter.Patient.PatientInsurances.Select(
                                                                  pi => pi.InsurancePolicy.Insurer.InsurerPlanType)
                                                              ,
                                                              ap =>
                                                              ap.Encounter.Patient.ExternalProviders.Select(
                                                                  ep => ep.ExternalProvider)
                                                              , ap => ap.Encounter.ServiceLocation);

                BatchBuilderAppointmentResultViewModel[] viewModel =
                    results.Select(app => _batchBuilderAppointmentResultViewModelMapper.Map(app)).ToArray();
                return viewModel;
            }
            return null;
        }
    }


    /// <summary>
    ///   Maps a Appointments to a BatchBuilderResultViewModel.
    /// </summary>
    internal class BatchBuilderAppointmentResultViewModelMap : IMap<UserAppointment, BatchBuilderAppointmentResultViewModel>
    {
        private readonly IMapMember<UserAppointment, BatchBuilderAppointmentResultViewModel>[] _members;

        public BatchBuilderAppointmentResultViewModelMap()
        {
            _members = this
                .CreateMembers(source =>
                               new BatchBuilderAppointmentResultViewModel
                                   {
                                       PatientId = source.Encounter.PatientId,
                                       LastName = source.Encounter.Patient.LastName,
                                       FirstName = source.Encounter.Patient.FirstName,
                                       DateOfBirth = source.Encounter.Patient.DateOfBirth,
                                       AppointmentDate = source.DateTime,
                                       AppointmentType = source.AppointmentType.Name,
                                       Status = source.Encounter.EncounterStatus.GetDisplayName(),
                                       Doctor = source.User.DisplayName,
                                       Location = source.Encounter.ServiceLocation.ShortName,
                                       Insurance = source.Encounter.Patient.PatientInsurances.Select(
                                           p =>
                                           p.InsurancePolicy != null && p.InsurancePolicy.Insurer != null
                                               ? p.InsurancePolicy.Insurer.Name
                                               : string.Empty).FirstOrDefault(),
                                       InsuranceType = source.Encounter.Patient.PatientInsurances.Select(
                                           p =>
                                           p.InsurancePolicy != null && p.InsurancePolicy.Insurer != null &&
                                           p.InsurancePolicy.Insurer.InsurerPlanType != null
                                               ? p.InsurancePolicy.Insurer.InsurerPlanType.Name
                                               : string.Empty).
                                           FirstOrDefault(),
                                       ReferringDoctor =
                                           source.Encounter.Patient.ExternalProviders.Select(
                                               e =>
                                               e.ExternalProvider != null
                                                   ? e.ExternalProvider.DisplayName
                                                   : string.Empty).FirstOrDefault(),
                                       AppointmentId = source.Id,
                                       //Eligibility columns
                                       SubscriberEntityIdCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberEntityIdCode).FirstOrDefault(),
                                       SubscriberEntityTypeQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberEntityTypeQualifier).FirstOrDefault(),
                                       SubscriberLastName = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberLastName).FirstOrDefault(),
                                       SubscriberFirstName = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberFirstName).FirstOrDefault(),
                                       SubscriberIdCodeQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberIdCodeQualifier).FirstOrDefault(),
                                       SubscriberIdCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberIdCode).FirstOrDefault(),
                                       SubscriberReferenceIdQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberReferenceIdQualifier).FirstOrDefault(),
                                       SubscriberReferenceId = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberReferenceId).FirstOrDefault(),
                                       SubscriberTraceReferenceIdentifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberTraceReferenceIdentifier).FirstOrDefault(),
                                       SubscriberTraceOriginatingCompanyId = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.SubscriberTraceOriginatingCompanyId).FirstOrDefault(),

                                       PatientEntityIdCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientEntityIdCode).FirstOrDefault(),
                                       PatientTypeQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientTypeQualifier).FirstOrDefault(),
                                       PatientLastName = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientLastName).FirstOrDefault(),
                                       PatientFirstName = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientFirstName).FirstOrDefault(),
                                       PatientIdCodeQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientIdCodeQualifier).FirstOrDefault(),
                                       PatientIdCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientIdCode).FirstOrDefault(),
                                       PatientReferenceIdQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientReferenceIdQualifier).FirstOrDefault(),
                                       PatientReferenceId = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientReferenceId).FirstOrDefault(),
                                       PatientDateOfBirthFormatQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientDateOfBirthFormatQualifier).FirstOrDefault(),
                                       PatientDateOfBirth = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientDateOfBirth).FirstOrDefault(),

                                       PatientGender = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientGender).FirstOrDefault(),
                                       PatientPolicyHolderRelationship = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.PatientPolicyholderRelationship).FirstOrDefault(),
                                       BenefitInfoCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.BenefitInfoCode).FirstOrDefault(),
                                       BenefitCoverageLevelCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.BenefitCoverageLevelCode).FirstOrDefault(),
                                       BenefitInsuranceTypeCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.BenefitInsuranceTypeCode).FirstOrDefault(),
                                       BenefitPlanCoverageDescription = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.BenefitPlanCoverageDescription).FirstOrDefault(),
                                       BenefitMonetaryAmount = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.BenefitMonetaryAmount).FirstOrDefault(),
                                       BenefitCoInsurancePercent = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.BenefitCoInsurancePercent).FirstOrDefault(),
                                       BenefitQuantityQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.BenefitQuantityQualifier).FirstOrDefault(),
                                       BenefitQuantity = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.BenefitQuantity).FirstOrDefault(),

                                       InfoReceiverEntityIdCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverEntityIdCode).FirstOrDefault(),
                                       InfoReceiverEntityTypeQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverEntityTypeQualifier).FirstOrDefault(),
                                       InfoReceiverName = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverName).FirstOrDefault(),
                                       InfoReceiverIdCodeQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverIdCodeQualifier).FirstOrDefault(),
                                       InfoReceiverIdCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverIdCode).FirstOrDefault(),
                                       InfoReceiverAddressLine1 = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverAddressLine1).FirstOrDefault(),
                                       InfoReceiverAddressLine2 = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverAddressLine2).FirstOrDefault(),
                                       InfoReceiverCity = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverCity).FirstOrDefault(),
                                       InfoReceiverState = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverState).FirstOrDefault(),
                                       InfoReceiverZipCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverZipCode).FirstOrDefault(),

                                       InfoReceiverCountryCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverCountryCode).FirstOrDefault(),
                                       InfoReceiverProviderCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverProviderCode).FirstOrDefault(),
                                       InfoReceiverReferenceIdQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverReferenceIdQualifier).FirstOrDefault(),
                                       InfoReceiverReferenceId = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoReceiverReferenceId).FirstOrDefault(),
                                       InfoSourceEntityIdCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoSourceEntityIdCode).FirstOrDefault(),
                                       InfoSourceEntityTypeQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoSourceEntityTypeQualifier).FirstOrDefault(),
                                       InfoSourceName = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoSourceName).FirstOrDefault(),
                                       InfoSourceIdCodeQualifier = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoSourceIdCodeQualifier).FirstOrDefault(),
                                       InfoSourceIdCode = source.Encounter.Patient.X12EligibilityPatient.Select(x => x.InfoSourceIdCode).FirstOrDefault()
                                   })
                .ToArray();
        }

        #region IMap<UserAppointment,BatchBuilderAppointmentResultViewModel> Members

        public IEnumerable<IMapMember<UserAppointment, BatchBuilderAppointmentResultViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }

    /// <summary>
    ///   Maps a Patients to a BatchBuilderResultViewModel.
    /// </summary>
    internal class BatchBuilderPatientResultViewModelMap : IMap<Patient, BatchBuilderPatientResultViewModel>
    {
        private readonly IMapMember<Patient, BatchBuilderPatientResultViewModel>[] _members;

        public BatchBuilderPatientResultViewModelMap()
        {
            _members = this
                .CreateMembers(source =>
                               new BatchBuilderPatientResultViewModel
                                   {
                                       PatientId = source.Id,
                                       LastName = source.LastName,
                                       FirstName = source.FirstName,
                                       DateOfBirth = source.DateOfBirth,
                                       PatientType = source.PatientTags != null
                                                         ? source.PatientTags.Select(p => p.Tag.DisplayName ?? "").FirstOrDefault()
                                                         : "",
                                       Age =
                                           source.DateOfBirth != null
                                               ? ((DateTime.Now.ToClientTime().Subtract(source.DateOfBirth.Value).Days/365).ToString())
                                               : string.Empty,
                                       Insurance = source.PatientInsurances.Select(
                                           p =>
                                           p.InsurancePolicy != null && p.InsurancePolicy.Insurer != null
                                               ? p.InsurancePolicy.Insurer.Name
                                               : string.Empty).FirstOrDefault(),
                                       InsuranceType = source.PatientInsurances.Select(
                                           p => p.InsuranceType.ToString()
                                           ).FirstOrDefault(),
                                       PlanType = source.PatientInsurances.Select(p =>
                                                                                  p.InsurancePolicy != null && p.InsurancePolicy.Insurer != null &&
                                                                                  p.InsurancePolicy.Insurer.InsurerPlanType != null
                                                                                      ? p.InsurancePolicy.Insurer.InsurerPlanType.Name
                                                                                      : string.Empty).FirstOrDefault(),
                                       ReferringDoctor =
                                           source.ExternalProviders.Select(
                                               e =>
                                               e.ExternalProvider != null
                                                   ? e.ExternalProvider.DisplayName
                                                   : string.Empty).FirstOrDefault(),
                                       LastAppointment = source.LastAppointmentDateTime,
                                       PolicyStartDate = source.PatientInsurances.Select(
                                           p => p.InsurancePolicy.StartDateTime.Date
                                           ).FirstOrDefault(),
                                       PolicyEndDate = source.PatientInsurances.Select(
                                           p => p.EndDateTime
                                           ).FirstOrDefault(),
                                       //Eligibility columns
                                       SubscriberEntityIdCode = source.X12EligibilityPatient.Select(x => x.SubscriberEntityIdCode).FirstOrDefault(),
                                       SubscriberEntityTypeQualifier = source.X12EligibilityPatient.Select(x => x.SubscriberEntityTypeQualifier).FirstOrDefault(),
                                       SubscriberLastName = source.X12EligibilityPatient.Select(x => x.SubscriberLastName).FirstOrDefault(),
                                       SubscriberFirstName = source.X12EligibilityPatient.Select(x => x.SubscriberFirstName).FirstOrDefault(),
                                       SubscriberIdCodeQualifier = source.X12EligibilityPatient.Select(x => x.SubscriberIdCodeQualifier).FirstOrDefault(),
                                       SubscriberIdCode = source.X12EligibilityPatient.Select(x => x.SubscriberIdCode).FirstOrDefault(),
                                       SubscriberReferenceIdQualifier = source.X12EligibilityPatient.Select(x => x.SubscriberReferenceIdQualifier).FirstOrDefault(),
                                       SubscriberReferenceId = source.X12EligibilityPatient.Select(x => x.SubscriberReferenceId).FirstOrDefault(),
                                       SubscriberTraceReferenceIdentifier = source.X12EligibilityPatient.Select(x => x.SubscriberTraceReferenceIdentifier).FirstOrDefault(),
                                       SubscriberTraceOriginatingCompanyId = source.X12EligibilityPatient.Select(x => x.SubscriberTraceOriginatingCompanyId).FirstOrDefault(),

                                       PatientEntityIdCode = source.X12EligibilityPatient.Select(x => x.PatientEntityIdCode).FirstOrDefault(),
                                       PatientTypeQualifier = source.X12EligibilityPatient.Select(x => x.PatientTypeQualifier).FirstOrDefault(),
                                       PatientLastName = source.X12EligibilityPatient.Select(x => x.PatientLastName).FirstOrDefault(),
                                       PatientFirstName = source.X12EligibilityPatient.Select(x => x.PatientFirstName).FirstOrDefault(),
                                       PatientIdCodeQualifier = source.X12EligibilityPatient.Select(x => x.PatientIdCodeQualifier).FirstOrDefault(),
                                       PatientIdCode = source.X12EligibilityPatient.Select(x => x.PatientIdCode).FirstOrDefault(),
                                       PatientReferenceIdQualifier = source.X12EligibilityPatient.Select(x => x.PatientReferenceIdQualifier).FirstOrDefault(),
                                       PatientReferenceId = source.X12EligibilityPatient.Select(x => x.PatientReferenceId).FirstOrDefault(),
                                       PatientDateOfBirthFormatQualifier = source.X12EligibilityPatient.Select(x => x.PatientDateOfBirthFormatQualifier).FirstOrDefault(),
                                       PatientDateOfBirth = source.X12EligibilityPatient.Select(x => x.PatientDateOfBirth).FirstOrDefault(),

                                       PatientGender = source.X12EligibilityPatient.Select(x => x.PatientGender).FirstOrDefault(),
                                       PatientPolicyHolderRelationship = source.X12EligibilityPatient.Select(x => x.PatientPolicyholderRelationship).FirstOrDefault(),
                                       BenefitInfoCode = source.X12EligibilityPatient.Select(x => x.BenefitInfoCode).FirstOrDefault(),
                                       BenefitCoverageLevelCode = source.X12EligibilityPatient.Select(x => x.BenefitCoverageLevelCode).FirstOrDefault(),
                                       BenefitInsuranceTypeCode = source.X12EligibilityPatient.Select(x => x.BenefitInsuranceTypeCode).FirstOrDefault(),
                                       BenefitPlanCoverageDescription = source.X12EligibilityPatient.Select(x => x.BenefitPlanCoverageDescription).FirstOrDefault(),
                                       BenefitMonetaryAmount = source.X12EligibilityPatient.Select(x => x.BenefitMonetaryAmount).FirstOrDefault(),
                                       BenefitCoInsurancePercent = source.X12EligibilityPatient.Select(x => x.BenefitCoInsurancePercent).FirstOrDefault(),
                                       BenefitQuantityQualifier = source.X12EligibilityPatient.Select(x => x.BenefitQuantityQualifier).FirstOrDefault(),
                                       BenefitQuantity = source.X12EligibilityPatient.Select(x => x.BenefitQuantity).FirstOrDefault(),

                                       InfoReceiverEntityIdCode = source.X12EligibilityPatient.Select(x => x.InfoReceiverEntityIdCode).FirstOrDefault(),
                                       InfoReceiverEntityTypeQualifier = source.X12EligibilityPatient.Select(x => x.InfoReceiverEntityTypeQualifier).FirstOrDefault(),
                                       InfoReceiverName = source.X12EligibilityPatient.Select(x => x.InfoReceiverName).FirstOrDefault(),
                                       InfoReceiverIdCodeQualifier = source.X12EligibilityPatient.Select(x => x.InfoReceiverIdCodeQualifier).FirstOrDefault(),
                                       InfoReceiverIdCode = source.X12EligibilityPatient.Select(x => x.InfoReceiverIdCode).FirstOrDefault(),
                                       InfoReceiverAddressLine1 = source.X12EligibilityPatient.Select(x => x.InfoReceiverAddressLine1).FirstOrDefault(),
                                       InfoReceiverAddressLine2 = source.X12EligibilityPatient.Select(x => x.InfoReceiverAddressLine2).FirstOrDefault(),
                                       InfoReceiverCity = source.X12EligibilityPatient.Select(x => x.InfoReceiverCity).FirstOrDefault(),
                                       InfoReceiverState = source.X12EligibilityPatient.Select(x => x.InfoReceiverState).FirstOrDefault(),
                                       InfoReceiverZipCode = source.X12EligibilityPatient.Select(x => x.InfoReceiverZipCode).FirstOrDefault(),

                                       InfoReceiverCountryCode = source.X12EligibilityPatient.Select(x => x.InfoReceiverCountryCode).FirstOrDefault(),
                                       InfoReceiverProviderCode = source.X12EligibilityPatient.Select(x => x.InfoReceiverProviderCode).FirstOrDefault(),
                                       InfoReceiverReferenceIdQualifier = source.X12EligibilityPatient.Select(x => x.InfoReceiverReferenceIdQualifier).FirstOrDefault(),
                                       InfoReceiverReferenceId = source.X12EligibilityPatient.Select(x => x.InfoReceiverReferenceId).FirstOrDefault(),
                                       InfoSourceEntityIdCode = source.X12EligibilityPatient.Select(x => x.InfoSourceEntityIdCode).FirstOrDefault(),
                                       InfoSourceEntityTypeQualifier = source.X12EligibilityPatient.Select(x => x.InfoSourceEntityTypeQualifier).FirstOrDefault(),
                                       InfoSourceName = source.X12EligibilityPatient.Select(x => x.InfoSourceName).FirstOrDefault(),
                                       InfoSourceIdCodeQualifier = source.X12EligibilityPatient.Select(x => x.InfoSourceIdCodeQualifier).FirstOrDefault(),
                                       InfoSourceIdCode = source.X12EligibilityPatient.Select(x => x.InfoSourceIdCode).FirstOrDefault()
                                   })
                .ToArray();
        }

        #region IMap<Patient,BatchBuilderPatientResultViewModel> Members

        public IEnumerable<IMapMember<Patient, BatchBuilderPatientResultViewModel>> Members
        {
            get { return _members; }
        }

        #endregion
    }
}