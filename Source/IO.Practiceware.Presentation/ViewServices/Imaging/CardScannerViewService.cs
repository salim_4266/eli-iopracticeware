﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewServices.Imaging;
using IO.Practiceware.Services.Documents;
using IO.Practiceware.Services.Imaging;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

[assembly: Component(typeof(CardScannerViewService), typeof(ICardScannerViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Imaging
{
    public class CardScannerProcessingReponse
    {
        public int PatientId { get; set; }
        public string ValidationMessage { get; set; }
    }

    public interface ICardScannerViewService
    {
        /// <summary>
        /// Validates scanned data values against Patient Validation rules.  If a Patient object is passed in, the values of the patient combined with the scanned values will be used for validation
        /// </summary>
        /// <param name="imageData"> The image data. </param>
        /// <param name="cardType"> Type of the card. </param>
        /// <param name="scannedDataValues"> The values. </param>
        /// <param name="patientIdToValidateAgainst">Optional. Pass in an existing Patient which is used as the basis for validation. The patient will have it's existing data merged with the scanned values before validation occurs</param>
        CardScannerProcessingReponse ValidateProcessedData(byte[] imageData, CardType cardType, IEnumerable<CardScannerDataModel> scannedDataValues, int? patientIdToValidateAgainst = null);

        ///// <summary>
        /////   Saves the processed data to a patient.
        ///// </summary>
        ///// <param name="patientId"> The patient id. </param>
        ///// <param name="imageData"> The image data. </param>
        ///// <param name="cardType"> Type of the card. </param>
        ///// <param name="scannedDataValues"> The values. </param>
        CardScannerProcessingReponse SaveProcessedData(int patientId, byte[] imageData, CardType cardType, IEnumerable<CardScannerDataModel> scannedDataValues);
    }

    internal class CardScannerViewService : BaseViewService, ICardScannerViewService
    {
        private readonly IPatientImageImporterService _patientImageImporterService;        
        private readonly IImageService _imageService;
        
        public CardScannerViewService(IPatientImageImporterService patientImageImporterService,  IImageService imageService)
        {
            _patientImageImporterService = patientImageImporterService;
            
            _imageService = imageService;
        }

        #region ICardScannerViewService Members

        public CardScannerProcessingReponse ValidateProcessedData(byte[] imageData, CardType cardType, IEnumerable<CardScannerDataModel> scannedDataValues, int? patientIdToValidateAgainst)
        {
            scannedDataValues = scannedDataValues.ToArray();

            Patient patientToValidate = null;
            if (patientIdToValidateAgainst.HasValue)
            {
                patientToValidate = (PracticeRepository.Patients
                                                       .Include(p => p.PatientAddresses.Select(a => a.StateOrProvince), p => p.PatientEmailAddresses, p => p.PatientPhoneNumbers)
                                                       .WithId(patientIdToValidateAgainst));
            }
            
            // we are mapping 
            var patient = patientToValidate ?? new Patient();

            foreach (var value in scannedDataValues)
            {
                ApplyCardScannerDataToPatient(patient, value.Key, value.Value);
            }
            
            if (imageData != null)
            {
                _patientImageImporterService.Import(new[]
                                                        {
                                                            new PatientDocument
                                                                {                                                                    
                                                                    FileType = cardType.GetDisplayName(),
                                                                    PatientId = patient.Id,
                                                                    FileExtension = ".jpg",
                                                                    DateTime = DateTime.Now.ToClientTime(),
                                                                    Pages = {new PatientDocumentBinaryPage(imageData)}
                                                                }
                                                        }, true, false, onError: e => { throw new InvalidOperationException(e.Message); });
            }

            var validationMessage = ValidatePatient(patient);          

            return new CardScannerProcessingReponse { PatientId = patient.Id, ValidationMessage = validationMessage };
        }

        private string ValidatePatient(Patient patient)
        {
            var validationMessage = new object[] { patient }.Concat(patient.PatientAddresses)
                                                          .SelectMany(o => o.Validate()).Select(i => i.Value)
                                                          .Join(Environment.NewLine);

            return validationMessage;
        }

        private static void IgnoreInvalidPatientAddresses(Patient patient)
        {
            foreach (var address in patient.PatientAddresses.ToList())
            {
                if (!address.IsValid()) patient.PatientAddresses.Remove(address);
            }
        }

        public CardScannerProcessingReponse SaveProcessedData(int patientId, byte[] imageData, CardType cardType, IEnumerable<CardScannerDataModel> scannedDataValues)
        {           
            var patient = PracticeRepository.Patients.Include(p => p.PatientAddresses.Select(a => a.StateOrProvince), p => p.PatientEmailAddresses, p => p.PatientPhoneNumbers).WithId(patientId) ?? new Patient();          

            foreach (var value in scannedDataValues)
            {
                ApplyCardScannerDataToPatient(patient, value.Key, value.Value);
            }

            IgnoreInvalidPatientAddresses(patient);
            var validationMessage = ValidatePatient(patient);
            if (validationMessage.IsNotNullOrEmpty()) return new CardScannerProcessingReponse {ValidationMessage = validationMessage, PatientId = patient.Id};

            PracticeRepository.Save(patient);

            if (imageData != null)
            {
                _patientImageImporterService.Import(new[]
                                                        {
                                                            new PatientDocument
                                                                {                                                                    
                                                                    FileType = cardType.GetDisplayName(),
                                                                    PatientId = patient.Id,
                                                                    FileExtension = ".jpg",
                                                                    DateTime = DateTime.Now.ToClientTime(),
                                                                    Pages = {new PatientDocumentBinaryPage(imageData)}
                                                                }
                                                        }, true, false, onError: e => { throw new InvalidOperationException(e.Message); });
            }

            var photo = scannedDataValues.FirstOrDefault(v => v.Key == CardScannerDataField.Photo);
            if (photo != null) _imageService.SavePatientPhoto(patient.Id, (byte[])photo.Value);

            return new CardScannerProcessingReponse { PatientId = patient.Id, ValidationMessage = validationMessage };
        }

        /// <summary>
        /// Applies the specified card scanner data values to the patient. It will overwrite any existing values
        /// the same as an existing one
        /// </summary>
        /// <param name="patient">The patient.</param>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        public void ApplyCardScannerDataToPatient(Patient patient, CardScannerDataField field, object value)
        {
            switch (field)
            {
                case CardScannerDataField.Prefix:
                    patient.Prefix = (string)value;
                    break;
                case CardScannerDataField.Email:
                    var existingEmail = patient.PatientEmailAddresses.FirstOrDefault(e => e.Value.Equals((string)value, StringComparison.OrdinalIgnoreCase));
                    if (existingEmail == null) patient.PatientEmailAddresses.Add(new PatientEmailAddress { Value = (string)value });
                    break;
                case CardScannerDataField.Telephone:
                    patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home, NotFoundBehavior.Add).SetValuesFromString((string)value);
                    break;
                case CardScannerDataField.FirstName:
                    patient.FirstName = (string)value;
                    break;
                case CardScannerDataField.LastName:
                    patient.LastName = (string)value;
                    break;
                case CardScannerDataField.MiddleName:
                    patient.MiddleName = (string)value;
                    break;
                case CardScannerDataField.Suffix:
                    patient.Suffix = (string)value;
                    break;
                case CardScannerDataField.Sex:
                    patient.Gender = ((string)value).ToEnumFromDisplayNameOrNull<Gender>() ?? patient.Gender;
                    break;
                case CardScannerDataField.SocialSecurityNumber:
                    patient.SocialSecurityNumber = (string)value;
                    break;
                case CardScannerDataField.DateOfBirth:
                    patient.DateOfBirth = ((string)value).ToDateTime("MM-dd-yy") ?? patient.DateOfBirth;
                    break;
                case CardScannerDataField.AddressLine1:
                    patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.Add).Line1 = (string)value;
                    break;
                case CardScannerDataField.AddressLine2:
                    patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.Add).Line2 = (string)value;
                    break;
                case CardScannerDataField.City:
                    patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home, NotFoundBehavior.Add).City = (string)value;
                    break;
                case CardScannerDataField.State:
                    var stateOrProvince = PracticeRepository.StateOrProvinces.FirstOrDefault(s => (s.Abbreviation.ToLower() == ((string) value).ToLower()) || (s.Name.ToLower() == ((string) value).ToLower()));
                    if(stateOrProvince != null)
                        patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home).StateOrProvince = stateOrProvince;
                    break;
                case CardScannerDataField.PostalCode:
                    patient.PatientAddresses.WithAddressType(PatientAddressTypeId.Home).PostalCode = (string)value;
                    break;
            }
        }

        #endregion
    }
 
}