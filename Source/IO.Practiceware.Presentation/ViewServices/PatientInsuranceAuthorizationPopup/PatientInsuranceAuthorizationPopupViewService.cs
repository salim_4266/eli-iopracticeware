﻿using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInsuranceAuthorizationPopup;
using IO.Practiceware.Presentation.ViewServices.PatientInsuranceAuthorizationPopup;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System.Linq;

[assembly: Component(typeof(PatientInsuranceAuthorizationPopupViewService), typeof(IPatientInsuranceAuthorizationPopupViewService))]

namespace IO.Practiceware.Presentation.ViewServices.PatientInsuranceAuthorizationPopup
{
    public interface IPatientInsuranceAuthorizationPopupViewService
    {
        /// <summary>
        /// Get an authorization from the system
        /// </summary>
        /// <param name="authorizationId">The authorization id</param>
        /// <returns>Authorization</returns>
        PatientInsuranceAuthorizationViewModel GetAuthorization(int authorizationId);

        /// <summary>
        /// Delete an authorization from the system
        /// </summary>
        /// <param name="authorizationId">The authorization to delete</param>
        void DeleteAuthorization(int authorizationId);

        /// <summary>
        /// Delete all authorizations from the system for a given encounter
        /// </summary>
        /// <param name="encounterId">The encounter whose authorizations are to be deleted</param>
        void DeleteAllAuthorization(int encounterId);

        /// <summary>
        /// Delete any invalid authorizations from the system 
        /// </summary>
        /// <param name="encounterId">The encounter whose invalid authorizations are to be deleted</param>
        /// <param name="authorizationId">The authorization to keep during deleting</param>
        void DeleteInvalidAuthorizations(int encounterId, int? authorizationId);

        /// <summary>
        /// Save information to existing authorization
        /// </summary>
        /// <param name="authorization">Authorization</param>
        void EditAuthorization(PatientInsuranceAuthorizationViewModel authorization);
    }

    internal class PatientInsuranceAuthorizationPopupViewService : BaseViewService, IPatientInsuranceAuthorizationPopupViewService
    {
        #region IPatientInsuranceAuthorizationPopupViewService

        public PatientInsuranceAuthorizationViewModel GetAuthorization(int authorizationId)
        {
            var fromDatabaseAuthorization = PracticeRepository.PatientInsuranceAuthorizations.Where(pia => pia.Id == authorizationId).Include(pia => pia.Encounter.ServiceLocation).First();
            var location = new ColoredViewModel { Id = fromDatabaseAuthorization.Encounter.ServiceLocation.Id, Name = fromDatabaseAuthorization.Encounter.ServiceLocation.Name, Color = fromDatabaseAuthorization.Encounter.ServiceLocation.Color };
            var authorization = new PatientInsuranceAuthorizationViewModel { Id = fromDatabaseAuthorization.Id, Code = fromDatabaseAuthorization.AuthorizationCode, Comments = fromDatabaseAuthorization.Comment, PatientInsuranceId = fromDatabaseAuthorization.PatientInsuranceId, EncounterId = fromDatabaseAuthorization.EncounterId, Location = location };
            authorization.Date = fromDatabaseAuthorization.AuthorizationDateTime.HasValue ? fromDatabaseAuthorization.AuthorizationDateTime : null;
            return authorization;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void DeleteAuthorization(int authorizationId)
        {
            var fromDatabaseAuthorization = PracticeRepository.PatientInsuranceAuthorizations.WithId(authorizationId);
            var invoiceReceivables = PracticeRepository.InvoiceReceivables.Where(x => x.PatientInsuranceAuthorizationId == authorizationId);
            invoiceReceivables.ForEachWithSinglePropertyChangedNotification(x => x.PatientInsuranceAuthorizationId = null);
            PracticeRepository.Delete(fromDatabaseAuthorization);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void DeleteAllAuthorization(int encounterId)
        {
            var fromDatabaseAuthorizations = PracticeRepository.PatientInsuranceAuthorizations.Where(a => a.EncounterId == encounterId);
            fromDatabaseAuthorizations.ForEachWithSinglePropertyChangedNotification(a => PracticeRepository.Delete(a));
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void DeleteInvalidAuthorizations(int encounterId, int? authorizationId)
        {
            var fromDatabaseAuthorizations = PracticeRepository.PatientInsuranceAuthorizations.Where(a => a.EncounterId == encounterId);
            foreach (var authorization in fromDatabaseAuthorizations)
            {
                if (authorizationId.HasValue && authorizationId.Value == authorization.Id) continue;
                PracticeRepository.Delete(authorization);
            }
        }

        public void EditAuthorization(PatientInsuranceAuthorizationViewModel authorization)
        {
            var fromDatabaseAuthorization = PracticeRepository.PatientInsuranceAuthorizations.WithId(authorization.Id);
            fromDatabaseAuthorization.AuthorizationCode = authorization.Code;
            fromDatabaseAuthorization.PatientInsuranceId = authorization.PatientInsuranceId;
            fromDatabaseAuthorization.Comment = authorization.Comments;

            PracticeRepository.Save(fromDatabaseAuthorization);
        }

        #endregion
    }
}