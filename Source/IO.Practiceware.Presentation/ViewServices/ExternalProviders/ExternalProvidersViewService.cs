﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ExternalProviders;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.ExternalProviders;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;

[assembly: Component(typeof(ExternalProvidersViewService), typeof(IExternalProvidersViewService))]

namespace IO.Practiceware.Presentation.ViewServices.ExternalProviders
{
    public interface IExternalProvidersViewService
    {
        ExternalProvidersLoadInformation GetDataLists(ContactTypeId contactTypeId);
        ExternalProvidersLoadInformation GetExternalProviders(String searchText, ContactTypeId contactTypeId);
        ExternalProviderViewModel GetExternalProviderDetails(int id, ContactTypeId contactTypeId);
        List<int> Save(List<ExternalProviderViewModel> externalProviders, List<int> deletedExternalProviderIds, ContactTypeId contactTypeId);
        bool CanDelete(int externalProviderId, ContactTypeId contactTypeId);
    }
    public class ExternalProvidersViewService : BaseViewService, IExternalProvidersViewService
    {
        #region public methods
        public ExternalProvidersViewService(IUnitOfWorkProvider unitOfWorkProvider)
        {
            _unitOfWorkProvider = unitOfWorkProvider;
            InitMappers();
        }

        public virtual ExternalProvidersLoadInformation GetExternalProviders(String searchText, ContactTypeId contactTypeId)
        {
            var externalContactsLoadViewModel = new ExternalProvidersLoadInformation();
            if (contactTypeId == ContactTypeId.ExternalProviders)
            {
                var externalProviders = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().Where(x => x.FirstName.Contains(searchText) || x.LastNameOrEntityName.Contains(searchText)).ToList();
                  PracticeRepository.AsQueryableFactory().Load(externalProviders,
                  x => x.ExternalContactAddresses.Select(sop => new
                  {
                      sop.StateOrProvince.Country,
                      sop.ExternalContactAddressType
                  }),
                  x => x.ExternalProviderClinicalSpecialtyTypes.Select(cst => cst.ClinicalSpecialtyType)
                    );

                  externalContactsLoadViewModel.ExternalProviders = _initialExternalProviderMapper.MapAll(externalProviders).OrderBy(x => x.Id).ToObservableCollection();
            }
            
            else 
            {
                var externalOrganizations = PracticeRepository.ExternalOrganizations.Where(x => x.Name.Contains(searchText) || x.DisplayName.Contains(searchText)).ToList();
                if (contactTypeId == ContactTypeId.Hospitals)
                {
                    externalOrganizations = externalOrganizations.Where(x => x.ExternalOrganizationTypeId == (int)ExternalOrganizationTypeId.Hospital).ToList();
                }
                else if (contactTypeId == ContactTypeId.Labs)
                {
                    externalOrganizations = externalOrganizations.Where(x => x.ExternalOrganizationTypeId == (int)ExternalOrganizationTypeId.DiagnosticTestLaboratory).ToList();
                }
                else if (contactTypeId == ContactTypeId.Vendors)
                {
                    externalOrganizations = externalOrganizations.Where(x => x.ExternalOrganizationTypeId == (int)ExternalOrganizationTypeId.Vendor).ToList();
                }
                else if (contactTypeId == ContactTypeId.Others)
                {
                    externalOrganizations = externalOrganizations.Where(x => x.ExternalOrganizationTypeId == (int)ExternalOrganizationTypeId.Other).ToList();
                }
                PracticeRepository.AsQueryableFactory().Load(externalOrganizations,
                           x => x.ExternalOrganizationAddresses.Select(sop => new
                           {
                               sop.StateOrProvince.Country,
                               sop.ExternalOrganizationAddressType
                           })
                           );

                externalContactsLoadViewModel.ExternalProviders = _initialExternalOrganizationMapper.MapAll(externalOrganizations).OrderBy(x => x.Id).ToObservableCollection();
            }
            return externalContactsLoadViewModel;
        }


        public virtual ExternalProvidersLoadInformation GetDataLists(ContactTypeId contactTypeId)
        {
            var externalContactsLoadViewModel = new ExternalProvidersLoadInformation();

            externalContactsLoadViewModel.DataLists = LoadDataLists(contactTypeId);

            return externalContactsLoadViewModel;
        }

        public virtual ExternalProviderViewModel GetExternalProviderDetails(int id, ContactTypeId contactTypeId)
        {
            if (contactTypeId == ContactTypeId.ExternalProviders)
            {
                var externalProvider = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().SingleOrDefault(x => x.Id == id);

                PracticeRepository.AsQueryableFactory().Load(externalProvider,
                    x => x.ExternalContactPhoneNumbers.Select(pn => pn.ExternalContactPhoneNumberType),
                    x => x.ExternalContactEmailAddresses,
                    x => x.ExternalOrganization,
                    x => x.IdentifierCodes
                );

                var externalProviderViewModel = _externalProviderMapper.Map(externalProvider, true, false);

                return externalProviderViewModel;
            }
            else
            {
                var externalOrganization = PracticeRepository.ExternalOrganizations.SingleOrDefault(x => x.Id == id);
                PracticeRepository.AsQueryableFactory().Load(externalOrganization,
                    x => x.ExternalOrganizationPhoneNumbers.Select(pn => pn.ExternalOrganizationPhoneNumberType),
                    x => x.ExternalOrganizationEmailAddresses,
                    x => x.ExternalOrganizationType
                    );

                var externalProviderViewModel = _externalOrganizationMapper.Map(externalOrganization, true, false);

                return externalProviderViewModel;
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual List<int> Save(List<ExternalProviderViewModel> externalProviders, List<int> deletedExternalProviderIds, ContactTypeId contactTypeId)
        {
            // Fetch existing External Providers
            var existingExternalProvidersIds = externalProviders.Where(ep => ep.Id.HasValue).Select(ep => ep.Id.Value).ToList();
            if (contactTypeId == ContactTypeId.ExternalProviders)
            {
                var allItems = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().Where(ep => existingExternalProvidersIds.Contains(ep.Id) || deletedExternalProviderIds.Contains(ep.Id)).ToList();

                PracticeRepository.AsQueryableFactory().Load(allItems,
                    x => x.ExternalContactAddresses.Select(sop => new
                                                                  {
                                                                      sop.StateOrProvince.Country,
                                                                      sop.ExternalContactAddressType
                                                                  }),
                    x => x.ExternalProviderClinicalSpecialtyTypes.Select(cst => cst.ClinicalSpecialtyType),
                    x => x.ExternalContactPhoneNumbers.Select(pn => pn.ExternalContactPhoneNumberType),
                    x => x.ExternalContactEmailAddresses,
                    x => x.ExternalOrganization,
                    x => x.IdentifierCodes
                    );

                // set defaults for address type and phone number type
                var defaultAddressType = PracticeRepository.ExternalContactAddressTypes.First();
                var defaultPhoneNumberType = PracticeRepository.ExternalContactPhoneNumberTypes.First();

                // Prepare mapping information to set back keys for new entities
                var providerMapping = new Dictionary<ExternalProvider, ExternalProviderViewModel>();


                // if this is one of the items we're deleting
                // remove all linked items that we can safely delete (ie Addresses, phone numbers, identifier codes, emails, and specialty types)
                // this is done here because the code never gets hit in the synchronizer below since the view models for deleted external providers are not passed through 
                allItems.Where(x => deletedExternalProviderIds.Contains(x.Id)).ForEach(x =>
                                                                                       {
                                                                                           // Delete Addresses
                                                                                           var addresses = x.ExternalContactAddresses.ToList();
                                                                                           addresses.ForEach(a =>
                                                                                                             {
                                                                                                                 PracticeRepository.Delete(a);
                                                                                                                 x.ExternalContactAddresses.Remove(a);
                                                                                                             });

                                                                                           // Delete phone numbers
                                                                                           var numbers = x.ExternalContactPhoneNumbers.ToList();
                                                                                           numbers.ForEach(pn =>
                                                                                                           {
                                                                                                               PracticeRepository.Delete(pn);
                                                                                                               x.ExternalContactPhoneNumbers.Remove(pn);
                                                                                                           });

                                                                                           // Delete emails
                                                                                           var emails = x.ExternalContactEmailAddresses.ToList();
                                                                                           emails.ForEach(e =>
                                                                                                          {
                                                                                                              PracticeRepository.Delete(e);
                                                                                                              x.ExternalContactEmailAddresses.Remove(e);
                                                                                                          });

                                                                                           //Delete Identifier Codes
                                                                                           var identifierCodesToDelete = x.IdentifierCodes.ToList();
                                                                                           identifierCodesToDelete.ForEach(ic =>
                                                                                                                           {
                                                                                                                               PracticeRepository.Delete(ic);
                                                                                                                               x.IdentifierCodes.Remove(ic);
                                                                                                                           });

                                                                                           // Delete SpecialtyTypes
                                                                                           var specialtyTypes = x.ExternalProviderClinicalSpecialtyTypes.ToList();
                                                                                           specialtyTypes.ForEach(st =>
                                                                                                                  {
                                                                                                                      PracticeRepository.Delete(st);
                                                                                                                      x.ExternalProviderClinicalSpecialtyTypes.Remove(st);
                                                                                                                  });

                                                                                       });

                // Sync the items, anything not in the External Providers view modelsa that is in allItems will be deleted
                // Everything else that matchesthe condition source.Id != 0 && destination.Id == source.Id will
                // be synced as defined below
                PracticeRepository.SyncDependencyCollection(externalProviders, allItems,
                    (source, destination) => source.Id != 0
                                             && destination.Id == source.Id,
                    (source, destination) =>
                    {
                        // Update To Newly Entered Values
                        destination.LastNameOrEntityName = source.LastNameOrEntityName;
                        destination.IsEntity = source.IsEntity;

                        destination.IsArchived = source.IsArchived;
                        destination.FirstName = !source.IsEntity ? source.FirstName : null;
                        destination.MiddleName = !source.IsEntity ? source.MiddleName : null;
                        destination.NickName = !source.IsEntity ? source.NickName : null;
                        destination.DisplayName = source.DisplayName ?? source.FormattedName;
                        destination.Honorific = !source.IsEntity ? source.Honorific : null;
                        destination.Suffix = !source.IsEntity ? source.Suffix : null;
                        destination.Prefix = !source.IsEntity ? source.Title : null;
                        destination.ExcludeOnClaim = source.ExcludeFromClaim;
                        destination.Comment = source.Comment;

                        // Sync Specialty Type
                        PracticeRepository.SyncDependencyCollection(source.SpecialtyType != null
                                                                    && !String.IsNullOrEmpty(source.SpecialtyType.Name) ?
                            new List<NamedViewModel> {source.SpecialtyType}
                            : new List<NamedViewModel>(),
                            destination.ExternalProviderClinicalSpecialtyTypes,
                            (sourceSpecialty, destinationSpecialty) => sourceSpecialty.Id == destinationSpecialty.ClinicalSpecialtyTypeId,
                            (sourceSpecialty, destinationSpecialty) =>
                            {
                                destinationSpecialty.ClinicalSpecialtyTypeId = sourceSpecialty.Id;
                            });

                        // Sync Addresses
                        PracticeRepository.SyncAddresses(new List<AddressViewModel> {source.Address}, destination.ExternalContactAddresses,
                            address => address.ContactAddressTypeId, (address, i) => address.ContactAddressTypeId = i,
                            defaultAddressType.Id);

                        // Sync phone numbers
                        PracticeRepository.SyncPhoneNumbers(source.PhoneNumbers, destination.ExternalContactPhoneNumbers,
                            number => number.ExternalContactPhoneNumberTypeId, (number, i) => number.ExternalContactPhoneNumberTypeId = i,
                            defaultPhoneNumberType.Id);


                        // Sync emails
                        PracticeRepository.SyncEmailAddresses(source.EmailAddresses, destination.ExternalContactEmailAddresses,
                            email => email.EmailAddressTypeId, (email, i) => email.EmailAddressTypeId = i
                            );

                        var identifierCodes = new List<IdentifierCode>();
                        // Set Taxonomy
                        if (!String.IsNullOrEmpty(source.Taxonomy))
                        {
                            var taxonomyIdentifierCode = destination.IdentifierCodes.SingleOrDefault(x => x.IdentifierCodeType == IdentifierCodeType.Taxonomy);
                            if (taxonomyIdentifierCode != null)
                            {
                                taxonomyIdentifierCode.Value = source.Taxonomy;
                            }
                            identifierCodes.Add(taxonomyIdentifierCode ?? new IdentifierCode
                                                                          {
                                                                              ExternalProvider = destination,
                                                                              IdentifierCodeType = IdentifierCodeType.Taxonomy,
                                                                              Value = source.Taxonomy
                                                                          });
                        }
                        // Set Npi
                        if (!String.IsNullOrEmpty(source.Npi))
                        {
                            var npiIdentifierCode = destination.IdentifierCodes.SingleOrDefault(x => x.IdentifierCodeType == IdentifierCodeType.Npi);
                            if (npiIdentifierCode != null)
                            {
                                npiIdentifierCode.Value = source.Npi;
                            }
                            identifierCodes.Add(npiIdentifierCode ?? new IdentifierCode
                                                                     {
                                                                         ExternalProvider = destination,
                                                                         IdentifierCodeType = IdentifierCodeType.Npi,
                                                                         Value = source.Npi
                                                                     });
                        }
                        // Sync Identifier Codes (Save Taxonomy & Npi)
                        PracticeRepository.SyncDependencyCollection(identifierCodes, destination.IdentifierCodes,
                            (sourceIdentifierCode, destinationIdentifierCode) => sourceIdentifierCode.IdentifierCodeType == destinationIdentifierCode.IdentifierCodeType,
                            (sourceIdentifierCode, destinationIdentifierCode) =>
                            {
                                destinationIdentifierCode.IdentifierCodeType = sourceIdentifierCode.IdentifierCodeType;
                                destinationIdentifierCode.Value = sourceIdentifierCode.Value;
                            });


                        // Wasn't saving changes to External Provider in the mapping before doing this
                        providerMapping.Add(destination, source);
                    });

                // Save
                allItems.ForEach(PracticeRepository.Save);
                // End unit of work to force all pending changes to be saved (so we can get Ids in next step)
                _unitOfWorkProvider.Current.AcceptChanges();


                // Confirm ids
                foreach (var mapping in providerMapping)
                {
                    mapping.Value.Id = mapping.Key.Id;
                }

            }
            else
            {
                var allItems = PracticeRepository.ExternalOrganizations.Where(ep => existingExternalProvidersIds.Contains(ep.Id) || deletedExternalProviderIds.Contains(ep.Id)).ToList();

                PracticeRepository.AsQueryableFactory().Load(allItems,
                    x => x.ExternalOrganizationAddresses.Select(sop => new
                    {
                        sop.StateOrProvince.Country,
                        sop.ExternalOrganizationAddressType
                    }),
                    x => x.ExternalOrganizationPhoneNumbers.Select(pn => pn.ExternalOrganizationPhoneNumberType),
                    x => x.ExternalOrganizationEmailAddresses
                    );

                // set defaults for address type and phone number type
                var defaultAddressType = PracticeRepository.ExternalOrganizationAddressTypes.First();
                var defaultPhoneNumberType = PracticeRepository.ExternalOrganizationPhoneNumberTypes.First();

                // Prepare mapping information to set back keys for new entities
                var providerMapping = new Dictionary<ExternalOrganization, ExternalProviderViewModel>();


                // if this is one of the items we're deleting
                // remove all linked items that we can safely delete (ie Addresses, phone numbers, identifier codes, emails, and specialty types)
                // this is done here because the code never gets hit in the synchronizer below since the view models for deleted external providers are not passed through 
                allItems.Where(x => deletedExternalProviderIds.Contains(x.Id)).ForEach(x =>
                {
                    // Delete Addresses
                    var addresses = x.ExternalOrganizationAddresses.ToList();
                    addresses.ForEach(a =>
                    {
                        PracticeRepository.Delete(a);
                        x.ExternalOrganizationAddresses.Remove(a);
                    });

                    // Delete phone numbers
                    var numbers = x.ExternalOrganizationPhoneNumbers.ToList();
                    numbers.ForEach(pn =>
                    {
                        PracticeRepository.Delete(pn);
                        x.ExternalOrganizationPhoneNumbers.Remove(pn);
                    });

                    // Delete emails
                    var emails = x.ExternalOrganizationEmailAddresses.ToList();
                    emails.ForEach(e =>
                    {
                        PracticeRepository.Delete(e);
                        x.ExternalOrganizationEmailAddresses.Remove(e);
                    });

                });

                // Sync the items, anything not in the External Organizations view modelsa that is in allItems will be deleted
                // Everything else that matchesthe condition source.Id != 0 && destination.Id == source.Id will
                // be synced as defined below
                PracticeRepository.SyncDependencyCollection(externalProviders, allItems,
                    (source, destination) => source.Id != 0
                                             && destination.Id == source.Id,
                    (source, destination) =>
                    {
                        // Update To Newly Entered Values
                        destination.Name = source.LastNameOrEntityName;
                        destination.IsArchived = source.IsArchived;
                        destination.DisplayName = source.DisplayName ?? source.FormattedName;
                        if ( contactTypeId == ContactTypeId.Hospitals)
                            destination.ExternalOrganizationTypeId = (int) ExternalOrganizationTypeId.Hospital;
                        else if (contactTypeId == ContactTypeId.Labs)
                            destination.ExternalOrganizationTypeId = (int)ExternalOrganizationTypeId.DiagnosticTestLaboratory;
                        else if (contactTypeId == ContactTypeId.Vendors)
                            destination.ExternalOrganizationTypeId = (int)ExternalOrganizationTypeId.Vendor;
                        else
                            destination.ExternalOrganizationTypeId = (int)ExternalOrganizationTypeId.Other;

                        // Sync Addresses
                        PracticeRepository.SyncAddresses(new List<AddressViewModel> { source.Address }, destination.ExternalOrganizationAddresses,
                            address => address.ExternalOrganizationAddressTypeId, (address, i) => address.ExternalOrganizationAddressTypeId = i,
                            defaultAddressType.Id);

                        // Sync phone numbers
                        PracticeRepository.SyncPhoneNumbers(source.PhoneNumbers, destination.ExternalOrganizationPhoneNumbers,
                            number => number.ExternalOrganizationPhoneNumberTypeId, (number, i) => number.ExternalOrganizationPhoneNumberTypeId = i,
                            defaultPhoneNumberType.Id);


                        // Sync emails
                        PracticeRepository.SyncEmailAddresses(source.EmailAddresses, destination.ExternalOrganizationEmailAddresses,
                            email => email.EmailAddressTypeId, (email, i) => email.EmailAddressTypeId = i
                            );

                        // Wasn't saving changes to External Provider in the mapping before doing this
                        providerMapping.Add(destination, source);
                    });

                // Save
                allItems.ForEach(PracticeRepository.Save);
                // End unit of work to force all pending changes to be saved (so we can get Ids in next step)
                _unitOfWorkProvider.Current.AcceptChanges();


                // Confirm ids
                foreach (var mapping in providerMapping)
                {
                    mapping.Value.Id = mapping.Key.Id;
                }
            }
            // Return mapping of external provider Ids for newly inserted records
            return externalProviders
                .Select(p => p.Id.GetValueOrDefault()).ToList();
        }

        public bool CanDelete(int id, ContactTypeId contactTypeId)
        {
            if (contactTypeId == ContactTypeId.ExternalProviders)
            {

                var externalProvider = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().WithId(id);


                PracticeRepository.AsQueryableFactory().Load(externalProvider,
                    // Properties that will cause us to be unable to delete
                    x => x.BillingServices,
                    x => x.CommunicationTransactions,
                    x => x.CopiedEncounterCommunicationWithOtherProviderOrders,
                    x => x.CopiesSentPatientLaboratoryTestResults,
                    x => x.CopiesSentPatientLaboratoryTestResults,
                    x => x.Invoices,
                    x => x.LabMedicalDirectorPatientLaboratoryTestResults,
                    x => x.OrderingPatientLaboratoryTestResults,
                    x => x.PatientExternalProviders,
                    x => x.PatientInsuranceReferrals,
                    x => x.PatientMedicationDetails,
                    x => x.PatientVaccinations,
                    x => x.ReceiverEncounterCommunicationWithOtherProviderOrders
                    );

                // Check if we can delete the external provider (if it has empty lists for the properties that we don't use on the screen)

                if (externalProvider.BillingServices.Any()
                    || externalProvider.CommunicationTransactions.Any()
                    || externalProvider.CopiedEncounterCommunicationWithOtherProviderOrders.Any()
                    || externalProvider.Invoices.Any()
                    || externalProvider.LabMedicalDirectorPatientLaboratoryTestResults.Any()
                    || externalProvider.OrderingPatientLaboratoryTestResults.Any()
                    || externalProvider.PatientExternalProviders.Any()
                    || externalProvider.PatientInsuranceReferrals.Any()
                    || externalProvider.PatientMedicationDetails.Any()
                    || externalProvider.PatientVaccinations.Any()
                    || externalProvider.ReceiverEncounterCommunicationWithOtherProviderOrders.Any()
                    )
                {
                    // return false if we can't
                    return false;
                }
            }
            else
            {
                var externalOrganization = PracticeRepository.ExternalOrganizations.WithId(id);


                PracticeRepository.AsQueryableFactory().Load(externalOrganization,
                    // Properties that will cause us to be unable to delete
                    x => x.PatientLaboratoryTestResults,
                    x => x.EncounterLaboratoryTestOrders,
                    x => x.ExternalContacts
                    );

                // Check if we can delete the external provider (if it has empty lists for the properties that we don't use on the screen)

                if (externalOrganization.PatientLaboratoryTestResults.Any()
                    || externalOrganization.EncounterLaboratoryTestOrders.Any()
                    || externalOrganization.ExternalContacts.Any()
                    )
                {
                    // return false if we can't
                    return false;
                }
            }
            // return true if we we're able to
            return true;
        }

        #endregion

        #region private methods
        private void InitMappers()
        {
            _initialExternalProviderMapper = Mapper.Factory.CreateMapper<ExternalProvider, ExternalProviderViewModel>(ep => new ExternalProviderViewModel
            {
                Id = ep.Id,
                FirstName = ep.FirstName,
                Honorific = ep.Honorific,
                LastNameOrEntityName = ep.LastNameOrEntityName,
                IsEntity = ep.IsEntity,
                Address = ep.ExternalContactAddresses.Select(x => _addressMapper.Map(x, true, false)).FirstOrDefault(x => !x.IsEmpty) ?? new AddressViewModel(),
                SpecialtyType = ep.ExternalProviderClinicalSpecialtyTypes.OrderBy(x => x.OrdinalId)
                                                    .Select(x => new NamedViewModel
                                                    {
                                                        Id = x.ClinicalSpecialtyType.Id, 
                                                        Name = x.ClinicalSpecialtyType.Name
                                                    }).FirstOrDefault() ?? new NamedViewModel(),
                IsArchived = ep.IsArchived
            }, false, false);

            _initialExternalOrganizationMapper = Mapper.Factory.CreateMapper<ExternalOrganization, ExternalProviderViewModel>(eo => new ExternalProviderViewModel
            {
                Id = eo.Id,
                FirstName = eo.DisplayName,
                Honorific = string.Empty,
                LastNameOrEntityName = eo.Name,
                IsEntity = true,
                Address = eo.ExternalOrganizationAddresses.Select(x => _externalOrgAddressMapper.Map(x, true, false)).FirstOrDefault(x => !x.IsEmpty) ?? new AddressViewModel(),
                IsArchived = eo.IsArchived
            }, false, false);

            _externalProviderMapper = Mapper.Factory.CreateMapper<ExternalProvider, ExternalProviderViewModel>(ep => new ExternalProviderViewModel
            {
                Id = ep.Id,
                Title = ep.Prefix,
                DisplayName = ep.DisplayName,
                MiddleName = ep.MiddleName,
                NickName = ep.NickName,
                PhoneNumbers = ep.ExternalContactPhoneNumbers.OrderBy(x => x.OrdinalId).Select(x => _phoneNumberMapper.Map(x, true, false)).ToExtendedObservableCollection(),
                EmailAddresses = ep.ExternalContactEmailAddresses.OrderBy(x => x.OrdinalId).Select(x => _emailAddressMapper.Map(x, true, false)).ToExtendedObservableCollection(),
                Suffix = ep.Suffix,
                Npi = !string.IsNullOrEmpty(ep.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi)) ?
                ep.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Npi)
                : String.Empty,
                Taxonomy = !string.IsNullOrEmpty(ep.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy)) ?
                ep.IdentifierCodes.WithIdentifierCodeType(IdentifierCodeType.Taxonomy)
                : String.Empty,
                ExcludeFromClaim = ep.ExcludeOnClaim,
                Comment = ep.Comment
            });

            _externalOrganizationMapper = Mapper.Factory.CreateMapper<ExternalOrganization, ExternalProviderViewModel>(eo => new ExternalProviderViewModel
            {
                Id = eo.Id,
                Title = String.Empty,
                DisplayName = eo.DisplayName,
                MiddleName = String.Empty,
                NickName = String.Empty,
                PhoneNumbers = eo.ExternalOrganizationPhoneNumbers.OrderBy(x => x.OrdinalId).Select(x => _externalOrgPhoneNumberMapper.Map(x, true, false)).ToExtendedObservableCollection(),
                EmailAddresses = eo.ExternalOrganizationEmailAddresses.OrderBy(x => x.OrdinalId).Select(x => _externalOrgEmailAddressMapper.Map(x, true, false)).ToExtendedObservableCollection(),
                Suffix = String.Empty,
                Npi = String.Empty,
                Taxonomy = String.Empty,
                ExcludeFromClaim = false,
                Comment = String.Empty
            });
            _addressMapper = Mapper.Factory.CreateAddressViewModelMapper<ExternalContactAddress>();

            _phoneNumberMapper = Mapper.Factory.CreatePhoneViewModelMapper<ExternalContactPhoneNumber>();

            _emailAddressTypeMapper = Mapper.Factory.CreateNamedViewModelMapper<EmailAddressType>();

            _emailAddressMapper = Mapper.Factory.CreateEmailAddressViewModelMapper<ExternalContactEmailAddress>();

            _stateOrProvinceMapper = Mapper.Factory.CreateStateOrProvinceViewModelMapper();

            _externalOrgAddressMapper = Mapper.Factory.CreateAddressViewModelMapper<ExternalOrganizationAddress>();

            _externalOrgPhoneNumberMapper = Mapper.Factory.CreatePhoneViewModelMapper<ExternalOrganizationPhoneNumber>();

            _externalOrgEmailAddressMapper = Mapper.Factory.CreateEmailAddressViewModelMapper<ExternalOrganizationEmailAddress>();
        }

        private DataListsViewModel LoadDataLists(ContactTypeId contactTypeId)
        {
            var dataLists = new DataListsViewModel();
            new Action[]
            {
                () => dataLists.AddressTypes = contactTypeId == ContactTypeId.ExternalProviders ? 
                    PracticeRepository.ExternalContactAddressTypes
                    .Select(x => new NamedViewModel
                    {
                        Id = x.Id,
                        Name=x.Name
                    }).ToObservableCollection() : 
                    PracticeRepository.ExternalOrganizationAddressTypes
                     .Select(x => new NamedViewModel
                    {
                        Id = x.Id,
                        Name=x.Name
                    }).ToObservableCollection(),
                () => dataLists.EmailAddressTypes = Enums.GetValues<EmailAddressType>().Select(_emailAddressTypeMapper.Map).ToObservableCollection(),
                () => dataLists.PhoneNumberTypes = contactTypeId == ContactTypeId.ExternalProviders ? 
                    PracticeRepository.ExternalContactPhoneNumberTypes
                    .Select(x => new NamedViewModel
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToObservableCollection() : 
                    PracticeRepository.ExternalOrganizationPhoneNumberTypes
                     .Select(x => new NamedViewModel
                    {
                        Id = x.Id,
                        Name=x.Name
                    }).ToObservableCollection(),
                () => dataLists.StatesOrProvinces = _stateOrProvinceMapper.MapAll(PracticeRepository.StateOrProvinces
                    .Include(x => x.Country))
                    .ToObservableCollection(),
                () => dataLists.NameSuffixes = NamedViewModelExtensions.ToNamedViewModel<PersonNameSuffix>().ToObservableCollection(),
                () => dataLists.SpecialtyTypes = PracticeRepository.ClinicalSpecialtyTypes
                    .Select(x => new NamedViewModel
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToObservableCollection()

            }.ForAllInParallel(a => a());
            return dataLists;
        }
        #endregion

        private IMapper<ExternalProvider, ExternalProviderViewModel> _externalProviderMapper;
        private IMapper<ExternalProvider, ExternalProviderViewModel> _initialExternalProviderMapper;
        private IMapper<ExternalContactEmailAddress, EmailAddressViewModel> _emailAddressMapper;
        private IMapper<EmailAddressType, NamedViewModel> _emailAddressTypeMapper;
        private IMapper<ExternalContactAddress, AddressViewModel> _addressMapper;
        private IMapper<ExternalContactPhoneNumber, PhoneNumberViewModel> _phoneNumberMapper;
        private IMapper<StateOrProvince, StateOrProvinceViewModel> _stateOrProvinceMapper;

        private IMapper<ExternalOrganization, ExternalProviderViewModel> _externalOrganizationMapper;
        private IMapper<ExternalOrganization, ExternalProviderViewModel> _initialExternalOrganizationMapper;
        private IMapper<ExternalOrganizationEmailAddress, EmailAddressViewModel> _externalOrgEmailAddressMapper;
        private IMapper<ExternalOrganizationAddress, AddressViewModel> _externalOrgAddressMapper;
        private IMapper<ExternalOrganizationPhoneNumber, PhoneNumberViewModel> _externalOrgPhoneNumberMapper;
        readonly IUnitOfWorkProvider _unitOfWorkProvider;
    }
}
