﻿using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Integration.X12;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.SubmitTransactions;
using IO.Practiceware.Presentation.ViewServices.SubmitTransactions;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Services.ExternalSystems;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Threading;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using Task = System.Threading.Tasks.Task;

using IO.Practiceware.Data;

[assembly: Component(typeof(SubmitTransactionsViewService), typeof(ISubmitTransactionsViewService))]
[assembly: Component(typeof(TransactionSearchResultsViewModelMap), typeof(IMap<IGrouping<InvoiceReceivable, BillingServiceTransaction>, TransactionSearchResultViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.SubmitTransactions
{
    [SupportsTransactionScopes]
    public interface ISubmitTransactionsViewService
    {
        /// <summary>
        /// Loads the information.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="machineName">Name of the machine.</param>
        /// <returns></returns>
        SubmitTransactionsLoadInformation LoadInformation(int userId, string machineName);

        /// <summary>
        /// Searches the BillingServiceTransaction table for any rows which match the filter criteria
        /// </summary>
        /// <param name="filterSelections"></param>
        /// <returns></returns>
        IEnumerable<TransactionSearchResultViewModel> SearchTransactions(SubmitTransactionsFilterSelectionViewModel filterSelections);

        /// <summary>
        /// Saves the custom user filter
        /// </summary>
        /// <param name="userSettingViewModel"></param>
        /// <returns></returns>
        int? SaveUserSetting(SubmitTransactionsUserSettingViewModel userSettingViewModel);

        /// <summary>
        /// Deletes the user filter
        /// </summary>
        /// <param name="userSettingId"></param>
        void DeleteUserSetting(int userSettingId);

        /// <summary>
        /// Gets the claims directory paths.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="machineName">Name of the machine.</param>
        /// <returns></returns>
        ClaimsMachineSettingViewModel GetClaimsDirectoryPaths(int userId, string machineName);

        /// <summary>
        /// Sets the claims directory paths.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns></returns>
        int? SetClaimsDirectoryPaths(ClaimsMachineSettingViewModel viewModel);

        /// <summary>
        /// Sets each Billing Service Transaction to 'Sent' status if and only if the status is 'Queued'.  All other status' will not be changed.
        /// </summary>
        /// <param name="transactionIds"></param>
        void SetTransactionsToSentStatus(IEnumerable<Guid> transactionIds);

        void SavePaperClaimExternalSystemMessage(string message, IEnumerable<Guid> transactionIds);

        void SaveElectronicClaimExternalSystemMessage(X12MessageViewModel x12Message, IEnumerable<Guid> transactionIds);

        void SavePaperClaimExternalSystemMessageBulk(List<Tuple<string, List<Guid>>> listOfMessages);

        X12MessageViewModel ProduceMessage837(IEnumerable<Guid> transactionIds, int claimFormTypeId, out IList<string> validationResults, out IEnumerable<Guid> excludedTransactionIds);

        /// <summary>
        /// Produce an Html Paper Claim Message
        /// </summary>
        /// <param name="transactionIdsByInvoiceReceivableId">A Dictionary of Transaction Ids grouped by Invoice Receivable Id</param>
        /// <param name="claimFormTypeId"></param>
        /// <returns></returns>
        Dictionary<long, List<string>> ProduceMessagePaperClaimHtml(Dictionary<long, List<Guid>> transactionIdsByInvoiceReceivableId, int claimFormTypeId);

        Tuple<int, string, string> GetClearingHouseInfo(int claimFileReceiverId);

        string GetDocumentName(int claimFormTypeId);
    }

    public class SubmitTransactionsViewService : BaseViewService, ISubmitTransactionsViewService
    {
        private readonly Func<SubmitTransactionsFilterViewModel> _createFilterViewModel;
        private readonly Func<SubmitTransactionsFilterSelectionViewModel> _createFilterSelectionViewModel;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private readonly IExternalSystemService _externalSystemService;
        private readonly PaperClaimsMessageProducer _messageProducerPaper;
        private readonly MessageProducer837 _messageProducerElectronic;

        readonly IMapper<IGrouping<InvoiceReceivable, BillingServiceTransaction>, TransactionSearchResultViewModel> _transactionSearchResultMapper;
        IMapper<SubmitTransactionsUserSettingViewModel, ApplicationSettingContainer<SubmitTransactionsApplicationSetting>> _userSettingViewModelToApplicationSettingMapper;
        IMapper<ApplicationSettingContainer<SubmitTransactionsApplicationSetting>, SubmitTransactionsUserSettingViewModel> _applicationSettingToUserSettingViewModelMapper;
        IMapper<ClaimsMachineSettingViewModel, ApplicationSettingContainer<ClaimsConfigurationApplicationSetting>> _claimsViewModelToApplicationSettingMapper;
        IMapper<ApplicationSettingContainer<ClaimsConfigurationApplicationSetting>, ClaimsMachineSettingViewModel> _applicationSettingToClaimsViewModelMapper;

        public SubmitTransactionsViewService(Func<SubmitTransactionsFilterViewModel> createFilterViewModel,
            Func<SubmitTransactionsFilterSelectionViewModel> createFilterSelectionViewModel,
            IMapper<IGrouping<InvoiceReceivable, BillingServiceTransaction>, TransactionSearchResultViewModel> transactionSearchResultMapper,
            IApplicationSettingsService applicationSettingsService,
            IExternalSystemService externalSystemService,
            MessageProducer837 messageProducer,
            PaperClaimsMessageProducer messageProducerPaper)
        {
            _createFilterViewModel = createFilterViewModel;
            _createFilterSelectionViewModel = createFilterSelectionViewModel;
            _applicationSettingsService = applicationSettingsService;
            _messageProducerPaper = messageProducerPaper;
            _externalSystemService = externalSystemService;
            _messageProducerElectronic = messageProducer;

            _transactionSearchResultMapper = transactionSearchResultMapper;
            InitMappers();
        }

        public virtual SubmitTransactionsLoadInformation LoadInformation(int userId, string machineName)
        {
            var loadInfo = new SubmitTransactionsLoadInformation();

            loadInfo.FilterSelectionViewModel = _createFilterSelectionViewModel();
            loadInfo.FilterViewModel = _createFilterViewModel();

            var getClaimsAvailableForTransmission = Task.Factory.StartNewWithCurrentTransaction(() => GetClaimsAvailableForTransmission());
            var getSupportedClaimForms = Task.Factory.StartNewWithCurrentTransaction(() => PracticeRepository.ClaimFormTypes.ToList());
            var getOldestBillDate = Task.Factory.StartNewWithCurrentTransaction(() => PracticeRepository.BillingServiceTransactions.Where(x => x.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued).OrderBy(x => x.DateTime).Select(x => x.DateTime).FirstOrDefault());

            var getBillingDoctors = Task.Factory.StartNewWithCurrentTransaction(() => LoadBillingDoctors());
            var getBillingOrganizations = Task.Factory.StartNewWithCurrentTransaction(() => LoadBillingOrganizations());
            var getInsurers = Task.Factory.StartNewWithCurrentTransaction(() => LoadInsurers());
            var getLocations = Task.Factory.StartNewWithCurrentTransaction(() => LoadLocations());
            var getScheduledDoctors = Task.Factory.StartNewWithCurrentTransaction(() => LoadScheduledDoctors());
            var getSavedFilters = Task.Factory.StartNewWithCurrentTransaction(() => LoadSavedFilters(userId));
            var getClaimsDirectoryPaths = Task.Factory.StartNewWithCurrentTransaction(() =>  GetClaimsDirectoryPaths(userId, machineName));

            loadInfo.ClaimsDirectoryPaths = getClaimsDirectoryPaths.Result;
            loadInfo.FilterViewModel.BillingDoctors = getBillingDoctors.Result;
            loadInfo.FilterViewModel.BillingOrganizations = getBillingOrganizations.Result;
            loadInfo.FilterViewModel.Insurers = getInsurers.Result;
            loadInfo.FilterViewModel.Locations = getLocations.Result;
            loadInfo.FilterViewModel.ScheduledDoctors = getScheduledDoctors.Result;
            loadInfo.FilterViewModel.SavedFilters = getSavedFilters.Result;
            
            var claimsAvailableForTransmission = getClaimsAvailableForTransmission.Result;
            var supportedClaimForms = getSupportedClaimForms.Result;

            loadInfo.FilterViewModel.ElectronicClaimForms = supportedClaimForms.Where(x => x.MethodSent == MethodSent.Electronic).Select(x => new ClaimSelectionViewModel { Id = x.Id, Name = x.Name, HasItemsAvailableForTransmission = claimsAvailableForTransmission.Any(z => z == x.Id) }).ToExtendedObservableCollection();
            loadInfo.FilterViewModel.PaperClaimForms = supportedClaimForms.Where(x => x.MethodSent == MethodSent.Paper).Select(x => new ClaimSelectionViewModel { Id = x.Id, Name = x.Name, HasItemsAvailableForTransmission = claimsAvailableForTransmission.Any(z => z == x.Id) }).ToExtendedObservableCollection();
            loadInfo.FilterViewModel.TypesOfClaims = Enums.GetValues<ClaimType>().Select(x => new ClaimSelectionViewModel { Name = x.GetDisplayName(), Id = (int)x, HasItemsAvailableForTransmission = (x == ClaimType.EClaims && loadInfo.FilterViewModel.ElectronicClaimForms.Any(y => y.HasItemsAvailableForTransmission)) || (x == ClaimType.PaperClaims && loadInfo.FilterViewModel.PaperClaimForms.Any(y => y.HasItemsAvailableForTransmission)) }).ToExtendedObservableCollection();


            loadInfo.FilterSelectionViewModel.StartDate = getOldestBillDate.Result;
            loadInfo.FilterSelectionViewModel.StartDate = loadInfo.FilterSelectionViewModel.StartDate == DateTime.MinValue ? DateTime.Now.ToClientTime() : loadInfo.FilterSelectionViewModel.StartDate;
            loadInfo.FilterSelectionViewModel.EndDate = DateTime.Now.ToClientTime();

            return loadInfo;
        }

        [TransactionScope(TransactionScopeOption.Suppress, IsolationLevel.ReadUncommitted)]
        public virtual IEnumerable<TransactionSearchResultViewModel> SearchTransactions(SubmitTransactionsFilterSelectionViewModel filterSelections)
        {
            var billingServiceTransactions = PracticeRepository.BillingServiceTransactions.Where(x => x.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued
                                                                                                      && x.MethodSent == (MethodSent)((int)filterSelections.ClaimType) && x.InvoiceReceivable.PatientInsurance != null);

            if (filterSelections.StartDate.HasValue)
            {
                billingServiceTransactions = billingServiceTransactions.Where(x => x.DateTime >= filterSelections.StartDate);
            }

            if (filterSelections.EndDate.HasValue)
            {
                billingServiceTransactions = billingServiceTransactions.Where(x => x.DateTime <= filterSelections.EndDate);
            }

            IEnumerable<BillingServiceTransaction> listOfTransactions = billingServiceTransactions.ToList();
            LoadBillingTransactionData(listOfTransactions);

            if (filterSelections.SelectedInsurers.IsNotNullOrEmpty())
            {
                var ids = filterSelections.SelectedInsurers.SelectMany(y => y.InsurerIds).ToList();
                listOfTransactions = listOfTransactions.Where(x => ids.Contains(x.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId));
            }

            if (filterSelections.SelectedBillingDoctors.IsNotNullOrEmpty() && !filterSelections.Ub04Selected) // billing doctor filter doesn't make sense when ub04 claims are selected because for Insitutional Claims the Billing Provider is the facility
            {
                var ids = filterSelections.SelectedBillingDoctors.Select(y => y.Id).ToList();
                listOfTransactions = listOfTransactions.Where(x => x.InvoiceReceivable.Invoice.BillingProvider.UserId.HasValue && ids.Contains(x.InvoiceReceivable.Invoice.BillingProvider.UserId.Value));
            }

            if (filterSelections.SelectedLocations.IsNotNullOrEmpty())
            {
                var ids = filterSelections.SelectedLocations.Select(y => y.Id).ToList();
                listOfTransactions = listOfTransactions.Where(x => ids.Contains(x.InvoiceReceivable.Invoice.Encounter.ServiceLocationId));
            }

            if (filterSelections.SelectedBillingOrganizations.IsNotNullOrEmpty())
            {
                var ids = filterSelections.SelectedBillingOrganizations.Select(y => y.Id).ToList();
                listOfTransactions = listOfTransactions.Where(x => ids.Contains(x.InvoiceReceivable.Invoice.BillingProvider.BillingOrganizationId));
            }

            if (filterSelections.SelectedScheduledDoctors.IsNotNullOrEmpty())
            {
                // need to do this in memory because the sql that entity framework produces is very nested and causes timeouts on the server
                var ids = filterSelections.SelectedScheduledDoctors.Select(y => y.Id).ToList();
                listOfTransactions = listOfTransactions.Where(x => ids.Contains(x.InvoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().Select(y => y.UserId).FirstOrDefault())).ToList();
            }

            if (filterSelections.ClaimType == ClaimType.PaperClaims && filterSelections.PaperClaimFormId.HasValue)
            {
                // by default, foreach invoice type, make sure that if the insurer does not support one of the paper claim types, then add support for cms-1500 to it.
                foreach (var trans in listOfTransactions)
                {
                    if (!trans.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Any(x => x.ClaimFormType.MethodSent == MethodSent.Paper && x.InvoiceType == trans.InvoiceReceivable.Invoice.InvoiceType))
                    {
                        var newInsurerClaimForm = new InsurerClaimForm
                        {
                            ClaimFormType = PracticeRepository.ClaimFormTypes.FirstOrDefault(c => c.Id == (int)ClaimFormTypeId.Cms1500),
                            InvoiceType = trans.InvoiceReceivable.Invoice.InvoiceType,
                            InsurerId = trans.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId
                        };
                        PracticeRepository.Save(newInsurerClaimForm);
                        trans.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Add(newInsurerClaimForm);
                    }
                }

                listOfTransactions = listOfTransactions.Where(x => x.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Any(y => y.ClaimFormTypeId == filterSelections.PaperClaimFormId && y.InvoiceType == x.InvoiceReceivable.Invoice.InvoiceType));
            }
            else if (filterSelections.ClaimType == ClaimType.EClaims && filterSelections.SelectedElectronicClaims.IsNotNullOrEmpty())
            {
                // by default, foreach invoice type, make sure that if the insurer does not support one of the electronic claim types, then add support for "837 professional" to it.
                foreach (var trans in listOfTransactions)
                {
                    if (!trans.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Any(x => x.ClaimFormType.MethodSent == MethodSent.Electronic && x.InvoiceType == trans.InvoiceReceivable.Invoice.InvoiceType))
                    {
                        var newInsurerClaimForm = new InsurerClaimForm
                                                 {
                                                     ClaimFormType = PracticeRepository.ClaimFormTypes.FirstOrDefault(c => c.Id == (int)ClaimFormTypeId.Professional837),
                                                     InvoiceType = trans.InvoiceReceivable.Invoice.InvoiceType,
                                                     InsurerId = trans.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId
                                                 };
                        PracticeRepository.Save(newInsurerClaimForm);
                        trans.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Add(newInsurerClaimForm);
                    }
                }

                var ids = filterSelections.SelectedElectronicClaims.Select(z => z.Id);

                listOfTransactions = listOfTransactions.Where(x => x.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Any(y => ids.Contains(y.ClaimFormTypeId) && y.InvoiceType == x.InvoiceReceivable.Invoice.InvoiceType));
            }

            var grouping = listOfTransactions.GroupBy(x => x.InvoiceReceivable);

            var viewModels = grouping.Select(x => _transactionSearchResultMapper.Map(x));

            // order in memory because most times it will be quicker
            viewModels = viewModels
                .OrderBy(x => x.LastName)
                .ThenBy(x => x.PatientId)
                .ToArray();

            return viewModels;
        }

        public virtual int? SaveUserSetting(SubmitTransactionsUserSettingViewModel userSettingViewModel)
        {
            if (userSettingViewModel == null) return null;

            var setting = _userSettingViewModelToApplicationSettingMapper.Map(userSettingViewModel);
            _applicationSettingsService.SetSetting(setting);

            return setting.Id;
        }

        public virtual void DeleteUserSetting(int userSettingId)
        {
            _applicationSettingsService.DeleteSettings(new[] { userSettingId });
        }

        public virtual ClaimsMachineSettingViewModel GetClaimsDirectoryPaths(int userId, string machineName)
        {
            var setting = _applicationSettingsService.GetOrCreateSetting<ClaimsConfigurationApplicationSetting>(ApplicationSetting.ClaimsDirectory, machineName, userId);
            if (setting.Value == null)
            {
                setting.Value = new ClaimsConfigurationApplicationSetting();
            }

            return _applicationSettingToClaimsViewModelMapper.Map(setting);
        }

        public virtual int? SetClaimsDirectoryPaths(ClaimsMachineSettingViewModel viewModel)
        {
            var setting = _claimsViewModelToApplicationSettingMapper.Map(viewModel);
            _applicationSettingsService.SetSetting(setting);
            return setting.Id;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SetTransactionsToSentStatus(IEnumerable<Guid> transactionsIds)
        {
            var transactions = PracticeRepository.BillingServiceTransactions.Where(x => transactionsIds.Contains(x.Id)).ToList();

            foreach (var trans in transactions)
            {
                if (trans.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued) // only set the status for transactions that are queued.
                {
                    trans.BillingServiceTransactionStatus = BillingServiceTransactionStatus.Sent;
                    trans.DateTime = DateTime.Now.ToClientTime();

                    PracticeRepository.Save(trans);
                }
            }
        }

        public void SavePaperClaimExternalSystemMessage(string message, IEnumerable<Guid> transactionIds)
        {
            const string desc = "Paper claim";
            SaveExternalSystemMessage(ExternalSystemMessageTypeId.PaperClaim_Outbound, desc, message, transactionIds);
        }

        public void SavePaperClaimExternalSystemMessageBulk(List<Tuple<string, List<Guid>>> listOfMessages)
        {
            const string desc = "Paper claim";
            listOfMessages.ForEach(x => SaveExternalSystemMessage(ExternalSystemMessageTypeId.PaperClaim_Outbound, desc, x.Item1, x.Item2));
        }

        public void SaveElectronicClaimExternalSystemMessage(X12MessageViewModel x12Message, IEnumerable<Guid> transactionIds)
        {
            var desc = "Electronic claim of type {0}".FormatWith(x12Message.MessageType);

            SaveExternalSystemMessage(ExternalSystemMessageTypeId.X12_837_Outbound, desc, x12Message.Message, transactionIds, x12Message.Id);
        }

        public X12MessageViewModel ProduceMessage837(IEnumerable<Guid> transactionIds, int claimFormTypeId, out IList<string> validationResults, out IEnumerable<Guid> excludedTransactionIds)
        {
            X12Message message = (X12Message)_messageProducerElectronic.ProduceMessage(transactionIds, out validationResults, out excludedTransactionIds, claimFormTypeId);

            if (message == null) return null;

            return new X12MessageViewModel { Message = message.ToString(), Id = message.Id.ToString(), MessageType = message.MessageType };
        }

        public Dictionary<long, List<string>> ProduceMessagePaperClaimHtml(Dictionary<long, List<Guid>> transactionIdsByInvoiceReceivableId, int claimFormTypeId)
        {
            var claimFormType = PracticeRepository.ClaimFormTypes.Include(x => x.TemplateDocument).SingleOrDefault(x => x.Id == claimFormTypeId);

            if (claimFormType == null) return null;

            var template = (string)claimFormType.TemplateDocument.GetContent();

            var baseMessages = _messageProducerPaper.ProduceMessageMany(transactionIdsByInvoiceReceivableId, template, claimFormType.TemplateDocument.ContentType);
            Dictionary<long, List<string>> messages = baseMessages.ToDictionary(x => x.Key, y => ((Services.Documents.HtmlDocument)y.Value).Content);

            return messages;
        }

        public Tuple<int, string, string> GetClearingHouseInfo(int claimFileReceiverId)
        {
            var clearingHouse = PracticeRepository.ClaimFileReceivers.Single(x => x.Id == claimFileReceiverId);

            return new Tuple<int, string, string>(clearingHouse.Id, clearingHouse.Name, clearingHouse.WebsiteUrl);
        }

        public string GetDocumentName(int claimFormTypeId)
        {
            var claimFormType = PracticeRepository.ClaimFormTypes.Include(x => x.TemplateDocument).SingleOrDefault(x => x.Id == claimFormTypeId);
            if (claimFormType != null && claimFormType.TemplateDocument != null)
            {
                return claimFormType.TemplateDocument.Name;
            }

            return null;
        }

        private void SaveExternalSystemMessage(ExternalSystemMessageTypeId externalSystemMessageTypeId, string description, string messageValue, IEnumerable<Guid> transactionIds, string correlationId = null)
        {
            ExternalSystemMessage message = _externalSystemService.CreateMessage(new Dictionary<PracticeRepositoryEntityId, IEnumerable<object>>(),
                ExternalSystemId.ClaimFileMessage.ToString(),
                externalSystemMessageTypeId, null);

            message.CreatedBy = "SubmitTransactionScreen";
            message.Description = description;
            message.Value = messageValue;
            message.ExternalSystemMessageProcessingStateId = (int)ExternalSystemMessageProcessingStateId.Processed;
            message.CreatedDateTime = DateTime.UtcNow;
            message.UpdatedDateTime = DateTime.UtcNow;
            message.CorrelationId = correlationId;  // for 837 messages set this to the unique id on the x12 message                                   

            var billingServiceTransactions = PracticeRepository.BillingServiceTransactions.Where(x => transactionIds.Contains(x.Id)).ToList();
            foreach (var bst in billingServiceTransactions)
            {
                message.ExternalSystemMessagePracticeRepositoryEntities.Add(new ExternalSystemMessagePracticeRepositoryEntity
                {
                    PracticeRepositoryEntityId = (int) PracticeRepositoryEntityId.BillingServiceTransaction,
                    PracticeRepositoryEntityKey = bst.Id.ToString()
                });
                PracticeRepository.Save(bst);
            }

            PracticeRepository.Save(message);
        }

        private void LoadBillingTransactionData(IEnumerable<BillingServiceTransaction> listOfTransactions)
        {
            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(listOfTransactions,
                                  bst => bst.BillingService,
                                  bst => bst.ClaimFileReceiver,
                                  bst => bst.InvoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().Select(x => x.AppointmentType),
                                  bst => bst.InvoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().Select(x => x.User),
                                  bst => bst.InvoiceReceivable.Invoice.Encounter.ServiceLocation,
                                  bst => bst.InvoiceReceivable.Invoice.Encounter.Patient,
                                  bst => bst.InvoiceReceivable.Invoice.BillingProvider.BillingOrganization,
                                  bst => bst.InvoiceReceivable.Invoice.BillingProvider.User,
                                  bst => bst.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.Select(x => x.ClaimFormType));
        }

        private IEnumerable<int> GetClaimsAvailableForTransmission()
        {
            var newTypes = PracticeRepository.BillingServiceTransactions
                .Where(x => x.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Queued)
                .Select(x => new { x.InvoiceReceivable.Invoice.InvoiceType, x.MethodSent, InsurerId = x.InvoiceReceivable.PatientInsurance != null ? x.InvoiceReceivable.PatientInsurance.InsurancePolicy.InsurerId : new int?() })
                .ToList();

            var insurerClaimForms = PracticeRepository.InsurerClaimForms.Include(x => x.ClaimFormType).ToList();
            var listOfClaimFormTypeIds = new List<int>();

            foreach (var claimForm in insurerClaimForms)
            {
                if (listOfClaimFormTypeIds.Contains(claimForm.ClaimFormTypeId)) continue;

                if (newTypes.Any(y => y.MethodSent == claimForm.ClaimFormType.MethodSent && y.InsurerId == claimForm.InsurerId && y.InvoiceType == claimForm.InvoiceType))
                {
                    listOfClaimFormTypeIds.Add(claimForm.ClaimFormTypeId);
                }
            }

            return listOfClaimFormTypeIds;
        }

        private ObservableCollection<SubmitTransactionsUserSettingViewModel> LoadSavedFilters(int userId)
        {
            var appSettings = _applicationSettingsService.GetSettings<SubmitTransactionsApplicationSetting>(ApplicationSetting.SubmitTransactions, null, userId);

            return appSettings.Select(x => _applicationSettingToUserSettingViewModelMapper.Map(x)).OrderBy(f => f.Name).ToExtendedObservableCollection();
        }

        private ObservableCollection<NamedViewModel> LoadLocations()
        {
            var locations = PracticeRepository.ServiceLocations.Where(x => !x.IsArchived).OrderBy(x => x.ShortName);
            return new ObservableCollection<NamedViewModel>(locations.Select(y => new NamedViewModel { Id = y.Id, Name = y.ShortName }));
        }

        private ObservableCollection<SubmitTransactionsInsurersViewModel> LoadInsurers()
        {
            var insurers = PracticeRepository.Insurers.Where(x => !x.IsArchived && x.InsurerClaimForms.Any()).OrderBy(x => x.Name).ToList();

            var groupedByName = insurers.GroupBy(x => x.Name);

            return new ObservableCollection<SubmitTransactionsInsurersViewModel>(groupedByName.Select(x => new SubmitTransactionsInsurersViewModel { InsurerIds = x.Select(y => y.Id).ToList(), Name = x.Key }));
        }

        private ObservableCollection<NamedViewModel> LoadBillingOrganizations()
        {
            var billingOrganizations = PracticeRepository.BillingOrganizations.Where(x => !x.IsArchived).OrderBy(x => x.ShortName);
            return new ObservableCollection<NamedViewModel>(billingOrganizations.Select(x => new NamedViewModel { Id = x.Id, Name = x.ShortName }));
        }

        private ObservableCollection<NamedViewModel> LoadScheduledDoctors()
        {
            // get all the practice's doctors
            var doctors = PracticeRepository.Users.OfType<Doctor>().Where(x => !x.IsArchived && x.IsSchedulable).OrderBy(x => x.UserName);

            if (doctors.Any())
            {
                return doctors.Select(x => new NamedViewModel { Id = x.Id, Name = x.UserName }).ToExtendedObservableCollection();
            }

            return new ObservableCollection<NamedViewModel>();
        }

        private ObservableCollection<NamedViewModel> LoadBillingDoctors()
        {
            // get all the practice's doctors who are billable
            var doctors = PracticeRepository.Users.OfType<Doctor>().Where(x => !x.IsArchived && x.Providers.Any(y => y.IsBillable)).OrderBy(x => x.UserName);

            if (doctors.Any())
            {
                return doctors.Select(x => new NamedViewModel { Id = x.Id, Name = x.UserName }).ToExtendedObservableCollection();
            }

            return new ObservableCollection<NamedViewModel>();
        }

        void InitMappers()
        {
            _userSettingViewModelToApplicationSettingMapper = Mapper.Factory.CreateMapper<SubmitTransactionsUserSettingViewModel, ApplicationSettingContainer<SubmitTransactionsApplicationSetting>>(
                source => new ApplicationSettingContainer<SubmitTransactionsApplicationSetting>
                    {
                        Id = source.Id,
                        UserId = source.UserId,
                        Name = ApplicationSetting.SubmitTransactions,
                        Value = new SubmitTransactionsApplicationSetting
                            {
                                Name = source.Name,
                                ClaimType = source.ClaimType,
                                StartDate = source.StartDate,
                                EndDate = source.EndDate,
                                PaperClaimFormId = source.PaperClaimFormId,
                                SelectedBillingDoctors = source.SelectedBillingDoctors,
                                SelectedBillingOrganizations = source.SelectedBillingOrganizations,
                                SelectedInsurers = source.SelectedInsurers,
                                ElectronicClaimFormIds = source.ElectronicClaimFormIds,
                                SelectedLocations = source.SelectedLocations,
                                SelectedScheduledDoctors = source.SelectedScheduledDoctors
                            }
                    });

            _applicationSettingToUserSettingViewModelMapper = Mapper.Factory.CreateMapper<ApplicationSettingContainer<SubmitTransactionsApplicationSetting>, SubmitTransactionsUserSettingViewModel>(
                source => new SubmitTransactionsUserSettingViewModel
                    {
                        Id = source.Id,
                        UserId = source.UserId.Value,
                        Name = source.Value.Name,
                        ClaimType = source.Value.ClaimType,
                        StartDate = source.Value.StartDate,
                        EndDate = source.Value.EndDate,
                        PaperClaimFormId = source.Value.PaperClaimFormId,
                        SelectedBillingDoctors = source.Value.SelectedBillingDoctors,
                        SelectedBillingOrganizations = source.Value.SelectedBillingOrganizations,
                        SelectedInsurers = source.Value.SelectedInsurers,
                        ElectronicClaimFormIds = source.Value.ElectronicClaimFormIds,
                        SelectedLocations = source.Value.SelectedLocations,
                        SelectedScheduledDoctors = source.Value.SelectedScheduledDoctors
                    });

            _claimsViewModelToApplicationSettingMapper = Mapper.Factory.CreateMapper<ClaimsMachineSettingViewModel, ApplicationSettingContainer<ClaimsConfigurationApplicationSetting>>(
                source => new ApplicationSettingContainer<ClaimsConfigurationApplicationSetting>
                    {
                        Id = source.Id,
                        MachineName = source.MachineName,
                        UserId = source.UserId,
                        Name = ApplicationSetting.ClaimsDirectory,
                        Value = new ClaimsConfigurationApplicationSetting
                            {
                                PaperClaimsFileDirectory = source.PaperClaimsFileDirectory,
                                ElectronicClaimsFileDirectory = source.ElectronicClaimsFileDirectory
                            }
                    });

            _applicationSettingToClaimsViewModelMapper = Mapper.Factory.CreateMapper<ApplicationSettingContainer<ClaimsConfigurationApplicationSetting>, ClaimsMachineSettingViewModel>(
                source => new ClaimsMachineSettingViewModel
                    {
                        Id = source.Id,
                        UserId = source.UserId,
                        MachineName = source.MachineName,
                        ElectronicClaimsFileDirectory = source.Value.ElectronicClaimsFileDirectory,
                        PaperClaimsFileDirectory = source.Value.PaperClaimsFileDirectory
                    });
        }
    }

    internal class TransactionSearchResultsViewModelMap : IMap<IGrouping<InvoiceReceivable, BillingServiceTransaction>, TransactionSearchResultViewModel>
    {
        private readonly IEnumerable<IMapMember<IGrouping<InvoiceReceivable, BillingServiceTransaction>, TransactionSearchResultViewModel>> _members;

        public TransactionSearchResultsViewModelMap()
        {
            _members = this.CreateMembers(Map, "TransactionSearchResultsViewModelMap");
        }

        public void Map(IGrouping<InvoiceReceivable, BillingServiceTransaction> group, TransactionSearchResultViewModel viewModel)
        {
            var bst = group.First();

            viewModel.InvoiceReceivableId = group.Key.Id;
            viewModel.BillingServiceTransactionIds = group.Select(g => g.Id).ToList();
            viewModel.ServiceLocation = bst.InvoiceReceivable.Invoice.Encounter.ServiceLocation.ShortName;
            viewModel.ServiceDate = bst.InvoiceReceivable.Invoice.Encounter.StartDateTime;
            viewModel.ScheduledDoctor = bst.InvoiceReceivable.Invoice.Encounter.Appointments.OfType<UserAppointment>().FirstOrDefault().IfNotNull(x => x.User.IfNotNull(y => y.UserName, () => ""), () => "");
            viewModel.PatientId = bst.InvoiceReceivable.Invoice.Encounter.PatientId;
            viewModel.LastName = bst.InvoiceReceivable.Invoice.Encounter.Patient.LastName;
            viewModel.FirstName = bst.InvoiceReceivable.Invoice.Encounter.Patient.FirstName;
            viewModel.DateQueued = bst.DateTime;
            viewModel.ClaimFormType = bst.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.InsurerClaimForms.FirstOrDefault(x => x.InvoiceType == bst.InvoiceReceivable.Invoice.InvoiceType && x.ClaimFormType.MethodSent == bst.MethodSent).IfNotNull(x => new NamedViewModel { Id = x.ClaimFormType.Id, Name = x.ClaimFormType.Name }, new NamedViewModel());
            viewModel.ClaimFileReceiver = bst.ClaimFileReceiver != null ? new NamedViewModel { Id = bst.ClaimFileReceiver.Id, Name = bst.ClaimFileReceiver.Name } : null;
            viewModel.BillingOrganization = bst.InvoiceReceivable.Invoice.BillingProvider != null && bst.InvoiceReceivable.Invoice.BillingProvider.BillingOrganization != null ? bst.InvoiceReceivable.Invoice.BillingProvider.BillingOrganization.ShortName : null;
            viewModel.BillingDoctor = bst.InvoiceReceivable.Invoice.BillingProvider != null && bst.InvoiceReceivable.Invoice.BillingProvider.User is Doctor ? bst.InvoiceReceivable.Invoice.BillingProvider.User.UserName : "";
            viewModel.BilledInsurer = bst.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.Name;
            viewModel.AppointmentType = bst.InvoiceReceivable.Invoice.Encounter.Appointments.FirstOrDefault().IfNotNull(x => x.AppointmentType != null ? x.AppointmentType.Name : "", () => "");
            viewModel.AmountBilled = @group.Sum(g => g.AmountSent);
            viewModel.DisplayDecimal = bst.InvoiceReceivable.PatientInsurance.InsurancePolicy.Insurer.IsIcd10Decimal;
        }

        public IEnumerable<IMapMember<IGrouping<InvoiceReceivable, BillingServiceTransaction>, TransactionSearchResultViewModel>> Members
        {
            get { return _members; }
        }
    }

}
