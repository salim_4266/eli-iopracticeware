﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientAppointments;
using IO.Practiceware.Presentation.ViewModels.PatientReferralPopup;
using IO.Practiceware.Presentation.ViewServices.PatientReferralPopup;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;

[assembly: Component(typeof(PatientReferralPopupViewService), typeof(IPatientReferralPopupViewService))]
[assembly: Component(typeof(ReferralViewModelMap), typeof(IMap<PatientInsuranceReferral, ReferralViewModel>))]
namespace IO.Practiceware.Presentation.ViewServices.PatientReferralPopup
{
    public interface IPatientReferralPopupViewService
    {
        /// <summary>
        /// Attache the referral.
        /// </summary>
        /// <param name="encounterId">Encounter id</param>
        /// <param name="referralId">Referral id to attach</param>
        void AddReferralToEncounter(int encounterId, int referralId);

        /// <summary>
        /// Attache the referral.
        /// </summary>
        /// <param name="invoiceReceivableIds">Invoice receivable id collection</param>
        /// <param name="referralId">Referral id to attach</param>
        void AddReferralToInvoiceReceivables(IList<int> invoiceReceivableIds, int referralId);

        /// <summary>
        /// Remove the referral.
        /// </summary>
        /// <param name="encounterId">Encounter id</param>
        /// <param name="referralId">Referral id to remove</param>
        void RemoveReferralFromEncounter(int encounterId, int referralId);

        /// <summary>
        /// Get list of referrals
        /// </summary>
        /// <param name="referralIds">The list of ids</param>
        /// <returns>Referrals</returns>
        ObservableCollection<ReferralViewModel> GetReferrals(IEnumerable<int> referralIds);

        /// <summary>
        /// Removed the referral.
        /// </summary>
        /// <param name="invoiceReceivableIds">Invoice receivable id collection</param>
        /// <param name="referralId">Referral id to remove</param>
        void RemoveReferralFromInvoiceReceivables(IList<int> invoiceReceivableIds, int referralId);

    }

    public class PatientReferralPopupViewService : BaseViewService, IPatientReferralPopupViewService
    {
        private readonly IMapper<PatientInsuranceReferral, ReferralViewModel> _referralMapper;

        public PatientReferralPopupViewService(IMapper<PatientInsuranceReferral, ReferralViewModel> referralMapper)
        {
            _referralMapper = referralMapper;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void AddReferralToEncounter(int encounterId, int referralId)
        {
            var encounter = PracticeRepository.Encounters.Include(x => x.PatientInsuranceReferrals).Single(x => x.Id == encounterId);
            var referral = PracticeRepository.PatientInsuranceReferrals.Single(x => x.Id == referralId);
            encounter.PatientInsuranceReferrals.Add(referral);
            referral.Encounters.Add(encounter);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void AddReferralToInvoiceReceivables(IList<int> invoiceReceivableIds, int referralId)
        {
            var invoiceReceivables = PracticeRepository.InvoiceReceivables.Include(x => x.Invoice.Encounter).Where(x => invoiceReceivableIds.Contains(x.Id));
            var referral = PracticeRepository.PatientInsuranceReferrals.Include(x => x.Encounters).Single(x => x.Id == referralId);
            var referralEncounterIds = referral.Encounters.Select(x => x.Id).ToList();
            invoiceReceivables.Where(x => x.PatientInsuranceId == referral.PatientInsuranceId).ForEachWithSinglePropertyChangedNotification(x =>
            {
                x.PatientInsuranceReferral = referral;
                if (!referralEncounterIds.Contains(x.Invoice.EncounterId))
                {
                    referral.Encounters.Add(x.Invoice.Encounter);
                }
            });
        }


        [UnitOfWork(AcceptChanges = true)]
        public virtual void RemoveReferralFromEncounter(int encounterId, int referralId)
        {
            var encounter = PracticeRepository.Encounters.Include(x => x.PatientInsuranceReferrals).Single(x => x.Id == encounterId);

            var referralToRemove = encounter.PatientInsuranceReferrals.Single(x => x.Id == referralId);
            encounter.PatientInsuranceReferrals.Remove(referralToRemove);
            referralToRemove.Encounters.Remove(encounter);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void RemoveReferralFromInvoiceReceivables(IList<int> invoiceReceivableIds, int referralId)
        {
            var invoiceReceivables = PracticeRepository.InvoiceReceivables.Include(x => x.Invoice.Encounter).Where(x => invoiceReceivableIds.Contains(x.Id));
            var referral = PracticeRepository.PatientInsuranceReferrals.Include(x => x.Encounters).Single(x => x.Id == referralId);
            var referralEncounterIds = new List<int>();
            referral.Encounters.ForEachWithSinglePropertyChangedNotification(x => referralEncounterIds.Add(x.Id));
            invoiceReceivables.Where(x => x.PatientInsuranceId == referral.PatientInsuranceId).ForEachWithSinglePropertyChangedNotification(x =>
            {
                x.PatientInsuranceReferral = null;
                if (referralEncounterIds.Contains(x.Invoice.Encounter.Id))
                {
                    referral.Encounters.Remove(x.Invoice.Encounter);
                }
            });
        }

        public ObservableCollection<ReferralViewModel> GetReferrals(IEnumerable<int> referralIds)
        {
            if (referralIds.IsNullOrEmpty()) return null;

            var fromDatabaseReferrals = PracticeRepository.PatientInsuranceReferrals.Where(x => referralIds.Contains(x.Id)).ToArray();

            PracticeRepository.AsQueryableFactory().Load(fromDatabaseReferrals,
                                                          r => r.Encounters,
                                                          r => r.Doctor,
                                                          r => r.Patient,
                                                          r => r.PatientInsurance,
                                                          r => r.PatientInsurance.InsurancePolicy,
                                                          r => r.PatientInsurance.InsurancePolicy.Insurer);

            return fromDatabaseReferrals.Select(x => _referralMapper.Map(x)).ToExtendedObservableCollection();
        }

    }

    internal class ReferralViewModelMap : IMap<PatientInsuranceReferral, ReferralViewModel>
    {
        private readonly IEnumerable<IMapMember<PatientInsuranceReferral, ReferralViewModel>> _members;

        public ReferralViewModelMap()
        {
            _members = this.CreateMembers(referral => new ReferralViewModel
            {
                Id = referral.Id,
                StartDateTime = referral.StartDateTime.GetValueOrDefault(),
                EncounterIds = referral.Encounters.Select(x => x.Id).ToList(),
                EndDateTime = referral.EndDateTime.HasValue ? referral.EndDateTime.Value : new DateTime?(),
                ReferralCode = referral.ReferralCode,
                TotalEncountersCovered = referral.TotalEncountersCovered.GetValueOrDefault(),
                EncountersRemaining = referral.TotalEncountersCovered.GetValueOrDefault() - referral.Encounters.Count(e => !(e.EncounterStatus.IsCancelledStatus())),
                Resource = referral.Doctor != null ? new ResourceViewModel { Id = referral.Doctor.Id, FirstName = referral.Doctor.FirstName, LastName = referral.Doctor.LastName, DisplayName = referral.Doctor.UserName } : new ResourceViewModel(),
                Patient = new NamedViewModel { Id = referral.Patient.Id, Name = referral.Patient.DisplayName },
                Insurance = new InsuranceViewModel
                {
                    Id = (int)referral.PatientInsurance.Id,
                    PolicyId = referral.PatientInsurance.InsurancePolicy.Id,
                    PolicyCode = referral.PatientInsurance.InsurancePolicy.PolicyCode,
                    GroupCode = referral.PatientInsurance.InsurancePolicy.GroupCode,
                    StartDateTime = referral.PatientInsurance.InsurancePolicy.StartDateTime,
                    EndDateTime = referral.PatientInsurance.EndDateTime.HasValue ? referral.PatientInsurance.EndDateTime.Value : new DateTime?(),
                    InsurerId = referral.PatientInsurance.InsurancePolicy.Insurer.Id,
                    InsurerName = referral.PatientInsurance.InsurancePolicy.Insurer.Name
                }
            });
        }

        public IEnumerable<IMapMember<PatientInsuranceReferral, ReferralViewModel>> Members
        {
            get { return _members; }
        }
    }
}
