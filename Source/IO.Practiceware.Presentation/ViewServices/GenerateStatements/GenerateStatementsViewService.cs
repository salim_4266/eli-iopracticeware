﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using IO.Practiceware.Integration.Common;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.GenerateStatements;
using IO.Practiceware.Presentation.ViewModels.Setup.Billing;
using IO.Practiceware.Presentation.ViewServices.GenerateStatements;
using IO.Practiceware.Services.ApplicationSettings;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Domain;
using BillingServiceTransactionStatus = IO.Practiceware.Model.BillingServiceTransactionStatus;
using FinancialSourceType = IO.Practiceware.Model.FinancialSourceType;
using MethodSent = IO.Practiceware.Model.MethodSent;
using System.Collections;
using System.Data;

[assembly: Component(typeof(GenerateStatementsViewService), typeof(GenerateStatementsViewService))]
namespace IO.Practiceware.Presentation.ViewServices.GenerateStatements
{
    public interface IGenerateStatementsViewService
    {
        DataListsViewModel LoadInformation();
        ObservableCollection<GenerateStatementViewModel> GetStatements(FilterViewModel filter);
        Dictionary<int, string> GenerateExportContent(Hashtable parameters, int statementExportFormatId);
        void ForwardBillingServiceTransactions(Dictionary<int, byte[]> individualReportPdfs, Hashtable parameters, bool isExport);
        IEnumerable<Patient> FilterPatients(Hashtable parameters);
    }

    public class GenerateStatementsViewService : BaseViewService, IGenerateStatementsViewService
    {
        private IMapper<IGrouping<Patient, BillingServiceTransaction>, GenerateStatementViewModel> _statementPatientMapper;
        private IMapper<BillingServiceTransactionStatus, NamedViewModel> _billingServiceTransactionStatusMapper;
        private IMapper<IGrouping<Invoice, BillingServiceTransaction>, ExportStatementViewModel> _invoiceBillingServiceTransactionMapper;
        private IMapper<PatientAddress, AddressViewModel> _addressMapper;
        private IMapper<PatientPhoneNumber, PhoneNumberViewModel> _phoneNumberMapper;
        private IMapper<BillingService, BillingServiceViewModel> _billingServiceMapper;
        private readonly IRepositoryService _repositoryService;
        private readonly Lazy<int> _externalSystemExternalSystemMessageTypeId;
        private IMapper<IGrouping<BillingService, BillingServiceTransaction>, GenerateStatementViewModel> _statementsinvoiceBillingServiceTransactionMapper;
        private IMapper<IGrouping<Invoice, BillingServiceTransaction>, GenerateStatementViewModel> _statementsinvoiceTransactionMapper;

        public GenerateStatementsViewService(IMapper<BillingServiceTransactionStatus, NamedViewModel> billingServiceTransactionStatusMapper,
            IMapper<IGrouping<Invoice, BillingServiceTransaction>, ExportStatementViewModel> invoiceBillingServiceTransactionMapper,
            IRepositoryService repositoryService, IMapper<BillingService, BillingServiceViewModel> billingServiceMapper,
            IMapper<IGrouping<BillingService, BillingServiceTransaction>, GenerateStatementViewModel> statementsinvoiceBillingServiceTransactionMapper,
            IMapper<IGrouping<Invoice, BillingServiceTransaction>, GenerateStatementViewModel> statementsinvoiceTransactionMapper)
        {
            _billingServiceTransactionStatusMapper = billingServiceTransactionStatusMapper;
            _invoiceBillingServiceTransactionMapper = invoiceBillingServiceTransactionMapper;
            _repositoryService = repositoryService;
            _billingServiceMapper = billingServiceMapper;
            _statementsinvoiceBillingServiceTransactionMapper = statementsinvoiceBillingServiceTransactionMapper;
            _statementsinvoiceTransactionMapper = statementsinvoiceTransactionMapper;
            InitMappers();

            _externalSystemExternalSystemMessageTypeId = new Lazy<int>(() =>
            {
                var cachedExternalSystemExternalSystemMessageTypeId = PracticeRepository.ExternalSystemExternalSystemMessageTypes
                    .Where(esesmt => esesmt.ExternalSystemId == (int)ExternalSystemId.PatientStatement &&
                                     esesmt.ExternalSystemMessageTypeId == (int)ExternalSystemMessageTypeId.PatientStatement_Outbound)
                    .Select(esesmt => esesmt.Id).Single();
                return cachedExternalSystemExternalSystemMessageTypeId;
            });
        }

        private void InitMappers()
        {
            var mapperFactory = Mapper.Factory;

            _billingServiceTransactionStatusMapper = mapperFactory.CreateNamedViewModelMapper<BillingServiceTransactionStatus>();
            _addressMapper = Mapper.Factory.CreateAddressViewModelMapper<PatientAddress>();
            _phoneNumberMapper = Mapper.Factory.CreatePhoneViewModelMapper<PatientPhoneNumber>();




            _statementPatientMapper = mapperFactory.CreateMapper<IGrouping<Patient, BillingServiceTransaction>, GenerateStatementViewModel>(groupedBillingServiceTransaction => new GenerateStatementViewModel
            {
                OpenBalanceType = OpenBalanceType.Patient,
                Patient = new NamedViewModel(groupedBillingServiceTransaction.Key.Id, groupedBillingServiceTransaction.Key.DisplayName),
                BillingServiceTransactions = groupedBillingServiceTransaction
                    .Select(bst => new BillingServiceTransactionViewModel
                    {
                        Id = bst.Id,
                        BillingServiceTransactionStatus = new NamedViewModel { Id = (int)bst.BillingServiceTransactionStatus, Name = bst.BillingServiceTransactionStatus.GetDisplayName() },
                        Amount = bst.AmountSent,
                        DateTime = bst.DateTime,


                    })
                    .ToObservableCollection(),

                Total_Charges = groupedBillingServiceTransaction.Select(bst => bst.InvoiceReceivable).Distinct().Select(i => i.Invoice).SelectMany(b => b.BillingServices).Sum(x => x.UnitCharge.GetValueOrDefault() * x.Unit.GetValueOrDefault()),
                Total_Paid = groupedBillingServiceTransaction.Select(bst => bst.InvoiceReceivable).Distinct().Select(i => i.Invoice).SelectMany(b => b.BillingServices).SelectMany(x => x.Adjustments).Sum(x => x.Amount),
                Total_Patient = groupedBillingServiceTransaction.Select(bst => bst.InvoiceReceivable).Distinct().Select(i => i.Invoice).SelectMany(b => b.BillingServices).SelectMany(x => x.Adjustments).Where(adj => adj.AdjustmentTypeId == (int)AdjustmentTypeId.Payment && adj.FinancialSourceType == FinancialSourceType.Patient).Sum(x => x.Amount),
                OnAccountBalance = groupedBillingServiceTransaction.Select(bst => bst.BillingService).Distinct().Select(i => i.Invoice).Distinct().Select(inv => inv.Encounter.Patient).Distinct().SelectMany(e => e.Adjustments).Where(adj => adj.BillingServiceId == null && adj.InvoiceReceivableId == null).Sum(amt => amt.Amount), 
                NegativeAdjusments=groupedBillingServiceTransaction.Select(bst => bst.InvoiceReceivable).Distinct().Select(i => i.Invoice).SelectMany(b => b.BillingServices).SelectMany(x => x.Adjustments).Where(adj =>  adj.AdjustmentType !=null && adj.AdjustmentType.IsDebit ==true).Sum(x => x.Amount),

            });

            mapperFactory.CreateMapper<IGrouping<Patient, BillingServiceTransaction>, GenerateStatementViewModel>(groupedBillingServiceTransaction => new GenerateStatementViewModel
            {
                Patient = new NamedViewModel(groupedBillingServiceTransaction.Key.Id, groupedBillingServiceTransaction.Key.DisplayName),
                BillingServices = groupedBillingServiceTransaction.Select(bst => bst.BillingService).Select(billingService => new BillingServiceViewModel
                {
                    OpenBalance = ((billingService.UnitCharge.GetValueOrDefault() * billingService.Unit.GetValueOrDefault()) - billingService.Adjustments.CalculateTotalAdjustmentAmount(false)),
                }).ToObservableCollection()
            });

            _statementsinvoiceTransactionMapper = mapperFactory.CreateMapper<IGrouping<Invoice, BillingServiceTransaction>, GenerateStatementViewModel>(invoiceTransaction => new GenerateStatementViewModel
            {
                OpenBalanceType = OpenBalanceType.Invoice,
                Patient = new NamedViewModel { Id = invoiceTransaction.Key.Encounter.Patient.Id, Name = invoiceTransaction.Key.Encounter.Patient.DisplayName },
                BillingServices = invoiceTransaction.Select(x => x.BillingService).Distinct()
                    .Select(billingService => new BillingServiceViewModel
                    {
                        Id = billingService.Id,
                        Code = billingService.EncounterService.Code,
                        Description = billingService.EncounterService.Description,
                        UnitCharge = billingService.UnitCharge.GetValueOrDefault(),
                        UnitAmount = billingService.Unit.GetValueOrDefault(),
                        OrdinalId = billingService.OrdinalId.GetValueOrDefault(),
                        Diagnoses = billingService.BillingServiceBillingDiagnoses.Distinct().Where(x => x.IsIcd10 == false).IfNotNull(x => x.Take(4)).OrderBy(d => d.BillingDiagnosis.OrdinalId)
                            .Select(x => new DiagnosisViewModel
                            {
                                Id = x.BillingDiagnosis.Id,
                                Code = x.BillingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey,
                            }).ToObservableCollection(),
                        BillingServiceAdjustmentInformation = new BillingServiceAdjustmentInformationViewModel
                        {
                            TotalAdjustments = billingService.Adjustments.CalculateTotalAdjustmentAmount(true),
                            PatientPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Patient, null),
                            InsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, null),
                            PrimaryInsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, true),
                            OtherInsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, false),
                        },
                        OpenBalance = Math.Round(((billingService.UnitCharge.GetValueOrDefault() * billingService.Unit.GetValueOrDefault()) -
                                    (billingService.Adjustments.Count > 0 ? billingService.Adjustments.CalculateTotalAdjustmentAmount(false) : 0)), 2),

                        //billingService.Adjustments.CalculateTotalAdjustmentAmount(false)),
                        ServiceNotes = billingService.BillingServiceComments.Where(c => c.IsIncludedOnStatement && !c.IsArchived).Select(c => c.Value).ToObservableCollection(),
                        Modifiers = billingService.BillingServiceModifiers.OrderBy(m => m.OrdinalId).Select(m => m.ServiceModifier.Code).ToObservableCollection(),
                        BillingServiceDatetime = billingService.PatientBillDate.HasValue ? billingService.PatientBillDate.Value : DateTime.Now
                    }).ToObservableCollection(),
                Total_Charges = invoiceTransaction.Select(bst => bst.InvoiceReceivable).Distinct().Select(i => i.Invoice).SelectMany(b => b.BillingServices).Sum(x => x.UnitCharge.GetValueOrDefault() * x.Unit.GetValueOrDefault()),
                Total_Paid = invoiceTransaction.Select(bst => bst.InvoiceReceivable).Distinct().Select(i => i.Invoice).SelectMany(b => b.BillingServices).SelectMany(x => x.Adjustments).Sum(x => x.Amount),
                InvoiceAdjustments = invoiceTransaction.Select(x => x.InvoiceReceivable.Adjustments).
                    Select(adjustments => new InvoiceAdjustmentInformationViewModel
                    {
                        TotalAdjustments = adjustments.CalculateTotalAdjustmentAmount(false),
                        InvoiceDate = invoiceTransaction.Key.Encounter.StartDateTime,
                        InvoiceId = invoiceTransaction.Key.Id
                    }).ToObservableCollection()
            });


            _statementsinvoiceBillingServiceTransactionMapper = mapperFactory.CreateMapper<IGrouping<BillingService, BillingServiceTransaction>, GenerateStatementViewModel>(invoiceTransaction => new GenerateStatementViewModel
            {
                OpenBalanceType = OpenBalanceType.Service,
                Patient = new NamedViewModel { Id = invoiceTransaction.Key.Invoice.Encounter.Patient.Id, Name = invoiceTransaction.Key.Invoice.Encounter.Patient.DisplayName },
                BillingServices = invoiceTransaction.Select(x => x.BillingService).Distinct()
.Select(billingService => new BillingServiceViewModel
{
    Id = billingService.Id,
    Code = billingService.EncounterService.Code,
    Description = billingService.EncounterService.Description,
    UnitCharge = billingService.UnitCharge.GetValueOrDefault(),
    UnitAmount = billingService.Unit.GetValueOrDefault(),
    OrdinalId = billingService.OrdinalId.GetValueOrDefault(),
    Diagnoses = billingService.BillingServiceBillingDiagnoses.Distinct().Where(x => x.IsIcd10 == false).IfNotNull(x => x.Take(4)).OrderBy(d => d.BillingDiagnosis.OrdinalId)
        .Select(x => new DiagnosisViewModel
        {
            Id = x.BillingDiagnosis.Id,
            Code = x.BillingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey,
        }).ToObservableCollection(),
    BillingServiceAdjustmentInformation = new BillingServiceAdjustmentInformationViewModel
    {
        TotalAdjustments = billingService.Adjustments.CalculateTotalAdjustmentAmount(true),
        PatientPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Patient, null),
        InsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, null),
        PrimaryInsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, true),
        OtherInsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, false),
    },
    OpenBalance = Math.Round(((billingService.UnitCharge.GetValueOrDefault() * billingService.Unit.GetValueOrDefault()) -
                (billingService.Adjustments.Count > 0 ? billingService.Adjustments.CalculateTotalAdjustmentAmount(false) : 0)), 2),

    //billingService.Adjustments.CalculateTotalAdjustmentAmount(false)),

    ServiceNotes = billingService.BillingServiceComments.Where(c => c.IsIncludedOnStatement && !c.IsArchived).Select(c => c.Value).ToObservableCollection(),
    Modifiers = billingService.BillingServiceModifiers.OrderBy(m => m.OrdinalId).Select(m => m.ServiceModifier.Code).ToObservableCollection(),
    BillingServiceDatetime = invoiceTransaction.Key.Invoice.Encounter.StartDateTime
})
.ToObservableCollection(),
            });

            _invoiceBillingServiceTransactionMapper = mapperFactory.CreateMapper<IGrouping<Invoice, BillingServiceTransaction>, ExportStatementViewModel>(invoiceTransaction => new ExportStatementViewModel
            {
                Patient = new PatientViewModel
                {
                    Id = invoiceTransaction.Key.Encounter.PatientId,
                    FirstName = invoiceTransaction.Select(x => x.InvoiceReceivable.Invoice.Encounter.Patient.FirstName).FirstOrDefault(),
                    LastName = invoiceTransaction.Select(x => x.InvoiceReceivable.Invoice.Encounter.Patient.LastName).FirstOrDefault(),
                    MiddleName = invoiceTransaction.Select(x => x.InvoiceReceivable.Invoice.Encounter.Patient.MiddleName).FirstOrDefault(),
                    Suffix = invoiceTransaction.Select(x => x.InvoiceReceivable.Invoice.Encounter.Patient.Suffix).FirstOrDefault(),
                    PatientAddress = invoiceTransaction.Select(x => _addressMapper.Map(x.InvoiceReceivable.Invoice.Encounter.Patient.PatientAddresses.OrderBy(p => p.PatientAddressTypeId).FirstOrDefault(), true, false)).FirstOrDefault(),
                    PhoneNumber = invoiceTransaction.Select(x => _phoneNumberMapper.Map(x.InvoiceReceivable.Invoice.Encounter.Patient.PatientPhoneNumbers.OrderBy(p => p.PatientPhoneNumberType).FirstOrDefault(), true, false)).FirstOrDefault(),
                    PriorPatientCode = invoiceTransaction.Select(x => x.InvoiceReceivable.Invoice.Encounter.Patient.PriorPatientCode).FirstOrDefault(),
                    CanSendCollectionLetter = !invoiceTransaction.Select(x => x.InvoiceReceivable.Invoice.Encounter.Patient.PatientCommunicationPreferences.Where(y => y.PatientCommunicationType == PatientCommunicationType.Collection).Select(y => y.IsOptOut).FirstOrDefault()).FirstOrDefault() ? "Y" : "N",
                    PatientFinancialNote = invoiceTransaction.Select(x => x.InvoiceReceivable.Invoice.Encounter.Patient.PatientFinancialComments.Where(c => c.IsIncludedOnStatement && !c.IsArchived).OrderByDescending(c => c.Id).Select(y => y.Value).FirstOrDefault()).FirstOrDefault()
                },
                EncounterId = invoiceTransaction.Key.EncounterId,
                InvoiceDate = invoiceTransaction.Select(x => x.InvoiceReceivable.Invoice.Encounter.StartDateTime).FirstOrDefault(),
                InvoiceNote = invoiceTransaction.Select(x => x.InvoiceReceivable.Invoice.InvoiceComments.Where(c => c.IncludeOnStatement && !c.IsArchived).OrderByDescending(c => c.Id).Select(y => y.Value).FirstOrDefault()).FirstOrDefault(),
                PatientBilledDate = GetPatientBillDate(invoiceTransaction),
                Diagnoses = invoiceTransaction.Key.BillingDiagnoses.IfNotNull(d => d.Take(8)).OrderBy(d => d.OrdinalId)
                    .Select(x => new DiagnosisViewModel
                    {
                        Id = x.Id,
                        Code = x.BillingServiceBillingDiagnoses.Select(z => z.BillingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey).FirstOrDefault(),
                        OrdinalId = x.OrdinalId,
                    }).ToObservableCollection(),
                BillingServices = invoiceTransaction.Select(x => x.BillingService).Distinct().Take(12)
.Select(billingService => new BillingServiceViewModel
{
    Id = billingService.Id,
    Code = billingService.EncounterService.Code,
    Description = billingService.EncounterService.Description,
    UnitCharge = billingService.UnitCharge.GetValueOrDefault(),
    UnitAmount = billingService.Unit.GetValueOrDefault(),
    OrdinalId = billingService.OrdinalId.GetValueOrDefault(),
    Diagnoses = billingService.BillingServiceBillingDiagnoses.Distinct().Where(x => x.IsIcd10 == false).IfNotNull(x => x.Take(4)).OrderBy(d => d.BillingDiagnosis.OrdinalId)
        .Select(x => new DiagnosisViewModel
        {
            Id = x.BillingDiagnosis.Id,
            Code = x.BillingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey,
        }).ToObservableCollection(),
    BillingServiceAdjustmentInformation = new BillingServiceAdjustmentInformationViewModel
    {
        TotalAdjustments = billingService.Adjustments.CalculateTotalAdjustmentAmount(true),
        PatientPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Patient, null),
        InsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, null),
        PrimaryInsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, true),
        OtherInsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, false),
    },
    OpenBalance = ((billingService.UnitCharge.GetValueOrDefault() * billingService.Unit.GetValueOrDefault()) -
        billingService.Adjustments.CalculateTotalAdjustmentAmount(false)),
    ServiceNotes = billingService.BillingServiceComments.Where(c => c.IsIncludedOnStatement && !c.IsArchived).Select(c => c.Value).ToObservableCollection(),
    Modifiers = billingService.BillingServiceModifiers.OrderBy(m => m.OrdinalId).Select(m => m.ServiceModifier.Code).ToObservableCollection()
})
.ToObservableCollection(),
                AdjustmentInformation = new AdjustmentInformationViewModel
                {
                    InsuranceAmountPending = invoiceTransaction.Where(x => x.InvoiceReceivable.PatientInsuranceId != null
                                                                           && (x.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Sent
                                                                               || x.BillingServiceTransactionStatus == BillingServiceTransactionStatus.SentCrossOver))
                        .Sum(x => x.AmountSent),
                    AgingIntervals = GetAgingIntervals(invoiceTransaction),
                    PatientBalance = GetAgingIntervals(invoiceTransaction).Sum(x => x.Amount)
                }
            });

            _billingServiceMapper = mapperFactory.CreateMapper<BillingService, BillingServiceViewModel>(billingService => new BillingServiceViewModel
            {
                Id = billingService.Id,
                Code = billingService.EncounterService.IfNotNull(x => x.Code, ""),
                Description = billingService.EncounterService.IfNotNull(x => x.Description, ""),
                UnitCharge = billingService.UnitCharge.GetValueOrDefault(),
                UnitAmount = billingService.Unit.GetValueOrDefault(),
                OrdinalId = billingService.OrdinalId.GetValueOrDefault(),
                Diagnoses = billingService.BillingServiceBillingDiagnoses.Distinct().Where(x => x.IsIcd10 == false).IfNotNull(x => x.Take(4)).OrderBy(d => d.BillingDiagnosis.OrdinalId)
                    .Select(x => new DiagnosisViewModel
                    {
                        Id = x.BillingDiagnosis.Id,
                        Code = x.BillingDiagnosis.ExternalSystemEntityMapping.ExternalSystemEntityKey,
                    }).ToObservableCollection(),
                BillingServiceAdjustmentInformation = new BillingServiceAdjustmentInformationViewModel
                {
                    TotalAdjustments = billingService.Adjustments.CalculateTotalAdjustmentAmount(true),
                    PatientPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Patient, null),
                    InsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, null),
                    PrimaryInsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, true),
                    OtherInsurerPayments = billingService.Adjustments.CalculateTotalPaymentAmountBySource(FinancialSourceType.Insurer, false),
                },
                OpenBalance = ((billingService.UnitCharge.GetValueOrDefault() * billingService.Unit.GetValueOrDefault()) -
                               billingService.Adjustments.CalculateTotalAdjustmentAmount(false)),
                ServiceNotes = billingService.BillingServiceComments.Where(c => c.IsIncludedOnStatement && !c.IsArchived).Select(c => c.Value).ToObservableCollection(),
                Modifiers = billingService.BillingServiceModifiers.OrderBy(m => m.OrdinalId).Select(m => m.ServiceModifier.Code).ToObservableCollection(),
                BillingServiceDatetime = billingService.PatientBillDate.Value
            });
        }

        private static ObservableCollection<AgingIntervalViewModel> GetAgingIntervals(IGrouping<Invoice, BillingServiceTransaction> invoiceTransaction)
        {
            var results = new ObservableCollection<AgingIntervalViewModel>();

            var statuses = new[]
                {
                    BillingServiceTransactionStatus.Queued,
                    BillingServiceTransactionStatus.Sent,
                    BillingServiceTransactionStatus.Wait
                };

            for (var i = 1; i < 6; i++)
            {
                var interval = i;
                var date = DateTime.Now.ToClientTime();
                //var startDate = interval != 1 ? date.Date.AddDays((interval - 1) * -30) : date;


                DateTime startDate = date;
                switch (interval)
                {
                    case 1:
                        startDate = date;
                        break;
                    case 2:
                        startDate = date.Date.AddDays(-31);//date.Date.AddDays((interval - 1) * -30);
                        break;
                    case 3:
                        startDate = date.Date.AddDays(-61);
                        break;
                    case 4:
                        startDate = date.Date.AddDays(-91);
                        break;
                    case 5:
                        startDate = date.Date.AddDays(-121);
                        break;
                    default:
                        break;

                }




                var endDate = date.Date.AddDays(interval * -30);

                results.Add(new AgingIntervalViewModel
                {
                    Amount = invoiceTransaction.Where(x => statuses.Contains(x.BillingServiceTransactionStatus)
                            && x.InvoiceReceivable.PatientInsuranceId == null
                            && (x.BillingService.PatientBillDate.HasValue ? (interval == 5 ? x.BillingService.PatientBillDate <= startDate : (x.BillingService.PatientBillDate <= startDate && x.BillingService.PatientBillDate >= endDate))
                                    : (interval == 5 ? x.DateTime <= startDate : (x.DateTime <= startDate && x.DateTime >= endDate))))
                        .Sum(x => x.AmountSent),
                    Interval = (Interval)interval
                });
            }
            return results;
        }

        public DataListsViewModel LoadInformation()
        {
            var dataLists = new DataListsViewModel();
            new Action[]
                {
                    () =>
                    {
                        dataLists.ServiceLocations = PracticeRepository.ServiceLocations
                            .Select(x => new NamedViewModel
                            {
                                Id = x.Id,
                                Name = x.ShortName
                            })
                            .ToObservableCollection();
                    },
                    () =>
                    {
                        dataLists.RenderingProviders = PracticeRepository.Providers
                            .Include(p => p.ServiceLocation, p => p.User, p => p.BillingOrganization)
                            .Where(x => ((x.User != null && !x.User.IsArchived && x.User.IsSchedulable) || x.ServiceLocation != null) && x.BillingOrganization != null)
                            .OrderBy(x => x.User.UserName)
                            .ThenBy(x => x.BillingOrganization.ShortName)
                            .ThenBy(x => x.ServiceLocation.ShortName)
                            .ThenBy(x => x.Id)
                            .ToArray()
                            .Select(p => new NamedViewModel {Id = p.Id, Name = p.DisplayName})
                            .ToObservableCollection();
                    },
                    () =>
                    {
                        dataLists.TransactionStatuses = Enums.GetValues<BillingServiceTransactionStatus>()
                            .Where(x => (int) x < 4)
                            .Select(_billingServiceTransactionStatusMapper.Map)
                            .ToObservableCollection();
                    },
                    () =>
                    {
                        dataLists.StatmentCommunicationPreferences = new List<string>
                        {
                            "Opted In",
                            "Opted Out"
                        }
                            .Select((x, id) => new NamedViewModel
                            {
                                Id = id, Name = x
                            }).ToObservableCollection();
                    }
                }.ForAllInParallel(a => a());

            return dataLists;
        }

        public ObservableCollection<GenerateStatementViewModel> GetStatements(FilterViewModel filter)
        {
            if (filter == null) return new ObservableCollection<GenerateStatementViewModel>();

            var renderingProviders = filter.RenderingProviders.Select(x => x.Id).ToArray();
            var serviceLocations = filter.ServiceLocations.Select(x => x.Id).ToArray();
            var transactionStatuses = filter.TransactionStatuses.Select(x => x.Id).ToArray();
            var isOptOut = filter.StatmentCommunicationPreferences.Count() == 1 && filter.StatmentCommunicationPreferences.Single().Id == 1;

            if (string.IsNullOrEmpty(filter.PatientLastNameFrom))
                filter.PatientLastNameFrom = "a";
            if (string.IsNullOrEmpty(filter.PatientLastNameTo))
                filter.PatientLastNameTo = "z";

            using (new DbViewQueryingOptimizationScope())
            {
                var billingServiceTransactionExternalSystemMessages =
                    (from bst in PracticeRepository.BillingServiceTransactions
                     join esmpre in PracticeRepository.ExternalSystemMessagePracticeRepositoryEntities on CommonQueries.ConvertGuidToString(bst.Id) equals esmpre.PracticeRepositoryEntityKey into g
                     from esmpre in g.DefaultIfEmpty()
                     select new { BillingServiceTransaction = bst, esmpre.ExternalSystemMessage }
                        );

                var billingServiceTransactions =
                    (from i in billingServiceTransactionExternalSystemMessages
                     where i.BillingServiceTransaction.InvoiceReceivable.PatientInsuranceId == null
                           && (!renderingProviders.Any() ||
                               renderingProviders.Contains(i.BillingServiceTransaction.InvoiceReceivable.Invoice.BillingProviderId))
                           && (!serviceLocations.Any() ||
                               serviceLocations.Contains(i.BillingServiceTransaction.InvoiceReceivable.Invoice.Encounter.ServiceLocationId))
                           && (!transactionStatuses.Any() ||
                               transactionStatuses.Contains((int)i.BillingServiceTransaction.BillingServiceTransactionStatus))
                           && ((filter.StatmentCommunicationPreferences.Count() == 2) ||
                               ((i.BillingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.PatientCommunicationPreferences.All(x => x.PatientCommunicationType != PatientCommunicationType.Statement)) && isOptOut == false) ||
                               (i.BillingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.PatientCommunicationPreferences.Any(x => x.PatientCommunicationType == PatientCommunicationType.Statement && x.IsOptOut == isOptOut)))
                           && (filter.PatientLastNameFrom.IsNullOrEmpty() ||
                               String.Compare(i.BillingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.LastName, filter.PatientLastNameFrom, StringComparison.Ordinal) > 0)
                           && (filter.PatientLastNameTo.IsNullOrEmpty() ||
                               String.Compare(i.BillingServiceTransaction.InvoiceReceivable.Invoice.Encounter.Patient.LastName.Substring(0, filter.PatientLastNameTo == null ? 0 : filter.PatientLastNameTo.Length), filter.PatientLastNameTo, StringComparison.Ordinal) <= 0)
                     group i by i.BillingServiceTransaction
                         into g
                     where (!filter.BillingCycleDays.HasValue ||
                            !g.Any(x => x.BillingServiceTransaction.BillingServiceTransactionStatus == BillingServiceTransactionStatus.Sent
                                        && x.BillingServiceTransaction.DateTime >= DateTime.Now.ToClientTime().Date.AddDays(-filter.BillingCycleDays.GetValueOrDefault()).AddDays(1).AddTicks(-1)
                                        && (!serviceLocations.Any() || serviceLocations.Contains(x.BillingServiceTransaction.InvoiceReceivable.Invoice.Encounter.ServiceLocationId))
                                        && (!renderingProviders.Any() || renderingProviders.Contains(x.BillingServiceTransaction.InvoiceReceivable.Invoice.BillingProviderId))
                                        && (x.ExternalSystemMessage != null
                                            && x.ExternalSystemMessage.CreatedDateTime >= DateTime.Now.ToUniversalTime().Date.AddDays(-filter.BillingCycleDays.GetValueOrDefault()).AddDays(1).AddTicks(-1)))
                         )
                     select g.Key
                        )
                        .ToArray();

                PracticeRepository.AsQueryableFactory().Load(billingServiceTransactions,
                    bst => bst.InvoiceReceivable,
                    bst => bst.InvoiceReceivable.Invoice,
                    bst => bst.InvoiceReceivable.Invoice.InvoiceReceivables,
                    bst => bst.InvoiceReceivable.Invoice.InvoiceReceivables.Select(x => x.BillingServiceTransactions),
                    bst => bst.InvoiceReceivable.Invoice.InvoiceComments,
                    bst => bst.InvoiceReceivable.Invoice.Encounter,
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient,
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientAddresses,
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientAddresses.Select(x => x.StateOrProvince),
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientPhoneNumbers,
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientInsurances,
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientInsurances.Select(x => x.InsurancePolicy.PolicyholderPatient),
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientInsurances.Select(x => x.InsurancePolicy.PolicyholderPatient.PatientAddresses),
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientFinancialComments,
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.Adjustments,
                    bst => bst.InvoiceReceivable.Invoice.InvoiceReceivables.Select(x => x.PatientInsurance),
                    bst => bst.InvoiceReceivable.Invoice.BillingServices,
                    bst => bst.InvoiceReceivable.Invoice.BillingServices.Select(x => x.Adjustments),
                    bst => bst.InvoiceReceivable.Invoice.BillingDiagnoses,
                    bst => bst.BillingService,
                    bst => bst.BillingService.Adjustments,
                    bst => bst.BillingService.Adjustments.Select(x => x.AdjustmentType),
                    bst => bst.BillingService.Adjustments.Select(x => x.FinancialBatch),
                    bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable),
                    bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable.Invoice),
                    bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable.Invoice.Encounter),
                    bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable.PatientInsurance),
                    bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable.PatientInsurance.InsurancePolicy),
                    bst => bst.BillingService.EncounterService,
                    bst => bst.BillingService.BillingServiceBillingDiagnoses.Select(x => x.BillingDiagnosis.ExternalSystemEntityMapping),
                    bst => bst.BillingService.BillingServiceModifiers.Select(y => y.ServiceModifier),
                    bst => bst.BillingService.BillingServiceComments
                    );


                var openBalanceTypeSettings = ApplicationSettings.Cached.GetSetting<NamedViewModel>(ApplicationSetting.OpenBalanceDeterminer);

                var openBalanceType = openBalanceTypeSettings != null ? openBalanceTypeSettings.Value : new NamedViewModel { Id = (int)OpenBalanceType.Service, Name = OpenBalanceType.Service.ToString() };

                if (openBalanceType.Id == (int)OpenBalanceType.Patient)
                {

                    var results = billingServiceTransactions.GroupBy(bst => bst.InvoiceReceivable.Invoice.Encounter.Patient).Where(x => x.Sum(amt => amt.AmountSent) > 0 && x.Sum(amt => amt.AmountSent) >= filter.MinimumAmount).ToArray();

                    // The below code added for fixing issue- Patinet getting multiple statements for a given billingcycle days
                    if (transactionStatuses.Contains(2))
                    {
                        var rperson2 = (from e in results
                                        select e).ToArray();
                        string tempPatientIds = "";
                        string tempPIdsRemove = "";
                        for (int i = 0; i < rperson2.Length; i++)
                        {
                            tempPatientIds += rperson2[i].Key.Id + ",";
                        }
                        if (rperson2.Length > 0)
                        {
                            tempPatientIds = tempPatientIds.TrimEnd(",");// add empty condition 
                            string _SelectQry = " SELECT G.patientid from MODEL.ENCOUNTERS G ";
                            _SelectQry += "INNER JOIN   MODEL.Invoices Inv ON G.id=Inv.EncounterId ";
                            _SelectQry += "INNER JOIN   MODEL.BillingServices Bsv ON Bsv.InvoiceId=Inv.Id ";
                            _SelectQry += "INNER JOIN   MODEL.BillingServiceTransactions Bst ON Bst.BillingserviceId=Bsv.Id ";
                            _SelectQry += "INNER JOIN    MODEL.INVOICERECEIVABLES IncR ON Bst.InvoiceReceivableId=IncR.Id ";
                            _SelectQry += "WHERE IncR.PatientInsuranceId IS NULL AND Bst.DateTime >='" + DateTime.Now.ToClientTime().Date.AddDays(-filter.BillingCycleDays.GetValueOrDefault()).AddDays(1).AddTicks(-1).ToString() + "'  AND Bst.BillingServiceTransactionStatusId=2 ";
                            _SelectQry += "AND  G.patientid IN (" + tempPatientIds + ")";
                            DataTable dtTempPids = new DataTable();
                            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                            {
                                dbConnection.Open();
                                using (IDbCommand cmd = dbConnection.CreateCommand())
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.CommandText = _SelectQry;
                                    using (var reader = cmd.ExecuteReader())
                                    {
                                        dtTempPids.Load(reader);
                                    }
                                }
                                dbConnection.Close();
                            }
                            if (dtTempPids.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtTempPids.Rows.Count; i++)
                                {
                                    if (!Convert.ToString(dtTempPids.Rows[i]["patientid"]).IsNullOrEmpty())
                                    {
                                        tempPIdsRemove += Convert.ToString(dtTempPids.Rows[i]["patientid"]) + ",";
                                    }

                                }
                                tempPIdsRemove = tempPIdsRemove.TrimEnd(",");// add empty condition 
                                var rperson = tempPIdsRemove.Split(',').ToArray();
                                results = results.Where(b => !rperson.Any(a => a == Convert.ToString(b.Key.Id))).ToArray();
                            }
                        }
                    }
                    var validResults = results.Where(g => g.Any(z => z.BillingService != null)).ToArray();

                    var statementPatient = _statementPatientMapper.MapAll(validResults).ToObservableCollection();
                    var resultsPatient = statementPatient.Where(x => (x.BillingServiceTransactions.Sum(i => i.Amount)) > 0).ToObservableCollection();
                    var resultsStatement = resultsPatient.Where(x => (x.Total_Charges + (x.NegativeAdjusments)) - (x.Total_Paid - (x.NegativeAdjusments)) - (x.OnAccountBalance) > 0).ToObservableCollection();
                    return resultsStatement;
                }

                if (openBalanceType.Id == (int)OpenBalanceType.Invoice)
                {


                    // var results = billingServiceTransactions.GroupBy(bst => bst.InvoiceReceivable.Invoice).ToArray(); //Invoice OpenBalance

                    var results = billingServiceTransactions.GroupBy(bst => bst.InvoiceReceivable.Invoice).Where(x => x.Sum(amt => amt.AmountSent) > 0 && x.Sum(amt => amt.AmountSent) >= filter.MinimumAmount).ToArray();

                    if (transactionStatuses.Contains(2))
                    {
                        var rperson2 = (from e in results
                                        select e).ToArray();
                        string temInvIds = "";
                        string tempInvIdsRemove = "";
                        for (int i = 0; i < rperson2.Length; i++)
                        {
                            temInvIds += rperson2[i].Key.Id + ",";
                        }
                        if (rperson2.Length > 0)
                        {
                            temInvIds = temInvIds.TrimEnd(",");// add empty condition 
                            string _SelectQry = " SELECT Inv.Id FROM MODEL.Invoices Inv ";
                            _SelectQry += "INNER JOIN   MODEL.BillingServices Bsv ON Bsv.InvoiceId=Inv.Id ";
                            _SelectQry += "INNER JOIN   MODEL.BillingServiceTransactions Bst ON Bst.BillingserviceId=Bsv.Id ";
                            _SelectQry += "INNER JOIN    MODEL.INVOICERECEIVABLES IncR ON Bst.InvoiceReceivableId=IncR.Id ";
                            _SelectQry += "WHERE IncR.PatientInsuranceId IS NULL AND Bst.DateTime >='" + DateTime.Now.ToClientTime().Date.AddDays(-filter.BillingCycleDays.GetValueOrDefault()).AddDays(1).AddTicks(-1).ToString() + "'  AND Bst.BillingServiceTransactionStatusId=2 ";
                            _SelectQry += "AND  Inv.Id IN (" + temInvIds + ")";
                            DataTable dtTempPids = new DataTable();
                            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
                            {
                                dbConnection.Open();
                                using (IDbCommand cmd = dbConnection.CreateCommand())
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.CommandText = _SelectQry;
                                    using (var reader = cmd.ExecuteReader())
                                    {
                                        dtTempPids.Load(reader);
                                    }
                                }
                                dbConnection.Close();
                            }
                            if (dtTempPids.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtTempPids.Rows.Count; i++)
                                {
                                    if (!Convert.ToString(dtTempPids.Rows[i]["Id"]).IsNullOrEmpty())
                                    {
                                        tempInvIdsRemove += Convert.ToString(dtTempPids.Rows[i]["Id"]) + ",";
                                    }

                                }
                                tempInvIdsRemove = tempInvIdsRemove.TrimEnd(",");// add empty condition 
                                var rperson = tempInvIdsRemove.Split(',').ToArray();
                                results = results.Where(b => !rperson.Any(a => a == Convert.ToString(b.Key.Id))).ToArray();
                            }
                        }
                    }
                    var validResults = results.Where(g => g.Any(z => z.BillingService != null)).ToArray();

                    var validResultsInv = _statementsinvoiceTransactionMapper.MapAll(validResults).ToObservableCollection();
                    validResultsInv = validResultsInv.Where(x => (x.Total_Charges) - (x.Total_Paid) > 0).ToObservableCollection();
                    return validResultsInv;
                }
                else
                {
                    //var results = billingServiceTransactions.GroupBy(bst => bst.BillingService).ToArray(); //BillingService OpenBalance

                    var results = billingServiceTransactions.GroupBy(bst => bst.BillingService).Where(x => x.Sum(amt => amt.AmountSent) > 0 && x.Sum(amt => amt.AmountSent) >= filter.MinimumAmount).ToArray();

                    var validResults = results.Where(g => g.Any(z => z.BillingService != null)).ToArray();
                    return _statementsinvoiceBillingServiceTransactionMapper.MapAll(validResults).ToObservableCollection();

                }
            }
        }

        public virtual void ForwardBillingServiceTransactions(Dictionary<int, byte[]> individualReportPdfs, Hashtable parameters, bool isExport)
        {
            var patientExternalSystemMessages = GenerateNewPatientExternalSystemMessages(individualReportPdfs);
            ForwardBillingServiceTransactions(patientExternalSystemMessages, parameters, isExport);
        }

        public IEnumerable<Patient> FilterPatients(Hashtable parameters)
        {
            using (new DbViewQueryingOptimizationScope())
            {
                var invoiceIds = parameters["InvoiceIds"] as int[];
                var billingServiceIds = parameters["BillingServiceIds"] as int[];
                var patientIds = parameters["PatientIds"] as int[];
                var billingServiceTransactionIds = parameters["BillingServiceTransactionIds"] as Guid[];

                if (invoiceIds == null) invoiceIds = new int[0];
                if (billingServiceIds == null) billingServiceIds = new int[0];
                if (patientIds == null) patientIds = new int[0];

                BillingServiceTransaction[] billingServiceTransactions;

                if (billingServiceTransactionIds != null)
                {
                    billingServiceTransactions = PracticeRepository.BillingServiceTransactions
                        .Where(bst => bst.InvoiceReceivable.PatientInsuranceId == null
                                      && billingServiceTransactionIds.Contains(bst.Id)
                                      && (!patientIds.Any() || patientIds.Contains(bst.InvoiceReceivable.Invoice.Encounter.PatientId)))
                        .ToArray();
                }
                else
                {
                    billingServiceTransactions = PracticeRepository.BillingServiceTransactions
                        .Where(bst => bst.InvoiceReceivable.PatientInsuranceId == null
                                      && (!invoiceIds.Any() || invoiceIds.Contains(bst.InvoiceReceivable.Invoice.Id))
                                      && (!billingServiceIds.Any() || billingServiceIds.Contains(bst.BillingService.Id))
                                      && (!patientIds.Any() || patientIds.Contains(bst.InvoiceReceivable.Invoice.Encounter.PatientId)))
                        .ToArray();
                }
                PracticeRepository.AsQueryableFactory().Load(billingServiceTransactions,
                    bst => bst.InvoiceReceivable,
                    bst => bst.InvoiceReceivable.Invoice,
                    bst => bst.InvoiceReceivable.Invoice.InvoiceReceivables,
                    bst => bst.InvoiceReceivable.Invoice.InvoiceReceivables.Select(x => x.BillingServiceTransactions),
                    bst => bst.InvoiceReceivable.Invoice.InvoiceComments,
                    bst => bst.InvoiceReceivable.Invoice.Encounter,
                    bst => bst.InvoiceReceivable.Invoice.Encounter.Patient);

                return billingServiceTransactions.Select(i => i.InvoiceReceivable.Invoice.Encounter.Patient).Distinct();
            }
        }

        private Dictionary<int, ExternalSystemMessage> GenerateNewPatientExternalSystemMessages(Dictionary<int, byte[]> individualReportPdfs)
        {
            var date = DateTime.Now.ToClientTime().Date.ToUniversalTime();
            var patientExternalSystemMessages = new Dictionary<int, ExternalSystemMessage>();
            foreach (var individualReportPdf in individualReportPdfs)
            {
                var externalMessage = GenerateNewExternalSystemMessage(individualReportPdf, date);
                patientExternalSystemMessages.Add(individualReportPdf.Key, externalMessage);
            }

            _repositoryService.Persist(patientExternalSystemMessages.Values.Select(x => new ObjectStateEntry(x) { State = ObjectState.Added }).ToArray(), typeof(IPracticeRepository).FullName);

            return patientExternalSystemMessages;
        }

        private void ForwardBillingServiceTransactions(Dictionary<int, ExternalSystemMessage> patientExternalSystemMessages, Hashtable parameters, bool isExport)
        {
            var objectStateEntries = new List<IObjectStateEntry>();

            var statuses = new[]
            {
                (int) BillingServiceTransactionStatus.Queued,
                (int) BillingServiceTransactionStatus.Sent,
                (int) BillingServiceTransactionStatus.Wait
            };

            var invoiceIds = parameters["InvoiceIds"] as int[];
            var billingServiceIds = parameters["BillingServiceIds"] as int[];
            var patientIds = patientExternalSystemMessages.Keys.ToArray();
            var billingServiceTransactionIds = parameters["BillingServiceTransactionIds"] as Guid[];

            if (invoiceIds == null) invoiceIds = new int[0];
            if (billingServiceIds == null) billingServiceIds = new int[0];


            BillingServiceTransaction[] billingServiceTransactions;

            if (billingServiceTransactionIds != null)
            {
                billingServiceTransactions = PracticeRepository.BillingServiceTransactions
                    .Where(bst => statuses.Contains((int)bst.BillingServiceTransactionStatus)
                                  && bst.InvoiceReceivable.PatientInsuranceId == null
                                  && billingServiceTransactionIds.Contains(bst.Id)
                                  && patientIds.Contains(bst.InvoiceReceivable.Invoice.Encounter.PatientId))
                    .ToArray();
            }
            else
            {
                billingServiceTransactions = PracticeRepository.BillingServiceTransactions
                    .Where(bst => statuses.Contains((int)bst.BillingServiceTransactionStatus)
                                  && bst.InvoiceReceivable.PatientInsuranceId == null
                                  && (!invoiceIds.Any() || invoiceIds.Contains(bst.InvoiceReceivable.Invoice.Id))
                                  && (!billingServiceIds.Any() || billingServiceIds.Contains(bst.BillingService.Id))
                                  && patientIds.Contains(bst.InvoiceReceivable.Invoice.Encounter.PatientId))
                    .ToArray();
            }

            PracticeRepository.AsQueryableFactory().Load(billingServiceTransactions,
                bst => bst.InvoiceReceivable.Invoice.Encounter.Patient,
                bst => bst.BillingService);

            var patientBillingServiceTransactions = billingServiceTransactions
                .GroupBy(bst => bst.InvoiceReceivable.Invoice.Encounter.PatientId)
                .ToDictionary(x => x.Key).ToArray();


            var newBillingServiceTransactionObjectStateEntries = new List<IObjectStateEntry>();
            var externalSystemMessagePracticeRepositoryEntities = new Dictionary<BillingServiceTransaction, ExternalSystemMessage>();
            foreach (var patientBillingServiceTransaction in patientBillingServiceTransactions)
            {
                var externalSystemMessage = patientExternalSystemMessages[patientBillingServiceTransaction.Key];

                foreach (var billingServiceTransaction in patientBillingServiceTransaction.Value)
                {
                    switch (billingServiceTransaction.BillingServiceTransactionStatus)
                    {
                        case BillingServiceTransactionStatus.Queued:
                            // Update BillingServiceTransaction.Status = Sent, and DateTime to Now.
                            billingServiceTransaction.BillingServiceTransactionStatus = BillingServiceTransactionStatus.Sent;
                            billingServiceTransaction.DateTime = DateTime.Now.ToClientTime();
                            billingServiceTransaction.MethodSent = isExport ? MethodSent.Electronic : MethodSent.Paper;
                            if (!billingServiceTransaction.BillingService.PatientBillDate.HasValue)
                            {
                                billingServiceTransaction.BillingService.PatientBillDate = DateTime.Now.ToClientTime().Date;
                                objectStateEntries.Add(new ObjectStateEntry(billingServiceTransaction.BillingService) { State = ObjectState.Modified });
                            }
                            externalSystemMessagePracticeRepositoryEntities.Add(billingServiceTransaction, externalSystemMessage);
                            //objectStateEntries.Add(new ObjectStateEntry(externalSystemMessagePracticeRepositoryEntityQueued) { State = ObjectState.Added });
                            objectStateEntries.Add(new ObjectStateEntry(billingServiceTransaction) { State = ObjectState.Modified });
                            break;
                        case BillingServiceTransactionStatus.Sent:
                            // Close all BillingServiceTransactions where Status = Sent, do no change DateTime. 
                            // Generate new BillingServiceTransactions with Status = Sent and DateTime = Now
                            billingServiceTransaction.BillingServiceTransactionStatus = BillingServiceTransactionStatus.ClosedSent;
                            if (!billingServiceTransaction.BillingService.PatientBillDate.HasValue)
                            {
                                billingServiceTransaction.BillingService.PatientBillDate = DateTime.Now.ToClientTime().Date;
                                objectStateEntries.Add(new ObjectStateEntry(billingServiceTransaction.BillingService) { State = ObjectState.Modified });
                            }
                            //var newBillingServiceTransaction = GenerateNewBillingServiceTransaction(billingServiceTransaction, externalSystemMessage.Value);
                            var newBillingServiceTransaction = GenerateNewBillingServiceTransaction(billingServiceTransaction, isExport);
                            newBillingServiceTransactionObjectStateEntries.Add(new ObjectStateEntry(newBillingServiceTransaction) { State = ObjectState.Added });
                            externalSystemMessagePracticeRepositoryEntities.Add(newBillingServiceTransaction, externalSystemMessage);
                            objectStateEntries.Add(new ObjectStateEntry(billingServiceTransaction) { State = ObjectState.Modified });
                            break;
                        case BillingServiceTransactionStatus.Wait:
                            // Update BillingServiceTransaction.Status = Sent, and DateTime to Now.
                            billingServiceTransaction.BillingServiceTransactionStatus = BillingServiceTransactionStatus.Sent;
                            billingServiceTransaction.DateTime = DateTime.Now.ToClientTime();
                            billingServiceTransaction.MethodSent = isExport ? MethodSent.Electronic : MethodSent.Paper;
                            if (!billingServiceTransaction.BillingService.PatientBillDate.HasValue)
                            {
                                billingServiceTransaction.BillingService.PatientBillDate = DateTime.Now.ToClientTime().Date;
                                objectStateEntries.Add(new ObjectStateEntry(billingServiceTransaction.BillingService) { State = ObjectState.Modified });
                            }
                            externalSystemMessagePracticeRepositoryEntities.Add(billingServiceTransaction, externalSystemMessage);
                            //objectStateEntries.Add(new ObjectStateEntry(externalSystemMessagePracticeRepositoryEntityWait) { State = ObjectState.Added });
                            objectStateEntries.Add(new ObjectStateEntry(billingServiceTransaction) { State = ObjectState.Modified });
                            break;
                    }
                }
            }
            _repositoryService.Persist(newBillingServiceTransactionObjectStateEntries, typeof(IPracticeRepository).FullName);

            objectStateEntries.AddRange(externalSystemMessagePracticeRepositoryEntities.Select(x => GenerateNewExternalSystemMessagePracticeRepositoryEntity(x.Value, x.Key.Id))
                .Select(x => new ObjectStateEntry(x) { State = ObjectState.Added })
                .ToArray());

            _repositoryService.Persist(objectStateEntries, typeof(IPracticeRepository).FullName);
        }

        public Dictionary<int, string> GenerateExportContent(Hashtable parameters, int statementExportFormatId)
        {
            var renderingProviderIds = parameters["RenderingProviderIds"] as int[];
            var serviceLocationIds = parameters["ServiceLocationIds"] as int[];
            var invoiceIds = parameters["InvoiceIds"] as int[];
            var billingServiceIds = parameters["BillingServiceIds"] as int[];
            var billingServiceTransactionIds = parameters["BillingServiceTransactionIds"] as Guid[];
            var patientIds = parameters["PatientIds"] as int[];

            if (renderingProviderIds == null) renderingProviderIds = new int[0];
            if (serviceLocationIds == null) serviceLocationIds = new int[0];
            if (invoiceIds == null) invoiceIds = new int[0];
            if (billingServiceIds == null) billingServiceIds = new int[0];
            if (patientIds == null) patientIds = new int[0];

            BillingServiceTransaction[] billingServiceTransactions;

            if (billingServiceTransactionIds != null)
            {
                billingServiceTransactions = PracticeRepository.BillingServiceTransactions
                    .Where(bst => billingServiceTransactionIds.Contains(bst.Id)
                                  && (!patientIds.Any() || patientIds.Contains(bst.InvoiceReceivable.Invoice.Encounter.PatientId))
                                  && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent
                                  && (!renderingProviderIds.Any() || renderingProviderIds.Contains(bst.InvoiceReceivable.Invoice.BillingProviderId))
                                  && (!serviceLocationIds.Any() || serviceLocationIds.Contains(bst.InvoiceReceivable.Invoice.Encounter.ServiceLocationId)))
                    .ToArray();
            }
            else
            {
                billingServiceTransactions = PracticeRepository.BillingServiceTransactions
                    .Where(bst => (!invoiceIds.Any() || invoiceIds.Contains(bst.InvoiceReceivable.Invoice.Id))
                                  && (!billingServiceIds.Any() || billingServiceIds.Contains(bst.BillingService.Id))
                                  && (!patientIds.Any() || patientIds.Contains(bst.InvoiceReceivable.Invoice.Encounter.PatientId))
                                  && bst.BillingServiceTransactionStatus != BillingServiceTransactionStatus.ClosedNotSent
                                  && (!renderingProviderIds.Any() || renderingProviderIds.Contains(bst.InvoiceReceivable.Invoice.BillingProviderId))
                                  && (!serviceLocationIds.Any() || serviceLocationIds.Contains(bst.InvoiceReceivable.Invoice.Encounter.ServiceLocationId)))
                    .ToArray();
            }

            PracticeRepository.AsQueryableFactory().Load(billingServiceTransactions,
                bst => bst.InvoiceReceivable,
                bst => bst.InvoiceReceivable.Invoice,
                bst => bst.InvoiceReceivable.Invoice.InvoiceReceivables,
                bst => bst.InvoiceReceivable.Invoice.InvoiceReceivables.Select(x => x.BillingServiceTransactions),
                bst => bst.InvoiceReceivable.Invoice.InvoiceComments,
                bst => bst.InvoiceReceivable.Invoice.Encounter,
                bst => bst.InvoiceReceivable.Invoice.Encounter.Patient,
                bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientAddresses,
                bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientAddresses.Select(x => x.StateOrProvince),
                bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientPhoneNumbers,
                bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientInsurances,
                bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientInsurances.Select(x => x.InsurancePolicy.PolicyholderPatient),
                bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientInsurances.Select(x => x.InsurancePolicy.PolicyholderPatient.PatientAddresses),
                bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientFinancialComments,
                bst => bst.InvoiceReceivable.Invoice.InvoiceReceivables.Select(x => x.PatientInsurance),
                bst => bst.InvoiceReceivable.Invoice.BillingServices,
                bst => bst.InvoiceReceivable.Invoice.BillingServices.Select(x => x.Adjustments),
                bst => bst.InvoiceReceivable.Invoice.BillingDiagnoses,
                bst => bst.InvoiceReceivable.Invoice.DiagnosisType,
                bst => bst.BillingService,
                bst => bst.BillingService.Adjustments,
                bst => bst.BillingService.Adjustments.Select(x => x.AdjustmentType),
                bst => bst.BillingService.Adjustments.Select(x => x.FinancialBatch),
                bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable),
                bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable.Invoice),
                bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable.Invoice.Encounter),
                bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable.PatientInsurance),
                bst => bst.BillingService.Adjustments.Select(x => x.InvoiceReceivable.PatientInsurance.InsurancePolicy),
                bst => bst.BillingService.EncounterService,
                bst => bst.BillingService.BillingServiceBillingDiagnoses.Select(x => x.BillingDiagnosis.ExternalSystemEntityMapping),
                bst => bst.BillingService.BillingServiceModifiers.Select(y => y.ServiceModifier),
                bst => bst.BillingService.BillingServiceComments
                );

            var results = billingServiceTransactions
                .GroupBy(bst => bst.InvoiceReceivable.Invoice)
                .ToArray();

            var validResults = results
                .Where(g => (g.Any(bst => bst.InvoiceReceivable.Invoice.Encounter.Patient.PatientAddresses.Any())
                    && g.Any(z => z.InvoiceReceivable.Invoice.Encounter.Patient.FirstName.IsNotNullOrEmpty())
                    && g.Any(z => z.InvoiceReceivable.Invoice.Encounter.Patient.LastName.IsNotNullOrEmpty())
                    && g.Any(z => z.BillingService.EncounterService != null)))
                .ToArray();

            validResults.Where(x => x.Key.DiagnosisTypeId != null && x.Key.DiagnosisTypeId == (int)DiagnosisTypeId.Icd10).ForEach(x => IntegrationCommon.ReplaceInvoiceWithIcd10(x.Key));
            var exportStatements = _invoiceBillingServiceTransactionMapper
                .MapAll(validResults)
                .Where(x => x.AdjustmentInformation.PatientBalance > 0)
                .OrderBy(x => x.Patient.LastName)
                .ThenBy(x => x.Patient.FirstName)
                .ThenBy(x => x.Patient.MiddleName)
                .ThenBy(x => x.Patient.Suffix)
                .ThenBy(x => x.Patient.Id)
                .ToArray();

            var patientExportStatements = new Dictionary<int, string>();

            foreach (var patientExportStatementGroup in exportStatements
                .GroupBy(x => x.Patient.Id)
                .ToArray())
            {
                var sb = new StringBuilder();

                foreach (var encounterExportStatementGroup in patientExportStatementGroup
                    .GroupBy(x => x.EncounterId)
                    .ToArray())
                {
                    sb.AppendLine("STATEMENT START,{0}".FormatWith(string.Join(",", encounterExportStatementGroup.Select(x => x.FormattedInvoice).FirstOrDefault(), encounterExportStatementGroup.Select(x => x.InvoiceDate.ToString("yyyyMMdd")).FirstOrDefault())));
                    sb.AppendLine("PATIENT BILL DATE,{0}".FormatWith(encounterExportStatementGroup.Select(x => x.PatientBilledDate.ToString("yyyyMMdd")).FirstOrDefault()));
                    sb.AppendLine("PATIENT,{0}".FormatWith(string.Join(",", encounterExportStatementGroup.Select(x => x.Patient.Id).FirstOrDefault(),
                        encounterExportStatementGroup.Select(x => x.Patient.PriorPatientCode).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.FirstName).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.LastName).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.MiddleName).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.Suffix).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.PatientAddress.Line1).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.PatientAddress.Line2).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.PatientAddress.City).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.PatientAddress.StateOrProvince.Abbreviation).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.PatientAddress.PostalCode).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty)),
                        encounterExportStatementGroup.Select(x => x.Patient.PhoneNumber.PhoneNumber).FirstOrDefault().IfNotDefault(x => x.Replace(@",", string.Empty))
                        )));

                    if (encounterExportStatementGroup.Where(x => !string.IsNullOrEmpty(x.Patient.PatientFinancialNote)).Select(x => x.Patient.PatientFinancialNote).Any()) sb.AppendLine("BASE-NOTE, {0}".FormatWith(encounterExportStatementGroup.Where(x => !string.IsNullOrEmpty(x.Patient.PatientFinancialNote)).Select(x => x.Patient.PatientFinancialNote).FirstOrDefault().ToUpper()));
                    sb.AppendLine("DIAGS,{0}".FormatWith(encounterExportStatementGroup.SelectMany(x => x.Diagnoses.OrderBy(d => d.OrdinalId).Select(d => d.Code)).Join(",")));

                    foreach (var billingService in encounterExportStatementGroup.SelectMany(x => x.BillingServices).OrderByDescending(x => x.OrdinalId))
                    {
                        var service = billingService;

                        sb.AppendLine("SERVICE,{0}".FormatWith(string.Join(",", service.Code.Replace(@",", string.Empty),
                            service.Description.Replace(@",", string.Empty),
                            service.Modifiers.Join("").Replace(@",", string.Empty),
                            encounterExportStatementGroup.SelectMany(x => x.Diagnoses.Where(d => service.Diagnoses.Select(z => z.Id).Contains(d.Id)))
                                .Where(d => d.OrdinalId.HasValue)
                                .Select(d => d.OrdinalId)
                                .Join(""),
                            encounterExportStatementGroup.Select(x => x.InvoiceDate.ToString("yyyyMMdd")).FirstOrDefault(),
                            service.UnitCharge.TrimDecimalIfWholeNumber(),
                            service.UnitAmount.TrimDecimalIfWholeNumber(),
                            service.BillingServiceAdjustmentInformation.TotalPayments.TrimDecimalIfWholeNumber(),
                            statementExportFormatId == (int)StatementExportFormat.InsurancePaymentsBrokenOut
                            || statementExportFormatId == (int)StatementExportFormat.InsurancePaymentsBrokenOutAndAging
                                ? string.Join(",", billingService.BillingServiceAdjustmentInformation.PrimaryInsurerPayments.TrimDecimalIfWholeNumber(), billingService.BillingServiceAdjustmentInformation.OtherInsurerPayments.TrimDecimalIfWholeNumber())
                                : billingService.BillingServiceAdjustmentInformation.InsurerPayments.TrimDecimalIfWholeNumber(),
                            service.BillingServiceAdjustmentInformation.PatientPayments.TrimDecimalIfWholeNumber(),
                            service.BillingServiceAdjustmentInformation.TotalAdjustments.TrimDecimalIfWholeNumber(),
                            service.OpenBalance.TrimDecimalIfWholeNumber()
                            )));

                        if (billingService.ServiceNotes.Any())
                        {
                            billingService.ServiceNotes.ForEach(n => sb.AppendLine("SRV-NOTE, {0}".FormatWith(n.ToUpper())));
                        }
                    }

                    sb.AppendLine("INSURANCE PENDING,{0}".FormatWith(encounterExportStatementGroup.Select(x => x.AdjustmentInformation.InsuranceAmountPending).FirstOrDefault()));
                    if (encounterExportStatementGroup.Where(x => !string.IsNullOrEmpty(x.InvoiceNote)).Select(x => x.InvoiceNote).Any())
                    {
                        string invoice = encounterExportStatementGroup.Where(x => !string.IsNullOrEmpty(x.InvoiceNote)).Select(x => x.InvoiceNote).FirstOrDefault().ToUpper();
                        string[] inv = Regex.Split(invoice, "\r\n");
                        string strToRemove = "";
                        inv = inv.Where(val => val.Trim() != strToRemove).ToArray();
                        string InvoiceNote = string.Join("\r\n", inv);

                        sb.AppendLine("INV-NOTE,{0} {1}".FormatWith(encounterExportStatementGroup.Select(x => x.FormattedInvoice).FirstOrDefault(), InvoiceNote));
                    }
                }


                if (statementExportFormatId == (int)StatementExportFormat.InsurancePaymentsBrokenOutAndAging)
                {
                    sb.AppendLine("AGING BALANCE,{0}".FormatWith(patientExportStatementGroup
                        .SelectMany(x => x.AdjustmentInformation.AgingIntervals)
                        .GroupBy(x => x.Interval)
                        .Select(g => new { Interval = g.Key, SumOfInterval = g.Sum(h => h.Amount) })
                        .Select(x => x.SumOfInterval.TrimDecimalIfWholeNumber())
                        .Join(",")));
                }

                sb.AppendLine("CLOSING BALANCE,{0}".FormatWith(string.Join(",", patientExportStatementGroup.Sum(x => x.AdjustmentInformation.PatientBalance),
                    patientExportStatementGroup.SelectMany(x => x.AdjustmentInformation.AgingIntervals.Where(a => a.Amount > 0))
                        .OrderByDescending(a => a.Interval)
                        .Select(a => a.Interval)
                        .FirstOrDefault()
                        .ConvertTo<int>() - 1,
                    patientExportStatementGroup.Select(x => x.Patient.CanSendCollectionLetter).FirstOrDefault())));


                patientExportStatements.Add(patientExportStatementGroup.Key, sb.ToString());
            }

            results.ToArray().Except(validResults)
                .Select(x => x.Key.Encounter.PatientId)
                .ForEach(x =>
                {
                    if (!patientExportStatements.ContainsKey(x))
                    {
                        patientExportStatements.Add(x, null);
                    }

                });
            return patientExportStatements;
        }

        private static DateTime GetPatientBillDate(IEnumerable<BillingServiceTransaction> billingServiceTransaction)
        {
            var statuses = new[]
            {
                BillingServiceTransactionStatus.Queued,
                BillingServiceTransactionStatus.Sent,
                BillingServiceTransactionStatus.Wait,
                BillingServiceTransactionStatus.ClosedSent
            };

            var transactions = billingServiceTransaction.ToArray();
            //bug# 20269
            //var result = transactions.Min(x => x.BillingService.PatientBillDate);
            var result = transactions.Max(x => (x.DateTime.Date));
            //if (!result.HasValue)
            //{

            //    return result.GetValueOrDefault().Date;
            //}

            if (result != DateTime.MinValue)
            {
                return result;
            }
            if (transactions.Any(x => x.InvoiceReceivable.PatientInsuranceId == null && statuses.Contains(x.BillingServiceTransactionStatus)))
            {
                //bug# took max date instead of Min date
                return transactions.Where(x => x.InvoiceReceivable.PatientInsuranceId == null && statuses.Contains(x.BillingServiceTransactionStatus))
                    .Max(x => x.DateTime.Date);
            }

            return DateTime.Now.ToClientTime().Date;
        }

        private ExternalSystemMessage GenerateNewExternalSystemMessage(KeyValuePair<int, byte[]> individualReportPdf, DateTime date)
        {
            var externalMessage = new ExternalSystemMessage
            {
                Description = "AggregateStatement",
                Value = individualReportPdf.Value != null ? Convert.ToBase64String(individualReportPdf.Value) : string.Empty,
                ExternalSystemExternalSystemMessageTypeId = _externalSystemExternalSystemMessageTypeId.Value,
                ExternalSystemMessageProcessingStateId = (int)ExternalSystemMessageProcessingStateId.Processed,
                ProcessingAttemptCount = 0,
                CreatedBy = "GenerateStatementsViewService",
                CreatedDateTime = date,
                UpdatedDateTime = date
            };

            return externalMessage;
        }

        // private static BillingServiceTransaction GenerateNewBillingServiceTransaction(BillingServiceTransaction billingServiceTransaction, string externalSystemMessageValue)

        private static BillingServiceTransaction GenerateNewBillingServiceTransaction(BillingServiceTransaction billingServiceTransaction, bool isExport)
        {
            var bst = new BillingServiceTransaction
            {
                DateTime = DateTime.Now.ToClientTime(),
                AmountSent = billingServiceTransaction.AmountSent,
                ClaimFileNumber = billingServiceTransaction.ClaimFileNumber,
                BillingServiceTransactionStatus = BillingServiceTransactionStatus.Sent,
                //MethodSent = externalSystemMessageValue.IsNullOrEmpty() ? MethodSent.Electronic : MethodSent.Paper,
                MethodSent = isExport ? MethodSent.Electronic : MethodSent.Paper,
                BillingServiceId = billingServiceTransaction.BillingServiceId,
                InvoiceReceivableId = billingServiceTransaction.InvoiceReceivableId,
                ClaimFileReceiverId = billingServiceTransaction.ClaimFileReceiverId
            };

            return bst;
        }

        private static ExternalSystemMessagePracticeRepositoryEntity GenerateNewExternalSystemMessagePracticeRepositoryEntity(ExternalSystemMessage externalSystemMessage, Guid billingServiceTransactionId)
        {
            var externalSystemMessagePracticeRepositoryEntities = new ExternalSystemMessagePracticeRepositoryEntity
            {
                ExternalSystemMessage = externalSystemMessage,
                PracticeRepositoryEntityId = (int)PracticeRepositoryEntityId.BillingServiceTransaction,
                PracticeRepositoryEntityKey = billingServiceTransactionId.ToString()
            };

            return externalSystemMessagePracticeRepositoryEntities;
        }
    }
}
