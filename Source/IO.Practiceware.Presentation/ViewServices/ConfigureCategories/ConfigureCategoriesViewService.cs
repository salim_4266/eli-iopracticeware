﻿using IO.Practiceware.Model;
using IO.Practiceware.Model.Auditing;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.ConfigureCategories;
using IO.Practiceware.Presentation.ViewServices.ConfigureCategories;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows.Media;

[assembly: Component(typeof(ConfigureCategoriesViewService), typeof(IConfigureCategoriesViewService))]

namespace IO.Practiceware.Presentation.ViewServices.ConfigureCategories
{
    public interface IConfigureCategoriesViewService
    {
        ConfigureCategoriesInitializeData LoadInitializeData();

        IEnumerable<ArchivedAppointmentCategoryViewModel> LoadArchivedCategories();
        IEnumerable<AppointmentTypeViewModel> LoadPreviouslyAssociatedAppointmentTypes(ArchivedAppointmentCategoryViewModel archivedCategory);

        IEnumerable<NamedViewModel> LoadAssociatedTemplates(int appointmentCategoryId);

        /// <summary>
        /// Saves a new Appointment Category and returns the Id of the newly created Category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        int SaveNewAppointmentCategory(AppointmentCategoryViewModel category);
        void SaveAppointmentCategory(AppointmentCategoryViewModel category);
        void SaveAppointmentTypes(IList<AppointmentTypeViewModel> appointmentTypes);
        ArchivedAppointmentCategoryViewModel ArchiveAppointmentCategory(int categoryId);
        AppointmentCategoryViewModel RestoreAppointmentCategory(int categoryId);
    }

    [DataContract]
    public class ConfigureCategoriesInitializeData
    {
        [DataMember]
        [Dependency]
        public List<AppointmentCategoryViewModel> ExistingCategories { get; set; }

        [DataMember]
        [Dependency]
        public List<ArchivedAppointmentCategoryViewModel> ArchivedCategories { get; set; }

        [DataMember]
        [Dependency]
        public List<AppointmentTypeViewModel> AppointmentTypes { get; set; }
    }

    public class ConfigureCategoriesViewService : BaseViewService, IConfigureCategoriesViewService
    {        
        private readonly IAuditRepository _auditRepository;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;
        private readonly Func<AppointmentCategoryViewModel> _createCategoryViewModel;
        private readonly Func<AppointmentTypeViewModel> _createAppointmentTypeViewModel;
        private readonly Func<ArchivedAppointmentCategoryViewModel> _createArchivedAppointmentCategoryViewModel;

        public ConfigureCategoriesViewService( IAuditRepository auditRepository, IUnitOfWorkProvider unitOfWorkProvider,
                                              Func<AppointmentCategoryViewModel> createCategoryViewModel, Func<AppointmentTypeViewModel> createAppointmentTypeViewModel, Func<ArchivedAppointmentCategoryViewModel> createArchivedAppointmentCategoryViewModel)
        {
            _auditRepository = auditRepository;
            _unitOfWorkProvider = unitOfWorkProvider;
            _createCategoryViewModel = createCategoryViewModel;
            _createAppointmentTypeViewModel = createAppointmentTypeViewModel;
            _createArchivedAppointmentCategoryViewModel = createArchivedAppointmentCategoryViewModel;
        }

        public ConfigureCategoriesInitializeData LoadInitializeData()
        {
            IList<AppointmentCategoryViewModel> existingCategories = null;
            IList<ArchivedAppointmentCategoryViewModel> archivedCategories = null;
            IList<AppointmentType> databaseAppointmentTypes = null;

            var a1 = new Action(() => databaseAppointmentTypes = PracticeRepository.AppointmentTypes.Where(at => !at.IsArchived).Include(at => at.AppointmentCategory).ToArray());
            var a2 = new Action(() => existingCategories = LoadExistingCategories());
            var a3 = new Action(() => archivedCategories = LoadArchivedCategories().ToArray());
            new[] { a1, a2, a3 }.ForAllInParallel(a => a());

            var appointmentTypes = MapAppointmentTypes(databaseAppointmentTypes, existingCategories);
            return new ConfigureCategoriesInitializeData
                {
                    AppointmentTypes = appointmentTypes.ToList(),
                    ExistingCategories = existingCategories.ToList(),
                    ArchivedCategories = archivedCategories.ToList()
                };
        }

        public IEnumerable<NamedViewModel> LoadAssociatedTemplates(int appointmentCategoryId)
        {
            var databaseCategories = PracticeRepository.AppointmentCategories.Where(c => c.Id == appointmentCategoryId).Include(c => c.ScheduleTemplateBlockAppointmentCategories.Select(stbac => stbac.ScheduleTemplateBlock.ScheduleTemplate));
            var databaseTemplates = databaseCategories.SelectMany(c => c.ScheduleTemplateBlockAppointmentCategories).Select(stbac => stbac.ScheduleTemplateBlock.ScheduleTemplate).Distinct();
            return databaseTemplates.Select(t => new NamedViewModel { Id = t.Id, Name = t.Name }).ToList();
        }

        public IEnumerable<ArchivedAppointmentCategoryViewModel> LoadArchivedCategories()
        {
            var databaseCategories = PracticeRepository.AppointmentCategories.Where(c => c.IsArchived).ToList();
            return MapAppointmentCategories(databaseCategories, true).Cast<ArchivedAppointmentCategoryViewModel>();
        }

        public IEnumerable<AppointmentTypeViewModel> LoadPreviouslyAssociatedAppointmentTypes(ArchivedAppointmentCategoryViewModel archivedCategory)
        {
            var appointmentTypeIds = _auditRepository.AuditEntriesFor<AppointmentType>()
                .Where(ae => ae.ChangeType == AuditEntryChangeType.Update && ae.AuditEntryChanges.Any(ec => ec.ColumnName == "AppointmentCategoryId" && ec.OldValue == archivedCategory.Id.ToString()))
                .Select(ae => ae.KeyValueNumeric)
                .ToList();

            var result = new List<AppointmentTypeViewModel>();

            var databaseAppointmentTypes = PracticeRepository.AppointmentTypes.Where(at => !at.IsArchived).Include(at => at.AppointmentCategory).ToList();
            foreach (var appointmentType in databaseAppointmentTypes.Where(at => appointmentTypeIds.Contains(at.Id)))
            {
                var viewModel = _createAppointmentTypeViewModel();
                viewModel.Id = appointmentType.Id;
                viewModel.Name = appointmentType.Name;
                viewModel.AppointmentCategory = archivedCategory;

                result.Add(viewModel);
            }
            return result;
        }

        private IList<AppointmentCategoryViewModel> LoadExistingCategories()
        {
            var databaseCategories = PracticeRepository.AppointmentCategories.Where(c => !c.IsArchived).ToList();
            return MapAppointmentCategories(databaseCategories);
        }

        public void SaveAppointmentTypes(IList<AppointmentTypeViewModel> appointmentTypes)
        {
            var appointmentTypeIds = appointmentTypes.Select(at => at.Id);
            var databaseAppointmentTypes = PracticeRepository.AppointmentTypes.Where(at => appointmentTypeIds.Contains(at.Id)).ToList();
            foreach (var databaseAppointmentType in databaseAppointmentTypes)
            {
                var appointmentType = appointmentTypes.WithId(databaseAppointmentType.Id);
                if (appointmentType.AppointmentCategory == null)
                {
                    databaseAppointmentType.AppointmentCategoryId = null;
                }
                else
                {
                    databaseAppointmentType.AppointmentCategoryId = appointmentType.AppointmentCategory.Id;
                }
            }

            using (var unitOfWork = _unitOfWorkProvider.Create())
            {
                databaseAppointmentTypes.ForEach(PracticeRepository.Save);
                unitOfWork.AcceptChanges();
            }
        }

        public void SaveAppointmentCategory(AppointmentCategoryViewModel category)
        {
            var databaseCategory = PracticeRepository.AppointmentCategories.WithId(category.Id);

            databaseCategory.HexColor = ToHexColor(category.Color);
            databaseCategory.Description = category.Description;
            databaseCategory.IsArchived = false;
            databaseCategory.Name = category.Name;

            PracticeRepository.Save(databaseCategory);
        }

        public ArchivedAppointmentCategoryViewModel ArchiveAppointmentCategory(int categoryId)
        {
            var databaseCategory = PracticeRepository.AppointmentCategories.Where(ac => ac.Id == categoryId).
                                                                             Include(ac => ac.AppointmentTypes).
                                                                             Include(ac => ac.ScheduleTemplateBlockAppointmentCategories).
                                                                             FirstOrDefault();

            if (databaseCategory == null) throw new Exception("Could not find appointment category whose Id is {0}".FormatWith(categoryId));

            databaseCategory.IsArchived = true;

            var appointmentTypes = databaseCategory.AppointmentTypes.ToList();
            appointmentTypes.ForEach(at => at.AppointmentCategory = null);

            using (var unitOfWork = _unitOfWorkProvider.Create())
            {
                appointmentTypes.ForEach(PracticeRepository.Save);
                PracticeRepository.Save(databaseCategory);
                databaseCategory.ScheduleTemplateBlockAppointmentCategories.ToList().ForEach(PracticeRepository.Delete);
                unitOfWork.AcceptChanges();
            }

            return MapAppointmentCategory(databaseCategory, isArchived: true) as ArchivedAppointmentCategoryViewModel;
        }

        public AppointmentCategoryViewModel RestoreAppointmentCategory(int categoryId)
        {
            var databaseCategory = PracticeRepository.AppointmentCategories.First(ac => ac.Id == categoryId);

            databaseCategory.IsArchived = false;
            PracticeRepository.Save(databaseCategory);

            return MapAppointmentCategory(databaseCategory);
        }

        public int SaveNewAppointmentCategory(AppointmentCategoryViewModel category)
        {
            var databaseCategory = new AppointmentCategory();
            databaseCategory.HexColor = ToHexColor(category.Color);
            databaseCategory.Description = category.Description;
            databaseCategory.IsArchived = false;
            databaseCategory.Name = category.Name;

            PracticeRepository.Save(databaseCategory);
            int newId = databaseCategory.Id;

            return newId;
        }

        public static string ToHexColor(Color color)
        {
            return String.Format("#{0}{1}{2}",
                                 color.R.ToString("X2"),
                                 color.G.ToString("X2"),
                                 color.B.ToString("X2"));
        }

        private IEnumerable<AppointmentTypeViewModel> MapAppointmentTypes(IEnumerable<AppointmentType> appointmentTypes, IList<AppointmentCategoryViewModel> appointmentCategories)
        {
            var result = new List<AppointmentTypeViewModel>();
            foreach (var appointmentType in appointmentTypes)
            {
                var viewModel = _createAppointmentTypeViewModel();
                viewModel.AppointmentCategory = appointmentCategories.FirstOrDefault(c => appointmentType.AppointmentCategoryId == c.Id);
                viewModel.Id = appointmentType.Id;
                viewModel.Name = appointmentType.Name;
                result.Add(viewModel);
            }
            return result;
        }

        private IList<AppointmentCategoryViewModel> MapAppointmentCategories(IEnumerable<AppointmentCategory> categories, bool isArchived = false)
        {
            var result = new List<AppointmentCategoryViewModel>();

            categories.ForEachWithSinglePropertyChangedNotification(c => result.Add(MapAppointmentCategory(c, isArchived)));

            return result;
        }

        private AppointmentCategoryViewModel MapAppointmentCategory(AppointmentCategory databaseCategory, bool isArchived = false)
        {
            if (databaseCategory == null) return null;

            if (isArchived)
            {
                var viewModel = _createArchivedAppointmentCategoryViewModel();
                viewModel.Color = databaseCategory.Color;
                viewModel.Description = databaseCategory.Description;
                viewModel.Id = databaseCategory.Id;
                viewModel.Name = databaseCategory.Name;
                return viewModel;
            }
            else
            {
                var viewModel = _createCategoryViewModel();
                viewModel.Color = databaseCategory.Color;
                viewModel.Description = databaseCategory.Description;
                viewModel.Id = databaseCategory.Id;
                viewModel.Name = databaseCategory.Name;
                return viewModel;
            }
        }
    }
}
