﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Auditing;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.ViewServices.Alerts;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;

[assembly: Component(typeof(AlertViewService), typeof(AlertViewService))]
namespace IO.Practiceware.Presentation.ViewServices.Alerts
{
    public interface IAlertViewService
    {
        /// <summary>
        /// Load the alerts.
        /// </summary>
        /// <param name="loadArgument"></param>
        /// <returns></returns>
        NoteLoadInformation LoadAlerts(NoteLoadArguments loadArgument);

        /// <summary>
        /// Check if the alert exists.
        /// </summary>
        /// <param name="loadArgument"></param>
        /// <returns></returns>
        bool HasAlerts(NoteLoadArguments loadArgument);

        /// <summary>
        /// Delete all the alerts.
        /// </summary>
        /// <param name="alertIds"></param>
        void DeactivateAlerts(IList<int> alertIds);

        /// <summary>
        /// Update alerts.
        /// </summary>
        /// <param name="alerts"></param>
        void UpdateAlerts(IList<NoteViewModel> alerts);
    }
    public class AlertViewService : BaseViewService, IAlertViewService
    {
        private IMapper<Alert, NoteViewModel> _alertMapper;
        private IMapper<Screen, NamedViewModel> _screenMapper;
        private readonly IAuditRepository _auditRepository;
        public AlertViewService(IAuditRepository auditRepository)
        {
            InitMappers();
            _auditRepository = auditRepository;
        }

        private void InitMappers()
        {
            var mapperFactory = Mapper.Factory;
            _screenMapper = mapperFactory.CreateMapper<Screen, NamedViewModel>(item => new NamedViewModel
            {
                Name = item.Name,
                Id = item.Id
            });
            _alertMapper = mapperFactory.CreateMapper<Alert, NoteViewModel>(item => new NoteViewModel
            {
                AlertId = item.Id,
                Value = item.PatientFinancialComment != null ? item.PatientFinancialComment.Value : item.InvoiceComment != null ? item.InvoiceComment.Value : item.BillingServiceComment != null ? item.BillingServiceComment.Value : string.Empty,
                IsIncludedOnStatement = item.PatientFinancialComment != null ? item.PatientFinancialComment.IsIncludedOnStatement : item.InvoiceComment != null ? item.InvoiceComment.IncludeOnStatement : item.BillingServiceComment != null && item.BillingServiceComment.IsIncludedOnStatement,
                AlertableScreens = item.Screens.Select(x => _screenMapper.Map(x)).ToExtendedObservableCollection(),
                ExpirationDateTime = item.ExpirationDateTime,
                NoteType = item.PatientFinancialComment != null ? NoteType.PatientFinancialComment : item.InvoiceComment != null ? NoteType.InvoiceComment : item.BillingServiceComment != null ? NoteType.BillingServiceComment : (NoteType?)null,
                Id = item.PatientFinancialComment != null ? item.PatientFinancialCommentId : item.InvoiceComment != null ? item.InvoiceCommentId : item.BillingServiceComment != null ? (int)item.BillingServiceCommentId : (int?)null,
            });
        }

        public NoteLoadInformation LoadAlerts(NoteLoadArguments arguments)
        {
            var loadInformation = new NoteLoadInformation();

            var alerts = GetAlerts(arguments);
            PracticeRepository.AsQueryableFactory().Load(alerts,
                                                        x => x.PatientFinancialComment,
                                                        x => x.InvoiceComment,
                                                        x => x.BillingServiceComment,
                                                        x => x.Screens);

            var billingServiceCommentIds = alerts.Select(x => x.BillingServiceCommentId).ToArray();
            var patientFinancialCommentIds = alerts.Select(x => x.PatientFinancialCommentId).ToArray();
            var invoiceCommentIds = alerts.Select(x => x.InvoiceCommentId).ToArray();
            var notes = _alertMapper.MapAll(alerts).ToObservableCollection();

            if (billingServiceCommentIds.Count() > 0)
            {
                notes = PopulateAuditEntry<BillingServiceComment>(notes, billingServiceCommentIds);
            }
            if (patientFinancialCommentIds.Count() > 0)
            {
                notes = PopulateAuditEntry<PatientFinancialComment>(notes, patientFinancialCommentIds);
            }
            if (invoiceCommentIds.Count() > 0)
            {
                notes = PopulateAuditEntry<InvoiceComment>(notes, invoiceCommentIds);
            }

            loadInformation.Notes = notes.ToExtendedObservableCollection();

            new Action[]
            {
                () => {
                    loadInformation.AlertableScreens = PracticeRepository.Screens.Where(x => x.ScreenType == ScreenType.Financials).Select(s => new NamedViewModel { Id = s.Id, Name = s.Name }).ToExtendedObservableCollection();
                }

            }.ForAllInParallel(a => a());
            return loadInformation;
        }

        public bool HasAlerts(NoteLoadArguments arguments)
        {
            var alerts = GetAlerts(arguments);
            return alerts.Any();
        }

        private List<Alert> GetAlerts(NoteLoadArguments arguments)
        {
            int? invoiceId = arguments.NoteType == NoteType.InvoiceComment ? arguments.EntityId.GetValueOrDefault().EnsureNotDefault() : (int?)null;
            int? billingServiceId = arguments.NoteType == NoteType.BillingServiceComment ? arguments.EntityId.GetValueOrDefault().EnsureNotDefault() : (int?)null;
            int? patientId = arguments.NoteType == NoteType.PatientFinancialComment ? arguments.EntityId.GetValueOrDefault().EnsureNotDefault() : invoiceId.HasValue ? PracticeRepository.Invoices.Where(x => x.Id == invoiceId).Select(x => x.Encounter.PatientId).FirstOrDefault() : (int?)null;
            List<Alert> alerts;
            if (arguments.NoteType == NoteType.InvoiceComment)
            {
                alerts = PracticeRepository.Alerts.Where(x => (x.PatientFinancialComment.PatientId == patientId ||
                                                               (x.BillingServiceComment != null && (x.BillingServiceComment.BillingServiceId == billingServiceId || x.BillingServiceComment.BillingService.InvoiceId == invoiceId)) ||
                                                               (x.InvoiceComment != null && (x.InvoiceComment.InvoiceId == invoiceId))) &&
                                                                x.Screens.Any(s => s.Id == (int?)arguments.AlertScreen) &&
                                                                (!x.ExpirationDateTime.HasValue || x.ExpirationDateTime >= DateTime.Today.ToClientTime().Date)).ToList();
            }
            else if (arguments.NoteType == NoteType.PatientFinancialComment)
            {
                alerts = PracticeRepository.Alerts.Where(x => (x.PatientFinancialComment.PatientId == patientId ||
                                                       (x.BillingServiceComment != null && (x.BillingServiceComment.BillingServiceId == billingServiceId || x.BillingServiceComment.BillingService.Invoice.Encounter.PatientId == patientId)) ||
                                                       (x.InvoiceComment != null && (x.InvoiceComment.InvoiceId == invoiceId || x.InvoiceComment.Invoice.Encounter.PatientId == patientId))) &&
                                                       x.Screens.Any(s => s.Id == (int?)arguments.AlertScreen) &&
                                                       (!x.ExpirationDateTime.HasValue || x.ExpirationDateTime >= DateTime.Today.ToClientTime().Date)).ToList();
            }
            else
            {
                throw new NotSupportedException();
            }

            return alerts;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void DeactivateAlerts(IList<int> alertIds)
        {
            PracticeRepository.Alerts.Include(x => x.Screens).Where(x => alertIds.Contains(x.Id)).ForEach(x =>
            {
                x.Screens.Clear();
                PracticeRepository.Delete(x);
            });
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void UpdateAlerts(IList<NoteViewModel> alerts)
        {
            var screens = PracticeRepository.Screens.ToList();
            var alertIds = alerts.Select(x => x.AlertId).ToArray();
            var dbAlerts = PracticeRepository.Alerts.Where(x => alertIds.Contains(x.Id)).ToArray();

            PracticeRepository.AsQueryableFactory().Load(dbAlerts,
                                                       x => x.PatientFinancialComment,
                                                       x => x.InvoiceComment,
                                                       x => x.BillingServiceComment,
                                                       x => x.Screens);
            dbAlerts.ForEach(x =>
            {
                var alert = alerts.Where(a => x.Id == a.AlertId).Select(a => a).FirstOrDefault().EnsureNotDefault();

                //Updating comment entity under the alert.
                if (x.PatientFinancialCommentId.HasValue)
                {
                    x.PatientFinancialComment.Value = alert.Value;
                    x.PatientFinancialComment.IsIncludedOnStatement = alert.IsIncludedOnStatement;
                }
                else if (x.InvoiceCommentId.HasValue)
                {
                    x.InvoiceComment.Value = alert.Value;
                    x.InvoiceComment.IncludeOnStatement = alert.IsIncludedOnStatement;
                }
                else if (x.BillingServiceCommentId.HasValue)
                {
                    x.BillingServiceComment.Value = alert.Value;
                    x.BillingServiceComment.IsIncludedOnStatement = alert.IsIncludedOnStatement;
                }

                //If screens are present, it means the alert has some screen changes else need to delete the alert.
                if (alert.AlertableScreens.Count > 0)
                {
                    x.Screens.ToList().ForEach(s => x.Screens.Remove(s));
                    alert.AlertableScreens.ForEach(asc => x.Screens.Add(screens.WithId(asc.Id)));
                    x.ExpirationDateTime = alert.ExpirationDateTime;
                }
                else
                {
                    x.Screens.Clear();
                    PracticeRepository.Delete(x);
                }
            });
        }

        /// <summary>
        /// Populate the audit entry.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="notes"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private ObservableCollection<NoteViewModel> PopulateAuditEntry<TEntity>(IEnumerable<NoteViewModel> notes, IEnumerable ids)
        {
            var commentsAuditEntries = _auditRepository.AuditEntriesFor<TEntity>(ids).Include(i => i.AuditEntryChanges).ToArray();
            var userIds = commentsAuditEntries.Where(x => x.UserId.HasValue).Select(x => x.UserId).ToArray();
            var users = PracticeRepository.Users.Where(x => userIds.Contains(x.Id)).ToArray();

            var result = notes.ForEach(x =>
            {
                var commentAuditEntry = commentsAuditEntries.FirstOrDefault(a => a.KeyValues.Contains(x.Id.ToString()));
                if (commentAuditEntry != null)
                {
                    x.CreatedByUser = users.Where(u => commentAuditEntry.UserId.HasValue && u.Id == commentAuditEntry.UserId).Select(u => new NamedViewModel { Name = u.UserName }).FirstOrDefault();
                    x.CreatedOnDateTime = commentAuditEntry.AuditDateTime.ToClientTime();
                }
            }).ToObservableCollection();

            return result;
        }

    }
}
