﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Referrals;
using IO.Practiceware.Presentation.ViewServices.Referrals;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;


[assembly: Component(typeof(ReferralsViewService), typeof(IReferralsViewService))]
[assembly: Component(typeof(ReferralDetailsViewModelMap), typeof(IMap<PatientInsuranceReferral, ReferralDetailsViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.Referrals
{
    public class ReferralsViewService : BaseViewService, IReferralsViewService
    {
        private readonly IMapper<List<PatientInsuranceReferral>, List<ReferralDetailsViewModel>> _mapper;

        public ReferralsViewService(IMapper<List<PatientInsuranceReferral>, List<ReferralDetailsViewModel>> mapper)
        {
            _mapper = mapper;
        }


        public IEnumerable<ReferralDetailsViewModel> LoadReferralDetailsByReferralId(long referralId, DateTime appointmentDate)
        {
            var patientId = PracticeRepository.PatientInsuranceReferrals.Single(x => x.Id == referralId).PatientId;

            return LoadReferralDetailsByPatientId(patientId, appointmentDate);
        }

        public IEnumerable<ReferralDetailsViewModel> LoadReferralDetailsByPatientId(int patientId, DateTime appointmentDate)
        {
            var patient = PracticeRepository.Patients.Single(x => x.Id == patientId);

            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(patient,
                                  x => x.PatientInsuranceReferrals.Select(y => y.Encounters),
                                  x => x.PatientInsurances.Select(y => y.InsurancePolicy.Insurer),
                                  x => x.ExternalProviders.Select(y => y.ExternalProvider));


            var viewModels = _mapper.Map(patient.PatientInsuranceReferrals.ToList());

            foreach (var model in viewModels)
            {
                // In this case the Insurance Id we're getting back is actually the Patient Insurance Id as you can see below in MapReferralDetailViewModelToPatientInsuranceReferral
                ReferralDetailsViewModel referralDetailsViewModel = model;
                model.Insurances = new ExtendedObservableCollection<NameAndAbbreviationViewModel>(patient.PatientInsurances.Where(x => referralDetailsViewModel.InsuranceId == x.Id)
                    .Select(x => new NameAndAbbreviationViewModel
                    {
                        Id = x.Id,
                        Name = x.InsurancePolicy.Insurer.Name,
                        Abbreviation = x.InsurancePolicy.Insurer.Name + "(" + x.InsurancePolicy.StartDateTime.Date.ToString("d") + " - " + (x.EndDateTime.HasValue ? x.EndDateTime.Value.Date.ToString("d") : String.Empty) + ")"
                    }));
            }

            return viewModels;
        }

        public ObservableCollection<NameAndAbbreviationViewModel> LoadActiveInsurancesForPatient(int patientId, DateTime appointmentDate)
        {
            var patient = PracticeRepository.Patients.Single(x => x.Id == patientId);

            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(patient,
                                  x => x.PatientInsurances.Select(y => y.InsurancePolicy.Insurer));

            if (patient.PatientInsurances.Any())
            {
                return patient.PatientInsurances
                    .OrderByDescending(x => x.IsActiveForDateTime(appointmentDate))
                    .Select(x => new NameAndAbbreviationViewModel
                    {
                        Id = x.Id,
                        Name = x.InsurancePolicy.Insurer.Name,
                        Abbreviation = x.InsurancePolicy.Insurer.Name + "(" + x.InsurancePolicy.StartDateTime.Date.ToString("d") + " - " + (x.EndDateTime.HasValue ? x.EndDateTime.Value.Date.ToString("d") : String.Empty) + ")"
                    })
                    .ToExtendedObservableCollection();
            }


            return new ExtendedObservableCollection<NameAndAbbreviationViewModel>();
        }

        public ObservableCollection<NamedViewModel<int>> LoadExternalProviders()
        {
            var externalProviders = PracticeRepository.ExternalContacts.OfType<ExternalProvider>().Where(x => !x.IsArchived && x.ExcludeOnClaim == false);

            if (externalProviders.Any())
            {
                // Not returning ExtendedObservableCollection since this list can be really large and it slows down performance. 
                return new ObservableCollection<NamedViewModel<int>>(externalProviders.Select(x => new NamedViewModel<int> { Id = x.Id, Name = x.DisplayName }).Distinct().OrderBy(x => x.Name));
            }

            return new ObservableCollection<NamedViewModel<int>>();
        }

        public ObservableCollection<NamedViewModel<int>> LoadDoctors()
        {
            // get all the practice's doctors
            var doctors = PracticeRepository.Users.OfType<Doctor>().Where(x => !x.IsArchived).OrderBy(x => x.UserName);

            if (doctors.Any())
            {
                return doctors.Select(x => new NamedViewModel<int> { Id = x.Id, Name = x.UserName }).ToExtendedObservableCollection();
            }

            return new ExtendedObservableCollection<NamedViewModel<int>>();
        }

        public string GetPatientDisplayName(int patientId)
        {
            return PracticeRepository.Patients.Single(x => x.Id == patientId).DisplayName;
        }

        public int Save(ReferralDetailsViewModel viewModel)
        {
            PatientInsuranceReferral referral;
            if (viewModel.ReferralId.HasValue)
            {
                referral = PracticeRepository.PatientInsuranceReferrals
                    .WithId(viewModel.ReferralId);
            }
            else
            {
                referral = new PatientInsuranceReferral();
            }

            MapReferralDetailViewModelToPatientInsuranceReferral(viewModel, referral);
            PracticeRepository.Save(referral);

            return referral.Id;
        }

        public bool CheckForInvalidReferralStartDate(ReferralDetailsViewModel viewModel)
        {
            if (viewModel.InsuranceId.HasValue)
            {
                PatientInsurance insurance = PracticeRepository.PatientInsurances.Include(x => x.InsurancePolicy).WithId(viewModel.InsuranceId);
                if (insurance.InsurancePolicy.StartDateTime <= viewModel.StartDate && (insurance.EndDateTime >= viewModel.StartDate || insurance.EndDateTime == null))
                    return false;
                return true;
            }
            return false;
        }

        public bool CheckForInvalidReferralExpireDate(ReferralDetailsViewModel viewModel)
        {
            if (viewModel.InsuranceId.HasValue)
            {
                PatientInsurance insurance = PracticeRepository.PatientInsurances.Include(x => x.InsurancePolicy).WithId(viewModel.InsuranceId);
                if (insurance.InsurancePolicy.StartDateTime <= viewModel.ExpiresDate && (insurance.EndDateTime >= viewModel.ExpiresDate || insurance.EndDateTime == null))
                    return false;
                return true;
            }
            return false;
        }

        private static void MapReferralDetailViewModelToPatientInsuranceReferral(ReferralDetailsViewModel source, PatientInsuranceReferral destination)
        {
            destination.ReferralCode = source.ReferralNumber;
            destination.PatientInsuranceId = source.InsuranceId.HasValue ? source.InsuranceId.Value : 0;
            destination.ExternalProviderId = source.ExternalProviderId.HasValue ? source.ExternalProviderId.Value : 0;
            destination.DoctorId = source.ReferredToDoctorId.HasValue ? source.ReferredToDoctorId.Value : 0;
            destination.StartDateTime = source.StartDate;
            destination.EndDateTime = source.ExpiresDate;
            destination.TotalEncountersCovered = source.TotalNumberOfVisits;
            destination.PatientId = source.PatientId;
            destination.Comment = source.Comments;
        }
    }

    public interface IReferralsViewService
    {
        /// <summary>
        /// Returns a list of referrals by getting the patient's referral history attached to this referralId. Appointment Date specifies which insurances to include in the list of insurances
        /// </summary>
        /// <param name="referralId"></param>
        /// <param name="appointmentDate"></param>
        /// <returns></returns>
        IEnumerable<ReferralDetailsViewModel> LoadReferralDetailsByReferralId(long referralId, DateTime appointmentDate);

        /// <summary>
        /// Returns a list of referral history for this patient.  Appointment Date specifies which insurances to include in the list of insurances
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="appointmentDate"></param>
        /// <returns></returns>
        IEnumerable<ReferralDetailsViewModel> LoadReferralDetailsByPatientId(int patientId, DateTime appointmentDate);

        /// <summary>
        /// Returns a list of insurances for a patient active on the appointment date
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="appointmentDate"></param>
        /// <returns></returns>
        ObservableCollection<NameAndAbbreviationViewModel> LoadActiveInsurancesForPatient(int patientId, DateTime appointmentDate);

        /// <summary>
        /// Returns a list of external providers (doctors) for a patient
        /// </summary>
        /// <returns></returns>
        ObservableCollection<NamedViewModel<int>> LoadExternalProviders();

        /// <summary>
        /// Returns a list of all doctor's for this practice
        /// </summary>
        /// <returns></returns>
        ObservableCollection<NamedViewModel<int>> LoadDoctors();

        /// <summary>
        /// Gets the patients display name
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        string GetPatientDisplayName(int patientId);

        /// <summary>
        /// Maps the view model data to a Patient Insurance Referral and saves it to the data store
        /// </summary>
        /// <param name="viewModel"></param>
        int Save(ReferralDetailsViewModel viewModel);

        /// <summary>
        /// Checks whether the start date is within the insurance coverage dates or not
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        bool CheckForInvalidReferralStartDate(ReferralDetailsViewModel viewModel);

        /// <summary>
        /// Checks whether the expire date is within the insurance coverage dates or not
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        bool CheckForInvalidReferralExpireDate(ReferralDetailsViewModel viewModel);
    }

    public class ReferralDetailsViewModelMap : IMap<PatientInsuranceReferral, ReferralDetailsViewModel>
    {
        private readonly IEnumerable<IMapMember<PatientInsuranceReferral, ReferralDetailsViewModel>> _members;

        public ReferralDetailsViewModelMap()
        {
            _members = this.CreateMembers(referral => new ReferralDetailsViewModel
            {
                PatientId = referral.PatientId,
                ReferralId = referral.Id,
                PatientName = referral.Patient.DisplayName,
                ReferralNumber = referral.ReferralCode,
                InsuranceId = referral.PatientInsuranceId,
                ExpiresDate = referral.EndDateTime,
                StartDate = referral.StartDateTime,
                ExternalProviderId = referral.ExternalProviderId,
                ReferredToDoctorId = referral.DoctorId,
                TotalNumberOfVisits = referral.TotalEncountersCovered,
                AlreadyVisited = referral.Encounters.Count(x => !(x.EncounterStatus.IsCancelledStatus())), // only count encounters that are not cancelled
                Comments = referral.Comment
            }).ToArray();
        }

        public IEnumerable<IMapMember<PatientInsuranceReferral, ReferralDetailsViewModel>> Members
        {
            get { return _members; }
        }
    }
}
