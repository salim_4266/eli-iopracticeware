﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Utilities.Merge;
using IO.Practiceware.Presentation.ViewServices.Utilities.Merge;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;

[assembly:Component(typeof(MergeEntitiesViewService), typeof(IMergeEntitiesViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Utilities.Merge
{
    public interface IMergeEntitiesViewService
    {
        /// <summary>
        /// Finds entities to merge matching specified search text.
        /// </summary>
        ICollection<EntityToMergeViewModel> Find(string searchText, MergeEntityType entityType);

        /// <summary>
        /// Merges specified entities.
        /// </summary>
        /// <param name="toUse">To use.</param>
        /// <param name="toOverwrite">To overwrite.</param>
        void Merge(EntityToMergeViewModel toUse, EntityToMergeViewModel toOverwrite);
    }

    public class MergeEntitiesViewService : BaseViewService, IMergeEntitiesViewService
    {
        private static IMapper<Insurer, EntityToMergeViewModel> _insurerAsMergeEntityMapper;
        private static IMapper<ExternalProvider, EntityToMergeViewModel> _providerAsMergeEntityMapper;

        static MergeEntitiesViewService()
        {
            InitializeMappers();
        }

        public ICollection<EntityToMergeViewModel> Find(string searchText, MergeEntityType entityType)
        {
            switch (entityType)
            {
                case MergeEntityType.Insurer:
                    return FindInsurers(searchText);
                case MergeEntityType.Provider:
                    return FindProviders(searchText);
                default:
                    throw new ArgumentOutOfRangeException("entityType");
            }
        }

        public void Merge(EntityToMergeViewModel toUse, EntityToMergeViewModel toOverwrite)
        {
            if (toUse.EntityType != toOverwrite.EntityType)
            {
                throw new ArgumentException("Merge entities must of the same type");
            }
            if (toUse.Id == toOverwrite.Id)
            {
                throw new ArgumentException("Merge entities must have different ids");
            }

            var parameters = new Dictionary<string, object>();
            string storedProcedureName;

            switch (toOverwrite.EntityType)
            {
                case MergeEntityType.Insurer:
                    storedProcedureName = "[model].[MergeInsurers]";
                    parameters.Add("@InsurerToUseId", toUse.Id);
                    parameters.Add("@InsurerToOverwriteId", toOverwrite.Id);
                    break;
                case MergeEntityType.Provider:
                    storedProcedureName = "[model].[MergeProviders]";
                    parameters.Add("@ProviderToUseId", toUse.Id);
                    parameters.Add("@ProviderToOverwriteId", toOverwrite.Id);
                    break;
                default:
                    throw new ArgumentException("Unsupported entity type: " + toOverwrite.EntityType);
            }

            // Run stored procedure to merge entities
            using (var connection = DbConnectionFactory.PracticeRepository)
            {
                connection.Execute(storedProcedureName, parameters, false, () =>
                {
                    var c = connection.CreateCommand();
                    c.CommandTimeout = connection.ConnectionTimeout;
                    c.CommandType = CommandType.StoredProcedure;
                    return c;
                });
            }
        }

        private ICollection<EntityToMergeViewModel> FindProviders(string searchText)
        {
            var matchingInsurers = PracticeRepository.ExternalContacts
                .OfType<ExternalProvider>()
                .Where(i => i.LastNameOrEntityName.Contains(searchText))
                .Include(i => i.ExternalContactAddresses.Select(a => a.StateOrProvince.Country))
                .OrderBy(i => i.LastNameOrEntityName)
                .ToList()
                .Select(_providerAsMergeEntityMapper.Map)
                .ToList();

            return matchingInsurers;
        }

        private ICollection<EntityToMergeViewModel> FindInsurers(string searchText)
        {
            var matchingInsurers = PracticeRepository.Insurers
                .Where(i => i.Name.Contains(searchText) || i.PlanName.Contains(searchText))
                .Include(i => i.InsurerAddresses.Select(a => a.StateOrProvince.Country))
                .OrderBy(i => i.Name)
                .ToList()
                .Select(_insurerAsMergeEntityMapper.Map)
                .ToList();

            return matchingInsurers;
        }

        private static void InitializeMappers()
        {
            var mapperFactory = Mapper.Factory;

            _insurerAsMergeEntityMapper = mapperFactory.CreateMapper<Insurer, EntityToMergeViewModel>(model => new EntityToMergeViewModel
            {
                Id = model.Id,
                EntityType = MergeEntityType.Insurer,
                DisplayName = string.IsNullOrEmpty(model.PlanName) 
                    ? model.Name 
                    : string.Format("{0} ({1})", model.Name, model.PlanName),
                Details = model.InsurerAddresses
                    .OrderBy(a => a.InsurerAddressTypeId == (int)InsurerAddressTypeId.Claims)
                    .FirstOrDefault()
                    .IfNotNull(a => new []
                        {
                            a.Line1, a.Line2, a.Line3, string.Format("{0}, {1} {2}", a.City, a.StateOrProvince.Name, a.PostalCode)
                        }.Join(Environment.NewLine, true), "Address not documented")
            });

            _providerAsMergeEntityMapper = mapperFactory.CreateMapper<ExternalProvider, EntityToMergeViewModel>(model => new EntityToMergeViewModel
            {
                Id = model.Id,
                EntityType = MergeEntityType.Provider,
                DisplayName = model.GetFormattedName(),
                Details = model.ExternalContactAddresses
                    .OrderBy(a => a.ContactAddressTypeId == (int)ExternalContactAddressTypeId.MainOffice)
                    .FirstOrDefault()
                    .IfNotNull(a => new[]
                        {
                            a.Line1, a.Line2, a.Line3, string.Format("{0}, {1} {2}", a.City, a.StateOrProvince.Name, a.PostalCode)
                        }.Join(Environment.NewLine, true), "Address not documented")
            });
        }
    }
}
