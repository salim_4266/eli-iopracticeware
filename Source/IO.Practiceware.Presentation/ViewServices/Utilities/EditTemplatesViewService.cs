﻿using IO.Practiceware.Integration.PaperClaims;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Utilities;
using IO.Practiceware.Presentation.ViewServices.Utilities;
using Soaf.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;

[assembly: Component(typeof(EditTemplatesViewService), typeof(IEditTemplatesViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Utilities
{
    public interface IEditTemplatesViewService
    {
        EditTemplatesViewModel GetTemplate(int documentId);
        void SaveTemplate(int documentId, string templateContent);
        List<NamedViewModel> GetListOfTemplates();
        IEnumerable<string> ProduceMessageTest(string template, ContentType contentType);
        void ResetTemplateBackToOriginal(int documentId);
    }

    public class EditTemplatesViewService : BaseViewService, IEditTemplatesViewService
    {
        private readonly Func<EditTemplatesViewModel> _createEditTemplateViewModel;
        private readonly PaperClaimsMessageProducer _messageProducer;

        public EditTemplatesViewService(Func<EditTemplatesViewModel> createEditTemplateViewModel, PaperClaimsMessageProducer messageProducer)
        {
            _createEditTemplateViewModel = createEditTemplateViewModel;
            _messageProducer = messageProducer;
        }

        public EditTemplatesViewModel GetTemplate(int documentId)
        {
            var document = PracticeRepository.TemplateDocuments.SingleOrDefault(x => x.Id == documentId);

            if (document != null)
            {
                var viewModel = _createEditTemplateViewModel();

                viewModel.TemplateDocumentId = document.Id;
                viewModel.DocumentName = document.DisplayName;
                viewModel.DocumentText = (string)document.GetContent();
                viewModel.ContentType = document.ContentType;

                return viewModel;
            }

            return null;
        }

        public void SaveTemplate(int documentId, string templateContent)
        {
            var document = PracticeRepository.TemplateDocuments.SingleOrDefault(x => x.Id == documentId);

            if (document != null)
            {
                document.SetContent(templateContent);

                PracticeRepository.Save(document);
            }
        }

        public List<NamedViewModel> GetListOfTemplates()
        {
            var documents = PracticeRepository.TemplateDocuments.Where(x => x.IsEditable);

            var list = documents.Select(x => new NamedViewModel {Id = x.Id, Name = x.Name}).ToList();

            return list;
        }

        public IEnumerable<string> ProduceMessageTest(string template, ContentType contentType)
        {
            if (contentType != ContentType.RazorTemplate) return null;

            var viewModel = CreateTestPaperClaim();

            var message = (Services.Documents.HtmlDocument)_messageProducer.ProduceMessage(viewModel, template, contentType);

            return message.Content;
        }

        public void ResetTemplateBackToOriginal(int documentId)
        {
            var document = PracticeRepository.TemplateDocuments.SingleOrDefault(x => x.Id == documentId);

            if (document != null && document.IsEditable)
            {
                document.SetContent(null); // by setting the Content to null, the behavior is to now grab the ContentOriginal field when accessing content

                PracticeRepository.Save(document);
            }
        }

        public static PaperClaim CreateTestPaperClaim()
        {
            var paperClaimInfo = new PaperClaim();

            paperClaimInfo.BillingProviderName = "Test Billing Provider";
            paperClaimInfo.BillingProviderAddressLine1 = "6 Tulip Place";
            paperClaimInfo.BillingProviderAddressLine2 = "Suite 500";
            paperClaimInfo.BillingProviderCity = "Howard Beach";
            paperClaimInfo.BillingProviderState = "NY";
            paperClaimInfo.BillingProviderZip = "11414";
            paperClaimInfo.BillingProviderTaxId = "TaxId555";
            paperClaimInfo.PatientId = "555";
            paperClaimInfo.PatientFirstName = "TestFirstName";
            paperClaimInfo.PatientLastName = "TestLastName";
            paperClaimInfo.PatientBirthDate = new DateTime(1982, 1, 7);

            paperClaimInfo.PatientAddressLine1 = "Patient Address";
            paperClaimInfo.PatientAddressLine2 = "Suite 500";
            paperClaimInfo.PatientCity = "Patient City";
            paperClaimInfo.PatientState = "NY";
            paperClaimInfo.PatientZip = "55555";
            paperClaimInfo.AccidentState = "NY";
            paperClaimInfo.BilledPolicyHolderFirstName = "TestGuarantorFirstName";
            paperClaimInfo.BilledPolicyHolderLastName = "TestGuarantorLastName";
            paperClaimInfo.BilledPolicyHolderAddressLine1 = "45-23 198 avenue";
            paperClaimInfo.BilledPolicyHolderAddressLine2 = "2ND Floor";
            paperClaimInfo.BilledPolicyHolderCity = "Ozone Park";
            paperClaimInfo.BilledPolicyHolderState = "NY";
            paperClaimInfo.BilledPolicyHolderZip = "55555";

            paperClaimInfo.TotalCharges = 1500.00m;
            paperClaimInfo.BillingProviderNpi = "TestBillingProviderNpi";
            paperClaimInfo.BilledInsuranceName = "TestBilledINsuredName";
            paperClaimInfo.BilledInsuranceAddressLine1 = "Address 1";
            paperClaimInfo.BilledInsuranceAddressLine2 = "Address 2";
            paperClaimInfo.BilledInsuranceAddressLine3 = "Address 3";
            paperClaimInfo.BilledInsuranceCity = "Test Billed INsurance City";
            paperClaimInfo.BilledInsuranceState = "Test Billed Insurance State";
            paperClaimInfo.BilledInsuranceZip = "Test Billed Insurance Zip";
            paperClaimInfo.BillingProviderTaxonomy = "Test BillingProviderTaxonomy";

            paperClaimInfo.RenderingProviderNpi = "TestAttendingDoctorNPI";

            paperClaimInfo.InsuranceTypeIsMedicare = true;
            paperClaimInfo.InsuranceTypeIsMedicaid = true;
            paperClaimInfo.InsuranceTypeIsTricare = true;
            paperClaimInfo.InsuranceTypeIsGroup = true;
            paperClaimInfo.InsuranceTypeIsOther = true;

            paperClaimInfo.BilledPolicyHolderPolicyNumber = "5555555";
            paperClaimInfo.BilledPolicyHolderMiddleName = "Middle";
            paperClaimInfo.BilledPolicyHolderTelephone = "555-555-5555";
            paperClaimInfo.BilledPolicyHolderGroupId = "58002";
            paperClaimInfo.BilledPolicyHolderBirthDate = new DateTime(1972, 3, 23);
            paperClaimInfo.BilledPolicyHolderGender = Gender.Male;
            paperClaimInfo.BilledPolicyHolderEmployerName = "Test Employer";

            paperClaimInfo.BilledInsurancePlanName = "Test Insurance Company";
            paperClaimInfo.AnotherPlan = true;
            paperClaimInfo.AssignBenefitsSignature = "SIGNATURE ON FILE";
            paperClaimInfo.ReleaseInfoSignature = "RELEASE INFO";

            paperClaimInfo.PatientMiddleName = "MiddlName";
            paperClaimInfo.PatientGender = Gender.Male;
            paperClaimInfo.PatientTelephone = "555-555-5555";
            paperClaimInfo.BilledRelationshipSelf = true;
            paperClaimInfo.BilledRelationshipSpouse = true;
            paperClaimInfo.BilledRelationshipChild = true;
            paperClaimInfo.BilledRelationshipOther = true;
            paperClaimInfo.MaritalStatusSingle = true;
            paperClaimInfo.MaritalStatusMarried = true;
            paperClaimInfo.MaritalStatusOther = true;
            paperClaimInfo.OtherPolicyHolderPolicyNumber = "5532j";
            paperClaimInfo.OtherPolicyHolderLastName = "OtherLast";
            paperClaimInfo.OtherPolicyHolderFirstName = "OtherFirst";
            paperClaimInfo.OtherPolicyHolderMiddleName = "OtherM";
            paperClaimInfo.OtherPolicyHolderAddressLine1 = "Other Address Line1";
            paperClaimInfo.OtherPolicyHolderAddressLine2 = "Other Address Line2";
            paperClaimInfo.OtherPolicyHolderCity = "OtherCity";
            paperClaimInfo.OtherPolicyHolderState = "NY";
            paperClaimInfo.OtherPolicyHolderZip = "55555";
            paperClaimInfo.OtherPolicyHolderTelephone = "555-555-5555";
            paperClaimInfo.OtherPolicyHolderGroupId = "5555";
            paperClaimInfo.OtherPolicyHolderBirthDate = new DateTime(1979, 4, 24);
            paperClaimInfo.OtherPolicyHolderGender = Gender.Female;
            paperClaimInfo.OtherPolicyHolderEmployerName = "Test Employer";
            paperClaimInfo.OtherInsuranceName = "Other Insurance";
            paperClaimInfo.EmploymentRelated = true;

            paperClaimInfo.IllnessDate = new DateTime(2009, 1, 2);
            paperClaimInfo.AdmitDate = new DateTime(2009, 1, 2);
            paperClaimInfo.DischargeDate = new DateTime(2009, 1, 2);
            paperClaimInfo.ResubmitCode = "ResubmitCode";
            paperClaimInfo.AuthorizationCode = "AuthCode";

            paperClaimInfo.ReferringDoctorFullName = "Ref Doc Fullname";
            paperClaimInfo.ReferringDoctorNpi = "Ref Doc Npi";

            paperClaimInfo.ServiceNote = "Service Note";

            paperClaimInfo.BillingProviderTaxId = "TaxId123";
            paperClaimInfo.TotalPaid = "1239.34";
            paperClaimInfo.BalanceDue = "234.23";


            paperClaimInfo.LocationName = "Location Name";
            paperClaimInfo.LocationAddressLine1 = "Loc Address Line1";
            paperClaimInfo.LocationAddressLine2 = "Loc Address Line2";
            paperClaimInfo.LocationCity = "LocCity";
            paperClaimInfo.LocationState = "NY";
            paperClaimInfo.LocationZip = "55555";
            paperClaimInfo.LocationNpi = "AAAEEFE";

            paperClaimInfo.Diagnoses = new List<DiagnosisInfo>
                                      {
                                          new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 1
                                              },
                                          new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 2
                                              },
                                          new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 3
                                              },
                                          new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 4
                                              },
                                               new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 5
                                              },
                                               new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 6
                                              },
                                               new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 7
                                              },
                                               new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 8
                                              },
                                               new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 9
                                              },
                                               new DiagnosisInfo
                                              {
                                                  Diagnosis = "0392.3234",
                                                  Id = 10
                                              },
                                      };

            paperClaimInfo.Services = new List<PaperClaimServiceInfo>
                                     {
                                         new PaperClaimServiceInfo
                                             {
                                                 ServiceDate = new DateTime(2009, 1, 7),
                                                 ServiceCode = "09",
                                                 ServiceLocationCode = "LLP",
                                                 ServiceCharge = 25.95m,
                                                 ServiceUnits = "45",
                                                DiagnosisLetters ="AC",
                                                 ServiceUnitOfMeasurement = ServiceUnitOfMeasurement.Minute,
                                                 Parent = paperClaimInfo
                                             },
                                         new PaperClaimServiceInfo
                                             {
                                                 IsAnesthesia = true,
                                                 ServiceDate = new DateTime(2009, 1, 8),
                                                 ServiceCode = "08",
                                                 ServiceLocationCode = "LLP",
                                                 ServiceCharge = 22.95m,
                                                 ServiceUnits = "67",
                                                 DiagnosisLetters ="ACBD",
                                                 ServiceUnitOfMeasurement = ServiceUnitOfMeasurement.Unit,
                                                 Parent = paperClaimInfo
                                             }
                                     };

            return paperClaimInfo;
        }
    }
}
