﻿using IO.Practiceware.Integration.HL7;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewServices.IntegrationMessageManagement;
using IO.Practiceware.Services.ExternalSystems;
using NHapi.Model.V23.Datatype;
using NHapi.Model.V23.Message;
using NHapi.Model.V23.Segment;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Xml;
using NHapiIMessage = NHapi.Base.Model.IMessage;


[assembly: Component(typeof(IntegrationMesageManagementViewService), typeof(IIntegrationMesageManagementViewService))]
namespace IO.Practiceware.Presentation.ViewServices.IntegrationMessageManagement
{

    #region HL7 Message

    public class MessageSearchModel
    {
        [Required(ErrorMessage = "Start Date field is required.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "End Date field is required.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [MustBeGreaterThan("StartDate", "EndDate", "StartTime", "EndTime", "End date must be greater than Start date")]
        public DateTime EndDate { get; set; }

        [RegularExpression("^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]$", ErrorMessage = "Invalid Start Time")]
        public string StartTime { get; set; }

           [RegularExpression("^[0-2][0-9]:[0-5][0-9]:[0-5][0-9]$", ErrorMessage = "Invalid End Time")]
        public string EndTime { get; set; }

        public int? ExternalSystemMessageProcessingStateId { get; set; }

        [Required(ErrorMessage = "External System field is required.")]
        public int? ExternalSystemId { get; set; }

        public IList<MessagesModel> MessageModelList { get; set; }

        public ExternalSystemMessageProcessingStateId? ExternalSystemMessageProcessingState { get; set; }

        public IDictionary<int, string> ExternalSystems { get; set; }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }

        public int TotalPages { get; set; }

        public List<string> PageList { get; set; }

        public int SelectedPage { get; set; }
    }
    public class MessagesModel
    {
        public DateTime? DateAndTime { get; set; }

        public string MessageType { get; set; }

        public string Status { get; set; }

        public string Id { get; set; }

        public string Value { get; set; }

        public string TransformedMessage { get; set; }

        public int RetryAttempt { get; set; }

        public int ExternalSystemMessageProcessingStateId { get; set; }

        public string ErrorMessage { get; set; }

        public int ProcessingAttemptCount { get; set; }

        public bool IsChecked { get; set; }
    }

    #endregion

    public interface IIntegrationMesageManagementViewService
    {
        IEnumerable<MessagesModel> SearchMessages(MessageSearchModel messages);
        List<string> Save(List<MessagesModel> hl7Message);
        List<string> HL7MessageValidation(string message);
        string GetMessage(string id);
    }

    public class IntegrationMesageManagementViewService : BaseViewService, IIntegrationMesageManagementViewService
    {
        private readonly Lazy<IExternalSystemService> _externalSystemService;
        private readonly Lazy<IPracticeRepository> _practiceRepository;


        public IntegrationMesageManagementViewService(Func<IPracticeRepository> practiceRepository, Func<IExternalSystemService> externalSystemService)
        {
            _practiceRepository = Lazy.For(practiceRepository);
            _externalSystemService = Lazy.For(externalSystemService);
        }

        public string GetMessage(string id)
        {
            string message = _practiceRepository.Value.ExternalSystemMessages.Single(i => i.Id == Guid.Parse(id)).Value;
            return message;
        }

        public List<string> HL7MessageValidation(string message)
        {
            var errorMessage = new List<string>();
            NHapiIMessage receiveMessage = HL7Messages.GetPipeParser().Parse(message);
            ValidateMessage(receiveMessage, errorMessage);
            return errorMessage;
        }

        public IEnumerable<MessagesModel> SearchMessages(MessageSearchModel model)
        {
            var searchParams = new Dictionary<MessagesSearchField, string>();

            searchParams.Add(MessagesSearchField.StartDate, model.StartDate.ToShortDateString() + " " + model.StartTime);
            if (string.IsNullOrEmpty(model.EndTime))
            {
                var time = new TimeSpan(23, 59, 0);
                model.EndTime = time.ToString();
            }
            searchParams.Add(MessagesSearchField.EndDate, model.EndDate.ToShortDateString() + " " + model.EndTime);
            if (model.ExternalSystemId > 0)
            {
                searchParams.Add(MessagesSearchField.ExternalSystem, model.ExternalSystemId.ToString());
            }
            if (model.ExternalSystemMessageProcessingStateId > 0)
            {
                searchParams.Add(MessagesSearchField.Status, model.ExternalSystemMessageProcessingStateId.ToString());
            }
            if (!searchParams.Any()) return new List<MessagesModel>();

            IEnumerable<MessagesModel> collection = SearchMessages(searchParams, model);
            return collection.ToList();
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual List<string> Save(List<MessagesModel> hl7Message)
        {
            var resultIds = new List<string>();
            hl7Message.ToList().ForEach(r => resultIds.Add(Save(r)));
            return resultIds;
        }

        #region HL7Message Validation

        protected List<string> ValidateMessage(NHapiIMessage message, List<string> errorMessage)
        {
            if (message.GetType() == typeof(ADT_A28))
            {
                return ValidateMessage((ADT_A28)message, errorMessage);
            }

            if (message.GetType() == typeof(ADT_A31))
            {
                return ValidateMessage((ADT_A31)message, errorMessage);
            }

            if (message.GetType() == typeof(SIU_S12))
            {
                return ValidateMessage((SIU_S12)message, errorMessage);
            }

            if (message.GetType() == typeof(SIU_S13))
            {
                return ValidateMessage((SIU_S13)message, errorMessage);
            }

            if (message.GetType() == typeof(SIU_S15))
            {
                return ValidateMessage((SIU_S15)message, errorMessage);
            }

            return null;
        }

        protected List<string> ValidateMessage(HL7Message message, List<string> errorMessage)
        {
            return ValidateMessage(message.Message, errorMessage);
        }

        private List<string> ValidateMessage(ADT_A28 message, List<string> errorMessage)
        {
            return ValidateMessageComponents(message.PID, errorMessage);
        }

        private List<string> ValidateMessage(ADT_A31 message, List<string> errorMessage)
        {
            return ValidateMessageComponents(message.PID, errorMessage);
        }

        private List<string> ValidateMessage(SIU_S12 message, List<string> errorMessage)
        {
            return ValidateMessageComponents(message.MSH, message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, GetAips(message), errorMessage);
        }

        private List<string> ValidateMessage(SIU_S13 message, List<string> errorMessage)
        {
            return ValidateMessageComponents(message.MSH, message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, GetAips(message), errorMessage);
        }

        private List<string> ValidateMessage(SIU_S15 message, List<string> errorMessage)
        {
            return ValidateMessageComponents(message.MSH, message.SCH, message.GetPATIENT().PID, message.GetPATIENT().PV1, message.GetRESOURCES().GetLOCATION_RESOURCE().AIL, GetAips(message), errorMessage);
        }

        private List<string> ValidateMessageComponents(PID pid, List<string> errorMessage)
        {
            errorMessage.Clear();
            if (pid.PatientIDExternalID.ID.Value.IsNullOrEmpty() && pid.GetPatientIDInternalID(0).ID.Value.IsNullOrEmpty())
                errorMessage.Add("PatientId is not found.");

            if (!pid.Sex.Value.Truncate(1).IsNullOrEmpty())
            {
                if (!Enums.GetValues<Gender>().Any(w => w.GetDescription() == pid.Sex.Value.Truncate(1)))
                {
                    errorMessage.Add("PID is not contain a valid gender.");
                }
            }

            #region Helper function: handleAddress - Returns true if PID Address segment contains a valid address.  Otherwise false.

            Action<XAD> handleAddress = pidAddressSegment =>
            {
                if (pidAddressSegment.StreetAddress.Value.IsNullOrEmpty())
                {
                    errorMessage.Add("PID is not contain a valid street address.");
                }
                if (pidAddressSegment.ZipOrPostalCode.Value.IsNullOrEmpty())
                {
                    errorMessage.Add("PID is not contain a valid zipcode");
                }
                if (pidAddressSegment.City.Value.IsNullOrEmpty())
                {
                    errorMessage.Add("PID is not contain a valid city.");
                }
                if (pidAddressSegment.StateOrProvince.Value.IsNullOrEmpty())
                {
                    errorMessage.Add("PID is not contain a valid state.");
                }
                StateOrProvince state = _practiceRepository.Value.StateOrProvinces.SingleOrDefault(s => s.Abbreviation == pidAddressSegment.StateOrProvince.Value.Truncate(2));
                if (state == null) errorMessage.Add("Could not find state {0}.".FormatWith(pidAddressSegment.StateOrProvince.Value));
            };
            handleAddress(pid.GetPatientAddress(0));

            #endregion

            return errorMessage;
        }


        private List<string> ValidateMessageComponents(MSH msh, SCH sch, PID pid, PV1 pv1, AIL ail, AIP[] aips, List<string> errorMessage)
        {
            string appointmentId = sch.FillerAppointmentID.EntityIdentifier.Value;
            if (string.IsNullOrWhiteSpace(appointmentId))
            {
                appointmentId = sch.PlacerAppointmentID.EntityIdentifier.Value;
            }
            if (appointmentId.IsNullOrEmpty())
                errorMessage.Add("AppointmentId is not found.");
            else
            {
                if (msh.Message is SIU_S13 || msh.Message is SIU_S15)
                {
                    ExternalSystemEntityMapping appointmentMapping = _externalSystemService.Value.GetMapping<Appointment>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Appointment, appointmentId);
                    if (appointmentMapping.Id == 0)
                    {
                        errorMessage.Add("AppointmentId is not mapped.");
                    }
                }
            }

            ExternalSystemEntityMapping appointmentTypeMapping = _externalSystemService.Value.GetMapping<AppointmentType>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.AppointmentType, string.IsNullOrWhiteSpace(sch.AppointmentType.Identifier.Value) ? sch.AppointmentReason.Identifier.Value : sch.AppointmentType.Identifier.Value);
            var appointmentType = ((AppointmentType)_externalSystemService.Value.GetMappedEntity(appointmentTypeMapping));
            if (appointmentType == null)
            {
                errorMessage.Add("Appointment Reason or Appointment Type is not mapped.");
            }

            string externalPatientId = pid.PatientIDExternalID.ID.Value;
            if (externalPatientId.IsNullOrEmpty())
            {
                externalPatientId = pid.GetPatientIDInternalID(0).ID.Value;
            }
            var patient = ((Patient)_externalSystemService.Value.GetMappedEntity(_externalSystemService.Value.GetMapping<Patient>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.Patient, externalPatientId)));
            if (patient == null)
            {
                errorMessage.Add("PatientId is not mapped.");
            }

            string attendingDoctorId = pv1.AttendingDoctor.IDNumber.Value;
            ExternalSystemEntityMapping scheduledUserMapping = null;
            if (attendingDoctorId.IsNotNullOrEmpty())
            {
                scheduledUserMapping = _externalSystemService.Value.GetMapping<User>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.User, attendingDoctorId);
            }
            if (attendingDoctorId.IsNullOrEmpty() || scheduledUserMapping == null || scheduledUserMapping.Id == 0)
            {
                attendingDoctorId = aips.Where(i => i.ResourceRole.Identifier.Value != "R").Select(i => i.PersonnelResourceID.IDNumber.Value).FirstOrDefault();
            }

            if (attendingDoctorId.IsNullOrEmpty()) errorMessage.Add("Scheduled doctor could not be found in the message. PV1 doctor was: {0}. AIP doctor was: {1}.".FormatWith(pv1.AttendingDoctor.IDNumber.Value, attendingDoctorId));


            ServiceLocation location = null;
            if (ail.LocationResourceID.PointOfCare.Value.IsNotNullOrEmpty())
            {
                ExternalSystemEntityMapping locationMapping = _externalSystemService.Value.GetMapping<ServiceLocation>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.ServiceLocation, ail.LocationResourceID.PointOfCare.Value);
                location = ((ServiceLocation)_externalSystemService.Value.GetMappedEntity(locationMapping));
                if (location == null)
                {
                    if (pv1.AssignedPatientLocation.Facility.NamespaceID.Value.IsNotNullOrEmpty())
                    {
                        ExternalSystemEntityMapping locationMappingwithpv1 = _externalSystemService.Value.GetMapping<ServiceLocation>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.ServiceLocation, pv1.AssignedPatientLocation.Facility.NamespaceID.Value);
                        location = ((ServiceLocation)_externalSystemService.Value.GetMappedEntity(locationMappingwithpv1));
                    }
                }
            }
            else if (pv1.AssignedPatientLocation.Facility.NamespaceID.Value.IsNotNullOrEmpty())
            {
                ExternalSystemEntityMapping locationMapping = _externalSystemService.Value.GetMapping<ServiceLocation>(msh.SendingApplication.NamespaceID.Value, PracticeRepositoryEntityId.ServiceLocation, pv1.AssignedPatientLocation.Facility.NamespaceID.Value);
                location = ((ServiceLocation)_externalSystemService.Value.GetMappedEntity(locationMapping));
            }
            if (location == null)
                errorMessage.Add("Location resource is not mapped.");
            return errorMessage;
        }

        private AIP[] GetAips(SIU_S12 message)
        {
            var results = new List<AIP>();
            for (int i = 0; i < message.GetRESOURCES().PERSONNEL_RESOURCERepetitionsUsed; i++)
            {
                results.Add(message.GetRESOURCES().GetPERSONNEL_RESOURCE(i).IfNotNull(r => r.AIP));
            }
            return results.WhereNotDefault().ToArray();
        }

        private AIP[] GetAips(SIU_S13 message)
        {
            var results = new List<AIP>();
            for (int i = 0; i < message.GetRESOURCES().PERSONNEL_RESOURCERepetitionsUsed; i++)
            {
                results.Add(message.GetRESOURCES().GetPERSONNEL_RESOURCE(i).IfNotNull(r => r.AIP));
            }
            return results.WhereNotDefault().ToArray();
        }

        private AIP[] GetAips(SIU_S15 message)
        {
            var results = new List<AIP>();
            for (int i = 0; i < message.GetRESOURCES().PERSONNEL_RESOURCERepetitionsUsed; i++)
            {
                results.Add(message.GetRESOURCES().GetPERSONNEL_RESOURCE(i).IfNotNull(r => r.AIP));
            }
            return results.WhereNotDefault().ToArray();
        }

        #endregion

        private IEnumerable<MessagesModel> SearchMessages(IEnumerable<KeyValuePair<MessagesSearchField, string>> searchParams, MessageSearchModel model)
        {
            IQueryable<ExternalSystemMessage> externalSystemMessage = _practiceRepository.Value.ExternalSystemMessages.AsQueryable();
            Expression<Func<ExternalSystemMessage, bool>> whereCondition = ConstructWhereCondition(externalSystemMessage, searchParams);
            if (whereCondition == null) return new List<MessagesModel>();

            externalSystemMessage = externalSystemMessage.Where(whereCondition);
            
            if (model.CurrentPage == 1)
            {
                decimal pages = Math.Ceiling((decimal)externalSystemMessage.Count() / model.PageSize);
                int totalPages;
                if (int.TryParse(pages.ToString(), out totalPages))
                {
                    model.TotalPages = totalPages;
                }
            }
            List<ExternalSystemMessage> fromDatabaseResults = externalSystemMessage.OrderByDescending(p => p.CreatedDateTime).Skip((model.CurrentPage - 1) * model.PageSize).Take(model.PageSize).ToList();

            // Load dependent data
            _practiceRepository.Value.AsQueryableFactory().Load(fromDatabaseResults,
                                                                r => r.ExternalSystemMessageProcessingState);
            IEnumerable<ExternalSystemMessage> sortedResults = fromDatabaseResults;

            // Build up view model data and return
            var results = new List<MessagesModel>();
            foreach (ExternalSystemMessage r in sortedResults)
            {
                var result = new MessagesModel();
                result.DateAndTime = r.CreatedDateTime;
                result.MessageType = r.Description;
                result.Status = r.ExternalSystemMessageProcessingState.Name;
                result.Id = r.Id.ToString();
                result.Value = r.Value;
                result.TransformedMessage = ConvertToXml(r.Value);
                result.RetryAttempt = r.ProcessingAttemptCount;
                result.ErrorMessage = r.Error;
                result.ExternalSystemMessageProcessingStateId = r.ExternalSystemMessageProcessingStateId;
                results.Add(result);
            }
            return results;
        }

        private static Expression<Func<ExternalSystemMessage, bool>> ConstructWhereCondition(IQueryable<ExternalSystemMessage> messages, IEnumerable<KeyValuePair<MessagesSearchField, string>> searchParams)
        {
            var wherePredicates = new List<Expression<Func<ExternalSystemMessage, bool>>>();

            foreach (KeyValuePair<MessagesSearchField, String> entry in searchParams)
            {
                var terms = new List<string>();
                var fields = new List<MessagesSearchField>();
                terms.Add(entry.Value);
                fields.Add(entry.Key);
                wherePredicates.AddRange(QueryContains(messages, terms, fields));
            }
            Expression<Func<ExternalSystemMessage, bool>> whereCondition = wherePredicates.FirstOrDefault();
            if (whereCondition == null) return null;
            if (wherePredicates.Count > 1)
            {
                whereCondition = wherePredicates.Skip(1).Aggregate(whereCondition, (current, predicate) => current.And(predicate));
            }
            return whereCondition;
        }

        private string Save(MessagesModel hl7Message)
        {
            ExternalSystemMessage externalSystemMessage = _practiceRepository.Value.ExternalSystemMessages.Single(x => x.Id == Guid.Parse(hl7Message.Id) && x.ExternalSystemMessageProcessingStateId == (int)ExternalSystemMessageProcessingStateId.Processing);
            externalSystemMessage.ProcessingAttemptCount = hl7Message.ProcessingAttemptCount;
            externalSystemMessage.ExternalSystemMessageProcessingStateId = (int)ExternalSystemMessageProcessingStateId.Unprocessed;
            _practiceRepository.Value.Save(externalSystemMessage);
            return externalSystemMessage.Id.ToString();
        }

        #region Message Parser

        private static XmlDocument _xmlDoc;

        private static string ConvertToXml(string sHL7)
        {
            try
            {
                _xmlDoc = CreateXmlDoc();
                string[] sHL7Lines = sHL7.Split('\r');
                for (int i = 0; i < sHL7Lines.Length; i++)
                {
                    sHL7Lines[i] = Regex.Replace(sHL7Lines[i], @"[^ -~]", "");
                }
                foreach (string t in sHL7Lines)
                {
                    // Don't care about empty lines
                    if (t != string.Empty)
                    {
                        // Get the line and get the line's segments
                        string sHL7Line = t;
                        string[] sFields = GetMessgeFields(sHL7Line);

                        // Create a new element in the XML for the line
                        XmlElement el = _xmlDoc.CreateElement(sFields[0]);
                        if (_xmlDoc.DocumentElement != null) _xmlDoc.DocumentElement.AppendChild(el);

                        // For each field in the line of HL7
                        for (int a = 0; a < sFields.Length; a++)
                        {
                            // Create a new element
                            XmlElement fieldEl = _xmlDoc.CreateElement(sFields[0] +
                                                                       "." + a);


                            if (sFields[a] != @"^~\&")
                            {
                                string[] sComponents = GetComponents(sFields[a]);
                                if (sComponents.Length > 1)
                                {
                                    for (int b = 0; b < sComponents.Length; b++)
                                    {
                                        XmlElement componentEl = _xmlDoc.CreateElement(sFields[0] +
                                                                                       "." + a +
                                                                                       "." + b);

                                        string[] subComponents = GetSubComponents(sComponents[b]);
                                        if (subComponents.Length > 1)
                                        // There were subcomponents
                                        {
                                            for (int c = 0; c < subComponents.Length; c++)
                                            {
                                                // Check for repetition
                                                string[] subComponentRepetitions =
                                                    GetRepetitions(subComponents[c]);
                                                if (subComponentRepetitions.Length > 1)
                                                {
                                                    for (int d = 0;
                                                         d < subComponentRepetitions.Length;
                                                         d++)
                                                    {
                                                        XmlElement subComponentRepEl =
                                                            _xmlDoc.CreateElement(sFields[0] +
                                                                                  "." + a +
                                                                                  "." + b +
                                                                                  "." + c +
                                                                                  "." + d);
                                                        subComponentRepEl.InnerText =
                                                            subComponentRepetitions[d];
                                                        componentEl.AppendChild(subComponentRepEl);
                                                    }
                                                }
                                                else
                                                {
                                                    XmlElement subComponentEl =
                                                        _xmlDoc.CreateElement(sFields[0] +
                                                                              "." + a + "." +
                                                                              b + "." + c);
                                                    subComponentEl.InnerText = subComponents[c];
                                                    componentEl.AppendChild(subComponentEl);
                                                }
                                            }
                                            fieldEl.AppendChild(componentEl);
                                        }
                                        else // There were no subcomponents
                                        {
                                            string[] sRepetitions =
                                                GetRepetitions(sComponents[b]);
                                            if (sRepetitions.Length > 1)
                                            {
                                                for (int c = 0; c < sRepetitions.Length; c++)
                                                {
                                                    XmlElement repetitionEl = _xmlDoc.CreateElement(sFields[0] + "." +
                                                                                                    a + "." + b +
                                                                                                    "." + c);
                                                    repetitionEl.InnerText = sRepetitions[c];
                                                    componentEl.AppendChild(repetitionEl);
                                                }
                                                fieldEl.AppendChild(componentEl);
                                                el.AppendChild(fieldEl);
                                            }
                                            else
                                            {
                                                componentEl.InnerText = sComponents[b];
                                                fieldEl.AppendChild(componentEl);
                                                el.AppendChild(fieldEl);
                                            }
                                        }
                                    }
                                    el.AppendChild(fieldEl);
                                }
                                else
                                {
                                    fieldEl.InnerText = sFields[a];
                                    el.AppendChild(fieldEl);
                                }
                            }
                            else
                            {
                                fieldEl.InnerText = sFields[a];
                                el.AppendChild(fieldEl);
                            }
                        }
                    }
                }
            }
            catch
            {
                return " Cannot display Transformed Message.";
            }

            return _xmlDoc.OuterXml;
        }

        private static string[] GetMessgeFields(string s)
        {
            return s.Split('|');
        }


        private static string[] GetComponents(string s)
        {
            return s.Split('^');
        }


        private static string[] GetSubComponents(string s)
        {
            return s.Split('&');
        }


        private static string[] GetRepetitions(string s)
        {
            return s.Split('~');
        }


        private static XmlDocument CreateXmlDoc()
        {
            var output = new XmlDocument();
            XmlElement rootNode = output.CreateElement("HL7Message");
            output.AppendChild(rootNode);
            return output;
        }

        #endregion

        #region QueryContains HL7 Messages

        private static IEnumerable<Expression<Func<ExternalSystemMessage, bool>>> QueryContains(IQueryable<ExternalSystemMessage> messages, IEnumerable<string> searchTerms, IEnumerable<MessagesSearchField> searchFields)
        {
            var predicates = new List<Expression<Func<ExternalSystemMessage, bool>>>();
            var searchTermsEnum = searchTerms as IList<string> ?? searchTerms.ToList();
            IEnumerable<MessagesSearchField> messagesSearchFields = searchFields as IList<MessagesSearchField> ?? searchFields.ToList();

            if (messagesSearchFields.Contains(MessagesSearchField.StartDate)) predicates.AddRange(QueryMatchFromDate(messages, searchTermsEnum));
            if (messagesSearchFields.Contains(MessagesSearchField.EndDate)) predicates.AddRange(QueryMatchToDate(messages, searchTermsEnum));
            if (messagesSearchFields.Contains(MessagesSearchField.Status)) predicates.AddRange(QueryMatchStatus(messages, searchTermsEnum));
            if (messagesSearchFields.Contains(MessagesSearchField.ExternalSystem)) predicates.AddRange(QueryMatchExternalSystems(messages, searchTermsEnum));
            return predicates;
        }

        private static IEnumerable<Expression<Func<ExternalSystemMessage, bool>>> QueryMatchFromDate(IQueryable<ExternalSystemMessage> messages, IEnumerable<string> searchTerms)
        {
            var queries = new List<Expression<Func<ExternalSystemMessage, bool>>>();
            foreach (string term in searchTerms)
            {
                string t = term;
                DateTime fromdate;
                if (DateTime.TryParse(t, out fromdate))
                {
                    var time = new TimeSpan(0, 0, 0);
                    fromdate = fromdate.Add(time);
                    Expression<Func<ExternalSystemMessage, bool>> queryActualDateTime = messages.CreatePredicate(p => p.CreatedDateTime >= fromdate);
                    queries.Add(queryActualDateTime);
                }
            }
            return queries;
        }

        private static IEnumerable<Expression<Func<ExternalSystemMessage, bool>>> QueryMatchToDate(IQueryable<ExternalSystemMessage> messages, IEnumerable<string> searchTerms)
        {
            var queries = new List<Expression<Func<ExternalSystemMessage, bool>>>();
            foreach (string term in searchTerms)
            {
                string t = term;
                DateTime todate;
                if (DateTime.TryParse(t, out todate))
                {
                    Expression<Func<ExternalSystemMessage, bool>> queryActualDateTime = messages.CreatePredicate(p => p.CreatedDateTime <= todate);
                    queries.Add(queryActualDateTime);
                }
            }
            return queries;
        }

        private static IEnumerable<Expression<Func<ExternalSystemMessage, bool>>> QueryMatchStatus(IQueryable<ExternalSystemMessage> messages, IEnumerable<string> searchTerms)
        {
            var queries = new List<Expression<Func<ExternalSystemMessage, bool>>>();
            foreach (string term in searchTerms)
            {
                string t = term;
                int externalSystemMessageProcessingStateId;
                if (int.TryParse(t, out externalSystemMessageProcessingStateId))
                {
                    if (externalSystemMessageProcessingStateId > 0)
                    {
                        Expression<Func<ExternalSystemMessage, bool>> queryActualDateTime = messages.CreatePredicate(p => p.ExternalSystemMessageProcessingStateId == externalSystemMessageProcessingStateId);
                        queries.Add(queryActualDateTime);
                    }
                }
            }
            return queries;
        }

        private static IEnumerable<Expression<Func<ExternalSystemMessage, bool>>> QueryMatchExternalSystems(IQueryable<ExternalSystemMessage> messages, IEnumerable<string> searchTerms)
        {
            var queries = new List<Expression<Func<ExternalSystemMessage, bool>>>();
            foreach (string term in searchTerms)
            {
                string t = term;
                int externalsystemId;
                if (int.TryParse(t, out externalsystemId))
                {
                    if (externalsystemId > 0)
                    {
                        Expression<Func<ExternalSystemMessage, bool>> queryActualDateTime = messages.CreatePredicate(p => p.ExternalSystemExternalSystemMessageType.ExternalSystemId == externalsystemId);
                        queries.Add(queryActualDateTime);
                    }
                }
            }
            return queries;
        }

        #endregion
    }

    public enum MessagesSearchField
    {
        [EnumMember]
        StartDate,
        [EnumMember]
        EndDate,
        [EnumMember]
        Status,
        [EnumMember]
        ExternalSystem
    }

    public class MustBeGreaterThanAttribute : ValidationAttribute
    {
        private readonly string _endDate;
        private readonly string _endTime;
        private readonly string _startDate;
        private readonly string _startTime;

        public MustBeGreaterThanAttribute(string startDate, string endDate, string startTime, string endTime, string errorMessage)
            : base(errorMessage)
        {
            _startDate = startDate;
            _startTime = startTime ?? new TimeSpan(0, 0, 0).ToString();
            _endTime = endTime ?? new TimeSpan(23, 59, 0).ToString();
            _endDate = endDate;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo otherProperty = validationContext.ObjectInstance.GetType().GetProperty(_startDate);
            object startDate = otherProperty.GetValue(validationContext.ObjectInstance, null);
            otherProperty = validationContext.ObjectInstance.GetType().GetProperty(_startTime);
            object startTime = otherProperty.GetValue(validationContext.ObjectInstance, null);
            otherProperty = validationContext.ObjectInstance.GetType().GetProperty(_endTime);
            object endTime = otherProperty.GetValue(validationContext.ObjectInstance, null);
            otherProperty = validationContext.ObjectInstance.GetType().GetProperty(_endDate);
            object endDate = otherProperty.GetValue(validationContext.ObjectInstance, null);

            string newStartDate = startDate.ToString().Split(' ')[0] + " " + startTime; // start Date
            string newEndDate = endDate.ToString().Split(' ')[0] + " " + endTime; // End Date 

            DateTime startDateValue = Convert.ToDateTime(newStartDate);
            DateTime endDateValue = Convert.ToDateTime(newEndDate);

            if (startDateValue <= endDateValue)
            {
                return ValidationResult.Success;
            }
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }
}