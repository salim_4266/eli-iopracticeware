﻿using System.Collections.Generic;

namespace IO.Practiceware.Presentation.ViewServices.InsurancePlansSetup
{
    public class DeleteInsurancePlansResult
    {
        /// <summary>
        /// Plans that were actually deleted
        /// </summary>
        public ICollection<int> DeletedPlans { get; set; }

        /// <summary>
        /// Insurer Id as Key. String value is error message
        /// </summary>
        public IDictionary<int, string> PlanRemovalErrors { get; set; } 
    }
}