﻿using IO.Practiceware.Model;
using IO.Practiceware.Model.Auditing;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup;
using IO.Practiceware.Presentation.ViewServices.Common;
using IO.Practiceware.Presentation.ViewServices.InsurancePlansSetup;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Domain;
using Soaf.Linq;
using Soaf.Reflection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[assembly: Component(typeof(InsurancePlansSetupViewService), typeof(IInsurancePlansSetupViewService))]

namespace IO.Practiceware.Presentation.ViewServices.InsurancePlansSetup
{
    public interface IInsurancePlansSetupViewService
    {
        ICollection<InsurancePlanToManageViewModel> LoadInsurancePlans(bool includeArchived, bool includeActive, string searchFilter = "");
        ManageInsurancePlansLoadInformation LoadManageInsurancePlansViewData(int? singleLoadInsurerId, bool includeArchived, bool includeActive);
        ICollection<AuditEntryViewModel> LoadChangeHistory(int insurancePlanId);
        ICollection<InsurancePlanToManageViewModel> LoadClaimCrossover(int insurancePlanId);
        ICollection<InsurerClaimFormViewModel> LoadClaimForms(int insurancePlanId);

        /// <summary>
        /// Saves plans
        /// </summary>
        /// <param name="insurancePlans"></param>
        /// <returns>New Ids mapping to DB Ids</returns>
        IDictionary<Guid, int> SavePlans(List<InsurancePlanToManageViewModel> insurancePlans);
        int SavePlan(InsurancePlanToManageViewModel insurancePlan);

        void ReactiveInsurancePlan(int insurancePlanId);
        void ArchivePlans(int[] insurancePlans);
        DeleteInsurancePlansResult DeleteInsurancePlans(int[] insurancePlans);

        /// <summary>
        /// Checks whether plan is currently used by dependents
        /// </summary>
        /// <param name="insurancePlanId"></param>
        /// <returns></returns>
        bool IsPlanUsedByDependents(int insurancePlanId);

        /// <summary>
        /// Returns payer mappings collection
        /// </summary>
        /// <returns></returns>
        ICollection<PayerMappingViewModel> GetPayerMappings();

        /// <summary>
        /// Saves updated mappings
        /// </summary>
        /// <param name="mappings"></param>
        void SavePayerMappings(ICollection<PayerMappingViewModel> mappings);
    }

    [SupportsUnitOfWork]
    public class InsurancePlansSetupViewService : BaseViewService, IInsurancePlansSetupViewService
    {
        readonly IAuditRepository _auditRepository;
        readonly IUnitOfWorkProvider _unitOfWorkProvider;
        readonly IRepositoryService _repositoryService;

        IMapper<Insurer, InsurancePlanToManageViewModel> _insurerMapper;
        IMapper<InsurerPhoneNumberType, NamedViewModel> _phoneNumberTypeMapper;
        IMapper<InsurerPhoneNumber, PhoneNumberViewModel> _phoneNumberMapper;
        IMapper<InsurerBusinessClass, NamedViewModel> _insurerBusinessClassMapper;
        IMapper<InsurerPlanType, NamedViewModel> _insurerPlanTypeMapper;
        IMapper<InsurerClaimForm, InsurerClaimFormViewModel> _insurerClaimFormMapper;
        IMapper<ClaimFileReceiver, NamedViewModel> _claimFileReceiverMapper;
        IMapper<ClaimFilingIndicatorCode, NamedViewModel> _claimFilingIndicatorCodeMapper;
        IMapper<ClaimFormType, ClaimFormTypeViewModel> _claimFormTypeMapper;
        readonly IMapper<StateOrProvince, StateOrProvinceViewModel> _stateOrProvinceMapper;
        IMapper<AuditEntryChange, AuditEntryViewModel> _auditEntryMapper;
        IMapper<Tuple<int, string>, NamedViewModel> _namedViewModelMapper;
        IMapper<DoctorInsurerAssignment, InsurerDoctorAssignmentViewModel> _doctorAssignmentMapper;
        IMapper<Doctor, NamedViewModel> _doctorMapper;
        IMapper<InsurerAddress, PostalCodeViewModel> _postalCodeMapper;

        public InsurancePlansSetupViewService(
            IAuditRepository auditRepository,
            IUnitOfWorkProvider unitOfWorkProvider,
            IMapper<StateOrProvince, StateOrProvinceViewModel> stateMapper,
            IRepositoryService repositoryService
            )
        {
            _auditRepository = auditRepository;
            _unitOfWorkProvider = unitOfWorkProvider;
            _stateOrProvinceMapper = stateMapper;
            _repositoryService = repositoryService;
            InitMappers();
        }

        public virtual ICollection<InsurancePlanToManageViewModel> LoadInsurancePlans(bool includeArchived, bool includeActive, string searchFilter = "")
        {
            return LoadInsurancePlansInternal(null, includeArchived, includeActive, searchFilter);
        }

        /// <summary>
        /// Loads insurance plans.
        /// Either single item or all that match archived and active flags
        /// </summary>
        /// <param name="singleLoadInsurerId">The single load insurer id.</param>
        /// <param name="includeArchived">if set to <c>true</c> [include archived].</param>
        /// <param name="includeActive">if set to <c>true</c> [include active].</param>
        /// <returns></returns>
        public virtual ManageInsurancePlansLoadInformation LoadManageInsurancePlansViewData(int? singleLoadInsurerId, bool includeArchived, bool includeActive)
        {
            var response = new ManageInsurancePlansLoadInformation();

            // Load insurers along with all reference data
            new Action[]
                {
                    () => response.MatchedInsurancePlans = singleLoadInsurerId != null ? LoadInsurancePlansInternal(singleLoadInsurerId, includeArchived, includeActive) : new ExtendedObservableCollection<InsurancePlanToManageViewModel>(),
                    () => response.PhoneTypes = PracticeRepository.InsurerPhoneNumberTypes
                            .Where(p => !p.IsArchived)
                            .ToList()
                            .Select(_phoneNumberTypeMapper.Map)
                            .ToList(),
                    () => response.PlanTypes = PracticeRepository.InsurerPlanTypes
                            .ToList()
                            .Select(_insurerPlanTypeMapper.Map)
                            .ToList(),
                    () => response.States = PracticeRepository.StateOrProvinces
                            .Include(x => x.Country)
                            .ToList()
                            .Select(_stateOrProvinceMapper.Map)
                            .ToList(),
                    () => response.BusinessClasses = PracticeRepository.InsurerBusinessClasses
                            .Where(p => !p.IsArchived)
                            .ToList()
                            .Select(_insurerBusinessClassMapper.Map)
                            .ToList(),
                    () => response.ClaimFileIndicators = Enums.GetValues<ClaimFilingIndicatorCode>()
                            .Select(_claimFilingIndicatorCodeMapper.Map)
                            .ToList(),
                    () => response.ClaimFileReceivers = PracticeRepository.ClaimFileReceivers
                            .ToList()
                            .Select(_claimFileReceiverMapper.Map)
                            .ToList(),
                    () => response.ClaimFormTypes = PracticeRepository.ClaimFormTypes
                            .ToList()
                            .Select(_claimFormTypeMapper.Map)
                            .ToList(),
                    () => response.InvoiceTypes = Enums.GetValues<InvoiceType>()
                            .Select(p => new Tuple<int, string>((int) p, p.GetDisplayName()))
                            .Select(_namedViewModelMapper.Map)
                            .ToList(),
                    () => response.Doctors = PracticeRepository.Users.OfType<Doctor>()
                            .ToList()
                            .Select(_doctorMapper.Map)
                            .ToList()
                }.ForAllInParallel(a => a());


            return response;
        }

        public virtual ICollection<AuditEntryViewModel> LoadChangeHistory(int insurancePlanId)
        {
            // Load insurer changes
            var insurerChanges = GetAuditRecordsQuery<Insurer>(new[] { insurancePlanId });

            // Fetch ids of all related entities
            IEnumerable<long?> relatedClaimFormIds = null;
            IEnumerable<long?> relatedCrossoverIds = null;
            IEnumerable<long?> relatedPhoneNumberIds = null;
            IEnumerable<long?> relatedAddressIds = null;
            IEnumerable<long?> relatedDoctorAssignmentIds = null;

            var parallelLoadActions = new List<Action>();
            parallelLoadActions.Add(() => relatedClaimFormIds = FindAllTimeRelatedRecords<InsurerClaimForm>(
                Reflector.GetMember<InsurerClaimForm>(t => t.InsurerId).Name, insurancePlanId));

            parallelLoadActions.Add(() => relatedCrossoverIds = FindAllTimeRelatedRecords<ClaimCrossOver>(
                Reflector.GetMember<ClaimCrossOver>(t => t.SendingInsurerId).Name, insurancePlanId));

            parallelLoadActions.Add(() => relatedPhoneNumberIds = FindAllTimeRelatedRecords<InsurerPhoneNumber>(
                Reflector.GetMember<InsurerPhoneNumber>(t => t.InsurerId).Name, insurancePlanId));

            parallelLoadActions.Add(() => relatedAddressIds = FindAllTimeRelatedRecords<InsurerAddress>(
                Reflector.GetMember<InsurerAddress>(t => t.InsurerId).Name, insurancePlanId));

            parallelLoadActions.Add(() => relatedDoctorAssignmentIds = FindAllTimeRelatedRecords<DoctorInsurerAssignment>(
                Reflector.GetMember<DoctorInsurerAssignment>(t => t.InsurerId).Name, insurancePlanId));

            // Execute queries in parallel
            parallelLoadActions.ForAllInParallel(a => a());

            // Prepare queries for related records
            var claimFormChanges = GetAuditRecordsQuery<InsurerClaimForm>(relatedClaimFormIds);
            var crossOverChanges = GetAuditRecordsQuery<ClaimCrossOver>(relatedCrossoverIds);
            var phoneNumberChanges = GetAuditRecordsQuery<InsurerPhoneNumber>(relatedPhoneNumberIds);
            var addressChanges = GetAuditRecordsQuery<InsurerAddress>(relatedAddressIds);
            var doctorInsurerChanges = GetAuditRecordsQuery<DoctorInsurerAssignment>(relatedDoctorAssignmentIds);

            // Return ordering by DateTime
            return insurerChanges
                .Union(claimFormChanges)
                .Union(crossOverChanges)
                .Union(phoneNumberChanges)
                .Union(addressChanges)
                .Union(doctorInsurerChanges)
                .ToList()
                .SelectMany(p => p.AuditEntryChanges)
                .Select(_auditEntryMapper.Map)
                .OrderByDescending(p => p.DateTime)
                .ToList();
        }

        private IEnumerable<long?> FindAllTimeRelatedRecords<TRelatedModelEntity>(string columnName, long entityId)
        {
            var result = _auditRepository
                .AuditEntriesFor<TRelatedModelEntity>()
                .Where(p => p.AuditEntryChanges
                             .Any(x => x.ColumnName == columnName
                                       && x.NewValueNumeric == entityId))
                .Select(p => p.KeyValueNumeric)
                .ToList();

            return result;
        }

        private IQueryable<AuditEntry> GetAuditRecordsQuery<TModelEntity>(IEnumerable keys)
        {
            var result = _auditRepository
                .AuditEntriesFor<TModelEntity>(keys)
                .Include(p => p.AuditEntryChanges);

            return result;
        }

        public virtual ICollection<InsurancePlanToManageViewModel> LoadClaimCrossover(int insurancePlanId)
        {
            // Load insurers which can receive claims
            var receivingInsurer = PracticeRepository.ClaimCrossOvers
                                                     .Where(p => p.SendingInsurerId == insurancePlanId)
                                                     .Include(
                                                         p => p.ReceivingInsurer,
                                                         p => p.ReceivingInsurer.InsurerPlanType,
                                                         p => p.ReceivingInsurer.InsurerAddresses.Select(x => x.StateOrProvince))
                                                     .ToList();

            var result = receivingInsurer
                .Select(p => p.ReceivingInsurer)
                .Select(_insurerMapper.Map)
                .OrderBy(p => p.InsuranceName)
                .ToList();

            return result;
        }

        public virtual ICollection<InsurerClaimFormViewModel> LoadClaimForms(int insurancePlanId)
        {
            // Load insurer claim forms
            var insurerClaimForms = PracticeRepository.InsurerClaimForms
                .Where(p => p.InsurerId == insurancePlanId)
                .Include(x => x.ClaimFormType)
                .ToList();

            // Map claim forms and group them by form type combining InvoiceTypes together
            var result = insurerClaimForms
                .Select(_insurerClaimFormMapper.Map)
                .OrderBy(p => p.FormType.Id)
                .ThenBy(p => p.InvoiceType.Id)
                .ToList();

            return result;
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual IDictionary<Guid, int> SavePlans(List<InsurancePlanToManageViewModel> insurancePlans)
        {
            var plansToUpdate = new List<Insurer>(insurancePlans.Count);

            // Fetch default values
            var defaultStateOrProvince = PracticeRepository.StateOrProvinces.First();
            var defaultPhoneNumberType = PracticeRepository.InsurerPhoneNumberTypes
                                                           .First(p => !p.IsArchived);
            var claimFormTypes = PracticeRepository.ClaimFormTypes.ToList();

            // If new plan -> create it
            // Also prepare mapping information to set back keys for new entities
            var planMapping = insurancePlans
                .Where(p => !p.Id.HasValue)
                .ToDictionary(p => new Insurer(), p => p);
            plansToUpdate.AddRange(planMapping.Keys);

            // Fetch existing plans
            var existingPlansIds = insurancePlans
                .Where(p => p.Id.HasValue)
                .Select(p => p.Id.Value)
                .ToArray();
            var existingPlans = PracticeRepository.Insurers
                                                  .Where(p => existingPlansIds.Contains(p.Id))
                                                  .ToList();
            plansToUpdate.AddRange(existingPlans);

            // Add the mapping information
            foreach (var existingPlan in existingPlans)
            {
                planMapping.Add(existingPlan, insurancePlans.First(p => p.Id.HasValue && p.Id == existingPlan.Id));
            }

            // Prefetch related entities for existing plans
            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(existingPlans,
                insurer => insurer.InsurerPhoneNumbers.Select(p => p.InsurerPhoneNumberType),
                insurer => insurer.InsurerAddresses.Select(p => p.StateOrProvince),
                insurer => insurer.InsurerClaimForms.Select(p => p.ClaimFormType),
                insurer => insurer.SendingClaimCrossOvers,
                insurer => insurer.DoctorInsurerAssignments);

            foreach (var planToUpdate in plansToUpdate)
            {
                // Get the source plan for this plan
                var insurancePlan = planMapping[planToUpdate];

                // Update to newly entered values
                planToUpdate.Name = insurancePlan.InsuranceName;             
                planToUpdate.PlanName = insurancePlan.PlanName;
                planToUpdate.PolicyNumberFormat = insurancePlan.PolicyNumberFormat;
                planToUpdate.AllowDependents = insurancePlan.CanBeUsedByDependent;
                planToUpdate.IsReferralRequired = insurancePlan.ReferralRequired;
                planToUpdate.IsSecondaryClaimPaper = insurancePlan.PaperClaimIfNotPrimary;
                planToUpdate.IsMedicareAdvantage = insurancePlan.MedicareAdvantage;
                planToUpdate.MedigapCode = insurancePlan.MedigapId;
                planToUpdate.PayerCode = insurancePlan.PayerId;
                planToUpdate.Hpid = insurancePlan.Hpid;
                planToUpdate.Oeid = insurancePlan.Oeid;
                planToUpdate.SendZeroCharge = insurancePlan.AllowZeroAmount;
                planToUpdate.IsIcd10Decimal = insurancePlan.DisplayICDTenDecimal;
                planToUpdate.IsCLIAIncludedOnClaims = insurancePlan.DisplayCLIAOnClaims;//task#22201
                planToUpdate.IsTaxonomyIncludedOnClaims = insurancePlan.DisplayTaxonomyOnClaims;//task#22264
                planToUpdate.Website = insurancePlan.Website;
                planToUpdate.Comment = insurancePlan.Comment;
                planToUpdate.DiagnosisTypeId = insurancePlan.SelectedDiagnosisCodeSet != null && insurancePlan.SelectedDiagnosisCodeSet.Id > 0 ? insurancePlan.SelectedDiagnosisCodeSet.Id : (int?) null;

                // Update reference values
                planToUpdate.InsurerBusinessClassId = insurancePlan.BusinessClass.IfNotNull(p => p.Id, (int)InsurerBusinessClassId.Commercial);
                planToUpdate.InsurerPlanTypeId = insurancePlan.PlanType.IfNotNull(p => (int?)p.Id);
                planToUpdate.ClaimFileReceiverId = insurancePlan.Receiver.IfNotNull(p => (int?)p.Id);
                planToUpdate.ClaimFilingIndicatorCode = insurancePlan.Cfic.IfNotNull(p => (ClaimFilingIndicatorCode)p.Id, ClaimFilingIndicatorCode.CommercialInsurance);

                // Update the address information
                var addressToEdit = planToUpdate.InsurerAddresses.WithAddressType(
                    InsurerAddressTypeId.Claims, NotFoundBehavior.Add);
                addressToEdit.SetLines(insurancePlan.InsurerAddressLine1 ?? "", insurancePlan.InsurerAddressLine2, insurancePlan.InsurerAddressLine3); // Line 1, 2, 3
                addressToEdit.City = insurancePlan.InsurerCity ?? string.Empty; // Required field
                addressToEdit.PostalCode = insurancePlan.InsurerPostalCode != null ? insurancePlan.InsurerPostalCode.Value : string.Empty; // Required field
                addressToEdit.StateOrProvinceId = insurancePlan.InsurerState.IfNotNull(p => p.Id, () => defaultStateOrProvince.Id);

                // Sync phone numbers
                PracticeRepository.SyncPhoneNumbers(insurancePlan.InsurerPhones, planToUpdate.InsurerPhoneNumbers,
                    number => number.InsurerPhoneNumberTypeId, (number, i) => number.InsurerPhoneNumberTypeId = i,
                    defaultPhoneNumberType.Id);

                // Sync doctor assignments
                // We will be removing ones which represent default values
                PracticeRepository.SyncDependencyCollection(
                    insurancePlan.DoctorAssignments.Where(a => !a.IsDefault).ToList(),
                    planToUpdate.DoctorInsurerAssignments,
                    (viewModel, model) => viewModel.Doctor.Id == model.DoctorId,
                    (viewModel, model) =>
                    {
                        model.DoctorId = viewModel.Doctor.Id;
                        //model.InsurerId <- set automatically
                        model.IsAssignmentAccepted = viewModel.AcceptAssignment;
                        model.SuppressPatientPayments = viewModel.SuppressPayments;
                    });

                // Sync claim form types
                if (insurancePlan.IsInsurerClaimFormsLoaded)
                {
                    PracticeRepository.SyncDependencyCollection(insurancePlan.InsurerClaimForms, planToUpdate.InsurerClaimForms,
                        (viewModel, model) => viewModel.FormType.Id == model.ClaimFormTypeId
                            && (InvoiceType)viewModel.InvoiceType.Id == model.InvoiceType,
                        (viewModel, model) =>
                        {
                            model.ClaimFormType = claimFormTypes.First(cft => cft.Id == viewModel.FormType.Id);
                            model.InvoiceType = (InvoiceType)viewModel.InvoiceType.Id;
                        });
                }

                // Business rule: no 837/Electronic claim form types when Receiver is not set
                if (planToUpdate.ClaimFileReceiverId == null && planToUpdate.InsurerClaimForms.Any(
                    p => p.ClaimFormType.MethodSent == MethodSent.Electronic))
                {
                    planToUpdate.InsurerClaimForms
                        .Where(p => p.ClaimFormType.MethodSent == MethodSent.Electronic)
                        .ToArray()
                        .ForEach(PracticeRepository.Delete);
                }

                // Sync sending claim crossover
                if (insurancePlan.IsClaimCrossoversLoaded)
                {
                    PracticeRepository.SyncDependencyCollection(insurancePlan.ClaimCrossovers, planToUpdate.SendingClaimCrossOvers,
                        (viewModel, model) => viewModel.Id.GetValueOrDefault() == model.ReceivingInsurerId,
                        (viewModel, model) =>
                        {
                            model.ReceivingInsurerId = viewModel.Id.GetValueOrDefault();
                        });
                }
            }

            // Commit all pending changes
            PracticeRepository.Save(plansToUpdate);

            // Persist now to get Ids
            _repositoryService.Persist(_unitOfWorkProvider.Current.ObjectStateEntries, typeof(IPracticeRepository).FullName);

            // Confirm ids
            foreach (var updatedPlan in plansToUpdate)
            {
                planMapping[updatedPlan].Id = updatedPlan.Id;
            }

            // Return mapping of plan Ids for newly inserted records
            return insurancePlans
                .Where(p => p.NewPlanId.HasValue && p.Id.HasValue)
                .ToDictionary(p => p.NewPlanId.Value, p => p.Id.Value);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual int SavePlan(InsurancePlanToManageViewModel insurancePlan)
        {
            // Save plan
            var idMapping = SavePlans(new List<InsurancePlanToManageViewModel> { insurancePlan });

            // Return the id of this plan
            return insurancePlan.Id.HasValue
                ? insurancePlan.Id.Value
                : idMapping[insurancePlan.NewPlanId.Value];
        }

        public virtual void ReactiveInsurancePlan(int insurancePlanId)
        {
            var planToUpdate = PracticeRepository.Insurers.WithId(insurancePlanId);

            // Set it back to non-archived
            planToUpdate.IsArchived = false;

            PracticeRepository.Save(planToUpdate);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void ArchivePlans(int[] insurancePlans)
        {
            var plansToUpdate = PracticeRepository.Insurers
                                                  .Where(p => insurancePlans.Contains(p.Id))
                                                  .ToList();

            // Set it to archived
            foreach (var planToUpdate in plansToUpdate)
            {
                planToUpdate.IsArchived = true;
                PracticeRepository.Save(planToUpdate);
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual DeleteInsurancePlansResult DeleteInsurancePlans(int[] insurancePlans)
        {
            var result = new DeleteInsurancePlansResult
                {
                    DeletedPlans = new List<int>(),
                    PlanRemovalErrors = new Dictionary<int, string>()
                };

            // Retrieve Insurers to delete with relations
            var plansToDelete = PracticeRepository.Insurers
                .Where(p => insurancePlans.Contains(p.Id))
                .Include(
                // Relations which cannot be cascade deleted
                    p => p.Adjustments,
                    p => p.FinancialInformations,
                    p => p.InsurancePolicies,
                    p => p.ClaimFileOverrideProviders,
                    p => p.InsurerFeeScheduleContracts,

                    // Relations which are to be recursively deleted
                    p => p.InsurerClaimForms,
                    p => p.InsurerAddresses,
                    p => p.InsurerPhoneNumbers,
                    p => p.EmailAddresses,
                    p => p.ReceivingClaimCrossOvers,
                    p => p.SendingClaimCrossOvers,
                    p => p.DoctorInsurerAssignments)
                .ToList();

            // Deferred evaluation whether insurer is in external system mappings
            Func<Insurer, bool> isPresentInExternalSystemEntityMappings = insurer =>
                {
                    var insurerKey = insurer.Id.ToString();
                    return PracticeRepository.ExternalSystemEntityMappings
                        .Include(p => p.PracticeRepositoryEntity)
                        .Any(p => p.PracticeRepositoryEntityKey == insurerKey
                            && p.PracticeRepositoryEntityId == (int)PracticeRepositoryEntityId.Insurer);
                };

            // Iterate and delete one by one
            foreach (var insurer in plansToDelete)
            {
                // Check if plan is in use by entities we won't delete recursively
                if (insurer.Adjustments.Any()
                    || insurer.FinancialInformations.Any()
                    || insurer.InsurancePolicies.Any()
                    || insurer.ClaimFileOverrideProviders.Any()
                    || insurer.InsurerFeeScheduleContracts.Any()
                    || isPresentInExternalSystemEntityMappings(insurer))
                {
                    result.PlanRemovalErrors.Add(insurer.Id, "Plan is in use");
                }
                else
                {
                    // Issue deletes to dependent entities
                    // Note: Copying collection to prevent errors when modified during iteration
                    insurer.InsurerClaimForms.ToList().ForEach(PracticeRepository.Delete);
                    insurer.EmailAddresses.ToList().ForEach(PracticeRepository.Delete);
                    insurer.InsurerAddresses.ToList().ForEach(PracticeRepository.Delete);
                    insurer.InsurerPhoneNumbers.ToList().ForEach(PracticeRepository.Delete);
                    insurer.ReceivingClaimCrossOvers.ToList().ForEach(PracticeRepository.Delete);
                    insurer.SendingClaimCrossOvers.ToList().ForEach(PracticeRepository.Delete);
                    insurer.DoctorInsurerAssignments.ToList().ForEach(PracticeRepository.Delete);

                    // Delete insurer
                    PracticeRepository.Delete(insurer);

                    // Record plan is deleted
                    result.DeletedPlans.Add(insurer.Id);
                }
            }

            return result;
        }

        public virtual bool IsPlanUsedByDependents(int insurancePlanId)
        {
            var isUsed = (from ip in PracticeRepository.InsurancePolicies
                          join pins in PracticeRepository.PatientInsurances on
                              ip.Id equals pins.InsurancePolicyId
                          where ip.InsurerId == insurancePlanId
                                && ip.PolicyholderPatientId != pins.InsuredPatientId
                                && pins.PolicyholderRelationshipType != PolicyHolderRelationshipType.Self
                          select pins.Id)
                .Any();

            return isUsed;
        }

        public virtual ICollection<PayerMappingViewModel> GetPayerMappings()
        {
            return PracticeRepository.Insurers
                .SelectMany(i => i.IncomingPayerCodes.Select(pc => new
                {
                    IncomingPayerCode = pc.Value,
                    OutgoingPayerCode = i.PayerCode
                }))
                .ToArray().Distinct()
                .Select(i => new PayerMappingViewModel
                {
                    IncomingPayerCode = i.IncomingPayerCode,
                    OutgoingPayerCode = i.OutgoingPayerCode
                })
                .ToExtendedObservableCollection();
        }

        [UnitOfWork(AcceptChanges = true, AutoAcceptChanges = true)]
        public virtual void SavePayerMappings(ICollection<PayerMappingViewModel> mappings)
        {
            var codes = PracticeRepository
                .IncomingPayerCodes
                .Include(c => c.Insurers)
                .ToList();

            var outgoingPayerCodes = mappings.Select(m => m.OutgoingPayerCode).ToArray();
            var insurers = PracticeRepository.Insurers.Where(i => outgoingPayerCodes.Contains(i.PayerCode)).ToArray();

            if (!mappings.Any())
            {
                codes.ForEach(x =>
                {
                    x.Insurers.Clear();
                    PracticeRepository.Delete(x);
                });
                return;
            }

            foreach (var code in codes.ToArray())
            {
                // remove incoming payer codes that don't exist anymore
                if (mappings.All(m => m.IncomingPayerCode != code.Value))
                {
                    code.Insurers.Clear();
                    PracticeRepository.Delete(code);
                    codes.Remove(code);
                }
            }

            foreach (var mapping in mappings.Where(m => m.IncomingPayerCode != null && m.OutgoingPayerCode != null).GroupBy(m => m.IncomingPayerCode, m => m.OutgoingPayerCode))
            {
                var code = codes.FirstOrDefault(c => c.Value == mapping.Key);

                if (code == null)
                {
                    code = new IncomingPayerCode { Value = mapping.Key };
                    codes.Add(code);
                }

                // insurers in the DB that have the outgoing payer code for this mapping
                var mappingInsurers = insurers.Where(i => mapping.Contains(i.PayerCode)).ToArray();

                foreach (var insurer in mappingInsurers)
                {
                    if (!code.Insurers.Contains(insurer))
                    {
                        // add insurers not yet present
                        code.Insurers.Add(insurer);
                    }
                }

                foreach (var insurer in code.Insurers.ToArray())
                {
                    if (!mappingInsurers.Contains(insurer))
                    {
                        // remove insurers not present anymore
                        code.Insurers.Remove(insurer);
                    }
                }
            }
        }

        /// <summary>
        /// Loads insurance plans.
        /// Either single item or all that match archived and active flags
        /// </summary>
        /// <param name="singleLoadInsurerId">The single load insurer id.</param>
        /// <param name="includeArchived">if set to <c>true</c> [include archived].</param>
        /// <param name="includeActive">if set to <c>true</c> [include active].</param>
        /// <param name="searchFilter">Search on Text entered</param>
        /// <returns></returns>
        private ICollection<InsurancePlanToManageViewModel> LoadInsurancePlansInternal(int? singleLoadInsurerId, bool includeArchived, bool includeActive, string searchFilter = "")
        {
            // Fetch insurers
            var query = PracticeRepository.Insurers; // All
            if (searchFilter.IsNotNullOrEmpty()) query = query.Where(p => p.Name.Contains(searchFilter) || p.PlanName.Contains(searchFilter));
            if (singleLoadInsurerId.HasValue)
            {
                // Just load one record
                query = query.Where(p => p.Id == singleLoadInsurerId.Value);
            }
            else
            {
                // Build filter based on active and archived flags
                if (includeActive && !includeArchived)
                {
                    // Just active
                    query = query.Where(p => !p.IsArchived);
                }
                else if (!includeActive && includeArchived)
                {
                    // Just archived
                    query = query.Where(p => p.IsArchived);
                }
                else if (!includeActive && !includeArchived)
                {
                    // None
                    query = new List<Insurer>().AsQueryable();
                }
            }

            // Fetch insurers
            var insurers = query.ToList();

            // Load dependencies and convert to view model
            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(insurers,
                insurer => insurer.InsurerBusinessClass,
                insurer => insurer.InsurerPhoneNumbers.Select(p => p.InsurerPhoneNumberType),
                insurer => insurer.InsurerAddresses.Select(p => p.StateOrProvince),
                insurer => insurer.ClaimFileReceiver,
                insurer => insurer.InsurerPlanType,
                insurer => insurer.DoctorInsurerAssignments.Select(p => p.Doctor),
                insurer => insurer.DiagnosisType);
            var viewModel = insurers.Select(_insurerMapper.Map).ToList();

            // Hand over the result
            return viewModel;
        }

        void InitMappers()
        {
            var mapperFactory = Mapper.Factory;

            // Various reference maps to NamedViewModel-s
            _namedViewModelMapper = mapperFactory.CreateMapper<Tuple<int, string>, NamedViewModel>(
                item => new NamedViewModel
                    {
                        Id = item.Item1,
                        Name = item.Item2
                    }, false);
            _insurerBusinessClassMapper = mapperFactory.CreateMapper<InsurerBusinessClass, NamedViewModel>(
                item => new NamedViewModel
                    {
                        Id = item.Id,
                        Name = item.Name
                    }, false);
            _insurerPlanTypeMapper = mapperFactory.CreateMapper<InsurerPlanType, NamedViewModel>(
                item => new NamedViewModel
                {
                    Id = item.Id,
                    Name = item.Name
                }, false);
            _claimFileReceiverMapper = mapperFactory.CreateMapper<ClaimFileReceiver, NamedViewModel>(
                item => new NamedViewModel
                    {
                        Id = item.Id,
                        // TODO: We have seen Name being empty string, so hint it
                        Name = string.IsNullOrEmpty(item.Name) ? "Receiver (no name)" : item.Name
                    }, false);
            _claimFilingIndicatorCodeMapper = mapperFactory.CreateMapper<ClaimFilingIndicatorCode, NamedViewModel>(
                item => new NamedViewModel
                {
                    Id = (int)item,
                    Name = item.GetDisplayName()
                }, false);
            _claimFormTypeMapper = mapperFactory.CreateMapper<ClaimFormType, ClaimFormTypeViewModel>(
                item => new ClaimFormTypeViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    IsElectronic = item.MethodSent == MethodSent.Electronic
                }, false);
            _postalCodeMapper = mapperFactory.CreateMapper<InsurerAddress, PostalCodeViewModel>(
                item => new PostalCodeViewModel
                        {
                            Value = item.PostalCode
                        }, false);
            _insurerClaimFormMapper = mapperFactory.CreateMapper<InsurerClaimForm, InsurerClaimFormViewModel>(
                item => new InsurerClaimFormViewModel
                    {
                        FormType = _claimFormTypeMapper.Map(item.ClaimFormType),
                        InvoiceType = _namedViewModelMapper.Map(new Tuple<int, string>(
                            (int)item.InvoiceType, item.InvoiceType.GetDisplayName()))
                    });
            _doctorMapper = mapperFactory.CreateMapper<Doctor, NamedViewModel>(
                item => new NamedViewModel
                    {
                        Id = item.Id,
                        Name = item.DisplayName
                    }, false);
            _doctorAssignmentMapper = mapperFactory.CreateMapper<DoctorInsurerAssignment, InsurerDoctorAssignmentViewModel>(
                item => new InsurerDoctorAssignmentViewModel
                    {
                        Doctor = _doctorMapper.Map(item.Doctor),
                        AcceptAssignment = item.IsAssignmentAccepted,
                        SuppressPayments = item.SuppressPatientPayments
                    }, false);

            // Phone number related mapping
            _phoneNumberTypeMapper = mapperFactory.CreateMapper<InsurerPhoneNumberType, NamedViewModel>(
                item => new NamedViewModel
                {
                    Id = item.Id,
                    Name = item.Name
                }, false);
            _phoneNumberMapper = mapperFactory.CreatePhoneViewModelMapper<InsurerPhoneNumber>();

            // Insurer map
            _insurerMapper = mapperFactory.CreateMapper<Insurer, InsurancePlanToManageViewModel>(
                item => new InsurancePlanToManageViewModel
                    {
                        // Property mapping
                        Id = item.Id,
                        InsuranceName = item.Name,
                        PlanName = item.PlanName,
                        PolicyNumberFormat = item.PolicyNumberFormat,
                        CanBeUsedByDependent = item.AllowDependents,
                        ReferralRequired = item.IsReferralRequired,
                        PaperClaimIfNotPrimary = item.IsSecondaryClaimPaper,
                        MedicareAdvantage = item.IsMedicareAdvantage,
                        MedigapId = item.MedigapCode,
                        PayerId = item.PayerCode,
                        Hpid = item.Hpid,
                        Oeid = item.Oeid,
                        AllowZeroAmount = item.SendZeroCharge,
                        DisplayICDTenDecimal = item.IsIcd10Decimal,
                        DisplayCLIAOnClaims = item.IsCLIAIncludedOnClaims,//task#22201
                        DisplayTaxonomyOnClaims = item.IsTaxonomyIncludedOnClaims,//task#22264
                        Website = item.Website,
                        Comment = item.Comment,
                        IsArchived = item.IsArchived,

                        // Referenced entity mappings
                        BusinessClass = item.InsurerBusinessClass.IfNotNull(_insurerBusinessClassMapper.Map),
                        PlanType = item.InsurerPlanType.IfNotNull(_insurerPlanTypeMapper.Map),
                        Receiver = item.ClaimFileReceiver.IfNotNull(_claimFileReceiverMapper.Map),
                        Cfic = _claimFilingIndicatorCodeMapper.Map(item.ClaimFilingIndicatorCode),

                        // Collection mappings
                        InsurerPhones = item.InsurerPhoneNumbers.Select(_phoneNumberMapper.Map).ToExtendedObservableCollection(),
                        DoctorAssignments = item.DoctorInsurerAssignments.Select(_doctorAssignmentMapper.Map).OrderBy(i => i.DoctorName).ToExtendedObservableCollection(),

                        // Mapping only claim address
                        InsurerAddressLine1 = item.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims).IfNotNull(p => p.Line1),
                        InsurerAddressLine2 = item.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims).IfNotNull(p => p.Line2),
                        InsurerAddressLine3 = item.InsurerAddresses.WithAddressType(InsurerAddressTypeId.Claims).IfNotNull(p => p.Line3),
                        InsurerCity = item.InsurerAddresses
                            .WithAddressType(InsurerAddressTypeId.Claims)
                            .IfNotNull(p => p.City),
                        InsurerPostalCode = item.InsurerAddresses
                            .WithAddressType(InsurerAddressTypeId.Claims)
                            .IfNotNull(p => _postalCodeMapper.Map(p)),
                        InsurerState = item.InsurerAddresses
                            .WithAddressType(InsurerAddressTypeId.Claims)
                            .IfNotNull(p => _stateOrProvinceMapper.Map(p.StateOrProvince)),
                        SelectedDiagnosisCodeSet = item.DiagnosisType != null ? new NamedViewModel { Id = item.DiagnosisType.Id, Name = item.DiagnosisType.Name } : null
                    }, false);

            // ToStringIfNotNull contains optional arguments -> cannot be used directly in expression
            var toStringIfNotNull = new Func<object, string>(o => o.ToStringIfNotNull());

            // Audit mapper
            _auditEntryMapper = mapperFactory.CreateMapper<AuditEntryChange, AuditEntryViewModel>(entry =>
                new AuditEntryViewModel
                    {
                        EntityId = (int)entry.AuditEntry.KeyValueNumeric,
                        ChangedFrom = toStringIfNotNull(entry.GetOldValue()) ?? toStringIfNotNull(entry.OldValueNumeric),
                        ChangedTo = toStringIfNotNull(entry.GetNewValue()) ?? toStringIfNotNull(entry.NewValueNumeric),
                        ChangeType = entry.AuditEntry.ChangeType.GetDisplayName(),
                        DateTime = entry.AuditEntry.AuditDateTime,
                        Field = entry.GetDisplayName(),
                        UserName = toStringIfNotNull(entry.AuditEntry.UserId),
                        IsInsertChange = entry.AuditEntry.ChangeType == AuditEntryChangeType.Insert
                    }, false);
        }
    }
}
