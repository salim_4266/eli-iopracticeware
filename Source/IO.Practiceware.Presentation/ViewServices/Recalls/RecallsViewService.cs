﻿using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Recalls;
using IO.Practiceware.Presentation.ViewServices.Recalls;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;


[assembly: Component(typeof(RecallsViewService), typeof(IRecallsViewService))]
[assembly: Component(typeof(RecallDisplayViewModelMap), typeof(IMap<PatientRecall, RecallDisplayViewModel>))]
[assembly: Component(typeof(RecallDetailViewModelMap), typeof(IMap<PatientRecall, RecallDetailViewModel>))]

namespace IO.Practiceware.Presentation.ViewServices.Recalls
{
    public class RecallsViewService : BaseViewService, IRecallsViewService
    {
        private readonly IMapper<List<PatientRecall>, List<RecallDisplayViewModel>> _displayMapper;
        private readonly IMapper<PatientRecall, RecallDetailViewModel> _detailMapper;

        public RecallsViewService( IMapper<List<PatientRecall>, List<RecallDisplayViewModel>> displayMapper, IMapper<PatientRecall, RecallDetailViewModel> detailMapper)
        {
            _displayMapper = displayMapper;
             _detailMapper = detailMapper;
        }


        public IEnumerable<RecallDisplayViewModel> LoadRecallHistory(int patientId)
        {
            var recalls = PracticeRepository.PatientRecalls
                .Include(x => x.AppointmentType)
                .Include(x => x.User)
                .OrderByDescending(x => x.DueDateTime)
                .ThenByDescending(x => x.SentOrClosedDateTime)
                .Where(x => x.PatientId == patientId).ToList();


            var viewModels = _displayMapper.Map(recalls);

            return viewModels;
        }

        public RecallDetailViewModel LoadRecallDetail(int recallId)
        {
            var recall = PracticeRepository.PatientRecalls.FirstOrDefault(x => x.Id == recallId);

            if( recall == null )
            {
                throw new ArgumentException("No recall exists for the supplied id");
            }

            var viewModel = _detailMapper.Map(recall);

            return viewModel;
        }

        public RecallSelectListsViewModel GetRecallSelectLists()
        {
            var viewModel = new RecallSelectListsViewModel();

            var appointmentTypes = PracticeRepository.AppointmentTypes.OrderBy(x => x.OrdinalId).ThenBy(x => x.Name).Where(x => !x.IsArchived).ToList();
            viewModel.AppointmentTypes = appointmentTypes.Select(y => new AppointmentTypeViewModel
                {
                    Id = y.Id, 
                    Name = y.Name,
                    MaximumRecallsPerEncounter = y.MaximumRecallsPerEncounter,
                    FrequencyBetweenRecallsPerEncounter = y.FrequencyBetweenRecallsPerEncounter
                }).ToExtendedObservableCollection();

            var locations = PracticeRepository.ServiceLocations.Where(x => !x.IsArchived);
            viewModel.Locations = new ObservableCollection<NamedViewModel<int>>(locations.Select(y => new NamedViewModel<int> {Id = y.Id, Name = y.Name}));

            var doctors = PracticeRepository.Users.OrderBy(i => i.UserName).Where(u => u.IsSchedulable);
            viewModel.Resources = new ObservableCollection<NamedViewModel<int>>(doctors.Select(x => new NamedViewModel<int> { Id = x.Id, Name = x.UserName }));
            
            return viewModel;
        }

        public RecallPatientInfo LoadPatientInfo(long patientId)
        {
            var patient = PracticeRepository.Patients.WithId(patientId);

            if (patient == null)
            {
                return null;
            }

            var queryableFactory = PracticeRepository.AsQueryableFactory();
            queryableFactory.Load(patient,                                                             
                                  x => x.PatientPhoneNumbers);

            var viewModel = new RecallPatientInfo
            {
                PatientId = patient.Id,                
                PatientName = patient.DisplayName,
                BusinessPhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business) != null ? patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Business).ToString() : string.Empty,
                CellPhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell) != null ? patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Cell).ToString() : string.Empty,
                HomePhone = patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home) != null ? patient.PatientPhoneNumbers.WithPhoneNumberType(PatientPhoneNumberType.Home).ToString() : string.Empty,                
                DefaultResourceId = patient.DefaultUserId
            };

            return viewModel;
        }

        [UnitOfWork(AcceptChanges=true)]
        public virtual IEnumerable<int> Save(IEnumerable<RecallDetailViewModel> recallViewModels)
        {
            var resultIds = new List<int>();
            recallViewModels.ToList().ForEach(r => resultIds.Add(Save(r)));
            return resultIds;
        }

        private int Save(RecallDetailViewModel recallViewModel)
        {
            PatientRecall patientRecall;
            if (recallViewModel.RecallId.HasValue)
            {
                patientRecall = PracticeRepository.PatientRecalls.Single(x => x.Id == recallViewModel.RecallId);
            }
            else
            {
                patientRecall = new PatientRecall();
                MapViewModelToPatientRecall(recallViewModel, patientRecall);
            }
            PracticeRepository.Save(patientRecall);
            
            return patientRecall.Id;
        }

        public void CloseRecalls(IEnumerable<int> idsToClose)
        {
            var recallsToClose = PracticeRepository.PatientRecalls.Where(x => idsToClose.Contains(x.Id));

            foreach( var recall in recallsToClose)
            {
                recall.RecallStatus = RecallStatus.Closed;
                recall.SentOrClosedDateTime = DateTime.Now.ToClientTime();
                PracticeRepository.Save(recall);
            }
        }

        private static void MapViewModelToPatientRecall(RecallDetailViewModel recallViewModel, PatientRecall patientRecall)
        {            
            if (recallViewModel.RecallId.HasValue)
            {
                patientRecall.Id = recallViewModel.RecallId.Value;
            }
            else
            {
                patientRecall.RecallStatus = RecallStatus.Pending;
            }
            patientRecall.AppointmentTypeId = recallViewModel.AppointmentTypeId;
            patientRecall.DueDateTime = recallViewModel.RecallDate.GetValueOrDefault();
            patientRecall.PatientId = recallViewModel.PatientId;
            patientRecall.ServiceLocationId = recallViewModel.LocationId;
            patientRecall.UserId = recallViewModel.ResourceId;

        }
    }

    public interface IRecallsViewService
    {
        IEnumerable<RecallDisplayViewModel> LoadRecallHistory(int patientId);

        RecallDetailViewModel LoadRecallDetail(int recallId);

        RecallSelectListsViewModel GetRecallSelectLists();

        RecallPatientInfo LoadPatientInfo(long patientId);

        IEnumerable<int> Save(IEnumerable<RecallDetailViewModel> recallViewModels);

        void CloseRecalls(IEnumerable<int> idsToClose);
    }

    public class RecallDisplayViewModelMap : IMap<PatientRecall, RecallDisplayViewModel>
    {
        private readonly IEnumerable<IMapMember<PatientRecall, RecallDisplayViewModel>> _members;

        public RecallDisplayViewModelMap()
        {
            _members = this.CreateMembers(recall => new RecallDisplayViewModel
            {
                PatientId = recall.PatientId,
                RecallId = recall.Id,
                RecallStatus = recall.RecallStatus,
                Action = recall.RecallStatus.GetDescription(),
                ActionDate = recall.SentOrClosedDateTime.ToMMDDYYYYString("/"),
                AppointmentType = recall.AppointmentType.IfNotNull(x => x.Name, () => ""),
                DueDate = recall.DueDateTime.ToMMDDYYYYString("/"),
                Resource = recall.User.IfNotNull(x => x.UserName, () => "")
            }).ToArray();
        }

        public IEnumerable<IMapMember<PatientRecall, RecallDisplayViewModel>> Members
        {
            get { return _members; }
        }
    }

    public class RecallDetailViewModelMap : IMap<PatientRecall, RecallDetailViewModel>
    {
        private readonly IEnumerable<IMapMember<PatientRecall, RecallDetailViewModel>> _members;

        public RecallDetailViewModelMap()
        {
            _members = this.CreateMembers(recall => new RecallDetailViewModel
            {
                RecallId = recall.Id,
                PatientId = recall.PatientId,
                AppointmentTypeId = recall.AppointmentTypeId,
                LocationId = recall.ServiceLocationId,
                RecallDate = recall.DueDateTime,
                ResourceId = recall.UserId
            }).ToArray();
        }

        public IEnumerable<IMapMember<PatientRecall, RecallDetailViewModel>> Members
        {
            get { return _members; }
        }
    }
}
