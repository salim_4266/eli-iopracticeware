﻿using System;
using System.Collections.Generic;
using System.Linq;
using IO.Practiceware.Model;
using IO.Practiceware.Model.Auditing;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Notes;
using IO.Practiceware.Presentation.ViewServices.Notes;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq;

[assembly: Component(typeof(NoteViewService), typeof(INoteViewService))]

namespace IO.Practiceware.Presentation.ViewServices.Notes
{
    public interface INoteViewService
    {
        /// <summary>
        /// Load the comments.
        /// </summary>
        /// <param name="argument"></param>
        /// <returns></returns>
        NoteLoadInformation LoadNoteInformation(NoteLoadArguments argument);

        /// <summary>
        /// Load the notes for manage note screen.
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        NoteLoadInformation LoadManageNotesInformation(int patientId);

        /// <summary>
        /// Deleting the standard comments.
        /// </summary>
        /// <param name="noteTemplateId"></param>
        void DeleteCommentTemplate(int noteTemplateId);

        /// <summary>
        /// Saving standard note.
        /// </summary>
        /// <param name="noteTemplate"></param>
        /// <returns></returns>
        int SaveNoteTemplate(NameAndAbbreviationAndDetailViewModel noteTemplate);

        /// <summary>
        /// Saving and sending alerts.
        /// </summary>
        /// <param name="note"></param>
        /// <param name="arguments"></param>
        void SaveAndSendCommentAlert(NoteViewModel note, NoteLoadArguments arguments);

        /// <summary>
        /// Saving and sending bunch of alerts at the same time.
        /// </summary>
        /// <param name="argument"></param>
        /// <param name="notes"></param>
        void SaveAndSendMultipleCommentAlert(IList<NoteViewModel> notes, NoteLoadArguments argument);

        /// <summary>
        /// Deleting the patient comment.
        /// </summary>
        /// <param name="id"></param>
        void DeleteComment(int id);
    }

    public class NoteViewService : BaseViewService, INoteViewService
    {
        private IMapper<InvoiceComment, NoteViewModel> _invoiceCommentMapper;
        private IMapper<BillingServiceComment, NoteViewModel> _billingCommentMapper;
        private IMapper<PatientFinancialComment, NoteViewModel> _financialCommentMapper;
        private IMapper<Screen, NamedViewModel> _screenMapper;
        private readonly IAuditRepository _auditRepository;
        private readonly IUnitOfWorkProvider _unitOfWorkProvider;


        public NoteViewService(IAuditRepository auditRepository, IUnitOfWorkProvider unitOfWorkProvider)
        {
            _auditRepository = auditRepository;
            _unitOfWorkProvider = unitOfWorkProvider;
            InitMapper();
        }

        private void InitMapper()
        {
            var mapper = Mapper.Factory;
            _invoiceCommentMapper = mapper.CreateMapper<InvoiceComment, NoteViewModel>(item => new NoteViewModel
            {
                Id = item.Id,
                Value = item.Value,
                IsIncludedOnStatement = item.IncludeOnStatement,
                IsArchived = item.IsArchived,
                AlertableScreens = item.Alerts.SelectMany(a => a.Screens.Select(x => _screenMapper.Map(x))).ToExtendedObservableCollection(),
                ExpirationDateTime = item.Alerts.Any() ? item.Alerts.Select(x => x.ExpirationDateTime).FirstOrDefault() : null,
                AlertId = item.Alerts.Any() ? item.Alerts.Select(x => x.Id).FirstOrDefault() : (int?)null
            });
            _billingCommentMapper = mapper.CreateMapper<BillingServiceComment, NoteViewModel>(item => new NoteViewModel
            {
                Id = (int?)item.Id,
                Value = item.Value,
                IsIncludedOnStatement = item.IsIncludedOnStatement,
                IsArchived = item.IsArchived,
                AlertableScreens = item.Alerts.SelectMany(a => a.Screens.Select(x => _screenMapper.Map(x))).ToExtendedObservableCollection(),
                ExpirationDateTime = item.Alerts.Any() ? item.Alerts.Select(x => x.ExpirationDateTime).FirstOrDefault() : null,
                AlertId = item.Alerts.Any() ? item.Alerts.Select(x => x.Id).FirstOrDefault() : (int?)null
            });

            _financialCommentMapper = mapper.CreateMapper<PatientFinancialComment, NoteViewModel>(item => new NoteViewModel
            {
                Id = item.Id,
                Value = item.Value,
                IsIncludedOnStatement = item.IsIncludedOnStatement,
                IsArchived = item.IsArchived,
                AlertableScreens = item.Alerts.SelectMany(a => a.Screens.Select(x => _screenMapper.Map(x))).ToExtendedObservableCollection(),
                ExpirationDateTime = item.Alerts.Any() ? item.Alerts.Select(x => x.ExpirationDateTime).FirstOrDefault() : null,
                AlertId = item.Alerts.Any() ? item.Alerts.Select(x => x.Id).FirstOrDefault() : (int?)null
            });

            _screenMapper = mapper.CreateMapper<Screen, NamedViewModel>(item => new NamedViewModel
            {
                Name = item.Name,
                Id = item.Id
            });

        }

        public NoteLoadInformation LoadManageNotesInformation(int patientId)
        {
            var noteLoadInformation = new NoteLoadInformation();
            var patientFinancialComments = PracticeRepository.PatientFinancialComments.Include(x => x.Alerts.Select(a => a.Screens)).Where(x => x.PatientId == patientId).ToArray();
            var patientFinancialCommentIds = patientFinancialComments.Select(x => x.Id).ToArray();
            noteLoadInformation.Notes = patientFinancialComments.Select(x => _financialCommentMapper.Map(x)).ToExtendedObservableCollection();
            noteLoadInformation.Notes = GetNotesWithAuditEntry(noteLoadInformation.Notes, patientFinancialCommentIds);

            return PopulateStandardNotesAndScreens(noteLoadInformation);
        }

        public NoteLoadInformation LoadNoteInformation(NoteLoadArguments argument)
        {
            var noteLoadInformation = new NoteLoadInformation();
            //Loading notes window with reference to the patient invoice comment.
            if (argument.NoteId.HasValue && argument.NoteType == NoteType.PatientFinancialComment)
            {
                var patientFinancialComments = PracticeRepository.PatientFinancialComments.Include(x => x.Alerts.Select(a => a.Screens)).WithId(argument.NoteId.GetValueOrDefault().EnsureNotDefault());
                noteLoadInformation.Note = _financialCommentMapper.Map(patientFinancialComments);
            }
            //Loading notes window with reference to the invoice comment.
            if (argument.NoteId.HasValue && argument.NoteType == NoteType.InvoiceComment)
            {
                var invoiceComment = PracticeRepository.InvoiceComments.Include(x => x.Alerts.Select(a => a.Screens)).WithId(argument.NoteId.GetValueOrDefault().EnsureNotDefault());
                noteLoadInformation.Note = _invoiceCommentMapper.Map(invoiceComment);
            }
            //Loading notes window with reference to the billing comment.
            if (argument.NoteId.HasValue && argument.NoteType == NoteType.BillingServiceComment)
            {
                var billingServiceComment = PracticeRepository.BillingServiceComments.Include(x => x.Alerts.Select(a => a.Screens)).WithId(argument.NoteId.GetValueOrDefault().EnsureNotDefault());
                noteLoadInformation.Note = _billingCommentMapper.Map(billingServiceComment);
            }
            return PopulateStandardNotesAndScreens(noteLoadInformation);
        }

        private NoteLoadInformation PopulateStandardNotesAndScreens(NoteLoadInformation noteLoadInformation)
        {
            new Action[]
            {
                () => {
                    noteLoadInformation.AlertableScreens = PracticeRepository.Screens.Where(x => x.ScreenType == ScreenType.Financials).Select(s => new NamedViewModel { Id = s.Id, Name = s.Name }).ToExtendedObservableCollection();
                },
                 () => {
                     noteLoadInformation.StandardNotes = PracticeRepository.NoteTemplates.Select(s => new NameAndAbbreviationAndDetailViewModel { Name = s.Name, Detail = s.Value, Id = s.Id }).ToExtendedObservableCollection();
                }

            }.ForAllInParallel(a => a());

            return noteLoadInformation;
        }

        private ExtendedObservableCollection<NoteViewModel> GetNotesWithAuditEntry(IEnumerable<NoteViewModel> notes, IEnumerable<int> patientFinancialCommentIds)
        {
            var patientFinancialCommentAuditEntries = _auditRepository.AuditEntriesFor<PatientFinancialComment>(patientFinancialCommentIds).Include(i => i.AuditEntryChanges).ToArray();
            var userIds = patientFinancialCommentAuditEntries.Where(x => x.UserId.HasValue).Select(x => x.UserId).ToArray();
            var users = PracticeRepository.Users.Where(x => userIds.Contains(x.Id)).ToArray();
            return notes.ForEach(x => patientFinancialCommentAuditEntries.ForEach(y =>
            {
                if (y.KeyValues.Contains(x.Id.ToString()))
                {
                    x.CreatedByUser = users.Where(u => y.UserId.HasValue && u.Id == y.UserId).Select(u => new NamedViewModel { Name = u.UserName }).FirstOrDefault();
                    x.CreatedOnDateTime = y.AuditDateTime;
                }
            })).ToExtendedObservableCollection();
        }

        public void DeleteCommentTemplate(int noteTemplateId)
        {
            var note = PracticeRepository.NoteTemplates.First(nt => nt.Id == noteTemplateId);
            PracticeRepository.Delete(note);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void DeleteComment(int id)
        {
            var comment = PracticeRepository.PatientFinancialComments.Include(x => x.Alerts).First(nt => nt.Id == id);
            if (comment.Alerts != null)
            {
                comment.Alerts.Clear();
            }
            PracticeRepository.Delete(comment);
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual int SaveNoteTemplate(NameAndAbbreviationAndDetailViewModel noteTemplate)
        {
            using (var work = _unitOfWorkProvider.Create())
            {
                var noteNoteTemplate = noteTemplate.Id > 0 ? PracticeRepository.NoteTemplates.WithId(noteTemplate.Id) : new NoteTemplate();
                noteNoteTemplate.Name = noteTemplate.Name;
                noteNoteTemplate.Value = noteTemplate.Detail;
                PracticeRepository.Save(noteNoteTemplate);
                work.AcceptChanges();
                return noteNoteTemplate.Id;
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SaveAndSendCommentAlert(NoteViewModel note, NoteLoadArguments arguments)
        {

            var screens = PracticeRepository.Screens.ToList();
            PatientFinancialComment patientFinancialComment = null;
            BillingServiceComment billingServiceComment = null;
            InvoiceComment invoiceComment = null;
            var noteId = arguments.NoteId.HasValue ? arguments.NoteId.GetValueOrDefault().EnsureNotDefault() : (int?)null; //TO DO : check get value or default.
            var entityId = arguments.EntityId.HasValue ? arguments.EntityId.GetValueOrDefault().EnsureNotDefault() : (int?)null;
            if (arguments.NoteType == NoteType.InvoiceComment)
            {
                //Invoice comments
                invoiceComment = CreateInvoiceComment(note, noteId, entityId);
                PracticeRepository.Save(invoiceComment);
            }
            else if (arguments.NoteType == NoteType.BillingServiceComment)
            {
                //Billing service comments.
                billingServiceComment = CreateBillingServiceComment(note, noteId, entityId);
                PracticeRepository.Save(billingServiceComment);
            }
            else if (arguments.NoteType == NoteType.PatientFinancialComment)
            {
                //Patient financial comments
                patientFinancialComment = CreatePatientFinancialComment(note, noteId, entityId);
                PracticeRepository.Save(patientFinancialComment);
            }

            if (note.AlertableScreens.Count > 0)
            {
                //Sending the whole object as unit or work is not committed yet and there are no ids for the new comments.
                SaveAlert(note, patientFinancialComment, billingServiceComment, invoiceComment, screens);
            }
        }

        [UnitOfWork(AcceptChanges = true)]
        public virtual void SaveAndSendMultipleCommentAlert(IList<NoteViewModel> notes, NoteLoadArguments argument)
        {
            //Save comment and send alerts.
            notes.ForEach(x =>
            {
                //As SaveAndSendCommentAlert method runs on NoteId to edit the existing comment, so assigning for each instance.
                argument.NoteId = x.Id;
                SaveAndSendCommentAlert(x, argument);
            });
        }

        /// <summary>
        /// Return the Save Invoice comment object to save.
        /// </summary>
        /// <param name="note"></param>
        /// <param name="invoiceId"></param>
        /// <param name="invoiceCommentId"></param>
        /// <returns></returns>
        static string StrCommentValueForCompare;
        private InvoiceComment CreateInvoiceComment(NoteViewModel note, int? invoiceCommentId, int? invoiceId)
        {

            InvoiceComment invoiceComment;
            if (invoiceCommentId.HasValue)
            {
                invoiceComment = PracticeRepository.InvoiceComments.WithId(invoiceCommentId);
                StrCommentValueForCompare = invoiceComment.Value;

            }
            else
            {
                //Only new comment 
                invoiceComment = new InvoiceComment();
                // ReSharper disable once PossibleInvalidOperationException
                invoiceComment.InvoiceId = invoiceId.Value;
            }
            invoiceComment.IncludeOnStatement = note.IsIncludedOnStatement;
            invoiceComment.Value = note.Value;
            //This If condition when only edit comment then date will be change not for check box "include on statement"

            if (StrCommentValueForCompare != invoiceComment.Value.ToString())
            {
                invoiceComment.DateTime = DateTime.Now.ToClientTime();
            }
            invoiceComment.IsArchived = note.IsArchived;
            return invoiceComment;
        }

        /// <summary>
        /// Return the Save Billing Service comment object to save.
        /// </summary>
        /// <param name="note"></param>
        /// <param name="billingServiceId"></param>
        /// /// <param name="billingServiceCommentId"></param>
        /// <returns></returns>
        static string StrServiceCommentForCompare;
        private BillingServiceComment CreateBillingServiceComment(NoteViewModel note, int? billingServiceCommentId, int? billingServiceId)
        {

            BillingServiceComment billingServiceComment;
            if (billingServiceCommentId.HasValue)
            {
                billingServiceComment = PracticeRepository.BillingServiceComments.WithId(billingServiceCommentId.Value);
                StrServiceCommentForCompare = billingServiceComment.Value;
            }
            else
            {
                billingServiceComment = new BillingServiceComment();
                billingServiceComment.BillingServiceId = billingServiceId.GetValueOrDefault().EnsureNotDefault();
            }
            billingServiceComment.IsIncludedOnStatement = note.IsIncludedOnStatement;
            billingServiceComment.IsArchived = note.IsArchived;
            billingServiceComment.Value = note.Value;
           // billingServiceComment.DateTime = DateTime.Now.ToClientTime();
            if (StrServiceCommentForCompare != billingServiceComment.Value.ToString())
            {
                billingServiceComment.DateTime = DateTime.Now.ToClientTime();
            }

            return billingServiceComment;
        }

        /// <summary>
        /// Return the Save Billing Service comment object to save.
        /// This is also helpful when I will have to save the collection from manage note screen.
        /// </summary>
        /// <param name="note"></param>
        /// <param name="patientId"></param>
        /// <param name="patientFinancialCommentId"></param>
        /// <returns></returns>
        private PatientFinancialComment CreatePatientFinancialComment(NoteViewModel note, int? patientFinancialCommentId, int? patientId)
        {

            PatientFinancialComment patientFinancialComment;
            if (patientFinancialCommentId.HasValue)
            {
                patientFinancialComment = PracticeRepository.PatientFinancialComments.WithId(patientFinancialCommentId.Value);
            }
            else
            {
                patientFinancialComment = new PatientFinancialComment();
                patientFinancialComment.PatientId = patientId.GetValueOrDefault().EnsureNotDefault();
            }

            patientFinancialComment.IsIncludedOnStatement = note.IsIncludedOnStatement;
            patientFinancialComment.IsArchived = note.IsArchived;
            patientFinancialComment.Value = note.Value;
            return patientFinancialComment;
        }

        [UnitOfWork(AcceptChanges = true)]
        protected virtual void SaveAlert(NoteViewModel note, PatientFinancialComment patientFinancialComment, BillingServiceComment billingServiceComment, InvoiceComment invoiceComment, IList<Screen> screens)
        {
            var alert = note.AlertId.HasValue ? PracticeRepository.Alerts.Include(x => x.Screens).WithId(note.AlertId.Value) : new Alert();
            alert.PatientFinancialComment = patientFinancialComment;
            alert.BillingServiceComment = billingServiceComment;
            alert.InvoiceComment = invoiceComment;
            alert.ExpirationDateTime = note.ExpirationDateTime;
            alert.Screens.ToList().ForEach(x => alert.Screens.Remove(x));
            note.AlertableScreens.ForEach(x => alert.Screens.Add(screens.WithId(x.Id)));
            PracticeRepository.Save(alert);
        }
    }
}
