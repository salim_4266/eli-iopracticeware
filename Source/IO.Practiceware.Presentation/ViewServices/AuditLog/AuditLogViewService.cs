﻿using IO.Practiceware.Model;
using IO.Practiceware.Model.Auditing;
using IO.Practiceware.Presentation.ViewModels.AuditLog;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewServices.AuditLog;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Reflection;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Transactions;
using System.Windows.Media;
using System.Xml.Linq;
using LogEntry = IO.Practiceware.Model.Auditing.LogEntry;

[assembly: Component(typeof(AuditLogViewService), typeof(IAuditLogViewService))]
[assembly: Component(typeof(GetAuditAndLogEntriesResultToViewModelMap), AddAllServices = true)]

namespace IO.Practiceware.Presentation.ViewServices.AuditLog
{
    [DataContract]
    public enum OrderByTypeId
    {
        [EnumMember]
        DateTime = 0,
        [EnumMember]
        UserId = 1,
        [EnumMember]
        PatientId = 2
    }

    [DataContract]
    public class AuditLogInformation
    {
        [Dependency]
        [DataMember]
        public ObservableCollection<AuditLogViewModel> AuditLogs { get; set; }

        [DataMember]
        public long TotalCount { get; set; }
    }

    public interface IAuditLogViewService
    {
        /// <summary>
        /// Gets a populated filter view model
        /// </summary>
        /// <returns></returns>
        AuditLogFilterViewModel GetFilterViewModel();

        /// <summary>
        /// Gets audit logs for the page index and provides the total counts for log entries and audit entry changes
        /// </summary>
        /// <param name="auditFilter">The audit filter.</param>
        /// <param name="additionalFilters">The additional filters.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="includeTotalCount">if set to <c>true</c> [include total count].</param>
        /// <returns></returns>
        AuditLogInformation GetAuditLogs(AuditLogFilterViewModel auditFilter, string additionalFilters, int pageIndex, int pageSize, bool includeTotalCount);

        IEnumerable<string> GetAllAccessLocations();

        IEnumerable<string> GetAllSourceOfAccess();

        IEnumerable<string> GetAllAccessDevices();
    }

    [SupportsTransactionScopes]
    public class AuditLogViewService : BaseViewService, IAuditLogViewService
    {
        private readonly IAuditRepository _auditRepository;
        private readonly IMapper<GetAuditAndLogEntriesResult, AuditLogViewModel> _dbResultToViewModelMapper;
        private readonly Func<AuditLogFilterViewModel> _createAuditLogFilterViewModel;

        public AuditLogViewService(IAuditRepository auditRepository,
                                   IMapper<GetAuditAndLogEntriesResult, AuditLogViewModel> dbResultToViewModelMapper,
                                   Func<AuditLogFilterViewModel> createAuditLogFilterViewModel)
        {
            _auditRepository = auditRepository;
            _dbResultToViewModelMapper = dbResultToViewModelMapper;
            _createAuditLogFilterViewModel = createAuditLogFilterViewModel;
        }

        [TransactionScope(TransactionScopeOption.Suppress, IsolationLevel.ReadUncommitted)]
        public virtual AuditLogFilterViewModel GetFilterViewModel()
        {
            var viewModel = _createAuditLogFilterViewModel();

            new Action[]
                {
                    () => viewModel.ActionTypes = GetAccessTypesFilter(),                    
                    () => viewModel.DetailsChanged = GetDetailsChangedFilter(),
                    () => viewModel.Users = GetUsersFilter()
                }.ForAllInParallel(a => a());

            return viewModel;
        }

        public IEnumerable<string> GetAllAccessLocations()
        {
            return _auditRepository.LogEntries.Select(le => le.ServerName).Union(_auditRepository.AuditEntries.Select(ae => ae.ServerName)).OrderBy(i => i).Distinct().ToArray();
        }

        public IEnumerable<string> GetAllSourceOfAccess()
        {
            return _auditRepository.LogEntries.Select(le => le.ProcessName).Union(_auditRepository.AuditEntries.Select(ae => ae.ObjectName)).OrderBy(i => i).Distinct().ToArray();
        }

        public IEnumerable<string> GetAllAccessDevices()
        {
            return _auditRepository.LogEntries.Select(le => le.MachineName).Union(_auditRepository.AuditEntries.Select(ae => ae.HostName)).OrderBy(i => i).Distinct().ToArray();
        }


        [TransactionScope(TransactionScopeOption.Suppress, IsolationLevel.ReadUncommitted)]
        public virtual AuditLogInformation GetAuditLogs(AuditLogFilterViewModel auditFilter, string additionalFilters, int pageIndex, int pageSize, bool includeTotalCount)
        {
            var startDate = auditFilter.StartDate;
            var endDate = auditFilter.AdjustedEndDate;
            var userIds = auditFilter.IsFilteringByUsers ? auditFilter.SelectedUsers.Select(i => i.Id).GetXmlFormattedString() : null;
            var patientId = auditFilter.IsFilteringByPatientId ? auditFilter.PatientId : null;
            var detailsChanged = auditFilter.IsFilteringByDetailsChanged ? auditFilter.SelectedDetailsChanged.Select(i => i.Name).GetXmlFormattedString() : null;
            var logActionTypes = auditFilter.IsFilteringByActionTypes ? auditFilter.SelectedActionTypes.Where(at => !at.IsAuditAccessType).Select(i => i.Name).GetXmlFormattedString() : null;
            var auditActionTypes = auditFilter.IsFilteringByActionTypes ? auditFilter.SelectedActionTypes.Where(at => at.IsAuditAccessType).Select(i => i.Id).GetXmlFormattedString() : null;

            long? totalCount;
            var dbResults = _auditRepository.GetAuditAndLogEntries(startDate, endDate, userIds, patientId, detailsChanged, logActionTypes, auditActionTypes, pageSize, pageIndex, (int)auditFilter.OrderByTypeId, additionalFilters, includeTotalCount, out totalCount).ToArray();

            // converted GMT time to local time
            dbResults.IfNotNull(x =>
            {
                x.ForEach(log => {
                    log.DateTime = TimeZone.CurrentTimeZone.ToLocalTime(log.DateTime);
                });
            });

            //If actionType is present exculde Empty ActionTypes
            if (auditFilter.IsFilteringByActionTypes && auditFilter.SelectedActionTypes.Count > 0) dbResults = dbResults.Where(r => r.ActionMessage != null).ToArray();

            // Query for patients
            var patientIds = dbResults.GroupBy(r => r.PatientId).Where(i => i.Key.HasValue).Select(i => i.Key.EnsureNotDefault().Value);
            var patients = PracticeRepository.Patients.Where(p => patientIds.Contains(p.Id)).ToArray();

            // Query for Users
            var originalStringUserIdToUserMap = GetDbResultToUserMap(dbResults);

            // Create view models and populate them with patients/users
            var dbResultAndViewModels = dbResults.Select(r => new { DbResult = r, ViewModel = _dbResultToViewModelMapper.Map(r) });
            var result = new ExtendedObservableCollection<AuditLogViewModel>();
            foreach (var dbResultAndViewModel in dbResultAndViewModels)
            {
                var viewModel = dbResultAndViewModel.ViewModel;
                var dbResult = dbResultAndViewModel.DbResult;

                if (dbResult.PatientId != null)
                {
                    var patientEntity = patients.WithId((int)dbResult.PatientId.Value);
                    if (patientEntity != null)
                    {
                        viewModel.Patient = new NamedViewModel {Id = patientEntity.Id, Name = "{0}, {1}".FormatWith(patientEntity.LastName, patientEntity.FirstName)};
                    }
                }

                if (dbResult.UserId != null && Convert.ToInt32(dbResult.UserId) > 0)
                {
                    var userEntity = originalStringUserIdToUserMap.GetValue(dbResult.UserId);
                    viewModel.User = new NamedViewModel { Id = userEntity.Id, Name = userEntity.DisplayName };
                }
                result.Add(viewModel);
            }

            return new AuditLogInformation { AuditLogs = result, TotalCount = totalCount.GetValueOrDefault() };
        }

        private Dictionary<string, User> GetDbResultToUserMap(IEnumerable<GetAuditAndLogEntriesResult> results)
        {
            var dbResults = results.ToArray();
            var userIdsToOriginalStringValues = dbResults.GroupBy(r => r.UserId).Where(i => i.Key.IsNumeric()).ToDictionary(i => int.Parse(i.Key), j => j.Key);
            return PracticeRepository.Users.Where(p => userIdsToOriginalStringValues.Keys.Contains(p.Id))
                                           .ToDictionary(i => userIdsToOriginalStringValues[i.Id]);
        }

        private ObservableCollection<NamedViewModel> GetDetailsChangedFilter()
        {
            return _auditRepository.AuditEntryChanges.Select(aec => aec.ColumnName)
                                                     .Distinct().ToArray()
                                                     .Select((cn, index) => new NamedViewModel { Id = index + 1, Name = cn })
                                                     .OrderBy(i => i.Name)
                                                     .ToExtendedObservableCollection();
        }

        private ObservableCollection<ActionTypeViewModel> GetAccessTypesFilter()
        {   
            var logEntryAccessTypes = GetGroupFilterName();
            return logEntryAccessTypes.ToExtendedObservableCollection();
        }

        /// <summary>
        /// Get Group Name based on Event
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<ActionTypeViewModel> GetGroupFilterName()
        {
            System.Data.DataTable dtGroupCategory = new System.Data.DataTable();
            ObservableCollection<ActionTypeViewModel> GroupCategory = new ObservableCollection<ActionTypeViewModel>();
            string _SelectQry = "SELECT  GroupName, 1 AS  Id  FROM  [dbo].[GroupFilterCategory] GROUP BY  GroupName";
            ActionTypeViewModel groupName;
            using (var dbConnection = IO.Practiceware.Data.DbConnectionFactory.PracticeRepository)
            {
                dbConnection.Open();
                using (System.Data.IDbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = _SelectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtGroupCategory.Load(reader);
                    }
                }
                dbConnection.Close();
            }
            foreach (System.Data.DataRow row in dtGroupCategory.Rows)
            {
                groupName = new ActionTypeViewModel()
                {
                    Id = (int)row["Id"],
                    Name = row["GroupName"].ToString()
                };
                GroupCategory.Add(groupName);
            }
            return GroupCategory;
        }

        private ObservableCollection<NamedViewModel> GetUsersFilter()
        {
            return PracticeRepository.Users.Select(u => new NamedViewModel { Id = u.Id, Name = u.DisplayName }).OrderBy(i => i.Name).ToExtendedObservableCollection();
        }
    }

    public static class AuditLogViewServiceExtensions
    {
        public static long? GetPatientId(this LogEntry source)
        {
            long patientId;
            var patientIdProperty = source.Properties.FirstOrDefault(p => p.Name.Equals("PatientId", StringComparison.InvariantCultureIgnoreCase));
            if (patientIdProperty != null && long.TryParse(patientIdProperty.Value, out patientId))
            {
                return patientId;
            }
            return null;
        }

        public static int? GetUserId(this LogEntry source)
        {
            if (source.UserId.IsNotNullOrEmpty())
            {
                int userId;
                var startIndex = source.UserId.IndexOf(i => i != '0' && i != '-');
                var userIdString = source.UserId.Substring(startIndex, source.UserId.Length - startIndex);

                if (int.TryParse(userIdString, out userId))
                {
                    return userId;
                }
            }
            return null;
        }

        public static string GetXmlFormattedString(this IEnumerable<int> ids)
        {
            var enumerable = ids as int[] ?? ids.ToArray();
            if (enumerable.IsNullOrEmpty()) return null;

            var rootElement = new XElement("Ids");
            enumerable.ForEachWithSinglePropertyChangedNotification(i => rootElement.Add(new XElement("Id") { Value = i.ToString() }));
            return rootElement.ToString();
        }

        public static string GetXmlFormattedString(this IEnumerable<string> values)
        {
            var enumerable = values as string[] ?? values.ToArray();
            if (enumerable.IsNullOrEmpty()) return null;

            var rootElement = new XElement("Values");
            enumerable.ForEachWithSinglePropertyChangedNotification(i => rootElement.Add(new XElement("Value") { Value = i }));
            return rootElement.ToString();
        }

        public static ContentType GetContentType(this GetAuditAndLogEntriesResult result)
        {
            if (result.ContentTypeId.HasValue) return (ContentType)result.ContentTypeId.Value;

            if (!result.IsLogEntry)
            {
                // ReSharper disable RedundantArgumentDefaultValue
                // Need to define optional parameters when used in expression tree (mapper)
                return PracticeRepository.GetEntityType(result.SourceOfAccess).IfNotNull(t => t.GetProperty(result.DetailChanged).IfNotNull(p => p.GetAttribute<ContentTypeAttribute>(true, true).IfNotNull(a => (ContentType?)a.ContentType, () => null), () => null), () => null) ?? ContentType.Text;
                // ReSharper restore RedundantArgumentDefaultValue
            }
            return ContentType.Text;
        }
    }

    public class GetAuditAndLogEntriesResultToViewModelMap : IMap<GetAuditAndLogEntriesResult, AuditLogViewModel>
    {
        public GetAuditAndLogEntriesResultToViewModelMap()
        {
            Members = this.CreateMembers(i => new AuditLogViewModel
                {
                    AccessDevice = i.AccessDevice,
                    AccessLocation = i.AccessLocation,
                    ActionMessage = i.ActionMessage,
                    ContentType = i.GetContentType(),
                    DateTime = i.DateTime,
                    DetailChanged = i.DetailChanged,
                    NewValue = i.NewValue,
                    OldValue = i.OldValue,
                    SourceOfAccess = i.SourceOfAccess,
                    Color = i.IsLogEntry ? i.ActionMessage.IfNotNull(am => am.Contains(Logging.LogCategory.AuditLog)) ? Colors.Gray : Colors.LightBlue
                                         : Colors.LightGreen,
                    // Intentionally leave out Patient since we only have PatientId
                    // Intentionally leave out User since we only have UserId
                });
        }

        public IEnumerable<IMapMember<GetAuditAndLogEntriesResult, AuditLogViewModel>> Members { get; private set; }
    }
}
