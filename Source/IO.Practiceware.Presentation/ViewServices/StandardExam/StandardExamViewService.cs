﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using IO.Practiceware.Configuration;
using IO.Practiceware.Data;
using IO.Practiceware.Model;
using IO.Practiceware.Presentation.ViewModels.Common;
using IO.Practiceware.Presentation.ViewModels.Financials.Common.Provider;
using IO.Practiceware.Presentation.ViewModels.StandardExam;
using IO.Practiceware.Presentation.ViewServices.StandardExam;
using IO.Practiceware.Services.ApplicationSettings;
using IO.Practiceware.Services.Icd;
using IO.Practiceware.Storage;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Linq;
using System.ComponentModel;
using System.Windows.Data;
using IO.Practiceware.Presentation.ViewServices.PatientInfo;
using IO.Practiceware.Presentation.ViewModels.PatientInfo;
using System.ComponentModel.DataAnnotations;
using IO.Practiceware.Presentation.ViewModels.PatientInfo.PatientInsurance;
using IO.Practiceware.Presentation.ViewModels.InsurancePlansSetup;

[assembly: Component(typeof(StandardExamViewService), typeof(IStandardExamViewService))]
[assembly: Component(typeof(PatientInfoViewModelMap), typeof(IMap<Patient, PatientInfoViewModel>))]
namespace IO.Practiceware.Presentation.ViewServices.StandardExam
{

    public interface IStandardExamViewService
    {
        DataListViewModel LoadInformation(int patientId, int encounterId);
        bool IsDefaultDiagnosisTypeIcd10(int patientId);
        InvoiceInfoViewModel LoadInvoiceTypes();
        ObservableCollection<DiagnosisViewModel> LoadBillingDiagnosis(int? patientId, int? encounterId, bool isIcd10Mode, List<DiagnosesLoadArguments> diagnoses, string[] diagCodes, bool isToggled, out Dictionary<string, string> watermark);
        ObservableCollection<ServiceViewModel> GetBillingService(bool isIcd10Mode, ObservableCollection<DiagnosisViewModel> diagnosis, string[] fileNames);
        ObservableCollection<DiagnosisCodeViewModel> LoadIcdCodes(bool isIcd10Mode);
        ObservableCollection<NameAndAbbreviationViewModel> LoadModifierCodes();
        ObservableCollection<ServiceCodeViewModel> LoadOfficeVisits();

        ObservableCollection<ServiceViewModel> GetOfficeVisit(string encounterCode);
        ObservableCollection<ServiceCodeViewModel> LoadAllBillingServices();
        ServiceViewModel GetServiceUnitCharge(ServiceViewModel service);
        bool IsDiagnosisLinked(string serviceCode, string diagnosisCode);
    }

    public class StandardExamViewService : BaseViewService, IStandardExamViewService
    {
        private readonly Func<AppointmentInfoViewModel> _appointmentInfoViewModel;
        private readonly Func<InvoiceInfoViewModel> _invoiceInfoViewModel;
        private IMapper<Tuple<int, string>, NamedViewModel> _namedViewModelMapper;
        private readonly Func<ObservableCollection<DiagnosisViewModel>> _createDiagnosisViewModelCollection;
        private readonly Func<DiagnosisViewModel> _createDiagnosisViewModel;
        private readonly Func<ObservableCollection<DiagnosisCodeViewModel>> _createDiagnosisCodeViewModelCollection;
        private readonly Func<ObservableCollection<ServiceCodeViewModel>> _createServiceCodeViewModelCollection;
        private readonly Func<DiagnosisCodeViewModel> _createDiagnosisCodeViewModel;
        private readonly Func<ObservableCollection<ServiceViewModel>> _createServiceViewModelCollection;
        private readonly Func<ServiceViewModel> _createServiceViewModel;
        private readonly Func<ServiceCodeViewModel> _createServiceCodeViewModel;
        private readonly Func<DiagnosisLinkViewModel> _createDiagnosisLinkViewModel;
        private readonly Func<ObservableCollection<NameAndAbbreviationViewModel>> _createNameAndAbbreviationViewModelCollection;
        private readonly Func<NameAndAbbreviationViewModel> _createNameAndAbbreviationViewModel;
        private readonly Func<ObservableCollection<DiagnosisLinkViewModel>> _createDiagnosisLinkViewModelCollection;
        private readonly IIcd9To10Service _icd9To10Service;
        public string[] Alpha = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L" };
        public string[] Colors = { "#FF00CD7F", "#FF00CDCD", "#FF00AFFF", "#FF6478FF", "#FFBB78FF", "#FFF57AE8", "#FFDE6498", "#FFFA7776", "#FFEA7460", "#FFFDA46C", "#FFE5B844", "#FFC0D674" };
        private readonly ICloner _cloner;

        private IMapper<EncounterService, ServiceCodeViewModel> _encounterServiceMapper;
        private readonly IMapper<Patient, PatientInfoViewModel> _patientInfoMapper;
        public StandardExamViewService(Func<AppointmentInfoViewModel> appointmentInfoViewModel,
            Func<InvoiceInfoViewModel> invoiceInfoViewModel,
            Func<ObservableCollection<DiagnosisViewModel>> createDiagnosisViewModelCollection,
            Func<DiagnosisViewModel> createDiagnosisViewModel,
            Func<ObservableCollection<DiagnosisCodeViewModel>> createDiagnosisCodeViewModelCollection,
            Func<DiagnosisCodeViewModel> createDiagnosisCodeViewModel,
            Func<ServiceViewModel> createServiceViewModel,
            Func<ObservableCollection<ServiceViewModel>> createServiceViewModelCollection,
            Func<ServiceCodeViewModel> createServiceCodeViewModel,
            Func<ObservableCollection<ServiceCodeViewModel>> createServiceCodeViewModelCollection,
            Func<DiagnosisLinkViewModel> createDiagnosisLinkViewModel,
            Func<ObservableCollection<DiagnosisLinkViewModel>> createDiagnosisLinkViewModelCollection,
            Func<ObservableCollection<NameAndAbbreviationViewModel>> createNameAndAbbreviationViewModelCollection,
            Func<NameAndAbbreviationViewModel> createNameAndAbbreviationViewModel,

            IIcd9To10Service icd9To10Service, IMapper<EncounterService, ServiceCodeViewModel> encounterServiceMapper, ICloner cloner,
            IMapper<Patient, PatientInfoViewModel> patientInfoMapper)
        {
            _appointmentInfoViewModel = appointmentInfoViewModel;
            _invoiceInfoViewModel = invoiceInfoViewModel;
            _createDiagnosisViewModelCollection = createDiagnosisViewModelCollection;
            _createDiagnosisViewModel = createDiagnosisViewModel;
            _createDiagnosisCodeViewModelCollection = createDiagnosisCodeViewModelCollection;
            _createDiagnosisCodeViewModel = createDiagnosisCodeViewModel;
            _createServiceViewModel = createServiceViewModel;
            _createServiceViewModelCollection = createServiceViewModelCollection;
            _createServiceCodeViewModel = createServiceCodeViewModel;
            _createDiagnosisLinkViewModel = createDiagnosisLinkViewModel;
            _createDiagnosisLinkViewModelCollection = createDiagnosisLinkViewModelCollection;
            _icd9To10Service = icd9To10Service;
            _createNameAndAbbreviationViewModelCollection = createNameAndAbbreviationViewModelCollection;
            _createNameAndAbbreviationViewModel = createNameAndAbbreviationViewModel;
            _createServiceCodeViewModelCollection = createServiceCodeViewModelCollection;
            _encounterServiceMapper = encounterServiceMapper;
            _cloner = cloner;
            _patientInfoMapper = patientInfoMapper;

            InitMappers();
        }

        private static readonly IMapper<Model.PatientInsurance, PolicyViewModel> InsuranceMapper =
         Mapper.Factory.CreateMapper<Model.PatientInsurance, PolicyViewModel>(pi => new PolicyViewModel
         {
             InsuranceType = pi.InsuranceType,
             Copay = pi.InsurancePolicy.Copay,
             InsurancePlan = new InsurancePlanViewModel
             {
                 InsuranceName = string.Format("{0}{1}", pi.InsurancePolicy.Insurer.Name, pi.InsurancePolicy.Insurer.PlanName.IsNullOrWhiteSpace() || pi.InsurancePolicy.Insurer.PlanName.Equals(pi.InsurancePolicy.Insurer.Name, StringComparison.OrdinalIgnoreCase) ? string.Empty : " - " + pi.InsurancePolicy.Insurer.PlanName),
                 PlanType = pi.InsurancePolicy.Insurer.InsurerPlanType.IfNotNull(x => x.Name),
                 DiagnosisType = pi.InsurancePolicy.Insurer.DiagnosisTypeId == null ? ((DiagnosisTypeId)Convert.ToInt32(ApplicationSettings.Cached.GetSetting<string>(ApplicationSetting.DefaultDiagnosisTypeId).IfNotNull(x => x.Value))).GetDisplayName() : ((DiagnosisTypeId)pi.InsurancePolicy.Insurer.DiagnosisTypeId).GetDisplayName(),
                 Id = pi.InsurancePolicy.Insurer.Id
             },
             PolicyRank = pi.OrdinalId,
             InsurerClaimForm = pi.InsurancePolicy.Insurer.InsurerClaimForms.Select(x => new InsurerClaimFormViewModel
             {
                 InvoiceType = new NamedViewModel
                 {
                     Id = (int)x.InvoiceType,
                     Name = x.InvoiceType.GetDisplayName()
                 },
                 InsurerId = x.InsurerId
             }).ToObservableCollection(),

             Relationship = pi.PolicyholderRelationshipType
         });

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public DataListViewModel LoadInformation(int patientId, int encounterId)
        {
            var appointment = PracticeRepository.Appointments.OfType<UserAppointment>().Single(x => x.Id == encounterId);

            PracticeRepository.AsQueryableFactory().Load(appointment,
                x => x.Encounter,
                x => x.Encounter.Patient,
                x => x.Encounter.Patient.PatientTags.Select(y => y.Tag),
                x => x.Encounter.Patient.PatientPhoneNumbers,
                x => x.Encounter.Patient.PatientComments,
                x => x.Encounter.Patient.PatientInsurances,           
                x => x.Encounter.Patient.PatientInsurances.Select(ins => new
        {
                    InsurancePolicy = ins.InsurancePolicy.Insurer
                }),
                x => x.Encounter.Patient.PatientInsurances.Select(y => y.InsurancePolicy.Insurer.InsurerAddresses),
                x => x.Encounter.Patient.PatientInsurances.Select(y => y.InsurancePolicy.Insurer.InsurerClaimForms),
                x => x.Encounter.Patient.PatientInsurances.Select(y => y.InsurancePolicy.PolicyholderPatient),
                x => x.Encounter.ServiceLocation,
                x => x.AppointmentType,
                x => x.AppointmentType.AppointmentCategory,
                x => x.User,
                x => x.User.Providers
                );

            var user = appointment.User != null ? appointment.User : new User();
            var serviceLocationId = appointment.Encounter.ServiceLocationId;
            var scheduledDate = appointment.Encounter.StartDateTime;
            var appointmentType = appointment.AppointmentType != null ? appointment.AppointmentType : new AppointmentType();

            PatientInfoViewModel patientInfoViewModel = new PatientInfoViewModel();
            var appointmentInfoViewModel = _appointmentInfoViewModel();
            bool defaultDiagnosisType = false;
            var invoiceInfoViewModel = _invoiceInfoViewModel();
            new Action[]
            {
                () => { appointmentInfoViewModel.SchedulingProvider = new NamedViewModel {Id = user.Id, Name = user.UserName};},
                () =>
                {
                    var userProviders = PracticeRepository.Providers.Where(x => x.UserId == user.Id && x.User != null && !x.User.IsArchived && x.IsBillable)
                         .OrderBy(x => x.User.OrdinalId)
                         .ThenBy(x => x.Id)
                         .Select(x => new ProviderViewModel
                         {
                             Provider = new NamedViewModel
                             {
                                 Id = x.User.Id,
                                 Name = x.User.UserName
                             },
                            BillingOrganization = new NamedViewModel {Id = x.BillingOrganization.Id, Name = x.BillingOrganization.ShortName},
                             IsProviderUser = true,
                             Id = x.Id
                         })
                         .ToExtendedObservableCollection();
                    var serviceLocationProviders = PracticeRepository.Providers.Where(x => x.UserId == user.Id && x.User == null && x.ServiceLocation != null)
                        .OrderBy(x => x.ServiceLocation.ShortName)
                        .Select(x => new ProviderViewModel
                        {
                            Provider = new NamedViewModel
                            {
                                Id = x.ServiceLocation.Id,
                                Name = x.ServiceLocation.ShortName
                            },
                            BillingOrganization = new NamedViewModel {Id = x.BillingOrganization.Id, Name = x.BillingOrganization.ShortName},
                            IsProviderUser = false,
                            Id = x.Id
                        })
                        .ToExtendedObservableCollection();
                    appointmentInfoViewModel.RenderingProvider = userProviders.AddRange(serviceLocationProviders).FirstOrDefault();
                 },
                () => { appointmentInfoViewModel.ServiceLocation = new NamedViewModel {Id = appointment.Encounter.ServiceLocation.Id, Name = appointment.Encounter.ServiceLocation.ShortName}; },
                () => { appointmentInfoViewModel.ScheduleDate = scheduledDate; },
                () =>
                {
                    appointmentInfoViewModel.AppointmentType = new AppointmentTypeViewModel
                                                               {
                                                                   CategoryId = appointmentType.AppointmentCategoryId,
                                                                   Id = appointmentType.Id,
                                                                   Name = appointmentType.Name,
                                                                   IsFirstVisit=  appointmentType.IsFirstVisit
                                                               };
                },

                () => 
            { 
               patientInfoViewModel = _patientInfoMapper.Map(appointment.Encounter.Patient);                               
               patientInfoViewModel.Insurances = InsuranceMapper.MapAll(appointment.Encounter.Patient.PatientInsurances.Where(z=>z.IsDeleted==false && (z.EndDateTime>=DateTime.Now || z.EndDateTime==null))).OrderBy(p => p.InsuranceType.GetAttribute<DisplayAttribute>().Order).ThenBy(p => p.PolicyRank).ToObservableCollection();
               patientInfoViewModel.Insurances.OrderByInsuranceTypeThenRank().ApplyIndexToRank();
            },

            () =>
            {
                Insurer insurer = appointment.Encounter.Patient.PatientInsurances.Where(pi => pi.OrdinalId == 1 && pi.IsDeleted == false).Select(pi => pi.InsurancePolicy.Insurer).FirstOrDefault();
            if (insurer != null && insurer.DiagnosisTypeId != null)
            {
                defaultDiagnosisType = (insurer.DiagnosisTypeId == (int)DiagnosisTypeId.Icd10);
                }
            else
            {
                defaultDiagnosisType = (ApplicationSettings.Cached.GetSetting<string>(ApplicationSetting.DefaultDiagnosisTypeId).Value == Convert.ToString((int)DiagnosisTypeId.Icd10));
            }
            },

            () =>
            {
                invoiceInfoViewModel.InvoiceType = Enums.GetValues<InvoiceType>().Select(p => new Tuple<int, string>((int)p, p.GetDisplayName())).Select(_namedViewModelMapper.Map).ToExtendedObservableCollection();
                }
            }.ForAllInParallel(a => a());

            return new DataListViewModel
             {
                 DefaultDiagnosisType = defaultDiagnosisType,
                 AppointmentInfo = appointmentInfoViewModel,
                 PatientInfo = patientInfoViewModel,
                 InvoiceInfo = invoiceInfoViewModel
             };
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public InvoiceInfoViewModel LoadInvoiceTypes()
        {
            var invoiceInfoViewModel = _invoiceInfoViewModel();

            invoiceInfoViewModel.InvoiceType = Enums.GetValues<InvoiceType>()
                .Select(p => new Tuple<int, string>((int)p, p.GetDisplayName()))
                .Select(_namedViewModelMapper.Map)
                .ToExtendedObservableCollection();

            return invoiceInfoViewModel;
        }


        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public ObservableCollection<ServiceCodeViewModel> LoadOfficeVisits()
        {
            const string loadFavoritesQuery = " SELECT CPT,Description FROM PracticeFavorites pf " +
                                              " INNER JOIN dm.ICD9CPTLinkTable lnk on lnk.CPT = pf.Diagnosis " +
                                              " WHERE pf.System = '?' AND lnk.EM = 'T' AND lnk.Extra > 0 AND lnk.Rank = 1 " +
                                              " ORDER BY pf.Rank ASC ";

            const string getallEmProcQuery = " SELECT CPT,Description FROM dm.ICD9CPTLinkTable WHERE (EM = 'T') And (Extra > 0) And CPT >= '' And Rank =  1 ORDER BY Extra ASC, CPT ASC ";

            var officeVisits = new List<ServiceCodeViewModel>();

            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                var favoritesProc = dbConnection.Execute<DataTable>(loadFavoritesQuery);

                if (favoritesProc != null)
                {
                    officeVisits.AddRange(from DataRow favoriteProc in favoritesProc.Rows select new ServiceCodeViewModel { Code = favoriteProc["CPT"] as string, Description = favoriteProc["Description"] as string });
                }

                var allEmProc = dbConnection.Execute<DataTable>(getallEmProcQuery);
                if (allEmProc != null)
                {
                    foreach (DataRow proc in allEmProc.Rows)
                    {
                        bool procFound = false;

                        if (favoritesProc != null)
                        {
                            if (favoritesProc.Rows.Cast<DataRow>().Any(favProc => proc["CPT"].ToString() == favProc["CPT"].ToString()))
                            {
                                procFound = true;
                            }
                        }
                        if (!procFound) officeVisits.Add(new ServiceCodeViewModel { Code = proc["CPT"] as string, Description = proc["Description"] as string });
                    }
                }
            }

            return officeVisits.ToObservableCollection();
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public ObservableCollection<ServiceViewModel> GetOfficeVisit(string encounterCode)
        {
            EncounterService encounterService = null;

            if (!string.IsNullOrEmpty(encounterCode)) encounterService = PracticeRepository.EncounterServices.FirstOrDefault(i => i.Code == encounterCode);

            var officeVisits = new ObservableCollection<ServiceViewModel>();

            var serviceViewModel = new ServiceViewModel
            {
                AllowableAmount = 0,
                Code = new ServiceCodeViewModel(),
                DiagnosisLinks = new ObservableCollection<DiagnosisViewModel>(),
                IsOfficeVisit = true,
                IsQueuedToBill = true,
                Modifiers = new ObservableCollection<NameAndAbbreviationViewModel>(),
                UnitCharge = 0,
                Units = 1
            };

            if (encounterService != null)
            {
                serviceViewModel.Code = new ServiceCodeViewModel { Description = encounterService.Description, Code = encounterService.Code };
                serviceViewModel.UnitCharge = encounterService.UnitFee;
                serviceViewModel.Units = encounterService.DefaultUnit;
            }

            officeVisits.Add(serviceViewModel);

            return officeVisits;
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public ObservableCollection<DiagnosisViewModel> LoadBillingDiagnosis(int? patientId, int? encounterId, bool isIcd10Mode, List<DiagnosesLoadArguments> diagnoses, string[] diagCodes, bool isToggled, out Dictionary<string, string> watermark)
        {
            watermark = new Dictionary<string, string>();
            var diagnosisViewModels = _createDiagnosisViewModelCollection();
            if (diagnoses != null)
            {
                ObservableCollection<Icd9To10Model> icd9To10Mappings = null;
                if (isIcd10Mode)
                {
                    var diagTotalCount = diagnoses.Count();
                    var icd9Models = new ObservableCollection<IcdModel>();

                    for (int j = 0; j <= diagTotalCount - 1; j++)
                    {
                        var diagnosis = diagnoses[j];
                        var diagnosisCodeModel = _createDiagnosisCodeViewModel();
                        var code = diagnosis.DiagnosisCode;
                        int p = code.IndexOf('.') + 1;
                        if (p > 0)
                        {
                            int q = code.Substring(p, code.Length - p).IndexOf('.');
                            if (q > 0)
                            {
                                code = code.Substring(0, p + q);
                            }
                        }
                        diagnosisCodeModel.Code = code;
                        diagnosisCodeModel.Description = diagnosis.DiagnosisDescription;

                        var icd9 = new IcdModel();
                        icd9.Code = diagnosis.DiagnosisCode;
                        icd9.Description = diagnosis.DiagnosisDescription;
                        icd9.EyeContext = diagnosis.EyeContext;
                        icd9.EyeLidLocation = diagnosis.EyeLidLocation;
                        icd9Models.Add(icd9);

                    }
                    //Calling API request & response
                    //Get all ICD10 Codes at time
                    icd9To10Mappings = _icd9To10Service.GetBulkICD9to10Models(icd9Models).ToObservableCollection();
                }


                int i = 1;
                foreach (DiagnosesLoadArguments param in diagnoses)
                {
                    var diagnosisCodeModel = _createDiagnosisCodeViewModel();
                    var code = param.DiagnosisCode;
                    int p = code.IndexOf('.') + 1;
                    if (p > 0)
                    {
                        int q = code.Substring(p, code.Length - p).IndexOf('.');
                        if (q > 0)
                        {
                            code = code.Substring(0, p + q);
                        }
                    }
                    diagnosisCodeModel.Code = code;
                    diagnosisCodeModel.Description = param.DiagnosisDescription;

                    var icd9 = new IcdModel();
                    icd9.Code = param.DiagnosisCode;
                    icd9.Description = param.DiagnosisDescription;
                    icd9.EyeContext = param.EyeContext;
                    icd9.EyeLidLocation = param.EyeLidLocation;

                    DiagnosisViewModel toAdd = _createDiagnosisViewModel();
                    toAdd.Id = i;
                    toAdd.OrdinalId = i - 1;
                    toAdd.Icd9Code = diagnosisCodeModel;
                    toAdd.DiagnosisCodes = new ObservableCollection<DiagnosisCodeViewModel>();

                    int hasmultipleItems = 0;
                    if (isIcd10Mode) // ICD10 API calling here
                    {
                        if (icd9To10Mappings.Any())
                        {
                            var mappings = icd9To10Mappings.Where(icd => icd.Icd9.Code == icd9.Code && icd.Icd9.EyeContext == icd9.EyeContext).SelectMany(x => x.Icd10Mappings);
                            var icd10DiagnosisCodes = LoadDiagnosisCodes(mappings);
                            hasmultipleItems = icd10DiagnosisCodes.Select(c => c.ToList()).Count();

                            foreach (var item in icd10DiagnosisCodes)
                            {
                                if (hasmultipleItems > 1) //To make separate group call such as in bialateral case where All other will be return
                                {
                                    var j = i + 1;
                                    DiagnosisViewModel toAddnew = _createDiagnosisViewModel();
                                    toAddnew.Id = j;
                                    toAddnew.OrdinalId = j - 1;
                                    toAddnew.Icd9Code = diagnosisCodeModel;
                                    toAddnew.DiagnosisCodes = new ObservableCollection<DiagnosisCodeViewModel>();

                                    foreach (var item1 in item)
                                    {
                                        toAddnew.DiagnosisCodes.Add(item1);
                                        toAddnew.Icd9Code.CodeNL = icd9.Code;
                                        toAddnew.Icd9Code.Side = item1.Side;
                                    }

                                    if (toAddnew.DiagnosisCodes.Any() && toAddnew.DiagnosisCodes.Count == 1)
                                        toAddnew.Code = toAddnew.DiagnosisCodes.First();
                                    else if (toAddnew.DiagnosisCodes.Any() && toAddnew.DiagnosisCodes.Count > 1)
                                    {
                                        string side = string.Empty;
                                        if (toAddnew.DiagnosisCodes[0].Side.ToLower() == "right")
                                        {
                                            side = "OD";
                                        }
                                        else if (toAddnew.DiagnosisCodes[0].Side.ToLower() == "left")
                                        {
                                            side = "OS";
                                        }
                                        else
                                        {
                                            side = param.EyeContext;
                                        }

                                        //add subgrouping one to many watermark description
                                        toAddnew.DiagnosisCodeWaterMark = string.IsNullOrEmpty(side) ? toAddnew.DiagnosisCodes[0].Lingo : side + " - " + toAddnew.DiagnosisCodes[0].Lingo;
                                        if (diagCodes != null)
                                        {
                                            if (diagCodes.IsNotNullOrEmpty())
                                            {
                                                foreach (var diagCode in diagCodes)
                                                {
                                                    toAddnew.Code = toAddnew.DiagnosisCodes.FirstOrDefault(r => r.Code == diagCode.Substring(0, diagCode.IndexOf("-")) && !(diagnosisViewModels.Any(s => s.Code != null && s.Code.Code == diagCode.Substring(0, diagCode.IndexOf("-")))));
                                                    if (toAddnew.Code != null)
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                    else
                                        toAddnew.Code = null;
                                    diagnosisViewModels.Add(toAddnew);
                                }
                                else // Normal single Right / Left or Bilateral call
                                {
                                    foreach (var item1 in item)
                                    {
                                        toAdd.DiagnosisCodes.Add(item1);
                                        toAdd.Icd9Code.CodeNL = icd9.Code;
                                        toAdd.Icd9Code.Side = item1.Side;
                                    }
                                }
                            }

                        }

                        if (toAdd.DiagnosisCodes.Any() && toAdd.DiagnosisCodes.Count == 1)
                            toAdd.Code = toAdd.DiagnosisCodes.First();
                        else if (toAdd.DiagnosisCodes.Any() && toAdd.DiagnosisCodes.Count > 1)
                        {
                            string side = string.Empty;
                            if (toAdd.DiagnosisCodes[0].Side.ToLower() == "right")
                            {
                                side = "OD";
                            }
                            else if (toAdd.DiagnosisCodes[0].Side.ToLower() == "left")
                            {
                                side = "OS";
                            }
                            else
                            {
                                side = param.EyeContext;
                            }
                            toAdd.DiagnosisCodeWaterMark = string.IsNullOrEmpty(side) ? toAdd.DiagnosisCodes[0].Lingo : side + " - " + toAdd.DiagnosisCodes[0].Lingo;
                            //add single watermark description

                            if (diagCodes != null)
                            {
                                if (diagCodes.IsNotNullOrEmpty())
                                {
                                    foreach (var diagCode in diagCodes)
                                    {
                                        toAdd.Code = toAdd.DiagnosisCodes.FirstOrDefault(r => r.Code == diagCode.Substring(0, diagCode.IndexOf("-")) && !(diagnosisViewModels.Any(s => s.Code != null && s.Code.Code == diagCode.Substring(0, diagCode.IndexOf("-")))));
                                        if (toAdd.Code != null)
                                            break;
                                    }
                                }
                            }
                        }
                        else
                            toAdd.Code = null;
                    }
                    else // ICD9 API calling here
                    {
                        ObservableCollection<DiagnosisCodeViewModel> icd9DiagnosisCodes = _createDiagnosisCodeViewModelCollection();
                        icd9DiagnosisCodes.Add(diagnosisCodeModel);
                        toAdd.DiagnosisCodes = icd9DiagnosisCodes.ToObservableCollection();
                        toAdd.Code = diagnosisCodeModel;
                    }

                    if (hasmultipleItems <= 1 || !isIcd10Mode) diagnosisViewModels.Add(toAdd);

                    i = i + 1;
                }

                if (!isToggled)
                {
                    if (diagCodes.IsNotNullOrEmpty())
                    {
                        int ordinalId = 0;
                        foreach (string diagCode in diagCodes)
                        {
                            foreach (var diagnosis in diagnosisViewModels)
                            {
                                var icdCode = diagCode.Substring(0, diagCode.IndexOf("-"));

                                if (diagnosis.Code != null)
                                {
                                    if (string.Equals(diagnosis.Code.Code, icdCode, StringComparison.OrdinalIgnoreCase))
                                    {
                                        diagnosis.IsQueuedToBill = true;
                                        diagnosis.Id = ordinalId + 1;
                                        diagnosis.OrdinalId = ordinalId;
                                        ordinalId = ordinalId + 1;
                                        diagCodes = diagCodes.Where(item => item != diagCode).ToArray();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return diagnosisViewModels;
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public ObservableCollection<ServiceViewModel> GetBillingService(bool isIcd10Mode, ObservableCollection<DiagnosisViewModel> diagnosisCodes, string[] fileNames)
        {
            var serviceViewModelCollections = _createServiceViewModelCollection();

            if (fileNames != null && fileNames.Length > 0)
            {
                int x = 0;
                foreach (string filepath in fileNames)
                {

                    bool hasClinicalRows = false;
                    var controlName = string.Empty;
                    var filename = Path.GetFileNameWithoutExtension(filepath);
                    // Ignore T and Take order
                    var order = filename.Substring(1, 3);

                    //var questionParty = "T";
                    var question = string.Empty;
                    var questionSet = "First Visit";
                    var questionOrder = order;
                    var clinicalQuery = "SELECT * FROM df.Questions WHERE QuestionOrder = '" + questionOrder.Trim() + "' AND QuestionSet = '" + questionSet + "' AND  Question <> 'SMOKING' AND QuestionParty = 'T'";

                    using (var dbConnection = DbConnectionFactory.PracticeRepository)
                    {
                        var clinicalTable = dbConnection.Execute<DataTable>(clinicalQuery);
                        if (clinicalTable.Rows.Count > 0)
                        {
                            hasClinicalRows = true;
                            controlName = Convert.ToString(clinicalTable.Rows[0]["ControlName"]);
                            clinicalTable.Dispose();
                        }
                    }
                    if (hasClinicalRows)
                    {
                        if (controlName != string.Empty)
                        {
                            var aProc = controlName.Trim();
                            if (controlName.Substring(controlName.Length - 1, 1) == ",")
                            {
                                aProc = controlName.Substring(0, controlName.Length - 2);
                            }

                            var allProc = aProc.Split(','); // To get all the procedures  
                            var maxChoice = allProc.Length;
                            for (int k = 0; k < maxChoice; k++)
                            {
                                var serviceViewModels = _createServiceViewModel();
                                serviceViewModels.Id = x;
                                serviceViewModels.IsQueuedToBill = true;
                                serviceViewModels.IsOfficeVisit = false;

                                var serviceCodeViewModel = _createServiceCodeViewModel();
                                serviceViewModels.DiagnosisLinks = _createDiagnosisViewModelCollection();
                                if (serviceViewModelCollections.Any(i => i.Code.Code == allProc[k].Trim()))
                                    continue;

                                aProc = allProc[k];

                                var loadProceduresQuery = string.Empty;

                                if (aProc.Length == 1)
                                    loadProceduresQuery = "SELECT * FROM dm.ICD9CPTLinkTable WHERE CPT >= '" + aProc.Trim().ToUpper() + "' ";
                                else
                                    loadProceduresQuery = "SELECT * FROM dm.ICD9CPTLinkTable WHERE CPT = '" + aProc.Trim().ToUpper() + "' ";

                                loadProceduresQuery += "ORDER BY ICD9 ASC, Rank ASC ";

                                bool isExculded = false;
                                string[] exculdedProcedures = { "G8553" };
                                if (exculdedProcedures.Contains(aProc.Trim().ToUpper())) isExculded = true;

                                if (!isExculded)
                                {
                                    bool cptExists = false;
                                    using (var dbConnection = DbConnectionFactory.PracticeRepository)
                                    {
                                        var procData = dbConnection.Execute<DataTable>(loadProceduresQuery);
                                        var icdList = new List<string>();
                                        if (procData != null && procData.Rows.Count > 0)
                                        {
                                            cptExists = true;
                                            serviceCodeViewModel.Id = Convert.ToInt32(procData.Rows[0]["CPTLinkId"]);
                                            serviceCodeViewModel.Code = Convert.ToString(procData.Rows[0]["CPT"]);
                                            serviceCodeViewModel.Description = Convert.ToString(procData.Rows[0]["Description"]);
                                            foreach (DataRow dr in procData.Rows)
                                            {
                                                icdList.Add(Convert.ToString(dr["ICD9"]));
                                            }
                                            procData.Dispose();
                                        }
                                        foreach (var diag in diagnosisCodes)
                                        {
                                            if (serviceViewModels.DiagnosisLinks.Count < 4)
                                            {
                                                var aDiag = diag.Code.Code;
                                                if (isIcd10Mode)
                                                {
                                                    var icd10Codes = new List<IcdModel>();
                                                    if (icd10Codes.Any())
                                                    {
                                                        foreach (var icdCode in icd10Codes)
                                                        {
                                                            int r = icdCode.Code.IndexOf('.');
                                                            if (r > 0)
                                                            {
                                                                int q = icdCode.Code.IndexOf('.', r + 1);
                                                                if (q > 0)
                                                                {
                                                                    icdCode.Code = icdCode.Code.Substring(0, q);
                                                                }
                                                            }
                                                            var icdLength = icdCode.Code.Length;
                                                            var code = icdCode.Code;
                                                            if (icdList.Any(i => i.Substring(0, i.Length > icdLength ? icdLength : i.Length) == diag.Icd9Code.Code))
                                                            {
                                                                var linkDiag = diagnosisCodes.Where(i => i.Code.Code.Substring(0, i.Code.Code.Length > icdLength ? icdLength : i.Code.Code.Length) == code);
                                                                linkDiag.ForEach(i =>
                                                                {
                                                                    i.IsLinked = true;
                                                                    if (!(serviceViewModels.DiagnosisLinks.Any(j => j.Id == i.Id)))
                                                                    {
                                                                        serviceViewModels.DiagnosisLinks.Add((DiagnosisViewModel)_cloner.Clone(i, i.GetType(), i.GetType()));
                                                                    }
                                                                });
                                                            }
                                                            if (serviceViewModels.DiagnosisLinks.Count > 0)
                                                                break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        int r = aDiag.IndexOf('.');
                                                        if (r > 0)
                                                        {
                                                            int q = aDiag.IndexOf('.', r + 1);
                                                            if (q > 0)
                                                            {
                                                                aDiag = aDiag.Substring(0, q);
                                                            }
                                                        }

                                                        //serviceViewModels.DiagnosisLinks = _createDiagnosisViewModelCollection();
                                                        if (icdList.Any(i => i.Substring(0, i.Length > aDiag.Length ? aDiag.Length : i.Length) == diag.Icd9Code.Code))
                                                        {
                                                            var linkDiag = diagnosisCodes.Where(i => i.Code.Code.Substring(0, i.Code.Code.Length > aDiag.Length ? aDiag.Length : i.Code.Code.Length) == aDiag);
                                                            linkDiag.ForEach(i =>
                                                            {
                                                                i.IsLinked = true;
                                                                if (!(serviceViewModels.DiagnosisLinks.Any(j => j.Id == i.Id)))
                                                                {
                                                                    serviceViewModels.DiagnosisLinks.Add((DiagnosisViewModel)_cloner.Clone(i, i.GetType(), i.GetType()));
                                                                }
                                                            });


                                                        }

                                                    }
                                                }

                                                else
                                                {
                                                    int r = aDiag.IndexOf('.');
                                                    if (r > 0)
                                                    {
                                                        int q = aDiag.IndexOf('.', r + 1);
                                                        if (q > 0)
                                                        {
                                                            aDiag = aDiag.Substring(0, q);
                                                        }
                                                    }

                                                    //serviceViewModels.DiagnosisLinks = _createDiagnosisViewModelCollection();
                                                    if (icdList.Any(i => i.Substring(0, i.Length > aDiag.Length ? aDiag.Length : i.Length) == aDiag))
                                                    {
                                                        var linkDiag = diagnosisCodes.Where(i => i.Code.Code.Substring(0, i.Code.Code.Length > aDiag.Length ? aDiag.Length : i.Code.Code.Length) == aDiag);
                                                        linkDiag.ForEach(i =>
                                                        {
                                                            i.IsLinked = true;
                                                            if (!(serviceViewModels.DiagnosisLinks.Any(j => j.Id == i.Id)))
                                                            {
                                                                serviceViewModels.DiagnosisLinks.Add((DiagnosisViewModel)_cloner.Clone(i, i.GetType(), i.GetType()));
                                                            }
                                                        });


                                                    }
                                                }
                                            }
                                        }

                                        serviceViewModels.Code = serviceCodeViewModel;
                                        ObservableCollection<ServiceCodeViewModel> serviceCodes = _createServiceCodeViewModelCollection();
                                        serviceCodes.Add(serviceCodeViewModel);
                                        serviceViewModels.ServiceCodes = serviceCodes;
                                    }

                                    var encounterService = GetServiceCharges(aProc);
                                    serviceViewModels.Units = encounterService != null ? encounterService.DefaultUnit : 1;
                                    serviceViewModels.UnitCharge = encounterService != null ? encounterService.UnitFee : 0;
                                    serviceViewModels.AllowableAmount = (Decimal)0.00;
                                    serviceViewModels.Modifiers = _createNameAndAbbreviationViewModelCollection();

                                    if (cptExists) serviceViewModelCollections.Add(serviceViewModels);
                                    x = x + 1;
                                }
                            }
                        }
                    }
                }
            }


            return serviceViewModelCollections;
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public ObservableCollection<NameAndAbbreviationViewModel> LoadModifierCodes()
        {
            ObservableCollection<NameAndAbbreviationViewModel> modifierCodes = _createNameAndAbbreviationViewModelCollection();
            var serviceModifiers = PracticeRepository.ServiceModifiers.Where(i => !i.IsArchived);
            foreach (var serviceModifier in serviceModifiers)
            {
                NameAndAbbreviationViewModel toAdd = _createNameAndAbbreviationViewModel();
                toAdd.Id = serviceModifier.Id;
                toAdd.Name = serviceModifier.Description;
                toAdd.Abbreviation = serviceModifier.Code;
                modifierCodes.Add(toAdd);
            }

            return modifierCodes;
        }


        private void InitMappers()
        {

            var mapperFactory = Mapper.Factory;

            _namedViewModelMapper = mapperFactory.CreateMapper<Tuple<int, string>, NamedViewModel>(
                item => new NamedViewModel
                {
                    Id = item.Item1,
                    Name = item.Item2
                }, false);


            _encounterServiceMapper = mapperFactory.CreateMapper<EncounterService, ServiceCodeViewModel>(item =>
                new ServiceCodeViewModel
                {
                    Code = item.Code,
                    Description = item.Description
                    
                }, false);
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public bool IsDefaultDiagnosisTypeIcd10(int patientId)
        {
            Insurer insurer = PracticeRepository.PatientInsurances.Where(pi => pi.InsuredPatientId == patientId && pi.OrdinalId == 1 && pi.IsDeleted == false && ((pi.EndDateTime != null && pi.EndDateTime > DateTime.Now) || (pi.EndDateTime == null))).Select(pi => pi.InsurancePolicy.Insurer).FirstOrDefault(x => !x.IsArchived);
            bool isDiagnosisTypeIcd10;
            if (insurer != null && insurer.DiagnosisTypeId != null)
            {
                isDiagnosisTypeIcd10 = (insurer.DiagnosisTypeId == (int)DiagnosisTypeId.Icd10);
            }
            else
            {
                isDiagnosisTypeIcd10 = (ApplicationSettings.Cached.GetSetting<string>(ApplicationSetting.DefaultDiagnosisTypeId).Value == Convert.ToString((int)DiagnosisTypeId.Icd10));
            }
            return isDiagnosisTypeIcd10;
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        private IEnumerable<IGrouping<object, DiagnosisCodeViewModel>> LoadDiagnosisCodes(IEnumerable<IcdModel> icd10List)
        {
            ObservableCollection<DiagnosisCodeViewModel> diagnosisCodes = _createDiagnosisCodeViewModelCollection();

            foreach (IcdModel icd10 in icd10List)
            {
                DiagnosisCodeViewModel diagnosisCode = _createDiagnosisCodeViewModel();
                diagnosisCode.Code = icd10.Code;
                diagnosisCode.Description = icd10.Description;
                diagnosisCode.Side = icd10.Side;
                diagnosisCode.Lingo = icd10.Lingo;
                diagnosisCode.EyeLid = icd10.EyeLidLocation;
                diagnosisCodes.Add(diagnosisCode);
            }

            var diagnosesGroupBySide = (from u in diagnosisCodes.ToObservableCollection() select u).GroupBy(o => new { o.Side, o.EyeLid });
            return diagnosesGroupBySide;
        }
        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public ObservableCollection<DiagnosisCodeViewModel> LoadIcdCodes(bool isIcd10Mode)
        {
            ObservableCollection<DiagnosisCodeViewModel> icdCodes = _createDiagnosisCodeViewModelCollection();
            var dataTable = new DataTable("ICD");
            dataTable.Columns.Add(new DataColumn("Code"));
            dataTable.Columns.Add(new DataColumn("Description"));
            if (!isIcd10Mode)
            {
                var createCommand = new Func<IDbCommand>(() =>
                                                         {
                                                             var c = DbConnectionFactory.PracticeRepository.CreateCommand();
                                                             c.CommandTimeout = 1200;
                                                             c.CommandType = CommandType.Text;
                                                             return c;
                                                         });

                dataTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("SELECT DISTINCT DiagnosisCode AS Code, ShortDescription AS Description FROM [icd9].ICD9", null, false, createCommand);

                if (dataTable.Rows.Count > 0)
                {
                    var icd9List = dataTable.AsEnumerable();
                    foreach (var icd9 in icd9List)
                    {
                        var diagnosisCode = _createDiagnosisCodeViewModel();
                        diagnosisCode.Code = icd9[0].ToString();
                        diagnosisCode.Description = icd9[1].ToString();
                        icdCodes.Add(diagnosisCode);
                    }
                }
            }
            else
            {
                ObservableCollection<IcdModel> icds = _icd9To10Service.GetIcd10Codes(string.Empty, "ExamBilling").ToObservableCollection();
                if (icds.Any())
                {
                    foreach (var icd in icds)
                    {
                        var diagnosisCode = _createDiagnosisCodeViewModel();
                        diagnosisCode.Code = icd.Code;
                        diagnosisCode.Description = icd.Description;
                        icdCodes.Add(diagnosisCode);
                    }
                }
            }
            return icdCodes.ToObservableCollection();
        }


        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public ObservableCollection<ServiceCodeViewModel> LoadAllBillingServices()
        {
            ObservableCollection<ServiceCodeViewModel> billingServices = _createServiceCodeViewModelCollection();
            var serviceCommand = new Func<IDbCommand>(() =>
            {
                var c = DbConnectionFactory.PracticeRepository.CreateCommand();
                c.CommandTimeout = 1200;
                c.CommandType = CommandType.Text;
                return c;
            });

            var serviceTable = DbConnectionFactory.PracticeRepository.Execute<DataTable>("SELECT DISTINCT  CPT, Description FROM dm.ICD9CPTLinkTable", null, false, serviceCommand);

            if (serviceTable.Rows.Count > 0)
            {
                var servicesList = serviceTable.AsEnumerable();
                foreach (var service in servicesList)
                {
                    var serviceCode = _createServiceCodeViewModel();
                    //serviceCode.Id = Convert.ToInt32(service[0]);
                    serviceCode.Code = Convert.ToString(service[0]);
                    serviceCode.Description = Convert.ToString(service[1]);
                    billingServices.Add(serviceCode);
                }
            }
            return billingServices;
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public ServiceViewModel GetServiceUnitCharge(ServiceViewModel service)
        {
            var encounterService = GetServiceCharges(service.Code.Code);
            service.Units = encounterService != null ? encounterService.DefaultUnit : 1;
            service.UnitCharge = encounterService != null ? encounterService.UnitFee : 0;

            return service;
        }

        private EncounterService GetServiceCharges(string code)
        {
            return PracticeRepository.EncounterServices.FirstOrDefault(i => i.Code == code);
        }

        [TransactionScope(System.Transactions.TransactionScopeOption.Suppress, System.Transactions.IsolationLevel.ReadUncommitted)]
        public bool IsDiagnosisLinked(string serviceCode, string diagnosisCode)
        {
            var isLinked = false;
            var diagLinkQuery = "SELECT * FROM dm.ICD9CPTLinkTable WHERE CPT = '" + serviceCode.Trim().ToUpper() + "' AND Icd9 = '" + diagnosisCode.Trim() + "' ";
            using (var dbConnection = DbConnectionFactory.PracticeRepository)
            {
                var table = dbConnection.Execute<DataTable>(diagLinkQuery);

                if (table.Rows.Count > 0)
                {
                    isLinked = true;
                }

                table.Dispose();
            }
            return isLinked;
        }


    }

}

