﻿using System.Web;
using Soaf.ComponentModel;
using Soaf.Web;
using Soaf.Web.Client;
using Soaf.Web.Services;
using TestApplication1.Model;

[assembly: Component(typeof(Soaf.Testing.Web.RepositoryServicesConfiguration), typeof(IRepositoryServicesConfiguration))]

namespace Soaf.Testing.Web
{
    public class RepositoryServicesConfiguration : IRepositoryServicesConfiguration
    {
        public bool ServicesEnabled
        {
            get { return true; }
        }
    }

    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        private static WebComponents webComponents;
        private static readonly object SyncRoot = new object();

        public override void Init()
        {
            base.Init();
            if (webComponents != null) webComponents.UserPrincipalBootstrapper.Initialize();
        }

        protected void Application_Start()
        {
            lock (SyncRoot)
            {
                if (webComponents == null)
                {
                    webComponents = Bootstrapper.Run<WebComponents>(new AssemblyRepository(new[] { typeof(MvcApplication).Assembly, typeof(IFederatedTestRepository).Assembly, typeof(IEndpointConfigurationProvider).Assembly, typeof(Common).Assembly }));
                    webComponents.InitializeAll();
                }
            }
        }

    }
}