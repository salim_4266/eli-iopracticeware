﻿using System;
using System.Collections.Generic;
using System.Linq;
using Soaf.ComponentModel;
using Soaf.Domain;
using Soaf.Linq;
using Soaf.Web.Client;

[assembly: Component(typeof(DataServicesRepositoryProvider))]


namespace Soaf.Web.Client
{
    [RepositoryProviderFactory]
    internal interface IDataServicesRepositoryProviderFactory : IRepositoryProviderFactory
    {
    }

    internal class DataServicesRepositoryProvider : IQueryProducer, IIncludeProvider, IInterceptor, IUpdatable, IDisposable
    {
        #region IDisposable Members

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IIncludeProvider Members

        public IQueryable ApplyQueryInclusions(IQueryable queryable, IEnumerable<QueryInclusion> includes)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IInterceptor Members

        public void Intercept(IInvocation invocation)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IQueryProducer Members

        public IQueryable CreateQuery(Type elementType)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IUpdatable Members

        public void AcceptChanges()
        {
            throw new NotImplementedException();
        }

        public bool IsChanged
        {
            get { throw new NotImplementedException(); }
        }

        public void RejectChanges()
        {
            throw new NotImplementedException();
        }

        public void Save(object value)
        {
            throw new NotImplementedException();
        }

        public void Delete(object value)
        {
            throw new NotImplementedException();
        }

        public void Persist(IEnumerable<IObjectStateEntry> objectStateEntries)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}