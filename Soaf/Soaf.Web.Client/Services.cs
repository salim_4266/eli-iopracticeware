﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Threading;
using System.Xml;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Threading;
using Soaf.TypeSystem;
using Soaf.Web.Client;
using MetaType = Soaf.TypeSystem.MetaType;
using System.Net.Security;
using ProtoBuf.Meta;
using ProtoBuf.ServiceModel;

[assembly: Component(typeof(RemoteServiceBehavior))]
[assembly: Component(typeof(ConfigurationFileEndpointConfigurationProvider), typeof(IEndpointConfigurationProvider), Priority = -1)]
[assembly: RemoteService]

namespace Soaf.Web.Client
{
    public enum RemoteServiceAuthenticationScope
    {
        /// <summary>
        /// Single authentication is supported. Default behavior
        /// </summary>
        AppDomain,

        /// <summary>
        /// Scopes authentication cookie to principal identity's username
        /// </summary>
        PrincipalIdentity
    }

    /// <summary>
    ///   Denotes that a type should automatically support remote service connections via WCF.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class | AttributeTargets.Assembly)]
    public class RemoteServiceAttribute : GlobalConcernAttribute
    {
        /// <summary>
        /// Denotes the authentication scope for remote service calls.
        /// </summary>
        public static RemoteServiceAuthenticationScope AuthenticationScope { get; set; }

        public RemoteServiceAttribute()
            : base(typeof(RemoteServiceBehavior), -1000)
        {
        }

        #region Global concern behavior (not per instance)

        private string[] endpointContractNames;

        protected string[] EndpointContractNames
        {
            get { return endpointContractNames ?? (endpointContractNames = ServiceModel.GetEndpointConfigurations().IfNotNull(epc => epc.Select(ep => ep.Contract).ToArray())); }
        }

        public override bool IsApplicableFor(Type type)
        {
            return
                // Note: these are here to prevent stack overflow exception by making calls to ServiceModel.GetEndpointConfigurations()
                // via EndpointContractNames property getter
                   !typeof(IConfigurationContainer).IsAssignableFrom(type)
                && !typeof(IEndpointConfigurationProvider).IsAssignableFrom(type)

                // The service is applicable when service provider is initialized and it's present in endpoint contracts
                && (ServiceProvider.IsInitialized && EndpointContractNames != null && EndpointContractNames.Contains(type.FullName));
        }

        #endregion

        public override void Visit(ComponentRegistration registration)
        {
            // INFO: with RemoteService attribute we want remote service implementation to be only created
            // when the service is to be run locally. So we use proxy behavior to solve this

            var concerns = registration.Concerns.ToArray();

            // No point in replacing with proxy if type doesn't register for something abstract, since it would still have to create an implementation
            // + this is actually what prevents stack overflow through proxy trying to create instance of the type
            if (!registration.For.Any(t => t.IsAbstract)) return;

            // If proxy concern is already applied -> skip optimization
            if (concerns.Any(c => c.Is(typeof (ProxyAttribute)))) return;

            // Store current registration type
            var implementationType = registration.Type;
            // Remove implementation type from "For" if it was there
            registration.For = registration.For.ToList().RemoveAll(new[] { implementationType });
            // Replace implementation type with first abstract/interface from "For"
            registration.Type = registration.For.First(p => p.IsAbstract);

            // Ensure proxy concern priority is less than ours
            var proxyConcern = new ProxyAttribute(Priority - 1) { ProxiedType = implementationType };
            registration.Concerns = concerns.Concat(proxyConcern.CreateEnumerable());
        }
    }

    /// <summary>
    /// Delegates operations to remote instances of services if enabled.
    /// </summary>
    [SupportsSynchronization]
    [Singleton]
    internal class RemoteServiceBehavior : IInterceptor
    {
        private string globalAuthenticationCookie;
        private readonly IDictionary<string, string> authenticationServiceCookies = new Dictionary<string, string>().Synchronized();

        static RemoteServiceBehavior()
        {
            ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateValidationCallback;
        }

        private void SetAuthenticationServiceCookie(string userName, string value)
        {
            if (RemoteServiceAttribute.AuthenticationScope == RemoteServiceAuthenticationScope.AppDomain)
            {
                globalAuthenticationCookie = value;   
            }

            userName = userName ?? PrincipalContext.Current.Principal.IfNotNull(p => p.Identity.Name);
            if (string.IsNullOrEmpty(userName)) return;

            authenticationServiceCookies[userName] = value;
        }

        private string GetAuthenticationServiceCookie(string userName = null)
        {
            if (RemoteServiceAttribute.AuthenticationScope == RemoteServiceAuthenticationScope.AppDomain)
            {
                return globalAuthenticationCookie;
            }

            string cookie = null;
            userName = userName ?? PrincipalContext.Current.Principal.IfNotNull(p => p.Identity.Name);
            if (!string.IsNullOrEmpty(userName))
            {
                authenticationServiceCookies.TryGetValue(userName, out cookie);
            }

            return cookie ?? globalAuthenticationCookie;
        }

        public static bool RemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        #region IInterceptor Members

        public void Intercept(IInvocation invocation)
        {
            if (invocation.Method.DeclaringType == null)
            {
                invocation.Proceed();
                return;
            }

            MethodInfo serviceMethod = invocation.Method.GetServiceMethod();

            if (serviceMethod == null || serviceMethod.DeclaringType == null || !serviceMethod.HasAttribute<OperationContractAttribute>())
            {
                invocation.Proceed();
                return;
            }

            EndpointConfiguration endpoint = ServiceModel.GetEndpointName(serviceMethod.DeclaringType.IfNotNull(t => t.FullName));
            if (endpoint == null)
            {
                invocation.Proceed();
                return;
            }

            ChannelFactory channelFactory = serviceMethod.DeclaringType.CreateChannelFactory(endpoint.Name, endpoint.Uri);
            object instance = ((dynamic)channelFactory).CreateChannel();

            var contextChannel = instance as IClientChannel;
            if (contextChannel != null)
            {
                contextChannel.OperationTimeout = TimeSpan.FromMinutes(20);
            }

            using (new OperationContextScope((IContextChannel)instance))
            {
                var request = new HttpRequestMessageProperty();
                request.Headers["Content-Encoding"] = "gzip";

                var authenticationCookie = GetAuthenticationServiceCookie();
                if (authenticationCookie != null && (!invocation.Target.Is<IAuthenticationService>() || invocation.Method.Name != "Login"))
                {
                    // Add our cookie into the request header
                    request.Headers[HttpRequestHeader.Cookie] = authenticationCookie;
                }

                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = request;

                ContextData.StoreInOperationContext();

                try
                {
                    // Make the data services call - the cookie will be sent and the client authenticated
                    invocation.ReturnValue = serviceMethod.Invoke(instance, invocation.Arguments);
                }
                catch (Exception ex)
                {
                    // Check if error actually happened on server-side
                    var fault = ex.SearchFor<FaultException<ExceptionDetail>>();
                    if (fault == null) ex.Rethrow();

                    throw new FaultException(fault);
                }
                if (invocation.Target.Is<IAuthenticationService>())
                {
                    if (invocation.Method.Name == "Logout")
                    {
                        SetAuthenticationServiceCookie(null, null);
                    }
                    else if (invocation.Method.Name == "Login")
                    {
                        // Retrieve the cookie that is returned in the response
                        MessageProperties props = OperationContext.Current.IncomingMessageProperties;
                        var prop = (HttpResponseMessageProperty)props[HttpResponseMessageProperty.Name];
                        // First argument of login method is username
                        var userName = (string) invocation.Arguments[0];
                        SetAuthenticationServiceCookie(userName, prop.Headers["Set-Cookie"]);
                    }
                }
            }
        }

        #endregion
    }

    /// <summary>
    /// FaultException that returns full string value. Default FaultException truncates at 1023 characters.
    /// </summary>
    public class FaultException : FaultException<ExceptionDetail>
    {
        private readonly Type originalFaultType;

        public FaultException(FaultException<ExceptionDetail> fault)
            : base(fault.Detail, fault.Reason, fault.Code, fault.Action)
        {
            originalFaultType = fault.GetType();
            this.SetInnerException(ExceptionDetailException.TryReconstruct(fault.Detail));
        }

        public override string Message
        {
            get { return Detail != null ? "A remote service call failed." : base.Message; }
        }

        public override string ToString()
        {
            return string.Format("{0}: {1} (Fault Detail is equal to {2}).", originalFaultType, Message, Detail != null ? Detail.ToString() : string.Empty);
        }
    }

    /// <summary>
    /// A wrapper around an ExceptionDetail.
    /// </summary>
    public class ExceptionDetailException : Exception
    {
        private readonly ExceptionDetail exceptionDetail;

        private static readonly IDictionary<string, Type> ExceptionTypeCache = new Dictionary<string, Type>().Synchronized();

        private ExceptionDetailException(ExceptionDetail exceptionDetail)
            : base(exceptionDetail.Type + ": " + exceptionDetail.Message, GetInnerException(exceptionDetail))
        {
            this.exceptionDetail = exceptionDetail;
            this.PreservStackTrace();
        }

        private static Exception GetInnerException(ExceptionDetail exceptionDetail)
        {
            if (exceptionDetail.InnerException == null) return null;

            return TryReconstruct(exceptionDetail.InnerException);
        }

        public static Exception TryReconstruct(ExceptionDetail exceptionDetail)
        {
            var exceptionType = exceptionDetail.Type.ToType() ?? TryResolveTypeFromAppDomain(exceptionDetail.Type);
            if (exceptionType == null) return new ExceptionDetailException(exceptionDetail);

            var constructor = exceptionType.GetConstructor(new[] { typeof(string), typeof(Exception) });
            if (constructor == null) return new ExceptionDetailException(exceptionDetail);

            var parameters = constructor.GetParameters();
            if (parameters[0].Name != "message") return new ExceptionDetailException(exceptionDetail);

            Exception result = (Exception)constructor.Invoke(new object[] { exceptionDetail.Message, GetInnerException(exceptionDetail) });

            result.SetStackTrace(exceptionDetail.StackTrace);

            return result;
        }

        private static Type TryResolveTypeFromAppDomain(string typeName)
        {
            return ExceptionTypeCache.GetValue(typeName, () => AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetType(typeName)).FirstOrDefault(t => t != null));
        }

        public override string StackTrace
        {
            get
            {
                return exceptionDetail.StackTrace;
            }
        }
    }

    public static class ServiceModel
    {
        private static readonly IDictionary<Type, Type> ServiceContractCache = new Dictionary<Type, Type>().Synchronized();

        private static readonly IDictionary<MethodInfo, MethodInfo> ServiceMethodCache = new Dictionary<MethodInfo, MethodInfo>().Synchronized();

        private static readonly HashSet<Type> VisitedKnownTypes = new HashSet<Type>();

        static ServiceModel()
        {
            RuntimeTypeModel.Default.SetDefaultFactory(Reflector.GetMember(() => CreateInstance(null)).CastTo<MethodInfo>());
            RuntimeTypeModel.Default.InferTagFromNameDefault = true;
            RuntimeTypeModel.Default.AutoAddProtoContractTypesOnly = true;

            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;
        }

        /// <summary>
        /// Initializes ServiceModel
        /// </summary>
        public static void Initialize()
        {
            // Just ensures static constructor is called
        }

        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.Split(',')[0] == "protobuf-net")
            {
                return typeof(Bootstrapper).Assembly;
            }
            return null;
        }

        public static object CreateInstance(Type t)
        {
            return ServiceProvider.Current.GetService(t);
        }

        /// <summary>
        /// Dynamically creates a ServiceContract proxy type for the type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="createAsInterface">if set to <c>true</c> [as interface].</param>
        /// <returns></returns>
        public static Type CreateServiceContractType(this Type type, bool createAsInterface)
        {
            return ServiceContractCache
                .GetValue(type, () => CreateServiceContractMetaType(type, createAsInterface).ToType());
        }

        internal static void AddServiceMethodKnownTypes(MethodInfo method)
        {
            foreach (Type t in method.GetParameterTypes())
            {
                AddKnownTypeHierarchy(t, null, false);
            }
            if (method.ReturnType != typeof(void)) AddKnownTypeHierarchy(method.ReturnType, null, false);
        }

        public static void AddKnowntype(Type type)
        {
            AddKnownTypeHierarchy(type, null, false);
        }

        internal static void AddKnownTypeHierarchy(Type type, Type derivedType, bool forceAdd)
        {
            Type elementType = type.FindElementType();
            if (elementType != null)
            {
                AddKnownTypeHierarchy(elementType, null, false);
            }

            if (!type.HasDataContract() && !forceAdd) return;

            ProtoBuf.Meta.MetaType metaType = RuntimeTypeModel.Default.Add(type, false);
            metaType.AsReferenceDefault = true;
            metaType.UseConstructor = false;

            if (type.IsEnum)
            {
                metaType.EnumPassthru = true;
                return;
            }

            if (type.BaseType != null && type.BaseType != typeof(object)
                && !type.HasAttribute<CollectionDataContractAttribute>())
            {
                AddKnownTypeHierarchy(type.BaseType, type, true);
            }

            if (derivedType != null && metaType.GetSubtypes().All(s => s.DerivedType.Type != derivedType))
            {
                var fieldNumber = derivedType.MetadataToken;
                metaType.AddSubType(fieldNumber, derivedType);
            }

            if (!VisitedKnownTypes.Add(type)) return;

            ValueMember[] fields = metaType.GetFields();
            foreach (DataMember member in type.GetDataMembers())
            {
                if (fields.All(f => f.Name != member.Name) && member.MemberInfo.DeclaringType == type)
                {
                    var memberType = member.MemberInfo.As<PropertyInfo>().IfNotNull(p => p.PropertyType) ?? member.As<FieldInfo>().IfNotNull(f => f.FieldType);

                    var field = metaType.AddField(member.Order, member.MemberInfo.Name);

                    if (!memberType.IsValueType && memberType != typeof(string))
                        field.AsReference = true;

                    if (memberType == typeof(object) || memberType.EqualsGenericTypeFor(typeof(IEnumerable<>))) field.DynamicType = true;

                    AddKnownTypeHierarchy(memberType, null, false);
                }
            }

            type.GetAttributes<KnownTypeAttribute>().Select(a => a.Type).Distinct().ForEach(t => AddKnownTypeHierarchy(t, null, false));
        }

        internal static MetaType CreateServiceContractMetaType(this Type type, bool createAsInterface)
        {
            bool isAspNetApplicationService = false;
            if (type.Is<IAuthenticationService>())
            {
                type = typeof(IAuthenticationService);
                isAspNetApplicationService = true;
            }

            MetaType serviceContractType = type.ToMetaType();

            serviceContractType.Interfaces.Clear();
            serviceContractType.BaseType = null;

            string ns;
            if (isAspNetApplicationService) ns = "http://asp.net/ApplicationServices/v200";
            else if (type.Namespace != null && type.Namespace.StartsWith("Soaf")) ns = Namespaces.SoafXmlNamespace;
            else ns = type.Namespace ?? string.Empty;

            serviceContractType.Attributes.Add(new ServiceContractAttribute { Namespace = ns, Name = type.IsInterface && type.Name.StartsWith("I") ? type.Name.Substring(1) : type.Name });
            foreach (var group in serviceContractType.Members.OfType<MetaMethod>().GroupBy(m => m.Name))
            {
                int methodIndex = 1;
                foreach (MetaMethod method in group)
                {
                    var operationContractAttribute = new OperationContractAttribute();
                    if (methodIndex > 1) operationContractAttribute.Name = "{0}_{1}".FormatWith(method.Name, methodIndex);

                    method.Attributes.Add(operationContractAttribute);
                    method.Attributes.Add(new TransactionFlowAttribute(TransactionFlowOption.Allowed));
                    methodIndex++;
                }
            }

            if (!createAsInterface)
            {
                serviceContractType.Namespace += ".Implementation";
            }
            serviceContractType.IsInterface = createAsInterface;
            serviceContractType.IsAbstract = createAsInterface;
            serviceContractType.Members.OfType<MetaMethod>().ForEach(m => m.IsAbstract = createAsInterface);
            serviceContractType.Members.OfType<MetaProperty>().ForEach(m => m.IsAbstract = createAsInterface);
            serviceContractType.AssemblyName = "Soaf.DynamicTypes";

            return serviceContractType;
        }

        /// <summary>
        /// Gets an operation contract method for the specified method. First searches base classes and interfaces. Alternatively dynamically creates a service contract type.
        /// </summary>
        /// <returns></returns>
        internal static MethodInfo GetServiceMethod(this MethodInfo method)
        {
            return ServiceMethodCache.GetValue(method, () =>
                {
                    Type type = method.DeclaringType;

                    Type[] serviceContractTypes = type.GetTypeBaseTypesAndInterfaces()
                        .Where(i => i.HasAttribute<ServiceContractAttribute>()).ToArray();

                    if (serviceContractTypes.Length == 0)
                    {
                        Type t = type;
                        type = ServiceProvider.Current.GetService<IComponentRegistry>().ComponentRegistrations.Where(r => r.Type == t || r.For.Contains(t))
                                   .Select(r => r.For.FirstOrDefault(method.IsMethodImplementationForInterface) ?? r.For.FirstOrDefault() ?? r.Type).FirstOrDefault() ?? t;

                        serviceContractTypes = new[] { type.CreateServiceContractType(true) };
                    }

                    MethodInfo serviceMethod = serviceContractTypes
                        .SelectMany(t => t.GetMethods().Where(m =>
                            // find applicable service contract method
                                                              method.IsMethodImplementationForInterfaceMethod(m) ||
                                                              m == method ||
                                                              (m.Name == method.Name && m.GetParameterTypes().SequenceEqual(method.GetParameterTypes()))))
                        .FirstOrDefault();

                    if (serviceMethod != null)
                    {
                        AddServiceMethodKnownTypes(serviceMethod);
                    }

                    return serviceMethod;
                });
        }

        internal static ChannelFactory CreateChannelFactory(this Type type, string endpointConfigurationName, Uri endpointUri)
        {
            Type channelFactoryType = typeof(ChannelFactory<>).MakeGenericType(type);
            var channelFactory = (ChannelFactory)Activator.CreateInstance(channelFactoryType, endpointConfigurationName, new EndpointAddress(endpointUri));
            channelFactory.Endpoint.Contract.Behaviors.Add(new DataContractSerializerOperationBehavior());
            channelFactory.Endpoint.Behaviors.Add(new MaxFaultSizeBehavior());
            return channelFactory;
        }


        internal static EndpointConfiguration GetEndpointName(string contractName)
        {
            // find matching service contract name (allow for IMyService == MyService equality)
            IEnumerable<EndpointConfiguration> endpointConfigurations = GetEndpointConfigurations();
            EndpointConfiguration match = endpointConfigurations.FirstOrDefault(e => e.Contract.Equals(contractName, StringComparison.InvariantCulture));
            return match;
        }

        /// <summary>
        /// Gets all endpoint configuration contract/name pairs from the xml configuration.
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<EndpointConfiguration> GetEndpointConfigurations()
        {
            var endpointConfigurationProvider = ServiceProvider.Current.GetService<IEndpointConfigurationProvider>();
            return endpointConfigurationProvider.EndpointConfigurations;
        }
    }

    public interface IEndpointConfigurationProvider
    {
        IEnumerable<EndpointConfiguration> EndpointConfigurations { get; }
    }

    public class ConfigurationFileEndpointConfigurationProvider : IEndpointConfigurationProvider
    {
        /// <summary>
        /// Synchronization root to access and modify endpoint configurations.
        /// </summary>
        /// <remarks>
        /// Expected to be used when modifying clientSection.Endpoints collection via reflection to prevent enumeration issues
        /// </remarks>
        protected static ReaderWriterLockSlim EndpointsLock = Locks.GetLockInstance();

        private readonly IConfigurationContainer configurationContainer;

        #region Implementation of IEndpointConfigurationProvider

        public virtual IEnumerable<EndpointConfiguration> EndpointConfigurations
        {
            get { return GetEndpointConfigurations(); }
        }

        #endregion

        public ConfigurationFileEndpointConfigurationProvider(IConfigurationContainer configurationContainer)
        {
            this.configurationContainer = configurationContainer;
        }

        private IEnumerable<EndpointConfiguration> GetEndpointConfigurations()
        {
            using (new ReadOnlyLock(EndpointsLock))
            {
                var clientSection = configurationContainer.Configuration.GetSection("system.serviceModel/client") as ClientSection;
                if (clientSection == null) return new EndpointConfiguration[0];

                var endpointConfigurations = (from endpoint in clientSection.Endpoints
                                                  .OfType<ChannelEndpointElement>()
                                                  .ToArray() // Slightly cheaper than enumerating all EndpointConfiguration-s
                                              select new EndpointConfiguration
                                              {
                                                  Name = endpoint.Name,
                                                  Contract = endpoint.Contract,
                                                  Uri = endpoint.Address
                                              });

                return endpointConfigurations;
            }
        }
    }

    public class EndpointConfiguration
    {
        public virtual string Name { get; set; }

        public virtual string Contract { get; set; }

        public virtual Uri Uri { get; set; }
    }

    /// <summary>
    /// Increases the max fault size.
    /// </summary>
    public class MaxFaultSizeBehavior : IEndpointBehavior
    {
        public void Validate(ServiceEndpoint endpoint)
        {
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MaxFaultSize = int.MaxValue;
        }
    }
}

namespace Soaf.Web
{
    public class DataContractSerializerOperationBehavior : IContractBehavior
    {
        #region IContractBehavior Members

        public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        {
        }

        public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        {
            contractDescription.Operations.ForEach(ReplaceBehavior);
        }

        public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            contractDescription.Operations.ForEach(ReplaceBehavior);
        }

        public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        #endregion

        private static void ReplaceBehavior(OperationDescription operation)
        {
            var behavior = operation.Behaviors.Find<System.ServiceModel.Description.DataContractSerializerOperationBehavior>();
            var newBehavior = new InnerDataContractSerializerOperationBehavior(operation);
            if (behavior != null)
            {
                operation.Behaviors.Remove(behavior);

                newBehavior.DataContractSurrogate = behavior.DataContractSurrogate;

                newBehavior.DataContractResolver = behavior.DataContractResolver ?? Serialization.DataContractResolver;

                if (behavior.DataContractFormatAttribute != null)
                {
                    newBehavior.DataContractFormatAttribute.Style = behavior.DataContractFormatAttribute.Style;
                }
                newBehavior.MaxItemsInObjectGraph = behavior.MaxItemsInObjectGraph != 0 ? behavior.MaxItemsInObjectGraph : int.MaxValue;
            }
            operation.Behaviors.Add(newBehavior);
        }

        #region Nested type: InnerDataContractSerializerOperationBehavior

        protected class InnerDataContractSerializerOperationBehavior : System.ServiceModel.Description.DataContractSerializerOperationBehavior
        {
            public InnerDataContractSerializerOperationBehavior(OperationDescription operation)
                : base(operation)
            {
            }

            public override XmlObjectSerializer CreateSerializer(Type type, string name, string ns, IList<Type> knownTypes)
            {
                knownTypes = knownTypes ?? new List<Type>();
                AddKnownTypes(type, knownTypes);
                var result = new DataContractSerializer(type, name, ns, knownTypes,
                    MaxItemsInObjectGraph, IgnoreExtensionDataObject,
                    true, DataContractSurrogate ?? new CustomDataContractSurrogate(), DataContractResolver ?? Serialization.DataContractResolver);

                return result;
            }

            public override XmlObjectSerializer CreateSerializer(Type type, XmlDictionaryString name, XmlDictionaryString ns, IList<Type> knownTypes)
            {
                knownTypes = knownTypes ?? new List<Type>();
                AddKnownTypes(type, knownTypes);
                var result = new DataContractSerializer(type, name, ns, knownTypes,
                    MaxItemsInObjectGraph, IgnoreExtensionDataObject,
                    true, DataContractSurrogate ?? new CustomDataContractSurrogate(), DataContractResolver ?? Serialization.DataContractResolver);

                return result;
            }

            /// <summary>
            /// Adds the known types. By default, if not a DataContract, include all property types and enumerable element type.
            /// </summary>
            /// <param name="rootType">Type of the root.</param>
            /// <param name="knownTypes">The known types.</param>
            private static void AddKnownTypes(Type rootType, IList<Type> knownTypes)
            {
                if (!rootType.HasAttribute<DataContractAttribute>() && !rootType.HasAttribute<CollectionDataContractAttribute>() && !knownTypes.Contains(rootType))
                {
                    knownTypes.Add(rootType);
                    Type elementType = rootType.FindElementType();
                    if (elementType != null && elementType != typeof(object))
                    {
                        AddKnownTypes(elementType, knownTypes);
                    }
                    foreach (PropertyInfo pi in rootType.GetProperties())
                    {
                        AddKnownTypes(pi.PropertyType, knownTypes);
                    }
                }
            }

        }

        #endregion
    }

    /// <summary>
    /// Describes a WCF operation behaviour that can perform protobuf serialization
    /// </summary>
    public sealed class ProtoOperationBehavior : System.ServiceModel.Description.DataContractSerializerOperationBehavior
    {
        private TypeModel model;

        /// <summary>
        /// Create a new ProtoOperationBehavior instance
        /// </summary>
        public ProtoOperationBehavior(OperationDescription operation)
            : base(operation)
        {
#if !NO_RUNTIME
            model = RuntimeTypeModel.Default;
#endif
            DataContractResolver = Serialization.DataContractResolver;
            DataContractSurrogate = new CustomDataContractSurrogate();
        }

        /// <summary>
        /// The type-model that should be used with this behaviour
        /// </summary>
        public TypeModel Model
        {
            get { return model; }
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                model = value;
            }
        }

        /// <summary>
        /// Creates a protobuf serializer if possible (falling back to the default WCF serializer)
        /// </summary>
        public override XmlObjectSerializer CreateSerializer(Type type, XmlDictionaryString name, XmlDictionaryString ns, IList<Type> knownTypes)
        {
            var originalType = type;
            if (type.IsEnum) return base.CreateSerializer(type, name, ns, knownTypes);

            var registration = ServiceProvider.Current.GetService<IComponentRegistry>()
                .ComponentRegistrations.FirstOrDefault(r => r.For.Any(t => type == t || type.EqualsGenericTypeFor(t)));

            if (registration != null && registration.Type != null)
            {
                type = registration.Type.IsGenericTypeDefinition && type.IsGenericType ? registration.Type.MakeGenericType(type.GetGenericArguments()) : registration.Type;
            }

            if (model == null) throw new InvalidOperationException("No Model instance has been assigned to the ProtoOperationBehavior");
            var innerSerializer = XmlProtoSerializer.TryCreate(model, type) ?? base.CreateSerializer(originalType, name, ns, knownTypes);

            return new XmlObjectSerializerWrapper(innerSerializer);
        }

        /// <summary>
        /// Wraps an arbitrary XmlObjectSerializer, using SuppressChangeTrackingScope and SuppressPropertyChangedScope during serialization and deserialization.
        /// </summary>
        public class XmlObjectSerializerWrapper : XmlObjectSerializer
        {
            private readonly XmlObjectSerializer innerSerializer;

            public XmlObjectSerializerWrapper(XmlObjectSerializer innerSerializer)
            {
                this.innerSerializer = innerSerializer;
            }

            public override void WriteStartObject(XmlDictionaryWriter writer, object graph)
            {
                innerSerializer.WriteStartObject(writer, graph);
            }

            public override void WriteObjectContent(XmlDictionaryWriter writer, object graph)
            {
                using (new SuppressChangeTrackingScope())
                using (new SuppressPropertyChangedScope())
                {
                    innerSerializer.WriteObjectContent(writer, graph);
                }
            }

            public override void WriteEndObject(XmlDictionaryWriter writer)
            {
                innerSerializer.WriteEndObject(writer);
            }

            public override object ReadObject(XmlDictionaryReader reader, bool verifyObjectName)
            {
                using (new SuppressChangeTrackingScope())
                using (new SuppressPropertyChangedScope())
                {
                    return innerSerializer.ReadObject(reader, verifyObjectName);
                }
            }

            public override bool IsStartObject(XmlDictionaryReader reader)
            {
                return innerSerializer.IsStartObject(reader);
            }
        }
    }

    /// <summary>
    /// Behavior to swap out DatatContractSerilaizer with the XmlProtoSerializer for a given endpoint.
    ///  <example>
    /// Add the following to the server and client app.config in the system.serviceModel section:
    ///  <behaviors>
    ///    <endpointBehaviors>
    ///      <behavior name="ProtoBufBehaviorConfig">
    ///        <ProtoBufSerialization/>
    ///      </behavior>
    ///    </endpointBehaviors>
    ///  </behaviors>
    ///  <extensions>
    ///    <behaviorExtensions>
    ///      <add name="ProtoBufSerialization" type="ProtoBuf.ServiceModel.ProtoBehaviorExtension, protobuf-net, Version=1.0.0.255, Culture=neutral, PublicKeyToken=257b51d87d2e4d67"/>
    ///    </behaviorExtensions>
    ///  </extensions>
    /// 
    /// Configure your endpoints to have a behaviorConfiguration as follows:
    /// 
    ///  <service name="TK.Framework.Samples.ServiceModel.Contract.SampleService">
    ///    <endpoint address="http://myhost:9003/SampleService" binding="basicHttpBinding" behaviorConfiguration="ProtoBufBehaviorConfig"
    ///     bindingConfiguration="basicHttpBindingConfig" name="basicHttpProtoBuf" contract="ISampleServiceContract" />
    ///  </service>
    ///  <client>
    ///      <endpoint address="http://myhost:9003/SampleService" binding="basicHttpBinding"
    ///          bindingConfiguration="basicHttpBindingConfig" contract="ISampleServiceContract"
    ///          name="BasicHttpProtoBufEndpoint" behaviorConfiguration="ProtoBufBehaviorConfig"/>
    ///   </client>
    /// </example>
    /// </summary>
    public class ProtoEndpointBehavior : IEndpointBehavior
    {
        #region IEndpointBehavior Members

        void IEndpointBehavior.AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        void IEndpointBehavior.ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            ReplaceDataContractSerializerOperationBehavior(endpoint);
        }

        void IEndpointBehavior.ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            ReplaceDataContractSerializerOperationBehavior(endpoint);
        }

        void IEndpointBehavior.Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion

        private static void ReplaceDataContractSerializerOperationBehavior(ServiceEndpoint serviceEndpoint)
        {
            foreach (OperationDescription operationDescription in serviceEndpoint.Contract.Operations)
            {
                ReplaceDataContractSerializerOperationBehavior(operationDescription);
            }
        }


        private static void ReplaceDataContractSerializerOperationBehavior(OperationDescription description)
        {
            var dcsOperationBehavior = description.Behaviors.Find<System.ServiceModel.Description.DataContractSerializerOperationBehavior>();
            if (dcsOperationBehavior != null)
            {
                description.Behaviors.Remove(dcsOperationBehavior);
                description.Behaviors.Add(new ProtoOperationBehavior(description));
            }
        }
    }

    /// <summary>
    /// Configuration element to swap out DatatContractSerilaizer with the XmlProtoSerializer for a given endpoint.
    /// </summary>
    /// <seealso cref="ProtoBuf.ServiceModel.ProtoEndpointBehavior"/>
    public class ProtoBehaviorExtension : BehaviorExtensionElement
    {
        /// <summary>
        /// Gets the type of behavior.
        /// </summary>     
        public override Type BehaviorType
        {
            get { return typeof(ProtoEndpointBehavior); }
        }

        /// <summary>
        /// Creates a behavior extension based on the current configuration settings.
        /// </summary>
        /// <returns>The behavior extension.</returns>
        protected override object CreateBehavior()
        {
            return new ProtoEndpointBehavior();
        }
    }
}