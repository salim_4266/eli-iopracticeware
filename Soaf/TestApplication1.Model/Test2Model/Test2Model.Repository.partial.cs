﻿using System.Linq;
using Soaf.Domain;

namespace TestApplication1.Model.Test2Model
{
    public partial interface ITest2Repository : ISupportsLazyLoading
    {
        IQueryable<Contact> Contacts { get; }
    }

    public abstract partial class Test2Repository
    {
        public virtual IQueryable<Contact> Contacts
        {
            get
            {
                return
                    from c in Test2Contacts
                    from a in Test2Addresses
                    where a.Id == c.ContactAddresses.FirstOrDefault().AddressId
                    select new Contact
                               {
                                   Id = c.Id,
                                   FirstName = c.FirstName,
                                   LastName = c.LastName,
                                   City = a.City,
                                   State = "Unknown"
                               };

            }
        }

        public bool IsLazyLoadingEnabled { get; set; }
    }
}
