//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Soaf.Collections;


namespace TestApplication1.Model.Test1Model
{
    /// <summary>
    /// Summary - Long Description
    /// </summary>
    public partial class Test1Contact : INotifyPropertyChanged
    {
    	public event PropertyChangedEventHandler PropertyChanged;
    	
    	protected void OnPropertyChanged(string propertyName)
    	{
    		if (PropertyChanged != null)
    		{
    			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    		}
    	}
        #region Primitive Properties
    
      public virtual int Id
        {
            get { return _id; }
            set 
    		{ 
    			var isChanged = _id != value; 
    			_id = value; 
    			if (isChanged) OnPropertyChanged("Id");
    		}
        }
        private int _id;
    
    	/// <summary>
    	/// Summary - Long Description
    	/// </summary>
      public virtual string FirstName
        {
            get { return _firstName; }
            set 
    		{ 
    			var isChanged = _firstName != value; 
    			_firstName = value; 
    			if (isChanged) OnPropertyChanged("FirstName");
    		}
        }
        private string _firstName;
    
      public virtual string LastName
        {
            get { return _lastName; }
            set 
    		{ 
    			var isChanged = _lastName != value; 
    			_lastName = value; 
    			if (isChanged) OnPropertyChanged("LastName");
    		}
        }
        private string _lastName;

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<Test1ContactAddress> ContactAddresses
        {
            get
            {
                if (_contactAddresses == null)
                {
                    var newCollection = new FixupCollection<Test1ContactAddress>();
                    newCollection.ItemSet += FixupContactAddressesItemSet;
                    newCollection.ItemRemoved += FixupContactAddressesItemRemoved;
                    _contactAddresses = newCollection;
                }
                return _contactAddresses;
            }
            set
            {
                if (!ReferenceEquals(_contactAddresses, value))
                {
                    var previousValue = _contactAddresses as FixupCollection<Test1ContactAddress>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupContactAddressesItemSet;
                        previousValue.ItemRemoved -= FixupContactAddressesItemRemoved;
                    }
                    _contactAddresses = value;
                    var newValue = value as FixupCollection<Test1ContactAddress>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupContactAddressesItemSet;
                        newValue.ItemRemoved += FixupContactAddressesItemRemoved;
                    }
    				OnPropertyChanged("ContactAddresses");
                }
            }
        }
        private ICollection<Test1ContactAddress> _contactAddresses;

        #endregion
        #region Association Fixup
    
        private void FixupContactAddressesItemSet(object sender, Soaf.EventArgs<Test1ContactAddress> e)
        {
            var item = e.Value;
     
            item.Contact = this;
        }
    
        private void FixupContactAddressesItemRemoved(object sender, Soaf.EventArgs<Test1ContactAddress> e)
        {
            var item = e.Value;
     
            if (ReferenceEquals(item.Contact, this))
            {
                item.Contact = null;
            }
        }
    

        #endregion
    }
}
