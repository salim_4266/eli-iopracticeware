﻿using Soaf.ComponentModel;

namespace TestApplication1.Model.Test1Model
{
    [SupportsNotifyPropertyChanged]
    public partial class Test1Contact
    {
        public byte[] NonEntitiesCollectionReturningNull
        {
            get { return null; }
        }
    }
}
