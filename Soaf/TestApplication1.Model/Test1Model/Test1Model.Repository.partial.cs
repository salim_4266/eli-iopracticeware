﻿using System.Linq;
using Soaf.Domain;

namespace TestApplication1.Model.Test1Model
{
    public partial interface ITest1Repository : ISupportsLazyLoading
    {
        IQueryable<Contact> Contacts { get; }
    }

    public abstract partial class Test1Repository
    {
        public virtual IQueryable<Contact> Contacts
        {
            get
            {
                return
                    from c in Test1Contacts
                    from pm in Test1ProviderMaps
                    where c.Id == pm.EntityKey && pm.EntityName == "Contact"
                    let address = c.ContactAddresses.FirstOrDefault().Address
                    select new Contact
                               {
                                   Id = pm.ProviderKey,
                                   FirstName = c.FirstName,
                                   LastName = c.LastName,
                                   City = address.Address2,
                                   State = address.State.Name,
                               };
            }
        }

        public bool IsLazyLoadingEnabled { get; set; }
    }
}
