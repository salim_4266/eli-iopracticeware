﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Soaf.ComponentModel;
using Soaf.Domain;
using TestApplication1.Model;
using TestApplication1.Model.Test1Model;
using TestApplication1.Model.Test2Model;

[assembly: Component(typeof(FederatedTestRepository), typeof(IFederatedTestRepository))]


namespace TestApplication1.Model
{
    [Repository(Name = "FederatedTestRepository")]
    public interface IFederatedTestRepository
    {
        IQueryable<Contact> Contacts { get; }
        IQueryable<Contact> GetContactInfoByFirstNameAndLastName(string firstName, string lastName);
    }

    public class FederatedTestRepository : IFederatedTestRepository
    {
        private readonly ITest1Repository test1Repository;
        private readonly ITest2Repository test2Repository;

        public FederatedTestRepository(ITest1Repository test1Repository, ITest2Repository test2Repository)
        {
            this.test1Repository = test1Repository;
            this.test2Repository = test2Repository;
        }

        #region IFederatedTestRepository Members

        public IQueryable<Contact> Contacts
        {
            get
            {
                return
                    from c in test1Repository.Contacts
                    select new Contact
                               {
                                   Id = c.Id,
                                   FirstName = c.FirstName,
                                   LastName = c.LastName,
                                   City = c.City,
                                   State = c.State
                               };
            }
        }

        public IQueryable<Contact> GetContactInfoByFirstNameAndLastName(string firstName, string lastName)
        {
            return Contacts.Where(c => c.FirstName.Contains(firstName) && c.LastName.Contains(lastName));
        }

        #endregion
    }

    [Description("LastName")]
    public class Contact
    {
        public int Id { get; set; }

        [Description("First Name")]
        public string FirstName { get; set; }

        [Description("Last Name")]
        public string LastName { get; set; }

        public ICollection<Address> Addresses { get; set; }

        public string City { get; set; }

        public string State { get; set; }
    }

    public class Address
    {
        [Description("Line 1")]
        public string Line1 { get; set; }

        public string City { get; set; }

        [Description("State")]
        public string State { get; set; }

        [Description("Zip Code")]
        public string ZipCode { get; set; }
    }
}