﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Common;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.MappingViews;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Soaf.Caching;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;
using Soaf.Domain;
using Soaf.EntityFramework;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Collections;
using Soaf.Threading;
using Soaf.Xml;
using ObjectStateEntry = System.Data.Entity.Core.Objects.ObjectStateEntry;

[assembly: Component(typeof(EntityFrameworkRepositoryProvider))]
[assembly: Component(typeof(ConnectionStringObjectContextFactoryCollection))]
[assembly: Component(typeof(EntityFrameworkRepositoryProviderFactory), typeof(IEntityFrameworkRepositoryProviderFactory))]
[assembly: Component(typeof(ConnectionStringObjectContextFactory))]

namespace Soaf.EntityFramework
{
    /// <summary>
    /// A wrapper for queries executed by EF.
    /// </summary>
    internal class EntityFrameworkQueryProvider : QueryProvider
    {
        private readonly EntityFrameworkRepositoryProvider provider;

        public EntityFrameworkQueryProvider(EntityFrameworkRepositoryProvider provider)
        {
            this.provider = provider;
        }

        protected override object Execute(Expression expression)
        {
            using (new TimedScope(s => Debug.WriteLine("Executed repository query in {0}.".FormatWith(s))))
            {
                expression = new EntityFrameworkQueryVisitor(provider.ObjectContext).Visit(expression);
                var result = Expression.Lambda(expression).Compile().DynamicInvoke();
                var queryable = result as IQueryable;

                if (queryable != null)
                {
                    result = queryable.ToInferredElementTypeList();
                }

                return result;
            }
        }
    }

    /// <summary>
    /// Represents a query root for an Entity Framework query.
    /// </summary>
    internal class EntityFrameworkQueryRootExpression : Expression
    {
        private readonly Type elementType;

        public EntityFrameworkQueryRootExpression(Type elementType, EntityFrameworkRepositoryProvider provider)
        {
            this.elementType = elementType;
            Provider = provider;
            QueryInclusions = new List<QueryInclusion>();
        }

        public IList<QueryInclusion> QueryInclusions { get; private set; }

        public override ExpressionType NodeType
        {
            get { return ExpressionType.Extension; }
        }

        public EntityFrameworkRepositoryProvider Provider { get; private set; }

        public override Type Type
        {
            get { return typeof(IQueryable<>).MakeGenericType(elementType); }
        }

        protected override Expression Accept(ExpressionVisitor visitor)
        {
            return this;
        }

        public override bool Equals(object obj)
        {
            return obj.As<EntityFrameworkQueryRootExpression>().IfNotNull(i => elementType == i.elementType && QueryInclusions.SelectMany(e => e.Expression.ToMemberAccessStrings()).SequenceEqual(i.QueryInclusions.SelectMany(e => e.Expression.ToMemberAccessStrings())));
        }

        public override int GetHashCode()
        {
            return elementType.GetHashCode();
        }
    }

    /// <summary>
    /// Swaps out query roots with EF ObjectSets.
    /// </summary>
    internal class EntityFrameworkQueryVisitor : ExpressionVisitor
    {
        private readonly ObjectContext objectContext;

        public EntityFrameworkQueryVisitor(ObjectContext objectContext)
        {
            this.objectContext = objectContext;
        }

        public override Expression Visit(Expression node)
        {
            var root = node as EntityFrameworkQueryRootExpression;
            if (root != null)
            {
                return Expression.Constant(objectContext.CreateObjectSet(root.Type.GetQueryableElementType()).ApplyQueryInclusions(root.QueryInclusions));
            }
            return base.Visit(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            var queryable = node.Value.As<IQueryable>();
            if (queryable != null && queryable.Expression != node)
            {
                return Visit(queryable.Expression);
            }
            if (queryable != null) return queryable.Expression;

            return base.VisitConstant(node);
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (new[] { ExpressionType.Convert, ExpressionType.ConvertChecked }.Contains(node.NodeType) && node.Operand.Type.IsEnum)
            {
                return Visit(node.Operand);
            }
            return base.VisitUnary(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Type.IsEnum)
            {
                var underlyingMemberName = node.Member.Name + "Id";
                var underlyingMember = node.Expression.Type.GetMember(underlyingMemberName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).FirstOrDefault();

                if (underlyingMember != null)
                {
                    return Expression.MakeMemberAccess(node.Expression, underlyingMember);
                }
            }

            return base.VisitMember(node);
        }

        protected override Expression VisitNew(NewExpression node)
        {

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            if (node.Members == null || node.Arguments == null)
            {
                return base.VisitNew(node);
            }
            // ReSharper restore HeuristicUnreachableCode
            // ReSharper restore ConditionIsAlwaysTrueOrFalse


            var args = node.Arguments.ToArray();

            for (int i = 0; i < node.Members.Count; i++)
            {
                if (i < args.Length)
                {
                    var member = node.Members[i];
                    var arg = node.Arguments[i];

                    args[i] = HandleEnumMemberAssignment(member, arg);
                }
            }

            return Expression.New(node.Constructor, args, node.Members);
        }

        private Expression HandleEnumMemberAssignment(MemberInfo memberInfo, Expression expression)
        {
            expression = base.Visit(expression);

            if (expression == null) return null;

            if (memberInfo.MemberType == MemberTypes.Property && ((PropertyInfo)memberInfo).PropertyType.IsEnum && !expression.Type.IsEnum)
            {
                expression = Expression.Convert(expression, ((PropertyInfo)memberInfo).PropertyType);
                return expression;
            }

            return expression;
        }

    }

    /// <summary>
    /// An Entity Framework based provider for repositories.
    /// </summary>
    [SupportsCaching]
    internal class EntityFrameworkRepositoryProvider : IQueryProducer, IIncludeProvider, IInterceptor, IUpdatable, IChangeTrackingExtended, IRevertibleChangeTrackingExtended, ISupportsLazyLoading, IDisposable
    {
        private readonly Func<IPrincipalContext> principalContextResolver;
        public readonly ObjectContext ObjectContext;
        private readonly bool originalProxyCreationEnabled;

        public EntityFrameworkRepositoryProvider(Func<ObjectContext> objectContextResolver, Func<IPrincipalContext> principalContextResolver)
        {
            this.principalContextResolver = principalContextResolver;

            ObjectContext = objectContextResolver();

            ObjectContext.CommandTimeout = ObjectContext.Connection.ConnectionTimeout;

            ObjectContext.SavingChanges += OnObjectContextSavingChanges;

            originalProxyCreationEnabled = ObjectContext.ContextOptions.ProxyCreationEnabled;

            lock (Transaction.Current == null || Transaction.Current.TransactionInformation.DistributedIdentifier == default(Guid) ? new object() : Transaction.Current.TransactionInformation)
                ObjectContext.EnsureConnectionOpen();
        }

        private void OnObjectContextSavingChanges(object sender, EventArgs e)
        {
            var oldUnchanged = ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Unchanged);

            ObjectContext.DetectChanges();

            ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Unchanged).Where(i => i.IsRelationship).ToArray().ForEach(ObjectContext.UpdateObjectStateEntry);

            ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Unchanged).Except(oldUnchanged).Where(i => !i.IsRelationship).ToArray().ForEach(ObjectContext.UpdateObjectStateEntry);

            SetUserContext();
        }

        public virtual void Dispose()
        {
            ObjectContext.ContextOptions.LazyLoadingEnabled = false;
            lock (Transaction.Current == null || Transaction.Current.TransactionInformation.DistributedIdentifier == default(Guid) ? new object() : Transaction.Current.TransactionInformation)
                ObjectContext.Dispose();
        }

        public void AcceptChanges()
        {
            AcceptingChanges(this, new EventArgs());

            using (new TimedScope(s => Debug.WriteLine("Persisted repository changes in {0}.".FormatWith(s))))
            {
                ObjectContext.EnsureConnectionOpen().SaveChanges();
            }
            AcceptedChanges(this, new EventArgs());
        }

        private void SetUserContext()
        {
            try
            {
                var ec = (EntityConnection)ObjectContext.Connection;
                DbConnection storeConnection = ec.StoreConnection;
                if (storeConnection is SqlConnection)
                {
                    ((SqlConnection)storeConnection).SetUserContext(principalContextResolver().IfNotNull(pc => pc.Principal));
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        public bool IsChanged
        {
            get { return ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Deleted | EntityState.Modified).Any(); }
        }

        public void RejectChanges()
        {
            RejectingChanges(this, new EventArgs());

            ObjectContext.Refresh(RefreshMode.StoreWins, ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Deleted | EntityState.Modified));

            RejectedChanges(this, new EventArgs());
        }

        public IQueryable CreateQuery(Type elementType)
        {
            if (IsEntityType(elementType))
            {
                return new EntityFrameworkQueryProvider(this).CreateQuery(new EntityFrameworkQueryRootExpression(elementType, this));
            }
            throw new NotSupportedException("Cannot produce queryable for {0} because it is not an entity type.".FormatWith(elementType.Name));
        }

        [Cache]
        protected virtual bool IsEntityType(Type type)
        {
            lock (type.Assembly)
            {
                if (type.IsValueType || type.FindElementType().IfNotNull(t => t.IsValueType)) return false;

                var metadataWorkspace = ObjectContext.MetadataWorkspace;

                LoadType(type);

                ReadOnlyCollection<EntityType> cSpaceEntityTypes = metadataWorkspace.GetItems<EntityType>(DataSpace.CSpace);
                if (cSpaceEntityTypes.IsNotNullOrEmpty())
                {
                    return cSpaceEntityTypes.Any(t => metadataWorkspace.GetClrType(t) == type);
                }
                return false;
            }
        }

        private void LoadType(Type type)
        {
            // try load to avoid errors about duplicate definitions (for example, from WSDL generated code).
            TryLoadFromAssembly(ObjectContext.MetadataWorkspace, type.Assembly);
            if (type.BaseType != null && type.BaseType != typeof(object))
            {
                TryLoadFromAssembly(ObjectContext.MetadataWorkspace, type.BaseType.Assembly);
            }
            if (type.IsGenericType) type.GetGenericArguments().ToList().ForEach(LoadType);
        }

        [DebuggerNonUserCode]
        private static void TryLoadFromAssembly(MetadataWorkspace metadataWorkspace, Assembly assembly)
        {
            try
            {
                metadataWorkspace.LoadFromAssembly(assembly);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
        }

        public IQueryable ApplyQueryInclusions(IQueryable queryable, IEnumerable<QueryInclusion> inclusions)
        {
            var root = queryable.Expression as EntityFrameworkQueryRootExpression;
            if (root == null)
            {
                throw new InvalidOperationException("QueryInclusions are only supported for root queryables created by this provider.");
            }

            root.QueryInclusions.AddRange(inclusions);

            return queryable;
        }

        public void Intercept(IInvocation invocation)
        {
            if (IsEntityType(invocation.Method.ReturnType) && invocation.Method.GetParameters().Length == 0 && invocation.Method.Name == "Create{0}".FormatWith(invocation.Method.ReturnType.Name))
            {
                // CreateObject
                invocation.ReturnValue = ObjectContext.CreateObject(invocation.Method.ReturnType);
            }
            else
            {
                // Function
                var parameters = invocation.Method.GetParameters().Select((pi, i) => GetObjectParameter(pi.Name, pi.ParameterType, invocation.Arguments[i])).ToArray();
                var result = ObjectContext.ExecuteFunction(invocation.Method.ReturnType.FindElementType(), invocation.Method.Name, parameters).ToInferredElementTypeList();

                // assign out/ref parameters
                invocation.Method.GetParameters().Select((p, i) => new { Parameter = p, Index = i }).Where(x => x.Parameter.IsOut || x.Parameter.ParameterType.IsByRef)
                    .ForEach(x => invocation.Arguments[x.Index] = parameters[x.Index].Value == DBNull.Value ? x.Parameter.ParameterType.GetDefaultValue() : parameters[x.Index].Value.ConvertTo(x.Parameter.ParameterType.GetElementType() ?? x.Parameter.ParameterType));

                invocation.ReturnValue = result;
            }
        }

        private static ObjectParameter GetObjectParameter(string name, Type parameterType, object value)
        {
            var parameter = value as ObjectParameter;

            if (parameter == null)
            {
                var defaultValue = parameterType.GetDefaultValue();

                if (parameterType.IsByRef) { parameterType = parameterType.GetElementType(); }
                parameterType = parameterType.AsUnderlyingTypeIfNullable();

                parameter = (Equals(value, defaultValue)) ? new ObjectParameter(name, parameterType) : new ObjectParameter(name, value);
            }

            return parameter;
        }

        public void Save(object value)
        {
            using (new TimedScope(s => Debug.WriteLine("Attached " + (value == null ? "null" : value.GetType().Name) + " to ObjectContext in {0}.".FormatWith(s))))
            {
                ObjectContext.Attach(value);
            }
        }

        public void Delete(object value)
        {
            ObjectContext.Attach(value);
            ObjectContext.DeleteObject(value);
        }

        public void Persist(IEnumerable<IObjectStateEntry> objectStateEntries)
        {
            using (var ts = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(20)))
            {
                if (ObjectContext.Connection.State != ConnectionState.Open) ObjectContext.Connection.Open();
                int sqlVersion;
                using (var command = ObjectContext.Connection.CastTo<EntityConnection>().StoreConnection.CreateCommand())
                {
                    command.CommandText = @"DECLARE @Sql nvarchar(150)
SET @Sql = 'SELECT compatibility_level
FROM sys.databases
WHERE database_id = ' + CONVERT(nvarchar, DB_ID())
EXEC(@Sql)";
                    sqlVersion = Convert.ToInt32(command.ExecuteScalar());
                }
                PersistInternal(objectStateEntries, sqlVersion);
                ts.Complete();
            }
        }

        private void PersistInternal(IEnumerable<IObjectStateEntry> objectStateEntries, int sqlVersion)
        {
            var stateEntries = objectStateEntries.ToArray();

            foreach (var feedItem in GetUpdatesFeed(stateEntries))
            {
                switch (feedItem.Item1)
                {
                    case ObjectState.Added:
                        BulkInsert(feedItem.Item2, feedItem.Item3, sqlVersion);
                        break;
                    case ObjectState.Modified:
                        BulkUpdate(feedItem.Item2, feedItem.Item3);
                        break;
                    case ObjectState.Deleted:
                        BulkDelete(feedItem.Item2, feedItem.Item3);
                        break;
                }
            }

            // Done. Reset state to unchanged
            stateEntries.ForEach(e => e.State = ObjectState.Unchanged);
        }

        private void BulkInsert(Type entryType, IEnumerable<IObjectStateEntry> objectStateEntries, int sqlVersion)
        {
            var clrEntityType = ObjectContext.GetObjectType(entryType);
            var propertyMap = ObjectContext.GetStoragePropertyMap(clrEntityType);
            var columnMap = propertyMap.ToDictionary(p => p.Value, p => p.Key);
            var tableName = ObjectContext.GetTableName(clrEntityType);
            var entityType = ObjectContext.GetEntityType(clrEntityType);

            var nonComputedColumns = propertyMap.Where(i => entityType.Properties[i.Key].HasStoreGeneratedPattern(StoreGeneratedPattern.None)).Select(p => p.Value).ToList();
            var computedColumns = propertyMap.Where(i => !entityType.Properties[i.Key].HasStoreGeneratedPattern(StoreGeneratedPattern.None)).Select(p => p.Value).ToList();

            var columnDataTypes = ObjectContext.Connection.CastTo<EntityConnection>().StoreConnection.Execute<DataTable>(
                @"SELECT c.name, 
	t.name + CASE 
	WHEN t.name IN ('binary', 'varbinary', 'varchar', 'nvarchar') 
		THEN '(' + (CASE WHEN c.max_length = -1 THEN 'max' WHEN t.name = 'nvarchar' AND c.max_length > 1 THEN CONVERT(nvarchar, c.max_length / 2) ELSE CONVERT(nvarchar, c.max_length) END) + ')'
	WHEN t.name IN ('decimal') 
		THEN '(' + CONVERT(nvarchar, c.precision) + ',' + CONVERT(nvarchar, c.scale) + ')'
	ELSE '' END 
FROM sys.columns c 
JOIN sys.types t ON 
	c.system_type_id = t.system_type_id 
WHERE c.object_id = OBJECT_ID('{0}') AND t.name <> 'sysname'"
                    .FormatWith(tableName)).Rows.OfType<DataRow>()
                .GroupBy(i => (string)i[0])
                .ToDictionary(i => i.Key, i => (string)i.First()[1]);

            if (columnDataTypes.ContainsKey(EntityFramework.EntityTypeColumnName))
            {
                nonComputedColumns.Add(EntityFramework.EntityTypeColumnName);
                columnMap[EntityFramework.EntityTypeColumnName] = EntityFramework.EntityTypeColumnName;
            }
            columnDataTypes[EntityFramework.LocalIndexColumnName] = "int";
            columnMap[EntityFramework.LocalIndexColumnName] = EntityFramework.LocalIndexColumnName;

            var setters = propertyMap.Where(i => !entityType.Properties[i.Key].HasStoreGeneratedPattern(StoreGeneratedPattern.None))
                .ToDictionary(p => p.Key,
                    p => entryType.GetProperty(p.Key, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).GetSetMethod(true).GetInvoker());

            var navigationPropertyGetters = entryType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(p => entityType.NavigationProperties.Contains(p.Name))
                .Select(p => p.GetGetMethod(true).GetInvoker()).ToArray();

            foreach (var batch in objectStateEntries.Batch(sqlVersion >= 100 ? 10000 : 500))
            {
                // Prepare entries Xml
                var entries = batch.Select(o => o.Object).ToArray();
                Dictionary<string, string> compactPropertyNamesMap;
                var xml = entries.ToCompactEntitiesXml(entityType, clrEntityType, out compactPropertyNamesMap);

                using (var command = ObjectContext.Connection.CastTo<EntityConnection>().StoreConnection.CreateCommand())
                {
                    // Add local index column if we will be loading ids
                    var extraColumnsFromXml = computedColumns.Count == 0 ? new string[0] : new[] { EntityFramework.LocalIndexColumnName };

                    var entitiesFromXmlQuery = "SELECT {0} FROM @xml.nodes('/root/i') AS p(i)"
                        .FormatWith(nonComputedColumns.Concat(extraColumnsFromXml)
                            .Select(p => "p.i.value('@{2}', '{1}') AS [{0}]".FormatWith(p, columnDataTypes[p], compactPropertyNamesMap[columnMap[p]]))
                            .Join());

                    command.CommandTimeout = ObjectContext.Connection.ConnectionTimeout;
                    command.CommandText += computedColumns.Count == 0 
                        // No computed columns -> perform quickest bulk insert
                        ? @"INSERT {0}({1})
{2}".FormatWith(tableName, nonComputedColumns.Select(c => string.Format("[{0}]", c)).Join(), entitiesFromXmlQuery)
                        // Has computed columns & MERGE INTO supported (>=SQL 2008)
                        : sqlVersion >= 100 
                            ? @"DECLARE @out table({1})
MERGE INTO {0}
USING (
{6}
) insertSource
ON 1 = 0
WHEN NOT MATCHED THEN
	INSERT ({4})
	VALUES ({5})
	OUTPUT insertSource.[__LocalIndex__], {2} INTO @out ([__LocalIndex__], {3});

SELECT {3} FROM @out ORDER BY [__LocalIndex__]".FormatWith(tableName,
                                   computedColumns.Concat("__LocalIndex__".CreateEnumerable()).Select(c => string.Format("[{0}] {1}", c, columnDataTypes[c])).Join(),
                                   computedColumns.Select(c => string.Format("inserted.[{0}]", c)).Join(),
                                   computedColumns.Select(c => string.Format("[{0}]", c)).Join(),
                                   nonComputedColumns.Select(c => string.Format("[{0}]", c)).Join(),
                                   nonComputedColumns.Select(c => string.Format("insertSource.[{0}]", c)).Join(), 
                                   entitiesFromXmlQuery)
                            // MERGE INTO not supported, so reset to insert one by one
                            : @"DECLARE @out table({1})
DECLARE {5}
DECLARE db_cursor CURSOR FOR  
{7}

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO {6}, @__LocalIndex__
WHILE @@FETCH_STATUS = 0   
BEGIN   
    INSERT INTO {0} ({4})
	OUTPUT @__LocalIndex__, {2} INTO @out ([__LocalIndex__], {3})
	VALUES ({6})

    FETCH NEXT FROM db_cursor INTO {6}, @__LocalIndex__
END   
CLOSE db_cursor   
DEALLOCATE db_cursor

SELECT {3} FROM @out ORDER BY [__LocalIndex__]".FormatWith(tableName,
                                   computedColumns.Concat("__LocalIndex__".CreateEnumerable()).Select(c => string.Format("[{0}] {1}", c, columnDataTypes[c])).Join(),
                                   computedColumns.Select(c => string.Format("inserted.[{0}]", c)).Join(),
                                   computedColumns.Select(c => string.Format("[{0}]", c)).Join(),
                                   nonComputedColumns.Select(c => string.Format("[{0}]", c)).Join(),
                                   nonComputedColumns.Concat("__LocalIndex__".CreateEnumerable()).Select(c => string.Format("@{0} {1}", c, columnDataTypes[c])).Join(),
                                   nonComputedColumns.Select(c => string.Format("@{0}", c)).Join(),
                                   entitiesFromXmlQuery);

                    var parameter = command.CreateParameter();
                    parameter.ParameterName = "xml";
                    parameter.Value = xml.ToString(new XmlWriterSettings {CheckCharacters = false, Indent = false, OmitXmlDeclaration = true});
                    parameter.DbType = DbType.Xml;

                    command.Parameters.Add(parameter);

                    // Execute depending on whether computed columns output is expected
                    if (computedColumns.Count == 0)
                    {
                        command.ExecuteNonQuery();
                    }
                    else
                    {
                        using (var dbReader = command.ExecuteReader())
                        {
                            int entryIndex = 0;
                            while (dbReader.HasRows && dbReader.Read() && entries.Length > entryIndex)
                            {
                                int keyIndex = 0;
                                foreach (var setter in setters)
                                {
                                    setter.Value(entries[entryIndex], dbReader[keyIndex]);

                                    keyIndex++;
                                }

                                entryIndex++;
                            }
                        }
                    }
                }

                // Refresh navigation properties at all times. 
                // This helps ensures primary key updates are properly propagated up the dependency chain
                foreach (var entry in entries)
                {
                    // fix up collections with retrieved PK values
                    foreach (var getter in navigationPropertyGetters)
                    {
                        var collection = getter(entry) as IList;
                        if (collection == null) continue;

                        var contents = collection.OfType<object>().ToArray();
                        try
                        {
                            collection.Clear();
                            collection.AddRange(contents);
                        }
                        catch (NotSupportedException)
                        {
                            // Collection is readonly
                        }
                    }
                }
            }
        }

        private void BulkUpdate(Type entryType, IEnumerable<IObjectStateEntry> objectStateEntries)
        {
            var clrEntityType = ObjectContext.GetObjectType(entryType);
            var entityType = ObjectContext.GetEntityType(clrEntityType);
            var propertyMap = ObjectContext.GetStoragePropertyMap(clrEntityType);
            var tableName = ObjectContext.GetTableName(clrEntityType);

            foreach (var batch in objectStateEntries.Batch(10000))
            {
                var entries = batch.Select(o => o.Object).ToArray();
                Dictionary<string, string> compactPropertyNamesMap;
                var xml = entries.ToCompactEntitiesXml(entityType, clrEntityType, out compactPropertyNamesMap);

                var subQuery = "SELECT {0} FROM @xml.nodes('/root/i') AS p(i)"
                    .FormatWith(entityType.Properties.Select(p => "p.i.value('@{1}', 'nvarchar(max)') AS [{0}]"
                        .FormatWith(p.Name, compactPropertyNamesMap[p.Name])).Join());

                var sets = propertyMap
                    .Where(i => !entityType.KeyMembers.Contains(i.Key))
                    .Select(property => "[{0}] = y.[{1}]".FormatWith(property.Value, property.Key))
                    .ToList();

                var join = propertyMap
                    .Where(i => entityType.KeyMembers.Contains(i.Key))
                    .Select(p => "x.[{0}] = y.[{1}]".FormatWith(p.Value, p.Key)).Join(" AND ");

                var query =
                    @"UPDATE {0} 
SET {1}
FROM {0} x
JOIN ({2}) y ON {3}".FormatWith(tableName, sets.Join(), subQuery, @join);

                using (var command = ObjectContext.Connection.CastTo<EntityConnection>().StoreConnection.CreateCommand())
                {
                    var parameter = command.CreateParameter();
                    parameter.ParameterName = "xml";
                    parameter.Value = xml.ToString(new XmlWriterSettings {CheckCharacters = false, Indent = false, OmitXmlDeclaration = true});
                    parameter.DbType = DbType.Xml;

                    command.Parameters.Add(parameter);

                    command.CommandTimeout = ObjectContext.Connection.ConnectionTimeout;
                    command.CommandText += query;
                    command.ExecuteNonQuery();
                }
            }
        }

        private void BulkDelete(Type entryType, IEnumerable<IObjectStateEntry> objectStateEntries)
        {
            var clrEntityType = ObjectContext.GetObjectType(entryType);
            var entityType = ObjectContext.GetEntityType(clrEntityType);
            var propertyMap = ObjectContext.GetStoragePropertyMap(clrEntityType);
            var tableName = ObjectContext.GetTableName(clrEntityType);

            foreach (var batch in objectStateEntries.Batch(10000))
            {
                var entries = batch.Select(o => o.Object).ToArray();
                Dictionary<string, string> compactPropertyNamesMap;
                var xml = entries.ToCompactEntitiesXml(entityType, clrEntityType, out compactPropertyNamesMap);

                var subQuery = "SELECT {0} FROM @xml.nodes('/root/i') AS p(i)"
                    .FormatWith(entityType.Properties.Select(p => "p.i.value('@{1}', 'nvarchar(max)') AS [{0}]"
                        .FormatWith(p.Name, compactPropertyNamesMap[p.Name])).Join());

                var join = propertyMap.Where(i => entityType.KeyMembers.Contains(i.Key))
                    .Select(p => "x.[{0}] = y.[{1}]".FormatWith(p.Value, p.Key)).Join(" AND ");

                var query =
                    @"DELETE {0} 
FROM {0} x
JOIN ({1}) y ON {2}".FormatWith(tableName, subQuery, @join);

                using (var command = ObjectContext.Connection.CastTo<EntityConnection>().StoreConnection.CreateCommand())
                {
                    var parameter = command.CreateParameter();
                    parameter.ParameterName = "xml";
                    parameter.Value = xml.ToString(new XmlWriterSettings {CheckCharacters = false, Indent = false, OmitXmlDeclaration = true});
                    parameter.DbType = DbType.Xml;

                    command.Parameters.Add(parameter);

                    command.CommandTimeout = ObjectContext.Connection.ConnectionTimeout;
                    command.CommandText += query;
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Turns modified object entries collection into batched update feed suitable for pushing changes to database
        /// </summary>
        /// <param name="objectStateEntries">The object state entries.</param>
        /// <returns></returns>
        private IEnumerable<Tuple<ObjectState, Type, IList<IObjectStateEntry>>> GetUpdatesFeed(IObjectStateEntry[] objectStateEntries)
        {
            // Order according to entity dependencies
            var orderedEntries = new EntityDependencyOrderer(objectStateEntries).ToArray();

            var result = orderedEntries
                .Where(o => !o.IsRelationship && o.State != ObjectState.Unchanged)
                .GroupBy(i => new { i.State, Type = i.Object.GetType() })
                .OrderBy(i => orderedEntries
                    .IndexOf(o => o.Object.GetType() == i.Key.Type && o.State == i.Key.State));

            // Execute deletes first in reverse order (we can't delete record which still has dependencies)
            foreach (var feedItem in result.Where(g => g.Key.State == ObjectState.Deleted).Reverse())
            {
                yield return new Tuple<ObjectState, Type, IList<IObjectStateEntry>>(feedItem.Key.State, feedItem.Key.Type, feedItem.ToList());
            }

            // Execute inserts and updates according to dependencies ordering
            foreach (var feedItem in result.Where(g => g.Key.State != ObjectState.Deleted))
            {
                yield return new Tuple<ObjectState, Type, IList<IObjectStateEntry>>(feedItem.Key.State, feedItem.Key.Type, feedItem.ToList());
            }
        }


        public event EventHandler<EventArgs> AcceptingChanges = delegate { };
        public event EventHandler<EventArgs> AcceptedChanges = delegate { };

        public IEnumerable<IObjectStateEntry> ObjectStateEntries
        {
            get
            {
                var attachedStates = EntityState.Added | EntityState.Deleted | EntityState.Modified | EntityState.Unchanged;

                ObjectContext.DetectChanges();

                // don't update/refresh the states of any newly added objects - this will be done later by saving

                return ObjectContext.ObjectStateManager.GetObjectStateEntries(attachedStates)
                    .Select(i => new EntityFrameworkObjectStateEntry(i, ObjectContext.ObjectStateManager))
                    .ToArray();
            }
        }

        public event EventHandler<EventArgs> RejectingChanges = delegate { };
        public event EventHandler<EventArgs> RejectedChanges = delegate { };

        /// <summary>
        /// An IObjectStateEntry corresponding to an instance from an ObjectContext's ObjectStateManager.
        /// </summary>
        private class EntityFrameworkObjectStateEntry : IObjectStateEntry
        {
            private readonly ObjectStateEntry objectStateEntry;
            private readonly ObjectStateManager manager;

            public EntityFrameworkObjectStateEntry(ObjectStateEntry objectStateEntry, ObjectStateManager manager)
            {
                this.objectStateEntry = objectStateEntry;
                this.manager = manager;
            }

            public ObjectState State
            {
                get
                {
                    switch (objectStateEntry.State)
                    {
                        case EntityState.Added:
                            return ObjectState.Added;
                        case EntityState.Deleted:
                            return ObjectState.Deleted;
                        case EntityState.Modified:
                            return ObjectState.Modified;
                        default:
                            return ObjectState.Unchanged;
                    }
                }
                set
                {
                    switch (value)
                    {
                        case ObjectState.Added:
                            objectStateEntry.ChangeState(EntityState.Added);
                            break;
                        case ObjectState.Deleted:
                            objectStateEntry.ChangeState(EntityState.Deleted);
                            break;
                        case ObjectState.Modified:
                            objectStateEntry.ChangeState(EntityState.Modified);
                            break;
                        case ObjectState.Unchanged:
                            objectStateEntry.ChangeState(EntityState.Unchanged);
                            break;
                    }
                }
            }

            public object Object
            {
                get
                {
                    if (!IsRelationship) return objectStateEntry.Entity;

                    var attachedStates = EntityState.Added | EntityState.Deleted | EntityState.Modified | EntityState.Unchanged;

                    var relationship = new Relationship();
                    var values = objectStateEntry.State == EntityState.Added ? objectStateEntry.CurrentValues : objectStateEntry.OriginalValues;

                    for (int i = 0; i < Math.Min(2, values.FieldCount); i++)
                    {
                        var member = manager.GetObjectStateEntries(attachedStates).FirstOrDefault(e => Equals(e.EntityKey, values[i]));

                        if (member == null) continue;

                        if (i == 0)
                        {
                            // end 1
                            relationship.End1 = member.Entity;
                            relationship.Name1 = values.GetName(i);
                        }
                        else if (i == 1)
                        {
                            relationship.End2 = member.Entity;
                            relationship.Name2 = values.GetName(i);
                        }
                    }

                    return relationship;
                }
            }

            public bool IsRelationship
            {
                get { return objectStateEntry.IsRelationship; }
            }

            public override string ToString()
            {
                return string.Format("State: {0}, Object: {1}", State, Object);
            }

        }

        public bool IsLazyLoadingEnabled
        {
            get { return ObjectContext.ContextOptions.LazyLoadingEnabled; }
            set
            {
                ObjectContext.ContextOptions.LazyLoadingEnabled = value;
                ObjectContext.ContextOptions.ProxyCreationEnabled = value || originalProxyCreationEnabled;
            }
        }

        public class Relationship
        {
            public object End1 { get; set; }

            public object End2 { get; set; }

            public string Name1 { get; set; }

            public string Name2 { get; set; }

            protected bool Equals(Relationship other)
            {
                return Equals(End1, other.End1) && Equals(End2, other.End2) && string.Equals(Name1, other.Name1) && string.Equals(Name2, other.Name2);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != GetType()) return false;
                return Equals((Relationship)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int hashCode = (End1 != null ? End1.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (End2 != null ? End2.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (Name1 != null ? Name1.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (Name2 != null ? Name2.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }
    }

    [Singleton]
    public class ConnectionStringObjectContextFactoryCollection : List<ConnectionStringObjectContextFactory>
    {

    }

    [RepositoryProviderFactory]
    public interface IEntityFrameworkRepositoryProviderFactory : IRepositoryProviderFactory
    {
        ConnectionStringObjectContextFactoryCollection ObjectContextFactories { get; }

        void ClearEntityFrameworkCache();
    }

    /// <summary>
    ///   Returns a IObjectContextFactory for a requested repository type based on an xml configuration.
    /// </summary>
    [Singleton]
    [SupportsSynchronization]
    internal class EntityFrameworkRepositoryProviderFactory : IEntityFrameworkRepositoryProviderFactory
    {
        private readonly Func<Func<ObjectContext>, EntityFrameworkRepositoryProvider> getProvider;

        private readonly ConcurrentDictionary<Type, bool> contextTypesWithViewCacheInitialized = new ConcurrentDictionary<Type, bool>();

        public EntityFrameworkRepositoryProviderFactory(Func<Func<ObjectContext>, EntityFrameworkRepositoryProvider> getProvider, ConnectionStringObjectContextFactoryCollection objectContextFactories, ILogger logger, Func<ConnectionStringObjectContextFactory> createConnectionStringObjectContextFactory, IConfigurationContainer configurationContainer, IConnectionStringRepository connectionStringRepository, IMapper<ConnectionStringObjectContextFactory, ConnectionStringObjectContextFactory> factoryMapper)
        {
            ObjectContextFactories = objectContextFactories;

            this.getProvider = getProvider;

            DbConfiguration.SetConfiguration(new AdoServiceDbConfiguration());

            var entityFrameworkConfiguration = configurationContainer.Configuration.GetSection<EntityFrameworkConfiguration>("entityFrameworkConfiguration");

            if (entityFrameworkConfiguration != null)
            {
                foreach (var factory in entityFrameworkConfiguration.Connections)
                {
                    if (factory.ConnectionString.IsNullOrEmpty() && factory.ConnectionStringName.IsNotNullOrEmpty())
                    {
                        var connectionStringSettings = connectionStringRepository[factory.ConnectionStringName];

                        if (connectionStringSettings == null)
                        {
                            Debug.WriteLine("Could not find connection string named {0}.".FormatWith(factory.ConnectionStringName), Severity.Error);
                            continue;
                        }

                        var connectionString = connectionStringSettings.ConnectionString;
                        if (connectionString != null && !connectionString.Contains("metadata="))
                        {
                            var providerName = connectionStringSettings.ProviderName;
                            factory.ConnectionString = "metadata={0};provider={1};provider connection string=\"{2}\"".FormatWith(factory.Metadata, providerName == null ? AdoServiceProviderFactory.Name : providerName, connectionString.Replace(@"""", "&quot;"));
                        }
                        else if (connectionString != null)
                        {
                            factory.ConnectionString = connectionString;
                        }

                        Debug.WriteLine("Added entity framework factory for repository {0} with connection string {1}.".FormatWith(factory.RepositoryTypeName, factory.ConnectionString));
                    }
                    if (factory.RepositoryType != null)
                    {
                        // make sure the factory gets resolved as a service (so implementers can override behavior)
                        var mappedFactory = factoryMapper.Map(factory);
                        ObjectContextFactories.Add(mappedFactory);
                    }
                    else
                    {
                        logger.Log("Repository type {0} could not be found.".FormatWith(factory.RepositoryTypeName), Severity.Error);
                    }
                }
            }
            foreach (var connectionString in connectionStringRepository.ConnectionStrings
                .Where(i => i.ConnectionString.StartsWith("metadata=")))
            {
                var factory = createConnectionStringObjectContextFactory();

                factory.ConnectionString = connectionString.ConnectionString;
                factory.LazyLoadingEnabled = true;
                factory.ProxyCreationEnabled = true;
                factory.RepositoryTypeName = connectionString.Name;

                if (factory.RepositoryType != null)
                {
                    if (factory.RepositoryType.HasAttribute<RepositoryAttribute>())
                    {
                        ObjectContextFactories.Add(factory);
                    }
                }
                else
                {
                    logger.Log("Repository type {0} could not be found.".FormatWith(factory.RepositoryTypeName), Severity.Error);
                }
            }

            Task.Factory.StartNew(() => WarmUpObjectContexts(objectContextFactories.ToArray()));
        }

        /// <summary>
        /// Warms up the object contexts by forcing them to load the storage layer and compile views.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        private void WarmUpObjectContexts(IEnumerable<ConnectionStringObjectContextFactory> objectContextFactories)
        {
            foreach (var f in objectContextFactories)
            {
                try
                {
                    if (f.RepositoryType == null) continue;

                    var objectContext = f.GetObjectContext();

                    if (objectContext == null || objectContext.MetadataWorkspace == null) continue;

                    InitializeMappingViewCacheFactory(objectContext, f.RepositoryType, f.CacheType);

                    objectContext.MetadataWorkspace.LoadFromAssembly(f.RepositoryType.Assembly);

                    var entityType = f.RepositoryType.GetProperties().Where(p => p.PropertyType.EqualsGenericTypeFor(typeof(IQueryable<>))).Select(p => p.PropertyType.GetGenericArguments()[0]).FirstOrDefault();

                    if (entityType == null) continue;

                    // force load of workspace
                    objectContext.CreateObjectSet(entityType).ToTraceString();

                }
                catch (Exception ex)
                {
                    Trace.TraceWarning(ex.ToString());
                }
            }
        }


        /// <summary>
        /// A custom pre-generated views loader (since the built in one requires custom derived ObjectContext types (which we don't use).
        /// </summary>
        internal void InitializeMappingViewCacheFactory(ObjectContext objectContext, Type repositoryType, Type cacheType)
        {
            var itemCollection = (StorageMappingItemCollection)objectContext.MetadataWorkspace.GetItemCollection(DataSpace.CSSpace);
            if (itemCollection == null)
                return;

            contextTypesWithViewCacheInitialized.GetOrAdd(repositoryType, t =>
            {
                // by convention, the pre-generated views should be named as the repository type name + "DbMappingViewCache"
                // would use DbMappingViewCacheTypeAttribute.ContextType and allow assembly attributes, but it's internal for some reason
                if (cacheType != null && cacheType.Is<DbMappingViewCache>())
                {
                    itemCollection.MappingViewCacheFactory = new DefaultDbMappingViewCacheFactory(cacheType);
                    return true;
                }

                return false;
            });
        }

        public ConnectionStringObjectContextFactoryCollection ObjectContextFactories { get; private set; }

        public void ClearEntityFrameworkCache()
        {
            foreach (var ocf in ObjectContextFactories.ToArray())
            {
                var oc = ocf.GetObjectContext();
                var itemColleciton = oc.MetadataWorkspace.GetItemCollection(DataSpace.SSpace);
                var queryCacheManagerProperty = itemColleciton.GetType().GetProperty("QueryCacheManager", BindingFlags.Instance | BindingFlags.NonPublic);
                if (queryCacheManagerProperty == null)
                {
                    throw new InvalidOperationException("SSpace's items collection must have QueryCacheManager. EF 6 implementation changed?");
                }
                var queryCacheManager = queryCacheManagerProperty.GetGetter()(itemColleciton);
                var clearMethod = queryCacheManager.GetType().GetMethod("Clear", BindingFlags.Instance | BindingFlags.NonPublic);
                clearMethod.GetInvoker()(queryCacheManager);
            }
        }

        private ObjectContext GetObjectContext(Type repositoryType)
        {
            var factory = ObjectContextFactories.FirstOrDefault(i => i.RepositoryType.Is(repositoryType));
            if (factory == null)
            {
                throw new InvalidOperationException("Repository type {0} is not supported.".FormatWith(repositoryType.Name));
            }
            var objectContext = factory.GetObjectContext();
            if (objectContext == null)
            {
                throw new InvalidOperationException("Repository type {0} is not supported.".FormatWith(repositoryType.Name));
            }
            return objectContext;
        }

        #region IRepositoryProviderFactory Members

        public IEnumerable<Type> SupportedRepositoryTypes
        {
            get { return ObjectContextFactories.Select(i => i.RepositoryType); }
        }

        public object GetRepositoryProvider(Type repositoryType)
        {
            return getProvider(() => GetObjectContext(repositoryType));
        }

        #endregion
    }

    /// <summary>
    /// Copy of System.Data.Entity.Infrastructure.MappingViews.DefaultDbMappingViewCacheFactory which is internal.
    /// </summary>
    internal class DefaultDbMappingViewCacheFactory : DbMappingViewCacheFactory
    {
        private readonly Type cacheType;

        public DefaultDbMappingViewCacheFactory(Type cacheType)
        {
            this.cacheType = cacheType;
        }

        public override DbMappingViewCache Create(string conceptualModelContainerName, string storeModelContainerName)
        {
            return (DbMappingViewCache)Activator.CreateInstance(cacheType);
        }

        public override int GetHashCode()
        {
            return cacheType.GetHashCode() * 397 ^ typeof(DefaultDbMappingViewCacheFactory).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var viewCacheFactory = obj as DefaultDbMappingViewCacheFactory;
            if (viewCacheFactory != null)
                return ReferenceEquals(viewCacheFactory.cacheType, cacheType);
            return false;
        }
    }

    /// <summary>
    ///   A default IObjectContextFactory that uses a metadata connection string.
    /// </summary>
    public class ConnectionStringObjectContextFactory
    {
        private string connectionString;

        [XmlIgnore]
        public virtual Type ContextType { get; set; }

        [XmlAttribute("repositoryTypeName")]
        public virtual string RepositoryTypeName { get; set; }

        [XmlAttribute("connectionString")]
        public virtual string ConnectionString
        {
            get
            {
                var pcs = ProviderConnectionString;
                if (pcs == null) return connectionString;

                var builder = DbConnections.TryGetConnectionStringBuilder<EntityConnectionStringBuilder>(connectionString);
                if (builder == null) return connectionString;

                builder.ProviderConnectionString = pcs;

                return builder.ToString();
            }
            set { connectionString = value; }
        }

        [XmlAttribute("connectionStringName")]
        public virtual string ConnectionStringName { get; set; }

        [XmlAttribute("metadata")]
        public virtual string Metadata { get; set; }

        [XmlAttribute("lazyLoadingEnabled")]
        public virtual bool LazyLoadingEnabled { get; set; }

        [XmlAttribute("proxyCreationEnabled")]
        public virtual bool ProxyCreationEnabled { get; set; }

        [XmlAttribute("containerName")]
        public virtual string ContainerName { get; set; }

        [XmlIgnore]
        public virtual Type RepositoryType
        {
            get { return RepositoryTypeName.ToType(); }
        }

        public virtual ObjectContext GetObjectContext()
        {
            ObjectContext objectContext;
            if (ContextType != null)
            {
                var instance = ContextType.CreateInstance(ConnectionString) as ObjectContext;

                if (instance == null) throw new NotSupportedException();

                objectContext = instance;
            }
            else
            {
                objectContext = new ObjectContext(ConnectionString);
                if (objectContext.DefaultContainerName.IsNullOrEmpty())
                {
                    var defaultContainerName = ContainerName;
                    if (defaultContainerName.IsNullOrEmpty())
                    {
                        defaultContainerName = RepositoryType.GetAttribute<RepositoryAttribute>().Name;
                    }
                    if (defaultContainerName.IsNullOrEmpty())
                    {
                        defaultContainerName = RepositoryType.Name;
                    }
                    var cspaceItems = objectContext.MetadataWorkspace.GetItems<EntityContainer>(DataSpace.CSpace);
                    if (cspaceItems != null)
                    {
                        var defaultContainer = cspaceItems.FirstOrDefault(i => i.Name == defaultContainerName);
                        if (defaultContainer != null)
                        {
                            objectContext.DefaultContainerName = defaultContainer.Name;
                        }
                    }
                }
            }
            objectContext.ContextOptions.LazyLoadingEnabled = LazyLoadingEnabled;
            objectContext.ContextOptions.ProxyCreationEnabled = ProxyCreationEnabled;
            return objectContext;
        }

        /// <summary>
        /// Gets the provider connection string. Allows derived types to specify custom handling for determining the connection string at runtime without knowledge of EF formatted connection strings.
        /// </summary>
        /// <value>
        /// The provider connection string.
        /// </value>
        public virtual string ProviderConnectionString
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets or sets the fully qualified type name of the DbMappingViewCache to use.
        /// </summary>
        /// <value>
        /// The name of the cache type.
        /// </value>
        [XmlAttribute("cacheTypeName")]
        public string CacheTypeName { get; set; }

        [XmlIgnore]
        public virtual Type CacheType
        {
            get { return CacheTypeName.IsNullOrEmpty() ? null : CacheTypeName.ToType(); }
        }
    }

    [XmlRoot("entityFrameworkConfiguration")]
    public class EntityFrameworkConfiguration
    {
        [XmlElement("connection")]
        public List<ConnectionStringObjectContextFactory> Connections { get; set; }
    }

    /// <summary>
    /// Extensions/Utility Methods for Entity Framework
    /// </summary>
    public static class EntityFramework
    {
        public const string EntityTypeColumnName = "__EntityType__";
        public const string LocalIndexColumnName = "__LocalIndex__";

        private static readonly MethodInfo ExecuteFunctionMethod = typeof(ObjectContext).GetMethods().First(m => m.Name == "ExecuteFunction" && m.IsGenericMethodDefinition);
        private static readonly MethodInfo CreateObjectMethod = typeof(ObjectContext).GetMethod("CreateObject");

        private static readonly IDictionary<Tuple<string, Type>, EntityType> EntityTypeCache = new Dictionary<Tuple<string, Type>, EntityType>();

        /// <summary>
        /// Ensures the connection is open. EF 6 recognize connection that are permanently open.
        /// </summary>
        /// <param name="oc">The oc.</param>
        /// <returns></returns>
        public static ObjectContext EnsureConnectionOpen(this ObjectContext oc)
        {
            var ec = oc.Connection as EntityConnection;

            if (oc.Connection.State == ConnectionState.Closed)
            {
                if (ec != null && ec.StoreConnection is AdoServiceConnection)
                {
                    ec.StoreConnection.Open();
                }
                else
                {
                    oc.Connection.Open();
                }
            }

            return oc;
        }

        public static string GetTableName(this ObjectContext context, Type entityType)
        {
            string sql = context.CreateObjectSet(entityType).ToTraceString();
            var regex = new Regex("FROM (?<table>.*) AS");
            Match match = regex.Match(sql);

            string table = match.Groups["table"].Value;
            return table;
        }

        public static Dictionary<string, string> GetStoragePropertyMap(this ObjectContext context, Type entityType)
        {
            string sql = context.CreateObjectSet(entityType).ToTraceString();
            var regex = new Regex(@"\[Extent1\]\.\[(?<SSpaceProperty>.*)\] AS \[(?<CSpaceProperty>.*)\]");

            var properties = new Dictionary<string, string>();

            foreach (var match in regex.Matches(sql).OfType<Match>())
            {
                // in case of Enums, CSpace and SSpace will both have Id suffix, but the CLR property name will not have the Id suffix...need to account for that
                var cSpaceProperty = match.Groups["CSpaceProperty"].Value;
                if (entityType.GetProperty(cSpaceProperty, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic) == null && cSpaceProperty.EndsWith("Id"))
                {
                    cSpaceProperty = cSpaceProperty.Substring(0, cSpaceProperty.Length - 2);
                }

                if (entityType.GetProperty(cSpaceProperty, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic) != null)
                {
                    properties[cSpaceProperty] = match.Groups["SSpaceProperty"].Value;
                }
            }

            return properties.GroupBy(i => i.Value).ToDictionary(i => i.First().Key, i => i.Key);
        }

        public static XDocument ToCompactEntitiesXml(this object[] entities, EntityType entityType, Type clrType, out Dictionary<string, string> compactPropertyNamesMap)
        {
            // Shorten property names to minimize output Xml footprint
            compactPropertyNamesMap = new Dictionary<string, string>();

            var root = new XElement("root");
            var document = new XDocument(root);
            if (entities.Length == 0) return document;

            var propertyGetters = entityType.Properties.ToDictionary(
                p => p.Name,
                p => clrType.GetProperty(p.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                    .GetGetMethod(true).GetInvoker());
            var nextUniquePropertyNumber = 1;

            for (int index = 0; index < entities.Length; index++)
            {
                var item = entities[index];
                var propertyNamesMapLocal = compactPropertyNamesMap;
                var element = new XElement("i", propertyGetters
                    .Select(g => new {g.Key, Value = g.Value(item)})
                    .Concat(new[]
                    {
                        new {Key = EntityTypeColumnName, Value = (object) entityType.Name}, // Add entity type
                        new {Key = LocalIndexColumnName, Value = (object) index} // Add index
                    })
                    .Select(g => new
                    {
                        Key = propertyNamesMapLocal.GetValue(g.Key, () => "p" + nextUniquePropertyNumber++),
                        Value = g.Value is DateTime 
                            ? ((DateTime) g.Value).ToString() 
                            : g.Value is Enum 
                                ? (int)g.Value 
                                : g.Value
                    })
                    .Where(pair => pair.Value != null)
                    .Select(pair => new XAttribute(pair.Key, pair.Value)));

                root.Add(element);
            }

            return document;
        }

        /// <summary>
        /// Sets the SQL compatibility mode on an Entity Framework SSDL.
        /// </summary>
        /// <param name="ssdl">The SSDL.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string SetSqlCompatibilityLevel(string ssdl, string value)
        {
            try
            {
                XDocument document = XDocument.Parse(ssdl);
                XAttribute attribute = document.Elements().First().Attribute("ProviderManifestToken");
                if (attribute != null)
                {
                    attribute.Value = value;
                    return document.ToString();
                }
                throw new InvalidOperationException("Could not find ProviderManifestToken on {0}.".FormatWith(value));
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Error setting SQL compatibility mode {0} on {1}.".FormatWith(value, ssdl), ex);
            }
        }

        /// <summary>
        ///   Attaches the specified object or objects (if enumerable) to the objectContext if it is not already attached. Also performs the specified action
        ///   on all objects in toAttach.
        /// </summary>
        /// <param name = "objectContext">The object context.</param>
        /// <param name = "toAttach">To attach.</param>
        /// <param name = "action">The action.</param>
        public static void Attach(this ObjectContext objectContext, object toAttach, Action<object> action = null)
        {
            AttachInternal(objectContext, toAttach, action);
        }

        /// <summary>
        /// Determines whether the clr entity type is a read only entity in the metadata workspace (a view with no update functions mapped).
        /// </summary>
        /// <param name="clrEntityType">Type of the CLR entity.</param>
        /// <param name="objectContext">The object context.</param>
        /// <returns>
        ///   <c>true</c> if [is read only] [the specified CLR entity type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsReadOnly(this Type clrEntityType, ObjectContext objectContext)
        {
            // force load of workspace
            objectContext.MetadataWorkspace.LoadFromAssembly(clrEntityType.Assembly);

            clrEntityType = ObjectContext.GetObjectType(clrEntityType);

            var objectItemCollection = (ObjectItemCollection)objectContext.MetadataWorkspace.GetItemCollection(DataSpace.OSpace);

            var entityType = (from oType in objectItemCollection.GetItems<EntityType>()
                              where objectItemCollection.GetClrType(oType) == clrEntityType
                              select objectContext.MetadataWorkspace.GetEdmSpaceType(oType)).OfType<EntityType>().First();

            var entitySet = (from container in objectContext.MetadataWorkspace.GetItems<EntityContainer>(DataSpace.CSpace)
                             from set in container.BaseEntitySets.OfType<EntitySet>()
                             where entityType.GetTypeAndBaseTypes().Contains(set.ElementType)
                             select set).First();

            // force load of workspace
            objectContext.CreateQuery<EntityObject>("{0}.{1}".FormatWith(entitySet.EntityContainer.Name, entitySet.Name)).ToTraceString();

            var mapping = objectContext.MetadataWorkspace.GetItems(DataSpace.CSSpace).EnsureNotDefault(new InvalidOperationException("Mapping space not found."))
                .Select(Objects.FullAccessDynamicWrapper.For).SelectMany(i => ((IEnumerable)i.AllSetMaps).OfType<object>().Select(Objects.FullAccessDynamicWrapper.For))
                .First(m => m.Set == entitySet);

            var entityTypeMapping = Objects.FullAccessDynamicWrapper.For(((IEnumerable)mapping.TypeMappings).OfType<object>().First(i => Objects.FullAccessDynamicWrapper.For(i).IsOfTypes.Contains(entityType) || Objects.FullAccessDynamicWrapper.For(i).Types.Contains(entityType)));

            var hasDefiningQuery = ((IEnumerable)entityTypeMapping.MappingFragments).OfType<object>().Any(i => Objects.FullAccessDynamicWrapper.For(Objects.FullAccessDynamicWrapper.For(i).TableSet).DefiningQuery != null);

            var isReadOnly = hasDefiningQuery && !((bool)mapping.HasModificationFunctionMapping);

            return isReadOnly;
        }

        [DebuggerNonUserCode]
        public static void TryRefresh(this ObjectContext objectContext, object entity)
        {
            try
            {
                objectContext.Refresh(RefreshMode.ClientWins, entity);
            }
            catch (InvalidOperationException)
            {
                objectContext.ObjectStateManager.ChangeObjectState(entity, EntityState.Added);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
        }

        private static void AttachInternal(this ObjectContext objectContext, object toAttach, Action<object> action = null)
        {
            if (toAttach is IEnumerable)
            {
                foreach (object item in toAttach as IEnumerable)
                {
                    objectContext.AttachInternal(item, action);
                }
                return;
            }

            ObjectStateEntry objectStateEntry;
            if (!objectContext.ObjectStateManager.TryGetObjectStateEntry(toAttach, out objectStateEntry))
            {
                if (toAttach is IEntityWithKey)
                {
                    objectContext.Attach(toAttach as IEntityWithKey);
                }
                else
                {
                    Type entityType = ObjectContext.GetObjectType(toAttach.GetType());

                    EntitySet entitySet = objectContext.GetEntitySet(entityType).EnsureNotDefault(new InvalidOperationException("Could not find entity set for type {0}.".FormatWith(entityType.Name)));

                    var oldObjectStateEntries = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added).ToArray();

                    objectContext.AddObject("{0}.{1}".FormatWith(entitySet.EntityContainer.Name, entitySet.Name), toAttach);

                    var newObjectStateEntries = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added).Except(oldObjectStateEntries).ToArray();
                    newObjectStateEntries.ForEach(objectContext.UpdateObjectStateEntry);

                }
            }

            if (action != null)
            {
                action(toAttach);
            }
        }


        /// <summary>
        /// Updates the state of the object state entry.
        /// </summary>
        /// <param name="objectContext">The object context.</param>
        /// <param name="objectStateEntry">The object state entry.</param>
        internal static void UpdateObjectStateEntry(this ObjectContext objectContext, ObjectStateEntry objectStateEntry)
        {
            if (objectStateEntry.Entity != null)
            {
                var entityType = objectContext.GetEntityType(objectStateEntry.Entity.GetType());

                // if any keys have default value then the entity is new, so mark it such
                if (entityType.HasIdentityAndIsNew(objectStateEntry.Entity))
                {
                    objectStateEntry.ChangeState(EntityState.Added);
                    return;
                }

                objectStateEntry.ChangeState(EntityState.Modified);

                if (!entityType.HasStoreGeneratePatternPrimaryKey(StoreGeneratedPattern.Identity))
                {
                    // refresh from store in case entity has primary key with manually set value (eg. Guid)
                    objectContext.TryRefresh(objectStateEntry.Entity);
                }
            }
            else if (objectStateEntry.IsRelationship)
            {
                // check if new by querying the DB

                var currentValues = objectStateEntry.CurrentValues;

                if (currentValues.FieldCount == 0) return;

                var entityKeys = Enumerable.Range(0, currentValues.FieldCount).ToDictionary(currentValues.GetName, i => currentValues.GetValue(i) as EntityKey)
                    .Where(i => i.Value != null && i.Value.EntityKeyValues != null && i.Value.EntityKeyValues.IsNotNullOrEmpty()).ToArray();

                if (entityKeys.Length != currentValues.FieldCount)
                {
                    objectStateEntry.ChangeState(EntityState.Added);
                    return;
                }

                int index = 0;

                var parameters =
                    (from k in entityKeys
                     from v in k.Value.EntityKeyValues
                     let parameterName = "id" + index++
                     select new
                     {
                         Filter = "x.{0}.{1} = {2}".FormatWith(k.Key, v.Key, "@" + parameterName),
                         ObjectParameter = new ObjectParameter(parameterName, v.Value)
                     }).ToArray();

                var queryString = @"SELECT VALUE x.{0} FROM {1}.{2} AS x WHERE {3}".FormatWith(
                    entityKeys.First().Key,
                    objectStateEntry.EntitySet.EntityContainer.Name, objectStateEntry.EntitySet.Name,
                    parameters.Select(p => p.Filter).Join(" AND "));

                var query = objectContext.CreateQuery<EntityObject>(queryString, parameters.Select(p => p.ObjectParameter).ToArray());

                query.MergeOption = MergeOption.NoTracking;

                var count = query.Count();

                if (count == 0) objectStateEntry.ChangeState(EntityState.Added);
            }
        }

        /// <summary>
        ///   Creates an EntityFramework generic ObjectSet using the specified context for the specific elementType.
        /// </summary>
        /// <param name = "objectContext">The context.</param>
        /// <param name = "elementType">Type of the element.</param>
        /// <returns></returns>
        public static ObjectQuery CreateObjectSet(this ObjectContext objectContext, Type elementType)
        {
            Type originalElementType = elementType;
            ObjectQuery objectQuery = null;
            while (elementType.BaseType != null && elementType.BaseType != elementType && !TryCreateObjectSet(objectContext, elementType, out objectQuery))
            {
                elementType = elementType.BaseType;
            }

            if (objectQuery != null && originalElementType != elementType)
            {
                objectQuery = Reflector.GetMember(() => new object[0].AsQueryable().OfType<object>())
                    .CastTo<MethodInfo>()
                    .GetGenericMethodDefinition()
                    .MakeGenericMethod(originalElementType)
                    .Invoke(null, new[] { objectQuery }).CastTo<ObjectQuery>();
            }

            return objectQuery;
        }

        [DebuggerNonUserCode]
        private static bool TryCreateObjectSet(ObjectContext objectContext, Type elementType, out ObjectQuery objectQuery)
        {
            try
            {
                MethodInfo method = objectContext.GetType().GetMethod("CreateObjectSet", Type.EmptyTypes).MakeGenericMethod(elementType);
                object value = method.Invoke(objectContext, new object[] { });
                objectQuery = value.CastTo<ObjectQuery>();
                return true;
            }
            catch
            {
                objectQuery = null;
                return false;
            }
        }

        /// <summary>
        /// Gets the EntityType for the CLR type.
        /// </summary>
        /// <returns></returns>
        public static EntityType GetEntityType(this ObjectContext objectContext, Type clrEntityType)
        {
            return EntityTypeCache
                .GetValue(
                    Tuple.Create(objectContext.DefaultContainerName, clrEntityType), () =>
                                                                                         {
                                                                                             clrEntityType = ObjectContext.GetObjectType(clrEntityType);

                                                                                             // force load ospace
                                                                                             objectContext.MetadataWorkspace.LoadFromAssembly(clrEntityType.Assembly);

                                                                                             var objectItemCollection = (ObjectItemCollection)objectContext.MetadataWorkspace.GetItemCollection(DataSpace.OSpace);

                                                                                             var entityType = (from oType in objectItemCollection.GetItems<EntityType>()
                                                                                                               let edmSpaceType = objectContext.MetadataWorkspace.GetEdmSpaceType(oType)
                                                                                                               where objectItemCollection.GetClrType(oType) == clrEntityType && edmSpaceType is EntityType
                                                                                                               select (EntityType)edmSpaceType).FirstOrDefault();

                                                                                             return entityType;
                                                                                         });
        }

        /// <summary>
        /// Gets the entity set value for the specified entityType. Assumes that there is an EntitySet property on the type.
        /// </summary>
        /// <param name="objectContext">The object context.</param>
        /// <param name="clrEntityType">Type of the entity.</param>
        /// <returns></returns>
        public static EntitySet GetEntitySet(this ObjectContext objectContext, Type clrEntityType)
        {
            var entityType = objectContext.GetEntityType(clrEntityType);

            if (entityType == null) throw new InvalidOperationException("No Edm Entity Type was found for type {0}.".FormatWith(clrEntityType.Name));

            var entityTypes = entityType.GetTypeAndBaseTypes();

            var entitySets = from container in objectContext.MetadataWorkspace.GetItems<EntityContainer>(DataSpace.CSpace)
                             from set in container.BaseEntitySets.OfType<EntitySet>()
                             where entityTypes.Contains(set.ElementType)
                             select set;

            return entitySets.FirstOrDefault();
        }

        public static IEnumerable<EntityType> GetTypeAndBaseTypes(this EntityType entityType)
        {
            yield return entityType;

            while (entityType.BaseType is EntityType)
            {
                entityType = (EntityType)entityType.BaseType;
                yield return entityType;
            }
        }

        /// <summary>
        /// Determines whether the entity has a primary key property with the specified store generated pattern type.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="pattern">The pattern.</param>
        /// <returns>
        ///   <c>true</c> if [has store generate pattern primary key] [the specified entity set]; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasStoreGeneratePatternPrimaryKey(this EntityType entityType, StoreGeneratedPattern pattern)
        {
            return entityType.KeyMembers.All(i => i.HasStoreGeneratedPattern(pattern));
        }

        /// <summary>
        /// Determines whether the member has a specified store generated pattern.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static bool HasStoreGeneratedPattern(this EdmMember member, StoreGeneratedPattern pattern)
        {
            MetadataProperty metadataProperty;
            return (member.MetadataProperties.TryGetValue("http://schemas.microsoft.com/ado/2009/02/edm/annotation:StoreGeneratedPattern", false, out metadataProperty) && metadataProperty.Value.ToString() == pattern.ToString())
                || (metadataProperty == null && pattern == StoreGeneratedPattern.None);
        }

        /// <summary>
        /// Determines whether the specified entity type is new by looking at the entity's key values.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="toCheck">To check.</param>
        /// <returns>
        ///   <c>true</c> if the specified entity set is new; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasIdentityAndIsNew(this EntityType entityType, object toCheck)
        {
            var toCheckType = ObjectContext.GetObjectType(toCheck.GetType());
            bool isNew = entityType.HasStoreGeneratePatternPrimaryKey(StoreGeneratedPattern.Identity) &&
                entityType.KeyMembers.All(i => i.TypeUsage.EdmType.As<PrimitiveType>().IfNotNull(pt =>
                    Equals(pt.ClrEquivalentType != null && pt.ClrEquivalentType.IsValueType ? pt.ClrEquivalentType.CreateInstance() : null,
                    toCheckType.GetProperty(i.Name).GetValue(toCheck, null))));
            return isNew;
        }

        /// <summary>
        ///   Applies the inclusions specified to the ObjectQuery (calls ObjectQuery.Include).
        /// </summary>
        /// <param name = "objectQuery">The object query.</param>
        /// <param name = "inclusions">The inclusions.</param>
        /// <returns></returns>
        internal static ObjectQuery ApplyQueryInclusions(this ObjectQuery objectQuery, IEnumerable<QueryInclusion> inclusions)
        {
            dynamic objectQueryDynamic = objectQuery;

            if (inclusions != null)
            {
                foreach (QueryInclusion inclusion in inclusions)
                {
                    string[] paths = inclusion.Expression.ToMemberAccessStrings();
                    foreach (string path in paths)
                    {
                        objectQueryDynamic = objectQueryDynamic.Include(path);
                    }
                }
            }
            return objectQueryDynamic;
        }

        /// <summary>
        ///   Executes the mapped function in the objectContext.
        /// </summary>
        /// <param name = "objectContext">The object context.</param>
        /// <param name = "resultElementType">Type of the result element.</param>
        /// <param name = "functionName">Name of the function.</param>
        /// <param name = "objectParameters">The object parameters.</param>
        /// <returns></returns>
        public static ObjectResult ExecuteFunction(this ObjectContext objectContext, Type resultElementType, string functionName, params ObjectParameter[] objectParameters)
        {
            return ExecuteFunctionMethod.MakeGenericMethod(resultElementType).GetInvoker()(objectContext, functionName, objectParameters).CastTo<ObjectResult>();
        }

        /// <summary>
        /// Gets the resolved CLR type corresponding to the specified cSpaceEdmType, searching the specified assemblies.
        /// </summary>
        /// <param name="metadataWorkspace">The metadata workspace.</param>
        /// <param name="cSpaceEdmType">Type of the c space edm.</param>
        /// <returns></returns>
        internal static Type GetClrType(this MetadataWorkspace metadataWorkspace, EdmType cSpaceEdmType)
        {
            var collectionType = cSpaceEdmType.As<CollectionType>();
            if (collectionType != null)
            {
                Type elementType = metadataWorkspace.GetClrType(collectionType.TypeUsage.EdmType);
                elementType.IfNull(() => new ArgumentException("Could not resolve item type for collection type {0}".FormatWith(collectionType.FullName)).Throw());

                return elementType;
            }

            var structuralType = cSpaceEdmType.As<StructuralType>();
            if (structuralType != null)
            {
                return ((ObjectItemCollection)metadataWorkspace.GetItemCollection(DataSpace.OSpace)).GetClrType(metadataWorkspace.GetObjectSpaceType(structuralType));
            }

            var primitiveType = cSpaceEdmType.As<PrimitiveType>();
            if (primitiveType != null)
            {
                return primitiveType.ClrEquivalentType;
            }

            return null;
        }

        /// <summary>
        ///   Creates the object, invoking the generic CreateObject method on the objectContext.
        /// </summary>
        /// <param name = "objectContext">The object context.</param>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        public static object CreateObject(this ObjectContext objectContext, Type type)
        {
            return CreateObjectMethod.MakeGenericMethod(type).GetInvoker()(objectContext, type);
        }

        /// <summary>
        ///   Creates an ObjectComparison, with the default options for EF (ignoring certain properties, and using base types for EF specific property types).
        /// </summary>
        /// <param name = "allowProxyToNonProxyComparison">if set to <c>true</c> [allow proxy to non proxy comparison].</param>
        /// <returns></returns>
        public static Objects.ObjectComparison CreateEntityFrameworkObjectComparison(bool allowProxyToNonProxyComparison = false)
        {
            var comparison = new Objects.ObjectComparison { ElementsToIgnore = new[] { "RelationshipManager", "RelationshipSet", "_entityWrapper", "IsLoaded" }.ToList() };
            if (allowProxyToNonProxyComparison)
            {
                comparison.TypeToCompareResolver = t => t.IsGenericTypeFor(typeof(EntityCollection<>)) || t.Name == "FixupCollection`1" ? typeof(ICollection<>).MakeGenericType(t.GetGenericArguments()[0]) : ObjectContext.GetObjectType(t);
            }
            return comparison;
        }
    }

    /// <summary>
    /// Configures Entity Framework to suppor the AdoServiceProvider infrastructure.
    /// </summary>
    public class AdoServiceDbConfiguration : DbConfiguration
    {
        public AdoServiceDbConfiguration()
        {
            SetExecutionStrategy(AdoServiceProviderFactory.Name, () => new DefaultExecutionStrategy());
            SetDefaultConnectionFactory(new AdoServiceConnectionFactory());
            SetProviderFactory(AdoServiceProviderFactory.Name, AdoServiceProviderFactory.Instance);
            SetProviderServices(AdoServiceProviderFactory.Name, new AdoServiceDbProviderServices());
        }
    }

    /// <summary>
    /// Returns AdoServiceConnections for Entity Framework.
    /// </summary>
    public class AdoServiceConnectionFactory : IDbConnectionFactory
    {
        public DbConnection CreateConnection(string nameOrConnectionString)
        {
            return new AdoServiceConnection { ConnectionString = nameOrConnectionString };
        }
    }


    public class AdoServiceDbCommandDefinition : DbCommandDefinition
    {
        public AdoServiceDbCommandDefinition(AdoServiceCommand command)
            : base(command)
        {
        }
    }

    public class AdoServiceDbProviderServices : DbProviderServices
    {
        internal static readonly DbProviderServices SqlProviderServices = System.Data.Entity.SqlServer.SqlProviderServices.Instance;

        protected override void DbCreateDatabase(DbConnection connection, int? commandTimeout, StoreItemCollection storeItemCollection)
        {
            SqlProviderServices.CreateDatabase(new SqlConnection(connection.ConnectionString), commandTimeout, storeItemCollection);
        }

        protected override bool DbDatabaseExists(DbConnection connection, int? commandTimeout, StoreItemCollection storeItemCollection)
        {
            return SqlProviderServices.DatabaseExists(new SqlConnection(connection.ConnectionString), commandTimeout, storeItemCollection);
        }

        protected override string DbCreateDatabaseScript(string providerManifestToken, StoreItemCollection storeItemCollection)
        {
            return SqlProviderServices.CreateDatabaseScript(providerManifestToken, storeItemCollection);
        }

        protected override void DbDeleteDatabase(DbConnection connection, int? commandTimeout, StoreItemCollection storeItemCollection)
        {
            SqlProviderServices.DeleteDatabase(new SqlConnection(connection.ConnectionString), commandTimeout, storeItemCollection);
        }

        protected override DbCommandDefinition CreateDbCommandDefinition(DbProviderManifest providerManifest, DbCommandTree commandTree)
        {
            var sqlCommand = SqlProviderServices.CreateCommandDefinition(providerManifest, commandTree).CreateCommand();
            AdoServiceCommand adoServiceCommand = sqlCommand.ToAdoServiceCommand();
            return new AdoServiceDbCommandDefinition(adoServiceCommand);
        }

        protected override string GetDbProviderManifestToken(DbConnection connection)
        {
            return SqlProviderServices.GetProviderManifestToken(connection);
        }

        protected override DbProviderManifest GetDbProviderManifest(string manifestToken)
        {
            var manifest = SqlProviderServices.GetProviderManifest(manifestToken);
            if (AdoServiceDbProviderManifest.Functions.Any())
            {
                // HACK
                manifest.GetType().GetField("_functions", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).EnsureNotDefault("Could not set functions on manifest.")
                    .SetValue(manifest, new AdoServiceDbProviderManifest(manifestToken).GetStoreFunctions());
            }
            return manifest;
        }
    }

    public class AdoServiceDbProviderManifest : DbXmlEnabledProviderManifest
    {
        public static readonly List<MethodInfo> Functions = new List<MethodInfo>();

        private readonly DbProviderManifest innerDbProviderManifest;

        public AdoServiceDbProviderManifest(string manifestToken)
            : base(CreateDocument(manifestToken).CreateReader())
        {
            innerDbProviderManifest = AdoServiceDbProviderServices.SqlProviderServices.GetProviderManifest(manifestToken);
        }

        private static XDocument CreateDocument(string manifestToken)
        {
            var document = XDocument.Load((XmlReader)AdoServiceDbProviderServices.SqlProviderServices.GetProviderManifest(manifestToken).GetType().GetMethod("GetProviderManifest", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public).Invoke(null, new object[0]));
            var functionsElement = document.Descendants().OfType<XElement>().First(e => e.Name.LocalName == "Functions");
            foreach (var m in Functions.WhereNotDefault())
            {
                AddFunction(functionsElement, m);
            }
            return document;
        }

        private static void AddFunction(XElement functionsElement, MethodInfo methodInfo)
        {
            var ns = functionsElement.Name.Namespace;

            var functionAttribute = methodInfo.GetAttribute<DbFunctionAttribute>().EnsureNotDefault("No EdmFunction attribute found.");

            var function = new XElement(ns + "Function", new XAttribute("BuiltIn", true), new XAttribute("Name", functionAttribute.FunctionName),
                                              new XElement(ns + "ReturnType", new XAttribute("Type", methodInfo.ReturnType.Name)));

            function.Add(
                methodInfo.GetParameters().Select(p =>
                                                  new XElement(ns + "Parameter",
                                                               new XAttribute("Name", p.Name),
                                                               new XAttribute("Type", p.ParameterType.Name),
                                                               new XAttribute("Mode", "In"))).ToArray());

            functionsElement.Add(function);
        }

        public override TypeUsage GetEdmType(TypeUsage storeType)
        {
            return innerDbProviderManifest.GetEdmType(storeType);
        }

        public override TypeUsage GetStoreType(TypeUsage edmType)
        {
            return innerDbProviderManifest.GetStoreType(edmType);
        }

        protected override XmlReader GetDbInformation(string informationType)
        {
            return (XmlReader)typeof(DbProviderManifest).GetMethod("GetDbInformation", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Invoke(innerDbProviderManifest, new[] { informationType });
        }
    }

}