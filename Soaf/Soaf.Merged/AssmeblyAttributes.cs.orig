﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using Soaf;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;
using Soaf.Domain;
using Soaf.EntityFramework;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Threading;
using Soaf.TypeSystem;
using Soaf.Web.Client;

// NOTE: this a temporary workaround to get ILMerge produce Soaf.dll with all need assembly attributes
// TODO: replace this with having a specific components registration class in every assembly which supports fluent syntax.

[assembly: Component(typeof(ConnectionStringObjectContextFactoryCollection), new Type[] { })]
[assembly: Component(typeof(EntityFrameworkRepositoryProvider), new Type[] { })]
[assembly: Component(typeof(ConnectionStringObjectContextFactory), new Type[] { })]
[assembly: Component(typeof(EntityFrameworkRepositoryProviderFactory), new Type[] { typeof(IEntityFrameworkRepositoryProviderFactory) })]
[assembly: Component(typeof(AttributeDisassembler), new Type[] { typeof(IAttributeDisassembler) })]
[assembly: Component(typeof(TypeSystemRepository), new Type[] { typeof(ITypeSystemRepository) })]
[assembly: Component(typeof(DynamicModuleFactory), new Type[] { typeof(IDynamicModuleFactory) })]
[assembly: Component(typeof(TypeGenerator), new Type[] { typeof(ITypeGenerator) })]
[assembly: Component(typeof(Messenger), new Type[] { typeof(IMessenger) })]
[assembly: Component(typeof(RepositoryService), new Type[] { typeof(IRepositoryService) })]
[assembly: Component(typeof(Repository<>), new Type[] { typeof(IRepository<>) })]
[assembly: Component(typeof(RepositoryBehavior), new Type[] { })]
[assembly: Component(typeof(StringXmlConfiguration), new Type[] { typeof(IXmlConfiguration) })]
[assembly: Component(typeof(SelfCleaningCache), new Type[] { typeof(ICache) })]
[assembly: Component(typeof(CachingBehavior), new Type[] { })]
[assembly: Component(typeof(SynchronizationBehavior), new Type[] { })]
[assembly: Component(typeof(AuthorizationBehavior), new Type[] { })]
[assembly: Component(typeof(SynchronizedDictionary<,>), new Type[] { typeof(ISynchronizedDictionary<,>) })]
[assembly: Component(typeof(PrincipalContext), new Type[] { typeof(IPrincipalContext) }, Priority = -2)]
[assembly: Component(typeof(LambdaEvaluator), new Type[] { typeof(IEvaluator) })]
[assembly: Component(typeof(IServiceProvider<>), new Type[] { })]
[assembly: Component(typeof(TraceLogger), new Type[] { typeof(ILogger) })]
[assembly: Component(typeof(ILogManager), new Type[] { })]
[assembly: Component(typeof(IAuthenticationService), new Type[] { }, Priority = -1)]
[assembly: Component(typeof(DataContractSerializerCloner), new Type[] { typeof(ICloner) }, Priority = -1)]
[assembly: Component(typeof(ValidationBehavior), new Type[] { })]
[assembly: Component(typeof(DataErrorInfoBehavior), new Type[] { })]
[assembly: Component(typeof(Mapper<,>), new Type[] { typeof(IMapper<,>) })]
[assembly: Component(typeof(ReadOnlyBehavior), new Type[] { })]
[assembly: Component(typeof(EditableObjectBehavior), new Type[] { })]
[assembly: Component(typeof(NotifyPropertyChangedBehavior), new Type[] { })]
[assembly: Component(typeof(UnitOfWorkProvider), new Type[] { typeof(IUnitOfWorkProvider) })]
[assembly: Component(typeof(UnitOfWorkBehavior), new Type[] { })]
[assembly: Component(typeof(DisposableBehavior), new Type[] { })]
[assembly: Component(typeof(AutoMap<,>), new Type[] { typeof(IMap<,>) }, Priority = -1)]
[assembly: Component(typeof(MapReturnValueBehavior), new Type[] { })]
[assembly: Component(typeof(List<>), new Type[] { typeof(IEnumerable<>) })]
[assembly: Component(typeof(CustomMapperFactory), new Type[] { typeof(ICustomMapperFactory) })]
[assembly: Component(typeof(RemoteServiceBehavior), new Type[] { })]
[assembly: Component(typeof(ProtobufCloner), new Type[] {typeof(ICloner) })]
[assembly: Component(typeof(ConfigurationFileEndpointConfigurationProvider), new Type[] { typeof(IEndpointConfigurationProvider) }, Priority = -1)]
[assembly: RemoteService]
// >-- Presentation
//[assembly: Component(typeof(WindowInteractionManager), new Type[] { typeof(IInteractionManager) })]
//[assembly: Component(typeof(InteractionContext), new Type[] { typeof(IInteractionContext) })]
//[assembly: Component(typeof(InteractionContext<>), new Type[] { typeof(IInteractionContext<>) })]
//[assembly: Component(typeof(Window), new Type[] { typeof(IWindow) })]
//[assembly: XmlnsDefinition("http://soaf/presentation", "Soaf.Presentation")]
//[assembly: XmlnsDefinition("http://soaf/presentation", "Soaf.Presentation.Controls")]
//[assembly: Component(typeof(NavigableInteractionManager), new Type[] { typeof(INavigableInteractionManager) })]
//[assembly: Component(typeof(ViewModelTypeDescriptionProvider), new Type[] { }, Initialize = true)]
// --<
[assembly: Component(typeof(UserRepository), new Type[] { typeof(IUserRepository) })]
[assembly: Component(typeof(UserRepositoryMembershipProvider), new Type[] { typeof(MembershipProvider) })]
[assembly: Component(typeof(UserRepositoryRoleProvider), new Type[] { typeof(RoleProvider) })]
[assembly: Component(typeof(AuthenticationService), new Type[] { typeof(IAuthenticationService) })]
[assembly: Component(typeof(AdoService), new Type[] { typeof(IAdoService) })]
[assembly: Component(typeof(AdoComService), new Type[] { typeof(IAdoComService) })]
[assembly: Component(typeof(AdoServiceProviderFactoryRegistrar), new Type[] { }, Initialize = true)]
[assembly: Component(typeof(LoggingDatabaseConfiguration), new Type[] { typeof(ILoggingDatabaseConfiguration) }, Priority = -1)]
[assembly: Component(typeof(ExeConfigurationContainer), new Type[] { typeof(IConfigurationContainer) }, Priority = -2)]
[assembly: Component(typeof(TraceMethodCallBehavior), new Type[] { })]
[assembly: Component(typeof(ExtendedObservableCollection<>), new Type[] { typeof(ObservableCollection<>) })]
[assembly: Component(typeof(ConfigurationContainerConnectionStringRepository), new Type[] { typeof(IConnectionStringRepository) })]

namespace Soaf.Merged
{
    /// <summary>
    /// This class is used to ensure that merged assembly gets [assembly: Extension] attribute which cannot be added manually
    /// </summary>
    internal static class ExtensionAttributeEnabled
    {
        public static void EnableExtensionsUsage(this Assembly target)
        {
            // That is enough for compiler to generate attribute
        }
    }
}