﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;
using Soaf.Domain;
using Soaf.Linq;
using Soaf.Linq.Expressions;

[assembly: Component(typeof(RepositoryService), typeof(IRepositoryService))]
[assembly: Component(typeof(Repository<>), typeof(IRepository<>))]
[assembly: Component(typeof(RepositoryBehavior))]

// ReSharper disable once CheckNamespace
namespace Soaf.Domain
{
    /// <summary>
    /// Defines a type that can manage objects and handle update operations.
    /// </summary>
    internal interface IUpdatable : IRevertibleChangeTracking
    {
        void Save(object value);
        void Delete(object value);
        void Persist(IEnumerable<IObjectStateEntry> objectStateEntries);
    }

    /// <summary>
    /// Defines a type that supports lazy loading for the entities it provides.
    /// </summary>
    public interface ISupportsLazyLoading
    {
        bool IsLazyLoadingEnabled { get; set; }
    }

    /// <summary>
    /// Helper methods for Domain interactions.
    /// </summary>
    internal static class Domain
    {
        public const string SaveMethodName = "Save";
        public const string DeleteMethodName = "Delete";

        /// <summary>
        /// Determines whether the method is a conventionally defined Save(object value) or Delete(object value) method.
        /// </summary>
        /// <param name="methodInfo">The method info.</param>
        /// <param name="name">The name.</param>
        /// <returns>
        ///   <c>true</c> if [is attach or delete method] [the specified method info]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSaveOrDeleteMethod(MethodInfo methodInfo, string name = null)
        {
            return ((name == null && new[] { SaveMethodName, DeleteMethodName }.Contains(methodInfo.Name)) || methodInfo.Name == name) && methodInfo.ReturnType.Equals(typeof(void)) && methodInfo.GetParameters().Length == 1;
        }
    }

    /// <summary>
    ///   Denotes that a type is a repository and should behave as such.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = false)]
    public class RepositoryAttribute : ConcernAttribute
    {
        public RepositoryAttribute()
            : base(typeof(RepositoryBehavior))
        {
        }

        public string Name { get; set; }

        public string Namespace { get; set; }
    }

    /// <summary>
    /// Provides bulk operations for persisting changes.
    /// </summary>
    public interface IRepositoryService
    {
        /// <summary>
        /// Persists the specified object state entries.
        /// </summary>
        /// <param name="objectStateEntries">The object state entries.</param>
        /// <param name="repositoryType">Type of the repository.</param>
        /// <param name="returnPersisted">if set to <c>true</c> [return persisted].</param>
        /// <returns></returns>
        IEnumerable<IObjectStateEntry> Persist(IEnumerable<IObjectStateEntry> objectStateEntries, string repositoryType, bool returnPersisted = false);
    }

    internal class RepositoryService : IRepositoryService
    {
        private readonly RepositoryProviderFactoryManager repositoryProviderFactoryManager;

        public RepositoryService(RepositoryProviderFactoryManager repositoryProviderFactoryManager)
        {
            this.repositoryProviderFactoryManager = repositoryProviderFactoryManager;
        }

        public IEnumerable<IObjectStateEntry> Persist(IEnumerable<IObjectStateEntry> objectStateEntries, string repositoryType, bool returnPersisted)
        {
            var repositoryTypeResolved = repositoryType.ToType().EnsureNotDefault("Could not resolve repository type.");
            var provider = repositoryProviderFactoryManager.GetRepositoryProviderFactory(repositoryTypeResolved)
                .EnsureNotDefault("Could not resolve repository type.")
                .GetRepositoryProvider(repositoryTypeResolved)
                .EnsureNotDefault("Could not resolve repository type.")
                .As<IUpdatable>().EnsureNotDefault("Provider is not updatable.");

            provider.Persist(objectStateEntries);

            return returnPersisted ? objectStateEntries : new IObjectStateEntry[0];
        }
    }


    /// <summary>
    /// Visits all entities in a graph and returns a flattened collection of them.
    /// </summary>
    public class EntityGraphVisitor : IEnumerable<object>
    {
        private readonly HashSet<object> visited = new HashSet<object>();

        private readonly Dictionary<Type, IEnumerable<Func<object, object>>> associationGetterCache = new Dictionary<Type, IEnumerable<Func<object, object>>>();

        private readonly Queue<object> toVisit = new Queue<object>();

        public EntityGraphVisitor(IEnumerable<object> entities)
        {
            foreach (var entity in entities)
            {
                toVisit.Enqueue(entity);
            }

            // keep visiting until queue empty
            while (toVisit.Count > 0)
            {
                Visit(toVisit.Dequeue());
            }
        }

        private void Visit(object entity)
        {
            if (entity == null)
            {
                return;
            }

            // don't visit an entity more than once
            if (visited.Add(entity))
            {
                // get/cache getters for association properties on the entity
                var associationsGetters = associationGetterCache.GetValue(entity.GetType(), () => entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                    .Where(p => p.HasAttribute<AssociationAttribute>() && p.CanRead && p.CanWrite)
                    .Select(p => p.GetGetter())
                    .ToArray());

                foreach (var associatedObject in associationsGetters.Select(a => a(entity)))
                {
                    var enumerable = associatedObject as IEnumerable;
                    if (enumerable != null)
                    {
                        // association is one to many - enqueue all items in collection
                        foreach (var item in enumerable)
                        {
                            toVisit.Enqueue(item);
                        }
                    }
                    else if (associatedObject != null)
                    {
                        // association is one to one or many to one, enqueue the singular associated entity
                        toVisit.Enqueue(associatedObject);
                    }
                }
            }
        }

        public IEnumerator<object> GetEnumerator()
        {
            return visited.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

    }

    /// <summary>
    /// Orders entities by their dependencies so they are in a suitable order to save, given foreign key constraints.
    /// </summary>
    public class EntityDependencyOrderer : IEnumerable<IObjectStateEntry>
    {
        private readonly List<IObjectStateEntry> ordered = new List<IObjectStateEntry>();

        public EntityDependencyOrderer(IObjectStateEntry[] objectStateEntries)
        {

            objectStateEntries = objectStateEntries.Where(e => !e.IsRelationship).ToArray();
            var distinctTypes = objectStateEntries.Select(e => e.Object.GetType()).Distinct().ToArray();

            var entityTypeDependencies = distinctTypes
                .ToDictionary(t => t,
                t => t.GetProperties().Where(p => p.CanRead && p.CanWrite && distinctTypes.Any(dt => dt.Is(p.PropertyType))).Select(p => p.PropertyType).Distinct().ToArray());

            var entityTypeOrder = GetEntityTypeOrder(entityTypeDependencies);

            ordered.AddRange(objectStateEntries
                .GroupBy(e => e.Object.GetType())
                .OrderBy(i => entityTypeOrder.IndexOf(i.Key))
                .SelectMany(g => g)
                .ToArray());
        }

        public IEnumerator<IObjectStateEntry> GetEnumerator()
        {
            return ordered.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private Type[] GetEntityTypeOrder(Dictionary<Type, Type[]> entityTypeDependencies)
        {
            var orderedResults = new List<Type>();

            foreach (var type in entityTypeDependencies.Keys)
            {
                if (!orderedResults.Contains(type))
                {
                    orderedResults.Add(type);
                }

                foreach (var dependencyType in entityTypeDependencies[type])
                {
                    var dependencyTypeIndex = orderedResults.IndexOf(dependencyType);

                    if (dependencyTypeIndex == -1)
                    {
                        orderedResults.Insert(0, dependencyType);
                    }
                    else if (dependencyTypeIndex > orderedResults.IndexOf(type))
                    {
                        orderedResults.RemoveAt(dependencyTypeIndex);

                        orderedResults.Insert(orderedResults.IndexOf(type), dependencyType);
                    }
                }
            }

            return orderedResults.ToArray();
        }
    }

    /// <summary>
    /// A repository for a type that automatically identifies the correct repository to use.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Queries this instance.
        /// </summary>
        /// <returns></returns>
        IQueryable<T> Query();

        /// <summary>
        /// Saves the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        void Save(T value);

        /// <summary>
        /// Deletes the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        void Delete(T value);
    }

    /// <summary>
    /// Implementation for IRepositoryQuery that uses the RepositoryQueryProvider.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class Repository<T> : IRepository<T>
    {
        private readonly RepositoryProviderFactoryManager repositoryProviderFactoryManager;
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly RepositoryQueryProvider provider;
        private static Type repositoryType;

        public Repository(IUnitOfWorkProvider unitOfWorkProvider, RepositoryQueryProvider repositoryQueryProvider, RepositoryProviderFactoryManager repositoryProviderFactoryManager)
        {
            repositoryType = ServiceProvider.Current.GetService<RepositoryProviderFactoryManager>().RepositoryProviderFactoryTypes.Keys.FirstOrDefault(t => t.GetProperties().Any(i => i.PropertyType == typeof(IQueryable<T>)));
            if (repositoryType == null) throw new InvalidOperationException("No supporting repository found for type {0}.".FormatWith(typeof(T).Name));

            this.unitOfWorkProvider = unitOfWorkProvider;
            provider = repositoryQueryProvider;
            this.repositoryProviderFactoryManager = repositoryProviderFactoryManager;
        }

        private object GetProvider(IUnitOfWork unitOfWork)
        {
            return unitOfWork.CastTo<UnitOfWork>().Components.GetValue(
                typeof(T), () =>
                                {
                                    var factory = repositoryProviderFactoryManager.GetRepositoryProviderFactory(repositoryType);
                                    var provider = factory.GetRepositoryProvider(repositoryType);
                                    return provider;
                                });
        }

        public IQueryable<T> Query()
        {
            var providerQuery = provider.CreateQuery(unitOfWork =>
                GetProvider(unitOfWork).As<IQueryProducer>()
                .EnsureNotDefault(new InvalidCastException("The repository provider cannot produce queries for type {0}.".FormatWith(typeof(T).Name))),
                typeof(T), typeof(T));

            return (IQueryable<T>)providerQuery;
        }

        public void Save(T value)
        {
            bool isNew;
            IUnitOfWork unitOfWork = unitOfWorkProvider.GetUnitOfWork(out isNew);

            var provider = GetProvider(unitOfWork) as IUpdatable;
            if (provider == null)
            {
                throw new InvalidOperationException("Provider does not support update operations.");
            }

            provider.Save(value);

            if (isNew || unitOfWork.AutoAcceptChanges)
            {
                try
                {
                    unitOfWork.AcceptChanges();
                }
                finally
                {
                    if (isNew) unitOfWork.Dispose();
                }
            }
        }

        public void Delete(T value)
        {
            bool isNew;
            IUnitOfWork unitOfWork = unitOfWorkProvider.GetUnitOfWork(out isNew);

            var provider = GetProvider(unitOfWork) as IUpdatable;
            if (provider == null)
            {
                throw new InvalidOperationException("Provider does not support update operations.");
            }

            provider.Delete(value);

            if (isNew || unitOfWork.AutoAcceptChanges)
            {
                try
                {
                    unitOfWork.AcceptChanges();
                }
                finally
                {
                    if (isNew) unitOfWork.Dispose();
                }
            }
        }
    }

    /// <summary>
    ///   Implementation of behavior supporting a repository
    /// </summary>
    internal class RepositoryBehavior : IInterceptor
    {
        private readonly RepositoryProviderFactoryManager repositoryProviderFactoryManager;
        private readonly Func<RepositoryQueryProvider> repositoryQueryProviderResolver;
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private object target;

        public RepositoryBehavior(RepositoryProviderFactoryManager repositoryProviderFactoryManager, IUnitOfWorkProvider unitOfWorkProvider, Func<RepositoryQueryProvider> repositoryQueryProviderResolver)
        {
            this.repositoryProviderFactoryManager = repositoryProviderFactoryManager;
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.repositoryQueryProviderResolver = repositoryQueryProviderResolver;
        }

        #region IInterceptor Members

        void IInterceptor.Intercept(IInvocation invocation)
        {
            target = invocation.Target;

            if (invocation.Method.ReturnType.IsGenericTypeFor(typeof(IQueryable<>)))
            {
                InterceptQueryable(invocation);
            }
            else if (!invocation.TryProceed())
            {
                if (Domain.IsSaveOrDeleteMethod(invocation.Method, Domain.SaveMethodName))
                {
                    InterceptSave(invocation);
                }
                else if (Domain.IsSaveOrDeleteMethod(invocation.Method, Domain.DeleteMethodName))
                {
                    InterceptDelete(invocation);
                }
                else
                {
                    bool isNew;
                    IUnitOfWork unitOfWork = unitOfWorkProvider.GetUnitOfWork(out isNew);
                    object provider = GetProvider(invocation, unitOfWork);
                    if (provider is IInterceptor)
                    {
                        provider.CastTo<IInterceptor>().Intercept(invocation);
                    }
                    else
                    {
                        throw new NotSupportedException("The method {0} is not supported.".FormatWith(invocation.Method.Name));
                    }
                    if (isNew || unitOfWork.AutoAcceptChanges)
                    {
                        try
                        {
                            unitOfWork.AcceptChanges();
                        }
                        finally
                        {
                            if (isNew) unitOfWork.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        private void InterceptQueryable(IInvocation invocation)
        {
            bool hasImplementation = invocation.TryProceed();
            if (!hasImplementation)
            {

                RepositoryQueryProvider queryProvider = repositoryQueryProviderResolver();
                IQueryable query = queryProvider.CreateQuery(unitOfWork => GetProvider(invocation, unitOfWork).CastTo<IQueryProducer>(), invocation.Method.ReturnType.FindElementType(), invocation.Method);

                invocation.ReturnValue = query;
            }

        }

        private void InterceptSave(IInvocation invocation)
        {
            bool isNew;
            IUnitOfWork unitOfWork = unitOfWorkProvider.GetUnitOfWork(out isNew);

            invocation.Arguments[0].IfNull(() => new ArgumentException("Cannot attach a null value.").Throw());

            var provider = GetProvider(invocation, unitOfWork) as IUpdatable;
            if (provider == null)
            {
                throw new InvalidOperationException("Provider does not support update operations.");
            }

            provider.Save(invocation.Arguments[0]);

            if (isNew || unitOfWork.AutoAcceptChanges)
            {
                try
                {
                    unitOfWork.AcceptChanges();
                }
                finally
                {
                    if (isNew) unitOfWork.Dispose();
                }
            }
        }

        private void InterceptDelete(IInvocation invocation)
        {
            bool isNew;
            IUnitOfWork unitOfWork = unitOfWorkProvider.GetUnitOfWork(out isNew);

            invocation.Arguments[0].IfNull(() => new ArgumentException("Cannot delete a null value.").Throw());

            var provider = GetProvider(invocation, unitOfWork) as IUpdatable;
            if (provider == null)
            {
                throw new InvalidOperationException("Provider does not support update operations.");
            }

            provider.Delete(invocation.Arguments[0]);

            if (isNew || unitOfWork.AutoAcceptChanges)
            {
                try
                {
                    unitOfWork.AcceptChanges();
                }
                finally
                {
                    if (isNew) unitOfWork.Dispose();
                }
            }
        }

        protected virtual object GetProvider(IInvocation invocation, IUnitOfWork unitOfWork)
        {
            var result = unitOfWork.CastTo<UnitOfWork>().Components.GetValue(
                invocation.TargetType, () =>
                                           {
                                               Type repositoryType = invocation.TargetType.GetTypeBaseTypesAndInterfaces().First(t => t.HasAttribute<RepositoryAttribute>(false, false));
                                               object provider = repositoryProviderFactoryManager.GetRepositoryProviderFactory(repositoryType).GetRepositoryProvider(repositoryType);

                                               var changeTrackingProvider = provider as IChangeTrackingExtended;
                                               if (changeTrackingProvider != null)
                                               {
                                                   KeyValuePair<object, ObjectState>[] entries = null;

                                                   changeTrackingProvider.AcceptingChanges += delegate
                                                                                                  {
                                                                                                      entries = changeTrackingProvider.ObjectStateEntries.Select(i => new KeyValuePair<object, ObjectState>(i.Object, i.State)).ToArray();
                                                                                                      entries.ForEach(i => HandleObjectStateEntryChanges(i.Key, i.Value, "OnAcceptingChanges"));
                                                                                                  };

                                                   changeTrackingProvider.AcceptedChanges += delegate { entries.ForEach(i => HandleObjectStateEntryChanges(i.Key, i.Value, "OnAcceptedChanges")); };
                                               }

                                               return provider;
                                           });

            var lazyLoadingProvider = result as ISupportsLazyLoading;
            var lazyLoadingRepository = invocation.Target as ISupportsLazyLoading;
            if (lazyLoadingProvider != null && lazyLoadingRepository != null)
            {
                lazyLoadingProvider.IsLazyLoadingEnabled = lazyLoadingRepository.IsLazyLoadingEnabled;
            }

            return result;
        }

        /// <summary>
        /// Allows repository instance to handle Save/Delete events upon IUnitOfWork AcceptingChanges/AcceptedChanges.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <param name="state">The state.</param>
        /// <param name="methodName">Name of the method.</param>
        private void HandleObjectStateEntryChanges(object instance, ObjectState state, string methodName)
        {
            object target = this.target;
            if (target == null) return;

            var handlerCandidates = target.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Select(i => new { Method = i, Parameters = i.GetParameters() })
                .Where(i => i.Method.Name == methodName && i.Parameters.Length == 2 && i.Parameters[0].ParameterType.IsInstanceOfType(instance) && i.Parameters[1].ParameterType == typeof(ObjectState));

            // handle in a separate UnitOfWork to prevent circular firing if the handler performs its own modifications.
            using (IUnitOfWork work = unitOfWorkProvider.Create())
            {
                work.AutoAcceptChanges = true;
                handlerCandidates.ForEach(i => i.Method.GetInvoker().Invoke(target, instance, state));
            }
        }
    }

    /// <summary>
    ///   A IQueryProvider for repositories.
    /// </summary>
    internal class RepositoryQueryProvider : QueryProvider, IIncludeProvider
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;

        private IRepositoryQueryRoot lastQueryRoot;

        public RepositoryQueryProvider(IUnitOfWorkProvider unitOfWorkProvider)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
        }

        #region IIncludeProvider Members

        public IQueryable ApplyQueryInclusions(IQueryable queryable, IEnumerable<QueryInclusion> inclusions)
        {
            lastQueryRoot.QueryInclusions.AddRange(inclusions);
            return queryable;
        }

        #endregion

        public IQueryable CreateQuery(Func<IUnitOfWork, IQueryProducer> queryProducerResolver, Type elementType, object key)
        {
            var result = base.CreateQuery(null, e =>
                   (IRepositoryQueryRoot)typeof(RepositoryQueryRoot<>).MakeGenericType(elementType).CreateInstance(this, new Func<IQueryProducer>(() => queryProducerResolver(unitOfWorkProvider.GetUnitOfWork())), key, null));

            lastQueryRoot = (IRepositoryQueryRoot)result;

            return result;
        }

        protected override object Execute(Expression expression)
        {
            bool isNew;
            IUnitOfWork unitOfWork = unitOfWorkProvider.GetUnitOfWork(out isNew);

            if (isNew)
            {
                QueryExecutionScope.Completed += delegate
                                                     {
                                                         unitOfWork.Dispose();
                                                     };
            }

            Func<ConstantExpression, Expression> visitor = c =>
                                                               {
                                                                   var root = c.Value as IRepositoryQueryRoot;
                                                                   if (root != null) return root.CreateProviderQueryExpression();
                                                                   var queryable = c.Value as IQueryable;
                                                                   if (queryable != null && queryable.Expression.NodeType != ExpressionType.Extension) return queryable.Expression;
                                                                   return c;
                                                               };

            // create the actual provider queries from the RepositoryQueryRoots
            expression = expression.Replace(visitor);

            object result = ExecuteQuery(expression);

            return result;
        }

        /// <summary>
        /// Executes the entire query expression as a single invocation.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        private static object ExecuteQuery(Expression expression)
        {
            Delegate compiledLambda = Expression.Lambda(expression).Compile();

            try
            {
                // can execute whole query
                object result = compiledLambda.DynamicInvoke();

                var enumerable = result as IEnumerable;
                var elementType = expression.Type.GetQueryableElementType() ?? expression.Type.FindElementType();
                if (elementType != null && enumerable != null && typeof(List<>).MakeGenericType(elementType).Is(expression.Type))
                {
                    result = enumerable.ToList(elementType);
                }
                else if (enumerable != null && expression.Type.Is(typeof(IQueryable<>).MakeGenericType(elementType)))
                {
                    result = enumerable.ToList(elementType).AsQueryable();
                }

                return result;
            }
            catch (TargetInvocationException ex)
            {
                ex.RethrowInnerException();
                // ReSharper disable HeuristicUnreachableCode
                throw;
                // ReSharper restore HeuristicUnreachableCode
            }
        }

        #region Nested type: RepositoryQueryRoot

        internal interface IRepositoryQueryRoot : IQueryable
        {
            IList<QueryInclusion> QueryInclusions { get; }

            Expression CreateProviderQueryExpression();
        }

        /// <summary>
        ///   An expression representing a Query root that can be reduced to a queryProducer queryable.
        /// </summary>
        internal class RepositoryQueryRoot<TElement> : Query<TElement>, IRepositoryQueryRoot
        {
            private readonly Func<IQueryProducer> queryProducerResolver;

            public RepositoryQueryRoot(IQueryProvider provider, Func<IQueryProducer> queryProducerResolver, object key, IList<QueryInclusion> inclusions = null)
                : base(provider, null)
            {
                this.queryProducerResolver = queryProducerResolver;
                Key = key;
                QueryInclusions = inclusions ?? new List<QueryInclusion>();
            }

            public IList<QueryInclusion> QueryInclusions { get; private set; }

            public Expression CreateProviderQueryExpression()
            {
                var queryProducer = queryProducerResolver();
                var reducedValue = ApplyQueryInclusions(queryProducer, Expression.Constant(queryProducer.CreateQuery(ElementType)).ExpandInvocations().ApplyQueryInclusions().EvaluateLocalExpressions());
                return reducedValue;
            }

            public object Key { get; private set; }

            public override string ToString()
            {
                return "{{Query Root of {0}}}".FormatWith(ElementType);
            }

            private Expression ApplyQueryInclusions(IQueryProducer queryProducer, Expression expression)
            {
                var includeProvider = queryProducer as IIncludeProvider;
                if (includeProvider != null && expression.NodeType == ExpressionType.Constant)
                {
                    var rootQueryable = expression.CastTo<ConstantExpression>().Value as IQueryable;
                    if (rootQueryable != null)
                    {
                        rootQueryable = includeProvider.ApplyQueryInclusions(rootQueryable, QueryInclusions);
                        return Expression.Constant(rootQueryable);
                    }
                }
                else if (includeProvider == null && expression.As<ConstantExpression>().IfNotNull(c => c.Value is IRepositoryQueryRoot))
                {
                    expression.CastTo<ConstantExpression>().Value.CastTo<IRepositoryQueryRoot>().QueryInclusions.AddRange(QueryInclusions);
                }
                return expression;
            }

            public override bool Equals(object obj)
            {
                return obj is RepositoryQueryRoot<TElement> && obj.CastTo<RepositoryQueryRoot<TElement>>().Key == Key;
            }

            public override int GetHashCode()
            {
                return Key.GetHashCode();
            }
        }

        #endregion
    }

    /// <summary>
    ///   Returns IRepositoryProviderFactories for a specified Repository type. Scans assemblies in domain for RepositoryProviderFactoryAttribute and
    ///   uses those as the source for the providers.
    /// </summary>
    [Singleton]
    internal class RepositoryProviderFactoryManager
    {
        private readonly Dictionary<Type, Type> repositoryProviderFactoryTypes = new Dictionary<Type, Type>();
        private readonly IServiceProvider serviceProvider;

        public Dictionary<Type, Type> RepositoryProviderFactoryTypes
        {
            get { return repositoryProviderFactoryTypes; }
        }

        public RepositoryProviderFactoryManager(IComponentRegistry componentRegistry, IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;

            foreach (var item in componentRegistry.ComponentRegistrations.SelectMany(r => r.For.IsNullOrEmpty() ? new[] { r.Type } : r.For)
                .Select(t => new { Attribute = t.GetAttribute<RepositoryProviderFactoryAttribute>(), Type = t })
                .Where(i => i.Attribute != null)
                .OrderBy(i => i.Attribute.Priority))
            {
                var factory = serviceProvider.GetService(item.Type).CastTo<IRepositoryProviderFactory>();
                try
                {
                    foreach (Type repositoryType in factory.SupportedRepositoryTypes)
                    {
                        if (repositoryType == null)
                        {
                            throw new InvalidOperationException("One of the supported repository types from factory {0} could not be resolved.".FormatWith(factory.GetType().Name));
                        }
                        repositoryProviderFactoryTypes[repositoryType] = item.Type;
                    }
                }
                finally
                {
                    var disposable = factory.As<IDisposable>();
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
            }
        }

        public IRepositoryProviderFactory GetRepositoryProviderFactory(Type repositoryType)
        {
            Type repositoryProviderType;
            if (repositoryType != null && repositoryProviderFactoryTypes.TryGetValue(repositoryType, out repositoryProviderType))
            {
                return serviceProvider.GetService(repositoryProviderType).CastTo<IRepositoryProviderFactory>();
            }
            throw new NotImplementedException("Could not find a provider for the requested repository type {0}.".FormatWith(repositoryType == null ? "null" : repositoryType.FullName));
        }
    }

    /// <summary>
    ///   Defines a type that can create providers that offer implementation support for repositories.
    /// </summary>
    public interface IRepositoryProviderFactory
    {
        IEnumerable<Type> SupportedRepositoryTypes { get; }

        object GetRepositoryProvider(Type repositoryType);
    }

    /// <summary>
    ///   Denotes that a type can create repository implementation providers and should be registered as such.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class, Inherited = false)]
    internal class RepositoryProviderFactoryAttribute : Attribute
    {
        public int Priority { get; set; }
    }
}
