﻿namespace Soaf.Presentation
{
    /// <summary>
    /// Information about an alert.
    /// </summary>
    public sealed class AlertArguments : InteractionArguments
    {
    }
}