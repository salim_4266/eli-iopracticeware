﻿namespace Soaf.Presentation
{
    /// <summary>
    /// Information about a prompt.
    /// </summary>
    public sealed class PromptArguments : InteractionArguments
    {
        public string DefaultInput { get; set; }
    }
}