﻿using System;
using System.Windows.Controls;

namespace Soaf.Presentation
{
    /// <summary>
    /// Arguments for displaying an exception.
    /// </summary>
    public sealed class ExceptionInteractionArguments
    {
        public ExceptionInteractionArguments()
        {
            Message = "We're sorry, an error has occurred. Please report this to your support representative.";
        }

        /// <summary>
        /// Gets or sets the owner.
        /// </summary>
        /// <value>
        /// The owner.
        /// </value>
        public ContentControl Owner { get; set; }

        /// <summary>
        /// Gets or sets the exception.
        /// </summary>
        /// <value>
        /// The exception.
        /// </value>
        public Exception Exception { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }
    }
}