﻿namespace Soaf.Presentation
{
    /// <summary>
    ///   Choices of buttons to display in confirmation dialogs.
    /// </summary>
    public enum ConfirmButtons
    {
        /// <summary>
        /// </summary>
        OkCancel,

        /// <summary>
        /// </summary>
        YesNo
    }

    /// <summary>
    /// Information about a confirmation.
    /// </summary>
    public sealed class ConfirmationArguments : InteractionArguments
    {
        public ConfirmButtons Buttons { get; set; }

        public object OkButtonContent { get; set; }

        public object CancelButtonContent { get; set; }
    }
}