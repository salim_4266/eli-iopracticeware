﻿using Soaf.ComponentModel;

namespace Soaf.Presentation
{
    /// <summary>
    ///   Indicates that a type requires a dialog context to operate.
    /// </summary>
    public interface IInteractionContextAware
    {
        [Dependency(IsOptional = true)]
        IInteractionContext InteractionContext { get; set; }
    }
}