﻿namespace Soaf.Presentation
{
    public class InteractionEventArgs<TInteractionArguments, TResult> : InteractionEventArgs<TInteractionArguments> where TInteractionArguments : InteractionArguments
    {
        public TResult Result { get; set; }

        public InteractionEventArgs(TInteractionArguments interaction)
            : base(interaction)
        {
        }
    }

    public class InteractionEventArgs<TInteractionArguments> : HandledEventArgs where TInteractionArguments : InteractionArguments
    {
        private readonly TInteractionArguments interaction;

        public TInteractionArguments Interaction
        {
            get { return interaction; }
        }

        public InteractionEventArgs(TInteractionArguments interaction)
        {
            this.interaction = interaction;
        }
    }
}