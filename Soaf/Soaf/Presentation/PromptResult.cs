﻿namespace Soaf.Presentation
{
    /// <summary>
    /// Information about the result of a prompt interaction.
    /// </summary>
    public class PromptResult
    {
        public PromptResult(bool? dialogResult, string input)
        {
            DialogResult = dialogResult;
            Input = input;
        }

        public bool? DialogResult { get; private set; }

        public string Input { get; private set; }
    }
}