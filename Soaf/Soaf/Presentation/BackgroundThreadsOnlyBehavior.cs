﻿using System;
using System.Linq;
using System.Threading;
using Soaf.ComponentModel;

namespace Soaf.Presentation
{
    /// <summary>
    ///   Implementation of behavior supporting check that data access should only happen on a background thread
    /// </summary>
    internal class BackgroundThreadsOnlyBehavior : IInterceptor
    {
        #region IInterceptor Members

        public void Intercept(IInvocation invocation)
        {
            if (BackgroundThreadOnlyAttribute.IsEnabled
                && System.Windows.Application.Current != null
                && !Thread.CurrentThread.IsBackground
                && System.Windows.Application.Current.Dispatcher.CheckAccess()
                && System.Windows.Application.Current.Windows.OfType<System.Windows.Window>().Any(w => w.Dispatcher.CheckAccess() && w.IsActive))
            {
                throw new InvalidOperationException("This operation should only happen on a background thread.");
            }

            invocation.TryProceed();
        }

        #endregion
    }
}