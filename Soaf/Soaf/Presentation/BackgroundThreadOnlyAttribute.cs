﻿using System;
using Soaf.ComponentModel;

namespace Soaf.Presentation
{
    /// <summary>
    ///   Denotes that data access should only happen on a background thread
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = false)]
    public class BackgroundThreadOnlyAttribute : ConcernAttribute
    {
        public BackgroundThreadOnlyAttribute()
            : base(typeof(BackgroundThreadsOnlyBehavior), 1000)
        {
        }

        static BackgroundThreadOnlyAttribute()
        {
            IsEnabled = true;
        }

        public static bool IsEnabled { get; set; }
    }
}