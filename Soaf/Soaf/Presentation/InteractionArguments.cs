﻿using System.Windows;

namespace Soaf.Presentation
{
    /// <summary>
    /// Basic information for an interaction.
    /// </summary>
    public abstract class InteractionArguments : IInteractionContextAware
    {
        private IInteractionContext interactionContext;

        public virtual object Content { get; set; }

        public virtual object Header { get; set; }

        public virtual object Owner { get; set; }

        public IInteractionContext InteractionContext
        {
            get
            {
                if (interactionContext != null) return interactionContext;

                var result = Content.As<FrameworkElement>().IfNotNull(e => e.DataContext).IfNotNull(d => d.As<IInteractionContextAware>())
                                    .IfNotNull(i => i.InteractionContext);

                if (result != null) return result;

                result = Content.As<IInteractionContextAware>().IfNotNull(a => a.InteractionContext);

                return result;
            }
            set
            {
                interactionContext = value;

                Content.As<FrameworkElement>().IfNotNull(e => e.DataContext).IfNotNull(d => d.As<IInteractionContextAware>())
                       .IfNotNull(i => i.InteractionContext = interactionContext);

                Content.As<IInteractionContextAware>().IfNotNull(a => a.InteractionContext = interactionContext);
            }
        }
    }
}