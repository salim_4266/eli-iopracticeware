﻿using System;
using System.Windows.Controls;

namespace Soaf.Presentation
{
    public static class InteractionExtensions
    {
        /// <summary>
        /// Prompts the user for confirmation.
        /// </summary>
        /// <param name="interactionManager">The interaction manager.</param>
        /// <param name="content">The content.</param>
        /// <param name="header">The header.</param>
        /// <param name="defaultInput">The default input.</param>
        /// <param name="owner">The owner.</param>
        /// <returns></returns>
        public static PromptResult Prompt(this IInteractionManager interactionManager, object content, object header = null, string defaultInput = null, ContentControl owner = null)
        {
            var args = new PromptArguments { Content = content, Header = header, Owner = owner, DefaultInput = defaultInput };
            return interactionManager.Prompt(args);
        }

        /// <summary>
        /// Shows the alert.
        /// </summary>
        /// <param name="interactionManager">The interaction manager.</param>
        /// <param name="content">The content.</param>
        /// <param name="header">The header.</param>
        /// <param name="owner">The owner.</param>
        public static void Alert(this IInteractionManager interactionManager, object content, object header = null, ContentControl owner = null)
        {
            var args = new AlertArguments { Content = content, Header = header, Owner = owner };
            interactionManager.Alert(args);
        }

        /// <summary>
        /// Prompts the user for confirmation.
        /// </summary>
        /// <param name="interactionManager"></param>
        /// <param name="content"></param>
        /// <param name="cancelButtonContent"></param>
        /// <param name="okButtonContent"></param>
        /// <param name="header"></param>
        /// <param name="buttons"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static bool? Confirm(this IInteractionManager interactionManager, object content, object cancelButtonContent = null, object okButtonContent = null, object header = null, ConfirmButtons buttons = ConfirmButtons.YesNo, ContentControl owner = null)
        {
            var args = new ConfirmationArguments { Content = content, CancelButtonContent = cancelButtonContent, OkButtonContent = okButtonContent, Header = header, Owner = owner };
            return interactionManager.Confirm(args);
        }

        /// <summary>
        /// Displays the exception.
        /// </summary>
        /// <param name="interactionManager">The interaction manager.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="owner">The owner.</param>
        public static void DisplayException(this IInteractionManager interactionManager, Exception exception, ContentControl owner = null)
        {
            interactionManager.DisplayException(new ExceptionInteractionArguments { Exception = exception, Owner = owner });
        }
    }
}
