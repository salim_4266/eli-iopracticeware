﻿using System;

namespace Soaf.Presentation
{
    /// <summary>
    ///   Provides methods for displaying interactivity content with the user.
    /// </summary>
    public interface IInteractionManager
    {
        /// <summary>
        /// Displays an alert.
        /// </summary>
        /// <param name="arguments">The alert.</param>
        void Alert(AlertArguments arguments);

        /// <summary>
        /// Occurs when [alerting]. Allows for automated handling of interactions.
        /// </summary>
        event EventHandler<InteractionEventArgs<AlertArguments>> Alerting;

        /// <summary>
        /// Prompts the user for text input.
        /// </summary>
        /// <param name="arguments">The prompt.</param>
        PromptResult Prompt(PromptArguments arguments);

        /// <summary>
        /// Occurs when [prompting]. Allows for automated handling of interactions.
        /// </summary>
        event EventHandler<InteractionEventArgs<PromptArguments, PromptResult>> Prompting;

        /// <summary>
        /// Prompts the user for confirmation.
        /// </summary>
        /// <param name="arguments">The confirmation.</param>
        /// <returns></returns>
        bool? Confirm(ConfirmationArguments arguments);

        /// <summary>
        /// Occurs when [confirming]. Allows for automated handling of interactions.
        /// </summary>
        event EventHandler<InteractionEventArgs<ConfirmationArguments, bool?>> Confirming;

        /// <summary>
        /// Displays the error to the user.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        void DisplayException(ExceptionInteractionArguments arguments);

        /// <summary>
        /// Occurs when displaying an exception.
        /// </summary>
        event EventHandler<HandledEventArgs<ExceptionInteractionArguments>> DisplayingException;

        /// <summary>
        /// Shows the specified arguments.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        void Show(InteractionArguments arguments);

        /// <summary>
        /// Occurs when [showing].
        /// </summary>
        event EventHandler<InteractionEventArgs<InteractionArguments>> Showing;
    }
}