﻿namespace Soaf.Presentation
{
    /// <summary>
    /// A static wrapper for the current InteractionManager.
    /// </summary>
    public static class InteractionManager
    {
        private static IInteractionManager current;

        public static IInteractionManager Current
        {
            get { return current ?? ServiceProvider.Current.GetService<IInteractionManager>(); }
            set { current = value; }
        }
    }
}
