﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Soaf.Presentation
{
    public interface IHasBusyStatus : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets a value indicating whether this instance is busy. Returns true if there are any busy components.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is busy; otherwise, <c>false</c>.
        /// </value>
        bool IsBusy { get; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        object Status { get; set; }

        /// <summary>
        /// Gets the busy components.
        /// </summary>
        IList<object> BusyComponents { get; }
    }
}