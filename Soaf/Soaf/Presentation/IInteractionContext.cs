﻿using System;
using System.ComponentModel;

namespace Soaf.Presentation
{
    /// <summary>
    ///   Represents a dialog between the user and the application.
    /// </summary>
    public interface IInteractionContext : IHasBusyStatus, IDisposable
    {
        bool IsComplete { get; }

        /// <summary>
        ///   Gets the result of the dialog
        /// </summary>
        /// <value>The result.</value>
        bool? DialogResult { get; }

        /// <summary>
        /// Completes the dialog with the specified result. Returns false if the completion is canceled.
        /// </summary>
        /// <param name="dialogResult">The result.</param>
        bool Complete(bool? dialogResult = null);

        /// <summary>
        ///   Occurs when completed.
        /// </summary>
        event EventHandler<CancelEventArgs> Completing;

        /// <summary>
        ///   Occurs when completed.
        /// </summary>
        event EventHandler<EventArgs<bool?>> Completed;

        /// <summary>
        /// A messenger scoped to the owning interaction context.
        /// </summary>
        IMessenger Messenger { get; set; }
    }
}