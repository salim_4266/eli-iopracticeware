﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Soaf.Reflection;

namespace Soaf.ComponentModel
{
    /// <summary>
    ///   Information about and for interaction with a runtime method invocation.
    /// </summary>
    public interface IInvocation
    {
        object[] Arguments { get; }
        object Target { get; }
        Type TargetType { get; }
        IConcernDescriptor[] ConcernDescriptors { get; }
        MethodInfo Method { get; }
        object ReturnValue { get; set; }
        void Proceed();
        bool CanProceed { get; }
    }

    /// <summary>
    /// Helper methods for IInvocations.
    /// </summary>
    public static class InvocationExtensions
    {
        /// <summary>
        /// Tries to execute the current invocation. If not implemented returns false.
        /// </summary>
        /// <param name="invocation"></param>
        /// <returns></returns>
        public static bool TryProceed(this IInvocation invocation)
        {
            if (invocation.CanProceed) { invocation.Proceed(); return true; }
            return false;
        }

    }

    /// <summary>
    /// Express interfaces a type should implement (automatically or explicitly).
    /// </summary>
    public interface ITypeInterfacesConcern
    {
        IEnumerable<Type> Interfaces { get; }
    }

    /// <summary>
    ///   A definition for a type that intercepts runtime method invocations.
    /// </summary>
    public interface IInterceptor
    {
        void Intercept(IInvocation invocation);
    }

    /// <summary>
    /// Filters intercepted methods
    /// </summary>
    internal interface IInterceptedMethodsFilter
    {
        bool ShouldIntercept(Type type, MethodInfo method);
    }

    /// <summary>
    /// Identitifes a IInterceptedMethodsFilter for an IInterceptor implementation. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    internal class InterceptedMethodsFilterAttribute : Attribute
    {
        public Type Type { get; private set; }
        public InterceptedMethodsFilterAttribute(Type type)
        {
            Type = type;
            if (type == null || type.GetConstructor(Type.EmptyTypes) == null || !type.Is<IInterceptedMethodsFilter>()) throw new InvalidOperationException("Filter type must have a public parameterless constructor and implement IInterceptedMethodsFilter.");
        }
    }

    /// <summary>
    ///   Describes a concern type for the attributed type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public abstract class ConcernAttribute : Attribute, IConcernDescriptor
    {
        protected ConcernAttribute(Type concernType, int priority = 0)
        {
            ConcernType = concernType;
            Priority = priority;
        }

        /// <summary>
        ///   Gets or sets the order in which this concern type is applied.
        /// </summary>
        /// <value>The order.</value>
        internal virtual int Priority { get; private set; }

        #region IConcernDescriptor Members

        public Type ConcernType { get; private set; }

        public virtual void Visit(ComponentRegistration registration)
        { }

        #endregion

    }

    /// <summary>
    /// Describes a global concern.
    /// </summary>
    public abstract class GlobalConcernAttribute : ConcernAttribute, IGlobalConcernDescriptor
    {
        public GlobalConcernAttribute(Type concernType, int priority = 0)
            : base(concernType, priority)
        {
        }

        /// <summary>
        /// Determines whether this global concern is applicable for the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if [is applicable for] [the specified type]; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool IsApplicableFor(Type type);
    }

    /// <summary>
    ///   Describes a concern.
    /// </summary>
    public interface IConcernDescriptor
    {
        Type ConcernType { get; }

        /// <summary>
        /// Allow concern to modify component registration it is used for
        /// </summary>
        /// <param name="registration"></param>
        void Visit(ComponentRegistration registration);
    }

    /// <summary>
    ///   Describes a concern which applies globally to a set of types.
    /// </summary>
    public interface IGlobalConcernDescriptor : IConcernDescriptor
    {
        bool IsApplicableFor(Type type);
    }

    /// <summary>
    ///   A base class for a a runtime method invocation interceptor that intercepts only members annotated with certain attribute types.
    /// </summary>
    public abstract class AttributedMemberInterceptor : IInterceptor, IInterceptedMethodsFilter
    {
        public abstract IEnumerable<Type> AttributeTypes { get; }

        public abstract void Intercept(IInvocation invocation);

        public virtual bool ShouldIntercept(Type type, MethodInfo method)
        {
            if (AttributeTypes.Any(t => type.HasAttribute(t)) && method.CanBeOveridden()) return true;

            if (AttributeTypes.Any(t => method.HasAttribute(t) || method.GetProperty().IfNotDefault(p => p.HasAttribute(t)))) return true;

            return false;
        }
    }

    /// <summary>
    ///   Lifestyle types describing the lifetime of an object instance.
    /// </summary>
    public enum LifestyleType
    {
        /// <summary>
        ///   An instance-per-resolution lifestyle.
        /// </summary>
        Transient,

        /// <summary>
        ///   A singleton lifestyle.
        /// </summary>
        Singleton,

        /// <summary>
        ///   A lifestyle that is used and spread across the CallContext.
        /// </summary>
        CallContext,

        /// <summary>
        /// A lifestyle that is per thread - a single instance is used across the thread.
        /// </summary>
        Thread,

        /// <summary>
        ///   A lifestyle that is scoped to a resolution context.
        /// </summary>
        Scoped,

        /// <summary>
        /// A lifestyle that is per creation context - a single instance is used across a dependency subtree..
        /// </summary>
        CreationContext

    }

    /// <summary>
    /// Describes a component's lifestyle.
    /// </summary>
    public interface ILifestyleDescriptor
    {
        LifestyleType LifestyleType { get; }
    }

    /// <summary>
    ///   A base class for an attribute describing a types LifestyleType.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public abstract class LifestyleAttribute : Attribute, ILifestyleDescriptor
    {
        private readonly LifestyleType lifestyleType;

        internal LifestyleAttribute(LifestyleType lifestyleType)
        {
            this.lifestyleType = lifestyleType;
        }

        #region ILifestyleDescriptor Members

        public LifestyleType LifestyleType
        {
            get { return lifestyleType; }
        }

        #endregion
    }

    /// <summary>
    /// Defines a scope under which components with a scoped lifestyle are resolved.
    /// </summary>
    public interface IScope : IDisposable
    {
    }

    /// <summary>
    /// A descriptor for how a scoped lifestyle should behave.
    /// </summary>
    public interface IScopedLifestyleDescriptor : ILifestyleDescriptor
    {
        LifestyleType LifestyleOutsideScope { get; }
    }

    /// <summary>
    ///   Denotes a lifestyle that is scoped to a resolution context.
    /// </summary>
    public class ScopedLifestyleAttribute : LifestyleAttribute, IScopedLifestyleDescriptor
    {
        public ScopedLifestyleAttribute(LifestyleType lifestyleOutsideScope = LifestyleType.Transient)
            : base(LifestyleType.Scoped)
        {
            LifestyleOutsideScope = lifestyleOutsideScope;
            if (lifestyleOutsideScope == LifestyleType.Scoped)
            {
                throw new InvalidOperationException("Lifestyle outside scope cannot be Scoped itself.");
            }
        }

        public LifestyleType LifestyleOutsideScope { get; private set; }
    }

    /// <summary>
    /// Denotes a Transient LifestyleType for an annotated type.
    /// </summary>
    public class TransientAttribute : LifestyleAttribute
    {
        public TransientAttribute()
            : base(LifestyleType.Transient)
        {
        }
    }

    /// <summary>
    ///   Denotes a Singleton LifestyleType for an annotated type.
    /// </summary>
    public class SingletonAttribute : LifestyleAttribute
    {
        public SingletonAttribute()
            : base(LifestyleType.Singleton)
        {
        }
    }

    /// <summary>
    ///   Denotes a per thread LifestyleType for an annotated type.
    /// </summary>
    public class CallContextLifestyleAttribute : LifestyleAttribute
    {
        public CallContextLifestyleAttribute()
            : base(LifestyleType.CallContext)
        {
        }
    }

    /// <summary>
    ///   Denotes a per thread LifestyleType for an annotated type.
    /// </summary>
    public class ThreadLifestyleAttribute : LifestyleAttribute
    {
        public ThreadLifestyleAttribute()
            : base(LifestyleType.Thread)
        {
        }
    }

    /// <summary>
    ///   Denotes a lifestyle that is scoped to a resolution context.
    /// </summary>
    public class CreationContextLifestyleAttribute : LifestyleAttribute
    {
        public CreationContextLifestyleAttribute()
            : base(LifestyleType.CreationContext)
        {
        }
    }


    /// <summary>
    /// A container for wrapping an instance. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Container<T> : IDisposable
    {
        public virtual T Instance { get; set; }
        public virtual void Dispose()
        {
        }
    }

    public class PerThreadContainer<T> : Container<T>
    {
        [ThreadStatic]
        private static T instance;

        public override T Instance
        {
            get { return instance; }
            set { instance = value; }
        }

    }

    [CallContextLifestyle]
    public class CallContextLifestyleContainer<T> : Container<T>
    {
    }

    [Singleton]
    public class SingletonContainer<T> : Container<T>
    {
    }

    /// <summary>
    ///   A definition for a type that is concerned with instances of a type.
    /// </summary>
    internal interface IInstanceConcern
    {
        void ApplyConcern(object value);
    }

    /// <summary>
    /// Denotes that interception should be suppressed while a scope is present.
    /// </summary>
    public class SuppressInterceptionScope : IDisposable
    {
        [ThreadStatic]
        private static SuppressInterceptionScope current;

        private readonly SuppressInterceptionScope last;

        public static SuppressInterceptionScope Current
        {
            get { return current; }
        }

        public SuppressInterceptionScope()
        {
            last = Current;
            current = this;
        }

        public void Dispose()
        {
            current = last;
        }
    }

    /// <summary>
    ///   Denotes that a type supports Initialization after construction.
    /// </summary>
    [Initializable]
    public interface IInitializable
    {
        void Initialize();
    }

    /// <summary>
    ///   Denotes that a type should be wired for automatic firing of Initialize after construction.
    /// </summary>
    internal class InitializableAttribute : ConcernAttribute
    {
        public InitializableAttribute()
            : base(typeof(InitializableConcern))
        {
        }
    }

    /// <summary>
    ///   Performs the behavior that fires Initialize after construction of an IInitializable type annotated with InitializableAttribute.
    /// </summary>
    internal class InitializableConcern : IInstanceConcern
    {
        #region IInstanceConcern Members

        public void ApplyConcern(object instance)
        {
            instance.CastTo<IInitializable>().Initialize();
        }

        #endregion
    }

    /// <summary>
    /// Only intercepts setters.
    /// </summary>
    public class InterceptSettersFilter : IInterceptedMethodsFilter
    {
        public bool ShouldIntercept(Type type, MethodInfo method)
        {
            return method.CanBeOveridden() && method.IsSpecialName && method.Name.StartsWith("set_");
        }
    }

    public class InterceptMethodsForTypeFilter<T> : IInterceptedMethodsFilter
    {
        public bool ShouldIntercept(Type type, MethodInfo method)
        {
            return method.CanBeOveridden() && (method.IsMethodImplementationForInterface(typeof(T)) || method.GetMethodAndOverridenMethods().Any(m => m.DeclaringType == typeof(T)));
        }
    }

    /// <summary>
    /// Simply intercepts all methods
    /// </summary>
    public class InterceptAllMethodsFilter : IInterceptedMethodsFilter
    {
        public bool ShouldIntercept(Type type, MethodInfo method)
        {
            return method.CanBeOveridden()
                // Also, avoid methods declared on object type
                && !(method.DeclaringType != null && method.DeclaringType == typeof(object));
        }
    }
}