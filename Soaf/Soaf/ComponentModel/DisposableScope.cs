using System;

namespace Soaf.ComponentModel
{
    public static class DisposableScope
    {
        public static DisposableScope<T> Create<T>(T instance, Action<T> onCreate, Action<T> onDispose)
        {
            return new DisposableScope<T>(instance, onCreate, onDispose);
        }
    }

    public class DisposableScope<T> : IDisposable
    {
        private readonly Action<T> onDispose;
        public T Instance { get; set; }

        public DisposableScope(T instance, Action<T> onCreate, Action<T> onDispose)
        {
            if (onCreate != null) onCreate(instance);
            Instance = instance;
            this.onDispose = onDispose;
        }

        public void Dispose()
        {
            if (onDispose != null) onDispose(Instance);
        }
    }
}