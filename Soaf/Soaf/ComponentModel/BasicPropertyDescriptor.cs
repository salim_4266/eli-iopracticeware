using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using Soaf.Reflection;

namespace Soaf.ComponentModel
{
    /// <summary>
    ///   A basic property descriptor that wraps another PropertyDescriptor, but provides the ability to add additional attributes.
    /// </summary>
    public class BasicPropertyDescriptor : PropertyDescriptor
    {
        private readonly PropertyDescriptor descriptor;
        private readonly Func<object, object> componentSelector;

        public BasicPropertyDescriptor(PropertyDescriptor descriptor, IEnumerable<Attribute> attributes = null, Func<object, object> componentSelector = null)
            : base(descriptor)
        {
            this.descriptor = descriptor;
            this.componentSelector = componentSelector;
            if (attributes != null) AttributeArray = attributes.ToArray();
            PreventValueChangedHandlers = true;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to [prevent value changed handlers]. If true, will ignore requests to AddValueChanged and will 
        /// clear the ValueChangedEventManager upon such requests. The default value is true, to prevent memory leaks.
        /// </summary>
        /// <value>
        /// <c>true</c> if [prevent value changed handlers]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool PreventValueChangedHandlers { get; set; }

        public override Type ComponentType
        {
            get { return descriptor.ComponentType; }
        }

        public override bool IsReadOnly
        {
            get { return descriptor.IsReadOnly; }
        }

        public override Type PropertyType
        {
            get { return descriptor.PropertyType; }
        }

        // Prevent binding from attaching strong reference event handlers and suppress the ValueChangedEventManager
        // to prevent memory leaks. Classes that need value change notification should implement INotifyPropertyChanged,
        // so this method will not be called.
        public override void AddValueChanged(object component, EventHandler handler)
        {
            if (PreventValueChangedHandlers) RemoveSourceFromValueChangedEventManager(GetValueChangedEventManager(null), component);
            else base.AddValueChanged(component, handler);
        }

        private static readonly Type ValueChangedEventManagerType = typeof(FrameworkElement).Assembly.GetType("MS.Internal.Data.ValueChangedEventManager");

        private static readonly Invoker GetValueChangedEventManager =
            ValueChangedEventManagerType
                .GetProperty("CurrentManager", BindingFlags.NonPublic | BindingFlags.Static)
                .GetGetMethod(true).GetInvoker();

        private static readonly Invoker RemoveSourceFromValueChangedEventManager =
            ValueChangedEventManagerType.GetMethod("Remove", BindingFlags.NonPublic | BindingFlags.Instance)
                .GetInvoker();

        public override bool CanResetValue(object component)
        {
            return descriptor.CanResetValue(GetComponent(component));
        }

        public override object GetValue(object component)
        {
            return descriptor.GetValue(GetComponent(component));
        }

        public override void ResetValue(object component)
        {
            descriptor.ResetValue(GetComponent(component));
        }

        public override void SetValue(object component, object value)
        {
            descriptor.SetValue(GetComponent(component), value);
        }

        public override bool ShouldSerializeValue(object component)
        {
            return descriptor.ShouldSerializeValue(GetComponent(component));
        }

        private object GetComponent(object component)
        {
            return componentSelector == null ? component : componentSelector(component);
        }

    }
}