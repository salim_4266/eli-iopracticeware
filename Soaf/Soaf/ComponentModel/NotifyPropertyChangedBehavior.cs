using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;

[assembly: Component(typeof(NotifyPropertyChangedBehavior))]

namespace Soaf.ComponentModel
{
    /// <summary>
    ///   Denotes that a type should automatically implement behavior and support for INotifyPropertyChanged.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class SupportsNotifyPropertyChangedAttribute : ConcernAttribute
    {
        public bool NotifyAnyPropertyChanged { get; private set; }
        public bool NotifyChildPropertyChanged { get; private set; }

        public SupportsNotifyPropertyChangedAttribute(bool notifyAnyPropertyChanged = false, bool notifyChildPropertyChanged = false)
            : base(typeof(NotifyPropertyChangedBehavior))
        {
            NotifyAnyPropertyChanged = notifyAnyPropertyChanged;
            NotifyChildPropertyChanged = notifyChildPropertyChanged;
        }
    }

    /// <summary>
    ///   Used to denote that attribute usage on a property should enable property change events to be raised.
    ///   Also allows to retrieve dependencies for calculated properties
    /// </summary>
    public interface INotifyPropertyChangedAttribute
    {
        /// <summary>
        /// Returns dependencies of current property
        /// </summary>
        /// <returns></returns>
        string[] GetDependencies();
    }

    /// <summary>
    ///   Denotes that a member property should fire a PropertyChanged event on set invocation.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NotifyPropertyChangedAttribute : Attribute, INotifyPropertyChangedAttribute
    {
        private static readonly string[] NoDependencies = new string[0];

        public virtual string[] GetDependencies()
        {
            return NoDependencies;
        }

        public virtual bool AlwaysNotifyWhenDependenciesChange
        {
            get { return false; }
        }
    }

    /// <summary>
    /// Identifies that a property depends on another property or properties and should notify that it has change when those change.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DependsOnAttribute : NotifyPropertyChangedAttribute
    {
        /// <summary>
        /// Gets the properties.
        /// </summary>
        public string[] Properties { get; private set; }

        public DependsOnAttribute(params string[] properties)
        {
            Properties = properties;
        }

        public override string[] GetDependencies()
        {
            return Properties;
        }
    }

    /// <summary>
    /// Defines that a property should not fire property changes
    /// </summary>
    /// <remarks>
    /// This does not prevent re-calculating dependent properties within same class, but prevents validation of this property
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnorePropertyChangesAttribute : Attribute
    {}

    /// <summary>
    /// Indicates that within a defined scope, Change Tracking should not occur.
    /// </summary>
    public class SuppressChangeTrackingScope : IDisposable
    {
        [ThreadStatic]
        private static SuppressChangeTrackingScope current;

        private readonly SuppressChangeTrackingScope last;

        public SuppressChangeTrackingScope()
        {
            last = Current;
            current = this;
        }

        public void Dispose()
        {
            current = last;
        }

        public static SuppressChangeTrackingScope Current { get { return current; } }
    }

    /// <summary>
    /// Indicates that within a defined scope, PropertyChanged events should not fire.
    /// </summary>
    public class SuppressPropertyChangedScope : IDisposable
    {
        [ThreadStatic]
        private static SuppressPropertyChangedScope current;

        private readonly SuppressPropertyChangedScope last;

        public static SuppressPropertyChangedScope Current
        {
            get { return current; }
        }

        public SuppressPropertyChangedScope()
        {
            last = Current;
            current = this;
        }

        public void Dispose()
        {
            current = last;
        }
    }

    /// <summary>
    /// PropertyChangedEventArgs used to bubble up property changed notifications.
    /// </summary>
    public class CascadingPropertyChangedEventArgs : PropertyChangedEventArgs
    {
        private readonly bool useSenderPropertyName;

        public const string PropertyNamePrefix = "_CascadedPropertyChangeNotify_";

        public CascadingPropertyChangedEventArgs(string propertyName, bool useSenderPropertyName, object originalSender, PropertyChangedEventArgs originalPropertyChangedEventArgs)
            : base(propertyName)
        {
            this.useSenderPropertyName = useSenderPropertyName;

            SenderPropertyName = propertyName;
            OriginalSender = originalSender;
            OriginalPropertyChangedEventArgs = originalPropertyChangedEventArgs;

            FullChangedPropertyPath = originalPropertyChangedEventArgs != null
                ? string.Format("{0}.{1}", propertyName, originalPropertyChangedEventArgs.PropertyName)
                : propertyName;

            // If cascading -> recover and use original event args
            var cascadingPropertyChangedEventArgs = originalPropertyChangedEventArgs as CascadingPropertyChangedEventArgs;
            if (cascadingPropertyChangedEventArgs != null)
            {
                OriginalSender = cascadingPropertyChangedEventArgs.OriginalSender;
                OriginalPropertyChangedEventArgs = cascadingPropertyChangedEventArgs.OriginalPropertyChangedEventArgs;
            }
        }

        public override string PropertyName
        {
            get 
            { 
                return useSenderPropertyName
                    ? SenderPropertyName 
                    : FullChangedPropertyPath; 
            }
        }

        /// <summary>
        /// Property name on current sender
        /// </summary>
        public string SenderPropertyName { get; private set; }

        public object OriginalSender { get; private set; }

        public PropertyChangedEventArgs OriginalPropertyChangedEventArgs { get; private set; }

        public string FullChangedPropertyPath { get; private set; }

        public override string ToString()
        {
            return "Changed property: " + PropertyName;
        }
    }

    /// <summary>
    /// The implementation of automatic INotifyPropertyChanged behavior for a type annotated with SupportsNotifyPropertyChangedAttribute.
    /// </summary>
    internal class NotifyPropertyChangedBehavior : IInterceptor, ITypeInterfacesConcern, IInterceptedMethodsFilter
    {
        private static readonly InterceptSettersFilter SettersFilter = new InterceptSettersFilter();
        private static readonly IEnumerable<Type> InterfacesInternal = new[] { typeof(INotifyPropertyChanged), typeof(IRaisePropertyChanged), typeof(IDisposable) };
        private static readonly IDictionary<Type, PropertyChangedInfo> PropertyChangedInfoByType = new Dictionary<Type, PropertyChangedInfo>();

        private object instance;
        private Type instanceType;
        private PropertyChangedEventHandler handler;

        private PropertyChangedInfo propertyChangedInfo;

        /// <summary>
        ///  Information about child objects currently being tracked
        /// </summary>
        private readonly IDictionary<object, TrackedChildObject> trackedChildObjects = new Dictionary<object, TrackedChildObject>();
        
        private readonly IDictionary<string, object> knownDependentPropertyValues = new Dictionary<string, object>();

        #region IInterceptor Members

        public void Intercept(IInvocation invocation)
        {
            if (invocation.Method.Name == "Dispose" && invocation.Method.IsMethodImplementationForInterface(typeof(IDisposable)))
            {
                InterceptedDispose();
                invocation.TryProceed();
                return;
            }

            if (invocation.Method.Name == "OnPropertyChanged" && invocation.Method.IsMethodImplementationForInterface(typeof(IRaisePropertyChanged)))
            {
                OnPropertyChanged(invocation.Target, (PropertyChangedEventArgs)invocation.Arguments[0]);
                invocation.TryProceed();
                return;
            }

            if (PropertyChangedEventAddRemoveHandler(invocation))
            {
                invocation.TryProceed();
                return;
            }

            // don't need to ensure initialization until simple cases above have already been handled
            EnsureInitialized(invocation.Target);

            if (!HandlePropertyValueChange(invocation))
            {
                invocation.TryProceed();
            }
        }


        private void InterceptedDispose()
        {
            if (handler == null)
                return;

            // Unsubscribe all events when disposing
            handler.GetInvocationList().ToArray().ForEach(PropertyChangedEvent_Remove);
            handler = null;
        }

        private bool PropertyChangedEventAddRemoveHandler(IInvocation invocation)
        {
            string methodName = invocation.Method.Name;
            object[] arguments = invocation.Arguments;

            bool isAddMethod = methodName == "add_PropertyChanged";
            bool isRemoveMethod = methodName == "remove_PropertyChanged";

            // Is it calls to add or remove event handler methods on INotifyPropertyChanged interface?
            if ((!isAddMethod && !isRemoveMethod) || !invocation.Method.IsMethodImplementationForInterface(typeof(INotifyPropertyChanged)))
            {
                return false;
            }

            if (isAddMethod)
            {
                PropertyChangedEvent_Add((Delegate)arguments[0]);
            }
            else
            {
                PropertyChangedEvent_Remove((Delegate)arguments[0]);
            }
            return true;
        }

        // ReSharper disable InconsistentNaming
        protected void PropertyChangedEvent_Remove(Delegate @delegate)
            // ReSharper restore InconsistentNaming
        {
            handler = (PropertyChangedEventHandler)Delegate.Remove(handler, @delegate);
        }

        // ReSharper disable InconsistentNaming
        protected void PropertyChangedEvent_Add(Delegate @delegate)
            // ReSharper restore InconsistentNaming
        {
            handler = (PropertyChangedEventHandler)Delegate.Combine(handler, @delegate);
        }

        #endregion

        #region ITypeInterfacesConcern Members

        public IEnumerable<Type> Interfaces
        {
            get
            {
                return InterfacesInternal;
            }
        }

        #endregion

        /// <summary>
        /// Initializes class dependencies if they haven't been already.
        /// </summary>
        /// <param name="instance">The instance.</param>
        private void EnsureInitialized(object instance)
        {
            // we can't use an IInstanceConcern for this, since the constructor may call virtual members, thus firing interception (before ApplyConcern can get called).
            if (this.instance == null)
            {
                this.instance = instance;
                instanceType = instance.GetType();
                // cache info about this instance
                propertyChangedInfo = PropertyChangedInfoByType.GetValue(instanceType, () => new PropertyChangedInfo(instanceType));
            }
        }

        protected void OnPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (SuppressPropertyChangedScope.Current != null) return;

            // Store value of properties with DependsOn attribute if we got notified of it's change (via setter or manually fired)
            if (!(e is CascadingPropertyChangedEventArgs))
            {
                PropertyChangedPropertyInfo info;
                if (propertyChangedInfo.InfoByName.TryGetValue(e.PropertyName, out info)
                    && info.IsDependentProperty && !info.NotifyOfAllChangesAndAsSelf)
                {
                    var dependentPropertyValue = info.SafeGetValue(instance);
                    if (dependentPropertyValue != PropertyChangedPropertyInfo.ValueThrowingException)
                    {
                        lock (knownDependentPropertyValues)
                        {
                            knownDependentPropertyValues[e.PropertyName] = dependentPropertyValue;
                        }   
                    }
                }
            }

            // Notify subscribers
            PropertyChangedEventHandler eventHandler = handler;
            if (eventHandler != null) eventHandler(sender, e);

            // .. and dependents
            NotifyDependentPropertiesChanged(e);
        }

        /// <summary>
        /// Notifies any dependent properties that the value has changed.
        /// </summary>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void NotifyDependentPropertiesChanged(PropertyChangedEventArgs e)
        {
            // Get property name from changed args
            var cascadingArgs = e as CascadingPropertyChangedEventArgs;
            var propertyName = cascadingArgs != null
                ? cascadingArgs.SenderPropertyName
                : e.PropertyName;

            PropertyChangedPropertyInfo info;
            if (propertyChangedInfo.InfoByName.TryGetValue(propertyName, out info))
            {
                foreach (var dependency in info.Dependents)
                {
                    // Property path must match filter set via DependsOn attribute
                    if (!dependency.IsMatch(e.PropertyName))
                    {
                        continue;
                    }

                    var raisePropertyChanged = true;

                    // Evaluate whether dependency property actually changed
                    PropertyChangedPropertyInfo dependentInfo;
                    if (propertyChangedInfo.InfoByName.TryGetValue(dependency.DependentPropertyName, out dependentInfo)
                        && !dependentInfo.NotifyOfAllChangesAndAsSelf)
                    {
                        object dependentPropertyValue = dependentInfo.SafeGetValue(instance);
                        if (dependentPropertyValue != PropertyChangedPropertyInfo.ValueThrowingException)
                        {
                            lock (knownDependentPropertyValues)
                            {
                                object knownDependentPropertyValue;
                                if (knownDependentPropertyValues.TryGetValue(dependency.DependentPropertyName, out knownDependentPropertyValue)
                                    && Equals(dependentPropertyValue, knownDependentPropertyValue))
                                {
                                    raisePropertyChanged = false;
                                }
                                else
                                {
                                    // Cache newly evaluated value
                                    knownDependentPropertyValues[dependency.DependentPropertyName] = dependentPropertyValue;
                                }
                            }
                        }
                    }

                    // Dependent property actually changed
                    if (raisePropertyChanged)
                    {
                        var eventArgs = new CascadingPropertyChangedEventArgs(dependency.DependentPropertyName, true, instance, e);
                        OnPropertyChanged(instance, eventArgs);
                    }
                }
            }
        }

        private bool HandlePropertyValueChange(IInvocation invocation)
        {
            PropertyChangedPropertyInfo info;
            // Is it a change for a property?
            if (!propertyChangedInfo.InfoByName.TryGetValue(invocation.Method.Name.Substring(4), out info))
            {
                return false;
            }

            // Get previous value
            var oldValueKnown = info.GetValue != null; // Possible for setter-only properties
            var oldValue = oldValueKnown ? info.GetValue(instance) : null;

            // Ensure that the value is changed by comparing current one and the one about to be set
            if (oldValueKnown && Equals(invocation.Arguments[0], oldValue))
            {
                return false;
            }

            HandleChildTracking(info, invocation.Arguments[0], oldValue);

            // Actually allow setter to execute
            invocation.TryProceed();

            var changedEventArgs = new PropertyChangedEventArgs(info.Property.Name);
            if (!info.IgnoresPropertyChanges)
            {
                OnPropertyChanged(instance, changedEventArgs);
            }
            else if (info.Dependents.Any())
            {
                NotifyDependentPropertiesChanged(changedEventArgs);
            }

            return true;
        }

        /// <summary>
        /// Handles tracking of child objects introduced in a set operation.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="newPropertyValue"></param>
        /// <param name="oldPropertyValue"></param>
        private void HandleChildTracking(PropertyChangedPropertyInfo info, object newPropertyValue, object oldPropertyValue)
        {
            // No dependencies and we don't listen to children by default, so skip
            if (!propertyChangedInfo.NotifyChildPropertyChanged && !info.Dependents.Any())
            {
                return;
            }

            var oldChild = oldPropertyValue as INotifyPropertyChanged;
            var newChild = newPropertyValue as INotifyPropertyChanged;

            lock (trackedChildObjects)
            {
                TrackedChildObject trackingInfo;
                // Old instance was being tracked?
                if (oldChild != null && trackedChildObjects.TryGetValue(oldChild, out trackingInfo))
                {
                    // the property changing no longer refers to this child
                    trackingInfo.AssociatedProperties.Remove(info);

                    // no properties refer to this child anymore, dispose and remove from tracked children
                    if (!trackingInfo.AssociatedProperties.Any())
                    {
                        trackingInfo.Dispose();
                        trackedChildObjects.Remove(trackingInfo);
                    }
                }

                // Is new value trackable?
                if (newChild != null)
                {
                    trackingInfo = trackedChildObjects.GetValue(newChild, () => new TrackedChildObject(newChild, OnChildPropertyChanged));
                    trackingInfo.AssociatedProperties.Add(info);   
                }
            }
        }

        /// <summary>
        /// Called when a property on a tracked child property is changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnChildPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (SuppressPropertyChangedScope.Current != null || handler == null) return;

            lock (trackedChildObjects)
            {
                TrackedChildObject child;
                if (!trackedChildObjects.TryGetValue(sender, out child)) return;

                // go through all properties on our instance that currently refer to this child
                foreach (var property in child.AssociatedProperties)
                {
                    var changedEventArgs = new CascadingPropertyChangedEventArgs(property.Property.Name, property.NotifyOfAllChangesAndAsSelf, sender, e);

                    if (propertyChangedInfo.NotifyChildPropertyChanged && !property.IgnoresPropertyChanges)
                    {
                        // fire that the child itself has changed because one of its properties have changed
                        OnPropertyChanged(instance, changedEventArgs);
                    }
                    else if (property.Dependents.Any())
                    {
                        // Otherwise, process only dependents of properties
                        NotifyDependentPropertiesChanged(changedEventArgs);
                    }
                }
            }
        }

        public bool ShouldIntercept(Type type, MethodInfo method)
        {
            // Allow only setters and always intercept PropertyChanged event
            return SettersFilter.ShouldIntercept(type, method)
                   || ((method.Name == "add_PropertyChanged" || method.Name == "remove_PropertyChanged")
                       && method.IsMethodImplementationForInterface(typeof(INotifyPropertyChanged)));
        }


        /// <summary>
        /// Info about a property dependency indicated using a DependsOn attribute.
        /// </summary>
        private class PropertyDependency
        {
            private readonly string dependencyPathWithDot;

            public PropertyDependency(string dependencyPath, string dependentPropertyName)
            {
                DependencyPath = dependencyPath;
                DependentPropertyName = dependentPropertyName;

                dependencyPathWithDot = dependencyPath + ".";
            }

            /// <summary>
            /// Gets the dependency path for the dependency (will be empty for simple, non-complex properties).
            /// </summary>
            /// <value>
            /// The dependency path.
            /// </value>
            public string DependencyPath { get; private set; }

            /// <summary>
            /// Gets the name of the dependent property.
            /// </summary>
            /// <value>
            /// The name of the dependent property.
            /// </value>
            public string DependentPropertyName { get; private set; }

            public override int GetHashCode()
            {
                return new[] { DependencyPath, DependentPropertyName }.GetSequenceHashCode();
            }

            public override bool Equals(object obj)
            {
                return obj is PropertyDependency &&
                       new[] { DependencyPath, DependentPropertyName }.SequenceEqual(obj.CastTo<PropertyDependency>().IfNotNull(d => new[] { d.DependencyPath, DependentPropertyName }));
            }

            /// <summary>
            /// Determines whether the specified path is a match. The path must either be equal or start with the dependency path.
            /// </summary>
            /// <param name="path">The path.</param>
            /// <returns></returns>
            public bool IsMatch(string path)
            {
                return path == DependencyPath 
                    || path.StartsWith(dependencyPathWithDot)
                    || DependencyPath.StartsWith(path + ".");
            }
        }

        /// <summary>
        /// Information about a tracked child object. Listens for propertiy changes using a weak reference and cleans up on dispose. 
        /// </summary>
        private class TrackedChildObject : IDisposable
        {
            private readonly PropertyChangedEventHandler weakPropertyChangedHandler;
            private readonly HashSet<PropertyChangedPropertyInfo> associatedProperties = new HashSet<PropertyChangedPropertyInfo>();

            public TrackedChildObject(INotifyPropertyChanged instance, PropertyChangedEventHandler propertyChangedHandler)
            {
                Instance = instance;
                weakPropertyChangedHandler = propertyChangedHandler.MakeWeak();
                instance.PropertyChanged += weakPropertyChangedHandler;
            }

            /// <summary>
            /// Gets or sets the child instance being tracked.
            /// </summary>
            /// <value>
            /// The instance.
            /// </value>
            public INotifyPropertyChanged Instance { get; private set; }

            /// <summary>
            /// Gets the properties this tracked child is stored under.
            /// </summary>
            /// <value>
            /// The associated properties.
            /// </value>
            public HashSet<PropertyChangedPropertyInfo> AssociatedProperties { get { return associatedProperties; } }

            public void Dispose()
            {
                Instance.PropertyChanged -= weakPropertyChangedHandler;
            }
        }

        /// <summary>
        /// Calculated information to help make decisions when dealing with objects supported by NotifyPropertyChangedBehavior.
        /// </summary>
        private class PropertyChangedInfo
        {
            private readonly IDictionary<string, PropertyChangedPropertyInfo> infoByName;

            private readonly bool notifyChildPropertyChanged;

            /// <summary>
            /// Gets information about all properties for this type.
            /// </summary>
            /// <value>
            /// The name of the information by.
            /// </value>
            public IDictionary<string, PropertyChangedPropertyInfo> InfoByName
            {
                get { return infoByName; }
            }

            public bool NotifyChildPropertyChanged { get { return notifyChildPropertyChanged; } }

            public PropertyChangedInfo(Type type)
            {
                var supportsNotifyPropertyChangedAttribute = type.GetAttribute<SupportsNotifyPropertyChangedAttribute>();
                bool notifyAnyPropertyChanged = supportsNotifyPropertyChangedAttribute.NotifyAnyPropertyChanged;
                notifyChildPropertyChanged = supportsNotifyPropertyChangedAttribute.NotifyChildPropertyChanged;

                var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

                // collect property dependencies by the parent property name (eg. Child.Prop will be grouped under Child)
                var propertyDependencies =
                    (from property in properties
                        from dependsOn in property.GetAttributes().OfType<INotifyPropertyChangedAttribute>()
                        from dependsOnPath in dependsOn.GetDependencies()
                        select new
                               {
                                   DependentPropertyName = property.Name,
                                   DependsOnPropertyName = dependsOnPath.Split('.')[0],
                                   DependsOnPath = dependsOnPath
                               })
                        .GroupBy(i => i.DependsOnPropertyName)
                        .ToDictionary(
                            i => i.Key,
                            i => i.Select(j => new PropertyDependency(j.DependsOnPath, j.DependentPropertyName))
                                .Distinct()
                                .ToArray()
                        );

                // build info about all properties for this type and their dependents
                var infos =
                    (from property in properties
                        where (notifyAnyPropertyChanged || property.GetAttributes().OfType<INotifyPropertyChangedAttribute>().Any())
                        let setMethod = property.GetSetMethod(true)
                        let getMethod = property.GetGetMethod(true)
                        let getInvoker = getMethod != null ? getMethod.GetInvoker() : null
                        let getter = getInvoker != null ? new Func<object, object>(o => getInvoker(o)) : null
                        select new PropertyChangedPropertyInfo(property, getter, propertyDependencies.GetValue(property.Name) ?? new PropertyDependency[0]))
                        .ToArray();

                infoByName = infos.ToDictionary(i => i.Property.Name);
            }
        }

        /// <summary>
        /// Information about an individual property on an object supported by NotifyPropertyChangedBehavior.
        /// </summary>
        private class PropertyChangedPropertyInfo
        {
            public static readonly object ValueThrowingException = new object();

            public PropertyChangedPropertyInfo(PropertyInfo property, Func<object, object> getValue, PropertyDependency[] dependents)
            {
                Property = property;
                GetValue = getValue;
                Dependents = dependents;

                var attributes = property.GetAttributes()
                    .ToList();

                IgnoresPropertyChanges = attributes
                    .OfType<IgnorePropertyChangesAttribute>()
                    .Any();

                // Validation depends on both current property changes, child or dependencies
                NotifyOfAllChangesAndAsSelf = attributes
                    .OfType<ValidationAttribute>()
                    .Any();

                IsDependentProperty = attributes
                    .OfType<INotifyPropertyChangedAttribute>()
                    .Any(a => a.GetDependencies().Any());

                SafeGetValue = o =>
                {
                    try
                    {
                        return getValue(o);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Couldn't retrieve value of {0} property. Error: {1}", property.Name, ex);
                        return ValueThrowingException;
                    }
                };
            }

            public PropertyInfo Property { get; private set; }

            public bool IsDependentProperty { get; private set; }

            public bool IgnoresPropertyChanges { get; private set; }

            /// <summary>
            /// Gets a collection of all properties with a DependsOn path that refers to this property (either as the root of a complex path or just by itself). e.g. [DependsOn("Other.Property") and DependsOn("Other") will both be included.
            /// </summary>
            /// <value>
            /// The dependencies.
            /// </value>
            public PropertyDependency[] Dependents { get; private set; }

            /// <summary>
            /// Gets the get value of the property on the target specified.
            /// Please note that if the property has no getter, this may be null.
            /// </summary>
            /// <value>
            /// The get value.
            /// </value>
            public Func<object, object> GetValue { get; private set; }

            /// <summary>
            /// Gets value of property on the target specified. Will not throw exception (returns <c>null</c> instead)
            /// </summary>
            public Func<object, object> SafeGetValue { get; private set; }

            /// <summary>
            /// Denotes that a property must always notify of its change (even if it's dependency or child property change)
            /// </summary>
            public bool NotifyOfAllChangesAndAsSelf { get; private set; }
        }

    }
}