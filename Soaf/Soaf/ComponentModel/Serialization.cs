using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Soaf.Collections;
using Soaf.Reflection;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// StringWriter class that provides the ability to override the Encoding property.
    /// </summary>
    public class CustomEncodingStringWriter : StringWriter
    {
        private readonly Encoding encoding;

        public CustomEncodingStringWriter()
        {
            encoding = Encoding.Unicode;
        }

        public CustomEncodingStringWriter(Encoding encoding)
        {
            this.encoding = encoding;
        }

        public override Encoding Encoding
        {
            get { return encoding; }
        }
    }

    public class CustomDataContractSurrogate : IDataContractSurrogate
    {
        public Type GetDataContractType(Type type)
        {
            if (type.EqualsGenericTypeFor(typeof(ICollection<>)))
            {
                return typeof(Collection<>).MakeGenericType(type.GetGenericArguments()[0]);
            }

            return type;
        }

        public object GetObjectToSerialize(object obj, Type targetType)
        {
            return obj;
        }

        public object GetDeserializedObject(object obj, Type targetType)
        {
            return obj;
        }

        public object GetCustomDataToExport(MemberInfo memberInfo, Type dataContractType)
        {
            return null;
        }

        public object GetCustomDataToExport(Type clrType, Type dataContractType)
        {
            return null;
        }

        public void GetKnownCustomDataTypes(Collection<Type> customDataTypes)
        {
        }

        public Type GetReferencedTypeOnImport(string typeName, string typeNamespace, object customData)
        {
            return new[] { typeNamespace, typeName }.Join(".").ToType();
        }

        public CodeTypeDeclaration ProcessImportedType(CodeTypeDeclaration typeDeclaration, CodeCompileUnit compileUnit)
        {
            return typeDeclaration;
        }
    }

    /// <summary>
    /// Helper/utility methods for serialization.
    /// </summary>
    public static class Serialization
    {
        private static readonly Lazy<DataContractResolver> LazyDataContractResolver = Lazy.For<DataContractResolver>(() => new CustomDataContractResolver());
        private static readonly Dictionary<string, XmlSerializer> CachedXmlSerializers = new Dictionary<string, XmlSerializer>(); 

        internal static DataContractResolver DataContractResolver
        {
            get { return LazyDataContractResolver.Value; }
        }

        /// <summary>
        /// Returns instance of XmlSerializer which is cached according to input parameters
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static XmlSerializer CreateCachedXmlSerializer(Type type)
        {
            // Per .NET documentation this constructor is cached internally
            return new XmlSerializer(type);
        }

        /// <summary>
        /// Returns instance of XmlSerializer which is cached according to input parameters
        /// </summary>
        /// <param name="type"></param>
        /// <param name="defaultNamespace"></param>
        /// <returns></returns>
        public static XmlSerializer CreateCachedXmlSerializer(Type type, string defaultNamespace)
        {
            // Per .NET documentation this constructor is cached internally
            return new XmlSerializer(type, defaultNamespace);
        }

        /// <summary>
        /// Returns instance of XmlSerializer which is cached according to input parameters
        /// </summary>
        /// <remarks>
        /// There is a known issue with XmlSerializer introducing memory leak by generating dynamic assemblies which can't unload until AppDomain is shutdown
        /// </remarks>
        /// <param name="type"></param>
        /// <param name="rootElementName"></param>
        /// <param name="rootElementNamespace"></param>
        /// <returns></returns>
        public static XmlSerializer CreateCachedXmlSerializerWithRoot(Type type, string rootElementName, string rootElementNamespace = null)
        {
            var key = string.Format("{0}:{1}:{2}", type, rootElementName, rootElementNamespace).ToLowerInvariant();
            var result = CachedXmlSerializers.GetValue(key, () =>
            {
                var rootAttribute = new XmlRootAttribute(rootElementName);
                if (rootElementNamespace != null)
                {
                    rootAttribute.Namespace = rootElementNamespace;
                }
                return new XmlSerializer(type, rootAttribute);
            });
            return result;
        }

        /// <summary>
        /// Serializes the instance of T to xml.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="type">The type.</param>
        /// <param name="indent">if set to <c>true</c> indent output xml.</param>
        /// <param name="omitXmlDeclaration">if set to <c>true</c> omits XML declaration.</param>
        /// <returns></returns>
        public static string ToXml(this object value, Type type = null, bool indent = true, bool omitXmlDeclaration = true)
        {
            using (var sw = new StringWriter())
            {
                using (var writer = XmlWriter.Create(sw, new XmlWriterSettings
                {
                    CheckCharacters = false, 
                    Indent = indent, 
                    OmitXmlDeclaration = omitXmlDeclaration, 
                    Encoding = new UnicodeEncoding(false, false)
                }))
                {
                    var typeToCheckForDatacontract = type ?? value.GetType();

                    if (typeToCheckForDatacontract.HasDataContract() || (typeToCheckForDatacontract.IsGenericType && typeToCheckForDatacontract.GetGenericArguments().Any(t => Types.HasDataContract(t))))
                    {
                        var serializer = CreateDataContractSerializer(type ?? typeof(object));
                        serializer.WriteObject(writer, value);
                    }
                    else
                    {

                        var ns = new XmlSerializerNamespaces();
                        // prevent including xmlns
                        ns.Add("", "");

                        var serializer = CreateCachedXmlSerializer(type ?? value.GetType());
                        serializer.Serialize(writer, value, ns);
                    }
                }

                return sw.ToString();
            }
        }

        /// <summary>
        /// Creates a data contract serializer using default values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static DataContractSerializer CreateDataContractSerializer<T>()
        {
            return CreateDataContractSerializer(typeof(T));
        }

        /// <summary>
        /// Creates a data contract serializer using default values.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static DataContractSerializer CreateDataContractSerializer(Type type = null)
        {
            if (type == null) type = typeof(object);
            return new DataContractSerializer(type, new Type[0], int.MaxValue, false, true, new CustomDataContractSurrogate(), DataContractResolver);
        }

        /// <summary>
        /// Deserializes to T from xml.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml">The XML.</param>
        /// <param name="mapToComponent">if set to <c>true</c> [map to component].</param>
        /// <returns></returns>
        public static T FromXml<T>(this string xml, bool mapToComponent = false)
        {
            using (new SuppressPropertyChangedScope())
            using (new SuppressChangeTrackingScope())
            using (var sr = new StringReader(xml))
            using (var xmlReader = XmlReader.Create(sr, new XmlReaderSettings { CheckCharacters = false }))
            {
                var typeToCheckForDatacontract = typeof(T);

                T value;
                if (typeToCheckForDatacontract.HasDataContract() || (typeToCheckForDatacontract.IsGenericType && typeToCheckForDatacontract.GetGenericArguments().Any(t => t.HasDataContract())))
                {
                    var serializer = CreateDataContractSerializer(typeof(T));
                    value = (T)serializer.ReadObject(xmlReader);
                }
                else
                {
                    var serializer = CreateCachedXmlSerializer(typeof(T));
                    value = (T)serializer.Deserialize(xmlReader);
                }

                if (mapToComponent)
                {
                    var valueType = value.GetType();
                    T destination = (T)ServiceProvider.Current.GetService(valueType);
                    ServiceProvider.Current.GetService(typeof(IMapper<,>)
                        .MakeGenericType(valueType, valueType)).Invoke("Map",
                            value,
                            destination);

                    return destination;
                }

                return value;
            }
        }

        #region Nested type: CustomDataContractResolver

        internal class CustomDataContractResolver : DataContractResolver
        {
            private const string DefaultDataContractNamespace = "http://schemas.datacontract.org/2004/07/";

            public override bool TryResolveType(Type type, Type declaredType, DataContractResolver knownTypeResolver, out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace)
            {
                if (type.Assembly.IsDynamic && type.BaseType != null && type.BaseType != typeof(object))
                {
                    type = type.BaseType;
                }

                // ReSharper disable AssignNullToNotNullAttribute
                var result = knownTypeResolver.TryResolveType(type, declaredType, null, out typeName, out typeNamespace);
                // ReSharper restore AssignNullToNotNullAttribute
                if (!result)
                {
                    var dictionary = new XmlDictionary();

                    var name = XmlConvert.EncodeName(type.Namespace == null ? type.Name : type.FullName.Substring(type.Namespace.Length + 1));
                    typeName = dictionary.Add(name);
                    typeNamespace = dictionary.Add("{0}{1}".FormatWith(DefaultDataContractNamespace, type.Namespace));
                }

                return true;
            }

            public override Type ResolveName(string typeName, string typeNamespace, Type declaredType, DataContractResolver knownTypeResolver)
            {
                // ReSharper disable AssignNullToNotNullAttribute
                var type = knownTypeResolver.ResolveName(typeName, typeNamespace, declaredType, null);
                // ReSharper restore AssignNullToNotNullAttribute
                if (type == null && typeNamespace.StartsWith(DefaultDataContractNamespace))
                {
                    typeName = XmlConvert.DecodeName(typeName) ?? string.Empty;

                    type = "{0}.{1}".FormatWith(typeNamespace.Substring(DefaultDataContractNamespace.Length), typeName).ToType();
                }
                return declaredType.IsAssignableFrom(type) ? type : declaredType;
            }
        }

        #endregion
    }
}