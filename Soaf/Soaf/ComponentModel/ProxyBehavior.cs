using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Soaf.Collections;
using Soaf.Reflection;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// Defines that a type is a service contract proxy for another type.
    /// </summary>
    internal class ProxyAttribute : ConcernAttribute
    {
        public Type ProxiedType { get; set; }

        public ProxyAttribute(int priority = 0)
            : base(typeof(ProxyBehavior), priority)
        { }
    }

    /// <summary>
    /// Proxies methods from one type to a matching instance of an underlying type.
    /// </summary>
    internal class ProxyBehavior : IInterceptor
    {
        private volatile object proxiedInstance;
        private readonly object syncRoot = new object();

        private static readonly IDictionary<MethodInfo, Invoker> MethodInvokers = new Dictionary<MethodInfo, Invoker>().Synchronized();

        public void Intercept(IInvocation invocation)
        {
            if (proxiedInstance == null)
            {
                lock (syncRoot)
                {
                    if (proxiedInstance == null)
                    {
                        // Get the attribute from our concern descriptor
                        var attribute = invocation.ConcernDescriptors
                            .OfType<ProxyAttribute>()
                            .FirstOrDefault()
                            .EnsureNotDefault("ProxyAttribute expected.");

                        // Build a service using provided type
                        proxiedInstance = ServiceProvider.Current.GetService(attribute.ProxiedType);
                    }
                }
            }

            var invoker = MethodInvokers.GetValue(
                invocation.Method, () =>
                    proxiedInstance.GetType().GetMethod(invocation.Method.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, invocation.Method.GetParameterTypes().ToArray(), null).EnsureNotDefault("Could not find method on proxy.").GetInvoker()
                );
            invocation.ReturnValue = invoker(proxiedInstance, invocation.Arguments);
        }
    }
}