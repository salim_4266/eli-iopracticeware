namespace Soaf.ComponentModel
{
    public static class EditableObjectExtensions
    {
        /// <summary>
        /// Name of the custom "NamedEdit" session
        /// </summary>
        public const string CustomEditName = "Custom";

        /// <summary>
        /// Initiates custom named edit and accept changes unless specified otherwise 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="acceptChanges"></param>
        public static void InitiateCustomEdit(this IEditableObjectExtended target, bool acceptChanges = true)
        {
            if (acceptChanges) target.AcceptChanges();
            target.BeginNamedEdit(CustomEditName);
        }

        /// <summary>
        /// Accepts current edits for custom editing session and starts tracking again
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="acceptChanges">if set to <c>true</c> [accept changes].</param>
        public static void AcceptCustomEdits(this IEditableObjectExtended target, bool acceptChanges = true)
        {
            target.EndNamedEdit(CustomEditName);
            if (acceptChanges) target.AcceptChanges();
            target.BeginNamedEdit(CustomEditName);
        }

        /// <summary>
        /// Cancels current edits for custom editing session and starts tracking again
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="acceptChanges">if set to <c>true</c> [accept changes].</param>
        public static void CancelCustomEdits(this IEditableObjectExtended target, bool acceptChanges = false)
        {
            target.CancelNamedEdit(CustomEditName);
            if (acceptChanges) target.AcceptChanges();
            target.BeginNamedEdit(CustomEditName);
        }
    }
}