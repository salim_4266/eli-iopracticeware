using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Linq.Expressions;
using Soaf.Reflection;

[assembly: Component(typeof(AutoMap<,>), typeof(IMap<,>), Priority = -1)]
[assembly: Component(typeof(MapReturnValueBehavior))]
[assembly: Component(typeof(Mapper<,>), typeof(IMapper<,>))]
[assembly: Component(typeof(CustomMapperFactory), typeof(ICustomMapperFactory))]

namespace Soaf.ComponentModel
{
    /// <summary>
    ///   Denotes a type that uses a map to map a source instance onto a destination instance.
    /// </summary>
    /// <typeparam name = "TSource">The type of the 1.</typeparam>
    /// <typeparam name = "TDestination">The type of the 2.</typeparam>
    public interface IMapper<TSource, TDestination>
    {
        /// <summary>
        ///   Maps the specified source onto the destination.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <param name = "destination">The destination.</param>
        void Map(TSource source, TDestination destination);
    }

    /// <summary>
    ///   A default mapper that uses IMaps.
    /// </summary>
    /// <typeparam name = "TSource">The type of the 1.</typeparam>
    /// <typeparam name = "TDestination">The type of the 2.</typeparam>
    internal class Mapper<TSource, TDestination> : IMapper<TSource, TDestination>
    {
        private readonly IMap<TSource, TDestination> map;

        internal IMap<TSource, TDestination> InternalMap
        {
            get { return map; }
        }

        private static readonly object SyncRoot = new object();

        private static bool hasRegisteredProvider;

        public Mapper(IMap<TSource, TDestination> map)
        {
            this.map = map;

            lock (SyncRoot)
            {
                if (!hasRegisteredProvider)
                {
                    Dictionary<string, ICustomAttributeProvider> providers = map.Members.ToDictionary(m => m.Name, m => (ICustomAttributeProvider)m);
                    TypeDescriptor.AddProviderTransparent(
                        new CompositeCustomAttributeProviderDescriptionProvider(typeof(TDestination), typeof(TSource), providers),
                        typeof(TDestination));

                    hasRegisteredProvider = true;
                }
            }
        }

        #region IMapper<TSource,TDestination> Members

        public void Map(TSource source, TDestination destination)
        {
            // Map suppressing change tracking, since it's object initialization and we call AcceptChanges() afterwards
            using (new SuppressChangeTrackingScope())
            using (new SuppressPropertyChangedScope())
            {
                foreach (IMapMember<TSource, TDestination> member in map.Members)
                {
                    member.Map(source, destination);
                }
            }

            // Accept any changes on IChangeTracking implementations
            // We are doing it only on topmost mapper (in case we have another mapper used inside mapping expression)
            if (SuppressChangeTrackingScope.Current == null)
            {
                destination.As<IChangeTracking>().IfNotNull(o => o.AcceptChanges());
            }
        }

        #endregion
    }

    /// <summary>
    ///   Denotes a type that contains a collection of map elements, mapping instances of one type to instances of another type.
    /// </summary>
    /// <typeparam name = "TSource">The type of the 1.</typeparam>
    /// <typeparam name = "TDestination">The type of the 2.</typeparam>
    public interface IMap<TSource, TDestination>
    {
        /// <summary>
        ///   Gets the members of this map.
        /// </summary>
        /// <value>The members.</value>
        IEnumerable<IMapMember<TSource, TDestination>> Members { get; }
    }

    /// <summary>
    ///   An element within a map, mapping a single element of one type to a single element of another type.
    /// </summary>
    public interface IMapMember<TSource, TDestination> : ICustomAttributeProvider
    {
        /// <summary>
        ///   Gets or sets the name on the destination.
        /// </summary>
        /// <value>The name of the destination.</value>
        string Name { get; }

        /// <summary>
        ///   Maps the specified source onto the destination.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <param name = "destination">The destination.</param>
        void Map(TSource source, TDestination destination);
    }

    /// <summary>
    /// An object to object map which takes input expression as parameter
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TDestination"></typeparam>
    public class ExpressionSourceToDestinationMap<TSource, TDestination> : IMap<TSource, TDestination>
    {
        public IEnumerable<IMapMember<TSource, TDestination>> Members { get; private set; }

        public ExpressionSourceToDestinationMap(Expression<Func<TSource, TDestination>> selector, bool includeSourceAttributes = false, bool useServiceProvider = true)
        {
            Members = this.CreateMembers(selector, includeSourceAttributes, useServiceProvider);
        }
    }

    /// <summary>
    /// A factory which allows creating "one-off" mappers
    /// </summary>
    public interface ICustomMapperFactory
    {
        /// <summary>
        /// Create mapper based on mapping expression
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="selector">The selector.</param>
        /// <param name="includeSourceAttributes">if set to <c>true</c> [include source attributes].</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        IMapper<TSource, TDestination> CreateMapper<TSource, TDestination>(Expression<Func<TSource, TDestination>> selector, bool includeSourceAttributes, bool useServiceProvider = true);

        IMapper<TSource, TDestination> CreateMapper<TSource, TDestination>(Expression<Func<TSource, TDestination>> selector);
    }

    /// <summary>
    /// Implementation of custom mapper interface
    /// </summary>
    public class CustomMapperFactory : ICustomMapperFactory
    {
        /// <summary>
        /// Create mapper based on mapping expression
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="selector">The selector.</param>
        /// <param name="includeSourceAttributes">if set to <c>true</c> [include source attributes].</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        public IMapper<TSource, TDestination> CreateMapper<TSource, TDestination>(Expression<Func<TSource, TDestination>> selector, bool includeSourceAttributes, bool useServiceProvider = true)
        {
            return new Mapper<TSource, TDestination>(new ExpressionSourceToDestinationMap<TSource, TDestination>(selector, includeSourceAttributes, useServiceProvider));
        }

        /// <summary>
        /// Create mapper based on mapping expression
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="selector">The selector.</param>
        /// <returns></returns>
        public IMapper<TSource, TDestination> CreateMapper<TSource, TDestination>(Expression<Func<TSource, TDestination>> selector)
        {
            return CreateMapper(selector, false);
        }
    }

    /// <summary>
    ///   A map member that exposes an expression but doesn't actually map anything. Used purely for metadata purposes. The creator is responsible for using expressions.
    /// </summary>
    internal class ExpressionMapMember<TSource, TDestination> : IMapMember<TSource, TDestination>
    {
        private readonly Expression<Action<TSource, TDestination>> expression;
        private readonly string name;
        private readonly ICustomAttributeProvider sourceCustomAttributeProvider;

        public ExpressionMapMember(Expression<Action<TSource, TDestination>> expression, string name, ICustomAttributeProvider sourceCustomAttributeProvider)
        {
            this.expression = expression;
            this.name = name;
            this.sourceCustomAttributeProvider = sourceCustomAttributeProvider;
        }

        public Expression<Action<TSource, TDestination>> Expression
        {
            get { return expression; }
        }

        #region IMapMember<TSource,TDestination> Members

        public object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            var attributes = new List<object>();

            if (sourceCustomAttributeProvider != null)
            {
                attributes.AddRange(sourceCustomAttributeProvider.GetAttributes(attributeType, inherit));
            }

            return attributes.ToArray();
        }

        public object[] GetCustomAttributes(bool inherit)
        {
            var attributes = new List<object>();
            if (sourceCustomAttributeProvider != null)
            {
                attributes.AddRange(sourceCustomAttributeProvider.GetAttributes(inherit: inherit));
            }

            return attributes.ToArray();
        }

        public bool IsDefined(Type attributeType, bool inherit)
        {
            bool isDefined = false;
            if (sourceCustomAttributeProvider != null)
            {
                isDefined = sourceCustomAttributeProvider.GetAttributes(attributeType, inherit).Any(attributeType.IsInstanceOfType);
            }

            return isDefined;
        }

        public string Name
        {
            get { return name; }
        }

        public void Map(TSource source, TDestination destination)
        {
        }

        #endregion
    }

    /// <summary>
    ///   A map member that operates off of a delegate.
    /// </summary>
    internal class DelegateMapMember<TSource, TDestination> : IMapMember<TSource, TDestination>
    {
        private readonly Action<TSource, TDestination> action;
        private readonly string name;
        private readonly ICustomAttributeProvider sourceCustomAttributeProvider;
        private readonly bool suppressNullReferenceExceptions;

        public DelegateMapMember(Action<TSource, TDestination> action, string name, ICustomAttributeProvider sourceCustomAttributeProvider, bool suppressNullReferenceExceptions)
        {
            this.action = action;
            this.name = name;
            this.sourceCustomAttributeProvider = sourceCustomAttributeProvider;
            this.suppressNullReferenceExceptions = suppressNullReferenceExceptions;
        }

        #region IMapMember<TSource,TDestination> Members

        public object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            var attributes = new List<object>();

            if (sourceCustomAttributeProvider != null)
            {
                attributes.AddRange(sourceCustomAttributeProvider.GetAttributes(attributeType, inherit));
            }

            return attributes.ToArray();
        }

        public object[] GetCustomAttributes(bool inherit)
        {
            var attributes = new List<object>();
            if (sourceCustomAttributeProvider != null)
            {
                attributes.AddRange(sourceCustomAttributeProvider.GetAttributes(inherit: inherit));
            }

            return attributes.ToArray();
        }

        public bool IsDefined(Type attributeType, bool inherit)
        {
            bool isDefined = false;
            if (sourceCustomAttributeProvider != null)
            {
                isDefined = sourceCustomAttributeProvider.GetAttributes(attributeType, inherit).Any(attributeType.IsInstanceOfType);
            }

            return isDefined;
        }

        public string Name
        {
            get { return name; }
        }

        [DebuggerNonUserCode]
        public void Map(TSource source, TDestination destination)
        {
            try
            {
                action(source, destination);
            }
            catch (NullReferenceException)
            {
                if (suppressNullReferenceExceptions) Debug.WriteLine("Null reference mapping member: {0}.".FormatWith(Name));
                else if (Name != Mapper.MapExpressionsMemberName) throw new NullReferenceMemberMappingException("Null reference mapping member {0}.".FormatWith(Name));
                else throw;
            }
            catch (Exception ex)
            {
                if (Name != Mapper.MapExpressionsMemberName) throw new MemberMappingException("Error mapping member {0}.".FormatWith(Name), ex);
                throw;
            }
        }

        #endregion
    }

    /// <summary>
    /// An exception describing an error mapping a member in an IMap.
    /// </summary>
    public class MemberMappingException : Exception
    {
        internal static readonly ConstructorInfo ConstructorWithInnerException = typeof(MemberMappingException).GetConstructor(new[] { typeof(string), typeof(Exception) });

        public MemberMappingException(string message) : base(message) { }
        public MemberMappingException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// An exception describing an null error mapping a member in an IMap.
    /// </summary>
    public class NullReferenceMemberMappingException : MemberMappingException
    {
        public NullReferenceMemberMappingException(string message) : base(message) { }
        public NullReferenceMemberMappingException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Denotes that a type supports automatically mapping return values of methods and properties using a registered IMapper.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class SupportsMappingReturnValueAttribute : ConcernAttribute
    {
        public bool EnableForAll { get; private set; }

        public SupportsMappingReturnValueAttribute() : this(false) { }

        public SupportsMappingReturnValueAttribute(bool enableForAll)
            : base(typeof(MapReturnValueBehavior), 500)
        {
            EnableForAll = enableForAll;
        }
    }

    /// <summary>
    /// Performs the automatic mapping fo
    /// </summary>
    internal class MapReturnValueBehavior : IInterceptor, IInstanceConcern
    {
        private bool enableForAll;

        public void Intercept(IInvocation invocation)
        {
            invocation.TryProceed();
            if (invocation.ReturnValue != null
                && !IsDynamic(invocation.ReturnValue) && Mapper.IsComplexType(invocation.ReturnValue)
                && ServiceProvider.IsInitialized && (enableForAll || invocation.Method.HasAttribute<MapReturnValueAttribute>()))
            {
                using (new SuppressPropertyChangedScope())
                using (new SuppressChangeTrackingScope())
                {
                    var type = invocation.ReturnValue.GetType();
                    var destinationType = type;
                    if (destinationType.Is<IEnumerable>())
                    {
                        type = typeof(IEnumerable<>).MakeGenericType(type.FindElementType());
                        destinationType = typeof(List<object>);
                    }
                    object mapper = ServiceProvider.Current.GetService(typeof(IMapper<,>).MakeGenericType(type, destinationType));
                    object result = ServiceProvider.Current.GetService(destinationType);
                    mapper.Invoke("Map", invocation.ReturnValue, result);
                    if (result.TryConvertTo(invocation.Method.ReturnType, out result))
                    {
                        invocation.ReturnValue = result;
                    }
                }
            }
        }

        private bool IsDynamic(object value)
        {
            if (value.GetType().Assembly.IsDynamic) return true;
            if (value is IEnumerable)
            {
                return ((IEnumerable)value).OfType<object>().Any(i => i != null && i.GetType().Assembly.IsDynamic);
            }
            return false;
        }

        public void ApplyConcern(object value)
        {
            enableForAll = value.GetType().GetAttribute<SupportsMappingReturnValueAttribute>().IfNotNull(a => a.EnableForAll);
        }
    }

    /// <summary>
    /// Denotes that a method or property should have its return value automatically mapped using a registered IMapper.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class MapReturnValueAttribute : Attribute
    {
    }

    /// <summary>
    /// A map that's automatically generated based on matching members.
    /// </summary>
    /// <typeparam name="TSource">The type of the source.</typeparam>
    /// <typeparam name="TDestination">The type of the destination.</typeparam>
    [Singleton]
    public class AutoMap<TSource, TDestination> : IMap<TSource, TDestination>
    {
        private readonly IEnumerable<IMapMember<TSource, TDestination>> members;

        public AutoMap()
        {
            members = this.CreateAutoMapMembers().ToArray();
        }

        public IEnumerable<IMapMember<TSource, TDestination>> Members
        {
            get { return members; }
        }
    }

    /// <summary>
    /// Initialized to indicate that the mapper is running on a thread and keeps track of all mapped objects during a parent Map call.
    /// </summary>
    internal class MappingContextScope : IDisposable
    {
        public IDictionary<object, object> MappingContext { get; private set; }

        [ThreadStatic]
        private static MappingContextScope current;

        private MappingContextScope last;

        public static MappingContextScope Current
        {
            get { return current; }
        }

        public MappingContextScope()
        {
            last = current;
            current = this;
            MappingContext = last == null ? new Dictionary<object, object>() : last.MappingContext;
        }

        public void Dispose()
        {
            current = last;
        }
    }

    /// <summary>
    ///   Helper methods for working with Maps.
    /// </summary>
    public static class Mapper
    {
        internal const string MapExpressionsMemberName = "__MapExpressions__";

        private static readonly MethodInfo GetInlineMappingExpressionMethod = Reflector.GetMember(() => GetInlineMappingExpression<object, object>(null, true)).CastTo<MethodInfo>().GetGenericMethodDefinition();

        /// <summary>
        /// Returns an ICustomMapperFactory. This can be used to create IMappers from an inline lambda.
        /// </summary>
        /// <returns></returns>
        public static ICustomMapperFactory Factory
        {
            get { return new CustomMapperFactory(); }
        }

        /// <summary>
        /// Maps to TDestination using the specified mapper, resolving a new instance of TDestination using the current IServiceProvider.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="mapper">The mapper.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static TDestination Map<TSource, TDestination>(this IMapper<TSource, TDestination> mapper, TSource source)
        {
            return mapper.Map(source, true, true);
        }

        /// <summary>
        /// Maps to TDestination using the specified mapper, resolving a new instance of TDestination using the current IServiceProvider.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="mapper">The mapper.</param>
        /// <param name="source">The source.</param>
        /// <param name="suppressPropertyChanged">if set to <c>true</c> [suppress property changed].</param>
        /// <returns></returns>
        public static TDestination Map<TSource, TDestination>(this IMapper<TSource, TDestination> mapper, TSource source, bool suppressPropertyChanged)
        {
            return Map(mapper, source, suppressPropertyChanged, true);
        }

        /// <summary>
        /// Maps to TDestination using the specified mapper, resolving a new instance of TDestination using the current IServiceProvider.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="mapper">The mapper.</param>
        /// <param name="source">The source.</param>
        /// <param name="suppressPropertyChanged">if set to <c>true</c> [suppress property changed].</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        public static TDestination Map<TSource, TDestination>(this IMapper<TSource, TDestination> mapper, TSource source, bool suppressPropertyChanged, bool useServiceProvider)
        {
            TDestination destination;
            if (suppressPropertyChanged)
            {
                using (new SuppressPropertyChangedScope())
                using (new SuppressChangeTrackingScope())
                {
                    destination = useServiceProvider ? ServiceProvider.Current.GetService<TDestination>() : Activator.CreateInstance<TDestination>();
                    mapper.Map(source, destination);
                }
            }
            else
            {
                destination = ServiceProvider.Current.GetService<TDestination>();
                mapper.Map(source, destination);
            }
            return destination;
        }

        /// <summary>
        /// Maps from source to destination using specified mapper.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="mapper">The mapper.</param>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="suppressPropertyChanged">if set to <c>true</c> [suppress property changed].</param>
        public static void Map<TSource, TDestination>(this IMapper<TSource, TDestination> mapper, TSource source, TDestination destination, bool suppressPropertyChanged)
        {
            if (suppressPropertyChanged)
            {
                using (new SuppressPropertyChangedScope())
                using (new SuppressChangeTrackingScope())
                {
                    mapper.Map(source, destination);
                }
            }
            mapper.Map(source, destination);
        }

        /// <summary>
        /// Creates automatically mapped members.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <returns></returns>
        public static IEnumerable<IMapMember<TSource, TDestination>> CreateAutoMapMembers<TSource, TDestination>(this IMap<TSource, TDestination> map)
        {
            var sourceMembers = typeof(TSource).GetDataMembers();
            var destinationMembers = typeof(TDestination).GetDataMembers();

            var memberMap = sourceMembers.ToDictionary(i => i, i => destinationMembers.WithName(i.Name)).Where(i => i.Value != null);

            foreach (var member in memberMap)
            {
                var getSourceValue = member.Key.MemberInfo.As<PropertyInfo>().IfNotNull(p => p.GetGetMethod(true).IfNotNull(m => m.GetInvoker())) ??
                                     member.Key.MemberInfo.As<FieldInfo>().IfNotNull(f => new Invoker((o, p) => f.GetValue(o)));

                Invoker setDestinationValue = GetDestinationSetter(member.Value.MemberInfo);

                if (getSourceValue == null || setDestinationValue == null) continue;

                yield return new DelegateMapMember<TSource, TDestination>((s, d) => setDestinationValue(d, getSourceValue(s)), member.Key.Name, null, false);
            }

            if (typeof(IEnumerable).IsAssignableFrom(typeof(TSource)) && typeof(IEnumerable).IsAssignableFrom(typeof(TDestination)))
            {
                yield return new DelegateMapMember<TSource, TDestination>((s, d) => AutoMapListItems((IList)d, (IEnumerable)s), "Items", null, false);
            }
        }

        private static Invoker GetDestinationSetter(MemberInfo destinationMember)
        {
            var memberType = destinationMember.As<PropertyInfo>().IfNotNull(p => p.PropertyType) ?? destinationMember.As<FieldInfo>().IfNotNull(f => f.FieldType);

            if (memberType == null) return null;

            var getExistingValue = destinationMember.As<PropertyInfo>().IfNotNull(p => !p.CanRead ? null : p.GetGetMethod(true).IfNotNull(m => m.GetInvoker())) ??
                                   destinationMember.As<FieldInfo>().IfNotNull(f => new Invoker((o, p) => f.GetValue(o)));

            var setValue = destinationMember.As<PropertyInfo>().IfNotNull(p => !p.CanWrite ? null : p.GetSetMethod(true).IfNotNull(m => m.GetInvoker())) ??
                           destinationMember.As<FieldInfo>().IfNotNull(f => new Invoker((o, p) => { f.SetValue(o, p[0]); return null; }));

            if (getExistingValue == null) return null;

            var invoker = new Invoker((o, p) =>
                                      {
                                          AutoMap(getExistingValue(o), setValue, o, p[0], memberType);
                                          return null;
                                      });
            return invoker;
        }

        private static void AutoMap(object existingDestinationValue, Invoker setValue, object target, object source, Type memberType)
        {
            using (var scope = new MappingContextScope())
            {
                var mappingContext = scope.MappingContext;
                object value;
                if (source != null && mappingContext.TryGetValue(source, out value))
                {
                    setValue(target, value);
                }

                value = source;
                if (source != null && IsComplexType(source))
                {
                    var sourceType = source.GetType();

                    // Use source type as member type if supported
                    if (memberType.IsInstanceOfType(source)
                        && (existingDestinationValue == null
                            || sourceType.IsInstanceOfType(existingDestinationValue)))
                    {
                        memberType = sourceType;
                    }

                    value = existingDestinationValue ?? CreateValueForMember(memberType);

                    mappingContext[source] = value;

                    var relatedMapper = ServiceProvider.Current.GetService(typeof(IMapper<,>).MakeGenericType(sourceType, memberType));
                    if (value != null)
                    {
                        relatedMapper.Invoke("Map", source, value);
                    }
                }
                else if (source != null)
                {
                    mappingContext[source] = value;
                }

                if (setValue != null && value.TryConvertTo(memberType, out value))
                {
                    setValue(target, value);
                }
            }
        }

        private static object CreateValueForMember(Type memberType)
        {
            return ServiceProvider.Current.GetService(memberType);
        }

        private static void AutoMapListItems(IList destination, IEnumerable source)
        {
            using (var scope = new MappingContextScope())
            {
                var mappingContext = scope.MappingContext;
                Type destinationElementType = destination.FindElementType();

                if (source == null) return;

                foreach (var item in source)
                {
                    if (item == null)
                    {
                        // ReSharper disable AssignNullToNotNullAttribute
                        destination.Add(null);
                        // ReSharper restore AssignNullToNotNullAttribute
                        continue;
                    }

                    object value;
                    if (mappingContext.TryGetValue(item, out value))
                    {
                        destination.Add(value);
                    }

                    var sourceElementType = item.GetType();

                    if (IsComplexType(item))
                    {
                        var relatedMapper = ServiceProvider.Current.GetService(typeof(IMapper<,>).MakeGenericType(sourceElementType, sourceElementType.Is(destinationElementType) ? sourceElementType : destinationElementType));
                        value = CreateValueForMember(sourceElementType.Is(destinationElementType) ? sourceElementType : destinationElementType);

                        mappingContext[item] = value;

                        relatedMapper.Invoke("Map", item, value);

                        destination.Add(value);
                    }
                    else if (item.TryConvertTo(destinationElementType, out value))
                    {
                        mappingContext[item] = value;
                        destination.Add(value);
                    }
                }
            }
        }

        internal static bool IsComplexType(object item)
        {
            if (item == null) return false;

            return IsComplexType(item.GetType());
        }

        private static bool IsComplexType(Type type)
        {
            if (type.IsPrimitive || type.IsValueType || type == typeof(string)
                // non collection system types are not for mapping
                || (type.Assembly == typeof(Type).Assembly && !IsComplexCollectionType(type))) return false;

            return true;
        }

        private static bool IsComplexCollectionType(Type type)
        {
            return type.Is<IEnumerable>() && IsComplexType(type.FindElementType());
        }

        /// <summary>
        /// Creates a member from the specified delegate, using the specified map to infer argument types.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="map">The map.</param>
        /// <param name="action">The action.</param>
        /// <param name="name">The name.</param>
        /// <param name="sourceCustomAttributeProvider">The source custom attribute provider.</param>
        /// <returns></returns>
        public static IMapMember<TSource, TDestination> CreateMember<TSource, TDestination>(this IMap<TSource, TDestination> map, Action<TSource, TDestination> action, string name, ICustomAttributeProvider sourceCustomAttributeProvider = null)
        {
            return new DelegateMapMember<TSource, TDestination>(action, name, sourceCustomAttributeProvider, false);
        }

        /// <summary>
        /// Creates a member from the specified delegate, using the specified map to infer argument types.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TMember">The type of the member.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="map">The map.</param>
        /// <param name="sourceSelector">The source selector.</param>
        /// <param name="destinationSelector">The destination selector.</param>
        /// <param name="includeSourceAttributes">if set to <c>true</c> [include source attributes].</param>
        /// <returns></returns>
        public static IMapMember<TSource, TDestination> CreateMember<TSource, TMember, TDestination>(this IMap<TSource, TDestination> map, Expression<Func<TSource, TMember>> sourceSelector, Expression<Func<TDestination, TMember>> destinationSelector, bool includeSourceAttributes = false)
        {
            return CreateMember(sourceSelector, destinationSelector, includeSourceAttributes);
        }

        /// <summary>
        /// Creates a member from the specified delegate.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="action">The action.</param>
        /// <param name="name">The name.</param>
        /// <param name="sourceCustomAttributeProvider">The source custom attribute provider.</param>
        /// <returns></returns>
        public static IMapMember<TSource, TDestination> CreateMember<TSource, TDestination>(Action<TSource, TDestination> action, string name, ICustomAttributeProvider sourceCustomAttributeProvider = null)
        {
            return new DelegateMapMember<TSource, TDestination>(action, name, sourceCustomAttributeProvider, false);
        }


        /// <summary>
        /// Creates a member from the specified delegate.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TMember">The type of the member.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="sourceSelector">The source selector.</param>
        /// <param name="destinationSelector">The destination selector.</param>
        /// <param name="includeSourceAttributes">if set to <c>true</c> [include source attributes].</param>
        /// <param name="suppressNullReferenceExceptions">if set to <c>true</c> [suppress null reference exceptions].</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        public static IMapMember<TSource, TDestination> CreateMember<TSource, TMember, TDestination>(Expression<Func<TSource, TMember>> sourceSelector, Expression<Func<TDestination, TMember>> destinationSelector, bool includeSourceAttributes = false, bool suppressNullReferenceExceptions = false, bool useServiceProvider = true)
        {
            return CreateMemberInternal<TSource, TDestination>(sourceSelector.Body, destinationSelector.Body, includeSourceAttributes, true, suppressNullReferenceExceptions, useServiceProvider);
        }

        /// <summary>
        /// Creates a member from the specified expressions.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <param name="includeSourceAttributes">if set to <c>true</c> [include source attributes].</param>
        /// <param name="suppressNullReferenceExceptions">if set to <c>true</c> [suppress null reference exceptions].</param>
        /// <returns></returns>
        public static IMapMember<TSource, TDestination> CreateMember<TSource, TDestination>(Expression source, Expression destination, bool includeSourceAttributes = false, bool suppressNullReferenceExceptions = false, bool useServiceProvider = true)
        {
            return CreateMemberInternal<TSource, TDestination>(source, destination, includeSourceAttributes, true, suppressNullReferenceExceptions, useServiceProvider);
        }

        /// <summary>
        /// Creates the member internal.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="includeSourceAttributes">if set to <c>true</c> [include source attributes].</param>
        /// <param name="compile">if set to <c>true</c> [compile].</param>
        /// <param name="suppressNullReferenceExceptions">if set to <c>true</c> [suppress null reference exceptions].</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException">Could not find parameter in destination.</exception>
        private static IMapMember<TSource, TDestination> CreateMemberInternal<TSource, TDestination>(Expression source, Expression destination, bool includeSourceAttributes, bool compile, bool suppressNullReferenceExceptions, bool useServiceProvider)
        {
            var sourceExpressions = source.AsEnumerable().ToArray();
            var destinationExpressions = destination.AsEnumerable().ToArray();

            /* 
             * if source is foo.Bar.Bazz.Addresses.Select(a => a.City) we want to get the MemberInfo for City (for metadata). (This is debatable).
             * This will be the last item when calling AsEnumerable with the default visit order of 
             * ExpressionVisitMode.TopDown.
             */
            MemberExpression sourceMember = sourceExpressions.OfType<MemberExpression>().LastOrDefault();


            /* 
             * if source is foo.Bar.Bazz.Addresses.Select(a => a.City) we want to get the ParameterExpression for foo (not a) so that we can replace it with our new source parameter.
             * This will be the first item when calling AsEnumerable with the default visit order of 
             * ExpressionVisitMode.TopDown.
             */
            var originalSourceParameter = sourceExpressions.OfType<ParameterExpression>().FirstOrDefault();

            /*
             * if destination is x.City = we want to get the ParameterExpression for foo (not a) so that we can replace it with our new destination parameter.
             * This will be the first item when calling AsEnumerable with the default visit order of 
             * ExpressionVisitMode.TopDown.
             */
            var originalDestinationParameter = destinationExpressions.OfType<ParameterExpression>().FirstOrDefault();
            if (originalDestinationParameter == null) throw new InvalidOperationException("Could not find parameter in destination.");

            // new source and destination parameters for our lambda.
            ParameterExpression sourceParameter = Expression.Parameter(typeof(TSource), "source");
            ParameterExpression destinationParameter = Expression.Parameter(typeof(TDestination), "destination");

            // swap out original parameters with our new ones
            if (originalSourceParameter != null) source = new ParameterExpressionVisitor(originalSourceParameter, sourceParameter).Visit(source);
            destination = new ParameterExpressionVisitor(originalDestinationParameter, destinationParameter).Visit(destination);

            /*
             * if destination is x.City = we want to get the MemberInfo for City (for metadata and name) of this member.
             * This will be the first item when calling AsEnumerable with the default visit order of 
             * ExpressionVisitMode.TopDown.
             */
            MemberExpression destinationMember = destinationExpressions.OfType<MemberExpression>().FirstOrDefault();
            if (destinationMember == null) throw new InvalidOperationException(string.Format("Could not find a member in the destination expression {0}.", destination));

            // expand calls to Map as inline expressions
            Func<MethodInfo, bool> isMapMethod = m => m.DeclaringType == typeof(Mapper) && m.Name == "Map";

            Func<MethodInfo, Type[]> getMapGenericArguments = m => m.IsGenericMethod ? m.GetGenericArguments() : m.DeclaringType.IfNotNull(t => t.GetGenericArguments());

            source = source.Replace<MethodCallExpression>(e => isMapMethod(e.Method)
                ? (Expression)GetInlineMappingExpressionMethod.MakeGenericMethod(getMapGenericArguments(e.Method)).Invoke(null, new object[] { e, useServiceProvider })
                : e);

            // replace new and member init expressions in the mapping to use the ServiceProvider.Current (for DI and proxy support).
            source = source.Replace<MemberInitExpression>(x => ReplaceMemberInit(x, suppressNullReferenceExceptions, useServiceProvider)).Replace<NewExpression>(x => ReplaceNewExpression(x, useServiceProvider));

            BinaryExpression assignment = Expression.Assign(destination, source);

            var nullReferenceExceptionParameter = Expression.Parameter(typeof(NullReferenceException));
            Expression catchNullReferenceExceptionBody = Expression.Empty();
            if (!suppressNullReferenceExceptions)
            {
                catchNullReferenceExceptionBody = Expression.Throw(Expression.Constant(new NullReferenceMemberMappingException("Null reference mapping {0}.".FormatWith(assignment.ToString()))));
            }

            var memberMappingExceptionParameter = Expression.Parameter(typeof(MemberMappingException));
            Expression memberMappingExceptionBody = Expression.Throw(Expression.New(MemberMappingException.ConstructorWithInnerException, Expression.Constant("Error mapping {0}.".FormatWith(assignment.ToString())), memberMappingExceptionParameter));

            // suppress null reference exceptions mapping source to destination.
            var tryCatch = Expression.TryCatch(Expression.Block(typeof(void), new ParameterExpression[0], assignment),
                Expression.Catch(nullReferenceExceptionParameter, catchNullReferenceExceptionBody),
                Expression.Catch(memberMappingExceptionParameter, memberMappingExceptionBody)
                );

            Expression<Action<TSource, TDestination>> lambda = Expression.Lambda<Action<TSource, TDestination>>(tryCatch, sourceParameter, destinationParameter);

            if (compile)
            {
                // compile the lambda now.
                var mapMember = new DelegateMapMember<TSource, TDestination>(lambda.Compile(), destinationMember.Member.Name, includeSourceAttributes && sourceMember != null ? sourceMember.Member : null, suppressNullReferenceExceptions);
                return mapMember;
            }
            else
            {
                // defer compilation and create a no-op member.
                var mapMember = new ExpressionMapMember<TSource, TDestination>(lambda, destinationMember.Member.Name, includeSourceAttributes && sourceMember != null ? sourceMember.Member : null);
                return mapMember;
            }
        }

        /// <summary>
        /// Gets an inline expression for a mapper.Map call embedded inside another map member.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="e">The e.</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        private static Expression GetInlineMappingExpression<TSource, TDestination>(MethodCallExpression e, bool useServiceProvider)
        {
            var targetExpressions = e.Arguments[0].AsEnumerable().ToArray();
            var scopedLambdaParameters = targetExpressions.OfType<LambdaExpression>().SelectMany(l => l.Parameters).ToArray();
            var allParameterReferences = targetExpressions.OfType<ParameterExpression>().ToArray();

            /*
             * Make sure the target doesn't have any lambda parameters that are defined outside the scope of this methodcall itself. 
             * If it does, we can't inline it.
            */
            if (allParameterReferences.Any(p => !scopedLambdaParameters.Contains(p))) return e;

            var getMapperExpression = e.Arguments[0];

            var members = getMapperExpression.Compile<Func<IMapper<TSource, TDestination>>>()().As<Mapper<TSource, TDestination>>().IfNotNull(m => m.InternalMap.Members.ToArray());

            // we only can inline mappers that are totally defined of expressions
            if (members == null || !members.All(m => m is ExpressionMapMember<TSource, TDestination> || m.Name == MapExpressionsMemberName)) return e;

            var lambda = AsLambda(members.OfType<ExpressionMapMember<TSource, TDestination>>().ToArray());

            var variable = Expression.Variable(typeof(TDestination));

            var create = Expression.Assign(variable, CreateGetInstanceExpression(typeof(TDestination), useServiceProvider)) as Expression;

            var invocation = Expression.Invoke(lambda, e.Arguments[1], variable);

            /*
             * Originally getMapper(...).Map<TSource, TDestination>(x)
             * convert to 
             * 
             * var mapper = getMapper(...)
             * var expression = GetExpression(mapper)
             * 
             * then in-line block
             * var x = ServiceProvider.Current.Resolve<TDestination>();
             * Invoke(mapper, source, x);
             */

            var block = Expression.Block(new[] { variable }, new[] { create, invocation, variable });

            return block;
        }

        /// <summary>
        /// Replaces in-line member init expressions (new XYZ {...}) with calls to ServiceProvider.Resolve + member assignments (for proxying support).
        /// </summary>
        /// <param name="arg">The arg.</param>
        /// <param name="suppressNullReferenceExceptions">if set to <c>true</c> [suppress null reference exceptions].</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        private static Expression ReplaceMemberInit(MemberInitExpression arg, bool suppressNullReferenceExceptions, bool useServiceProvider)
        {
            if (arg.NewExpression.Arguments.Any() || !IsComplexType(arg.NewExpression.Type) || !arg.Bindings.All(b => b is MemberAssignment)) return arg;

            var variable = Expression.Variable(arg.NewExpression.Type);

            var create = Expression.Assign(variable, CreateGetInstanceExpression(arg.NewExpression.Type, useServiceProvider)) as Expression;

            var assignments = arg.Bindings.OfType<MemberAssignment>().Select(a => Expression.Assign(Expression.MakeMemberAccess(variable, a.Member), a.Expression)).OfType<Expression>().ToArray();

            var nullReferenceExceptionParameter = Expression.Parameter(typeof(NullReferenceException));
            Expression catchNullReferenceExceptionBody = Expression.Empty();
            if (!suppressNullReferenceExceptions)
            {
                catchNullReferenceExceptionBody = Expression.Throw(Expression.Constant(new MemberMappingException("Null reference mapping {0}.".FormatWith(arg.ToString()))));
            }

            var memberMappingExceptionParameter = Expression.Parameter(typeof(MemberMappingException));
            Expression memberMappingExceptionBody = Expression.Throw(Expression.New(MemberMappingException.ConstructorWithInnerException, Expression.Constant("Error mapping {0}.".FormatWith(arg.ToString())), memberMappingExceptionParameter));

            var tryCatch = Expression.TryCatch(Expression.Block(typeof(void), new ParameterExpression[0], assignments),
                Expression.Catch(nullReferenceExceptionParameter, catchNullReferenceExceptionBody),
                Expression.Catch(memberMappingExceptionParameter, memberMappingExceptionBody));
            /*
             * originally:
             * destination.X = new Foo 
             * {
             *   Y = source.Y,
             *   Z = source.Z
             * }
             * 
             * var x = ServiceProvider.Resolve(typeof(Foo))
             * try 
             * {
             *   x.Y = source.Y;
             *   x.Z = source.Z;
             * }
             * catch(NullReferenceException) { }
             * 
             * destination.X = x;
             * 
             */

            var block = Expression.Block(new[] { variable }, new[] { create, tryCatch, variable });

            return block;
        }

        /// <summary>
        /// Calls the ServiceProvider.Current to create an instance of the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        private static Expression CreateGetInstanceExpression(Type type, bool useServiceProvider)
        {
            if (useServiceProvider)
            {
                return Expression.Call(typeof(ServiceProvider), "GetService", new[] { type }, Expression.Property(null, typeof(ServiceProvider), "Current"));
            }
            return Expression.New(type);
        }

        /// <summary>
        /// Replaces a new expression with a call to ServiceProvider.Current.GetService.
        /// </summary>
        /// <param name="arg">The arg.</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        private static Expression ReplaceNewExpression(NewExpression arg, bool useServiceProvider)
        {
            if (!useServiceProvider || arg.Arguments.Any() || !IsComplexType(arg.Type)) return arg;

            return CreateGetInstanceExpression(arg.Type, true);
        }

        /// <summary>
        /// Creates map members from a select new { X = i.y } expression.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="map">The map.</param>
        /// <param name="selector">The selector.</param>
        /// <param name="includeSourceAttributes">if set to <c>true</c> [include source attributes].</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        public static IEnumerable<IMapMember<TSource, TDestination>> CreateMembers<TSource, TDestination>(this IMap<TSource, TDestination> map, Expression<Func<TSource, TDestination>> selector, bool includeSourceAttributes = false, bool useServiceProvider = true)
        {
            return CreateMembers(selector, includeSourceAttributes, false, useServiceProvider);
        }

        /// <summary>
        /// Creates map members from a select new { X = i.y } expression.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="selector">The selector.</param>
        /// <param name="includeSourceAttributes">if set to <c>true</c> [include source attributes].</param>
        /// <param name="suppressNullReferenceExceptions">if set to <c>true</c> [suppress null reference exceptions].</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException">Selector must be a new { ... } expression.</exception>
        public static IEnumerable<IMapMember<TSource, TDestination>> CreateMembers<TSource, TDestination>(Expression<Func<TSource, TDestination>> selector, bool includeSourceAttributes = false, bool suppressNullReferenceExceptions = false, bool useServiceProvider = true)
        {
            Expression body = selector.Body;

            if (body is UnaryExpression)
            {
                body = ((UnaryExpression)body).Operand;
            }

            var memberInitExpression = body as MemberInitExpression;

            if (memberInitExpression == null) throw new InvalidOperationException("Selector must be a new { ... } expression.");

            var members = new List<IMapMember<TSource, TDestination>>();

            foreach (var binding in memberInitExpression.Bindings.OfType<MemberAssignment>())
            {
                IMapMember<TSource, TDestination> member = CreateMemberInternal<TSource, TDestination>(binding.Expression, Expression.MakeMemberAccess(Expression.Parameter(typeof(TDestination)), binding.Member), includeSourceAttributes, false, suppressNullReferenceExceptions, useServiceProvider);
                members.Add(member);
            }

            var lambda = AsLambda(members.OfType<ExpressionMapMember<TSource, TDestination>>().ToArray());

            var compiledExpressionsMapper = lambda.Compile();

            members.Add(new DelegateMapMember<TSource, TDestination>(compiledExpressionsMapper, MapExpressionsMemberName, null, suppressNullReferenceExceptions));

            return members;
        }

        private static Expression<Action<TSource, TDestination>> AsLambda<TSource, TDestination>(IEnumerable<ExpressionMapMember<TSource, TDestination>> members)
        {
            // create an extra member using the expressions from all the expression map members (since they are no-ops) (this is for performance purposes).
            var sourceParameter = Expression.Parameter(typeof(TSource), "source");
            var destinationParameter = Expression.Parameter(typeof(TDestination), "destination");

            // create a since lambda with all mapping expressions in it (so we don't need to call as many compiled lambdas.
            var lambda = Expression.Lambda<Action<TSource, TDestination>>(
                Expression.Block(new ParameterExpression[0],
                    members.Select(m =>
                        Expression.Invoke(m.Expression, sourceParameter, destinationParameter)).ToArray()), sourceParameter, destinationParameter);

            lambda = (Expression<Action<TSource, TDestination>>)lambda.ExpandInvocations();
            return lambda;
        }

        public static IEnumerable<IMapMember<TSource, TDestination>> CreateMembers<TSource, TDestination>(this IMap<TSource, TDestination> map, Action<TSource, TDestination> selector, string name)
        {
            return new List<IMapMember<TSource, TDestination>>
                   {
                       map.CreateMember(selector, name)
                   };

        }

        #region Nested type: ParameterExpressionVisitor

        private class ParameterExpressionVisitor : ExpressionVisitor
        {
            private readonly ParameterExpression toReplace;
            private readonly ParameterExpression replacement;

            public ParameterExpressionVisitor(ParameterExpression toReplace, ParameterExpression replacement)
            {
                this.toReplace = toReplace;
                this.replacement = replacement;
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                if (node == toReplace) return replacement;
                return node;
            }
        }

        #endregion

        /// <summary>
        /// Takes an Enumerable of source items
        /// and wraps each map call within a try catch.  Gives the option to suppress
        /// or throw exceptions caught.  If we supress, we get back a partial list of
        /// destination items (assuming an exception was thrown at some point). If we do not suppress
        /// then an AggregateException will be thrown.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="mapper">The mapper.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        /// <exception cref="System.AggregateException"></exception>
        public static IEnumerable<TDestination> MapAll<TSource, TDestination>(this IMapper<TSource, TDestination> mapper, IEnumerable<TSource> source)
        {
            return MapAll(mapper, source, false, true, true);
        }

        /// <summary>
        /// Takes an Enumerable of source items
        /// and wraps each map call within a try catch.  Gives the option to suppress
        /// or throw exceptions caught.  If we supress, we get back a partial list of
        /// destination items (assuming an exception was thrown at some point). If we do not suppress
        /// then an AggregateException will be thrown.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="mapper">The mapper.</param>
        /// <param name="source">The source.</param>
        /// <param name="suppressNullReferenceExceptions">if set to <c>true</c> [suppress null reference exceptions].</param>
        /// <param name="suppressPropertyChanged">if set to <c>true</c> [suppress property changed].</param>
        /// <param name="useServiceProvider">if set to <c>true</c> [use service provider].</param>
        /// <returns></returns>
        /// <exception cref="System.AggregateException">
        /// </exception>
        public static IEnumerable<TDestination> MapAll<TSource, TDestination>(this IMapper<TSource, TDestination> mapper, IEnumerable<TSource> source, bool suppressNullReferenceExceptions, bool suppressPropertyChanged, bool useServiceProvider)
        {
            using (suppressPropertyChanged ? new SuppressPropertyChangedScope() : null)
            using (suppressPropertyChanged ? new SuppressChangeTrackingScope() : null)
            {

                var results = new List<TDestination>();
                var exceptions = new List<Exception>();

                var idProperty = typeof(TSource).GetProperty("Id");

                var idGetter = idProperty == null || !idProperty.CanRead || idProperty.GetIndexParameters().Count() != 0 ? null : idProperty.GetGetMethod().GetInvoker();

                foreach (var item in source)
                {
                    try
                    {
                        results.Add(mapper.Map(item, suppressPropertyChanged, useServiceProvider));
                    }
                    catch (Exception ex)
                    {
                        var message = "Error mapping {0} with Id: {1}".FormatWith(typeof(TSource).Name, idGetter == null ? "" : idGetter(item));
                        exceptions.Add(new Exception(message, ex));
                    }
                }

                if (exceptions.IsNotNullOrEmpty())
                {
                    if (!suppressNullReferenceExceptions)
                    {
                        throw new AggregateException(exceptions);
                    }

                    if (!exceptions.Recurse(x => x.InnerException == null ? new Exception[] { } : new[] { x.InnerException }).All(x => x.InnerException != null || (x is NullReferenceException || x is NullReferenceMemberMappingException)))
                    {
                        throw new AggregateException(exceptions);
                    }


                    Trace.TraceError(new AggregateException(exceptions).ToString());
                }

                foreach (var i in results.OfType<IChangeTracking>()) i.AcceptChanges();

                return results;
            }
        }
    }

    /// <summary>
    ///   A TypeDescriptionProvider that utilizes a map of ICustomAttributeProviders for providing additional type and property attributes.
    /// </summary>
    public class CompositeCustomAttributeProviderDescriptionProvider : TypeDescriptionProvider
    {
        private readonly ICustomAttributeProvider additionalTypeCustomAttributeProvider;
        private readonly Dictionary<string, ICustomAttributeProvider> additionalPropertyCustomAttributeProviders;
        private readonly IDictionary<Type, TypeDescriptor> typeDescriptors;

        public CompositeCustomAttributeProviderDescriptionProvider(Type type, ICustomAttributeProvider additionalTypeCustomAttributeProvider, Dictionary<string, ICustomAttributeProvider> additionalPropertyCustomAttributeProviders)
            : base(System.ComponentModel.TypeDescriptor.GetProvider(type))
        {
            this.additionalTypeCustomAttributeProvider = additionalTypeCustomAttributeProvider;
            this.additionalPropertyCustomAttributeProviders = additionalPropertyCustomAttributeProviders;
            typeDescriptors = new Dictionary<Type, TypeDescriptor>();
        }

        public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
        {
            return typeDescriptors.GetValue(
                objectType, () =>
                    new TypeDescriptor(base.GetTypeDescriptor(objectType, null), objectType, additionalTypeCustomAttributeProvider, additionalPropertyCustomAttributeProviders));
        }

        #region Nested type: TypeDescriptor

        /// <summary>
        ///   A CustomTypeDescriptor that appends attributes from the customAttributeProviders map.
        /// </summary>
        private class TypeDescriptor : CustomTypeDescriptor
        {
            private readonly AttributeCollection attributes;
            private readonly PropertyDescriptorCollection properties;

            public TypeDescriptor(ICustomTypeDescriptor parent, Type objectType, ICustomAttributeProvider typeCustomAttributeProvider, Dictionary<string, ICustomAttributeProvider> propertyCustomAttributeProviders)
                : base(parent)
            {
                attributes = new AttributeCollection(objectType.GetAttributes().ToArray());
                if (typeCustomAttributeProvider != null)
                {
                    Attribute[] additionalAttributes = typeCustomAttributeProvider.GetAttributes<Attribute>().Except(attributes.OfType<Attribute>()).ToArray();
                    attributes = AttributeCollection.FromExisting(
                        new AttributeCollection(additionalAttributes),
                        attributes.OfType<Attribute>().ToArray());
                }

                properties = new PropertyDescriptorCollection(base.GetProperties()
                    .OfType<PropertyDescriptor>()
                    .Select(pd =>
                    {
                        ICustomAttributeProvider customAttributeProvider;
                        if (propertyCustomAttributeProviders.TryGetValue(pd.Name, out customAttributeProvider))
                        {
                            pd = new BasicPropertyDescriptor(pd, customAttributeProvider.GetAttributes<Attribute>().Concat(pd.Attributes.OfType<Attribute>()).Distinct().ToArray());
                        }

                        return pd;
                    }).ToArray());
            }

            public override AttributeCollection GetAttributes()
            {
                return attributes;
            }

            public override PropertyDescriptorCollection GetProperties()
            {
                return properties;
            }

            public override PropertyDescriptorCollection GetProperties(Attribute[] attributes)
            {
                return new PropertyDescriptorCollection(GetProperties().OfType<PropertyDescriptor>().Where(pd => pd.Attributes.OfType<Attribute>().Any(attributes.Contains)).ToArray());
            }
        }

        #endregion
    }
}