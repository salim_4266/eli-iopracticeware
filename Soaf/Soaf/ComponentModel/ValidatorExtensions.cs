using System.ComponentModel.DataAnnotations;
using Soaf.Validation;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// More to object extensions
    /// </summary>
    public static class ValidatorExtensions
    {
        /// <summary>
        /// Validates that the given value is not a default value for this type (such as <c>null</c> for reference types)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [AssertionMethod]
        public static T ValidateNotDefault<T>([AssertionCondition(AssertionConditionType.IsNotNull)] this T value, string message = null)
        {
            if (Equals(value, default(T)))
                throw message == null ? new ValidationException("Value cannot be null") : new ValidationException(message);

            return value;
        }

        /// <summary>
        /// Gets the ErrorMessage property if it is not null or empty.  Otherwise gets the default error message parameter.
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="defaultErrorMessage"></param>
        /// <returns></returns>
        public static string GetErrorMessageOrDefault(this ValidationAttribute attribute, string defaultErrorMessage)
        {
            return string.IsNullOrEmpty(attribute.ErrorMessage)
                ? defaultErrorMessage
                : attribute.ErrorMessage;
        }
    }
}