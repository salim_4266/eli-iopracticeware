using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Soaf.Collections;
using System.Diagnostics;
using Soaf.ComponentModel;
using Soaf.Logging;
using Soaf.Reflection;

[assembly: Component(typeof(List<>), typeof(IEnumerable<>))]

namespace Soaf.ComponentModel
{
    /// <summary>
    /// Denotes that an interface is a factory for producing instances.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false)]
    public class FactoryAttribute : Attribute
    {

    }

    /// <summary>
    ///   Wraps an exception resolving a component. Does not preserve the underlying inner exception, only the message.
    /// </summary>
    internal class ResolutionException : Exception
    {
        public ResolutionException(Exception innerException)
            : base("Component could not be resolved.", innerException)
        {
        }
    }

    /// <summary>
    ///   Denotes that a property is a required depdency and must be injected on instance construction.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DependencyAttribute : Attribute
    {
        public bool IsOptional { get; set; }
    }

    /// <summary>
    ///   Denotes a type that should be registered for automatic resolution and injection.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public class ComponentAttribute : Attribute
    {
        public ComponentAttribute(Type type, params Type[] @for)
        {
            Type = type;
            For = @for;
            AddDefaultServices = true;
        }

        public Type Type { get; private set; }
        public IEnumerable<Type> For { get; private set; }
        public bool Initialize { get; set; }
        public int Priority { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to add default services. Checked for any interfaces named I + Type.Name. Default is true.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [add default services]; otherwise, <c>false</c>.
        /// </value>
        public bool AddDefaultServices { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether add all services (all interfaces implemented).
        /// </summary>
        /// <value>
        ///   <c>true</c> if [add all services]; otherwise, <c>false</c>.
        /// </value>
        public bool AddAllServices { get; set; }
    }

    /// <summary>
    ///   A collection of components for registration.
    /// </summary>
    public interface IComponentRegistry
    {
        IEnumerable<ComponentRegistration> ComponentRegistrations { get; }

        ComponentRegistration GetComponentRegistration(Type type, IEnumerable<Type> @for, object instance = null);
    }

    /// <summary>
    ///   Information about a type to register as a component for resolution and injection.
    /// </summary>
    public class ComponentRegistration
    {
        public ComponentRegistration(Type type, IEnumerable<Type> @for = null, ILifestyleDescriptor lifestyleDescriptor = null, IEnumerable<IConcernDescriptor> concerns = null, bool initialize = false, bool isFactory = false)
        {
            For = @for.IsNotNullOrEmpty() ? @for : new[] { type };
            Type = type ?? For.FirstOrDefault();
            LifestyleDescriptor = lifestyleDescriptor ?? new TransientAttribute();
            Concerns = concerns ?? new IConcernDescriptor[0];
            Initialize = initialize;
            IsFactory = isFactory;
        }

        public ComponentRegistration(object instance, IEnumerable<Type> @for = null, ILifestyleDescriptor lifestyleDescriptor = null, IEnumerable<IConcernDescriptor> concerns = null, bool initialize = false, bool isFactory = false)
        {
            Instance = instance;
            Type = instance.GetType();
            For = @for.IsNotNullOrEmpty() ? @for : new[] { Type };
            LifestyleDescriptor = lifestyleDescriptor ?? new TransientAttribute();
            Concerns = concerns ?? new IConcernDescriptor[0];
            Initialize = initialize;
            IsFactory = isFactory;
        }

        public IEnumerable<Type> For { get; internal set; }
        public Type Type { get; internal set; }
        public object Instance { get; internal set; }
        public ILifestyleDescriptor LifestyleDescriptor { get; internal set; }
        public IEnumerable<IConcernDescriptor> Concerns { get; internal set; }
        public bool Initialize { get; internal set; }
        public bool IsFactory { get; internal set; }

        public string Name
        {
            get { return (Type ?? For.First()).FullName; }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (Type != null)
            {
                sb.AppendFormat("{0}: ", Type.Name);
            }
            if (For != null)
            {
                sb.Append(For.Select(i => i.Name).Join());
            }

            if (LifestyleDescriptor != null)
            {
                sb.AppendFormat(" ({0})", LifestyleDescriptor.LifestyleType);
            }

            return sb.ToString();
        }
    }

    /// <summary>
    ///   A registry of components for types attributed with ComponentAttribute.
    /// </summary>
    internal class AttributedTypeComponentRegistry : IComponentRegistry
    {
        private readonly Func<ComponentRegistration, bool> componentFilter;
        private readonly List<ComponentRegistration> componentRegistrations = new List<ComponentRegistration>();
        private readonly List<IGlobalConcernDescriptor> globalConcerns = new List<IGlobalConcernDescriptor>();

        public AttributedTypeComponentRegistry(IAssemblyRepository assemblyRepository, Func<ComponentRegistration, bool> componentFilter = null, params object[] singletons)
        {
            this.componentFilter = componentFilter;
            using (new TimedScope(s => Debug.WriteLine("Created components for {0} types in {1} assemblies in {2}.".FormatWith(componentRegistrations.Count, assemblyRepository.Assemblies.Count(), s))))
            {
                // singleton assembly repository
                componentRegistrations.Add(new ComponentRegistration(assemblyRepository, new[] { typeof(IAssemblyRepository) }));
                componentRegistrations.Add(new ComponentRegistration(this, new[] { typeof(IComponentRegistry) }));

                var orderedAssemblies = assemblyRepository.Assemblies
                    .OrderBy(a => a.FullName.StartsWith("Soaf") ? 0 : 1);

                globalConcerns.AddRange(orderedAssemblies.SelectMany(t => t.GetAttributes<GlobalConcernAttribute>()));

                // finds Components attributed
                orderedAssemblies
                    .SelectMany(t => t.GetAttributes<ComponentAttribute>())
                    .GroupBy(i => i.For.FirstOrDefault() ?? i.Type)
                    .Select(grouping =>
                    {
                        // only include highest priority or last componenent type for a service, so one can override another
                        var information = grouping.Select((item, index) => new { item, index }).OrderBy(i => i.item.Priority).ThenBy(i => i.index).Last().item;
                        object singleton = singletons.FirstOrDefault(s => s.GetType() == information.Type);
                        return CreateComponentRegistration(information.Type, information.For, singleton, information.Initialize, information.AddDefaultServices, information.AddAllServices);
                    }).ForEach(component => componentRegistrations.Add(component));
            }
        }

        #region IComponentRegistry Members

        public IEnumerable<ComponentRegistration> ComponentRegistrations
        {
            get
            {
                lock (componentRegistrations)
                {
                    // We need to copy current registrations, since new ones can be added while consumer iterates over the items
                    return componentRegistrations.ToArray();
                }
            }
        }

        public ComponentRegistration GetComponentRegistration(Type type, IEnumerable<Type> @for, object instance = null)
        {
            lock (componentRegistrations)
            {
                if (instance == null)
                {
                    return componentRegistrations
                        .FirstOrDefault(c => c.For.Any(t => t == type || (@for != null && @for.Contains(t))))
                           ?? AddComponentRegistration(type, @for, null, false, true, false);
                }
                if (type == null) type = instance.GetType();
                return componentRegistrations
                    .FirstOrDefault(c => c.For.Any(t => t == type || (@for != null && @for.Contains(t))))
                       ?? AddComponentRegistration(type, @for, instance, false, true, false);
            }
        }

        #endregion

        private ComponentRegistration AddComponentRegistration(Type type, IEnumerable<Type> @for, object instance, bool initialize, bool addDefaultServices, bool addAllServices)
        {
            var componentRegistration = CreateComponentRegistration(type, @for, instance, initialize, addDefaultServices, addAllServices);
            lock (componentRegistrations)
            {
                componentRegistrations.Add(componentRegistration);
            }
            return componentRegistration;
        }

        private ComponentRegistration CreateComponentRegistration(Type type, IEnumerable<Type> @for, object instance, bool initialize, bool addDefaultServices, bool addAllServices)
        {
            @for = @for ?? new Type[0];
            // Ensure type is always registered at least for itself
            // Case 1: Lazy loaded type which needs to be registered for itself
            // Case 2: Type is registered for default interface and then we try resolving it for  
            //            itself -> this causes exception due to duplicate registration names
            if (type != null && !@for.Contains(type))
            {
                // Check that all "for" types are assignable from this type
                // This is currently "false" for all generic type registrations
                // Otherwise, Castle will throw an exception. See https://github.com/castleproject/Castle.Windsor-READONLY/blob/master/src/Castle.Windsor/Core/Internal/TypeByInheritanceDepthMostSpecificFirstComparer.cs
                if (@for.All(t => t.IsAssignableFrom(type)))
                {
                    @for = @for.Concat(type.CreateEnumerable()).ToArray();
                }
            }

            if (addDefaultServices && type != null)
            {
                var defaultServiceInterface = type
                    .GetInterfaces()
                    .FirstOrDefault(i => i.Name == "I{0}".FormatWith(type.Name));

                // Check if we have already a registration for default interface.
                var isDefaultServiceInterfaceRegistered = defaultServiceInterface != null
                                                          && ComponentRegistrations.Any(cr => cr.For.Contains(defaultServiceInterface));

                // If we do -> don't add it into "For", since it will produce exceptions in Windsor
                if (!isDefaultServiceInterfaceRegistered)
                {
                    if (type.IsGenericTypeDefinition && defaultServiceInterface != null && defaultServiceInterface.IsGenericType)
                    {
                        defaultServiceInterface = defaultServiceInterface.GetGenericTypeDefinition();
                    }

                    if (defaultServiceInterface != null && !@for.Contains(defaultServiceInterface))
                    {
                        @for = @for.Concat(new[] { defaultServiceInterface });
                    }
                }
            }

            if (addAllServices && type != null)
            {
                // Add only for services which are not yet added as components
                // Otherwise it would produce an exception upon registration
                var typeServices = type
                    .GetInterfaces()
                    .Where(i => !ComponentRegistrations.Any(cr => cr.For.Contains(i)))
                    .ToArray();

                @for = @for.Concat(typeServices).Distinct().ToArray();
            }

            Type[] componentRegistrationTypes = (type == null ? new Type[0] : type.CreateEnumerable()).Concat(@for).Distinct().ToArray();

            bool isFactory = componentRegistrationTypes.Any(t => t.GetCustomAttributes(typeof(FactoryAttribute), true).Length > 0);

            var lifestyleAttribute = componentRegistrationTypes
                .Select(t => t.GetAttributes<LifestyleAttribute>().FirstOrDefault())
                .FirstOrDefault(a => a != null);

            var concernDescriptors = GetConcerns(componentRegistrationTypes.ToArray());

            var componentRegistration = instance == null
                ? new ComponentRegistration(type, @for, lifestyleAttribute, concernDescriptors, initialize, isFactory)
                : new ComponentRegistration(instance, @for, lifestyleAttribute, concernDescriptors, initialize, isFactory);

            if (componentFilter != null && !componentFilter(componentRegistration))
            {
                // filtered out
                return null;
            }
            return componentRegistration;
        }

        private IEnumerable<IConcernDescriptor> GetConcerns(Type[] types)
        {
            // Get concerns based on attribute defined
            var privateConcerns = types
                .SelectMany(t => t.GetAttributes<ConcernAttribute>())
                .OrderByDescending(a => a.Priority)
                .OfType<IConcernDescriptor>()
                .Distinct()
                .ToArray();

            // Apply global concerns
            // NOTE/TODO: Global concern might yet not know where it is applicable or not! Allow deferred execution through lambda for now and refactor later
            var appliedGlobalConcerns = globalConcerns
                .Where(gc => types.Any(gc.IsApplicableFor)

                    // Private concerns always override the global concerns when they are of the same type
                             && privateConcerns.All(c => c.GetType() != gc.GetType())
                );

            var concerns = privateConcerns.Concat(appliedGlobalConcerns);

            // Ensure we are not trying to apply concern to itself and that concern is valid
            // NOTE/TODO: Again, we cannot enumerate into collection to allow deferred execution 
            concerns = concerns
                .Where(c => !types.Contains(c.ConcernType) && ValidateConcern(c));

            return concerns;
        }

        /// <summary>
        ///   Validates the concerns.
        /// </summary>
        /// <param name = "concern">The concern.</param>
        private static bool ValidateConcern(IConcernDescriptor concern)
        {
            if (!concern.ConcernType.Is<ITypeInterfacesConcern>() &&
                !concern.ConcernType.Is<IInterceptor>() &&
                !concern.ConcernType.Is<IInstanceConcern>())
            {
                throw new ArgumentException("Concern types must be an ITypeInterfacesConcern, IInterceptor, or IInstanceConcern.");
            }
            return true;
        }
    }
}