namespace Soaf.ComponentModel
{
    /// <summary>
    /// Information about a tracked object.
    /// </summary>
    public interface IObjectStateEntry
    {
        ObjectState State { get; set; }

        object Object { get; }

        bool IsRelationship { get; }
    }

    /// <summary>
    /// Represents informations about the state of a tracked object.
    /// </summary>
    public enum ObjectState
    {
        Unchanged,
        Added,
        Modified,
        Deleted
    }

    public class ObjectStateEntry : IObjectStateEntry
    {
        public ObjectStateEntry(object o)
        {
            Object = o;
            IsRelationship = o == null;
        }

        public ObjectState State { get; set; }
        public object Object { get; private set; }
        public bool IsRelationship { get; private set; }

        public override string ToString()
        {
            return string.Format("State: {0}, Object: {1}", State, Object);
        }
    }
}