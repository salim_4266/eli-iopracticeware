﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Soaf.Collections;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// Denotes that property value should be recursively validated
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ValidateObjectAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (!value.IsValid()) return false;
            if (value is IEnumerable)
            {
                return ((IEnumerable)value).Cast<object>().All(item => item.IsValid());
            }
            return true;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool hasValidated;
            if (value == null || validationContext.Items.TryGetValue(value, out hasValidated)) return ValidationResult.Success;

            validationContext.Items[value] = true;

            var result = base.IsValid(value, validationContext);
            // TODO: should we try validate value as IEnumerable if result on root == Success?
            return result;
        }
    }

    /// <summary>
    ///   Validates that a value is a valid file path.
    /// </summary>
    public class FileExistsAttribute : ValidationAttribute
    {
        public FileExistsAttribute()
            : base("The file specified does not exist.")
        {
        }

        public override bool IsValid(object value)
        {
            return value == null || value.ToString().IsNullOrEmpty() || File.Exists(value.ToString());
        }
    }

    /// <summary>
    ///   Validates that a value is a valid directory path.
    /// </summary>
    public class DirectoryExistsAttribute : ValidationAttribute
    {
        public DirectoryExistsAttribute()
            : base("The directory specified does not exist.")
        {
        }

        public override bool IsValid(object value)
        {
            return value == null || value.ToString().IsNullOrEmpty() || Directory.Exists(value.ToString());
        }
    }


    /// <summary>
    ///   Validates that a value is a valid regex..
    /// </summary>
    public class ValidRegularExpressionAttribute : ValidationAttribute
    {
        public ValidRegularExpressionAttribute()
            : base("The value specified is not a valid regular expression.")
        {
        }

        [DebuggerNonUserCode]
        public override bool IsValid(object value)
        {
            if (value == null || value.ToString().IsNullOrEmpty()) return true;

            try
            {
                // ReSharper disable ReturnValueOfPureMethodIsNotUsed
                Regex.Match(string.Empty, value.ToString());
                // ReSharper restore ReturnValueOfPureMethodIsNotUsed
            }
            catch (ArgumentException)
            {
                return false;
            }

            return true;
        }
    }
}