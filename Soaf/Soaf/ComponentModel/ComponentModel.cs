﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// Defines a type that has a unique identifier.
    /// </summary>
    public interface IHasId
    {
        object Id { get; }
    }

    public interface IHasTimeZoneId
    {
        string TimeZoneId { get; }
    }

    /// <summary>
    /// Defines a type that supports a caller firing PropertyChanged events.
    /// </summary>
    public interface IRaisePropertyChanged
    {
        void OnPropertyChanged(PropertyChangedEventArgs args);
    }

    /// <summary>
    /// Defines a type that supports a caller firing CollectionChanged events.
    /// </summary>
    public interface IRaiseCollectionChanged
    {
        void OnCollectionChanged();
    }

    /// <summary>
    /// An extension of IChangeTracking that provides additional functionality.
    /// </summary>
    public interface IChangeTrackingExtended : IChangeTracking
    {
        event EventHandler<EventArgs> AcceptingChanges;
        event EventHandler<EventArgs> AcceptedChanges;

        /// <summary>
        /// Gets information about the tracked objects.
        /// </summary>
        IEnumerable<IObjectStateEntry> ObjectStateEntries { get; }
    }

    /// <summary>
    /// Defines a type that supports the functionality of IRevertibleChangeTracking and also supports
    /// notifications of related events.
    /// </summary>
    public interface IRevertibleChangeTrackingExtended : IRevertibleChangeTracking
    {
        event EventHandler<EventArgs> RejectingChanges;
        event EventHandler<EventArgs> RejectedChanges;
    }

    /// <summary>
    /// An IDisposable that notifies before and after disposal.
    /// </summary>
    public interface INotifyingDisposable : IDisposable
    {
        /// <summary>
        ///   Occurs before disposing.
        /// </summary>
        event EventHandler<EventArgs> Disposing;

        /// <summary>
        ///   Occurs when disposed.
        /// </summary>
        event EventHandler<EventArgs> Disposed;
    }

    /// <summary>
    /// Describes a disposable instance where the disposed status can be checked
    /// </summary>
    public interface IDisposalInfo : IDisposable
    {
        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        bool IsDisposed { get; }
    }

    /// <summary>
    /// Information about a member of a type.
    /// </summary>
    public class DataMember
    {
        public DataMember()
        {
        }

        public DataMember(string name, MemberInfo memberInfo = null, int order = 0)
        {
            Name = name;
            MemberInfo = memberInfo;
            Order = order;
        }

        public string Name { get; set; }
        public int Order { get; set; }
        public MemberInfo MemberInfo { get; set; }

        public bool Equals(string other)
        {
            return other == Name;
        }

        public override bool Equals(object obj)
        {
            return obj.As<DataMember>().IfNotNull(dm => dm.Name == Name && dm.MemberInfo.IfNotNull(m => m.DeclaringType) == MemberInfo.IfNotNull(m => m.DeclaringType));
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
