using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;

[assembly: Component(typeof(DataErrorInfoBehavior))]

namespace Soaf.ComponentModel
{
    /// <summary>
    ///   Denotes that a type should support automatic validation via IDataErrorInfo.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class SupportsDataErrorInfoAttribute : ConcernAttribute
    {
        public SupportsDataErrorInfoAttribute()
            : base(typeof(DataErrorInfoBehavior))
        {
        }
    }

    /// <summary>
    ///   The implementation of automatic IDataErrorInfo behavior for a type annotated with SupportsDataErrorInfo.
    /// </summary>
    [InterceptedMethodsFilter(typeof(InterceptSettersFilter))] //<-- Getters and methods for IDataErrorInfo will automatically be supported
    internal class DataErrorInfoBehavior : IInterceptor, ITypeInterfacesConcern
    {
        private static readonly MethodInfo GetErrorMethod = Reflector.GetMember<IDataErrorInfo>(e => e.Error).CastTo<PropertyInfo>().GetGetMethod();
        private static readonly MethodInfo GetItemMethod = typeof(IDataErrorInfo).GetProperty("Item").EnsureNotDefault("Could not find indexer on IDataErrorInfo.").GetGetMethod();

        private MethodInfo[] getErrorMethods;
        private MethodInfo[] getItemMethods;

        private static readonly IDictionary<Type, Tuple<MethodInfo[], MethodInfo[]>> MethodCache = new Dictionary<Type, Tuple<MethodInfo[], MethodInfo[]>>().Synchronized();

        public void Intercept(IInvocation invocation)
        {
            if (SuppressPropertyChangedScope.Current != null)
            {
                invocation.TryProceed();
                return;
            }

            if (getErrorMethods == null || getItemMethods == null)
            {
                var methods = MethodCache.GetValue(invocation.Target.GetType(), () =>
                                                                                {

                                                                                    var map = invocation.Target.GetType().GetInterfaceMap(typeof(IDataErrorInfo));
                                                                                    var getErrorMethodIndex = map.InterfaceMethods.IndexOf(GetErrorMethod);
                                                                                    var getItemMethodIndex = map.InterfaceMethods.IndexOf(GetItemMethod);
                                                                                    return Tuple.Create(new[] { GetErrorMethod, map.TargetMethods[getErrorMethodIndex] }, new[] { GetItemMethod, map.TargetMethods[getItemMethodIndex] });
                                                                                });
                getErrorMethods = methods.Item1;
                getItemMethods = methods.Item2;
            }

            bool isGetErrorMethod = getErrorMethods.Contains(invocation.Method);
            bool isGetItemMethod = !isGetErrorMethod && getItemMethods.Contains(invocation.Method);
            if (isGetErrorMethod || isGetItemMethod)
            {
                ProcessGetErrorOrGetItem(invocation, isGetErrorMethod);
                return;
            }

            invocation.Proceed();
        }

        private static void ProcessGetErrorOrGetItem(IInvocation invocation, bool isGetErrorMethod)
        {
            var validationResults = invocation.Target.Validate();
            if (isGetErrorMethod)
            {
                var error = validationResults.Select(i => new[] { i.Key, i.Value }.Where(v => v.IsNotNullOrEmpty()).Join(": ")).Join(Environment.NewLine);
                invocation.ReturnValue = error.IsNullOrWhiteSpace() ? null : error;
            }
            else invocation.ReturnValue = validationResults.GetValue(invocation.Arguments[0] as string);
        }

        #region ITypeInterfacesConcern Members

        public IEnumerable<Type> Interfaces
        {
            get { yield return typeof(IDataErrorInfo); }
        }

        #endregion

    }
}