using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using Soaf.Collections;
using Soaf.Linq.Expressions;
using Soaf.Reflection;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// Helper/extension validation methods.
    /// </summary>
    public static class Validator
    {
        /// <summary>
        /// Determines whether all collection items are valid
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool AreItemsValid<TItem>(this IEnumerable<TItem> target)
        {
            return target.All(item => item.IsValid());
        }

        /// <summary>
        /// Validates specified items. Non-recursively
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static Dictionary<TItem, Dictionary<string, string>> ValidateItems<TItem>(this IEnumerable<TItem> target)
        {
            var result = target
                .Distinct() // Do not validate duplicates in collection to prevent exception on dictionary
                .Select(i => new { Item = i, Errors = i.Validate() })
                .Where(i => i.Errors.Any())
                .ToDictionary(i => i.Item, i => i.Errors);

            return result;
        }

        /// <summary>
        /// Determines whether the specified target is valid.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns>
        ///   <c>true</c> if the specified target is valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValid(this object target)
        {
            return target != null && target.Validate().Count == 0;
        }

        /// <summary>
        /// Validates the specified target. Will also validate all properties in graph marked with [ValidateObject] attribute.
        /// </summary>
        /// <param name="target">The target. Can be a collection of items as well.</param>
        /// <param name="throwValidationException">if set to <c>true</c> [throw validation exception].</param>
        /// <returns></returns>
        /// <exception cref="System.ComponentModel.DataAnnotations.ValidationException"></exception>
        public static Dictionary<string, string> Validate(this object target, bool throwValidationException = false)
        {
            var validationResults = target.Validate(false, throwValidationException);

            // Group validation result by first mentioned member name and then concatenate error messages by that member
            var errorsByMember = validationResults
                .Where(vr => vr != ValidationResult.Success)
                .GroupBy(i => i.MemberNames.FirstOrDefault() ?? string.Empty)
                .ToDictionary(
                    i => i.Key,
                    i => i.Select(vr => vr.ErrorMessage).Join(Environment.NewLine));

            if (throwValidationException && validationResults.Count > 0)
            {
                var exceptionMessage = "{0} is invalid.\r\n{1}".FormatWith(target.GetType().Name, errorsByMember
                    .Select(i => new[] { i.Key, i.Value }
                        .Where(v => v.IsNotNullOrEmpty())
                        .Join(": ")).Join(Environment.NewLine));

                throw new ValidationException(exceptionMessage);
            }

            return errorsByMember;
        }

        /// <summary>
        /// Validates the specified target. Will also validate all properties in graph marked with [ValidateObject] attribute.
        /// </summary>
        /// <param name="target">The target. Can be a collection of items as well.</param>
        /// <param name="breakOnFirstError">If set to <c>true</c>, will break on the first validation error</param>
        /// <param name="throwValidationException">if set to <c>true</c> [throw validation exception].</param>
        /// <returns></returns>
        public static List<ValidationResult> Validate(this object target, bool breakOnFirstError, bool throwValidationException)
        {
            RegisterTypeAndMetadataType(target);

            return ValidateAndGetValidationResults(target, breakOnFirstError);
        }


        /// <summary>
        /// Validates the value of the property expressed by <see cref="propertyPath"/> against the <see cref="target"/> object instance.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="target"/> instance</typeparam>
        /// <param name="target">object instance containing the property to validate</param>
        /// <param name="propertyPath">An expression delegate that specifies the property path</param>
        /// <returns></returns>
        public static List<ValidationResult> Validate<T>(this T target, Expression<Func<T, object>> propertyPath)
        {
            var propertyName = propertyPath.MemberPath();
            var propertyValue = propertyPath.Invoke(target);

            return Validate(target, propertyName, propertyValue);
        }


        /// <summary>
        /// Validates the value of the property specified by <see cref="propertyName"/> against the <see cref="target"/> object instance.
        /// </summary>
        /// <param name="target">object instance containing the property to validate</param>
        /// <param name="propertyName">The name of the property to validate</param>
        /// <param name="propertyValue">The value of the property to validate</param>
        /// <returns></returns>
        public static List<ValidationResult> Validate(this object target, string propertyName, object propertyValue)
        {
            // Storage for all validation results
            var validationResults = new List<ValidationResult>();

            var validationContext = new ValidationContext(target, null, null) { MemberName = propertyName };

            System.ComponentModel.DataAnnotations.Validator.TryValidateProperty(propertyValue, validationContext, validationResults);

            return validationResults;
        }

        private static void RegisterTypeAndMetadataType(object target)
        {
            var vr = new ValidationResult("");
            vr.Validate(x => x.ErrorMessage);
            var targetType = target.GetType();
            var metadataType = target.GetType().GetMetadataType();

            // Add metadata if defined for this target
            if (metadataType != target.GetType())
            {
                TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(targetType, metadataType), target);
            }
        }

        private static List<ValidationResult> ValidateAndGetValidationResults(object target, bool breakOnFirstError)
        {
            // Storage for all validation results
            var validationResults = new List<ValidationResult>();

            // Validate
            var validationContext = new ValidationContext(target, null, null);
            System.ComponentModel.DataAnnotations.Validator.TryValidateObject(target, validationContext, validationResults, !breakOnFirstError);

            return validationResults;
        }

        /// <summary>
        /// Returns true if this <see cref="validationResult"/> equals <see cref="ValidationResult.Success"/>
        /// </summary>
        /// <param name="validationResult"></param>
        /// <returns></returns>
        public static bool IsSuccessful(this ValidationResult validationResult)
        {
            return validationResult == ValidationResult.Success;
        }

        /// <summary>
        /// Returns true if this <see cref="validationResult"/> does not equals <see cref="ValidationResult.Success"/>
        /// </summary>
        /// <param name="validationResult"></param>
        /// <returns></returns>
        public static bool IsNotSuccessful(this ValidationResult validationResult)
        {
            return validationResult != ValidationResult.Success;
        }

        /// <summary>
        /// Returns true if the <see cref="validationResult"/> ErrorMessage property has content.
        /// </summary>
        /// <param name="validationResult"></param>
        /// <returns></returns>
        public static bool HasErrorMessage(this ValidationResult validationResult)
        {
            return !string.IsNullOrEmpty(validationResult.ErrorMessage);
        }

        /// <summary>
        /// Takes a dictionary of string key and string value and returns a concatenated string of all the messages joined by a newline.
        /// </summary>
        /// <param name="validationMessages"></param>
        /// <returns></returns>
        public static string ToValidationMessageString(this Dictionary<string, string> validationMessages)
        {
            return validationMessages != null && validationMessages.Count > 0 ? validationMessages.Values.Distinct().Join(Environment.NewLine, true) : null;
        }

        public static string ToValidationMessageString(this List<ValidationResult> validationResults)
        {
            return validationResults.IfNotNull(vr => vr.Join(x => string.Format("{0}: {1}", x.MemberNames.Join(",", true), x.ErrorMessage), Environment.NewLine));
        }

        /// <summary>
        /// Converts dictionary of string to a list of string with key and value combined
        /// </summary>
        /// <param name="inputDictionary"></param>
        /// <param name="keyValueSeparator"></param>
        /// <returns></returns>
        public static IList<string> ToValidationMessagesList(this Dictionary<string, string> inputDictionary, string keyValueSeparator = ": ")
        {
            var result = inputDictionary
                .Select(k => string.Join(keyValueSeparator, k.Key, k.Value))
                .ToList();
            return result;
        }
    }
}