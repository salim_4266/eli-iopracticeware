using System;
using System.Collections.Generic;
using Soaf.ComponentModel;

[assembly: Component(typeof(DisposableBehavior))]

namespace Soaf.ComponentModel
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class DisposableAttribute : ConcernAttribute
    {
        public DisposableAttribute()
            : base(typeof(DisposableBehavior))
        {
        }
    }

    /// <summary>
    /// Place holder to denote that a type is IDisposable and should be treated as such in terms of its lifestyle.
    /// </summary>
    internal class DisposableBehavior : ITypeInterfacesConcern
    {
        public IEnumerable<Type> Interfaces
        {
            get { yield return typeof(IDisposable); }
        }
    }
}