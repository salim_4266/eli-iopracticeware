using System;
using System.Transactions;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// A late bound wrapper for a dependent clone based transaction scope.
    /// </summary>
    public class DependentTransactionScope : IDisposable
    {
        private readonly bool autoComplete;
        private readonly DependentTransaction dependentClone;
        private readonly Transaction originalTransaction;
        private readonly TransactionScope scope;

        public DependentTransactionScope(Transaction originalTransaction, bool autoComplete = false)
        {
            this.autoComplete = autoComplete;
            this.originalTransaction = originalTransaction;
            if (originalTransaction != null)
            {
                lock (this.originalTransaction)
                {
                    dependentClone = this.originalTransaction.DependentClone(DependentCloneOption.BlockCommitUntilComplete);
                }
                scope = new TransactionScope(dependentClone);
            }
        }

        public void Complete()
        {
            lock (originalTransaction ?? new object())
            {
                if (scope != null)
                {
                    scope.Complete();
                    dependentClone.Complete();
                }
            }
        }

        public void Dispose()
        {
            lock (originalTransaction ?? new object())
            {
                try
                {
                    if (autoComplete) Complete();
                }
                finally
                {
                    if (scope != null)
                    {
                        scope.Dispose();
                        dependentClone.Dispose();
                    }
                }
            }
        }
    }
}