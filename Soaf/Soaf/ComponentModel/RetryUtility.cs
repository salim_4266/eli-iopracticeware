using System;
using System.Collections.Generic;
using System.Threading;

namespace Soaf.ComponentModel
{
    /// <summary>
    ///   Provides a mechanism for retrying delegates.
    /// </summary>
    public static class RetryUtility
    {
        /// <summary>
        ///   Executes the action with retry handling.
        /// </summary>
        /// <typeparam name="TResult"> The type of the result. </typeparam>
        /// <param name="action"> The action. </param>
        /// <param name="numberOfRetries"> The number of retries. </param>
        /// <param name="retryInterval"> The retry interval. </param>
        /// <param name="shouldRetry"> The should retry. </param>
        /// <returns> </returns>
        public static TResult ExecuteWithRetry<TResult>(this Func<TResult> action, int numberOfRetries, TimeSpan retryInterval = default(TimeSpan), Func<Exception, bool> shouldRetry = null)
        {
            if (retryInterval == default(TimeSpan)) retryInterval = TimeSpan.FromMilliseconds(100);

            var exceptionsThrown = new List<Exception>();
            var retriesLeft = numberOfRetries;
            TResult result = default(TResult);
            do
            {
                try
                {
                    result = action();
                    return result;
                }
                catch (Exception ex)
                {
                    exceptionsThrown.Add(ex);
                    if ((shouldRetry != null && !shouldRetry(ex)) || retriesLeft <= 0)
                    {
                        // Throw original exception if we are not allowed to retry
                        if (numberOfRetries <= 0) throw;

                        throw new AggregateException("Operation failed to complete after {0}/{1} retries. See list of thrown exceptions for every attempt"
                            .FormatWith(exceptionsThrown.Count, numberOfRetries), exceptionsThrown);
                    }
                    Thread.Sleep(retryInterval);
                }
            } while (retriesLeft-- > 0);
            return result;
        }

        /// <summary>
        ///   Executes the action with retry handling.
        /// </summary>
        /// <param name="action"> The action. </param>
        /// <param name="numberOfRetries"> The number of retries. </param>
        /// <param name="retryInterval"> The retry interval. </param>
        /// <param name="shouldRetry"> The should retry. </param>
        public static void ExecuteWithRetry(this Action action, int numberOfRetries, TimeSpan retryInterval, Func<Exception, bool> shouldRetry = null)
        {
            ExecuteWithRetry(new Func<object>(() =>
                                              {
                                                  action();
                                                  return null;
                                              }), numberOfRetries, retryInterval, shouldRetry);
        }
    }
}