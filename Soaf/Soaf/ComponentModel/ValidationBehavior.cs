using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;

[assembly: Component(typeof(ValidationBehavior))]

namespace Soaf.ComponentModel
{
    /// <summary>
    ///   Denotes that a type should support automatic validation on attributed members.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class SupportsValidationAttribute : ConcernAttribute
    {
        public SupportsValidationAttribute()
            : base(typeof(ValidationBehavior))
        {
        }
    }

    /// <summary>
    ///   The implementation of automatic validation on members of a type annotated with Validates.
    /// </summary>
    internal class ValidationBehavior : AttributedMemberInterceptor
    {
        private readonly IDictionary<MethodInfo, List<Action<IInvocation>>> methodPostInvocationValidators = new Dictionary<MethodInfo, List<Action<IInvocation>>>().Synchronized();
        private readonly IDictionary<MethodInfo, List<Action<IInvocation>>> methodPreInvocationValidators = new Dictionary<MethodInfo, List<Action<IInvocation>>>().Synchronized();

        private readonly object syncRoot = new object();

        public override IEnumerable<Type> AttributeTypes
        {
            get { yield return typeof(ValidationAttribute); }
        }

        public override void Intercept(IInvocation invocation)
        {
            List<Action<IInvocation>> validators;

            lock (syncRoot)
            {
                if (!methodPreInvocationValidators.TryGetValue(invocation.Method, out validators))
                {
                    methodPreInvocationValidators[invocation.Method] = validators = GetPreInvocationValidators(invocation.Method);
                }
            }

            try
            {
                foreach (var validator in validators)
                {
                    validator(invocation);
                }

                invocation.Proceed();

                lock (syncRoot)
                {
                    if (!methodPostInvocationValidators.TryGetValue(invocation.Method, out validators))
                    {
                        methodPostInvocationValidators[invocation.Method] = validators = GetPostInvocationValidators(invocation.Method);
                    }
                }

                foreach (var validator in validators)
                {
                    validator(invocation);
                }
            }
            catch (Exception ex)
            {
                if (ex is ValidationException)
                {
                    throw new ValidationException("An error occurred invoking {0} with parameters {1}".FormatWith(invocation.Method.Name, invocation.Method.GetParameters().Select((p, i) => p.Name + ": " + (i < invocation.Arguments.Length ? invocation.Arguments[i] ?? "null" : "null")).Join()), ex);
                }
                throw;
            }
        }

        public override bool ShouldIntercept(Type type, MethodInfo method)
        {
            return base.ShouldIntercept(type, method)
                   || method.GetParameters().Any(p => p.HasAttribute<ValidationAttribute>())
                   || method.ReturnParameter.HasAttribute<ValidationAttribute>();
        }

        /// <summary>
        ///   Gets the pre invocation validators. These are for parameters.
        /// </summary>
        /// <param name = "methodInfo">The method info.</param>
        /// <returns></returns>
        private List<Action<IInvocation>> GetPreInvocationValidators(MethodInfo methodInfo)
        {
            var results = new List<Action<IInvocation>>();

            ParameterInfo[] parameters = methodInfo.GetParameters();

            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];

                var parameterAttributes = parameter.GetAttributes().OfType<ValidationAttribute>();
                int index = i;
                results.AddRange(from attribute in parameterAttributes
                    let argumentIndex = index
                    select (Action<IInvocation>)(invocation =>
                                                 {
                                                     var vc = new ValidationContext(invocation.Arguments[argumentIndex] ?? new object(), null, null);
                                                     vc.DisplayName = parameter.Name;
                                                     attribute.Validate(invocation.Arguments[argumentIndex], vc);
                                                 }));
            }

            PropertyInfo property = methodInfo.GetProperty();
            if (property != null && property.GetSetMethod(true) == methodInfo)
            {
                var propertyAttributes = property.GetAttributes().OfType<ValidationAttribute>();
                results.AddRange(propertyAttributes.Select(attribute => (Action<IInvocation>)(invocation =>
                                                                                              {
                                                                                                  var vc = new ValidationContext(invocation.Arguments[0] ?? new object(), null, null);
                                                                                                  vc.DisplayName = property.Name;
                                                                                                  attribute.Validate(invocation.Arguments[0], vc);
                                                                                              })));
            }

            return results;
        }

        /// <summary>
        ///   Gets the post invocation validators. These are for return values.
        /// </summary>
        /// <param name = "methodInfo">The method info.</param>
        /// <returns></returns>
        private List<Action<IInvocation>> GetPostInvocationValidators(MethodInfo methodInfo)
        {
            var attributes = methodInfo.GetAttributes().OfType<ValidationAttribute>();

            return attributes.Select(attribute => (Action<IInvocation>)(invocation =>
                                                                        {
                                                                            var vc = new ValidationContext(invocation.ReturnValue ?? new object(), null, null);
                                                                            vc.DisplayName = methodInfo.Name;
                                                                            attribute.Validate(invocation.ReturnValue, vc);
                                                                        })).ToList();
        }
    }
}