using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Transactions;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// A BackgroundWorker wrapper that provides some additional functionality.
    /// </summary>
    public class BackgroundWorker : System.ComponentModel.BackgroundWorker
    {
        private Transaction transaction;
        private bool propogateTransactions;
        private object nextRunArgument;
        protected bool RunAgain { get; private set; }

        public BackgroundWorker()
        {
            WorkerSupportsCancellation = true;
        }

        /// <summary>
        ///   Runs the async action and assigns member with the result.
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "getResult">The get result.</param>
        /// <param name = "propertySelector">The property selector.</param>
        /// <param name = "callback">The callback.</param>
        /// <param name = "event">The @event.</param>
        public static void RunAsyncAndAssignMember(object sender, Func<object> getResult, Expression<Func<object>> propertySelector, Action<object> callback = null, PropertyChangedEventHandler @event = null)
        {
            MemberExpression memberExpression = propertySelector.Body is UnaryExpression ? ((UnaryExpression)propertySelector.Body).Operand as MemberExpression : propertySelector.Body as MemberExpression;

            if (memberExpression == null || !(memberExpression.Member is PropertyInfo)) throw new InvalidOperationException("Property selector must project a member.");

            var property = memberExpression.Member as PropertyInfo;
            if (property == null) throw new InvalidOperationException("Member must be a property.");

            object result = null;
            var worker = new System.ComponentModel.BackgroundWorker();
            worker.DoWork += delegate
                             {
                                 result = getResult();
                             };
            worker.RunWorkerCompleted += (s, e) =>
                                         {
                                             if (e.Error != null) throw e.Error;
                                             property.SetValue(sender, result, null);
                                             if (callback != null) callback(result);
                                             if (@event != null) @event.Fire(sender, propertySelector);
                                         };
            worker.RunWorkerAsync();
        }

        public virtual void RunWorkerAsync(object argument = null, bool cancelAndRunAgainIfBusy = true, bool propogateTransactions = true)
        {
            if (propogateTransactions)
            {
                this.propogateTransactions = true;
                transaction = Transaction.Current;
            }
            if (cancelAndRunAgainIfBusy && IsBusy)
            {
                RunAgain = true;
                nextRunArgument = argument;
                CancelAsync();
            }
            else
            {
                base.RunWorkerAsync(argument);
            }
        }

        protected override void OnDoWork(DoWorkEventArgs e)
        {
            if (transaction != null)
            {
                using (new DependentTransactionScope(transaction, true))
                {
                    base.OnDoWork(e);
                }
            }
            else
            {
                base.OnDoWork(e);
            }
        }

        protected override void OnRunWorkerCompleted(RunWorkerCompletedEventArgs e)
        {
            transaction = null;
            base.OnRunWorkerCompleted(e);
            if (RunAgain)
            {
                RunAgain = false;
                var propogateTransactions = this.propogateTransactions;
                this.propogateTransactions = false;
                RunWorkerAsync(nextRunArgument, true, propogateTransactions);
            }
        }
    }
}