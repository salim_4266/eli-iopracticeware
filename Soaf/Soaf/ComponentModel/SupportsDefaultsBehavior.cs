using System;
using System.ComponentModel;
using System.Linq;
using Soaf.Reflection;

namespace Soaf.ComponentModel
{
    /// <summary>
    ///   Denotes that a type has properties annotated with a DefaultValueAttribute and should have those default values set after construction.
    /// </summary>
    public class SupportsDefaultsAttribute : ConcernAttribute
    {
        public SupportsDefaultsAttribute()
            : base(typeof(SupportsDefaultsBehavior))
        {
        }
    }

    /// <summary>
    ///   The implementation of automatically setting values for properties annotated with DefaultValueAttribute in types annotated with SupportsDefaultsAttribute.
    /// </summary>
    internal class SupportsDefaultsBehavior : IInstanceConcern
    {
        private readonly IServiceProvider serviceProvider;

        public SupportsDefaultsBehavior(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        #region IInstanceConcern Members

        public void ApplyConcern(object value)
        {
            foreach (var i in value.GetType().GetProperties().Select(pi => new { pi, attribute = Reflector.GetAttribute<DefaultValueAttribute>(pi) }).Where(i => i.attribute != null))
            {
                if (!i.pi.CanWrite)
                {
                    throw new ArgumentException("Properties with default values must have a setter.");
                }

                i.pi.SetValue(value, GetDefaultValue(i.pi.PropertyType, i.attribute), null);
            }
        }

        #endregion

        private object GetDefaultValue(Type propertyType, DefaultValueAttribute attribute)
        {
            if (attribute.Value is Type) return serviceProvider.GetService(attribute.Value.CastTo<Type>());
            if (attribute.Value == null && propertyType.IsValueType) return Activator.CreateInstance(propertyType);
            if (attribute.Value == null) return serviceProvider.GetService(propertyType);
            return attribute.Value;
        }
    }
}