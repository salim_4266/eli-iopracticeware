using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using Soaf.Collections;
using Soaf.Threading;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// A CallContext based contextual dictionary data store that can transfer over WCF calls. Use sparingly.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace, Name = "ContextData")]
    [Serializable]
    public static class ContextData
    {
        private const string Key = "__ContextData__";

        private static readonly ReaderWriterLockSlim StoreLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

        /// <summary>
        /// The name for OperationContext header.
        /// </summary>
        private static readonly string HeaderName = "ArrayOfKeyValueOfstringanyType";

        private static readonly string HeaderNamespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays";


        /// <summary>
        /// Gets the store of tracked objects. An object is tracked when Set is called.
        /// </summary>
        /// <value>
        /// The store.
        /// </value>
        public static IEnumerable<string> Keys
        {
            get
            {
                return InternalStore.Keys;
            }
        }

        /// <summary>
        /// Gets the internal store (an actual dictionary, which may or may not be set in the logical CallContext).
        /// </summary>
        /// <value>
        /// The internal store.
        /// </value>
        private static Dictionary<string, bool> InternalStore
        {
            get
            {
                var instance = CallContext.LogicalGetData(Key) as Dictionary<string, bool>;
                if (instance == null)
                {
                    // lazy initialize
                    instance = new Dictionary<string, bool>();
                    CallContext.LogicalSetData(Key, instance);
                }
                return instance;
            }
        }

        /// <summary>
        /// Gets the specified key value from the logical CallContext.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static object Get(string key)
        {
            using (new ReadOnlyLock(StoreLock))
                return CallContext.LogicalGetData(key).As<ContextDataObjectProxy>().IfNotNull(p => p.Instance);
        }

        /// <summary>
        /// Gets the specified key value from the logical CallContext. If value at key doesn't exist -> sets it
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValueFunc">The default value function.</param>
        /// <returns></returns>
        public static TValue Get<TValue>(string key, Func<TValue> defaultValueFunc)
        {
            bool contains;
            using (new ReadOnlyLock(StoreLock))
            {
                contains = Keys.Contains(key);
            }

            if (!contains)
            {
                // Ensure value is set when possible
                using (new UpgradeableReadLock(StoreLock))
                {
                    if (!Keys.Contains(key))
                    {
                        Set(key, defaultValueFunc());
                    }
                }
            }

            return (TValue)Get(key);
        }

        /// <summary>
        /// Sets the specified key and value in the logical CallContext and internal store.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public static void Set(string key, object value)
        {
            if (!value.Is<ContextDataObjectProxy>())
            {
                value = new ContextDataObjectProxy(value);
            }

            CallContext.LogicalSetData(key, value);

            using (new WriteLock(StoreLock))
            {
                InternalStore[key] = true;
            }
        }

        /// <summary>
        /// Sets the key and value or removes the key if the value is null.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public static void SetOrRemove(string key, object value)
        {
            if (value == null) Remove(key);
            Set(key, value);
        }

        /// <summary>
        /// Removes the specified key from the logical CallContext and internal store.
        /// </summary>
        /// <param name="key">The key.</param>
        public static void Remove(string key)
        {
            CallContext.FreeNamedDataSlot(key);
            using (new WriteLock(StoreLock))
            {
                InternalStore.Remove(key);
            }
        }

        /// <summary>
        /// Clears all tracked objects.
        /// </summary>
        public static void Clear()
        {
            foreach (var key in Keys.ToArray())
            {
                Remove(key);
            }
        }


        /// <summary>
        /// Loads items from HttpContext into the logical CallContext and tracks them.
        /// </summary>
        internal static void LoadFromHttpContext()
        {
            try
            {
                using (new WriteLock(StoreLock))
                {
                    if (HostingEnvironment.IsHosted && HttpContext.Current != null)
                    {
                        var store = HttpContext.Current.Application[Key] as Dictionary<string, object>;
                        if (store != null)
                        {
                            store.ForEach(pair => Set(pair.Key, pair.Value));
                        }
                    }
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
                // sometimes HttpContext.Current throws a not accessible error
            }
        }

        /// <summary>
        /// Loads items from the OperationContext into the logical CallContext and tracks them.
        /// </summary>
        internal static void LoadFromOperationContext()
        {
            using (new WriteLock(StoreLock))
            {
                if (OperationContext.Current != null && OperationContext.Current.IncomingMessageHeaders != null && OperationContext.Current.IncomingMessageHeaders.FindHeader(HeaderName, HeaderNamespace) >= 0)
                {
                    var store = OperationContext.Current.IncomingMessageHeaders.GetHeader<Dictionary<string, object>>(HeaderName, HeaderNamespace, Serialization.CreateDataContractSerializer<Dictionary<string, object>>());
                    if (store != null)
                    {
                        store.ForEach(pair => Set(pair.Key, pair.Value));
                    }
                }
            }
        }

        /// <summary>
        /// Stores the tracked items in the HttpContext.
        /// </summary>
        internal static void StoreInHttpContext()
        {
            try
            {
                using (new ReadOnlyLock(StoreLock))
                {
                    if (InternalStore.Count > 0 && HostingEnvironment.IsHosted && HttpContext.Current != null)
                    {
                        var contextStore = Keys.ToDictionary(k => k, Get);
                        HttpContext.Current.Application[Key] = contextStore;
                    }
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
                // sometimes HttpContext.Current throws a not accessible error
            }
        }

        /// <summary>
        /// Stores the tracked items in the current OperationContext.
        /// </summary>
        /// <returns></returns>
        internal static void StoreInOperationContext()
        {
            using (new ReadOnlyLock(StoreLock))
            {
                if (InternalStore.Count > 0 && OperationContext.Current != null)
                {
                    var contextStore = Keys.ToDictionary(k => k, Get);

                    var header = MessageHeader.CreateHeader(HeaderName, HeaderNamespace, contextStore, Serialization.CreateDataContractSerializer<Dictionary<string, object>>());

                    OperationContext.Current.OutgoingMessageHeaders.Add(header);
                }
            }
        }

        /// <summary>
        /// A proxy object that is MarshalByRef to prevent serialization problems.
        /// </summary>
        private class ContextDataObjectProxy : MarshalByRefObject
        {
            [NonSerialized]
            private readonly object instance;
            public object Instance { get { return instance; } }

            public ContextDataObjectProxy(object instance)
            {
                this.instance = instance;
            }
        }
    }
}