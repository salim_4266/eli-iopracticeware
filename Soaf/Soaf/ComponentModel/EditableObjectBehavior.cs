using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;

[assembly: Component(typeof(EditableObjectBehavior))]

namespace Soaf.ComponentModel
{
    /// <summary>
    /// An extended interface that offers additional functionality for IEditableObjects.
    /// It also adds a requirement of implementing IChangeTracking
    /// </summary>
    public interface IEditableObjectExtended : IEditableObject, IChangeTracking
    {
        /// <summary>
        /// Reports whether current object is in editing or named editing mode
        /// </summary>
        bool IsEditing { get; }

        /// <summary>
        /// Modifies cascading of edit events behavior to children
        /// </summary>
        bool CascadeEditingEventsToChildren { get; set; }

        /// <summary>
        /// Begins edit session with a name to separate it from Begin/End edit session without name.
        /// IMPORTANT: the event will cascade only to children which implement <see cref="IEditableObjectExtended"/> 
        /// </summary>
        /// <param name="name"></param>
        void BeginNamedEdit(string name);

        /// <summary>
        /// Ends edit session with a name. Considers performed edits as final.
        /// IMPORTANT: the event will cascade only to children which implement <see cref="IEditableObjectExtended"/> 
        /// </summary>
        /// <param name="name"></param>
        void EndNamedEdit(string name);

        /// <summary>
        /// Cancels edit session with a name. Reverts edits.
        /// IMPORTANT: the event will cascade only to children which implement <see cref="IEditableObjectExtended"/> 
        /// </summary>
        /// <param name="name"></param>
        void CancelNamedEdit(string name);

        /// <summary>
        /// Returns a dictionary of properties that were changed with their original values when BeginNamedEdit was first called.
        /// IMPORTANT: the original values/properties returned which are reference types will contain the original reference. The value data of that
        /// reference will not be the original values (if any of them were changed). You must call GetOriginalValuesForNamedEdit on that
        /// reference as well (As long it implements IEditableObjectExtended) to get the original values. /> 
        /// </summary>
        /// <returns></returns>
        Dictionary<string, object> GetOriginalValues();

        /// <summary>
        /// Returns a dictionary of properties that were changed with their original values when BeginNamedEdit was first called.
        /// IMPORTANT: the original values/properties returned which are reference types will contain the original reference. The value data of that
        /// reference will not be the original values (if any of them were changed). You must call GetOriginalValuesForNamedEdit on that
        /// reference as well (As long it implements IEditableObjectExtended) to get the original values. /> 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Dictionary<string, object> GetOriginalValuesForNamedEdit(string name);
    }

    /// <summary>
    ///   Denotes that a type should automatically implement behavior and support for IEditableObject.
    /// </summary>
    public class EditableObjectAttribute : ConcernAttribute
    {
        public EditableObjectAttribute()
            : base(typeof(EditableObjectBehavior))
        {
            CascadeEditingEventsToChildren = true;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to cascade editing events to children. Will call EndEdit, BeginEdit, AcceptChanges, etc. on children if set to true. Default is true.
        /// </summary>
        /// <value>
        /// <c>true</c> if [cascade editing events to children]; otherwise, <c>false</c>.
        /// </value>
        public bool CascadeEditingEventsToChildren { get; set; }
    }

    /// <summary>
    /// Defines that a property should be ignored for change tracking.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreChangeTrackingAttribute : Attribute
    {

    }

    /// <summary>
    ///   The implementation of automatic IEditableObject behavior for a type annotated with EditableObjectAttribute.
    /// </summary>
    [InterceptedMethodsFilter(typeof(InterceptSettersFilter))] // Getters & methods for IEditableObjectExtended will automatically be supported
    internal class EditableObjectBehavior : IInterceptor, ITypeInterfacesConcern
    {
        private static readonly ICollection<object> EmptyObjectCollection = new Collection<object>();
        private static readonly IDictionary<Type, TypeInfo> TypeInfoCache = new Dictionary<Type, TypeInfo>().Synchronized();
        private readonly IDictionary<string, EditCancellationData> edits = new Dictionary<string, EditCancellationData>();

        /// <summary>
        /// Keeps tracking changes made to the object
        /// </summary>
        private readonly EditCancellationData changeTrackingSession = new EditCancellationData();

        private IDictionary<string, PropertyDetails> properties;
        private IDictionary<MethodInfo, bool> setMethods;
        private bool cascadeEditingEventsToChildren;

        /// <summary>
        /// Information stored between BeginEdit and EndEdit to make CancelEdit possible
        /// </summary>
        private class EditCancellationData
        {
            public EditCancellationData()
            {
                OldValues = new Dictionary<Invoker, object>();
            }

            public IDictionary<Invoker, object> OldValues { get; private set; }
        }

        private class PropertyDetails
        {
            public Invoker GetMethodInvoker { get; set; }
            public Invoker SetMethodInvoker { get; set; }
            public bool IsIgnoredFromChangeTracking { get; set; }

            /// <summary>
            /// Specifies whether property type could potentially hold instances of
            /// <see cref="IChangeTracking"/> or <see cref="IEditableObject"/> interfaces.
            /// This is an optimization to avoid reading all property values when cascading actions
            /// </summary>
            public bool CanImplementConcernInterace { get; set; }
        }

        private class TypeInfo
        {
            public IDictionary<string, PropertyDetails> Properties { get; set; }
            public IDictionary<MethodInfo, bool> SetMethods { get; set; }
            public bool CascadeEditingEventsToChildren { get; set; }
        }

        /// <summary>
        /// Returns whether property type can potentially implement
        /// <see cref="IChangeTracking"/> or <see cref="IEditableObject"/> interfaces
        /// </summary>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        private static bool CanPropertyInstanceImplementOneOfConcernInterfaces(Type propertyType)
        {
            return !propertyType.IsPrimitive
                   && !(propertyType.IsSealed // this will take care of DateTime, Guid, string and etc.
                        && !typeof(IChangeTracking).IsAssignableFrom(propertyType)
                        && !typeof(IEditableObject).IsAssignableFrom(propertyType));
        }

        private static TypeInfo GetTypeInfo(Type type)
        {
            return TypeInfoCache.GetValue(type, () =>
                                                {

                                                    IEnumerable<PropertyInfo> propertyInfos = type
                                                        .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                                        .Where(p => p.CanWrite && p.CanRead && p.GetIndexParameters().Length == 0);

                                                    var properties = propertyInfos
                                                        .ToDictionary(
                                                            p => p.GetSetMethod(true).Name,
                                                            p => new PropertyDetails
                                                                 {
                                                                     GetMethodInvoker = p.GetGetMethod(true).GetInvoker(),
                                                                     SetMethodInvoker = p.GetSetMethod(true).GetInvoker(),
                                                                     IsIgnoredFromChangeTracking = p.HasAttribute<IgnoreChangeTrackingAttribute>(),
                                                                     CanImplementConcernInterace = CanPropertyInstanceImplementOneOfConcernInterfaces(p.PropertyType)
                                                                 });

                                                    var setMethods = type
                                                        .GetTypeBaseTypesAndInterfaces()
                                                        .SelectMany(t => t.GetProperties())
                                                        .Distinct()
                                                        .Where(p => p.CanWrite)
                                                        .Select(p => new
                                                                     {
                                                                         SetMethod = p.GetSetMethod(),
                                                                         IgnoreChangeTracking = p.HasAttribute<IgnoreChangeTrackingAttribute>()
                                                                     })
                                                        .Where(i => i.SetMethod != null)
                                                        .ToDictionary(i => i.SetMethod, i => !i.IgnoreChangeTracking);

                                                    var cascadeEditingEventsToChildren = type.GetAttribute<EditableObjectAttribute>().IfNotNull(a => a.CascadeEditingEventsToChildren, true);

                                                    return new TypeInfo
                                                           {
                                                               Properties = properties,
                                                               SetMethods = setMethods,
                                                               CascadeEditingEventsToChildren = cascadeEditingEventsToChildren
                                                           };
                                                });
        }

        public void Intercept(IInvocation invocation)
        {
            if (properties == null)
            {
                TypeInfo typeInfo = GetTypeInfo(invocation.Target.GetType());
                properties = typeInfo.Properties;
                setMethods = typeInfo.SetMethods;
                cascadeEditingEventsToChildren = typeInfo.CascadeEditingEventsToChildren;
            }

            switch (invocation.Method.Name)
            {
                case "BeginEdit":
                    BeginEdit(invocation);
                    return;
                case "CancelEdit":
                    CancelEdit(invocation);
                    return;
                case "EndEdit":
                    EndEdit(invocation);
                    return;
                case "BeginNamedEdit":
                    BeginNamedEdit(invocation);
                    return;
                case "CancelNamedEdit":
                    CancelNamedEdit(invocation);
                    return;
                case "EndNamedEdit":
                    EndNamedEdit(invocation);
                    return;
                case "GetOriginalValuesForNamedEdit":
                    GetOriginalValuesForNamedEdit(invocation);
                    return;
                case "get_IsEditing":
                    GetIsEditing(invocation);
                    return;
                case "AcceptChanges":
                    AcceptChanges(invocation);
                    return;
                case "get_IsChanged":
                    if (!GetIsChanged(invocation))
                    {
                        // Call the implementation here to track IsChanged calls separately when tracing
                        invocation.Proceed();
                    }
                    return;
                case "get_CascadeEditingEventsToChildren":
                    GetCascadeEditingEventsToChildren(invocation);
                    return;
                case "set_CascadeEditingEventsToChildren":
                    SetCascadeEditingEventsToChildren(invocation);
                    return;
            }

            HandleSetOperation(invocation);

            invocation.Proceed();
        }

        private void SetCascadeEditingEventsToChildren(IInvocation invocation)
        {
            cascadeEditingEventsToChildren = (bool)invocation.Arguments[0];
        }

        private void GetCascadeEditingEventsToChildren(IInvocation invocation)
        {
            invocation.ReturnValue = cascadeEditingEventsToChildren;
        }

        private void BeginEdit(IInvocation invocation)
        {
            var editName = string.Empty;
            BeginNamedEdit(editName);
            DoCascadeEditingEventsToChildren<IEditableObject>(editName, invocation.Target, i => i.BeginEdit());
        }

        private void CancelEdit(IInvocation invocation)
        {
            var editName = string.Empty;
            DoCascadeEditingEventsToChildren<IEditableObject>(editName, invocation.Target, i => i.CancelEdit());
            CancelNamedEdit(editName, invocation.Target);
        }

        private void EndEdit(IInvocation invocation)
        {
            var editName = string.Empty;
            DoCascadeEditingEventsToChildren<IEditableObject>(editName, invocation.Target, i => i.EndEdit());
            EndNamedEdit(editName);
        }

        private void BeginNamedEdit(IInvocation invocation)
        {
            var editName = (string)invocation.Arguments.First();
            BeginNamedEdit(editName);
            DoCascadeEditingEventsToChildren<IEditableObjectExtended>(editName, invocation.Target, i => i.BeginNamedEdit(editName));
        }

        private void CancelNamedEdit(IInvocation invocation)
        {
            var editName = (string)invocation.Arguments.First();
            DoCascadeEditingEventsToChildren<IEditableObjectExtended>(editName, invocation.Target, i => i.CancelNamedEdit(editName));
            CancelNamedEdit(editName, invocation.Target);
        }

        private void EndNamedEdit(IInvocation invocation)
        {
            var editName = (string)invocation.Arguments.First();
            DoCascadeEditingEventsToChildren<IEditableObjectExtended>(editName, invocation.Target, i => i.EndNamedEdit(editName));
            EndNamedEdit(editName);
        }

        private void GetOriginalValuesForNamedEdit(IInvocation invocation)
        {
            var editName = (string)invocation.Arguments.First();
            var oldValues = GetOldValues(editName);
            invocation.ReturnValue = oldValues;
        }

        private void GetIsEditing(IInvocation invocation)
        {
            invocation.ReturnValue = edits.Count > 0;
        }

        private void AcceptChanges(IInvocation invocation)
        {
            changeTrackingSession.OldValues.Clear();
            if (cascadeEditingEventsToChildren)
            {
                GetChildren<IChangeTracking>(invocation.Target).ForEach(i => i.AcceptChanges());
            }
        }

        private bool GetIsChanged(IInvocation invocation)
        {
            if (invocation.Method.IsMethodImplementationForInterface(typeof(IChangeTracking)))
            {
                invocation.ReturnValue = changeTrackingSession.OldValues.Count > 0
                                         || (cascadeEditingEventsToChildren && GetChildren<IChangeTracking>(invocation.Target).Any(i => i.IsChanged));
                return true;
            }
            return false;
        }

        private void DoCascadeEditingEventsToChildren<TChild>(string editName, object target, Action<TChild> action)
        {
            // Can cascade?
            if (!cascadeEditingEventsToChildren) return;

            // Include old values when cascading event
            var oldValues = edits.ContainsKey(editName)
                ? edits[editName].OldValues.Values
                : EmptyObjectCollection;

            // Find all editable values and execute action
            var editables = properties.Values
                .Where(p => !p.IsIgnoredFromChangeTracking && p.CanImplementConcernInterace) // Optimization
                .Select(p => p.GetMethodInvoker(target))
                .OfType<TChild>()
                .Concat(oldValues.OfType<TChild>()); // Not doing distinct as performance optimization

            editables.ForEach(action);
        }

        private IEnumerable<T> GetChildren<T>(object target)
        {
            return properties.Values
                .Where(p => !p.IsIgnoredFromChangeTracking) // Ensure property is included in change tracking
                .Select(p => p.GetMethodInvoker(target))
                .OfType<T>();
        }

        private void HandleSetOperation(IInvocation invocation)
        {
            if (SuppressChangeTrackingScope.Current != null) return;

            // if this is a method we are supposed to track changes for
            if (setMethods.ContainsKey(invocation.Method))
            {
                // check if the property has the IgnoreChangeTrackingAttribute (and is thus included for change tracking)...if it is not, skip tracking this set operation.
                if (!setMethods[invocation.Method]) return;

                PropertyDetails invokers;
                // get the property for this setter
                if (properties.TryGetValue(invocation.Method.Name, out invokers))
                {
                    // get the current value on the object
                    var currentValue = invokers.GetMethodInvoker(invocation.Target);
                    var newValue = invocation.Arguments[0];
                    // if current value on the object is not equal to the new value being set
                    if (!Equals(currentValue, newValue))
                    {
                        // Create rollback information for every pending edit (+change tracking)
                        lock (edits)
                        {
                            foreach (var cancellationData in edits.Values.Union(new[] { changeTrackingSession }))
                            {
                                var oldValues = cancellationData.OldValues;

                                // if we haven't already stored an old value for the property, store the current value (pre-change) in the old values dictionary 
                                if (!oldValues.ContainsKey(invokers.SetMethodInvoker))
                                {
                                    oldValues[invokers.SetMethodInvoker] = currentValue;
                                }
                                    // if we have the old value and the current value being set is the same as the original, remove that old value from the old values dictionary.
                                else if (Equals(oldValues[invokers.SetMethodInvoker], newValue))
                                {
                                    oldValues.Remove(invokers.SetMethodInvoker);
                                }
                            }
                        }
                    }
                }
            }
        }

        #region ITypeInterfacesConcern Members

        public IEnumerable<Type> Interfaces
        {
            get { yield return typeof(IEditableObjectExtended); }
        }

        #endregion

        public void BeginNamedEdit(string editName)
        {
            lock (edits)
            {
                // Editing?
                if (edits.ContainsKey(editName)) return;

                // Start tracking state
                var editingData = new EditCancellationData();
                edits.Add(editName, editingData);
            }
        }

        public void CancelNamedEdit(string editName, object target)
        {
            lock (edits)
            {
                // Not editing?
                if (!edits.ContainsKey(editName)) return;

                // Get editing information
                // IMPORTANT: current editing data must be removed from edits before setting back values
                var editingData = edits[editName];
                edits.Remove(editName);

                // Restore values
                foreach (var invokerPair in editingData.OldValues)
                {
                    invokerPair.Key(target, invokerPair.Value);
                }
            }
        }

        public void EndNamedEdit(string editName)
        {
            lock (edits)
            {
                // Not editing?
                if (!edits.ContainsKey(editName)) return;

                // Abandon edit
                edits.Remove(editName);
            }
        }

        private Dictionary<string, object> GetOldValues(string editName)
        {
            lock (edits)
            {
                if (!edits.ContainsKey(editName)) return null;

                var editingData = edits[editName];

                var oldValues = new Dictionary<string, object>();
                foreach (var kvp in editingData.OldValues)
                {
                    // try to get the property name
                    var prop = properties.FirstOrDefault(x => x.Value.SetMethodInvoker == kvp.Key);
                    oldValues.Add(prop.Key.Substring(4), kvp.Value);
                }

                return oldValues;
            }
        }
    }
}