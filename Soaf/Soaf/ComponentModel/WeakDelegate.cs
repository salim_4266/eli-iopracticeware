using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using Soaf.Collections;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// Class that allows "weak reference delegates" that point to private methods to be called via late binding.
    /// </summary>
    public static class Weakness
    {
        public static void InvokeDelegate(object action, object target, object invoker, object args)
        {
            var d = (Delegate)action;
            d.DynamicInvoke(target, invoker, args);
        }
    }

    public static class WeakDelegate
    {
        #region Statics

        private static readonly ModuleBuilder DynamicModule;

        /// <summary>
        ///   Static reference, used by MethodBoundTemplate.DefineInvoke
        /// </summary>
        private static readonly MethodInfo WeakReferenceTargetGetter =
            typeof(WeakReference).GetProperty("Target").GetGetMethod();

        /// <summary>
        ///   Static reference, used by MethodBoundTemplate.DefineInvoke
        /// </summary>
        private static readonly MethodInfo DisposalInfoIsDisposedGetter =
            typeof(IDisposalInfo).GetProperty("IsDisposed").GetGetMethod();


        static WeakDelegate()
        {
            var assemblyName = new AssemblyName("Soaf.Weakness");
            assemblyName.SetPublicKey(Convert.FromBase64String(WellKnownAssemblies.SoafPublicKey));

            AssemblyBuilder dynamicAssembly = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName,
                AssemblyBuilderAccess.Run);
            DynamicModule = dynamicAssembly.DefineDynamicModule("WeakDelegates");
        }

        #endregion

        #region MakeWeak extension methods and overloads

        /// <summary>
        /// Returns a weak-reference version of a delegate
        /// </summary>
        /// <param name = "aDelegate">The delegate to convert to weak referencing</param>
        /// <returns>A new, weak referencing delegate</returns>
        public static TDelegate MakeWeak<TDelegate>(this Delegate aDelegate)
            where TDelegate : class
        {
            return MethodBoundTemplate<TDelegate>.CreateWeak(aDelegate as TDelegate, null);
        }

        /// <summary>
        ///   Returns a weak-reference version of a delegate
        /// </summary>
        /// <param name = "aDelegate">The delegate to convert to weak referencing</param>
        /// <param name = "fnUnregister">The unregister action to invoke if the target is garbage collected</param>
        /// <returns>A new, weak referencing delegate</returns>
        public static TDelegate MakeWeak<TDelegate>(this Delegate aDelegate, Action<TDelegate> fnUnregister)
            where TDelegate : class
        {
            return MethodBoundTemplate<TDelegate>.CreateWeak(aDelegate as TDelegate, fnUnregister);
        }

        /// <summary>
        /// Returns a weak-reference version of an event handler
        /// </summary>
        /// <param name = "aDelegate">The delegate to convert to weak referencing</param>
        /// <returns>A new, weak referencing delegate</returns>
        public static EventHandler<T> MakeWeak<T>(this EventHandler<T> aDelegate)
            where T : EventArgs
        {
            return MethodBoundTemplate<EventHandler<T>>.CreateWeak(aDelegate, null);
        }

        /// <summary>
        ///   Returns a weak-reference version of a event handler
        /// </summary>
        /// <param name = "handler">The delegate to convert to weak referencing</param>
        /// <param name = "fnUnregister">The unregister action to invoke if the target is garbage collected</param>
        /// <returns>A new, weak referencing delegate</returns>
        public static EventHandler<T> MakeWeak<T>(this EventHandler<T> handler, Action<EventHandler<T>> fnUnregister)
            where T : EventArgs
        {
            return MethodBoundTemplate<EventHandler<T>>.CreateWeak(handler, fnUnregister);
        }

        /// <summary>
        /// Returns a weak-reference version of a event handler
        /// </summary>
        /// <param name="handler">The delegate to convert to weak referencing</param>
        /// <param name="fnUnregister">The unregister action to invoke if the target is garbage collected</param>
        /// <returns>
        /// A new, weak referencing delegate
        /// </returns>
        public static EventHandler MakeWeak(this EventHandler handler, Action<EventHandler> fnUnregister)
        {
            return MethodBoundTemplate<EventHandler>.CreateWeak(handler, fnUnregister);
        }

        /// <summary>
        /// Returns a weak-reference version of a event handler
        /// </summary>
        /// <param name="handler">The delegate to convert to weak referencing</param>
        /// <returns>
        /// A new, weak referencing delegate
        /// </returns>
        public static EventHandler MakeWeak(this EventHandler handler)
        {
            return MethodBoundTemplate<EventHandler>.CreateWeak(handler, null);
        }

        /// <summary>
        ///   Returns a weak-reference version of a PropertyChangedEventHandler
        /// </summary>
        /// <param name = "handler">The delegate to convert to weak referencing</param>
        /// <param name = "fnUnregister">The unregister action to invoke if the target is garbage collected</param>
        /// <returns>A new, weak referencing delegate</returns>
        public static PropertyChangedEventHandler MakeWeak(this PropertyChangedEventHandler handler, Action<PropertyChangedEventHandler> fnUnregister)
        {
            return MethodBoundTemplate<PropertyChangedEventHandler>.CreateWeak(handler, fnUnregister);
        }

        /// <summary>
        /// Returns a weak-reference version of a PropertyChangedEventHandler
        /// </summary>
        /// <param name = "handler">The delegate to convert to weak referencing</param>
        /// <returns>A new, weak referencing delegate</returns>
        public static PropertyChangedEventHandler MakeWeak(this PropertyChangedEventHandler handler)
        {
            return MethodBoundTemplate<PropertyChangedEventHandler>.CreateWeak(handler, null);
        }

        /// <summary>
        ///   Returns a weak-reference version of a delegate
        /// </summary>
        /// <param name = "aDelegate">The delegate to convert to weak referencing</param>
        /// <returns>A new, weak referencing delegate</returns>
        public static TDelegate MakeWeak<TDelegate>(TDelegate aDelegate)
            where TDelegate : class
        {
            return MethodBoundTemplate<TDelegate>.CreateWeak(aDelegate, null);
        }

        /// <summary>
        ///   Returns a weak-reference version of a delegate
        /// </summary>
        /// <param name = "aDelegate">The delegate to convert to weak referencing</param>
        /// <param name = "fnUnregister">The unregister action to invoke if the target is garbage collected</param>
        /// <returns>A new, weak referencing delegate</returns>
        public static TDelegate MakeWeak<TDelegate>(TDelegate aDelegate, Action<TDelegate> fnUnregister)
            where TDelegate : class
        {
            return MethodBoundTemplate<TDelegate>.CreateWeak(aDelegate, fnUnregister);
        }

        #endregion

        #region Remove

        /// <summary>
        /// Removes the specified value Delegate from the source if it's weak. Standard Delegate.Remove is called too in case it's not weak.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static Delegate Remove(Delegate source, Delegate value)
        {
            if (source == null || value == null) return source;

            Delegate result = source;

            foreach (var sourceDelegate in source.GetInvocationList())
            {
                MethodInfo sourceMethod;
                object sourceTarget;

                TryUnwrapIfWeak(sourceDelegate, out sourceMethod, out sourceTarget);

                foreach (var toRemove in value.GetInvocationList())
                {
                    MethodInfo valueMethod;
                    object valueTarget;

                    TryUnwrapIfWeak(toRemove, out valueMethod, out valueTarget);

                    if (sourceMethod == valueMethod && sourceTarget == valueTarget)
                    {
                        // do a favor and clean up this dead WeakDelegate
                        result = Delegate.Remove(source, sourceDelegate);
                    }
                }
            }

            // regular delegate removal
            return Delegate.Remove(result, value);
        }

        /// <summary>
        /// Unwraps the delegate to its underlying Method/Target Delegate if it's a WeakDelegate with a target.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="method">The method.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        private static void TryUnwrapIfWeak(Delegate d, out MethodInfo method, out object target)
        {
            method = d.Method;
            target = d.Target;

            var wd = d.Target as DelegateWeakReference;

            if (wd != null)
            {
                method = ((DelegateWeakReference)d.Target).Delegate.Method;

                // is this a non-multicast version of a WeakDelegate class (e.g. has a target)
                if (wd.IsAlive)
                {
                    target = wd.Target;
                }
            }
        }

        #endregion

        #region Nested type: DelegateConverter

        /// <summary>
        ///   Defines the delegate for converting a delegate to it's weak referenced counterpart
        /// </summary>
        /// <param name = "original">The original delegate, to be converted</param>
        /// <returns>A weak referencing delegate which does not keep it's target alive.</returns>
        [CompilerGenerated]
        private delegate TDelegate DelegateConverter<TDelegate>(TDelegate original);

        #endregion

        #region Nested type: DelegateWeakReference

        /// <summary>
        /// A base class for dynamically generated WeakDelegate. Allows to get access to open delegate and target weak reference
        /// </summary>
        public abstract class DelegateWeakReference : WeakReference
        {
            protected DelegateWeakReference(object target)
                : base(target)
            {
            }

            public abstract Delegate Delegate { get; }
        }

        #endregion

        #region Nested type: WeakDelegateTemplate

        /// <summary>
        /// A template for dynamically generated WeakDelegate-s allowing to minimize manual IL code generation
        /// </summary>
        public abstract class WeakDelegateTemplate<TDelegate> : DelegateWeakReference
            where TDelegate : class
        {
            /// <summary>
            /// Using List` and not HashSet, because we want unsubscribe to called as many times as it has been passed in
            /// </summary>
            private readonly List<Action<TDelegate>> unsubscribeCallbacks;

            // ReSharper disable PublicConstructorInAbstractClass
            public WeakDelegateTemplate(object target)
                // ReSharper restore PublicConstructorInAbstractClass
                : base(target)
            {
                unsubscribeCallbacks = new List<Action<TDelegate>>();
            }

            public void AddUnsubscribeCallback(Action<TDelegate> unsubscribeCallback)
            {
                if (unsubscribeCallback != null)
                {
                    // Add callback to the list
                    unsubscribeCallbacks.Add(unsubscribeCallback);
                }
            }

            public void ExecuteAllUnsubscribeCallbacks(TDelegate weakDelegate)
            {
                // Execute each callback and clear
                unsubscribeCallbacks.ForEach(callback => callback(weakDelegate));
                unsubscribeCallbacks.Clear();
            }
        }

        #endregion

        #region Nested type: MethodBoundTemplate

        /// <summary>
        ///   Encapsulates the logic for a converter which converts a MethodInfo bound delegate into a weak referencing version of itself
        /// </summary>
        /// <typeparam name = "TDelegate">The type of delegate to be converted</typeparam>
        [CompilerGenerated]
        private class MethodBoundTemplate<TDelegate>
            where TDelegate : class
        {
            #region Nested Types

            #region Nested type: InnerConverter

            /// <summary>
            ///   Defines the signature for the IL-generated delegate converter method
            /// </summary>
            /// <param name = "original">The original delegate</param>
            /// <param name = "openDlegate">The type to be used as an open version of TDelegate</param>
            /// <returns>A weak referencing delegate</returns>
            [CompilerGenerated]
            private delegate TDelegate InnerConverter(TDelegate original, Delegate openDlegate);

            #endregion

            #region Nested type: WeakDelegateBuilder

            /// <summary>
            ///   Encapsulates construction of new type to implement weak referencing
            /// </summary>
            /// <remarks>
            ///   The type produced by this builder is equivalent to the following:
            /// 
            ///   public class WeakDelegate : WeakDelegateTemplate
            ///   {
            ///      OpenDelegate _openDelegate;
            /// 
            ///      public WeakDelegate(TDelegate aDelegate) : base(aDelegate.Target)
            ///      {
            ///         _openDelegate = aDelegate;
            ///      }
            /// 
            ///      public void Invoke (Arg1 a, Arg2 b, ...)    --- signature matches TDelegate's signature
            ///      {
            ///          var target = this._Target;
            ///          if(target != null)
            ///          {
            ///             var asDisposalInfo = target as DisposalInfo;
            ///             if(asDisposalInfo != null &amp;&amp; !asDisposalInfo.IsDisposed)
            ///             {
            ///                 _openDelegate(target, a, b, ...);
            ///                 return;
            ///             }
            ///          }
            ///          this.ExecuteAllUnsubscribeCallbacks(new TDelegate(this.Invoke)) 
            ///      }
            ///   }
            /// </remarks>
            [CompilerGenerated]
            private class WeakDelegateBuilder
            {
                private readonly TypeBuilder classBuilder;
                private readonly FieldBuilder delegateField;
                private readonly MethodInfo method;
                private readonly Type weakType;
                private readonly Type baseType;

                public InnerConverter Inner;
                public Delegate OpenDelegate;
                private Type openDelegateType;

                public WeakDelegateBuilder(MethodInfo method)
                {
                    this.method = method;
                    baseType = typeof(WeakDelegateTemplate<TDelegate>);
                    classBuilder =
                        DynamicModule.DefineType(string.Format("{0}.{1}", method.DeclaringType.EnsureNotDefault().FullName, method.Name),
                            TypeAttributes.Class | TypeAttributes.Public,
                            baseType);
                    InitializeOpenDelegate();

                    delegateField = classBuilder.DefineField("_delegate", openDelegateType, FieldAttributes.Public);

                    ImplementDelegateWeakReference();

                    DefineConstructor();
                    DefineInvoke();

                    weakType = classBuilder.CreateType();

                    DefineConverter();
                }

                private void ImplementDelegateWeakReference()
                {
                    var pb = classBuilder.DefineProperty("Delegate", PropertyAttributes.None, typeof(Delegate), new Type[0]);

                    const MethodAttributes methodAttributes = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.ReuseSlot | MethodAttributes.HideBySig | MethodAttributes.Virtual;
                    var getMethod = classBuilder.DefineMethod("get_Delegate", methodAttributes, typeof(Delegate), new Type[0]);

                    var il = getMethod.GetILGenerator();
                    il.Emit(OpCodes.Ldarg_0);
                    il.Emit(OpCodes.Ldfld, delegateField);
                    il.Emit(OpCodes.Castclass, typeof(Delegate));
                    il.Emit(OpCodes.Ret);

                    pb.SetGetMethod(getMethod);
                }

                /// <summary>
                ///   Produces a new delegate type which is signature compatible with an open TDelegate
                /// </summary>
                private void InitializeOpenDelegate()
                {
                    var argTypes = new Type[DelgParamTypes.Length + 1];
                    argTypes[0] = method.DeclaringType;
                    DelgParamTypes.CopyTo(argTypes, 1);

                    openDelegateType = BuildDelegateType(argTypes);

                    OpenDelegate = Delegate.CreateDelegate(openDelegateType, null, method, true);
                }

                /// <summary>
                /// Builds a custom delegate type. Previously used generic Action, but that doesn't support ByRef parameter types.
                /// </summary>
                /// <param name="argTypes"></param>
                private Type BuildDelegateType(Type[] argTypes)
                {
                    var name = "Delegate{0}".FormatWith(Guid.NewGuid().ToString("N"));

                    var typeBuilder = DynamicModule.DefineType(name, TypeAttributes.Class | TypeAttributes.Public | TypeAttributes.Sealed | TypeAttributes.AnsiClass | TypeAttributes.AutoClass, typeof(MulticastDelegate));
                    ConstructorBuilder constructorBuilder = typeBuilder.DefineConstructor(MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public, CallingConventions.Standard, new[] { typeof(object), typeof(IntPtr) });
                    constructorBuilder.SetImplementationFlags(MethodImplAttributes.CodeTypeMask);

                    var methodBuilder = typeBuilder.DefineMethod("Invoke", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual, typeof(void), argTypes);
                    methodBuilder.SetImplementationFlags(MethodImplAttributes.CodeTypeMask);

                    Type t = typeBuilder.CreateType();

                    return t;
                }

                /// <summary>
                ///   Emits the constructor for the new type.
                /// </summary>
                private void DefineConstructor()
                {
                    var weakTypeCtorArgs = new[] { typeof(TDelegate), openDelegateType };
                    var weakTypeCtor = classBuilder.DefineConstructor(MethodAttributes.Public,
                        CallingConventions.Standard,
                        weakTypeCtorArgs);
                    var il = weakTypeCtor.GetILGenerator();
                    var ilTemp = il.DeclareLocal(typeof(object));

                    // construct (TDelegate delegate, OpenDelegateType aDelegate) : base (delegate.Target)
                    // {                  
                    il.Emit(OpCodes.Ldarg_1); // delegate
                    il.Emit(OpCodes.Call, typeof(TDelegate).GetProperty("Target").GetGetMethod()); // .Target
                    il.Emit(OpCodes.Stloc, ilTemp);

                    il.Emit(OpCodes.Ldarg_0); // must pass this to base()
                    il.Emit(OpCodes.Ldloc, ilTemp);
                    il.Emit(OpCodes.Call, baseType.GetConstructor(new[] { typeof(object) }).EnsureNotDefault());

                    //  this._delegate = aDelegate
                    il.Emit(OpCodes.Ldarg_0);
                    il.Emit(OpCodes.Ldarg_2);
                    il.Emit(OpCodes.Stfld, delegateField);
                    // }
                    il.Emit(OpCodes.Ret);

                }

                /// <summary>
                ///   Emits the Invoke method for the new type
                /// </summary>
                private void DefineInvoke()
                {
                    #region Preamble

                    var invokeMethod = classBuilder.DefineMethod("Invoke", MethodAttributes.Public,
                        DelgSignature.ReturnType, DelgParamTypes);
                    var il = invokeMethod.GetILGenerator();
                    var ilTarget = il.DeclareLocal(typeof(object));
                    var ilAsDisposalInfo = il.DeclareLocal(typeof(IDisposalInfo));
                    var ilEndLabel = il.DefineLabel();
                    var ilIsNullLabel = il.DefineLabel();
                    var ilTargetOkLabel = il.DefineLabel();

                    #endregion


                    // var target = this.Target
                    il.Emit(OpCodes.Ldarg_0);
                    il.Emit(OpCodes.Callvirt, WeakReferenceTargetGetter); // this.Target
                    il.Emit(OpCodes.Stloc, ilTarget); // store in local variable

                    // if(target != null)
                    // {
                    il.Emit(OpCodes.Ldloc, ilTarget);
                    il.Emit(OpCodes.Brfalse, ilIsNullLabel);

                    // var disp = target as IDisposalInfo
                    il.Emit(OpCodes.Ldloc, ilTarget);
                    il.Emit(OpCodes.Isinst, typeof(IDisposalInfo));
                    il.Emit(OpCodes.Stloc, ilAsDisposalInfo);

                    // if(disp == null ) goto: TargetOK
                    il.Emit(OpCodes.Ldloc, ilAsDisposalInfo);
                    il.Emit(OpCodes.Brfalse_S, ilTargetOkLabel);

                    // if(disp.IsDisposed) goto: IsNullLabel
                    il.Emit(OpCodes.Ldloc, ilAsDisposalInfo);
                    il.Emit(OpCodes.Callvirt, DisposalInfoIsDisposedGetter);
                    il.Emit(OpCodes.Brtrue_S, ilIsNullLabel);

                    // TargetOK:
                    il.MarkLabel(ilTargetOkLabel);

                    //      _delegate( @target, parm1, parm2 ...);
                    il.Emit(OpCodes.Ldarg_0);
                    il.Emit(OpCodes.Ldfld, delegateField);

                    il.Emit(OpCodes.Ldloc, ilTarget);

                    for (short i = 0; i < DelgParams.Length; i++)
                    {
                        il.Emit(OpCodes.Ldarg, i + 1);
                        if (DelgParams[i].ParameterType.IsValueType)
                        {
                            il.Emit(OpCodes.Box, DelgParams[i].ParameterType);
                        }
                    }
                    il.Emit(OpCodes.Call, typeof(Weakness).GetMethod("InvokeDelegate"));
                    il.Emit(OpCodes.Ret);

                    // } else
                    // ilIsNullLabel:
                    il.MarkLabel(ilIsNullLabel);

                    // this.ExecuteAllUnsubscribeCallbacks(
                    il.Emit(OpCodes.Ldarg_0);

                    // new TDelegate(this.Invoke)
                    il.Emit(OpCodes.Ldarg_0);
                    il.Emit(OpCodes.Ldftn, invokeMethod);
                    il.Emit(OpCodes.Newobj, DelegateConstructor);

                    // )
                    il.Emit(OpCodes.Call, baseType.GetMethod("ExecuteAllUnsubscribeCallbacks"));

                    // }
                    il.MarkLabel(ilEndLabel);

                    il.Emit(OpCodes.Ret);
                }

                /// <summary>
                ///   Produces a delegate which constructs a new weak referencing type
                /// </summary>
                private void DefineConverter()
                {
                    var args = new[] { typeof(TDelegate), typeof(Delegate) };
                    var dynamicMethod = new DynamicMethod("Construct", typeof(TDelegate), args, typeof(Weakness), true);

                    var weakTypeCtorArgs = new[] { typeof(TDelegate), openDelegateType };
                    var weakTypeCtor = weakType.GetConstructor(weakTypeCtorArgs).EnsureNotDefault();

                    var il = dynamicMethod.GetILGenerator();

                    //  TDelegate Construct(TDelegate delegate, _openDelegate)
                    //  {
                    //      ilThis = new WeakDelegate(delegate,  _openDelegate) 
                    il.Emit(OpCodes.Ldarg_0); // push delegate
                    il.Emit(OpCodes.Ldarg_1); // push _openDelegate
                    il.Emit(OpCodes.Castclass, openDelegateType); // .. as OpenDelegateType
                    il.Emit(OpCodes.Newobj, weakTypeCtor); // new object on stack

                    //      return new TDelegate( ilThis, Invoke );
                    //  }
                    il.Emit(OpCodes.Ldftn, weakType.GetMethod("Invoke")); // add invoke arg
                    il.Emit(OpCodes.Newobj, DelegateConstructor);
                    il.Emit(OpCodes.Ret);

                    Inner = (InnerConverter)dynamicMethod.CreateDelegate(typeof(InnerConverter));
                }
            }

            #endregion

            #endregion


            #region Fields

            /// <summary>
            ///   Dictionary of all method-bound templates for this delegate.
            /// </summary>
            private static readonly IDictionary<MethodInfo, DelegateConverter<TDelegate>> Templates =
                new Dictionary<MethodInfo, DelegateConverter<TDelegate>>();

            /// <summary>
            ///  Dictionary of existing weak delegates to instance methods.
            /// </summary>
            private static readonly ConditionalWeakTable<object, Dictionary<MethodInfo, TDelegate>> BuiltWeakDelegates =
                new ConditionalWeakTable<object, Dictionary<MethodInfo, TDelegate>>();
            private static readonly object WeakDelegateCreationLock = new object();

            private static readonly MethodInfo DelgSignature = typeof(TDelegate).GetMethod("Invoke");
            private static readonly ParameterInfo[] DelgParams = DelgSignature.GetParameters();
            private static readonly Type[] DelgParamTypes = DelgParams.Select(p => p.ParameterType).ToArray();

            private static readonly ConstructorInfo DelegateConstructor =
                typeof(TDelegate).GetConstructor(new[] { typeof(object), typeof(IntPtr) }) ??
                typeof(TDelegate).GetConstructor(new[] { typeof(object), typeof(UIntPtr) });

            private static bool isDelegateVerified;

            private readonly InnerConverter innerConverter;
            private readonly Delegate openDelegate;

            #endregion

            #region Methods


            /// <summary>
            ///   Returns a weak-reference version of a delegate
            /// </summary>
            /// <param name = "aDelegate">The delegate to convert to weak referencing</param>
            /// <param name = "fnUnregister">The unregister action to invoke if the target is garbage collected</param>
            /// <returns>A new, weak referencing delegate</returns>
            public static TDelegate CreateWeak(TDelegate aDelegate, Action<TDelegate> fnUnregister)
            {
                var asDelegate = aDelegate as MulticastDelegate;

                if (asDelegate == null)
                    throw new ArgumentException(@"TDelegate must be a delegate type");

                var target = asDelegate.Target;

                // if delegate is Static return delegate. Is delegate already weak? return delegate
                if (target == null || target is WeakReference)
                    return aDelegate;

                if (!isDelegateVerified)
                {
                    if (DelgParams.Any(p => p.IsOptional))
                        throw new ArgumentException(@"Delegates with ref (ByRef) out out arguments are not supported by the weak reference pattern.");

                    if (DelgSignature.ReturnType != typeof(void))
                        throw new ArgumentException(@"Delegates with return values are not supported by the weak reference pattern.");

                    isDelegateVerified = true;
                }

                // Default Delegate-s equality behavior, using Equals & GetHashCode, is to ensure Target & Method match
                // When using WeakDelegates, Target is WeakReference to actual Target and such references will not be equal when compared.
                // This leads to issues when unsubscribing WeakDelegates from events, since weak delegate instance, created upon subscription, needs to be kept around
                // We try solving this problem by re-using weak delegates as long as original delegate's target alive
                lock (WeakDelegateCreationLock)
                {
                    Dictionary<MethodInfo, TDelegate> existingDelegates;
                    if (!BuiltWeakDelegates.TryGetValue(target, out existingDelegates))
                    {
                        existingDelegates = new Dictionary<MethodInfo, TDelegate>();
                        BuiltWeakDelegates.Add(target, existingDelegates);
                    }

                    TDelegate wDelegate;
                    if (!existingDelegates.TryGetValue(asDelegate.Method, out wDelegate))
                    {
                        // Create delegate with weak reference to target
                        var converter = Templates.GetValue(asDelegate.Method,
                            () => (new MethodBoundTemplate<TDelegate>(asDelegate.Method)).ConvertDelegateToWeak);
                        wDelegate = converter(aDelegate);

                        // Cache it
                        existingDelegates.Add(asDelegate.Method, wDelegate);
                    }

                    // Append unsubscribe callback to the WeakDelegate
                    var weakDelegateRef = wDelegate.As<Delegate>().Target.As<WeakDelegateTemplate<TDelegate>>();
                    weakDelegateRef.AddUnsubscribeCallback(fnUnregister);

                    return wDelegate;
                }
            }

            /// <summary>
            ///   Constructs a new instance, bound to specific MethodInfo
            /// </summary>
            /// <param name = "method">The delegate's underlying method</param>
            private MethodBoundTemplate(MethodInfo method)
            {
                var args = new WeakDelegateBuilder(method);
                innerConverter = args.Inner;
                openDelegate = args.OpenDelegate;
            }

            /// <summary>
            ///   Returns a weak-reference version of a delegate
            /// </summary>
            /// <param name = "original">The delegate to convert to weak referencing</param>
            /// <returns>A weak referencing delegate</returns>
            private TDelegate ConvertDelegateToWeak(TDelegate original)
            {
                return innerConverter(original, openDelegate);
            }

            #endregion
        }

        #endregion

    }
}