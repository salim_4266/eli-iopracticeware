using System;
using System.Reflection;
using Soaf.ComponentModel;
using Soaf.Reflection;

[assembly: Component(typeof(ReadOnlyBehavior))]

namespace Soaf.ComponentModel
{
    /// <summary>
    /// Defines a type that supports making its members read-only.
    /// </summary>
    [ReadOnly]
    public interface IIsReadOnly
    {
        bool IsReadOnly { get; }
    }

    /// <summary>
    /// Attribute to support IIsReadOnly behavior
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    internal class ReadOnlyAttribute : ConcernAttribute
    {
        public ReadOnlyAttribute()
            : base(typeof(ReadOnlyBehavior))
        {
        }
    }

    /// <summary>
    /// Behavior to support IIsReadOnly.
    /// </summary>
    internal class ReadOnlyBehavior : IInterceptor, IInterceptedMethodsFilter
    {
        public void Intercept(IInvocation invocation)
        {
            IIsReadOnly isReadOnly = invocation.Target.As<IIsReadOnly>().EnsureNotDefault(new InvalidOperationException("Instance should implement IIsReadOnly."));
            if (isReadOnly.IsReadOnly)
            {
                throw new InvalidOperationException("Instance is read only. Cannot set property {0}.".FormatWith(invocation.Method.Name.Substring(4)));
            }
            invocation.Proceed();
        }

        public bool ShouldIntercept(Type type, MethodInfo method)
        {
            return method.CanBeOveridden() && method.GetProperty().IfNotDefault(p => p.CanWrite && p.GetSetMethod() == method);
        }
    }
}