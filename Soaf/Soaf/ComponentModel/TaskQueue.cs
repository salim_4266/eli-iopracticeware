using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Threading;
using System.Transactions;
using System.Windows.Threading;
using Soaf.Collections;
using Soaf.Threading;

namespace Soaf.ComponentModel
{
    /// <summary>
    /// Allows tasks to be queued and executed serially.
    /// </summary>
    public class SerializedTaskQueue : TaskQueue
    {
        public SerializedTaskQueue() : base(1, 1) { }
        public SerializedTaskQueue(int queuedTaskSize) : base(1, queuedTaskSize) { }
    }

    /// <summary>
    /// Allows tasks to be queued and is able to limit the number of concurrent running tasks.
    /// </summary>
    /// <remarks>
    /// If the ActiveTaskSize is met, then any additional tasks are enqueued.
    /// If the QueuedTaskSize is met, then the oldest task is dequeued, and the newest task is enqueued.
    /// </remarks>
    public class TaskQueue : INotifyPropertyChanged
    {
        private readonly SynchronizationContext synchronizationContext;
        private readonly System.Threading.Tasks.TaskScheduler onCompleteScheduler;
        private readonly System.Threading.Tasks.TaskScheduler scheduler;
        private readonly ConcurrentDictionary<CancellationTokenSource, bool> cancelTokens = new ConcurrentDictionary<CancellationTokenSource, bool>();
        private bool isBusy;
        private bool isDisabled;

        public TaskQueue(int activeTaskSize, int? queuedTaskSize = null, SynchronizationContext synchronizationContext = null)
        {
            if (activeTaskSize <= 0) throw new ArgumentException("ActiveTaskSize must be greater then 0");
            if (queuedTaskSize <= 0) throw new ArgumentException("QueuedTaskSize must be greater then 0");

            ActiveTaskSize = activeTaskSize;
            QueuedTaskSize = queuedTaskSize;

            if (synchronizationContext == null)
            {
                if (System.Windows.Application.Current != null)
                {
                    SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(System.Windows.Application.Current.Dispatcher));    
                }
                else if (SynchronizationContext.Current == null)
                {
                    SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
                }
                synchronizationContext = SynchronizationContext.Current;
            }

            this.synchronizationContext = synchronizationContext;

            var last = SynchronizationContext.Current;
            SynchronizationContext.SetSynchronizationContext(synchronizationContext);
            onCompleteScheduler = System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(last);

            scheduler = new LimitedConcurrencyLevelTaskScheduler(activeTaskSize, queuedTaskSize);
        }

        public virtual void CancelAll()
        {
            synchronizationContext.Send(i => CancelAllInternal(), null);
        }

        private void CancelAllInternal()
        {
            cancelTokens.Keys.ForEach(ct => ct.Cancel());
        }

        /// <summary>
        /// Loads a new task.  If there is a spot available, the task will run immediately.  Otherwise it is queued.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="onComplete">The on complete action</param>
        /// <remarks>
        /// If the Queue is full, the oldest item is dequeued, and the new task is enqueued.
        /// </remarks>
        /// <returns>
        /// Returns a token that allows the enqueued action to be cancelled.
        /// </returns>
        public virtual CancellationTokenSource Enqueue<T>(Func<T> action, Action<T> onComplete = null)
        {
            CancellationTokenSource result = null;
            synchronizationContext.Send(i => result = EnqueueInternal(action, onComplete), null);
            return result;
        }

        private CancellationTokenSource EnqueueInternal<T>(Func<T> action, Action<T> onComplete)
        {
            if (IsDisabled) return null;

            IsBusy = true;

            var cancelToken = new CancellationTokenSource();
            cancelTokens[cancelToken] = true;

            var lastTransaction = Transaction.Current;
            if (Transaction != null) Transaction.Current = Transaction;
            try
            {
                System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => !cancelToken.IsCancellationRequested ? action() : default(T), cancelToken.Token, System.Threading.Tasks.TaskCreationOptions.None, scheduler).
                    ContinueWith(t =>
                    {
                        using (new DependentTransactionScope(Transaction, true))
                        {
                            bool ctsValue;
                            if (!cancelTokens.TryRemove(cancelToken, out ctsValue))
                            {
                                throw new InvalidOperationException("Expected cancellation token was not found.");
                            }

                            if (!cancelToken.IsCancellationRequested && !t.IsCanceled && onComplete != null)
                            {
                                onComplete(t.Result);
                            }
                        }
                        if (cancelTokens.IsEmpty)
                        {
                            IsBusy = false;
                        }
                    }, onCompleteScheduler ?? scheduler);
                // we don't pass CancellationToken to ContinueWith because it will prevent ContinueWith from being called if Cancel is called before task completes.
                // http://stackoverflow.com/questions/6997480/task-continuewith-not-working-how-i-expected
            }
            finally
            {
                if (Transaction != null) Transaction.Current = lastTransaction;
            }

            return cancelToken;
        }

        /// <summary>
        /// Loads a new task.  If there is a spot available, the task will run immediately.  Otherwise it is queued.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="onComplete">The on complete action</param>
        /// <remarks>
        /// If the Queue is full, the oldest item is dequeued, and the new task is enqueued.
        /// </remarks>
        /// <returns>
        /// Returns a token that allows the enqueued action to be cancelled.
        /// </returns>
        public virtual CancellationTokenSource Enqueue(Action action, Action onComplete = null)
        {
            CancellationTokenSource result = null;
            synchronizationContext.Send(i => result = EnqueueInternal(action, onComplete), null);
            return result;
        }

        private CancellationTokenSource EnqueueInternal(Action action, Action onComplete)
        {
            if (IsDisabled) return null;

            IsBusy = true;

            var cancelToken = new CancellationTokenSource();
            cancelTokens[cancelToken] = true;

            var lastTransaction = Transaction.Current;
            if (Transaction != null) Transaction.Current = Transaction;
            try
            {
                System.Threading.Tasks.Task.Factory.StartNewWithCurrentTransaction(() => { if (!cancelToken.IsCancellationRequested) action(); }, cancelToken.Token, System.Threading.Tasks.TaskCreationOptions.None, scheduler).
                    ContinueWith(t =>
                    {
                        using (new DependentTransactionScope(Transaction, true))
                        {
                            bool ctsValue;
                            if (!cancelTokens.TryRemove(cancelToken, out ctsValue))
                            {
                                throw new InvalidOperationException("Expected cancellation token was not found.");
                            }

                            if (!cancelToken.IsCancellationRequested && !t.IsCanceled && onComplete != null)
                            {
                                onComplete();
                            }
                        }
                        if (cancelTokens.IsEmpty)
                        {
                            IsBusy = false;
                        }
                    }, onCompleteScheduler ?? scheduler);
                // we don't pass CancellationToken to ContinueWith because it will prevent ContinueWith from being called if Cancel is called before task completes.
                // http://stackoverflow.com/questions/6997480/task-continuewith-not-working-how-i-expected
            }
            finally
            {
                if (Transaction != null) Transaction.Current = lastTransaction;
            }
            return cancelToken;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Gets the PropertyChanged event
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the max number of tasks able to run concurrently
        /// </summary>
        public int ActiveTaskSize { get; protected set; }

        /// <summary>
        /// Gets the max queuable number of tasks.  If null, the queue size has no limit
        /// </summary>
        public int? QueuedTaskSize { get; protected set; }

        /// <summary>
        /// Gets whether the task queue is busy.
        /// </summary>
        public bool IsBusy
        {
            get
            {
                bool result = false;
                synchronizationContext.Send(o => result = isBusy, null);
                return result;
            }
            protected set
            {
                synchronizationContext.Send(o =>
                {
                    if (isBusy == value) return;

                    isBusy = value;
                    OnPropertyChanged("IsBusy");
                }, null);
            }
        }

        /// <summary>
        /// Gets the synchronization context for the instance. It is determined once upon construction.
        /// </summary>
        /// <value>
        /// The synchronization context.
        /// </value>
        public SynchronizationContext SynchronizationContext { get { return synchronizationContext; } }

        public bool IsDisabled
        {
            get
            {
                bool result = false;
                synchronizationContext.Send(o => result = isDisabled, null);
                return result;
            }
            set
            {
                synchronizationContext.Send(o => isDisabled = value, null);
            }
        }

        public System.Threading.Tasks.TaskScheduler Scheduler
        {
            get { return scheduler; }
        }

        /// <summary>
        /// Gets or sets the transaction to use for new threads.
        /// </summary>
        /// <value>
        /// The transaction.
        /// </value>
        public Transaction Transaction { get; set; }

    }
}