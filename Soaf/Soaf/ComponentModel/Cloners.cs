using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using ProtoBuf.Meta;
using ProtoBuf.ServiceModel;
using Soaf.ComponentModel;

[assembly: Component(typeof (ProtobufCloner), typeof (ICloner))]

namespace Soaf.ComponentModel
{
    /// <summary>
    ///     Defines a type that maps objects graphs to a different type
    /// </summary>
    public interface ICloner
    {
        object Clone(object source, Type destinationType = null, Type sourceType = null);
    }

    /// <summary>
    ///     The default Cloner implementation using DataContractSerializer.
    /// </summary>
    [Singleton]
    internal class DataContractSerializerCloner : ICloner
    {
        public object Clone(object source, Type destinationType, Type sourceType)
        {
            sourceType = sourceType ?? source.GetType();
            if (destinationType == null) destinationType = sourceType;

            using (new SuppressChangeTrackingScope())
            using (new SuppressPropertyChangedScope())
            {
                using (var ms = new MemoryStream())
                {
                    DataContractSerializer serializer = Serialization.CreateDataContractSerializer(sourceType);
                    serializer.WriteObject(ms, source);
                    ms.Position = 0;
                    serializer = Serialization.CreateDataContractSerializer(destinationType);
                    return serializer.ReadObject(ms);
                }
            }
        }
    }

    /// <summary>
    ///     Cloner using Protobuf.
    /// </summary>
    [Singleton]
    internal class ProtobufCloner : ICloner
    {
        private readonly DataContractSerializerCloner dataContractSerializerCloner;

        public ProtobufCloner(DataContractSerializerCloner dataContractSerializerCloner)
        {
            this.dataContractSerializerCloner = dataContractSerializerCloner;
        }

        public object Clone(object source, Type destinationType, Type sourceType)
        {
            if (source == null) return null;
            if (sourceType == null) sourceType = source.GetType();
            if (destinationType == null) destinationType = sourceType;

            if (sourceType.IsEnum) return dataContractSerializerCloner.Clone(source, destinationType, sourceType);

            var sourceAndDestinationMatch = sourceType == destinationType;
            var protoSerializer = GetSerializer(sourceType);
            var protoDeserializer = !sourceAndDestinationMatch 
                ? GetSerializer(destinationType) 
                : protoSerializer;

            if (protoSerializer == null || protoDeserializer == null)
            {
                return dataContractSerializerCloner.Clone(source, destinationType, sourceType);
            }

            using (new SuppressChangeTrackingScope())
            using (new SuppressPropertyChangedScope())
            {
                using (var ms = new MemoryStream())
                {
                    protoSerializer.WriteObject(ms, source);
                    ms.Position = 0;

                    var result = protoDeserializer.ReadObject(ms);
                    return result;
                }
            }
        }

        private XmlProtoSerializer GetSerializer(Type type)
        {
            var resolvedType = type;
            var registration = ServiceProvider.Current.GetService<IComponentRegistry>()
                .ComponentRegistrations.FirstOrDefault(r => r.For.Any(t => resolvedType == t || resolvedType.EqualsGenericTypeFor(t)));

            if (registration != null && registration.Type != null)
            {
                resolvedType = registration.Type.IsGenericTypeDefinition && resolvedType.IsGenericType
                    ? registration.Type.MakeGenericType(resolvedType.GetGenericArguments()) 
                    : registration.Type;
            }

            var protoSerializer = XmlProtoSerializer.TryCreate(RuntimeTypeModel.Default, resolvedType);
            return protoSerializer;
        }
    }
}