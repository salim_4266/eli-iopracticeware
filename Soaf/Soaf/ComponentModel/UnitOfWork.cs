using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;

[assembly: Component(typeof(UnitOfWorkProvider), typeof(IUnitOfWorkProvider))]
[assembly: Component(typeof(UnitOfWorkBehavior))]

namespace Soaf.ComponentModel
{
    /// <summary>
    /// Denotes that a type supports method should be wrapped in a unit of work.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class SupportsUnitOfWorkAttribute : ConcernAttribute
    {
        public SupportsUnitOfWorkAttribute()
            : base(typeof(UnitOfWorkBehavior))
        {
        }
    }

    /// <summary>
    /// Denotes that the execution of a method should be wrapped in a unit of work.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class UnitOfWorkAttribute : Attribute
    {
        public bool CreateNew { get; set; }
        public bool AcceptChanges { get; set; }
        public bool AutoAcceptChanges { get; set; }
    }

    /// <summary>
    /// Implmenents logic for wrapping a method in a unit of work.
    /// </summary>
    internal class UnitOfWorkBehavior : AttributedMemberInterceptor
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;

        public UnitOfWorkBehavior()
        {
        }

        public UnitOfWorkBehavior(IUnitOfWorkProvider unitOfWorkProvider)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
        }

        public override void Intercept(IInvocation invocation)
        {
            var attribute = invocation.Method.GetAttribute<UnitOfWorkAttribute>().EnsureNotDefault(new InvalidOperationException("UnitOfWork attribute should be present."));

            if (attribute.CreateNew)
            {
                using (var work = unitOfWorkProvider.Create())
                {
                    if (attribute.AutoAcceptChanges)
                    {
                        work.AutoAcceptChanges = true;
                    }
                    invocation.Proceed();
                    if (attribute.AcceptChanges)
                    {
                        work.AcceptChanges();
                    }

                }
            }
            else
            {
                using (var scope = unitOfWorkProvider.GetUnitOfWorkScope())
                {
                    bool lastAutoAcceptChanges = false;
                    if (attribute.AutoAcceptChanges)
                    {
                        lastAutoAcceptChanges = scope.UnitOfWork.AutoAcceptChanges;
                        scope.UnitOfWork.AutoAcceptChanges = true;
                    }
                    invocation.Proceed();
                    if (attribute.AcceptChanges)
                    {
                        scope.UnitOfWork.AcceptChanges();
                    }
                    scope.UnitOfWork.AutoAcceptChanges = lastAutoAcceptChanges;
                }
            }
        }

        public override IEnumerable<Type> AttributeTypes
        {
            get { yield return typeof(UnitOfWorkAttribute); }
        }
    }

    /// <summary>
    ///   Defines a type that provides functionality for performing a unit of work.
    /// </summary>
    public interface IUnitOfWork : IChangeTrackingExtended, IRevertibleChangeTrackingExtended, INotifyingDisposable
    {
        /// <summary>
        /// Gets or sets a value indicating whether to automatically accept changes as soon as an action is added.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [auto accept changes]; otherwise, <c>false</c>.
        /// </value>
        bool AutoAcceptChanges { get; set; }

        /// <summary>
        /// Adds the specified action to the unit of work.
        /// </summary>
        /// <param name="action">The action.</param>
        void Add(Action action);
    }

    /// <summary>
    /// A default IUnitOfWorkProvider.
    /// </summary>
    [Singleton]
    internal class UnitOfWorkProvider : IUnitOfWorkProvider
    {
        private static readonly object SyncRoot = new object();

        #region IUnitOfWorkProvider Members

        private static readonly Container<IUnitOfWork> Container = new PerThreadContainer<IUnitOfWork>();

        public IUnitOfWork Current
        {
            get { return Container.Instance; }
            set
            {
                lock (SyncRoot)
                {
                    if (value != null && !value.Is<UnitOfWork>())
                    {
                        throw new InvalidOperationException("Unsupported unit of work.");
                    }

                    var container = Container;

                    if (value == container.Instance) return;

                    if (container.Instance != null)
                    {
                        container.Instance.Disposing -= OnUnitOfWorkDisposing;
                    }
                    if (value != null)
                    {
                        value.CastTo<UnitOfWork>().Last = (UnitOfWork)container.Instance;
                        value.Disposing += OnUnitOfWorkDisposing;
                    }
                    container.Instance = value;
                }
            }
        }

        public IUnitOfWork Create(bool asCurrent = true)
        {
            var work = new UnitOfWork();
            if (asCurrent)
            {
                Current = work;
            }
            return work;
        }

        #endregion

        private void OnUnitOfWorkDisposing(object sender, EventArgs e)
        {
            lock (SyncRoot)
            {
                if (Current != null) Current.Disposing -= OnUnitOfWorkDisposing;
                var last = Current.IfNotNull(c => c.CastTo<UnitOfWork>().Last);
                Container.Instance = last;
                if (last != null)
                {
                    last.Disposing += OnUnitOfWorkDisposing;
                }
            }
        }
    }

    public static class UnitOfWorkExtensions
    {
        /// <summary>
        /// Gets a unit of work. Creates a new one if none is present.
        /// </summary>
        /// <param name="unitOfWorkProvider">The unit of work provider.</param>
        /// <returns></returns>
        public static IUnitOfWork GetUnitOfWork(this IUnitOfWorkProvider unitOfWorkProvider)
        {
            bool isNew;
            return unitOfWorkProvider.GetUnitOfWork(out isNew);
        }

        /// <summary>
        /// Gets a unit of work. Creates a new one if none is present.
        /// </summary>
        /// <param name="unitOfWorkProvider">The unit of work provider.</param>
        /// <param name="isNew">if set to <c>true</c> [is new].</param>
        /// <returns></returns>
        public static IUnitOfWork GetUnitOfWork(this IUnitOfWorkProvider unitOfWorkProvider, out bool isNew)
        {
            if (unitOfWorkProvider.Current == null)
            {
                isNew = true;
                return unitOfWorkProvider.Create();
            }
            isNew = false;
            return unitOfWorkProvider.Current;
        }

        /// <summary>
        /// Gets a unit of work scope that will dispose the unit of work if it is newly created by the scope.
        /// </summary>
        /// <param name="unitOfWorkProvider">The unit of work provider.</param>
        /// <param name="unitOfWork">An existing unit of work to use.</param>
        /// <returns></returns>
        public static IUnitOfWorkScope GetUnitOfWorkScope(this IUnitOfWorkProvider unitOfWorkProvider, IUnitOfWork unitOfWork = null)
        {
            if (unitOfWork == null)
            {
                bool isNew;
                IUnitOfWork work = unitOfWorkProvider.GetUnitOfWork(out isNew);
                return new UnitOfWorkScope(work, isNew);
            }
            var scope = new UnitOfWorkScope(unitOfWork, false);
            unitOfWorkProvider.Current = unitOfWork;
            EventHandler<EventArgs> disposed = null;
            disposed = delegate
            {
                unitOfWorkProvider.Current = ((UnitOfWork)unitOfWork).Last;
                // ReSharper disable AccessToModifiedClosure
                scope.Disposed -= disposed;
                // ReSharper restore AccessToModifiedClosure
            };
            scope.Disposed += disposed;
            return scope;
        }
    }

    /// <summary>
    /// A wrapper for a Unit of Work that will dispose of the work when it is disposed if so directed.
    /// </summary>
    public interface IUnitOfWorkScope : IDisposable
    {
        IUnitOfWork UnitOfWork { get; }
    }

    /// <summary>
    /// An implementation ofr a unit of work scope.
    /// </summary>
    internal class UnitOfWorkScope : IUnitOfWorkScope, INotifyingDisposable
    {
        private readonly bool disposeUnitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkScope"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="disposeUnitOfWork">if set to <c>true</c> [dispose].</param>
        public UnitOfWorkScope(IUnitOfWork unitOfWork, bool disposeUnitOfWork)
        {
            this.disposeUnitOfWork = disposeUnitOfWork;
            UnitOfWork = unitOfWork;
        }

        #region IUnitOfWorkScope Members

        public IUnitOfWork UnitOfWork { get; private set; }

        public void Dispose()
        {
            Disposing.Fire(this, new EventArgs());
            if (disposeUnitOfWork)
            {
                UnitOfWork.Dispose();
            }
            Disposed.Fire(this, new EventArgs());
        }

        #endregion

        public event EventHandler<EventArgs> Disposing;
        public event EventHandler<EventArgs> Disposed;
    }

    /// <summary>
    /// Defines a type that serves and managers IUnitsOfWork
    /// </summary>
    public interface IUnitOfWorkProvider
    {
        IUnitOfWork Current { get; set; }

        IUnitOfWork Create(bool asCurrent = true);
    }

    /// <summary>
    /// Defines a type that provides functionality for performing a unit of work.
    /// </summary>
    internal class UnitOfWork : IUnitOfWork
    {
        private bool isDisposed;
        private readonly Queue<Action> actions = new Queue<Action>();
        private readonly object syncRoot = new object();

        public UnitOfWork()
        {
            Components = new Dictionary<object, object>().Synchronized();
        }

        public UnitOfWork Last { get; set; }

        /// <summary>
        /// Components tracked by this unit of work. The unit of work will try to use these components in the appropriate scenario if it implements IChangeTracking/IDisposable/etc.
        /// </summary>
        public IDictionary<object, object> Components { get; private set; }

        #region IUnitOfWork Members

        public bool AutoAcceptChanges { get; set; }

        public void Add(Action action)
        {
            lock (syncRoot)
            {
                if (isDisposed) throw new ObjectDisposedException("UnitOfWork");

                actions.Enqueue(action);

                if (AutoAcceptChanges)
                {
                    AcceptChanges();
                }
            }
        }

        public void Dispose()
        {
            lock (syncRoot)
            {
                Disposing(this, new EventArgs());

                Components.Values.OfType<IDisposable>().TryForEach(i => i.Dispose());

                Components.Clear();

                isDisposed = true;

                Disposed(this, new EventArgs());
            }
        }

        public void AcceptChanges()
        {
            lock (syncRoot)
            {
                if (isDisposed) throw new ObjectDisposedException("UnitOfWork");

                AcceptingChanges(this, new EventArgs());

                while (actions.Count > 0)
                {
                    actions.Dequeue()();
                }

                Components.Values.OfType<IChangeTracking>().TryForEach(i => i.AcceptChanges());

                AcceptedChanges(this, new EventArgs());
            }
        }

        public bool IsChanged
        {
            get
            {
                lock (syncRoot)
                {
                    return actions.Count > 0 || Components.Values.OfType<IChangeTracking>().Any(i => i.IsChanged);
                }
            }
        }

        public void RejectChanges()
        {
            lock (syncRoot)
            {
                RejectingChanges(this, new EventArgs());

                actions.Clear();

                Components.Values.OfType<IRevertibleChangeTracking>().TryForEach(i => i.RejectChanges());

                RejectedChanges(this, new EventArgs());
            }
        }

        public event EventHandler<EventArgs> Disposing = delegate { };
        public event EventHandler<EventArgs> Disposed = delegate { };
        public event EventHandler<EventArgs> AcceptingChanges = delegate { };
        public event EventHandler<EventArgs> AcceptedChanges = delegate { };

        public IEnumerable<IObjectStateEntry> ObjectStateEntries
        {
            get
            {
                lock (syncRoot)
                {
                    return Components.Values.OfType<IChangeTrackingExtended>().SelectMany(i => i.ObjectStateEntries).ToArray();
                }
            }
        }

        public event EventHandler<EventArgs> RejectingChanges = delegate { };
        public event EventHandler<EventArgs> RejectedChanges = delegate { };

        #endregion
    }
}