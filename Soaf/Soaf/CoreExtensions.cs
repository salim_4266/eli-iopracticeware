﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Threading;
using Soaf.Xml;

#region System.Reactive dependencies (SavingTraceListener and LimitedConcurrencyLevelTaskPoolScheduler)
using System.Reactive.Concurrency;
using System.Reactive.Linq;
#endregion

[assembly: Component(typeof(LoggingDatabaseConfiguration), typeof(ILoggingDatabaseConfiguration), Priority = -1)]
[assembly: Component(typeof(ExeConfigurationContainer), typeof(IConfigurationContainer), Priority = -2)]
[assembly: Component(typeof(TraceMethodCallBehavior))]
[assembly: Component(typeof(ExtendedObservableCollection<>), typeof(ObservableCollection<>))]
[assembly: Component(typeof(ConfigurationContainerConnectionStringRepository), typeof(IConnectionStringRepository))]

namespace Soaf.Xml
{
    /// <summary>
    /// Helper methods for xml.
    /// </summary>
    public static class Xml
    {
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents the document.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="settings">The settings.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public static string ToString(this XDocument document, XmlWriterSettings settings)
        {
            var sb = new StringBuilder();
            using (var writer = XmlWriter.Create(sb, settings))
            {
                document.Save(writer);
            }
            return sb.ToString();
        }
    }
}

namespace Soaf.Logging
{
    /// <summary>
    ///   An EventLogTraceListener targeted specifically to LogEntries and knows how to format them.
    /// </summary>
    public class LogEntryEventLogTraceListener : SavingTraceListener
    {
        public LogEntryEventLogTraceListener(Lazy<EventLogTraceListener> eventLogTraceListener)
            : base(le => SaveLogEntry(le, eventLogTraceListener.Value))
        {
        }

        private static void SaveLogEntry(LogEntry logEntry, EventLogTraceListener eventLogTraceListener)
        {
            if (logEntry.Severity == Severity.Information)
            {
                eventLogTraceListener.Write(logEntry);
            }
            else
            {
                eventLogTraceListener.TraceData(new TraceEventCache(), eventLogTraceListener.EventLog.Source, logEntry.Severity.TranslateEnum<TraceEventType>(), logEntry.EventId ?? 0, logEntry);
            }
            eventLogTraceListener.Flush();
        }
    }

    /// <summary>
    /// Configuration for logging repository
    /// </summary>
    [Singleton]
    public interface ILoggingDatabaseConfiguration
    {
        IEnumerable<string> CategoryFilters { get; }
    }

    internal sealed class LoggingDatabaseConfiguration : ILoggingDatabaseConfiguration
    {
        public IEnumerable<string> CategoryFilters { get; set; }
    }

    /// <summary>
    ///   Writes log entries to the database directly
    /// </summary>
    public class LoggingDatabaseTraceListener : SavingTraceListener
    {
        private const string InsertLogEntry = @"
INSERT INTO [dbo].[LogEntries]
           ([Id]
           ,[Source]
           ,[Title]
           ,[Message]
           ,[SeverityId]
           ,[ActivityId]
           ,[RelatedActivityId]
           ,[EventId]
           ,[MachineName]
           ,[ThreadName]
           ,[ProcessId]
           ,[ProcessName]
           ,[UserId])
     VALUES
           (@Id
           ,@Source
           ,@Title
           ,@Message
           ,@SeverityId
           ,@ActivityId
           ,@RelatedActivityId
           ,@EventId
           ,@MachineName
           ,@ThreadName
           ,@ProcessId
           ,@ProcessName
           ,@UserId)";

        /// <summary>
        /// Insert association between log entry and category. Can upsert new category names
        /// </summary>
        private const string InsertLogCategoryTemplate = @"
DECLARE @Categories_Id_{0} INT

SELECT TOP 1 @Categories_Id_{0} = Id FROM dbo.LogCategories WHERE Name = @Category_Name_{0}
IF (@Categories_Id_{0} IS NULL)
BEGIN
	INSERT INTO dbo.LogCategories (Name) VALUES (@Category_Name_{0})
	SET @Categories_Id_{0} = SCOPE_IDENTITY()
END

INSERT INTO [dbo].[LogEntryLogCategory]
           ([LogEntryLogCategory_LogCategory_Id]
           ,[Categories_Id])
     VALUES
           (@Id
           ,@Categories_Id_{0})";

        /// <summary>
        /// Insert log entry property
        /// </summary>
        private const string InsertLogPropertyTemplate = @"
INSERT INTO [dbo].[LogEntryProperties]
           ([Id]
           ,[LogEntryId]
           ,[Name]
           ,[Value])
     VALUES
           (@LogEntryProperty_Id_{0}
           ,@Id
           ,@LogEntryProperty_Name_{0}
           ,@LogEntryProperty_Value_{0})";

        private readonly Lazy<ILoggingDatabaseConfiguration> configuration;

        public LoggingDatabaseTraceListener(Func<LogEntry, string> connectionString)
            : this(connectionString, ServiceProvider.Current.GetService<IAdoService>())
        {
        }

        private LoggingDatabaseTraceListener(Func<LogEntry, string> connectionString, IAdoService adoService)
            : base(le => SaveLogEntry(le, connectionString, adoService))
        {
            configuration = Lazy.For(() => ServiceProvider.Current.GetService<ILoggingDatabaseConfiguration>());
            Filter = new LogEntryTraceFilter(ShouldTrace);
        }

        private bool ShouldTrace(LogEntry logEntry)
        {
            bool shouldTrace = logEntry.Severity >= Severity.Warning;
            if (configuration.Value.CategoryFilters != null && logEntry.Categories != null) shouldTrace = shouldTrace || configuration.Value.CategoryFilters.Intersect(logEntry.Categories.Select(lc => lc.Name), StringComparer.OrdinalIgnoreCase).Any();
            return shouldTrace;
        }

        private static void SaveLogEntry(LogEntry logEntry, Func<LogEntry, string> connectionString, IAdoService adoService)
        {
            // Build a command to insert the log entry
            var commandTextBuilder = new StringBuilder();

            // Setup command
            var insertLogEntryCommand = new AdoServiceCommandExecutionArgument();
            insertLogEntryCommand.ConnectionString = connectionString(logEntry);
            insertLogEntryCommand.ResultType = AdoServiceCommandExecutionArgumentResultType.NonQuery;
            insertLogEntryCommand.Parameters = new List<AdoServiceParameterExecutionArgument>();

            // Generate command text and prepare parameters
            commandTextBuilder.AppendLine(InsertLogEntry);
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@Id",
                                                  Value = logEntry.Id,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.Guid.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@Source",
                                                  Value = logEntry.Source ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.String.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@Title",
                                                  Value = logEntry.Title ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.String.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@Message",
                                                  Value = logEntry.Message ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.String.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@SeverityId",
                                                  Value = logEntry.SeverityId,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.Int32.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@ActivityId",
                                                  Value = logEntry.ActivityId ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.Guid.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@RelatedActivityId",
                                                  Value = logEntry.RelatedActivityId ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.Guid.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@EventId",
                                                  Value = logEntry.EventId ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.Int32.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@MachineName",
                                                  Value = logEntry.MachineName ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.String.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@ThreadName",
                                                  Value = logEntry.ThreadName ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.String.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@ProcessId",
                                                  Value = logEntry.ProcessId ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.Int32.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@ProcessName",
                                                  Value = logEntry.ProcessName ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.String.ToString()
                                              });
            insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                                              {
                                                  ParameterName = "@UserId",
                                                  Value = logEntry.UserId ?? (object)DBNull.Value,
                                                  Direction = ParameterDirection.Input.ToString(),
                                                  DbType = DbType.String.ToString()
                                              });

            // Insert category mappings
            if (logEntry.Categories != null)
            {
                int paramIndex = 0;
                foreach (var category in logEntry.Categories)
                {
                    // Append insert category statements to command text
                    commandTextBuilder.AppendLine(InsertLogCategoryTemplate.FormatWith(paramIndex));

                    // Add parameters specific to this category insert
                    insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                    {
                        ParameterName = "@Category_Name_{0}".FormatWith(paramIndex),
                        Value = category.Name,
                        Direction = ParameterDirection.Input.ToString(),
                        DbType = DbType.String.ToString()
                    });

                    paramIndex++;
                }
            }

            // Insert properties
            if (logEntry.Properties != null)
            {
                int paramIndex = 0;
                foreach (var property in logEntry.Properties)
                {
                    // Append insert property statements to command text
                    commandTextBuilder.AppendLine(InsertLogPropertyTemplate.FormatWith(paramIndex));

                    // Add parameters specific to this property insert
                    insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                    {
                        ParameterName = "@LogEntryProperty_Id_{0}".FormatWith(paramIndex),
                        Value = property.Id,
                        Direction = ParameterDirection.Input.ToString(),
                        DbType = DbType.Guid.ToString()
                    });
                    insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                    {
                        ParameterName = "@LogEntryProperty_Name_{0}".FormatWith(paramIndex),
                        Value = property.Name ?? (object)DBNull.Value,
                        Direction = ParameterDirection.Input.ToString(),
                        DbType = DbType.String.ToString()
                    });
                    insertLogEntryCommand.Parameters.Add(new AdoServiceParameterExecutionArgument
                    {
                        ParameterName = "@LogEntryProperty_Value_{0}".FormatWith(paramIndex),
                        Value = property.Value ?? (object)DBNull.Value,
                        Direction = ParameterDirection.Input.ToString(),
                        DbType = DbType.String.ToString()
                    });

                    paramIndex++;
                }
            }

            // Set command text
            insertLogEntryCommand.CommandText = commandTextBuilder.ToString();

            // Execute the insert command
            adoService.ExecuteCommandNonQuery(insertLogEntryCommand);
        }
    }

    /// <summary>
    ///   A trace filter targeted specifically to LogEntries.
    /// </summary>
    public class LogEntryTraceFilter : TraceFilter
    {
        private readonly Func<LogEntry, bool> filter;

        public LogEntryTraceFilter(Func<LogEntry, bool> filter)
        {
            this.filter = filter;
        }

        public override bool ShouldTrace(TraceEventCache cache, string source, TraceEventType eventType, int id, string formatOrMessage, object[] args, object data1, object[] data)
        {
            LogEntry logEntry = data1 as LogEntry ?? (data != null ? data.FirstOrDefault() as LogEntry : null);
            if (logEntry == null)
            {
                logEntry = new LogEntry
                               {
                                   Severity = ToSeverity(eventType),
                                   Source = source,
                                   EventId = id,
                                   Message = args == null ? formatOrMessage : string.Format(formatOrMessage, args)
                               };
            }

            return filter(logEntry);
        }

        private static Severity ToSeverity(TraceEventType eventType)
        {
            switch (eventType)
            {
                case TraceEventType.Information:
                    return Severity.Information;
                case TraceEventType.Warning:
                    return Severity.Warning;
                case TraceEventType.Error:
                    return Severity.Error;
            }
            return Severity.Information;
        }
    }

    /// <summary>
    ///   Writes trace messages to an saving store.
    /// </summary>
    public class SavingTraceListener : LogEntryTraceListener
    {
        private readonly BlockingCollection<Tuple<LogEntry, Func<bool>, IPrincipal>> logEntryQueue;

        [ThreadStatic]
        private static bool isSaving;

        [DebuggerNonUserCode]
        static SavingTraceListener()
        {
            try
            {
                Assembly.Load("System.Reactive, Version=1.0.10621.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");
            }
            catch
            {
                AssemblyResolutionProvider.LoadAssemblyResources(WellKnownAssemblies.Soaf, "Soaf.System.Reactive.dll");
            }
        }

        public SavingTraceListener(Action<LogEntry> save)
        {
            Action = EnqueueSaveLogEntry;

            logEntryQueue = new BlockingCollection<Tuple<LogEntry, Func<bool>, IPrincipal>>();

            // Perform actual saving on 1 background thread per trace listener
            var poolScheduler = new LimitedConcurrencyLevelTaskPoolScheduler(1);
            logEntryQueue.GetConsumingEnumerable().ToObservable(poolScheduler).Subscribe(l =>
            {
                isSaving = true;
                var existingPrincipal = PrincipalContext.Current.Principal;
                if (l.Item3 != null)
                {
                    PrincipalContext.Current.Principal = l.Item3;
                }
                try
                {
                    if (l.Item2()) save(l.Item1);
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {
                    // we don't want to crash the program...
                    // and since we are running on a background thread we have nothing further to do but swallow the exception, unfortunately.
                }
                finally
                {
                    PrincipalContext.Current.Principal = existingPrincipal;
                    isSaving = false;
                }
            });
        }

        private void EnqueueSaveLogEntry(LogEntry logEntry, Func<bool> shouldTrace)
        {
            // Do not log anything from the same thread during saving operation
            if (isSaving) return;

            logEntryQueue.Add(Tuple.Create(logEntry, shouldTrace, PrincipalContext.Current.Principal));
        }
    }

    /// <summary>
    ///   A trace listener targeted to LogEntries.
    /// </summary>
    public class LogEntryTraceListener : TraceListener
    {
        protected LogEntryTraceListener()
        {

        }

        /// <summary>
        ///   Initializes a new instance of the <see cref="SavingTraceListener" /> class.
        /// </summary>
        /// <param name="action"> The action. </param>
        public LogEntryTraceListener(Action<LogEntry, Func<bool>> action)
            : this()
        {
            Action = action;
            Name = GetType().Name;
        }

        /// <summary>
        /// Gets or sets the action (takes a log entry and a delegate to re-invoke the filter).
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        protected Action<LogEntry, Func<bool>> Action { get; set; }

        public override void Write(string message, string category)
        {
            WriteLine(message, category);
        }

        public override void Write(object o, string category)
        {
            WriteLine(o, category);
        }

        public override void Write(string message)
        {
            WriteLine(message, null);
        }

        public override void WriteLine(string message)
        {
            WriteLine(message, null);
        }

        public override void WriteLine(object o)
        {
            WriteLine(o, null);
        }

        public override void Write(object o)
        {
            WriteLine(o, null);
        }

        public override void WriteLine(string message, string category)
        {
            WriteLine(message as object, category);
        }

        public override void WriteLine(object o, string category)
        {
            var traceEventType = TraceEventType.Verbose;

            var logEntry = o as LogEntry;

            if (logEntry == null)
            {
                logEntry = new LogEntry { Message = string.Format("{0}", o) };
            }
            else
            {
                switch (logEntry.Severity)
                {
                    case Severity.Information:
                        traceEventType = TraceEventType.Information;
                        break;
                    case Severity.Warning:
                        traceEventType = TraceEventType.Warning;
                        break;
                    case Severity.Error:
                        traceEventType = TraceEventType.Error;
                        break;
                }
            }

            if (category != null)
            {
                logEntry.Categories.Add(new LogCategory { Name = category });
            }

            Func<bool> shouldTrace = () => Filter == null || Filter.ShouldTrace(null, "", traceEventType, 0, logEntry.Message, null, logEntry, null);

            if (shouldTrace())
            {
                Action(logEntry, shouldTrace);
            }
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
        {
            TraceData(eventCache, source, eventType, id, message);
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id)
        {
            TraceData(eventCache, source, eventType, id, null);
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
        {
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            // .NET internally may pass args as null when calling TraceSource.TraceInformation(message)
            TraceData(eventCache, source, eventType, id, args == null ? format : string.Format(format, args));
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {
            var logEntry = data as LogEntry;
            if (logEntry == null)
            {
                logEntry = new LogEntry();
                logEntry.Source = source;
                logEntry.Severity = ToSeverity(eventType);
                logEntry.EventId = id;
                logEntry.Message = string.Format("{0}", data);
            }
            else
            {
                source = logEntry.Source;
                id = logEntry.EventId ?? 0;

                switch (logEntry.Severity)
                {
                    case Severity.Information:
                        eventType = TraceEventType.Information;
                        break;
                    case Severity.Warning:
                        eventType = TraceEventType.Warning;
                        break;
                    case Severity.Error:
                        eventType = TraceEventType.Error;
                        break;
                }
            }

            Func<bool> shouldTrace = () => Filter == null || Filter.ShouldTrace(eventCache, source, eventType, id, logEntry.Message, null, logEntry, null);

            if (shouldTrace())
            {
                Action(logEntry, shouldTrace);
            }
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, params object[] data)
        {
            var logEntry = data.FirstOrDefault() as LogEntry;
            if (logEntry == null)
            {
                logEntry = new LogEntry();
                logEntry.Source = source;
                logEntry.Severity = ToSeverity(eventType);
                logEntry.EventId = id;
                logEntry.Message = string.Join(", ", data.Select(i => string.Format("{0}", i)).ToArray());
            }
            else
            {
                source = logEntry.Source;
                id = logEntry.EventId ?? 0;

                switch (logEntry.Severity)
                {
                    case Severity.Information:
                        eventType = TraceEventType.Information;
                        break;
                    case Severity.Warning:
                        eventType = TraceEventType.Warning;
                        break;
                    case Severity.Error:
                        eventType = TraceEventType.Error;
                        break;
                }
            }

            Func<bool> shouldTrace = () => Filter == null || Filter.ShouldTrace(eventCache, source, eventType, id, logEntry.Message, null, logEntry, data);
            if (shouldTrace())
            {
                Action(logEntry, shouldTrace);
            }
        }

        private static Severity ToSeverity(TraceEventType eventType)
        {
            switch (eventType)
            {
                case TraceEventType.Information:
                    return Severity.Information;
                case TraceEventType.Warning:
                    return Severity.Warning;
                case TraceEventType.Error:
                    return Severity.Error;
            }
            return Severity.Information;
        }
    }



    /// <summary>
    /// Identifies that this type supports method call tracing.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class SupportsMethodCallTracingAttribute : ConcernAttribute
    {
        public SupportsMethodCallTracingAttribute()
            : base(typeof(TraceMethodCallBehavior), 2000)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether to automatically enable for all methods.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable for all]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableForAll { get; set; }

        /// <summary>
        /// Gets or sets the source. Required.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use XML when tracing. Default is false.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use XML]; otherwise, <c>false</c>.
        /// </value>
        public bool UseXml { get; set; }
    }

    /// <summary>
    /// Identifies that this method call should be traced.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class TraceMethodCallAttribute : Attribute
    {

    }

    /// <summary>
    /// Implementation for method call tracing behavior. Automatically traces method calls to Trace.
    /// </summary>
    [Singleton]
    public class TraceMethodCallBehavior : IInterceptor, IInstanceConcern
    {
        private string source;
        private bool enableForAll;
        private TraceSource traceSource;
        private int i = 1;

        private bool useXml;

        public void Intercept(IInvocation invocation)
        {
            if (traceSource != null && traceSource.Listeners.OfType<TraceListener>().Any(l => l.Name != "Default")
                && (enableForAll || invocation.Method.HasAttribute<TraceMethodCallAttribute>()))
            {
                var sw = new Stopwatch();
                sw.Start();
                invocation.TryProceed();
                sw.Stop();

                var call = new MethodCall(invocation, sw.Elapsed.TotalMilliseconds, DateTime.Now);

                traceSource.TraceEvent(TraceEventType.Information, i++, useXml ? call.ToXml() : call.ToString());
            }
            else
            {
                invocation.TryProceed();
            }
        }

        public void ApplyConcern(object value)
        {
            var attribute = value.GetType().GetAttribute<SupportsMethodCallTracingAttribute>();
            if (attribute != null)
            {
                source = attribute.Source;
                enableForAll = attribute.EnableForAll;
                useXml = attribute.UseXml;
                if (source != null)
                {
                    traceSource = new TraceSource(source);
                }
            }
        }

        [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
        public class MethodCall
        {
            private readonly MethodInfo methodInfo;

            public MethodCall() { }

            public MethodCall(IInvocation invocation, double durationMilliseconds, DateTime dateTime)
            {
                DurationMilliseconds = durationMilliseconds;
                DateTime = dateTime;
                Arguments = new object[invocation.Arguments.Length];
                invocation.Arguments.CopyTo(Arguments, 0);
                ReturnValue = invocation.ReturnValue;
                methodInfo = invocation.Method;
            }

            [DataMember]
            public DateTime DateTime { get; private set; }

            [DataMember]
            public double DurationMilliseconds { get; private set; }

            [DataMember]
            public object[] Arguments { get; private set; }

            [DataMember]
            public object ReturnValue { get; private set; }

            [DataMember]
            public string Method
            {
                get { return methodInfo.ToString(); }
                // ReSharper disable ValueParameterNotUsed
                set { }
                // ReSharper restore ValueParameterNotUsed
            }

            public override bool Equals(object obj)
            {
                return obj.As<MethodCall>().IfNotNull(o =>
                                                 o.Arguments.SequenceEqualWithNullCheck(Arguments) &&
                                                 Equals(o.methodInfo, methodInfo));
            }

            public override int GetHashCode()
            {
                return Arguments.GetSequenceHashCode() ^ Objects.GetHashCodeWithNullCheck(methodInfo);
            }

            public override string ToString()
            {
                var document = new XDocument(new XElement("MethodCall",
                    new XElement("Method", Method),
                    new XElement("DurationMilliseconds", DurationMilliseconds),
                    new XElement("DateTime", DateTime),
                    new XElement("Arguments", Arguments.Select((a, i) => new XElement("Argument", GetArgumentStringValue(a, i))).ToArray()),
                    new XElement("ReturnValue", GetReturnValueStringValue(ReturnValue))));

                return document.ToString(new XmlWriterSettings { CheckCharacters = false, Indent = true, OmitXmlDeclaration = true });
            }

            private object GetReturnValueStringValue(object returnValue)
            {
                if (returnValue == null) return string.Empty;

                if (returnValue is string) return returnValue;

                var items = returnValue as IEnumerable;
                if (items != null)
                {
                    return items.OfType<object>().Select(i => new XElement("Item", TryParse(i)));
                }

                return returnValue;
            }

            [DebuggerNonUserCode]
            private object TryParse(object o)
            {
                if (o == null) return string.Empty;

                try
                {
                    return XElement.Parse(o.ToString());
                }
                catch
                {
                    return o;
                }
            }

            private object GetArgumentStringValue(object o, int index)
            {
                if (o == null) return string.Empty;

                if (o is string) return o;

                var parameters = methodInfo.GetParameters();

                var parameterName = index <= parameters.Length ? parameters[index].Name : "Parameter";

                var items = o as IEnumerable;
                if (items != null)
                {
                    return new XElement(parameterName, items.OfType<object>().Select(i => new XElement("Item", TryParse(i))));
                }

                return new XElement(parameterName, TryParse(o));
            }
        }
    }

    /// <summary>
    /// Trace listener that traces events and data without any header or footer.
    /// </summary>
    public class RawTextWriterTraceListener : TextWriterTraceListener
    {
        public RawTextWriterTraceListener() { }

        public RawTextWriterTraceListener(string fileName, string name) : base(fileName, name) { }

        public RawTextWriterTraceListener(string fileName) : base(fileName) { }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
        {
            WriteLine(String.Format(CultureInfo.InvariantCulture, format, args));
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
        {
            WriteLine(message);
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {
            WriteLine(data.ToString());
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, params object[] data)
        {
            var sb = new StringBuilder();
            if (data != null)
            {
                for (int i = 0; i < data.Length; i++)
                {
                    if (i != 0)
                        sb.Append(", ");

                    if (data[i] != null)
                        sb.Append(data[i]);
                }
            }
            WriteLine(sb.ToString());
        }
    }
}

namespace Soaf.Threading
{
    /// <summary>
    ///   Provides a task scheduler that ensures a maximum concurrency level while running on top of the ThreadPool.
    /// </summary>
    public class LimitedConcurrencyLevelTaskScheduler : TaskScheduler
    {
        /// <summary>
        ///   Whether the current thread is processing work items.
        /// </summary>
        [ThreadStatic]
        private static bool currentThreadIsProcessingItems;

        /// <summary>
        ///   The maximum concurrency level allowed by this scheduler.
        /// </summary>
        private readonly int maxDegreeOfParallelism;

        private readonly int? maxQueueSize;

        /// <summary>
        ///   The list of tasks to be executed.
        /// </summary>
        private readonly LinkedList<Task> tasks = new LinkedList<Task>(); // protected by lock(_tasks)

        /// <summary>
        ///   Whether the scheduler is currently processing work items.
        /// </summary>
        private int delegatesQueuedOrRunning; // protected by lock(_tasks)

        /// <summary>
        /// Initializes an instance of the LimitedConcurrencyLevelTaskScheduler class with the specified degree of parallelism.
        /// </summary>
        /// <param name="maxDegreeOfParallelism">The maximum degree of parallelism provided by this scheduler.</param>
        /// <param name="maxQueueSize">Length of the max queue.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">maxDegreeOfParallelism</exception>
        public LimitedConcurrencyLevelTaskScheduler(int maxDegreeOfParallelism, int? maxQueueSize = null)
        {
            if (maxDegreeOfParallelism < 1) throw new ArgumentOutOfRangeException("maxDegreeOfParallelism");
            this.maxDegreeOfParallelism = maxDegreeOfParallelism;
            this.maxQueueSize = maxQueueSize;
        }

        /// <summary>
        ///   Gets the maximum concurrency level supported by this scheduler.
        /// </summary>
        public override sealed int MaximumConcurrencyLevel
        {
            get { return maxDegreeOfParallelism; }
        }

        /// <summary>
        ///   Queues a task to the scheduler.
        /// </summary>
        /// <param name="task"> The task to be queued. </param>
        protected override sealed void QueueTask(Task task)
        {
            // Add the task to the list of tasks to be processed.  If there aren't enough
            // delegates currently queued or running to process tasks, schedule another.
            lock (tasks)
            {
                while (maxQueueSize != null && tasks.Count == maxQueueSize)
                {
                    tasks.RemoveFirst();
                }

                tasks.AddLast(task);
                if (delegatesQueuedOrRunning < maxDegreeOfParallelism)
                {
                    ++delegatesQueuedOrRunning;
                    NotifyThreadPoolOfPendingWork();
                }
            }
        }

        /// <summary>
        ///   Informs the ThreadPool that there's work to be executed for this scheduler.
        /// </summary>
        private void NotifyThreadPoolOfPendingWork()
        {
            ThreadPool.UnsafeQueueUserWorkItem(_ =>
                                                   {
                                                       // Note that the current thread is now processing work items.
                                                       // This is necessary to enable inlining of tasks into this thread.
                                                       currentThreadIsProcessingItems = true;
                                                       try
                                                       {
                                                           // Process all available items in the queue.
                                                           while (true)
                                                           {
                                                               Task item;
                                                               lock (tasks)
                                                               {
                                                                   // When there are no more items to be processed,
                                                                   // note that we're done processing, and get out.
                                                                   if (tasks.Count == 0)
                                                                   {
                                                                       --delegatesQueuedOrRunning;
                                                                       break;
                                                                   }

                                                                   // Get the next item from the queue
                                                                   item = tasks.First.Value;
                                                                   tasks.RemoveFirst();
                                                               }

                                                               // Execute the task we pulled out of the queue
                                                               TryExecuteTask(item);
                                                           }
                                                       }
                                                       // We're done processing items on the current thread
                                                       finally
                                                       {
                                                           currentThreadIsProcessingItems = false;
                                                       }
                                                   }, null);
        }

        /// <summary>
        ///   Attempts to execute the specified task on the current thread.
        /// </summary>
        /// <param name="task"> The task to be executed. </param>
        /// <param name="taskWasPreviouslyQueued"> </param>
        /// <returns> Whether the task could be executed on the current thread. </returns>
        protected override sealed bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            // If this thread isn't already processing a task, we don't support inlining
            if (!currentThreadIsProcessingItems) return false;

            // If the task was previously queued, remove it from the queue
            if (taskWasPreviouslyQueued) TryDequeue(task);

            // Try to run the task.
            return TryExecuteTask(task);
        }

        /// <summary>
        ///   Attempts to remove a previously scheduled task from the scheduler.
        /// </summary>
        /// <param name="task"> The task to be removed. </param>
        /// <returns> Whether the task could be found and removed. </returns>
        protected override sealed bool TryDequeue(Task task)
        {
            lock (tasks) return tasks.Remove(task);
        }

        /// <summary>
        ///   Gets an enumerable of the tasks currently scheduled on this scheduler.
        /// </summary>
        /// <returns> An enumerable of the tasks currently scheduled. </returns>
        protected override sealed IEnumerable<Task> GetScheduledTasks()
        {
            bool lockTaken = false;
            try
            {
                Monitor.TryEnter(tasks, ref lockTaken);
                if (lockTaken) return tasks.ToArray();
                else throw new NotSupportedException();
            }
            finally
            {
                if (lockTaken) Monitor.Exit(tasks);
            }
        }
    }

    /// <summary>
    ///   A Reactive IScheduler that uses LimitedConcurrencyLevelTaskScheduler.
    /// </summary>
    public class LimitedConcurrencyLevelTaskPoolScheduler : IScheduler
    {
        private readonly TaskPoolScheduler taskPoolScheduler;

        public LimitedConcurrencyLevelTaskPoolScheduler(int maxDegreeOfParallelism, int? maxQueueLength = null)
        {
            taskPoolScheduler = new TaskPoolScheduler(new TaskFactory(new LimitedConcurrencyLevelTaskScheduler(maxDegreeOfParallelism, maxQueueLength)));
        }

        #region IScheduler Members

        public IDisposable Schedule<TState>(TState state, Func<IScheduler, TState, IDisposable> action)
        {
            return taskPoolScheduler.Schedule(state, action);
        }

        public IDisposable Schedule<TState>(TState state, TimeSpan dueTime, Func<IScheduler, TState, IDisposable> action)
        {
            return taskPoolScheduler.Schedule(state, dueTime, action);
        }

        public IDisposable Schedule<TState>(TState state, DateTimeOffset dueTime, Func<IScheduler, TState, IDisposable> action)
        {
            return taskPoolScheduler.Schedule(state, dueTime, action);
        }

        public DateTimeOffset Now
        {
            get { return taskPoolScheduler.Now; }
        }

        #endregion
    }

}

namespace Soaf.Configuration
{
    /// <summary>
    /// A container for a system configuration object.
    /// </summary>
    public interface IConfigurationContainer
    {
        System.Configuration.Configuration Configuration { get; }
    }

    /// <summary>
    /// Loads a System.Configuration.Configuration using the entry assembly (OpenExeConfiguration).
    /// </summary>
    [Singleton]
    public class ExeConfigurationContainer : IConfigurationContainer
    {
        private readonly Lazy<System.Configuration.Configuration> configuration = Lazy.For(() => ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None));

        public virtual System.Configuration.Configuration Configuration
        {
            get { return configuration.Value; }
        }
    }

    /// <summary>
    /// Collection of KeyValueConfigurations (matches system configuration KeyValueConfigurationCollection xml definition).
    /// </summary>
    public class KeyValueConfigurationCollection
    {
        [XmlElement("add")]
        public List<KeyValueConfiguration> Items { get; set; }

        public string this[string key]
        {
            get { return Items.Where(i => string.Equals(i.Key, key, StringComparison.OrdinalIgnoreCase)).Select(i => i.Value).FirstOrDefault(); }
            set
            {
                var item = Items.FirstOrDefault(i => i.Key.Equals(key, StringComparison.OrdinalIgnoreCase));
                if (item != null) item.Value = value;
                else Items.Add(new KeyValueConfiguration { Key = key, Value = value });
            }
        }
    }
    /// <summary>
    /// Key/Value pair configuration settings.
    /// </summary>
    public class KeyValueConfiguration
    {
        public KeyValueConfiguration()
        {
        }

        public KeyValueConfiguration(string key, string value)
        {
            Key = key;
            Value = value;
        }

        [XmlAttribute("key")]
        public string Key { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }
    }

    /// <summary>
    /// Collection of ConnectionStringConfiguration (matches system configuration connectionStrings setting xml definition).
    /// </summary>
    public class ConnectionStringConfigurationCollection
    {
        [XmlElement("add")]
        public List<ConnectionStringConfiguration> Items { get; set; }

        public ConnectionStringConfiguration this[string name]
        {
            get { return Items.FirstOrDefault(i => string.Equals(i.Name, name, StringComparison.OrdinalIgnoreCase)); }
            set
            {
                var item = Items.FirstOrDefault(i => i.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
                if (item != null)
                {
                    item.ProviderName = value.ProviderName;
                    item.ConnectionString = value.ConnectionString;
                }
                else
                {
                    Items.Add(value);
                }
            }
        }
    }

    /// <summary>
    /// Connection string settings.
    /// </summary>
    public class ConnectionStringConfiguration
    {
        public ConnectionStringConfiguration()
        {
        }

        public ConnectionStringConfiguration(string name, string connectionString, string providerName = null)
        {
            Name = name;
            ConnectionString = connectionString;
            ProviderName = providerName;
        }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("connectionString")]
        public string ConnectionString { get; set; }

        [XmlAttribute("providerName")]
        public string ProviderName { get; set; }
    }


    /// <summary>
    /// Interface that retrieves connection strings.
    /// </summary>
    public interface IConnectionStringRepository
    {
        IEnumerable<ConnectionStringConfiguration> ConnectionStrings { get; }

        ConnectionStringConfiguration this[string name] { get; set; }
    }

    /// <summary>
    /// An IConnectionStringRepository that uses IConfigurationContainer.
    /// </summary>
    [Singleton]
    public class ConfigurationContainerConnectionStringRepository : IConnectionStringRepository
    {
        private readonly IConfigurationContainer configurationContainer;

        public ConfigurationContainerConnectionStringRepository(IConfigurationContainer configurationContainer)
        {
            this.configurationContainer = configurationContainer;
        }

        public virtual IEnumerable<ConnectionStringConfiguration> ConnectionStrings
        {
            get
            {
                return
                    configurationContainer.Configuration.ConnectionStrings.ConnectionStrings.OfType<ConnectionStringSettings>()
                                          .Select(cs => new ConnectionStringConfiguration(cs.Name, cs.ConnectionString, cs.ProviderName))
                                          .ToList().AsReadOnly();
            }
        }

        public virtual ConnectionStringConfiguration this[string name]
        {
            get { return configurationContainer.Configuration.ConnectionStrings.ConnectionStrings[name].IfNotNull(cs => new ConnectionStringConfiguration(cs.Name, cs.ConnectionString, cs.ProviderName)); }
            set
            {
                var item = configurationContainer.Configuration.ConnectionStrings.ConnectionStrings[name];
                if (item != null)
                {
                    item.SetReadOnly(false);
                    item.ProviderName = value.ProviderName;
                    item.ConnectionString = value.ConnectionString;
                }
                else
                {
                    configurationContainer.Configuration.ConnectionStrings.SetReadOnly(false);
                    configurationContainer.Configuration.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings(value.Name, value.ConnectionString, value.ProviderName));
                }
            }
        }
    }

    /// <summary>
    /// Any easy means of accessing strongly typed configuration sections.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Singleton]
    public class ConfigurationSection<T> where T : class
    {
        private static readonly Lazy<IConfigurationContainer> ConfigurationContainer = Lazy.For(() => ServiceProvider.Current.GetService<IConfigurationContainer>(), true);

        public static T Current
        {
            get { return Named(null); }
        }

        public static T Named(string name)
        {
            if (!ServiceProvider.IsInitialized) return null;
            if (ConfigurationContainer == null || ConfigurationContainer.Value.Configuration == null) return null;
            return ConfigurationContainer.Value.Configuration.GetSection<T>(name);
        }
    }

    /// <summary>
    /// Any xml based configuration section that supports reading/refreshing/saving using xml from an external file.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class XmlConfigurationSection<T> : ConfigurationSection where T : class
    {
        /* Usage:
            <configSections>
                <section name="customAppSettings" type="XmlConfigurationSection`1[[AppSettings, ConsoleApplication1]], ConsoleApplication1" />
            </configSections>
            <customAppSettings source="Custom.config" xpath="/configuration/customAppSettings" />
         * 
         */
        private T value;
        private bool isSaving;
        private string[] sourcesToWatch;

        /// <summary>
        /// Cache the raw xml on deserialization, because if it changes prior to serialization (Save), .NET will throw the error "The configuration file has been changed by another program." at System.Configuration.BaseConfigurationRecord.GetSectionXmlReader(String[] keys, SectionInput input).
        /// </summary>
        private string rawXml;

        public XmlConfigurationSection()
        {
            TryFindSourceInParentDirectory = true;
        }

        public XmlConfigurationSection(string name, string source, string xpath)
            : this()
        {
            SectionInformation.SetRawXml(@"<{0} source=""{1}"" xpath=""{2}""".FormatWith(name, source, xpath));
        }

        public virtual T Value
        {
            get { return value ?? (value = DeserializeValue()); }
        }

        protected virtual string Source { get; set; }

        protected virtual string XPath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to try to find the source file in a parent directory if not present in current directory. Default is true.
        /// </summary>
        /// <value>
        /// <c>true</c> if [try find source in parent directory]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool TryFindSourceInParentDirectory { get; set; }

        public static implicit operator T(XmlConfigurationSection<T> instance)
        {
            return instance.Value;
        }

        protected virtual IEnumerable<string> GetSourcesToWatch()
        {
            return new[] { Source }.Where(i => i != null).ToArray();
        }

        protected override void DeserializeSection(XmlReader reader)
        {
            reader.Read();
            XPath = reader.GetAttribute("xpath");

            // Read source
            var sourceFile = reader.GetAttribute("source");

            // Analyze specified source file path and convert it to full path
            if (!string.IsNullOrEmpty(sourceFile))
            {
                // If source represents relative path -> make it relative to config file instead of Environment.CurrenctDirectory
                if (!Path.IsPathRooted(sourceFile) && CurrentConfiguration != null && CurrentConfiguration.HasFile)
                {
                    sourceFile = Path.Combine(Path.GetDirectoryName(CurrentConfiguration.FilePath) ?? string.Empty, sourceFile);
                }

                // Transform C:\\test1\\test2\\..\\someapp.config into C:\\test1\\someapp.config
                sourceFile = Path.GetFullPath(sourceFile);

                // If file is not present at evaluated location -> walk up the directory structure
                if (!File.Exists(sourceFile) && TryFindSourceInParentDirectory)
                {
                    sourceFile = FindSourceInParentDirectory(sourceFile);
                }
            }

            Source = sourceFile;
            sourcesToWatch = GetSourcesToWatch().ToArray();

            foreach (var directory in sourcesToWatch.Select(s => Path.GetDirectoryName(Path.GetFullPath(s))).Distinct())
            {
                var fsw = ConfigurationSystem.FileSystemWatchers.GetOrAdd(directory, d => new FileSystemWatcher { Path = d, EnableRaisingEvents = true });
                fsw.Changed -= new FileSystemEventHandler(OnSourceChanged).MakeWeak<FileSystemEventHandler>();
                fsw.Changed += new FileSystemEventHandler(OnSourceChanged).MakeWeak<FileSystemEventHandler>();
            }

            // see comment for rawXml field
            rawXml = reader.ReadOuterXml();
        }

        protected static string FindSourceInParentDirectory(string source)
        {
            var fileName = Path.GetFileName(source);

            if (string.IsNullOrEmpty(fileName)) return null;

            string currentDirectory = Path.GetDirectoryName(Path.GetFullPath(source));

            while (!string.IsNullOrEmpty(currentDirectory))
            {
                var parentDirectory = Path.GetFullPath(Path.Combine(currentDirectory, ".."));

                if (parentDirectory == currentDirectory) return source;

                var currentFilePath = Path.Combine(parentDirectory, fileName);

                if (File.Exists(currentFilePath)) return currentFilePath;

                currentDirectory = parentDirectory;
            }

            return source;
        }

        private void OnSourceChanged(object sender, FileSystemEventArgs e)
        {
            lock (ConfigurationSystem.SyncRoot)
            {
                if (!isSaving && sourcesToWatch.Contains(e.FullPath, StringComparer.OrdinalIgnoreCase))
                {
                    Refresh();
                }
            }
        }

        [DebuggerNonUserCode]
        protected virtual void Refresh()
        {
            value = null;
            try
            {
                // don't bother refreshing using ConfigurationManager if this section is part of the
                // default ConfigurationManager configuration (since we will already refresh this instance, having nulled value above).
                if (!ReferenceEquals(ConfigurationManager.GetSection(SectionInformation.Name), value))
                {
                    ConfigurationManager.RefreshSection(SectionInformation.Name);
                }
            }
            catch (ConfigurationException)
            {

            }
        }

        protected override object GetRuntimeObject()
        {
            return Value;
        }

        /// <summary>
        /// Creates an XML string containing an unmerged view of the <see cref="T:System.Configuration.ConfigurationSection" /> object as a single section to write to a file.
        /// Serializes the T value back to its source. If the section has an external source, writes to there at the specified xpath. If not, writes back embedded xml in the section itself.
        /// </summary>
        /// <param name="parentElement">The <see cref="T:System.Configuration.ConfigurationElement" /> instance to use as the parent when performing the un-merge.</param>
        /// <param name="name">The name of the section to create.</param>
        /// <param name="saveMode">The <see cref="T:System.Configuration.ConfigurationSaveMode" /> instance to use when writing to a string.</param>
        /// <returns>
        /// An XML string containing an unmerged view of the <see cref="T:System.Configuration.ConfigurationSection" /> object.
        /// </returns>
        protected override string SerializeSection(ConfigurationElement parentElement, string name, ConfigurationSaveMode saveMode)
        {
            lock (ConfigurationSystem.SyncRoot)
            {
                // prevent unintentional refresh of other sections
                ConfigurationSystem.FileSystemWatchers.Values.ForEach(fsw => fsw.EnableRaisingEvents = false);

                try
                {

                    // maybe this is wrong and we should clear the value...but for now, we leave the section in tact if the value is null
                    if (Value == null) return rawXml;

                    var configurationXml = new XmlDocument();

                    // Load current (old) xml from source file at xpath or section raw xml
                    if (Source == null)
                    {
                        configurationXml.LoadXml(rawXml);
                    }
                    else
                    {
                        using (var fs = new FileStream(Source, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                            configurationXml.Load(fs);
                    }

                    var configurationXmlElement = XPath == null ? configurationXml.DocumentElement : configurationXml.SelectSingleNode(XPath) as XmlElement;

                    // we couldn't find the xml in the source file (shouldn't happen since we loaded it from there in the first place)
                    if (configurationXmlElement == null) return rawXml;

                    var serializedXmlElement = SerializeValue(configurationXmlElement.Name);

                    configurationXmlElement.ParentNode.EnsureNotDefault().ReplaceChild(configurationXmlElement.OwnerDocument.EnsureNotDefault().ImportNode(serializedXmlElement, true), configurationXmlElement);

                    if (serializedXmlElement != null && configurationXmlElement.OuterXml != serializedXmlElement.OuterXml)
                        Save(serializedXmlElement, configurationXml);

                    // we should not embed any additional content in this file's section xml. If the value xml is embedded directly in this file, 
                    // we already updated it above by calling SetRawXml
                    return rawXml;
                }
                finally
                {
                    // Resume watching over configured paths
                    foreach (var fsw in ConfigurationSystem.FileSystemWatchers.ToList())
                    {
                        var obsoleteWatcher = false;
                        try
                        {
                            fsw.Value.EnableRaisingEvents = true;
                        }
                        catch (FileNotFoundException)
                        {
                            obsoleteWatcher = true;
                        }
                        catch (ArgumentException)
                        {
                            obsoleteWatcher = true;
                        }

                        // In case watched file path is no longer valid for some reason -> remove from list of watchers
                        if (obsoleteWatcher)
                        {
                            FileSystemWatcher removedWatcher;
                            ConfigurationSystem.FileSystemWatchers.TryRemove(fsw.Key, out removedWatcher);
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Saves the serialized XML element to the external file or directly in this section (if the Source is null).
        /// </summary>
        /// <param name="serializedXmlElement">The serialized XML element.</param>
        /// <param name="configurationXml">The configuration XML.</param>
        private void Save(XmlElement serializedXmlElement, XmlDocument configurationXml)
        {

            // prevent loopback
            isSaving = true;
            try
            {
                if (Source == null)
                {
                    // value xml is embedded right in this file
                    SectionInformation.SetRawXml(serializedXmlElement.OuterXml);
                }
                else
                {
                    var saveLocation = Source;
                    if (ConfigurationSavingToNewLocationScope.Current != null)
                    {
                        saveLocation = Path.Combine(ConfigurationSavingToNewLocationScope.Current.NewFolderForConfiguration,
                            Path.GetFileName(Source));
                    }
                    // value xml is in another file and we should save it
                    configurationXml.Save(saveLocation);
                }
                try
                {
                    ConfigurationManager.RefreshSection(SectionInformation.Name);
                }
                // may occur if file locked
                catch (ConfigurationException)
                {
                }
            }
            finally
            {
                isSaving = false;
            }
        }

        /// <summary>
        /// Serializes the T value.
        /// </summary>
        /// <param name="rootName">Name of the root.</param>
        /// <returns></returns>
        private XmlElement SerializeValue(string rootName)
        {
            var sb = new StringBuilder();

            var serializer = Serialization.CreateCachedXmlSerializerWithRoot(typeof(T), rootName);

            using (var xmlWriter = XmlWriter.Create(sb, new XmlWriterSettings { OmitXmlDeclaration = true }))
            {
                var ns = new XmlSerializerNamespaces();
                // prevent including xmlns
                ns.Add("", "");

                serializer.Serialize(xmlWriter, Value, ns);
            }

            var serializedXml = new XmlDocument();
            serializedXml.LoadXml(sb.ToString());
            var serializedXmlElement = serializedXml.DocumentElement;
            return serializedXmlElement;
        }

        protected override bool IsModified()
        {
            // required to support saving via System.Configuration.Configuration.Save().
            return true;
        }

        /// <summary>
        /// Deserializes the source xml at the specified xpath as an instance of T.
        /// </summary>
        /// <returns></returns>
        protected T DeserializeValue()
        {
            var xml = new XmlDocument();

            if (Source == null)
            {
                if (rawXml == null) return default(T);
                xml.LoadXml(rawXml);
            }
            else if (File.Exists(Source))
            {
                using (var fs = new FileStream(Source, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    xml.Load(fs);
            }

            XmlNode node = XPath == null ? xml.DocumentElement : xml.SelectSingleNode(XPath);

            if (node == null) return default(T);

            var serializer = Serialization.CreateCachedXmlSerializerWithRoot(typeof(T), node.Name);
            using (var sr = new StringReader(node.OuterXml))
            using (XmlReader xmlReader = XmlReader.Create(sr))
            {
                T result = serializer.Deserialize(xmlReader) as T;
                return result;
            }
        }
    }

    /// <summary>
    /// A configuration section that loads different data depending on the CurrentContext.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ContextSpecificXmlConfigurationSection<T> : XmlConfigurationSection<T> where T : class
    {
        private readonly Dictionary<object, T> cachedValues = new Dictionary<object, T>();

        /// <summary>
        /// Gets or sets the source. If there a CurrentContext file exists with an element at the specified xpath, uses that, otherwise uses the default source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        protected override string Source
        {
            get
            {
                var filePath = CurrentContextFileName;

                // If source represents relative path -> make it relative to config file instead of Environment.CurrenctDirectory
                if (!Path.IsPathRooted(filePath) && CurrentConfiguration != null && CurrentConfiguration.HasFile)
                {
                    filePath = Path.Combine(Path.GetDirectoryName(CurrentConfiguration.FilePath) ?? string.Empty, filePath);
                }

                // Transform C:\\test1\\test2\\..\\someapp.config into C:\\test1\\someapp.config
                filePath = Path.GetFullPath(filePath);

                // If file is not present at evaluated location -> walk up the directory structure
                if (!File.Exists(filePath) && TryFindSourceInParentDirectory)
                {
                    filePath = FindSourceInParentDirectory(filePath);
                }

                if (File.Exists(filePath))
                {
                    var document = new XmlDocument();
                    using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        document.Load(fs);

                    if (XPath != null && document.SelectSingleNode(XPath) != null) return filePath;
                }
                return base.Source;
            }
            set
            {
                base.Source = value;
            }
        }

        /// <summary>
        /// Gets the name of the current context file. By default uses the string representation of the CurrentContext + ".config" file extension.
        /// </summary>
        /// <value>
        /// The name of the current context file.
        /// </value>
        protected virtual string CurrentContextFileName
        {
            get { return CurrentContext + ".config"; }
        }

        /// <summary>
        /// Used as cache key for default source.
        /// </summary>
        private static readonly object NullCurrentContext = new object();

        /// <summary>
        /// Gets the current context. Derived types may return a "current tenant" for example.
        /// </summary>
        /// <value>
        /// The current context.
        /// </value>
        protected abstract object CurrentContext { get; }

        /// <summary>
        /// Gets the sources to watch. Watches all config files in the default source directory.
        /// </summary>
        /// <returns></returns>
        protected override IEnumerable<string> GetSourcesToWatch()
        {
            if (Source == null) return base.GetSourcesToWatch();

            // watch all config files in the same directory as Source
            var directory = Path.GetDirectoryName(Path.GetFullPath(Source));
            if (string.IsNullOrEmpty(directory)) return new string[0];

            return Directory.GetFiles(directory, "*.config");
        }

        /// <summary>
        /// Gets the deserialized section value. Caches deserialized result (until refreshed). Uses the CurrentContext source if one exists, otherwise the default source.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public override T Value
        {
            get
            {
                lock (this)
                {
                    T result;

                    // retrieve from cache
                    if (cachedValues.TryGetValue(CurrentContext ?? NullCurrentContext, out result)) return result;

                    if (Source != base.Source) // Client.config defines a custom configuration for this section
                    {
                        // deserialize client specific config and cache
                        return cachedValues[CurrentContext ?? NullCurrentContext] = DeserializeValue();
                    }
                }
                // not client specific
                return base.Value;
            }
        }

        protected override void Refresh()
        {
            base.Refresh();
            cachedValues.Clear();
        }
    }

    /// <summary>
    /// Sections do not receive new saving location when Configuration.SaveAs(newLocation...)
    /// Thats why we use SaveToNewLocation() extension method along with this scope
    /// </summary>
    internal class ConfigurationSavingToNewLocationScope : IScope
    {
        private readonly ConfigurationSavingToNewLocationScope previous;

        public ConfigurationSavingToNewLocationScope(string newPath)
        {
            NewFolderForConfiguration = Path.GetDirectoryName(newPath);
            previous = Current;
            Current = this;
        }

        public static ConfigurationSavingToNewLocationScope Current { get; private set; }

        /// <summary>
        /// Folder to store configuration
        /// </summary>
        public string NewFolderForConfiguration { get; private set; }

        public void Dispose()
        {
            Current = previous;
        }
    }

    /// <summary>
    /// Helpers for the .NET configuration system.
    /// </summary>
    public static class ConfigurationSystem
    {
        internal static readonly object SyncRoot = new object();

        internal static readonly ConcurrentDictionary<string, FileSystemWatcher> FileSystemWatchers = new ConcurrentDictionary<string, FileSystemWatcher>(StringComparer.OrdinalIgnoreCase);

        private static readonly FieldInfo ReadOnlyCollectionField = typeof(ConfigurationElementCollection).GetField("bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        private static readonly FieldInfo ReadOnlyElementField = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);

        /// <summary>
        /// Saves configuration to new location
        /// </summary>
        /// <param name="that">The that.</param>
        /// <param name="configurationPath">The configuration path.</param>
        public static void SaveToNewLocation(this System.Configuration.Configuration @that, string configurationPath)
        {
            using (new ConfigurationSavingToNewLocationScope(configurationPath))
            {
                @that.SaveAs(configurationPath, ConfigurationSaveMode.Modified, false);
            }
        }

        /// <summary>
        /// Sets the collection as read only or not (by force).
        /// </summary>
        /// <param name="c">The c.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetReadOnly(this ConfigurationElementCollection c, bool value)
        {
            if (c.IsReadOnly() != value)
            {
                ReadOnlyCollectionField.SetValue(c, value);
            }
        }

        /// <summary>
        /// Sets the element as read only or not (by force).
        /// </summary>
        /// <param name="e">The e.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetReadOnly(this ConfigurationElement e, bool value)
        {
            if (e.IsReadOnly() != value)
            {
                ReadOnlyElementField.SetValue(e, value);
            }
        }

        /// <summary>
        /// Gets the section with the specified name, casted as T. Uses XmlRootAttribute.ElementName or typeof(T).Name if name is omitted.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="configuration">The configuration.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static T GetSection<T>(this System.Configuration.Configuration configuration, string name = null) where T : class
        {
            var section = configuration.GetSection(GetSectionName<T>(name));
            if (section == null) return default(T);

            return (T)(dynamic)section;
        }

        private static string GetSectionName<T>(string name)
        {
            if (name == null && typeof(T).Name.Length > 2) name = typeof(T).GetAttribute<XmlRootAttribute>().IfNotNull(a => a.ElementName) ?? (typeof(T).Name.Substring(0, 1).ToLower() + typeof(T).Name.Substring(1));
            if (name == null) name = typeof(T).Name;
            return name;
        }

        /// <summary>
        /// Sets the section value of type T for the given Configuration.
        /// Creates a new wrapper XmlConfigurationSection for the section of type T if it does not exist.
        /// </summary>
        /// <typeparam name="T">The type of section</typeparam>
        /// <param name="configuration">The configuration</param>
        /// <param name="sectionName"> </param>
        /// <param name="section">The optional section value to set.  If not provided, a blank 'T' element is set.</param>
        /// <returns>Returns the newly set section.</returns>
        public static T SetSection<T>(this System.Configuration.Configuration configuration, T section = null, string sectionName = null) where T : class
        {
            sectionName = GetSectionName<T>(sectionName);
            var xmlConfigurationSection = configuration.GetSection(sectionName);
            if (xmlConfigurationSection == null)
            {
                // Create XmlConfigurationSection if it does not exist for the section of type 'T'.
                configuration.Sections.Add(sectionName, new XmlConfigurationSection<T>());
                xmlConfigurationSection = configuration.GetSection(sectionName);
            }

            xmlConfigurationSection.SectionInformation.SetRawXml(section == null ? "<{0} />".FormatWith(sectionName) : section.ToXml());
            return configuration.GetSection<T>();
        }
    }
}

namespace Soaf.Collections
{
    public static class Collections
    {
        /// <summary>
        /// Swaps the element at the 'fromIndex', with the element at the 'toIndex'.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="fromIndex">From index.</param>
        /// <param name="toIndex">To index.</param>
        public static void Swap(this IList source, int fromIndex, int toIndex)
        {
            var fromItem = source[fromIndex];
            var toItem = source[toIndex];
            if (fromIndex < toIndex)
            {
                source.RemoveAt(toIndex);
                source.RemoveAt(fromIndex);
                source.Insert(fromIndex, toItem);
                source.Insert(toIndex, fromItem);
            }
            else
            {
                source.RemoveAt(fromIndex);
                source.RemoveAt(toIndex);
                source.Insert(toIndex, fromItem);
                source.Insert(fromIndex, toItem);
            }

        }
    }

    public static class ObservableCollections
    {
        /// <summary>
        /// Creates an extended observable collection from the source.
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="source"> The source. </param>
        /// <returns> </returns>
        public static ExtendedObservableCollection<T> ToExtendedObservableCollection<T>(this IEnumerable<T> source)
        {
            return new ExtendedObservableCollection<T>(source);
        }

        /// <summary>
        /// Creates an observable collection from the source.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> source)
        {
            return new ObservableCollection<T>(source);
        }

        /// <summary>
        ///   Creates an observable dictionary from the source.
        /// </summary>
        /// <typeparam name="TKey"> The type of the key. </typeparam>
        /// <typeparam name="TValue"> The type of the value. </typeparam>
        /// <param name="source"> The source. </param>
        /// <returns> </returns>
        public static ObservableDictionary<TKey, TValue> ToObservableDictionary<TKey, TValue>(this IDictionary<TKey, TValue> source)
        {
            return new ObservableDictionary<TKey, TValue>(source);
        }
    }

    public class ObservableDictionary<TKey, TValue> : IDictionary<TKey, TValue>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        private const string CountString = "Count";
        private const string IndexerName = "Item[]";
        private const string KeysName = "Keys";
        private const string ValuesName = "Values";

        private IDictionary<TKey, TValue> dictionary;

        protected IDictionary<TKey, TValue> Dictionary
        {
            get { return dictionary; }
        }

        #region Constructors

        public ObservableDictionary()
        {
            dictionary = new Dictionary<TKey, TValue>();
        }

        public ObservableDictionary(IDictionary<TKey, TValue> dictionary)
        {
            this.dictionary = new Dictionary<TKey, TValue>(dictionary);
        }

        public ObservableDictionary(IEqualityComparer<TKey> comparer)
        {
            dictionary = new Dictionary<TKey, TValue>(comparer);
        }

        public ObservableDictionary(int capacity)
        {
            dictionary = new Dictionary<TKey, TValue>(capacity);
        }

        public ObservableDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
        {
            this.dictionary = new Dictionary<TKey, TValue>(dictionary, comparer);
        }

        public ObservableDictionary(int capacity, IEqualityComparer<TKey> comparer)
        {
            dictionary = new Dictionary<TKey, TValue>(capacity, comparer);
        }

        #endregion

        #region IDictionary<TKey,TValue> Members

        public void Add(TKey key, TValue value)
        {
            Insert(key, value, true);
        }

        public bool ContainsKey(TKey key)
        {
            return Dictionary.ContainsKey(key);
        }

        public ICollection<TKey> Keys
        {
            get { return Dictionary.Keys; }
        }

        public bool Remove(TKey key)
        {
            if (Equals(key, default(TKey))) throw new ArgumentNullException("key");

            TValue value;
            Dictionary.TryGetValue(key, out value);
            bool removed = Dictionary.Remove(key);
            if (removed)
                //OnCollectionChanged(NotifyCollectionChangedAction.Remove, new KeyValuePair<TKey, TValue>(key, value));
                OnCollectionChanged();
            return removed;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return Dictionary.TryGetValue(key, out value);
        }

        public ICollection<TValue> Values
        {
            get { return Dictionary.Values; }
        }

        public TValue this[TKey key]
        {
            get { return Dictionary[key]; }
            set { Insert(key, value, false); }
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            Insert(item.Key, item.Value, true);
        }

        public void Clear()
        {
            if (Dictionary.Count > 0)
            {
                Dictionary.Clear();
                OnCollectionChanged();
            }
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return Dictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            Dictionary.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return Dictionary.Count; }
        }

        public bool IsReadOnly
        {
            get { return Dictionary.IsReadOnly; }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return Dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)Dictionary).GetEnumerator();
        }

        #endregion

        #region INotifyCollectionChanged Members

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public void AddRange(IDictionary<TKey, TValue> items)
        {
            if (items == null) throw new ArgumentNullException("items");

            if (items.Count > 0)
            {
                if (Dictionary.Count > 0)
                {
                    if (items.Keys.Any(k => Dictionary.ContainsKey(k)))
                        throw new ArgumentException("An item with the same key has already been added.");

                    foreach (KeyValuePair<TKey, TValue> item in items) Dictionary.Add(item);
                }
                else
                    dictionary = new Dictionary<TKey, TValue>(items);

                OnCollectionChanged(NotifyCollectionChangedAction.Add, items.ToArray());
            }
        }

        private void Insert(TKey key, TValue value, bool add)
        {
            if (Equals(key, default(TKey))) throw new ArgumentNullException("key");

            TValue item;
            if (Dictionary.TryGetValue(key, out item))
            {
                if (add) throw new ArgumentException("An item with the same key has already been added.");
                if (Equals(item, value)) return;
                Dictionary[key] = value;

                OnCollectionChanged(NotifyCollectionChangedAction.Replace, new KeyValuePair<TKey, TValue>(key, value), new KeyValuePair<TKey, TValue>(key, item));
            }
            else
            {
                Dictionary[key] = value;

                OnCollectionChanged(NotifyCollectionChangedAction.Add, new KeyValuePair<TKey, TValue>(key, value));
            }
        }

        private void OnPropertyChanged()
        {
            OnPropertyChanged(CountString);
            OnPropertyChanged(IndexerName);
            OnPropertyChanged(KeysName);
            OnPropertyChanged(ValuesName);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnCollectionChanged()
        {
            OnPropertyChanged();
            if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, KeyValuePair<TKey, TValue> changedItem)
        {
            OnPropertyChanged();
            if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(action, changedItem));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, KeyValuePair<TKey, TValue> newItem, KeyValuePair<TKey, TValue> oldItem)
        {
            OnPropertyChanged();
            if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(action, newItem, oldItem));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, IList newItems)
        {
            OnPropertyChanged();
            if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(action, newItems));
        }
    }

    /// <summary>
    /// An observable collection that supports tracking items and background thread access.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [CollectionDataContract(Name = "ExtendedObservableCollection`1[[{0}]]")]
    public class ExtendedObservableCollection<T> : ObservableCollection<T>, IEditableObjectExtended, IRaiseCollectionChanged, IRaisePropertyChanged, ICollectionViewFactory
    {
        private readonly Dictionary<string, CapturedState> edits = new Dictionary<string, CapturedState>();

        /// <summary>
        /// Used to track changed state internally.
        /// IMPORTANT: changing back all items in collection won't cause IsChanged to be reverted to False yet. This is todo.
        /// </summary>
        private bool isChanged;

        public ExtendedObservableCollection()
        {
            Initialize();
        }

        public ExtendedObservableCollection(IEnumerable<T> source)
            : base(source)
        {
            Initialize();
        }

        private void Initialize()
        {
            Dispatcher = System.Windows.Application.Current != null ? System.Windows.Application.Current.Dispatcher : null;
            TrackItemsPropertyChanged = true;
            RaiseEventsUsingDispatcher = true;
            CascadeEditingEventsToChildren = true;

            foreach (INotifyPropertyChanged item in this.OfType<INotifyPropertyChanged>())
            {
                item.PropertyChanged += new PropertyChangedEventHandler(OnItemPropertyChanged).MakeWeak();
            }

            // Clear changes (if any) once initializing over
            isChanged = false;
        }

        /// <summary>
        /// Gets or sets the dispatcher to use when raising events. Default is the current dispatcher when the collection was instantiated.
        /// </summary>
        /// <value>
        /// The dispatcher.
        /// </value>
        public virtual Dispatcher Dispatcher { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether to fire Item[] changed when a child changes. Default is true.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [track children]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool TrackItemsPropertyChanged { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether to raise events using dispatcher, instead of on the calling thread. Default is true.
        /// </summary>
        /// <value>
        /// <c>true</c> if [raise events using dispatcher]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool RaiseEventsUsingDispatcher { get; set; }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            // Mark it as changed only when outside suppressive change tracking scope
            if (SuppressChangeTrackingScope.Current == null) isChanged = true;

            if (TrackItemsPropertyChanged)
            {
                // Manage event subscriptions to individual items
                if (e.OldItems != null)
                {
                    foreach (INotifyPropertyChanged item in e.OldItems.OfType<INotifyPropertyChanged>())
                    {
                        item.PropertyChanged -= new PropertyChangedEventHandler(OnItemPropertyChanged).MakeWeak();
                    }
                }

                if (e.NewItems != null)
                {
                    foreach (INotifyPropertyChanged item in e.NewItems.OfType<INotifyPropertyChanged>())
                    {
                        item.PropertyChanged += new PropertyChangedEventHandler(OnItemPropertyChanged).MakeWeak();
                    }
                }
            }

            if (SuppressPropertyChangedScope.Current == null)
            {
                OptionallyExecuteViaDispatcher(() => base.OnCollectionChanged(e));
            }
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (SuppressPropertyChangedScope.Current != null) return;

            OptionallyExecuteViaDispatcher(() => base.OnPropertyChanged(e));
        }

        protected override void ClearItems()
        {
            // Unsubscribe from remaining items, since CollectionChanged(Reset) doesn't have a list of items
            foreach (INotifyPropertyChanged item in this.OfType<INotifyPropertyChanged>())
            {
                item.PropertyChanged -= new PropertyChangedEventHandler(OnItemPropertyChanged).MakeWeak();
            }

            base.ClearItems();
        }

        /// <summary>
        /// Fires OnPropertyChanged.
        /// </summary>
        public void FirePropertyChanged()
        {
            OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));
        }

        public void AcceptChanges()
        {
            if (CascadeEditingEventsToChildren)
            {
                this.OfType<IChangeTracking>().ForEach(i => i.AcceptChanges());
            }
            isChanged = false;
        }

        public bool IsChanged
        {
            get { return isChanged || (CascadeEditingEventsToChildren && this.OfType<IChangeTracking>().Any(i => i.IsChanged)); }
        }

        public bool IsEditing
        {
            get { return edits.Any(); }
        }

        public bool CascadeEditingEventsToChildren { get; set; }

        public void BeginEdit()
        {
            DoBeginNamedEdit(string.Empty);
            DoCascadeEditingEventToChildren<IEditableObject>(string.Empty, i => i.BeginEdit());
        }

        public void CancelEdit()
        {
            DoCascadeEditingEventToChildren<IEditableObject>(string.Empty, i => i.CancelEdit());
            DoCancelNamedEdit(string.Empty);
        }

        public void EndEdit()
        {
            DoCascadeEditingEventToChildren<IEditableObject>(string.Empty, i => i.EndEdit());
            DoEndNamedEdit(string.Empty);
        }

        public void BeginNamedEdit(string name)
        {
            DoBeginNamedEdit(name);
            DoCascadeEditingEventToChildren<IEditableObjectExtended>(name, i => i.BeginNamedEdit(name));
        }

        public void CancelNamedEdit(string name)
        {
            DoCascadeEditingEventToChildren<IEditableObjectExtended>(name, i => i.CancelNamedEdit(name));
            DoCancelNamedEdit(name);
        }

        public void EndNamedEdit(string name)
        {
            DoCascadeEditingEventToChildren<IEditableObjectExtended>(name, i => i.EndNamedEdit(name));
            DoEndNamedEdit(name);
        }

        public Dictionary<string, object> GetOriginalValues()
        {
            return DoGetOriginalValuesForNamedEdit(string.Empty);
        }

        public Dictionary<string, object> GetOriginalValuesForNamedEdit(string name)
        {
            return DoGetOriginalValuesForNamedEdit(name);
        }

        void IRaiseCollectionChanged.OnCollectionChanged()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        void IRaisePropertyChanged.OnPropertyChanged(PropertyChangedEventArgs args)
        {
            OnPropertyChanged(args);
        }

        private void DoBeginNamedEdit(string name)
        {
            lock (edits)
            {
                // Already editing with this name?
                if (edits.ContainsKey(name)) return;

                // Capture current state
                var capturedState = new CapturedState(this, isChanged);
                edits.Add(name, capturedState);
            }
        }

        private void DoCancelNamedEdit(string name)
        {
            lock (edits)
            {
                // Not editing?
                if (!edits.ContainsKey(name)) return;

                // Clear
                Clear();

                // Restore to previous state
                var capturedState = edits[name];
                edits.Remove(name);
                capturedState.PreEditContents.ForEach(Add);
                isChanged = capturedState.PreEditIsChanged;
            }
        }

        private void DoEndNamedEdit(string name)
        {
            lock (edits)
            {
                // Not editing?
                if (!edits.ContainsKey(name)) return;

                // Abandon captured state
                edits.Remove(name);
            }
        }

        private Dictionary<string, object> DoGetOriginalValuesForNamedEdit(string name)
        {
            lock (edits)
            {
                // Not editing?
                if (!edits.ContainsKey(name)) return null;

                var capturedState = edits[name];
                var newExtendedObserableCollection = new ExtendedObservableCollection<T>();
                capturedState.PreEditContents.ForEach(newExtendedObserableCollection.Add);
                return new Dictionary<string, object>
                           {
                               {"Items", newExtendedObserableCollection}
                           };
            }
        }

        private void DoCascadeEditingEventToChildren<TChild>(string editName, Action<TChild> action)
        {
            // Check if cascading
            if (!CascadeEditingEventsToChildren) return;

            // Get pre edit contents to include in cascade
            var preEditContents = edits.ContainsKey(editName)
                                      ? edits[editName].PreEditContents
                                      : new List<T>();

            // Cascade
            var editables = preEditContents
                .OfType<TChild>()
                .Concat(this.OfType<TChild>())
                .Distinct(ObjectReferenceEqualityComparerer<TChild>.Instance)
                .ToList();
            editables.ForEach(action);
        }

        private void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (TrackItemsPropertyChanged)
            {
                OnPropertyChanged(new CascadingPropertyChangedEventArgs("Item", false, sender, e));
            }
        }

        private void OptionallyExecuteViaDispatcher(Action action)
        {
            if (RaiseEventsUsingDispatcher && Dispatcher != null && !Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(action);
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// Captured state when edit begins
        /// </summary>
        private class CapturedState
        {
            public CapturedState(IEnumerable<T> preEditContents, bool preEditIsChanged)
            {
                PreEditContents = new List<T>(preEditContents);
                PreEditIsChanged = preEditIsChanged;
            }

            public bool PreEditIsChanged { get; private set; }
            public List<T> PreEditContents { get; private set; }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public ICollectionView CreateView()
        {
            ICollectionView collectionView;

            // Let's see if we need to return ICollectionView for Telerik's Grid which doesn't work with ListCollectionView
            // Are we being called from within Telerik's GridViewDataControl?
            var callerMethod = new StackFrame(1, true).GetMethod();
            if (callerMethod != null
                && callerMethod.DeclaringType != null
                && callerMethod.DeclaringType.FullName.StartsWith("Telerik.Windows.Controls.GridView"))
            {
                var queryableCollectionViewType = Type.GetType("Telerik.Windows.Data.QueryableCollectionView, Telerik.Windows.Data", true);
                collectionView = (ICollectionView)Activator.CreateInstance(queryableCollectionViewType, this, typeof(T));
            }
            else
            {
                collectionView = new WeakListCollectionView(this);
            }

            // Do not preselect the first item
            collectionView.MoveCurrentToPosition(-1);

            return collectionView;
        }
    }

    public class WeakListCollectionView : ListCollectionView
    {
        public WeakListCollectionView(IList list)
            : base(list)
        {
            var notifyCollectionChanged = list as INotifyCollectionChanged;
            if (notifyCollectionChanged != null)
            {
                // ListCollectionView subscribes to OnCollectionChanged in the constructor. Unsubscribe first using non-weak event
                notifyCollectionChanged.CollectionChanged -= OnCollectionChanged;
                notifyCollectionChanged.CollectionChanged += new NotifyCollectionChangedEventHandler(OnCollectionChanged).MakeWeak<NotifyCollectionChangedEventHandler>();
            }
        }

        // TODO: add once migrated to .NET 4.5
        //public override void DetachFromSourceCollection()
        //{
        //    // Unsubscribe from CollectionChanged weak event
        //    var notifyCollectionChanged = SourceCollection as INotifyCollectionChanged;
        //    if (notifyCollectionChanged != null)
        //    {
        //        notifyCollectionChanged.CollectionChanged -= new NotifyCollectionChangedEventHandler(OnCollectionChanged).MakeWeak<NotifyCollectionChangedEventHandler>();
        //    }
        //}
    }

    /// <summary>
    /// An ObservableCollection with set-like behavior
    /// </summary>
    [CollectionDataContract(Name = "ObservableSet`1[[{0}]]")]
    public class ObservableSet<T> : ExtendedObservableCollection<T>
    {
        public ObservableSet()
        { }

        public ObservableSet(IEnumerable<T> items)
        {
            this.AddRangeWithSinglePropertyChangedNotification(items);
        }

        protected override void InsertItem(int index, T item)
        {
            int foundAt = IndexOf(item);

            // Index == Count when new items are appended to this set
            if (foundAt >= 0 && foundAt != index && index != Count)
            {
                throw new ArgumentException("Item already exists", "item");
            }

            // Insert items only if they are not duplicates
            if (foundAt < 0)
            {
                base.InsertItem(index, item);
            }
        }

        protected override void SetItem(int index, T item)
        {
            int foundAt = IndexOf(item);

            // Insert items only if they are not duplicates
            if (foundAt >= 0 && foundAt != index)
            {
                throw new ArgumentException("Item already exists", "item");
            }

            // Actually set only non-duplicates
            if (foundAt < 0)
            {
                base.SetItem(index, item);
            }
        }
    }
}