﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Soaf;
using Soaf.ComponentModel;

[assembly: Component(typeof(Messenger), typeof(IMessenger))]

namespace Soaf
{
    /// <summary>
    ///   A signature for a method that handles published messages.
    /// </summary>
    public delegate void MessageHandler(IMessenger messenger, object token, object message);

    /// <summary>
    ///   A signature for a method that handles published messages.
    /// </summary>
    /// <typeparam name="TMessage"> The type of the message. </typeparam>
    /// <param name="messenger"> The message messenger. </param>
    /// <param name="token"> The token. </param>
    /// <param name="message"> The message. </param>
    public delegate void MessageHandler<in TMessage>(IMessenger messenger, object token, TMessage message);

    /// <summary>
    ///   A menas of transporting messages across an application.
    /// </summary>
    public interface IMessenger : IDisposable
    {
        /// <summary>
        ///   Publishes the specified message to all subscribers for the message's type.
        /// </summary>
        /// <param name="message"> The message. </param>
        void Publish(object message);

        /// <summary>
        ///   Subscribes for publications of the specified message type.
        /// </summary>
        /// <param name="messageType"> Type of the message. </param>
        /// <param name="handler"> The handler. </param>
        /// <param name="useWeakReference"> if set to <c>true</c> [use weak reference]. </param>
        /// <returns> A token for the subscription created. </returns>
        object Subscribe(Type messageType, MessageHandler handler, bool useWeakReference = false);

        /// <summary>
        ///   Unsubscribes the handler for the specified token for publications.
        /// </summary>
        void Unsubscribe(object token);

        /// <summary>
        ///   Unsubscribes for all publications of the specified message type.
        /// </summary>
        /// <param name="messageType"> Type of the message. </param>
        void Unsubscribe(Type messageType);
    }

    /// <summary>
    ///   Helper methods for a IMessengeres
    /// </summary>
    public static class MessengerExtensions
    {
        /// <summary>
        ///   Subscribes for publications of the specified message type.
        /// </summary>
        /// <typeparam name="TMessage"> The type of the message. </typeparam>
        /// <param name="messenger"> The messenger. </param>
        /// <param name="handler"> The handler. </param>
        /// <param name="filter"> The filter. </param>
        /// <param name="useWeakReference"> if set to <c>true</c> [use weak reference]. </param>
        /// <returns> A token for the subscription. </returns>
        public static object Subscribe<TMessage>(this IMessenger messenger, MessageHandler<TMessage> handler, Func<TMessage, bool> filter = null, bool useWeakReference = false)
        {
            return messenger.Subscribe(typeof(TMessage), (sender, t, m) => { if (filter == null || filter((TMessage)m)) handler(sender, t, (TMessage)m); }, useWeakReference);
        }

        /// <summary>
        ///   Subscribes for publications of the specified message type.
        /// </summary>
        /// <typeparam name="TMessage"> The type of the message. </typeparam>
        /// <param name="messenger"> The messenger. </param>
        /// <param name="handler"> The handler. </param>
        /// <param name="filter"> The filter. </param>
        /// <param name="useWeakReference"> if set to <c>true</c> [use weak reference]. </param>
        /// <returns> A token for the subscription. </returns>
        public static object Subscribe<TMessage>(this IMessenger messenger, MessageHandler<TMessage> handler, Func<IMessenger, object, TMessage, bool> filter, bool useWeakReference = false)
        {
            return messenger.Subscribe(typeof(TMessage), (sender, t, m) => { if (filter == null || filter(sender, t, (TMessage)m)) handler(sender, t, (TMessage)m); }, useWeakReference);
        }

        /// <summary>
        ///   Unsubscribes for publications of the specified message type.
        /// </summary>
        /// <typeparam name="TMessage"> The type of the message. </typeparam>
        /// <param name="messenger"> The messenger. </param>
        public static void Unsubscribe<TMessage>(this IMessenger messenger)
        {
            messenger.Unsubscribe(typeof(TMessage));
        }
    }

    /// <summary>
    ///   A very simple default implementation of an IMessenger.
    /// </summary>
    [Singleton]
    public class Messenger : IMessenger
    {
        /// <summary>
        ///   Subscriptions managed by this IMessenger instance.
        /// </summary>
        private readonly IDictionary<object, Subscription> subscriptions = new Dictionary<object, Subscription>();

        private readonly ReaderWriterLockSlim syncRoot = new ReaderWriterLockSlim();

        public Messenger()
        {
            ScavengingTimer = new Timer(s => Scavenge());
            ScavengingTimer.Change(DefaultScavengingPeriod, DefaultScavengingPeriod);
        }

        internal Timer ScavengingTimer { get; private set; }

        public static int DefaultScavengingPeriod { get; set; }

        #region IMessenger Members

        public void Publish(object message)
        {
            message.EnsureNotDefault(new ArgumentNullException("message"));

            var subscriptionsToHandle = new List<Subscription>();

            Type messageType = message.GetType();
            syncRoot.EnterReadLock();
            subscriptionsToHandle.AddRange(subscriptions.Values.Where(i => i.MessageType.IsAssignableFrom(messageType)));
            syncRoot.ExitReadLock();
            var exceptions = new List<Exception>();
            // go through all subscriptions on all active IMessengeres and publish to subscriptions for this message's type
            foreach (Subscription subscription in subscriptionsToHandle)
            {
                try
                {
                    HandleSubscription(subscription, message);
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }
            if (exceptions.Count > 0) throw new AggregateException(exceptions);
        }

        public object Subscribe(Type messageType, MessageHandler handler, bool useWeakReference = false)
        {
            messageType.EnsureNotDefault(new ArgumentNullException("messageType"));
            handler.EnsureNotDefault(new ArgumentNullException("handler"));

            var token = new object();
            var subscription = new Subscription(messageType, handler, token, useWeakReference);
            syncRoot.EnterWriteLock();
            try
            {
                subscriptions.Add(subscription.Token, subscription);
            }
            finally
            {
                syncRoot.ExitWriteLock();
            }
            return token;
        }

        public void Unsubscribe(object token)
        {
            token.EnsureNotDefault(new ArgumentNullException("token"));
            syncRoot.EnterWriteLock();
            try
            {
                if (!subscriptions.ContainsKey(token)) throw new InvalidOperationException("Subscription for token was not found.");
                subscriptions.Remove(token);
            }
            finally
            {
                syncRoot.ExitWriteLock();
            }
        }

        public void Unsubscribe(Type messageType)
        {
            messageType.EnsureNotDefault(new ArgumentNullException("messageType"));
            syncRoot.EnterWriteLock();
            try
            {
                subscriptions.Values.Where(s => s.MessageType.IsAssignableFrom(messageType)).ToList().ForEach(s => subscriptions.Remove(s));
            }
            finally
            {
                syncRoot.ExitWriteLock();
            }
        }

        #endregion

        private void Scavenge()
        {
            syncRoot.EnterUpgradeableReadLock();

            foreach (Subscription subscription in subscriptions.Values.ToList())
            {
                if (subscription.Handler == null)
                {
                    syncRoot.EnterWriteLock();
                    subscriptions.Remove(subscription.Token);
                    syncRoot.ExitWriteLock();
                }
            }

            syncRoot.ExitUpgradeableReadLock();
        }

        /// <summary>
        ///   Fires the handler for the subscription if applicable for the message.
        /// </summary>
        /// <param name="subscription"> The subscription. </param>
        /// <param name="message"> The message. </param>
        private void HandleSubscription(Subscription subscription, object message)
        {
            MessageHandler handler = subscription.Handler;
            if (handler != null)
            {
                handler(this, subscription.Token, message);
            }
        }

        #region Nested type: Subscription

        /// <summary>
        ///   A subscription for a message type.
        /// </summary>
        private class Subscription
        {
            private readonly bool useWeakReference;
            private MessageHandler handler;
            private WeakReference<MessageHandler> handlerReference;

            public Subscription(Type messageType, MessageHandler handler, object token, bool useWeakReference)
            {
                MessageType = messageType;
                Token = token;
                this.useWeakReference = useWeakReference;

                Handler = handler;
            }

            public Type MessageType { get; private set; }

            public MessageHandler Handler
            {
                get { return useWeakReference ? handlerReference.Target : handler; }
                private set
                {
                    if (useWeakReference)
                    {
                        handlerReference = new WeakReference<MessageHandler>(value);
                    }
                    else
                    {
                        handler = value;
                    }
                }
            }

            public object Token { get; private set; }
        }

        #endregion

        public void Dispose()
        {
            ScavengingTimer.Dispose();
            subscriptions.Clear();
        }
    }
}