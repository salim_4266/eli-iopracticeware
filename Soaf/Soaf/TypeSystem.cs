﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;
using Soaf.Threading;
using Soaf.TypeSystem;

[assembly: Component(typeof(AttributeDisassembler), typeof(IAttributeDisassembler))]
[assembly: Component(typeof(TypeSystemRepository), typeof(ITypeSystemRepository))]
[assembly: Component(typeof(DynamicModuleFactory), typeof(IDynamicModuleFactory))]
[assembly: Component(typeof(TypeGenerator), typeof(ITypeGenerator))]

// ReSharper disable CheckNamespace
namespace Soaf.TypeSystem
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Visits a CLR MemberInfo type system graph.
    /// </summary>
    internal class MemberInfoGraph : IEnumerable<MemberInfo>
    {
        private readonly HashSet<MemberInfo> visitedMembers = new HashSet<MemberInfo>();

        public MemberInfoGraph(params MemberInfo[] membersToVisit)
        {
            membersToVisit.ForEach(Visit);
        }

        #region IEnumerable<MemberInfo> Members

        public IEnumerator<MemberInfo> GetEnumerator()
        {
            return visitedMembers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public virtual void Visit(MemberInfo item)
        {
            if (visitedMembers.Add(item))
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (item.DeclaringType != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    Visit(item.DeclaringType);
                }

                switch (item.MemberType)
                {
                    case MemberTypes.TypeInfo:
                        VisitType(item.CastTo<Type>());
                        break;
                    case MemberTypes.NestedType:
                        VisitType(item.CastTo<Type>());
                        break;
                    case MemberTypes.Property:
                        VisitProperty(item.CastTo<PropertyInfo>());
                        break;
                    case MemberTypes.Method:
                        VisitMethod(item.CastTo<MethodInfo>());
                        break;
                    case MemberTypes.Constructor:
                        VisitConstructor(item.CastTo<ConstructorInfo>());
                        break;
                }
            }
        }

        public virtual void VisitConstructor(ConstructorInfo item)
        {
            item.GetParameterTypes().ForEach(Visit);
        }

        public virtual void VisitType(Type item)
        {
            var toVisit = new[] { item.BaseType, item.GetElementType(), (item.IsGenericType ? item.GetGenericTypeDefinition() : null) }
                .Concat(item.GetInterfaces().OfType<MemberInfo>())
                .Concat((item.IsGenericType ? item.GetGenericArguments() : new Type[] { }));

            if (!item.Assembly.FullName.StartsWith("System") && !item.Assembly.FullName.StartsWith("mscorlib") && !item.Assembly.FullName.StartsWith("Microsoft"))
            {
                toVisit = toVisit.Concat(item.GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).Where(m => (m.MemberType == MemberTypes.Property && m.CastTo<PropertyInfo>().CanRead) || (m.MemberType == MemberTypes.Method && !m.CastTo<MethodInfo>().IsSpecialName)));
            }

            toVisit.WhereNotDefault().ForEach(Visit);
        }

        public virtual void VisitMethod(MethodInfo item)
        {
            new[] { item.ReturnType as MemberInfo, (item.IsGenericMethod ? item.GetGenericMethodDefinition() : null) }
                .Concat(item.GetParameterTypes().OfType<MemberInfo>())
                .Concat(item.IsGenericMethod ? item.GetGenericArguments() : new Type[] { })
                .WhereNotDefault().ForEach(Visit);
        }

        public virtual void VisitProperty(PropertyInfo item)
        {
            Visit(item.PropertyType);
        }
    }

    /// <summary>
    /// Creates a meta type system graph from CLR MemberInfos.
    /// </summary>
    internal class MemberInfoToMetaTypeSystemConverter : IEnumerable<KeyValuePair<MemberInfo, MetaMember>>
    {
        private readonly IDictionary<MemberInfo, MetaMember> map = new Dictionary<MemberInfo, MetaMember>();

        public MemberInfoToMetaTypeSystemConverter(MemberInfo memberInfo, IDictionary<MemberInfo, MetaMember> existingSource = null)
        {
            foreach (MemberInfo i in new MemberInfoGraph(memberInfo))
            {
                MetaMember metaMember;
                if (existingSource != null && existingSource.TryGetValue(i, out metaMember))
                {
                    map.Add(i, metaMember);
                }
                else
                {
                    switch (i.MemberType)
                    {
                        case MemberTypes.NestedType:
                        case MemberTypes.TypeInfo:
                            map[i] = new MetaType();
                            break;
                        case MemberTypes.Method:
                            map[i] = new MetaMethod();
                            break;
                        case MemberTypes.Property:
                            map[i] = new MetaProperty();
                            break;
                        case MemberTypes.Constructor:
                            map[i] = new MetaConstructor();
                            break;
                    }
                }
            }

            foreach (var i in map.ToArray())
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (i.Key.DeclaringType != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    i.Value.DeclaringType = map[i.Key.DeclaringType].CastTo<MetaType>();
                }
                switch (i.Key.MemberType)
                {
                    case MemberTypes.NestedType:
                    case MemberTypes.TypeInfo:
                        VisitType(i.Key.CastTo<Type>(), i.Value.CastTo<MetaType>());
                        break;
                    case MemberTypes.Method:
                        VisitMethod(i.Key.CastTo<MethodInfo>(), i.Value.CastTo<MetaMethod>());
                        break;
                    case MemberTypes.Property:
                        VisitProperty(i.Key.CastTo<PropertyInfo>(), i.Value.CastTo<MetaProperty>());
                        break;
                    case MemberTypes.Constructor:
                        VisitConstructor(i.Key.CastTo<ConstructorInfo>(), i.Value.CastTo<MetaConstructor>());
                        break;
                }
            }
        }

        #region IEnumerable<KeyValuePair<MemberInfo,MetaMember>> Members

        public IEnumerator<KeyValuePair<MemberInfo, MetaMember>> GetEnumerator()
        {
            return map.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        protected virtual void VisitType(Type type, MetaType metaType)
        {
            metaType.AssemblyName = type.Assembly.FullName;
            metaType.Name = type.Name;
            metaType.Namespace = type.Namespace;

            if (type.BaseType != null)
            {
                metaType.BaseType = map[type.BaseType].CastTo<MetaType>();
            }
            if (type.IsGenericParameter && type.DeclaringMethod != null)
            {
                metaType.DeclaringMethod = map[type.DeclaringMethod].CastTo<MetaMethod>();
            }
            if (type.GetElementType() != null)
            {
                metaType.ElementType = map[type.GetElementType()].CastTo<MetaType>();
            }

            metaType.GenericArguments = type.IsGenericType ? type.GetGenericArguments().Select(i => map[i].CastTo<MetaType>()).ToList() : new List<MetaType>();
            metaType.GenericTypeDefinition = type.IsGenericType ? map[type.GetGenericTypeDefinition()].CastTo<MetaType>() : null;
            metaType.Interfaces = type.GetInterfaces().Select(i => map[i].CastTo<MetaType>()).ToList();
            metaType.IsAbstract = type.IsAbstract;
            metaType.IsByRef = type.IsByRef;
            metaType.IsInterface = type.IsInterface;
            metaType.IsPrimitive = type.IsPrimitive;
            metaType.IsArray = type.IsArray;
            metaType.IsGenericTypeDefinition = type.IsGenericTypeDefinition;
            metaType.IsGenericParameter = type.IsGenericParameter;

            if (!type.Assembly.FullName.StartsWith("System") && !type.Assembly.FullName.StartsWith("mscorlib") && !type.Assembly.FullName.StartsWith("Microsoft"))
            {
                metaType.Members = type.GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).Where(i => (i.MemberType == MemberTypes.Property && i.CastTo<PropertyInfo>().CanRead) || (i.MemberType == MemberTypes.Method && !i.CastTo<MethodInfo>().IsSpecialName) || (i.MemberType == MemberTypes.Constructor && i.CastTo<ConstructorInfo>().GetParameters().Length > 0)).Select(i => map.GetValue(i)).WhereNotDefault().ToList();
            }
        }

        protected virtual void VisitMethod(MethodInfo method, MetaMethod metaMethod)
        {
            metaMethod.GenericArguments = method.IsGenericMethod ? method.GetGenericArguments().Select(i => map[i].CastTo<MetaType>()).ToList() : new List<MetaType>();
            metaMethod.GenericMethodDefinition = method.IsGenericMethod && !method.IsGenericMethodDefinition ? map[method.GetGenericMethodDefinition()].CastTo<MetaMethod>() : null;
            metaMethod.IsGenericMethodDefinition = method.IsGenericMethodDefinition;
            metaMethod.IsAbstract = method.IsAbstract;
            metaMethod.IsVirtual = method.IsVirtual;
            metaMethod.IsGenericMethodDefinition = method.IsGenericMethodDefinition;
            metaMethod.Name = method.Name;
            metaMethod.Parameters = method.GetParameters().Select(p =>
                                                                  new MetaParameter
                                                                      {
                                                                          Name = p.Name,
                                                                          IsOut = p.IsOut,
                                                                          ParameterType = map[p.ParameterType].CastTo<MetaType>()
                                                                      }).ToList();
            metaMethod.ReturnType = map[method.ReturnType].CastTo<MetaType>();
        }

        protected virtual void VisitProperty(PropertyInfo property, MetaProperty metaProperty)
        {
            metaProperty.IsAbstract = property.GetGetMethod(true).IsAbstract;
            metaProperty.IsVirtual = property.GetGetMethod(true).IsVirtual;
            metaProperty.Name = property.Name;
            metaProperty.PropertyType = map[property.PropertyType].CastTo<MetaType>();
        }

        protected virtual void VisitConstructor(ConstructorInfo constructor, MetaConstructor metaConstructor)
        {
            metaConstructor.Name = constructor.Name;
            metaConstructor.Parameters = constructor.GetParameters().Select(p =>
                                                         new MetaParameter
                                                         {
                                                             Name = p.Name,
                                                             IsOut = p.IsOut,
                                                             ParameterType = map[p.ParameterType].CastTo<MetaType>()
                                                         }).ToList();
        }

    }

    /// <summary>
    /// Visits a meta type system graph.
    /// </summary>
    internal class MetaMemberGraph : IEnumerable<MetaMember>
    {
        private readonly HashSet<MetaMember> visitedMembers = new HashSet<MetaMember>(ObjectReferenceEqualityComparerer<MetaMember>.Instance);

        public MetaMemberGraph(params MetaMember[] membersToVisit)
        {
            membersToVisit.ForEach(Visit);
        }

        #region IEnumerable<MetaMember> Members

        public IEnumerator<MetaMember> GetEnumerator()
        {
            return visitedMembers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public virtual void Visit(MetaMember item)
        {
            if (visitedMembers.Add(item))
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (item.DeclaringType != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    Visit(item.DeclaringType);
                }

                switch (item.MemberType)
                {
                    case MetaMemberType.Type:
                        VisitType(item.CastTo<MetaType>());
                        break;
                    case MetaMemberType.Property:
                        VisitProperty(item.CastTo<MetaProperty>());
                        break;
                    case MetaMemberType.Method:
                        VisitMethod(item.CastTo<MetaMethod>());
                        break;
                    case MetaMemberType.Constructor:
                        VisitConstructor(item.CastTo<MetaConstructor>());
                        break;
                }
            }
        }

        public virtual void VisitConstructor(MetaConstructor item)
        {
            item.Parameters.Select(i => i.ParameterType).ForEach(Visit);
        }

        public virtual void VisitType(MetaType item)
        {
            new[] { item.BaseType, item.ElementType, item.GenericTypeDefinition }
                .Concat(item.Interfaces.OfType<MetaMember>())
                .Concat(item.GenericArguments.OfType<MetaMember>())
                .Concat(item.Members)
                .WhereNotDefault()
                .ForEach(Visit);
        }

        public virtual void VisitMethod(MetaMethod item)
        {
            new[] { item.ReturnType as MetaMember, item.GenericMethodDefinition }
                .Concat(item.Parameters.Select(p => p.ParameterType).OfType<MetaMember>())
                .Concat(item.GenericArguments.OfType<MetaMember>())
                .WhereNotDefault().ForEach(Visit);
        }

        public virtual void VisitProperty(MetaProperty item)
        {
            Visit(item.PropertyType);
        }
    }

    /// <summary>
    /// Rewrites a meta type system graph, copying all the contained nodes.
    /// </summary>
    internal class MetaMemberRewriter : IEnumerable<KeyValuePair<MetaMember, MetaMember>>
    {
        private readonly IDictionary<MetaMember, MetaMember> map = new Dictionary<MetaMember, MetaMember>(ObjectReferenceEqualityComparerer<MetaMember>.Instance);
        private readonly HashSet<MetaMember> existingMembers = new HashSet<MetaMember>(ObjectReferenceEqualityComparerer<MetaMember>.Instance);

        public MetaMemberRewriter(MetaMember metaMember, Func<MetaMember, MetaMember> existingMemberSelector = null)
        {
            foreach (MetaMember i in new MetaMemberGraph(metaMember))
            {
                MetaMember newMember = null;
                if (existingMemberSelector != null)
                {
                    newMember = existingMemberSelector(i);
                    existingMembers.Add(newMember);
                }
                if (newMember == null)
                {
                    switch (i.MemberType)
                    {
                        case MetaMemberType.Type:
                            newMember = new MetaType();
                            break;
                        case MetaMemberType.Method:
                            newMember = new MetaMethod();
                            break;
                        case MetaMemberType.Property:
                            newMember = new MetaProperty();
                            break;
                        case MetaMemberType.Constructor:
                            newMember = new MetaConstructor();
                            break;
                    }
                }
                map.Add(i, newMember);
            }

            foreach (var i in map.Where(i => !existingMembers.Contains(i.Value)))
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (i.Key.DeclaringType != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    i.Value.DeclaringType = map[i.Key.DeclaringType].CastTo<MetaType>();
                }
                i.Value.Name = i.Key.Name;
                switch (i.Key.MemberType)
                {
                    case MetaMemberType.Type:
                        VisitType(i.Key.CastTo<MetaType>(), i.Value.CastTo<MetaType>());
                        break;
                    case MetaMemberType.Method:
                        VisitMethod(i.Key.CastTo<MetaMethod>(), i.Value.CastTo<MetaMethod>());
                        break;
                    case MetaMemberType.Property:
                        VisitProperty(i.Key.CastTo<MetaProperty>(), i.Value.CastTo<MetaProperty>());
                        break;
                    case MetaMemberType.Constructor:
                        VisitConstructor(i.Key.CastTo<MetaConstructor>(), i.Value.CastTo<MetaConstructor>());
                        break;
                }
            }
        }

        #region IEnumerable<KeyValuePair<MetaMember,MetaMember>> Members

        public IEnumerator<KeyValuePair<MetaMember, MetaMember>> GetEnumerator()
        {
            return map.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        protected virtual void VisitType(MetaType old, MetaType newCopy)
        {
            newCopy.AssemblyName = old.AssemblyName;
            if (old.BaseType != null)
            {
                newCopy.BaseType = map[old.BaseType].CastTo<MetaType>();
            }
            if (old.ElementType != null)
            {
                newCopy.ElementType = map[old.ElementType].CastTo<MetaType>();
            }
            newCopy.GenericArguments = old.GenericArguments.Select(i => map[i].CastTo<MetaType>()).ToList();
            if (old.GenericTypeDefinition != null)
            {
                newCopy.GenericTypeDefinition = map[old.GenericTypeDefinition].CastTo<MetaType>();
            }
            newCopy.Interfaces = old.Interfaces.Select(i => map[i].CastTo<MetaType>()).ToList();
            newCopy.IsAbstract = old.IsAbstract;
            newCopy.IsByRef = old.IsByRef;
            newCopy.IsInterface = old.IsInterface;
            newCopy.IsPrimitive = old.IsPrimitive;
            newCopy.IsArray = old.IsArray;
            newCopy.IsGenericParameter = old.IsGenericParameter;
            newCopy.IsGenericTypeDefinition = old.IsGenericTypeDefinition;
            newCopy.Members = old.Members.Select(i => map[i]).ToList();
            newCopy.Namespace = old.Namespace;
        }

        protected virtual void VisitMethod(MetaMethod old, MetaMethod newCopy)
        {
            newCopy.GenericArguments = old.GenericArguments.Select(i => map[i].CastTo<MetaType>()).ToList();
            if (old.GenericMethodDefinition != null)
            {
                newCopy.GenericMethodDefinition = map[old.GenericMethodDefinition].CastTo<MetaMethod>();
            }
            newCopy.IsAbstract = old.IsAbstract;
            newCopy.IsVirtual = old.IsVirtual;
            newCopy.IsGenericMethodDefinition = old.IsGenericMethodDefinition;
            newCopy.Parameters = old.Parameters.Select(p =>
                                                       new MetaParameter
                                                           {
                                                               Name = p.Name,
                                                               IsOut = p.IsOut,
                                                               ParameterType = map[p.ParameterType].CastTo<MetaType>()
                                                           }).ToList();
            newCopy.ReturnType = map[old.ReturnType].CastTo<MetaType>();
        }

        protected virtual void VisitProperty(MetaProperty old, MetaProperty newCopy)
        {
            newCopy.IsAbstract = old.IsAbstract;
            newCopy.IsVirtual = old.IsVirtual;
            newCopy.PropertyType = map[old.PropertyType].CastTo<MetaType>();
        }

        private void VisitConstructor(MetaConstructor old, MetaConstructor newCopy)
        {
            newCopy.BodyExpression = old.BodyExpression;
            newCopy.Parameters = old.Parameters.Select(p =>
                                                       new MetaParameter
                                                       {
                                                           Name = p.Name,
                                                           IsOut = p.IsOut,
                                                           ParameterType = map[p.ParameterType].CastTo<MetaType>()
                                                       }).ToList();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    internal class MetaTypeSystemEqualityComparer : IEqualityComparer<MetaMember>, IEqualityComparer<object>, IEqualityComparer
    {
        public bool UseDeepComparison { get; private set; }

        private static readonly Func<MetaMember, object[]> GetMetaMemberComparisonMembers = i => new object[] { i.MemberType, i.Name, i.DeclaringType }.Concat(i.Attributes.OfType<object>()).ToArray();

        private Func<MetaType, object[]> GetMetaTypeComparisonMembers
        {
            get
            {
                return i => !UseDeepComparison ? new object[] { i.AssemblyQualifiedName } : new object[] { i.AssemblyQualifiedName, i.IsAbstract, i.IsArray, i.IsByRef, i.IsGenericParameter, i.IsGenericTypeDefinition, i.IsInterface, i.IsPrimitive, i.BaseType, i.ElementType, i.GenericTypeDefinition, i.DeclaringMethod }
                                                                                              .Concat(i.GenericArguments.OfType<object>()).Concat(i.Interfaces.OfType<object>()).Concat(i.Members.OfType<object>())
                                                                                              .Concat(GetMetaMemberComparisonMembers(i)).ToArray();
            }
        }

        private static readonly Func<MetaProperty, object[]> GetMetaPropertyComparisonMembers = i => new object[] { i.Name, i.IsAbstract, i.IsVirtual, i.PropertyType }
                                                                                                         .Concat(GetMetaMemberComparisonMembers(i)).ToArray();

        private static readonly Func<MetaMethod, object[]> GetMetaMethodComparisonMembers = i => new object[] { i.Name, i.IsAbstract, i.IsGenericMethodDefinition, i.IsVirtual, i.ReturnType, i.GenericMethodDefinition }
                                                                                                     .Concat(i.GenericArguments.OfType<object>())
                                                                                                     .Concat(i.Parameters.Select(p => new { p.Name, p.IsOut }).OfType<object>()).Concat(i.Parameters.SelectMany(p => p.Attributes.OfType<object>())).Concat(i.Parameters.Select(p => p.ParameterType).OfType<object>())
                                                                                                     .Concat(GetMetaMemberComparisonMembers(i)).ToArray();

        private static readonly Func<MetaConstructor, object[]> GetMetaConstructorComparisonMembers = i => new object[] { i.Name }
                                                                                                          .Concat(i.Parameters.Select(p => new { p.Name, p.IsOut }).OfType<object>()).Concat(i.Parameters.SelectMany(p => p.Attributes.OfType<object>())).Concat(i.Parameters.Select(p => p.ParameterType).OfType<object>())
                                                                                                          .Concat(GetMetaMemberComparisonMembers(i)).ToArray();

        private readonly HashSet<object> visited = new HashSet<object>(ObjectReferenceEqualityComparerer<object>.Instance);

        public static IEqualityComparer<MetaMember> Instance
        {
            get { return new MetaTypeSystemEqualityComparer(false); }
        }

        public MetaTypeSystemEqualityComparer(bool useDeepComparison)
        {
            UseDeepComparison = useDeepComparison;
        }

        #region IEqualityComparer<MetaMember> Members

        public bool Equals(MetaMember x, MetaMember y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (x == null || y == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return false;
            }
            // ReSharper restore HeuristicUnreachableCode

            if (x.MemberType != y.MemberType)
            {
                return false;
            }

            if (!visited.Add(x))
            {
                return true;
            }

            switch (x.MemberType)
            {
                case MetaMemberType.Type:
                    if (!GetMetaTypeComparisonMembers(x.CastTo<MetaType>()).SequenceEqual(GetMetaTypeComparisonMembers(y.CastTo<MetaType>()), this))
                    {
                        return false;
                    }
                    break;
                case MetaMemberType.Method:
                    if (!GetMetaMethodComparisonMembers(x.CastTo<MetaMethod>()).SequenceEqual(GetMetaMethodComparisonMembers(y.CastTo<MetaMethod>()), this))
                    {
                        return false;
                    }
                    break;
                case MetaMemberType.Property:
                    if (!GetMetaPropertyComparisonMembers(x.CastTo<MetaProperty>()).SequenceEqual(GetMetaPropertyComparisonMembers(y.CastTo<MetaProperty>()), this))
                    {
                        return false;
                    }
                    break;
                case MetaMemberType.Constructor:
                    if (!GetMetaConstructorComparisonMembers(x.CastTo<MetaConstructor>()).SequenceEqual(GetMetaConstructorComparisonMembers(y.CastTo<MetaConstructor>()), this))
                    {
                        return false;
                    }
                    break;
            }

            return true;
        }

        public int GetHashCode(MetaMember obj)
        {
            int hash = 0;

            switch (obj.MemberType)
            {
                case MetaMemberType.Type:
                    hash ^= GetMetaTypeComparisonMembers(obj.CastTo<MetaType>()).Select(i => i.Is<MetaMember>() ? i.ToString() : i).GetSequenceHashCode();
                    break;
                case MetaMemberType.Property:
                    hash ^= GetMetaPropertyComparisonMembers(obj.CastTo<MetaProperty>()).Select(i => i.Is<MetaMember>() ? i.ToString() : i).Where(i => !i.Is<MetaMember>()).GetSequenceHashCode();
                    break;
                case MetaMemberType.Method:
                    hash ^= GetMetaMethodComparisonMembers(obj.CastTo<MetaMethod>()).Select(i => i.Is<MetaMember>() ? i.ToString() : i).Where(i => !i.Is<MetaMember>()).GetSequenceHashCode();
                    break;
            }

            return hash;
        }

        #endregion

        #region IEqualityComparer<object> Members

        public new bool Equals(object x, object y)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if ((x != null && !x.Is<MetaMember>()) || (y != null && !y.Is<MetaMember>()))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return object.Equals(x, y);
            }
            return Equals(x.As<MetaMember>(), y.As<MetaMember>());
        }

        public int GetHashCode(object obj)
        {
            if (!obj.Is<MetaMember>())
            {
                return obj.GetHashCode();
            }
            return obj.CastTo<MetaMember>().GetHashCode();
        }

        #endregion
    }

    /// <summary>
    ///   Describes the type of a MetaMember.
    /// </summary>
    public enum MetaMemberType
    {
        /// <summary>
        /// </summary>
        Type,

        /// <summary>
        /// </summary>
        Method,

        /// <summary>
        /// </summary>
        Property,

        /// <summary>
        /// </summary>
        Constructor
    }

    /// <summary>
    ///   Describes a MemberInfo
    /// </summary>
    internal abstract class MetaMember
    {
        protected MetaMember()
        {
            Attributes = new List<Attribute>();
        }

        public virtual string Name { get; set; }

        public abstract MetaMemberType MemberType { get; }

        public virtual MetaType DeclaringType { get; set; }

        public virtual List<Attribute> Attributes { get; set; }

        public override bool Equals(object obj)
        {
            return MetaTypeSystemEqualityComparer.Instance.Equals(this, obj.As<MetaMember>());
        }

        public override int GetHashCode()
        {
            return MetaTypeSystemEqualityComparer.Instance.GetHashCode(this);
        }

        public override string ToString()
        {
            return "{0}: {1}".FormatWith(MemberType, Name);
        }
    }

    /// <summary>
    ///   Describes a PropertyInfo
    /// </summary>
    internal class MetaProperty : MetaMember
    {
        private bool isVirtual;

        public override MetaMemberType MemberType
        {
            get { return MetaMemberType.Property; }
        }

        public virtual MetaType PropertyType { get; set; }

        public virtual bool IsAbstract { get; set; }

        public virtual bool IsVirtual
        {
            get { return isVirtual || IsAbstract; }
            set { isVirtual = value; }
        }

        public virtual LambdaExpression GetBodyExpression { get; set; }

        public virtual LambdaExpression SetBodyExpression { get; set; }
    }

    /// <summary>
    /// Describes a ConstructorInfo.
    /// </summary>
    internal class MetaConstructor : MetaMember
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MetaConstructor"/> class.
        /// </summary>
        public MetaConstructor()
        {
            Parameters = new List<MetaParameter>();
        }

        public override MetaMemberType MemberType
        {
            get { return MetaMemberType.Constructor; }
        }

        public virtual List<MetaParameter> Parameters { get; set; }

        public virtual LambdaExpression BodyExpression { get; set; }
    }

    /// <summary>
    ///   Describes a MethodInfo.
    /// </summary>
    internal class MetaMethod : MetaMember
    {
        private bool isVirtual;

        public MetaMethod()
        {
            Parameters = new List<MetaParameter>();
            GenericArguments = new List<MetaType>();
        }

        public override MetaMemberType MemberType
        {
            get { return MetaMemberType.Method; }
        }

        public virtual MetaType ReturnType { get; set; }

        public virtual bool IsAbstract { get; set; }

        public virtual bool IsVirtual
        {
            get { return isVirtual || IsAbstract; }
            set { isVirtual = value; }
        }

        public virtual List<MetaParameter> Parameters { get; set; }

        public virtual MetaMethod GenericMethodDefinition { get; set; }

        public virtual bool IsGenericMethodDefinition { get; set; }

        public virtual List<MetaType> GenericArguments { get; set; }

        public virtual LambdaExpression BodyExpression { get; set; }
    }

    /// <summary>
    ///   Describes a Type.
    /// </summary>
    internal class MetaType : MetaMember
    {
        private bool isAbstract;
        private MetaType baseType;

        public MetaType()
        {
            Members = new List<MetaMember>();
            Interfaces = new List<MetaType>();
            GenericArguments = new List<MetaType>();
        }

        public override MetaMemberType MemberType
        {
            get { return MetaMemberType.Type; }
        }

        public virtual string FullName
        {
            get
            {
                if (IsGenericParameter)
                {
                    return Name;
                }
                string fullName = new[] { Namespace, Name }.Where(i => i.IsNotNullOrEmpty()).Join(".");
                if (IsGenericTypeDefinition)
                {
                    return fullName;
                }
                if (GenericArguments.Count > 0 && GenericTypeDefinition != null)
                {
                    fullName = "{0}[[{1}]]".FormatWith(fullName, GenericArguments.Select(a => a.AssemblyQualifiedName).Join());
                }

                return fullName;
            }
        }

        public virtual string AssemblyQualifiedName
        {
            get
            {
                if (IsGenericParameter)
                {
                    return Name;
                }
                if (AssemblyName != null && FullName != null)
                {
                    return "{0}, {1}".FormatWith(FullName, AssemblyName);
                }
                return FullName;
            }
        }

        public virtual string Namespace { get; set; }

        public virtual string AssemblyName { get; set; }

        public virtual List<MetaMember> Members { get; set; }

        public virtual List<MetaType> Interfaces { get; set; }

        public virtual MetaType GenericTypeDefinition { get; set; }

        public virtual bool IsGenericTypeDefinition { get; set; }

        public virtual List<MetaType> GenericArguments { get; set; }

        public virtual bool IsGenericParameter { get; set; }

        public virtual MetaType BaseType
        {
            get { return IsInterface ? null : baseType; }
            set { baseType = value; }
        }

        public virtual MetaType ElementType { get; set; }

        public virtual MetaMethod DeclaringMethod { get; set; }

        public virtual bool IsInterface { get; set; }

        public virtual bool IsAbstract
        {
            get { return isAbstract || IsInterface; }
            set { isAbstract = value; }
        }

        public virtual bool IsByRef { get; set; }

        public virtual bool IsPrimitive { get; set; }

        public virtual bool IsArray { get; set; }
    }

    /// <summary>
    ///   Describes a ParameterInfo.
    /// </summary>
    internal class MetaParameter
    {
        public MetaParameter()
        {
            Attributes = new List<Attribute>();
        }

        public virtual string Name { get; set; }

        public virtual MetaType ParameterType { get; set; }

        public virtual bool IsOut { get; set; }

        public virtual List<Attribute> Attributes { get; set; }
    }

    /// <summary>
    ///   Disassembles an Attribute instance into a CustomAttributeBuilder. Taken from Castle DynamicProxy.
    /// </summary>
    internal interface IAttributeDisassembler
    {
        CustomAttributeBuilder Disassemble(Attribute attribute);
    }

    [Singleton]
    internal class AttributeDisassembler : IAttributeDisassembler
    {
        public static IDictionary<Type, Func<Attribute, CustomAttributeBuilder>> CustomDisassemblers = new Dictionary<Type, Func<Attribute, CustomAttributeBuilder>>().Synchronized();

        static AttributeDisassembler()
        {
        }

        #region IAttributeDisassembler Members

        public CustomAttributeBuilder Disassemble(Attribute attribute)
        {
            Type attType = attribute.GetType();
            Func<Attribute, CustomAttributeBuilder> customDisassember;
            if (CustomDisassemblers.TryGetValue(attType, out customDisassember))
            {
                return customDisassember(attribute);
            }

            try
            {
                ConstructorInfo info;
                PropertyInfo[] propertyInfos;
                var fieldInfos = new FieldInfo[0];
                object[] args = GetConstructorAndArgs(attType, attribute, out info);
                var replicated = (Attribute)Activator.CreateInstance(attType, args);
                object[] propertyValues = GetPropertyValues(attType, out propertyInfos, attribute, replicated);
                return new CustomAttributeBuilder(info, args, propertyInfos, propertyValues, fieldInfos, GetFieldValues(attType, out fieldInfos, attribute, replicated));
            }
            catch (Exception exception)
            {
                return HandleError(attType, exception);
            }
        }

        #endregion

        private static bool AreAttributeElementsEqual(object first, object second)
        {
            if (first == null)
            {
                return (second == null);
            }
            var str = first as string;
            if (str != null)
            {
                return AreStringsEqual(str, second as string);
            }
            return first.Equals(second);
        }

        private static bool AreStringsEqual(string first, string second)
        {
            return first.Equals(second, StringComparison.Ordinal);
        }

        private static object ConvertValue(object obj, Type parameterType)
        {
            if (obj == null)
            {
                return null;
            }
            if (parameterType == typeof(string))
            {
                return obj.ToString();
            }
            return obj;
        }

        public bool Equals(AttributeDisassembler other)
        {
            return !ReferenceEquals(null, other);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != typeof(AttributeDisassembler))
            {
                return false;
            }
            return Equals((AttributeDisassembler)obj);
        }

        private static object GetArgValue(Type attributeType, Attribute attribute, ParameterInfo parameterInfo)
        {
            Type parameterType = parameterInfo.ParameterType;
            PropertyInfo[] properties = attributeType.GetProperties();
            foreach (PropertyInfo info in properties)
            {
                if ((info.CanRead || (info.GetIndexParameters().Length == 0)) && (string.Compare(info.Name, parameterInfo.Name, StringComparison.CurrentCultureIgnoreCase) == 0))
                {
                    return ConvertValue(info.GetValue(attribute, null), parameterType);
                }
            }
            PropertyInfo bestMatch = properties.Where(info3 => info3.CanRead || (info3.GetIndexParameters().Length == 0))
                .Aggregate<PropertyInfo, PropertyInfo>(null, (current, info3) => ReplaceIfBetterMatch(parameterInfo, info3, current));

            if (bestMatch != null)
            {
                return ConvertValue(bestMatch.GetValue(attribute, null), parameterType);
            }
            return GetDefaultValueFor(parameterType);
        }

        private static object[] GetConstructorAndArgs(Type attributeType, Attribute attribute, out ConstructorInfo constructorInfo)
        {
            var args = new object[0];
            constructorInfo = attributeType.GetConstructors()[0];
            ParameterInfo[] parameters = constructorInfo.GetParameters();
            if (parameters.Length != 0)
            {
                args = new object[parameters.Length];
                InitializeConstructorArgs(attributeType, attribute, args, parameters);
            }
            return args;
        }

        private static object GetDefaultValueFor(Type type)
        {
            if (type == typeof(bool))
            {
                return false;
            }
            if (type.IsEnum)
            {
                return type.CastTo<Enum>().GetValues().GetValue(0);
            }
            if (type == typeof(char))
            {
                return '\0';
            }
            if (type.IsPrimitive)
            {
                return 0;
            }
            return null;
        }

        private static object[] GetFieldValues(Type attributeType, out FieldInfo[] fields, Attribute original, Attribute replicated)
        {
            FieldInfo[] infoArray = attributeType.GetFields(BindingFlags.Public | BindingFlags.Instance);
            var values = new List<object>(infoArray.Length);
            var fieldInfos = new List<FieldInfo>(infoArray.Length);
            foreach (FieldInfo info in infoArray)
            {
                object first = info.GetValue(original);
                object second = info.GetValue(replicated);
                if (!AreAttributeElementsEqual(first, second))
                {
                    fieldInfos.Add(info);
                    values.Add(first);
                }
            }
            fields = fieldInfos.ToArray();
            return values.ToArray();
        }

        public override int GetHashCode()
        {
            return GetType().GetHashCode();
        }

        private static List<PropertyInfo> GetPropertyCandidates(Type attributeType)
        {
            return
                attributeType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(
                    info => info.CanRead && info.CanWrite).ToList();
        }

        private static object[] GetPropertyValues(Type attType, out PropertyInfo[] properties, Attribute original, Attribute replicated)
        {
            List<PropertyInfo> propertyCandidates = GetPropertyCandidates(attType);
            var values = new List<object>(propertyCandidates.Count);
            var propertyInfos = new List<PropertyInfo>(propertyCandidates.Count);
            foreach (PropertyInfo propertyInfo in propertyCandidates)
            {
                object first = propertyInfo.GetValue(original, null);
                object second = propertyInfo.GetValue(replicated, null);
                if (!AreAttributeElementsEqual(first, second))
                {
                    propertyInfos.Add(propertyInfo);
                    values.Add(first);
                }
            }
            properties = propertyInfos.ToArray();
            return values.ToArray();
        }

        protected virtual CustomAttributeBuilder HandleError(Type attributeType, Exception exception)
        {
            throw new NotSupportedException("Unable to disassemble attribute " + attributeType.Name);
        }

        private static void InitializeConstructorArgs(Type attributeType, Attribute attribute, object[] args, ParameterInfo[] parameterInfos)
        {
            for (int i = 0; i < args.Length; i++)
            {
                args[i] = GetArgValue(attributeType, attribute, parameterInfos[i]);
            }
        }

        private static PropertyInfo ReplaceIfBetterMatch(ParameterInfo parameterInfo, PropertyInfo propertyInfo, PropertyInfo bestMatch)
        {
            bool flag = (bestMatch == null) || (bestMatch.PropertyType != parameterInfo.ParameterType);
            if ((propertyInfo.PropertyType == parameterInfo.ParameterType) && flag)
            {
                return propertyInfo;
            }
            if ((parameterInfo.ParameterType == typeof(string)) && flag)
            {
                return propertyInfo;
            }
            return bestMatch;
        }
    }

    /// <summary>
    ///   Manages existing Type and MetaType mappings and creates new Types and MetaTypes on demand.
    /// </summary>
    internal interface ITypeSystemRepository
    {
        Type GetType(MetaType metaType);
        MethodInfo GetMethod(MetaMethod metaMethod);
        PropertyInfo GetProperty(MetaProperty metaProperty);
        ConstructorInfo GetConstructor(MetaConstructor metaConstructor);
        MetaType GetMetaType(Type type);
        MetaMethod GetMetaMethod(MethodInfo method);
        MetaProperty GetMetaProperty(PropertyInfo property);
        MetaConstructor GetMetaConstructor(ConstructorInfo constructor);
        MetaType GetDataContractType(MetaType metaType, bool isReference, bool ensureHasKey);
    }

    /// <summary>
    ///   Manages existing Type and MetaType mappings and creates new Types and MetaTypes on demand.
    /// </summary>
    [Singleton]
    [SupportsSynchronization]
    internal class TypeSystemRepository : ITypeSystemRepository
    {
        private readonly IDictionary<MemberInfo, MetaMember> memberInfoToMetaMember = new Dictionary<MemberInfo, MetaMember>().Synchronized();
        private readonly IDictionary<MetaMember, MemberInfo> metaMemberToMemberInfo = new Dictionary<MetaMember, MemberInfo>().Synchronized();
        private readonly IDictionary<DataContractMapKey, MetaType> dataContractTypeMap = new Dictionary<DataContractMapKey, MetaType>().Synchronized();

        private readonly ITypeGenerator typeGenerator;

        public TypeSystemRepository(ITypeGenerator typeGenerator)
        {
            this.typeGenerator = typeGenerator;
        }

        #region ITypeSystemRepository Members

        [Synchronized]
        public virtual Type GetType(MetaType metaType)
        {
            if (metaType == null) return null;

            Type result;

            if (metaMemberToMemberInfo.TryGetValue(metaType, out result))
            {
                return result;
            }

            result = Type.GetType(metaType.AssemblyQualifiedName);

            if (result == null)
            {
                if (metaType.IsByRef)
                {
                    result = metaType.ElementType.ToType().MakeByRefType();
                }
                else if (metaType.IsArray)
                {
                    result = metaType.ElementType.ToType().MakeArrayType();
                }
                else if (metaType.GenericTypeDefinition != null)
                {
                    result = metaType.GenericTypeDefinition.ToType().MakeGenericType(metaType.GenericArguments.Select(i => i.ToType()).ToArray());
                }
                else if (metaType.BaseType != null)
                {
                    metaType.BaseType.ToType();
                }
            }

            if (result != null)
            {
                Register(result, metaType);
                return result;
            }

            result = typeGenerator.GenerateType(metaType);

            Register(result, metaType);

            return result;
        }

        public virtual MethodInfo GetMethod(MetaMethod metaMethod)
        {
            if (metaMethod == null) return null;

            MethodInfo result;
            if (metaMemberToMemberInfo.TryGetValue(metaMethod, out result))
            {
                return result;
            }

            Type type = GetType(metaMethod.DeclaringType);

            if (type != null)
            {
                MethodInfo method = type.GetMethod(metaMethod.Name, metaMethod.Parameters.Select(p => GetType(p.ParameterType)).ToArray());
                if (method != null)
                {
                    Register(method, metaMethod);
                    return method;
                }
            }
            return null;
        }

        public virtual PropertyInfo GetProperty(MetaProperty metaProperty)
        {
            if (metaProperty == null) return null;

            PropertyInfo result;
            if (metaMemberToMemberInfo.TryGetValue(metaProperty, out result))
            {
                return result;
            }
            Type type = GetType(metaProperty.DeclaringType);
            if (type != null)
            {
                PropertyInfo property = type.GetProperty(metaProperty.Name, GetType(metaProperty.PropertyType));
                if (property != null)
                {
                    Register(property, metaProperty);
                    return property;
                }
            }
            return null;
        }

        public ConstructorInfo GetConstructor(MetaConstructor metaConstructor)
        {
            if (metaConstructor == null) return null;

            ConstructorInfo result;
            if (metaMemberToMemberInfo.TryGetValue(metaConstructor, out result))
            {
                return result;
            }

            Type type = GetType(metaConstructor.DeclaringType);

            if (type != null)
            {
                ConstructorInfo constructor = type.GetConstructor(metaConstructor.Parameters.Select(p => GetType(p.ParameterType)).ToArray());
                if (constructor != null)
                {
                    Register(constructor, metaConstructor);
                    return constructor;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the type of the meta.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public virtual MetaType GetMetaType(Type type)
        {
            if (type == null) return null;

            MetaType metaType;
            if (memberInfoToMetaMember.TryGetValue(type, out metaType))
            {
                return metaType;
            }

            foreach (var i in new MemberInfoToMetaTypeSystemConverter(type, memberInfoToMetaMember).ToDictionary(i => i.Key, i => i.Value))
            {
                if (i.Key == type)
                {
                    metaType = i.Value.CastTo<MetaType>();
                }
                Register(i.Key, i.Value);
            }

            return metaType;
        }

        public virtual MetaMethod GetMetaMethod(MethodInfo method)
        {
            if (method == null) return null;

            MetaMethod metaMethod;
            if (memberInfoToMetaMember.TryGetValue(method, out metaMethod))
            {
                return metaMethod;
            }

            foreach (var i in new MemberInfoToMetaTypeSystemConverter(method, memberInfoToMetaMember).ToDictionary(i => i.Key, i => i.Value))
            {
                if (i.Key == method)
                {
                    metaMethod = i.Value.CastTo<MetaMethod>();
                }
                Register(i.Key, i.Value);
            }

            return metaMethod;
        }

        public virtual MetaProperty GetMetaProperty(PropertyInfo property)
        {
            if (property == null) return null;

            MetaProperty metaProperty;
            if (memberInfoToMetaMember.TryGetValue(property, out metaProperty))
            {
                return metaProperty;
            }

            foreach (var i in new MemberInfoToMetaTypeSystemConverter(property, memberInfoToMetaMember).ToDictionary(i => i.Key, i => i.Value))
            {
                if (i.Key == property)
                {
                    metaProperty = i.Value.CastTo<MetaProperty>();
                }
                Register(i.Key, i.Value);
            }

            return metaProperty;
        }

        public MetaConstructor GetMetaConstructor(ConstructorInfo constructor)
        {
            if (constructor == null) return null;

            MetaConstructor metaConstructor;
            if (memberInfoToMetaMember.TryGetValue(constructor, out metaConstructor))
            {
                return metaConstructor;
            }

            foreach (var i in new MemberInfoToMetaTypeSystemConverter(constructor, memberInfoToMetaMember).ToDictionary(i => i.Key, i => i.Value))
            {
                if (i.Key == constructor)
                {
                    metaConstructor = i.Value.CastTo<MetaConstructor>();
                }
                Register(i.Key, i.Value);
            }

            return metaConstructor;
        }

        public MetaType GetDataContractType(MetaType metaType, bool isReference, bool ensureHasKey)
        {
            var dataContractAttribute = metaType.Attributes.OfType<DataContractAttribute>().FirstOrDefault();
            if (dataContractAttribute != null && dataContractAttribute.IsReference == isReference)
            {
                return metaType;
            }

            if (metaType.Attributes.OfType<CollectionDataContractAttribute>().Any())
            {
                return metaType;
            }

            MetaType result;
            if (dataContractTypeMap.TryGetValue(new DataContractMapKey(metaType, isReference, ensureHasKey), out result))
            {
                return result;
            }

            var dataContractTypes = new MetaMemberRewriter(metaType, m =>
                                                                         {
                                                                             MetaType existingDataContractMetaType = null;
                                                                             if (m is MetaType)
                                                                             {
                                                                                 dataContractTypeMap.TryGetValue(new DataContractMapKey(m.CastTo<MetaType>(), isReference, ensureHasKey), out existingDataContractMetaType);
                                                                             }
                                                                             return existingDataContractMetaType;
                                                                         }).ToArray();

            lock (dataContractTypeMap)
            {
                foreach (var i in dataContractTypes.Select(t => new { Source = t.Key.As<MetaType>(), Target = t.Value.As<MetaType>() }).Where(t => t.Source != null && t.Target != null)
                    .Where(t =>
                           !t.Target.AssemblyName.StartsWith("System") && !t.Target.AssemblyName.StartsWith("Microsoft") && !t.Target.AssemblyName.StartsWith("mscorlib") &&
                           t.Target.Members.OfType<MetaProperty>().Count() > 0 &&
                           !t.Target.IsPrimitive &&
                           !t.Target.IsInterface &&
                           !t.Target.IsArray &&
                           !t.Target.Interfaces.Any(i => i.AssemblyQualifiedName.StartsWith("System.Collections.Generic.IEnumerable`1[")) &&
                           !t.Target.Attributes.Any(a => a is DataContractAttribute || a is CollectionDataContractAttribute)))
                {
                    foreach (var metaProperty in i.Target.Members.OfType<MetaProperty>())
                    {
                        metaProperty.Attributes.Add(new DataMemberAttribute());
                        metaProperty.IsVirtual = false;
                    }

                    dataContractAttribute = new DataContractAttribute();
                    dataContractAttribute.Name = i.Target.Name;
                    dataContractAttribute.IsReference = isReference;
                    i.Target.Attributes.Add(dataContractAttribute);

                    var keyProperty = i.Target.Members.OfType<MetaProperty>().FirstOrDefault(p => new[] { "id", "{0}id".FormatWith(metaType.Name) }.Contains(p.Name, StringComparer.OrdinalIgnoreCase));
                    if (keyProperty == null && ensureHasKey)
                    {
                        keyProperty = new MetaProperty { Name = "Id", PropertyType = typeof(Guid).ToMetaType() };
                        i.Target.Members.Add(keyProperty);
                    }

                    i.Target.Name += "Contract_{0}_With{1}Key".FormatWith(isReference ? "IsReference" : "IsNotReference", keyProperty != null ? string.Empty : "out");

                    // remove non properties and interfaces
                    i.Target.Interfaces.Clear();
                    var target = i.Target;
                    i.Target.Members.Where(m => !m.Is<MetaProperty>()).ToArray().ForEach(m => target.Members.Remove(m));
                }

                foreach (var pair in dataContractTypes.Select(t => new { Source = t.Key.As<MetaType>(), Target = t.Value.As<MetaType>() }).Where(t => t.Source != null && t.Target != null))
                {
                    var keyProperty = pair.Target.Members.OfType<MetaProperty>().FirstOrDefault(p => new[] { "id", "{0}id".FormatWith(metaType.Name) }.Contains(p.Name, StringComparer.OrdinalIgnoreCase));

                    dataContractTypeMap[new DataContractMapKey(pair.Source, isReference, keyProperty != null || ensureHasKey)] = pair.Target;
                }

            }
            return dataContractTypes.First().Value.CastTo<MetaType>();
        }

        #endregion

        [Synchronized]
        protected virtual void Register(MemberInfo memberInfo, MetaMember metaMember)
        {
            if (!memberInfo.GetType().Is<Type>() || memberInfo.GetType() == Types.RuntimeTypeType)
            {
                memberInfoToMetaMember[memberInfo] = metaMember;
                metaMemberToMemberInfo[metaMember] = memberInfo;
            }
        }

        private class DataContractMapKey
        {
            private MetaType MetaType { get; set; }
            private bool IsReference { get; set; }
            private bool HasKey { get; set; }

            public DataContractMapKey(MetaType metaType, bool isReference, bool hasKey)
            {
                MetaType = metaType;
                IsReference = isReference;
                HasKey = hasKey;
            }

            public override int GetHashCode()
            {
                return new object[] { MetaType.AssemblyQualifiedName, IsReference, HasKey }.GetSequenceHashCode();
            }

            public override bool Equals(object obj)
            {
                return obj.As<DataContractMapKey>().IfNotNull(y => new object[] { MetaType, IsReference, HasKey }.SequenceEqual(new object[] { y.MetaType, y.IsReference, y.HasKey }));
            }
        }
    }

    /// <summary>
    ///   Generates dynamic types from a metaType.
    /// </summary>
    internal interface ITypeGenerator
    {
        Type GenerateType(MetaType metaType);
    }

    /// <summary>
    ///   Provides an instance of a dynamic module.
    /// </summary>
    internal interface IDynamicModuleFactory
    {
        ModuleBuilder GetModuleBuilder();
    }

    /// <summary>
    ///   Provides an instance of a dynamic module. Shares single ModuleBuilder.
    /// </summary>
    [Singleton]
    internal class DynamicModuleFactory : IDynamicModuleFactory
    {
        public static readonly AssemblyName AssemblyName = new AssemblyName("Soaf.DynamicTypes");

        private readonly Lazy<ModuleBuilder> moduleBuilder;

        public DynamicModuleFactory()
        {
            moduleBuilder = Lazy.For(CreateModuleBuilder);
        }

        #region IDynamicModuleFactory Members

        public ModuleBuilder GetModuleBuilder()
        {
            return moduleBuilder.Value;
        }

        #endregion

        private static ModuleBuilder CreateModuleBuilder()
        {
            var assemblyName = AssemblyName;
            assemblyName.SetPublicKey(Convert.FromBase64String(WellKnownAssemblies.SoafPublicKey));
            AssemblyBuilder assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            ModuleBuilder module = assembly.DefineDynamicModule("DynamicModule");
            return module;
        }
    }

    /// <summary>
    ///   Generates dynamic types from a metaType.
    /// </summary>
    [Singleton]
    [SupportsSynchronization]
    internal class TypeGenerator : ITypeGenerator
    {
        private readonly IAttributeDisassembler attributeDisassembler;
        private readonly Queue<Action> fixups = new Queue<Action>();
        private readonly ModuleBuilder module;
        private readonly Dictionary<MetaType, TypeBuilder> typesBeingGenerated = new Dictionary<MetaType, TypeBuilder>();
        private bool isGenerating;

        public TypeGenerator(IDynamicModuleFactory dynamicModuleFactory, IAttributeDisassembler attributeDisassembler)
        {
            module = dynamicModuleFactory.GetModuleBuilder();
            this.attributeDisassembler = attributeDisassembler;
        }

        #region ITypeGenerator Members

        [Synchronized]
        public virtual Type GenerateType(MetaType metaType)
        {
            TypeAttributes typeAttributes = TypeAttributes.Public;
            if (metaType.IsInterface)
            {
                typeAttributes |= TypeAttributes.Interface;
            }
            if (metaType.IsAbstract)
            {
                typeAttributes |= TypeAttributes.Abstract;
            }

            TypeBuilder type;

            if (typesBeingGenerated.TryGetValue(metaType, out type))
            {
                return type;
            }

            string name = metaType.FullName;
            if (name.IsNullOrEmpty())
            {
                name = metaType.Name;
            }
            if (name.IsNullOrEmpty())
            {
                name = Guid.NewGuid().ToString("N");
            }

            type = module.DefineType(name, typeAttributes);

            typesBeingGenerated[metaType] = type;

            if (isGenerating)
            {
                fixups.Enqueue(() =>
                                   {
                                       GenerateTypeImplementation(metaType, type);
                                       type.CreateType();
                                   });
                return type;
            }

            GenerateTypeImplementation(metaType, type);

            Type result = type.CreateType();

            while (fixups.Count > 0)
            {
                fixups.Dequeue()();
            }

            typesBeingGenerated.Remove(metaType);

            return result;
        }

        #endregion

        private void GenerateTypeImplementation(MetaType metaType, TypeBuilder type)
        {
            isGenerating = true;

            if (metaType.BaseType != null)
            {
                type.SetParent(metaType.BaseType.ToType());
            }
            if (metaType.IsGenericTypeDefinition)
            {
                type.DefineGenericParameters(metaType.GenericArguments.Select(i => i.Name).ToArray());
            }
            metaType.Interfaces.ForEach(i => type.AddInterfaceImplementation(i.ToType()));

            metaType.Attributes.ForEach(a => type.SetCustomAttribute(attributeDisassembler.Disassemble(a)));

            GenerateMembers(metaType, type);

            isGenerating = false;
        }

        private void GenerateMembers(MetaType metaType, TypeBuilder type)
        {

            var fields = new List<FieldInfo>();
            foreach (MetaMember metaMember in metaType.Members)
            {
                switch (metaMember.MemberType)
                {
                    case MetaMemberType.Property:
                        FieldInfo underlyingField;
                        GenerateProperty(metaMember.CastTo<MetaProperty>(), type, out underlyingField);
                        if (underlyingField != null)
                        {
                            fields.Add(underlyingField);
                        }
                        break;
                    case MetaMemberType.Method:
                        GenerateMethod(metaMember.CastTo<MetaMethod>(), type);
                        break;
                    case MetaMemberType.Constructor:
                        GenerateConstructor(metaMember.CastTo<MetaConstructor>(), type);
                        break;
                }
            }
            if (fields.IsNotNullOrEmpty())
            {
                GenerateGetHashCode(type);
                GenerateEquals(type);
            }
        }

        private void GenerateConstructor(MetaConstructor metaConstructor, TypeBuilder type)
        {
            var constructor = type.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard | CallingConventions.HasThis, metaConstructor.Parameters.Select(p => p.ParameterType.ToType()).ToArray());
            metaConstructor.Attributes.ForEach(a => constructor.SetCustomAttribute(attributeDisassembler.Disassemble(a)));

            var baseConstructor = type.BaseType == null ? null : type.BaseType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public, null, Type.EmptyTypes, null) ?? type.BaseType.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).FirstOrDefault();

            var generator = constructor.GetILGenerator();

            if (baseConstructor != null)
            {
                generator.Emit(OpCodes.Ldarg_0);
                foreach (var p in baseConstructor.GetParameters())
                {
                    var baseConstructorParameterInfo = p;
                    var metaConstructorParameterIndex = metaConstructor.Parameters.Select((parameter, index) => new { parameter, index = index + 1 }).Where(i => i.parameter.ParameterType.ToType() == baseConstructorParameterInfo.ParameterType).Select(i => i.index).FirstOrDefault();
                    if (metaConstructorParameterIndex > 0)
                    {
                        generator.Emit(OpCodes.Ldarg, metaConstructorParameterIndex);
                    }
                }
                generator.Emit(OpCodes.Call, baseConstructor);
            }

            if (metaConstructor.BodyExpression != null)
            {
                var bodyMethod = type.DefineMethod("ctor_{0}".FormatWith(metaConstructor.DeclaringType.Members.OfType<MetaConstructor>().IndexOf(metaConstructor)), MethodAttributes.Private | MethodAttributes.Static);
                metaConstructor.BodyExpression.CompileToMethod(bodyMethod);

                generator.Emit(OpCodes.Call, bodyMethod);
            }

            generator.Emit(OpCodes.Ret);
        }

        private void GenerateProperty(MetaProperty metaProperty, TypeBuilder type, out FieldInfo underlyingField)
        {
            underlyingField = null;

            PropertyBuilder property = type.DefineProperty(metaProperty.Name, PropertyAttributes.HasDefault, metaProperty.PropertyType.ToType(), Type.EmptyTypes);

            metaProperty.Attributes.ForEach(a => property.SetCustomAttribute(attributeDisassembler.Disassemble(a)));

            MethodAttributes methodAttributes = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig;
            if (metaProperty.IsVirtual)
            {
                methodAttributes |= MethodAttributes.Virtual;
            }
            if (metaProperty.IsAbstract)
            {
                methodAttributes |= MethodAttributes.Abstract;
            }

            MethodBuilder getMethod = type.DefineMethod("get_{0}".FormatWith(metaProperty.Name), methodAttributes, property.PropertyType, Type.EmptyTypes);
            property.SetGetMethod(getMethod);

            MethodBuilder setMethod = type.DefineMethod("set_{0}".FormatWith(metaProperty.Name), methodAttributes, null, new[] { property.PropertyType });
            property.SetSetMethod(setMethod);

            if (!metaProperty.IsAbstract)
            {
                FieldBuilder field = type.DefineField("_{0}".FormatWith(metaProperty.Name), property.PropertyType, FieldAttributes.Private);
                underlyingField = field;

                if (metaProperty.GetBodyExpression != null)
                {
                    var getBody = type.DefineMethod("get_{0}_Body".FormatWith(metaProperty.Name), MethodAttributes.Private | MethodAttributes.Static, property.PropertyType, Type.EmptyTypes);
                    metaProperty.GetBodyExpression.CompileToMethod(getBody);
                    var generator = getMethod.GetILGenerator();
                    generator.Emit(OpCodes.Call, getBody);
                    generator.Emit(OpCodes.Ret);
                }
                else
                {
                    ILGenerator getGenerator = getMethod.GetILGenerator();
                    getGenerator.Emit(OpCodes.Ldarg_0);
                    getGenerator.Emit(OpCodes.Ldfld, field);
                    getGenerator.Emit(OpCodes.Ret);
                }
                if (metaProperty.SetBodyExpression != null)
                {
                    var setBody = type.DefineMethod("set_{0}_Body".FormatWith(metaProperty.Name), MethodAttributes.Private | MethodAttributes.Static, null, new[] { property.PropertyType });
                    metaProperty.SetBodyExpression.CompileToMethod(setBody);
                    var generator = setMethod.GetILGenerator();
                    generator.Emit(OpCodes.Call, setBody);
                    generator.Emit(OpCodes.Ret);
                }
                else
                {
                    ILGenerator setGenerator = setMethod.GetILGenerator();
                    setGenerator.Emit(OpCodes.Ldarg_0);
                    setGenerator.Emit(OpCodes.Ldarg_1);
                    setGenerator.Emit(OpCodes.Stfld, field);
                    setGenerator.Emit(OpCodes.Ret);
                }
            }
        }

        private void GenerateMethod(MetaMethod metaMethod, TypeBuilder type)
        {
            MethodAttributes methodAttributes = MethodAttributes.Public | MethodAttributes.Virtual;
            if (metaMethod.IsVirtual)
            {
                methodAttributes |= MethodAttributes.Virtual;
            }
            if (metaMethod.IsAbstract)
            {
                methodAttributes |= MethodAttributes.Abstract;
            }
            MethodBuilder method = type.DefineMethod(metaMethod.Name, methodAttributes);

            method.SetReturnType(metaMethod.ReturnType == null ? typeof(void) : metaMethod.ReturnType.ToType());
            method.SetParameters(metaMethod.Parameters.Select(p => p.ParameterType.ToType()).ToArray());
            metaMethod.Attributes.ForEach(a => method.SetCustomAttribute(attributeDisassembler.Disassemble(a)));

            int i = 1;
            foreach (MetaParameter metaParameter in metaMethod.Parameters)
            {
                ParameterAttributes parameterAttributes = ParameterAttributes.None;
                if (metaParameter.IsOut)
                {
                    parameterAttributes = ParameterAttributes.Out;
                }
                ParameterBuilder parameter = method.DefineParameter(i, parameterAttributes, metaParameter.Name);
                metaParameter.Attributes.ForEach(a => parameter.SetCustomAttribute(attributeDisassembler.Disassemble(a)));
                i++;
            }

            if (!metaMethod.IsAbstract)
            {
                if (metaMethod.BodyExpression != null)
                {
                    MethodBuilder bodyMethod = type.DefineMethod("{0}_Body".FormatWith(metaMethod.Name), MethodAttributes.Private | MethodAttributes.Static);
                    metaMethod.BodyExpression.CompileToMethod(bodyMethod);

                    bodyMethod.SetReturnType(metaMethod.ReturnType == null ? typeof(void) : metaMethod.ReturnType.ToType());
                    bodyMethod.SetParameters(metaMethod.Parameters.Select(p => p.ParameterType.ToType()).ToArray());

                    foreach (MetaParameter metaParameter in metaMethod.Parameters)
                    {
                        ParameterAttributes parameterAttributes = ParameterAttributes.None;
                        if (metaParameter.IsOut)
                        {
                            parameterAttributes = ParameterAttributes.Out;
                        }
                        bodyMethod.DefineParameter(i, parameterAttributes, metaParameter.Name);
                    }

                    var generator = method.GetILGenerator();
                    generator.Emit(OpCodes.Call, bodyMethod);
                    generator.Emit(OpCodes.Ret);
                }
                else
                {
                    ILGenerator generator = method.GetILGenerator();
                    if (metaMethod.ReturnType == null)
                    {
                        generator.Emit(OpCodes.Ret);
                    }
                    else
                    {
                        // ReSharper disable AssignNullToNotNullAttribute
                        generator.Emit(OpCodes.Newobj, typeof(NotImplementedException).GetConstructor(Type.EmptyTypes));
                        // ReSharper restore AssignNullToNotNullAttribute
                        generator.Emit(OpCodes.Throw);
                    }
                }
            }
        }

        private static void GenerateEquals(TypeBuilder type)
        {
            MethodBuilder method = type.DefineMethod("Equals", MethodAttributes.Public | MethodAttributes.ReuseSlot | MethodAttributes.Virtual | MethodAttributes.HideBySig, typeof(bool), new[] { typeof(object) });
            ILGenerator generator = method.GetILGenerator();
            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldarg_1);
            generator.EmitCall(OpCodes.Call, Reflector.GetMember(() => Objects.FieldsEqual(null, null)).CastTo<MethodInfo>(), null);
            generator.Emit(OpCodes.Ret);
        }

        private static void GenerateGetHashCode(TypeBuilder type)
        {
            MethodBuilder method = type.DefineMethod("GetHashCode", MethodAttributes.Public | MethodAttributes.ReuseSlot | MethodAttributes.Virtual | MethodAttributes.HideBySig, typeof(int), Type.EmptyTypes);
            ILGenerator generator = method.GetILGenerator();
            generator.Emit(OpCodes.Ldarg_0);
            generator.EmitCall(OpCodes.Call, Reflector.GetMember(() => Objects.GetFieldsHashCode(null)).CastTo<MethodInfo>(), null);
            generator.Emit(OpCodes.Ret);
        }
    }
}