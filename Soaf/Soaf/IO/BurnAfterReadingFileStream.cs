﻿using System;
using System.IO;
using Soaf.ComponentModel;

namespace Soaf.IO
{
    /// <summary>
    /// File stream which deletes underlying file once stream is disposed
    /// </summary>
    public class BurnAfterReadingFileStream : Stream
    {
        private readonly FileStream fs;

        public BurnAfterReadingFileStream(string path)
        {
            fs = File.OpenRead(path);
        }

        public BurnAfterReadingFileStream(FileStream stream)
        {
            fs = stream;
        }

        public override bool CanRead { get { return fs.CanRead; } }

        public override bool CanSeek { get { return fs.CanRead; } }

        public override bool CanWrite { get { return fs.CanRead; } }

        public override void Flush() { fs.Flush(); }

        public override long Length { get { return fs.Length; } }

        public override long Position { get { return fs.Position; } set { fs.Position = value; } }

        public override int Read(byte[] buffer, int offset, int count) { return fs.Read(buffer, offset, count); }

        public override long Seek(long offset, SeekOrigin origin) { return fs.Seek(offset, origin); }

        public override void SetLength(long value) { fs.SetLength(value); }

        public override void Write(byte[] buffer, int offset, int count) { fs.Write(buffer, offset, count); }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (Position > 0) //web service quickly disposes the object (with the position at 0), but it must get rebuilt and re-disposed when the client reads it (when the position is not zero)
            {
                fs.Close();
                try
                {
                    RetryUtility.ExecuteWithRetry(() =>
                    {
                        if (File.Exists(fs.Name))
                        {
                            File.Delete(fs.Name);
                        }
                    }, 5, TimeSpan.FromMilliseconds(20));
                }
                finally
                {
                }
            }
        }
    }
}
