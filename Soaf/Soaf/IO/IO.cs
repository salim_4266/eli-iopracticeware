﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Soaf.IO
{
    /// <summary>
    ///   Extension and helper methods for dealing with IO and streams.
    /// </summary>
    public static class IO
    {
        /// <summary>
        ///   Converts a stream into a byte array.
        /// </summary>
        /// <param name = "stream">The stream.</param>
        /// <returns></returns>
        public static byte[] ToArray(this Stream stream)
        {
            var memoryStream = stream as MemoryStream;
            if (memoryStream != null)
            {
                return memoryStream.ToArray();
            }

            if (stream.CanSeek)
            {
                var fullBuffer = new byte[stream.Length];
                stream.Read(fullBuffer, 0, fullBuffer.Length);
                return fullBuffer;
            }

            if (stream.Position > 0)
            {
                stream.Position = 0;
            }
            var bytes = new List<byte>();
            var buffer = new byte[1024];
            int bytesRead = stream.Read(buffer, 0, buffer.Length);
            while (bytesRead > 0)
            {
                bytes.AddRange(buffer.Take(bytesRead));
                bytesRead = stream.Read(buffer, 0, buffer.Length);
            }
            return bytes.ToArray();
        }

        /// <summary>
        ///   Gets the string representation of specified stream using the specified encoding. If no encoding is specified, uses UTF8.
        /// </summary>
        /// <param name = "stream">The stream.</param>
        /// <param name = "encoding">The encoding.</param>
        /// <returns></returns>
        public static string GetString(this Stream stream, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            using (var sr = new StreamReader(stream, encoding))
            {
                return sr.ReadToEnd();
            }
        }


        public static string[] ReadAllLines(string path)
        {
            var lines = new List<string>();
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var reader = new StreamReader(fs))
                {
                    while (!reader.EndOfStream)
                    {
                        lines.Add(reader.ReadLine());
                    }
                }
            }
            return lines.ToArray();
        }

        public static string ReadAllText(string path)
        {
            string text;
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var reader = new StreamReader(fs))
                {
                    text = reader.ReadToEnd();
                }
            }
            return text;
        }

        public static byte[] ReadAllBytes(string path, bool throwOnException = true)
        {
            try
            {
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    return stream.ToArray();
                }
            }
            catch
            {
                if (throwOnException) throw;
                return null;
            }
        }

        public static string GetString(this byte[] data, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            return encoding.GetString(data, 0, data.Length);
        }
    }
}