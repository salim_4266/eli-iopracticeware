﻿using System;
using System.Reflection;
using Soaf.Caching;

namespace Soaf
{
    /// <summary>
    /// Represents a value which is evaluated using provided function when dependencies change.
    /// All created values remain cached and are returned once supplied dependencies match
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class RefreshableCachedValue<TValue> : IDisposable
    {
        private static readonly MethodInfo GetOrRefreshValueMethodInfo = typeof(RefreshableCachedValue<TValue>).GetMethod("GetOrRefreshValue", new[] { typeof(object[]) });

        /// <summary>
        /// Underlying cache
        /// </summary>
        private Lazy<ICache> cache = new Lazy<ICache>(() => ServiceProvider.Current.GetService<ICache>());
        private readonly Func<TValue> newValueGetter;
        private readonly CacheHoldInterval cacheHoldInterval;

        /// <summary>
        /// Initializes a new instance of the <see cref="RefreshableCachedValue{TValue}"/> class.
        /// </summary>
        /// <param name="cacheHoldInterval">Specified how long should EACH generated value remain cached. Defaults to Short</param>
        public RefreshableCachedValue(CacheHoldInterval cacheHoldInterval = null)
        {
            this.cacheHoldInterval = cacheHoldInterval ?? CacheHoldInterval.Short;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RefreshableCachedValue{TValue}"/> class.
        /// </summary>
        /// <param name="newValueGetter"></param>
        /// <param name="cacheHoldInterval">The cache hold interval. Defaults to Short</param>
        public RefreshableCachedValue(Func<TValue> newValueGetter, CacheHoldInterval cacheHoldInterval = null)
        {
            if (newValueGetter == null) throw new ArgumentNullException("newValueGetter");

            this.newValueGetter = newValueGetter;
            this.cacheHoldInterval = cacheHoldInterval ?? CacheHoldInterval.Short;
        }

        /// <summary>
        /// Gets the or refresh value.
        /// </summary>
        /// <param name="currentDependencies">The current dependencies.</param>
        /// <returns></returns>
        public TValue GetOrRefreshValue(object[] currentDependencies = null)
        {
            if (!cache.IsValueCreated && !ServiceProvider.IsInitialized) return newValueGetter();

            var value = cache.Value.GetOrAddValue(
                new CachedMethodCallDescriptor(this, GetOrRefreshValueMethodInfo, currentDependencies), 
                () => new CachedValue(newValueGetter(), cacheHoldInterval));
            return (TValue)value.Value;
        }

        /// <summary>
        /// Get the cached value or refreshes it if dependencies do not match
        /// </summary>
        /// <param name="getNewValue"></param>
        /// <param name="dependencies">Dependencies for this value (not null)</param>
        /// <returns></returns>
        public TValue GetOrRefreshValue(Func<TValue> getNewValue, params object[] dependencies)
        {
            if (!cache.IsValueCreated && !ServiceProvider.IsInitialized) return getNewValue();

            var value = cache.Value.GetOrAddValue(
                new CachedMethodCallDescriptor(this, GetOrRefreshValueMethodInfo, dependencies),
                () => new CachedValue(getNewValue(), cacheHoldInterval));
            return (TValue)value.Value;
        }

        /// <summary>
        /// Resets cached values
        /// </summary>
        public void Reset()
        {
            if (!cache.IsValueCreated && !ServiceProvider.IsInitialized) return;

            cache.Value.RemoveAll(kv =>
            {
                var cachedMethodCall = kv.Key as CachedMethodCallDescriptor;
                return cachedMethodCall != null && cachedMethodCall.Target == this;
            });
            cache = new Lazy<ICache>(() => ServiceProvider.Current.GetService<ICache>());
        }

        public void Dispose()
        {
            Reset();
        }
    }
}
