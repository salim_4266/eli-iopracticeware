﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Diagnostics;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Transactions;
using Soaf;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Domain;
using Soaf.IO;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Threading;
using Soaf.TypeSystem;
using Soaf.Validation;
using File = System.IO.File;

[assembly: Component(typeof(SynchronizationBehavior))]
[assembly: Component(typeof(AuthorizationBehavior))]
[assembly: Component(typeof(PrincipalContext), typeof(IPrincipalContext), Priority = -2)]
[assembly: Component(typeof(LambdaEvaluator), typeof(IEvaluator))]
[assembly: Component(typeof(IServiceProvider<>))]
[assembly: Component(typeof(TraceLogger), typeof(ILogger))]
[assembly: Component(typeof(ILogManager))]

[assembly: Component(typeof(IAuthenticationService), Priority = -1)]

namespace Soaf.Collections
{
    public static class Enumerables
    {
        private static readonly IDictionary<Type, Action<object, IDictionary<string, object>>> PropertyDictionaryConverterCache = new Dictionary<Type, Action<object, IDictionary<string, object>>>().Synchronized();

        internal static readonly MethodInfo ToListMethod = Reflector.GetMember(() => new object[0].ToList()).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo ToArrayMethod = Reflector.GetMember(() => new object[0].ToArray()).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo OfTypeMethod = Reflector.GetMember(() => new object[0].OfType<int>()).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo ContainsMethod = Reflector.GetMember(() => new int[0].Contains(0)).CastTo<MethodInfo>().GetGenericMethodDefinition();

        public enum MoveDirection { Forward, Backward }

        /// <summary>
        /// Recurses the specified collection.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="selector">The selector.</param>
        /// <returns></returns>
        public static IEnumerable<TSource> Recurse<TSource>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TSource>> selector)
        {
            foreach (var item in source)
            {
                yield return item;
                foreach (var subItem in selector(item).Recurse(selector))
                {
                    yield return subItem;
                }
            }
        }

        /// <summary>
        /// Batches the source sequence into sized buckets.
        /// </summary>
        /// <typeparam name="TSource">Type of elements in <paramref name="source"/> sequence.</typeparam>
        /// <param name="source">The source sequence.</param>
        /// <param name="size">Size of buckets.</param>
        /// <returns>A sequence of equally sized buckets containing elements of the source collection.</returns>
        /// <remarks> This operator uses deferred execution and streams its results (buckets and bucket content).</remarks>
        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(this IEnumerable<TSource> source, int size)
        {
            return Batch(source, size, x => x);
        }

        /// <summary>
        /// Batches the source sequence into sized buckets and applies a projection to each bucket.
        /// </summary>
        /// <typeparam name="TSource">Type of elements in <paramref name="source"/> sequence.</typeparam>
        /// <typeparam name="TResult">Type of result returned by <paramref name="resultSelector"/>.</typeparam>
        /// <param name="source">The source sequence.</param>
        /// <param name="size">Size of buckets.</param>
        /// <param name="resultSelector">The projection to apply to each bucket.</param>
        /// <returns>A sequence of projections on equally sized buckets containing elements of the source collection.</returns>
        /// <remarks> This operator uses deferred execution and streams its results (buckets and bucket content).</remarks>
        public static IEnumerable<TResult> Batch<TSource, TResult>(this IEnumerable<TSource> source, int size,
                                                                   Func<IEnumerable<TSource>, TResult> resultSelector)
        {
            return BatchImpl(source, size, resultSelector);
        }

        private static IEnumerable<TResult> BatchImpl<TSource, TResult>(this IEnumerable<TSource> source, int size,
                                                                        Func<IEnumerable<TSource>, TResult> resultSelector)
        {
            TSource[] bucket = null;
            int count = 0;

            foreach (TSource item in source)
            {
                if (bucket == null)
                {
                    bucket = new TSource[size];
                }

                bucket[count++] = item;

                // The bucket is fully buffered before it's yielded
                if (count != size)
                {
                    continue;
                }

                // Select is necessary so bucket contents are streamed too
                yield return resultSelector(bucket.Select(x => x));

                bucket = null;
                count = 0;
            }

            // Return the last bucket with all remaining elements
            if (bucket != null && count > 0)
            {
                yield return resultSelector(bucket.Take(count));
            }
        }

        /// <summary>
        /// Takes the last n elements from the specified source.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        public static IEnumerable<T> Last<T>(this IEnumerable<T> source, int count)
        {
            // ReSharper disable PossibleMultipleEnumeration
            return source.Skip(Math.Max(0, source.Count() - count));
            // ReSharper restore PossibleMultipleEnumeration
        }

        public static void Move(this IList collection, object itemToMove, MoveDirection direction)
        {
            var newIndex = collection.IndexOf(itemToMove);
            if ((direction == MoveDirection.Backward))
            {
                newIndex -= 1;
            }
            else
            {
                newIndex += 1;
            }

            if ((newIndex < 0))
            {
                newIndex = 0;
            }
            else if (newIndex > collection.Count - 1)
            {
                newIndex = collection.Count - 1;
            }

            // remove the item to move
            collection.Remove(itemToMove);

            if ((newIndex < collection.Count))
            {
                collection.Insert(newIndex, itemToMove);
            }
            else
            {
                collection.Add(itemToMove);
            }
        }

        /// <summary>
        /// Gets the entity from the collection with the specified id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public static T WithId<T>(this IEnumerable<T> source, object id)
        {
            PropertyInfo idProperty = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).FirstOrDefault(p => p.Name.Equals("id", StringComparison.OrdinalIgnoreCase) || p.Name.Equals(typeof(T).Name + "id", StringComparison.OrdinalIgnoreCase));
            if (idProperty == null || !idProperty.CanRead || idProperty.GetIndexParameters().Length != 0)
            {
                throw new InvalidOperationException("{0} does not have an id property.".FormatWith(typeof(T).Name));
            }

            return source.FirstOrDefault(i => Equals(idProperty.GetValue(i, null), id));
        }

        /// <summary>
        /// Gets the entity from the collection with the specified name.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public static T WithName<T>(this IEnumerable<T> source, object id)
        {
            PropertyInfo nameProperty = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).FirstOrDefault(p => p.Name.Equals("name", StringComparison.OrdinalIgnoreCase) || p.Name.Equals(typeof(T).Name + "id", StringComparison.OrdinalIgnoreCase));
            if (nameProperty == null || !nameProperty.CanRead || nameProperty.GetIndexParameters().Length != 0)
            {
                throw new InvalidOperationException("{0} does not have a name property.".FormatWith(typeof(T).Name));
            }

            return source.FirstOrDefault(i => Equals(nameProperty.GetValue(i, null), id));
        }

        /// <summary>
        ///   Returns a synchronized version of the specified source dictionary.
        /// </summary>
        /// <typeparam name = "TKey">The type of the key.</typeparam>
        /// <typeparam name = "TValue">The type of the value.</typeparam>
        /// <param name = "source">The source.</param>
        /// <returns></returns>
        public static IDictionary<TKey, TValue> Synchronized<TKey, TValue>(this IDictionary<TKey, TValue> source)
        {
            return new SynchronizedDictionary<TKey, TValue>(source);
        }

        /// <summary>
        /// Tries to execute the action on each item. Throws an aggregate exception if any fail.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="action">The action.</param>
        public static void TryForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            int count = 0;
            var exceptions = new List<Exception>();
            source.ForEach(i =>
            {
                count++;
                try
                {
                    action(i);
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            });
            if (exceptions.Count == 1 && count == 1)
            {
                exceptions.First().Rethrow();
            }
            if (exceptions.Count > 0)
            {
                throw new AggregateException(exceptions);
            }
        }

        /// <summary>
        ///   Gets a hash code comprised of the items in the source..
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns></returns>
        internal static int GetSequenceHashCode(this IEnumerable source)
        {
            return source.Cast<object>().Aggregate(173, (current, item) => current ^ (item != null ? item.GetHashCode() : -1));
        }

        /// <summary>
        /// Gets the index of the specified element in the source, using the specified equality comparer.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="element">The element.</param>
        /// <param name="comparer">The comparer.</param>
        /// <returns></returns>
        public static int IndexOf<T>(this IEnumerable<T> source, T element, IEqualityComparer<T> comparer = null)
        {
            if (comparer == null)
            {
                comparer = EqualityComparer<T>.Default;
            }

            int index = 0;

            foreach (T item in source)
            {
                if (comparer.Equals(item, element))
                {
                    return index;
                }
                index++;
            }

            return -1;
        }

        /// <summary>
        /// Gets the last index of the specified value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int LastIndexOf<T>(this IEnumerable<T> source, T value)
        {
            int reverseIndex = source.Reverse().IndexOf(value);
            return reverseIndex == -1 ? -1 : source.Count() - reverseIndex - 1;
        }

        /// <summary>
        /// Gets the last index of the specified value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        public static int LastIndexOf<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            int reverseIndex = source.Reverse().IndexOf(predicate);
            return reverseIndex == -1 ? -1 : source.Count() - reverseIndex - 1;
        }

        /// <summary>
        ///   Returns the indexes matching the specified predicate.
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="source"> The source. </param>
        /// <param name="predicate"> The predicate. </param>
        /// <returns> </returns>
        public static IEnumerable<int> IndexesOf<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            return source.Select((i, index) => new { i, index }).Where(x => predicate(x.i)).Select(x => x.index);
        }

        /// <summary>
        ///   Returns the indexes matching the specified predicate.
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="source"> The source. </param>
        /// <param name="predicate"> The predicate. </param>
        /// <returns> </returns>
        public static int IndexOf<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            return source.IndexesOf(predicate).DefaultIfEmpty(-1).FirstOrDefault();
        }

        /// <summary>
        ///   Infers the enumerable element type and creates a typed list based on the enumerable.
        /// </summary>
        /// <param name = "source">The enumerable.</param>
        /// <returns></returns>
        public static IList ToInferredElementTypeList(this IEnumerable source)
        {
            Type elementType = source.FindElementType();
            return ToListMethod.MakeGenericMethod(elementType).GetInvoker()(null, source).CastTo<IList>();
        }

        /// <summary>
        ///   Infers the enumerable element type and creates a typed array based on the enumerable.
        /// </summary>
        /// <param name = "source">The enumerable.</param>
        /// <returns></returns>
        public static Array ToInferredElementTypeArray(this IEnumerable source)
        {
            Type elementType = source.FindElementType();
            return ToArrayMethod.MakeGenericMethod(elementType).GetInvoker()(null, OfTypeMethod.MakeGenericMethod(elementType).GetInvoker()(null, source)).CastTo<Array>();
        }

        /// <summary>
        /// Creates a strongly typed array.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="elementType">Type of the element.</param>
        /// <returns></returns>
        public static Array ToArray(this IEnumerable source, Type elementType)
        {
            return (Array)ToArrayMethod.MakeGenericMethod(elementType).Invoke(null, new[] { OfTypeMethod.MakeGenericMethod(elementType).Invoke(null, new[] { source }) });
        }

        /// <summary>
        /// Creates a strongly typed list.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="elementType">Type of the element.</param>
        /// <returns></returns>
        public static IList ToList(this IEnumerable source, Type elementType)
        {
            return (IList)ToListMethod.MakeGenericMethod(elementType).Invoke(null, new[] { OfTypeMethod.MakeGenericMethod(elementType).Invoke(null, new[] { source }) });
        }

        /// <summary>
        /// Creates a strongly typed enumerable.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="elementType">Type of the element.</param>
        /// <returns></returns>
        public static IEnumerable OfType(this IEnumerable source, Type elementType)
        {
            return (IEnumerable)OfTypeMethod.MakeGenericMethod(elementType).Invoke(null, new[] { source });
        }


        /// <summary>
        ///   Converts a non generic enumerable to an array of objects.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns></returns>
        public static object[] ToObjectArray(this IEnumerable source)
        {
            return source.OfType<object>().ToArray();
        }

        /// <summary>
        ///   Creates an enumerable of the single element specified.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "value">The value.</param>
        /// <returns></returns>
        public static IEnumerable<T> CreateEnumerable<T>(this T value)
        {
            yield return value;
        }

        /// <summary>
        ///   Creates an arrray of the single element specified.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "value">The value.</param>
        /// <returns></returns>
        public static T[] CreateArray<T>(T value)
        {
            return CreateEnumerable(value).ToArray();
        }

        /// <summary>
        ///   Performs the specified action on each item in the source. Property changed notifications are suppressed until operation is complete
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <param name = "action">The action.</param>
        /// <returns></returns>
        public static IEnumerable<T> ForEachWithSinglePropertyChangedNotification<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source is IRaisePropertyChanged || source is IRaiseCollectionChanged)
            {
                using (new SuppressPropertyChangedScope())
                {
                    foreach (T item in source)
                    {
                        action(item);
                    }
                }
                source.As<IRaisePropertyChanged>().IfNotNull(i => i.OnPropertyChanged(new PropertyChangedEventArgs("Item[]")));
                source.As<IRaiseCollectionChanged>().IfNotNull(i => i.OnCollectionChanged());
            }
            else
            {
                foreach (T item in source)
                {
                    action(item);
                }
            }
            return source;
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T item in source)
            {
                action(item);
            }
            return source;
        }

        /// <summary>
        ///   Adds the items to the list.
        /// </summary>
        /// <param name = "list">The list.</param>
        /// <param name = "items">The items.</param>
        /// <returns></returns>
        public static IList<T> AddRangeWithSinglePropertyChangedNotification<T>(this IList<T> list, IEnumerable items)
        {
            if (list is IRaisePropertyChanged || list is IRaiseCollectionChanged)
            {
                var oldCount = list.Count;

                using (new SuppressPropertyChangedScope())
                {
                    foreach (object item in items.OfType<object>().ToArray())
                    {
                        list.Add((T)item);
                    }
                }

                if (list.Count != oldCount)
                {
                    list.As<IRaisePropertyChanged>().IfNotNull(i => i.OnPropertyChanged(new PropertyChangedEventArgs("Count")));
                    list.As<IRaiseCollectionChanged>().IfNotNull(i => i.OnCollectionChanged());
                }
            }
            else
            {
                foreach (object item in items.OfType<object>().ToArray())
                {
                    list.Add((T)item);
                }
            }
            return list;
        }

        public static IList<T> AddRange<T>(this IList<T> list, IEnumerable items)
        {
            foreach (object item in items.OfType<object>().ToArray())
            {
                list.Add((T)item);
            }
            return list;
        }

        /// <summary>
        ///   Removes the items from the list.
        /// </summary>
        /// <param name = "list">The list.</param>
        /// <param name = "items">The items.</param>
        /// <returns></returns>
        public static IList<T> RemoveRangeWithSinglePropertyChangedNotification<T>(this IList<T> list, IEnumerable items)
        {
            if (list is IRaisePropertyChanged || list is IRaiseCollectionChanged)
            {
                var oldCount = list.Count;

                using (new SuppressPropertyChangedScope())
                {
                    foreach (object item in items.OfType<object>().ToArray())
                    {
                        list.Remove((T)item);
                    }
                }

                if (list.Count != oldCount)
                {
                    list.As<IRaisePropertyChanged>().IfNotNull(i => i.OnPropertyChanged(new PropertyChangedEventArgs("Count")));
                    list.As<IRaiseCollectionChanged>().IfNotNull(i => i.OnCollectionChanged());
                }
            }
            else
            {
                foreach (object item in items.OfType<object>().ToArray())
                {
                    list.Remove((T)item);
                }
            }
            return list;
        }

        public static IList<T> RemoveRange<T>(this IList<T> list, IEnumerable items)
        {
            foreach (object item in items.OfType<object>().ToArray())
            {
                list.Remove((T)item);
            }
            return list;
        }

        /// <summary>
        ///   Adds the items to the list.
        /// </summary>
        /// <param name = "list">The list.</param>
        /// <param name = "items">The items.</param>
        /// <returns></returns>
        public static T AddRangeWithSinglePropertyChangedNotification<T>(this T list, IEnumerable items) where T : IList
        {
            if (list is IRaisePropertyChanged || list is IRaiseCollectionChanged)
            {
                var oldCount = list.Count;

                using (new SuppressPropertyChangedScope())
                {
                    foreach (object item in items.OfType<object>().ToArray())
                    {
                        list.Add(item);
                    }
                }

                if (list.Count != oldCount)
                {
                    list.As<IRaisePropertyChanged>().IfNotNull(i => i.OnPropertyChanged(new PropertyChangedEventArgs("Count")));
                    list.As<IRaiseCollectionChanged>().IfNotNull(i => i.OnCollectionChanged());
                }
            }
            else
            {
                foreach (object item in items.OfType<object>().ToArray())
                {
                    list.Add(item);
                }
            }
            return list;
        }

        public static T AddRange<T>(this T list, IEnumerable items) where T : IList
        {
            foreach (object item in items.OfType<object>().ToArray())
            {
                list.Add(item);
            }
            return list;
        }

        /// <summary>
        ///   Yields items in the source that are not default(T).
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "source">The source.</param>
        /// <returns></returns>
        public static IEnumerable<T> WhereNotDefault<T>(this IEnumerable<T> source)
        {
            return source.Where(i => !Equals(i, default(T)));
        }

        /// <summary>
        /// Gets the value or inserts if not present. Locks the dictionary during the check and insert.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="getValue">The get value.</param>
        /// <returns></returns>
        public static bool TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey key, out TValue value, Func<TValue> getValue)
        {
            lock (source)
            {
                if (!source.TryGetValue(key, out value))
                {
                    source[key] = value = getValue();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        ///   Tries the get value when the value type doesn't correspond exactly to the generic dictionary's value type.
        /// </summary>
        /// <typeparam name = "TKey">The type of the key.</typeparam>
        /// <typeparam name = "TValue">The type of the value.</typeparam>
        /// <typeparam name = "TValueOut">The type of the value out.</typeparam>
        /// <param name = "source">The source.</param>
        /// <param name = "key">The key.</param>
        /// <param name = "value">The value.</param>
        /// <returns></returns>
        public static bool TryGetValue<TKey, TValue, TValueOut>(this IDictionary<TKey, TValue> source, TKey key, out TValueOut value)
        {
            TValue v;
            if (source.TryGetValue(key, out v))
            {
                value = v.CastTo<TValueOut>();
                return true;
            }
            value = default(TValueOut);
            return false;
        }

        /// <summary>
        /// Executes the action in parallel for all elements of the enumerable.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="action">The action.</param>
        /// <param name="propogateTransactions">if set to <c>true</c> [propogate transactions] to the new threads.</param>
        /// <param name="degreeOfParallelism">The degree of parallelism.</param>
        public static void ForAllInParallel<TSource>(this IEnumerable<TSource> source, Action<TSource> action, bool propogateTransactions = true, int? degreeOfParallelism = null)
        {
            var parallelSource = source.AsParallel();

            if (degreeOfParallelism.HasValue)
            {
                parallelSource = parallelSource.WithDegreeOfParallelism(degreeOfParallelism.Value);
            }

            var transaction = Transaction.Current;
            parallelSource.ForAll(i =>
            {
                if (propogateTransactions)
                {
                    using (new DependentTransactionScope(transaction, true)) action(i);
                }
                else
                {
                    action(i);
                }
            });
        }

        /// <summary>
        ///   Replaces the specified itemToReplace in the source.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "source">The source.</param>
        /// <param name = "itemToReplace">The item to newValue.</param>
        /// <param name = "replacement">The replacement.</param>
        public static void Replace<T>(this IList<T> source, T itemToReplace, T replacement)
        {
            int index = source.IndexOf(itemToReplace);
            if (index >= 0)
            {
                source.Remove(itemToReplace);
                source.Insert(index, replacement);
            }
        }

        /// <summary>
        ///   Finds the element type of the object if it is enumerable.
        /// </summary>
        /// <param name = "value">The value.</param>
        /// <returns></returns>
        public static Type FindElementType(this IEnumerable value)
        {
            if (value == null) return null;

            if (value is IQueryable)
            {
                return value.CastTo<IQueryable>().ElementType;
            }
            Type type = value.GetType().FindElementType();
            if (type == typeof(object))
            {
                type = value.As<IEnumerable>().OfType<object>().FirstOrDefault(i => i != null).IfNotNull(i => i.GetType());
            }
            return type ?? typeof(object);
        }

        /// <summary>
        ///   Returns the maximal element of the given sequence, based on
        ///   the given projection.
        /// </summary>
        /// <remarks>
        ///   If more than one element has the maximal projected value, the first
        ///   one encountered will be returned. This overload uses the default comparer
        ///   for the projected type. This operator uses immediate execution, but
        ///   only buffers a single result (the current maximal element).
        /// </remarks>
        /// <typeparam name = "TSource">Type of the source sequence</typeparam>
        /// <typeparam name = "TKey">Type of the projected element</typeparam>
        /// <param name = "source">Source sequence</param>
        /// <param name = "selector">Selector to use to pick the results to compare</param>
        /// <returns>The maximal element, according to the projection.</returns>
        /// <exception cref = "ArgumentNullException"><paramref name = "source" /> or <paramref name = "selector" /> is null</exception>
        /// <exception cref = "InvalidOperationException"><paramref name = "source" /> is empty</exception>
        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source,
                                                   Func<TSource, TKey> selector)
        {
            return source.MaxBy(selector, Comparer<TKey>.Default);
        }

        /// <summary>
        ///   Returns the maximal element of the given sequence, based on
        ///   the given projection and the specified comparer for projected values.
        /// </summary>
        /// <remarks>
        ///   If more than one element has the maximal projected value, the first
        ///   one encountered will be returned. This overload uses the default comparer
        ///   for the projected type. This operator uses immediate execution, but
        ///   only buffers a single result (the current maximal element).
        /// </remarks>
        /// <typeparam name = "TSource">Type of the source sequence</typeparam>
        /// <typeparam name = "TKey">Type of the projected element</typeparam>
        /// <param name = "source">Source sequence</param>
        /// <param name = "selector">Selector to use to pick the results to compare</param>
        /// <param name = "comparer">Comparer to use to compare projected values</param>
        /// <returns>The maximal element, according to the projection.</returns>
        /// <exception cref = "ArgumentNullException"><paramref name = "source" />, <paramref name = "selector" /> 
        ///   or <paramref name = "comparer" /> is null</exception>
        /// <exception cref = "InvalidOperationException"><paramref name = "source" /> is empty</exception>
        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source,
                                                   Func<TSource, TKey> selector, IComparer<TKey> comparer)
        {
            using (IEnumerator<TSource> sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    return default(TSource);
                }
                TSource max = sourceIterator.Current;
                TKey maxKey = selector(max);
                while (sourceIterator.MoveNext())
                {
                    TSource candidate = sourceIterator.Current;
                    TKey candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, maxKey) > 0)
                    {
                        max = candidate;
                        maxKey = candidateProjected;
                    }
                }
                return max;
            }
        }

        /// <summary>
        /// Checks if the sequences are equal, checking if the first is null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first">The source.</param>
        /// <param name="second">The second.</param>
        /// <param name="equalityComparer">The equality comparer.</param>
        /// <returns></returns>
        public static bool SequenceEqualWithNullCheck<T>(this IEnumerable<T> first, IEnumerable second, IEqualityComparer<T> equalityComparer = null)
        {
            if (first == null && second == null)
            {
                return true;
            }
            if (first == null || second == null)
            {
                return false;
            }

            return first.SequenceEqual(second.Cast<T>(), equalityComparer);
        }

        /// <summary>
        ///   Merges the source dictionary into the target dictionary. Overwrites existing values in target.
        /// </summary>
        /// <typeparam name = "TKey">The type of the key.</typeparam>
        /// <typeparam name = "TValue">The type of the value.</typeparam>
        /// <param name = "target">The target.</param>
        /// <param name = "source">The source.</param>
        public static IDictionary<TKey, TValue> Merge<TKey, TValue>(this IDictionary<TKey, TValue> target, IDictionary<TKey, TValue> source)
        {
            foreach (var item in source)
            {
                target[item.Key] = item.Value;
            }
            return target;
        }

        /// <summary>
        ///   Merges the source dictionary into the target dictionary. Overwrites existing values in target.
        /// </summary>
        /// <param name = "target">The target.</param>
        /// <param name = "source">The source.</param>
        public static IDictionary Merge(this IDictionary target, IDictionary source)
        {
            foreach (object key in source.Keys)
            {
                target[key] = source[key];
            }
            return target;
        }

        /// <summary>
        ///   Determines whether the specified source is not null or empty.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns>
        ///   <c>true</c> if [is null or empty] [the specified source]; otherwise, <c>false</c>.
        /// </returns>
        [AssertionMethod]
        public static bool IsNotNullOrEmpty([AssertionCondition(AssertionConditionType.IsNotNull)] this IEnumerable source)
        {
            return !source.IsNullOrEmpty();
        }

        /// <summary>
        ///   Determines whether the specified source is null or empty.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns>
        ///   <c>true</c> if [is null or empty] [the specified source]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty(this IEnumerable source)
        {
            var collection = source as ICollection;
            if (collection != null)
            {
                return collection.Count == 0;
            }
            return source == null || !source.GetEnumerator().MoveNext();
        }

        /// <summary>
        ///   Converts the value into a dictionary of property-value pairs.
        /// </summary>
        /// <param name = "value">The value.</param>
        /// <returns></returns>
        public static IDictionary<string, object> ToPropertyDictionary(object value)
        {
            var result = new Dictionary<string, object>();

            if (value == null)
            {
                return result;
            }

            Type valueType = value.GetType();

            Action<object, IDictionary<string, object>> converter;

            lock (PropertyDictionaryConverterCache)
            {
                PropertyDictionaryConverterCache.TryGetValue(valueType, out converter);
            }
            if (converter == null)
            {
                converter = CreatePropertyDictionaryConverter(valueType);
                lock (PropertyDictionaryConverterCache)
                {
                    PropertyDictionaryConverterCache[valueType] = converter;
                }
            }

            converter(value, result);

            return result;
        }


        /// <summary>
        ///   Creates a converter that takes an object and puts its properties into a dictionary.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        internal static Action<object, IDictionary<string, object>> CreatePropertyDictionaryConverter(this Type type)
        {
            ParameterExpression source = Expression.Parameter(typeof(object));
            ParameterExpression target = Expression.Parameter(typeof(IDictionary<string, object>));

            List<Expression> statements =
                (from pi in type.GetProperties().Where(pi => pi.CanRead && pi.GetIndexParameters().Length == 0)
                 let key = Expression.Constant(pi.Name, typeof(object))
                 let value =
                     Expression.Convert(
                         Expression.Call(Expression.Convert(source, pi.DeclaringType.EnsureNotDefault("PropertyInfo should not be null.")), pi.GetGetMethod(true)),
                         typeof(object))
                 select
                     Expression.Call(target, target.Type.GetMethod("Add", new[] { typeof(string), typeof(object) }), key,
                                     value)).Cast<Expression>().ToList();

            BlockExpression block = Expression.Block(statements);

            Expression<Action<object, IDictionary<string, object>>> lambda = Expression.Lambda<Action<object, IDictionary<string, object>>>(block, source, target);

            return lambda.Compile();
        }

        /// <summary>
        ///   A get safe means of accessing dictionary values (no exception thrown if key is not present).
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <param name = "key">The key.</param>
        /// <returns></returns>
        public static object GetValue(this IDictionary source, object key)
        {
            if (source.Contains(key))
            {
                return source[key];
            }
            return null;
        }

        /// <summary>
        /// A get safe means of accessing dictionary values (no exception thrown if key is not present).
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey key)
        {
            return source.GetValue(key, null);
        }

        /// <summary>
        /// A get safe means of accessing dictionary values (no exception thrown if key is not present).
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="key">The key.</param>
        /// <param name="getValue">The get value.</param>
        /// <returns></returns>
        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey key, Func<TValue> getValue)
        {
            TValue value;
            if (getValue != null)
            {
                source.TryGetValue(key, out value, getValue);
            }
            else
            {
                source.TryGetValue(key, out value);
            }
            return value;
        }

        /// <summary>
        ///   Adds from the items specified where the source list does not already contain the item.
        /// </summary>
        /// <typeparam name = "TList">The type of the list.</typeparam>
        /// <typeparam name = "TItem">The type of the item.</typeparam>
        /// <param name = "list">The list.</param>
        /// <param name = "items">The items.</param>
        /// <returns></returns>
        public static TList AddDistinct<TList, TItem>(this TList list, IEnumerable<TItem> items) where TList : IList<TItem>
        {
            foreach (TItem item in items)
            {
                if (!list.Contains(item))
                {
                    list.Add(item);
                }
            }
            return list;
        }

        /// <summary>
        ///   Removes all the items specified from the source list.
        /// </summary>
        /// <typeparam name = "TList">The type of the list.</typeparam>
        /// <typeparam name = "TItem">The type of the item.</typeparam>
        /// <param name = "list">The list.</param>
        /// <param name = "items">The items.</param>
        /// <returns></returns>
        public static TList RemoveAll<TList, TItem>(this TList list, IEnumerable<TItem> items) where TList : IList<TItem>
        {
            foreach (TItem item in items)
            {
                if (list.Contains(item))
                {
                    list.Remove(item);
                }
            }
            return list;
        }

        /// <summary>
        /// Caches the results of enumerating over a given object so that subsequence enumerations
        /// don't require interacting with the object a second time.
        /// </summary>
        /// <typeparam name="T">The type of element found in the enumeration.</typeparam>
        /// <param name="sequence">The enumerable object.</param>
        /// <returns>
        /// Either a new enumerable object that caches enumerated results, or the original, <paramref name="sequence"/>
        /// object if no caching is necessary to avoid additional CPU work.
        /// </returns>
        /// <remarks>
        /// 	<para>This is designed for use on the results of generator methods (the ones with <c>yield return</c> in them)
        /// so that only those elements in the sequence that are needed are ever generated, while not requiring
        /// regeneration of elements that are enumerated over multiple times.</para>
        /// 	<para>This can be a huge performance gain if enumerating multiple times over an expensive generator method.</para>
        /// 	<para>Some enumerable types such as collections, lists, and already-cached generators do not require
        /// any (additional) caching, and this method will simply return those objects rather than caching them
        /// to avoid double-caching.</para>
        /// </remarks>
        public static IEnumerable<T> Cached<T>(this IEnumerable<T> sequence)
        {
            // Don't create a cache for types that don't need it.
            if (sequence is IList<T> ||
                sequence is ICollection<T> ||
                sequence is Array ||
                sequence is CachedEnumerable<T>)
            {
                return sequence;
            }

            return new CachedEnumerable<T>(sequence);
        }

        /// <summary>
        /// Joins the specified string property based on the specified selector, separated by the specified delimiter.
        /// </summary>
        /// <typeparam name="T">The type of the collection values</typeparam>
        /// <param name="source">The values to join</param>
        /// <param name="selector">Selector delegate to choose the property to join on</param>
        /// <param name="delimiter">The delimiter string used to separate each string value</param>
        /// <param name="excludeEmptyItems">if set to <c>true</c> [exclude empty items].</param>
        /// <returns></returns>
        public static string Join<T>(this IEnumerable<T> source, Func<T, string> selector, string delimiter = ", ", bool excludeEmptyItems = false)
        {
            return source.Select(selector).Join(delimiter, excludeEmptyItems);
        }

        /// <summary>
        ///   Joins the specified strings values, separated by the specified delimiter.
        /// </summary>
        /// <param name = "values">The values.</param>
        /// <param name = "delimiter">The delimiter.</param>
        /// <param name = "excludeEmptyItems">if set to <c>true</c> [exclude empty items].</param>
        /// <returns></returns>
        public static string Join(this IEnumerable values, string delimiter = ", ", bool excludeEmptyItems = false)
        {
            var sb = new StringBuilder();

            foreach (object value in values)
            {
                if (!excludeEmptyItems || (value != null && value.ToString().Trim() != string.Empty))
                {
                    sb.Append(value);
                    sb.Append(delimiter);
                }
            }
            if (sb.Length > 0)
            {
                sb.Remove(sb.Length - delimiter.Length, delimiter.Length);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Synchronizes target collection items of one type with source items of another type
        /// </summary>
        /// <typeparam name="TSourceItem">The type of the source item.</typeparam>
        /// <typeparam name="TDestinationItem">The type of the destination item.</typeparam>
        /// <param name="sourceCollection">The source collection.</param>
        /// <param name="targetCollection">The target collection.</param>
        /// <param name="isSameItem">Checks and returns whether item from source and destination represent the same record. For example, with same Id.</param>
        /// <param name="updateTargetItem">Updates target item contents from source</param>
        /// <param name="deleteTargetItem">By default item is simply deleted from its collection. Override if another behavior expected.</param>
        /// <param name="syncItemsOrder">if set to <c>true</c> syncs also order of items in target collection.</param>
        public static void SyncToTargetCollection<TSourceItem, TDestinationItem>(this ICollection<TSourceItem> sourceCollection,
                                                                                 ICollection<TDestinationItem> targetCollection,
                                                                                 Func<TSourceItem, TDestinationItem, bool> isSameItem,
                                                                                 Action<TSourceItem, TDestinationItem> updateTargetItem,
                                                                                 Action<TDestinationItem, ICollection<TDestinationItem>> deleteTargetItem = null,
                                                                                 bool syncItemsOrder = true)
            where TDestinationItem : class, new()
        {
            SyncToTargetCollection(sourceCollection, targetCollection, isSameItem,
                                   item =>
                                   {
                                       var targetItem = new TDestinationItem();
                                       updateTargetItem(item, targetItem);
                                       return targetItem;
                                   }, updateTargetItem, deleteTargetItem);
        }

        /// <summary>
        /// Synchronizes target collection items of one type with source items of another type
        /// </summary>
        /// <typeparam name="TSourceItem">The type of the source item.</typeparam>
        /// <typeparam name="TDestinationItem">The type of the destination item.</typeparam>
        /// <param name="sourceCollection">The source collection.</param>
        /// <param name="targetCollection">The target collection.</param>
        /// <param name="isSameItem">Checks and returns whether item from source and destination represent the same record. For example, with same Id.</param>
        /// <param name="createNewDestinationItem">Creates new destination item.</param>
        /// <param name="updateTargetItem">Updates target item contents from source</param>
        /// <param name="deleteTargetItem">By default item is simply deleted from its collection. Override if another behavior expected.</param>
        /// <param name="syncItemsOrder">if set to <c>true</c> syncs also order of items in target collection.</param>
        public static void SyncToTargetCollection<TSourceItem, TDestinationItem>(this ICollection<TSourceItem> sourceCollection,
                                                                                 ICollection<TDestinationItem> targetCollection,
                                                                                 Func<TSourceItem, TDestinationItem, bool> isSameItem,
                                                                                 Func<TSourceItem, TDestinationItem> createNewDestinationItem,
                                                                                 Action<TSourceItem, TDestinationItem> updateTargetItem,
                                                                                 Action<TDestinationItem, ICollection<TDestinationItem>> deleteTargetItem = null,
                                                                                 bool syncItemsOrder = true)
        {
            // If source or destination collections are not present -> quit sync
            if (sourceCollection == null || targetCollection == null) return;

            var lastItemIndex = 0;
            var targetCollectionItems = new Dictionary<TDestinationItem, int>(sourceCollection.Count);

            // Upsert items first
            foreach (var sourceItem in sourceCollection)
            {
                // Find item. Not using LINQ here, since we need to know if the item has been found
                var found = false;
                TDestinationItem itemToAdd = default(TDestinationItem);
                foreach (var destinationItem in targetCollection)
                {
                    found = isSameItem(sourceItem, destinationItem);
                    if (!found) continue;

                    itemToAdd = destinationItem;
                    break;
                }

                // Update it or create
                if (!found)
                {
                    itemToAdd = createNewDestinationItem(sourceItem);
                    targetCollection.Add(itemToAdd);
                }
                else
                {
                    updateTargetItem(sourceItem, itemToAdd);
                }

                // Remember the item and its index
                targetCollectionItems.Add(itemToAdd, lastItemIndex++);
            }

            // Remove items not present in source collection anymore
            var itemsToDelete = targetCollection
                .Where(p => !targetCollectionItems.ContainsKey(p))
                .ToList();

            foreach (var itemToDelete in itemsToDelete)
            {
                if (deleteTargetItem == null)
                {
                    targetCollection.Remove(itemToDelete);
                }
                else
                {
                    deleteTargetItem(itemToDelete, targetCollection);
                }
            }

            // Ensure items are properly ordered
            if (!syncItemsOrder) return;

            var targetCollectionList = targetCollection as IList;
            if (targetCollectionList == null) throw new ArgumentException("Target collection must support IList interface to synchronize order with source", "targetCollection");

            // Sort either with ISortableList interface or ArrayList
            var comparer = GenericComparer<TDestinationItem>.With((x, y) => targetCollectionItems[x] - targetCollectionItems[y]);
            var sortableTarget = targetCollectionList as ISortableList<TDestinationItem>;
            if (sortableTarget == null)
            {
                var wrappedList = ArrayList.Adapter(targetCollectionList);
                wrappedList.Sort(comparer);
            }
            else
            {
                sortableTarget.Sort(comparer);
            }
        }

        #region Nested type: EnumerableCache

        /// <summary>
        ///   A wrapper for <see cref = "IEnumerable&lt;T&gt;" /> types and returns a caching <see cref = "IEnumerator&lt;T&gt;" />
        ///   from its <see cref = "IEnumerable&lt;T&gt;.GetEnumerator" /> method.
        /// </summary>
        /// <typeparam name = "T">The type of element in the sequence.</typeparam>
        private class CachedEnumerable<T> : IEnumerable<T>
        {
            /// <summary>
            ///   The original generator method or other enumerable object whose contents should only be enumerated once.
            /// </summary>
            private readonly IEnumerable<T> generator;

            /// <summary>
            ///   The sync object our caching enumerators use when adding a new live generator method result to the cache.
            /// </summary>
            /// <remarks>
            ///   Although individual enumerators are not thread-safe, this <see cref = "IEnumerable&lt;T&gt;" /> should be
            ///   thread safe so that multiple enumerators can be created from it and used from different threads.
            /// </remarks>
            private readonly object generatorLock = new object();

            /// <summary>
            ///   The results from enumeration of the live object that have been collected thus far.
            /// </summary>
            private List<T> cache;

            /// <summary>
            ///   The enumerator we're using over the generator method's results.
            /// </summary>
            private IEnumerator<T> generatorEnumerator;

            /// <summary>
            ///   Initializes a new instance of the EnumerableCache class.
            /// </summary>
            /// <param name = "generator">The generator.</param>
            internal CachedEnumerable(IEnumerable<T> generator)
            {
                if (generator == null)
                {
                    throw new ArgumentNullException("generator");
                }

                this.generator = generator;
            }

            #region IEnumerable<T> Members

            /// <summary>
            ///   Returns an enumerator that iterates through the collection.
            /// </summary>
            /// <returns>
            ///   A <see cref = "T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
            /// </returns>
            public IEnumerator<T> GetEnumerator()
            {
                if (generatorEnumerator == null)
                {
                    cache = new List<T>();
                    generatorEnumerator = generator.GetEnumerator();
                }

                return new CachedEnumerator(this);
            }

            /// <summary>
            ///   Returns an enumerator that iterates through a collection.
            /// </summary>
            /// <returns>
            ///   An <see cref = "T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
            /// </returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            #endregion

            #region Nested type: EnumeratorCache

            /// <summary>
            ///   An enumerator that uses cached enumeration results whenever they are available,
            ///   and caches whatever results it has to pull from the original <see cref = "IEnumerable&lt;T&gt;" /> object.
            /// </summary>
            private class CachedEnumerator : IEnumerator<T>
            {
                /// <summary>
                ///   The parent enumeration wrapper class that stores the cached results.
                /// </summary>
                private readonly CachedEnumerable<T> parent;

                /// <summary>
                ///   The position of this enumerator in the cached list.
                /// </summary>
                private int cachePosition = -1;

                /// <summary>
                ///   Initializes a new instance of the <see cref = "CachedEnumerator" /> class.
                /// </summary>
                /// <param name = "parent">The parent cached enumerable whose GetEnumerator method is calling this constructor.</param>
                internal CachedEnumerator(CachedEnumerable<T> parent)
                {
                    if (parent == null)
                    {
                        throw new ArgumentNullException("parent");
                    }

                    this.parent = parent;
                }

                #region IEnumerator<T> Members

                /// <summary>
                ///   Gets the element in the collection at the current position of the enumerator.
                /// </summary>
                /// <returns>
                ///   The element in the collection at the current position of the enumerator.
                /// </returns>
                public T Current
                {
                    get
                    {
                        if (cachePosition < 0 || cachePosition >= parent.cache.Count)
                        {
                            throw new InvalidOperationException();
                        }

                        return parent.cache[cachePosition];
                    }
                }

                /// <summary>
                ///   Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
                /// </summary>
                public void Dispose()
                {
                    Dispose(true);
                    GC.SuppressFinalize(this);
                }

                #endregion

                #region IEnumerator Methods

                /// <summary>
                ///   Advances the enumerator to the next element of the collection.
                /// </summary>
                /// <returns>
                ///   true if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.
                /// </returns>
                /// <exception cref = "T:System.InvalidOperationException">
                ///   The collection was modified after the enumerator was created.
                /// </exception>
                public bool MoveNext()
                {
                    cachePosition++;
                    if (cachePosition >= parent.cache.Count)
                    {
                        lock (parent.generatorLock)
                        {
                            if (parent.generatorEnumerator.MoveNext())
                            {
                                parent.cache.Add(parent.generatorEnumerator.Current);
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }

                    return true;
                }

                /// <summary>
                ///   Sets the enumerator to its initial position, which is before the first element in the collection.
                /// </summary>
                /// <exception cref = "T:System.InvalidOperationException">
                ///   The collection was modified after the enumerator was created.
                /// </exception>
                public void Reset()
                {
                    cachePosition = -1;
                }

                #endregion

                #region IEnumerator Properties

                /// <summary>
                ///   Gets the element in the collection at the current position of the enumerator.
                /// </summary>
                /// <returns>
                ///   The element in the collection at the current position of the enumerator.
                /// </returns>
                object IEnumerator.Current
                {
                    get { return Current; }
                }

                #endregion

                /// <summary>
                ///   Releases unmanaged and - optionally - managed resources
                /// </summary>
                /// <param name = "disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
                protected virtual void Dispose(bool disposing)
                {
                    // Nothing to do here.
                }
            }

            #endregion
        }

        #endregion
    }

    /// <summary>
    /// A generic object comparerer that only uses an object's reference, 
    /// ignoring any <see cref="IEquatable{T}"/> or <see cref="object.Equals(object)"/>  overrides.
    /// </summary>
    public class ObjectReferenceEqualityComparerer<T> : IEqualityComparer<T>
    {
        public static readonly ObjectReferenceEqualityComparerer<T> Instance = new ObjectReferenceEqualityComparerer<T>();

        #region IEqualityComparer<T> Members

        public bool Equals(T x, T y)
        {
            return ReferenceEquals(x, y);
        }

        public int GetHashCode(T obj)
        {
            return RuntimeHelpers.GetHashCode(obj);
        }

        #endregion
    }

    /// <summary>
    /// An equality comparer that uses Members.
    /// </summary>
    public class PropertyEqualityComparer : EqualityComparer<object>
    {
        private readonly IEnumerable<Invoker> properties;
        private readonly IEnumerable<FieldInfo> fields;

        public PropertyEqualityComparer(IEnumerable<PropertyInfo> properties = null, IEnumerable<FieldInfo> fields = null)
        {
            this.properties = (properties ?? new PropertyInfo[0]).Select(p => p.GetGetMethod(true).GetInvoker());
            this.fields = (fields ?? new FieldInfo[0]);
        }

        public override int GetHashCode(object obj)
        {
            return properties.Select(p => p(obj)).Concat(fields.Select(f => f.GetValue(obj))).GetSequenceHashCode();
        }

        public override bool Equals(object x, object y)
        {
            if (x == null && y == null) return true;
            if (x == null || y == null) return false;

            if (x.GetType() != y.GetType()) return false;

            return properties.Select(p => p(x)).Concat(fields.Select(f => f.GetValue(x)))
                .SequenceEqual(
                    properties.Select(p => p(y)).Concat(fields.Select(f => f.GetValue(y))));
        }
    }

    public class GenericComparer<T> : IComparer<T>, IComparer
    {
        private readonly Comparison<T> comparison;

        public GenericComparer(Comparison<T> comparison)
        {
            this.comparison = comparison;
        }

        public int Compare(T x, T y)
        {
            return comparison(x, y);
        }

        public int Compare(object x, object y)
        {
            return comparison((T)x, (T)y);
        }

        public static GenericComparer<T> With(Comparison<T> comparison)
        {
            return new GenericComparer<T>(comparison);
        }
    }

    public static class EqualityComparer
    {
        public static IEqualityComparer<T> For<T>(Func<T, T, bool> comparer, Func<T, int> getHashCode = null)
        {
            return new GenericEqualityComparer<T>(comparer, getHashCode ?? (o => -1));
        }

        #region Nested type: GenericEqualityComparer

        private class GenericEqualityComparer<T> : EqualityComparer<T>
        {
            private readonly Func<T, T, bool> comparer;
            private readonly Func<T, int> getHashCode;

            public GenericEqualityComparer(Func<T, T, bool> comparer, Func<T, int> getHashCode)
            {
                this.comparer = comparer;
                this.getHashCode = getHashCode;
            }


            public override bool Equals(T x, T y)
            {
                return comparer(x, y);
            }

            public override int GetHashCode(T obj)
            {
                return getHashCode(obj);
            }
        }

        #endregion
    }

    /// <summary>
    /// A list which supports sorting operation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISortableList<T> : IList<T>
    {
        void Sort();
        void Sort(IComparer<T> comparer);
        void Sort(Comparison<T> comparison);
    }

    /// <summary>
    /// A collection that performs fix ups when items are added or removed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FixupCollection<T> : Collection<T>, ISortableList<T>
    {
        public event EventHandler<EventArgs<T>> ItemSet;
        public event EventHandler<EventArgs<T>> ItemRemoved;

        // See: http://stackoverflow.com/questions/9319545/how-to-sort-a-collectiont-in-place
        #region Constructors. By forcing List<T> we will always know that Items is of List<T>

        public FixupCollection(List<T> source) : base(source) { }
        public FixupCollection() : base(new List<T>()) { }

        #endregion

        private HashSet<T> items = new HashSet<T>();

        protected override void ClearItems()
        {
            items.Clear();
            new List<T>(this).ForEach(t => Remove(t));
        }

        protected override void InsertItem(int index, T item)
        {
            if (Contains(item)) return;

            base.InsertItem(index, item);

            items.Add(item);

            if (ItemSet != null)
            {
                ItemSet(this, new EventArgs<T>(item));
            }
        }

        protected override void RemoveItem(int index)
        {
            T item = base[index];
            base.RemoveItem(index);
            items.Remove(item);
            if (ItemRemoved != null)
            {
                ItemRemoved(this, new EventArgs<T>(item));
            }
        }

        protected override void SetItem(int index, T item)
        {
            if (Contains(item)) return;

            base.SetItem(index, item);

            items.Add(item);

            if (ItemSet != null)
            {
                ItemSet(this, new EventArgs<T>(item));
            }
        }

        public new bool Contains(T item)
        {
            return items.Contains(item);
        }

        public void Sort()
        {
            ((List<T>)Items).Sort();
        }

        public void Sort(IComparer<T> comparer)
        {
            ((List<T>)Items).Sort(comparer);
        }

        public void Sort(Comparison<T> comparison)
        {
            ((List<T>)Items).Sort(comparison);
        }
    }
}

namespace Soaf.Threading
{
    public static class Tasks
    {
        /// <summary>
        /// Waits a specified amount of time until the condition is true.
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="timeout"></param>
        /// <param name="sleepTimeout"></param>
        /// <returns></returns>
        public static bool Wait(this Func<bool> predicate, TimeSpan timeout, TimeSpan sleepTimeout = default(TimeSpan))
        {
            if (sleepTimeout == default(TimeSpan)) sleepTimeout = TimeSpan.FromMilliseconds(10);

            var start = DateTime.Now;
            bool lastPredicateResult;
            var waitEvent = new ManualResetEventSlim(false);
            while (true)
            {
                lastPredicateResult = predicate();
                if (!lastPredicateResult && (DateTime.Now - start).TotalMilliseconds <= timeout.TotalMilliseconds)
                {
                    waitEvent.Wait(sleepTimeout);
                    continue;
                }
                break;
            }
            return lastPredicateResult;
        }

        public static System.Threading.Tasks.Task<TResult> StartNewWithCurrentTransaction<TResult>(this System.Threading.Tasks.TaskFactory factory,
                                                                                                   Func<TResult> function,
                                                                                                   CancellationToken cancellationToken,
                                                                                                   System.Threading.Tasks.TaskCreationOptions options,
                                                                                                   System.Threading.Tasks.TaskScheduler scheduler)
        {
            var transaction = Transaction.Current;
            var originalFunction = function;
            function = () =>
                           {
                               TResult result;
                               using (new DependentTransactionScope(transaction, true))
                               {
                                   result = originalFunction();
                               }
                               return result;
                           };

            return factory.StartNew(function, cancellationToken, options, scheduler);
        }

        public static System.Threading.Tasks.Task<TResult> StartNewWithCurrentTransaction<TResult>(this System.Threading.Tasks.TaskFactory factory,
                                                                                                   Func<TResult> function)
        {
            var transaction = Transaction.Current;
            var originalFunction = function;
            function = () =>
                           {
                               TResult result;
                               using (new DependentTransactionScope(transaction, true))
                               {
                                   result = originalFunction();
                               }
                               return result;
                           };

            return factory.StartNew(function);
        }

        public static System.Threading.Tasks.Task StartNewWithCurrentTransaction(this System.Threading.Tasks.TaskFactory factory,
                                                                                 Action action,
                                                                                 CancellationToken cancellationToken,
                                                                                 System.Threading.Tasks.TaskCreationOptions options,
                                                                                 System.Threading.Tasks.TaskScheduler scheduler)
        {
            var transaction = Transaction.Current;
            var originalAction = action;
            action = () =>
            {
                using (new DependentTransactionScope(transaction, true))
                {
                    originalAction();
                }
            };

            return factory.StartNew(action, cancellationToken, options, scheduler);
        }

        public static System.Threading.Tasks.Task StartNewWithCurrentTransaction(this System.Threading.Tasks.TaskFactory factory,
                                                                                 Action action)
        {
            var transaction = Transaction.Current;
            var originalAction = action;
            action = () =>
            {
                using (new DependentTransactionScope(transaction, true))
                {
                    originalAction();
                }
            };

            return factory.StartNew(action);
        }
    }

    /// <summary>
    ///   Identifies a type that wants to support members with the SynchronizedAttribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class SupportsSynchronizationAttribute : ConcernAttribute
    {
        public SupportsSynchronizationAttribute()
            : base(typeof(SynchronizationBehavior))
        {
        }
    }

    /// <summary>
    ///   Denotes that a member's invocation should be synchronized with a lock.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property, Inherited = false)]
    public class SynchronizedAttribute : Attribute
    {
    }

    /// <summary>
    ///   The implementation of automatic synchronization behavior for a member annotated with SynchronizedAttribute.
    /// </summary>
    internal class SynchronizationBehavior : AttributedMemberInterceptor
    {
        public override IEnumerable<Type> AttributeTypes
        {
            get { yield return typeof(SynchronizedAttribute); }
        }

        [DebuggerNonUserCode]
        public override void Intercept(IInvocation invocation)
        {
            lock (invocation.Method)
            {
                invocation.Proceed();
            }
        }
    }

    public interface ISynchronizedDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        /// <summary>
        ///   Merge is similar to the SQL merge or upsert statement.
        /// </summary>
        /// <param name = "key">Key to lookup</param>
        /// <param name = "newValue">New Value</param>
        void MergeSafe(TKey key, TValue newValue);


        /// <summary>
        ///   This is a blind remove. Prevents the need to check for existence first.
        /// </summary>
        /// <param name = "key">Key to Remove</param>
        void RemoveSafe(TKey key);
    }

    internal class SynchronizedDictionary<TKey, TValue> : ISynchronizedDictionary<TKey, TValue>
    {
        //This is the internal dictionary that we are wrapping
        private readonly IDictionary<TKey, TValue> dictionary;

        private readonly ReaderWriterLockSlim dictionaryLock = Locks.GetLockInstance(LockRecursionPolicy.NoRecursion); //setup the lock;

        public SynchronizedDictionary(IDictionary<TKey, TValue> source = null)
        {
            dictionary = source ?? new Dictionary<TKey, TValue>();
        }

        #region ISynchronizedDictionary<TKey,TValue> Members

        /// <summary>
        /// Retrieves value. If value is not yet stored -> gets and then stores it.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="getValue"></param>
        /// <returns></returns>
        public TValue GetOrAddValue(TKey key, Func<TKey, TValue> getValue)
        {
            TValue value;
            if (!TryGetValue(key, out value))
            {
                value = getValue(key);
                MergeSafe(key, value);
            }
            return value;
        }

        /// <summary>
        ///   This is a blind remove. Prevents the need to check for existence first.
        /// </summary>
        /// <param name = "key">Key to remove</param>
        public void RemoveSafe(TKey key)
        {
            using (new UpgradeableReadLock(dictionaryLock))
            {
                if (dictionary.ContainsKey(key))
                {
                    using (new WriteLock(dictionaryLock))
                    {
                        dictionary.Remove(key);
                    }
                }
            }
        }

        /// <summary>
        ///   Merge does a blind remove, and then add.  Basically a blind Upsert.
        /// </summary>
        /// <param name = "key">Key to lookup</param>
        /// <param name = "newValue">New Value</param>
        public void MergeSafe(TKey key, TValue newValue)
        {
            using (new WriteLock(dictionaryLock)) // take a writelock immediately since we will always be writing
            {
                if (dictionary.ContainsKey(key))
                {
                    dictionary.Remove(key);
                }

                dictionary.Add(key, newValue);
            }
        }

        public IList<KeyValuePair<TKey, TValue>> RemoveAll(Func<KeyValuePair<TKey, TValue>, bool> shouldRemoveCheck)
        {
            using (new WriteLock(dictionaryLock))
            {
                var removedEntries = dictionary.Where(shouldRemoveCheck).ToList();
                removedEntries.ForEach(e => dictionary.Remove(e.Key));
                return removedEntries;
            }
        }

        public virtual bool Remove(TKey key)
        {
            using (new WriteLock(dictionaryLock))
            {
                return dictionary.Remove(key);
            }
        }


        public virtual bool ContainsKey(TKey key)
        {
            using (new ReadOnlyLock(dictionaryLock))
            {
                return dictionary.ContainsKey(key);
            }
        }


        public virtual bool TryGetValue(TKey key, out TValue value)
        {
            using (new ReadOnlyLock(dictionaryLock))
            {
                return dictionary.TryGetValue(key, out value);
            }
        }


        public virtual TValue this[TKey key]
        {
            get
            {
                using (new ReadOnlyLock(dictionaryLock))
                {
                    return dictionary[key];
                }
            }
            set
            {
                using (new WriteLock(dictionaryLock))
                {
                    dictionary[key] = value;
                }
            }
        }


        public virtual ICollection<TKey> Keys
        {
            get
            {
                using (new ReadOnlyLock(dictionaryLock))
                {
                    return new List<TKey>(dictionary.Keys);
                }
            }
        }


        public virtual ICollection<TValue> Values
        {
            get
            {
                using (new ReadOnlyLock(dictionaryLock))
                {
                    return new List<TValue>(dictionary.Values);
                }
            }
        }


        public virtual void Clear()
        {
            using (new WriteLock(dictionaryLock))
            {
                dictionary.Clear();
            }
        }


        public virtual int Count
        {
            get
            {
                using (new ReadOnlyLock(dictionaryLock))
                {
                    return dictionary.Count;
                }
            }
        }


        public virtual bool Contains(KeyValuePair<TKey, TValue> item)
        {
            using (new ReadOnlyLock(dictionaryLock))
            {
                return dictionary.Contains(item);
            }
        }


        public virtual void Add(KeyValuePair<TKey, TValue> item)
        {
            using (new WriteLock(dictionaryLock))
            {
                dictionary.Add(item);
            }
        }


        public virtual void Add(TKey key, TValue value)
        {
            using (new WriteLock(dictionaryLock))
            {
                dictionary.Add(key, value);
            }
        }


        public virtual bool Remove(KeyValuePair<TKey, TValue> item)
        {
            using (new WriteLock(dictionaryLock))
            {
                return dictionary.Remove(item);
            }
        }


        public virtual void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            using (new ReadOnlyLock(dictionaryLock))
            {
                dictionary.CopyTo(array, arrayIndex);
            }
        }


        public virtual bool IsReadOnly
        {
            get
            {
                using (new ReadOnlyLock(dictionaryLock))
                {
                    return dictionary.IsReadOnly;
                }
            }
        }


        public virtual IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            throw new NotSupportedException("Cannot enumerate a threadsafe dictionary.  Instead, enumerate the keys or values collection");
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotSupportedException("Cannot enumerate a threadsafe dictionary.  Instead, enumerate the keys or values collection");
        }

        #endregion
    }

    public static class Locks
    {
        public static void GetUpgradeableReadLock(ReaderWriterLockSlim locks)
        {
            bool lockAcquired = false;
            while (!lockAcquired)
                lockAcquired = locks.TryEnterUpgradeableReadLock(1);
        }


        public static void GetReadOnlyLock(ReaderWriterLockSlim locks)
        {
            bool lockAcquired = false;
            while (!lockAcquired)
                lockAcquired = locks.TryEnterReadLock(1);
        }


        public static void GetWriteLock(ReaderWriterLockSlim locks)
        {
            bool lockAcquired = false;
            while (!lockAcquired)
                lockAcquired = locks.TryEnterWriteLock(1);
        }


        public static void ReleaseReadOnlyLock(ReaderWriterLockSlim locks)
        {
            if (locks.IsReadLockHeld)
                locks.ExitReadLock();
        }

        public static void ReleaseUpgradeableReadLock(ReaderWriterLockSlim locks)
        {
            if (locks.IsUpgradeableReadLockHeld)
                locks.ExitUpgradeableReadLock();
        }

        public static void ReleaseWriteLock(ReaderWriterLockSlim locks)
        {
            if (locks.IsWriteLockHeld)
                locks.ExitWriteLock();
        }

        public static void ReleaseLock(ReaderWriterLockSlim locks)
        {
            ReleaseWriteLock(locks);
            ReleaseUpgradeableReadLock(locks);
            ReleaseReadOnlyLock(locks);
        }


        public static ReaderWriterLockSlim GetLockInstance()
        {
            return GetLockInstance(LockRecursionPolicy.SupportsRecursion);
        }


        public static ReaderWriterLockSlim GetLockInstance(LockRecursionPolicy recursionPolicy)
        {
            return new ReaderWriterLockSlim(recursionPolicy);
        }
    }

    public abstract class BaseLock : IDisposable
    {
        protected ReaderWriterLockSlim ReaderWriterLockSlim;

        protected BaseLock(ReaderWriterLockSlim readerWriterLockSlim)
        {
            ReaderWriterLockSlim = readerWriterLockSlim;
        }

        #region IDisposable Members

        public abstract void Dispose();

        #endregion
    }

    public class ReadOnlyLock : BaseLock
    {
        public ReadOnlyLock(ReaderWriterLockSlim readerWriterLockSlim)
            : base(readerWriterLockSlim)
        {
            Locks.GetReadOnlyLock(ReaderWriterLockSlim);
        }

        public override void Dispose()
        {
            Locks.ReleaseReadOnlyLock(ReaderWriterLockSlim);
        }
    }

    public class UpgradeableReadLock : BaseLock
    {
        public UpgradeableReadLock(ReaderWriterLockSlim readerWriterLockSlim)
            : base(readerWriterLockSlim)
        {
            Locks.GetUpgradeableReadLock(ReaderWriterLockSlim);
        }

        public override void Dispose()
        {
            Locks.ReleaseUpgradeableReadLock(ReaderWriterLockSlim);
        }
    }

    public class WriteLock : BaseLock
    {
        public WriteLock(ReaderWriterLockSlim readerWriterLockSlim)
            : base(readerWriterLockSlim)
        {
            Locks.GetWriteLock(ReaderWriterLockSlim);
        }


        public override void Dispose()
        {
            Locks.ReleaseWriteLock(ReaderWriterLockSlim);
        }
    }

    public class MultiLock : IDisposable
    {
        private readonly BaseLock[] locks;

        public MultiLock(params BaseLock[] locks)
        {
            this.locks = locks;
        }

        public void Dispose()
        {
            locks.ForEach(l => l.Dispose());
        }
    }
}

namespace Soaf.Security
{
    /// <summary>
    /// Helper/extension methods for security.
    /// </summary>
    public static class Security
    {
        /// <summary>
        /// Gets the user id of the user stored in the specified principal context, if the user implements IHasId.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <returns></returns>
        public static object GetUserId(this IPrincipalContext principalContext)
        {
            if (principalContext != null && principalContext.Principal != null)
            {
                var hasId = principalContext.Principal as IHasId ?? principalContext.Principal.Identity as IHasId;
                if (hasId != null && hasId.Id != null)
                {
                    return hasId.Id;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the access type providers for the specified object.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<IAccessTypeProvider> GetAccessTypeProviders(object item)
        {
            if (item is ICustomAttributeProvider)
            {
                return item.CastTo<ICustomAttributeProvider>().GetAttributes().OfType<IAccessTypeProvider>();
            }
            return new IAccessTypeProvider[0];
        }

        /// <summary>
        /// Determines whether the principal has read only access based on the access type providers.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <param name="providers">The providers.</param>
        /// <param name="defaultAccessType">Default type of the access.</param>
        /// <returns>
        ///   <c>true</c> if [has access type] [the specified principal]; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasReadOnlyAccess(this IPrincipal principal, IEnumerable<IAccessTypeProvider> providers, AccessType defaultAccessType)
        {
            var providersInternal = providers.ToArray();
            var result = principal.HasAccessType(providersInternal, AccessType.Read, defaultAccessType) && !(principal.HasAccessType(providersInternal, AccessType.Create, defaultAccessType) || principal.HasAccessType(providersInternal, AccessType.Update, defaultAccessType) || principal.HasAccessType(providersInternal, AccessType.Delete, defaultAccessType));
            return result;
        }

        /// <summary>
        /// Determines whether the principal has the specified access type based on the specified access type providers.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <param name="providers">The providers.</param>
        /// <param name="accessType">Type of the access.</param>
        /// <param name="defaultAccessType">Default type of the access.</param>
        /// <returns>
        ///   <c>true</c> if [has access type] [the specified principal]; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasAccessType(this IPrincipal principal, IEnumerable<IAccessTypeProvider> providers, AccessType accessType, AccessType defaultAccessType)
        {
            var providersInternal = providers.ToArray();
            // no authorization specified...or has specified access and not explicitly denied access
            var result = (providersInternal.Count() == 0 && defaultAccessType.HasFlag(accessType)) ||
                   (providersInternal.Where(p => p.HasAccessType(new AuthorizationContext(principal), accessType)).Count() > 0 &&
                    providersInternal.Where(p => p.IsDeniedAccessType(new AuthorizationContext(principal), accessType)).Count() == 0);

            return result;
        }
    }

    /// <summary>
    /// Specifies the 
    /// </summary>
    [Flags]
    public enum AccessTypeAction
    {
        Permit = 1, Deny = 2
    }

    /// <summary>
    /// Represents information about the level of access a entity may have for another entity.
    /// </summary>
    [Flags]
    public enum AccessType
    {
        /// <summary>
        /// 
        /// </summary>
        Create = 1,
        /// <summary>
        /// 
        /// </summary>
        Read = 2,
        /// <summary>
        /// 
        /// </summary>
        Update = 4,
        /// <summary>
        /// 
        /// </summary>
        Delete = 8,
        /// <summary>
        /// 
        /// </summary>
        All = Create | Read | Update | Delete
    }

    /// <summary>
    ///   Information about the context under which authorization is being performed.
    /// </summary>
    public class AuthorizationContext
    {
        public IPrincipal Principal { get; set; }

        public AuthorizationContext(IPrincipal principal)
        {
            Principal = principal;
        }
    }

    /// <summary>
    /// Secures functionality for a given context.
    /// </summary>
    public interface IAuthorizationProvider
    {
        bool IsAuthorized(AuthorizationContext authorizationContext);

        void Authorize(AuthorizationContext authorizationContext);
    }

    /// <summary>
    /// Secures functionality for a specific context for a specified access type.
    /// </summary>
    public interface IAccessTypeProvider
    {
        bool HasAccessType(AuthorizationContext authorizationContext, AccessType accessType);

        bool IsDeniedAccessType(AuthorizationContext authorizationContext, AccessType accessType);

        void Authorize(AuthorizationContext authorizationContext, AccessType accessType);
    }

    /// <summary>
    ///   Denotes that a type supports Authorization via AuthorizeAttributes.
    /// </summary>
    public class SupportsAuthorizationAttribute : ConcernAttribute
    {
        public SupportsAuthorizationAttribute()
            : base(typeof(AuthorizationBehavior), 1000)
        {
        }
    }

    /// <summary>
    /// A configurable access type provider based on role names.
    /// </summary>
    public class RoleAccessTypeProvider : IAccessTypeProvider
    {
        private readonly string createRoleName;
        private readonly string readRoleName;
        private readonly string updateRoleName;
        private readonly string deleteRoleName;

        public RoleAccessTypeProvider(string createRoleName, string readRoleName, string updateRoleName, string deleteRoleName)
        {
            this.createRoleName = createRoleName;
            this.readRoleName = readRoleName;
            this.updateRoleName = updateRoleName;
            this.deleteRoleName = deleteRoleName;
        }

        public bool HasAccessType(AuthorizationContext authorizationContext, AccessType accessType)
        {
            if (AccessType.Create.HasFlag(accessType))
            {
                return authorizationContext.Principal.IsInRole(createRoleName);
            }
            if (AccessType.Read.HasFlag(accessType))
            {
                return authorizationContext.Principal.IsInRole(readRoleName);
            }
            if (AccessType.Update.HasFlag(accessType))
            {
                return authorizationContext.Principal.IsInRole(updateRoleName);
            }
            if (AccessType.Delete.HasFlag(accessType))
            {
                return authorizationContext.Principal.IsInRole(deleteRoleName);
            }
            return false;
        }

        public bool IsDeniedAccessType(AuthorizationContext authorizationContext, AccessType accessType)
        {
            return false;
        }

        public void Authorize(AuthorizationContext authorizationContext, AccessType accessType)
        {
            if (!HasAccessType(authorizationContext, accessType)) throw new UnauthorizedAccessException();
        }
    }

    /// <summary>
    /// Authorizes principals in specified roles for a certain type of access.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = false)]
    public class RoleAcessTypeAttribute : Attribute, IAccessTypeProvider
    {
        public bool HasAccessType(AuthorizationContext authorizationContext, AccessType accessType)
        {
            return AccessTypeAction == AccessTypeAction.Permit && Roles.Any(r => PrincipalContext.Current.Principal.IsInRole(r)) && AccessType.HasFlag(accessType);
        }

        public bool IsDeniedAccessType(AuthorizationContext authorizationContext, AccessType accessType)
        {
            return AccessTypeAction == AccessTypeAction.Deny && Roles.Any(r => PrincipalContext.Current.Principal.IsInRole(r)) && AccessType.HasFlag(accessType);
        }

        public void Authorize(AuthorizationContext authorizationContext, AccessType accessType)
        {
            if (!HasAccessType(authorizationContext, accessType))
            {
                throw new UnauthorizedAccessException(string.Format("Current principal is not authorized for {0} access.",
                    string.Join(", ", Enums.GetValues(typeof(AccessType)).Cast<AccessType>().Where(a => AccessType.HasFlag(a)))));
            }
        }

        public AccessType AccessType { get; set; }

        public AccessTypeAction AccessTypeAction { get; set; }

        public string[] Roles { get; set; }
    }

    /// <summary>
    ///   A base attribute class for implementing authorization.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property)]
    public abstract class AuthorizeAttribute : Attribute, IAuthorizationProvider
    {
        public UnauthorizedBehavior UnauthorizedBehavior { get; set; }
        public abstract bool IsAuthorized(AuthorizationContext authorizationContext);
        public abstract void Authorize(AuthorizationContext authorizationContext);
    }

    /// <summary>
    ///   Authorizes based on the principal's roles or identity name.
    /// </summary>
    public class AuthorizeRoleOrNameAttribute : AuthorizeAttribute
    {
        public string[] Roles { get; set; }
        public string[] IdentityNames { get; set; }
        public override bool IsAuthorized(AuthorizationContext authorizationContext)
        {
            return authorizationContext.Principal != null && authorizationContext.Principal.Identity.IsAuthenticated &&
                   ((Roles ?? new string[0]).Any(r => authorizationContext.Principal.IsInRole(r)) ||
                    ((IdentityNames ?? new string[0]).Any(i => authorizationContext.Principal.Identity.Name.Equals(i))));
        }

        public override void Authorize(AuthorizationContext authorizationContext)
        {
            if (!IsAuthorized(authorizationContext)) throw new UnauthorizedAccessException();
        }
    }

    /// <summary>
    ///   Describes how to handle unauthorized access.
    /// </summary>
    public enum UnauthorizedBehavior
    {
        Throw,
        SkipInvocation
    }

    /// <summary>
    ///   Implements the behavior for providing authorization via interception.
    /// </summary>
    internal class AuthorizationBehavior : AttributedMemberInterceptor, IInstanceConcern
    {
        private readonly IDictionary<MemberInfo, AuthorizeAttribute[]> authorizeAttributes = new Dictionary<MemberInfo, AuthorizeAttribute[]>().Synchronized();
        private readonly IPrincipalContext principalContext;

        /// <summary>
        /// Required by IInterceptedMethodsFilter. Do not use
        /// </summary>
        public AuthorizationBehavior()
        {
        }

        public AuthorizationBehavior(IPrincipalContext principalContext)
        {
            this.principalContext = principalContext;
        }

        public override IEnumerable<Type> AttributeTypes
        {
            get { return Enumerables.CreateArray(typeof(AuthorizeAttribute)); }
        }

        public override void Intercept(IInvocation invocation)
        {
            if (Authorize(invocation.Method))
            {
                invocation.Proceed();
            }
        }

        /// <summary>
        ///   Performs authorization for the specified member.
        /// </summary>
        /// <param name = "member">The member.</param>
        /// <returns></returns>
        private bool Authorize(MemberInfo member)
        {
            AuthorizeAttribute[] attributes;
            if (!authorizeAttributes.TryGetValue(member, out attributes))
            {
                authorizeAttributes[member] = attributes = member.GetAttributes<AuthorizeAttribute>().ToArray();
            }
            var context = new AuthorizationContext(principalContext.Principal);

            bool proceed = true;

            foreach (AuthorizeAttribute attribute in attributes)
            {
                var exceptions = new List<Exception>();
                try
                {
                    attribute.Authorize(context);
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
                if (exceptions.Count > 0)
                {
                    proceed = false;
                    if (attribute.UnauthorizedBehavior == UnauthorizedBehavior.Throw)
                    {
                        if (exceptions.Count == 1) throw exceptions.First();
                        throw new UnauthorizedAccessException("One or more unauthorized access exceptions occurred.", new AggregateException(exceptions.ToArray()));
                    }
                }
            }

            return proceed;
        }

        public void ApplyConcern(object value)
        {
            Authorize(value.GetType());
        }
    }

    /// <summary>
    /// Defines a source for information about an IPrincipal.
    /// </summary>
    public interface IPrincipalContext
    {
        /// <summary> 
        ///   Gets or sets the principal.
        /// </summary>
        /// <value>The principal.</value>
        IPrincipal Principal { get; set; }

        /// <summary>
        /// Gets or sets the scope.
        /// </summary>
        /// <value>
        /// The scope.
        /// </value>
        PrincipalScope Scope { get; set; }
    }

    /// <summary>
    /// Defines the scope of the current principal.
    /// </summary>
    public enum PrincipalScope
    {
        Thread,
        AppDomain
    }

    /// <summary>
    /// Defines a type that provides authentication. Same contract as System.Web.ApplicationServices.IAuthenticationService.
    /// </summary>
    public interface IAuthenticationService
    {
        bool IsLoggedIn();
        bool Login(string username, string password, string customCredential, bool isPersistent);
        void Logout();
        bool ValidateUser(string username, string password, string customCredential);

        /// <summary>
        /// Accepts an assembly full name and tries to find a match in the current app domain
        /// </summary>
        /// <param name="assemblyFullName"></param>
        /// <returns></returns>
        bool ValidateAssemblyNameAndVersion(string assemblyFullName);
    }

    /// <summary>
    ///   Default IPrincipalContext that tries to use the Thread.CurrentPrincipal if available, otherwise uses a local instance.
    /// </summary>
    [Singleton]
    public class PrincipalContext : IPrincipalContext
    {
        private IPrincipal principal;

        public static IPrincipalContext Current
        {
            get { return ServiceProvider.Current.GetService<IPrincipalContext>(); }
        }

        #region IPrincipalContext Members

        public virtual IPrincipal Principal
        {
            get { return Scope == PrincipalScope.Thread ? Thread.CurrentPrincipal : principal; }
            set
            {
                if (Scope == PrincipalScope.Thread)
                {
                    Thread.CurrentPrincipal = value;
                }
                else
                {
                    principal = value;
                }
            }
        }

        public PrincipalScope Scope { get; set; }

        #endregion
    }

    /// <summary>
    ///   A generic IPrincipal, because SL is missing one.
    /// </summary>
    public class GenericPrincipal : IPrincipal
    {
        public GenericPrincipal(IIdentity identity, params string[] roles)
        {
            Identity = identity;
            Roles = roles;
        }

        public string[] Roles { get; private set; }

        #region IPrincipal Members

        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }

        public IIdentity Identity { get; private set; }

        #endregion
    }

    /// <summary>
    ///   A generic IIdentity.
    /// </summary>
    public class GenericIdentity : IIdentity, IHasId
    {
        public GenericIdentity(string name, bool isAuthenticated = false, string authenticationType = null, object id = null)
        {
            Name = name;
            IsAuthenticated = isAuthenticated;
            AuthenticationType = authenticationType;
            Id = id;
        }

        #region IHasId Members

        public object Id { get; private set; }

        #endregion

        #region IIdentity Members

        public string Name { get; private set; }

        public string AuthenticationType { get; private set; }

        public bool IsAuthenticated { get; private set; }

        #endregion
    }

    /// <summary>
    /// A principal for a UserIdentity.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class UserPrincipal : IPrincipal
    {
        [DataMember]
        public UserIdentity Identity { get; private set; }

        public UserPrincipal(UserIdentity identity)
        {
            Identity = identity;
        }

        #region IPrincipal Members

        public bool IsInRole(string role)
        {
            return Identity.User.Roles.Any(r => r.Name == role);
        }

        IIdentity IPrincipal.Identity
        {
            get { return Identity; }
        }

        #endregion
    }

    /// <summary>
    /// An identity for the User model.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class UserIdentity : IIdentity, IHasId, IHasTimeZoneId
    {
        [DataMember]
        public User User { get; private set; }

        public UserIdentity(User user, bool isAuthenticated, string authenticationType)
        {
            User = user;
            AuthenticationType = authenticationType;
            IsAuthenticated = isAuthenticated;
        }

        #region IIdentity Members

        public string Name
        {
            get { return User.UserName; }
        }

        public string AuthenticationType { get; private set; }

        public bool IsAuthenticated { get; private set; }

        #endregion

        public object Id
        {
            get { return User == null ? null : User.Id; }
        }

        public string TimeZoneId
        {
            get { return User == null ? null : User.TimeZoneId; }
        }
    }

    /// <summary>
    /// A user model.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class User
    {
        #region Primitive Properties

        [DataMember]
        public virtual string Id { get; set; }

        [DataMember]
        public virtual string UserName { get; set; }

        [DataMember]
        public virtual string Email { get; set; }

        [DataMember]
        public virtual string Password { get; set; }

        [DataMember]
        public virtual string PasswordQuestion { get; set; }

        [DataMember]
        public virtual string PasswordAnswer { get; set; }

        [DataMember]
        public virtual bool IsApproved { get; set; }

        [DataMember]
        public virtual DateTime LastActivityDate { get; set; }

        [DataMember]
        public virtual DateTime LastLoginDate { get; set; }

        [DataMember]
        public virtual DateTime LastPasswordChangedDate { get; set; }

        [DataMember]
        public virtual DateTime CreationDate { get; set; }

        [DataMember]
        public virtual bool IsOnline { get; set; }

        [DataMember]
        public virtual bool IsLockedOut { get; set; }

        [DataMember]
        public virtual DateTime LastLockedOutDate { get; set; }

        [DataMember]
        public virtual int FailedPasswordAttemptCount { get; set; }

        [DataMember]
        public virtual DateTime FailedPasswordAttemptWindowStart { get; set; }

        [DataMember]
        public virtual int FailedPasswordAnswerAttemptCount { get; set; }

        [DataMember]
        public virtual DateTime FailedPasswordAnswerAttemptWindowStart { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        #endregion

        #region Navigation Properties

        private ICollection<Role> roles;
        private IEnumerable<Role> rolesSource;

        [DataMember]
        public virtual ICollection<Role> Roles
        {
            get
            {
                if (roles == null)
                {
                    if (rolesSource != null)
                    {
                        var newCollection = new FixupCollection<Role>(rolesSource.ToList());
                        newCollection.ItemSet += FixupRolesItemSet;
                        newCollection.ItemRemoved += FixupRolesItemRemoved;
                        roles = newCollection;
                    }
                    else
                    {
                        var newCollection = new FixupCollection<Role>();
                        newCollection.ItemSet += FixupRolesItemSet;
                        newCollection.ItemRemoved += FixupRolesItemRemoved;
                        roles = newCollection;
                    }
                }
                return roles;
            }
            set
            {
                if (!ReferenceEquals(roles, value))
                {
                    var previousValue = roles as FixupCollection<Role>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupRolesItemSet;
                        previousValue.ItemRemoved -= FixupRolesItemRemoved;
                    }
                    roles = value;
                    var newValue = value as FixupCollection<Role>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupRolesItemSet;
                        newValue.ItemRemoved += FixupRolesItemRemoved;
                    }
                }
            }
        }

        /// <summary>
        /// Allows the Roles property to be populated as part of a query.
        /// If the roles property is set directly, this property is not used.
        /// </summary>
        public virtual IEnumerable<Role> RolesSource
        {
            get
            {
                return Roles;
            }
            set
            {
                if (value == null)
                {
                    Roles = null;
                }
                else if (value is ICollection<Role>)
                {
                    Roles = (ICollection<Role>)value;
                }
                else
                {
                    rolesSource = value;
                }
            }
        }

        #endregion

        #region Association Fixup

        private void FixupRolesItemSet(object sender, EventArgs<Role> e)
        {
            Role item = e.Value;

            if (!item.Users.Contains(this))
            {
                item.Users.Add(this);
            }
        }

        private void FixupRolesItemRemoved(object sender, EventArgs<Role> e)
        {
            Role item = e.Value;

            if (item.Users.Contains(this))
            {
                item.Users.Remove(this);
            }
        }

        #endregion

        [DataMember]
        public virtual object Tag { get; set; }

        [DataMember]
        public virtual string TimeZoneId { get; set; }
    }

    /// <summary>
    /// A role model.
    /// </summary>
    public class Role
    {
        #region Primitive Properties

        public virtual Guid Id { get; set; }

        public virtual string Name { get; set; }

        #endregion

        #region Navigation Properties

        private ICollection<User> users;

        public virtual ICollection<User> Users
        {
            get
            {
                if (users == null)
                {
                    var newCollection = new FixupCollection<User>();
                    newCollection.ItemSet += FixupUsersItemSet;
                    newCollection.ItemRemoved += FixupUsersItemRemoved;
                    users = newCollection;
                }
                return users;
            }
            set
            {
                if (!ReferenceEquals(users, value))
                {
                    var previousValue = users as FixupCollection<User>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupUsersItemSet;
                        previousValue.ItemRemoved -= FixupUsersItemRemoved;
                    }
                    users = value;
                    var newValue = value as FixupCollection<User>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupUsersItemSet;
                        newValue.ItemRemoved += FixupUsersItemRemoved;
                    }
                }
            }
        }

        #endregion

        #region Association Fixup

        private void FixupUsersItemSet(object sender, EventArgs<User> e)
        {
            User item = e.Value;

            if (!item.Roles.Contains(this))
            {
                item.Roles.Add(this);
            }
        }

        private void FixupUsersItemRemoved(object sender, EventArgs<User> e)
        {
            User item = e.Value;

            if (item.Roles.Contains(this))
            {
                item.Roles.Remove(this);
            }
        }

        #endregion

        public virtual object Tag { get; set; }
    }

    /// <summary>
    /// A repository of users and roles.
    /// </summary>
    [Repository(Name = "UserRepository")]
    public interface IUserRepository
    {
        #region IQueryable Properties

        IQueryable<User> Users { get; }

        IQueryable<Role> Roles { get; }

        #endregion

        #region Save

        void Save(User value);

        void Save(Role value);

        #endregion

        #region Delete

        void Delete(User value);

        void Delete(Role value);

        #endregion

        #region Create Objects

        User CreateUser();

        Role CreateRole();

        #endregion

    }

}

namespace Soaf
{
    /// <summary>
    /// Helper for creating lazy wrappers.
    /// </summary>
    public static class Lazy
    {
        /// <summary>
        /// Creates a lazy for the specified factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="factory">The factory.</param>
        /// <param name="lazyThreadSafetyMode">The lazy thread safety mode.</param>
        /// <returns></returns>
        public static Lazy<T> For<T>(Func<T> factory, LazyThreadSafetyMode lazyThreadSafetyMode)
        {
            return new Lazy<T>(factory, lazyThreadSafetyMode);
        }

        /// <summary>
        /// Creates a lazy for the specified factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="factory">The factory.</param>
        /// <param name="isThreadSafe">if set to <c>true</c> [is thread safe].</param>
        /// <returns></returns>
        public static Lazy<T> For<T>(Func<T> factory, bool isThreadSafe)
        {
            return new Lazy<T>(factory, isThreadSafe);
        }

        /// <summary>
        /// Creates a lazy for the specified factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="factory">The factory.</param>
        /// <returns></returns>
        public static Lazy<T> For<T>(Func<T> factory)
        {
            return new Lazy<T>(factory);
        }

    }

    /// <summary>
    ///   A bootstrapper for the current domain that sets up resolution, dependency injection, and services.
    /// </summary>
    public class Bootstrapper
    {
        static Bootstrapper()
        {
            AssemblyResolutionProvider.Initialize();
        }

        [DebuggerNonUserCode]
        private static string GetConfigurationManagerConfig()
        {
            if (WellKnownTypes.System_Web_Configuration_ConfigurationManager != null)
            {
                try
                {
                    dynamic configuration = Objects.StaticMembersDynamicWrapper.For(WellKnownTypes.System_Web_Configuration_ConfigurationManager).OpenWebConfiguration("~");
                    if (configuration != null && configuration.HasFile && File.Exists((string)configuration.FilePath))
                    {
                        return File.ReadAllText((string)configuration.FilePath);
                    }
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {
                }
            }

            try
            {
                var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                if (configuration.HasFile && File.Exists(configuration.FilePath))
                {
                    return File.ReadAllText(configuration.FilePath);
                }
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }

            return null;
        }

        private static string GetConfigFromAssemblyResources(Assembly assembly)
        {
            return assembly == null ? null : GetConfigFromAssemblyResources(".App.Config", assembly) ?? GetConfigFromAssemblyResources(".Web.Config", assembly);
        }

        private static string GetConfigFromAssemblyResources(string resourceName, Assembly assembly)
        {
            if (assembly == null) return null;

            string name = assembly.GetManifestResourceNames().FirstOrDefault(n => n.EndsWith(resourceName, StringComparison.OrdinalIgnoreCase));
            if (name != null)
            {
                return assembly.GetManifestResourceStream(name).GetString();
            }
            return null;
        }

        /// <summary>
        /// Runs the bootstrapper.
        /// </summary>
        /// <typeparam name="TEntryPoint">The type of the entry point.</typeparam>
        /// <param name="assemblyRepository">The assembly repository.</param>
        /// <param name="xmlConfiguration">The XML configuration.</param>
        /// <param name="serviceProviderType">Type of the service provider.</param>
        /// <param name="singletons">The singletons.</param>
        /// <returns></returns>
        public static TEntryPoint Run<TEntryPoint>(IAssemblyRepository assemblyRepository, IXmlConfiguration xmlConfiguration = null, Type serviceProviderType = null, object[] singletons = null)
        {
            if (xmlConfiguration == null)
            {
                xmlConfiguration = new StringXmlConfiguration(GetConfigurationManagerConfig() ?? GetConfigFromAssemblyResources(Reflector.TryGetEntryAssembly() ?? Assembly.GetCallingAssembly()));
            }

            if (singletons == null) singletons = new[] { xmlConfiguration };
            else singletons = new[] { xmlConfiguration }.Concat(singletons).ToArray();

            var componentRegistry = new AttributedTypeComponentRegistry(assemblyRepository, null, singletons);

            return Run<TEntryPoint>(componentRegistry, serviceProviderType);
        }

        /// <summary>
        /// Runs the bootstrapper.
        /// </summary>
        /// <typeparam name="TEntryPoint">The type of the entry point.</typeparam>
        /// <param name="componentRegistry">The component registry.</param>
        /// <param name="serviceProviderType">Type of the service provider.</param>
        /// <returns></returns>
        public static TEntryPoint Run<TEntryPoint>(IComponentRegistry componentRegistry, Type serviceProviderType = null)
        {
            using (new TimedScope(s => Debug.WriteLine("Ran Soaf Bootstraper in {0}.".FormatWith(s))))
            {
                if (serviceProviderType == null) serviceProviderType = WellKnownTypes.Soaf_Castle_WindsorServiceProvider;

                var serviceProvider = (IServiceProvider)Activator.CreateInstance(serviceProviderType, componentRegistry);
                ServiceProvider.Initialize(() => serviceProvider);
                serviceProvider.As<IInitializable>().IfNotNull(i => i.Initialize());

                return serviceProvider.GetService<TEntryPoint>();
            }
        }
    }

    /// <summary>
    /// A generic service provider factory that supports creation and disposing of instances.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Factory]
    // ReSharper disable TypeParameterCanBeVariant
    public interface IServiceProvider<T>
    // ReSharper restore TypeParameterCanBeVariant
    {
        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <returns></returns>
        T GetService();
    }

    /// <summary>
    /// An IServiceProvider that uses the Resolver to resolve services.
    /// </summary>
    public static class ServiceProvider
    {
        private static Func<IServiceProvider> serviceProviderProvider;

        public static bool IsInitialized
        {
            get { return serviceProviderProvider != null; }
        }

        /// <summary>
        /// Gets the static instance. Use ONLY for static methods that need resolution functionality.
        /// </summary>        
        internal static IServiceProvider Current
        {
            get
            {
                if (serviceProviderProvider == null)
                {
                    throw new InvalidOperationException("serviceProviderProvider must be set.");
                }
                return serviceProviderProvider();
            }
        }

        internal static void Initialize(Func<IServiceProvider> serviceProviderProvider)
        {
            ServiceProvider.serviceProviderProvider = serviceProviderProvider;
        }

        /// <summary>
        /// Gets the strongly typed service.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serviceProvider">The service provider.</param>
        /// <returns></returns>
        public static T GetService<T>(this IServiceProvider serviceProvider)
        {
            return serviceProvider.GetService(typeof(T)).CastTo<T>();
        }

        /// <summary>
        /// Gets the strongly typed service.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="args">The constructor arguments.</param>
        /// <returns></returns>
        public static T GetService<T>(this IServiceProvider serviceProvider, params object[] args)
        {
            var funcType = Expression.GetFuncType(args.Select(a => a == null ? typeof(object) : a.GetType()).Select(t => t.Assembly.IsDynamic && t.BaseType != null ? t.BaseType : t)
                .Concat(new[] { typeof(T) })
                .ToArray());

            var func = (Delegate)serviceProvider.GetService(funcType);

            return (T)func.DynamicInvoke(args);
        }

        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="args">The constructor arguments.</param>
        /// <returns></returns>
        public static object GetService(this IServiceProvider serviceProvider, Type serviceType, params object[] args)
        {
            var funcType = Expression.GetFuncType(args.Select(a => a == null ? typeof(object) : a.GetType()).Select(t => t.Assembly.IsDynamic && t.BaseType != null ? t.BaseType : t)
                .Concat(new[] { serviceType })
                .ToArray());

            var func = (Delegate)serviceProvider.GetService(funcType);

            return func.DynamicInvoke(args);
        }

    }

    /// <summary>
    ///   A bootstrapper that assists in assembly resolution
    /// </summary>
    internal class AssemblyResolutionProvider
    {
        [ThreadStatic]
        private static bool isResolvingAssembly;

        [ThreadStatic]
        private static bool isResolvingType;

        private AssemblyResolutionProvider()
        {
        }

        static AssemblyResolutionProvider()
        {
            try
            {
                WellKnownAssemblies.Initialize();

                AppDomain.CurrentDomain.AssemblyResolve += OnCurrentDomainAssemblyResolve;

                AppDomain.CurrentDomain.TypeResolve += OnTypeResolve;
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
        }

        private static Assembly OnTypeResolve(object sender, ResolveEventArgs args)
        {
            if (isResolvingType) return null;

            isResolvingType = true;
            try
            {
                // help resolve types stored in CallContext that are accessed by assemblies in inaccessible load contexts (eg. AppDomain.GetEvidence)
                var result = args.Name.ToType().IfNotNull(t => t.Assembly);
                return result;
            }
            finally
            {
                isResolvingType = false;
            }
        }

        public static void Initialize()
        {

        }

        private static Assembly OnCurrentDomainAssemblyResolve(object sender, ResolveEventArgs args)
        {
            string shortName = args.Name.Split(',').First().Trim();
            if (new[] { ".resources", ".XmlSerializers" }.Any(shortName.EndsWith))
            {
                return null;
            }
            if (shortName == WellKnownAssemblies.Soaf.GetName().Name)
            {
                return WellKnownAssemblies.Soaf;
            }

            if (!isResolvingAssembly)
            {
                isResolvingAssembly = true;
                try
                {
                    if (args.Name.StartsWith("Soaf.") && args.Name.Split(',').Select(i => i.Trim()).First() != DynamicModuleFactory.AssemblyName.FullName.Split(',').Select(i => i.Trim()).First())
                    {
                        return Assembly.GetExecutingAssembly();
                    }

                    foreach (Assembly @assembly in AppDomain.CurrentDomain.GetAssemblies())
                    {
                        if (assembly.FullName.Equals(args.Name, StringComparison.OrdinalIgnoreCase))
                        {
                            return assembly;
                        }
                    }
                    foreach (Assembly @assembly in AppDomain.CurrentDomain.GetAssemblies())
                    {
                        if (assembly.FullName.Split(',').First().Trim().Equals(args.Name.Split(',').First().Trim(), StringComparison.OrdinalIgnoreCase))
                        {
                            return assembly;
                        }
                    }
                }
                finally
                {
                    isResolvingAssembly = false;
                }
            }

            return null;
        }

        [DebuggerNonUserCode]
        public static Assembly TryResolveAssembly(params string[] names)
        {
            foreach (string name in names)
            {
                try
                {
                    Assembly assembly = Assembly.Load(name);
                    if (assembly != null)
                    {
                        return assembly;
                    }
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {
                }
            }
            return null;
        }

        /// <summary>
        /// Loads the specified stream as an Assembly.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static Assembly Load(Stream stream)
        {
            return Assembly.Load(stream.ToArray());
        }

        /// <summary>
        /// Loads the specified bytes as an Assembly.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns></returns>
        public static Assembly Load(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                return Load(stream);
            }
        }

        /// <summary>
        /// Loads the embedded resource as an assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="resourceNames">The resource names.</param>
        /// <returns></returns>
        public static Assembly[] LoadAssemblyResources(Assembly assembly, params string[] resourceNames)
        {
            var assemblies = new List<Assembly>();
            var assemblyResourceNames = assembly.GetManifestResourceNames();
            foreach (string resourceName in resourceNames)
            {
                if (assemblyResourceNames.Contains(resourceName))
                    using (Stream stream = assembly.GetManifestResourceStream(resourceName)) assemblies.Add(Load(stream));
            }
            return assemblies.ToArray();
        }
    }

    /// <summary>
    ///   Well known assemblies resolved.
    /// </summary>
    internal static class WellKnownAssemblies
    {
        public const string SoafPublicKeyToken = "75e2634e27c46854";

        public const string SoafPublicKey = "ACQAAASAAACUAAAABgIAAAAkAABSU0ExAAQAAAEAAQDhyxrdhz3B7j50CwxLYWANSHnW1uxAWPaAN2hwMjz4kLGzUZfCq5jJ2jc+4E0aJmtdKMYH44E8R0jkrJBymJsMfNGtmI8VYMIZgskcwohmpjSWvenGaaOUc2FBZNdt+x0UTcFa74uqMS98yT3KVqjAz2zgMxjy38bwZMqyKkmAtw==";

        public static string SoafVersion = typeof(WellKnownAssemblies).Assembly.FullName.Split(',').Select(i => i.Trim()).ToArray()[1].Substring("Version=".Length);

        // ReSharper disable InconsistentNaming

        public static readonly Assembly Soaf;

        public static Assembly Soaf_Castle
        {
            get { return soaf_Castle; }
        }

        public static Assembly Soaf_EntityFramework
        {
            get { return soaf_EntityFramework; }
        }

        public static Assembly Soaf_Web
        {
            get { return soaf_Web; }
        }

        /// <summary>
        /// System.Web to allow code execution in both 4.0 Client profile and Full profile
        /// </summary>
        public static Assembly System_Web
        {
            get { return system_Web.Value; }
        }

        public static Assembly Soaf_Presentation
        {
            get { return soaf_Presentation; }
        }

        private static readonly Assembly soaf_Castle;
        private static readonly Assembly soaf_Presentation;
        private static readonly Assembly soaf_EntityFramework;
        private static readonly Assembly soaf_Web;
        private static readonly Lazy<Assembly> system_Web;

        // ReSharper restore InconsistentNaming

        static WellKnownAssemblies()
        {
            Soaf = Assembly.GetExecutingAssembly();

            soaf_Castle = AssemblyResolutionProvider.TryResolveAssembly("Soaf.Castle, Version={0}, Culture=neutral, PublicKeyToken={1}".FormatWith(SoafVersion, SoafPublicKeyToken)) ?? Soaf;

            soaf_EntityFramework = AssemblyResolutionProvider.TryResolveAssembly("Soaf.EntityFramework, Version={0}, Culture=neutral, PublicKeyToken={1}".FormatWith(SoafVersion, SoafPublicKeyToken));

            soaf_Web = AssemblyResolutionProvider.TryResolveAssembly("Soaf.Web, Version={0}, Culture=neutral, PublicKeyToken={1}".FormatWith(SoafVersion, SoafPublicKeyToken));

            soaf_Presentation = AssemblyResolutionProvider.TryResolveAssembly("Soaf.Presentation, Version={0}, Culture=neutral, PublicKeyToken={1}".FormatWith(SoafVersion, SoafPublicKeyToken));

            system_Web = Lazy.For(() => AssemblyResolutionProvider.TryResolveAssembly("System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"));
        }

        public static void Initialize()
        {
        }
    }

    /// <summary>
    ///   Well known types.
    /// </summary>
    internal static class WellKnownTypes
    {
        // ReSharper disable InconsistentNaming

        private static readonly Lazy<Type> system_Web_Configuration_ConfigurationManager = Lazy.For(() => WellKnownAssemblies.System_Web.IfNotNull(a => a.GetType("System.Web.Configuration.WebConfigurationManager")));

        private static readonly Lazy<Type> soaf_Castle_WindsorServiceProvider = Lazy.For(() => WellKnownAssemblies.Soaf_Castle.IfNotNull(a => a.GetType("Soaf.Castle.WindsorServiceProvider")) ?? Assembly.GetExecutingAssembly().GetType("Soaf.Castle.WindsorServiceProvider"));

        /// <summary>
        /// Web ConfigurationManager
        /// </summary>
        public static Type System_Web_Configuration_ConfigurationManager
        {
            get { return system_Web_Configuration_ConfigurationManager.Value; }
        }

        /// <summary>
        /// Dynamically resolved reference to Soaf.Castle.WindsorServiceProvider to avoid circular dependency
        /// </summary>
        public static Type Soaf_Castle_WindsorServiceProvider
        {
            get { return soaf_Castle_WindsorServiceProvider.Value; }
        }

        // ReSharper restore InconsistentNaming
    }

    internal class Namespaces
    {
        public const string SoafXmlNamespace = "http://soaf";
    }

    /// <summary>
    ///   A repository of resource assemblies for a logical domain
    /// </summary>
    public interface IAssemblyRepository
    {
        IEnumerable<Assembly> Assemblies { get; }
    }

    /// <summary>
    ///   A repository of resource assemblies for a logical domain.
    /// </summary>
    public class AssemblyRepository : IAssemblyRepository
    {
        private readonly IEnumerable<Assembly> assemblies;

        static AssemblyRepository()
        {
            WellKnownAssemblies.Initialize();
        }

        public AssemblyRepository(IEnumerable<Assembly> assemblies)
        {
            var systemAssemblies = new[]
                                       {
                                           WellKnownAssemblies.Soaf,
                                           WellKnownAssemblies.Soaf_Presentation,
                                           WellKnownAssemblies.Soaf_Castle,
                                           WellKnownAssemblies.Soaf_EntityFramework, 
                                           WellKnownAssemblies.Soaf_Web, 
                                           Assembly.GetExecutingAssembly()

                                       }.WhereNotDefault().Distinct().ToArray();
            this.assemblies = systemAssemblies.Concat(assemblies).WhereNotDefault().Distinct().ToArray();
        }

        #region IAssemblyRepository Members

        public virtual IEnumerable<Assembly> Assemblies
        {
            get { return assemblies; }
        }

        #endregion
    }

    public static class Exceptions
    {
        private static readonly MethodInfo PreserveStackTraceMethod;

        private static readonly FieldInfo InnerExceptionField;

        private static readonly FieldInfo RemoteStackTraceField;

        private static readonly FieldInfo StackTraceStringField;

        private static readonly FieldInfo StackTraceField;


        static Exceptions()
        {
            try
            {
                PreserveStackTraceMethod = typeof(Exception).GetMethod("InternalPreserveStackTrace", BindingFlags.Instance | BindingFlags.NonPublic);
                InnerExceptionField = typeof(Exception).GetField("_innerException", BindingFlags.Instance | BindingFlags.NonPublic);
                RemoteStackTraceField = typeof(Exception).GetField("_remoteStackTraceString", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                StackTraceStringField = typeof(Exception).GetField("_stackTraceString", BindingFlags.Instance | BindingFlags.NonPublic);
                StackTraceField = typeof(Exception).GetField("_stackTrace", BindingFlags.Instance | BindingFlags.NonPublic);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
        }

        /// <summary>
        /// Iterates over all inner exception of given exception (first returns exception itself).
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns></returns>
        public static IEnumerable<Exception> GetExceptions(this Exception exception)
        {
            var toProcess = new List<Exception> { exception.EnsureNotDefault() };
            while (toProcess.Count > 0)
            {
                var current = toProcess.First();
                if (current is AggregateException)
                {
                    toProcess.AddRange(current.CastTo<AggregateException>().InnerExceptions);
                }
                else if (current.InnerException != null)
                {
                    toProcess.Add(current.InnerException);
                }

                yield return current;
                toProcess.Remove(current);
            }
        }

        /// <summary>
        /// Sets the inner exception on the target exception using reflection.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="innerException">The inner exception.</param>
        public static void SetInnerException(this Exception target, Exception innerException)
        {
            if (InnerExceptionField != null) InnerExceptionField.SetValue(target, innerException);
        }

        public static void SetStackTrace(this Exception target, string stackTrace)
        {
            if (RemoteStackTraceField != null && StackTraceStringField != null && StackTraceField != null)
            {
                RemoteStackTraceField.SetValue(target, stackTrace);
                StackTraceStringField.SetValue(target, null);
                StackTraceField.SetValue(target, null);
            }
        }

        public static void PreservStackTrace(this Exception ex)
        {
            try
            {
                if (PreserveStackTraceMethod != null) PreserveStackTraceMethod.Invoke(ex, null);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
        }

        /// <summary>
        /// Find an inner exception of a specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="that">The that.</param>
        /// <param name="reconstructWithMessage">The reconstruct with message.</param>
        /// <returns></returns>
        public static T SearchFor<T>(this Exception @that, Func<string, T> reconstructWithMessage = null)
            where T : Exception
        {
            return @that.SearchForAll<T>().FirstOrDefault();
        }

        /// <summary>
        /// Find top level inner exceptions of a specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="that">The that.</param>
        /// <param name="reconstructWithMessage">The reconstruct with message.</param>
        /// <returns></returns>
        public static IEnumerable<T> SearchForAll<T>(this Exception @that, Func<string, T> reconstructWithMessage = null)
            where T : Exception
        {
            // Exception is already of needed type -> returning
            if (@that is T)
            {
                yield return (T)@that;
                yield break;
            }

            var exceptionsToProcess = new List<Exception> { @that };
            while (exceptionsToProcess.Count > 0)
            {
                // Get the first one
                var toExamine = exceptionsToProcess.First();
                // Remove it from the list to process
                exceptionsToProcess.RemoveAt(0);

                // Check if it's what we need
                if (toExamine is T)
                {
                    // Return and move to next one
                    yield return (T)toExamine;
                    continue;
                }

                // Check if it is AggregatedException -> process all inner exceptions
                if (toExamine is AggregateException)
                {
                    var aggregateException = toExamine as AggregateException;
                    exceptionsToProcess.AddRange(aggregateException.InnerExceptions.Where(p => p != null));
                    // Move to next
                    continue;
                }

                // Process fault details
                if (toExamine is FaultException<ExceptionDetail>)
                {
                    var faultException = toExamine as FaultException<ExceptionDetail>;

                    // And all details converted back to exceptions
                    var faultDetailExceptions = faultException.Detail.SearchFaultException(reconstructWithMessage).ToArray();
                    exceptionsToProcess.AddRange(faultDetailExceptions);
                }

                // Process inner exception
                if (toExamine.InnerException != null)
                {
                    exceptionsToProcess.Add(toExamine.InnerException);
                }
            }
        }

        private static IEnumerable<T> SearchFaultException<T>(this ExceptionDetail @that, Func<string, T> reconstructWithMessage)
            where T : Exception
        {
            var detailToAnalyze = @that;

            while (detailToAnalyze != null)
            {
                try
                {
                    // Load its type
                    var exceptionType = Type.GetType(detailToAnalyze.Type);

                    // Is it what we were looking for?
                    if (exceptionType == typeof(T))
                    {
                        if (reconstructWithMessage != null)
                        {
                            yield return reconstructWithMessage(detailToAnalyze.Message);
                        }
                        else
                        {
                            var constructor = exceptionType.GetConstructor(new[] { typeof(string), typeof(Exception) }) ?? exceptionType.GetConstructor(new[] { typeof(string) });
                            if (constructor != null)
                            {
                                var parameters = new List<object> { detailToAnalyze.Message };
                                if (constructor.GetParameters().Length == 2) parameters.Add(null);
                                yield return (T)constructor.Invoke(parameters.ToArray());
                            }
                        }
                    }
                }
                finally
                {
                    // Switch to inner and try again
                    detailToAnalyze = detailToAnalyze.InnerException;
                }
            }
        }

        /// <summary>
        ///   Fluent interface to throw the specified exception.
        /// </summary>
        /// <param name = "ex">The ex.</param>
        [TerminatesProgram]
        public static object Throw(this Exception ex)
        {
            throw ex;
        }

        /// <summary>
        /// Rethrows the specified exception, preserving stack trace.
        /// </summary>
        /// <param name="ex">The ex.</param>
        [TerminatesProgram]
        public static object Rethrow(this Exception ex)
        {
            try
            {
                if (PreserveStackTraceMethod != null) PreserveStackTraceMethod.Invoke(ex, null);
            }
            catch
            {
                throw new Exception(null, ex);
            }
            throw ex;
        }

        /// <summary>
        /// Rethrows the inner exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        [TerminatesProgram]
        public static object RethrowInnerException(this Exception ex)
        {
            if (ex != null && ex.InnerException != null)
            {
                if (PreserveStackTraceMethod != null)
                    PreserveStackTraceMethod.Invoke(ex.InnerException, null);
                throw ex.InnerException;
            }
            return null;
        }
    }

    public static class Types
    {
        internal static readonly Type RuntimeTypeType = typeof(Type).Assembly.GetType("System.RuntimeType");

        private static readonly Type NoParameterFuncType = typeof(Func<>);

        internal static readonly Type[] FuncTypes = NoParameterFuncType.Assembly.GetLoadableTypes().Where(t =>
                                                                                                  t.Is<Delegate>() &&
                                                                                                  t.Namespace == NoParameterFuncType.Namespace &&
                                                                                                  t.Name.StartsWith(NoParameterFuncType.Name.Split('`')[0])).ToArray();


        public static bool HasDataContract(this Type type)
        {
            return type.HasAttribute<DataContractAttribute>()
                   || type.HasAttribute<CollectionDataContractAttribute>();
        }

        /// <summary>
        /// Returns a CLR Type for the specified MetaType.
        /// </summary>
        /// <param name="metaType">Type of the meta.</param>
        /// <returns></returns>
        internal static Type ToType(this MetaType metaType)
        {
            return ServiceProvider.Current.GetService<ITypeSystemRepository>().GetType(metaType);
        }

        /// <summary>
        /// Returns a MethodInfo for the specified MetaMethod.
        /// </summary>
        /// <param name="metaMethod">The meta method.</param>
        /// <returns></returns>
        internal static MethodInfo ToMethod(this MetaMethod metaMethod)
        {
            return ServiceProvider.Current.GetService<ITypeSystemRepository>().GetMethod(metaMethod);
        }

        /// <summary>
        /// Returns a PropertyInfo for the specified MetaProperty.
        /// </summary>
        /// <param name="metaProperty">The meta property.</param>
        /// <returns></returns>
        internal static PropertyInfo ToProperty(this MetaProperty metaProperty)
        {
            return ServiceProvider.Current.GetService<ITypeSystemRepository>().GetProperty(metaProperty);
        }

        /// <summary>
        /// Returns a ConstructorInfo for the specified MetaConstructor.
        /// </summary>
        /// <param name="metaConstructor">The meta constructor.</param>
        /// <returns></returns>
        internal static ConstructorInfo ToProperty(this MetaConstructor metaConstructor)
        {
            return ServiceProvider.Current.GetService<ITypeSystemRepository>().GetConstructor(metaConstructor);
        }

        /// <summary>
        /// Returns a MetaType for the specified CLR Type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        internal static MetaType ToMetaType(this Type type)
        {
            return ServiceProvider.Current.GetService<ITypeSystemRepository>().GetMetaType(type);
        }

        /// <summary>
        /// Returns a MetaMethod for the specified MethodInfo.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <returns></returns>
        internal static MetaMethod ToMetaMethod(this MethodInfo method)
        {
            return ServiceProvider.Current.GetService<ITypeSystemRepository>().GetMetaMethod(method);
        }

        /// <summary>
        /// Returns a MetaProperty for the specified PropertyInfo.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        internal static MetaProperty ToMetaProperty(this PropertyInfo property)
        {
            return ServiceProvider.Current.GetService<ITypeSystemRepository>().GetMetaProperty(property);
        }

        /// <summary>
        /// Returns a MetaConstructor for the specified ConstrutorInfo.
        /// </summary>
        /// <param name="constructor">The constructor.</param>
        /// <returns></returns>
        internal static MetaConstructor ToMetaConstructor(this ConstructorInfo constructor)
        {
            return ServiceProvider.Current.GetService<ITypeSystemRepository>().GetMetaConstructor(constructor);
        }

        /// <summary>
        /// Creates a meta type system graph with DataContractAttributes for the specified metaType.
        /// </summary>
        /// <param name="metaType">Type of the meta.</param>
        /// <param name="isReference">if set to <c>true</c> [is reference].</param>
        /// <param name="ensureHasKey">if set to <c>true</c> [ensure has key].</param>
        /// <returns></returns>
        internal static MetaType ToDataContractType(this MetaType metaType, bool isReference = true, bool ensureHasKey = true)
        {
            return ServiceProvider.Current.GetService<ITypeSystemRepository>().GetDataContractType(metaType, isReference, ensureHasKey);
        }

        /// <summary>
        /// Creates an identical DataContract annotated type graph for the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="isReference">if set to <c>true</c> [is reference].</param>
        /// <param name="ensureHasKey">if set to <c>true</c> [ensure has key].</param>
        /// <returns></returns>
        internal static Type ToDataContractType(this Type type, bool isReference = true, bool ensureHasKey = true)
        {
            return type.ToMetaType().ToDataContractType(isReference, ensureHasKey).ToType();
        }

        /// <summary>
        /// Makes a generic List type for the type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Type MakeListType(this Type type)
        {
            return typeof(List<>).MakeGenericType(type);
        }

        /// <summary>
        /// Makes a generic IEnumerable type for the type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Type MakeEnumerableType(this Type type)
        {
            return typeof(IEnumerable<>).MakeGenericType(type);
        }

        /// <summary>
        /// Modifies objects within fluent syntax expressions
        /// </summary>
        /// <typeparam name="TInput"></typeparam>
        /// <param name="source"></param>
        /// <param name="modifyAction"></param>
        /// <returns></returns>
        public static TInput Modify<TInput>(this TInput source, Action<TInput> modifyAction)
        {
            modifyAction(source);
            return source;
        }

        /// <summary>
        ///   Gets the Func type for the specified result type and parameter types.
        /// </summary>
        /// <param name = "resultType">Type of the result.</param>
        /// <param name = "parameterTypes">The parameter types.</param>
        /// <returns></returns>
        internal static Type GetFuncType(Type resultType, params Type[] parameterTypes)
        {
            Type type = FuncTypes.FirstOrDefault(t => t.GetGenericArguments().Length == parameterTypes.Length + 1);
            if (type != null)
            {
                type = type.MakeGenericType(parameterTypes.Concat(Enumerables.CreateEnumerable(resultType)).ToArray());
            }
            return type;
        }

        /// <summary>
        /// Determines whether the specified value is of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if the specified value is type; otherwise, <c>false</c>.
        /// </returns>
        public static bool Is<T>(this object value)
        {
            return value.Is<T>(false);
        }

        /// <summary>
        ///   Determines whether the specified value is of the specified type.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "value">The value.</param>
        /// <param name = "exactTypeMatch">if set to <c>true</c> [exact type match].</param>
        /// <returns>
        ///   <c>true</c> if the specified value is type; otherwise, <c>false</c>.
        /// </returns>
        public static bool Is<T>(this object value, bool exactTypeMatch)
        {
            return value.Is(typeof(T), exactTypeMatch);
        }

        /// <summary>
        ///   Determines whether the specified value is of the specified type.
        /// </summary>
        /// <param name = "value">The value.</param>
        /// <param name = "isType">Type of the is.</param>
        /// <param name = "exactTypeMatch">if set to <c>true</c> [exact type].</param>
        /// <returns>
        ///   <c>true</c> if the specified value is type; otherwise, <c>false</c>.
        /// </returns>
        public static bool Is(this object value, Type isType, bool exactTypeMatch = false)
        {
            bool result = isType.IsInstanceOfType(value);
            if (result && exactTypeMatch)
            {
                result &= value.GetType() == isType;
            }
            return result;
        }

        /// <summary>
        /// Determines whether the specified type is assignable to the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if the specified value is type; otherwise, <c>false</c>.
        /// </returns>
        public static bool Is<T>(this Type type)
        {
            return type.Is<T>(false);
        }

        /// <summary>
        ///   Determines whether the specified type is assignable to the specified type.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "type">The type.</param>
        /// <param name = "exactTypeMatch">if set to <c>true</c> [exact type].</param>
        /// <returns>
        ///   <c>true</c> if the specified value is type; otherwise, <c>false</c>.
        /// </returns>
        public static bool Is<T>(this Type type, bool exactTypeMatch)
        {
            return type.Is(typeof(T));
        }

        /// <summary>
        ///   Determines whether the specified type is assignable to the specified type.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "isType">Type of the is.</param>
        /// <param name = "exactTypeMatch">if set to <c>true</c> [exact type].</param>
        /// <returns>
        ///   <c>true</c> if the specified value is type; otherwise, <c>false</c>.
        /// </returns>
        public static bool Is(this Type type, Type isType, bool exactTypeMatch = false)
        {
            return type != null && isType != null && isType.IsAssignableFrom(type) && (!exactTypeMatch || isType == type);
        }

        /// <summary>
        /// Determines whether the type is castable to the specified type.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <returns>
        ///   <c>true</c> if [is castable to] [the specified from]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsCastableTo(this Type from, Type to)
        {
            if (to.IsAssignableFrom(from))
            {
                return true;
            }
            bool castable = from.GetMethods(BindingFlags.Public | BindingFlags.Static)
                .Any(
                    m => m.ReturnType == to &&
                         m.Name == "op_Implicit" ||
                         m.Name == "op_Explicit"
                );
            return castable;
        }

        /// <summary>
        ///   Gets the type for the specified typeName. Uses Type.GetType. Optionally searches specified assemblies.
        /// </summary>
        /// <param name = "typeName">Name of the type.</param>
        /// <param name = "assemblies">The assemblies.</param>
        /// <returns></returns>
        public static Type ToType(this string typeName, IEnumerable<Assembly> assemblies = null)
        {
            if (typeName.IsNullOrEmpty())
            {
                return null;
            }
            Type result = Type.GetType(typeName);
            if (result == null)
            {
                if (assemblies == null)
                {
                    assemblies = ServiceProvider.Current.GetService<IAssemblyRepository>().Assemblies;
                }
                foreach (Assembly assembly in assemblies)
                {
                    result = assembly.GetType(typeName.Split(',')[0]);
                    if (result != null)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        ///   Determines if the type inherits from any types that are a closed version of the generic type specified.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "genericTypeDefinition">Type of the generic.</param>
        /// <returns></returns>
        public static bool IsGenericTypeFor(this Type type, Type genericTypeDefinition)
        {
            return type != null && type.GetInheritedGenericType(genericTypeDefinition) != null;
        }

        /// <summary>
        ///   Gets the closed version of the genericTypeDefinition specified if the type implements or inherits it.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "genericTypeDefinition">Type of the generic.</param>
        /// <returns></returns>
        public static Type GetInheritedGenericType(this Type type, Type genericTypeDefinition)
        {
            return type.GetTypeBaseTypesAndInterfaces().FirstOrDefault(t => t.IsGenericType && t.GetGenericTypeDefinition() == genericTypeDefinition);
        }

        /// <summary>
        ///   Determines whether the specified type is a closed version of the genericTypeDefinition specified.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "genericTypeDefinition">Type of the generic.</param>
        /// <returns>
        ///   <c>true</c> if [is closed generic type] [the specified type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool EqualsGenericTypeFor(this Type type, Type genericTypeDefinition)
        {
            return type != null && type.IsGenericType && type.GetGenericTypeDefinition() == genericTypeDefinition;
        }

        /// <summary>
        ///   Finds the type of the element in the enumerable type. Checks if array or implements a generic or non-generic IEnumerable.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        public static Type FindElementType(this Type type)
        {
            if (type == null)
            {
                return null;
            }
            if (type.IsArray)
            {
                return type.GetElementType();
            }
            Type enumerableInteface = type.GetTypeAndInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>));
            if (enumerableInteface != null)
            {
                return enumerableInteface.GetGenericArguments()[0];
            }
            if (type.IsNonStringEnumerable())
            {
                return typeof(object);
            }
            return null;
        }

        /// <summary>
        ///   Gets all inherited types including base types and interfaces.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetBaseTypesAndInterfaces(this Type type)
        {
            return type.GetInterfaces().Concat(type.GetBaseTypes()).Distinct();
        }

        /// <summary>
        ///   Gets all the base types of the specified type.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetBaseTypes(this Type type)
        {
            Type baseType = type.BaseType;
            while (baseType != null)
            {
                yield return baseType;
                baseType = baseType.BaseType;
            }
        }

        /// <summary>
        ///   Gets the type and all of its interfaces.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetTypeAndInterfaces(this Type type)
        {
            return new[] { type }.Concat(type.GetInterfaces());
        }

        /// <summary>
        ///   Gets the type and all of its inherited types.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetTypeBaseTypesAndInterfaces(this Type type)
        {
            return new[] { type }.Concat(type.GetBaseTypesAndInterfaces());
        }

        /// <summary>
        ///   Determines whether the type is a non string enumerable.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns>
        ///   <c>true</c> if [is non string enumerable] [the specified type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNonStringEnumerable(this Type type)
        {
            return typeof(IEnumerable).IsAssignableFrom(type) && type != typeof(string);
        }

        /// <summary>
        ///   Determines whether the the specified type is a Nullable type.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns>
        ///   <c>true</c> if [is nullable type] [the specified type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullableType(this Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        /// <summary>
        /// Gets the type or underlying type if nullable.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Type AsUnderlyingTypeIfNullable(this Type type)
        {
            return Nullable.GetUnderlyingType(type) ?? type;
        }

        /// <summary>
        ///   Determines whether this type can have a null value (is a reference type of a Nullable type).
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can have null value] the specified type; otherwise, <c>false</c>.
        /// </returns>
        public static bool CanHaveNullValue(this Type type)
        {
            return type == null || !type.IsValueType || type.IsNullableType();
        }

        /// <summary>
        /// Gets the default value. Equivalent to default(T) when T is not known at compile type.
        /// </summary>
        /// <param name="type">The type.</param>
        public static object GetDefaultValue(this Type type)
        {
            return type.CanHaveNullValue() ? null : Activator.CreateInstance(type);
        }

        /// <summary>
        ///   Checks if x and y are the same type.
        /// </summary>
        /// <param name = "x">The x.</param>
        /// <param name = "y">The y.</param>
        /// <returns></returns>
        public static bool AreSameTypes(object x, object y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                return false;
            }
            return x.GetType() == y.GetType();
        }
    }

    public static class Objects
    {
        public static IEqualityComparer GetEqualityComparer(this Type type)
        {
            return typeof(EqualityComparer<>).MakeGenericType(type).GetInvoker("get_Default")(null).CastTo<IEqualityComparer>();
        }

        /// <summary>
        /// Gets the hash code with null check.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int GetHashCodeWithNullCheck(object value)
        {
            return value == null ? 0 : value.GetHashCode();
        }

        /// <summary>
        /// Gets a hash code based on the objects fields.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int GetFieldsHashCode(object value)
        {
            return value.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Select(f => f.GetValue(value).IfNotNull(v => v.GetHashCode(), 173)).Aggregate((x, y) => x ^ y);
        }

        /// <summary>
        /// Determines if the two objects are equal based on a comparison of their fields.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns></returns>
        public static bool FieldsEqual(object x, object y)
        {
            return x.GetType() == y.GetType() && x.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Select(f => f.GetValue(x) == f.GetValue(y)).All(i => i);
        }


        /// <summary>
        /// Gets a hash code based on the objects DataMembers.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int GetDataMembersHashCode(object value)
        {
            return value.GetType().GetDataMembers().Select(m => m.MemberInfo.GetGetter()(value)).GetSequenceHashCode();
        }

        /// <summary>
        /// Determines if the two objects are equal based on a comparison of their DataMembers.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns></returns>
        public static bool DataMembersEqual(object x, object y)
        {
            if (x == null && y == null) return true;
            if (x == null || y == null) return false;

            return x.GetType() == y.GetType() &&
                   x.GetType().GetDataMembers().Select(m => m.MemberInfo.GetGetter()).All(g => Equals(g(x), g(y)));
        }

        /// <summary>
        ///   Casts the specified value to type of T.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "value">The value.</param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public static T CastTo<T>(this object value)
        {
            try
            {
                return (T)value;
            }
            catch
            {
                // ReSharper disable RedundantCast
                return (T)(dynamic)value;
                // ReSharper restore RedundantCast
            }
        }

        /// <summary>
        ///   Applies the as operator for T to the specified value.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "value">The value.</param>
        /// <returns></returns>
        public static T As<T>(this object value) where T : class
        {
            return value as T;
        }

        /// <summary>
        /// Tries to convert the specified value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="result">The result.</param>
        /// <param name="formatProvider">The format provider.</param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public static bool TryConvertTo<T>(this object value, out T result, IFormatProvider formatProvider = null)
        {
            object resultInternal;
            bool success = value.TryConvertTo(typeof(T), out resultInternal, formatProvider);
            if (success) result = (T)resultInternal;
            else result = default(T);
            return success;
        }

        /// <summary>
        /// Tries to convert the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="type">The type.</param>
        /// <param name="result">The result.</param>
        /// <param name="formatProvider">The format provider.</param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public static bool TryConvertTo(this object value, Type type, out object result, IFormatProvider formatProvider = null)
        {
            try
            {
                result = value.ConvertTo(type, formatProvider);
                return true;
            }
            catch
            {
                result = type.IsValueType ? Activator.CreateInstance(type) : null;
                return false;
            }
        }

        /// <summary>
        /// Converts the specified value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="formatProvider">The format provider.</param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public static T ConvertTo<T>(this object value, IFormatProvider formatProvider = null)
        {
            return (T)value.ConvertTo(typeof(T), formatProvider);
        }

        /// <summary>
        /// Converts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="type">The type.</param>
        /// <param name="formatProvider">The format provider.</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidCastException">Could not convert value {0} of type {1} to type {2}.FormatWith(value == null ? null : value.ToString(), value == null ? null : value.GetType().FullName, typeof(T).FullName)</exception>
        [DebuggerNonUserCode]
        public static object ConvertTo(this object value, Type type, IFormatProvider formatProvider = null)
        {
            try
            {
                if (value == null) return type.IsValueType ? Activator.CreateInstance(type) : null;

                if (type.IsInstanceOfType(value)) return value;

                var underlyingNullableType = Nullable.GetUnderlyingType(type);
                if (underlyingNullableType != null)
                {
                    value = Convert.ChangeType(value, underlyingNullableType, formatProvider);
                }

                if (value is IConvertible) return Convert.ChangeType(value, type, formatProvider);

                if (type == typeof(string)) return value.ToString();

                var collection = value as IEnumerable;
                var targetElementType = type.Is<IEnumerable>() ? type.FindElementType() : null;
                if (collection != null && targetElementType != null)
                {
                    collection = new List<object>(collection.OfType<object>().Select(o => ConvertTo(o, targetElementType, formatProvider))).ToArray(targetElementType);

                    if (collection != null && type.Is<Array>()) return collection;

                    var suggestedListType = typeof(List<>).MakeGenericType(targetElementType);
                    if (type.IsAssignableFrom(suggestedListType)) type = suggestedListType;

                    var collectionConstructor = type.IsAbstract ? null : type.GetConstructors().FirstOrDefault(c => c.GetParameters().Length == 1 && c.GetParameters()[0].ParameterType.IsInstanceOfType(collection));

                    if (collectionConstructor != null)
                    {
                        return collectionConstructor.Invoke(new[] { collection });
                    }

                    if (collection != null && type.IsGenericTypeFor(typeof(ReadOnlyCollection<>)))
                        return ((dynamic)Activator.CreateInstance(typeof(List<>).MakeGenericType(collection.FindElementType()), collection.ToArray(type.GetElementType()))).AsReadOnly();

                }

                throw new InvalidCastException("Could not convert value {0} of type {1} to type {2}".FormatWith(value.ToString(), value.GetType().FullName, type.FullName));
            }
            catch (Exception ex)
            {
                throw new InvalidCastException("Could not convert value {0} of type {1} to type {2}".FormatWith(value == null ? "null" : value.ToString(), value == null ? "null" : value.GetType().FullName, type.FullName), ex);
            }
        }

        /// <summary>
        /// Checks if the source is default(T). If it is, returns default(TResult). If it's not, returns the result of ifNotDefault.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="ifNotDefault">If not default.</param>
        /// <returns></returns>
        public static TResult IfNotDefault<T, TResult>(this T source, Func<T, TResult> ifNotDefault)
        {
            return source.IfNotDefault(ifNotDefault, default(TResult));
        }

        /// <summary>
        /// Checks if the source is default(T). If it is, returns default(TResult). If it's not, returns the result of ifNotDefault.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="ifNotDefault">If not default.</param>
        /// <param name="ifDefault">If default.</param>
        /// <returns></returns>
        public static TResult IfNotDefault<T, TResult>(this T source, Func<T, TResult> ifNotDefault, TResult ifDefault)
        {
            if (Equals(source, default(T)))
            {
                return ifDefault;
            }
            return ifNotDefault(source);
        }

        /// <summary>
        /// Checks if the source is null. If it is, returns ifNull. If it's not, returns the result of ifNotNull.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="ifNotNull">If not null.</param>
        /// <returns></returns>
        public static TResult IfNotNull<T, TResult>(this T source, Func<T, TResult> ifNotNull) where T : class
        {
            return source.IfNotNull(ifNotNull, default(TResult));
        }

        /// <summary>
        ///   Checks if the source is null. If it is, returns ifNull. If it's not, returns the result of ifNotNull.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <typeparam name = "TResult">The type of the result.</typeparam>
        /// <param name = "source">The source.</param>
        /// <param name = "ifNotNull">If not null.</param>
        /// <param name = "ifNull">If null.</param>
        /// <returns></returns>
        public static TResult IfNotNull<T, TResult>(this T source, Func<T, TResult> ifNotNull, TResult ifNull) where T : class
        {
            if (source == null)
            {
                return ifNull;
            }
            return ifNotNull(source);
        }

        /// <summary>
        /// Checks if the source is null. If it is, performs the ifNull action. If it's not, performs the ifNotNull action.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="ifNotNull">If not null.</param>
        /// <param name="ifNull">If null.</param>
        public static void IfNotNull<T>(this T source, Action<T> ifNotNull, Action ifNull = null) where T : class
        {
            if (source != null) ifNotNull(source);
            else if (ifNull != null) ifNull();
        }

        /// <summary>
        ///   Checks if the source is null. If it is, returns the result of ifNull. If it's not, returns the result of ifNotNull.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <typeparam name = "TResult">The type of the result.</typeparam>
        /// <param name = "source">The source.</param>
        /// <param name = "ifNotNull">If not null.</param>
        /// <param name = "ifNull">If null.</param>
        /// <returns></returns>
        public static TResult IfNotNull<T, TResult>(this T source, Func<T, TResult> ifNotNull, Func<TResult> ifNull) where T : class
        {
            if (source == null)
            {
                return ifNull != null ? ifNull() : default(TResult);
            }
            return ifNotNull != null ? ifNotNull(source) : default(TResult);
        }

        /// <summary>
        ///   Checks if the source is null. If it is, returns the result of ifNull. If it's not, returns the result of ifNotNull.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <typeparam name = "TResult">The type of the result.</typeparam>
        /// <param name = "source">The source.</param>
        /// <param name = "ifNull">If null.</param>
        /// <param name = "ifNotNull">If not null.</param>
        /// <returns></returns>
        [AssertionMethod]
        public static TResult IfNull<T, TResult>([AssertionCondition(AssertionConditionType.IsNotNull)] this T source, Func<TResult> ifNull, Func<T, TResult> ifNotNull = null) where T : class
        {
            if (ifNotNull == null) ifNotNull = o => source.CastTo<TResult>();
            return source.IfNotNull(ifNotNull, ifNull);
        }

        /// <summary>
        ///   Ensures the value is not null/default.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "value">The value.</param>
        /// <param name = "ex">The ex.</param>
        /// <returns></returns>
        [AssertionMethod]
        public static T EnsureNotDefault<T>([AssertionCondition(AssertionConditionType.IsNotNull)] this T value, Exception ex)
        {
            if (Equals(value, default(T)))
            {
                throw ex ?? new InvalidOperationException("Value should not be null");
            }
            return value;
        }

        /// <summary>
        /// Ensures the value is of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="allowDefault">if set to <c>true</c> [allow default].</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        [AssertionMethod]
        public static T EnsureType<T>([AssertionCondition(AssertionConditionType.IsNotNull)] this object value, bool allowDefault = false, string message = null)
        {
            if ((!allowDefault && Equals(value, default(T))) || !value.Is<T>())
            {
                throw message == null ? new InvalidOperationException("Value must be of type {0}, but is of type {1}.".FormatWith(typeof(T).Name, value == null ? "null" : value.GetType().Name)) : new InvalidOperationException(message);
            }
            return (T)value;
        }

        /// <summary>
        /// Ensures the value is not null/default.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        [AssertionMethod]
        public static T EnsureNotDefault<T>([AssertionCondition(AssertionConditionType.IsNotNull)] this T value, string message = null)
        {
            if (Equals(value, default(T)))
            {
                throw message == null ? new InvalidOperationException("Value should not be null") : new InvalidOperationException(message);
            }
            return value;
        }

        /// <summary>
        ///   Performs the specified action with the specified source.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "source">The source.</param>
        /// <param name = "action">The action.</param>
        /// <returns></returns>
        public static T With<T>(this T source, Action<T> action)
        {
            action(source);
            return source;
        }

        /// <summary>
        ///   Uses the specified IDisposable to perform the selection specified, in a using block.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <typeparam name = "TResult">The type of the result.</typeparam>
        /// <param name = "using">The @using.</param>
        /// <param name = "selector">The selector.</param>
        /// <returns></returns>
        public static TResult Using<T, TResult>(this T @using, Func<T, TResult> selector) where T : IDisposable
        {
            using (@using)
            {
                return selector(@using);
            }
        }

        /// <summary>
        ///   Fires a generic event.
        /// </summary>
        /// <typeparam name = "TEventArgs">The type of the event args.</typeparam>
        /// <param name = "handler">The handler.</param>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "TEventArgs" /> instance containing the event data.</param>
        public static void Fire<TEventArgs>(this EventHandler<TEventArgs> handler, object sender, TEventArgs e = null) where TEventArgs : EventArgs
        {
            if (handler != null)
                handler(sender, e ?? Activator.CreateInstance<TEventArgs>());
        }

        /// <summary>
        /// Fires a generic event.
        /// </summary>
        /// <typeparam name="TValue">The type of the event args.</typeparam>
        /// <param name="handler">The handler.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="value">The value.</param>
        public static void Fire<TValue>(this EventHandler<EventArgs<TValue>> handler, object sender, TValue value = default(TValue))
        {
            if (handler != null)
                handler(sender, new EventArgs<TValue>(value));
        }

        /// <summary>
        /// Fires a basic event.
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public static void Fire(this EventHandler handler, object sender, EventArgs e = null)
        {
            if (handler != null)
                handler(sender, e ?? new EventArgs());
        }

        /// <summary>
        ///   Fires PropertyChanged.
        /// </summary>
        /// <param name = "handler">The handler.</param>
        /// <param name = "sender">The sender.</param>
        /// <param name = "propertyName">Name of the property.</param>
        public static void Fire(this PropertyChangedEventHandler handler, object sender, string propertyName)
        {
            if (handler != null)
                handler(sender, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        ///   Fires PropertyChanged.
        /// </summary>
        /// <param name = "handler">The handler.</param>
        /// <param name = "sender">The sender.</param>
        /// <param name = "propertyNameSelector">The property name selector.</param>
        public static void Fire(this PropertyChangedEventHandler handler, object sender, Expression<Func<object>> propertyNameSelector)
        {
            MemberExpression memberExpression = propertyNameSelector.Body is UnaryExpression ? ((UnaryExpression)propertyNameSelector.Body).Operand as MemberExpression : propertyNameSelector.Body as MemberExpression;

            if (memberExpression == null) throw new InvalidOperationException("Property name selector must project a member.");

            handler.Fire(sender, memberExpression.Member.Name);
        }

        /// <summary>
        ///   Fires the handler if the property has been changed and assigns the member with the value.
        /// </summary>
        /// <param name = "handler">The handler.</param>
        /// <param name = "sender">The sender.</param>
        /// <param name = "memberEqualityComparer">The member comparer.</param>
        /// <param name = "propertyNameSelector">The property name selector.</param>
        public static void AssignAndFireIfChanged(this PropertyChangedEventHandler handler, object sender, Expression<Func<bool>> memberEqualityComparer, Expression<Func<object>> propertyNameSelector)
        {
            var binaryExpression = memberEqualityComparer.Body as BinaryExpression;
            if (binaryExpression == null || binaryExpression.NodeType != ExpressionType.Equal) throw new InvalidOperationException("Expression must be an equality comparison.");

            var left = binaryExpression.Left as MemberExpression;
            if (left == null || !(left.Member is FieldInfo) || !(left.Expression is ConstantExpression) || ((ConstantExpression)left.Expression).Value != sender) throw new InvalidOperationException("Expression must compare to a field.");
            var field = (FieldInfo)left.Member;

            object right = Expression.Lambda(binaryExpression.Right).Compile().DynamicInvoke();

            bool isChanged = !Equals(field.GetValue(sender), right);

            field.SetValue(sender, right);

            if (isChanged) handler.Fire(sender, propertyNameSelector);
        }

        #region Nested type: StaticMembersDynamicWrapper

        /// <summary>
        ///   Allows dynamic keyword access to static members
        /// </summary>
        public class StaticMembersDynamicWrapper : DynamicObject
        {
            private readonly Type type;

            public static dynamic For(Type type)
            {
                return new StaticMembersDynamicWrapper(type);
            }

            private StaticMembersDynamicWrapper(Type type)
            {
                this.type = type;
            }

            // Handle static properties
            [DebuggerNonUserCode]
            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                PropertyInfo prop = type.GetProperty(binder.Name, BindingFlags.FlattenHierarchy | BindingFlags.Static | BindingFlags.Public);
                if (prop == null)
                {
                    result = null;
                    return false;
                }

                result = prop.GetValue(null, null);
                return true;
            }

            // Handle static methods
            [DebuggerNonUserCode]
            public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
            {
                // ReSharper disable RedundantEnumerableCastCall
                var method = type.GetMethods(BindingFlags.FlattenHierarchy | BindingFlags.Static | BindingFlags.Public).Where(m => m.Name == binder.Name).OfType<MethodBase>()
                    // ReSharper restore RedundantEnumerableCastCall
                                 .FindBestMethodMatch(args.Select(a => a != null ? a.GetType() : typeof(object))) as MethodInfo;

                if (method == null)
                {
                    result = null;
                    return false;
                }

                result = method.Invoke(null, args);
                return true;
            }
        }

        #endregion

        #region Nested type: FullAccessDynamicWrapper

        public class FullAccessDynamicWrapper : DynamicObject
        {
            /// <summary>
            ///   Specify the flags for accessing members
            /// </summary>
            private const BindingFlags Flags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public;

            /// <summary>
            ///   The object we are going to wrap
            /// </summary>
            private readonly object wrapped;

            /// <summary>
            ///   Create a simple private wrapper
            /// </summary>
            public FullAccessDynamicWrapper(object o)
            {
                wrapped = o;
            }

            public static dynamic For(object o)
            {
                return new FullAccessDynamicWrapper(o);
            }

            /// <summary>
            ///   Create an instance via the constructor matching the arguments
            /// </summary>
            public static dynamic FromType(Assembly asm, string type, params object[] args)
            {
                Type[] allt = asm.GetLoadableTypes();
                Type t = allt.First(item => item.Name == type);


                IEnumerable<Type> types = from a in args
                                          select a.GetType();

                //Gets the constructor matching the specified set of arguments
                ConstructorInfo ctor = t.GetConstructor(Flags, null, types.ToArray(), null);

                if (ctor != null)
                {
                    object instance = ctor.Invoke(args);
                    return new FullAccessDynamicWrapper(instance);
                }

                return null;
            }

            /// <summary>
            ///   Try invoking a method
            /// </summary>
            [DebuggerNonUserCode]
            public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
            {
                IEnumerable<Type> types = from a in args
                                          select a.GetType();

                MethodInfo method = wrapped.GetType().GetMethod
                    (binder.Name, Flags, null, types.ToArray(), null);

                if (method == null)
                {
                    return base.TryInvokeMember(binder, args, out result);
                }
                try
                {
                    result = method.Invoke(wrapped, args);
                }
                catch (TargetInvocationException tie)
                {
                    result = null;
                    tie.InnerException.RethrowInnerException();
                }
                return true;
            }

            /// <summary>
            ///   Tries to get a property or field with the given name
            /// </summary>
            [DebuggerNonUserCode]
            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                //Try getting a property of that name
                PropertyInfo prop = wrapped.GetType().GetProperty(binder.Name, Flags);

                if (prop == null)
                {
                    //Try getting a field of that name
                    FieldInfo fld = wrapped.GetType().GetField(binder.Name, Flags);
                    if (fld != null)
                    {
                        result = fld.GetValue(wrapped);
                        return true;
                    }
                    return base.TryGetMember(binder, out result);
                }
                result = prop.GetValue(wrapped, null);
                return true;
            }

            /// <summary>
            ///   Tries to set a property or field with the given name
            /// </summary>
            [DebuggerNonUserCode]
            public override bool TrySetMember(SetMemberBinder binder, object value)
            {
                PropertyInfo prop = wrapped.GetType().GetProperty(binder.Name, Flags);
                if (prop == null)
                {
                    FieldInfo fld = wrapped.GetType().GetField(binder.Name, Flags);
                    if (fld != null)
                    {
                        fld.SetValue(wrapped, value);
                        return true;
                    }
                    return base.TrySetMember(binder, value);
                }
                prop.SetValue(wrapped, value, null);
                return true;
            }
        }

        #endregion

        #region Nested type: ObjectComparison

        /// <summary>
        ///   Class that allows comparison of two objects of the same type to each other.  Supports classes, lists, arrays, dictionaries, child comparison and more.
        /// </summary>
        public class ObjectComparison
        {
            #region Class Variables

            private readonly List<object> parents = new List<object>();

            #endregion

            #region Properties

            /// <summary>
            ///   Ignore classes, properties, or fields by name during the comparison.
            ///   Case sensitive.
            /// </summary>
            public List<string> ElementsToIgnore { get; set; }

            /// <summary>
            ///   If true, private properties and fields will be compared. The default is false.
            /// </summary>
            public bool ComparePrivateProperties { get; set; }

            /// <summary>
            ///   If true, private fields will be compared. The default is false.
            /// </summary>
            public bool ComparePrivateFields { get; set; }

            /// <summary>
            ///   If true, static properties will be compared.  The default is true.
            /// </summary>
            public bool CompareStaticProperties { get; set; }

            /// <summary>
            ///   If true, static fields will be compared.  The default is true.
            /// </summary>
            public bool CompareStaticFields { get; set; }

            /// <summary>
            ///   If true, child objects will be compared. The default is true. 
            ///   If false, and a list or array is compared list items will be compared but not their children.
            /// </summary>
            public bool CompareChildren { get; set; }

            /// <summary>
            ///   If true, compare read only properties (only the getter is implemented).
            ///   The default is true.
            /// </summary>
            public bool CompareReadOnly { get; set; }

            /// <summary>
            ///   If true, compare fields of a class (see also CompareProperties).
            ///   The default is true.
            /// </summary>
            public bool CompareFields { get; set; }

            /// <summary>
            ///   If true, compare properties of a class (see also CompareFields).
            ///   The default is true.
            /// </summary>
            public bool CompareProperties { get; set; }

            /// <summary>
            ///   The maximum number of differences to detect
            /// </summary>
            /// <remarks>
            ///   Default is 1 for performance reasons.
            /// </remarks>
            public int MaxDifferences { get; set; }

            /// <summary>
            ///   The differences found during the compare
            /// </summary>
            public List<String> Differences { get; set; }

            /// <summary>
            ///   Gets or sets the type to compare resolver.
            /// </summary>
            /// <value>The type to compare resolver.</value>
            public Func<Type, Type> TypeToCompareResolver { get; set; }

            /// <summary>
            ///   The differences found in a string suitable for a textbox
            /// </summary>
            public string DifferencesString
            {
                get
                {
                    var sb = new StringBuilder(4096);

                    sb.Append("\r\nBegin Differences:\r\n");

                    foreach (string item in Differences)
                    {
                        sb.AppendFormat("{0}\r\n", item);
                    }

                    sb.AppendFormat("End Differences (Maximum of {0} differences shown).", MaxDifferences);

                    return sb.ToString();
                }
            }

            #endregion

            #region Constructor

            /// <summary>
            ///   Set up defaults for the comparison
            /// </summary>
            public ObjectComparison()
            {
                Differences = new List<string>();
                ElementsToIgnore = new List<string>();
                CompareStaticFields = true;
                CompareStaticProperties = true;
                ComparePrivateProperties = false;
                ComparePrivateFields = false;
                CompareChildren = true;
                CompareReadOnly = true;
                CompareFields = true;
                CompareProperties = true;
                MaxDifferences = 1;
            }

            #endregion

            #region Public Methods

            /// <summary>
            ///   Compare two objects of the same type to each other.
            /// </summary>
            /// <remarks>
            ///   Check the Differences or DifferencesString Properties for the differences.
            ///   Default MaxDifferences is 1 for performance
            /// </remarks>
            /// <param name = "object1"></param>
            /// <param name = "object2"></param>
            /// <returns>True if they are equal</returns>
            public bool Compare(object object1, object object2)
            {
                string defaultBreadCrumb = string.Empty;

                Differences.Clear();
                Compare(object1, object2, defaultBreadCrumb);

                return Differences.Count == 0;
            }

            #endregion

            #region Private Methods

            /// <summary>
            ///   Compare two objects
            /// </summary>
            /// <param name = "object1">The object1.</param>
            /// <param name = "object2">The object2.</param>
            /// <param name = "breadCrumb">Where we are in the object hiearchy</param>
            private void Compare(object object1, object object2, string breadCrumb)
            {
                //If both null return true
                if (object1 == null && object2 == null)
                    return;

                //Check if one of them is null
                if (object1 == null)
                {
                    Differences.Add(string.Format("object1{0} == null && object2{0} != null ((null),{1})", breadCrumb, ToString(object2)));
                    return;
                }

                if (object2 == null)
                {
                    Differences.Add(string.Format("object1{0} != null && object2{0} == null ({1},(null))", breadCrumb, ToString(object1)));
                    return;
                }

                Type t1 = object1.GetType();
                Type t2 = object2.GetType();

                if (TypeToCompareResolver != null)
                {
                    t1 = TypeToCompareResolver(t1) ?? t1;
                    t2 = TypeToCompareResolver(t2) ?? t2;
                }

                //Objects must be the same type
                if (t1 != t2)
                {
                    Differences.Add(string.Format("Different Types:  object1{0}.GetType() != object2{0}.GetType()", breadCrumb));
                    return;
                }

                if (IsComplexIEnumerable(t1)) //This will do arrays, multi-dimensional arrays and generic enumerables
                {
                    CompareIList(object1, object2, breadCrumb);
                }
                else if (IsIDictionary(t1))
                {
                    CompareIDictionary(object1, object2, breadCrumb);
                }
                else if (IsEnum(t1))
                {
                    CompareEnum(object1, object2, breadCrumb);
                }
                else if (IsSimpleType(t1))
                {
                    CompareSimpleType(object1, object2, breadCrumb);
                }
                else if (IsClass(t1))
                {
                    CompareClass(object1, object2, breadCrumb);
                }
                else if (IsTimespan(t1))
                {
                    CompareTimespan(object1, object2, breadCrumb);
                }
                else if (IsStruct(t1))
                {
                    CompareStruct(object1, object2, breadCrumb);
                }
                else if (t1.Is<IEnumerable>())
                {
                    CompareIList(object1.CastTo<IEnumerable>().OfType<object>().ToList(), object2.CastTo<IEnumerable>().OfType<object>().ToList(), breadCrumb);
                }
                else
                {
                    throw new NotImplementedException("Cannot compare object of type " + t1.Name);
                }
            }

            private static bool IsTimespan(Type t)
            {
                return t == typeof(TimeSpan);
            }

            private static bool IsEnum(Type t)
            {
                return t.IsEnum;
            }

            private static bool IsStruct(Type t)
            {
                return t.IsValueType && !IsSimpleType(t);
            }

            private static bool IsSimpleType(Type t)
            {
                if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    t = Nullable.GetUnderlyingType(t);
                }

                return (t.IsPrimitive
                        || t == typeof(DateTime)
                        || t == typeof(decimal)
                        || t == typeof(string)
                        || t == typeof(Guid)) && t.Is<IComparable>();
            }

            private static bool ValidStructSubType(Type t)
            {
                return IsSimpleType(t)
                       || IsEnum(t)
                       || IsArray(t)
                       || IsClass(t)
                       || IsIDictionary(t)
                       || IsTimespan(t)
                       || IsComplexIEnumerable(t);
            }

            private static bool IsArray(Type t)
            {
                return t.IsArray;
            }

            private static bool IsClass(Type t)
            {
                return t.IsClass;
            }

            private static bool IsIDictionary(Type t)
            {
                return t.GetInterface("System.Collections.IDictionary", true) != null;
            }

            private static bool IsComplexIEnumerable(Type t)
            {
                return t != typeof(string) && new[] { t }.Concat(t.GetInterfaces()).Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>));
            }

            private static bool IsChildType(Type t)
            {
                return !IsSimpleType(t)
                       && (IsClass(t)
                           || IsComplexIEnumerable(t)
                           || IsArray(t)
                           || IsIDictionary(t)
                           || IsStruct(t));
            }

            /// <summary>
            ///   Compare a timespan struct
            /// </summary>
            /// <param name = "object1"></param>
            /// <param name = "object2"></param>
            /// <param name = "breadCrumb"></param>
            private void CompareTimespan(object object1, object object2, string breadCrumb)
            {
                if (((TimeSpan)object1).Ticks != ((TimeSpan)object2).Ticks)
                {
                    Differences.Add(string.Format("object1{0}.Ticks != object2{0}.Ticks", breadCrumb));
                }
            }

            /// <summary>
            ///   Compare an enumeration
            /// </summary>
            /// <param name = "object1"></param>
            /// <param name = "object2"></param>
            /// <param name = "breadCrumb"></param>
            private void CompareEnum(object object1, object object2, string breadCrumb)
            {
                if (object1.ToString() != object2.ToString())
                {
                    string currentBreadCrumb = AddBreadCrumb(breadCrumb, object1.GetType().Name, string.Empty, -1);
                    Differences.Add(string.Format("object1{0} != object2{0} ({1},{2})", currentBreadCrumb, object1, object2));
                }
            }

            /// <summary>
            ///   Compare a simple type
            /// </summary>
            /// <param name = "object1"></param>
            /// <param name = "object2"></param>
            /// <param name = "breadCrumb"></param>
            private void CompareSimpleType(object object1, object object2, string breadCrumb)
            {
                if (object2 == null) //This should never happen, null check happens one level up
                    throw new ArgumentNullException("object2");

                var valOne = object1 as IComparable;

                if (valOne == null) //This should never happen, null check happens one level up
                    throw new ArgumentNullException("object1");

                if (valOne.CompareTo(object2) != 0)
                {
                    Differences.Add(string.Format("object1{0} != object2{0} ({1},{2})", breadCrumb, object1, object2));
                }
            }


            /// <summary>
            ///   Compare a struct
            /// </summary>
            /// <param name = "object1"></param>
            /// <param name = "object2"></param>
            /// <param name = "breadCrumb"></param>
            private void CompareStruct(object object1, object object2, string breadCrumb)
            {
                try
                {
                    parents.Add(object1);
                    parents.Add(object2);

                    Type t1 = object1.GetType();

                    //Compare the fields
                    FieldInfo[] currentFields = t1.GetFields();

                    foreach (FieldInfo item in currentFields)
                    {
                        //Only compare simple types within structs (Recursion Problems)
                        if (!ValidStructSubType(item.FieldType))
                        {
                            continue;
                        }

                        string currentCrumb = AddBreadCrumb(breadCrumb, item.Name, string.Empty, -1);

                        Compare(item.GetValue(object1), item.GetValue(object2), currentCrumb);

                        if (Differences.Count >= MaxDifferences)
                            return;
                    }

                    PerformCompareProperties(t1, object1, object2, breadCrumb);
                }
                finally
                {
                    parents.Remove(object1);
                    parents.Remove(object2);
                }
            }

            /// <summary>
            ///   Compare the properties, fields of a class
            /// </summary>
            /// <param name = "object1">The object1.</param>
            /// <param name = "object2">The object2.</param>
            /// <param name = "breadCrumb">The bread crumb.</param>
            private void CompareClass(object object1, object object2, string breadCrumb)
            {
                try
                {
                    parents.Add(object1);
                    parents.Add(object2);
                    Type t1 = object1.GetType();
                    if (TypeToCompareResolver != null)
                    {
                        t1 = TypeToCompareResolver(t1) ?? t1;
                    }

                    if (Equals(object1, object2))
                    {
                        return;
                    }

                    //We ignore the class name
                    if (ElementsToIgnore.Contains(t1.Name))
                        return;

                    //Compare the properties
                    if (CompareProperties)
                        PerformCompareProperties(t1, object1, object2, breadCrumb);

                    //Compare the fields
                    if (CompareFields)
                        PerformCompareFields(t1, object1, object2, breadCrumb);
                }
                finally
                {
                    parents.Remove(object1);
                    parents.Remove(object2);
                }
            }

            /// <summary>
            ///   Compare the fields of a class
            /// </summary>
            /// <param name = "t1"></param>
            /// <param name = "object1"></param>
            /// <param name = "object2"></param>
            /// <param name = "breadCrumb"></param>
            private void PerformCompareFields(Type t1,
                                              object object1,
                                              object object2,
                                              string breadCrumb)
            {
                FieldInfo[] currentFields;

                if (ComparePrivateFields && !CompareStaticFields)
                    currentFields = t1.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                else if (ComparePrivateFields)
                    currentFields = t1.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
                else if (!CompareStaticFields)
                    currentFields = t1.GetFields(BindingFlags.Public | BindingFlags.Instance);
                else
                    currentFields = t1.GetFields(); //Default is public instance and static

                foreach (FieldInfo item in currentFields)
                {
                    //Skip if this is a shallow compare
                    if (!CompareChildren && IsChildType(item.FieldType))
                        continue;

                    //If we should ignore it, skip it
                    if (ElementsToIgnore.Contains(item.Name))
                        continue;

                    object objectValue1 = item.GetValue(object1);
                    object objectValue2 = item.GetValue(object2);

                    bool object1IsParent = objectValue1 != null && (objectValue1 == object1 || parents.Contains(objectValue1));
                    bool object2IsParent = objectValue2 != null && (objectValue2 == object2 || parents.Contains(objectValue2));

                    //Skip fields that point to the parent
                    if (IsClass(item.FieldType)
                        && (object1IsParent || object2IsParent))
                    {
                        continue;
                    }

                    string currentCrumb = AddBreadCrumb(breadCrumb, item.Name, string.Empty, -1);

                    Compare(objectValue1, objectValue2, currentCrumb);

                    if (Differences.Count >= MaxDifferences)
                        return;
                }
            }


            /// <summary>
            ///   Compare the properties of a class
            /// </summary>
            /// <param name = "t1"></param>
            /// <param name = "object1"></param>
            /// <param name = "object2"></param>
            /// <param name = "breadCrumb"></param>
            private void PerformCompareProperties(Type t1,
                                                  object object1,
                                                  object object2,
                                                  string breadCrumb)
            {
                PropertyInfo[] currentProperties;

                if (ComparePrivateProperties && !CompareStaticProperties)
                    currentProperties = t1.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                else if (ComparePrivateProperties)
                    currentProperties = t1.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
                else if (!CompareStaticProperties)
                    currentProperties = t1.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                else
                    currentProperties = t1.GetProperties(); //Default is public instance and static

                foreach (PropertyInfo info in currentProperties)
                {
                    //If we can't read it, skip it
                    if (info.CanRead == false)
                        continue;

                    //Skip if this is a shallow compare
                    if (!CompareChildren && IsChildType(info.PropertyType))
                        continue;

                    //If we should ignore it, skip it
                    if (ElementsToIgnore.Contains(info.Name))
                        continue;

                    //If we should ignore read only, skip it
                    if (!CompareReadOnly && info.CanWrite == false)
                        continue;

                    object objectValue1;
                    object objectValue2;
                    if (!IsValidIndexer(info, breadCrumb))
                    {
                        objectValue1 = info.GetValue(object1, null);
                        objectValue2 = info.GetValue(object2, null);
                    }
                    else
                    {
                        CompareIndexer(info, object1, object2, breadCrumb);
                        continue;
                    }

                    bool object1IsParent = objectValue1 != null && (objectValue1 == object1 || parents.Contains(objectValue1));
                    bool object2IsParent = objectValue2 != null && (objectValue2 == object2 || parents.Contains(objectValue2));

                    //Skip properties where both point to the corresponding parent
                    if ((IsClass(info.PropertyType) || IsStruct(info.PropertyType)) && (object1IsParent && object2IsParent))
                    {
                        continue;
                    }

                    string currentCrumb = AddBreadCrumb(breadCrumb, info.Name, string.Empty, -1);

                    Compare(objectValue1, objectValue2, currentCrumb);

                    if (Differences.Count >= MaxDifferences)
                        return;
                }
            }

            private static bool IsValidIndexer(PropertyInfo info, string breadCrumb)
            {
                ParameterInfo[] indexers = info.GetIndexParameters();

                if (indexers.Length == 0)
                {
                    return false;
                }

                if (indexers.Length > 1)
                {
                    throw new Exception("Cannot compare objects with more than one indexer for object " + breadCrumb);
                }

                if (indexers[0].ParameterType != typeof(Int32))
                {
                    throw new Exception("Cannot compare objects with a non integer indexer for object " + breadCrumb);
                }

                if (info.ReflectedType.GetProperty("Count") == null)
                {
                    throw new Exception("Indexer must have a corresponding Count property for object " + breadCrumb);
                }

                if (info.ReflectedType.GetProperty("Count").PropertyType != typeof(Int32))
                {
                    throw new Exception("Indexer must have a corresponding Count property that is an integer for object " + breadCrumb);
                }

                return true;
            }

            private void CompareIndexer(PropertyInfo info, object object1, object object2, string breadCrumb)
            {
                string currentCrumb;
                var indexerCount1 = (int)info.ReflectedType.GetProperty("Count").GetGetMethod(true).Invoke(object1, new object[] { });
                var indexerCount2 = (int)info.ReflectedType.GetProperty("Count").GetGetMethod(true).Invoke(object2, new object[] { });

                //Indexers must be the same length
                if (indexerCount1 != indexerCount2)
                {
                    currentCrumb = AddBreadCrumb(breadCrumb, info.Name, string.Empty, -1);
                    Differences.Add(string.Format("object1{0}.Count != object2{0}.Count ({1},{2})", currentCrumb,
                                                  indexerCount1, indexerCount2));

                    if (Differences.Count >= MaxDifferences)
                        return;
                }

                // Run on indexer
                for (int i = 0; i < indexerCount1; i++)
                {
                    currentCrumb = AddBreadCrumb(breadCrumb, info.Name, string.Empty, i);
                    object objectValue1 = info.GetValue(object1, new object[] { i });
                    object objectValue2 = info.GetValue(object2, new object[] { i });
                    Compare(objectValue1, objectValue2, currentCrumb);

                    if (Differences.Count >= MaxDifferences)
                        return;
                }
            }

            /// <summary>
            ///   Compare a dictionary
            /// </summary>
            /// <param name = "object1"></param>
            /// <param name = "object2"></param>
            /// <param name = "breadCrumb"></param>
            private void CompareIDictionary(object object1, object object2, string breadCrumb)
            {
                var iDict1 = object1 as IDictionary;
                var iDict2 = object2 as IDictionary;

                if (iDict1 == null) //This should never happen, null check happens one level up
                    throw new ArgumentNullException("object1");

                if (iDict2 == null) //This should never happen, null check happens one level up
                    throw new ArgumentNullException("object2");

                try
                {
                    parents.Add(object1);
                    parents.Add(object2);

                    //Objects must be the same length
                    if (iDict1.Count != iDict2.Count)
                    {
                        Differences.Add(string.Format("object1{0}.Count != object2{0}.Count ({1},{2})", breadCrumb,
                                                      iDict1.Count, iDict2.Count));

                        if (Differences.Count >= MaxDifferences)
                            return;
                    }

                    IDictionaryEnumerator enumerator1 = iDict1.GetEnumerator();
                    IDictionaryEnumerator enumerator2 = iDict2.GetEnumerator();

                    while (enumerator1.MoveNext() && enumerator2.MoveNext())
                    {
                        string currentBreadCrumb = AddBreadCrumb(breadCrumb, "Key", string.Empty, -1);

                        Compare(enumerator1.Key, enumerator2.Key, currentBreadCrumb);

                        if (Differences.Count >= MaxDifferences)
                            return;

                        currentBreadCrumb = AddBreadCrumb(breadCrumb, "Value", string.Empty, -1);

                        Compare(enumerator1.Value, enumerator2.Value, currentBreadCrumb);

                        if (Differences.Count >= MaxDifferences)
                            return;
                    }
                }
                finally
                {
                    parents.Remove(object1);
                    parents.Remove(object2);
                }
            }

            /// <summary>
            ///   Convert an object to a nicely formatted string
            /// </summary>
            /// <param name = "obj"></param>
            /// <returns></returns>
            private static string ToString(object obj)
            {
                try
                {
                    if (obj == null)
                        return "(null)";

                    if (obj == DBNull.Value)
                        return "System.DBNull.Value";

                    return obj.ToString();
                }
                catch
                {
                    return string.Empty;
                }
            }


            /// <summary>
            ///   Compare an array or something that implements IList
            /// </summary>
            /// <param name = "object1"></param>
            /// <param name = "object2"></param>
            /// <param name = "breadCrumb"></param>
            private void CompareIList(object object1, object object2, string breadCrumb)
            {
                var enumerable1 = object1 as IEnumerable;
                var enumerable2 = object2 as IEnumerable;

                if (enumerable1 == null) //This should never happen, null check happens one level up
                    throw new ArgumentNullException("object1");

                if (enumerable2 == null) //This should never happen, null check happens one level up
                    throw new ArgumentNullException("object2");

                try
                {
                    parents.Add(object1);
                    parents.Add(object2);

                    //Objects must be the same length
                    if (enumerable1.OfType<object>().Count() != enumerable2.OfType<object>().Count())
                    {
                        Differences.Add(string.Format("object1{0}.Count != object2{0}.Count ({1},{2})", breadCrumb,
                                                      enumerable1.OfType<object>().Count(), enumerable2.OfType<object>().Count()));

                        if (Differences.Count >= MaxDifferences)
                            return;
                    }

                    IEnumerator enumerator1 = enumerable1.GetEnumerator();
                    IEnumerator enumerator2 = enumerable2.GetEnumerator();
                    int count = 0;

                    while (enumerator1.MoveNext() && enumerator2.MoveNext())
                    {
                        string currentBreadCrumb = AddBreadCrumb(breadCrumb, string.Empty, string.Empty, count);

                        Compare(enumerator1.Current, enumerator2.Current, currentBreadCrumb);

                        if (Differences.Count >= MaxDifferences)
                            return;

                        count++;
                    }
                }
                finally
                {
                    parents.Remove(object1);
                    parents.Remove(object2);
                }
            }


            /// <summary>
            ///   Add a breadcrumb to an existing breadcrumb
            /// </summary>
            /// <param name = "existing"></param>
            /// <param name = "name"></param>
            /// <param name = "extra"></param>
            /// <param name = "index"></param>
            /// <returns></returns>
            private static string AddBreadCrumb(string existing, string name, string extra, string index)
            {
                bool useIndex = !String.IsNullOrEmpty(index);
                bool useName = name.Length > 0;
                var sb = new StringBuilder();

                sb.Append(existing);

                if (useName)
                {
                    sb.AppendFormat(".");
                    sb.Append(name);
                }

                sb.Append(extra);

                if (useIndex)
                {
                    int result;
                    sb.AppendFormat(Int32.TryParse(index, out result) ? "[{0}]" : "[\"{0}\"]", index);
                }

                return sb.ToString();
            }

            /// <summary>
            ///   Add a breadcrumb to an existing breadcrumb
            /// </summary>
            /// <param name = "existing"></param>
            /// <param name = "name"></param>
            /// <param name = "extra"></param>
            /// <param name = "index"></param>
            /// <returns></returns>
            private static string AddBreadCrumb(string existing, string name, string extra, int index)
            {
                return AddBreadCrumb(existing, name, extra, index >= 0 ? index.ToString() : null);
            }

            #endregion
        }

        #endregion
    }

    /// <summary>
    /// Helpers for enums.
    /// </summary>
    public static class Enums
    {
        /// <summary>
        /// Gets the display name based on attributes.
        /// </summary>
        /// <param name="value">The provider.</param>
        /// <returns></returns>
        public static string GetDisplayName(this Enum value)
        {
            var attributes = value.GetAttributes<Attribute>().ToArray();
            return
                attributes.Where(a => a.GetType().FullName == "System.ComponentModel.DisplayNameAttribute").Select(i => i.CastTo<dynamic>().DisplayName).FirstOrDefault() ??
                attributes.Where(a => a.GetType().FullName == "System.ComponentModel.DataAnnotations.DisplayAttribute").Select(i => i.CastTo<dynamic>().Name).FirstOrDefault() ??
                attributes.OfType<DescriptionAttribute>().Select(i => i.Description).FirstOrDefault() ??
                attributes.Where(a => a.GetType().FullName == "System.Runtime.Serialization.DataMemberAttribute").Select(i => i.CastTo<dynamic>().Name).FirstOrDefault() ??
                value.ToString();
        }

        /// <summary>
        /// Gets the description for the enum value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {
            return value.GetAttribute<DescriptionAttribute>().IfNotNull(a => a.Description, value.ToString);
        }

        /// <summary>
        /// Gets the enum value with the specified display name.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static TEnum ToEnumFromDisplayName<TEnum>(this string value) where TEnum : struct
        {
            return GetValues<TEnum>().First(v => ((Enum)(object)v).GetDisplayName() == value);
        }

        /// <summary>
        /// Gets the enum value with the specified display name.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static TEnum ToEnumFromDisplayNameOrDefault<TEnum>(this string value) where TEnum : struct
        {
            return GetValues<TEnum>().FirstOrDefault(v => ((Enum)(object)v).GetDisplayName() == value);
        }

        /// <summary>
        /// Gets the enum value with the specified display name.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static TEnum? ToEnumFromDisplayNameOrNull<TEnum>(this string value) where TEnum : struct
        {
            if (GetValues<TEnum>().Any(v => ((Enum)(object)v).GetDisplayName() == value))
            {
                return value.ToEnumFromDisplayName<TEnum>();
            }
            return null;
        }

        /// <summary>
        ///   Translate's an enum name value to Enum of type T, with the same name.
        /// </summary>
        /// <typeparam name = "TEnum"></typeparam>
        /// <param name = "value"></param>
        /// <returns></returns>
        public static TEnum TranslateEnum<TEnum>(this Enum value) where TEnum : struct
        {
            return value.ToString().ToEnum<TEnum>();
        }

        /// <summary>
        /// Converts the string value to an enum of type T.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static TEnum ToEnum<TEnum>(this string value) where TEnum : struct
        {
            TEnum result;
            if (value.TryToEnum(out result))
            {
                return result;
            }
            throw new InvalidOperationException("Could not convert \"{0}\" to enum of type {1}.".FormatWith(value, typeof(TEnum).FullName));
        }

        /// <summary>
        /// Converts the string to the specified enum type if possible. Returns default value on failure.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static TEnum ToEnumOrDefault<TEnum>(this string value, TEnum defaultValue = default(TEnum)) where TEnum : struct
        {
            TEnum result;
            if (value.TryToEnum(out result))
            {
                return result;
            }
            return defaultValue;
        }

        /// <summary>
        /// Converts the string to the specified enum type if possible. Returns null on failure.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static TEnum? ToEnumOrNull<TEnum>(this string value) where TEnum : struct
        {
            TEnum result;
            if (value.TryToEnum(out result))
            {
                return result;
            }
            return null;
        }

        /// <summary>
        ///   Tries to convert the string value to the specified type of enum.
        /// </summary>
        /// <typeparam name = "TEnum">The type of the enum.</typeparam>
        /// <param name = "value">The value.</param>
        /// <param name = "result">The result.</param>
        /// <returns>Success or failure of the conversion.</returns>
        public static bool TryToEnum<TEnum>(this string value, out TEnum result) where TEnum : struct
        {
            bool success = false;
            if (default(TEnum).CastTo<Enum>().GetNames().Contains(value, StringComparer.OrdinalIgnoreCase))
            {
                result = (TEnum)Enum.Parse(typeof(TEnum), value, true);
                success = true;
            }
            else
            {
                result = default(TEnum);
            }

            return success;
        }

        /// <summary>
        /// Removes the specified value from the flags enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The type.</param>
        /// <param name="toRemove">To remove.</param>
        /// <returns></returns>
        public static T Remove<T>(this Enum value, T toRemove)
        {
            try
            {
                return (T)(((dynamic)value & ~(dynamic)toRemove));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    string.Format(
                        "Could not remove value {0} from enumerated value '{1}'.",
                        value, toRemove)
                        , ex);
            }
        }

        /// <summary>
        /// Adds the specified value from the flags enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The type.</param>
        /// <param name="toAdd">To remove.</param>
        /// <returns></returns>
        public static T Add<T>(this Enum value, T toAdd)
        {
            try
            {
                return (T)(((dynamic)value | (dynamic)toAdd));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    string.Format(
                        "Could not add value {0} from enumerated value '{1}'.",
                        value, toAdd)
                        , ex);
            }
        }

        /// <summary>
        ///   Gets the names of the values in the specified enum's type..
        /// </summary>
        /// <param name = "e">The e.</param>
        /// <returns></returns>
        public static string[] GetNames(this Enum e)
        {
            List<string> enumNames =
                e.GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Select(fi => fi.Name).ToList();

            return enumNames.ToArray();
        }

        /// <summary>
        ///   Gets the underlying values for the type of the specified enum.
        /// </summary>
        /// <param name = "e">The e.</param>
        /// <returns></returns>
        public static Array GetValues(this Enum e)
        {
            return GetValues(e.GetType());
        }

        /// <summary>
        /// Gets the underlying values for the type of the specified enum type.
        /// </summary>
        /// <param name="enumType">Type of the enum.</param>
        /// <returns></returns>
        public static object[] GetValues(Type enumType)
        {
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Type '" + enumType.Name + "' is not an enum");
            }

            var fields = from field in enumType.GetFields()
                         where field.IsLiteral
                         select field;

            return fields.Select(field => field.GetValue(enumType)).ToArray();
        }

        /// <summary>
        /// Gets the strongly typed values defined by the enum.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <returns></returns>
        public static TEnum[] GetValues<TEnum>() where TEnum : struct
        {
            Type enumType = typeof(TEnum);

            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Type '" + enumType.Name + "' is not an enum");
            }

            var fields = from field in enumType.GetFields()
                         where field.IsLiteral
                         select field;

            return fields.Select(field => field.GetValue(enumType)).Select(value => (TEnum)value).ToArray();
        }

        /// <summary>
        /// Gets the attributes on the enum value.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this Enum value) where TAttribute : Attribute
        {
            var field = value.GetType().GetField(value.ToString());
            return field.GetAttributes<TAttribute>();
        }

        /// <summary>
        /// Gets the attributes on the enum value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static IEnumerable<Attribute> GetAttributes(this Enum value, Type type)
        {
            var field = value.GetType().GetField(value.ToString());
            return field.GetAttributes(type);
        }

        /// <summary>
        /// Gets the attribute on the enum value.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static TAttribute GetAttribute<TAttribute>(this Enum value) where TAttribute : Attribute
        {
            var field = value.GetType().GetField(value.ToString());
            return field.GetAttribute<TAttribute>();
        }

        /// <summary>
        /// Gets the attribute on the enum value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="type">Type of the attribute.</param>
        /// <returns></returns>
        public static Attribute GetAttribute(this Enum value, Type type)
        {
            var field = value.GetType().GetField(value.ToString());
            return field.GetAttribute(type);
        }

        /// <summary>
        /// Gets the flags in the specified enum value, including combined ones.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static IEnumerable<Enum> GetFlags(this Enum value)
        {
            return GetFlags(value, GetValues(value.GetType()).Cast<Enum>().ToArray());
        }

        /// <summary>
        /// Gets the individual flags, excluding combined values.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static IEnumerable<Enum> GetIndividualFlags(this Enum value)
        {
            return GetFlags(value, GetFlagValues(value.GetType()).ToArray());
        }

        private static IEnumerable<Enum> GetFlags(Enum value, Enum[] values)
        {
            ulong bits = Convert.ToUInt64(value);
            var results = new List<Enum>();
            for (int i = values.Length - 1; i >= 0; i--)
            {
                ulong mask = Convert.ToUInt64(values[i]);
                if (i == 0 && mask == 0L)
                    break;
                if ((bits & mask) == mask)
                {
                    results.Add(values[i]);
                    bits -= mask;
                }
            }
            if (bits != 0L)
                return new Enum[0];
            if (Convert.ToUInt64(value) != 0L)
                return results.Reverse<Enum>();
            if (bits == Convert.ToUInt64(value) && values.Length > 0 && Convert.ToUInt64(values[0]) == 0L)
                return values.Take(1);
            return new Enum[0];
        }

        private static IEnumerable<Enum> GetFlagValues(Type enumType)
        {
            ulong flag = 0x1;
            foreach (var value in GetValues(enumType).Cast<Enum>())
            {
                ulong bits = Convert.ToUInt64(value);
                if (bits == 0L)
                    //yield return value;
                    continue; // skip the zero value
                while (flag < bits) flag <<= 1;
                if (flag == bits)
                    yield return value;
            }
        }
    }

    public static class Strings
    {
        /// <summary>
        /// Determines whether the string is null or whitespace.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpace(this string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }

        /// <summary>
        /// Determines whether the string is null or empty.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        /// <summary>
        /// Trims the start.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="trim">The trim.</param>
        /// <returns></returns>
        public static string TrimStart(this string value, string trim)
        {
            while (value.StartsWith(trim))
            {
                value = value.Substring(trim.Length);
            }
            return value;
        }

        /// <summary>
        /// Trims the end.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="trim">The trim.</param>
        /// <returns></returns>
        public static string TrimEnd(this string value, string trim)
        {
            while (value.EndsWith(trim))
            {
                value = value.Substring(0, value.Length - trim.Length);
            }
            return value;
        }

        /// <summary>
        /// Removes the non numeric parts of the string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string ToNumeric(this string value)
        {
            return new string(value.Where(char.IsNumber).ToArray());
        }

        /// <summary>
        /// Determines whether the specified value is numeric.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if the specified value is numeric; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNumeric(this string value)
        {
            return !string.IsNullOrEmpty(value) && value.All(char.IsNumber);
        }

        /// <summary>
        /// Truncates the specified string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static string Truncate(this string value, int length)
        {
            return value.Truncate(length, false);
        }

        /// <summary>
        /// Truncates the specified string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="length">The length.</param>
        /// <param name="truncateStart">if set to true truncates the beginning of the string instead of it's tail.</param>
        /// <returns></returns>
        public static string Truncate(this string value, int length, bool truncateStart)
        {
            if ((value == null))
            {
                return string.Empty;
            }

            var newLength = Math.Min(value.Length, length);
            return value.Substring(truncateStart ? value.Length - newLength : 0, newLength);
        }

        /// <summary>
        /// Tries to conver the string to an int.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int? ToInt(this string value)
        {
            if (value == null)
            {
                return null;
            }
            Int32 intValue;
            if ((Int32.TryParse(value, out intValue)))
            {
                return intValue;
            }
            return null;
        }

        /// <summary>
        /// Tries to convert the string to a decimal.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static decimal? ToDecimal(this string value)
        {
            if (value == null)
            {
                return null;
            }
            decimal decimalValue;
            if ((decimal.TryParse(value, out decimalValue)))
            {
                return decimalValue;
            }
            return null;
        }

        /// <summary>
        /// Tries to convert the string to a boolean.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static bool? ToBoolean(this string value)
        {
            if (value == null)
            {
                return null;
            }
            bool booleanValue;
            if (bool.TryParse(value, out booleanValue)) return booleanValue;
            return null;
        }

        /// <summary>
        /// Converts to date time.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        public static DateTime? ToDateTime(this string value, string format)
        {
            DateTime result;
            if ((DateTime.TryParseExact(value, format, null, DateTimeStyles.None, out result)))
            {
                return result;
            }
            return null;
        }

        /// <summary>
        /// Replaces the first occurance.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="oldValue">The old value.</param>
        /// <param name="newValue">The new value.</param>
        /// <returns></returns>
        public static string ReplaceFirst(this string text, string oldValue, string newValue)
        {
            int pos = text.IndexOf(oldValue);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + newValue + text.Substring(pos + oldValue.Length);
        }

        public static string ToString(object o, string format)
        {
            return ToString(o, format, null);
        }

        public static string ToString(this object o, string format, IFormatProvider formatProvider)
        {
            var sb = new StringBuilder();
            Type type = o.GetType();
            var regex = new Regex("({)([^}]+)(})", RegexOptions.IgnoreCase);
            MatchCollection mc = regex.Matches(format);
            int startIndex = 0;
            foreach (Match m in mc)
            {
                Group g = m.Groups[2];
                // it's second in the match between { and }
                int length = g.Index - startIndex - 1;
                sb.Append(format.Substring(startIndex, length));

                string toGet;
                string toFormat = String.Empty;
                int formatIndex = g.Value.IndexOf(":");
                // formatting would be to the right of a :
                if (formatIndex == -1)
                {
                    // no formatting, no worries
                    toGet = g.Value;
                }
                else
                {
                    // pickup the formatting
                    toGet = g.Value.Substring(0, formatIndex);
                    toFormat = g.Value.Substring(formatIndex + 1);
                }

                // first try properties
                PropertyInfo retrievedProperty = type.GetProperty(toGet);
                Type retrievedType = null;
                object retrievedObject = null;
                if (retrievedProperty != null)
                {
                    retrievedType = retrievedProperty.PropertyType;
                    retrievedObject = retrievedProperty.GetValue(o, null);
                }
                else
                {
                    // try fields
                    FieldInfo retrievedField = type.GetField(toGet);
                    if (retrievedField != null)
                    {
                        retrievedType = retrievedField.FieldType;
                        retrievedObject = retrievedField.GetValue(o);
                    }
                }

                if (retrievedType != null)
                {
                    // we found something
                    string result;
                    if (toFormat == String.Empty)
                    {
                        // no format info
                        result = retrievedType.InvokeMember("ToString", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.IgnoreCase, null, retrievedObject, null) as string;
                    }
                    else
                    {
                        // format info
                        result = retrievedType.InvokeMember("ToString", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.IgnoreCase, null, retrievedObject, new object[] {
							toFormat,
							formatProvider
						}) as string;
                    }
                    sb.Append(result);
                }
                else
                {
                    // didn't find a property with that name, so be gracious and put it back
                    sb.Append("{");
                    sb.Append(g.Value);
                    sb.Append("}");
                }
                startIndex = g.Index + g.Length + 1;
            }
            if (startIndex < format.Length)
            {
                // include the rest (end) of the string
                sb.Append(format.Substring(startIndex));
            }
            return sb.ToString();
        }

        /// <summary>
        ///   Returns ToString if the value is not null, otherwise returns the value ifNull.
        /// </summary>
        /// <param name = "value">The value.</param>
        /// <param name = "ifNull">If null.</param>
        /// <returns></returns>
        public static string ToStringIfNotNull(this object value, string ifNull = "null")
        {
            return value.IfNull(() => ifNull, v => v.ToString());
        }

        /// <summary>
        ///   Matches the specified string against a wilcard template.
        /// </summary>
        /// <param name = "value">The value.</param>
        /// <param name = "template">The template.</param>
        /// <returns>
        ///   <c>true</c> if the specified value is match; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsMatch(this string value, string template)
        {
            if (template.Length == 0) return value.Length == 0;
            if (value.Length == 0) return false;
            if (template[0] == '*' && template.Length > 1)
                for (int index = 0; index < value.Length; index++)
                {
                    if (IsMatch(template.Substring(1), value.Substring(index)))
                        return true;
                }
            else if (template[0] == '*')
                return true;
            else if (template[0] == value[0])
                return IsMatch(template.Substring(1), value.Substring(1));
            return false;
        }

        /// <summary>
        ///   Formats the format string with the specified arguments.
        /// </summary>
        /// <param name = "format">The format.</param>
        /// <param name = "arguments">The arguments.</param>
        /// <returns></returns>
        [StringFormatMethod("format")]
        public static string FormatWith(this string format, params object[] arguments)
        {
            return string.Format(format, arguments);
        }



        /// <summary>
        ///   Creates a JSON StringBuilder representing values for the properties specified using the propertyExpressions on the source object.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "source">The source.</param>
        /// <param name = "propertyExpressions">The properties.</param>
        /// <returns></returns>
        public static StringBuilder ToJson<T>(T source, IEnumerable<Expression<Func<object>>> propertyExpressions)
        {
            IEnumerable<PropertyInfo> propertyInfos = propertyExpressions.Select(m => m.GatherMemberExpressions().FirstOrDefault()).Where(i => i != null).Select(e => e.Member).OfType<PropertyInfo>();
            return ToJson(source, propertyInfos);
        }

        /// <summary>
        ///   Creates a JSON StringBuilder representing all of the properties on the source object.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns></returns>
        public static StringBuilder ToJson(object source)
        {
            return ToJson(source, source.GetType().GetProperties().Where(pi => pi.CanRead));
        }

        /// <summary>
        ///   Creates a JSON StringBuilder representing the specified properties on the source object.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <param name = "propertyInfos">The property infos.</param>
        /// <returns></returns>
        public static StringBuilder ToJson(object source, IEnumerable<PropertyInfo> propertyInfos)
        {
            var sb = new StringBuilder();
            sb.Append("{ ");
            foreach (PropertyInfo pi in propertyInfos)
            {
                sb.AppendFormat("\"{0}\": \"{1}\", ", pi.Name, pi.GetGetMethod(true).GetInvoker()(source));
            }
            if (sb.Length > 0)
            {
                sb.Remove(sb.Length - 2, 2);
            }
            sb.Append(" }");

            return sb;
        }

        /// <summary>
        /// Splits the string on the separators specified when matched outside of quotations.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <param name="separators">The separators.</param>
        /// <param name="enclosingValues">The enclosing values. Defaults to double quotes.</param>
        /// <param name="escapeCharacters">The escape characters. Defaults to \</param>
        /// <param name="includeSeparators">if set to <c>true</c> [include separators].</param>
        /// <param name="comparisonType">Type of the comparison.</param>
        /// <returns></returns>
        public static IEnumerable<string> SplitNotEnclosed(this string s, IEnumerable<string> separators, Dictionary<string, string> enclosingValues = null, IEnumerable<char> escapeCharacters = null, bool includeSeparators = false, StringComparison comparisonType = StringComparison.Ordinal)
        {
            var results = new List<string>();

            var enclosureStack = new Stack<KeyValuePair<string, string>>();
            bool atEscapedCharacter = false;

            if (escapeCharacters == null) escapeCharacters = new[] { '\\' };
            if (enclosingValues == null) enclosingValues = new[] { "\"" }.ToDictionary(i => i);

            var orderedEnclosingValues = enclosingValues.OrderByDescending(i => i.Key.Length).ToArray();
            separators = separators.OrderByDescending(v => v.Length).ToArray();

            var currentPart = new StringBuilder();

            while (s.Length > 0)
            {
                int addToIndex = 0;

                var newEnclosingValue = orderedEnclosingValues.FirstOrDefault(v => s.StartsWith(v.Key, comparisonType));

                if (enclosureStack.Count > 0 && !atEscapedCharacter && s.StartsWith(enclosureStack.Peek().Value))
                {
                    addToIndex = enclosureStack.Peek().Value.Length;
                    enclosureStack.Pop();
                }
                else if (newEnclosingValue.Key != null && !atEscapedCharacter)
                {
                    enclosureStack.Push(newEnclosingValue);
                    addToIndex = newEnclosingValue.Key.Length;
                }
                else if (escapeCharacters.Contains(s[0]) && enclosureStack.Count > 0)
                {
                    atEscapedCharacter = !atEscapedCharacter;
                    addToIndex = 1;
                }
                else if (enclosureStack.Count > 0)
                {
                    atEscapedCharacter = false;
                    addToIndex = 1;
                }

                if (enclosureStack.Count == 0)
                {
                    string separator = separators.FirstOrDefault(v => s.StartsWith(v, comparisonType));

                    if (separator != null)
                    {
                        if (currentPart.Length > 0) results.Add(currentPart.ToString());
                        results.Add(separator);
                        s = s.Substring(separator.Length);
                        currentPart = new StringBuilder();

                        addToIndex = 0;
                    }
                    else
                    {
                        addToIndex = 1;
                    }
                }

                currentPart.Append(s.Substring(0, addToIndex));
                s = s.Substring(addToIndex);
            }

            if (currentPart.Length > 0) results.Add(currentPart.ToString());

            if (!includeSeparators)
            {
                results = results.Except(separators).ToList();
            }

            return results.ToArray();
        }

        /// <summary>
        /// Strips the quotes surrounding the string.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        public static string StripQuotes(this string s)
        {
            return s.Strip("\"");
        }

        /// <summary>
        /// Surrounds the string with quotes.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        public static string Enquote(this string s)
        {
            return s.Surround("\"");
        }

        /// <summary>
        /// Ensures the string is enclosed in quotes.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        public static string EnsureQuoted(this string s)
        {
            return s.Strip("\"").Surround("\"");
        }

        /// <summary>
        /// Strips the specified string if it's surrounded on both sides by the specified value.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <param name="value">To strip.</param>
        /// <param name="comparisonType">Type of the comparison.</param>
        /// <returns></returns>
        public static string Strip(this string s, string value, StringComparison comparisonType = StringComparison.Ordinal)
        {
            if (s.StartsWith(value, comparisonType) && s.EndsWith(value, comparisonType) && s.Length >= (2 * value.Length))
            {
                s = s.Substring(value.Length);
                s = s.Substring(0, s.Length - value.Length);
            }

            return s;
        }

        /// <summary>
        /// Surrounds the string with the specified value.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string Surround(this string s, string value)
        {
            return string.Format("{0}{1}{0}", value, s);
        }
    }

    public static class DateTimes
    {
        public static string ToMMDDYYString(this DateTime? dateTime, string delimiter = "/")
        {
            return dateTime.ToString(string.Format("MM{0}dd{0}yy", delimiter));
        }

        public static string ToMMDDYYString(this DateTime dateTime, string delimiter = "/")
        {
            return dateTime.ToString(string.Format("MM{0}dd{0}yy", delimiter));
        }

        public static string ToMMDDYYYYString(this DateTime? dateTime, string delimiter = "/")
        {
            return dateTime.ToString(string.Format("MM{0}dd{0}yyyy", delimiter));
        }

        public static string ToMMDDYYYYString(this DateTime dateTime, string delimiter = "/")
        {
            return dateTime.ToString(string.Format("MM{0}dd{0}yyyy", delimiter));
        }

        public static string ToString(this DateTime? date)
        {
            return date.ToString(null, DateTimeFormatInfo.CurrentInfo);
        }
        public static string ToString(this DateTime? date, string format)
        {
            return date.ToString(format, DateTimeFormatInfo.CurrentInfo);
        }
        public static string ToString(this DateTime? date, IFormatProvider provider)
        {
            return date.ToString(null, provider);
        }
        public static string ToString(this DateTime? date, string format, IFormatProvider provider)
        {
            return date.HasValue ? date.Value.ToString(format, provider) : string.Empty;
        }
    }

    public static class Booleans
    {
        /// <summary>
        /// Returns "Y" if true, "N" if false
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static string ToYesNoSingleLetter(this bool arg)
        {
            return arg.ToString("Y", "N");
        }

        /// <summary>
        /// Returns "Yes" if true, "No" if false
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static string ToYesNo(this bool arg)
        {
            return arg.ToString("Yes", "No");
        }

        /// <summary>
        /// Returns returnIfTrue if true, else returns returnIfFalse
        /// </summary>
        /// <param name="arg"></param>
        /// <param name="returnIfTrue"></param>
        /// <param name="returnIfFalse"></param>
        /// <returns></returns>
        public static string ToString(this bool arg, string returnIfTrue, string returnIfFalse)
        {
            return arg ? returnIfTrue : returnIfFalse;
        }

        /// <summary>
        /// Converts the hex string to a byte array
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static byte[] HexStringToByteArray(this string s)
        {
            var numberChars = s.Length / 2;
            var bytes = new byte[numberChars];
            var sr = new StringReader(s);
            for (var i = 0; i < numberChars; i++)
                bytes[i] = Convert.ToByte(new string(new[] { (char)sr.Read(), (char)sr.Read() }), 16);
            sr.Dispose();
            return bytes;
        }
    }

    public static class ByteArrays
    {
        public static string ToHexString(this byte[] bytes)
        {
            var c = new char[bytes.Length * 2];
            int b;
            for (int i = 0; i < bytes.Length; i++)
            {
                b = bytes[i] >> 4;
                c[i * 2] = (char)(55 + b + (((b - 10) >> 31) & -7));
                b = bytes[i] & 0xF;
                c[i * 2 + 1] = (char)(55 + b + (((b - 10) >> 31) & -7));
            }
            return new string(c);
        }
    }


    /// <summary>
    ///   A generic EventArgs construct.
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    public class EventArgs<T> : EventArgs
    {
        public EventArgs(T value = default(T))
        {
            Value = value;
        }

        public T Value { get; set; }
    }

    /// <summary>
    /// EventArgs that can indicate whether the event is handled and some action should proceed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class HandledEventArgs<T> : EventArgs<T>
    {
        public HandledEventArgs(T value)
            : base(value)
        {
        }

        public bool Handled { get; set; }
    }

    /// <summary>
    /// EventArgs that can indicate whether the event is handled and some action should proceed.
    /// </summary>
    public class HandledEventArgs : EventArgs
    {
        public bool Handled { get; set; }
    }

    /// <summary>
    ///   A generic WeakReference.
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    public class WeakReference<T> where T : class
    {
        private readonly WeakReference weakReferenceInternal;

        public WeakReference(T target)
        {
            weakReferenceInternal = new WeakReference(target);
        }

        public T Target
        {
            get { return (T)weakReferenceInternal.Target; }
            set { weakReferenceInternal.Target = value; }
        }

        public bool IsAlive
        {
            get { return weakReferenceInternal.IsAlive; }
        }

        public override int GetHashCode()
        {
            object target = weakReferenceInternal.Target;
            return target != null ? target.GetHashCode() : -1;
        }

        public override bool Equals(object obj)
        {
            object target = weakReferenceInternal.Target;
            return target != null && target.Equals(obj);
        }
    }

    /// <summary>
    ///   Some utilities
    /// </summary>
    public static class OperatingSystems
    {
        #region IsOS Methods

        /// <summary>
        ///   Is current operative system XP or above?
        /// </summary>
        public static bool IsXpOrAbove(this OperatingSystem os)
        {
            if (os.Version.Major > 5)
                return true;
            return (os.Version.Major == 5) &&
                   (os.Version.Minor >= 1);
        }

        /// <summary>
        ///   Is current operative system Vista or above?
        /// </summary>
        public static bool IsVistaOrAbove(this OperatingSystem os)
        {
            if (os.Version.Major > 6)
                return true;
            return (os.Version.Major == 6) &&
                   (os.Version.Minor >= 0);
        }

        /// <summary>
        ///   Is current operative system Seven or above?
        /// </summary>
        public static bool IsSevenOrAbove(this OperatingSystem os)
        {
            if (os.Version.Major > 6)
                return true;
            return (os.Version.Major == 6) &&
                   (os.Version.Minor >= 1);
        }

        #endregion
    }
}

namespace Soaf.Reflection
{
    /// <summary>
    /// Performs string evaluations.
    /// </summary>
    public interface IEvaluator
    {
        /// <summary>
        /// Registers the type as safe for static invocations.
        /// </summary>
        /// <param name="type">The type.</param>
        void RegisterType(Type type);

        /// <summary>
        /// Unregisters the type as safe for static invocations.
        /// </summary>
        /// <param name="type">The type.</param>
        void UnregisterType(Type type);

        /// <summary>
        /// Evaluates the specified expression.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        object Evaluate(string expression, object target = null);
    }

    /// <summary>
    ///   Performs string evaluations.
    /// </summary>
    [Singleton]
    internal class LambdaEvaluator : IEvaluator
    {
        private readonly IDictionary<string, Func<object, object>> expressionCache = new Dictionary<string, Func<object, object>>().Synchronized();

        /// <summary>
        /// Registers the type as safe for static invocations.
        /// </summary>
        /// <param name="type">The type.</param>
        public void RegisterType(Type type)
        {
            ExpressionParser.PredefinedTypes[type] = type;
        }

        /// <summary>
        /// Unregisters the type as safe for static invocations.
        /// </summary>
        /// <param name="type">The type.</param>
        public void UnregisterType(Type type)
        {
            ExpressionParser.PredefinedTypes.Remove(type);
        }

        [DebuggerNonUserCode]
        public object Evaluate(string expression, object target = null)
        {
            var targetType = target == null ? typeof(object) : target.GetType();

            var key = "{0}_{1}".FormatWith(target == null ? "null" : target.GetType().AssemblyQualifiedName, expression);

            Func<object, object> compiledExpression;
            if (!expressionCache.TryGetValue(key, out compiledExpression))
            {
                var lambda = Linq.DynamicExpression.ParseLambda(targetType, typeof(object), expression);
                if (targetType != typeof(object))
                {
                    var parameter = Expression.Parameter(typeof(object), "i");
                    lambda = (LambdaExpression)Expression.Lambda(Expression.Invoke(lambda, Expression.Convert(parameter, targetType)), parameter).ExpandInvocations();
                }
                expressionCache[key] = compiledExpression = (Func<object, object>)lambda.Compile();
            }

            return compiledExpression(target);
        }
    }

    /// <summary>
    /// Singleton access to IEvaluator.
    /// </summary>
    public static class Evaluator
    {
        public static IEvaluator Current
        {
            get { return ServiceProvider.Current.GetService<IEvaluator>(); }
        }
    }

    /// <summary>
    /// Reflection helper and extension methods.
    /// </summary>
    public static class Reflector
    {
        #region PropertyMethod enum

        public enum PropertyMethod
        {
            Get,
            Set
        }

        #endregion

        private static readonly IDictionary<MemberInfo, Delegate> MemberDelegateCache = new Dictionary<MemberInfo, Delegate>().Synchronized();
        private static readonly IDictionary<AttributeCacheKey, IEnumerable<Attribute>> AttributeCache = new Dictionary<AttributeCacheKey, IEnumerable<Attribute>>().Synchronized();
        private static readonly IDictionary<MethodInfo, PropertyInfo> MethodPropertyCache = new Dictionary<MethodInfo, PropertyInfo>().Synchronized();

        public static Type FindTypeInAppDomain(string typeName)
        {
            if (typeName.IsNullOrWhiteSpace()) return null;

            var typeResolved = Type.GetType(typeName);
            if (typeResolved != null) return typeResolved;

            if (typeName.Contains(","))
            {
                typeName = typeName.Split(',')[0].Trim();

                typeResolved = Type.GetType(typeName);

                if (typeResolved != null) return typeResolved;
            }

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                typeResolved = assembly.GetType(typeName);
                if (typeResolved != null)
                {
                    return typeResolved;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the loadable types from the assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static Type[] GetLoadableTypes(this Assembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException("assembly");
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null).ToArray();
            }
        }

        /// <summary>
        /// Clears all property changed handlers listening to the PropertyChanged event.
        /// </summary>
        /// <param name="instance">The instance.</param>
        public static void ClearPropertyChangedHandlers(this INotifyPropertyChanged instance)
        {
            var propertyChangedField = instance.GetType().GetField("PropertyChanged", BindingFlags.NonPublic | BindingFlags.Instance);
            var propertyChangedEvent = instance.GetType().GetEvent("PropertyChanged", BindingFlags.NonPublic | BindingFlags.Instance);

            if (propertyChangedField != null && propertyChangedEvent != null)
            {
                var propertyChanged = propertyChangedField.GetValue(instance) as Delegate;
                if (propertyChanged != null)
                {
                    var privateRemoveMethod = propertyChangedEvent.GetRemoveMethod(true);
                    foreach (var invocation in propertyChanged.GetInvocationList())
                    {
                        privateRemoveMethod.Invoke(instance, new object[] { invocation });
                    }
                }
            }
        }

        /// <summary>
        /// Gets the key properties on the type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static IEnumerable<PropertyInfo> GetKeyProperties(this Type type)
        {
            return type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Where(p => p.IsKey());
        }

        /// <summary>
        /// Determines whether [is key property] [the specified property info]. Key properties are considered properties named: Id, ID, TypeNameId, TypeNameID, or that have a KeyAttribute.
        /// </summary>
        /// <param name="propertyInfo">The property info.</param>
        /// <returns>
        ///   <c>true</c> if [is key property] [the specified property info]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsKey(this PropertyInfo propertyInfo)
        {
            var type = propertyInfo.DeclaringType;
            if (type == null) throw new InvalidOperationException("Property {0} must have a type.".FormatWith(propertyInfo.Name));

            return
                propertyInfo.HasAttribute("System.ComponentModel.DataAnnotations.KeyAttribute") ||
                new[] { type.Name + "Id", "Id" }.Any(i => i.Equals(propertyInfo.Name, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Creates a wrapper delegate that calls the source and populates the results based on the specified initializer.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="initializer">The initializer.</param>
        /// <returns></returns>
        public static Func<T> InitializeWith<T>(this Func<T> source, Expression<Func<T>> initializer)
        {
            Expression body = initializer.Body;

            if (body is UnaryExpression)
            {
                body = ((UnaryExpression)body).Operand;
            }

            var memberInitExpression = body as MemberInitExpression;

            if (memberInitExpression == null) throw new InvalidOperationException("Selector must be a new { ... } expression.");

            return () =>
                       {
                           var result = source();

                           foreach (var binding in memberInitExpression.Bindings.OfType<MemberAssignment>())
                           {
                               var value = Expression.Lambda(binding.Expression).Compile().DynamicInvoke();

                               var f = binding.Member as FieldInfo;
                               if (f != null)
                               {
                                   f.SetValue(result, value);
                                   continue;
                               }
                               var p = binding.Member as PropertyInfo;
                               if (p != null && p.GetIndexParameters().Length == 0)
                               {
                                   p.SetValue(result, value, null);
                               }
                           }

                           return result;
                       };
        }

        /// <summary>
        /// Tries to get the entry assembly.
        /// </summary>
        /// <returns></returns>
        [DebuggerNonUserCode]
        internal static Assembly TryGetEntryAssembly()
        {
            try
            {
                return Assembly.GetEntryAssembly();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Binds a generic delegate to an event.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="handlerInstance">The handler instance.</param>
        /// <param name="eventName">Name of the event.</param>
        /// <param name="eventHandler">The event handler.</param>
        /// <returns></returns>
        public static KeyValuePair<EventInfo, Delegate> BindToEvent(object target, Type targetType, object handlerInstance, string eventName, Action<string, object, object> eventHandler)
        {
            EventInfo eventInfo;
            if ((targetType != null))
            {
                eventInfo = targetType.GetEvent(eventName);
            }
            else
            {
                eventInfo = target.GetType().GetEvent(eventName);
            }
            var handlerInvoke = eventInfo.EventHandlerType.GetMethod("Invoke");
            var handlerParams = handlerInvoke.GetParameters().Select(item => item.ParameterType).ToList();
            handlerParams.Insert(0, handlerInstance.GetType());

            var handlerName = string.Format("{0}_{1}", eventInfo.EventHandlerType, eventHandler.GetType());
            var handlerMethod = new DynamicMethod(handlerName, typeof(void), handlerParams.ToArray(), handlerInstance.GetType());
            var generator = handlerMethod.GetILGenerator();
            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldstr, eventInfo.Name);
            generator.Emit(OpCodes.Ldarg_1);
            generator.Emit(OpCodes.Ldarg_2);
            generator.Emit(OpCodes.Callvirt, eventHandler.Method);
            generator.Emit(OpCodes.Ret);

            var @delegate = handlerMethod.CreateDelegate(eventInfo.EventHandlerType, handlerInstance);
            eventInfo.AddEventHandler(target, @delegate);

            return new KeyValuePair<EventInfo, Delegate>(eventInfo, @delegate);
        }

        /// <summary>
        /// Gets the display name based on attributes.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <returns></returns>
        public static string GetDisplayName(this ICustomAttributeProvider provider)
        {
            var attributes = provider.GetAttributes();
            return
                attributes.Where(a => a.GetType().FullName == "System.ComponentModel.DisplayNameAttribute").Select(i => i.CastTo<dynamic>().DisplayName).FirstOrDefault() ??
                attributes.Where(a => a.GetType().FullName == "System.ComponentModel.DataAnnotations.DisplayAttribute").Select(i => i.CastTo<dynamic>().Name).FirstOrDefault() ??
                attributes.OfType<DescriptionAttribute>().Select(i => i.Description).FirstOrDefault() ??
                attributes.Where(a => a.GetType().FullName == "System.Runtime.Serialization.DataMemberAttribute").Select(i => i.CastTo<dynamic>().Name).FirstOrDefault() ??
                (provider is MemberInfo ? provider.CastTo<MemberInfo>().Name : provider.ToString());
        }

        /// <summary>
        /// Clones the specified value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static T Clone<T>(this T value)
        {
            var valueType = (object)value == null ? typeof(object) : value.GetType();
            return (T)ServiceProvider.Current.GetService<ICloner>().Clone(value, valueType, valueType);
        }

        /// <summary>
        /// Changes type of specified value via cloning
        /// </summary>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TTarget ChangeType<TTarget>(this object value)
        {
            var valueType = value == null ? typeof(object) : value.GetType();
            return (TTarget)ServiceProvider.Current.GetService<ICloner>().Clone(value, typeof(TTarget), valueType);
        }

        /// <summary>
        /// Gets the method and overriden methods.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <returns></returns>
        public static IEnumerable<MethodInfo> GetMethodAndOverridenMethods(this MethodInfo method)
        {
            return new[] { method }.Concat(method.GetOverridenMethods());
        }

        /// <summary>
        /// Gets the overriden methods.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <returns></returns>
        public static IEnumerable<MethodInfo> GetOverridenMethods(this MethodInfo method)
        {
            while (method != null)
            {
                var current = method.GetBaseDefinition();
                if (current != null && current != method)
                {
                    method = current;
                    yield return current;
                }
                else
                {
                    method = null;
                }
            }
        }

        /// <summary>
        ///   Gets the members of the type. If type is an interface and includeInterfaceMembersIfInterface, this method will walk up the chain and include inherited interfaces.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "includeInterfaceMembersIfInterface">if set to <c>true</c> [include interface members if interface].</param>
        /// <param name = "bindingFlags">The binding flags.</param>
        /// <returns></returns>
        internal static IEnumerable<MemberInfo> GetMembers(this Type type, bool includeInterfaceMembersIfInterface, BindingFlags bindingFlags = BindingFlags.Default)
        {
            return type.IsInterface && includeInterfaceMembersIfInterface ? type.GetMembers(bindingFlags).Concat(type.GetInterfaces().Distinct().SelectMany(i => i.GetMembers(true, bindingFlags))) : type.GetMembers(bindingFlags);
        }

        /// <summary>
        /// A strongly typed means of reflecting a MethodInfo or PropertyInfo. Returns a MethodInfo or PropertyInfo for the member expression specified.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="memberAccess">The member access.</param>
        /// <returns></returns>
        public static MemberInfo GetMember<T>(Expression<Func<T, object>> memberAccess)
        {
            return GetMember((LambdaExpression)memberAccess);
        }

        /// <summary>
        ///   A strongly typed means of reflecting a MethodInfo or PropertyInfo. Returns a MethodInfo or PropertyInfo for the member expression specified.
        /// </summary>
        /// <param name = "memberAccess">The member access.</param>
        /// <returns></returns>
        public static MemberInfo GetMember(Expression<Action> memberAccess)
        {
            return GetMember((LambdaExpression)memberAccess);
        }

        /// <summary>
        ///   A strongly typed means of reflecting a MethodInfo or PropertyInfo. Returns a MethodInfo or PropertyInfo for the member expression specified.
        /// </summary>
        /// <param name = "memberAccess">The member access.</param>
        /// <returns></returns>
        public static MemberInfo GetMember<TResult>(Expression<Func<TResult>> memberAccess)
        {
            return GetMember((LambdaExpression)memberAccess);
        }

        /// <summary>
        ///   A strongly typed means of reflecting a MethodInfo or PropertyInfo. Returns a MethodInfo or PropertyInfo for the member expression specified.
        /// </summary>
        /// <param name = "memberAccess">The member access.</param>
        /// <returns></returns>
        public static MemberInfo GetMember<TType>(Expression<Action<TType>> memberAccess)
        {
            return GetMember((LambdaExpression)memberAccess);
        }

        /// <summary>
        ///   A strongly typed means of reflecting a MethodInfo or PropertyInfo. Returns a MethodInfo or PropertyInfo for the member expression specified.
        /// </summary>
        /// <param name = "memberAccess">The member access.</param>
        /// <returns></returns>
        public static MemberInfo GetMember<TType, TMember>(Expression<Func<TType, TMember>> memberAccess)
        {
            return GetMember((LambdaExpression)memberAccess);
        }

        /// <summary>
        ///   A strongly typed means of reflecting a MethodInfo or PropertyInfo. Returns a MethodInfo or PropertyInfo for the member expression specified.
        /// </summary>
        /// <param name = "memberAccess">The member access.</param>
        /// <returns></returns>
        internal static MemberInfo GetMember(LambdaExpression memberAccess)
        {
            switch (memberAccess.Body.NodeType)
            {
                case ExpressionType.MemberAccess:
                    return memberAccess.Body.CastTo<MemberExpression>().Member;
                case ExpressionType.Call:
                    return memberAccess.Body.CastTo<MethodCallExpression>().Method;
                case ExpressionType.Convert: /* Supporting the value types boxing to object */
                    return memberAccess.Body.CastTo<UnaryExpression>().Operand.CastTo<MemberExpression>().Member;
                default:
                    throw new InvalidOperationException("memberAccess must be a simple method call or member access expression.");
            }
        }

        /// <summary>
        ///   Gets the methods for the type, optionally including non public methods.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "includeNonPublic">if set to <c>true</c> [include non public].</param>
        /// <returns></returns>
        internal static MethodInfo[] GetMethods(this Type type, bool includeNonPublic)
        {
            if (!includeNonPublic)
            {
                return type.GetMethods();
            }
            return type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        }

        /// <summary>
        ///   Gets the parameter types for the specified method.
        /// </summary>
        /// <param name = "method">The method.</param>
        /// <returns></returns>
        internal static IEnumerable<Type> GetParameterTypes(this MethodBase method)
        {
            return method.GetParameters().Select(p => p.ParameterType);
        }

        /// <summary>
        ///   Determines whether this method can be overridden in a derived class.
        /// </summary>
        /// <param name = "methodInfo">The member info.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can be overidden] the specified member info; otherwise, <c>false</c>.
        /// </returns>
        internal static bool CanBeOveridden(this MethodInfo methodInfo)
        {
            return (methodInfo.IsVirtual || methodInfo.IsAbstract) && !methodInfo.IsFinal && (methodInfo.IsFamily || methodInfo.IsPublic);
        }

        /// <summary>
        ///   Determines if the methods are equal or if any of the base definitions for the method are equal.
        /// </summary>
        /// <param name = "first">The first.</param>
        /// <param name = "second">The second.</param>
        /// <returns></returns>
        internal static bool AreMethodsEqualForDeclaringType(this MethodInfo first, MethodInfo second)
        {
            if (first == null && second == null)
            {
                return true;
            }
            if (first == null || second == null)
            {
                return false;
            }
            first = first.ReflectedType == first.DeclaringType ? first : first.DeclaringType.EnsureNotDefault("MethodInfo should have a declaring type.").GetMethod(first.Name, first.GetParameters().Select(p => p.ParameterType).ToArray());
            second = second.ReflectedType == second.DeclaringType ? second : second.DeclaringType.EnsureNotDefault("MethodInfo should have a declaring type.").GetMethod(second.Name, second.GetParameters().Select(p => p.ParameterType).ToArray());
            return first == second;
        }

        /// <summary>
        ///   Determines whether the specified methodInfo is an implementation for the specified interface type.
        /// </summary>
        /// <param name = "methodInfo">The method info.</param>
        /// <param name = "interfaceType">Type of the interface.</param>
        /// <returns>
        ///   <c>true</c> if [is method implementation for interface] [the specified method info]; otherwise, <c>false</c>.
        /// </returns>
        internal static bool IsMethodImplementationForInterface(this MethodInfo methodInfo, Type interfaceType)
        {
            return methodInfo.DeclaringType == interfaceType || (!methodInfo.DeclaringType.EnsureNotDefault("MethodInfo should have a declaring type.").IsInterface && interfaceType.IsAssignableFrom(methodInfo.DeclaringType) && methodInfo.DeclaringType.GetInterfaceMap(interfaceType).TargetMethods.Contains(methodInfo));
        }

        /// <summary>
        ///   Determines whether the specified methodInfo is an implementation for the specified interface method.
        /// </summary>
        /// <param name = "methodInfo">The method info.</param>
        /// <param name = "interfaceMethod">The interface method.</param>
        /// <returns>
        ///   <c>true</c> if [is method implementation for interface] [the specified method info]; otherwise, <c>false</c>.
        /// </returns>
        internal static bool IsMethodImplementationForInterfaceMethod(this MethodInfo methodInfo, MethodInfo interfaceMethod)
        {
            bool result = methodInfo.Equals(interfaceMethod);
            if (!result && interfaceMethod.DeclaringType.EnsureNotDefault("MethodInfo should have a declaring type.").IsInterface && !methodInfo.DeclaringType.EnsureNotDefault("MethodInfo should have a declaring type.").IsInterface && methodInfo.DeclaringType.Is(interfaceMethod.DeclaringType))
            {
                InterfaceMapping map = methodInfo.DeclaringType.GetInterfaceMap(interfaceMethod.DeclaringType);
                if (map.TargetMethods.ToList().IndexOf(methodInfo) == map.InterfaceMethods.ToList().IndexOf(interfaceMethod))
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the first attribute of the specified type on the specified provider.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="customAttributeProvider">The member info.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <param name="includeInterfaces">if set to <c>true</c> [include interfaces].</param>
        /// <returns>
        ///   <c>true</c> if the specified member info has attribute; otherwise, <c>false</c>.
        /// </returns>
        public static TAttribute GetAttribute<TAttribute>(this ICustomAttributeProvider customAttributeProvider, bool inherit = true, bool includeInterfaces = true) where TAttribute : Attribute
        {
            return customAttributeProvider.GetAttributes<TAttribute>(inherit).FirstOrDefault();
        }

        /// <summary>
        ///   Gets the first attribute of the specified type on the specified provider.
        /// </summary>
        /// <param name = "customAttributeProvider">The member info.</param>
        /// <param name = "attributeType">Type of the attribute.</param>
        /// <param name = "inherit">if set to <c>true</c> [inherit].</param>
        /// <param name = "?">The ?.</param>
        /// <param name = "includeInterfaces">if set to <c>true</c> [include interfaces].</param>
        /// <returns>
        ///   <c>true</c> if the specified member info has attribute; otherwise, <c>false</c>.
        /// </returns>
        public static Attribute GetAttribute(this ICustomAttributeProvider customAttributeProvider, Type attributeType, bool inherit = true, bool includeInterfaces = true)
        {
            return customAttributeProvider.GetAttributes(attributeType, inherit, includeInterfaces).FirstOrDefault();
        }

        /// <summary>
        ///   Gets all attributes of the specified type on the specified provider.
        /// </summary>
        /// <typeparam name = "TAttribute">The type of the attribute.</typeparam>
        /// <param name = "customAttributeProvider">The member info.</param>
        /// <param name = "inherit">if set to <c>true</c> [inherit].</param>
        /// <returns>
        ///   <c>true</c> if the specified member info has attribute; otherwise, <c>false</c>.
        /// </returns>
        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this ICustomAttributeProvider customAttributeProvider, bool inherit = true) where TAttribute : Attribute
        {
            return customAttributeProvider.GetAttributes(typeof(TAttribute), inherit).OfType<TAttribute>();
        }

        /// <summary>
        ///   Gets all attributes of the specified type on the specified provider with the specified criteria.
        /// </summary>
        /// <param name = "customAttributeProvider">The member info.</param>
        /// <param name = "attributeType">Type of the attribute.</param>
        /// <param name = "inherit">if set to <c>true</c> [inherit].</param>
        /// <param name = "includeInterfaces">if set to <c>true</c> [include interfaces].</param>
        /// <returns>
        ///   <c>true</c> if the specified member info has attribute; otherwise, <c>false</c>.
        /// </returns>
        public static IEnumerable<Attribute> GetAttributes(this ICustomAttributeProvider customAttributeProvider, Type attributeType = null, bool inherit = true, bool includeInterfaces = true)
        {
            if (customAttributeProvider == null) return new Attribute[0];

            IEnumerable<Attribute> result;

            AttributeCacheKey key = null;

            if (attributeType != null && attributeType != typeof(Attribute))
            {
                // only cache specific attribute type checks, otherwise the cache will be huge

                key = new AttributeCacheKey(customAttributeProvider, attributeType, inherit, includeInterfaces);
                if (AttributeCache.TryGetValue(key, out result))
                {
                    return result;
                }
            }

            if (attributeType == null) attributeType = typeof(Attribute);

            if (customAttributeProvider is Type)
            {
                result = GetAttributesInternal((Type)customAttributeProvider, attributeType, inherit, includeInterfaces);
            }
            else if (customAttributeProvider is PropertyInfo)
            {
                result = GetAttributesInternal((PropertyInfo)customAttributeProvider, attributeType, inherit, includeInterfaces);
            }
            else if (customAttributeProvider is MethodInfo)
            {
                result = GetAttributesInternal((MethodInfo)customAttributeProvider, attributeType, inherit, includeInterfaces);
            }
            else if (customAttributeProvider is EventInfo)
            {
                result = GetAttributesInternal((EventInfo)customAttributeProvider, attributeType, inherit, includeInterfaces);
            }
            else if (customAttributeProvider is MemberInfo)
            {
                result = Attribute.GetCustomAttributes((MemberInfo)customAttributeProvider, attributeType, inherit);
            }
            else if (customAttributeProvider is ParameterInfo)
            {
                result = GetAttributesInternal((ParameterInfo)customAttributeProvider, attributeType, inherit, includeInterfaces);
            }
            else if (customAttributeProvider is Assembly)
            {
                result = Attribute.GetCustomAttributes((Assembly)customAttributeProvider, attributeType, inherit);
            }
            else if (customAttributeProvider is Module)
            {
                result = Attribute.GetCustomAttributes((Module)customAttributeProvider, attributeType, inherit);
            }
            else
            {
                result = customAttributeProvider.GetCustomAttributes(attributeType, inherit).OfType<Attribute>();
            }

            if (key != null)
            {
                AttributeCache[key] = result.ToArray();
            }

            return result;
        }

        /// <summary>
        /// Gets all attributes of the specified type on the specified provider with the specified criteria.
        /// </summary>
        /// <param name="customAttributeProvider">The custom attribute provider.</param>
        /// <param name="attributeTypeName">Name of the attribute type.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <param name="includeInterfaces">if set to <c>true</c> [include interfaces].</param>
        /// <returns></returns>
        public static IEnumerable<Attribute> GetAttributes(this ICustomAttributeProvider customAttributeProvider, string attributeTypeName, bool inherit = true, bool includeInterfaces = true)
        {
            return customAttributeProvider.GetAttributes(inherit: inherit, includeInterfaces: includeInterfaces).Where(a => a.GetType().FullName == attributeTypeName);
        }

        private static IEnumerable<Attribute> GetAttributesInternal(Type type, Type attributeType, bool inherit, bool includeInterfaces)
        {
            var result = (IEnumerable<Attribute>)Attribute.GetCustomAttributes(type, attributeType, inherit);

            var metadataType = type.GetMetadataType();

            if (metadataType != type) result = result.Concat(GetAttributesInternal(metadataType, attributeType, inherit, includeInterfaces));

            if (includeInterfaces && !type.IsInterface)
            {
                foreach (var i in type.GetInterfaces())
                {
                    result = result.Concat(GetAttributesInternal(i, attributeType, inherit, false).Where(a => a.Inherited()));
                }
            }

            return result;
        }

        private static IEnumerable<Attribute> GetAttributesInternal(PropertyInfo propertyInfo, Type attributeType, bool inherit, bool includeInterfaces)
        {
            var result = (IEnumerable<Attribute>)Attribute.GetCustomAttributes(propertyInfo, attributeType, inherit);

            var metadataMember = propertyInfo.GetMetadataMember();
            if (metadataMember != propertyInfo) result = result.Concat(GetAttributesInternal(metadataMember, attributeType, inherit, includeInterfaces));

            if (includeInterfaces && propertyInfo.DeclaringType != null && !propertyInfo.DeclaringType.IsInterface)
            {
                var type = propertyInfo.DeclaringType;

                foreach (var i in type.GetInterfaces())
                {
                    var map = type.GetInterfaceMap(i);

                    var memberIndex = propertyInfo.GetGetMethod().GetMethodAndOverridenMethods().Concat(propertyInfo.GetSetMethod().GetMethodAndOverridenMethods()).Select(m => map.TargetMethods.IndexOf(m)).Max();

                    if (memberIndex < 0 || memberIndex >= map.InterfaceMethods.Length) continue;

                    var interfaceMember = map.InterfaceMethods[memberIndex];

                    var interfaceProperty = interfaceMember.GetProperty();

                    if (interfaceProperty == null) continue;

                    result = result.Concat(GetAttributesInternal(interfaceProperty, attributeType, inherit, false).Where(a => a.Inherited()));
                }
            }

            return result;
        }

        private static IEnumerable<Attribute> GetAttributesInternal(MethodInfo methodInfo, Type attributeType, bool inherit, bool includeInterfaces)
        {
            var result = (IEnumerable<Attribute>)Attribute.GetCustomAttributes(methodInfo, attributeType, inherit);

            if (includeInterfaces && methodInfo.DeclaringType != null && !methodInfo.DeclaringType.IsInterface)
            {
                var type = methodInfo.DeclaringType;

                foreach (var i in type.GetInterfaces())
                {
                    var map = type.GetInterfaceMap(i);

                    var memberIndex = methodInfo.GetMethodAndOverridenMethods().Select(m => map.TargetMethods.IndexOf(methodInfo)).Max();

                    if (memberIndex < 0 || memberIndex >= map.InterfaceMethods.Length) continue;

                    var interfaceMember = map.InterfaceMethods[memberIndex];

                    result = result.Concat(GetAttributesInternal(interfaceMember, attributeType, inherit, false).Where(a => a.Inherited()));
                }
            }

            return result;
        }

        private static IEnumerable<Attribute> GetAttributesInternal(EventInfo eventInfo, Type attributeType, bool inherit, bool includeInterfaces)
        {
            var result = (IEnumerable<Attribute>)Attribute.GetCustomAttributes(eventInfo, attributeType, inherit);

            if (includeInterfaces && eventInfo.DeclaringType != null && !eventInfo.DeclaringType.IsInterface)
            {
                var type = eventInfo.DeclaringType;

                foreach (var i in type.GetInterfaces())
                {
                    var map = type.GetInterfaceMap(i);

                    var memberIndex = eventInfo.GetAddMethod().GetMethodAndOverridenMethods().Concat(eventInfo.GetRemoveMethod().GetMethodAndOverridenMethods()).Select(m => map.TargetMethods.IndexOf(m)).Max();

                    if (memberIndex < 0 || memberIndex >= map.InterfaceMethods.Length) continue;

                    var interfaceMember = map.InterfaceMethods[memberIndex];

                    var interfaceEvent = i.GetEvents().FirstOrDefault(e => e.GetAddMethod() == interfaceMember || e.GetRemoveMethod() == interfaceMember);

                    if (interfaceEvent == null) continue;

                    result = result.Concat(GetAttributesInternal(interfaceEvent, attributeType, inherit, false).Where(a => a.Inherited()));
                }
            }

            return result;
        }

        private static IEnumerable<Attribute> GetAttributesInternal(ParameterInfo parameterInfo, Type attributeType, bool inherit, bool includeInterfaces)
        {
            // ReSharper disable once ConstantNullCoalescingCondition
            var result = (IEnumerable<Attribute>)Attribute.GetCustomAttributes(parameterInfo, attributeType, inherit) ?? new Attribute[0];

            if (includeInterfaces && parameterInfo.Member.DeclaringType != null && !parameterInfo.Member.DeclaringType.IsInterface && parameterInfo.Member is MethodInfo)
            {
                var parameterIndex = ((MethodInfo)parameterInfo.Member).GetParameters().IndexOf(parameterInfo);

                if (parameterIndex < 0) return result;

                var type = parameterInfo.Member.DeclaringType;

                foreach (var i in type.GetInterfaces())
                {
                    var map = type.GetInterfaceMap(i);

                    var parameterMethod = (MethodInfo)parameterInfo.Member;

                    var memberIndex = parameterMethod.GetMethodAndOverridenMethods().Select(m => map.TargetMethods.IndexOf(m)).Max();

                    if (memberIndex < 0 || memberIndex >= map.InterfaceMethods.Length) continue;

                    var interfaceMember = map.InterfaceMethods[memberIndex];

                    var interfaceParameters = interfaceMember.GetParameters();

                    if (parameterIndex >= interfaceParameters.Length) continue;

                    var interfaceParameter = interfaceParameters[parameterIndex];

                    result = result.Concat(GetAttributesInternal(interfaceParameter, attributeType, inherit, false).Where(a => a.Inherited()));
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the data members for the specified type (as specified by all public instance PropertyInfos, or DataMemberAttribute annotated members).
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static IEnumerable<DataMember> GetDataMembers(this Type type)
        {
            IEnumerable<DataMember> results;

            if (type.HasDataContract())
            {
                results = type.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Select(m => new { Attribute = m.GetAttributes("System.Runtime.Serialization.DataMemberAttribute").FirstOrDefault(), Member = m })
                    .Where(i => i.Attribute != null)
                    .OrderBy(i => (int)((dynamic)i.Attribute).Order).ThenBy(i => ((dynamic)i.Attribute).Name)
                    .Select(i => new DataMember((string)((dynamic)i.Attribute).Name ?? i.Member.Name, i.Member, (int)((dynamic)i.Attribute).Order)).ToArray();
            }
            else
            {
                results = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead && p.CanWrite && p.GetIndexParameters().Length == 0).Select(i => new DataMember(i.Name, i)).ToArray();
            }
            foreach (var result in results)
            {
                if (result.Order <= 0)
                {
                    var withPositiveOrder = results.Where(r => r.Order > 0).ToArray();
                    result.Order = withPositiveOrder.Any() ? withPositiveOrder.Max(r => r.Order) + 1 : 1;
                }
            }
            return results;
        }

        /// <summary>
        /// Returns true if the attribute is inherited
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        /// <returns></returns>
        public static bool Inherited(this Attribute attribute)
        {
            return attribute.GetAttributeUsage().IfNotNull(au => au.Inherited, () => true);
        }

        /// <summary>
        ///   Gets the AttributeUsageAttribute for the specified attribute.
        /// </summary>
        /// <param name = "attribute">The attribute.</param>
        /// <returns></returns>
        public static AttributeUsageAttribute GetAttributeUsage(this Attribute attribute)
        {
            return attribute.GetType().GetCustomAttributes(typeof(AttributeUsageAttribute), true).OfType<AttributeUsageAttribute>().FirstOrDefault();
        }

        /// <summary>
        /// Gets the member to use for describing member metadata using the MetadataTypeAttribute.
        /// </summary>
        /// <param name="member">The member.</param>
        /// <returns></returns>
        public static T GetMetadataMember<T>(this T member) where T : MemberInfo
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (member == null || member.DeclaringType == null) return member;
            // ReSharper restore ConditionIsAlwaysTrueOrFalse

            var metadataType = member.DeclaringType.GetMetadataType();

            T metadataMember = (T)metadataType.GetMember(member.Name).FirstOrDefault();

            return metadataMember ?? member;
        }

        public static string MemberPath<T>(this Expression<Func<T, object>> selector)
        {
            return Expressions.ToMemberAccessStrings(selector.Body).FirstOrDefault();
        }

        /// <summary>
        /// Gets the type to use for attribute metadata (based on MetadataTypeAttribute).
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Type GetMetadataType(this Type type)
        {
            var metadataTypeAttribute = (MetadataTypeAttribute)type.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
            if (metadataTypeAttribute == null) return type;

            var metadataType = metadataTypeAttribute.MetadataClassType;
            return metadataType;
        }

        /// <summary>
        ///   Gets the namespace for the member. If it is a type, returns the type namespace. If it is a member with a declaring type, it returns the delcaring type's namespace.
        /// </summary>
        /// <param name = "memberInfo">The member info.</param>
        /// <returns></returns>
        internal static string GetNamespace(this MemberInfo memberInfo)
        {
            if (memberInfo != null)
            {
                Type type = memberInfo is Type ? (Type)memberInfo : memberInfo.DeclaringType.EnsureNotDefault("MemberInfo should have a declaring type.");

                return type.Namespace;
            }
            return null;
        }

        /// <summary>
        ///   Gets the associated PropertyInfo for a property's get or set method.
        /// </summary>
        /// <param name = "method">The method.</param>
        /// <returns></returns>
        internal static PropertyInfo GetProperty(this MethodInfo method)
        {
            if (!method.IsSpecialName || !(method.Name.StartsWith("get_") || method.Name.StartsWith("set_"))) return null;

            var result = MethodPropertyCache.GetValue(method, () =>
                {
                    const int getterOrSetterLength = 4;

                    // Properties have either return value or single input parameter -> check it
                    bool takesArg = method.GetParameters().Length == 1;
                    bool hasReturn = method.ReturnType != typeof(void);

                    PropertyInfo propertyInfo = null;
                    if (takesArg != hasReturn)
                    {
                        propertyInfo = method.Name.Length <= getterOrSetterLength
                                     ? null
                                     : method.DeclaringType.EnsureNotDefault("MethodInfo should have a declaring type.")
                                             .GetProperty(method.Name.Substring(getterOrSetterLength), BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                    }

                    return propertyInfo;
                });

            return result;
        }

        /// <summary>
        ///   Determines whether the specified provider has the attribute type.
        /// </summary>
        /// <param name = "customAttributeProvider">The member info.</param>
        /// <param name = "attributeType">Type of the attribute.</param>
        /// <param name = "includeInterfaces">if set to <c>true</c> [include interfaces].</param>
        /// <param name = "inherit">if set to <c>true</c> [inherit].</param>
        /// <returns>
        ///   <c>true</c> if the specified member info has attribute; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasAttribute(this ICustomAttributeProvider customAttributeProvider, Type attributeType, bool inherit = true, bool includeInterfaces = true)
        {
            return customAttributeProvider.GetAttribute(attributeType, inherit, includeInterfaces) != null;
        }

        /// <summary>
        /// Determines whether the specified provider has the attribute type.
        /// </summary>
        /// <param name="customAttributeProvider">The member info.</param>
        /// <param name="attributeTypeName">Name of the attribute type.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <param name="includeInterfaces">if set to <c>true</c> [include interfaces].</param>
        /// <returns>
        ///   <c>true</c> if the specified member info has attribute; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasAttribute(this ICustomAttributeProvider customAttributeProvider, string attributeTypeName, bool inherit = true, bool includeInterfaces = true)
        {
            return customAttributeProvider.GetAttributes(inherit: inherit, includeInterfaces: includeInterfaces).Any(a => a.GetType().FullName == attributeTypeName);
        }

        /// <summary>
        ///   Determines whether the specified provider has the attribute type.
        /// </summary>
        /// <typeparam name = "TAttribute">The type of the attribute.</typeparam>
        /// <param name = "customAttributeProvider">The member info.</param>
        /// <param name = "inherit">if set to <c>true</c> [inherit].</param>
        /// <param name = "includeInterfaces">if set to <c>true</c> [include interfaces].</param>
        /// <returns>
        ///   <c>true</c> if the specified member info has attribute; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasAttribute<TAttribute>(this ICustomAttributeProvider customAttributeProvider, bool inherit = true, bool includeInterfaces = true) where TAttribute : Attribute
        {
            return customAttributeProvider.HasAttribute(typeof(TAttribute), inherit, includeInterfaces);
        }

        /// <summary>
        ///   Gets the underlying method for the member. If a Method, returns the method itself. If a Property, returns the Get method.
        /// </summary>
        /// <param name = "memberInfo">The member info.</param>
        /// <param name = "propertyMethodType">Type of the property method.</param>
        /// <returns></returns>
        internal static MethodInfo GetUnderlyingMethod(this MemberInfo memberInfo, PropertyMethod propertyMethodType = PropertyMethod.Get)
        {
            if (memberInfo.MemberType == MemberTypes.Property)
            {
                var propertyInfo = (PropertyInfo)memberInfo;
                if (propertyMethodType == PropertyMethod.Get)
                {
                    if (propertyInfo.CanRead)
                    {
                        return propertyInfo.GetGetMethod(true);
                    }
                }
                if (propertyMethodType == PropertyMethod.Set)
                {
                    if (propertyInfo.CanWrite)
                    {
                        return propertyInfo.GetSetMethod(true);
                    }
                }
                return null;
            }
            if (memberInfo.MemberType == MemberTypes.Method)
            {
                return (MethodInfo)memberInfo;
            }

            return null;
        }

        /// <summary>
        ///   Creates an instance of the specified type
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "arguments">The arguments.</param>
        /// <returns></returns>
        public static object CreateInstance(this Type type, params object[] arguments)
        {
            return type.GetInstantiator(arguments)(arguments);
        }

        /// <summary>
        ///   Gets an instantiator delegate for the specified type accepting the specified arguments.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "arguments">The arguments.</param>
        /// <returns></returns>
        public static Instantiator GetInstantiator(this Type type, object[] arguments)
        {
            return type.GetInstantiator(arguments.Select(a => a != null ? a.GetType() : null).ToArray());
        }

        /// <summary>
        ///   Gets an instantiator delegate for the specified type accepting the specified argument types.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "argumentTypes">The argument types.</param>
        /// <returns></returns>
        public static Instantiator GetInstantiator(this Type type, Type[] argumentTypes)
        {
            if (argumentTypes.IsNullOrEmpty()) return GetInstantiator(type);

            ConstructorInfo[] constructors = type.GetConstructors(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            var constructorInfo = constructors.AsEnumerable<MethodBase>().FindBestMethodMatch(argumentTypes).As<ConstructorInfo>();
            if (constructorInfo == null)
            {
                throw new MissingMethodException("No applicable constructor found for {0} with argument types: [{1}].".FormatWith(type.FullName, argumentTypes.Select(t => t.FullName).Join()));
            }
            return constructorInfo.GetInstantiator();
        }

        /// <summary>
        ///   Gets an instantiator delegate for the specified type. Uses default constructor if available, otherwise returns default(type).
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        public static Instantiator GetInstantiator(this Type type)
        {
            Delegate instantiator;
            MemberDelegateCache.TryGetValue(type, out instantiator);
            if (instantiator == null)
            {
                instantiator = type.CreateInstantiator();
                MemberDelegateCache[type] = instantiator;
            }
            return (Instantiator)instantiator;
        }

        /// <summary>
        ///   Create an instantiator delegate for the specified type. Uses default constructor if available, otherwise returns default(type).
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        internal static Instantiator CreateInstantiator(this Type type)
        {
            return type.CreateInstantiatorLambdaExpression().Compile();
        }

        /// <summary>
        ///   Creates a lambda expression for an instantiator delegate for the specified type. Uses default constructor if available, otherwise returns default(type).
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        internal static Expression<Instantiator> CreateInstantiatorLambdaExpression(this Type type)
        {
            ConstructorInfo constructorInfo = type.GetConstructor(Type.EmptyTypes);
            if (constructorInfo != null)
            {
                return constructorInfo.CreateInstantiatorLambdaExpression();
            }
            if (type.IsValueType)
            {
                ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]));

                DefaultExpression defaultExpression = Expression.Default(type);

                Expression<Instantiator> lambda = Expression.Lambda<Instantiator>(
                    Expression.Convert(defaultExpression, typeof(object)), argumentsParameter);

                return lambda;
            }

            throw new MissingMethodException("No default constructor found for {0}.".FormatWith(type.FullName));
        }

        /// <summary>
        ///   Gets an instantiator delegate for the specified constructor.
        /// </summary>
        /// <param name = "constructorInfo">The constructor info.</param>
        /// <returns></returns>
        public static Instantiator GetInstantiator(this ConstructorInfo constructorInfo)
        {
            Delegate instantiator;
            MemberDelegateCache.TryGetValue(constructorInfo, out instantiator);
            if (instantiator == null)
            {
                instantiator = constructorInfo.CreateInstantiator();
                MemberDelegateCache[constructorInfo] = instantiator;
            }
            return (Instantiator)instantiator;
        }

        /// <summary>
        ///   Creates a fast instantiator delegate for the specified constructor.
        /// </summary>
        /// <param name = "constructorInfo">The constructor info.</param>
        /// <returns></returns>
        internal static Instantiator CreateInstantiator(this ConstructorInfo constructorInfo)
        {
            return constructorInfo.CreateInstantiatorLambdaExpression().Compile();
        }

        /// <summary>
        ///   Creates a fast instantiator expression for the specified constructor.
        /// </summary>
        /// <param name = "constructorInfo">The constructor info.</param>
        /// <returns></returns>
        internal static Expression<Instantiator> CreateInstantiatorLambdaExpression(this ConstructorInfo constructorInfo)
        {
            ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]));

            UnaryExpression[] argumentsAccessors = constructorInfo.GetParameters().Select((parameter, index) =>
                                                                                          Expression.Convert(
                                                                                              Expression.ArrayIndex(argumentsParameter, Expression.Constant(index)), parameter.ParameterType)).ToArray();

            NewExpression newExpression = Expression.New(constructorInfo, argumentsAccessors);

            Expression<Instantiator> lambda = Expression.Lambda<Instantiator>(
                Expression.Convert(newExpression, typeof(object)), argumentsParameter);

            return lambda;
        }

        /// <summary>
        ///   Finds the best method match for the specified argument type.s
        /// </summary>
        /// <param name = "sources">The sources.</param>
        /// <param name = "toMatch">To match.</param>
        /// <returns></returns>
        public static MethodBase FindBestMethodMatch(this IEnumerable<MethodBase> sources, IEnumerable<Type> toMatch)
        {
            Dictionary<IEnumerable<Type>, MethodBase> sourceParametersByMethod = sources.ToDictionary(i => i.GetParameters().Select(p => p.ParameterType).AsEnumerable(), i => i);
            IEnumerable<Type> bestMatch = sourceParametersByMethod.Keys.FindBestTypeMatch(toMatch);
            if (bestMatch != null)
            {
                return sourceParametersByMethod[bestMatch];
            }
            return null;
        }

        /// <summary>
        ///   Finds the best type match for the toMatch types among all of the sources specified.
        /// </summary>
        /// <param name = "sources">The sources.</param>
        /// <param name = "toMatch">To match.</param>
        /// <returns></returns>
        public static IEnumerable<Type> FindBestTypeMatch(this IEnumerable<IEnumerable<Type>> sources, IEnumerable<Type> toMatch)
        {
            return sources.Select(i => new { Source = i, Rank = i.GetTypeMatchRank(toMatch) }).Where(i => i.Rank >= 0).OrderByDescending(i => i.Rank).Select(i => i.Source).FirstOrDefault();
        }

        /// <summary>
        ///   Gets the type match rank between two sets of types. Returns -1 if incompatible.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <param name = "candidate">To match.</param>
        /// <returns></returns>
        internal static int GetTypeMatchRank(this IEnumerable<Type> source, IEnumerable<Type> candidate)
        {
            int rank = 0;
            Type[] sourceArray = source.ToArray();
            Type[] candidateArray = candidate.ToArray();

            if (sourceArray.Count() != candidateArray.Length)
            {
                return -1;
            }
            for (int i = 0; i < sourceArray.Length; i++)
            {
                Type sourceItem = sourceArray[i];

                sourceItem.IfNull(() => new InvalidOperationException().Throw());

                Type candidateItem = candidateArray[i];

                if (candidateItem == null && !sourceItem.CanHaveNullValue())
                {
                    return -1;
                }
                if (candidateItem != null && !candidateItem.IsCastableTo(sourceItem))
                {
                    return -1;
                }
                if (sourceItem == candidateItem)
                {
                    rank++;
                }
            }

            return rank;
        }

        /// <summary>
        ///   Invokes a fast invoker for the specified method name and arguments.
        /// </summary>
        /// <param name = "instance">The instance.</param>
        /// <param name = "methodName">Name of the method.</param>
        /// <param name = "arguments">The arguments.</param>
        /// <returns></returns>
        public static object Invoke(this object instance, string methodName, params object[] arguments)
        {
            Invoker invoker = instance.GetType().GetInvoker(methodName, arguments.Select(a => a != null ? a.GetType() : null).ToArray());
            if (invoker == null)
            {
                throw new InvalidOperationException("Could not find a supported method named {0} for the argument types {1}".FormatWith(methodName, arguments.Select(a => a != null ? a.GetType().Name : "null").Join()));
            }
            return invoker(instance, arguments);
        }

        /// <summary>
        /// Invokes a fast invoker for the specified method name and arguments.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns></returns>
        public static object Invoke(this Type type, string methodName, params object[] arguments)
        {
            return type.GetInvoker(methodName, arguments.Select(a => a != null ? a.GetType() : null).ToArray())(null, arguments);
        }

        /// <summary>
        ///   Gets a fast invoker for the specified method name and argument types.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "methodName">Name of the method.</param>
        /// <param name = "argumentTypes">The argument types.</param>
        /// <returns></returns>
        public static Invoker GetInvoker(this Type type, string methodName, Type[] argumentTypes = null)
        {
            IEnumerable<MethodInfo> methods = type.GetMember(methodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic).OfType<MethodInfo>();
            // ReSharper disable RedundantEnumerableCastCall
            var methodInfo = methods.OfType<MethodBase>().FindBestMethodMatch(argumentTypes ?? new Type[0]).As<MethodInfo>();
            // ReSharper restore RedundantEnumerableCastCall
            if (methodInfo == null && (argumentTypes == null || argumentTypes.Length == methods.Select(m => m.GetParameters().Length).FirstOrDefault()))
            {
                methodInfo = methods.FirstOrDefault(m => m.GetParameters().Length == 0) ?? methods.FirstOrDefault();
            }
            if (methodInfo == null)
            {
                return null;
            }
            return methodInfo.GetInvoker();
        }

        /// <summary>
        /// Gets a getter delegate for a field or property.
        /// </summary>
        /// <param name="member">The member.</param>
        /// <returns></returns>
        public static Func<object, object> GetGetter(this MemberInfo member)
        {
            var field = member as FieldInfo;
            if (field != null)
            {
                return o => field.GetValue(o);
            }
            var property = member as PropertyInfo;
            if (property != null)
            {
                var invoker = property.GetGetMethod(true).GetInvoker();
                return o => invoker(o);
            }
            throw new NotSupportedException("Member {0} should be a property or field.".FormatWith(member.Name));
        }

        /// <summary>
        /// Gets a setter delegate for a field or property.
        /// </summary>
        /// <param name="member">The member.</param>
        /// <returns></returns>
        public static Action<object, object> GetSetter(this MemberInfo member)
        {
            var field = member as FieldInfo;
            if (field != null)
            {
                return (o, v) => field.SetValue(o, v);
            }
            var property = member as PropertyInfo;
            if (property != null)
            {
                var invoker = property.GetSetMethod(true).GetInvoker();
                return (o, v) => invoker(o, v);
            }
            throw new NotSupportedException("Member {0} should be a property or field.".FormatWith(member.Name));
        }

        /// <summary>
        ///   Gets a fast invoker for the specified methodInfo.
        /// </summary>
        /// <param name = "methodInfo">The method info.</param>
        /// <returns></returns>
        public static Invoker GetInvoker(this MethodInfo methodInfo)
        {
            Delegate invoker;
            MemberDelegateCache.TryGetValue(methodInfo, out invoker);
            if (invoker == null)
            {
                invoker = methodInfo.CreateInvoker();
                MemberDelegateCache[methodInfo] = invoker;
            }
            return (Invoker)invoker;
        }

        /// <summary>
        ///   Creates a new fast invoker delegate for the specified methodInfo.
        /// </summary>
        /// <param name = "methodInfo">The method info.</param>
        /// <returns></returns>
        internal static Invoker CreateInvoker(this MethodInfo methodInfo)
        {
            return methodInfo.CreateInvokerLambdaExpression().Compile();
        }

        /// <summary>
        ///   Creates a new fast invoker expression for the specified methodInfo.
        /// </summary>
        /// <param name = "methodInfo">The method info.</param>
        /// <returns></returns>
        internal static Expression<Invoker> CreateInvokerLambdaExpression(this MethodInfo methodInfo)
        {
            ParameterExpression instanceParameter = Expression.Parameter(typeof(object), "instance");
            ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]), "arguments");

            ParameterExpression[] variables = methodInfo.GetParameters().Select((parameter, index) => Expression.Variable(parameter.ParameterType.IsByRef ? parameter.ParameterType.GetElementType() : parameter.ParameterType, "argument{0}".FormatWith(index))).ToArray();

            BinaryExpression[] variablesSetters = methodInfo.GetParameters().Select((parameter, index) => Expression.Assign(variables[index],
                                                                                     Expression.Convert(
                                                                                         Expression.ArrayIndex(argumentsParameter, Expression.Constant(index)), parameter.ParameterType.IsByRef ? parameter.ParameterType.GetElementType() : parameter.ParameterType))).ToArray();

            ParameterExpression result = Expression.Variable(typeof(object), "result");

            MethodCallExpression call = Expression.Call(methodInfo.IsStatic ? null : Expression.Convert(instanceParameter, methodInfo.DeclaringType.EnsureNotDefault("MethodInfo must have a declaring type.")), methodInfo, variables);

            BinaryExpression[] argumentSetters = methodInfo.GetParameters().Select((parameter, index) => Expression.Assign(
                Expression.ArrayAccess(argumentsParameter, Expression.Constant(index)),
                Expression.Convert(variables[index], typeof(object)))).ToArray();

            var bodyExpressions = new List<Expression>();
            bodyExpressions.AddRange(variables);
            bodyExpressions.AddRange(variablesSetters);

            if (methodInfo.ReturnType != typeof(void))
            {
                bodyExpressions.Add(Expression.Assign(result, Expression.Convert(call, typeof(object))));
            }
            else
            {
                bodyExpressions.Add(call);
                bodyExpressions.Add(Expression.Assign(result, Expression.Constant(null)));
            }

            bodyExpressions.AddRange(argumentSetters);

            bodyExpressions.Add(result);

            Expression body = Expression.Block(variables.Concat(new[] { result }), bodyExpressions);

            Expression<Invoker> lambda = Expression.Lambda<Invoker>(
                Expression.Convert(body, typeof(object)),
                instanceParameter,
                argumentsParameter);

            return lambda;
        }

        #region Nested type: AttributeCacheKey

        private class AttributeCacheKey
        {
            private readonly Type attributeType;
            private readonly ICustomAttributeProvider customAttributeProvider;
            private readonly bool inherit;
            private readonly bool includeInterfaces;

            public AttributeCacheKey(ICustomAttributeProvider customAttributeProvider, Type attributeType, bool inherit, bool includeInterfaces)
            {
                this.customAttributeProvider = customAttributeProvider;
                this.attributeType = attributeType;
                this.inherit = inherit;
                this.includeInterfaces = includeInterfaces;
            }

            public override bool Equals(object obj)
            {
                return obj.As<AttributeCacheKey>().IfNotNull(o =>
                                                        Equals(o.customAttributeProvider, customAttributeProvider) &&
                                                        o.attributeType == attributeType &&
                                                        o.inherit == inherit &&
                                                        o.includeInterfaces == includeInterfaces);
            }

            public override int GetHashCode()
            {
                return Objects.GetHashCodeWithNullCheck(customAttributeProvider) ^ Objects.GetHashCodeWithNullCheck(attributeType);
            }
        }

        #endregion
    }

    /// <summary>
    ///   Allows a invocation of a constructor with the specified arguments to create an instance of a type used to create the delegate.
    /// </summary>
    public delegate object Instantiator(params object[] arguments);

    /// <summary>
    ///   Allows a invocation of some method on the specified instance and arguments.
    /// </summary>
    public delegate object Invoker(object instance, params object[] arguments);
}

namespace Soaf.Logging
{
    /// <summary>
    ///   Represents a scoped activity. Used for nesting and correlating acitivities.
    /// </summary>
    public class ActivityScope : IDisposable
    {
        [ThreadStatic]
        private static ActivityScope current;

        public ActivityScope(Guid activityId, object state)
        {
            Last = Current;
            Current = this;
            ActivityId = activityId;
            State = state;
        }

        /// <summary>
        ///   Gets or sets the current scope. Local to this thread.
        /// </summary>
        /// <value>The current.</value>
        public static ActivityScope Current
        {
            get { return current; }
            private set { current = value; }
        }

        /// <summary>
        ///   Gets or sets the last scope.
        /// </summary>
        /// <value>The last.</value>
        public ActivityScope Last { get; private set; }

        /// <summary>
        ///   Gets or sets the activity id.
        /// </summary>
        /// <value>The activity id.</value>
        public Guid ActivityId { get; private set; }

        /// <summary>
        ///   Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public object State { get; private set; }

        #region IDisposable Members

        public void Dispose()
        {
            Current = Last;
        }

        #endregion
    }

    /// <summary>
    ///   Represents the severity of a log entry.
    /// </summary>
    public enum Severity
    {
        /// <summary>
        /// </summary>
        Information,

        /// <summary>
        /// </summary>
        Warning,

        /// <summary>
        /// </summary>
        Error
    }

    /// <summary>
    /// A category associated with a log entry.
    /// </summary>
    [Description("Log Category")]
    public class LogCategory
    {
        #region Primitive Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public virtual int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public virtual string Name { get; set; }

        #endregion
    }

    /// <summary>
    /// A name value property for a log entry.
    /// </summary>
    [Description("Log Entry Property")]
    public class LogEntryProperty
    {
        public LogEntryProperty()
        {
            Id = Guid.NewGuid();
        }

        #region Primitive Properties

        private Guid logEntryId;

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the log entry id.
        /// </summary>
        /// <value>The log entry id.</value>
        [Description("Log Entry Id")]
        public virtual Guid LogEntryId
        {
            get { return logEntryId; }
            set
            {
                if (logEntryId != value)
                {
                    if (LogEntry != null && LogEntry.Id != value)
                    {
                        LogEntry = null;
                    }
                    logEntryId = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public virtual string Value { get; set; }

        #endregion

        #region Navigation Properties

        private LogEntry logEntry;

        /// <summary>
        /// Gets or sets the log entry.
        /// </summary>
        /// <value>The log entry.</value>
        [Description("Log Entry")]
        public virtual LogEntry LogEntry
        {
            get { return logEntry; }
            set
            {
                if (!ReferenceEquals(logEntry, value))
                {
                    LogEntry previousValue = logEntry;
                    logEntry = value;
                    FixupLogEntry(previousValue);
                }
            }
        }

        #endregion

        #region Association Fixup

        private void FixupLogEntry(LogEntry previousValue)
        {
            if (previousValue != null && previousValue.Properties.Contains(this))
            {
                previousValue.Properties.Remove(this);
            }

            if (LogEntry != null)
            {
                if (!LogEntry.Properties.Contains(this))
                {
                    LogEntry.Properties.Add(this);
                }
                LogEntryId = LogEntry.Id;
            }
        }

        #endregion

        public override string ToString()
        {
            return "{0}: {1}".FormatWith(Name, Value);
        }
    }

    /// <summary>
    ///   Represents information to log.
    /// </summary>
    [Description("Log Entry")]
    public class LogEntry
    {
        private static string CachedMachineName = Environment.MachineName;
        private static string CachedProcessName = Process.GetCurrentProcess().ProcessName;
        private static int CachedProcessId = Process.GetCurrentProcess().Id;
        private static string cachedIpAddress;

        static LogEntry()
        {
            Trace.UseGlobalLock = false;
        }

        public LogEntry()
        {
            Id = Guid.NewGuid();

            Type callingType = new StackFrame(1).GetMethod().DeclaringType;
            if (callingType != null) Source = callingType.Name;
            else Source = string.Empty;

            if (ActivityScope.Current != null) ActivityId = ActivityScope.Current.ActivityId;
            if (ActivityScope.Current != null && ActivityScope.Current.Last != null) RelatedActivityId = ActivityScope.Current.Last.ActivityId;
            ThreadName = Thread.CurrentThread.Name;
            MachineName = CachedMachineName;
            ProcessId = CachedProcessId;
            ProcessName = CachedProcessName;
            DateTime = DateTime.UtcNow;

            if (cachedIpAddress.IsNullOrEmpty())
            {
                try
                {
                    var currentIpAddress = Net.Net.GetLocalInternetIpAddress();
                    if (currentIpAddress != null) cachedIpAddress = currentIpAddress.ToString();
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {
                    // catch any exception and do nothing. just leave ip address empty if it cannot be retrieved.
                }
            }

            IpAddress = cachedIpAddress;

            if (PrincipalContext.Current != null && PrincipalContext.Current.Principal != null)
            {
                var identity = PrincipalContext.Current.Principal.Identity as IHasId;
                if (identity != null && identity.Id != null)
                {
                    UserId = identity.Id.ToString();
                }
            }
        }

        public Severity Severity
        {
            get { return (Severity)SeverityId; }
            set { SeverityId = (int)value; }
        }

        internal IFormatProvider FormatProvider { get; set; }

        #region Primitive Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the source of the log entry.
        /// </summary>
        /// <value>The source.</value>
        public virtual string Source { get; set; }

        /// <summary>
        /// Gets or sets the title of the log entry.
        /// </summary>
        /// <value>The title.</value>
        public virtual string Title { get; set; }

        /// <summary>
        /// Gets or sets the message to log.
        /// </summary>
        /// <value>The message.</value>
        public virtual string Message { get; set; }

        /// <summary>
        /// Gets or sets the severity id.
        /// </summary>
        /// <value>The severity id.</value>
        public virtual int SeverityId { get; set; }

        /// <summary>
        /// Gets or sets the activity id. Used to group log entries together.
        /// </summary>
        /// <value>The activity id.</value>
        [Description("Activity")]
        public virtual Guid? ActivityId { get; set; }

        /// <summary>
        /// Gets or sets the related/parent activity id. Used to group log entries together.
        /// </summary>
        /// <value>The activity id.</value>
        [Description("Related Activity")]
        public virtual Guid? RelatedActivityId { get; set; }

        /// <summary>
        /// Gets or sets the event id. Used to identify event types.
        /// </summary>
        /// <value>The event id.</value>
        [Description("Event Id")]
        public virtual int? EventId { get; set; }

        /// <summary>
        /// Gets or sets the name of the machine on which the event is being logged.
        /// </summary>
        /// <value>The name of the machine.</value>
        [Description("Machine")]
        public virtual string MachineName { get; set; }

        /// <summary>
        /// Gets or sets the name of the thread that the logging originated on.
        /// </summary>
        /// <value>The name of the thread.</value>
        [Description("Thread")]
        public virtual string ThreadName { get; set; }

        /// <summary>
        /// Gets or sets the process id under which the logging originated.
        /// </summary>
        /// <value>The process id.</value>
        [Description("Process Id")]
        public virtual int? ProcessId { get; set; }

        /// <summary>
        /// Gets or sets the name of the process under which the logging originated.
        /// </summary>
        /// <value>The name of the process.</value>
        [Description("Process")]
        public virtual string ProcessName { get; set; }

        /// <summary>
        /// Gets or sets the date time when the message was logged.
        /// </summary>
        /// <value>The date time.</value>
        [Description("Date/Time")]
        public virtual DateTime DateTime { get; set; }

        /// <summary>
        /// Gets or sets the user id in context.
        /// </summary>
        /// <value>The user id.</value>
        [Description("User Id")]
        public virtual string UserId { get; set; }

        /// <summary>
        /// Gets or sets the server name that this log entry is recorded on.
        /// </summary>
        /// <value>The user id.</value>
        [Description("Server Name")]
        public virtual string ServerName { get; set; }

        /// <summary>
        /// Gets or sets the Ip Address of the client computer
        /// </summary>
        [Description("IP Address")]
        public virtual string IpAddress { get; set; }

        #endregion

        #region Navigation Properties

        private ICollection<object> extendedProperties = new List<object>();
        private ICollection<LogCategory> categories = new FixupCollection<LogCategory>();
        private ICollection<LogEntryProperty> properties;

        /// <summary>
        /// Gets or sets the property name/value pairs associated with this log entry.
        /// </summary>
        /// <value>The properties.</value>
        public virtual ICollection<LogEntryProperty> Properties
        {
            get
            {
                if (properties == null)
                {
                    var newCollection = new FixupCollection<LogEntryProperty>();
                    newCollection.ItemSet += FixupPropertiesItemSet;
                    newCollection.ItemRemoved += FixupPropertiesItemRemoved;
                    properties = newCollection;
                }
                return properties;
            }
            set
            {
                if (!ReferenceEquals(properties, value))
                {
                    var previousValue = properties as FixupCollection<LogEntryProperty>;
                    if (previousValue != null)
                    {
                        previousValue.ItemSet -= FixupPropertiesItemSet;
                        previousValue.ItemRemoved -= FixupPropertiesItemRemoved;
                    }
                    properties = value;
                    var newValue = value as FixupCollection<LogEntryProperty>;
                    if (newValue != null)
                    {
                        newValue.ItemSet += FixupPropertiesItemSet;
                        newValue.ItemRemoved += FixupPropertiesItemRemoved;
                    }
                }
            }
        }

        /// <summary>
        /// Extra log entry properties which are not automatically persisted or carried over the wire
        /// </summary>
        public virtual ICollection<object> ExtendedProperties
        {
            get { return extendedProperties; }
        }

        /// <summary>
        /// Gets or sets the categories associated with this log entry.
        /// </summary>
        /// <value>The categories.</value>
        public virtual ICollection<LogCategory> Categories
        {
            get { return categories; }
            set { categories = value; }
        }

        #endregion

        #region Association Fixup

        private void FixupPropertiesItemSet(object sender, EventArgs<LogEntryProperty> e)
        {
            LogEntryProperty item = e.Value;

            item.LogEntry = this;
        }

        private void FixupPropertiesItemRemoved(object sender, EventArgs<LogEntryProperty> e)
        {
            LogEntryProperty item = e.Value;

            if (ReferenceEquals(item.LogEntry, this))
            {
                item.LogEntry = null;
            }
        }

        #endregion

        /// <summary>
        ///   Returns a <see cref = "System.String" /> that represents this instance. Uses the specified format provider or a default.
        /// </summary>
        /// <returns>
        ///   A <see cref = "System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format((FormatProvider ?? new LogEntryFormatter()), "{0}", this);
        }
    }

    /// <summary>
    ///   Provides a default mechanism for producing text output of a LogEntry.
    /// </summary>
    internal class LogEntryFormatter : ICustomFormatter, IFormatProvider
    {
        #region ICustomFormatter Members

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            var logEntry = arg as LogEntry;
            if (logEntry == null) throw new InvalidOperationException("Argument should be a log entry.");

            var builder = new StringBuilder();

            builder.AppendFormat("Title: {0}\n", logEntry.Title);
            builder.AppendFormat("Message: {0}\n", logEntry.Message);
            builder.AppendFormat("ActivityId: {0}\n", logEntry.ActivityId);
            if (logEntry.RelatedActivityId.HasValue) builder.AppendFormat("Related Activity Id: {0}\n", logEntry.RelatedActivityId);
            builder.AppendFormat("Categories: {0}\n", string.Join(", ", logEntry.Categories.Select(i => i.Name).ToArray()));
            builder.AppendFormat("DateTime: {0}\n", logEntry.DateTime);
            builder.AppendFormat("Event Id: {0}\n", logEntry.EventId);
            builder.AppendFormat("Machine Name: {0}\n", logEntry.MachineName);
            builder.AppendFormat("Ip Address: {0}\n", logEntry.IpAddress);
            builder.AppendFormat("Process Id: {0}\n", logEntry.ProcessId);
            builder.AppendFormat("Process Name: {0}\n", logEntry.ProcessName);
            builder.AppendFormat("Severity: {0}\n", logEntry.Severity);
            builder.AppendFormat("Source: {0}\n", logEntry.Source);
            builder.AppendFormat("Thread: {0}\n", logEntry.ThreadName);
            builder.AppendFormat("User Id: {0}\n", logEntry.UserId);

            if (logEntry.Properties != null)
            {
                builder.AppendLine("Properties: ");
                foreach (LogEntryProperty property in logEntry.Properties)
                {
                    builder.AppendFormat("    {0}: {1}\n", property.Name, property.Value);
                }
            }

            return format == null ? builder.ToString() : string.Format(format, builder);
        }

        #endregion

        #region IFormatProvider Members

        public object GetFormat(Type formatType)
        {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        #endregion
    }

    /// <summary>
    ///   Defines a type that performs logging operations.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        ///   Logs the specified log entry.
        /// </summary>
        /// <param name = "logEntry">The log entry.</param>
        void Log(LogEntry logEntry);
    }

    /// <summary>
    ///   A base logger implementation.
    /// </summary>
    internal abstract class Logger : ILogger
    {
        protected Logger(string defaultSource)
        {
            DefaultSource = defaultSource;
        }

        /// <summary>
        ///   Gets or sets the source. Uses this source for all log entries that do not already have a source specified.
        /// </summary>
        /// <value>The source.</value>
        public string DefaultSource { get; set; }

        #region ILogger Members

        void ILogger.Log(LogEntry logEntry)
        {
            if (string.IsNullOrEmpty(logEntry.Source)) logEntry.Source = DefaultSource;

            Log(logEntry);
        }

        #endregion

        protected abstract void Log(LogEntry logEntry);
    }

    /// <summary>
    ///   A default logger implementation that uses tracing.
    /// </summary>
    internal class TraceLogger : Logger
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TraceLogger"/> class.
        /// </summary>
        public TraceLogger()
            : this(null)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "TraceLogger" /> class. Uses the specified source for all logged log entries that do not already have a source.
        /// </summary>
        /// <param name = "source">The source.</param>
        public TraceLogger(string source)
            : base(source)
        {
        }

        /// <summary>
        ///   Logs the specified log entry.
        /// </summary>
        /// <param name = "logEntry">The log entry.</param>
        protected override void Log(LogEntry logEntry)
        {
            var traceEventCache = new TraceEventCache();

            foreach (var listener in Trace.Listeners.Cast<TraceListener>())
            {
                var traceEventType = TraceEventType.Information;

                switch (logEntry.Severity)
                {
                    case Severity.Warning:
                        traceEventType = TraceEventType.Warning;
                        break;
                    case Severity.Error:
                        traceEventType = TraceEventType.Error;
                        break;
                }

                listener.TraceData(traceEventCache, logEntry.Source, traceEventType, logEntry.EventId.GetValueOrDefault(), logEntry);

                if (Trace.AutoFlush)
                {
                    listener.Flush();
                }
            }
        }
    }

    /// <summary>
    ///   Defines a type that provides instances of ILoggers.
    /// </summary>
    [Factory]
    public interface ILogManager
    {
        /// <summary>
        ///   Gets the logger.
        /// </summary>
        /// <param name = "source">The source requesting the logger.</param>
        /// <returns></returns>
        ILogger GetLogger(string source);
    }

    /// <summary>
    ///   Extensions/helper methods for logging.
    /// </summary>
    public static class Logging
    {
        /// <summary>
        ///   Gets a logger bound to the calling type.
        /// </summary>
        /// <param name = "logManager">The log manager.</param>
        /// <returns></returns>
        public static ILogger GetCurrentTypeLogger(this ILogManager logManager)
        {
            return logManager.GetLogger(new StackFrame(1, false).GetMethod().IfNotNull(m => m.DeclaringType).IfNotNull(t => t.FullName) ?? string.Empty);
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="categories">The categories.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="title">The title.</param>
        /// <param name="source">The source.</param>
        /// <param name="properties">The properties.</param>
        public static void Log(this ILogger logger, object message, IEnumerable<LogCategory> categories, Severity severity = Severity.Information, string title = null, string source = null, IEnumerable<LogEntryProperty> properties = null)
        {
            logger.Log(new LogEntry { Message = message.ToString(), Severity = severity, Source = source, Categories = categories == null ? new List<LogCategory>() : categories.ToList(), Title = title, Properties = (properties ?? new LogEntryProperty[0]).ToList() });
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="message">The message.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="categories">The categories.</param>
        /// <param name="title">The title.</param>
        /// <param name="source">The source.</param>
        /// <param name="properties">The properties.</param>
        public static void Log(this ILogger logger, object message, Severity severity = Severity.Information, IEnumerable<string> categories = null, string title = null, string source = null, IEnumerable<LogEntryProperty> properties = null)
        {
            logger.Log(new LogEntry { Message = message.ToString(), Severity = severity, Source = source, Categories = categories == null ? new List<LogCategory>() : categories.Select(i => new LogCategory { Name = i }).ToList(), Title = title, Properties = (properties ?? new LogEntryProperty[0]).ToList() });
        }
    }

    /// <summary>
    /// A helper to time a region of code.
    /// </summary>
    public class TimedScope : IDisposable
    {
        private readonly DateTime start;
        private readonly Action<string> write;

        public TimedScope(Action<string> write)
        {
            this.write = write;
            start = DateTime.Now;
        }

        #region IDisposable Members

        public void Dispose()
        {
            write("{0}ms".FormatWith((DateTime.Now - start).TotalMilliseconds));
        }

        #endregion
    }

}

namespace Soaf.Net
{
    /// <summary>
    /// Helper/extension methods for networking.
    /// </summary>
    public class Net
    {
        /// <summary>
        /// Sets the cookie with the specified url, name, and value on the client computer.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetCookie(string uri, string name, string value);

        /// <summary>
        /// Gets the internet IP address (looks for v4 first then v6) for the computer in which this function executes
        /// </summary>
        /// <returns></returns>
        public static IPAddress GetLocalInternetIpAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            return host.AddressList.FirstOrDefault(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) ?? host.AddressList.FirstOrDefault(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6);
        }
    }
}