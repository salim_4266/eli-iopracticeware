﻿using System;
using System.Xml.Linq;
using Soaf.ComponentModel;
using Soaf.Configuration;

[assembly: Component(typeof(StringXmlConfiguration), typeof(IXmlConfiguration))]

namespace Soaf.Configuration
{
    /// <summary>
    ///   An xml based configuration source.
    /// </summary>
    public interface IXmlConfiguration
    {
        XElement Xml { get; }
    }

    /// <summary>
    ///   An xml based configuration store initialized from a string value.
    /// </summary>
    public class StringXmlConfiguration : IXmlConfiguration
    {
        private readonly Lazy<XElement> xml;

        public StringXmlConfiguration(string xml)
        {
            this.xml = Lazy.For(() => xml != null ? XElement.Parse(xml) : new XElement("configuration"));
        }

        #region IXmlConfiguration Members

        public XElement Xml
        {
            get { return xml.Value; }
        }

        #endregion
    }
}