﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using Soaf.ComponentModel;
using Soaf.Linq;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Collections;

[assembly: Component(typeof(UserRepository), typeof(IUserRepository))]
[assembly: Component(typeof(UserRepositoryMembershipProvider), typeof(System.Web.Security.MembershipProvider))]
[assembly: Component(typeof(UserRepositoryRoleProvider), typeof(System.Web.Security.RoleProvider))]
[assembly: Component(typeof(AuthenticationService), typeof(IAuthenticationService))]

// ReSharper disable once CheckNamespace
namespace Soaf.Security
{
    public abstract class UserRepository : IUserRepository
    {
        #region IQueryable Properties

        public abstract IQueryable<User> Users { get; }

        public abstract IQueryable<Role> Roles { get; }

        #endregion

        #region Save

        public abstract void Save(User value);

        public abstract void Save(Role value);

        #endregion

        #region Delete

        public abstract void Delete(User value);

        public abstract void Delete(Role value);

        #endregion

        #region Create Objects

        public abstract User CreateUser();

        public abstract Role CreateRole();

        #endregion
    }

    /// <summary>
    /// A MembershipProvider that delegates to a registered MembershipProvider componenent. This type is the one that should be registered in the web.config.
    /// </summary>
    public class MembershipProvider : System.Web.Security.MembershipProvider
    {
        /// <summary>
        /// Singleton instance of the Soaf membership provider.
        /// </summary>
        public static readonly MembershipProvider Current = new MembershipProvider();


        private readonly object syncRoot = new object();
        private System.Web.Security.MembershipProvider underlyingMembershipProvider;

        private System.Web.Security.MembershipProvider UnderlyingMembershipProvider
        {
            get
            {
                lock (syncRoot)
                {
                    if (underlyingMembershipProvider == null)
                    {
                        underlyingMembershipProvider = ServiceProvider.Current.GetService<System.Web.Security.MembershipProvider>();
                    }
                    return underlyingMembershipProvider;
                }
            }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return UnderlyingMembershipProvider.EnablePasswordRetrieval; }
        }

        public override bool EnablePasswordReset
        {
            get { return UnderlyingMembershipProvider.EnablePasswordReset; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return UnderlyingMembershipProvider.RequiresQuestionAndAnswer; }
        }

        public override string ApplicationName
        {
            get { return UnderlyingMembershipProvider.ApplicationName; }
            set { UnderlyingMembershipProvider.ApplicationName = value; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return UnderlyingMembershipProvider.MaxInvalidPasswordAttempts; }
        }

        public override int PasswordAttemptWindow
        {
            get { return UnderlyingMembershipProvider.PasswordAttemptWindow; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return UnderlyingMembershipProvider.RequiresUniqueEmail; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return UnderlyingMembershipProvider.PasswordFormat; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return UnderlyingMembershipProvider.MinRequiredPasswordLength; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return UnderlyingMembershipProvider.MinRequiredNonAlphanumericCharacters; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return UnderlyingMembershipProvider.PasswordStrengthRegularExpression; }
        }

        public override System.Web.Security.MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            return UnderlyingMembershipProvider.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, providerUserKey, out status);
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            return UnderlyingMembershipProvider.ChangePasswordQuestionAndAnswer(username, password, newPasswordQuestion, newPasswordAnswer);
        }

        public override string GetPassword(string username, string answer)
        {
            return UnderlyingMembershipProvider.GetPassword(username, answer);
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            return UnderlyingMembershipProvider.ChangePassword(username, oldPassword, newPassword);
        }

        public override string ResetPassword(string username, string answer)
        {
            return UnderlyingMembershipProvider.ResetPassword(username, answer);
        }

        public override void UpdateUser(System.Web.Security.MembershipUser user)
        {
            UnderlyingMembershipProvider.UpdateUser(user);
        }

        public override bool ValidateUser(string username, string password)
        {
            return UnderlyingMembershipProvider.ValidateUser(username, password);
        }

        public override bool UnlockUser(string userName)
        {
            return UnderlyingMembershipProvider.UnlockUser(userName);
        }

        public override System.Web.Security.MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            return UnderlyingMembershipProvider.GetUser(providerUserKey, userIsOnline);
        }

        public override System.Web.Security.MembershipUser GetUser(string username, bool userIsOnline)
        {
            return UnderlyingMembershipProvider.GetUser(username, userIsOnline);
        }

        public override string GetUserNameByEmail(string email)
        {
            return UnderlyingMembershipProvider.GetUserNameByEmail(email);
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            return UnderlyingMembershipProvider.DeleteUser(username, deleteAllRelatedData);
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            return UnderlyingMembershipProvider.GetAllUsers(pageIndex, pageSize, out totalRecords);
        }

        public override int GetNumberOfUsersOnline()
        {
            return UnderlyingMembershipProvider.GetNumberOfUsersOnline();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            return UnderlyingMembershipProvider.FindUsersByName(usernameToMatch, pageIndex, pageSize, out totalRecords);
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            return UnderlyingMembershipProvider.FindUsersByEmail(emailToMatch, pageIndex, pageSize, out totalRecords);
        }
    }


    /// <summary>
    /// A RoleProvider that delegates to a registered RoleProvider componenent. This type is the one that should be registered in the web.config.
    /// </summary>
    public class RoleProvider : System.Web.Security.RoleProvider
    {
        private readonly object syncRoot = new object();
        private System.Web.Security.RoleProvider underlyingRoleProvider;

        private System.Web.Security.RoleProvider UnderlyingRoleProvider
        {
            get
            {
                lock (syncRoot)
                {
                    return underlyingRoleProvider ?? (underlyingRoleProvider = ServiceProvider.Current.GetService<System.Web.Security.RoleProvider>());
                }
            }
        }

        public override string ApplicationName
        {
            get { return UnderlyingRoleProvider.ApplicationName; }
            set { UnderlyingRoleProvider.ApplicationName = value; }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            return UnderlyingRoleProvider.IsUserInRole(username, roleName);
        }

        public override string[] GetRolesForUser(string username)
        {
            return UnderlyingRoleProvider.GetRolesForUser(username);
        }

        public override void CreateRole(string roleName)
        {
            UnderlyingRoleProvider.CreateRole(roleName);
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            return UnderlyingRoleProvider.DeleteRole(roleName, throwOnPopulatedRole);
        }

        public override bool RoleExists(string roleName)
        {
            return UnderlyingRoleProvider.RoleExists(roleName);
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            UnderlyingRoleProvider.AddUsersToRoles(usernames, roleNames);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            UnderlyingRoleProvider.RemoveUsersFromRoles(usernames, roleNames);
        }

        public override string[] GetUsersInRole(string roleName)
        {
            return UnderlyingRoleProvider.GetUsersInRole(roleName);
        }

        public override string[] GetAllRoles()
        {
            return UnderlyingRoleProvider.GetAllRoles();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return UnderlyingRoleProvider.FindUsersInRole(roleName, usernameToMatch);
        }
    }

    /// <summary>
    /// A custom MembershipUser that allows updating the IsOnline property directly.
    /// </summary>
    public class MembershipUser : System.Web.Security.MembershipUser
    {
        private bool isOnline;

        public MembershipUser()
        {
        }

        public MembershipUser(System.Web.Security.MembershipUser membershipUser)
            : this(membershipUser.ProviderName, membershipUser.UserName, membershipUser.ProviderUserKey, membershipUser.Email, membershipUser.PasswordQuestion, membershipUser.Comment, membershipUser.IsApproved, membershipUser.IsLockedOut, membershipUser.CreationDate, membershipUser.LastLoginDate, membershipUser.LastActivityDate, membershipUser.LastPasswordChangedDate, membershipUser.LastLockoutDate)
        {

        }

        public MembershipUser(string providerName, string name, object providerUserKey, string email, string passwordQuestion, string comment, bool isApproved, bool isLockedOut, DateTime creationDate, DateTime lastLoginDate, DateTime lastActivityDate, DateTime lastPasswordChangedDate, DateTime lastLockoutDate)
            : base(providerName, name, providerUserKey, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLoginDate, lastActivityDate, lastPasswordChangedDate, lastLockoutDate)
        {
        }

        public override bool IsOnline
        {
            get { return isOnline; }
        }

        /// <summary>
        /// Sets the is online.
        /// </summary>
        /// <param name="isOnline">if set to <c>true</c> [is online].</param>
        public void SetIsOnline(bool isOnline)
        {
            this.isOnline = isOnline;
        }
    }

    /// <summary>
    /// A default MembershipProvider implementation.
    /// </summary>
    [Singleton]
    public class UserRepositoryMembershipProvider : System.Web.Security.MembershipProvider
    {
        private const int NewPasswordLength = 8;
        private readonly IUserRepository repository;

        private bool enablePasswordReset;
        private bool enablePasswordRetrieval;
        private ConfigurationSection machineKeySection; // Used when determining encryption key values.
        private int maxInvalidPasswordAttempts;
        private int minRequiredNonAlphanumericCharacters;
        private int minRequiredPasswordLength;
        private int passwordAttemptWindow;
        private MembershipPasswordFormat passwordFormat;
        private string passwordStrengthRegularExpression;
        private bool requiresQuestionAndAnswer;
        private bool requiresUniqueEmail;

        #region properties

        /// <summary>
        ///   Indicates whether the membership provider is configured to allow users to retrieve their passwords.
        /// </summary>
        /// <returns>
        ///   true if the membership provider is configured to support password retrieval; otherwise, false. The default is false.
        /// </returns>
        public override bool EnablePasswordRetrieval
        {
            get { return enablePasswordRetrieval; }
        }

        /// <summary>
        ///   Indicates whether the membership provider is configured to allow users to reset their passwords.
        /// </summary>
        /// <returns>
        ///   true if the membership provider supports password reset; otherwise, false. The default is true.
        /// </returns>
        public override bool EnablePasswordReset
        {
            get { return enablePasswordReset; }
        }

        /// <summary>
        ///   Gets a value indicating whether the membership provider is configured to require the user to answer a password question for password reset and retrieval.
        /// </summary>
        /// <returns>
        ///   true if a password answer is required for password reset and retrieval; otherwise, false. The default is true.
        /// </returns>
        public override bool RequiresQuestionAndAnswer
        {
            get { return requiresQuestionAndAnswer; }
        }

        /// <summary>
        ///   The name of the application using the custom membership provider.
        /// </summary>
        /// <returns>
        ///   The name of the application using the custom membership provider.
        /// </returns>
        public override string ApplicationName { get; set; }

        /// <summary>
        ///   Gets the number of invalid password or password-answer attempts allowed before the membership user is locked out.
        /// </summary>
        /// <returns>
        ///   The number of invalid password or password-answer attempts allowed before the membership user is locked out.
        /// </returns>
        public override int MaxInvalidPasswordAttempts
        {
            get { return maxInvalidPasswordAttempts; }
        }

        /// <summary>
        ///   Gets the number of minutes in which a maximum number of invalid password or password-answer attempts are allowed before the membership user is locked out.
        /// </summary>
        /// <returns>
        ///   The number of minutes in which a maximum number of invalid password or password-answer attempts are allowed before the membership user is locked out.
        /// </returns>
        public override int PasswordAttemptWindow
        {
            get { return passwordAttemptWindow; }
        }

        /// <summary>
        ///   Gets a value indicating whether the membership provider is configured to require a unique e-mail address for each user name.
        /// </summary>
        /// <returns>
        ///   true if the membership provider requires a unique e-mail address; otherwise, false. The default is true.
        /// </returns>
        public override bool RequiresUniqueEmail
        {
            get { return requiresUniqueEmail; }
        }

        /// <summary>
        ///   Gets a value indicating the format for storing passwords in the membership data store.
        /// </summary>
        /// <returns>
        ///   One of the <see cref = "T:System.Web.Security.MembershipPasswordFormat" /> values indicating the format for storing passwords in the data store.
        /// </returns>
        public override MembershipPasswordFormat PasswordFormat
        {
            get { return passwordFormat; }
        }

        /// <summary>
        ///   Gets the minimum length required for a password.
        /// </summary>
        /// <returns>
        ///   The minimum length required for a password. 
        /// </returns>
        public override int MinRequiredPasswordLength
        {
            get { return minRequiredPasswordLength; }
        }

        /// <summary>
        ///   Gets the minimum number of special characters that must be present in a valid password.
        /// </summary>
        /// <returns>
        ///   The minimum number of special characters that must be present in a valid password.
        /// </returns>
        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return minRequiredNonAlphanumericCharacters; }
        }

        /// <summary>
        ///   Gets the regular expression used to evaluate a password.
        /// </summary>
        /// <returns>
        ///   A regular expression used to evaluate a password.
        /// </returns>
        public override string PasswordStrengthRegularExpression
        {
            get { return passwordStrengthRegularExpression; }
        }

        #endregion

        #region public methods

        /// <summary>
        ///   Initialize this membership provider. Loads the configuration settings.
        /// </summary>
        /// <param name = "name">membership provider name</param>
        /// <param name = "config">configuration</param>
        public override void Initialize(string name, NameValueCollection config)
        {
            // Initialize values from web.config.
            if (config == null) throw new ArgumentNullException("config");

            if (String.IsNullOrEmpty(name)) name = GetType().Name;

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Soaf Membership Provider");
            }
            // Initialize the abstract base class.
            base.Initialize(name, config);

            maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
            minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));
            enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));

            string passwordFormatName = config["passwordFormat"] ?? "Hashed";

            switch (passwordFormatName)
            {
                case "Hashed":
                    passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }

            // Get encryption and decryption key information from the configuration.
            machineKeySection = ConfigurationManager.GetSection("system.web/machineKey") as ConfigurationSection;
        }

        /// <summary>
        ///   Adds a new membership user to the data source.
        /// </summary>
        /// <returns>A <see cref = "T:System.Web.Security.MembershipUser" /> object populated with the information for the newly created user.</returns>
        /// <param name = "userName">The user name for the new user.</param>
        /// <param name = "password">The password for the new user.</param>
        /// <param name = "email">The e-mail address for the new user.</param>
        /// <param name = "passwordQuestion">The password question for the new user.</param>
        /// <param name = "passwordAnswer">The password answer for the new user</param>
        /// <param name = "isApproved">Whether or not the new user is approved to be validated.</param>
        /// <param name = "providerUserKey">The unique identifier from the membership data source for the user.</param>
        /// <param name = "status">A <see cref = "T:System.Web.Security.MembershipCreateStatus" /> enumeration value indicating whether the user was created successfully.</param>
        public override System.Web.Security.MembershipUser CreateUser(string userName, string password, string email,
                                                  string passwordQuestion, string passwordAnswer, bool isApproved,
                                                  object providerUserKey, out MembershipCreateStatus status)
        {
            providerUserKey.EnsureNotDefault("providerUserKey must be supplied when creating new User.");

            // Validate userName/password
            var args = new ValidatePasswordEventArgs(userName, password, true);
            OnValidatingPassword(args);

            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }

            // Check whether user with passed userName already exists
            System.Web.Security.MembershipUser u = GetUser(userName, false);

            if (u == null)
            {
                DateTime createdDate = DateTime.Now;

                var user = new User
                    {
                        UserName = userName,
                        Email = email,
                        Password = EncodePassword(password),
                        PasswordQuestion = passwordQuestion,
                        PasswordAnswer = passwordAnswer,
                        IsApproved = isApproved,
                        LastActivityDate = createdDate,
                        LastLoginDate = DateTime.Now,
                        LastPasswordChangedDate = createdDate,
                        CreationDate = createdDate,
                        IsOnline = false,
                        IsLockedOut = false,
                        LastLockedOutDate = createdDate,
                        FailedPasswordAttemptCount = 0,
                        FailedPasswordAttemptWindowStart = createdDate,
                        FailedPasswordAnswerAttemptCount = 0,
                        FailedPasswordAnswerAttemptWindowStart = createdDate,
                        Comment = string.Empty, 
                        Id = providerUserKey.ToString()
                    };

                try
                {
                    repository.Save(user);
                    status = MembershipCreateStatus.Success;
                }
                catch
                {
                    status = MembershipCreateStatus.UserRejected;
                }

                return GetUser(userName, false);
            }
            status = MembershipCreateStatus.DuplicateUserName;

            return null;
        }

        /// <summary>
        ///   Processes a request to update the password question and answer for a membership user.
        /// </summary>
        /// <returns>true if the password question and answer are updated successfully; otherwise, false.</returns>
        /// <param name = "userName">The user to change the password question and answer for.</param>
        /// <param name = "password">The password for the specified user.</param>
        /// <param name = "newPasswordQuestion">The new password question for the specified user.</param>
        /// <param name = "newPasswordAnswer">The new password answer for the specified user.</param>
        public override bool ChangePasswordQuestionAndAnswer(string userName, string password,
                                                             string newPasswordQuestion, string newPasswordAnswer)
        {
            //check if user is authenticated
            if (!ValidateUser(userName, password)) return false;

            IQueryable<User> users = from u in repository.Users
                                     where u.UserName.ToLower() == userName.ToLower()
                                     select u;

            if (users.Count() != 1) throw new ProviderException("Change password question and answer failed. No unique user found.");

            User user = users.First();
            user.PasswordAnswer = EncodePassword(newPasswordAnswer);
            user.PasswordQuestion = newPasswordQuestion;

            try
            {
                repository.Save(user);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        ///   Gets the password for the specified user name from the data source.
        /// </summary>
        /// <returns>The password for the specified user name.</returns>
        /// <param name = "userName">The user to retrieve the password for.</param>
        /// <param name = "answer">The password answer for the user.</param>
        public override string GetPassword(string userName, string answer)
        {
            if (!EnablePasswordRetrieval) throw new ProviderException("Password Retrieval Not Enabled.");
            if (PasswordFormat == MembershipPasswordFormat.Hashed) throw new ProviderException("Cannot retrieve Hashed passwords.");

            string password = string.Empty;

            IQueryable<User> users = from u in repository.Users
                                     where u.UserName.ToLower() == userName.ToLower()
                                     select u;

            if (users.Count() != 1) throw new ProviderException("Get password failed. No unique user found.");

            User user = users.FirstOrDefault();

            if (user != null)
            {
                if (Convert.ToBoolean(user.IsLockedOut)) throw new MembershipPasswordException("The supplied user is locked out.");
            }
            else throw new MembershipPasswordException("The supplied user name is not found.");

            if (RequiresQuestionAndAnswer && !CheckPassword(answer, user.PasswordAnswer))
            {
                UpdateFailureCount(userName, "passwordAnswer");
                throw new MembershipPasswordException("Incorrect password answer.");
            }

            if (PasswordFormat == MembershipPasswordFormat.Encrypted) password = UnEncodePassword(user.Password);

            return password;
        }

        /// <summary>
        ///   Processes a request to update the password for a membership user.
        /// </summary>
        /// <returns>true if the password was updated successfully; otherwise, false.</returns>
        /// <param name = "userName">The user to update the password for.</param>
        /// <param name = "oldPassword">The current password for the specified user.</param>
        /// <param name = "newPassword">The new password for the specified user.</param>
        public override bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            //check if user is authenticated
            if (!ValidateUser(userName, oldPassword)) return false;

            //notify that password is going to change
            var args = new ValidatePasswordEventArgs(userName, newPassword, false);
            OnValidatingPassword(args);

            if (args.Cancel)
            {
                if (args.FailureInformation != null) throw args.FailureInformation;
                throw new MembershipPasswordException("Change password canceled due to new password validation failure.");
            }

            IQueryable<User> users = from u in repository.Users
                                     where u.UserName.ToLower() == userName.ToLower()
                                     select u;

            if (users.Count() != 1) throw new ProviderException("Change password failed. No unique user found.");

            User user = users.First();
            user.Password = EncodePassword(newPassword);
            user.LastPasswordChangedDate = DateTime.Now;

            try
            {
                repository.Save(user);
                return true;
            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        ///   Resets a user's password to a new, automatically generated password.
        /// </summary>
        /// <returns>The new password for the specified user.</returns>
        /// <param name = "userName">The user to reset the password for.</param>
        /// <param name = "answer">The password answer for the specified user.</param>
        public override string ResetPassword(string userName, string answer)
        {
            if (!EnablePasswordReset) throw new NotSupportedException("Password reset is not enabled.");

            if (answer == null && RequiresQuestionAndAnswer)
            {
                UpdateFailureCount(userName, "passwordAnswer");
                throw new ProviderException("Password answer required for password reset.");
            }

            var membershipType = WellKnownAssemblies.System_Web.EnsureNotDefault(new NotSupportedException("Resetting password is not supported in this environment."))
                .GetType("System.Web.Security.Membership").EnsureNotDefault("Resetting password is not supported in this environment.");

            var newPassword = (string)membershipType.GetMethod("GeneratePassword", new[] { typeof(int), typeof(int) }).Invoke(null, new[] { NewPasswordLength, MinRequiredNonAlphanumericCharacters });

            var args = new ValidatePasswordEventArgs(userName, newPassword, false);
            OnValidatingPassword(args);

            if (args.Cancel)
            {
                if (args.FailureInformation != null) throw args.FailureInformation;
                throw new MembershipPasswordException("Reset password canceled due to password validation failure.");
            }

            IQueryable<User> users = from u in repository.Users
                                     where u.UserName.ToLower() == userName.ToLower()
                                     select u;

            if (users.Count() != 1) throw new ProviderException("Reset password failed. No unique user found.");

            User user = users.FirstOrDefault();

            if (user != null)
            {
                if (Convert.ToBoolean(user.IsLockedOut)) throw new MembershipPasswordException("The supplied user is locked out.");
            }
            else
            {
                throw new MembershipPasswordException("The supplied user name is not found.");
            }

            if (RequiresQuestionAndAnswer && !CheckPassword(answer, user.PasswordAnswer))
            {
                UpdateFailureCount(userName, "passwordAnswer");
                throw new MembershipPasswordException("Incorrect password answer.");
            }

            try
            {
                user.Password = EncodePassword(newPassword);
                user.LastPasswordChangedDate = DateTime.Now;

                repository.Save(user);

                return newPassword;
            }
            catch
            {
                throw new MembershipPasswordException("User not found, or user is locked out. Password not Reset.");
            }
        }

        /// <summary>
        ///   Updates information about a user in the data source.
        /// </summary>
        /// <param name = "membershipUser">A <see cref = "T:System.Web.Security.MembershipUser" /> object that represents the user to update and the updated information for the user.</param>
        public override void UpdateUser(System.Web.Security.MembershipUser membershipUser)
        {
            IQueryable<User> users = from u in repository.Users
                                     where u.UserName.ToLower() == membershipUser.UserName.ToLower()
                                     select u;

            if (users.Count() != 1) throw new ProviderException("Update user failed. No unique user found.");

            User user = users.FirstOrDefault();

            if (user == null) return;

            user.Email = membershipUser.Email;
            user.Comment = membershipUser.Comment;
            user.IsApproved = membershipUser.IsApproved;
            user.IsOnline = membershipUser.IsOnline;
            user.UserName = membershipUser.UserName;
            user.LastActivityDate = membershipUser.LastActivityDate;
            user.IsLockedOut = membershipUser.IsLockedOut;
            user.LastLockedOutDate = membershipUser.LastLockoutDate;
            user.Comment = membershipUser.Comment;

            repository.Save(user);
        }

        /// <summary>
        ///   Verifies that the specified user name and password exist in the data source.
        /// </summary>
        /// <returns>true if the specified userName and password are valid; otherwise, false.</returns>
        /// <param name = "userName">The name of the user to validate.</param>
        /// <param name = "password">The password for the specified user.</param>
        public override bool ValidateUser(string userName, string password)
        {
            OnValidatingPassword(new ValidatePasswordEventArgs(userName, password, false));

            bool isValid = false;

            if (userName.IsNullOrEmpty() || password.IsNullOrEmpty())
            {
                return false;
            }

            IQueryable<User> users = from u in repository.Users
                                     where u.UserName.ToLower() == userName.ToLower()
                                     select u;

            if (users.Count() != 1) return false;

            User user = users.FirstOrDefault();

            if (user == null) return false;

            if (CheckPassword(password, user.Password))
            {
                if (user.IsApproved)
                {
                    isValid = true;

                    user.LastLoginDate = DateTime.Now;
                    user.LastActivityDate = user.LastLoginDate;
                    
                    repository.Save(user);
                }
            }
            else
            {
                UpdateFailureCount(userName, "password");
            }

            return isValid;
        }

        /// <summary>
        ///   Clears a lock so that the membership user can be validated.
        /// </summary>
        /// <returns>true if the membership user was successfully unlocked; otherwise, false.</returns>
        /// <param name = "userName">The membership user whose lock status you want to clear.</param>
        public override bool UnlockUser(string userName)
        {
            try
            {
                IQueryable<User> users = from u in repository.Users
                                         where u.UserName.ToLower() == userName.ToLower()
                                         select u;

                if (users.Count() != 1) return false;

                User user = users.First();

                if (user == null) return false;

                user.LastLockedOutDate = DateTime.Now;

                repository.Save(user);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        ///   Gets user information from the data source based on the unique identifier for the membership user. Provides an option to update the last-activity date/time stamp for the user.
        /// </summary>
        /// <returns>A <see cref = "T:System.Web.Security.MembershipUser" /> object populated with the specified user's information from the data source.</returns>
        /// <param name = "providerUserKey">The unique identifier for the membership user to get information for.</param>
        /// <param name = "userIsOnline">true to update the last-activity date/time stamp for the user; false to return user information without updating the last-activity date/time stamp for the user.</param>
        public override System.Web.Security.MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            providerUserKey.EnsureNotDefault("providerUserKey may not be null.");

            System.Web.Security.MembershipUser membershipUser = null;

            IQueryable<User> users = from u in repository.Users
                                     where u.Id == providerUserKey.ToString()
                                     select u;

            User user = users.FirstOrDefault();
            if (user != null)
            {
                membershipUser = GetMembershipUser(user);

                if (userIsOnline)
                {
                    user.LastActivityDate = DateTime.Now;
                    user.IsOnline = true;
                    repository.Save(user);
                }
            }

            return membershipUser;
        }

        /// <summary>
        ///   Gets information from the data source for a user. Provides an option to update the last-activity date/time stamp for the user.
        /// </summary>
        /// <returns>A <see cref = "T:System.Web.Security.MembershipUser" /> object populated with the specified user's information from the data source.</returns>
        /// <param name = "userName">The name of the user to get information for.</param>
        /// <param name = "userIsOnline">true to update the last-activity date/time stamp for the user; false to return user information without updating the last-activity date/time stamp for the user.</param>
        public override System.Web.Security.MembershipUser GetUser(string userName, bool userIsOnline)
        {
            System.Web.Security.MembershipUser membershipUser = null;

            IQueryable<User> users = from u in repository.Users
                                     where u.UserName.ToLower() == userName.ToLower()
                                     select u;


            User user = users.FirstOrDefault();
            if (user != null)
            {
                membershipUser = GetMembershipUser(user);

                if (userIsOnline)
                {
                    user.LastActivityDate = DateTime.Now;
                    user.IsOnline = true;
                    repository.Save(user);
                }
            }

            return membershipUser;
        }

        /// <summary>
        ///   Gets the user name associated with the specified e-mail address.
        /// </summary>
        /// <returns>The user name associated with the specified e-mail address. If no match is found, return null.</returns>
        /// <param name = "email">The e-mail address to search for.</param>
        public override string GetUserNameByEmail(string email)
        {
            try
            {
                IQueryable<User> users = from u in repository.Users
                                         where u.Email == email
                                         select u;

                if (users.Count() != 1) return string.Empty;

                User user = users.FirstOrDefault();
                return user != null ? user.UserName : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        ///   Removes a user from the membership data source.
        /// </summary>
        /// <returns>true if the user was successfully deleted; otherwise, false.</returns>
        /// <param name = "userName">The name of the user to delete.</param>
        /// <param name = "deleteAllRelatedData">true to delete data related to the user from the database; false to leave data related to the user in the database.</param>
        public override bool DeleteUser(string userName, bool deleteAllRelatedData)
        {
            try
            {
                IQueryable<User> users = from u in repository.Users
                                         where u.UserName.ToLower() == userName.ToLower()
                                         select u;

                if (users.Count() != 1) return false;

                User user = users.First();

                repository.Delete(user);

                if (deleteAllRelatedData)
                {
                }
                return true;
            }

            catch
            {
                return false;
            }
        }

        /// <summary>
        ///   Gets a collection of all the users in the data source in pages of data.
        /// </summary>
        /// <returns>
        ///   A <see cref = "T:System.Web.Security.MembershipUserCollection" /> collection that contains a page of <paramref name = "pageSize" /><see cref = "T:System.Web.Security.MembershipUser" /> objects beginning at the page specified by <paramref name = "pageIndex" />.
        /// </returns>
        /// <param name = "pageIndex">The index of the page of results to return. <paramref name = "pageIndex" /> is zero-based.</param>
        /// <param name = "pageSize">The size of the page of results to return.</param>
        /// <param name = "totalRecords">The total number of matched users.</param>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            var result = new MembershipUserCollection();

            List<User> users = (from u in repository.Users
                                select u).ToList();

            totalRecords = users.Count;

            if (totalRecords <= 0) return result;

            foreach (User user in users)
            {
                result.Add(GetMembershipUser(user));
            }

            return result;
        }

        /// <summary>
        ///   Gets the number of users currently accessing the application.
        /// </summary>
        /// <returns>The number of users currently accessing the application.</returns>
        public override int GetNumberOfUsersOnline()
        {
            return (from u in repository.Users
                    where u.IsOnline
                    select u).Distinct().Count();
        }

        /// <summary>
        ///   Gets a collection of membership users where the user name contains the specified user name to match.
        /// </summary>
        /// <returns>
        ///   A <see cref = "T:System.Web.Security.MembershipUserCollection" /> collection that contains a page of <paramref name = "pageSize" /><see cref = "T:System.Web.Security.MembershipUser" /> objects beginning at the page specified by <paramref name = "pageIndex" />.
        /// </returns>
        /// <param name = "userNameToMatch">The user name to search for.</param>
        /// <param name = "pageIndex">The index of the page of results to return. <paramref name = "pageIndex" /> is zero-based.</param>
        /// <param name = "pageSize">The size of the page of results to return.</param>
        /// <param name = "totalRecords">The total number of matched users.</param>
        public override MembershipUserCollection FindUsersByName(string userNameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            var membershipUsers = new MembershipUserCollection();

            IOrderedQueryable<User> users = from u in repository.Users
                                            where u.UserName.ToLower().Contains(userNameToMatch.ToLower())
                                            orderby u.UserName
                                            select u;

            totalRecords = users.Count();
            if (!users.Any()) return membershipUsers;

            foreach (User user in users.OrderBy(i => i.Id).Skip(pageIndex * pageSize).Take(pageSize))
            {
                membershipUsers.Add(GetMembershipUser(user));
            }

            return membershipUsers;
        }

        /// <summary>
        ///   Gets a collection of membership users where the e-mail address contains the specified e-mail address to match.
        /// </summary>
        /// <returns>
        ///   A <see cref = "T:System.Web.Security.MembershipUserCollection" /> collection that contains a page of <paramref name = "pageSize" /><see cref = "T:System.Web.Security.MembershipUser" /> objects beginning at the page specified by <paramref name = "pageIndex" />.
        /// </returns>
        /// <param name = "emailToMatch">The e-mail address to search for.</param>
        /// <param name = "pageIndex">The index of the page of results to return. <paramref name = "pageIndex" /> is zero-based.</param>
        /// <param name = "pageSize">The size of the page of results to return.</param>
        /// <param name = "totalRecords">The total number of matched users.</param>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            var membershipUsers = new MembershipUserCollection();

            IQueryable<User> users = from u in repository.Users
                                     where u.Email.Contains(emailToMatch)
                                     select u;

            totalRecords = users.Count();
            if (!users.Any()) return membershipUsers;

            foreach (User user in users.OrderBy(i => i.Id).Skip(pageIndex * pageSize).Take(pageSize))
            {
                membershipUsers.Add(GetMembershipUser(user));
            }

            return membershipUsers;
        }

        #endregion

        #region private/protected methods

        /// <summary>
        /// A helper function that takes the current persistent user and creates a MembershiUser from the values.
        /// </summary>
        /// <param name="user">user object containing the user data retrieved from database</param>
        /// <returns>
        /// membership user object
        /// </returns>
        private System.Web.Security.MembershipUser GetMembershipUser(User user)
        {
            string providerName = Name;

            var membershipSection = ConfigurationManager.GetSection(@"system.web/membership") as ConfigurationSection;

            if (membershipSection != null)
            {
                providerName = membershipSection.CastTo<dynamic>().DefaultProvider;
            }

            var membershipUser = new MembershipUser(providerName,
                                      user.UserName,
                                      user.Id,
                                      user.Email,
                                      user.PasswordQuestion,
                                      user.Comment,
                                      user.IsApproved,
                                      user.IsLockedOut,
                                      user.CreationDate,
                                      user.LastLoginDate,
                                      user.LastActivityDate,
                                      user.LastPasswordChangedDate,
                                      user.LastLockedOutDate);

            membershipUser.SetIsOnline(user.IsOnline);

            return membershipUser;
        }

        /// <summary>
        ///   A helper method that performs the checks and updates associated with password failure tracking.
        /// </summary>
        private void UpdateFailureCount(string userName, string failureType)
        {
            IQueryable<User> users = from u in repository.Users
                                     where u.UserName.ToLower() == userName.ToLower()
                                     select u;

            if (users.Count() != 1) throw new ProviderException("Update failure count failed. No unique user found.");

            User user = users.First();

            var windowStart = new DateTime();
            int failureCount = 0;

            if (failureType == "password")
            {
                failureCount = user.FailedPasswordAttemptCount;
                windowStart = user.FailedPasswordAttemptWindowStart;
            }

            if (failureType == "passwordAnswer")
            {
                failureCount = user.FailedPasswordAnswerAttemptCount;
                windowStart = user.FailedPasswordAnswerAttemptWindowStart;
            }

            DateTime windowEnd = windowStart.AddMinutes(PasswordAttemptWindow);

            if (failureCount == 0 || DateTime.Now > windowEnd)
            {
                // First password failure or outside of PasswordAttemptWindow. 
                // Start a new password failure count from 1 and a new window starting now.
                if (failureType == "password")
                {
                    user.FailedPasswordAttemptCount = 1;
                    user.FailedPasswordAttemptWindowStart = DateTime.Now;
                }
                if (failureType == "passwordAnswer")
                {
                    user.FailedPasswordAnswerAttemptCount = 1;
                    user.FailedPasswordAnswerAttemptWindowStart = DateTime.Now;
                }

                try
                {
                    repository.Save(user);
                }
                catch
                {
                    throw new ProviderException("Unable to update failure count and window start.");
                }
            }
            else
            {
                if (failureCount++ >= MaxInvalidPasswordAttempts)
                {
                    // Max password attempts have exceeded the failure threshold. Lock out the user.
                    user.IsLockedOut = true;
                    user.LastLockedOutDate = DateTime.Now;

                    try
                    {
                        repository.Save(user);
                    }
                    catch
                    {
                        throw new ProviderException("Unable to lock out user.");
                    }
                }
                else
                {
                    // Max password attempts have not exceeded the failure threshold. Update
                    // the failure counts. Leave the window the same.
                    if (failureType == "password")
                    {
                        user.FailedPasswordAttemptCount = failureCount;
                    }
                    if (failureType == "passwordAnswer")
                    {
                        user.FailedPasswordAnswerAttemptCount = failureCount;
                    }

                    try
                    {
                        repository.Save(user);
                    }
                    catch
                    {
                        throw new ProviderException("Unable to update failure count.");
                    }
                }
            }
        }


        /// <summary>
        ///   Compares password values based on the MembershipPasswordFormat.
        /// </summary>
        /// <param name = "password">password</param>
        /// <param name = "dbpassword">database password</param>
        /// <returns>whether the passwords are identical</returns>
        private bool CheckPassword(string password, string dbpassword)
        {
            string pass1 = password;
            string pass2 = dbpassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Encrypted:
                    pass2 = UnEncodePassword(dbpassword);
                    break;
                case MembershipPasswordFormat.Hashed:
                    pass1 = EncodePassword(password);
                    break;
            }

            return pass1 == pass2;
        }

        /// <summary>
        ///   Encrypts, Hashes, or leaves the password clear based on the PasswordFormat.
        /// </summary>
        /// <param name = "password"></param>
        /// <returns></returns>
        private string EncodePassword(string password)
        {
            string encodedPassword = password;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    encodedPassword = Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    var hash = new HMACSHA1 { Key = HexToByte(machineKeySection.CastTo<dynamic>().ValidationKey) };
                    encodedPassword = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
                    break;
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return encodedPassword;
        }

        /// <summary>
        ///   Decrypts or leaves the password clear based on the PasswordFormat.
        /// </summary>
        /// <param name = "encodedPassword"></param>
        /// <returns></returns>
        private string UnEncodePassword(string encodedPassword)
        {
            string password = encodedPassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    password = Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Cannot unencode a hashed password.");
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return password;
        }

        /// <summary>
        ///   Converts a hexadecimal string to a byte array. Used to convert encryption key values from the configuration.
        /// </summary>
        /// <param name = "hexString"></param>
        /// <returns></returns>
        private static byte[] HexToByte(string hexString)
        {
            var returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }
            return returnBytes;
        }

        /// <summary>
        ///   A helper function to retrieve config values from the configuration file.
        /// </summary>
        /// <param name = "configValue"></param>
        /// <param name = "defaultValue"></param>
        /// <returns></returns>
        private static string GetConfigValue(string configValue, string defaultValue)
        {
            return String.IsNullOrEmpty(configValue) ? defaultValue : configValue;
        }

        #endregion

        public UserRepositoryMembershipProvider(IUserRepository repository)
        {
            this.repository = repository;
        }

        public void Initialize()
        {
            // initialized via injection, so call Initialize with setting read manually from config

            string name = null;
            var settings = new NameValueCollection();

            var config = ConfigurationManager.GetSection("system.web/membership") as ConfigurationSection;

            if (config != null)
            {
                string fullName = GetType().FullName;
                ProviderSettings provider = ((IEnumerable)config.CastTo<dynamic>().Providers).OfType<ProviderSettings>().FirstOrDefault(p => p.Name == fullName);
                if (provider != null)
                {
                    name = provider.Name;
                    settings = provider.Parameters;
                }
            }

            Initialize(name ?? GetType().Name, settings);
        }
    }


    /// <summary>
    /// A default RoleProvider implementation.
    /// </summary>
    [Singleton]
    internal class UserRepositoryRoleProvider : System.Web.Security.RoleProvider, IInitializable
    {
        private readonly IUserRepository repository;
        private readonly IUnitOfWorkProvider unitOfWorkProvider;

        public UserRepositoryRoleProvider(IUserRepository repository, IUnitOfWorkProvider unitOfWorkProvider)
        {
            this.repository = repository;
            this.unitOfWorkProvider = unitOfWorkProvider;
        }

        public override string ApplicationName { get; set; }

        #region IInitializable Members

        public void Initialize()
        {
            // initialized via injection, so call Initialize with setting read manually from config

            string name = null;
            var settings = new NameValueCollection();

            var config = ConfigurationManager.GetSection("system.web/roleManager") as ConfigurationSection;

            if (config != null)
            {
                string fullName = GetType().FullName;
                ProviderSettings provider = ((IEnumerable)config.CastTo<dynamic>().Providers).OfType<ProviderSettings>().FirstOrDefault(p => p.Name == fullName);
                if (provider != null)
                {
                    name = provider.Name;
                    settings = provider.Parameters;
                }
            }

            Initialize(name ?? GetType().Name, settings);
        }

        #endregion

        public override bool IsUserInRole(string username, string roleName)
        {
            return repository.Users.FirstOrDefault(u => u.UserName.ToLower() == username.ToLower() && u.Roles.Any(r => r.Name.ToLower() == roleName)) != null;
        }

        public override string[] GetRolesForUser(string username)
        {
            IEnumerable<string> roles = repository.Users.Where(u => u.UserName.ToLower() == username.ToLower()).Select(u => u.Roles.Select(r => r.Name)).FirstOrDefault();
            if (roles == null)
            {
                throw new InvalidOperationException("User {0} not found.".FormatWith(username));
            }
            return roles.ToArray();
        }

        public override void CreateRole(string roleName)
        {
            if (repository.Roles.Any(r => r.Name.ToLower() == roleName.ToLower()))
            {
                throw new InvalidOperationException("Role {0} already exists.".FormatWith(roleName));
            }
            repository.Save(new Role { Name = roleName });
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            using (IUnitOfWork work = unitOfWorkProvider.Create())
            {
                Role role = repository.Roles.Include(r => r.Users).FirstOrDefault(r => r.Name.ToLower() == roleName);
                if (role == null)
                {
                    throw new InvalidOperationException("Role {0} not found.".FormatWith(roleName));
                }
                if (throwOnPopulatedRole && role.Users.Count > 0)
                {
                    throw new InvalidOperationException("Role {0} contains users and could not be deleted.".FormatWith(roleName));
                }
                role.Users.ToArray().ForEach(u => repository.Delete(u));
                repository.Delete(role);
                work.AcceptChanges();
            }
            return true;
        }

        public override bool RoleExists(string roleName)
        {
            return repository.Roles.Any(r => r.Name.ToLower() == roleName.ToLower());
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            using (IUnitOfWork work = unitOfWorkProvider.Create())
            {
                foreach (string rn in roleNames)
                {
                    string roleName = rn;
                    Role role = repository.Roles.Include(r => r.Users).FirstOrDefault(r => r.Name.ToLower() == roleName);
                    if (role == null)
                    {
                        throw new InvalidOperationException("Role {0} not found.".FormatWith(roleName));
                    }
                    foreach (string un in usernames)
                    {
                        string userName = un;
                        if (role.Users.Any(u => u.UserName.ToLower() == userName.ToLower()))
                        {
                            throw new InvalidOperationException("User {0} is already a member of role {1}.".FormatWith(userName, roleName));
                        }
                        User user = repository.Users.FirstOrDefault(u => u.UserName.ToLower() == userName);
                        if (user == null)
                        {
                            throw new InvalidOperationException("User {0} not found.".FormatWith(userName));
                        }
                        role.Users.Add(user);
                    }
                    repository.Save(role);
                }
                work.AcceptChanges();
            }
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            using (IUnitOfWork work = unitOfWorkProvider.Create())
            {
                foreach (string rn in roleNames)
                {
                    string roleName = rn;
                    Role role = repository.Roles.Include(r => r.Users).FirstOrDefault(r => r.Name.ToLower() == roleName);
                    if (role == null)
                    {
                        throw new InvalidOperationException("Role {0} not found.".FormatWith(roleName));
                    }
                    foreach (string un in usernames)
                    {
                        string userName = un;
                        if (role.Users.All(u => u.UserName.ToLower() != userName.ToLower()))
                        {
                            throw new InvalidOperationException("User {0} is not a member of role {1}.".FormatWith(userName, roleName));
                        }
                        User user = repository.Users.FirstOrDefault(u => u.UserName.ToLower() == userName);
                        if (user == null)
                        {
                            throw new InvalidOperationException("User {0} not found.".FormatWith(userName));
                        }
                        role.Users.Remove(user);
                    }
                    repository.Save(role);
                }
                work.AcceptChanges();
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            return repository.Users.Where(u => u.Roles.Any(r => r.Name.ToLower() == roleName.ToLower())).Select(u => u.UserName).ToArray();
        }

        public override string[] GetAllRoles()
        {
            return repository.Roles.Select(r => r.Name).ToArray();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return repository.Users.Where(u =>
                                          u.UserName.ToLower().Contains(usernameToMatch.ToLower()) &&
                                          u.Roles.Any(r => r.Name.ToLower() == roleName)).Select(u => u.UserName).ToArray();
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            if (String.IsNullOrEmpty(name)) name = GetType().Name;

            base.Initialize(name, config);
        }
    }

    /// <summary>
    /// A default implementation of IAuthenticationService that uses the default MembershipProvider (a local implementation).
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        public virtual bool IsLoggedIn()
        {
            if (PrincipalContext.Current != null && PrincipalContext.Current.Principal != null)
            {
                var identity = PrincipalContext.Current.Principal.Identity as IHasId;
                if (identity != null && identity.Id != null)
                {
                    var user = ServiceProvider.Current.GetService<System.Web.Security.MembershipProvider>().GetUser(identity.Id, true);
                    return user != null && user.IsOnline;
                }
            }

            return false;
        }

        public virtual bool Login(string username, string password, string customCredential, bool isPersistent)
        {
            bool isValid = ValidateUser(username, password, customCredential);

            if (!isValid) return false;

            var provider = ServiceProvider.Current.GetService<System.Web.Security.MembershipProvider>();
            var user = (MembershipUser)provider.GetUser(username, false);
            if (user == null) throw new InvalidOperationException("User not found.");
            user.SetIsOnline(true);
            provider.UpdateUser(user);

            return true;
        }

        public virtual void Logout()
        {
            if (!IsLoggedIn()) return;
            if (PrincipalContext.Current != null && PrincipalContext.Current.Principal != null)
            {
                var identity = PrincipalContext.Current.Principal.Identity as IHasId;
                if (identity != null && identity.Id != null)
                {
                    var provider = ServiceProvider.Current.GetService<System.Web.Security.MembershipProvider>();
                    var user = (MembershipUser)provider.GetUser(identity.Id, true);
                    if (user != null)
                    {
                        user.SetIsOnline(false);
                        provider.UpdateUser(user);
                    }
                }
            }
        }       

        public virtual bool ValidateAssemblyNameAndVersion(string assemblyFullName)
        {
            return true;
        }

        public virtual bool ValidateUser(string username, string password, string customCredential)
        {
            return ServiceProvider.Current.GetService<System.Web.Security.MembershipProvider>().ValidateUser(username, password);
        }
    }
}