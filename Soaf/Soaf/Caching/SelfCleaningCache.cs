using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Soaf.ComponentModel;
using Soaf.Validation;

namespace Soaf.Caching
{
    /// <summary>
    ///   A simple self cleaning cache based on an underlying Dictionary store.
    /// </summary>    
    [Singleton]
    internal class SelfCleaningCache : ConcurrentDictionary<ICacheKeyDescriptor, ICachedValue>, ICache
    {
        [UsedImplicitly] private readonly Timer cleaningTimer;
        private bool isCleaning;

        public SelfCleaningCache()
        {
            // Run cleaning every 30 seconds
            cleaningTimer = new Timer(GoCleaning, null, TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(30));
        }

        /// <summary>
        /// Performs cleaning of cached values
        /// </summary>
        /// <param name="state"></param>
        private void GoCleaning(object state)
        {
            if (isCleaning) return;

            try
            {
                isCleaning = true;
                RemoveAll(kv => IsStale(kv.Value));
            }
            finally
            {
                isCleaning = false;
            }
        }

        /// <summary>
        /// Evaluates whether cached value is stale
        /// </summary>
        /// <param name="cachedValue"></param>
        /// <returns></returns>
        private bool IsStale(ICachedValue cachedValue)
        {
            var isStale = cachedValue.HoldInterval.IsExpired(DateTime.UtcNow - cachedValue.CachedOn);
            return isStale;
        }

        public ICachedValue GetOrAddValue(ICacheKeyDescriptor cacheKey, Func<ICachedValue> valueGetter)
        {
            return GetOrAdd(cacheKey, k => valueGetter());
        }

        public void UpdateValue(ICacheKeyDescriptor cacheKey, ICachedValue newValue)
        {
            TryUpdate(cacheKey, newValue, GetOrAdd(cacheKey, newValue));
        }

        public bool Contains(ICacheKeyDescriptor cacheKey)
        {
            return ContainsKey(cacheKey);
        }

        public void RemoveAll(Func<KeyValuePair<ICacheKeyDescriptor, ICachedValue>, bool> shouldRemoveCheck)
        {
            this.Where(shouldRemoveCheck)
                .Select(kv => kv.Key)
                .ToList()
                .ForEach(Remove);
        }

        public void Remove(ICacheKeyDescriptor cacheKey)
        {
            ICachedValue removed;
            TryRemove(cacheKey, out removed);
        }
    }
}