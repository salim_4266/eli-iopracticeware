﻿using System;
using System.Collections.Generic;
using Soaf.Caching;
using Soaf.ComponentModel;
using Soaf.Reflection;

[assembly: Component(typeof(CachingBehavior))]

namespace Soaf.Caching
{
    /// <summary>
    ///   Denotes that a method or property call should be cached on a class attributed with Caches.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
    public class CacheAttribute : Attribute
    {
        public CacheAttribute()
        {
            HoldInterval = CommonCacheHoldInterval.Long;
            PerInstance = false;
        }

        /// <summary>
        /// Specified how long a value should be held in cache. <code>CacheHoldInterval.XLong</code> by default
        /// </summary>
        public CommonCacheHoldInterval HoldInterval { get; set; }

        /// <summary>
        /// Configures whether caching of specified property or method is bound to instance being called
        /// By default - per class caching is used.
        /// </summary>
        public bool PerInstance { get; set; }
    }

    /// <summary>
    ///   Denotes that a type should automatically caching behavior for members annotated with CacheAttribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class SupportsCachingAttribute : ConcernAttribute
    {
        public SupportsCachingAttribute()
            : base(typeof(CachingBehavior))
        {
        }
    }

    /// <summary>
    ///   The implementation of automatic caching behavior for a type annotated with CachesAttribute and a member annotated with CacheAttribute.
    /// </summary>
    internal class CachingBehavior : AttributedMemberInterceptor
    {
        private readonly ICache cache;

        /// <summary>
        /// Required by IInterceptedMethodsFilter. Do not use
        /// </summary>
        public CachingBehavior()
        {}

        public CachingBehavior(ICache cache)
        {
            this.cache = cache;
        }

        public override IEnumerable<Type> AttributeTypes
        {
            get { yield return typeof(CacheAttribute); }
        }

        public override void Intercept(IInvocation invocation)
        {
            var configuration = invocation.Method.GetAttribute<CacheAttribute>();

            if (configuration == null)
            {
                configuration = invocation.Method.GetProperty().IfNotNull(pi => pi.GetAttribute<CacheAttribute>());
            }

            if (configuration == null) throw new InvalidOperationException("Could not find Cache attribute on method {0}.".FormatWith(invocation.Method.Name));

            var inputMethodCall = new CachedMethodCallDescriptor(
                configuration.PerInstance ? invocation.Target : null, 
                invocation.Method, invocation.Arguments);

            var executedOperation = false;
            Func<CachedValueWithUpdatedArguments> executeAndPrepareToCache = () =>
            {
                invocation.Proceed();
                executedOperation = true;
                var toCache = new CachedValueWithUpdatedArguments(invocation.ReturnValue, invocation.Arguments,
                    CacheHoldInterval.Common(configuration.HoldInterval));
                return toCache;
            };
            
            var cachedValue = cache.GetOrAddValue(inputMethodCall, executeAndPrepareToCache);

            // If same method has been cached by different means -> update value stored in cache
            if (!(cachedValue is CachedValueWithUpdatedArguments) && !executedOperation)
            {
                cachedValue = executeAndPrepareToCache();
                cache.UpdateValue(inputMethodCall, cachedValue);
            }

            // If we retrieved from cache -> restore value with arguments
            if (!executedOperation)
            {
                var cachedValueWithArguments = (CachedValueWithUpdatedArguments)cachedValue;
                invocation.ReturnValue = cachedValueWithArguments.Value;
                for (int i = 0; i < invocation.Arguments.Length; i++)
                {
                    invocation.Arguments[i] = cachedValueWithArguments.Arguments[i];
                }
            }
        }

        /// <summary>
        /// Allows to store a cached method result along with modified "ref" and "out" parameters
        /// </summary>
        private class CachedValueWithUpdatedArguments : CachedValue
        {
            public object[] Arguments { get; private set; }

            public CachedValueWithUpdatedArguments(object value, object[] arguments, CacheHoldInterval holdInterval) 
                : base(value, holdInterval)
            {
                Arguments = arguments;
            }
        }
    }
}