﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;

[assembly: Component(typeof(SelfCleaningCache), typeof(ICache))]

namespace Soaf.Caching
{
    public enum CommonCacheHoldInterval
    {
        Brief,
        Short,
        Long,
        XLong
    }

    public class CacheHoldInterval
    {
        public static readonly CacheHoldInterval Brief = new CacheHoldInterval(TimeSpan.FromSeconds(10));
        public static readonly CacheHoldInterval Short = new CacheHoldInterval(TimeSpan.FromMinutes(5));
        public static readonly CacheHoldInterval Long = new CacheHoldInterval(TimeSpan.FromHours(2));
        public static readonly CacheHoldInterval XLong = new CacheHoldInterval(TimeSpan.FromHours(19));

        private readonly TimeSpan lifeSpan;

        protected CacheHoldInterval(TimeSpan lifeSpan)
        {
            this.lifeSpan = lifeSpan;
        }

        public static CacheHoldInterval Common(CommonCacheHoldInterval interval)
        {
            switch (interval)
            {
                case CommonCacheHoldInterval.Brief:
                    return Brief;
                case CommonCacheHoldInterval.Short:
                    return Short;
                case CommonCacheHoldInterval.Long:
                    return Long;
                case CommonCacheHoldInterval.XLong:
                    return XLong;
                default:
                    throw new ArgumentOutOfRangeException("interval");
            }
        }

        public static CacheHoldInterval Custom(TimeSpan lifeSpan)
        {
            return new CacheHoldInterval(lifeSpan);
        }

        public bool IsExpired(TimeSpan currentHoldPeriod)
        {
            return currentHoldPeriod > lifeSpan;
        }
    }

    /// <summary>
    /// A key which uniquely identifies value stored in cache
    /// </summary>
    public interface ICacheKeyDescriptor
    {}

    /// <summary>
    /// Represents a cached entry
    /// </summary>
    public interface ICachedValue
    {
        DateTime CachedOn { get; }
        CacheHoldInterval HoldInterval { get; }
        object Value { get; }
    }

    /// <summary>
    ///   A definition for a type that provides caching.
    /// </summary>
    public interface ICache
    {
        ICachedValue GetOrAddValue(ICacheKeyDescriptor cacheKey, Func<ICachedValue> valueGetter);

        void UpdateValue(ICacheKeyDescriptor cacheKey, ICachedValue newValue);

        bool Contains(ICacheKeyDescriptor cacheKey);

        void RemoveAll(Func<KeyValuePair<ICacheKeyDescriptor, ICachedValue>, bool> shouldRemoveCheck);

        void Remove(ICacheKeyDescriptor cacheKey);

        void Clear();
    }

    /// <summary>
    /// Represents a simple cached value
    /// </summary>
    internal class CachedValue : ICachedValue
    {
        public CachedValue(object value, CacheHoldInterval holdInterval)
        {
            CachedOn = DateTime.UtcNow;
            HoldInterval = holdInterval;
            Value = value;
        }

        public DateTime CachedOn { get; private set; }
        public CacheHoldInterval HoldInterval { get; private set; }
        public object Value { get; private set; }
    }

    /// <summary>
    /// Represents a key for caching method call results
    /// </summary>
    internal class CachedMethodCallDescriptor : ICacheKeyDescriptor
    {
        private readonly object target;
        private readonly MethodInfo method;
        private readonly object[] arguments;

        public CachedMethodCallDescriptor(object target, MethodInfo method, object[] arguments)
        {
            this.target = target;
            this.method = method;
            this.arguments = new object[arguments.Length];
            arguments.CopyTo(this.arguments, 0);
        }

        public object[] Arguments
        {
            get { return arguments; }
        }

        public object Target
        {
            get { return target; }
        }

        public MethodInfo Method
        {
            get { return method; }
        }

        public override bool Equals(object obj)
        {
            return obj.As<CachedMethodCallDescriptor>()
                .IfNotNull(o => Equals(o.Target, Target)
                    && o.arguments.SequenceEqualWithNullCheck(arguments)
                    && Equals(o.Method, Method));
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                hash = hash * 23 + Objects.GetHashCodeWithNullCheck(Target);
                hash = hash * 23 + Objects.GetHashCodeWithNullCheck(Method);
                hash = hash * 23 + arguments.GetSequenceHashCode();
                return hash;
            }
        }
    }
}