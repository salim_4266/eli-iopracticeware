﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Soaf.Linq.Expressions;
using Soaf.Reflection;

namespace Soaf.Caching
{
    public static class OperationCache
    {
        /// <summary>
        /// Removes all operation cache entries cached for specified instance
        /// </summary>
        /// <param name="instance"></param>
        public static void ClearInstanceCache(this object instance)
        {
            // Get current ICache instance
            var cache = ServiceProvider.Current.GetService<ICache>();

            // Remove all entries for specified instance
            cache.RemoveAll(kv =>
            {
                var cachedMethodCall = kv.Key as CachedMethodCallDescriptor;
                return cachedMethodCall != null && cachedMethodCall.Target == instance;
            });
        }

        /// <summary>
        /// Executes a cached non-instance specific operation.
        /// Supported: static method, extension method (instance is ignored), instance method call (instance is ignored). Otherwise -> instance is considered a method parameter
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="action">The action to execute. "out" and "ref" parameters caching is not supported.</param>
        /// <param name="holdInterval">The value hold interval. Defaults to Long</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException">Action signature is not supported by this caching operation</exception>
        public static TValue ExecuteCached<TValue>(Expression<Func<TValue>> action, CacheHoldInterval holdInterval = null)
        {
            return ExecuteCachedInternal((object)null, action.Body as MethodCallExpression, holdInterval ?? CacheHoldInterval.Long, () => action.Compile().Invoke());
        }

        /// <summary>
        /// Executes a cached non-instance specific operation. Action must be a method call or extension method call, ex: instance.GetValue(param1).
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="instance">The target.</param>
        /// <param name="action">The action to execute. "out" and "ref" parameters caching is not supported.</param>
        /// <param name="holdInterval">The value hold interval. Defaults to Long</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException">Action signature is not supported by this caching operation</exception>
        public static TValue ExecuteCached<TInstance, TValue>(this TInstance instance, Expression<Func<TInstance, TValue>> action, CacheHoldInterval holdInterval = null)
        {
            return ExecuteCachedInternal((object)null, action.Body as MethodCallExpression, holdInterval ?? CacheHoldInterval.Long, () => action.Compile().Invoke(instance));
        }

        /// <summary>
        /// Executes a cached operation over current instance. Action must be a method call or extension method call, ex: instance.GetValue(param1).
        /// </summary>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="instance">The target.</param>
        /// <param name="action">The action to execute. "out" and "ref" parameters caching is not supported.</param>
        /// <param name="holdInterval">The value hold interval.</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException">Action signature is not supported by this caching operation</exception>
        public static TValue ExecuteInstanceCached<TInstance, TValue>(this TInstance instance, Expression<Func<TInstance, TValue>> action, CacheHoldInterval holdInterval = null)
        {
            return ExecuteCachedInternal(instance, action.Body as MethodCallExpression, holdInterval ?? CacheHoldInterval.Short, () => action.Compile().Invoke(instance));
        }

        private static TResult ExecuteCachedInternal<TTarget, TResult>(TTarget target, MethodCallExpression methodCall, CacheHoldInterval holdInterval, Func<TResult> evaluateResult)
        {
            if (methodCall == null)
            {
                throw new InvalidOperationException("Action signature is not supported by this caching operation");
            }

            IEnumerable<Expression> argumentsQuery = methodCall.Arguments;

            // Exclude extension method target (per instance = target; per class -> need to exclude)
            if (methodCall.Method.HasAttribute<ExtensionAttribute>())
            {
                argumentsQuery = argumentsQuery.Skip(1);
            }

            // Evaluate current method arguments
            object[] arguments = argumentsQuery
                .Select(a => a.Compile<Func<object>>().Invoke())
                .ToArray();

            var inputMethodCall = new CachedMethodCallDescriptor(target, methodCall.Method, arguments);

            // Get current ICache instance
            var cache = ServiceProvider.Current.GetService<ICache>();
            var cachedValue = cache.GetOrAddValue(inputMethodCall, () =>
            {
                var newValue = evaluateResult();
                return new CachedValue(newValue, holdInterval);
            });

            return (TResult)cachedValue.Value;
        }
    }
}
