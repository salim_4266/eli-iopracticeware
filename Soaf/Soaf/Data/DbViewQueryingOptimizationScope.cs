﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using Soaf.Caching;
using Soaf.Collections;

namespace Soaf.Data
{
    /// <summary>
    /// Optimizes querying commands which involve querying slow views
    /// </summary>
    public class DbViewQueryingOptimizationScope : IDisposable
    {
        private const string DefaultViewSchemasToProcess = "model|dbo";

        private readonly bool reuseCachedData;
        private readonly bool fastDataCopy;
        private readonly TimeSpan cacheTimeout;
        private readonly string viewSchemasRegex;
        private readonly HashSet<int> attachedThreads = new HashSet<int>();
        private readonly ConcurrentDictionary<string, DatabaseViewsCache> scopeConnectionCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbViewQueryingOptimizationScope"/> class.
        /// </summary>
        /// <param name="reuseCachedData">if set to <c>true</c> allows reuse of previously cached data (might read stale).</param>
        /// <param name="cacheTimeout">specified how long reusable cached should be available</param>
        /// <param name="fastDataCopy">copies data faster, but uncommitted changes might be included into temp table</param>
        /// <param name="viewSchemasToProcess">View schemas to check (such as "model" and "dbo").</param>
        public DbViewQueryingOptimizationScope(bool reuseCachedData = false, TimeSpan? cacheTimeout = null, bool fastDataCopy = false, params string[] viewSchemasToProcess)
        {
            this.reuseCachedData = reuseCachedData;
            this.fastDataCopy = fastDataCopy;
            this.cacheTimeout = cacheTimeout ?? (reuseCachedData 
                ? TimeSpan.FromMinutes(10) // Will be cleaned up after timing out
                : TimeSpan.FromHours(1)); // Will be cleaned up via Dispose


            viewSchemasRegex = viewSchemasToProcess != null && viewSchemasToProcess.Length > 0
                ? string.Join("|", viewSchemasToProcess)
                : DefaultViewSchemasToProcess;

            if (!this.reuseCachedData)
            {
                scopeConnectionCache = new ConcurrentDictionary<string, DatabaseViewsCache>(StringComparer.OrdinalIgnoreCase);
            }

            // Attach to current thread
            AttachScopeToThread(Thread.CurrentThread);

            // Inject into Ado Service command execution
            AdoService.CommandExecuting += OnCommandExecuting;
            AdoService.CommandExecuted += OnCommandExecuted;
        }

        /// <summary>
        /// Enables scope optimizations on threads other than it has been initialized on
        /// </summary>
        /// <param name="executionThread"></param>
        public void AttachScopeToThread(Thread executionThread)
        {
            attachedThreads.Add(executionThread.ManagedThreadId);
        }

        /// <summary>
        /// Disables scope to perform optimizations for command executed on specified thread
        /// </summary>
        /// <param name="executionThread"></param>
        public void DetachScopeFromThread(Thread executionThread)
        {
            attachedThreads.Remove(executionThread.ManagedThreadId);
        }

        public void Dispose()
        {
            // Cleanup
            AdoService.CommandExecuted -= OnCommandExecuted;
            AdoService.CommandExecuting -= OnCommandExecuting;
            attachedThreads.Clear();

            if (scopeConnectionCache != null && scopeConnectionCache.Count > 0)
            {
                foreach (var connectionCacheItem in scopeConnectionCache.Values)
                {
                    try
                    {
                        connectionCacheItem.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceWarning("Failed to cleanup. Error: {0}", ex);
                    }

                }
                scopeConnectionCache.Clear();
            }
        }

        private bool ShouldIntercept(IDbCommand command)
        {
            return command.Connection is SqlConnection // Support only optimizations when connected to SqlServer
                && attachedThreads.Contains(Thread.CurrentThread.ManagedThreadId)
                && command.CommandType == CommandType.Text; // Skipping stored procedure and direct table commands
        }

        private void OnCommandExecuted(object sender, DbCommandExecutedEventArgs e)
        {
            if (!ShouldIntercept(e.DbCommand)) return;

            // We don't yet know why, but sometimes global temp table disappears while executing optimized command
            var sqlException = e.Exception.IfNotNull(p => p.SearchFor<SqlException>());
            var failedTempTable = sqlException.IfNotNull(se => Regex
                .Match(se.Message, "Invalid object name '##(?<ObjectName>.*)'", RegexOptions.IgnoreCase)
                .Groups["ObjectName"]);

            if (failedTempTable != null && failedTempTable.Success)
            {
                // Looks like cache is stale -> reset and then retry original query
                var connectionCacheItem = RetrieveConnectionCache(e.ConnectionString, e.DbCommand.Connection.Database);
                connectionCacheItem.InvalidateCacheForTempTable(failedTempTable.Value);
                e.RetryFailure = true;
            }
        }

        private void OnCommandExecuting(object sender, DbCommandExecutingEventArgs e)
        {
            if (!ShouldIntercept(e.DbCommand)) return;

            var connectionCacheItem = RetrieveConnectionCache(e.ConnectionString, e.DbCommand.Connection.Database);
            connectionCacheItem.OptimizeCommand(e.DbCommand, viewSchemasRegex, fastDataCopy);
        }

        private DatabaseViewsCache RetrieveConnectionCache(string connectionString, string database)
        {
// Ensure connection string will have a correct current database
            var connectionBuilder = new SqlConnectionStringBuilder(connectionString)
            {
                InitialCatalog = database
            };
            var modifiedConnectionString = connectionBuilder.ToString();

            // When reusing cached data -> use global cache, otherwise - local
            var connectionCacheItem = reuseCachedData
                ? this.ExecuteCached(s => s.GetNewCacheItemForConnection(modifiedConnectionString, cacheTimeout), CacheHoldInterval.Custom(cacheTimeout))
                : scopeConnectionCache.GetValue(modifiedConnectionString, () => GetNewCacheItemForConnection(modifiedConnectionString, cacheTimeout));
            return connectionCacheItem;
        }

        /// <summary>
        /// Returns new instance of per connection cache storage
        /// </summary>
        /// <returns></returns>
        private DatabaseViewsCache GetNewCacheItemForConnection(string connectionString, TimeSpan cacheTimeout)
        {
            var connectionCache = new DatabaseViewsCache(connectionString, cacheTimeout, !reuseCachedData);
            return connectionCache;
        }
    }
}