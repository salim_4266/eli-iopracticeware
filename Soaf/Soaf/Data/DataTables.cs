﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using Soaf.Reflection;

namespace Soaf.Data
{
    /// <summary>
    ///   Helper/extension methods for DataTables.
    /// </summary>
    public static class DataTables
    {
        /// <summary>
        ///   Gets the value of the specified type from the specified column in the data row.
        /// </summary>
        /// <typeparam name="TValue"> The type of the value. </typeparam>
        /// <param name="row"> The row. </param>
        /// <param name="columnName"> Name of the column. </param>
        /// <returns> </returns>
        public static TValue Get<TValue>(this DataRow row, string columnName)
        {
            if (row == null) throw new ArgumentNullException("row");
            if (String.IsNullOrWhiteSpace(columnName)) throw new ArgumentNullException("columnName");

            object value = row[columnName];
            if (value == DBNull.Value) return default(TValue);
            if (value is TValue) return (TValue)value;
            if (value is IConvertible) return (TValue)Convert.ChangeType(value, typeof(TValue));
            if (value == null) return default(TValue);

            throw new InvalidOperationException("Value of type {0} is not convertible to {1}.".FormatWith(value.GetType().Name, typeof(TValue).Name));
        }

        /// <summary>
        /// Converts the source collection to a DataTable.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static DataTable AsDataTable<T>(this IEnumerable<T> source)
        {
            var dt = new DataTable();

            var getters = new List<Invoker>();

            foreach (var property in typeof(T).GetProperties().Where(p => p.CanRead && p.GetGetMethod() != null
                                                                          && !Reflector.HasAttribute<AssociationAttribute>(p)
                                                                          && !Types.IsGenericTypeFor(p.PropertyType, typeof(ICollection<>))))
            {
                dt.Columns.Add(property.Name, property.PropertyType.AsUnderlyingTypeIfNullable());
                getters.Add(property.GetGetMethod().GetInvoker());
            }


            foreach (var i in source)
            {
                var item = i;
                dt.Rows.Add(getters.Select(g => g(item)).ToArray());
            }

            return dt;
        }

        public static DataTable RemoveColumns(this DataTable dataTable, params string[] columnNames)
        {
            foreach (var columnName in columnNames)
            {
                dataTable.Columns.Remove(columnName);
            }
            return dataTable;
        }
    }
}