﻿using System.Data;

namespace Soaf.Data
{
    /// <summary>
    /// A non-DbConnection IDbConnection that wraps another IDbConnection. Ensures COM visibility.
    /// </summary>
    public class WrapperDbConnection : IDbConnection
    {
        private readonly IDbConnection innerConnection;

        public IDbConnection InnerConnection
        {
            get { return innerConnection; }
        }

        public WrapperDbConnection(IDbConnection innerConnection)
        {
            this.innerConnection = innerConnection;
        }

        public void Dispose()
        {
            innerConnection.Dispose();
        }

        public IDbTransaction BeginTransaction()
        {
            return innerConnection.BeginTransaction();
        }

        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            return innerConnection.BeginTransaction(il);
        }

        public void Close()
        {
            innerConnection.Close();
        }

        public void ChangeDatabase(string databaseName)
        {
            innerConnection.ChangeDatabase(databaseName);
        }

        public IDbCommand CreateCommand()
        {
            return innerConnection.CreateCommand();
        }

        public void Open()
        {
            innerConnection.Open();
        }

        public string ConnectionString
        {
            get { return innerConnection.ConnectionString; }
            set { innerConnection.ConnectionString = value; }
        }

        public int ConnectionTimeout
        {
            get { return innerConnection.ConnectionTimeout; }
        }

        public string Database
        {
            get { return innerConnection.Database; }
        }

        public ConnectionState State
        {
            get { return innerConnection.State; }
        }
    }
}