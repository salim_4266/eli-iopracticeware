﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Transactions;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Reflection;

namespace Soaf.Data
{
    /// <summary>
    ///   Helper/extension methods for the System.Data DbConnections.
    /// </summary>
    public static class DbConnections
    {
        /// <summary>
        /// Executes the non query command asynchronously and performs a callback.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="callback">The callback.</param>
        public static void ExecuteNonQueryAsync(this IDbCommand command, Action<int> callback = null)
        {
            if (callback == null) callback = i => { };
            Task.Factory.StartNew(() => callback(command.ExecuteNonQuery()));
        }

        /// <summary>
        /// Executes the specified sql using the connection, then disposes the connection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="commandTimeout">The command timeout.</param>
        /// <returns></returns>
        public static T ExecuteAndDispose<T>(this IDbConnection connection, string sql, IDbTransaction transaction, Dictionary<string, object> parameters = null, int commandTimeout = 1200)
        {
            Func<IDbCommand> createCommand = () =>
                                             {
                                                 var cmd = connection.CreateCommand();
                                                 cmd.Connection = connection;
                                                 cmd.CommandTimeout = commandTimeout;
                                                 cmd.Transaction = transaction;
                                                 return cmd;
                                             };

            return connection.ExecuteAndDispose<T>(sql, parameters, createCommand);
        }

        /// <summary>
        ///   Executes the specified sql using the connection, then disposes the connection.
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="connection"> The connection. </param>
        /// <param name="sql"> The SQL. </param>
        /// <param name="parameters"> The parameters. </param>
        /// <param name="createCommand"> The create command. </param>
        /// <returns> </returns>
        public static T ExecuteAndDispose<T>(this IDbConnection connection, string sql, Dictionary<string, object> parameters = null, Func<IDbCommand> createCommand = null)
        {
            return connection.Execute<T>(sql, parameters, true);
        }

        /// <summary>
        /// Executes the specified sql using the connection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="dispose">if set to <c>true</c> [dispose].</param>
        /// <param name="commandTimeout">The command timeout.</param>
        /// <returns></returns>
        public static T Execute<T>(this IDbConnection connection, string sql, IDbTransaction transaction, Dictionary<string, object> parameters = null, bool dispose = false, int commandTimeout = 1200)
        {
            Func<IDbCommand> createCommand = () =>
                                             {
                                                 var cmd = connection.CreateCommand();
                                                 cmd.Connection = connection;
                                                 cmd.CommandTimeout = commandTimeout;
                                                 cmd.Transaction = transaction;
                                                 return cmd;
                                             };

            return connection.Execute<T>(sql, parameters, dispose, createCommand);
        }

        /// <summary>
        ///   Executes the specified sql using the connection.
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="connection"> The connection. </param>
        /// <param name="sql"> The SQL. </param>
        /// <param name="parameters"> The parameters. </param>
        /// <param name="dispose"> if set to <c>true</c> [dispose]. </param>
        /// <param name="createCommand"> The create command. </param>
        /// <returns> </returns>
        public static T Execute<T>(this IDbConnection connection, string sql, Dictionary<string, object> parameters = null, bool dispose = false, Func<IDbCommand> createCommand = null)
        {
            try
            {
                if (createCommand == null)
                    createCommand = () =>
                                    {
                                        // ReSharper disable AccessToDisposedClosure
                                        IDbCommand result = connection.CreateCommand();
                                        // ReSharper restore AccessToDisposedClosure
                                        result.CommandTimeout = 1200;
                                        return result;
                                    };

                if (connection.State != ConnectionState.Open) connection.Open();

                using (IDbCommand command = createCommand())
                {
                    command.Connection = connection;
                    command.CommandText = sql;

                    if (parameters != null)
                    {
                        foreach (var item in parameters)
                        {
                            IDbDataParameter parameter = command.CreateParameter();
                            parameter.ParameterName = item.Key;
                            parameter.Value = item.Value;
                            command.Parameters.Add(parameter);
                        }
                    }

                    if (typeof(T) == typeof(DataTable))
                    {
                        var dt = new DataTable();
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            // ReSharper disable ConditionIsAlwaysTrueOrFalse
                            if (reader != null) dt.Load(reader);
                            // ReSharper restore ConditionIsAlwaysTrueOrFalse
                            return (T)(object)dt;
                        }
                    }
                    if (typeof(T) == typeof(DataSet))
                    {
                        var tables = new List<DataTable>();
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            int i = 0;

                            // ReSharper disable ConditionIsAlwaysTrueOrFalse
                            while (reader != null && !reader.IsClosed)
                                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                            {
                                var dt = new DataTable("Result_{0}".FormatWith(++i));
                                dt.Load(reader);
                                tables.Add(dt);
                            }
                            var ds = new DataSet("Results");
                            ds.Tables.AddRange(tables.ToArray());
                            return (T)(object)ds;
                        }
                    }
                    object result = command.ExecuteScalar();
                    if (result == null || result == DBNull.Value) return default(T);
                    return result.ConvertTo<T>();
                }
            }
            finally
            {
                if (dispose) connection.Dispose();
            }
        }

        /// <summary>
        /// Executes the specified sql using the connection, then disposes the connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="commandTimeout">The command timeout.</param>
        public static void ExecuteAndDispose(this IDbConnection connection, string sql, IDbTransaction transaction, Dictionary<string, object> parameters = null, int commandTimeout = 1200)
        {
            Func<IDbCommand> createCommand = () =>
                                             {
                                                 var cmd = connection.CreateCommand();
                                                 cmd.Connection = connection;
                                                 cmd.CommandTimeout = commandTimeout;
                                                 cmd.Transaction = transaction;
                                                 return cmd;
                                             };

            connection.ExecuteAndDispose(sql, parameters, createCommand);
        }

        /// <summary>
        ///   Executes the specified sql using the connection, then disposes the connection.
        /// </summary>
        /// <param name="connection"> The connection. </param>
        /// <param name="sql"> The SQL. </param>
        /// <param name="parameters"> The parameters. </param>
        /// <param name="createCommand"> The create command. </param>
        public static void ExecuteAndDispose(this IDbConnection connection, string sql, Dictionary<string, object> parameters = null, Func<IDbCommand> createCommand = null)
        {
            connection.Execute(sql, parameters, true);
        }

        /// <summary>
        /// Executes the specified sql using the connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="dispose">if set to <c>true</c> [dispose].</param>
        /// <param name="commandTimeout">The command timeout.</param>
        public static void Execute(this IDbConnection connection, string sql, IDbTransaction transaction, Dictionary<string, object> parameters = null, bool dispose = false, int commandTimeout = 1200)
        {
            Func<IDbCommand> createCommand = () =>
                                             {
                                                 var cmd = connection.CreateCommand();
                                                 cmd.Connection = connection;
                                                 cmd.CommandTimeout = commandTimeout;
                                                 cmd.Transaction = transaction;
                                                 return cmd;
                                             };
            connection.Execute(sql, parameters, dispose, createCommand);
        }

        /// <summary>
        ///   Executes the specified sql using the connection.
        /// </summary>
        /// <param name="connection"> The connection. </param>
        /// <param name="sql"> The SQL. </param>
        /// <param name="parameters"> The parameters. </param>
        /// <param name="dispose"> if set to <c>true</c> [dispose]. </param>
        /// <param name="createCommand"> The create command. </param>
        public static void Execute(this IDbConnection connection, string sql, Dictionary<string, object> parameters = null, bool dispose = false, Func<IDbCommand> createCommand = null)
        {

            try
            {
                if (createCommand == null)
                    createCommand = () =>
                                    {
                                        // ReSharper disable AccessToDisposedClosure
                                        IDbCommand result = connection.CreateCommand();
                                        // ReSharper restore AccessToDisposedClosure
                                        result.CommandTimeout = 1200;
                                        return result;
                                    };

                if (connection.State != ConnectionState.Open) connection.Open();

                using (IDbCommand command = createCommand())
                {
                    command.Connection = connection;
                    command.CommandText = sql;

                    if (parameters != null)
                    {
                        foreach (var item in parameters)
                        {
                            IDbDataParameter parameter = command.CreateParameter();
                            parameter.ParameterName = item.Key;
                            parameter.Value = item.Value;
                            command.Parameters.Add(parameter);
                        }
                    }

                    command.ExecuteNonQuery();
                }
            }
            finally
            {
                if (dispose) connection.Dispose();
            }
        }

        /// <summary>
        /// Runs a multi-batch script.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="dispose">if set to <c>true</c> [dispose].</param>
        /// <param name="commandTimeout">The command timeout.</param>
        /// <param name="useSingleCommand">if set to <c>true</c> uses a single command by wrapping blocks inside EXEC statements.</param>
        public static void RunScript(this IDbConnection connection, string sql, IDbTransaction transaction, bool dispose = false, int commandTimeout = 1200, bool useSingleCommand = false)
        {
            Func<IDbCommand> createCommand = () =>
                                             {
                                                 var cmd = connection.CreateCommand();
                                                 cmd.Connection = connection;
                                                 cmd.CommandTimeout = commandTimeout;
                                                 cmd.Transaction = transaction;
                                                 return cmd;
                                             };

            connection.RunScript(sql, false, dispose, createCommand, useSingleCommand);
        }

        /// <summary>
        /// Runs a multi-batch script.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="useTransaction">if set to <c>true</c> [use transaction].</param>
        /// <param name="dispose">if set to <c>true</c> [dispose].</param>
        /// <param name="createCommand">The create command.</param>
        /// <param name="useSingleCommand">if set to <c>true</c> uses a single command by wrapping blocks inside EXEC statements.</param>
        public static void RunScript(this IDbConnection connection, string sql, bool useTransaction = true, bool dispose = false, Func<IDbCommand> createCommand = null, bool useSingleCommand = false)
        {
            try
            {
                if (useTransaction)
                {
                    using (var ts = new TransactionScope())
                    {
                        RunScriptInternal(connection, sql, createCommand);
                        ts.Complete();
                    }
                }
                else
                {
                    RunScriptInternal(connection, sql, createCommand, useSingleCommand);
                }
            }
            finally
            {
                if (dispose) connection.Dispose();
            }
        }

        /// <summary>
        /// Runs a multi-batch script, then disposes the connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="commandTimeout">The command timeout.</param>
        /// <param name="useSingleCommand">if set to <c>true</c> uses a single command by wrapping blocks inside EXEC statements.</param>
        public static void RunScriptAndDispose(this IDbConnection connection, string sql, IDbTransaction transaction, int commandTimeout = 1200, bool useSingleCommand = false)
        {
            Func<IDbCommand> createCommand = () =>
                                             {
                                                 var cmd = connection.CreateCommand();
                                                 cmd.Connection = connection;
                                                 cmd.CommandTimeout = commandTimeout;
                                                 cmd.Transaction = transaction;
                                                 return cmd;
                                             };
            connection.RunScriptAndDispose(sql, false, createCommand, useSingleCommand);
        }

        /// <summary>
        /// Runs a multi-batch script, then disposes the connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="sql">The SQL.</param>
        /// <param name="useTransaction">if set to <c>true</c> [use transaction].</param>
        /// <param name="createCommand">The create command.</param>
        /// <param name="useSingleCommand">if set to <c>true</c> uses a single command by wrapping blocks inside EXEC statements.</param>
        public static void RunScriptAndDispose(this IDbConnection connection, string sql, bool useTransaction = true, Func<IDbCommand> createCommand = null, bool useSingleCommand = false)
        {
            connection.RunScript(sql, useTransaction, true, createCommand, useSingleCommand);
        }

        private static void RunScriptInternal(this IDbConnection connection, string sql, Func<IDbCommand> createCommand = null, bool useSingleCommand = false)
        {
            if (string.IsNullOrWhiteSpace(sql)) return;

            if (createCommand == null)
                createCommand = () =>
                                {
                                    IDbCommand result = connection.CreateCommand();
                                    result.CommandTimeout = 1200;
                                    return result;
                                };

            if (connection.State != ConnectionState.Open) connection.Open();

            using (IDbCommand command = createCommand())
            {
                command.Connection = connection;

                var splitter = new[] { "GO" + Environment.NewLine, Environment.NewLine + "GO", "go" + Environment.NewLine, Environment.NewLine + "go", "Go" + Environment.NewLine, Environment.NewLine + "Go" };

                if (!useSingleCommand)
                {
                    foreach (string statement in sql.Split(splitter, StringSplitOptions.RemoveEmptyEntries))
                    {
                        command.CommandText = statement;

                        command.ExecuteNonQuery();
                    }
                }
                else
                {
                    var wrappedSql = sql.Split(splitter, StringSplitOptions.RemoveEmptyEntries)
                        .Select(s => "EXEC('{0}')".FormatWith(s.Replace("'", "''")))
                        .Join(Environment.NewLine);

                    command.CommandText = wrappedSql;

                    command.ExecuteNonQuery();
                }

            }
        }

        /// <summary>
        /// Maps a DataRow to an arbitrary class whose property names matches the respective column names.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataRow"></param>
        /// <returns></returns>
        public static T MapTo<T>(this DataRow dataRow)
        {
            var instance = (T)typeof(T).CreateInstance();

            foreach (var c in dataRow.Table.Columns.OfType<DataColumn>())
            {
                instance.Invoke("set_" + c.ColumnName, dataRow[c]);
            }
            return instance;
        }

        /// <summary>
        /// Tries to get a connection string builder for the specified connection string.
        /// </summary>
        /// <typeparam name="TConnectionStringBuilder">The type of the connection string builder.</typeparam>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public static TConnectionStringBuilder TryGetConnectionStringBuilder<TConnectionStringBuilder>(string connectionString) where TConnectionStringBuilder : DbConnectionStringBuilder, new()
        {
            TConnectionStringBuilder result;
            if (TryGetConnectionStringBuilder(connectionString, out result))
            {
                return result;
            }
            return null;
        }

        /// <summary>
        /// Tries to get a connection string builder for the specified connection string.
        /// </summary>
        /// <typeparam name="TConnectionStringBuilder">The type of the connection string builder.</typeparam>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="connectionStringBuilder">The connection string builder.</param>
        /// <returns></returns>
        [DebuggerNonUserCode]
        public static bool TryGetConnectionStringBuilder<TConnectionStringBuilder>(string connectionString, out TConnectionStringBuilder connectionStringBuilder) where TConnectionStringBuilder : DbConnectionStringBuilder, new()
        {
            if (connectionString.IsNullOrEmpty() || (typeof(SqlConnectionStringBuilder) == typeof(TConnectionStringBuilder) && !connectionString.Contains(";")))
            {
                // short circuit for speed
                connectionStringBuilder = null;
                return false;
            }

            try
            {
                connectionStringBuilder = new TConnectionStringBuilder();
                connectionStringBuilder.ConnectionString = connectionString;
                return true;
            }
            catch
            {
                connectionStringBuilder = null;
                return false;
            }
        }


        /// <summary>
        /// Ensures the machine accounts have access to the SQL database.
        /// </summary>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="account">The account.</param>
        public static void EnsureMachineAccountAccessToSqlServerDatabase(this SqlConnection sqlConnection, string account)
        {
            string sql =
                @"
BEGIN TRY
IF NOT EXISTS(SELECT name FROM master.dbo.syslogins WHERE name = '{0}')
BEGIN
    EXEC sp_grantlogin '{0}'
END
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = '{1}')
BEGIN
	EXEC sp_grantdbaccess '{0}', '{1}'
	EXEC sp_addrolemember 'db_owner', '{1}'
END
END TRY
BEGIN CATCH
END CATCH
".FormatWith(account, account.Split('\\').Last());

            if (sqlConnection != null)
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandText = sql;
                    command.ExecuteNonQuery();
                }

            }
        }

        /// <summary>
        /// Puts the user id on the principal or the identity (whichever implements IHasId) in a temp table called #UserContext.
        /// </summary>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="principal">The principal.</param>
        public static void SetUserContext(this SqlConnection sqlConnection, IPrincipal principal)
        {
            var sql = GetSetUserContextSql(principal);

            if (sqlConnection != null && sql != null)
            {
                if (sqlConnection.State == ConnectionState.Closed || sqlConnection.State == ConnectionState.Broken) sqlConnection.Open();
                using (DbCommand command = sqlConnection.CreateCommand())
                {
                    command.CommandText = sql;
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }

            }
        }

        /// <summary>
        /// Returns the raw sql to insert the current principal's user id and time zone id (if one exists) into the #UserContext temp table.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns></returns>
        internal static string GetSetUserContextSql(IPrincipal principal)
        {
            if (principal != null)
            {
                var hasId = principal.As<IHasId>() ?? principal.Identity.As<IHasId>();
                var hasTimeZoneId = principal.As<IHasTimeZoneId>() ?? principal.Identity.As<IHasTimeZoneId>();

                var userId = hasId != null && hasId.Id != null ? hasId.Id.ToString() : null;
                if (!userId.IsNullOrEmpty())
                {
                    string sql = @"
IF OBJECT_ID('tempdb..#UserContext') IS NOT NULL
BEGIN
   DROP TABLE #UserContext
END
CREATE TABLE #UserContext(UserId nvarchar(max), ClientTimeZoneOffset int null);
INSERT INTO #UserContext(UserId, ClientTimeZoneOffset) VALUES ('{0}', {1})
";
                    var utcOffset = new int?();
                    var timeZoneId = hasTimeZoneId.IfNotNull(i => i.TimeZoneId);
                    if (!timeZoneId.IsNullOrEmpty())
                    {
                        try
                        {
                            utcOffset = Convert.ToInt32(TimeZoneInfo.FindSystemTimeZoneById(timeZoneId).GetUtcOffset(DateTime.UtcNow).TotalMinutes);
                        }
                        catch (Exception ex)
                        {
                            if (!(ex is TimeZoneNotFoundException || ex is InvalidTimeZoneException))
                            {
                                throw;
                            }
                        }
                    }

                    return sql.FormatWith(userId, utcOffset.HasValue ? utcOffset.Value.ToString() : "NULL");

                }
            }
            return null;
        }

    }
}