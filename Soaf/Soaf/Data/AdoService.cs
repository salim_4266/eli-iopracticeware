﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading;
using System.Transactions;
using System.Xml;
using System.Xml.Linq;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;
using Soaf.Linq;
using Soaf.Logging;
using Soaf.Security;
using Soaf.Xml;
using IsolationLevel = System.Data.IsolationLevel;

[assembly: Component(typeof(AdoService), typeof(IAdoService))]
[assembly: Component(typeof(AdoServiceProviderFactoryRegistrar), Initialize = true)]

namespace Soaf.Data
{
    /// <summary>
    /// A service interface for executing ADO .NET commands.
    /// </summary>
    [SupportsMethodCallTracingAttribute(Source = "AdoService", EnableForAll = true)]
    public interface IAdoService
    {
        IEnumerable<AdoServiceCommandExecutionResult> ExecuteCommands(IEnumerable<AdoServiceCommandExecutionArgument> commands);

        void ExecuteBulkCopy(AdoServiceBulkCopyExecutionArgument argument);

        DataSet GetSchema(string connectionString, string collectionName, string[] restrictionValues);

        string ResolveConnectionString(string connectionString);

        /// <summary>
        /// Derives the command parameters using the configured DbCommandBuilder.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        AdoServiceCommandExecutionArgument DeriveCommandParameters(AdoServiceCommandExecutionArgument command);
    }

    public class AdoService : IAdoService
    {
        private readonly IConnectionStringRepository connectionStringRepository;

        public static event EventHandler<DbCommandExecutingEventArgs> CommandExecuting;
        public static event EventHandler<DbCommandExecutedEventArgs> CommandExecuted;

        private static readonly IDictionary<Guid, object> SyncRoots = new Dictionary<Guid, object>();

        private static readonly Func<Exception, bool> ShouldRetry = ex => ex is SqlException && ex.Message == "Transaction context in use by another session.";

        private static object CurrentSyncRoot
        {
            get
            {
                if (Transaction.Current != null && Transaction.Current.TransactionInformation.DistributedIdentifier != default(Guid))
                {
                    var key = Transaction.Current.TransactionInformation.DistributedIdentifier;
                    return SyncRoots.GetValue(
                        key,
                        () =>
                        {
                            var o = new object();
                            Transaction.Current.TransactionCompleted += (sender, e) => { lock (SyncRoots) SyncRoots.Remove(key); };
                            return o;
                        });
                }
                return null;
            }
        }

        public AdoService(IConnectionStringRepository connectionStringRepository)
        {
            this.connectionStringRepository = connectionStringRepository;
        }

        private DisposableScope<SqlConnection> GetSqlConnection(string connectionString, string database)
        {
            // Ensure pointing to right database
            var connectionStringBuilder = new SqlConnectionStringBuilder(connectionString);
            if (!string.IsNullOrEmpty(database))
            {
                connectionStringBuilder.InitialCatalog = database;
            }

            var connection = new SqlConnection(connectionStringBuilder.ConnectionString);

            var syncRoot = CurrentSyncRoot ?? Transaction.Current.IfNotNull(t => t.TransactionInformation);
            if (syncRoot != null) lock (syncRoot) RetryUtility.ExecuteWithRetry(connection.Open, 10, TimeSpan.FromMilliseconds(10), ShouldRetry);
            else RetryUtility.ExecuteWithRetry(connection.Open, 10, TimeSpan.FromMilliseconds(10), ShouldRetry);

            return DisposableScope.Create(connection, null, i =>
            {
                var disposingSyncRoot = CurrentSyncRoot ?? Transaction.Current.IfNotNull(t => t.TransactionInformation);
                if (disposingSyncRoot != null) lock (disposingSyncRoot) connection.Dispose();
                else connection.Dispose();
            });
        }

        private AdoServiceCommandExecutionDataTableResult ExecuteSqlCommandDataTable(SqlCommand sqlCommand)
        {
            var result = new AdoServiceCommandExecutionDataTableResult();
            using (var sqlDataReader = sqlCommand.ExecuteReader())
            {
                var table = new DataTable("Result");
                table.Load(sqlDataReader);
                result.Result = table;
                return result;
            }
        }

        private AdoServiceCommandExecutionNonQueryResult ExecuteSqlCommandNonQuery(SqlCommand sqlCommand)
        {
            var result = new AdoServiceCommandExecutionNonQueryResult();
            var value = sqlCommand.ExecuteNonQuery();
            result.Result = value;
            return result;
        }

        private AdoServiceCommandExecutionScalarResult ExecuteSqlCommandScalar(SqlCommand sqlCommand)
        {
            var result = new AdoServiceCommandExecutionScalarResult();
            var obj = sqlCommand.ExecuteScalar();
            result.Result = obj;
            return result;
        }

        private TResult UsingSqlCommand<TResult>(AdoServiceCommandExecutionArgument command, Func<SqlCommand, TResult> selector) where TResult : AdoServiceCommandExecutionResult
        {
            var connectionString = connectionStringRepository[command.ConnectionString].IfNotNull(cs => cs.ConnectionString) ?? command.ConnectionString;

            using (var scope = GetSqlConnection(connectionString, command.Database))
            {
                var sqlConnection = scope.Instance;
                bool isSelectStatement = command.CommandType.ToEnumOrDefault(CommandType.Text) == CommandType.Text && command.CommandText.StartsWith("SELECT", StringComparison.InvariantCultureIgnoreCase);
                if (!isSelectStatement)
                {
                    sqlConnection.SetUserContext(PrincipalContext.Current.IfNotNull(pc => pc.Principal));
                }

                if (sqlConnection.State != ConnectionState.Open) sqlConnection.Open();
                using (var sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.Connection = sqlConnection;
                    sqlCommand.CommandTimeout = command.CommandTimeout;
                    sqlCommand.CommandText = command.CommandText;
                    sqlCommand.CommandType = command.CommandType.ToEnumOrDefault(CommandType.Text);
                    sqlCommand.UpdatedRowSource = command.UpdatedRowSource.ToEnumOrDefault<UpdateRowSource>();

                    if (command.Parameters != null)
                    {
                        var commandParameters = GetCommandParameters(command.Parameters);
                        commandParameters.ForEach(p => sqlCommand.Parameters.Add(p));
                    }

                    // Save original command text in case anyone modifies it upon retries
                    var originalCommandText = sqlCommand.CommandText;
                    var executedArgs = new DbCommandExecutedEventArgs(null, false);

                    RetryUtility.ExecuteWithRetry(() =>
                    {
                        // Restore original command text
                        sqlCommand.CommandText = originalCommandText;

                        var executingArgs = new DbCommandExecutingEventArgs { DbCommand = sqlCommand, ConnectionString = connectionString };    

                        CommandExecuting.Fire(this, executingArgs);

                        executedArgs = new DbCommandExecutedEventArgs(executingArgs.DbCommand, executingArgs.Handled) { ConnectionString = connectionString };

                        // Reset state
                        var hasExecuted = false;
                        executedArgs.Exception = null;

                        // Attempt execution
                        lock (CurrentSyncRoot ?? new object())
                        {
                            try
                            {
                                if (!executingArgs.Handled)
                                {
                                    executedArgs.Result = selector(sqlCommand);
                                }
                                else
                                {
                                    executedArgs.Result = executingArgs.Result;
                                }

                                hasExecuted = true;

                                CommandExecuted.Fire(this, executedArgs);
                            }
                            catch (Exception ex)
                            {
                                executedArgs.Exception = ex;

                                if (!hasExecuted) CommandExecuted.Fire(this, executedArgs);

                                if (executedArgs.Exception != null) throw;
                            }
                        }
                    }, 3, TimeSpan.FromSeconds(1), exception => executedArgs.RetryFailure);

                    var result = (TResult)executedArgs.Result;

                    // In case of DeriveCommandParameters for example, result can be null
                    if (result == null) return result;

                    // Assign output parameters
                    if (result.OutputParameters == null) result.OutputParameters = new List<AdoServiceParameterExecutionArgument>();

                    if (command.Parameters != null)
                    {
                        foreach (var sqlParamter in sqlCommand.Parameters.OfType<SqlParameter>().Where(p => p.Direction != ParameterDirection.Input))
                        {
                            var serviceParameter = command.Parameters.FirstOrDefault(p => p.ParameterName == sqlParamter.ParameterName);
                            if (serviceParameter == null) continue;
                            serviceParameter.Value = sqlParamter.Value;
                            result.OutputParameters.Add(serviceParameter);
                        }
                    }

                    return result;
                }
            }
        }

        private IEnumerable<SqlParameter> GetCommandParameters(IEnumerable<AdoServiceParameterExecutionArgument> parameters)
        {
            return parameters.Select(parameter =>
            {
                var sqlParameter = new SqlParameter
                {
                    ParameterName = parameter.ParameterName,
                    Size = parameter.Size,
                    Value = parameter.Value,
                    Direction = parameter.Direction.ToEnumOrDefault(ParameterDirection.Input)
                };
                if (parameter.DbType.IsNotNullOrEmpty()) sqlParameter.DbType = parameter.DbType.ToEnum<DbType>();
                return sqlParameter;
            }).ToList();
        }

        public IEnumerable<AdoServiceCommandExecutionResult> ExecuteCommands(IEnumerable<AdoServiceCommandExecutionArgument> commands)
        {
            var results = new List<AdoServiceCommandExecutionResult>();

            foreach (var c in commands)
            {
                var command = c;

                var result = UsingSqlCommand(command, sqlCommand =>
                {
                    try
                    {
                        switch (command.ResultType)
                        {
                            case AdoServiceCommandExecutionArgumentResultType.DataTable:
                                return ExecuteSqlCommandDataTable(sqlCommand) as AdoServiceCommandExecutionResult;
                            case AdoServiceCommandExecutionArgumentResultType.NonQuery:
                                return ExecuteSqlCommandNonQuery(sqlCommand) as AdoServiceCommandExecutionResult;
                            case AdoServiceCommandExecutionArgumentResultType.Scalar:
                                return ExecuteSqlCommandScalar(sqlCommand) as AdoServiceCommandExecutionResult;
                            default:
                                throw new NotSupportedException("ResultType {0} is not supported.".FormatWith(command.ResultType));
                        }
                    }
                    catch (Exception ex)
                    {
                        var exception = new Exception("Error executing command\r\n{0}".FormatWith(command), ex);
                        throw exception;
                    }
                });

                results.Add(result);
            }
            return results;
        }

        public void ExecuteBulkCopy(AdoServiceBulkCopyExecutionArgument argument)
        {
            var connectionString = connectionStringRepository[argument.ConnectionString].IfNotNull(cs => cs.ConnectionString) ?? argument.ConnectionString;
            using (var scope = GetSqlConnection(connectionString, argument.Database))
            {
                var sqlConnection = scope.Instance;
                using (var transaction = sqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    var bc = new SqlBulkCopy(sqlConnection,
                        SqlBulkCopyOptions.CheckConstraints |
                        SqlBulkCopyOptions.FireTriggers, transaction);

                    bc.BatchSize = 1000;
                    bc.DestinationTableName = argument.Destination;
                    bc.WriteToServer(argument.DataTable);
                }
            }
        }

        public DataSet GetSchema(string connectionString, string collectionName, string[] restrictionValues)
        {
            DataTable dt = null;
            connectionString = connectionStringRepository[connectionString].IfNotNull(cs => cs.ConnectionString) ?? connectionString;

            using (var scope = GetSqlConnection(connectionString, null))
            {
                var sqlConnection = scope.Instance;

                if (collectionName == null && restrictionValues == null)
                {
                    dt = sqlConnection.GetSchema();
                }
                if (collectionName != null && restrictionValues == null)
                {
                    dt = sqlConnection.GetSchema(collectionName);
                }
                else if (restrictionValues != null)
                {
                    dt = sqlConnection.GetSchema(collectionName, restrictionValues);
                }
            }

            var ds = new DataSet("Schema");
            ds.Tables.Add(dt ?? new DataTable("Schema"));

            return ds;
        }

        public string ResolveConnectionString(string connectionString)
        {
            connectionString = connectionStringRepository[connectionString].IfNotNull(cs => cs.ConnectionString) ?? connectionString;
            SqlConnectionStringBuilder builder;
            if (DbConnections.TryGetConnectionStringBuilder(connectionString, out builder))
            {
                builder.TrustServerCertificate = true;
                builder.Encrypt = true;
                connectionString = builder.ConnectionString;
            }
            return connectionString;
        }

        public AdoServiceCommandExecutionArgument DeriveCommandParameters(AdoServiceCommandExecutionArgument command)
        {
            UsingSqlCommand(command, sqlCommand =>
            {
                SqlCommandBuilder.DeriveParameters(sqlCommand);

                command.CommandText = sqlCommand.CommandText;
                command.Parameters = sqlCommand.Parameters.OfType<SqlParameter>().Select(p =>
                    new AdoServiceParameterExecutionArgument
                    {
                        DbType = p.DbType.ToString(),
                        Direction = p.Direction.ToString(),
                        ParameterName = p.ParameterName,
                        Size = p.Size,
                        Value = p.Value
                    }).ToList();

                return null as AdoServiceCommandExecutionResult;
            });

            return command;

        }
    }

    public class DbCommandExecutingEventArgs : EventArgs
    {
        public IDbCommand DbCommand { get; set; }

        /// <summary>
        /// Full connection string used by specified DbCommand (SqlConnection removes passwords when accessing ConnectionString property)
        /// </summary>
        public string ConnectionString { get; set; }

        public object Result { get; set; }

        /// <summary>
        /// If true, command execution will be suppressed and the result will be returned.
        /// </summary>
        public bool Handled { get; set; }
    }

    public class DbCommandExecutedEventArgs : EventArgs
    {
        public DbCommandExecutedEventArgs(IDbCommand dbCommand, bool handledExternally)
        {
            DbCommand = dbCommand;
            HandledExternally = handledExternally;
        }

        /// <summary>
        /// Full connection string used by specified DbCommand (SqlConnection removes passwords when accessing ConnectionString property)
        /// </summary>
        public string ConnectionString { get; set; }

        public IDbCommand DbCommand { get; private set; }

        /// <summary>
        /// Gets a value indicating whether [handled externally]. If true, handler set Handled to true on Executing.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [handled externally]; otherwise, <c>false</c>.
        /// </value>
        public bool HandledExternally { get; private set; }

        public object Result { get; set; }

        public Exception Exception { get; set; }

        /// <summary>
        /// When command execution raises an exception, allow it to retry if possible
        /// </summary>
        public bool RetryFailure { get; set; }
    }


    /// <summary>
    /// A contract of parameters for a bulk copy operation.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoServiceBulkCopyExecutionArgument
    {
        [DataMember]
        public string ConnectionString { get; set; }

        [DataMember]
        public int ConnectionTimeout { get; set; }

        [DataMember]
        public string Database { get; set; }

        [DataMember]
        public string Destination { get; set; }

        private DataTable dataTable;
        private byte[] data;

        public DataTable DataTable
        {
            get { return dataTable; }
            set
            {
                dataTable = value;
                data = null;
            }
        }

        [DataMember]
        public byte[] Data
        {
            get
            {
                if (data == null && dataTable != null)
                {
                    var ds = new DataSet("DataSet");
                    if (dataTable.DataSet != null) dataTable.DataSet.Tables.Remove(dataTable);
                    ds.Tables.Add(dataTable);
                    using (var ms = new MemoryStream())
                    {
                        var serializer = Serialization.CreateDataContractSerializer<DataSet>();
                        serializer.WriteObject(ms, ds);
                        data = ms.ToArray();
                    }
                }
                return data;
            }
            set
            {
                data = value;
                if (value != null)
                {
                    using (var ms = new MemoryStream(data))
                    {
                        var serializer = Serialization.CreateDataContractSerializer<DataSet>();
                        var ds = (DataSet)serializer.ReadObject(ms);
                        dataTable = ds.Tables[0];
                    }
                }
            }
        }

        [DataMember]
        public int RowCount
        {
            get { return DataTable == null ? 0 : DataTable.Rows.Count; }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        public override string ToString()
        {
            var document = new XDocument(new XElement(GetType().Name,
                                                  new XAttribute("ConnectionString", ConnectionString ?? string.Empty),
                                                  new XAttribute("Database", Database ?? string.Empty),
                                                  new XAttribute("ConnectionTimeout", ConnectionTimeout),
                                                  new XAttribute("RowCount", RowCount)));
            return document.ToString(new XmlWriterSettings { CheckCharacters = false, Indent = true, OmitXmlDeclaration = true });
        }
    }

    /// <summary>
    /// A contract of parameters for the AdoService interface.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoServiceCommandExecutionArgument
    {
        public AdoServiceCommandExecutionArgument()
        {
            Parameters = new List<AdoServiceParameterExecutionArgument>();
        }

        [DataMember]
        public string ConnectionString { get; set; }

        [DataMember]
        public int ConnectionTimeout { get; set; }

        [DataMember]
        public string Database { get; set; }

        [DataMember]
        public string CommandText { get; set; }

        [DataMember]
        public int CommandTimeout { get; set; }

        [DataMember]
        public string CommandType { get; set; }

        [DataMember]
        public string UpdatedRowSource { get; set; }

        [DataMember]
        public IList<AdoServiceParameterExecutionArgument> Parameters { get; set; }

        [DataMember]
        public AdoServiceCommandExecutionArgumentResultType ResultType { get; set; }

        public override string ToString()
        {
            var document = new XDocument(new XElement(GetType().Name,
                                                      new XAttribute("CommandText", CommandText == null ? string.Empty : CommandText.Replace(Environment.NewLine, " ")),
                                                      new XAttribute("CommandType", CommandType ?? string.Empty),
                                                      new XAttribute("UpdatedRowSource", UpdatedRowSource ?? string.Empty),
                                                      new XAttribute("ConnectionString", ConnectionString ?? string.Empty),
                                                      new XAttribute("Database", Database ?? string.Empty),
                                                      new XAttribute("CommandTimeout", CommandTimeout),
                                                      new XAttribute("ConnectionTimeout", ConnectionTimeout),
                                                      new XAttribute("ResultType", ResultType),
                                                      new XElement("Parameters", Parameters == null ? string.Empty : Parameters.Select(p => new XElement("Parameter", XElement.Parse(p.ToString()))) as object)));

            return document.ToString(new XmlWriterSettings { CheckCharacters = false, Indent = true, OmitXmlDeclaration = true });
        }
    }

    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public enum AdoServiceCommandExecutionArgumentResultType
    {
        [EnumMember]
        NonQuery,
        [EnumMember]
        Scalar,
        [EnumMember]
        DataTable
    }

    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoServiceParameterExecutionArgument
    {
        [DataMember]
        public string ParameterName { get; set; }

        [DataMember]
        public int Size { get; set; }


        private byte[] data;
        private object value;

        public object Value
        {
            get { return value; }
            set
            {
                this.value = value;
                data = null;
            }
        }

        [DataMember]
        public byte[] Data
        {
            get
            {
                if (data == null && value != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        var serializer = Serialization.CreateDataContractSerializer();
                        serializer.WriteObject(ms, value);
                        data = ms.ToArray();
                    }
                }
                return data;
            }
            set
            {
                data = value;
                if (value != null)
                {
                    using (var ms = new MemoryStream(data))
                    {
                        var serializer = Serialization.CreateDataContractSerializer();
                        this.value = serializer.ReadObject(ms);
                    }
                }
            }
        }

        [DataMember]
        public string Direction { get; set; }

        [DataMember]
        public string DbType { get; set; }

        public override string ToString()
        {
            var document = new XDocument(new XElement(GetType().Name, new XAttribute("ParameterName", ParameterName), new XAttribute("Size", Size), new XAttribute("Value", Value ?? ""), new XAttribute("Direction", Direction)));

            return document.ToString(new XmlWriterSettings { CheckCharacters = false, Indent = true, OmitXmlDeclaration = true });
        }
    }

    /// <summary>
    /// A contract for the execution result of AdoServiceCommandExecutionArguments.
    /// </summary>
    [KnownType(typeof(AdoServiceCommandExecutionNonQueryResult)), KnownType(typeof(AdoServiceCommandExecutionDataTableResult)), KnownType(typeof(AdoServiceCommandExecutionScalarResult))]
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoServiceCommandExecutionResult
    {
        [DataMember]
        public IList<AdoServiceParameterExecutionArgument> OutputParameters { get; set; }
    }

    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoServiceCommandExecutionNonQueryResult : AdoServiceCommandExecutionResult
    {
        [DataMember]
        public int Result { get; set; }

        public override string ToString()
        {
            return new XDocument(new XElement(GetType().Name, new XAttribute("Result", Result))).ToString();
        }
    }

    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoServiceCommandExecutionScalarResult : AdoServiceCommandExecutionResult
    {
        private byte[] data;
        private object result;

        public object Result
        {
            get { return result; }
            set
            {
                result = value;
                data = null;
            }
        }

        [DataMember]
        public byte[] Data
        {
            get
            {
                if (data == null && result != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        var serializer = Serialization.CreateDataContractSerializer();
                        serializer.WriteObject(ms, result);
                        data = ms.ToArray();
                    }
                }
                return data;
            }
            set
            {
                data = value;
                if (value != null)
                {
                    using (var ms = new MemoryStream(data))
                    {
                        var serializer = Serialization.CreateDataContractSerializer();
                        result = serializer.ReadObject(ms);
                    }
                }
            }
        }


        public override string ToString()
        {
            return new XDocument(new XElement(GetType().Name, new XAttribute("Result", Result))).ToString();
        }
    }

    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoServiceCommandExecutionDataTableResult : AdoServiceCommandExecutionResult
    {
        private DataTable result;
        private byte[] data;

        public DataTable Result
        {
            get { return result; }
            set
            {
                result = value;
                data = null;
            }
        }

        [DataMember]
        public byte[] Data
        {
            get
            {
                if (data == null && result != null)
                {
                    result.Columns.OfType<DataColumn>().Where(c => c.DataType == typeof(DateTime)).ForEach(c => c.DateTimeMode = DataSetDateTime.Unspecified);
                    var ds = new DataSet("DataSet");
                    if (result.DataSet != null) result.DataSet.Tables.Remove(result);
                    ds.Tables.Add(result);
                    ds.RemotingFormat = SerializationFormat.Binary;
                    using (var ms = new MemoryStream())
                    {
                        var serializer = Serialization.CreateDataContractSerializer<DataSet>();
                        serializer.WriteObject(ms, ds);
                        data = ms.ToArray();
                    }
                }
                return data;
            }
            set
            {
                data = value;
                if (value != null)
                {
                    using (var ms = new MemoryStream(data))
                    {
                        var serializer = Serialization.CreateDataContractSerializer<DataSet>();
                        var ds = (DataSet)serializer.ReadObject(ms);
                        result = ds.Tables[0];
                    }
                }
            }
        }

        [DataMember]
        public int RowCount
        {
            get { return Result == null ? 0 : Result.Rows.Count; }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        public override string ToString()
        {
            return new XDocument(new XElement(GetType().Name, new XAttribute("RowCount", RowCount))).ToString();
        }
    }

    /// <summary>
    /// An IDbDataParameter for AdoServiceCommand commands.
    /// </summary>
    public class AdoServiceParameter : DbParameter
    {
        private object value;
        private ParameterDirection direction;
        private DbType dbType;
        private int size;

        public override DbType DbType
        {
            get { return dbType; }
            set
            {
                dbType = value;
                if (Size == 0 && Direction != ParameterDirection.Input)
                {
                    Size = InferSize(DbType);
                }
            }
        }

        public override ParameterDirection Direction
        {
            get { return direction; }
            set
            {
                direction = value;

                if (Size == 0 && value != ParameterDirection.Input)
                {
                    Size = InferSize(DbType);
                }

            }
        }

        private static int InferSize(DbType dbType)
        {
            switch (dbType)
            {
                case DbType.AnsiStringFixedLength:
                case DbType.StringFixedLength:
                    return 4000;
                default:
                    return int.MaxValue;
            }
        }

        public override bool IsNullable { get; set; }
        public override string ParameterName { get; set; }
        public override string SourceColumn { get; set; }
        public override DataRowVersion SourceVersion { get; set; }
        public override object Value
        {
            get { return value; }
            set
            {
                this.value = value;
                if (value != null && DbType == default(DbType))
                {
                    DbType = new SqlParameter { Value = value }.DbType;
                }
            }
        }

        public override bool SourceColumnNullMapping { get; set; }

        public override int Size
        {
            get { return size; }
            set
            {
                size = value;
                if (Size == 0 && Direction != ParameterDirection.Input)
                {
                    Size = InferSize(DbType);
                }
            }
        }

        public override void ResetDbType()
        {
        }
    }

    /// <summary>
    /// Collection of AdoServiceParameter.
    /// </summary>
    public class AdoServiceParameterCollection : DbParameterCollection
    {
        private readonly List<AdoServiceParameter> parameters = new List<AdoServiceParameter>();

        public override int Count
        {
            get { return parameters.Count; }
        }

        public override object SyncRoot
        {
            get { return ((IList)parameters).SyncRoot; }
        }

        public override bool IsFixedSize
        {
            get { return ((IList)parameters).IsFixedSize; }
        }

        public override bool IsReadOnly
        {
            get { return ((IList)parameters).IsReadOnly; }
        }

        public override bool IsSynchronized
        {
            get { return ((IList)parameters).IsSynchronized; }
        }

        public override int Add(object value)
        {
            parameters.Add(value.EnsureType<AdoServiceParameter>());
            return parameters.Count - 1;
        }

        public override bool Contains(object value)
        {
            if (!(value is AdoServiceParameter)) return false;
            return parameters.Contains((AdoServiceParameter)value);
        }

        public override void Clear()
        {
            parameters.Clear();
        }

        public override int IndexOf(object value)
        {
            if (!(value is AdoServiceParameter)) return -1;
            return parameters.IndexOf((AdoServiceParameter)value);
        }

        public override void Insert(int index, object value)
        {
            parameters.Insert(index, value.EnsureType<AdoServiceParameter>());
        }

        public override void Remove(object value)
        {
            parameters.Remove(value.EnsureType<AdoServiceParameter>());
        }

        public override void RemoveAt(int index)
        {
            parameters.RemoveAt(index);
        }

        public override void RemoveAt(string parameterName)
        {
            RemoveAt(IndexOf(parameterName));
        }

        protected override void SetParameter(int index, DbParameter value)
        {
            parameters[index] = value.EnsureType<AdoServiceParameter>();
        }

        protected override void SetParameter(string parameterName, DbParameter value)
        {
            SetParameter(IndexOf(parameterName), value.EnsureType<AdoServiceParameter>());
        }

        public override int IndexOf(string parameterName)
        {
            return parameters.FindIndex(p => p.ParameterName == parameterName);
        }

        public override IEnumerator GetEnumerator()
        {
            return parameters.GetEnumerator();
        }

        protected override DbParameter GetParameter(int index)
        {
            return parameters[index];
        }

        protected override DbParameter GetParameter(string parameterName)
        {
            return parameters[IndexOf(parameterName)];
        }

        public override bool Contains(string value)
        {
            return IndexOf(value) >= 0;
        }

        public override void CopyTo(Array array, int index)
        {
            parameters.CopyTo(array.EnsureType<AdoServiceParameter[]>());
        }

        public override void AddRange(Array values)
        {
            parameters.AddRange(values);
        }
    }

    /// <summary>
    /// A IDbCommand that uses the AdoService interface.
    /// </summary>
    public class AdoServiceCommand : DbCommand, ICloneable
    {
        public AdoServiceCommand()
        {
            CommandType = CommandType.Text;
        }

        public static readonly Func<Exception, bool> ShouldRetryDefault = ex => false;

        public static event EventHandler<DbCommandExecutingEventArgs> CommandExecuting;
        public static event EventHandler<DbCommandExecutedEventArgs> CommandExecuted;

        private readonly DbParameterCollection parameters = new AdoServiceParameterCollection();
        private DbConnection connection;

        public Func<Exception, bool> ShouldRetry { get; set; }

        public int NumberOfRetries { get; set; }

        public override string CommandText { get; set; }
        public override int CommandTimeout { get; set; }
        public override CommandType CommandType { get; set; }
        public override UpdateRowSource UpdatedRowSource { get; set; }

        protected override DbConnection DbConnection
        {
            get { return connection; }
            set
            {
                if (!value.Is<AdoServiceConnection>() && value != null)
                {
                    bool open = value.State == ConnectionState.Open;
                    value = new AdoServiceConnection { ConnectionString = value.ConnectionString };
                    if (open) value.Open();
                }
                connection = value;
            }
        }

        protected override DbParameterCollection DbParameterCollection
        {
            get { return parameters; }
        }

        protected override DbTransaction DbTransaction { get; set; }

        public override bool DesignTimeVisible { get; set; }

        public void Commit()
        {
        }

        public override void Prepare()
        {
        }

        public override void Cancel()
        {
        }

        protected override DbParameter CreateDbParameter()
        {
            return new AdoServiceParameter();
        }

        protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
        {
            object obj = RetryUtility.ExecuteWithRetry(ExecuteDbDataReaderHelper, NumberOfRetries, shouldRetry: ex => (ShouldRetry ?? ShouldRetryDefault)(ex));
            if (obj != null) return (DbDataReader)obj;
            return null;
        }

        public override int ExecuteNonQuery()
        {
            object obj = RetryUtility.ExecuteWithRetry(ExecuteNonQueryHelper, NumberOfRetries, shouldRetry: ex => (ShouldRetry ?? ShouldRetryDefault)(ex));
            if (obj != null) return (int)obj;
            return 0;
        }

        public override object ExecuteScalar()
        {
            return RetryUtility.ExecuteWithRetry(ExecuteScalarHelper, NumberOfRetries, shouldRetry: ex => (ShouldRetry ?? ShouldRetryDefault)(ex));
        }

        private DataTableReader ExecuteDbDataReaderHelper()
        {
            return ExecuteInternal(() =>
            {
                IAdoService service = GetAdoService();
                var arg = CreateExecutionArguments(this);
                AdoServiceCommandExecutionDataTableResult result = service.ExecuteCommandDbDataReader(arg).As<AdoServiceCommandExecutionDataTableResult>().EnsureNotDefault("No result was received for the specified command.");
                AssignOutputParameters(arg);
                return result.Result.CreateDataReader();
            });
        }

        private object ExecuteNonQueryHelper()
        {
            return ExecuteInternal(() =>
            {
                IAdoService service = GetAdoService();
                var arg = CreateExecutionArguments(this);
                AdoServiceCommandExecutionNonQueryResult result = service.ExecuteCommandNonQuery(arg).As<AdoServiceCommandExecutionNonQueryResult>().EnsureNotDefault("No result was received for the specified command.");
                AssignOutputParameters(arg);
                return result.Result;
            });
        }

        private object ExecuteScalarHelper()
        {
            return ExecuteInternal(() =>
            {
                IAdoService service = GetAdoService();
                var arg = CreateExecutionArguments(this);
                AdoServiceCommandExecutionScalarResult result = service.ExecuteCommandScalar(arg).As<AdoServiceCommandExecutionScalarResult>().EnsureNotDefault("No result was received for the specified command.");
                AssignOutputParameters(arg);
                return result.Result;
            });
        }

        private void AssignOutputParameters(AdoServiceCommandExecutionArgument arg)
        {
            if (arg.Parameters == null || parameters == null) return;
            foreach (var outputParameter in arg.Parameters.Where(p => p.Direction != ParameterDirection.Input.ToString()))
            {
                var commandParameter = parameters.OfType<DbParameter>().FirstOrDefault(p => p.ParameterName == outputParameter.ParameterName);
                if (commandParameter == null) continue;
                commandParameter.Value = outputParameter.Value;
            }
        }

        private T ExecuteInternal<T>(Func<T> selector)
        {
            var executingArgs = new DbCommandExecutingEventArgs { DbCommand = this, ConnectionString = DbConnection.ConnectionString };

            CommandExecuting.Fire(this, executingArgs);

            var executedArgs = new DbCommandExecutedEventArgs(executingArgs.DbCommand, executingArgs.Handled) { ConnectionString = DbConnection.ConnectionString };

            RetryUtility.ExecuteWithRetry(() =>
            {
                var hasExecuted = false;
                executedArgs.Exception = null;

                try
                {
                    if (!executingArgs.Handled)
                    {
                        executedArgs.Result = selector();
                    }
                    else
                    {
                        executedArgs.Result = executingArgs.Result;
                    }
                    hasExecuted = true;
                    CommandExecuted.Fire(this, executedArgs);
                }
                catch (Exception ex)
                {
                    executedArgs.Exception = ex;

                    if (!hasExecuted) CommandExecuted.Fire(this, executedArgs);

                    if (executedArgs.Exception != null) throw;
                }
            }, 3, TimeSpan.FromSeconds(1), exception => executedArgs.RetryFailure);

            return (T)executedArgs.Result;
        }

        internal static AdoServiceCommandExecutionArgument CreateExecutionArguments(AdoServiceCommand command)
        {
            return new AdoServiceCommandExecutionArgument
            {
                CommandText = command.CommandText,
                CommandTimeout = command.CommandTimeout,
                ConnectionTimeout = command.Connection.ConnectionTimeout,
                CommandType = command.CommandType.ToString(),
                ConnectionString = command.Connection.ConnectionString,
                Database = command.Connection.Database,
                Parameters = command.Parameters.Cast<AdoServiceParameter>().Select(CreateParameterExecutionArguments).ToArray(),
                UpdatedRowSource = command.UpdatedRowSource.ToString()
            };
        }

        private static AdoServiceParameterExecutionArgument CreateParameterExecutionArguments(AdoServiceParameter parameter)
        {
            return new AdoServiceParameterExecutionArgument
            {
                ParameterName = parameter.ParameterName,
                Size = parameter.Size,
                Direction = parameter.Direction.ToString(),
                Value = parameter.Value,
                DbType = parameter.DbType.ToString()
            };
        }

        private static IAdoService GetAdoService()
        {
            return ServiceProvider.Current.GetService<IAdoService>();
        }

        public object Clone()
        {
            var clone = this.ToAdoServiceCommand();
            return clone;
        }
    }

    /// <summary>
    /// A IDbConnection that uses the AdoService interface.
    /// </summary>
    public class AdoServiceConnection : DbConnection
    {
        private string database;
        private string connectionString;

        protected override DbProviderFactory DbProviderFactory
        {
            get { return AdoServiceProviderFactory.Instance; }
        }

        public override string ConnectionString
        {
            get
            {
                SqlConnectionStringBuilder builder;
                if (DbConnections.TryGetConnectionStringBuilder(connectionString, out builder))
                {
                    builder.InitialCatalog = database;
                    return builder.ConnectionString;
                }
                return connectionString;
            }
            set
            {
                SqlConnectionStringBuilder builder;
                if (DbConnections.TryGetConnectionStringBuilder(value, out builder))
                {
                    connectionString = builder.ConnectionString;
                    database = builder.InitialCatalog;
                }
                else { connectionString = value; }
            }
        }

        public override int ConnectionTimeout
        {
            get
            {
                SqlConnectionStringBuilder builder;
                if (DbConnections.TryGetConnectionStringBuilder(connectionString, out builder))
                {
                    return builder.ConnectTimeout;
                }
                return (int)TimeSpan.FromMinutes(2).TotalSeconds;
            }
        }

        public override string Database
        {
            get { return database; }
        }

        public override ConnectionState State
        {
            get { return ConnectionState.Open; }
        }

        public override string DataSource
        {
            get
            {
                SqlConnectionStringBuilder builder;
                if (DbConnections.TryGetConnectionStringBuilder(connectionString, out builder))
                {
                    return builder.DataSource;
                }
                return string.Empty;
            }
        }

        public override string ServerVersion
        {
            get { return string.Empty; }
        }

        protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
        {
            return new TransactionScopeDbTransaction(this, isolationLevel);
        }

        public override void Close()
        {
            OnStateChange(new StateChangeEventArgs(ConnectionState.Open, ConnectionState.Closed));
        }

        public override void ChangeDatabase(string databaseName)
        {
            database = databaseName;
        }

        public override void Open()
        {
            OnStateChange(new StateChangeEventArgs(ConnectionState.Closed, ConnectionState.Open));
        }

        protected override DbCommand CreateDbCommand()
        {
            var command = AdoServiceProviderFactory.Instance.CreateCommand().EnsureNotDefault();
            command.Connection = this;
            return command;
        }

        public override void EnlistTransaction(Transaction transaction)
        {

        }

        public override DataTable GetSchema()
        {
            return ServiceProvider.Current.GetService<IAdoService>().GetSchema(ConnectionString, null, null).Tables[0];
        }

        public override DataTable GetSchema(string collectionName)
        {
            return ServiceProvider.Current.GetService<IAdoService>().GetSchema(ConnectionString, collectionName, null).Tables[0];
        }

        public override DataTable GetSchema(string collectionName, string[] restrictionValues)
        {
            return ServiceProvider.Current.GetService<IAdoService>().GetSchema(ConnectionString, collectionName, restrictionValues).Tables[0];
        }
    }

    [Singleton]
    public class AdoServiceProviderFactoryRegistrar
    {
        public AdoServiceProviderFactoryRegistrar()
        {
            AdoServiceProviderFactory.Register();
        }
    }

    /// <summary>
    /// A provider factory for the AdoService interface.
    /// </summary>
    public class AdoServiceProviderFactory : DbProviderFactory, IServiceProvider
    {
        public const string Name = "AdoServiceProvider";

        public static readonly AdoServiceProviderFactory Instance = new AdoServiceProviderFactory();

        private AdoServiceProviderFactory()
        {
        }

        /// <summary>
        /// Registers this provider factory in the .NET configuration.
        /// </summary>
        public static void Register()
        {
            DataSet section = (ConfigurationManager.GetSection("system.data") as DataSet).EnsureNotDefault("Could not find system.data configuration.");
            DataTable providerTable = section.Tables["DbProviderFactories"].EnsureNotDefault("Could not find DbProviderFactories in configuration.");
            if (providerTable.Rows.Find(Name) == null)
            {
                providerTable.Rows.Add(Name, Name, Name, typeof(AdoServiceProviderFactory).AssemblyQualifiedName);
            }
        }

        public Func<AdoServiceCommand> CreateCommandFunc { get; set; }

        public override DbCommand CreateCommand()
        {
            return CreateCommandFunc == null ? new AdoServiceCommand() : CreateCommandFunc();
        }

        public override DbConnection CreateConnection()
        {
            return new AdoServiceConnection();
        }

        public override DbParameter CreateParameter()
        {
            return new AdoServiceParameter();
        }

        public override DbConnectionStringBuilder CreateConnectionStringBuilder()
        {
            return new SqlConnectionStringBuilder();
        }

        public override DbCommandBuilder CreateCommandBuilder()
        {
            return new AdoServiceCommandBuilder();
        }

        public object GetService(Type serviceType)
        {
            return ServiceProvider.Current.IfNotNull(sp => sp.GetService(serviceType));
        }
    }

    public class AdoServiceCommandBuilder : DbCommandBuilder
    {
        private readonly SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder();

        /// <summary>
        /// Required because some poorly designed components rely on invoking this method that exists on SqlCommandBuilder via reflection..
        /// </summary>
        /// <param name="command">The command.</param>
        public static void DeriveParameters(AdoServiceCommand command)
        {
            var commandWithDerivedParameters = ServiceProvider.Current.GetService<IAdoService>().DeriveCommandParameters(AdoServiceCommand.CreateExecutionArguments(command));

            command.CommandText = commandWithDerivedParameters.CommandText;
            command.Parameters.Clear();
            foreach (var derivedParameter in commandWithDerivedParameters.Parameters)
            {
                var parameter = command.CreateParameter();

                parameter.DbType = derivedParameter.DbType.ToEnumOrDefault<DbType>();
                parameter.Direction = derivedParameter.Direction.ToEnumOrDefault<ParameterDirection>();
                parameter.ParameterName = derivedParameter.ParameterName;
                parameter.Size = derivedParameter.Size;
                parameter.Value = derivedParameter.Value;

                command.Parameters.Add(parameter);
            }
        }

        protected override void ApplyParameterInfo(DbParameter parameter, DataRow row, StatementType statementType, bool whereClause)
        {
            sqlCommandBuilder.GetType().InvokeMember("ApplyParameterInfo",
                                                     BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
                                                     null,
                                                     sqlCommandBuilder, new object[] { parameter, row, statementType, whereClause });
        }

        protected override string GetParameterName(int parameterOrdinal)
        {
            return (string)
            sqlCommandBuilder.GetType().InvokeMember("GetParameterName",
                                                     BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
                                                     null,
                                                     sqlCommandBuilder, new object[] { parameterOrdinal });
        }

        protected override string GetParameterName(string parameterName)
        {
            return (string)
                sqlCommandBuilder.GetType().InvokeMember("GetParameterName",
                                            BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
                                            null,
                                            sqlCommandBuilder, new object[] { parameterName });
        }

        protected override string GetParameterPlaceholder(int parameterOrdinal)
        {
            return (string)
              sqlCommandBuilder.GetType().InvokeMember("GetParameterPlaceholder",
                                          BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
                                          null,
                                          sqlCommandBuilder, new object[] { parameterOrdinal });
        }

        protected override void SetRowUpdatingHandler(DbDataAdapter adapter)
        {
            sqlCommandBuilder.GetType().InvokeMember("SetRowUpdatingHandler",
                                 BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
                                 null,
                                 sqlCommandBuilder, new object[] { adapter });
        }
    }

    /// <summary>
    /// Helpers for AdoService.
    /// </summary>
    public static class AdoServices
    {
        public static AdoServiceCommand ToAdoServiceCommand(this DbCommand dbCommand)
        {
            var result = new AdoServiceCommand
            {
                CommandText = dbCommand.CommandText,
                CommandTimeout = dbCommand.CommandTimeout,
                CommandType = dbCommand.CommandType,
                UpdatedRowSource = dbCommand.UpdatedRowSource
            };
            result.Parameters.AddRange(dbCommand.Parameters.OfType<DbParameter>().Select(ToAdoServiceParameter));
            return result;
        }

        public static AdoServiceParameter ToAdoServiceParameter(this DbParameter parameter)
        {
            return new AdoServiceParameter
            {
                DbType = parameter.DbType,
                Direction = parameter.Direction,
                IsNullable = parameter.IsNullable,
                ParameterName = parameter.ParameterName,
                Size = parameter.Size,
                SourceColumn = parameter.SourceColumn,
                SourceColumnNullMapping = parameter.SourceColumnNullMapping,
                SourceVersion = parameter.SourceVersion,
                Value = parameter.Value
            };
        }

        public static AdoServiceCommandExecutionDataTableResult ExecuteCommandDbDataReader(this IAdoService service, AdoServiceCommandExecutionArgument command)
        {
            command.ResultType = AdoServiceCommandExecutionArgumentResultType.DataTable;
            var result = service.ExecuteCommands(new[] { command }).OfType<AdoServiceCommandExecutionDataTableResult>().FirstOrDefault();

            if (result == null || command.Parameters == null || result.OutputParameters == null) return result;

            AssignOutputParameters(command, result);

            return result;
        }

        public static AdoServiceCommandExecutionNonQueryResult ExecuteCommandNonQuery(this IAdoService service, AdoServiceCommandExecutionArgument command)
        {
            command.ResultType = AdoServiceCommandExecutionArgumentResultType.NonQuery;
            var result = service.ExecuteCommands(new[] { command }).OfType<AdoServiceCommandExecutionNonQueryResult>().FirstOrDefault();

            if (result == null || command.Parameters == null || result.OutputParameters == null) return result;

            AssignOutputParameters(command, result);

            return result;
        }

        public static AdoServiceCommandExecutionScalarResult ExecuteCommandScalar(this IAdoService service, AdoServiceCommandExecutionArgument command)
        {
            command.ResultType = AdoServiceCommandExecutionArgumentResultType.Scalar;
            var result = service.ExecuteCommands(new[] { command }).OfType<AdoServiceCommandExecutionScalarResult>().FirstOrDefault();

            if (result == null || command.Parameters == null || result.OutputParameters == null) return result;

            AssignOutputParameters(command, result);

            return result;
        }

        private static void AssignOutputParameters(AdoServiceCommandExecutionArgument command, AdoServiceCommandExecutionResult result)
        {
            foreach (var outputParameter in result.OutputParameters)
            {
                var commandParameter = command.Parameters.FirstOrDefault(p => p.ParameterName == outputParameter.ParameterName);
                if (commandParameter == null) continue;
                commandParameter.Value = outputParameter.Value;
            }
        }

        /// <summary>
        /// Intercepts any db commands issued during the execution of a query by queryable's INotifyingQueryProvider.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="executingHandler">The executing handler.</param>
        /// <param name="executedHandler">The executed handler.</param>
        /// <returns></returns>
        public static IQueryable<T> InterceptDbCommands<T>(this IQueryable<T> source, EventHandler<DbCommandExecutingEventArgs> executingHandler = null, EventHandler<DbCommandExecutedEventArgs> executedHandler = null)
        {
            source.Provider.EnsureType<INotifyingQueryProvider>().InterceptDbCommands(executingHandler, executedHandler);
            return source;
        }

        /// <summary>
        /// Intercepts any db commands issued during the execution of a query by the INotifyingQueryProvider.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="executingHandler">The executing handler.</param>
        /// <param name="executedHandler">The executed handler.</param>
        /// <returns></returns>
        public static INotifyingQueryProvider InterceptDbCommands(this INotifyingQueryProvider provider, EventHandler<DbCommandExecutingEventArgs> executingHandler = null, EventHandler<DbCommandExecutedEventArgs> executedHandler = null)
        {
            var threadsOnWhichQueryProviderIsExecutingRightNow = new HashSet<int>();

            // wrap handlers to make sure events only fire on the thread that the query is being executed on.
            EventHandler<DbCommandExecutingEventArgs> innerExecutingHandler = (sender, e) =>
            {
                if (threadsOnWhichQueryProviderIsExecutingRightNow.Contains(Thread.CurrentThread.ManagedThreadId))
                {
                    executingHandler.Fire(sender, e);
                }
            };
            EventHandler<DbCommandExecutedEventArgs> innerExecutedHandler = (sender, e) =>
            {
                if (threadsOnWhichQueryProviderIsExecutingRightNow.Contains(Thread.CurrentThread.ManagedThreadId))
                {
                    executedHandler.Fire(sender, e);
                }
            };

            provider.Executing += (sender, e) =>
            {
                threadsOnWhichQueryProviderIsExecutingRightNow.Add(Thread.CurrentThread.ManagedThreadId);
                if (executingHandler != null) AdoServiceCommand.CommandExecuting += innerExecutingHandler;
                if (executedHandler != null) AdoServiceCommand.CommandExecuted += innerExecutedHandler;
            };

            provider.Executed += (sender, e) =>
            {
                threadsOnWhichQueryProviderIsExecutingRightNow.Remove(Thread.CurrentThread.ManagedThreadId);
                AdoServiceCommand.CommandExecuting -= innerExecutingHandler;
                AdoServiceCommand.CommandExecuted -= innerExecutedHandler;
            };

            return provider;
        }
    }
}