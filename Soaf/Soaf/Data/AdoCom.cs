﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Xml;
using System.Xml.Linq;
using ADODB;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;
using Soaf.Logging;
using Soaf.Security;
using Soaf.Xml;
using Stream = ADODB.Stream;
using TransactionStatus = System.Transactions.TransactionStatus;

[assembly: Component(typeof(AdoComService), typeof(IAdoComService))]

namespace Soaf.Data
{
    /// <summary>
    /// A service that performs operations using legacy COM ADODB.
    /// </summary>
    [SupportsMethodCallTracing(Source = "AdoComService", EnableForAll = true)]
    public interface IAdoComService
    {
        IEnumerable<AdoComRecordset> ExecuteCommands(IEnumerable<AdoComServiceCommand> commands);
    }

    /// <summary>
    /// A COM recordset wrapper.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoComRecordset
    {
        private Recordset recordset;

        public AdoComRecordset() { }

        public AdoComRecordset(Recordset recordset)
        {
            if (recordset != null)
            {
                RecordsetData = recordset.SerializeToByteArray();
                RecordCount = recordset.State != 0 ? recordset.RecordCount : 0;
                Size = RecordsetData.Length;
            }
        }

        public Recordset Recordset
        {
            get { return recordset == null && RecordsetData != null ? (recordset = AdoCom.DeserializeRecordset(RecordsetData)) : recordset; }
        }

        [DataMember]
        public byte[] RecordsetData { get; set; }

        [DataMember]
        public int Size { get; set; }

        [DataMember]
        public int RecordCount { get; set; }

        public override string ToString()
        {
            var document = new XDocument(new XElement(GetType().Name,
                                                      new XAttribute("RecordCount", RecordCount),
                                                      new XAttribute("Size", Size)));

            return document.ToString(new XmlWriterSettings { CheckCharacters = false, Indent = true, OmitXmlDeclaration = true });
        }

    }

    /// <summary>
    /// A command to be executed by the AdoComService.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    [KnownType(typeof(AdoComServiceOpenRecordsetCommand)), KnownType(typeof(AdoComServiceUpdateRecordsetCommand))]
    public class AdoComServiceCommand
    {
    }


    /// <summary>
    /// A command to update an ADODB recordset.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoComServiceUpdateRecordsetCommand : AdoComServiceCommand
    {
        [DataMember]
        public string ConnectionString { get; set; }

        [DataMember]
        public AdoComRecordset Recordset { get; set; }

        public override string ToString()
        {
            var document = new XDocument(new XElement(GetType().Name,
                new XAttribute("ConnectionString", ConnectionString)));
            return document.ToString(new XmlWriterSettings { CheckCharacters = false, Indent = true, OmitXmlDeclaration = true });
        }
    }

    /// <summary>
    /// A command to be executed by the AdoComService to open a recordset.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoComServiceOpenRecordsetCommand : AdoComServiceCommand
    {
        [DataMember]
        public string CommandText { get; set; }

        [DataMember]
        public int? CommandType { get; set; }

        [DataMember]
        public List<AdoComServiceParameter> Parameters { get; set; }

        [DataMember]
        public string ConnectionString { get; set; }

        public override bool Equals(object obj)
        {
            return obj.As<AdoComServiceOpenRecordsetCommand>().IfNotNull(o => string.Equals(o.CommandText, CommandText, StringComparison.OrdinalIgnoreCase)
                                                                              && (o.CommandType == CommandType || CommandType == 8 || o.CommandType == 8) //8 == Unknown
                                                                              && o.ConnectionString == ConnectionString &&
                                                                              (o.Parameters ?? new List<AdoComServiceParameter>()).SequenceEqual(Parameters ?? new List<AdoComServiceParameter>()));
        }

        public override int GetHashCode()
        {
            return (CommandText ?? string.Empty).GetHashCode() ^ (ConnectionString ?? string.Empty).GetHashCode() ^ (CommandType ?? 1).GetHashCode();
        }

        public override string ToString()
        {
            var document = new XDocument(new XElement(GetType().Name,
                                                      new XAttribute("CommandText", CommandText == null ? string.Empty : CommandText.Replace(Environment.NewLine, " ")),
                                                      new XAttribute("CommandType", CommandType ?? 0),
                                                      new XElement("Parameters", Parameters == null ? string.Empty : Parameters.Select(p => new XElement("Parameter", XElement.Parse(p.ToString()))) as object),
                                                      new XAttribute("ConnectionString", ConnectionString ?? string.Empty)));

            return document.ToString(new XmlWriterSettings { CheckCharacters = false, Indent = true, OmitXmlDeclaration = true });
        }
    }

    /// <summary>
    /// A parameter for a AdoComService command.
    /// </summary>
    [DataContract(Namespace = Namespaces.SoafXmlNamespace)]
    public class AdoComServiceParameter
    {
        [DataMember]
        public string Name { get; set; }

        private byte[] data;
        private object value;

        public object Value
        {
            get { return value; }
            set
            {
                this.value = value;
                data = null;
            }
        }

        [DataMember]
        public byte[] Data
        {
            get
            {
                if (data == null && value != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        var serializer = Serialization.CreateDataContractSerializer();
                        serializer.WriteObject(ms, value);
                        data = ms.ToArray();
                    }
                }
                return data;
            }
            set
            {
                data = value;
                if (value != null)
                {
                    using (var ms = new MemoryStream(data))
                    {
                        var serializer = Serialization.CreateDataContractSerializer();
                        this.value = serializer.ReadObject(ms);
                    }
                }
            }
        }

        [DataMember]
        public int DataType { get; set; }

        [DataMember]
        public int Direction { get; set; }

        [DataMember]
        public int Size { get; set; }

        public override bool Equals(object obj)
        {
            return obj.As<AdoComServiceParameter>().IfNotNull(p => p.Name == Name && Equals(p.Value, Value) && p.DataType == DataType && p.Direction == Direction && p.Size == Size);
        }

        public override int GetHashCode()
        {
            return (Name ?? string.Empty).GetHashCode();
        }

        public override string ToString()
        {
            var document = new XDocument(new XElement(GetType().Name,
                new XAttribute("Name", Name),
                new XAttribute("Value", Value ?? string.Empty),
                new XAttribute("DataType", DataType),
                new XAttribute("Direction", Direction),
                new XAttribute("Size", Size)));

            return document.ToString(new XmlWriterSettings { CheckCharacters = false, Indent = true, OmitXmlDeclaration = true });
        }
    }

    public class AdoComService : IAdoComService
    {
        private readonly IConnectionStringRepository connectionStringRepository;

        private static readonly IDictionary<string, string> OdbcConnectionStrings = new Dictionary<string, string>().Synchronized();

        private static readonly IDictionary<KeyValuePair<Transaction, string>, Connection> TransactionConnections = new Dictionary<KeyValuePair<Transaction, string>, Connection>().Synchronized();

        public AdoComService(IConnectionStringRepository connectionStringRepository)
        {
            this.connectionStringRepository = connectionStringRepository;
        }

        private AdoComRecordset UpdateRecordset(AdoComServiceUpdateRecordsetCommand command)
        {
            var connection = GetConnection(command.ConnectionString);
            try
            {
                Recordset rs = command.Recordset.Recordset;
                rs.ActiveConnection = connection;
                rs.UpdateBatch();
                return new AdoComRecordset(rs);
            }
            finally
            {
                if (Transaction.Current == null)
                {
                    if (connection.State == 1) connection.Close();
                }
            }
        }

        private AdoComRecordset OpenRecordset(AdoComServiceOpenRecordsetCommand command)
        {
            bool setUserContextForAuditing = !(command.CommandType == (int)CommandType.Text && command.CommandText.StartsWith("SELECT", StringComparison.InvariantCultureIgnoreCase));
            var connection = GetConnection(command.ConnectionString, setUserContextForAuditing);
            try
            {
                Recordset rs = GetRecordset(command, connection);
                return rs.State == 0 ? new AdoComRecordset() : new AdoComRecordset(rs);
            }
            finally
            {
                if (Transaction.Current == null)
                {
                    if (connection.State == 1) connection.Close();
                }
            }
        }

        private static Recordset GetRecordset(AdoComServiceOpenRecordsetCommand command, Connection connection)
        {
            var rs = AdoCom.CreateRecordSet();
            rs.CursorType = CursorTypeEnum.adOpenStatic;
            rs.CursorLocation = CursorLocationEnum.adUseClient;
            rs.LockType = LockTypeEnum.adLockBatchOptimistic;

            var adoCommand = AdoCom.CreateCommand();
            
            adoCommand.ActiveConnection = connection;
            adoCommand.CommandTimeout = connection.CommandTimeout;

            if (command.CommandType.HasValue) adoCommand.CommandType = (CommandTypeEnum)command.CommandType;
            adoCommand.CommandText = command.CommandText;

            bool autoGeneratedParameters = adoCommand.Parameters.Count > 0;

            if (command.Parameters != null)
            {
                var inputParameterStartIndex = adoCommand.Parameters.Count > 0 && adoCommand.Parameters.OfType<Parameter>().First().Name == "@RETURN_VALUE" ? 1 : 0;

                // must add parameters before setting command text (otherwise ADODB will auto-add parameters for stored proc in CommandText).
                foreach (var parameter in command.Parameters)
                {
                    var adoParameter = adoCommand.CreateParameter(parameter.Name, (DataTypeEnum)parameter.DataType, (ParameterDirectionEnum)parameter.Direction, parameter.Size, parameter.Value);
                    if (adoParameter.Name.IsNotNullOrEmpty() && adoCommand.Parameters.OfType<Parameter>().Any(p => p.Name == adoParameter.Name))
                    {
                        ((dynamic)adoCommand.Parameters)[adoParameter.Name].Value = adoParameter.Value;
                    }
                    else if (adoParameter.Name.IsNullOrEmpty() && adoCommand.Parameters.Count > inputParameterStartIndex)
                    {
                        ((dynamic)adoCommand.Parameters)[inputParameterStartIndex].Value = adoParameter.Value;
                    }
                    else if (!autoGeneratedParameters)
                    {
                        ((dynamic)adoCommand).Parameters.Append(adoParameter);
                    }
                }
            }

            rs.Open(adoCommand);

            return rs;
        }

        private Connection GetConnection(string connectionString, bool setUserContext = true)
        {
            Connection connection;
            KeyValuePair<Transaction, string> currentTransactionConnection;
            if (Transaction.Current != null)
            {
                currentTransactionConnection = new KeyValuePair<Transaction, string>(Transaction.Current, connectionString);
                if (TransactionConnections.TryGetValue(currentTransactionConnection, out connection))
                {
                    if (setUserContext) { SetUserContext(connection); }
                    return connection;
                }
            }

            connection = AdoCom.CreateConnection();
            connection.CursorLocation = CursorLocationEnum.adUseClient;

            var strConn = connectionStringRepository[connectionString].IfNotNull(cs => cs.ConnectionString) ?? connectionString;
            connection.CursorLocation = CursorLocationEnum.adUseClient;
            connection.ConnectionString = GetOdbcConnectionString(strConn);
            connection.ConnectionTimeout = 1200;
            connection.Open();
            connection.CommandTimeout = 1200;

            if (Transaction.Current != null)
            {
                currentTransactionConnection = new KeyValuePair<Transaction, string>(Transaction.Current, connectionString);
                TransactionConnections.Add(currentTransactionConnection, connection);
                connection.BeginTrans();

                Transaction.Current.TransactionCompleted += ((sender, e) =>
                {
                    if (e.Transaction.TransactionInformation.Status == TransactionStatus.Committed)
                    {
                        connection.CommitTrans();
                    }
                    else
                    {
                        connection.RollbackTrans();
                    }

                    if (connection.State == 1)
                    {
                        connection.Close();
                    }

                    TransactionConnections.Remove(currentTransactionConnection);
                });
            }

            if (setUserContext) { SetUserContext(connection); }

            return connection;
        }

        private void SetUserContext(Connection connection)
        {
            var sql = DbConnections.GetSetUserContextSql(PrincipalContext.Current.IfNotNull(pc => pc.Principal));
            if (sql.IsNotNullOrEmpty())
            {
                object recordsAffected;
                connection.Execute(sql, out recordsAffected);
            }
        }

        private string GetOdbcConnectionString(string connectionString)
        {
            string result;
            if (OdbcConnectionStrings.TryGetValue(connectionString, out result))
            {
                return result;
            }

            try
            {
                OdbcConnectionStringBuilder odbcConnectionStringBuilder;
                if (DbConnections.TryGetConnectionStringBuilder(connectionString, out odbcConnectionStringBuilder) && odbcConnectionStringBuilder.Driver.IsNotNullOrEmpty())
                {
                    result = connectionString;
                }
                else
                {
                    SqlConnectionStringBuilder sqlConnectionStringBuilder;
                    if (DbConnections.TryGetConnectionStringBuilder(connectionString, out sqlConnectionStringBuilder))
                    {
                        var output = String.Format(@"Provider=sqloledb;Data Source={0};Initial Catalog={1};", sqlConnectionStringBuilder.DataSource, sqlConnectionStringBuilder.InitialCatalog);
                        if (!String.IsNullOrEmpty(sqlConnectionStringBuilder.UserID) && !String.IsNullOrEmpty(sqlConnectionStringBuilder.Password))
                        {
                            output += String.Format(@"User Id={0};Password={1};", sqlConnectionStringBuilder.UserID, sqlConnectionStringBuilder.Password);
                        }
                        else
                        {
                            output += "Integrated Security=SSPI;";
                        }
                        output += "Connect Timeout=1200;Command Timeout=1200;";
                        result = output;
                    }
                    else
                    {
                        result = connectionString;
                    }
                }
            }
            catch
            {
                result = connectionString;
            }
            OdbcConnectionStrings[connectionString] = result;
            return result;
        }

        public IEnumerable<AdoComRecordset> ExecuteCommands(IEnumerable<AdoComServiceCommand> commands)
        {
            var results = new List<AdoComRecordset>();

            foreach (var command in commands)
            {
                try
                {
                    if (command is AdoComServiceOpenRecordsetCommand)
                    {
                        results.Add(OpenRecordset((AdoComServiceOpenRecordsetCommand)command));
                    }
                    else if (command is AdoComServiceUpdateRecordsetCommand)
                    {
                        results.Add(UpdateRecordset((AdoComServiceUpdateRecordsetCommand)command));
                    }
                }
                catch (Exception ex)
                {
                    var exception = new Exception("Error executing command\r\n{0}".FormatWith(command), ex);
                    throw exception;
                }
            }

            return results;
        }
    }

    public static class AdoCom
    {
        private static readonly DesigntimeLicenseContext DesigntimeLicenseContext = new DesigntimeLicenseContext();
        private static readonly object SyncRoot = new object();

        public static Recordset CreateRecordSet()
        {
            lock (SyncRoot)
            {
                var originalLicenseManagerContext = LicenseManager.CurrentContext;
                try
                {
                    LicenseManager.CurrentContext = DesigntimeLicenseContext;
                    var recordSet = new Recordset();
                    return recordSet;
                }
                finally
                {
                    LicenseManager.CurrentContext = originalLicenseManagerContext;
                }
            }
        }

        public static Command CreateCommand()
        {
            lock (SyncRoot)
            {
                var originalLicenseManagerContext = LicenseManager.CurrentContext;
                try
                {
                    LicenseManager.CurrentContext = DesigntimeLicenseContext;
                    var command = new Command();
                    return command;
                }
                finally
                {
                    LicenseManager.CurrentContext = originalLicenseManagerContext;
                }
            }
        }

        public static Connection CreateConnection()
        {
            lock (SyncRoot)
            {
                var originalLicenseManagerContext = LicenseManager.CurrentContext;
                try
                {
                    LicenseManager.CurrentContext = DesigntimeLicenseContext;
                    var connection = new Connection();
                    return connection;
                }
                finally
                {
                    LicenseManager.CurrentContext = originalLicenseManagerContext;
                }
            }
        }

        public static Stream CreateStream()
        {
            lock (SyncRoot)
            {
                var originalLicenseManagerContext = LicenseManager.CurrentContext;
                try
                {
                    LicenseManager.CurrentContext = DesigntimeLicenseContext;
                    var stream = new Stream();
                    return stream;
                }
                finally
                {
                    LicenseManager.CurrentContext = originalLicenseManagerContext;
                }
            }
        }

        public static Recordset OpenRecordset(this IAdoComService service, string sql, string connectionString)
        {
            return service.ExecuteCommands(new[] { new AdoComServiceOpenRecordsetCommand { CommandText = sql, ConnectionString = connectionString } }).Select(i => Objects.IfNotNull<AdoComRecordset, Recordset>(i, r => r.Recordset)).FirstOrDefault();
        }

        public static Recordset UpdateRecordset(this IAdoComService service, Recordset recordset, string connectionString)
        {
            return service.ExecuteCommands(new[] { new AdoComServiceUpdateRecordsetCommand { Recordset = new AdoComRecordset(recordset), ConnectionString = connectionString } }).Select(i => i.IfNotNull(r => r.Recordset)).FirstOrDefault();
        }

        public static byte[] SerializeToByteArray(this Recordset recordset)
        {
            var s = CreateStream();
            s.Type = StreamTypeEnum.adTypeBinary;
            recordset.Save(s);
            return (byte[])s.Read(s.Size);
        }

        public static string SerializeToXml(this Recordset recordset)
        {
            var s = CreateStream();
            s.Type = StreamTypeEnum.adTypeText;
            recordset.Save(s, PersistFormatEnum.adPersistXML);
            return s.ReadText(s.Size);
        }

        public static Recordset DeserializeRecordset(byte[] data)
        {
            var s = CreateStream();
            s.Type = StreamTypeEnum.adTypeBinary;
            s.Open(Missing.Value);
            s.Write(data);
            s.Position = 0;

            var rs = CreateRecordSet();
            rs.CursorType = CursorTypeEnum.adOpenStatic;
            rs.CursorLocation = CursorLocationEnum.adUseClient;
            rs.LockType = LockTypeEnum.adLockBatchOptimistic;
            rs.Open(s, Missing.Value);
            return rs;
        }

        public static void ConvertToParameterizedSql(this Command command)
        {
            while (command.Parameters.Count != 0) command.Parameters.Delete(0);

            var parts = Regex.Split(command.CommandText, " WHERE ", RegexOptions.IgnoreCase);

            if (parts.Length != 2)
            {
                return;
            }

            command.CommandText = "sp_executesql";
            command.CommandType = CommandTypeEnum.adCmdStoredProc;

            var condition = parts[1];
            if (!condition.EndsWith(" ")) condition += " "; // required to ensure we match trailing space required by regex

            var valueParameters = new List<Parameter>();
            var valueParameterDefinitions = new List<string>();

            int parameterCount = 0;
            condition = Regex.Replace(condition, @"\s*=\s*(?<Value>('.*?')|\d*) ", match =>
                                                                                   {
                                                                                       var @group = match.Groups["Value"];
                                                                                       if (@group.Success & !string.IsNullOrWhiteSpace(@group.Value))
                                                                                       {
                                                                                           var parameterName = "@p" + parameterCount.ToString();

                                                                                           var parameterType = @group.Value.StartsWith("'") ? DataTypeEnum.adLongVarWChar : DataTypeEnum.adInteger;
                                                                                           object value = @group.Value.TrimStart((char)39).TrimEnd((char)39);
                                                                                           if ((parameterType == DataTypeEnum.adInteger))
                                                                                           {
                                                                                               value = Int32.Parse(@group.Value);
                                                                                           }

                                                                                           var parameter = command.CreateParameter(parameterName, parameterType, ParameterDirectionEnum.adParamInput, Math.Max(1, value.ToString().Length), value);
                                                                                           valueParameters.Add(parameter);
                                                                                           valueParameterDefinitions.Add(parameterName + (parameterType == DataTypeEnum.adInteger ? " int" : " nvarchar(max)"));

                                                                                           parameterCount = parameterCount + 1;
                                                                                           return " = " + parameterName + " ";
                                                                                       }

                                                                                       return match.Value;
                                                                                   });

            var parameterizedSql = parts[0] + " WHERE " + condition;

            var sqlParameter = command.CreateParameter("@statement", DataTypeEnum.adLongVarWChar, ParameterDirectionEnum.adParamInput, parameterizedSql.Length, parameterizedSql);
            command.Parameters.Append(sqlParameter);

            var valueParameterDefinitionsSql = string.Join(", ", valueParameterDefinitions);
            var parametersParameter = command.CreateParameter("@params", DataTypeEnum.adLongVarWChar, ParameterDirectionEnum.adParamInput, valueParameterDefinitionsSql.Length, valueParameterDefinitionsSql);
            command.Parameters.Append(parametersParameter);

            foreach (var p in valueParameters)
            {
                command.Parameters.Append(p);
            }
        }
    }
}