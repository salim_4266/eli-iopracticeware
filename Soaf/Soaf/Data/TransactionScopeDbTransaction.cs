﻿using System;
using System.Data.Common;
using System.Transactions;
using IsolationLevel = System.Data.IsolationLevel;

namespace Soaf.Data
{
    /// <summary>
    /// A Db Transaction that uses an ambient TransactionScope.
    /// </summary>
    internal class TransactionScopeDbTransaction : DbTransaction
    {
        private readonly DbConnection dbConnection;
        private readonly IsolationLevel isolationLevel;
        private readonly TransactionScope scope;

        public TransactionScopeDbTransaction(DbConnection dbConnection, IsolationLevel isolationLevel = default(IsolationLevel))
        {
            this.dbConnection = dbConnection;
            this.isolationLevel = isolationLevel;
            scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolationLevel.TranslateEnum<System.Transactions.IsolationLevel>(), Timeout = TimeSpan.Zero });
        }

        protected override DbConnection DbConnection
        {
            get { return dbConnection; }
        }

        public override IsolationLevel IsolationLevel
        {
            get { return isolationLevel; }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            scope.Dispose();
        }

        public override void Commit()
        {
            scope.Complete();
        }

        public override void Rollback()
        {
            scope.Dispose();
        }
    }
}