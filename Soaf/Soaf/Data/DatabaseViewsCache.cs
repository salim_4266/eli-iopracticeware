using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Soaf.ComponentModel;
using Soaf.Threading;

namespace Soaf.Data
{
    /// <summary>
    /// Caches views as global temp tables for specified cache time
    /// </summary>
    internal class DatabaseViewsCache : IDisposable
    {
        private const string TempTablesCatalogNameFormat = "{0}{1}TempTables";

        /// <summary>
        /// Additional time to keep alive expired cache (to attempt avoid crashing queries in execution)
        /// </summary>
        private static readonly TimeSpan GracefulCacheTransitionDelay = TimeSpan.FromSeconds(10);

        /// <summary>
        /// The minimum cache expiration (for binding to or creating new)
        /// </summary>
        private static readonly TimeSpan MinimumCacheExpiration = TimeSpan.FromSeconds(20);

        private readonly string connectionString;
        private readonly bool forceNewCache;
        private readonly object queryOptimizationLock = new object();
        private readonly TimeSpan desiredCacheExpiration;
        private readonly ConcurrentDictionary<string, DbObjectInfo> cachedDbObjects;

        private bool isInitialized;
        private DateTime cacheExpirationUtc;
        private ManualResetEvent keepAliveExitEvent;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseViewsCache"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="cacheExpiration">The cache expiration.</param>
        /// <param name="forceNewCache">if set to <c>true</c> ensures that new cache will be built upon initialization.</param>
        public DatabaseViewsCache(string connectionString, TimeSpan cacheExpiration, bool forceNewCache)
        {
            desiredCacheExpiration = cacheExpiration > MinimumCacheExpiration
                ? cacheExpiration
                : MinimumCacheExpiration;
            this.connectionString = connectionString;
            this.forceNewCache = forceNewCache;
            
            cachedDbObjects = new ConcurrentDictionary<string, DbObjectInfo>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Optimizes command execution
        /// </summary>
        /// <param name="executingCommand"></param>
        /// <param name="viewSchemasRegex"></param>
        /// <param name="fastDataCopy"></param>
        public void OptimizeCommand(IDbCommand executingCommand, string viewSchemasRegex, bool fastDataCopy)
        {
            // Locate all mentioned objects in query
            var commandQueryObjects = Regex.Matches(executingCommand.CommandText, @"(?<ObjectName>\[({0})\]\.\[.*?\])".FormatWith(viewSchemasRegex))
                .OfType<Match>()
                .Select(m => m.Groups["ObjectName"].Value)
                .Distinct()
                .ToList();

            lock (queryOptimizationLock)
            {
                EnsureInitialized();

                // See if we have any of located objects cached
                var cachedObjects = new List<DbObjectInfo>();
                foreach (var commandQueryObject in commandQueryObjects)
                {
                    DbObjectInfo cachedObject;
                    if (cachedDbObjects.TryGetValue(commandQueryObject, out cachedObject))
                    {
                        cachedObjects.Add(cachedObject);
                    }
                }

                // Nothing to optimize? -> quit
                if (!cachedObjects.Any()) return;

                var updatedCommandText = new StringBuilder(executingCommand.CommandText);
                var keepAliveStatements = new StringBuilder();
                foreach (var cachedObject in cachedObjects)
                {
                    // Replace references to view with references to temp table
                    updatedCommandText.Replace(cachedObject.ObjectName, "[##{0}]".FormatWith(cachedObject.TempTableName));

                    // We don't populate temp table rows upon initialization, so lazy append them if required
                    keepAliveStatements.AppendLine(GetEnsureTempTableRowsSql(cachedObject, fastDataCopy));
                }

                // Update command text
                executingCommand.CommandText = String.Format(@"{0}{1}", keepAliveStatements, updatedCommandText);
            }
        }

        private void EnsureInitialized()
        {
            if (isInitialized && (cacheExpirationUtc > DateTime.UtcNow)) return;

            ResetCacheInternal(true);

            var dbObjectsPopulatedWait = new ManualResetEvent(false);
            Task.Factory.StartNewWithCurrentTransaction(() =>
            {
                SqlConnection cacheInitializationConnection = null;
                try
                {
                    var initializedNewCache = false;

                    RetryUtility.ExecuteWithRetry(() =>
                    {
                        try
                        {
                            cacheInitializationConnection = new SqlConnection(connectionString);
                            cacheInitializationConnection.Open();

                            // Ensures connection is really closed once disposed
                            SqlConnection.ClearPool(cacheInitializationConnection);

                            if (TryBindToExistingCache(cacheInitializationConnection)) return;

                            InitializeCache(cacheInitializationConnection);
                            initializedNewCache = true;
                        }
                        catch (Exception)
                        {
                            // Ensure connection is closed between retries
                            if (cacheInitializationConnection != null)
                            {
                                cacheInitializationConnection.Dispose();
                                cacheInitializationConnection = null;   
                            }
                            throw;
                        }
                    }, 3, TimeSpan.FromSeconds(1));

                    // We are initialized now
                    isInitialized = true;
                    var shouldKeepAlive = initializedNewCache && cachedDbObjects.Count > 0;

                    dbObjectsPopulatedWait.Set();

                    if (shouldKeepAlive)
                    {
                        var keepAlivePeriod = cacheExpirationUtc - DateTime.UtcNow + GracefulCacheTransitionDelay; // To ensure we don't kill cache before all queries are done
                        // Keep alive unit expiration -> if anything goes wrong cache will be reset
                        var keepAliveExitEventLocal = new ManualResetEvent(false);
                        keepAliveExitEvent = keepAliveExitEventLocal;
                        keepAliveExitEventLocal.WaitOne(keepAlivePeriod);
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(string.Format("Error caching views: {0}", ex));
                }
                finally
                {
                    try
                    {
                        if (cacheInitializationConnection != null) cacheInitializationConnection.Dispose();
                        // Let go initialization thread
                        dbObjectsPopulatedWait.Set();
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine(string.Format("Error releasing view caching connection: {0}", ex));
                    }
                }
            });

            // Wait until cache is populated
            dbObjectsPopulatedWait.WaitOne();
        }

        private bool TryBindToExistingCache(SqlConnection connection)
        {
            // Implicitly within keep alive lock

            if (forceNewCache) return false;

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                var tempTablesCatalog = string.Format(TempTablesCatalogNameFormat, connection.Database, "%Shared");
                tempTablesCatalog = CorrectTempTableName(tempTablesCatalog);

                cmd.CommandText = String.Format(@"
SET NOCOUNT ON

declare @validateCompatibleCache nvarchar(1000)
	,@compatibleCacheCatalogName nvarchar(128)
declare searchCompatibleCache cursor for 
select '
declare @viewCacheExpires datetime,
    @viewCached datetime,
	@desiredMinimumExpiration datetime,
	@desiredMaximumCached datetime

select @desiredMinimumExpiration = dateadd(second, {2}, GETUTCDATE())
    ,@desiredMaximumCached = dateadd(second, -{1}, GETUTCDATE())

select top 1 @viewCacheExpires = Expires, @viewCached = Cached
from ' + name + '

if (@viewCacheExpires is null or @viewCached is null
	or datediff(second, @desiredMinimumExpiration, @viewCacheExpires) < 0
	or datediff(second, @desiredMaximumCached, @viewCached) < 0) return

set @compatibleCacheCatalogNameOUT = ''' + name + ''''
from tempdb.sys.tables
where name like '##{0}'

open searchCompatibleCache
while 1=1
begin
    fetch searchCompatibleCache into @validateCompatibleCache
    if @@fetch_status != 0 break
	EXECUTE sp_executesql @validateCompatibleCache, N'@compatibleCacheCatalogNameOUT nvarchar(128) OUTPUT', @compatibleCacheCatalogNameOUT = @compatibleCacheCatalogName OUTPUT
	if (@compatibleCacheCatalogName is not null) break
end
close searchCompatibleCache;
deallocate searchCompatibleCache

if (@compatibleCacheCatalogName is not null)
begin
	exec ('select * from ' + @compatibleCacheCatalogName + ' order by SourceTableName')
    -- Also return expiration along with server utc now
    exec ('select MIN(Expires) As Expires, GETUTCDATE() As ServerUtcNow from ' + @compatibleCacheCatalogName)
end
", tempTablesCatalog, desiredCacheExpiration.TotalSeconds, MinimumCacheExpiration.TotalSeconds);

                var objectsFound = FillObjectsCache(cmd);
                Debug.WriteLineIf(objectsFound, 
                    string.Format("Re-using existing cache for {0}, expires: {1}, views count: {2}, random temp table: {3}",
                        cmd.Connection.Database, cacheExpirationUtc, cachedDbObjects.Count, cachedDbObjects.Select(o => o.Value.TempTableName).FirstOrDefault()));

                return objectsFound;
            }
        }

        /// <summary>
        /// Initializes the cache.
        /// 1. Build a list of views to optimize (all within specified view schemas)
        /// 2. Create empty temp table for each view (see comments inside SQL to find out why we simply don't do select ##X from Y)
        /// 3. Validate source view is selectedable from and create unique index over Id column is it's there
        /// 4. Return a list of valid created temp tables for optimization 
        /// </summary>
        /// <param name="connection">The connection.</param>
        private void InitializeCache(SqlConnection connection)
        {
            // Implicitly within keep alive lock

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                var uniqueSuffix = ShortGuid.NewGuid().Value.Replace("-", "");
                uniqueSuffix = forceNewCache ? uniqueSuffix : uniqueSuffix + "Shared";

                var tempTablesCatalog = string.Format(TempTablesCatalogNameFormat, connection.Database, uniqueSuffix);
                tempTablesCatalog = CorrectTempTableName(tempTablesCatalog);

                cmd.CommandText = String.Format(@"
SET NOCOUNT ON

declare @viewTempTableSuffix nvarchar(40)
    ,@viewTempTableExpiration datetime
    ,@viewTempTableCached datetime

select @viewTempTableSuffix = '{1}'
select @viewTempTableCached = GETUTCDATE()
select @viewTempTableExpiration = dateadd(second, {2}, @viewTempTableCached)

declare @viewTempTablesCatalog table(
	GeneratedTempTableName nvarchar(116),
	SourceTableName nvarchar(500),
    Cached datetime,
    Expires datetime
)

declare @cmd nvarchar(max) 
	,@viewSchema nvarchar(128)
	,@viewName nvarchar(128)
	,@viewTempTableName nvarchar(116)
	,@viewFullName nvarchar(500)

declare @ViewColumnDefinitions table(
	ColumnName nvarchar(128),
	ColumnType nvarchar(200),
    ColumnOrdinal int
);

declare viewOptimizationCandidates cursor for 
select TABLE_SCHEMA, TABLE_NAME
from INFORMATION_SCHEMA.VIEWS
where TABLE_SCHEMA in ('model', 'dbo') and TABLE_CATALOG = DB_NAME()

open viewOptimizationCandidates
while 1=1
begin
    fetch viewOptimizationCandidates into @viewSchema, @viewName
    if @@fetch_status != 0 break
	set @viewFullName = '[' + @viewSchema + '].[' + @viewName + ']'
	set @viewTempTableName = RIGHT(REPLACE(REPLACE(DB_NAME() + @viewSchema + @viewName + @viewTempTableSuffix, '.', ''), ' ', ''), 116)

	-- We can't use SELECT * INTO ##X FROM Y, since it copies also Identity constraint for some columns comming from tables. Later we can't do INSERT INTO due to that
	insert into @ViewColumnDefinitions
	SELECT c.name as ColumnName, 
		t.name + CASE 
		WHEN t.name IN ('binary', 'varbinary', 'varchar', 'nvarchar') 
			THEN '(' + (CASE WHEN c.max_length = -1 THEN 'max' WHEN t.name = 'nvarchar' AND c.max_length > 1 THEN CONVERT(nvarchar, c.max_length / 2) ELSE CONVERT(nvarchar, c.max_length) END) + ')'
		WHEN t.name IN ('decimal') 
			THEN '(' + CONVERT(nvarchar, c.precision) + ',' + CONVERT(nvarchar, c.scale) + ')'
		ELSE '' END as ColumnType,
        c.column_id as ColumnOrdinal
	FROM sys.columns c 
	JOIN sys.types t ON 
		c.system_type_id = t.system_type_id 
	WHERE c.object_id = OBJECT_ID(@viewFullName) AND t.name <> 'sysname'
	ORDER BY c.column_id

	SET @cmd = NULL
	SELECT @cmd = COALESCE(@cmd + ', ', '') + '[' + ColumnName + '] ' + ColumnType FROM @ViewColumnDefinitions ORDER BY ColumnOrdinal
	SELECT @cmd = '
	-- Create new temp table
	create table ##' + @viewTempTableName + ' (' + @cmd + ')

	-- Just checking both source view and compatibility with temp table definition
	insert into ##' + @viewTempTableName + '
	select * from ' + @viewFullName + '
	where 1=0

	-- Speed up joins by Id column if found
	IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS isc WHERE isc.TABLE_NAME = ''##' + @viewTempTableName + ''' AND isc.COLUMN_NAME = ''Id'')
	BEGIN
		CREATE UNIQUE CLUSTERED INDEX [IX_Temp' + @viewTempTableName + 'Key] ON [##' + @viewTempTableName + '](Id)
	END'
	DELETE FROM @ViewColumnDefinitions

	BEGIN TRY
		exec(@cmd)
		insert into @viewTempTablesCatalog (GeneratedTempTableName, SourceTableName, Cached, Expires)
		values (@viewTempTableName, @viewFullName, @viewTempTableCached, @viewTempTableExpiration)
	END TRY
	BEGIN CATCH
		--PRINT ERROR_MESSAGE()
	END CATCH
end
close viewOptimizationCandidates;
deallocate viewOptimizationCandidates

-- Make catalog available for reuse
select * into ##{0} from @viewTempTablesCatalog order by SourceTableName

-- Get back view temp tables caching result
select * from ##{0} order by SourceTableName
-- Also return expiration along with server utc now
select MIN(Expires) As Expires, GETUTCDATE() As ServerUtcNow from ##{0}
", tempTablesCatalog, uniqueSuffix, desiredCacheExpiration.TotalSeconds);

                if (!FillObjectsCache(cmd))
                {
                    throw new InvalidOperationException("Generating cache failed");
                }

                Debug.WriteLine("Built cache for {0}, expires: {1}, views count: {2}, random temp table: {3}",
                    cmd.Connection.Database, cacheExpirationUtc, cachedDbObjects.Count, cachedDbObjects.Select(o => o.Value.TempTableName).FirstOrDefault());
            }
        }

        private bool FillObjectsCache(SqlCommand cmd)
        {
            cachedDbObjects.Clear();

            using (var result = new DataSet())
            {
                new SqlDataAdapter(cmd).Fill(result);

                // Found existing cache?
                if (result.Tables.Count == 0) return false;

                foreach (DataRow row in result.Tables[0].Rows)
                {
                    var cachedEntry = new DbObjectInfo(
                        row["SourceTableName"].CastTo<string>(),
                        row["GeneratedTempTableName"].CastTo<string>());
                    cachedDbObjects[cachedEntry.ObjectName] = cachedEntry;
                }

                if (result.Tables.Count > 1)
                {
                    var retrievedExpirationUtc = result.Tables[1].Rows[0]["Expires"].CastTo<DateTime>();
                    var serverNowUtc = result.Tables[1].Rows[0]["ServerUtcNow"].CastTo<DateTime>();

                    // Recalculate expiration according to local time
                    cacheExpirationUtc = DateTime.UtcNow + (retrievedExpirationUtc - serverNowUtc);
                }
                else
                {
                    cacheExpirationUtc = DateTime.UtcNow.Add(desiredCacheExpiration);
                }

                return true;
            }
        }

        private static string GetEnsureTempTableRowsSql(DbObjectInfo objectInfo, bool nolock)
        {
            // Command to ensure rows were lazy populated from source view into temp table

            // Using transaction to scope acquired locks on temp table for only initialization
            var command = @"
if not exists (select top 1 * from ##{0})
begin
    begin transaction
        insert into ##{0}
        select * from {1} {2}
        where not exists (SELECT TOP 1 * FROM ##{0} with (tablockx));
    commit
end
".FormatWith(objectInfo.TempTableName, objectInfo.ObjectName, nolock ? "with(nolock)" : string.Empty);

            return command;
        }

        private static string CorrectTempTableName(string desiredTempTableName)
        {
            // Strip out any invalid characters for Db name
            var generatedTempTableName = desiredTempTableName
                .Replace("[", string.Empty)
                .Replace("]", string.Empty)
                .Replace(".", string.Empty)
                .Replace(" ", string.Empty)
                .Truncate(116, true); // Max temp table name length in Sql Server
            return generatedTempTableName;
        }

        public void InvalidateCacheForTempTable(string tempTableName)
        {
            lock (queryOptimizationLock)
            {
                // Reset only if temp table among cached objects, since cache might have already been reset
                if (cachedDbObjects.Values.Any(v => 
                    string.Equals(v.TempTableName, tempTableName, StringComparison.OrdinalIgnoreCase)))
                {
                    ResetCacheInternal(false);    
                }  
            }
        }

        private void ResetCacheInternal(bool graceful)
        {
            // Reset initialized state
            isInitialized = false;

            // Reset cache
            cachedDbObjects.Clear();

            // Reset keep alive
            var activeKeepAliveExitEvent = keepAliveExitEvent;
            keepAliveExitEvent = null;
            if (activeKeepAliveExitEvent == null) return;

            if (!graceful)
            {
                activeKeepAliveExitEvent.Set();
            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    // 10 seconds graceful transition between cached tables
                    Thread.Sleep(GracefulCacheTransitionDelay);

                    // Cancel keep alive
                    activeKeepAliveExitEvent.Set();
                });
            }
        }

        public void Dispose()
        {
            lock (queryOptimizationLock)
            {
                ResetCacheInternal(false);   
            }
        }

        private class DbObjectInfo
        {
            private readonly string tempTableName;
            private readonly string objectName;

            public DbObjectInfo(string objectName, string tempTableName)
            {
                this.objectName = objectName;
                this.tempTableName = tempTableName;
            }

            public string ObjectName
            {
                get { return objectName; }
            }

            public string TempTableName
            {
                get { return tempTableName; }
            }

            public override string ToString()
            {
                return String.Join(" <> ", TempTableName, ObjectName);
            }
        }
    }
}