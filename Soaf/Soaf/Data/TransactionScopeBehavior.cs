﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Transactions;
using Soaf.ComponentModel;
using Soaf.Reflection;

namespace Soaf.Data
{
    [AttributeUsage(AttributeTargets.Method)]
    public class TransactionScopeAttribute : Attribute
    {
        public TransactionScopeOption TransactionScopeOption { get; private set; }
        public IsolationLevel IsolationLevel { get; private set; }
        public int TimeoutMinutes { get; set; }
        public int TimeoutSeconds { get; set; }

        public TransactionScopeAttribute(TransactionScopeOption transactionScopeOption, System.Transactions.IsolationLevel isolationLevel)
        {
            TransactionScopeOption = transactionScopeOption;
            IsolationLevel = isolationLevel;
        }
    }

    /// <summary>
    /// Denotes that a type supports auto-creation of transaction scopes for methods.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class SupportsTransactionScopesAttribute : ConcernAttribute
    {
        public SupportsTransactionScopesAttribute()
            : base(typeof(TransactionScopeBehavior), 10000)
        {
        }
    }

    /// <summary>
    /// Behavior for auto-transactions.
    /// </summary>
    public class TransactionScopeBehavior : AttributedMemberInterceptor
    {
        private static readonly IEnumerable<Type> Attributes = new[] { typeof(TransactionScopeAttribute) };

        public override IEnumerable<Type> AttributeTypes
        {
            get { return Attributes; }
        }

        public override void Intercept(IInvocation invocation)
        {
            var transactionScopeAttribute = invocation.Method.GetAttribute<TransactionScopeAttribute>();
            var isolationLevel = transactionScopeAttribute.IfNotNull(a => a.IsolationLevel, () => System.Transactions.IsolationLevel.ReadUncommitted);
            var timeoutMinutes = transactionScopeAttribute.IfNotNull(a => a.TimeoutMinutes);
            var timeoutSeconds = transactionScopeAttribute.IfNotNull(a => a.TimeoutSeconds);

            TimeSpan timeout = timeoutMinutes != 0 || timeoutSeconds != 0
                ? TimeSpan.FromMinutes(timeoutMinutes) + TimeSpan.FromSeconds(timeoutSeconds)
                : TimeSpan.FromMinutes(20);

            var transactionScopeOption = transactionScopeAttribute.TransactionScopeOption;

            if (Transaction.Current != null && Transaction.Current.IsolationLevel == isolationLevel) transactionScopeOption = TransactionScopeOption.Required;

            string commandExecutingKey = GetType().FullName;
            var key = new object();
            CallContext.LogicalSetData(commandExecutingKey, key);

            EventHandler<DbCommandExecutingEventArgs> executing = (sender, e) =>
                                                                  {
                                                                      if (CallContext.GetData(commandExecutingKey) != key) return;

                                                                      if (transactionScopeOption == TransactionScopeOption.Suppress)
                                                                      {
                                                                          string setIsolationLevel = null;

                                                                          switch (isolationLevel)
                                                                          {
                                                                              case System.Transactions.IsolationLevel.ReadCommitted:
                                                                                  setIsolationLevel = "READ COMMITTED";
                                                                                  break;
                                                                              case System.Transactions.IsolationLevel.ReadUncommitted:
                                                                                  setIsolationLevel = "READ UNCOMMITTED";
                                                                                  break;
                                                                              case System.Transactions.IsolationLevel.RepeatableRead:
                                                                                  setIsolationLevel = "REPEATABLE READ";
                                                                                  break;
                                                                              case System.Transactions.IsolationLevel.Serializable:
                                                                              case System.Transactions.IsolationLevel.Snapshot:
                                                                                  setIsolationLevel = isolationLevel.ToString().ToUpper();
                                                                                  break;
                                                                          }

                                                                          if (setIsolationLevel != null)
                                                                          {
                                                                              setIsolationLevel = "SET TRANSACTION ISOLATION LEVEL " + setIsolationLevel;
                                                                              var setIsolationLevelCommand = e.DbCommand.Connection.CreateCommand();
                                                                              setIsolationLevelCommand.CommandText = setIsolationLevel;
                                                                              setIsolationLevelCommand.Transaction = e.DbCommand.Transaction;
                                                                              setIsolationLevelCommand.ExecuteNonQuery();
                                                                          }
                                                                      }

                                                                      e.DbCommand.CommandTimeout = (int)timeout.TotalSeconds;
                                                                  };
            try
            {
                using (var ts = new TransactionScope(transactionScopeOption, new TransactionOptions { IsolationLevel = isolationLevel, Timeout = timeout }))
                {

                    AdoService.CommandExecuting += executing;
                    if (invocation.CanProceed) invocation.Proceed();
                    ts.Complete();
                }
            }
            finally
            {
                AdoService.CommandExecuting -= executing;
                CallContext.LogicalSetData(commandExecutingKey, null);
            }
        }
    }
}