﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Domain;
using Soaf.Linq.Expressions;
using Soaf.Reflection;

namespace Soaf.Linq
{
    /// <summary>
    ///   Defines a type that can create queries for an element type.
    /// </summary>
    internal interface IQueryProducer
    {
        IQueryable CreateQuery(Type elementType);
    }

    /// <summary>
    ///   Defines a type that supports applying QueryInclusions to a queryable.
    /// </summary>
    internal interface IIncludeProvider
    {
        IQueryable ApplyQueryInclusions(IQueryable queryable, IEnumerable<QueryInclusion> inclusions);
    }

    /// <summary>
    ///   A query inclusion path expression to include when querying data.
    /// </summary>
    internal class QueryInclusion
    {
        internal QueryInclusion(Expression expression)
        {
            Expression = expression;
        }

        /// <summary>
        ///   Gets or sets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public Expression Expression { get; private set; }
    }

    /// <summary>
    ///   A non-generic LINQ Query base class.
    /// </summary>
    internal abstract class Query : IOrderedQueryable
    {
        private readonly Type elementType;
        private readonly Expression expression;
        private readonly IQueryProvider provider;

        protected Query(IQueryProvider provider, Expression expression, Type elementType)
        {
            this.expression = expression ?? Expression.Constant(this);

            this.provider = provider;

            this.elementType = elementType ?? (this.expression.Type.IsGenericTypeFor(typeof(IQueryable<>)) ? this.expression.Type.GetGenericArguments()[0] : this.expression.Type);
        }

        #region IOrderedQueryable Members

        public Expression Expression
        {
            get { return expression; }
        }

        public Type ElementType
        {
            get { return elementType; }
        }

        public IQueryProvider Provider
        {
            get { return provider; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)Provider.Execute(Expression)).GetEnumerator();
        }

        #endregion

        public override string ToString()
        {
            return expression is ConstantExpression && (expression as ConstantExpression).Value == this ? string.Format("Query`1[[{0}]]", ElementType.Name) : expression.ToString();
        }
    }

    /// <summary>
    ///   A generic LINQ query.
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    internal class Query<T> : Query, IOrderedQueryable<T>
    {
        public Query(IQueryProvider provider, Expression expression)
            : base(provider, expression, typeof(T))
        {
        }

        #region IOrderedQueryable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)Provider.Execute(Expression)).GetEnumerator();
        }

        #endregion
    }

    /// <summary>
    ///   Represents a scope for the duration of the execution of a LINQ query.
    /// </summary>
    public class QueryExecutionScope
    {
        public event EventHandler<EventArgs<Expression>> Started = delegate { };

        public event EventHandler<EventArgs<Expression>> Completed = delegate { };

        public void Start(Expression e)
        {
            Started(this, new EventArgs<Expression>(e));
        }

        public void Complete(Expression e)
        {
            Completed(this, new EventArgs<Expression>(e));
        }
    }

    /// <summary>
    /// An IQueryProvider that supports interception of query execution events.
    /// </summary>
    public interface INotifyingQueryProvider : IQueryProvider
    {
        event EventHandler<QueryExecutingEventArgs> Executing;

        event EventHandler<QueryExecutedEventArgs> Executed;

        event EventHandler<CreatingQueryEventArgs> CreatingQuery;

        event EventHandler<CreatedQueryEventArgs> CreatedQuery;
    }

    /// <summary>
    ///   A base IQueryProvider implementation that uses the generic Query and non-generic Query implementations.
    /// </summary>
    internal abstract class QueryProvider : INotifyingQueryProvider
    {
        protected static readonly MethodInfo CreateQueryMethod = Reflector.GetMember<QueryProvider>(p => p.CreateQuery<object>(null)).CastTo<MethodInfo>().GetGenericMethodDefinition();

        [ThreadStatic]
        private static QueryExecutionScope queryExecutionScope;
        protected QueryExecutionScope QueryExecutionScope
        {
            get { return queryExecutionScope; }
            set { queryExecutionScope = value; }
        }

        #region IQueryProvider Members

        public IQueryable CreateQuery(Expression expression)
        {
            return (IQueryable)CreateQueryMethod.MakeGenericMethod(expression.Type.FindElementType()).Invoke(this, new[] { expression });
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return (IQueryable<TElement>)CreateQuery(expression, e => new Query<TElement>(this, e));
        }

        /// <summary>
        /// Creates a query using the specified factory and expression. Fires Creating/Created handlers.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <param name="queryFactory">The query factory.</param>
        /// <returns></returns>
        protected virtual IQueryable CreateQuery(Expression expression, Func<Expression, IQueryable> queryFactory)
        {
            var args = new CreatingQueryEventArgs { Expression = expression };

            if (args.Query != null) return args.Query;

            var result = queryFactory(args.Expression);

            var createdArgs = new CreatedQueryEventArgs(args.Expression) { Query = result };

            OnCreatedQuery(createdArgs);

            return createdArgs.Query;
        }

        object IQueryProvider.Execute(Expression expression)
        {
            try
            {
                var executingArgs = new QueryExecutingEventArgs { Expression = expression };
                OnExecuting(executingArgs);

                expression = executingArgs.Expression;

                bool startedQueryExecutionScope = false;
                if (QueryExecutionScope == null)
                {
                    QueryExecutionScope = new QueryExecutionScope();
                    QueryExecutionScope.Start(expression);
                    startedQueryExecutionScope = true;
                }

                var executedArgs = new QueryExecutedEventArgs(expression, executingArgs.Handled);
                try
                {
                    if (!executingArgs.Handled)
                    {
                        expression = expression.ExpandInvocations().ApplyQueryInclusions().EvaluateLocalExpressions().SimplifyComparisons();

                        executedArgs.Result = Execute(expression);
                    }
                    else
                    {
                        executedArgs.Result = executingArgs.Result;
                    }
                }
                catch (Exception ex)
                {
                    if (startedQueryExecutionScope)
                    {
                        QueryExecutionScope.Complete(expression);
                        QueryExecutionScope = null;
                    }
                    executedArgs.Exception = ex;

                    OnExecuted(executedArgs);
                    if (executedArgs.Exception != null) throw;
                }

                if (startedQueryExecutionScope)
                {
                    QueryExecutionScope.EnsureNotDefault().Complete(expression);
                    QueryExecutionScope = null;
                    OnExecuted(executedArgs);
                }
                else
                {
                    QueryExecutionScope.Completed += delegate { OnExecuted(executedArgs); };
                }

                return executedArgs.Result;
            }
            catch (TargetInvocationException ex)
            {
                ex.InnerException.RethrowInnerException();
                // ReSharper disable HeuristicUnreachableCode
                throw;
                // ReSharper restore HeuristicUnreachableCode
            }
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return (TResult)this.CastTo<IQueryProvider>().Execute(expression);
        }

        #endregion

        public event EventHandler<QueryExecutingEventArgs> Executing;

        public event EventHandler<QueryExecutedEventArgs> Executed;

        public event EventHandler<CreatingQueryEventArgs> CreatingQuery;

        public event EventHandler<CreatedQueryEventArgs> CreatedQuery;

        protected abstract object Execute(Expression expression);

        protected virtual void OnExecuting(QueryExecutingEventArgs args)
        {
            Executing.Fire(this, args);
        }

        protected virtual void OnExecuted(QueryExecutedEventArgs args)
        {
            Executed.Fire(this, args);
        }

        protected virtual void OnCreatingQuery(CreatingQueryEventArgs args)
        {
            CreatingQuery.Fire(this, args);
        }

        protected virtual void OnCreatedQuery(CreatedQueryEventArgs args)
        {
            CreatedQuery.Fire(this, args);
        }
    }

    public class QueryExecutingEventArgs : EventArgs
    {
        public Expression Expression { get; set; }

        public object Result { get; set; }

        /// <summary>
        /// If true, query execution will be suppressed and the result will be returned.
        /// </summary>
        public bool Handled { get; set; }
    }

    public class QueryExecutedEventArgs : EventArgs
    {
        public QueryExecutedEventArgs(Expression expression, bool handledExternally)
        {
            Expression = expression;
            HandledExternally = handledExternally;
        }

        public Expression Expression { get; private set; }

        /// <summary>
        /// Gets a value indicating whether [handled externally]. If true, handler set Handled to true on Executing.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [handled externally]; otherwise, <c>false</c>.
        /// </value>
        public bool HandledExternally { get; private set; }

        public object Result { get; set; }

        public Exception Exception { get; set; }
    }

    public class CreatingQueryEventArgs : EventArgs
    {
        public Expression Expression { get; set; }

        public IQueryable Query { get; set; }
    }

    public class CreatedQueryEventArgs : EventArgs
    {
        public CreatedQueryEventArgs(Expression expression)
        {
            Expression = expression;
        }

        public Expression Expression { get; private set; }

        public IQueryable Query { get; set; }
    }

    internal class QueryableFactoryLoader
    {
        private readonly Func<Type, IQueryable> queryableFactory;
        private readonly object target;
        private readonly int degreeOfParallelism;
        private readonly IDictionary<LoadedObjectKey, LoadedObject> loadedObjects = new Dictionary<LoadedObjectKey, LoadedObject>();
        private readonly IDictionary<TypePairLock, object> locks = new Dictionary<TypePairLock, object>();
        private readonly List<Tuple<object, PropertyInfo>> loadFailures = new List<Tuple<object, PropertyInfo>>();

        /// <summary>
        /// Objects and properties that tried to be loaded, but no corresponding entity was found.
        /// </summary>
        /// <value>
        /// The load failures.
        /// </value>
        public IEnumerable<Tuple<object, PropertyInfo>> LoadFailures
        {
            get { return loadFailures; }
        }

        public QueryableFactoryLoader(Func<Type, IQueryable> queryableFactory, object target, int degreeOfParallelism = 5)
        {
            this.queryableFactory = queryableFactory;
            this.target = target;
            this.degreeOfParallelism = degreeOfParallelism;
        }

        /// <summary>
        /// Gets a queryable factory that returns a queryable for a specific entity type.
        /// </summary>
        /// <param name="source">The object context.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> GetQueryableFactory(object source)
        {
            var queryablePropertiesByType = source.GetType().GetProperties().Where(p => p.GetIndexParameters().Length == 0 && p.PropertyType.IsGenericTypeFor(typeof(IQueryable<>)))
                .ToDictionary(p => p.PropertyType.FindElementType());

            return t =>
                       {
                           PropertyInfo property;
                           if (!queryablePropertiesByType.TryGetValue(t, out property))
                           {
                               property = queryablePropertiesByType.Values.FirstOrDefault(p => p.PropertyType.FindElementType().IsAssignableFrom(t))
                                   .EnsureNotDefault("Could not find queryable for entity type {0}.".FormatWith(t.Name));

                               var queryable = property.GetValue(source, null);

                               return (IQueryable)typeof(Queryable).GetMethod("OfType").MakeGenericMethod(t).Invoke(null, new[] { queryable });
                           }

                           return (IQueryable)property.GetValue(source, null);
                       };
        }

        /// <summary>
        /// Loads the target along the specified path, using the provided queryable factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="paths">The paths.</param>
        public void Load<T>(Expression<Func<T, object>>[] paths)
        {
            Dictionary<object, ObjectState> unitOfWorkObjects = null;

            Dictionary<Type, TypeToLoad> typesToLoad = new Dictionary<Type, TypeToLoad>();

            // Ensure objects loaded in Unit of Work are not attempted to be loaded twice
            var currentUnitOfWork = ServiceProvider.Current.GetService<IUnitOfWorkProvider>().Current;
            if (currentUnitOfWork != null)
            {
                // Record current items in Unit of Work which we will not touch
                unitOfWorkObjects = currentUnitOfWork.ObjectStateEntries.ToDictionary(e => e.Object, e => e.State);

                lock (loadedObjects)
                {
                    currentUnitOfWork.ObjectStateEntries.Where(e => !e.IsRelationship).Select(s => new LoadedObject(s.Object, typesToLoad.GetValue(s.Object.GetType(), () => new TypeToLoad(s.Object.GetType())))).Distinct().
                        ForEach(lo => loadedObjects.Add(lo.Key, lo));
                }
            }

            // make sure entire graph is in loadedObjects so we don't repeat load - this may be the case is there is no ambient unit of work...or even if there is one, but there are unattached/undetected entities in the graph
            foreach (var o in new EntityGraphVisitor(target as IEnumerable<object> ?? new[] { target })
                .Select(e => new LoadedObject(e, typesToLoad.GetValue(e.GetType(), () => new TypeToLoad(e.GetType())))))
            {
                var loadedObject = o;
                loadedObjects.GetValue(o.Key, () => loadedObject);
            }

            Load(target, BuildProperties(paths));

            if (currentUnitOfWork != null)
            {
                // set all object state entries that were there before the load back to original state
                currentUnitOfWork.ObjectStateEntries.Where(p => unitOfWorkObjects.ContainsKey(p.Object))
                    .ForEach(p => p.State = unitOfWorkObjects[p.Object]);

                // If we are in Unit of Work -> mark all new entries as unchanged
                currentUnitOfWork.ObjectStateEntries.Where(p => !unitOfWorkObjects.ContainsKey(p.Object))
                    .OrderBy(p => p.IsRelationship ? 1 : 0)
                    .ForEach(p => p.State = ObjectState.Unchanged);
            }
        }

        /// <summary>
        /// Builds the properties from the expressions.
        /// </summary>
        /// <param name="paths">The paths.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        private static PropertyPath[] BuildProperties<T>(Expression<Func<T, object>>[] paths)
        {
            var result = new List<PropertyPath>();
            foreach (var path in paths)
            {
                var visitor = new PropertyPathVisitor();
                visitor.Visit(path);
                result.AddRange(visitor.PropertyPaths);
            }
            return result.ToArray();
        }

        private void Load(object target, PropertyPath[] propertyPaths)
        {
            if (target == null) return;

            if (!target.Is<IEnumerable>()) target = new[] { target };

            if (target.As<IEnumerable>().IsNullOrEmpty()) return;

            var groups = propertyPaths.GroupBy(p => p.First(), p => p.Skip(1).ToArray()).ToArray();

            Action<IGrouping<PropertyToLoad, PropertyToLoad[]>> load = g => ProcessPathsAndLoad(target, g.Key, g.Where(i => i.Any()).Select(p => new PropertyPath(p)).ToArray());

            if (groups.Length == 1) load(groups.Single());
            else if (degreeOfParallelism > 1)
            {
                groups.ForAllInParallel(load, degreeOfParallelism: degreeOfParallelism);
            }
            else
            {
                groups.ForEach(load);
            }
        }

        /// <summary>
        /// Loads a specific property then moves on to the next property
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="property">The property.</param>
        /// <param name="nextPropertyPaths">The next properties.</param>
        private void ProcessPathsAndLoad(object target, PropertyToLoad property, PropertyPath[] nextPropertyPaths)
        {
            GroupTargetByTypeAndLoad(target, property);

            if (target is IEnumerable)
            {

                target.CastTo<IEnumerable>()
                    .OfType<object>()
                    .GroupBy(i => i.GetType())
                    .ForEach(i =>
                    {
                        Array newTarget;
                        // need to lock while getting this property in case another thread is setting it at the same time from a different direction
                        var l = new TypePairLock(i.Key, property.SourceType.Type);
                        lock (locks.GetValue(l, () => l))
                        {
                            // select elements along path target.Select(i => i.Part).ToArray()
                            newTarget = i.OfType<object>().Select(property.GetTargetPropertyValue).WhereNotDefault().Distinct().ToArray(property.TargetProperty.PropertyType);

                            var propertyElementType = property.TargetProperty.PropertyType.FindElementType();
                            if (propertyElementType != null)
                            {
                                newTarget = newTarget.Cast<IEnumerable>().SelectMany(j => j.CastTo<IEnumerable>().OfType<object>()).Distinct().ToArray(propertyElementType);
                            }
                        }

                        if (nextPropertyPaths.Length > 0)
                        {
                            Load(newTarget, nextPropertyPaths);
                        }
                    });
            }
            else if (target != null)
            {
                // need to lock while getting this property in case another thread is setting it at the same time from a different direction
                var l = new TypePairLock(target.GetType(), property.SourceType.Type);
                lock (locks.GetValue(l, () => l))
                {
                    target = property.GetTargetPropertyValue(target);
                }

                if (nextPropertyPaths.Length > 0)
                {
                    Load(target, nextPropertyPaths);
                }
            }
        }

        /// <summary>
        /// Loads the property on the target using the queryable source.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="property">The property.</param>
        private void GroupTargetByTypeAndLoad(object target, PropertyToLoad property)
        {
            var enumerable = target as IEnumerable;
            if (enumerable != null)
            {
                enumerable.OfType<object>().GroupBy(i => i.GetType()).ForEach(i => LoadProperty(i.ToArray(), property));
            }
            else
            {
                LoadProperty(new[] { target }, property);
            }
        }

        private void LoadProperty(object[] targets, PropertyToLoad property)
        {
            if (targets.Length == 0) return;
            var type = targets[0].GetType();
            type = type.FindElementType() ?? (type.Assembly.IsDynamic && type.BaseType != null ? type.BaseType : type);

            try
            {
                LoadedObject[] objectsToLoad;
                lock (loadedObjects)
                {

                    // get objects from context so we don't dupe instances,
                    // and filter out targets that already have this property loaded.
                    objectsToLoad = targets.Select(o => new LoadedObject(o, property.TargetType)).Select(o => loadedObjects.GetValue(o.Key, () => o))
                        .Where(o => o.LoadedProperties.Add(property))
                        .ToArray();
                }

                if (objectsToLoad.Length == 0) return;

                // need to lock while getting this property in case another thread is setting it at the same time from a different direction
                var l = new TypePairLock(type, property.SourceType.Type);
                lock (locks.GetValue(l, () => l))
                {
                    // Ensure we are loading either outside unit of work or in a new unit of work
                    // This allows to avoid duplicate entities loaded issue if PLINQ ended running this operation on caller thread
                    var unitOfWorkProvider = ServiceProvider.Current.GetService<IUnitOfWorkProvider>();
                    using (unitOfWorkProvider.Current == null ? null : unitOfWorkProvider.Create())
                    {
                        if (property.RelationshipType == RelationshipType.ManyToMany)
                        {
                            LoadManyToMany(objectsToLoad, property);
                        }
                        else
                        {
                            // go get the data and set the results.
                            LoadProperty(objectsToLoad, property);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Could not load property {0} on {1}.".FormatWith(property.TargetProperty.Name, type.Name), ex);
            }
        }

        /// <summary>
        /// Loads a many to many relationship by refetching the collection of targets using an Include call, then swapping out the related entities.
        /// </summary>
        /// <param name="targets">The targets.</param>
        /// <param name="propertyToLoad">The property to load.</param>
        private void LoadManyToMany(LoadedObject[] targets, PropertyToLoad propertyToLoad)
        {
            var query = queryableFactory(propertyToLoad.TargetType.Type);
            query = query.Include(propertyToLoad.TargetProperty.Name);

            var keys = targets.Select(t => t.Instance).Select(propertyToLoad.GetKeysForQuery).ToArray();

            if (keys.Length == 0) return;

            query = query.Join(propertyToLoad.TargetType.KeyProperties.Select(p => p.Name).ToArray(), keys);

            var results = query.ToObjectArray();

            var targetsDictionary = targets.ToDictionary(t => t);

            foreach (var result in results.Select(r => new LoadedObject(r, propertyToLoad.TargetType)).ToArray())
            {
                // clear the collection of many to many entities on the fetched result and add them to the real target.

                LoadedObject target;
                if (!targetsDictionary.TryGetValue(result, out target)) continue;

                var collectionOnResult = (IList)propertyToLoad.GetTargetPropertyValue(result.Instance);

                if (collectionOnResult == null) continue;

                LoadedObject[] toAdd;
                lock (loadedObjects)
                {
                    // pick the objects to add from the results and from objects already loaded where applicable
                    toAdd = collectionOnResult.ToObjectArray().Select(o => new LoadedObject(o, propertyToLoad.SourceType)).Select(o => loadedObjects.GetValue(o.Key, () => o)).ToArray();
                }
                var collectionOnTarget = (IList)propertyToLoad.GetTargetPropertyValue(target.Instance);

                if (collectionOnTarget == null) continue;

                collectionOnResult.Clear();

                collectionOnTarget.AddRangeWithSinglePropertyChangedNotification(
                    ApplyPredicate(toAdd.Select(o => o.Instance).OfType<object>().AsQueryable(), propertyToLoad.Predicate));
            }
        }

        /// <summary>
        /// Loads the property on enumerable.
        /// </summary>
        /// <param name="targets">The targets.</param>
        /// <param name="propertyToLoad">The property to load.</param>
        private void LoadProperty(LoadedObject[] targets, PropertyToLoad propertyToLoad)
        {
            var sourcePropertyToQueryOnNames = propertyToLoad.SourcePropertiesToQueryOn.Select(p => p.Name).ToArray();
            var sourcePropertiesToQueryOnGetters = propertyToLoad.SourcePropertiesToQueryOn.Select(p => p.GetGetMethod(true).GetInvoker()).ToArray();

            var sequenceEqualityComparer = EqualityComparer.For<object[]>((x, y) => x.SequenceEqual(y), x => x.GetSequenceHashCode());

            var targetsWithKeys = targets.Select(t => new { Target = t, Keys = propertyToLoad.GetKeysForQuery(t.Instance) }).Where(o => !o.Keys.All(k => k == null)).ToArray();

            var keys = new HashSet<object[]>(targetsWithKeys.Select(t => t.Keys), sequenceEqualityComparer);

            targets = targetsWithKeys.Select(t => t.Target).ToArray();

            object[] loadedInstances;
            lock (loadedObjects)
            {
                loadedInstances = loadedObjects.Select(o => o.Value.Instance).ToArray();
            }

            // these are objects that are already in memory, and would be returned by the query...so we want to find them, then exclude them.
            object[] objectsAlreadyLoaded = ApplyPredicate(loadedInstances
                .Where(o => propertyToLoad.SourceType.Type.IsInstanceOfType(o) && sourcePropertiesToQueryOnGetters.All(g => g(o) != null))
                .ToArray(propertyToLoad.SourceType.Type)
                .AsQueryable()
                .Join(sourcePropertyToQueryOnNames, keys),
                propertyToLoad.Predicate)
                .ToObjectArray();

            object[][] objectsAlreadyLoadedKeys = objectsAlreadyLoaded.Select(propertyToLoad.SourceType.GetKeyValues).ToArray();

            // filter out keys for objects we've already loaded (THIS IS ONLY SAFE FOR MANY -> ONE/ ONE -> ONE RELATIONSHIPS, SINCE THERE MAY BE OTHER INSTANCES IN ONE -> MANY RELATIONSHIPS WE HAVEN'T YET LOADED)
            if (propertyToLoad.RelationshipType == RelationshipType.ManyToOne || propertyToLoad.RelationshipType == RelationshipType.OneToOne)
            {
                keys = new HashSet<object[]>(keys.Except(objectsAlreadyLoadedKeys, sequenceEqualityComparer));
            }

            if (keys.Count == 0 && objectsAlreadyLoadedKeys.Length == 0) return;

            var results = new List<object>();

            Action<IEnumerable<object[]>> getResults = o =>
                                              {
                                                  var query = queryableFactory(propertyToLoad.SourceType.Type);
                                                  query = ApplyPredicate(query.Join(sourcePropertyToQueryOnNames, o), propertyToLoad.Predicate);

                                                  var currentBatchResults = query.ToObjectArray();

                                                  lock (results)
                                                  {
                                                      results.AddRange(currentBatchResults);
                                                  }
                                              };

            if (keys.Count > 1500 && degreeOfParallelism > 1)
            {
                keys
                    .Batch(1500).Select(o => o.ToArray())
                    .ForAllInParallel(b => getResults(b), degreeOfParallelism: degreeOfParallelism);
            }
            else if (keys.Count > 0)
            {
                getResults(keys);
            }

            IEnumerable resultsToSet;
            lock (loadedObjects)
            {
                // pick the objects to add from the results and from objects already loaded where applicable
                resultsToSet = results.Select(o => new LoadedObject(o, propertyToLoad.SourceType)).Select(o => loadedObjects.GetValue(o.Key, () => o)).Select(i => i.Instance).ToArray();
            }

            // add back objects that are already loaded that we filtered out
            if (objectsAlreadyLoaded.Length > 0) resultsToSet = resultsToSet.OfType<object>().Concat(objectsAlreadyLoaded).ToArray();

            var resultsLookup = resultsToSet.OfType<object>().GroupBy(x => sourcePropertiesToQueryOnGetters.Select(g => g(x)).ToArray(), sequenceEqualityComparer).ToDictionary(i => i.Key, i => i.ToArray(), sequenceEqualityComparer);

            if (resultsLookup.Count == 0) return;

            foreach (var target in targets)
            {
                object[] relevantResults;
                if (resultsLookup.TryGetValue(propertyToLoad.GetKeysForQuery(target.Instance), out relevantResults))
                {
                    if (relevantResults.Length == 0) continue;

                    if (propertyToLoad.RelationshipType == RelationshipType.OneToMany)
                    {
                        SetResultsForOneToMany(target.Instance, relevantResults, propertyToLoad);
                    }
                    else
                    {
                        propertyToLoad.SetTargetPropertyValue(target.Instance, relevantResults.FirstOrDefault());
                    }
                }
                else if (propertyToLoad.RelationshipType == RelationshipType.ManyToOne || propertyToLoad.RelationshipType == RelationshipType.OneToOne)
                {
                    loadFailures.Add(Tuple.Create(target.Instance, propertyToLoad.TargetProperty));
                }
            }
        }

        private void SetResultsForOneToMany(object target, object[] results, PropertyToLoad propertyToLoad)
        {
            var list = (IList)propertyToLoad.GetTargetPropertyValue(target);

            if (list == null)
            {
                list = (IList)typeof(List<>).MakeGenericType(propertyToLoad.SourceType.Type).CreateInstance();
                propertyToLoad.SetTargetPropertyValue(target, list);
            }

            foreach (var result in results) list.Add(result);
        }

        /// <summary>
        /// Applies the filter to the where, if present.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        private IQueryable ApplyPredicate(IQueryable query, LambdaExpression predicate)
        {
            if (predicate == null) return query;

            return (IQueryable)Queryables.WhereMethod.MakeGenericMethod(query.ElementType).Invoke(null, new object[] { query, predicate });
        }


        /// <summary>
        /// A lock for a set of two types.
        /// </summary>
        private class TypePairLock
        {
            public TypePairLock(Type type1, Type type2)
            {
                Type1 = type1;
                Type2 = type2;
            }

            public Type Type1 { get; private set; }
            public Type Type2 { get; private set; }

            public override int GetHashCode()
            {
                return Type1.GetHashCode() * Type2.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                return obj.As<TypePairLock>().IfNotNull(o => (o.Type1 == Type1 && o.Type2 == Type2) || (o.Type1 == Type2 && o.Type2 == Type1));
            }
        }

        /// <summary>
        /// Metadata about a loaded type.
        /// </summary>
        private class TypeToLoad
        {
            private Func<object, object>[] keyValueGetters;

            /// <summary>
            /// Gets or sets the type.
            /// </summary>
            /// <value>
            /// The type.
            /// </value>
            public Type Type { get; private set; }

            /// <summary>
            /// Gets or sets the key property on the target: User.Id.
            /// </summary>
            /// <value>
            /// The target id property.
            /// </value>
            public PropertyInfo[] KeyProperties { get; private set; }

            public TypeToLoad(Type type)
            {
                Type = type;

                KeyProperties = type.GetKeyProperties().ToArray();
                if (KeyProperties.IsNullOrEmpty()) throw new InvalidOperationException("Could not find any key properties on {0}.".FormatWith(Type));

                keyValueGetters = KeyProperties.Select(p => p.GetGetMethod(true).GetInvoker()).Select(i => new Func<object, object>(o => i(o))).ToArray();
            }

            /// <summary>
            /// Gets the key values on the target.
            /// </summary>
            /// <param name="target">The target.</param>
            /// <returns></returns>
            public object[] GetKeyValues(object target)
            {
                return keyValueGetters.Select(g => g(target)).ToArray();
            }

            public override int GetHashCode()
            {
                return Type.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                return obj.As<TypeToLoad>().IfNotNull(o => o.Type == Type);
            }

            public override string ToString()
            {
                return Type.Name;
            }
        }

        /// <summary>
        /// Information about a property to load.
        /// </summary>
        private class PropertyToLoad
        {
            private const BindingFlags PropertyBindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

            private readonly Invoker[] getForeignKeys;
            private readonly Invoker getValueOnTarget;
            private readonly Invoker setValueOnTarget;

            /* Use sample case for loading User.Addresses
             * class User 
             * {
             *  int Id { get; set; }
             *  ICollection<Address> Addresses { get; set; }
             * }
             * 
             * class Address
             * {
             *  int Id {get; set; }
             *  int UserId {get; set; }
             *  User User {get; set; }
             * }
             * 
             */

            /// <summary>
            /// Gets or sets the metadata about the type begin loaded: User.
            /// </summary>
            /// <value>
            /// The type to load.
            /// </value>
            public TypeToLoad TargetType { get; private set; }


            /// <summary>
            /// Gets or sets the property to be loaded on the target: User.Addresses.
            /// </summary>
            /// <value>
            /// The target property.
            /// </value>
            public PropertyInfo TargetProperty { get; private set; }

            /// <summary>
            /// Gets or sets the type of the source: Address.
            /// </summary>
            /// <value>
            /// The type of the source.
            /// </value>
            public TypeToLoad SourceType { get; private set; }

            /// <summary>
            /// Gets or sets foreign properties between the source or target. Can be on either source or target, depending on the relationship type: Address.UserId. In the case of Many to One, would be User.AddressId. Will be empty for One to One or Many to Many relationships.
            /// </summary>
            /// <value>
            /// The source property.
            /// </value>
            public PropertyInfo[] ForeignKeyProperties { get; private set; }

            /// <summary>
            /// Gets or sets the key property on the source type: Address.Id.
            /// </summary>
            /// <value>
            /// The source id property.
            /// </value>
            public PropertyInfo[] SourceKeyProperties { get; private set; }

            /// <summary>
            /// Gets or sets the type of the relationship represented by the property. User.Addresses is a One to Many relationship. Address.User is a Many to One relationship.
            /// </summary>
            /// <value>
            /// The type of the relationship.
            /// </value>
            public RelationshipType RelationshipType { get; private set; }

            /// <summary>
            /// Gets or sets the source properties to query on: repository.Address.Where(a => userIds.Contains(a.UserId))
            /// </summary>
            /// <value>
            /// The source properties to query on.
            /// </value>
            public PropertyInfo[] SourcePropertiesToQueryOn { get; private set; }

            /// <summary>
            /// Gets or sets the predicate to apply when loading.
            /// </summary>
            /// <value>
            /// The predicate.
            /// </value>
            public LambdaExpression Predicate { get; private set; }

            public PropertyToLoad(Type targetType, PropertyInfo targetProperty, LambdaExpression predicate)
            {
                TargetType = new TypeToLoad(targetType);

                TargetProperty = targetProperty;
                Predicate = predicate;
                EnsureValidProperties(new[] { TargetProperty });

                SourceType = new TypeToLoad(TargetProperty.PropertyType.FindElementType() ?? TargetProperty.PropertyType);

                SourceKeyProperties = SourceType.Type.GetKeyProperties().ToArray();
                EnsureValidProperties(SourceKeyProperties);

                SetRelationshipProperties();

                getForeignKeys = ForeignKeyProperties.Select(p => p.GetGetMethod(true).GetInvoker()).ToArray();

                SourcePropertiesToQueryOn = GetSourcePropertiesToQueryOn();

                getValueOnTarget = TargetProperty.GetGetMethod(true).GetInvoker();

                setValueOnTarget = TargetProperty.GetSetMethod(true).GetInvoker();
            }

            private PropertyInfo[] GetSourcePropertiesToQueryOn()
            {
                if (RelationshipType == RelationshipType.ManyToMany)
                {
                    return new PropertyInfo[0];
                }
                if (RelationshipType == RelationshipType.OneToMany)
                {
                    return ForeignKeyProperties;
                }
                return SourceKeyProperties;
            }

            private void SetRelationshipProperties()
            {
                var associationAttribute = GetAssociationAttribute(TargetProperty);

                if (associationAttribute != null)
                {
                    SetRelationshipPropertiesFromAssociationAttribute(associationAttribute);
                }
                else
                {
                    InferRelationshipProperties();
                }

                if (RelationshipType == RelationshipType.OneToMany || RelationshipType == RelationshipType.ManyToOne)
                {
                    EnsureValidProperties(ForeignKeyProperties);
                }
                else
                {
                    ForeignKeyProperties = new PropertyInfo[0];
                }
            }

            private void InferRelationshipProperties()
            {
                if (TargetType.Is<IEnumerable>())
                {
                    // try to get property TargetTypeId on source
                    var foreignKeyProperty = SourceType.Type.GetProperty(TargetType.Type.Name + "Id", PropertyBindingFlags) ?? SourceType.Type.GetProperty(TargetType.Type.Name + "ID", PropertyBindingFlags);

                    if (foreignKeyProperty == null) RelationshipType = RelationshipType.ManyToMany;
                    else
                    {
                        RelationshipType = RelationshipType.OneToMany;
                        ForeignKeyProperties = new[] { foreignKeyProperty };
                    }
                }
                else
                {
                    // try to get property SourceTypeId on target
                    var foreignKeyProperty =
                        TargetType.Type.GetProperty(TargetProperty.Name + "Id", PropertyBindingFlags) ?? TargetType.Type.GetProperty(TargetProperty.Name + "ID", PropertyBindingFlags) ??
                        TargetType.Type.GetProperty(SourceType.Type.Name + "Id", PropertyBindingFlags) ?? TargetType.Type.GetProperty(SourceType.Type.Name + "ID", PropertyBindingFlags);

                    if (foreignKeyProperty == null) RelationshipType = RelationshipType.OneToOne;
                    else
                    {
                        RelationshipType = RelationshipType.ManyToOne;
                        ForeignKeyProperties = new[] { foreignKeyProperty };
                    }
                }
            }

            private void SetRelationshipPropertiesFromAssociationAttribute(dynamic associationAttribute)
            {
                if (associationAttribute.ThisKey == string.Empty && associationAttribute.OtherKey == string.Empty)
                {
                    RelationshipType = RelationshipType.ManyToMany;
                }
                else
                {
                    if (((string[])associationAttribute.ThisKeyMembers).SequenceEqual(TargetType.KeyProperties.Select(p => p.Name)))
                    {
                        if (((string[])associationAttribute.OtherKeyMembers).SequenceEqual(SourceType.Type.GetKeyProperties().Select(p => p.Name)))
                        {
                            // keys on both sides of the association are the key properties for the respective types
                            RelationshipType = RelationshipType.OneToOne;
                        }
                        else
                        {
                            RelationshipType = RelationshipType.OneToMany;
                            ForeignKeyProperties = ((string[])associationAttribute.OtherKeyMembers).Select(m => SourceType.Type.GetProperty(m, PropertyBindingFlags)).WhereNotDefault().ToArray();

                        }
                    }
                    else
                    {
                        RelationshipType = RelationshipType.ManyToOne;
                        ForeignKeyProperties = ((string[])associationAttribute.ThisKeyMembers).Select(m => TargetType.Type.GetProperty(m, PropertyBindingFlags)).WhereNotDefault().ToArray();
                    }
                }
            }

            public object[] GetKeysForQuery(object target)
            {
                if (RelationshipType == RelationshipType.ManyToOne)
                {
                    return getForeignKeys.Where((g, i) => ForeignKeyProperties[i].DeclaringType != null && ForeignKeyProperties[i].DeclaringType.IsInstanceOfType(target)).Select(i => i(target)).ToArray();
                }
                return TargetType.GetKeyValues(target);
            }

            public object GetTargetPropertyValue(object target)
            {
                if (TargetProperty.DeclaringType != null && !TargetProperty.DeclaringType.IsInstanceOfType(target)) return null;

                return getValueOnTarget(target);
            }

            public void SetTargetPropertyValue(object target, object value)
            {
                if (TargetProperty.DeclaringType != null && !TargetProperty.DeclaringType.IsInstanceOfType(target)) return;

                setValueOnTarget(target, value);
            }

            private void EnsureValidProperties(PropertyInfo[] properties)
            {
                if (properties == null || properties.IsNullOrEmpty()) throw new InvalidOperationException("Could not find a required property to load {0} on {1}.".FormatWith(TargetProperty.Name, TargetType.Type));

                foreach (var property in properties)
                {
                    if (property.GetIndexParameters().Length != 0) throw new InvalidOperationException("Property {0} on type {1} must not have an indexer.".FormatWith(TargetProperty, TargetType.Type));
                    if (property.GetGetMethod(true) == null || property.GetSetMethod(true) == null) throw new InvalidOperationException("Property {0} on type {1} must have a getter and setter.".FormatWith(TargetProperty, TargetType.Type));
                }
            }

            private static dynamic GetAssociationAttribute(PropertyInfo property)
            {
                return property.GetAttributes().FirstOrDefault(a => a.GetType().FullName == "System.ComponentModel.DataAnnotations.AssociationAttribute");
            }

            public override int GetHashCode()
            {
                return TargetProperty.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                return obj.As<PropertyToLoad>().IfNotNull(o => o.TargetProperty.Equals(TargetProperty) && Predicate.IfNotNull(f => f.ToString()) == o.Predicate.IfNotNull(f => f.ToString()));
            }

            public override string ToString()
            {
                return TargetProperty.Name;
            }
        }

        /// <summary>
        /// Represents a path of a properties to load.
        /// </summary>
        private class PropertyPath : List<PropertyToLoad>
        {
            public PropertyPath() { }

            public PropertyPath(IEnumerable<PropertyToLoad> properties) : base(properties) { }

            public override string ToString()
            {
                return this.Select(i => i.ToString()).Join(".");
            }
        }

        /// <summary>
        /// The type of relationship represented by a navigation property: User.Addresses is One to Many.
        /// </summary>
        private enum RelationshipType
        {
            OneToOne,
            OneToMany,
            ManyToOne,
            ManyToMany
        }

        /// <summary>
        /// A key for a loaded object to uniquely identify it.
        /// </summary>
        private class LoadedObjectKey
        {
            public LoadedObjectKey(TypeToLoad typeToLoad, object[] keyValues)
            {
                TypeToLoad = typeToLoad;
                KeyValues = keyValues;
            }

            /// <summary>
            /// Gets or sets the metadata about the type to load.
            /// </summary>
            /// <value>
            /// The type to load.
            /// </value>
            public TypeToLoad TypeToLoad { get; private set; }

            /// <summary>
            /// Gets or sets the key values on the target instance.
            /// </summary>
            /// <value>
            /// The key values.
            /// </value>
            public object[] KeyValues { get; private set; }

            public override int GetHashCode()
            {
                return TypeToLoad.GetHashCode() ^ KeyValues.GetSequenceHashCode();
            }

            public override bool Equals(object obj)
            {
                return obj.As<LoadedObjectKey>().IfNotNull(o => o.TypeToLoad.Equals(TypeToLoad) && o.KeyValues.SequenceEqual(KeyValues));
            }

            public override string ToString()
            {
                return "{0}: {1}".FormatWith(TypeToLoad, KeyValues.Join());
            }
        }

        /// <summary>
        /// Describes a live target instance to load a property onto.
        /// </summary>
        private class LoadedObject
        {
            public LoadedObject(object instance, TypeToLoad typeToLoad)
            {
                Instance = instance;
                LoadedProperties = new HashSet<PropertyToLoad>();
                Key = new LoadedObjectKey(typeToLoad, typeToLoad.GetKeyValues(instance));
            }

            /// <summary>
            /// Gets or sets the key.
            /// </summary>
            /// <value>
            /// The key.
            /// </value>
            public LoadedObjectKey Key { get; private set; }

            /// <summary>
            /// Gets or sets the instance to load.
            /// </summary>
            /// <value>
            /// The instance.
            /// </value>
            public object Instance { get; private set; }

            /// <summary>
            /// Gets or sets the loaded properties.
            /// </summary>
            /// <value>
            /// The loaded properties.
            /// </value>
            public HashSet<PropertyToLoad> LoadedProperties { get; private set; }

            public override int GetHashCode()
            {
                return Key.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                return obj.As<LoadedObject>().IfNotNull(o => o.Key.Equals(Key));
            }

            public override string ToString()
            {
                return "{0}. Loaded: {1}".FormatWith(Key, LoadedProperties.Join());
            }
        }

        /// <summary>
        /// Builds a list of properties using the provided expression tree path.
        /// </summary>
        private class PropertyPathVisitor : ExpressionVisitor
        {
            private readonly List<PropertyPath> propertyPaths = new List<PropertyPath>();

            private readonly LambdaExpression predicate;

            public PropertyPathVisitor(LambdaExpression predicate = null)
            {
                this.predicate = predicate;
            }

            public PropertyPath[] PropertyPaths
            {
                get { return propertyPaths.ToArray(); }
            }

            protected override Expression VisitMethodCall(MethodCallExpression node)
            {
                LambdaExpression predicate = null;

                if (node.Method.Name == "Where" && new[] { typeof(Enumerable), typeof(Queryable) }.Contains(node.Method.DeclaringType))
                {
                    predicate = node.Arguments[1].StripQuotes() as LambdaExpression;
                }

                if (node.Method.GetAttribute<ExtensionAttribute>() != null)
                {
                    var visitor = new PropertyPathVisitor(predicate);
                    visitor.Visit(node.Arguments[0]);

                    AppendSubPaths(visitor.PropertyPaths);

                    if (new[] { "Select", "SelectMany" }.Contains(node.Method.Name) && new[] { typeof(Enumerable), typeof(Queryable) }.Contains(node.Method.DeclaringType))
                    {
                        visitor = new PropertyPathVisitor();
                        visitor.Visit(node.Arguments[1]);

                        AppendSubPaths(visitor.PropertyPaths);
                    }
                }

                return node;
            }

            protected override Expression VisitNew(NewExpression node)
            {
                var paths = new List<PropertyPath>();
                foreach (var argument in node.Arguments)
                {
                    var visitor = new PropertyPathVisitor();
                    visitor.Visit(argument);

                    paths.AddRange(visitor.PropertyPaths);
                }

                AppendSubPaths(paths);

                return node;
            }

            protected override Expression VisitNewArray(NewArrayExpression node)
            {
                var paths = new List<PropertyPath>();
                foreach (var e in node.Expressions)
                {
                    var visitor = new PropertyPathVisitor();
                    visitor.Visit(e);

                    paths.AddRange(visitor.PropertyPaths);
                }

                AppendSubPaths(paths);

                return node;
            }

            protected override Expression VisitMember(MemberExpression node)
            {
                base.VisitMember(node);

                if (node.Member.MemberType == MemberTypes.Property)
                {
                    var property = new PropertyToLoad(node.Member.DeclaringType, (PropertyInfo)node.Member, predicate);

                    AppendProperty(property);
                }

                return node;
            }

            private void AppendSubPaths(IEnumerable<PropertyPath> paths)
            {
                if (paths.Count() == 0) return;

                if (propertyPaths.Count > 0)
                {
                    foreach (var propertyPath in propertyPaths.ToArray())
                    {
                        propertyPaths.Remove(propertyPath);

                        foreach (var pathToAdd in paths)
                        {
                            propertyPaths.Add(new PropertyPath(propertyPath.Concat(pathToAdd)));
                        }
                    }
                }
                else
                {
                    foreach (var pathToAdd in paths)
                    {
                        propertyPaths.Add(pathToAdd);
                    }
                }
            }

            private void AppendProperty(PropertyToLoad property)
            {
                if (propertyPaths.Count > 0)
                {
                    foreach (var propertyPath in propertyPaths)
                    {
                        // append to all existing paths in this branch
                        propertyPath.Add(property);
                    }
                }
                else
                {
                    // brand new path
                    propertyPaths.Add(new PropertyPath { property });
                }
            }
        }

    }

    /// <summary>
    ///   IQueryable Helper/Extension methods.
    /// </summary>
    public static class Queryables
    {
        internal static readonly MethodInfo CastMethod = Reflector.GetMember(() => new int[0].Cast<object>()).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo ToListMethod = Reflector.GetMember(() => new object[0].ToList()).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo WhereMethod = Reflector.GetMember(() => new object[0].AsQueryable().Where(i => true)).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo SelectMethod = Reflector.GetMember(() => new object[0].AsQueryable().Select(o => o)).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo SelectManyMethod = Reflector.GetMember(() => new object[0].AsQueryable().SelectMany(o => new object[0], (o1, o2) => new { A = 1 })).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo JoinMethod = Reflector.GetMember(() => new object[0].AsQueryable().Join(new object[0], o1 => 1, o2 => 1, (o1, o2) => new { A = 1 })).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo FirstMethod = Reflector.GetMember(() => new object[0].AsQueryable().First(o => true)).CastTo<MethodInfo>().GetGenericMethodDefinition();

        internal static readonly MethodInfo FirstOrDefaultMethod = Reflector.GetMember(() => new object[0].AsQueryable().FirstOrDefault(o => true)).CastTo<MethodInfo>().GetGenericMethodDefinition();


        /// <summary>
        /// Caches query execution results by their expressions using the specified dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="cache">The cache.</param>
        /// <returns></returns>
        public static IQueryable<T> Cached<T>(this IQueryable<T> query, IDictionary<Expression, object> cache = null)
        {
            query.Provider.EnsureType<INotifyingQueryProvider>().Cache(cache);
            return query;
        }

        /// <summary>
        /// Caches query execution results by their expressions using the specified dictionary.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="cache">The cache.</param>
        public static INotifyingQueryProvider Cache(this INotifyingQueryProvider provider, IDictionary<Expression, object> cache = null)
        {
            if (cache == null)
            {
                cache = new ConcurrentDictionary<Expression, object>(new ExpressionEqualityComparer());
            }

            provider.Executing += (sender, e) =>
            {
                lock (cache)
                {
                    object result;
                    if (cache.TryGetValue(e.Expression, out result))
                    {
                        e.Handled = true;
                        e.Result = result;
                    }
                }
            };

            provider.Executed += (sender, e) =>
            {
                // Result can be null in case of errors. Don't attempt caching it
                if (e.Exception == null)
                {
                    cache[e.Expression] = e.Result;
                }
            };

            return provider;
        }

        /// <summary>
        /// Attaches an Executing handler to the source queryable's provider and returns the original queryable. Throws an exception if the queryable's provider doesn't implement INotifyingQueryProvider.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="handler">The handler.</param>
        /// <returns></returns>
        public static IQueryable<T> Executing<T>(IQueryable<T> source, EventHandler<QueryExecutingEventArgs> handler)
        {
            source.Provider.EnsureType<INotifyingQueryProvider>().Executing += handler;
            return source;
        }


        /// <summary>
        /// Attaches an Executed handler to the source queryable's provider and returns the original queryable. Throws an exception if the queryable's provider doesn't implement INotifyingQueryProvider.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="handler">The handler.</param>
        /// <returns></returns>
        public static IQueryable<T> Executed<T>(IQueryable<T> source, EventHandler<QueryExecutedEventArgs> handler)
        {
            source.Provider.EnsureType<INotifyingQueryProvider>().Executed += handler;
            return source;
        }

        /// <summary>
        /// Determines if a date is an exact match for another date.
        /// </summary>
        public static Expression<Func<DateTime, DateTime, bool>> IsOnDate = (dateTime, onDate) => dateTime >= onDate.Date && dateTime < onDate.AddDays(1).Date;

        /// <summary>
        /// Applies a join to the queryable against the collection provided, using the properties specified.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="properties">The properties.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static IQueryable Join(this IQueryable source, string[] properties, IEnumerable<object[]> collection)
        {
            return source.Join(properties, collection, false);
        }

        /// <summary>
        /// Applies a join to the queryable against the collection provided, using the properties specified.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="properties">The properties.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static IQueryable Except(this IQueryable source, string[] properties, IEnumerable<object[]> collection)
        {
            return source.Join(properties, collection, true);
        }

        /// <summary>
        /// Applies a join to the queryable against the collection provided, using the properties specified.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="properties">The properties.</param>
        /// <param name="collection">The collection.</param>
        /// <param name="negate">if set to <c>true</c> [negate].</param>
        /// <returns></returns>
        private static IQueryable Join(this IQueryable source, string[] properties, IEnumerable<object[]> collection, bool negate)
        {
            if (properties.Length == 1) return PropertyIn(source, properties.FirstOrDefault(), new HashSet<object>(collection.Select(i => i[0])), negate);

            var predicate = new List<string>();
            var parameters = new List<object>();

            Func<object, int> addAndGetIndex = o =>
            {
                parameters.Add(o);
                return parameters.Count - 1;
            };

            foreach (var item in collection)
            {
                if (item.Length != properties.Length) throw new InvalidOperationException("Properties length should be the same as each item in the collection.");
                var itemPredicate = item.Select((o, index) => "{0} = @{1}".FormatWith(properties[index], "@" + addAndGetIndex(o))).Join(" and ");
                predicate.Add("({0})".FormatWith(itemPredicate));
            }

            var finalPredicate = predicate.Join(" or ");
            if (negate) finalPredicate = "not ({0})".FormatWith(finalPredicate);

            return source.Where(finalPredicate, parameters.ToArray());
        }

        /// <summary>
        /// Applies a contains filter to the queryable. query.Where(i =&gt; collection.Contains(i))
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="property">The property.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static IQueryable PropertyIn(this IQueryable source, string property, IEnumerable collection)
        {
            return source.PropertyIn(property, collection, false);
        }

        /// <summary>
        /// Applies a not contains filter to the queryable. query.Where(i =&gt; !collection.Contains(i))
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="property">The property.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static IQueryable PropertyNotIn(this IQueryable source, string property, IEnumerable collection)
        {
            return source.PropertyIn(property, collection, true);
        }

        /// <summary>
        /// Applies a contains (or not contains) filter to the queryable. query.Where(i => collection.Contains(i))
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="property">The property.</param>
        /// <param name="collection">The collection.</param>
        /// <param name="negate">if set to <c>true</c> [negate].</param>
        /// <returns></returns>
        private static IQueryable PropertyIn(this IQueryable source, string property, IEnumerable collection, bool negate)
        {
            property.EnsureNotDefault("Property name is required.");

            var enumerator = collection.GetEnumerator();

            object current = null;
            bool hasItems = false;
            while (enumerator.MoveNext() && current == null)
            {
                hasItems = true;
                current = enumerator.Current;
            }

            if (!hasItems) return source;

            var collectionElementType = current == null ? typeof(object) : current.GetType();

            var set = (IEnumerable)Activator.CreateInstance(typeof(HashSet<>).MakeGenericType(collectionElementType), collection.OfType(collectionElementType));

            var parameterExpression = Expression.Parameter(source.ElementType);

            var collectionExpression = Expression.Constant(set, typeof(IEnumerable<>).MakeGenericType(collectionElementType));

            MemberExpression propertyExpression = Expression.Property(parameterExpression, property);

            if (Nullable.GetUnderlyingType(((PropertyInfo)propertyExpression.Member).PropertyType) != null)
            {
                propertyExpression = Expression.Property(propertyExpression, "Value");
            }

            Expression body = Expression.Call(Enumerables.ContainsMethod.MakeGenericMethod(collectionElementType), collectionExpression, propertyExpression);

            if (negate) body = Expression.Not(body);

            var lambda = Expression.Lambda(body, parameterExpression);

            source = (IQueryable)WhereMethod.MakeGenericMethod(source.ElementType).Invoke(null, new object[] { source, lambda });

            return source;
        }

        /// <summary>
        /// Filters the queryable to match the specified member values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="memberValues">The member values.</param>
        /// <returns></returns>
        public static IQueryable<T> WithMemberValues<T>(this IQueryable<T> source, IDictionary memberValues)
        {
            foreach (var key in memberValues.Keys)
            {
                source = source.Where("{0} = @0".FormatWith(key), memberValues[key]);
            }
            return source;
        }

        /// <summary>
        /// Returns a queryable with items that match all non id properties.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="match">The match.</param>
        /// <returns></returns>
        public static IQueryable<T> MatchingNonKeyProperties<T>(this IQueryable<T> source, T match)
        {
            var memberValues = typeof(T).GetProperties().Where(p => !p.IsKey() && (p.CanRead && p.GetIndexParameters().Length == 0))
                .ToDictionary(i => i.Name, i => i.GetValue(match, null));

            return source.WithMemberValues(memberValues);
        }

        /// <summary>
        /// Gets a queryable factory that returns a queryable for a specific entity type.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> GetQueryableFactory(object source)
        {
            return QueryableFactoryLoader.GetQueryableFactory(source);
        }

        /// <summary>
        /// Loads the target along the specified path, using the provided queryable factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryableFactory">The queryable factory.</param>
        /// <param name="target">The target.</param>
        /// <param name="paths">The paths.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> Load<T>(this Func<Type, IQueryable> queryableFactory, T target, params Expression<Func<T, object>>[] paths)
        {
            IEnumerable<Tuple<object, PropertyInfo>> loadFailures;
            return Load(queryableFactory, target, out loadFailures, paths);
        }

        /// <summary>
        /// Loads the target along the specified path, using the provided queryable factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryableFactory">The queryable factory.</param>
        /// <param name="target">The target.</param>
        /// <param name="paths">The paths.</param>
        /// <param name="loadFailures">The load failures.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> Load<T>(this Func<Type, IQueryable> queryableFactory, T target, out IEnumerable<Tuple<object, PropertyInfo>> loadFailures, params Expression<Func<T, object>>[] paths)
        {
            var loader = new QueryableFactoryLoader(queryableFactory, target);
            loader.Load(paths);
            loadFailures = loader.LoadFailures;
            return queryableFactory;
        }

        /// <summary>
        /// Loads the target along the specified path, using the provided queryable factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryableFactory">The queryable factory.</param>
        /// <param name="target">The target.</param>
        /// <param name="degreeOfParallelism">The degree of parallelism.</param>
        /// <param name="paths">The paths.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> Load<T>(this Func<Type, IQueryable> queryableFactory, T target, int degreeOfParallelism = 5, params Expression<Func<T, object>>[] paths)
        {
            IEnumerable<Tuple<object, PropertyInfo>> loadFailures;
            return Load(queryableFactory, target, out loadFailures, degreeOfParallelism, paths);
        }

        /// <summary>
        /// Loads the target along the specified path, using the provided queryable factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryableFactory">The queryable factory.</param>
        /// <param name="target">The target.</param>
        /// <param name="loadFailures">The load failures.</param>
        /// <param name="degreeOfParallelism">The degree of parallelism.</param>
        /// <param name="paths">The paths.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> Load<T>(this Func<Type, IQueryable> queryableFactory, T target, out IEnumerable<Tuple<object, PropertyInfo>> loadFailures, int degreeOfParallelism = 5, params Expression<Func<T, object>>[] paths)
        {
            var loader = new QueryableFactoryLoader(queryableFactory, target, degreeOfParallelism);
            loader.Load(paths);
            loadFailures = loader.LoadFailures;
            return queryableFactory;
        }

        /// <summary>
        /// Loads the target along the specified path, using the provided queryable factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryableFactory">The queryable factory.</param>
        /// <param name="target">The target.</param>
        /// <param name="paths">The paths.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> Load<T>(this Func<Type, IQueryable> queryableFactory, IEnumerable<T> target, params Expression<Func<T, object>>[] paths)
        {
            IEnumerable<Tuple<object, PropertyInfo>> loadFailures;

            return Load(queryableFactory, target, out loadFailures, paths);
        }

        /// <summary>
        /// Loads the target along the specified path, using the provided queryable factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryableFactory">The queryable factory.</param>
        /// <param name="target">The target.</param>
        /// <param name="loadFailures">The load failures.</param>
        /// <param name="paths">The paths.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> Load<T>(this Func<Type, IQueryable> queryableFactory, IEnumerable<T> target, out IEnumerable<Tuple<object, PropertyInfo>> loadFailures, params Expression<Func<T, object>>[] paths)
        {
            var loader = new QueryableFactoryLoader(queryableFactory, target);
            loader.Load(paths);
            loadFailures = loader.LoadFailures;
            return queryableFactory;

        }

        /// <summary>
        /// Loads the target along the specified path, using the provided queryable factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryableFactory">The queryable factory.</param>
        /// <param name="target">The target.</param>
        /// <param name="degreeOfParallelism">The degree of parallelism.</param>
        /// <param name="paths">The paths.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> Load<T>(this Func<Type, IQueryable> queryableFactory, IEnumerable<T> target, int degreeOfParallelism = 5, params Expression<Func<T, object>>[] paths)
        {
            IEnumerable<Tuple<object, PropertyInfo>> loadFailures;
            return Load(queryableFactory, target, out loadFailures, degreeOfParallelism, paths);
        }

        /// <summary>
        /// Loads the target along the specified path, using the provided queryable factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryableFactory">The queryable factory.</param>
        /// <param name="target">The target.</param>
        /// <param name="loadFailures">The load failures.</param>
        /// <param name="degreeOfParallelism">The degree of parallelism.</param>
        /// <param name="paths">The paths.</param>
        /// <returns></returns>
        public static Func<Type, IQueryable> Load<T>(this Func<Type, IQueryable> queryableFactory, IEnumerable<T> target, out    IEnumerable<Tuple<object, PropertyInfo>> loadFailures, int degreeOfParallelism = 5, params Expression<Func<T, object>>[] paths)
        {
            var loader = new QueryableFactoryLoader(queryableFactory, target, degreeOfParallelism);
            loader.Load(paths);
            loadFailures = loader.LoadFailures;
            return queryableFactory;
        }

        /// <summary>
        /// Takes the last n elements from the specified source.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        public static IQueryable<T> Last<T>(this IQueryable<T> source, int count)
        {
            return source.Skip<T>(Math.Max(0, source.Count() - count));
        }

        /// <summary>
        /// Gets the entity for the specified id value. Throws an exception if a property named ID/Id or TypeNameID/TypeNameId does not exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public static T WithId<T>(this IQueryable<T> source, object id)
        {
            PropertyInfo idProperty = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).FirstOrDefault(p => p.Name.Equals("id", StringComparison.OrdinalIgnoreCase) || p.Name.Equals(typeof(T).Name + "id", StringComparison.OrdinalIgnoreCase));
            if (idProperty == null || !idProperty.CanRead || idProperty.GetIndexParameters().Length != 0)
            {
                throw new InvalidOperationException("{0} does not have an id property.".FormatWith(typeof(T).Name));
            }
            ParameterExpression parameter = Expression.Parameter(typeof(T));
            Expression value = Expression.Constant(id);
            if (!idProperty.PropertyType.IsAssignableFrom(value.Type))
            {
                value = Expression.Convert(value, idProperty.PropertyType);
            }
            Expression<Func<T, bool>> filter = Expression.Lambda<Func<T, bool>>(Expression.Equal(Expression.MakeMemberAccess(parameter, idProperty), value), parameter);
            T result = source.FirstOrDefault(filter);
            return result;
        }

        /// <summary>
        /// Gets the entity by name.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static T WithName<T>(this IQueryable<T> source, object name)
        {
            var property = typeof(T).GetProperty("Name").EnsureNotDefault("Type must have a Name property.");

            var parameter = Expression.Parameter(typeof(T));
            var condition = Expression.Equal(Expression.Property(parameter, property), Expression.Constant(name));
            var lambda = Expression.Lambda<Func<T, bool>>(condition, parameter);

            var result = source.FirstOrDefault(lambda);

            return result;
        }

        /// <summary>
        /// Parses the inclusion string expression.
        /// </summary>
        /// <param name="elementType">Type of the element.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal static QueryInclusion ParseQueryInclusion(Type elementType, string value)
        {
            ParameterExpression parameterExpression = Expression.Parameter(elementType);
            Expression bodyExpression = parameterExpression;

            string[] parts = value.Split('.');
            int i = 0;
            foreach (string part in parts)
            {
                PropertyInfo property = elementType.GetProperty(part);
                if (property != null)
                {
                    bodyExpression = Expression.MakeMemberAccess(bodyExpression, property);

                    if (property.PropertyType.IsGenericTypeFor(typeof(IEnumerable<>)) && i < parts.Length - 1)
                    {
                        elementType = property.PropertyType.FindElementType();

                        bodyExpression = Expression.Call(
                            typeof(Enumerable),
                            "Select", new[] { elementType, typeof(object) }, bodyExpression,
                            ParseQueryInclusion(elementType, parts.Skip(i + 1).Join(".")).Expression);

                        break;
                    }
                    elementType = property.PropertyType;
                }
                i++;
            }

            return new QueryInclusion(Expression.Lambda(typeof(Func<,>).MakeGenericType(parameterExpression.Type, typeof(object)), bodyExpression, parameterExpression));
        }

        /// <summary>
        ///   Expands all InvocationExpression instances within the given query.
        /// </summary>
        /// <param name = "query">Query to expand.</param>
        /// <returns>Expanded query.</returns>
        internal static IQueryable<TElement> ExpandInvocations<TElement>(this IQueryable<TElement> query)
        {
            return query.Provider.CreateQuery<TElement>(query.Expression.ExpandInvocations());
        }

        /// <summary>
        ///   Adds QueryInclusion to the specified query.
        /// </summary>
        /// <param name = "queryable">The query.</param>
        /// <param name = "queryInclusions">The QueryInclusion.</param>
        /// <returns></returns>
        internal static IQueryable Include(this IQueryable queryable, params QueryInclusion[] queryInclusions)
        {
            IIncludeProvider includeProvider = queryable as IIncludeProvider ?? queryable.Provider as IIncludeProvider;
            if (includeProvider == null)
            {
                throw new InvalidOperationException("Query does not support QueryInclusion.");
            }
            return includeProvider.ApplyQueryInclusions(queryable, queryInclusions);
        }

        /// <summary>
        /// Adds QueryInclusion to the specified query.
        /// </summary>
        /// <param name="queryable">The query.</param>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        internal static IQueryable Include(this IQueryable queryable, string path)
        {
            return queryable.Include(ParseQueryInclusion(queryable.ElementType, path));
        }

        /// <summary>
        ///   Adds QueryInclusion to the specified query.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "queryable">The query.</param>
        /// <param name = "queryInclusions">The QueryInclusion.</param>
        /// <returns></returns>
        internal static IQueryable<T> Include<T>(this IQueryable<T> queryable, params QueryInclusion[] queryInclusions)
        {
            IIncludeProvider includeProvider = queryable as IIncludeProvider ?? queryable.Provider as IIncludeProvider;
            if (includeProvider == null)
            {
                throw new InvalidOperationException("Query does not support QueryInclusion.");
            }
            return (IQueryable<T>)includeProvider.ApplyQueryInclusions(queryable, queryInclusions);
        }

        /// <summary>
        /// Includes the specified values off of the base value in a queryable include statement.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static IQueryable<T> Include<T, TResult>(this IQueryable<T> query, Expression<Func<T, TResult>> expression)
        {
            return query.Include(new QueryInclusion(expression));
        }

        /// <summary>
        /// Includes the specified values off of the base value in a queryable include statement.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="expressions">The expressions.</param>
        /// <returns></returns>
        public static IQueryable<T> Include<T>(this IQueryable<T> query, params Expression<Func<T, object>>[] expressions)
        {
            if (expressions.Length == 0) return query;
            return expressions.Aggregate(query, (current, expression) => current.Include(new QueryInclusion(expression)));
        }

        /// <summary>
        ///   Applies the calls to Include in the expression.
        /// </summary>
        /// <param name = "expression">The expression.</param>
        /// <returns></returns>
        public static Expression ApplyQueryInclusions(this Expression expression)
        {
            return new QueryInclusionApplier().Visit(expression);
        }

        /// <summary>
        ///   Creates a predicate expression for the specified query, but does not apply it.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "source">The source.</param>
        /// <param name = "predicate">The predicate.</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> CreatePredicate<T>(this IEnumerable<T> source, Expression<Func<T, bool>> predicate = null)
        {
            return predicate;
        }

        /// <summary>
        ///   Creates a list of predicate expressions for the specified query, but does not apply it.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "source">The source.</param>
        /// <param name = "predicate">The predicate.</param>
        /// <returns></returns>
        public static List<Expression<Func<T, bool>>> CreatePredicateList<T>(this IEnumerable<T> source, Expression<Func<T, bool>> predicate = null)
        {
            var result = new List<Expression<Func<T, bool>>>();
            if (predicate != null) result.Add(predicate);
            return result;
        }

        /// <summary>
        ///   Determines whether the method is a Queryable ExtensionMethod.
        /// </summary>
        /// <param name = "methodInfo">The method info.</param>
        /// <param name = "name">The name.</param>
        /// <returns>
        ///   <c>true</c> if [is queryable method] [the specified method info]; otherwise, <c>false</c>.
        /// </returns>
        internal static bool IsQueryableMethod(this MethodInfo methodInfo, string name = null)
        {
            bool isQueryableMethod = methodInfo.HasAttribute(typeof(ExtensionAttribute), false) && methodInfo.DeclaringType == typeof(Queryable) && methodInfo.IsStatic;
            if (name != null)
            {
                isQueryableMethod &= methodInfo.Name == name;
            }
            return isQueryableMethod;
        }

        /// <summary>
        ///   Gets the element type of the queryable type.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        public static Type GetQueryableElementType(this Type type)
        {
            Type queryableType = type.GetTypeAndInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IQueryable<>));
            if (queryableType != null)
            {
                return queryableType.GetGenericArguments()[0];
            }
            return null;
        }

        #region Nested type: QueryInclusionApplier

        /// <summary>
        ///   Applies Include MethodCalls.
        /// </summary>
        private class QueryInclusionApplier : ExpressionVisitor
        {
            private readonly MethodInfo[] includeMethods = typeof(Queryable).GetMember("Include").OfType<MethodInfo>().ToArray();

            protected override Expression VisitMethodCall(MethodCallExpression node)
            {
                if (includeMethods.Contains(node.Method) || (node.Method.IsGenericMethod && includeMethods.Contains(node.Method.GetGenericMethodDefinition())))
                {
                    return Visit(Expression.Lambda(node).Compile().DynamicInvoke().CastTo<IQueryable>().Expression);
                }
                return base.VisitMethodCall(node);
            }

            protected override Expression VisitConstant(ConstantExpression node)
            {
                var nestedQuery = node.Value.As<IQueryable>();
                if (nestedQuery != null && (nestedQuery.Expression.NodeType != ExpressionType.Constant || !Equals(nestedQuery.Expression.CastTo<ConstantExpression>().Value, nestedQuery)))
                {
                    return Expression.Constant(nestedQuery.Provider.CreateQuery(Visit(nestedQuery.Expression)));
                }
                return base.VisitConstant(node);
            }
        }

        #endregion
    }
}

namespace Soaf.Linq.Expressions
{

    /// <summary>
    /// Helps building predicates.
    /// </summary>
    public static class PredicateBuilder<T>
    {
        /// <summary>
        /// 
        /// </summary>
        public static readonly Expression<Func<T, bool>> True = i => true;

        /// <summary>
        /// 
        /// </summary>
        public static readonly Expression<Func<T, bool>> False = i => false;

        /// <summary>
        /// Creates the specified predicate (or a constant true predicate).
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> Create(Expression<Func<T, bool>> predicate = null)
        {
            return predicate ?? True;
        }
    }

    /// <summary>
    /// Denotes a type that creates a predicate for the specified criteria and entity type.
    /// </summary>
    /// <typeparam name="TCriteria">The type of the criteria.</typeparam>
    /// <typeparam name="TElement">The type of the element.</typeparam>
    public interface ICriteriaVisitor<in TCriteria, TElement>
    {
        /// <summary>
        /// Creates a predicate from the specified criteria.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        Expression<Func<TElement, bool>> Visit(TCriteria criteria);
    }


    /// <summary>
    ///   Extension/Helper methods expressions
    /// </summary>
    public static class Expressions
    {
        #region ExpressionVisitMode enum

        /// <summary>
        /// Describes in what order nodes of an expression tree are visited.
        /// </summary>
        public enum ExpressionVisitMode
        {
            /// <summary>
            /// 
            /// </summary>
            TopDown,

            /// <summary>
            /// 
            /// </summary>
            BottomUp
        }

        #endregion

        internal static readonly ExpressionType[] AndExpressionTypes = { ExpressionType.And, ExpressionType.AndAlso };
        internal static readonly ExpressionType[] OrExpressionTypes = { ExpressionType.Or, ExpressionType.OrElse };

        /// <summary>
        /// Removes conditional null checks from an expression tree to improve readability (IFF(e == null, null, e.Member)).
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        internal static Expression RemoveConditionalNullChecks(this Expression expression)
        {
            Expression result = expression.Replace<ConditionalExpression>(c =>
                                                                              {
                                                                                  if (IsConditionalNullCheck(c.Test as BinaryExpression, c.IfTrue, c.IfFalse))
                                                                                  {
                                                                                      return c.Test.NodeType == ExpressionType.Equal ? c.IfFalse : c.IfTrue;
                                                                                  }
                                                                                  return c;
                                                                              });
            return result;
        }

        /// <summary>
        /// Removes any Convert expressions surrounding the expression.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        internal static Expression RemoveConverts(this Expression expression)
        {
            while (new[] { ExpressionType.Convert, ExpressionType.ConvertChecked }.Contains(expression.NodeType))
            {
                expression = ((UnaryExpression)expression).Operand;
            }
            return expression;
        }

        /// <summary>
        /// Replaces the specified expression.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <param name="replacer">The replacer.</param>
        /// <returns></returns>
        public static Expression Replace<T>(this Expression expression, Func<T, Expression> replacer) where T : Expression
        {
            return new ReplacingExpressionVisitor<T>(replacer).Visit(expression);
        }

        /// <summary>
        /// Replaces the specified expression.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <param name="replacer">The replacer.</param>
        /// <returns></returns>
        public static Expression Replace(this Expression expression, Func<Expression, Expression> replacer)
        {
            return new ReplacingExpressionVisitor<Expression>(replacer).Visit(expression);
        }

        /// <summary>
        /// Replaces the specified expression.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <param name="toReplace">To replace.</param>
        /// <param name="replacement">The replacement.</param>
        /// <returns></returns>
        public static Expression Replace(this Expression expression, Expression toReplace, Expression replacement)
        {
            return new ReplacingExpressionVisitor<Expression>(e => e == toReplace ? replacement : e).Visit(expression);
        }

        private static bool IsConditionalNullCheck(BinaryExpression test, Expression ifTrue, Expression ifFalse)
        {
            if (test == null || ifTrue == null) return false;

            if (IsNullCheck(test.NodeType, test.Left, test.Right) || IsNullCheck(test.NodeType, test.Left, test.Right))
            {
                Expression nullExpression = test.NodeType == ExpressionType.Equal ? ifTrue : ifFalse;
                return nullExpression.NodeType == ExpressionType.Constant && new object[] { null, false }.Contains(((ConstantExpression)nullExpression).Value);
            }

            return false;
        }

        private static bool IsNullCheck(ExpressionType type, Expression left, Expression right)
        {
            return new[] { ExpressionType.Equal, ExpressionType.NotEqual }.Contains(type) && new[] { ExpressionType.MemberAccess, ExpressionType.Call, ExpressionType.Convert, ExpressionType.ConvertChecked }.Contains(left.NodeType) && right.NodeType == ExpressionType.Constant && ((ConstantExpression)right).Value == null;
        }

        /// <summary>
        /// Strips the quotes.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        internal static Expression StripQuotes(this Expression e)
        {
            while (e.NodeType == ExpressionType.Quote)
                e = ((UnaryExpression)e).Operand;

            return e;
        }

        /// <summary>
        /// Visits the expression's nodes as an enumerable.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <param name="visitMode">The visit mode.</param>
        /// <returns></returns>
        public static IEnumerable<Expression> AsEnumerable(this Expression expression, ExpressionVisitMode visitMode = ExpressionVisitMode.TopDown)
        {
            return new EnumerableExpression(expression, visitMode);
        }

        /// <summary>
        /// Invokes the specified expression.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static TResult Invoke<TResult>(this Expression<Func<TResult>> expression)
        {
            return expression.Compile().Invoke();
        }

        /// <summary>
        /// Invokes the specified expression.
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="expression">The expression.</param>
        /// <param name="arg1">The arg1.</param>
        /// <returns></returns>
        public static TResult Invoke<T1, TResult>(this Expression<Func<T1, TResult>> expression, T1 arg1)
        {
            return expression.Compile().Invoke(arg1);
        }

        /// <summary>
        /// Invokes the specified expression.
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="T2">The type of the 2.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="expression">The expression.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <returns></returns>
        public static TResult Invoke<T1, T2, TResult>(this Expression<Func<T1, T2, TResult>> expression, T1 arg1, T2 arg2)
        {
            return expression.Compile().Invoke(arg1, arg2);
        }

        /// <summary>
        /// Invokes the specified expression.
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="T2">The type of the 2.</typeparam>
        /// <typeparam name="T3">The type of the 3.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="expression">The expression.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="arg3">The arg3.</param>
        /// <returns></returns>
        public static TResult Invoke<T1, T2, T3, TResult>(this Expression<Func<T1, T2, T3, TResult>> expression, T1 arg1, T2 arg2, T3 arg3)
        {
            return expression.Compile().Invoke(arg1, arg2, arg3);
        }

        /// <summary>
        /// Invokes the specified expression.
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="T2">The type of the 2.</typeparam>
        /// <typeparam name="T3">The type of the 3.</typeparam>
        /// <typeparam name="T4">The type of the 4.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="expression">The expression.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="arg3">The arg3.</param>
        /// <param name="arg4">The arg4.</param>
        /// <returns></returns>
        public static TResult Invoke<T1, T2, T3, T4, TResult>(this Expression<Func<T1, T2, T3, T4, TResult>> expression, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            return expression.Compile().Invoke(arg1, arg2, arg3, arg4);
        }

        /// <summary>
        ///   Gathers all the MemberExpressions from an expression tree top-down.
        /// </summary>
        /// <param name = "expression">The expression.</param>
        /// <returns></returns>
        internal static IEnumerable<MemberExpression> GatherMemberExpressions(this Expression expression)
        {
            return MemberExpressionVisitor.GetMemberExpressions(expression);
        }

        /// <summary>
        ///   Composes the two expressions specified using the specified merge function to combine them.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "first">The first.</param>
        /// <param name = "second">The second.</param>
        /// <param name = "merge">The merge.</param>
        /// <returns></returns>
        internal static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
        {
            if (first == null || second == null) return new[] { first, second }.WhereNotDefault().FirstOrDefault();

            ParameterExpression parameterExpression = Expression.Parameter(first.Parameters.First().Type, "x");
            Expression body = merge(
                Expression.Invoke(first, parameterExpression),
                Expression.Invoke(second, parameterExpression)
                );
            Expression<T> lambda = Expression.Lambda<T>(body.ExpandInvocations(), parameterExpression);
            return lambda;
        }

        /// <summary>
        ///   Combines the two expressions with an And condition.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "first">The first.</param>
        /// <param name = "second">The second.</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.And);
        }

        /// <summary>
        ///   Combines the two expressions with an Or condition.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "first">The first.</param>
        /// <param name = "second">The second.</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose(second, Expression.Or);
        }

        /// <summary>
        ///   Creates a Not for the specified expression.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "expression">The expression.</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> Not<T>(this Expression<Func<T, bool>> expression)
        {
            ParameterExpression parameterExpression = Expression.Parameter(expression.Parameters.First().Type, "x");
            Expression body = Expression.Not(Expression.Invoke(expression, parameterExpression));
            Expression<Func<T, bool>> lambda = Expression.Lambda<Func<T, bool>>(body, parameterExpression);
            return lambda;
        }

        /// <summary>
        ///   Expands all InvocationExpression instances within the given expression.
        /// </summary>
        /// <param name = "expression">Expression to expand.</param>
        /// <returns>Expanded expression.</returns>
        public static Expression ExpandInvocations(this Expression expression)
        {
            return InvocationExpander.Expand(expression);
        }

        /// <summary>
        ///   Creates a string representation of the MemberAccess expressions in the specified expression. Used specifically for query QueryInclusion string representations.
        /// </summary>
        /// <param name = "expression">The expression.</param>
        /// <returns></returns>
        public static string[] ToMemberAccessStrings(this Expression expression)
        {
            var results = new List<string>();
            try
            {
                switch (expression.NodeType)
                {
                    case ExpressionType.NewArrayInit:
                        results.AddRange(expression.CastTo<NewArrayExpression>().Expressions.SelectMany(ToMemberAccessStrings));
                        return results.ToArray();
                    case ExpressionType.New:
                        return expression.CastTo<NewExpression>().Arguments.SelectMany(ToMemberAccessStrings).ToArray();
                    case ExpressionType.MemberInit:
                        return expression.CastTo<MemberInitExpression>().NewExpression.Arguments.SelectMany(ToMemberAccessStrings)
                            .Concat(expression.CastTo<MemberInitExpression>().Bindings.OfType<MemberAssignment>().Select(ma => ma.Expression).SelectMany(ToMemberAccessStrings))
                            .ToArray();
                    case ExpressionType.Lambda:
                        return expression.CastTo<LambdaExpression>().Body.ToMemberAccessStrings();
                    case ExpressionType.MemberAccess:
                        var memberExpression = expression.CastTo<MemberExpression>();
                        var pi = memberExpression.Member.CastTo<PropertyInfo>();
                        return memberExpression.Expression.ToMemberAccessStrings().Select(v => new[] { v, pi.Name }.Join(".", true)).ToArray();
                    case ExpressionType.Call:
                        var methodCallExpression = expression.CastTo<MethodCallExpression>();
                        if (methodCallExpression.Method.GetAttribute<ExtensionAttribute>() != null)
                        {
                            string first = methodCallExpression.Arguments[0].ToMemberAccessStrings().First();
                            if (methodCallExpression.Arguments.Count > 1)
                            {
                                results.AddRange(methodCallExpression.Arguments[1].ToMemberAccessStrings().Select(item => new[] { first, item }.Join(".", true)));
                            }
                            else
                            {
                                results.Add(first);
                            }
                        }
                        return results.ToArray();
                    case ExpressionType.Quote:
                        Expression lambdaExpressionBody = expression.CastTo<UnaryExpression>().Operand.CastTo<LambdaExpression>().Body;
                        return lambdaExpressionBody.ToMemberAccessStrings();
                    case ExpressionType.Parameter:
                        return new[] { string.Empty };
                    case ExpressionType.Convert:
                        return ToMemberAccessStrings(expression.CastTo<UnaryExpression>().Operand);
                }
            }
            catch
            {
                throw new ArgumentException("Could not process the specified expression {0}.".FormatWith(expression));
            }
            throw new ArgumentException("Could not process the specified expression {0}.".FormatWith(expression));
        }

        /// <summary>
        ///   Gets the best known node type of the expression. If value is not null, gets the type of the instance. Otherwise returns the expression.Type.
        /// </summary>
        /// <param name = "expression">The expression.</param>
        /// <returns></returns>
        internal static Type GetBestNodeType(this ConstantExpression expression)
        {
            return expression.Value != null ? expression.Value.GetType() : expression.GetType();
        }

        /// <summary>
        ///   Emits a Call expression using the specified arguments, converting each to the required parameter type.
        /// </summary>
        /// <param name = "methodInfo">The method info.</param>
        /// <param name = "instance">The instance.</param>
        /// <param name = "arguments">The arguments.</param>
        /// <returns></returns>
        internal static Expression MakeConvertedCallExpression(this MethodInfo methodInfo, Expression instance, params Expression[] arguments)
        {
            var convertedArguments = new List<Expression>();
            int i = 0;
            foreach (ParameterInfo pi in methodInfo.GetParameters())
            {
                convertedArguments.Add(Expression.Convert(arguments[i], pi.ParameterType));
                i++;
            }
            return Expression.Call(instance, methodInfo, convertedArguments);
        }

        /// <summary>
        ///   Compiles the specified body.
        /// </summary>
        /// <typeparam name = "TDelegate">The type of the delegate.</typeparam>
        /// <param name = "body">The body.</param>
        /// <param name = "parameters">The parameters.</param>
        /// <returns></returns>
        internal static TDelegate Compile<TDelegate>(this Expression body, params ParameterExpression[] parameters)
        {
            MethodInfo methodInfo = typeof(TDelegate).GetMethod("Invoke");

            if (methodInfo.ReturnType == typeof(void))
            {
                body = Expression.Block(body, Expression.Constant(null));
            }

            return Expression.Lambda<TDelegate>(Expression.Convert(body, methodInfo.ReturnType), parameters.Length == 0 ? methodInfo.GetParameters().Select(p => Expression.Parameter(p.ParameterType)) : parameters).Compile();
        }

        /// <summary>
        ///   Determines whether the type is a lambda function with the specific arugment types.
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <param name = "argumentsToMatch">The arguments to match.</param>
        /// <returns>
        ///   <c>true</c> if [is lambda for func with] [the specified type]; otherwise, <c>false</c>.
        /// </returns>
        internal static bool IsLambdaForFuncWith(this Type type, params Type[] argumentsToMatch)
        {
            if (!type.IsGenericType || type.GetGenericTypeDefinition() != typeof(Expression<>))
            {
                return false;
            }

            Type funcType = type.GetGenericArguments()[0];

            if (!funcType.IsGenericType)
            {
                return false;
            }

            Type[] funcArguments = funcType.GetGenericArguments();
            if (funcArguments.Length != argumentsToMatch.Length)
            {
                return false;
            }

            return !funcArguments.Where((t, i) => argumentsToMatch[i] != null && t != argumentsToMatch[i]).Any();
        }

        /// <summary>
        ///   Converts parts of the expression tree that can be evaluated locally to inlined constants.
        /// </summary>
        /// <param name = "expression">The expression.</param>
        /// <returns></returns>
        public static Expression EvaluateLocalExpressions(this Expression expression)
        {
            return LocalExpressionEvaluator.Evaluate(expression);
        }

        /// <summary>
        ///   Visits the specified expression.
        /// </summary>
        /// <param name = "expression">The expression.</param>
        /// <param name = "visit">The visit.</param>
        /// <returns></returns>
        internal static Expression Visit<T>(this Expression expression, Action<T> visit = null) where T : Expression
        {
            return new ReplacingExpressionVisitor<T>(e =>
                                                         {
                                                             if (visit != null) visit(e);
                                                             return e;
                                                         }).Visit(expression);
        }

        /// <summary>
        ///   Visits the specified expression.
        /// </summary>
        /// <param name = "expression">The expression.</param>
        /// <param name = "visit">The visit.</param>
        /// <returns></returns>
        internal static Expression Visit(this Expression expression, Action<Expression> visit = null)
        {
            return new ReplacingExpressionVisitor<Expression>(e =>
                                                                  {
                                                                      if (visit != null) visit(e);
                                                                      return e;
                                                                  }).Visit(expression);
        }

        /// <summary>
        /// Simplifies the specified expression by replacing certain components with simpler, more compatible ones.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        internal static Expression SimplifyComparisons(this Expression expression)
        {
            return new ComparisonSimplifyingExpressionVisitor().Visit(expression);
        }

        #region Nested type: EnumerableExpression

        private sealed class EnumerableExpression : ExpressionVisitor, IEnumerable<Expression>
        {
            private readonly ExpressionVisitMode visitMode;
            private readonly List<Expression> visited = new List<Expression>();

            public EnumerableExpression(Expression expression, ExpressionVisitMode visitMode)
            {
                this.visitMode = visitMode;
                Visit(expression);
            }

            #region IEnumerable<Expression> Members

            public IEnumerator<Expression> GetEnumerator()
            {
                return visited.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            #endregion

            public override Expression Visit(Expression node)
            {
                if (visitMode == ExpressionVisitMode.TopDown)
                {
                    visited.Add(node);
                }
                Expression result = base.Visit(node);
                if (visitMode == ExpressionVisitMode.BottomUp)
                {
                    visited.Add(node);
                }
                return result;
            }
        }

        #endregion

        #region Nested type: InvocationExpander

        /// <summary>
        ///   Expands invocation expressions by replacing parameters with values.
        /// </summary>
        private sealed class InvocationExpander : ExpressionVisitor
        {
            private static readonly InvocationExpander Instance = new InvocationExpander();
            private readonly Expression expansion;
            private readonly ParameterExpression parameter;
            private readonly InvocationExpander previous;

            private InvocationExpander(ParameterExpression parameter, Expression expansion, InvocationExpander previous)
            {
                this.parameter = parameter;
                this.expansion = expansion;
                this.previous = previous;
            }

            private InvocationExpander()
            {
            }

            internal static Expression Expand(Expression expression)
            {
                return Instance.Visit(expression);
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                InvocationExpander expander = this;
                while (null != expander)
                {
                    if (expander.parameter == node)
                    {
                        return Visit(expander.expansion);
                    }
                    expander = expander.previous;
                }
                return base.VisitParameter(node);
            }

            protected override Expression VisitInvocation(InvocationExpression node)
            {
                if (node.Expression.NodeType == ExpressionType.Lambda)
                {
                    var lambda = (LambdaExpression)node.Expression;
                    return lambda
                        .Parameters
                        // zip together parameters and the corresponding argument values
                        .Zip(node.Arguments, (p, e) => new { Parameter = p, Expansion = e })
                        // add to the stack of available parameters bindings (this class doubles as an immutable stack)
                        .Aggregate(this, (previous, pair) => new InvocationExpander(pair.Parameter, pair.Expansion, previous))
                        // visit the body of the lambda using an expander including the new parameter bindings
                        .Visit(lambda.Body);
                }

                var innerCall = node.Expression as MethodCallExpression;
                if (innerCall != null && innerCall.Object != null && typeof(LambdaExpression).IsAssignableFrom(innerCall.Method.DeclaringType) && innerCall.Method.Name == "Compile")
                {
                    return Visit(Expression.Invoke(
                                    Expression.Lambda(innerCall.Object).Compile().DynamicInvoke().CastTo<LambdaExpression>(),
                                    node.Arguments));
                }

                return base.VisitInvocation(node);
            }

            protected override Expression VisitMethodCall(MethodCallExpression node)
            {
                if (node.Method.DeclaringType == typeof(Expressions) && node.Method.Name == "Invoke")
                {
                    return Visit(Expression.Invoke(
                        Expression.Lambda(node.Arguments[0]).Compile().DynamicInvoke().CastTo<LambdaExpression>(),
                        node.Arguments.Skip(1).ToArray()));
                }

                return base.VisitMethodCall(node);
            }
        }

        #endregion

        #region Nested type: LocalExpressionEvaluator

        private static class LocalExpressionEvaluator
        {
            public static Expression Evaluate(Expression expression)
            {
                List<Type> visitedNodeTypes;
                return Evaluate(expression, CanBeEvaluatedLocally, out visitedNodeTypes);
            }

            /// <summary>
            ///   Does a partial eval of the tree.
            /// </summary>
            /// <param name = "expression">The node.</param>
            /// <param name = "canBeEvaluated">Criteria for if the node can be evaluated.</param>
            /// <param name = "visitedNodeTypes">The visited node types.</param>
            /// <returns></returns>
            private static Expression Evaluate(Expression expression, Func<Expression, bool> canBeEvaluated,
                                               out List<Type> visitedNodeTypes)
            {
                var evaluator = new SubtreeEvaluator(new Nominator(canBeEvaluated).Nominate(expression));
                visitedNodeTypes = evaluator.VisitedNodeTypes;
                Expression result = evaluator.EvaluateCandidates(expression);
                return result;
            }

            private static bool CanBeEvaluatedLocally(Expression expression)
            {
                bool result = expression.NodeType != ExpressionType.Parameter &&
                              expression.NodeType != ExpressionType.Lambda &&
                              expression.NodeType != ExpressionType.New &&
                              (expression.NodeType != ExpressionType.Extension || expression.CanReduce) &&
                    // scenario queryable.Take(100)...don't want to eval queryable locally, otherwise we will never evaluate Take(100)
                              (!expression.Is<ConstantExpression>() ||
                               !expression.CastTo<ConstantExpression>().Value.Is<IQueryable>());
                return result;
            }

            #region Nested type: Nominator

            /// <summary>
            ///   Performs bottom-up analysis to determine which nodes can possibly
            ///   be part of an evaluated sub-tree.
            /// </summary>
            private class Nominator : ExpressionVisitor
            {
                private readonly Func<Expression, bool> canBeEvaluated;
                private List<Expression> candidates;
                private bool cannotBeEvaluated;

                internal Nominator(Func<Expression, bool> canBeEvaluated)
                {
                    this.canBeEvaluated = canBeEvaluated;
                }

                internal ICollection<Expression> Nominate(Expression e)
                {
                    candidates = new List<Expression>();
                    Visit(e);
                    return candidates.AsEnumerable().Reverse().ToArray();
                }


                public override Expression Visit(Expression node)
                {
                    if (node != null)
                    {
                        bool saveCannotBeEvaluated = cannotBeEvaluated;
                        cannotBeEvaluated = false;
                        base.Visit(node);
                        if (!cannotBeEvaluated)
                        {
                            if (canBeEvaluated(node))
                            {
                                candidates.Add(node);
                            }
                            else
                            {
                                cannotBeEvaluated = true;
                            }
                        }
                        cannotBeEvaluated |= saveCannotBeEvaluated;
                    }
                    return node;
                }

                protected override Expression VisitConstant(ConstantExpression node)
                {
                    var nestedQuery = node.Value.As<IQueryable>();
                    if (nestedQuery != null && (nestedQuery.Expression.NodeType != ExpressionType.Constant || !Equals(nestedQuery.Expression.CastTo<ConstantExpression>().Value, nestedQuery)))
                    {
                        Visit(nestedQuery.Expression);
                        return node;
                    }
                    return base.VisitConstant(node);
                }
            }

            #endregion

            #region Nested type: SubtreeEvaluator

            /// <summary>
            ///   Evaluates & replaces sub-trees when first candidate is reached (top-down)
            /// </summary>
            private class SubtreeEvaluator : ExpressionVisitor
            {
                private readonly ICollection<Expression> candidates;

                internal SubtreeEvaluator(ICollection<Expression> candidates)
                {
                    this.candidates = candidates;
                    VisitedNodeTypes = new List<Type>();
                }

                internal List<Type> VisitedNodeTypes { get; private set; }

                internal Expression EvaluateCandidates(Expression e)
                {
                    if (candidates.Count == 0) return e;

                    return Visit(e);
                }

                public override Expression Visit(Expression node)
                {
                    if (node == null)
                    {
                        return null;
                    }

                    VisitedNodeTypes.Add(node.Type);
                    if (candidates.Contains(node))
                    {
                        return Evaluate(node);
                    }
                    return base.Visit(node);
                }

                protected override Expression VisitConstant(ConstantExpression node)
                {
                    var nestedQuery = node.Value.As<IQueryable>();
                    if (nestedQuery != null && (nestedQuery.Expression.NodeType != ExpressionType.Constant || !Equals(nestedQuery.Expression.CastTo<ConstantExpression>().Value, nestedQuery)))
                    {
                        return Expression.Constant(nestedQuery.Provider.CreateQuery(nestedQuery.Expression.EvaluateLocalExpressions()));
                    }
                    return base.VisitConstant(node);
                }

                private Expression Evaluate(Expression e)
                {
                    if (e.NodeType == ExpressionType.Constant)
                    {
                        return VisitConstant((ConstantExpression)e);
                    }
                    LambdaExpression lambda = Expression.Lambda(e);
                    object result = lambda.Compile().DynamicInvoke();
                    return VisitConstant(Expression.Constant(result, e.Type));
                }
            }

            #endregion
        }

        #endregion

        #region Nested type: MemberExpressionVisitor

        /// <summary>
        ///   Gathers all the MemberExpressions from an expression tree top-down.
        /// </summary>
        private class MemberExpressionVisitor : ExpressionVisitor
        {
            private readonly List<MemberExpression> memberExpressions = new List<MemberExpression>();

            private MemberExpressionVisitor()
            {
            }

            public static IEnumerable<MemberExpression> GetMemberExpressions(Expression expression)
            {
                var visitor = new MemberExpressionVisitor();
                visitor.Visit(expression);
                return visitor.memberExpressions;
            }

            protected override Expression VisitMember(MemberExpression node)
            {
                memberExpressions.Add(node);
                return base.VisitMember(node);
            }
        }

        #endregion

        #region Nested type: ReplacingExpressionVisitor

        /// <summary>
        ///   A basic expression visitor that rewrites an expression.
        /// </summary>
        private class ReplacingExpressionVisitor<T> : ExpressionVisitor where T : Expression
        {
            private readonly Func<T, Expression> visit;

            public ReplacingExpressionVisitor(Func<T, Expression> visit)
            {
                this.visit = visit;
            }

            public override Expression Visit(Expression node)
            {
                node = base.Visit(node);
                if (visit != null && node is T)
                {
                    var replaced = visit(node.CastTo<T>());
                    if (replaced != node) node = Visit(replaced);
                }
                return node;
            }
        }

        #endregion

        #region Nested type: ComparisonSimplifyingExpressionVisitor

        /// <summary>
        /// A derived ExpressionVisitor that performs some simple additional logic to remove VB specific expressions.
        /// </summary>
        private class ComparisonSimplifyingExpressionVisitor : ExpressionVisitor
        {
            protected override Expression VisitBinary(BinaryExpression node)
            {
                node = TryVisitVisualBasicStringCompare(node);
                return base.VisitBinary(node);
            }

            /// <summary>
            /// Tries to replace the VB compare expression with a regular binary one.
            /// </summary>
            /// <param name="node">The node.</param>
            /// <returns></returns>
            private BinaryExpression TryVisitVisualBasicStringCompare(BinaryExpression node)
            {
                MethodCallExpression compareMethodCall = TryGetVisualBasicCompareMethod(node.Left) ?? TryGetVisualBasicCompareMethod(node.Right);
                if (compareMethodCall != null)
                {
                    Expression arg1 = compareMethodCall.Arguments[0];
                    Expression arg2 = compareMethodCall.Arguments[1];
                    switch (node.NodeType)
                    {
                        case ExpressionType.LessThan:
                            return Expression.LessThan(arg1, arg2);
                        case ExpressionType.LessThanOrEqual:
                            return Expression.LessThanOrEqual(arg1, arg2);
                        case ExpressionType.GreaterThan:
                            return Expression.GreaterThan(arg1, arg2);
                        case ExpressionType.GreaterThanOrEqual:
                            return Expression.GreaterThanOrEqual(arg1, arg2);
                        default:
                            return Expression.Equal(arg1, arg2);
                    }
                }
                return node;
            }

            private MethodCallExpression TryGetVisualBasicCompareMethod(Expression candidate)
            {
                var compareMethodCall = candidate as MethodCallExpression;
                return (compareMethodCall != null && compareMethodCall.Method.Name == "CompareString" && compareMethodCall.Method.DeclaringType != null && compareMethodCall.Method.DeclaringType.FullName == "Microsoft.VisualBasic.CompilerServices.Operators") ? compareMethodCall : null;
            }
        }

        #endregion
    }

    /// <summary>
    /// An equality comparer for expressions.
    /// </summary>
    public class ExpressionEqualityComparer : IEqualityComparer<Expression>
    {
        #region IEqualityComparer<Expression> Members

        public bool Equals(Expression x, Expression y)
        {
            return ExpressionComparer.AreEqual(x, y);
        }

        public int GetHashCode(Expression obj)
        {
            return ExpressionHashCodeGenerator.GetHashCode(obj);
        }

        #endregion
    }

    /// <summary>
    /// Generates a hash code for an expression. 
    /// </summary>
    public sealed class ExpressionHashCodeGenerator : ExpressionVisitor
    {
        private int hashCode;

        private ExpressionHashCodeGenerator(Expression expression)
        {
            Visit(expression);
        }

        public static int GetHashCode(Expression expression)
        {
            return new ExpressionHashCodeGenerator(expression).hashCode;
        }

        private void Add(int i)
        {
            hashCode *= 37;
            hashCode ^= i;
        }

        public override Expression Visit(Expression node)
        {
            if (node == null) return null;

            Add((int)node.NodeType);
            Add(node.Type.GetHashCode());

            return base.Visit(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            if (node.Value != null) Add(node.Value.GetHashCode());
            return base.VisitConstant(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            Add(node.Member.GetHashCode());
            return base.VisitMember(node);
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            Add(node.Method.GetHashCode());
            return base.VisitMethodCall(node);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            Add(node.Name.GetHashCode());
            return base.VisitParameter(node);
        }

        protected override Expression VisitTypeBinary(TypeBinaryExpression node)
        {
            Add(node.TypeOperand.GetHashCode());
            return base.VisitTypeBinary(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            if (node.Method != null) Add(node.Method.GetHashCode());
            return base.VisitBinary(node);
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.Method != null) Add(node.Method.GetHashCode());
            return base.VisitUnary(node);
        }

        protected override Expression VisitNew(NewExpression node)
        {
            Add(node.Constructor.GetHashCode());
            return base.VisitNew(node);
        }
    }

    /// <summary>
    /// Compare two expressions to determine if they are equivalent
    /// </summary>
    internal class ExpressionComparer
    {
        private readonly Func<object, object, bool> comparer;
        private ScopedDictionary<ParameterExpression, ParameterExpression> parameterScope;

        protected ExpressionComparer(
            ScopedDictionary<ParameterExpression, ParameterExpression> parameterScope,
            Func<object, object, bool> comparer
            )
        {
            this.parameterScope = parameterScope;
            this.comparer = comparer;
        }

        protected Func<object, object, bool> Comparer
        {
            get { return comparer; }
        }

        public static bool AreEqual(Expression a, Expression b)
        {
            return AreEqual(null, a, b);
        }

        public static bool AreEqual(Expression a, Expression b, Func<object, object, bool> fnCompare)
        {
            return AreEqual(null, a, b, fnCompare);
        }

        public static bool AreEqual(ScopedDictionary<ParameterExpression, ParameterExpression> parameterScope, Expression a, Expression b)
        {
            return new ExpressionComparer(parameterScope, null).Compare(a, b);
        }

        public static bool AreEqual(ScopedDictionary<ParameterExpression, ParameterExpression> parameterScope, Expression a, Expression b, Func<object, object, bool> fnCompare)
        {
            return new ExpressionComparer(parameterScope, fnCompare).Compare(a, b);
        }

        protected virtual bool Compare(Expression a, Expression b)
        {
            if (a == b)
                return true;
            if (a == null || b == null)
                return false;
            if (a.NodeType != b.NodeType)
                return false;
            if (a.Type != b.Type)
                return false;
            switch (a.NodeType)
            {
                case ExpressionType.Negate:
                case ExpressionType.NegateChecked:
                case ExpressionType.Not:
                case ExpressionType.Convert:
                case ExpressionType.ConvertChecked:
                case ExpressionType.ArrayLength:
                case ExpressionType.Quote:
                case ExpressionType.TypeAs:
                case ExpressionType.UnaryPlus:
                    return CompareUnary((UnaryExpression)a, (UnaryExpression)b);
                case ExpressionType.Add:
                case ExpressionType.AddChecked:
                case ExpressionType.Subtract:
                case ExpressionType.SubtractChecked:
                case ExpressionType.Multiply:
                case ExpressionType.MultiplyChecked:
                case ExpressionType.Divide:
                case ExpressionType.Modulo:
                case ExpressionType.And:
                case ExpressionType.AndAlso:
                case ExpressionType.Or:
                case ExpressionType.OrElse:
                case ExpressionType.LessThan:
                case ExpressionType.LessThanOrEqual:
                case ExpressionType.GreaterThan:
                case ExpressionType.GreaterThanOrEqual:
                case ExpressionType.Equal:
                case ExpressionType.NotEqual:
                case ExpressionType.Coalesce:
                case ExpressionType.ArrayIndex:
                case ExpressionType.RightShift:
                case ExpressionType.LeftShift:
                case ExpressionType.ExclusiveOr:
                case ExpressionType.Power:
                    return CompareBinary((BinaryExpression)a, (BinaryExpression)b);
                case ExpressionType.TypeIs:
                    return CompareTypeIs((TypeBinaryExpression)a, (TypeBinaryExpression)b);
                case ExpressionType.Conditional:
                    return CompareConditional((ConditionalExpression)a, (ConditionalExpression)b);
                case ExpressionType.Constant:
                    return CompareConstant((ConstantExpression)a, (ConstantExpression)b);
                case ExpressionType.Parameter:
                    return CompareParameter((ParameterExpression)a, (ParameterExpression)b);
                case ExpressionType.MemberAccess:
                    return CompareMemberAccess((MemberExpression)a, (MemberExpression)b);
                case ExpressionType.Call:
                    return CompareMethodCall((MethodCallExpression)a, (MethodCallExpression)b);
                case ExpressionType.Lambda:
                    return CompareLambda((LambdaExpression)a, (LambdaExpression)b);
                case ExpressionType.New:
                    return CompareNew((NewExpression)a, (NewExpression)b);
                case ExpressionType.NewArrayInit:
                case ExpressionType.NewArrayBounds:
                    return CompareNewArray((NewArrayExpression)a, (NewArrayExpression)b);
                case ExpressionType.Invoke:
                    return CompareInvocation((InvocationExpression)a, (InvocationExpression)b);
                case ExpressionType.MemberInit:
                    return CompareMemberInit((MemberInitExpression)a, (MemberInitExpression)b);
                case ExpressionType.ListInit:
                    return CompareListInit((ListInitExpression)a, (ListInitExpression)b);
                default:
                    return Equals(a, b);
            }
        }

        protected virtual bool CompareUnary(UnaryExpression a, UnaryExpression b)
        {
            return a.NodeType == b.NodeType
                   && a.Method == b.Method
                   && a.IsLifted == b.IsLifted
                   && a.IsLiftedToNull == b.IsLiftedToNull
                   && Compare(a.Operand, b.Operand);
        }

        protected virtual bool CompareBinary(BinaryExpression a, BinaryExpression b)
        {
            return a.NodeType == b.NodeType
                   && a.Method == b.Method
                   && a.IsLifted == b.IsLifted
                   && a.IsLiftedToNull == b.IsLiftedToNull
                   && Compare(a.Left, b.Left)
                   && Compare(a.Right, b.Right);
        }

        protected virtual bool CompareTypeIs(TypeBinaryExpression a, TypeBinaryExpression b)
        {
            return a.TypeOperand == b.TypeOperand
                   && Compare(a.Expression, b.Expression);
        }

        protected virtual bool CompareConditional(ConditionalExpression a, ConditionalExpression b)
        {
            return Compare(a.Test, b.Test)
                   && Compare(a.IfTrue, b.IfTrue)
                   && Compare(a.IfFalse, b.IfFalse);
        }

        protected virtual bool CompareConstant(ConstantExpression a, ConstantExpression b)
        {
            if (comparer != null)
            {
                return comparer(a.Value, b.Value);
            }
            return Equals(a.Value, b.Value);
        }

        protected virtual bool CompareParameter(ParameterExpression a, ParameterExpression b)
        {
            if (parameterScope != null)
            {
                ParameterExpression mapped;
                if (parameterScope.TryGetValue(a, out mapped))
                    return mapped == b;
            }
            return a == b;
        }

        protected virtual bool CompareMemberAccess(MemberExpression a, MemberExpression b)
        {
            return a.Member == b.Member
                   && Compare(a.Expression, b.Expression);
        }

        protected virtual bool CompareMethodCall(MethodCallExpression a, MethodCallExpression b)
        {
            return a.Method == b.Method
                   && Compare(a.Object, b.Object)
                   && CompareExpressionList(a.Arguments, b.Arguments);
        }

        protected virtual bool CompareLambda(LambdaExpression a, LambdaExpression b)
        {
            int n = a.Parameters.Count;
            if (b.Parameters.Count != n)
                return false;
            // all must have same type
            for (int i = 0; i < n; i++)
            {
                if (a.Parameters[i].Type != b.Parameters[i].Type)
                    return false;
            }
            ScopedDictionary<ParameterExpression, ParameterExpression> save = parameterScope;
            parameterScope = new ScopedDictionary<ParameterExpression, ParameterExpression>(parameterScope);
            try
            {
                for (int i = 0; i < n; i++)
                {
                    parameterScope.Add(a.Parameters[i], b.Parameters[i]);
                }
                return Compare(a.Body, b.Body);
            }
            finally
            {
                parameterScope = save;
            }
        }

        protected virtual bool CompareNew(NewExpression a, NewExpression b)
        {
            return a.Constructor == b.Constructor
                   && CompareExpressionList(a.Arguments, b.Arguments)
                   && CompareMemberList(a.Members, b.Members);
        }

        protected virtual bool CompareExpressionList(ReadOnlyCollection<Expression> a, ReadOnlyCollection<Expression> b)
        {
            if (a == b)
                return true;
            if (a == null || b == null)
                return false;
            if (a.Count != b.Count)
                return false;
            for (int i = 0, n = a.Count; i < n; i++)
            {
                if (!Compare(a[i], b[i]))
                    return false;
            }
            return true;
        }

        protected virtual bool CompareMemberList(ReadOnlyCollection<MemberInfo> a, ReadOnlyCollection<MemberInfo> b)
        {
            if (a == b)
                return true;
            if (a == null || b == null)
                return false;
            if (a.Count != b.Count)
                return false;
            for (int i = 0, n = a.Count; i < n; i++)
            {
                if (a[i] != b[i])
                    return false;
            }
            return true;
        }

        protected virtual bool CompareNewArray(NewArrayExpression a, NewArrayExpression b)
        {
            return CompareExpressionList(a.Expressions, b.Expressions);
        }

        protected virtual bool CompareInvocation(InvocationExpression a, InvocationExpression b)
        {
            return Compare(a.Expression, b.Expression)
                   && CompareExpressionList(a.Arguments, b.Arguments);
        }

        protected virtual bool CompareMemberInit(MemberInitExpression a, MemberInitExpression b)
        {
            return Compare(a.NewExpression, b.NewExpression)
                   && CompareBindingList(a.Bindings, b.Bindings);
        }

        protected virtual bool CompareBindingList(ReadOnlyCollection<MemberBinding> a, ReadOnlyCollection<MemberBinding> b)
        {
            if (a == b)
                return true;
            if (a == null || b == null)
                return false;
            if (a.Count != b.Count)
                return false;
            for (int i = 0, n = a.Count; i < n; i++)
            {
                if (!CompareBinding(a[i], b[i]))
                    return false;
            }
            return true;
        }

        protected virtual bool CompareBinding(MemberBinding a, MemberBinding b)
        {
            if (a == b)
                return true;
            if (a == null || b == null)
                return false;
            if (a.BindingType != b.BindingType)
                return false;
            if (a.Member != b.Member)
                return false;
            switch (a.BindingType)
            {
                case MemberBindingType.Assignment:
                    return CompareMemberAssignment((MemberAssignment)a, (MemberAssignment)b);
                case MemberBindingType.ListBinding:
                    return CompareMemberListBinding((MemberListBinding)a, (MemberListBinding)b);
                case MemberBindingType.MemberBinding:
                    return CompareMemberMemberBinding((MemberMemberBinding)a, (MemberMemberBinding)b);
                default:
                    throw new Exception(string.Format("Unhandled binding type: '{0}'", a.BindingType));
            }
        }

        protected virtual bool CompareMemberAssignment(MemberAssignment a, MemberAssignment b)
        {
            return a.Member == b.Member
                   && Compare(a.Expression, b.Expression);
        }

        protected virtual bool CompareMemberListBinding(MemberListBinding a, MemberListBinding b)
        {
            return a.Member == b.Member
                   && CompareElementInitList(a.Initializers, b.Initializers);
        }

        protected virtual bool CompareMemberMemberBinding(MemberMemberBinding a, MemberMemberBinding b)
        {
            return a.Member == b.Member
                   && CompareBindingList(a.Bindings, b.Bindings);
        }

        protected virtual bool CompareListInit(ListInitExpression a, ListInitExpression b)
        {
            return Compare(a.NewExpression, b.NewExpression)
                   && CompareElementInitList(a.Initializers, b.Initializers);
        }

        protected virtual bool CompareElementInitList(ReadOnlyCollection<ElementInit> a, ReadOnlyCollection<ElementInit> b)
        {
            if (a == b)
                return true;
            if (a == null || b == null)
                return false;
            if (a.Count != b.Count)
                return false;
            for (int i = 0, n = a.Count; i < n; i++)
            {
                if (!CompareElementInit(a[i], b[i]))
                    return false;
            }
            return true;
        }

        protected virtual bool CompareElementInit(ElementInit a, ElementInit b)
        {
            return a.AddMethod == b.AddMethod
                   && CompareExpressionList(a.Arguments, b.Arguments);
        }

        #region Nested type: ScopedDictionary

        public class ScopedDictionary<TKey, TValue>
        {
            private readonly Dictionary<TKey, TValue> map;
            private readonly ScopedDictionary<TKey, TValue> previous;

            public ScopedDictionary(ScopedDictionary<TKey, TValue> previous)
            {
                this.previous = previous;
                map = new Dictionary<TKey, TValue>();
            }

            public ScopedDictionary(ScopedDictionary<TKey, TValue> previous, IEnumerable<KeyValuePair<TKey, TValue>> pairs)
                : this(previous)
            {
                foreach (var p in pairs)
                {
                    map.Add(p.Key, p.Value);
                }
            }

            public void Add(TKey key, TValue value)
            {
                map.Add(key, value);
            }

            public bool TryGetValue(TKey key, out TValue value)
            {
                for (ScopedDictionary<TKey, TValue> scope = this; scope != null; scope = scope.previous)
                {
                    if (scope.map.TryGetValue(key, out value))
                        return true;
                }
                value = default(TValue);
                return false;
            }

            public bool ContainsKey(TKey key)
            {
                for (ScopedDictionary<TKey, TValue> scope = this; scope != null; scope = scope.previous)
                {
                    if (scope.map.ContainsKey(key))
                        return true;
                }
                return false;
            }
        }

        #endregion
    }
}