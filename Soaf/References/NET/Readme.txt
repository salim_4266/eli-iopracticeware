Interop.ADODB.dll

[Castle Windsor 3.2.1] { << Contains multi-threading fix (version >3.2.1 will contain it, so can be switched to NuGet)
Castle.Windsor.dll
}

[WPFSpark 1.1] { << Signed with Soaf key (but just needs to be strong named)
Microsoft.Expression.Interactions.dll
System.Windows.Interactivity.dll
WPFSpark.dll
}

[Protobuf .NET 2.0.0.627] { << Has a modified version of TypeModel.ResolveProxies() to support Castle's proxie. Also signed with Soaf key (for internalizing)
protobuf-net.dll
}

[Telerik ASP.NET AJAX] {
Telerik.Web.UI.dll
}

[Telerik WPF Controls] {
Telerik.Windows.Controls.dll
Telerik.Windows.Controls.Data.dll
Telerik.Windows.Controls.GridView.dll
Telerik.Windows.Controls.Input.dll
Telerik.Windows.Controls.Navigation.dll
Telerik.Windows.Data.dll
}