﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using Castle.Core;
using Castle.Core.Configuration;
using Castle.Core.Interceptor;
using Castle.Core.Internal;
using Castle.DynamicProxy;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel;
using Castle.MicroKernel.ComponentActivator;
using Castle.MicroKernel.Context;
using Castle.MicroKernel.ModelBuilder;
using Castle.MicroKernel.ModelBuilder.Descriptors;
using Castle.MicroKernel.Proxy;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Releasers;
using Castle.MicroKernel.Resolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.MicroKernel.SubSystems.Conversion;
using Castle.MicroKernel.SubSystems.Naming;
using Castle.MicroKernel.SubSystems.Resource;
using Castle.Windsor;
using Castle.Windsor.Diagnostics;
using Castle.Windsor.Installer;
using Castle.Windsor.Proxy;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Validation;
using ComponentRegistration = Soaf.ComponentModel.ComponentRegistration;
using IInitializable = Soaf.ComponentModel.IInitializable;
using IInterceptor = Castle.DynamicProxy.IInterceptor;
using IInvocation = Castle.DynamicProxy.IInvocation;
using LifestyleType = Soaf.ComponentModel.LifestyleType;

namespace Soaf.Castle
{
    using Collections;

    /// <summary>
    ///   A Castle Windsor based implementation of an IServiceProvider.
    /// </summary>
    public class WindsorServiceProvider : IServiceProvider, IInitializable
    {
        private readonly WindsorServiceProviderInternal windsorServiceProviderInternal;

        public WindsorServiceProvider(IComponentRegistry componentRegistry)
        {
            windsorServiceProviderInternal = new WindsorServiceProviderInternal(componentRegistry);
        }

        #region IInitializable Members

        public void Initialize()
        {
            windsorServiceProviderInternal.Initialize();
        }

        #endregion

        #region IServiceProvider Members

        [DebuggerNonUserCode]
        public object GetService(Type serviceType)
        {
            return windsorServiceProviderInternal.GetService(serviceType);
        }

        #endregion
    }

    internal class WindsorServiceProviderInternal : IServiceProvider, IInitializable
    {
        private readonly IComponentRegistry componentRegistry;
        private IWindsorContainer container;
        private bool isInitialized;

        public WindsorServiceProviderInternal(IComponentRegistry componentRegistry)
        {
            this.componentRegistry = componentRegistry;
        }

        #region IInitializable Members

        /// <summary>
        /// Initializes a new instance of the <see cref="WindsorServiceProvider"/> class.
        /// </summary>
        public void Initialize()
        {
            if (isInitialized) throw new InvalidOperationException("Already iniitialized.");
            isInitialized = true;
            using (new TimedScope(s => Debug.WriteLine("Initialized Windsor ServiceProvider in {0}.".FormatWith(s))))
            {
                container = new WindsorContainer(new DefaultKernel(), new DefaultComponentInstaller());
                container.Kernel.ProxyFactory = CreateProxyFactory();
                container.AddFacility<TypedFactoryFacility>();

                container.Register(Component.For<ILazyComponentLoader>().ImplementedBy<LazyOfTComponentLoader>().LifestyleSingleton());
                container.Register(Component.For<IServiceProvider>().Instance(this));
                container.Register(Component.For<DisposableComponentReleasingInterceptor>());

                container.Register(Component.For<ILazyComponentLoader>().Instance(new LazyComponentLoader(
                    container.Kernel,
                    t => CreateWindsorComponentRegistration(componentRegistry.GetComponentRegistration(t, null)))));

                container.Kernel.ComponentModelBuilder.AddContributor(new DependencyAttributePropertyConstructionContributer());

                container.AddFacility<ScopedLifestylesFacility>();

                // Register components in Windsor's container
                foreach (var registration in componentRegistry.ComponentRegistrations.OrderBy(i => i.Instance is IXmlConfiguration ? 0 : 1).ThenBy(i => i.Instance != null ? 0 : 1))
                {
                    var windsorRegistration = CreateWindsorComponentRegistration(registration);

                    if (windsorRegistration != null && !container.Kernel.HasComponent(registration.Name))
                        container.Register(windsorRegistration);
                }

                container.AddFacility<InitializeComponentFacility>();
            }
        }

        #endregion

        #region IServiceProvider Members

        [DebuggerNonUserCode]
        public object GetService(Type serviceType)
        {
            if (serviceType.Is<IProxyTargetAccessor>()) serviceType = serviceType.BaseType;

            return container.Resolve(serviceType);
        }

        #endregion

        /// <summary>
        /// Creates a custom proxy factory with our own module builders that support strong naming (since Castle's doesn't).
        /// </summary>
        /// <returns></returns>
        private static IProxyFactory CreateProxyFactory()
        {
            var scope = new ModuleScope();

            try
            {
                var moduleBuilder = CreateModuleBuilder();

                foreach (var f in new[] { "moduleBuilderWithStrongName", "moduleBuilder" }.Select(f => scope.GetType().GetField(f, BindingFlags.Instance | BindingFlags.NonPublic)))
                {
                    f.SetValue(scope, moduleBuilder);
                }
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }
            return new DefaultProxyFactory(new ProxyGenerator(new CustomProxyBuilder(scope)));
        }

        private static ModuleBuilder CreateModuleBuilder()
        {
            var assemblyName = new AssemblyName { Name = "DynamicProxyGenAssembly2" };
            assemblyName.SetPublicKey(Convert.FromBase64String(WellKnownAssemblies.SoafPublicKey));
            AssemblyBuilder assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            ModuleBuilder module = assembly.DefineDynamicModule("DynamicModule");
            return module;
        }

        /// <summary>
        ///   Creates a Windsor ComponentRegistration for the supplied Soaf ComponentRegistration.
        /// </summary>
        /// <param name = "componentRegistration">The component registration.</param>
        /// <returns></returns>
        private ComponentRegistration<object> CreateWindsorComponentRegistration(ComponentRegistration componentRegistration)
        {
            if (componentRegistration == null) return null;

            List<IConcernDescriptor> concernDescriptors = componentRegistration.Concerns.ToList();
            // TODO: once we don't need to defer evaluation of concerns, move visiting to a more proper location?
            concernDescriptors.ForEach(c => c.Visit(componentRegistration));
            // Concerns could be have been modified, so get them again
            concernDescriptors = componentRegistration.Concerns.ToList();

            ComponentRegistration<object> windsorComponent;

            if (componentRegistration.Instance != null)
            {
                windsorComponent = Component.For(componentRegistration.For.FirstOrDefault() ?? componentRegistration.Type ?? componentRegistration.Instance.GetType()).Instance(componentRegistration.Instance);
            }
            else
            {
                windsorComponent = Component.For(componentRegistration.For);

                if (componentRegistration.Type != null)
                {
                    if (componentRegistration.Type.IsAbstract)
                    {
                        windsorComponent = windsorComponent.Forward(componentRegistration.Type);
                    }
                    else
                    {
                        windsorComponent = windsorComponent.ImplementedBy(componentRegistration.Type);
                    }
                }
            }

            windsorComponent.Named(componentRegistration.Name);

            List<Type> componentTypes = componentRegistration.For.Concat(new[] { componentRegistration.Type }).WhereNotDefault().Distinct().ToList();

            int i = 0;
            var interceptorTypes = new HashSet<Type>();

            HashSet<Type> allAdditionalInterfaces = new HashSet<Type>();

            while (i < concernDescriptors.Count)
            {
                IConcernDescriptor concernDescriptor = concernDescriptors[i];

                if (concernDescriptor.ConcernType.Is<ITypeInterfacesConcern>())
                {
                    Type[] additionalInterfaces = container.Resolve(concernDescriptor.ConcernType).CastTo<ITypeInterfacesConcern>().Interfaces.Where(t => !componentTypes.Any(ct => ct.Is(t))).ToArray();
                    windsorComponent.Proxy.AdditionalInterfaces(additionalInterfaces);
                    componentTypes.AddRange(additionalInterfaces);
                    concernDescriptors.AddDistinct(additionalInterfaces.SelectMany(t => componentRegistry.GetComponentRegistration(t, null).IfNotNull(c => c.Concerns)).WhereNotDefault());
                    additionalInterfaces.ForEach(t => allAdditionalInterfaces.Add(t));
                }

                if (concernDescriptor.ConcernType.Is<ComponentModel.IInterceptor>())
                {
                    interceptorTypes.Add(concernDescriptor.ConcernType);

                    Type interceptorAdapterType = typeof(InterceptorAdapter<>).MakeGenericType(concernDescriptor.ConcernType);
                    windsorComponent.AddDescriptor(
                        new InterceptorDescriptor(new[]
                        {
                            new InterceptorReference(interceptorAdapterType)
                        }));

                    if (container.Kernel.GetHandler(interceptorAdapterType) == null)
                    {
                        container.Register(Component.For(interceptorAdapterType).LifeStyle.Transient);
                    }
                }

                i++;
            }

            if (componentTypes.Any(t => t.Is<IDisposable>()))
            {
                windsorComponent = windsorComponent.Interceptors<DisposableComponentReleasingInterceptor>();
            }

            // Save evaluated concern descriptors for later access in invocation
            windsorComponent.ExtendedProperties(Property.ForKey("appliedConcernDescriptors").Eq(concernDescriptors.ToArray()));

            // Save a list of concern types for ComponentActivatorWrapper
            windsorComponent.ExtendedProperties(Property.ForKey(typeof(IInstanceConcern)).Eq(concernDescriptors.Where(c => c.ConcernType.Is<IInstanceConcern>() && !c.ConcernType.Is<ComponentModel.IInterceptor>()).Select(c => c.ConcernType).ToArray()));

            windsorComponent.ExtendedProperties(Property.ForKey(typeof(ILifestyleDescriptor)).Eq(componentRegistration.LifestyleDescriptor));

            switch (componentRegistration.LifestyleDescriptor.LifestyleType)
            {
                case LifestyleType.Singleton:
                    windsorComponent = windsorComponent.LifeStyle.Singleton;
                    break;
                case LifestyleType.CallContext:
                    windsorComponent = windsorComponent.LifeStyle.Custom(typeof(ScopedLifestylesFacility.ReleasingCallContextLifestyleManager));
                    break;
                case LifestyleType.Thread:
                    windsorComponent = windsorComponent.LifeStyle.PerThread;
                    break;
                case LifestyleType.Transient:
                    windsorComponent = windsorComponent.LifeStyle.Transient;
                    break;
                case LifestyleType.Scoped:
                    windsorComponent = windsorComponent.LifeStyle.Scoped();
                    break;
                case LifestyleType.CreationContext:
                    windsorComponent = windsorComponent.LifeStyle.BoundTo(h => h.First());
                    break;
            }

            if (componentRegistration.Initialize)
            {
                windsorComponent.ExtendedProperties(new[] { Property.ForKey("Initialize").Eq(true) });
            }

            if (componentRegistration.IsFactory) windsorComponent.AsFactory();
            else if (componentTypes.All(t => t.IsAbstract) && interceptorTypes.Count == 0) return null;

            windsorComponent = windsorComponent.PropertiesIgnore(p => !p.HasAttribute<DependencyAttribute>(true));

            // allow interceptors to filter which methods they want to intercept

            var interceptedMethodsFilterTypes = interceptorTypes
                // get all IInterceptedMethodsFilters from interceptors that implement the interface directory
                .Where(t => t.Is<IInterceptedMethodsFilter>())
                .Select(t => new { InterceptorType = t, FilterType = t })
                .Concat(interceptorTypes
                // get IInterceptedMethodsFilters from attributes
                    .SelectMany(t => t.GetAttributes<InterceptedMethodsFilterAttribute>()
                        .Select(a => new { InterceptorType = t, FilterType = a.Type })))
                .GroupBy(x => x.InterceptorType, x => x.FilterType).ToDictionary(x => x.Key, x => x.ToArray());

            // Do we have interceptors which do not specify a filter?
            var interceptorsWithoutFilter = interceptorTypes
                .Where(t => interceptedMethodsFilterTypes.All(ft => ft.Key != t))
                .ToList();

            // ... then add filter which intercepts all methods 
            foreach (var interceptorType in interceptorsWithoutFilter)
            {
                interceptedMethodsFilterTypes.Add(interceptorType, new[] { typeof(InterceptAllMethodsFilter) });
            }

            if (interceptedMethodsFilterTypes.Values.SelectMany(v => v).Any(t => t.GetConstructor(Type.EmptyTypes) == null)) throw new InvalidOperationException("IInterceptedMethodsFilters must have default constructors.");
            if (interceptedMethodsFilterTypes.Count > 0)
            {
                var filters = interceptedMethodsFilterTypes.ToDictionary(x => x.Key, x => x.Value.Select(t => (IInterceptedMethodsFilter)t.CreateInstance()).ToArray());

                windsorComponent.ExtendedProperties(Property.ForKey("interceptedMethodsFilters").Eq(filters));

                windsorComponent.Proxy.Hook(new ProxyGenerationHook((t, m) => filters.SelectMany(f => f.Value).Any(f => f.ShouldIntercept(t, m)), allAdditionalInterfaces));
            }

            return windsorComponent;
        }

        private static bool CanInvocationProceed(IInterceptor interceptor, IInvocation invocation)
        {
            if (invocation.MethodInvocationTarget != null && !invocation.MethodInvocationTarget.Attributes.HasFlag(MethodAttributes.Abstract)) return true;
            var accessor = invocation.Proxy as IProxyTargetAccessor;
            if (accessor == null) return true;
            return accessor.GetInterceptors().LastOrDefault() != interceptor;
        }

        /// <summary>
        /// A filtering and validating IProxyGenerationHook.
        /// </summary>
        [SerializableAttribute]
        public class ProxyGenerationHook : IProxyGenerationHook
        {
            [NonSerialized]
            private readonly Func<Type, MethodInfo, bool> filter;

            private readonly HashSet<Type> additionalInterfaces;

            public ProxyGenerationHook(Func<Type, MethodInfo, bool> filter, IEnumerable<Type> additionalInterfaces)
            {
                this.filter = filter;
                this.additionalInterfaces = new HashSet<Type>(additionalInterfaces);
            }

            public void MethodsInspected()
            {
            }

            public void NonProxyableMemberNotification(Type type, MemberInfo memberInfo)
            {
                var methodInfo = memberInfo as MethodInfo;
                if (methodInfo != null && ShouldInterceptMethod(type, methodInfo) && !methodInfo.CanBeOveridden())
                    throw new InvalidOperationException("Only virtual public and protected methods or methods that are on interface based components can be intercepted.");
            }

            public bool ShouldInterceptMethod(Type type, MethodInfo methodInfo)
            {
                var result = MustBeOverriden(methodInfo) || filter(type, methodInfo);
                return result;
            }

            private bool MustBeOverriden(MethodInfo methodInfo)
            {
                // if a dynamically intercepted interface, method must be overridden, so ignore filter
                return methodInfo.IsAbstract || (methodInfo.DeclaringType != null && methodInfo.DeclaringType.IsInterface && additionalInterfaces.Any(i => i.Is(methodInfo.DeclaringType)));
            }

            public override bool Equals(object obj)
            {
                return ((obj != null) && (obj.GetType() == GetType()));
            }

            public override int GetHashCode()
            {
                return GetType().GetHashCode();
            }
        }

        #region Nested type: DefaultDependencyResolver

        private class DefaultDependencyResolver : global::Castle.MicroKernel.Resolvers.DefaultDependencyResolver, IDependencyResolver
        {
            private IKernelInternal kernel;

            private readonly IDictionary<DependencyModel, bool> canResolveDependencyCache = new Dictionary<DependencyModel, bool>().Synchronized();

            #region IDependencyResolver Members

            public new void Initialize(IKernelInternal kernel, DependencyDelegate dependencyDelegate)
            {
                base.Initialize(kernel, dependencyDelegate);
                this.kernel = kernel;
                kernel.ComponentCreated += OnKernelComponentCreated;
            }

            private void OnKernelComponentCreated(global::Castle.Core.ComponentModel model, object instance)
            {
                canResolveDependencyCache.Clear();
            }

            #endregion

            [DebuggerNonUserCode]
            protected override bool CanResolveFromKernel(CreationContext context, global::Castle.Core.ComponentModel model, DependencyModel dependency)
            {
                bool hasOtherBetterConstructorOrIsOptionalAndIsNotRegistered;
                if (!canResolveDependencyCache.TryGetValue(dependency, out hasOtherBetterConstructorOrIsOptionalAndIsNotRegistered))
                {
                    canResolveDependencyCache[dependency] = (dependency.IsOptional || (dependency is ConstructorDependencyModel && model.Constructors.Any(c => c.Dependencies.All(d => kernel.HasComponent(d.TargetType)))))
                                                            && !kernel.HasComponent(dependency.TargetItemType);
                }

                if (hasOtherBetterConstructorOrIsOptionalAndIsNotRegistered)
                {
                    return false;
                }

                return dependency.HasDefaultValue || base.CanResolveFromKernel(context, model, dependency);
            }

            [DebuggerNonUserCode]
            protected override object ResolveFromKernel(CreationContext context, global::Castle.Core.ComponentModel model, DependencyModel dependency)
            {
                try
                {
                    return base.ResolveFromKernel(context, model, dependency);
                }
                catch (Exception ex)
                {
                    if (dependency.HasDefaultValue)
                    {
                        return dependency.DefaultValue;
                    }
                    throw new ResolutionException(ex);
                }
            }

            protected override CreationContext RebuildContextForParameter(CreationContext current, Type parameterType)
            {
                var result = base.RebuildContextForParameter(current, parameterType);
                if (!result.HasAdditionalArguments && current.HasAdditionalArguments)
                {
                    foreach (var key in current.AdditionalArguments.Keys) result.AdditionalArguments.Add(key, current.AdditionalArguments[key]);
                }
                return result;
            }
        }

        #endregion

        private class NonPublicPropertiesDefaultComponentActivator : DefaultComponentActivator
        {
            private readonly ConcurrentDictionary<PropertySet, Type> staticResolutions;

            public NonPublicPropertiesDefaultComponentActivator(global::Castle.Core.ComponentModel model, IKernelInternal kernel, ComponentInstanceDelegate onCreation, ComponentInstanceDelegate onDestruction)
                : base(model, kernel, onCreation, onDestruction)
            {
                var propertiesForStaticResolution = model.Properties
                    // Important assumption: there is no collection type in Soaf with requires AOP aspects
                    // Otherwise -> exclude here
                    .Where(p => p.Property.PropertyType.IsGenericTypeFor(typeof (IList<>))
                        || p.Property.PropertyType.IsGenericTypeFor(typeof (ICollection<>)))
                    .ToDictionary(p => p, p => (Type)null);
                staticResolutions = new ConcurrentDictionary<PropertySet, Type>(propertiesForStaticResolution);
            }

            protected override void SetUpProperties(object instance, CreationContext context)
            {
                instance = ProxyUtil.GetUnproxiedInstance(instance);
                IDependencyResolver resolver = Kernel.Resolver;
                foreach (PropertySet property in Model.Properties)
                {
                    object obj;
                    Type propertyImplementationType;
                    if (staticResolutions.TryGetValue(property, out propertyImplementationType))
                    {
                        if (propertyImplementationType != null)
                        {
                            obj = propertyImplementationType.CreateInstance();
                        }
                        else
                        {
                            obj = ObtainPropertyValue(context, property, resolver);
                            var objType = obj.GetType();
                            staticResolutions.AddOrUpdate(property, objType, (k,v) => objType);
                        }
                    }
                    else
                    {
                        obj = ObtainPropertyValue(context, property, resolver);
                    }

                    if (obj != null)
                    {
                        var setMethod = property.Property.GetSetMethod(true).GetInvoker();
                        try
                        {
                            setMethod(instance, obj);
                        }
                        catch (Exception ex)
                        {
                            throw new ComponentActivatorException(string.Format("Error setting property {1}.{0} in component {2}. See inner exception for more information. If you don't want Windsor to set this property you can do it by either decorating it with {3} or via registration API.", (object)property.Property.Name, (object)instance.GetType().Name, Model.Name, (object)typeof(DoNotWireAttribute).Name), ex, Model);
                        }
                    }
                }
            }


            private object ObtainPropertyValue(CreationContext context, PropertySet property, IDependencyResolver resolver)
            {
                if (property.Dependency.IsOptional)
                {
                    if (!resolver.CanResolve(context, context.Handler, Model, property.Dependency))
                        return null;
                }
                try
                {
                    return resolver.Resolve(context, context.Handler, Model, property.Dependency);
                }
                catch
                {
                    if (!property.Dependency.IsOptional)
                        throw;
                }
                return null;
            }
        }

        #region Nested type: DefaultKernel


        private class DefaultKernel : global::Castle.MicroKernel.DefaultKernel, IKernelInternal
        {
            public DefaultKernel()
                : base(new DefaultDependencyResolver(), new NotSupportedProxyFactory())
            {
                ReleasePolicy = new DefaultReleasePolicy(this);
            }

            object IKernelInternal.Resolve(string key, Type service, IDictionary arguments, IReleasePolicy policy)
            {
                var handler = (this as IKernelInternal).LoadHandlerByName(key, service, arguments);
                if (handler == null)
                {
                    return ((IKernelInternal)this).Resolve(service, arguments, policy);
                }
                return ResolveComponent(handler, service ?? typeof(object), arguments, policy);
            }

            public override IComponentActivator CreateComponentActivator(global::Castle.Core.ComponentModel model)
            {
                if (model == null)
                    throw new ArgumentNullException("model");
                if (model.CustomComponentActivator == null)
                    return new NonPublicPropertiesDefaultComponentActivator(model, this, RaiseComponentCreated, RaiseComponentDestroyed);
                try
                {
                    return ReflectionUtil.CreateInstance<IComponentActivator>(model.CustomComponentActivator, (object)model, (object)this, (object)new ComponentInstanceDelegate(RaiseComponentCreated), (object)new ComponentInstanceDelegate(RaiseComponentDestroyed));
                }
                catch (Exception ex)
                {
                    throw new KernelException("Could not instantiate custom activator", ex);
                }
            }

            protected override void RegisterSubSystems()
            {
                AddSubSystem(SubSystemConstants.ConfigurationStoreKey, new DefaultConfigurationStore());
                AddSubSystem(SubSystemConstants.ConversionManagerKey, new DefaultConversionManager());
                AddSubSystem(SubSystemConstants.NamingKey, new DefaultNamingSubSystem());
                AddSubSystem(SubSystemConstants.ResourceKey, new DefaultResourceSubSystem());
                AddSubSystem(SubSystemConstants.DiagnosticsKey, new NullDiagnosticsSubSystem());
            }

            public override ILifestyleManager CreateLifestyleManager(global::Castle.Core.ComponentModel model, IComponentActivator activator)
            {
                if (activator != null) activator = new ComponentActivatorWrapper(this, activator);

                return base.CreateLifestyleManager(model, activator);
            }

        }

        #endregion

        /// <summary>
        /// Wraps an IComponentActivator, allowing for IInstanceConcerns to be applied.
        /// </summary>
        private class ComponentActivatorWrapper : IComponentActivator
        {
            private readonly IKernel kernel;
            private readonly IComponentActivator innerActivator;

            public ComponentActivatorWrapper(IKernel kernel, IComponentActivator innerActivator)
            {
                this.kernel = kernel;
                this.innerActivator = innerActivator;
            }

            public object Create(CreationContext context, Burden burden)
            {
                var instance = innerActivator.Create(context, burden);

                var instanceToApplyOn = instance;
                if (instanceToApplyOn is IProxyTargetAccessor)
                {
                    instanceToApplyOn = ((IProxyTargetAccessor)instanceToApplyOn).DynProxyGetTarget()
                                        ?? instance; // Proxy target can be null if component doesn't provide an implementation, so just use instance itself
                }

                var accessor = instance as IProxyTargetAccessor;
                if (accessor != null)
                {
                    accessor.GetInterceptors().OfType<IInterceptorAdapter>().Select(ii => ii.Interceptor).OfType<IInstanceConcern>().ForEach(c => c.ApplyConcern(instanceToApplyOn));
                }

                var instanceConcerns = burden.Model.ExtendedProperties[typeof(IInstanceConcern)] as Type[];
                if (instanceConcerns != null)
                {
                    instanceConcerns.Select(c => kernel.Resolve(c)).OfType<IInstanceConcern>().ForEach(c => c.ApplyConcern(instanceToApplyOn));
                }

                return instance;
            }

            public void Destroy(object instance)
            {
                innerActivator.Destroy(instance);
            }
        }

        private class NullDiagnosticsSubSystem : DefaultDiagnosticsSubSystem
        {
            protected override void InitStandardExtensions()
            {

            }
        }

        #region Nested type: DefaultReleasePolicy

        private class DefaultReleasePolicy : IReleasePolicy
        {
            private readonly IKernel kernel;
            private readonly LifecycledComponentsReleasePolicy innerReleasePolicy;
            private readonly LifestyleType[] noTrackingLifestyleTypes;

            public DefaultReleasePolicy(IKernel kernel)
            {
                this.kernel = kernel;
                noTrackingLifestyleTypes = new[] { LifestyleType.Transient, LifestyleType.Scoped };

                innerReleasePolicy = new LifecycledComponentsReleasePolicy(kernel);
            }

            public void Dispose()
            {
                innerReleasePolicy.Dispose();
            }

            public IReleasePolicy CreateSubPolicy()
            {
                return new DefaultReleasePolicy(kernel);
            }

            public bool HasTrack(object instance)
            {
                return innerReleasePolicy.HasTrack(instance);
            }

            public void Release(object instance)
            {
                innerReleasePolicy.Release(instance);
            }

            public void Track(object instance, Burden burden)
            {
                var descriptor = burden.Model.ExtendedProperties[typeof(ILifestyleDescriptor)] as ILifestyleDescriptor;
                
                if ((descriptor != null && noTrackingLifestyleTypes.Contains(descriptor.LifestyleType))
                    // Castle wants to track all IDisposable and Func<T>() transient instances for disposal, 
                    // but that just ends up with memory leak in our case
                    || burden.Model.LifestyleType == global::Castle.Core.LifestyleType.Transient)
                {
                    return;
                }
                innerReleasePolicy.Track(instance, burden);
            }
        }

        #endregion

        #region Nested type: DependencyAttributePropertyConstructionContributer

        /// <summary>
        ///   Enforces property injection requirement on properties annotated with DependencyAttribute.
        /// </summary>
        private class DependencyAttributePropertyConstructionContributer : IContributeComponentModelConstruction
        {
            #region IContributeComponentModelConstruction Members

            public void ProcessModel(IKernel kernel, global::Castle.Core.ComponentModel model)
            {
                foreach (PropertySet dependencyProperty in model.Properties.Where(p => p.Property.GetAttribute<DependencyAttribute>(true).IfNotNull(a => !a.IsOptional)))
                {
                    dependencyProperty.Dependency.IsOptional = false;
                }

                new[] { model.Implementation }.Concat(model.Services).SelectMany(s => s.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic))
                    .Where(x => x.HasAttribute<DependencyAttribute>())
                    .Distinct()
                    .ForEach(x => model.AddProperty(new PropertySet(x, new DependencyModel(x.Name, x.PropertyType, x.GetAttribute<DependencyAttribute>(true).IsOptional))));
            }

            #endregion
        }

        #endregion

        #region Nested type: DisposableComponentReleasingInterceptor

        /// <summary>
        ///   Releases components automatically when they are disposed.
        /// </summary>
        [UsedImplicitly]
        private class DisposableComponentReleasingInterceptor : IInterceptor, IOnBehalfAware
        {
            private static readonly MethodInfo DefaultDisposeMethod = typeof(IDisposable).GetMethods().Single();
            private readonly IKernel kernel;
            private MethodInfo disposeMethod = DefaultDisposeMethod;
            private bool isDisposing;

            public DisposableComponentReleasingInterceptor(IKernel kernel)
            {
                this.kernel = kernel;
            }

            #region IInterceptor Members

            public void Intercept(IInvocation invocation)
            {
                if (invocation.Method == disposeMethod && !isDisposing)
                {
                    isDisposing = true;
                    kernel.ReleaseComponent(invocation.Proxy);
                    IHandler handler = kernel.GetHandler(invocation.TargetType);
                    if (handler.ComponentModel.LifestyleType == global::Castle.Core.LifestyleType.Singleton)
                    {
                        handler.CastTo<IDisposable>().Dispose();
                    }

                    if (!kernel.ReleasePolicy.HasTrack(invocation.Proxy)) invocation.Proceed();

                    isDisposing = false;
                }
                else if (CanInvocationProceed(this, invocation))
                {
                    invocation.Proceed();
                }
            }

            #endregion

            #region IOnBehalfAware Members

            public void SetInterceptedComponentModel(global::Castle.Core.ComponentModel target)
            {
                Type disposableService = target.Services.FirstOrDefault(s => s.Is<IDisposable>()) ?? typeof(IDisposable);
                if (disposableService.IsClass)
                {
                    disposeMethod = disposableService.GetInterfaceMap(typeof(IDisposable)).TargetMethods.Single();
                }
            }

            #endregion
        }

        #endregion

        #region Nested type: IInterceptorAdapter

        private interface IInterceptorAdapter
        {
            ComponentModel.IInterceptor Interceptor { get; }
        }

        #endregion

        #region Nested type: InitializeComponentFacility

        /// <summary>
        /// Initializes applicable components on registration.
        /// </summary>
        internal class InitializeComponentFacility : IFacility
        {
            private IKernel kernel;

            #region IFacility Members

            public void Init(IKernel kernel, IConfiguration facilityConfig)
            {
                this.kernel = kernel;

                kernel.GetAssignableHandlers(typeof(object))
                    .Where(handler => handler.ComponentModel.ExtendedProperties["Initialize"] is bool && (bool)handler.ComponentModel.ExtendedProperties["initialize"])
                    .ToList().ForEach(h => Initialize(h.ComponentModel.Name));

                kernel.ComponentRegistered += OnComponentRegistered;
            }

            public void Terminate()
            {
            }

            #endregion

            private void Initialize(string name)
            {
                try
                {
                    using (new TimedScope(s => Debug.WriteLine("Initialized component {0} in {1}.".FormatWith(name, s))))
                    {
                        kernel.Resolve(name, typeof(object));
                    }
                }
                catch (Exception ex)
                {
                    if (!(ex is System.IO.FileNotFoundException) && !(ex.InnerException is System.IO.FileNotFoundException)) throw;
                }
            }

            private void OnComponentRegistered(string key, IHandler handler)
            {
                object initialize = handler.ComponentModel.ExtendedProperties["initialize"];
                if (initialize is bool && (bool)initialize)
                {
                    Initialize(handler.ComponentModel.Name);
                }
            }
        }

        #endregion


        #region Nested type: InterceptorAdapter

        /// <summary>
        ///   An adapter from a Castle IInterceptor to a IInterceptor.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        private class InterceptorAdapter<T> : IInterceptorAdapter, IOnBehalfAware, IInterceptor where T : ComponentModel.IInterceptor
        {
            private readonly T interceptor;

            /// <summary>
            /// InterceptorAdapter for a concrete behavior of type T uses same set of filters.
            /// Using following to cache filter evaluation results for every method to improve performance
            /// </summary>
            private static bool hasInitialized;

            private static readonly object SyncRoot = new object();
            private static IInterceptedMethodsFilter[] filters;
            private static readonly IDictionary<MethodInfo, bool> MethodsToIntercept = new Dictionary<MethodInfo, bool>().Synchronized();
            private static readonly IDictionary<MethodInfo, bool> MethodsMustIntercept = new Dictionary<MethodInfo, bool>().Synchronized();

            /// <summary>
            /// Concern descriptors differ by actual type the behavior is applied to, so not caching statically
            /// </summary>
            private IConcernDescriptor[] appliedConcernDescriptors;

            public InterceptorAdapter(T interceptor)
            {
                this.interceptor = interceptor;
            }

            #region IInterceptor Members

            [DebuggerNonUserCode]
            public void Intercept(IInvocation invocation)
            {
                bool shouldIntercept = true;
                bool mustIntercept;
                var method = invocation.Method ?? invocation.MethodInvocationTarget ?? invocation.GetConcreteMethod();

                if (!MethodsMustIntercept.TryGetValue(method, out mustIntercept))
                {
                    MethodsMustIntercept[method] = mustIntercept = method.DeclaringType.EnsureNotDefault().IsInterface && interceptor.As<ITypeInterfacesConcern>().IfNotNull(c => c.Interfaces.Any(i => i.Is(method.DeclaringType)));
                }

                if (!mustIntercept && filters != null && !MethodsToIntercept.TryGetValue(method, out shouldIntercept))
                {
                    MethodsToIntercept[invocation.Method] = shouldIntercept = filters.Any(f => f.ShouldIntercept(method.DeclaringType, method));
                }

                shouldIntercept = shouldIntercept && SuppressInterceptionScope.Current == null;

                if (shouldIntercept || mustIntercept)
                    interceptor.Intercept(new InvocationAdapter(invocation, () => CanInvocationProceed(this, invocation), appliedConcernDescriptors));
                else if (CanInvocationProceed(this, invocation))
                    invocation.Proceed();
            }

            #endregion

            #region IInterceptorAdapter Members

            public ComponentModel.IInterceptor Interceptor
            {
                get { return interceptor; }
            }

            #endregion

            #region IOnBehalfAware Members

            public void SetInterceptedComponentModel(global::Castle.Core.ComponentModel target)
            {
                appliedConcernDescriptors = (IConcernDescriptor[])target.ExtendedProperties["appliedConcernDescriptors"];

                lock (SyncRoot)
                {
                    if (hasInitialized) return;
                    hasInitialized = true;

                    filters = ((Dictionary<Type, IInterceptedMethodsFilter[]>)target.ExtendedProperties["interceptedMethodsFilters"]).IfNotNull(f => f.GetValue(typeof(T)));
                }
            }

            #endregion


            #region Nested type: InvocationAdapter

            /// <summary>
            ///   An adapter from a Castle IInvocation to an IInvocation.
            /// </summary>
            private class InvocationAdapter : ComponentModel.IInvocation
            {
                private readonly IInvocation invocation;
                private readonly Func<bool> canProceed;
                private MethodInfo method;

                [DebuggerNonUserCode]
                public InvocationAdapter(IInvocation invocation, Func<bool> canProceed, IConcernDescriptor[] concernDescriptors)
                {
                    this.invocation = invocation;
                    this.canProceed = canProceed;

                    ConcernDescriptors = concernDescriptors;
                }

                #region IInvocation Members

                [DebuggerNonUserCode]
                public void Proceed()
                {
                    invocation.Proceed();
                }

                public bool CanProceed
                {
                    get { return canProceed(); }
                }

                public object[] Arguments
                {
                    get { return invocation.Arguments; }
                }

                public object Target
                {
                    get { return invocation.Proxy; }
                }

                public Type TargetType
                {
                    get { return invocation.TargetType ?? Method.DeclaringType; }
                }

                public IConcernDescriptor[] ConcernDescriptors { get; private set; }

                public MethodInfo Method
                {
                    get { return method ?? (method = invocation.MethodInvocationTarget ?? invocation.GetConcreteMethod()); }
                }

                public object ReturnValue
                {
                    get { return invocation.ReturnValue; }
                    set { invocation.ReturnValue = value; }
                }

                #endregion
            }

            #endregion
        }

        #endregion

        #region Nested type: LazyComponentLoader

        /// <summary>
        ///   A lazy component loader for automatically registering late resolved components.
        /// </summary>
        private class LazyComponentLoader : ILazyComponentLoader
        {
            private readonly IKernel kernel;
            private readonly Func<Type, ComponentRegistration<object>> createComponent;

            public LazyComponentLoader(IKernel kernel, Func<Type, ComponentRegistration<object>> createComponent)
            {
                this.kernel = kernel;
                this.createComponent = createComponent;
            }

            #region ILazyComponentLoader Members

            public IRegistration Load(string key, Type service, IDictionary arguments)
            {
                ComponentRegistration<object> component;

                if (IsUnhandledType(service) || kernel.GetHandler(service) != null)
                    return null;

                if (service.EqualsGenericTypeFor(typeof(InterceptorAdapter<>)))
                {
                    component = Component.For(service).LifeStyle.Transient;
                }
                else
                {
                    component = createComponent(service);
                }

                return component;
            }

            private bool IsUnhandledType(Type service)
            {
                return service.IsPrimitive || service.IsByRef || service == typeof(string) || service.IsGenericTypeFor(typeof(Lazy<>));
            }

            #endregion
        }

        #endregion


    }


}
