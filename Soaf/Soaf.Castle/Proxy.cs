﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using Castle.Core.Logging;
using Castle.DynamicProxy;
using Castle.DynamicProxy.Contributors;
using Castle.DynamicProxy.Generators;
using Castle.DynamicProxy.Generators.Emitters;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Castle.DynamicProxy.Internal;
using Soaf.Reflection;

namespace Soaf.Castle
{

    /// <summary>
    ///   Default implementation of <see cref = "IProxyBuilder" /> interface producing in-memory proxy assemblies.
    /// </summary>
    public class CustomProxyBuilder : IProxyBuilder
    {
        private readonly ModuleScope scope;
        private ILogger logger = NullLogger.Instance;

        /// <summary>
        ///   Initializes a new instance of the <see>
        ///                                         <cref>Castle.DynamicProxy.DefaultProxyBuilder</cref>
        ///                                     </see>
        ///     class with new <see cref = "ModuleScope" />.
        /// </summary>
        public CustomProxyBuilder()
            : this(new ModuleScope())
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see>
        ///                                         <cref>Castle.DynamicProxy.DefaultProxyBuilder</cref>
        ///                                     </see>
        ///     class.
        /// </summary>
        /// <param name = "scope">The module scope for generated proxy types.</param>
        public CustomProxyBuilder(ModuleScope scope)
        {
            this.scope = scope;
        }

        public ILogger Logger
        {
            get { return logger; }
            set { logger = value; }
        }

        public ModuleScope ModuleScope
        {
            get { return scope; }
        }

        public Type CreateClassProxyType(Type classToProxy, Type[] additionalInterfacesToProxy, ProxyGenerationOptions options)
        {
            AssertValidType(classToProxy);
            AssertValidTypes(additionalInterfacesToProxy);

            var generator = new ClassProxyGenerator(scope, classToProxy) { Logger = logger };
            return generator.GenerateCode(additionalInterfacesToProxy, options);
        }

        public Type CreateClassProxyTypeWithTarget(Type classToProxy, Type[] additionalInterfacesToProxy,
                                                   ProxyGenerationOptions options)
        {
            AssertValidType(classToProxy);
            AssertValidTypes(additionalInterfacesToProxy);
            var generator = new ClassProxyWithTargetGenerator(scope, classToProxy, additionalInterfacesToProxy, options) { Logger = logger };
            return generator.GetGeneratedType();
        }

        public Type CreateInterfaceProxyTypeWithTarget(Type interfaceToProxy, Type[] additionalInterfacesToProxy,
                                                       Type targetType,
                                                       ProxyGenerationOptions options)
        {
            AssertValidType(interfaceToProxy);
            AssertValidTypes(additionalInterfacesToProxy);

            var generator = new InterfaceProxyWithTargetGenerator(scope, interfaceToProxy) { Logger = logger };
            return generator.GenerateCode(targetType, additionalInterfacesToProxy, options);
        }

        public Type CreateInterfaceProxyTypeWithTargetInterface(Type interfaceToProxy, Type[] additionalInterfacesToProxy,
                                                                ProxyGenerationOptions options)
        {
            AssertValidType(interfaceToProxy);
            AssertValidTypes(additionalInterfacesToProxy);

            var generator = new InterfaceProxyWithTargetInterfaceGenerator(scope, interfaceToProxy) { Logger = logger };
            return generator.GenerateCode(interfaceToProxy, additionalInterfacesToProxy, options);
        }

        public Type CreateInterfaceProxyTypeWithoutTarget(Type interfaceToProxy, Type[] additionalInterfacesToProxy,
                                                          ProxyGenerationOptions options)
        {
            AssertValidType(interfaceToProxy);
            AssertValidTypes(additionalInterfacesToProxy);

            var generator = new InterfaceProxyWithoutTargetGenerator(scope, interfaceToProxy) { Logger = logger };
            return generator.GenerateCode(typeof(object), additionalInterfacesToProxy, options);
        }

        private void AssertValidType(Type target)
        {
            if (target.IsGenericTypeDefinition)
            {
                throw new GeneratorException("Type " + target.FullName + " is a generic type definition. " +
                                             "Can not create proxy for open generic types.");
            }
            if (IsPublic(target) == false && IsAccessible(target) == false)
            {
                throw new GeneratorException("Type " + target.FullName + " is not visible to DynamicProxy. " +
                                             "Can not create proxy for types that are not accessible. " +
                                             "Make the type public, or internal and mark your assembly with " +
                                             "[assembly: InternalsVisibleTo(InternalsVisible.ToDynamicProxyGenAssembly2)] attribute.");
            }
        }

        private void AssertValidTypes(IEnumerable<Type> targetTypes)
        {
            if (targetTypes != null)
            {
                foreach (var t in targetTypes)
                {
                    AssertValidType(t);
                }
            }
        }

        private bool IsAccessible(Type target)
        {
            return IsInternal(target) && target.Assembly.IsInternalToDynamicProxy();
        }

        private bool IsPublic(Type target)
        {
            return target.IsPublic || target.IsNestedPublic;
        }

        private static bool IsInternal(Type target)
        {
            var isTargetNested = target.IsNested;
            var isNestedAndInternal = isTargetNested && (target.IsNestedAssembly || target.IsNestedFamORAssem);
            var isInternalNotNested = target.IsVisible == false && isTargetNested == false;

            return isInternalNotNested || isNestedAndInternal;
        }
    }

    public class BaseProxyGenerator : global::Castle.DynamicProxy.Generators.BaseProxyGenerator
    {
        public static readonly List<Func<Type, ITypeContributor[]>> ContributorSelectors = new List<Func<Type, ITypeContributor[]>>();

        static BaseProxyGenerator()
        {
            ContributorSelectors.Add(t => new[] {new DefaultGetterTypeContributor()});
        }

        public BaseProxyGenerator(ModuleScope scope, Type targetType)
            : base(scope, targetType)
        {
        }

        protected IEnumerable<ITypeContributor> AddAdditionalContributors(Type type, IList<ITypeContributor> contributors)
        {
            var additionalContributors = ContributorSelectors.SelectMany(s => s(type)).ToList();

            if (additionalContributors.Count > 0)
            {
                additionalContributors.ForEach(contributors.Add);
            }

            return contributors;
        }
    }

    public class ClassProxyGenerator : BaseProxyGenerator
    {
        public ClassProxyGenerator(ModuleScope scope, Type targetType)
            : base(scope, targetType)
        {
            CheckNotGenericTypeDefinition(targetType, "targetType");
            EnsureDoesNotImplementIProxyTargetAccessor(targetType, "targetType");
        }

        public Type GenerateCode(Type[] interfaces, ProxyGenerationOptions options)
        {
            // make sure ProxyGenerationOptions is initialized
            options.Initialize();

            interfaces = TypeUtil.GetAllInterfaces(interfaces);
            CheckNotGenericTypeDefinitions(interfaces, "interfaces");
            ProxyGenerationOptions = options;
            var cacheKey = new CacheKey(targetType, interfaces, options);
            return ObtainProxyType(cacheKey, (n, s) => GenerateType(n, interfaces, s));
        }

        protected virtual Type GenerateType(string name, Type[] interfaces, INamingScope namingScope)
        {
            IEnumerable<ITypeContributor> contributors;
            var implementedInterfaces = GetTypeImplementerMappingInternal(interfaces, out contributors, namingScope);

            var model = new MetaType();
            // Collect methods
            foreach (var contributor in contributors)
            {
                contributor.CollectElementsToProxy(ProxyGenerationOptions.Hook, model);
            }
            ProxyGenerationOptions.Hook.MethodsInspected();

            var emitter = BuildClassEmitter(name, targetType, implementedInterfaces);

            CreateFields(emitter);
            CreateTypeAttributes(emitter);

            // Constructor
            var cctor = GenerateStaticConstructor(emitter);

            var constructorArguments = new List<FieldReference>();
            foreach (var contributor in contributors)
            {
                contributor.Generate(emitter, ProxyGenerationOptions);

                // TODO: redo it
                if (contributor is MixinContributor)
                {
                    constructorArguments.AddRange((contributor as MixinContributor).Fields);
                }
            }

            // constructor arguments
            var interceptorsField = emitter.GetField("__interceptors");
            constructorArguments.Add(interceptorsField);
            var selector = emitter.GetField("__selector");
            if (selector != null)
            {
                constructorArguments.Add(selector);
            }

            GenerateConstructors(emitter, targetType, constructorArguments.ToArray());
            GenerateParameterlessConstructor(emitter, targetType, interceptorsField);

            // Complete type initializer code body
            CompleteInitCacheMethod(cctor.CodeBuilder);

            // Crosses fingers and build type

            var proxyType = emitter.BuildType();
            InitializeStaticFields(proxyType);
            return proxyType;
        }

        protected virtual IEnumerable<Type> GetTypeImplementerMappingInternal(Type[] interfaces,
                                                                      out IEnumerable<ITypeContributor> contributors,
                                                                      INamingScope namingScope)
        {
            var methodsToSkip = new List<MethodInfo>();
            var proxyInstance = new ClassProxyInstanceContributor(targetType, methodsToSkip, interfaces, ProxyTypeConstants.Class);
            // TODO: the trick with methodsToSkip is not very nice...
            var proxyTarget = new ClassProxyTargetContributor(targetType, methodsToSkip, namingScope) { Logger = Logger };
            IDictionary<Type, ITypeContributor> typeImplementerMapping = new Dictionary<Type, ITypeContributor>();

            // Order of interface precedence:
            // 1. first target
            // target is not an interface so we do nothing

            var targetInterfaces = targetType.GetAllInterfaces();
            var additionalInterfaces = TypeUtil.GetAllInterfaces(interfaces);
            // 2. then mixins
            var mixins = new MixinContributor(namingScope, false) { Logger = Logger };
            if (ProxyGenerationOptions.HasMixins)
            {
                foreach (var mixinInterface in ProxyGenerationOptions.MixinData.MixinInterfaces)
                {
                    if (targetInterfaces.Contains(mixinInterface))
                    {
                        // OK, so the target implements this interface. We now do one of two things:
                        if (additionalInterfaces.Contains(mixinInterface) && typeImplementerMapping.ContainsKey(mixinInterface) == false)
                        {
                            AddMappingNoCheck(mixinInterface, proxyTarget, typeImplementerMapping);
                            proxyTarget.AddInterfaceToProxy(mixinInterface);
                        }
                        // we do not intercept the interface
                        mixins.AddEmptyInterface(mixinInterface);
                    }
                    else
                    {
                        if (!typeImplementerMapping.ContainsKey(mixinInterface))
                        {
                            mixins.AddInterfaceToProxy(mixinInterface);
                            AddMappingNoCheck(mixinInterface, mixins, typeImplementerMapping);
                        }
                    }
                }
            }
            var additionalInterfacesContributor = new InterfaceProxyWithoutTargetContributor(namingScope,
                                                                                             (c, m) => NullExpression.Instance) { Logger = Logger };
            // 3. then additional interfaces
            foreach (var @interface in additionalInterfaces)
            {
                if (targetInterfaces.Contains(@interface))
                {
                    if (typeImplementerMapping.ContainsKey(@interface))
                    {
                        continue;
                    }

                    // we intercept the interface, and forward calls to the target type
                    AddMappingNoCheck(@interface, proxyTarget, typeImplementerMapping);
                    proxyTarget.AddInterfaceToProxy(@interface);
                }
                else if (ProxyGenerationOptions.MixinData.ContainsMixin(@interface) == false)
                {
                    additionalInterfacesContributor.AddInterfaceToProxy(@interface);
                    AddMapping(@interface, additionalInterfacesContributor, typeImplementerMapping);
                }
            }
#if !SILVERLIGHT
			// 4. plus special interfaces
			if (targetType.IsSerializable)
			{
				AddMappingForISerializable(typeImplementerMapping, proxyInstance);
			}
#endif
            try
            {
                AddMappingNoCheck(typeof(IProxyTargetAccessor), proxyInstance, typeImplementerMapping);
            }
            catch (ArgumentException)
            {
                HandleExplicitlyPassedProxyTargetAccessor(targetInterfaces, additionalInterfaces);
            }

            contributors = new List<ITypeContributor>
			{
				proxyTarget,
				mixins,
				additionalInterfacesContributor,
				proxyInstance
			};

            AddAdditionalContributors(targetType, (List<ITypeContributor>)contributors);

            return typeImplementerMapping.Keys;
        }

        private void EnsureDoesNotImplementIProxyTargetAccessor(Type type, string name)
        {
            if (!typeof(IProxyTargetAccessor).IsAssignableFrom(type))
            {
                return;
            }
            var message =
                string.Format(
                    "Target type for the proxy implements {0} which is a DynamicProxy infrastructure interface and you should never implement it yourself. Are you trying to proxy an existing proxy?",
                    typeof(IProxyTargetAccessor));
            throw new ArgumentException(message, name);
        }
    }

    public class ClassProxyWithTargetGenerator : BaseProxyGenerator
    {
        private readonly Type[] additionalInterfacesToProxy;

        public ClassProxyWithTargetGenerator(ModuleScope scope, Type classToProxy, Type[] additionalInterfacesToProxy,
                                             ProxyGenerationOptions options)
            : base(scope, classToProxy)
        {
            CheckNotGenericTypeDefinition(targetType, "targetType");
            EnsureDoesNotImplementIProxyTargetAccessor(targetType, "targetType");
            CheckNotGenericTypeDefinitions(additionalInterfacesToProxy, "additionalInterfacesToProxy");

            options.Initialize();
            ProxyGenerationOptions = options;
            this.additionalInterfacesToProxy = TypeUtil.GetAllInterfaces(additionalInterfacesToProxy);
        }

        public Type GetGeneratedType()
        {
            var cacheKey = new CacheKey(targetType, targetType, additionalInterfacesToProxy, ProxyGenerationOptions);
            return ObtainProxyType(cacheKey, GenerateType);
        }

        protected virtual IEnumerable<Type> GetTypeImplementerMappingInternal(out IEnumerable<ITypeContributor> contributors,
                                                                      INamingScope namingScope)
        {
            var methodsToSkip = new List<MethodInfo>();
            var proxyInstance = new ClassProxyInstanceContributor(targetType, methodsToSkip, additionalInterfacesToProxy,
                                                                  ProxyTypeConstants.ClassWithTarget);
            // TODO: the trick with methodsToSkip is not very nice...
            var proxyTarget = new ClassProxyWithTargetTargetContributor(targetType, methodsToSkip, namingScope) { Logger = Logger };
            IDictionary<Type, ITypeContributor> typeImplementerMapping = new Dictionary<Type, ITypeContributor>();

            // Order of interface precedence:
            // 1. first target
            // target is not an interface so we do nothing

            var targetInterfaces = targetType.GetAllInterfaces();
            // 2. then mixins
            var mixins = new MixinContributor(namingScope, false) { Logger = Logger };
            if (ProxyGenerationOptions.HasMixins)
            {
                foreach (var mixinInterface in ProxyGenerationOptions.MixinData.MixinInterfaces)
                {
                    if (targetInterfaces.Contains(mixinInterface))
                    {
                        // OK, so the target implements this interface. We now do one of two things:
                        if (additionalInterfacesToProxy.Contains(mixinInterface) &&
                            typeImplementerMapping.ContainsKey(mixinInterface) == false)
                        {
                            AddMappingNoCheck(mixinInterface, proxyTarget, typeImplementerMapping);
                            proxyTarget.AddInterfaceToProxy(mixinInterface);
                        }
                        // we do not intercept the interface
                        mixins.AddEmptyInterface(mixinInterface);
                    }
                    else
                    {
                        if (!typeImplementerMapping.ContainsKey(mixinInterface))
                        {
                            mixins.AddInterfaceToProxy(mixinInterface);
                            AddMappingNoCheck(mixinInterface, mixins, typeImplementerMapping);
                        }
                    }
                }
            }
            var additionalInterfacesContributor = new InterfaceProxyWithoutTargetContributor(namingScope,
                                                                                             (c, m) => NullExpression.Instance) { Logger = Logger };
            // 3. then additional interfaces
            foreach (var @interface in additionalInterfacesToProxy)
            {
                if (targetInterfaces.Contains(@interface))
                {
                    if (typeImplementerMapping.ContainsKey(@interface))
                    {
                        continue;
                    }

                    // we intercept the interface, and forward calls to the target type
                    AddMappingNoCheck(@interface, proxyTarget, typeImplementerMapping);
                    proxyTarget.AddInterfaceToProxy(@interface);
                }
                else if (ProxyGenerationOptions.MixinData.ContainsMixin(@interface) == false)
                {
                    additionalInterfacesContributor.AddInterfaceToProxy(@interface);
                    AddMapping(@interface, additionalInterfacesContributor, typeImplementerMapping);
                }
            }
#if !SILVERLIGHT
			// 4. plus special interfaces
			if (targetType.IsSerializable)
			{
				AddMappingForISerializable(typeImplementerMapping, proxyInstance);
			}
#endif
            try
            {
                AddMappingNoCheck(typeof(IProxyTargetAccessor), proxyInstance, typeImplementerMapping);
            }
            catch (ArgumentException)
            {
                HandleExplicitlyPassedProxyTargetAccessor(targetInterfaces, additionalInterfacesToProxy);
            }

            contributors = new List<ITypeContributor>
			{
				proxyTarget,
				mixins,
				additionalInterfacesContributor,
				proxyInstance
			};

            AddAdditionalContributors(targetType, (List<ITypeContributor>)contributors);

            return typeImplementerMapping.Keys;
        }

        private FieldReference CreateTargetField(ClassEmitter emitter)
        {
            var targetField = emitter.CreateField("__target", targetType);
			emitter.DefineCustomAttributeFor<XmlIgnoreAttribute>(targetField);
            return targetField;
        }

        private void EnsureDoesNotImplementIProxyTargetAccessor(Type type, string name)
        {
            if (!typeof(IProxyTargetAccessor).IsAssignableFrom(type))
            {
                return;
            }
            var message =
                string.Format(
                    "Target type for the proxy implements {0} which is a DynamicProxy infrastructure interface and you should never implement it yourself. Are you trying to proxy an existing proxy?",
                    typeof(IProxyTargetAccessor));
            throw new ArgumentException(message, name);
        }

        private Type GenerateType(string name, INamingScope namingScope)
        {
            IEnumerable<ITypeContributor> contributors;
            var implementedInterfaces = GetTypeImplementerMappingInternal(out contributors, namingScope);

            var model = new MetaType();
            // Collect methods
            foreach (var contributor in contributors)
            {
                contributor.CollectElementsToProxy(ProxyGenerationOptions.Hook, model);
            }
            ProxyGenerationOptions.Hook.MethodsInspected();

            var emitter = BuildClassEmitter(name, targetType, implementedInterfaces);

            CreateFields(emitter);
            CreateTypeAttributes(emitter);

            // Constructor
            var cctor = GenerateStaticConstructor(emitter);

            var targetField = CreateTargetField(emitter);
            var constructorArguments = new List<FieldReference> { targetField };

            foreach (var contributor in contributors)
            {
                contributor.Generate(emitter, ProxyGenerationOptions);

                // TODO: redo it
                if (contributor is MixinContributor)
                {
                    constructorArguments.AddRange((contributor as MixinContributor).Fields);
                }
            }

            // constructor arguments
            var interceptorsField = emitter.GetField("__interceptors");
            constructorArguments.Add(interceptorsField);
            var selector = emitter.GetField("__selector");
            if (selector != null)
            {
                constructorArguments.Add(selector);
            }

            GenerateConstructors(emitter, targetType, constructorArguments.ToArray());
            GenerateParameterlessConstructor(emitter, targetType, interceptorsField);

            // Complete type initializer code body
            CompleteInitCacheMethod(cctor.CodeBuilder);

            // Crosses fingers and build type

            var proxyType = emitter.BuildType();
            InitializeStaticFields(proxyType);
            return proxyType;
        }
    }

    public class InterfaceProxyWithTargetGenerator : BaseProxyGenerator
    {
        protected FieldReference targetField;

        public InterfaceProxyWithTargetGenerator(ModuleScope scope, Type @interface)
            : base(scope, @interface)
        {
            CheckNotGenericTypeDefinition(@interface, "@interface");
        }

        protected virtual bool AllowChangeTarget
        {
            get { return false; }
        }

        protected virtual string GeneratorType
        {
            get { return ProxyTypeConstants.InterfaceWithTarget; }
        }

        public Type GenerateCode(Type proxyTargetType, Type[] interfaces, ProxyGenerationOptions options)
        {
            // make sure ProxyGenerationOptions is initialized
            options.Initialize();

            CheckNotGenericTypeDefinition(proxyTargetType, "proxyTargetType");
            CheckNotGenericTypeDefinitions(interfaces, "interfaces");
            EnsureValidBaseType(options.BaseTypeForInterfaceProxy);
            ProxyGenerationOptions = options;

            interfaces = TypeUtil.GetAllInterfaces(interfaces);
            var cacheKey = new CacheKey(proxyTargetType, targetType, interfaces, options);

            return ObtainProxyType(cacheKey, (n, s) => GenerateType(n, proxyTargetType, interfaces, s));
        }

        protected virtual ITypeContributor AddMappingForTargetType(IDictionary<Type, ITypeContributor> typeImplementerMapping,
                                                                   Type proxyTargetType, ICollection<Type> targetInterfaces,
                                                                   ICollection<Type> additionalInterfaces,
                                                                   INamingScope namingScope)
        {
            var contributor = new InterfaceProxyTargetContributor(proxyTargetType, AllowChangeTarget, namingScope) { Logger = Logger };
            var proxiedInterfaces = targetType.GetAllInterfaces();
            foreach (var @interface in proxiedInterfaces)
            {
                contributor.AddInterfaceToProxy(@interface);
                AddMappingNoCheck(@interface, contributor, typeImplementerMapping);
            }

            foreach (var @interface in additionalInterfaces)
            {
                if (!ImplementedByTarget(targetInterfaces, @interface) || proxiedInterfaces.Contains(@interface))
                {
                    continue;
                }

                contributor.AddInterfaceToProxy(@interface);
                AddMappingNoCheck(@interface, contributor, typeImplementerMapping);
            }
            return contributor;
        }

#if (!SILVERLIGHT)
		protected override void CreateTypeAttributes(ClassEmitter emitter)
		{
			base.CreateTypeAttributes(emitter);
			emitter.DefineCustomAttribute<SerializableAttribute>();
		}
#endif

        protected virtual Type GenerateType(string typeName, Type proxyTargetType, Type[] interfaces, INamingScope namingScope)
        {
            IEnumerable<ITypeContributor> contributors;
            var allInterfaces = GetTypeImplementerMappingInternal(interfaces, proxyTargetType, out contributors, namingScope);

            ClassEmitter emitter;
            FieldReference interceptorsField;
            var baseType = Init(typeName, out emitter, proxyTargetType, out interceptorsField, allInterfaces);

            var model = new MetaType();
            // Collect methods
            foreach (var contributor in contributors)
            {
                contributor.CollectElementsToProxy(ProxyGenerationOptions.Hook, model);
            }

            ProxyGenerationOptions.Hook.MethodsInspected();

            // Constructor

            var cctor = GenerateStaticConstructor(emitter);
            var ctorArguments = new List<FieldReference>();

            foreach (var contributor in contributors)
            {
                contributor.Generate(emitter, ProxyGenerationOptions);

                // TODO: redo it
                if (contributor is MixinContributor)
                {
                    ctorArguments.AddRange((contributor as MixinContributor).Fields);
                }
            }

            ctorArguments.Add(interceptorsField);
            ctorArguments.Add(targetField);
            var selector = emitter.GetField("__selector");
            if (selector != null)
            {
                ctorArguments.Add(selector);
            }

            GenerateConstructors(emitter, baseType, ctorArguments.ToArray());

            // Complete type initializer code body
            CompleteInitCacheMethod(cctor.CodeBuilder);

            // Crosses fingers and build type
            var generatedType = emitter.BuildType();

            InitializeStaticFields(generatedType);

            return generatedType;
        }

        protected virtual InterfaceProxyWithoutTargetContributor GetContributorForAdditionalInterfaces(
            INamingScope namingScope)
        {
            return new InterfaceProxyWithoutTargetContributor(namingScope, (c, m) => NullExpression.Instance) { Logger = Logger };
        }

        protected virtual IEnumerable<Type> GetTypeImplementerMappingInternal(Type[] interfaces, Type proxyTargetType,
                                                                      out IEnumerable<ITypeContributor> contributors,
                                                                      INamingScope namingScope)
        {
            IDictionary<Type, ITypeContributor> typeImplementerMapping = new Dictionary<Type, ITypeContributor>();
            var mixins = new MixinContributor(namingScope, AllowChangeTarget) { Logger = Logger };
            // Order of interface precedence:
            // 1. first target
            var targetInterfaces = proxyTargetType.GetAllInterfaces();
            var additionalInterfaces = TypeUtil.GetAllInterfaces(interfaces);
            var target = AddMappingForTargetType(typeImplementerMapping, proxyTargetType, targetInterfaces, additionalInterfaces,
                                                 namingScope);

            // 2. then mixins
            if (ProxyGenerationOptions.HasMixins)
            {
                foreach (var mixinInterface in ProxyGenerationOptions.MixinData.MixinInterfaces)
                {
                    if (targetInterfaces.Contains(mixinInterface))
                    {
                        // OK, so the target implements this interface. We now do one of two things:
                        if (additionalInterfaces.Contains(mixinInterface))
                        {
                            // we intercept the interface, and forward calls to the target type
                            AddMapping(mixinInterface, target, typeImplementerMapping);
                        }
                        // we do not intercept the interface
                        mixins.AddEmptyInterface(mixinInterface);
                    }
                    else
                    {
                        if (!typeImplementerMapping.ContainsKey(mixinInterface))
                        {
                            mixins.AddInterfaceToProxy(mixinInterface);
                            typeImplementerMapping.Add(mixinInterface, mixins);
                        }
                    }
                }
            }

            var additionalInterfacesContributor = GetContributorForAdditionalInterfaces(namingScope);
            // 3. then additional interfaces
            foreach (var @interface in additionalInterfaces)
            {
                if (typeImplementerMapping.ContainsKey(@interface))
                {
                    continue;
                }
                if (ProxyGenerationOptions.MixinData.ContainsMixin(@interface))
                {
                    continue;
                }

                additionalInterfacesContributor.AddInterfaceToProxy(@interface);
                AddMappingNoCheck(@interface, additionalInterfacesContributor, typeImplementerMapping);
            }

            // 4. plus special interfaces
            var instance = new InterfaceProxyInstanceContributor(targetType, GeneratorType, interfaces);
            AddMappingForISerializable(typeImplementerMapping, instance);
            try
            {
                AddMappingNoCheck(typeof(IProxyTargetAccessor), instance, typeImplementerMapping);
            }
            catch (ArgumentException)
            {
                HandleExplicitlyPassedProxyTargetAccessor(targetInterfaces, additionalInterfaces);
            }

            contributors = new List<ITypeContributor>
			{
				target,
				additionalInterfacesContributor,
				mixins,
				instance
			};
            
            AddAdditionalContributors(targetType, (List<ITypeContributor>)contributors);

            return typeImplementerMapping.Keys;
        }

        protected virtual Type Init(string typeName, out ClassEmitter emitter, Type proxyTargetType,
                                    out FieldReference interceptorsField, IEnumerable<Type> interfaces)
        {
            var baseType = ProxyGenerationOptions.BaseTypeForInterfaceProxy;

            emitter = BuildClassEmitter(typeName, baseType, interfaces);

            CreateFields(emitter, proxyTargetType);
            CreateTypeAttributes(emitter);

            interceptorsField = emitter.GetField("__interceptors");
            return baseType;
        }

        private void CreateFields(ClassEmitter emitter, Type proxyTargetType)
        {
            base.CreateFields(emitter);
            targetField = emitter.CreateField("__target", proxyTargetType);
#if !SILVERLIGHT
			emitter.DefineCustomAttributeFor<XmlIgnoreAttribute>(targetField);
#endif
        }

        private void EnsureValidBaseType(Type type)
        {
            if (type == null)
            {
                throw new ArgumentException(
                    "Base type for proxy is null reference. Please set it to System.Object or some other valid type.");
            }

            if (!type.IsClass)
            {
                ThrowInvalidBaseType(type, "it is not a class type");
            }

            if (type.IsSealed)
            {
                ThrowInvalidBaseType(type, "it is sealed");
            }

            var constructor = type.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
                                                  null, Type.EmptyTypes, null);

            if (constructor == null || constructor.IsPrivate)
            {
                ThrowInvalidBaseType(type, "it does not have accessible parameterless constructor");
            }
        }

        private bool ImplementedByTarget(ICollection<Type> targetInterfaces, Type @interface)
        {
            return targetInterfaces.Contains(@interface);
        }

        private void ThrowInvalidBaseType(Type type, string doesNotHaveAccessibleParameterlessConstructor)
        {
            var format =
                "Type {0} is not valid base type for interface proxy, because {1}. Only a non-sealed class with non-private default constructor can be used as base type for interface proxy. Please use some other valid type.";
            throw new ArgumentException(string.Format(format, type, doesNotHaveAccessibleParameterlessConstructor));
        }
    }

    public class InterfaceProxyWithTargetInterfaceGenerator : InterfaceProxyWithTargetGenerator
    {
        public InterfaceProxyWithTargetInterfaceGenerator(ModuleScope scope, Type @interface)
            : base(scope, @interface)
        {
        }

        protected override bool AllowChangeTarget
        {
            get { return true; }
        }

        protected override string GeneratorType
        {
            get { return ProxyTypeConstants.InterfaceWithTargetInterface; }
        }

        protected override ITypeContributor AddMappingForTargetType(
            IDictionary<Type, ITypeContributor> typeImplementerMapping, Type proxyTargetType, ICollection<Type> targetInterfaces,
            ICollection<Type> additionalInterfaces, INamingScope namingScope)
        {
            var contributor = new InterfaceProxyWithTargetInterfaceTargetContributor(
                proxyTargetType,
                AllowChangeTarget,
                namingScope) { Logger = Logger };
            foreach (var @interface in targetType.GetAllInterfaces())
            {
                contributor.AddInterfaceToProxy(@interface);
                AddMappingNoCheck(@interface, contributor, typeImplementerMapping);
            }

            return contributor;
        }

        protected override InterfaceProxyWithoutTargetContributor GetContributorForAdditionalInterfaces(
            INamingScope namingScope)
        {
            return new InterfaceProxyWithOptionalTargetContributor(namingScope, GetTargetExpression, GetTarget) { Logger = Logger };
        }

        private Reference GetTarget(ClassEmitter @class, MethodInfo method)
        {
            return new AsTypeReference(@class.GetField("__target"), method.DeclaringType);
        }

        private Expression GetTargetExpression(ClassEmitter @class, MethodInfo method)
        {
            return GetTarget(@class, method).ToExpression();
        }
    }

    public class InterfaceProxyWithoutTargetGenerator : InterfaceProxyWithTargetGenerator
    {
        public InterfaceProxyWithoutTargetGenerator(ModuleScope scope, Type @interface)
            : base(scope, @interface)
        {
        }

        protected override string GeneratorType
        {
            get { return ProxyTypeConstants.InterfaceWithoutTarget; }
        }

        protected override ITypeContributor AddMappingForTargetType(
            IDictionary<Type, ITypeContributor> interfaceTypeImplementerMapping, Type proxyTargetType,
            ICollection<Type> targetInterfaces, ICollection<Type> additionalInterfaces, INamingScope namingScope)
        {
            var contributor = new InterfaceProxyWithoutTargetContributor(namingScope, (c, m) => NullExpression.Instance) { Logger = Logger };
            foreach (var @interface in targetType.GetAllInterfaces())
            {
                contributor.AddInterfaceToProxy(@interface);
                AddMappingNoCheck(@interface, contributor, interfaceTypeImplementerMapping);
            }
            return contributor;
        }

        protected override Type GenerateType(string typeName, Type proxyTargetType, Type[] interfaces,
                                             INamingScope namingScope)
        {
            IEnumerable<ITypeContributor> contributors;
            var allInterfaces = GetTypeImplementerMappingInternal(interfaces, targetType, out contributors, namingScope);
            var model = new MetaType();
            // collect elements
            foreach (var contributor in contributors)
            {
                contributor.CollectElementsToProxy(ProxyGenerationOptions.Hook, model);
            }

            ProxyGenerationOptions.Hook.MethodsInspected();

            ClassEmitter emitter;
            FieldReference interceptorsField;
            var baseType = Init(typeName, out emitter, proxyTargetType, out interceptorsField, allInterfaces);

            // Constructor

            var cctor = GenerateStaticConstructor(emitter);
            var mixinFieldsList = new List<FieldReference>();

            foreach (var contributor in contributors)
            {
                contributor.Generate(emitter, ProxyGenerationOptions);

                // TODO: redo it
                if (contributor is MixinContributor)
                {
                    mixinFieldsList.AddRange((contributor as MixinContributor).Fields);
                }
            }

            var ctorArguments = new List<FieldReference>(mixinFieldsList) { interceptorsField, targetField };
            var selector = emitter.GetField("__selector");
            if (selector != null)
            {
                ctorArguments.Add(selector);
            }

            GenerateConstructors(emitter, baseType, ctorArguments.ToArray());

            // Complete type initializer code body
            CompleteInitCacheMethod(cctor.CodeBuilder);

            // Crosses fingers and build type
            var generatedType = emitter.BuildType();

            InitializeStaticFields(generatedType);
            return generatedType;
        }
    }

    internal static class ProxyTypeConstants
    {
        public static readonly string Class = "class";
        public static readonly string ClassWithTarget = "class.with.target";
        public static readonly string InterfaceWithTarget = "interface.with.target";
        public static readonly string InterfaceWithTargetInterface = "interface.with.target.interface";
        public static readonly string InterfaceWithoutTarget = "interface.without.target";
    }

    /// <summary>
    /// Implement a default base.Get getter if setter will be intercepted.
    /// </summary>
    public class DefaultGetterTypeContributor : ITypeContributor
    {
        private List<MetaProperty> propertiesToFix;

        public void CollectElementsToProxy(IProxyGenerationHook hook, MetaType model)
        {
            // Find all readable properties with setters which don't yet have getters (they were excluded for optimization)
            propertiesToFix = model.Properties
                 .Where(p => p.CanWrite && !p.CanRead && p.SetMethod.GetProperty().CanRead)
                 .ToList();
        }

        public void Generate(ClassEmitter @class, ProxyGenerationOptions options)
        {
            foreach (var metaProperty in propertiesToFix)
            {
                // Load the get method via set method available on the property
                var propertyInfo = metaProperty.SetMethod.GetProperty();
                var propertyGetter = propertyInfo.GetGetMethod(true);

                // Excluding VtableLayoutMask attribute so that getter reads as:
                // 'override get_Prop()' instead of 'virtual get_Prop()'
                var getMethodAttributes = propertyGetter.Attributes & ~MethodAttributes.VtableLayoutMask;

                // Force create get method via emitter
                var emitter = metaProperty.Emitter.CreateGetMethod(propertyGetter.Name, getMethodAttributes, propertyGetter);

                // Implement the override
                emitter.CodeBuilder.AddStatement(new ReturnStatement(
                                    new MethodInvocationExpression(SelfReference.Self, propertyGetter)
                                        {
                                            // call base (so not virtual)
                                            VirtualCall = false
                                        }));
            }
        }

    }

}
