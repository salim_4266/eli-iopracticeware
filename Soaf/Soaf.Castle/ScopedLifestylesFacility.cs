﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using Castle.Core.Internal;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;
using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Lifestyle.Scoped;
using Castle.MicroKernel.ModelBuilder;
using Castle.MicroKernel.Registration;
using Soaf.ComponentModel;
using Soaf.Validation;
using IInterceptor = Castle.DynamicProxy.IInterceptor;
using IInvocation = Castle.DynamicProxy.IInvocation;
using LifestyleType = Castle.Core.LifestyleType;

namespace Soaf.Castle
{
    /// <summary>
    ///     Installs extended Scoped lifestyle support.
    /// </summary>
    internal class ScopedLifestylesFacility : AbstractFacility
    {
        protected override void Init()
        {
            Kernel.Register(
                Component.For<ScopeInterceptor>().LifeStyle.Transient,
                Component.For<IScope>().LifeStyle.Transient.Interceptors<ScopeInterceptor>());

            Kernel.ComponentModelBuilder.AddContributor(new ScopedLifestylesContributor());
        }

        public class ReleasingThreadScopeAccessor : IScopeAccessor
        {
            private readonly SimpleThreadSafeDictionary<int, ILifetimeScope> items = new SimpleThreadSafeDictionary<int, ILifetimeScope>();

            public void Dispose()
            {
                foreach (ILifetimeScope disposable in items.EjectAllValues().Reverse())
                    disposable.Dispose();
            }

            public ILifetimeScope GetScope(CreationContext context)
            {
                return items.GetOrAdd(GetCurrentThreadId(), id => (ILifetimeScope)new ReleasingLifetimeScope());
            }

            protected virtual int GetCurrentThreadId()
            {
                return Thread.CurrentThread.ManagedThreadId;
            }
        }


        /// <summary>
        ///     A custom IScopeAccessor that supports operates using the CallContext.
        /// </summary>
        public class CallContextLifetimeScopeAccessor : IScopeAccessor
        {
            /// <summary>
            ///     The scope store. We don't store the scopes directly in the CallContext to avoid serialization problems at the
            ///     AppDomain level.
            /// </summary>
            private static readonly ConcurrentDictionary<Guid, ILifetimeScope> ScopeStore = new ConcurrentDictionary<Guid, ILifetimeScope>();

            /// <summary>
            /// The call context key to use to store the scope.
            /// </summary>
            private readonly string callContextKey;

            private readonly Func<ILifetimeScope> createScope;

            private const string DefaultCallContextKey = "CallContextLifetimeScopeAccessor_CallContextKey";

            public CallContextLifetimeScopeAccessor(string callContextKey, Func<ILifetimeScope> createScope)
            {
                this.callContextKey = callContextKey;
                this.createScope = createScope;
            }

            public CallContextLifetimeScopeAccessor()
                : this(DefaultCallContextKey, null)
            {
            }

            public void Dispose()
            {
                ScopeStore.Clear();
            }

            public ILifetimeScope GetScope(CreationContext context)
            {
                ILifetimeScope scope = null;

                object id = CallContext.LogicalGetData(callContextKey);

                // Get scope if Id is set
                // Note: when using AsParallel().ForAll() it sometimes happens that CallContext is not cleared on a re-used thread
                if (id is Guid && ScopeStore.TryGetValue((Guid)id, out scope))
                {
                    // Got scope
                }
                else if (createScope != null)
                {
                    scope = createScope();
                    SetScope(scope);
                }
                return scope;
            }

            public void SetScope(ILifetimeScope value)
            {
                if (value != null)
                {
                    Guid id = Guid.NewGuid();
                    ScopeStore[id] = value;
                    CallContext.LogicalSetData(callContextKey, id);
                }
                else
                {
                    object id = CallContext.LogicalGetData(callContextKey);
                    if (id is Guid)
                    {
                        ILifetimeScope o;
                        ScopeStore.TryRemove((Guid)id, out o);
                    }
                    CallContext.FreeNamedDataSlot(callContextKey);
                }
            }
        }

        /// <summary>
        ///     A custom LifetimeScope that supports releasing scoped instances.
        /// </summary>
        internal class ReleasingLifetimeScope : ILifetimeScope
        {
            private readonly Lock @lock = Lock.Create();
            private IDictionary<object, Burden> cache = new Dictionary<object, Burden>();

            public void Dispose()
            {
                using (IUpgradeableLockHolder upgradeableLockHolder = @lock.ForReadingUpgradeable())
                {
                    if (cache == null)
                        return;
                    upgradeableLockHolder.Upgrade();
                    cache.Values.Reverse().ToList().ForEach(v => v.Release());
                    cache = null;
                }
            }

            public Burden GetCachedInstance(global::Castle.Core.ComponentModel model, ScopedInstanceActivationCallback createInstance)
            {
                using (IUpgradeableLockHolder upgradeableLockHolder = @lock.ForReadingUpgradeable())
                {
                    Burden burden;
                    if (!cache.TryGetValue(model, out burden))
                    {
                        // try to get from parent

                        upgradeableLockHolder.Upgrade();
                        burden = createInstance(i => { });
                        cache[model] = burden;
                    }
                    return burden;
                }
            }

            public void Release(global::Castle.Core.ComponentModel model)
            {
                using (IUpgradeableLockHolder upgradeableLockHolder = @lock.ForReadingUpgradeable())
                {
                    if (cache == null)
                        return;
                    upgradeableLockHolder.Upgrade();

                    Burden burden;
                    if (cache.TryGetValue(model, out burden))
                    {
                        cache.Remove(model);
                        burden.Release();
                    }
                }
            }
        }

        /// <summary>
        ///     A ScopedLifestyleManager that supports releasing components within the given scope.
        /// </summary>
        internal class ReleasingScopedLifestyleManager<TScopeAccessor> : ScopedLifestyleManager where TScopeAccessor : IScopeAccessor, new()
        {
            private readonly IScopeAccessor accessor;

            private ILifestyleManager lifestyleManagerOutsideScope;

            public static readonly TScopeAccessor DefaultScopeAccessor = new TScopeAccessor();

            public ReleasingScopedLifestyleManager() : this(DefaultScopeAccessor) { }

            protected ReleasingScopedLifestyleManager(IScopeAccessor accessor)
                : base(accessor)
            {
                this.accessor = accessor;
            }

            public override void Init(IComponentActivator componentActivator, IKernel kernel, global::Castle.Core.ComponentModel model)
            {
                if (kernel is DefaultKernel)
                {
                    var descriptor = model.ExtendedProperties[typeof(ILifestyleDescriptor)] as IScopedLifestyleDescriptor;
                    // Switch the lifestyle, create a second lifestyle manager and then switch back, 
                    // then we have a backup lifestyle manager to use when there is no scope

                    if (descriptor != null)
                    {
                        bool shouldCreateLifestyleManagerOutsideScope = true;
                        switch (descriptor.LifestyleOutsideScope)
                        {
                            case ComponentModel.LifestyleType.Transient:
                                model.LifestyleType = LifestyleType.Transient;
                                break;
                            case ComponentModel.LifestyleType.Singleton:
                                model.LifestyleType = LifestyleType.Singleton;
                                break;
                            case ComponentModel.LifestyleType.CallContext:
                                model.LifestyleType = LifestyleType.Custom;
                                model.CustomLifestyle = typeof(ReleasingCallContextLifestyleManager);
                                break;
                            case ComponentModel.LifestyleType.CreationContext:
                                model.LifestyleType = LifestyleType.Bound;
                                model.ExtendedProperties[Constants.ScopeRootSelector] = new Func<IHandler[], IHandler>(h => h.First());
                                break;
                            default:
                                shouldCreateLifestyleManagerOutsideScope = false;
                                break;
                        }

                        if (shouldCreateLifestyleManagerOutsideScope)
                        {
                            lifestyleManagerOutsideScope = ((DefaultKernel)kernel).CreateLifestyleManager(model, componentActivator);

                            // revert back
                            model.LifestyleType = LifestyleType.Custom;
                            model.CustomLifestyle = GetType();
                        }
                    }

                }

                base.Init(componentActivator, kernel, model);
            }

            protected override void Track(Burden burden, IReleasePolicy releasePolicy)
            {
                burden.RequiresDecommission = true;
                burden.TrackedExternally = false;
                base.Track(burden, releasePolicy);
            }

            public override object Resolve(CreationContext context, IReleasePolicy releasePolicy)
            {
                if (GetScope(context) == null && lifestyleManagerOutsideScope != null)
                {
                    return lifestyleManagerOutsideScope.Resolve(context, releasePolicy);

                }
                return base.Resolve(context, releasePolicy);
            }

            public override bool Release(object instance)
            {
                var scope = GetScope(null);

                var releasingScope = scope as ReleasingLifetimeScope;
                if (releasingScope != null)
                {
                    releasingScope.Release(Model);
                }

                if (scope == null && lifestyleManagerOutsideScope != null)
                {
                    return lifestyleManagerOutsideScope.Release(instance);
                }

                return base.Release(instance);
            }

            private ILifetimeScope GetScope(CreationContext context)
            {
                IScopeAccessor scopeAccessor = accessor;
                if (scopeAccessor == null)
                    throw new ObjectDisposedException("Scope was already disposed. This is most likely a bug in the calling code.");
                ILifetimeScope scope = scopeAccessor.GetScope(context);

                return scope;
            }

        }

        /// <summary>
        /// A CallContext based lifestyle manager that supports releasing. 
        /// </summary>
        public class ReleasingCallContextLifestyleManager : ReleasingScopedLifestyleManager<CallContextLifetimeScopeAccessor>
        {
            public ReleasingCallContextLifestyleManager()
                : base(new CallContextLifetimeScopeAccessor("PerThread_CallContextLifetimeScopeAccessor_CallContextKey", () => new ReleasingLifetimeScope()))
            {
            }
        }

        /// <summary>
        /// A replacement for the default per-thread lifestyle manager that supports releasing. 
        /// It's debatable, but we will handle the per-thread Lifestyle using CallContext storage instead of ThreadStatic.
        /// </summary>
        public class ReleasingThreadLifestyleManager : ReleasingScopedLifestyleManager<CallContextLifetimeScopeAccessor>
        {
            public ReleasingThreadLifestyleManager()
                : base(new ReleasingThreadScopeAccessor())
            {
            }
        }

        /// <summary>
        ///     Intercepts calls to an IScope and manages creation of new Call
        /// </summary>
        [UsedImplicitly]
        internal class ScopeInterceptor : IInterceptor
        {
            private static readonly MethodInfo DisposeMethod = typeof(IDisposable).GetMethods().Single();

            private readonly ILifetimeScope last;
            private readonly ILifetimeScope current;

            public ScopeInterceptor(IKernel kernel)
                : this()
            {
            }

            public ScopeInterceptor()
            {
                last = ReleasingScopedLifestyleManager<CallContextLifetimeScopeAccessor>.DefaultScopeAccessor.GetScope(null);
                current = new ReleasingLifetimeScope();
                ReleasingScopedLifestyleManager<CallContextLifetimeScopeAccessor>.DefaultScopeAccessor.SetScope(current);
            }

            #region IInterceptor Members

            public void Intercept(IInvocation invocation)
            {
                if (invocation.Method == DisposeMethod)
                {
                    Dispose();
                }
                else
                {
                    invocation.Proceed();
                }
            }

            #endregion

            private void Dispose()
            {
                current.Dispose();
                ReleasingScopedLifestyleManager<CallContextLifetimeScopeAccessor>.DefaultScopeAccessor.SetScope(last);
            }
        }

        /// <summary>
        ///     Adds handling for Thread and Scoped lifestyles using the included classes.
        /// </summary>
        internal class ScopedLifestylesContributor : IContributeComponentModelConstruction
        {
            public void ProcessModel(IKernel kernel, global::Castle.Core.ComponentModel model)
            {
                // store this for later so the scope accessor understands whether to check a global CallContext scope or a nested one.
                switch (model.LifestyleType)
                {
                    case LifestyleType.Scoped:
                        model.LifestyleType = LifestyleType.Custom;
                        model.CustomLifestyle = typeof(ReleasingScopedLifestyleManager<CallContextLifetimeScopeAccessor>);
                        break;
                    case LifestyleType.Thread:
                        model.LifestyleType = LifestyleType.Custom;
                        model.CustomLifestyle = typeof(ReleasingThreadLifestyleManager);
                        break;
                }
            }
        }
    }
}