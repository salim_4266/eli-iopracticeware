﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Soaf.Presentation.Controls
{
    public partial class RightButton : UserControl
    {
        public event RoutedEventHandler Click
        {
            add { Button.Click += value; }
            remove { Button.Click -= value; }
        }

        public RightButton()
        {
            InitializeComponent();
        }
    }
}
