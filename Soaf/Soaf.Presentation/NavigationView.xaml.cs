﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media.Animation;

namespace Soaf.Presentation
{
    /// <summary>
    /// Interaction logic for NavigationView.xaml
    /// </summary>
    public partial class NavigationView
    {
        public NavigationView()
        {
            InitializeComponent();
        }
    }

    public class NavigationViewContext : IViewContext
    {
        public IInteractionContext InteractionContext { get; set; }

        [DispatcherThread]
        public virtual ICommand NextAnimation { get; set; }

        [DispatcherThread]
        public virtual ICommand PreviousAnimation { get; set; }

        [DispatcherThread]
        public virtual object CurrentContent { get; set; }

        [DispatcherThread]
        public virtual object NextContent { get; set; }

        [DispatcherThread]
        public virtual object PreviousContent { get; set; }
    }

    public class NavigationViewAnimationBehavior : Behavior<NavigationView>
    {
        public static readonly DependencyProperty NextAnimationCommandProperty = DependencyProperty.Register("NextAnimationCommand", typeof(ICommand), typeof(NavigationViewAnimationBehavior));
        public static readonly DependencyProperty PreviousAnimationCommandProperty = DependencyProperty.Register("PreviousAnimationCommand", typeof(ICommand), typeof(NavigationViewAnimationBehavior));
        public static readonly DependencyProperty WizardWidthProperty = DependencyProperty.Register("WizardWidth", typeof(double), typeof(NavigationViewAnimationBehavior));
        public static readonly DependencyProperty WizardHeightProperty = DependencyProperty.Register("WizardHeight", typeof(double), typeof(NavigationViewAnimationBehavior));
        public static readonly DependencyProperty CurrentContentProperty = DependencyProperty.Register("CurrentContent", typeof(FrameworkElement), typeof(NavigationViewAnimationBehavior), new PropertyMetadata(null, OnPropertyCurrentContentChanged));
        public static readonly DependencyProperty NextContentProperty = DependencyProperty.Register("NextContent", typeof(FrameworkElement), typeof(NavigationViewAnimationBehavior), new PropertyMetadata(null, OnPropertyNextContentChanged));
        public static readonly DependencyProperty PreviousContentProperty = DependencyProperty.Register("PreviousContent", typeof(FrameworkElement), typeof(NavigationViewAnimationBehavior), new PropertyMetadata(null, OnPropertyPreviousContentChanged));
        public static readonly DependencyProperty PreviousStoryboardProperty = DependencyProperty.Register("PreviousStoryboard", typeof(Storyboard), typeof(NavigationViewAnimationBehavior), new PropertyMetadata(null, OnPropertyPreviousStoryboardChanged));
        public static readonly DependencyProperty NextStoryboardProperty = DependencyProperty.Register("NextStoryboard", typeof(Storyboard), typeof(NavigationViewAnimationBehavior), new PropertyMetadata(null, OnPropertyNextStoryboardChanged));

        private bool isNextStoryboardAnimating;
        private bool isPreviousStoryboardAnimating;
        private readonly Queue<Tuple<Action<FrameworkElement>, FrameworkElement>> storyboardActions = new Queue<Tuple<Action<FrameworkElement>, FrameworkElement>>();

        private static void OnPropertyNextStoryboardChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (NavigationViewAnimationBehavior)d;
            var storyboard = e.OldValue as Storyboard;
            if (storyboard != null)
            {
                storyboard.Completed -= behavior.NextStoryboardCompleted;
            }
            storyboard = e.NewValue as Storyboard;
            if (storyboard != null)
            {
                storyboard.Completed += behavior.NextStoryboardCompleted;
            }
        }

        private static void OnPropertyPreviousStoryboardChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (NavigationViewAnimationBehavior)d;
            var storyboard = e.OldValue as Storyboard;
            if (storyboard != null)
            {
                storyboard.Completed -= behavior.PreviousStoryboardCompleted;
            }
            storyboard = e.NewValue as Storyboard;
            if (storyboard != null)
            {
                storyboard.Completed += behavior.PreviousStoryboardCompleted;
            }
        }

        private static void OnPropertyPreviousContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (NavigationViewAnimationBehavior)d;
            var newContent = e.NewValue as FrameworkElement;
            if (newContent != null)
            {
                newContent.Height = behavior.WizardHeight;
                newContent.Width = behavior.WizardWidth;

                // Hides the previous content to the offscreen to the left of the canvas
                var position = behavior.WizardWidth * -1;
                newContent.SetValue(Canvas.LeftProperty, position);
            }
        }

        private static void OnPropertyNextContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (NavigationViewAnimationBehavior)d;
            var newContent = e.NewValue as FrameworkElement;
            if (newContent != null)
            {
                newContent.Height = behavior.WizardHeight;
                newContent.Width = behavior.WizardWidth;

                // Hides the next content to the offscreen to the right of the canvas
                var position = behavior.WizardWidth;
                newContent.SetValue(Canvas.LeftProperty, position);
            }
        }

        private static void OnPropertyCurrentContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var behavior = (NavigationViewAnimationBehavior)d;
            var newContent = e.NewValue as FrameworkElement;
            if (newContent != null)
            {
                newContent.Height = behavior.WizardHeight;
                newContent.Width = behavior.WizardWidth;
            }
        }

        protected override void OnAttached()
        {
            NextAnimationCommand = Command.Create<FrameworkElement>(ExecuteNextAnimationCommand, delegate { return NextStoryboard != null && !isNextStoryboardAnimating; });
            PreviousAnimationCommand = Command.Create<FrameworkElement>(ExecutePreviousAnimationCommand, delegate { return PreviousStoryboard != null && !isPreviousStoryboardAnimating; });

            base.OnAttached();
        }

        private void ExecuteNextAnimationCommand(FrameworkElement nextContent)
        {
            if (isNextStoryboardAnimating || isPreviousStoryboardAnimating)
            {
                storyboardActions.Enqueue(new Tuple<Action<FrameworkElement>, FrameworkElement>(ExecuteNextAnimationCommand, nextContent));
                return;
            }

            isNextStoryboardAnimating = true;
            NextContent = nextContent;
            NextStoryboard.Begin();

        }

        private void ExecutePreviousAnimationCommand(FrameworkElement previousContent)
        {
            if (isNextStoryboardAnimating || isPreviousStoryboardAnimating)
            {
                storyboardActions.Enqueue(new Tuple<Action<FrameworkElement>, FrameworkElement>(ExecutePreviousAnimationCommand, previousContent));
                return;
            }

            isPreviousStoryboardAnimating = true;
            PreviousContent = previousContent;
            PreviousStoryboard.Begin();
        }

        public void NextStoryboardCompleted(object sender, EventArgs e)
        {
            CurrentContent = NextContent;
            NextContent = null;
            isNextStoryboardAnimating = false;

            if (storyboardActions.Any())
            {
                var commandAndParameter = storyboardActions.Dequeue();
                commandAndParameter.Item1(commandAndParameter.Item2);
            }
        }

        private void PreviousStoryboardCompleted(object sender, EventArgs e)
        {
            CurrentContent = PreviousContent;
            PreviousContent = null;
            isPreviousStoryboardAnimating = false;

            if (storyboardActions.Any())
            {
                var commandAndParameter = storyboardActions.Dequeue();
                commandAndParameter.Item1(commandAndParameter.Item2);
            }
        }

        public ICommand NextAnimationCommand
        {
            get { return GetValue(NextAnimationCommandProperty) as ICommand; }
            set { SetValue(NextAnimationCommandProperty, value); }
        }

        public ICommand PreviousAnimationCommand
        {
            get { return GetValue(PreviousAnimationCommandProperty) as ICommand; }
            set { SetValue(PreviousAnimationCommandProperty, value); }
        }

        public double WizardHeight
        {
            get { return (double)GetValue(WizardHeightProperty); }
            set { SetValue(WizardHeightProperty, value); }
        }

        public double WizardWidth
        {
            get { return (double)GetValue(WizardWidthProperty); }
            set { SetValue(WizardWidthProperty, value); }
        }

        public FrameworkElement CurrentContent
        {
            get { return GetValue(CurrentContentProperty) as FrameworkElement; }
            set { SetValue(CurrentContentProperty, value); }
        }

        public FrameworkElement PreviousContent
        {
            get { return GetValue(PreviousContentProperty) as FrameworkElement; }
            set { SetValue(PreviousContentProperty, value); }
        }

        public FrameworkElement NextContent
        {
            get { return GetValue(NextContentProperty) as FrameworkElement; }
            set { SetValue(NextContentProperty, value); }
        }

        public Storyboard NextStoryboard
        {
            get { return GetValue(NextStoryboardProperty) as Storyboard; }
            set { SetValue(NextStoryboardProperty, value); }
        }

        public Storyboard PreviousStoryboard
        {
            get { return GetValue(PreviousStoryboardProperty) as Storyboard; }
            set { SetValue(PreviousStoryboardProperty, value); }
        }
    }
}
