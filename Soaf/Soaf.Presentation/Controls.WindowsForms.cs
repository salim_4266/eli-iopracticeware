﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using Soaf.Collections;
using Telerik.Windows.Controls;
using Application = System.Windows.Application;
using Binding = System.Windows.Data.Binding;
using ButtonBase = System.Windows.Controls.Primitives.ButtonBase;
using Color = System.Drawing.Color;
using Control = System.Windows.Forms.Control;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using Orientation = System.Windows.Controls.Orientation;
using SelectionMode = System.Windows.Controls.SelectionMode;

// ReSharper disable CheckNamespace
namespace Soaf.Presentation.Controls.WindowsForms
// ReSharper restore CheckNamespace
{
    /// <summary>
    ///   A generic Windows Forms wrapper for WPF controls.
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    public abstract class ElementHost<T> : Control where T : FrameworkElement, new()
    {
        public T Control { get; private set; }
        
        private readonly ElementHost host;

        private string style;
        [DefaultValue(null)]
        public virtual string StyleName
        {
            get { return style; }
            set
            {
                style = value;
                if (value.IsNotNullOrEmpty())
                {
                    var resource = Application.Current.FindResource(value);
                    if (resource is Style)
                    {
                        Style = (Style)resource;
                    }
                }
            }
        }

        protected virtual Style Style
        {
            get { return Control.Style; }
            set { Control.Style = value; }
        }

        static ElementHost()
        {
            StyleManager.Initialize();
        }

        protected ElementHost()
        {
            Control = new T { VerticalAlignment = VerticalAlignment.Stretch, HorizontalAlignment = HorizontalAlignment.Stretch };
            KeyboardNavigation.SetIsTabStop(Control, false);

            host = new ElementHostInternal { Child = Control };
            host.TabStop = false;

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime)
            {
                host.Enabled = false;
            }
            Controls.Add(host);
            host.Dock = DockStyle.Fill;
            Control.PreviewMouseDown += (sender, e) => OnMouseDown(e.ToMouseEventArgs());
            Control.PreviewMouseUp += (sender, e) => OnMouseUp(e.ToMouseEventArgs());
            System.Windows.Interactivity.Interaction.GetBehaviors(Control).AddRangeWithSinglePropertyChangedNotification(new Behavior<FrameworkElement>[] { new ClickBehavior(), new DoubleClickBehavior() });
            Control.AddHandler(ButtonBase.ClickEvent, new RoutedEventHandler((sender, e) =>
                                                                                 {
                                                                                     OnClick(e);
                                                                                     OnMouseClick(new System.Windows.Forms.MouseEventArgs(MouseButtons.Left, 1, (int)Mouse.GetPosition(Control).X, (int)Mouse.GetPosition(Control).Y, 0));
                                                                                 }));
            Control.AddHandler(System.Windows.Controls.Control.MouseDoubleClickEvent, new RoutedEventHandler((sender, e) =>
            {
                OnDoubleClick(e);
                OnMouseDoubleClick(new System.Windows.Forms.MouseEventArgs(MouseButtons.Left, 1, (int)Mouse.GetPosition(Control).X, (int)Mouse.GetPosition(Control).Y, 0));
            }));

        }

        public event MouseButtonEventHandler PreviewMouseDown
        {
            add { Control.PreviewMouseDown += value; }
            remove { Control.PreviewMouseDown -= value; }
        }

        protected override void Dispose(bool disposing)
        {
            DisposeHost();
            base.Dispose(disposing);
        }

        private void DisposeHost()
        {
            if (host != null)
            {
                var fe = host.Child as FrameworkElement;
                if (fe != null && !fe.CheckAccess()) { fe.Dispatcher.Invoke((Action)DisposeHost); return; }

                if (fe != null)
                {
                    // Memory leak workaround: elementHost.Child.SizeChanged -= elementHost.childFrameworkElement_SizeChanged;
                    var handler = (SizeChangedEventHandler)Delegate.CreateDelegate(typeof(SizeChangedEventHandler), host, "childFrameworkElement_SizeChanged");
                    fe.SizeChanged -= handler;
                }
                host.Dispose();
            }
        }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            CreateControl();
        }

        protected override void OnGotFocus(EventArgs e)
        {
            Control.Focus();
            base.OnGotFocus(e);
        }

        #region Nested type: ElementHostInternal

        private class ElementHostInternal : ElementHost
        {
        }

        #endregion
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF ComboBox Control
    /// </summary>
    public class ComboBox : ElementHost<Controls.ComboBox>
    {
        public ComboBox()
        {
            Control.SelectionChanged += OnSelectionChanged;
        }

        [DefaultValue(null)]
        public IEnumerable ItemsSource
        {
            get { return Control.ItemsSource; }
            set { Control.ItemsSource = value; }
        }

        [DefaultValue(null)]
        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }

        [DefaultValue(null)]
        public object SelectedValue
        {
            get { return Control.SelectedValue; }
            set { Control.SelectedValue = value; }
        }

        [DefaultValue(null)]
        public string SelectedValuePath
        {
            get { return Control.SelectedValuePath; }
            set { Control.SelectedValuePath = value; }
        }

        [DefaultValue(0)]
        public int SelectedIndex
        {
            get { return Control.SelectedIndex; }
            set { Control.SelectedIndex = value; }
        }

        [DefaultValue(null)]
        public string DisplayMemberPath
        {
            get { return Control.DisplayMemberPath; }
            set { Control.DisplayMemberPath = value; }
        }

        public event EventHandler<SelectionChangedEventArgs> SelectionChanged;

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, e);
            }
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF TextBox Control
    /// </summary>
    public class TextBox : ElementHost<System.Windows.Controls.TextBox>
    {
        public TextBox()
        {
            Control.TextChanged += delegate { base.OnTextChanged(new EventArgs()); };
        }

        private bool multiline;

        [DefaultValue("")]
        public override string Text
        {
            get { return Control.Text; }
            set { Control.Text = value; }
        }

        [DefaultValue(false)]
        public bool Multiline
        {
            get { return multiline; }
            set
            {
                multiline = value;
                if (value)
                {
                    Control.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                    Control.AcceptsReturn = true;
                    Control.VerticalContentAlignment = VerticalAlignment.Top;
                }
                else
                {
                    Control.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    Control.AcceptsReturn = false;
                    Control.VerticalContentAlignment = VerticalAlignment.Center;
                }
            }
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF OrderableListBox
    /// </summary>
    public class OrderableListBox : ElementHost<Controls.OrderableListBox>
    {
        [DefaultValue(null)]
        public IList ItemsSource
        {
            get { return Control.ItemsSource; }
            set { Control.ItemsSource = value; }
        }

        [DefaultValue(null)]
        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }

        [DefaultValue(null)]
        public object SelectedValue
        {
            get { return Control.SelectedValue; }
            set { Control.SelectedValue = value; }
        }

        [DefaultValue(null)]
        public string SelectedValuePath
        {
            get { return Control.SelectedValuePath; }
            set { Control.SelectedValuePath = value; }
        }

        [DefaultValue(0)]
        public int SelectedIndex
        {
            get { return Control.SelectedIndex; }
            set { Control.SelectedIndex = value; }
        }

        [DefaultValue(null)]
        public string DisplayMemberPath
        {
            get { return Control.DisplayMemberPath; }
            set { Control.DisplayMemberPath = value; }
        }

        public event EventHandler<SelectionChangedEventArgs> SelectionChanged
        {
            add { Control.SelectionChanged += value; }
            remove { Control.SelectionChanged -= value; }
        }

        public event EventHandler<EventArgs> OrderChanged
        {
            add { Control.OrderChanged += value; }
            remove { Control.OrderChanged -= value; }
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF ListBox Control
    /// </summary>
    public class ListBox : ElementHost<System.Windows.Controls.ListBox>
    {
        public ListBox()
        {
            Control.SelectionChanged += OnSelectionChanged;
        }

        [DefaultValue(null)]
        public IEnumerable ItemsSource
        {
            get { return Control.ItemsSource; }
            set { Control.ItemsSource = value; }
        }

        [DefaultValue(null)]
        public IList SelectedItems
        {
            get { return Control.SelectedItems; }
        }

        [DefaultValue(null)]
        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }

        [DefaultValue(null)]
        public object SelectedValue
        {
            get { return Control.SelectedValue; }
            set { Control.SelectedValue = value; }
        }

        [DefaultValue(null)]
        public string SelectedValuePath
        {
            get { return Control.SelectedValuePath; }
            set { Control.SelectedValuePath = value; }
        }

        [DefaultValue(0)]
        public int SelectedIndex
        {
            get { return Control.SelectedIndex; }
            set { Control.SelectedIndex = value; }
        }

        [DefaultValue(SelectionMode.Single)]
        public SelectionMode SelectionMode
        {
            get { return Control.SelectionMode; }
            set { Control.SelectionMode = value; }
        }

        [DefaultValue(null)]
        public string DisplayMemberPath
        {
            get { return Control.DisplayMemberPath; }
            set { Control.DisplayMemberPath = value; }
        }

        public void SelectAll()
        {
            Control.SelectAll();
        }

        public void UnselectAll()
        {
            Control.UnselectAll();
        }

        public event EventHandler<SelectionChangedEventArgs> SelectionChanged;

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, e);
            }
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF Label Control
    /// </summary>
    public sealed class Label : ElementHost<TextBlock>
    {
        private static readonly Style StyleResource = Application.Current.TryFindResource("Label") as Style;

        public Label()
        {
            MarginChanged += OnMarginChanged;
            Control.TextWrapping = TextWrapping.Wrap;
            Style = StyleResource;
        }

        void OnMarginChanged(object sender, EventArgs e)
        {
            Control.Margin = new Thickness(Margin.Left, Margin.Top, Margin.Right, Margin.Bottom);
        }

        public override Color ForeColor
        {
            get { return ((SolidColorBrush)Control.Foreground).Color.ToGdiColor(); }
            set { Control.Foreground = new SolidColorBrush(value.ToWpfColor()); }
        }

        [DefaultValue(TextAlignment.Left)]
        public TextAlignment TextAlignment
        {
            get { return Control.TextAlignment; }
            set { Control.TextAlignment = value; }
        }

        [DefaultValue(TextWrapping.Wrap)]
        public TextWrapping TextWrapping
        {
            get { return Control.TextWrapping; }
            set { Control.TextWrapping = value; }
        }

        [DefaultValue(VerticalAlignment.Stretch)]
        public VerticalAlignment VerticalAlignment
        {
            get { return Control.VerticalAlignment; }
            set { Control.VerticalAlignment = value; }
        }

        [DefaultValue(HorizontalAlignment.Stretch)]
        public HorizontalAlignment HorizontalAlignment
        {
            get { return Control.HorizontalAlignment; }
            set { Control.HorizontalAlignment = value; }
        }

        public override string Text
        {
            get { return Control.Text; }
            set { Control.Text = value; }
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF ToggleButton.
    /// </summary>
    public class ToggleButton : ElementHost<Controls.ToggleButton>
    {
        public ToggleButton()
        {
            Control.Checked += OnChecked;
            Control.Unchecked += OnUnchecked;
        }

        [DefaultValue(false)]
        public bool? IsChecked
        {
            get { return Control.IsChecked; }
            set { Control.IsChecked = value; }
        }

        public override string Text
        {
            get { return base.Text; }
            set
            {
                base.Text = value;
                Control.Content = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(null)]
        public object Content
        {
            get { return Control.Content; }
            set { Control.Content = value; }
        }

        public event EventHandler<EventArgs> Checked;
        public event EventHandler<EventArgs> Unchecked;

        private void OnUnchecked(object sender, EventArgs e)
        {
            if (Unchecked != null)
            {
                Unchecked(this, new EventArgs());
            }
        }

        private void OnChecked(object sender, EventArgs e)
        {
            if (Checked != null)
            {
                Checked(this, new EventArgs());
            }
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF RadioButton Control.
    /// </summary>
    public class RadioButton : ElementHost<Controls.RadioButton>
    {
        public RadioButton()
        {
            Control.Checked += OnChecked;
            Control.Unchecked += OnUnchecked;
        }

        public bool AllowUncheck
        {
            get { return Control.AllowUncheck; }
            set { Control.AllowUncheck = value; }
        }

        /// <summary>
        ///   Gets or sets the name of the group.
        /// </summary>
        /// <value>The index of the group.</value>
        [DefaultValue(null)]
        public string GroupName
        {
            get { return Control.GroupName; }
            set { Control.GroupName = value; }
        }

        [DefaultValue(false)]
        public bool? IsChecked
        {
            get { return Control.IsChecked; }
            set { Control.IsChecked = value; }
        }

        private IEnumerable<RadioButton> ButtonsInGroup
        {
            get
            {
                if (string.IsNullOrEmpty(GroupName) || Parent == null)
                {
                    return null;
                }
                Control parent = Parent;
                while (parent.Parent != null)
                {
                    if (parent is System.Windows.Forms.UserControl || parent is Form)
                    {
                        break;
                    }
                    parent = parent.Parent;
                }

                return GetRadioButtons(parent).Where(i => i.GroupName == GroupName);
            }
        }

        public override string Text
        {
            get { return base.Text; }
            set
            {
                base.Text = value;
                Control.Content = value;
            }
        }

        public event EventHandler<EventArgs> Checked;
        public event EventHandler<EventArgs> Unchecked;

        private void OnUnchecked(object sender, EventArgs e)
        {
            if (Unchecked != null)
            {
                Unchecked(this, new EventArgs());
            }
        }

        private void OnChecked(object sender, EventArgs e)
        {
            if (Checked != null)
            {
                Checked(this, new EventArgs());
            }
            if (ButtonsInGroup != null)
            {
                ButtonsInGroup.Where(i => i != this).ToList().ForEach(i => i.IsChecked = false);
            }
        }

        private IEnumerable<RadioButton> GetRadioButtons(Control control)
        {
            return control is RadioButton ?
                                              new[] { control as RadioButton } :
                                                                                 control.Controls.OfType<RadioButton>().Concat(control.Controls.OfType<Control>().SelectMany(GetRadioButtons));
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF Button Control.
    /// </summary>
    public class Button : ElementHost<System.Windows.Controls.Button>
    {
        public override string Text
        {
            get { return base.Text; }
            set
            {
                base.Text = value;

                Control.Content = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(null)]
        public object Content
        {
            get { return Control.Content; }
            set { Control.Content = value; }
        }

        [DefaultValue(null)]
        public string Tooltip
        {
            get { return Control.ToolTip as string; }
            set { Control.ToolTip = value; }
        }
    }


    /// <summary>
    ///   A Windows Forms wrapper for a WPF Up Button Control.
    /// </summary>
    public class UpButton : ElementHost<Controls.UpButton>
    {
        [DefaultValue(null)]
        public string Tooltip
        {
            get { return Control.ToolTip as string; }
            set { Control.ToolTip = value; }
        }
    }


    /// <summary>
    ///   A Windows Forms wrapper for a WPF Down Button Control.
    /// </summary>
    public class DownButton : ElementHost<Controls.DownButton>
    {
        [DefaultValue(null)]
        public string Tooltip
        {
            get { return Control.ToolTip as string; }
            set { Control.ToolTip = value; }
        }
    }


    /// <summary>
    ///   A Windows Forms wrapper for a WPF Left Button Control.
    /// </summary>
    public class LeftButton : ElementHost<Controls.LeftButton>
    {
        [DefaultValue(null)]
        public string Tooltip
        {
            get { return Control.ToolTip as string; }
            set { Control.ToolTip = value; }
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF Right Button Control.
    /// </summary>
    public class RightButton : ElementHost<Controls.RightButton>
    {
        [DefaultValue(null)]
        public string Tooltip
        {
            get { return Control.ToolTip as string; }
            set { Control.ToolTip = value; }
        }
    }

    /// <summary>
    /// A Windows Forms wrapper for a WPF StateButton Control.
    /// </summary>
    public class StateButton : ElementHost<Controls.StateButton>
    {
        public StateButton()
        {
            Control.Click += delegate { OnClick(new EventArgs()); };
        }

        public override string Text
        {
            get { return base.Text; }
            set
            {
                base.Text = value;
                Control.Content = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(null)]
        public object Content
        {
            get { return Control.Content; }
            set { Control.Content = value; }
        }

        [DefaultValue(null)]
        public string Tooltip
        {
            get { return Control.ToolTip as string; }
            set { Control.ToolTip = value; }
        }

        [DefaultValue(null)]
        public string[] States
        {
            get { return Control.States; }
            set { Control.States = value; }
        }

        [DefaultValue(null)]
        public string CurrentState
        {
            get { return Control.CurrentState; }
            set { Control.CurrentState = value; }
        }

        public event EventHandler<EventArgs> StateChanged
        {
            add { Control.StateChanged += value; }
            remove { Control.StateChanged -= value; }
        }
    }

    /// <summary>
    ///   A styled Windows Forms panel.
    /// </summary>
    public class Panel : System.Windows.Forms.Panel
    {
        internal static readonly Brush DefaultPanelBackgroundBrush = Application.Current.FindResource("DefaultPanelBackground").IfNotNull(r => r as Brush);

        [DefaultValue(false)]
        public bool OverrideBackColor { get; set; }

        protected override void OnHandleCreated(EventArgs e)
        {
            if (!OverrideBackColor && DefaultPanelBackgroundBrush is SolidColorBrush)
            {
                var solidColorBrush = (SolidColorBrush)DefaultPanelBackgroundBrush;
                BackColor = Color.FromArgb(solidColorBrush.Color.A,
                                           solidColorBrush.Color.R,
                                           solidColorBrush.Color.G,
                                           solidColorBrush.Color.B);
            }
            base.OnHandleCreated(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (!OverrideBackColor && DefaultPanelBackgroundBrush is LinearGradientBrush)
            {
                e.Graphics.FillRectangle(DefaultPanelBackgroundBrush.ToGdiBrush(ClientRectangle), ClientRectangle);
            }
            base.OnPaintBackground(e);
        }
    }

    /// <summary>
    /// A Windows Forms wrapper for a ListBoxComboBox.
    /// </summary>
    public class ListBoxComboBox : ElementHost<Controls.ListBoxComboBox>
    {
        [DefaultValue(null)]
        public IEnumerable ItemsSource
        {
            get { return Control.ItemsSource; }
            set { Control.ItemsSource = value; }
        }

        [DefaultValue(null)]
        public string DisplayMemberPath
        {
            get { return Control.DisplayMemberPath; }
            set { Control.DisplayMemberPath = value; }
        }

        [DefaultValue(null)]
        public IList SelectedItems
        {
            get { return Control.SelectedItems; }
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF DatePicker.
    /// </summary>
    public class DatePicker : ElementHost<Controls.DatePicker>
    {
        public event SelectionChangedEventHandler SelectionChanged
        {
            add { Control.SelectionChanged += value; }
            remove { Control.SelectionChanged -= value; }
        }

        [DefaultValue(null)]
        public DateTime? SelectedValue
        {
            get { return Control.SelectedValue; }
            set { Control.SelectedValue = value; }
        }
    }

    /// <summary>
    /// A Windows Forms wrapper for a WPF GridView.
    /// </summary>
    public class GridView : ElementHost<Controls.GridView>
    {
        public GridView()
        {
            Controls[0].Dock = DockStyle.None;
            Control.Loaded += OnControlLoaded;
        }

        private void OnControlLoaded(object sender, RoutedEventArgs e)
        {
            Controls[0].Dock = DockStyle.Fill;
        }

        [DefaultValue(true)]
        public bool AutoGenerateColumns
        {
            get { return Control.AutoGenerateColumns; }
            set { Control.AutoGenerateColumns = value; }
        }

        [DefaultValue(true)]
        public bool AutoGenerateRelations
        {
            get { return Control.AutoGenerateRelations; }
            set { Control.AutoGenerateRelations = value; }
        }

        [DefaultValue(null)]
        public object ItemsSource
        {
            get { return Control.ItemsSource; }
            set { Control.ItemsSource = value; }
        }

        [DefaultValue(null)]
        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }

        public ObservableCollection<object> SelectedItems
        {
            get { return Control.SelectedItems; }
        }

        public event EventHandler SelectionChanged
        {
            add { Control.SelectionChanged += value; }
            remove { Control.SelectionChanged -= value; }
        }

        [DefaultValue(false)]
        public bool IsReadOnly
        {
            get { return Control.IsReadOnly; }
            set { Control.IsReadOnly = value; }
        }

        [DefaultValue(false)]
        public bool IsFilteringAllowed
        {
            get { return Control.IsFilteringAllowed; }
            set { Control.IsFilteringAllowed = value; }
        }

        [DefaultValue(Visibility.Collapsed)]
        public Visibility RowIndicatorVisibility
        {
            get { return Control.RowIndicatorVisibility; }
            set { Control.RowIndicatorVisibility = value; }
        }

        public ObservableCollection<GridViewColumn> Columns
        {
            get { return Control.Columns; }
        }

        public ObservableCollection<GridViewRelation> Relations
        {
            get { return Control.Relations; }
        }
    }

    /// <summary>
    ///   A Windows Forms wrapper for a WPF TabControl
    /// </summary>
    public class TabControl : ElementHost<Controls.TabControl>
    {
        private readonly ObservableCollection<Control> items;
        private readonly ObservableCollection<RadTabItem> itemsSource;

        public TabControl()
        {
            items = new ObservableCollection<Control>();
            Control.ItemsSource = itemsSource = new ObservableCollection<RadTabItem>();
            items.CollectionChanged += OnItemsChanged;
            Control.SelectionChanged += (sender, e) => SelectionChanged(this, e);
        }

        private void OnItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                    itemsSource.Where(c => e.OldItems.OfType<object>().Except(e.NewItems.OfType<object>()).Contains(((WindowsFormsHost)c.Content).Child)).ToList().ForEach(i => itemsSource.Remove(i));
                    break;
                case NotifyCollectionChangedAction.Add:
                    e.NewItems.OfType<Control>().Except((e.OldItems ?? new object[0]).OfType<Control>()).Select(i => new RadTabItem
                    {
                        Content = new WindowsFormsHost { Child = i },
                        Header = HeaderHeight.HasValue ? new TextBlock { Text = i.Text, Padding = new Thickness(5), Height = HeaderHeight.Value } as object : i.Text
                    }).ToList().ForEach(i => itemsSource.Add(i));
                    break;
                case NotifyCollectionChangedAction.Replace:
                    itemsSource.Clear();
                    break;
            }
        }

        public event RoutedEventHandler SelectionChanged = delegate { };

        [DefaultValue(Orientation.Horizontal)]
        public Orientation TabOrientation
        {
            get { return Control.TabOrientation; }
            set { Control.TabOrientation = value; }
        }

        [DefaultValue(System.Windows.Controls.Dock.Bottom)]
        public Dock TabStripPlacement
        {
            get { return Control.TabStripPlacement; }
            set { Control.TabStripPlacement = value; }
        }

        public ICollection<Control> Items
        {
            get { return items; }
        }

        [DefaultValue(null)]
        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }

        [DefaultValue(null)]
        public double? HeaderHeight { get; set; }
    }


    /// <summary>
    /// A Windows Forms wrapper fror a WPF TreeView.
    /// </summary>
    public class TreeView : ElementHost<Controls.TreeView>
    {
        public IEnumerable ItemsSource
        {
            get { return Control.ItemsSource; }
            set { Control.ItemsSource = value; }
        }

        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }

        public object SelectedValue
        {
            get { return Control.SelectedValue; }
        }

        public string SelectedValuePath
        {
            get { return Control.SelectedValuePath; }
            set { Control.SelectedValuePath = value; }
        }

        public string DisplayMemberPath
        {
            get { return Control.DisplayMemberPath; }
            set { Control.DisplayMemberPath = value; }
        }

        public void Expand(string path, string separator = null)
        {
            Control.Expand(path, separator);
        }

        public void ExpandAll()
        {
            Control.ExpandAll();
        }

        public bool IsExpandOnDoubleClickEnabled
        {
            get { return Control.IsExpandOnDoubleClickEnabled; }
            set { Control.IsExpandOnDoubleClickEnabled = value; }
        }

        public bool IsExpandOnSingleClickEnabled
        {
            get { return Control.IsExpandOnSingleClickEnabled; }
            set { Control.IsExpandOnSingleClickEnabled = value; }
        }

        public void CollapseAll()
        {
            Control.CollapseAll();
        }

        public event SelectionChangedEventHandler SelectionChanged
        {
            add { Control.SelectionChanged += value; }
            remove { Control.SelectionChanged -= value; }
        }

        public event EventHandler Collapsed
        {
            add { Control.Collapsed += value; }
            remove { Control.Collapsed -= value; }
        }

        public event EventHandler Expanded
        {
            add { Control.Expanded += value; }
            remove { Control.Expanded -= value; }
        }

        public event EventHandler PreviewCollapsed
        {
            add { Control.PreviewCollapsed += value; }
            remove { Control.PreviewCollapsed -= value; }
        }

        public event EventHandler PreviewExpanded
        {
            add { Control.PreviewExpanded += value; }
            remove { Control.PreviewExpanded -= value; }
        }
    }

    /// <summary>
    /// A base UserControl with additional functionality.
    /// </summary>
    public class UserControl : System.Windows.Forms.UserControl, IInteractionContextAware
    {
        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        /// <value>
        /// The header.
        /// </value>
        public object Header { get; set; }

        [DefaultValue(false)]
        public bool OverrideBackColor { get; set; }

        static UserControl()
        {
            StyleManager.Initialize();
        }

        public UserControl()
        {
            AutoScaleMode = AutoScaleMode.None;
        }

        #region IInteractionContextAware Members

        private IInteractionContext interactionContext;

        public IInteractionContext InteractionContext
        {
            get { return interactionContext ?? this.Parents().OfType<IInteractionContextAware>().Select(i => i.InteractionContext).FirstOrDefault(); }
            set { interactionContext = value; }
        }

        #endregion

        protected override void OnHandleCreated(EventArgs e)
        {
            if (!OverrideBackColor && Panel.DefaultPanelBackgroundBrush is SolidColorBrush)
            {
                var solidColorBrush = (SolidColorBrush)Panel.DefaultPanelBackgroundBrush;
                BackColor = Color.FromArgb(solidColorBrush.Color.A,
                                           solidColorBrush.Color.R,
                                           solidColorBrush.Color.G,
                                           solidColorBrush.Color.B);
            }
            base.OnHandleCreated(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (!OverrideBackColor && Panel.DefaultPanelBackgroundBrush is LinearGradientBrush)
            {
                e.Graphics.FillRectangle(Panel.DefaultPanelBackgroundBrush.ToGdiBrush(ClientRectangle), ClientRectangle);
            }
            base.OnPaintBackground(e);
        }

        protected override void OnParentChanged(EventArgs e)
        {
            this.Suspend();
            base.OnParentChanged(e);
            this.Resume();
        }
    }

    /// <summary>
    /// A UserControl that is bound to a presenter.
    /// </summary>
    public class PresentationControl<TPresenter> : UserControl where TPresenter : class, ILoadable
    {
        /// <summary>
        /// Gets or sets the presenter.
        /// </summary>
        /// <value>
        /// The presenter.
        /// </value>
        public TPresenter Presenter { get; set; }

        public bool CloseOnErrorLoading { get; set; }

        private readonly PresentationBackgroundWorker loadPresenterWorker;

        private bool loadAgain;

        private bool setStatus;

        public PresentationControl()
        {
            loadPresenterWorker = new PresentationBackgroundWorker(this);
            loadPresenterWorker.DoWork += LoadPresenterInternal;
            loadPresenterWorker.RunWorkerCompleted += OnLoadPresenterInternalCompleted;
            CloseOnErrorLoading = true;
        }

        private void OnLoadPresenterInternalCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (setStatus)
            {
                setStatus = false;
                InteractionContext.Status = null;
            }

            if (e.Error != null)
            {
                if (CloseOnErrorLoading)
                {
                    InteractionContext.Complete(false);
                }
                throw e.Error;
            }

            OnPresenterLoaded();

            if (loadAgain)
            {
                loadAgain = false;
                loadPresenterWorker.RunWorkerAsync();
            }
        }

        private void LoadPresenterInternal(object sender, DoWorkEventArgs e)
        {
            Presenter.Load();
        }

        /// <summary>
        /// Loads the presenter in the background.
        /// </summary>
        public virtual void LoadPresenter()
        {
            TPresenter presenter = Presenter;
            if (presenter == null)
            {
                throw new InvalidOperationException("Presenter must be set to load this control.");
            }
            if ((loadPresenterWorker.IsBusy))
            {
                loadAgain = true;
            }
            else
            {
                if (InteractionContext != null && InteractionContext.Status == null)
                {
                    InteractionContext.Status = "Loading...";
                    setStatus = true;
                }
                loadPresenterWorker.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Called when the presenter is finished loading.
        /// </summary>
        protected virtual void OnPresenterLoaded()
        {
        }
    }

    /// <summary>
    /// Helper/extension methods for controls.
    /// </summary>
    public static class WindowsForms
    {
        [ThreadStatic]
        private static HashSet<Control> suspendedControls;

        private static HashSet<Control> SuspendedControls
        {
            get { return suspendedControls ?? (suspendedControls = new HashSet<Control>()); }
        }

        // ReSharper disable InconsistentNaming
        private const int WM_SETREDRAW = 0x000B;
        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Suspends all painting for specified control.
        /// </summary>
        /// <param name="control">The control.</param>
        public static void Suspend(this Control control)
        {
            if (control.Parents().Concat(new[] { control }).Any(c => SuspendedControls.Contains(c))) return;

            SuspendedControls.Add(control);

            Message message = Message.Create(control.Handle, WM_SETREDRAW, IntPtr.Zero,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref message);
        }

        /// <summary>
        /// Resumes all painting for the specified control.
        /// </summary>
        /// <param name="control">The control.</param>
        public static void Resume(this Control control)
        {
            if (!SuspendedControls.Remove(control)) return;

            var intPtr = new IntPtr(1);
            Message message = Message.Create(control.Handle, WM_SETREDRAW, intPtr,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref message);
        }

        /// <summary>
        /// Gets all the parents of this control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        public static IEnumerable<Control> Parents(this Control control)
        {
            control = control.Parent;
            while (control != null)
            {
                yield return control;
                control = control.Parent;
            }
        }

        /// <summary>
        /// Determines whether the UI is ready for the control (not disposed and handle created).
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>
        ///   <c>true</c> if [is UI ready] [the specified control]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsReady(this Control control)
        {
            return control != null && !control.IsDisposed && !control.Disposing && control.IsHandleCreated;
        }
    }
}