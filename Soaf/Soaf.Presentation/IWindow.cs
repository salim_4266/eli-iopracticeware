﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace Soaf.Presentation
{
    /// <summary>
    ///   Standard dialog buttons for a window.
    /// </summary>
    [Flags]
    public enum DialogButtons
    {
        Ok = 1,
        Cancel = 2,
        Yes = 4,
        No = 8,
        Apply = 16
    }

    /// <summary>
    /// Enumeration wrapping the default WindowStartupLocation and extending it with a new value, MousePosition
    /// </summary>
    public enum WindowStartupLocation
    {
        Manual,
        CenterScreen,
        CenterOwner,
        MousePosition
    }

    public enum WindowFrameStyle
    {
        Normal,
        Child,
        None
    }

    /// <summary>
    /// Describes a type that is a window.
    /// </summary>
    public interface IWindow : IInteractionContextAware, INotifyPropertyChanged
    {
        /// <summary>
        /// Shows this instance.
        /// </summary>
        void Show();

        /// <summary>
        /// Shows the window as a dialog.
        /// </summary>
        /// <returns></returns>
        bool? ShowModal();

        /// <summary>
        /// Closes this instance.
        /// </summary>
        void Close();

        /// <summary>
        /// Hides this instance.
        /// </summary>
        void Hide();

        /// <summary>
        /// Occurs when [closing].
        /// </summary>
        event EventHandler<CancelEventArgs> Closing;

        /// <summary>
        /// Occurs when [closed].
        /// </summary>
        event EventHandler<EventArgs> Closed;

        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        ImageSource Icon { get; set; }

        /// <summary>
        /// Gets or sets the window startup location.
        /// </summary>
        /// <value>
        /// The window startup location.
        /// </value>
        WindowStartupLocation WindowStartupLocation { get; set; }

        /// <summary>
        /// Gets or sets the state of the window.
        /// </summary>
        /// <value>
        /// The state of the window.
        /// </value>
        WindowState WindowState { get; set; }

        /// <summary>
        /// Gets or sets the dialog buttons.
        /// </summary>
        /// <value>
        /// The dialog buttons.
        /// </value>
        DialogButtons? DialogButtons { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        Size Size { get; set; }

        /// <summary>
        /// Gets or sets the position of the window's left edge in relation to the desktop
        /// </summary>
        double Left { get; set; }

        /// <summary>
        /// Gets or sets the position of the window's top edge in relation to the desktop
        /// </summary>
        double Top { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show in taskbar].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show in taskbar]; otherwise, <c>false</c>.
        /// </value>
        bool ShowInTaskbar { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance can close.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance can close; otherwise, <c>false</c>.
        /// </value>
        bool CanClose { get; set; }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        object Content { get; set; }

        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        /// <value>
        /// The header.
        /// </value>
        object Header { get; set; }

        /// <summary>
        /// Gets or sets the owner.
        /// </summary>
        /// <value>
        /// The owner.
        /// </value>
        object Owner { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the resize mode.
        /// </summary>
        /// <value>
        /// The resize mode.
        /// </value>
        ResizeMode ResizeMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is enabled.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is enabled; otherwise, <c>false</c>.
        /// </value>
        bool IsEnabled { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is active; otherwise, <c>false</c>.
        /// </value>
        bool IsActive { get; }

        /// <summary>
        /// Gets a value indicating whether this instance has loaded.
        /// </summary>
        bool HasLoaded { get; }

        /// <summary>
        /// Gets or sets a value indicating what style of window frame to display.
        /// </summary>
        WindowFrameStyle WindowFrameStyle { get; set; }

        /// <summary>
        /// Gets a flag indicating whether the window is modal.
        /// </summary>
        bool IsModal { get; }
    }
}