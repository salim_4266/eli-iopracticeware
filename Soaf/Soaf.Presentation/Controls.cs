using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using Soaf.Collections;
using Soaf.Reflection;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionChangedEventHandler = System.Windows.Controls.SelectionChangedEventHandler;
using Dock = System.Windows.Controls.Dock;

// ReSharper disable CheckNamespace
namespace Soaf.Presentation.Controls
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// A wrapper for a content control with a specific type of content.
    /// </summary>
    public abstract class WrappedControl : Control
    {
        protected FrameworkElement Control { get; private set; }

        protected abstract Type ControlType { get; }

        protected WrappedControl()
        {
            DataContext = this;

            Control = (FrameworkElement)Activator.CreateInstance(ControlType);
            const string template =
@"<ControlTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"">
    <ContentPresenter />
</ControlTemplate>";
            using (var ms = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(template)))
            {
                Template = (ControlTemplate)XamlReader.Load(ms);
            }

            Loaded += OnLoaded;
        }

        void OnLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.FindChildByType<ContentPresenter>().Content = Control;
        }
    }

    /// <summary>
    /// A WPF ToggleButton.
    /// </summary>
    public class ToggleButton : WrappedControl
    {
        public event RoutedEventHandler Checked
        {
            add { Control.Checked += value; }
            remove { Control.Checked -= value; }
        }

        public event RoutedEventHandler Unchecked
        {
            add { Control.Unchecked += value; }
            remove { Control.Unchecked -= value; }
        }

        public bool? IsChecked
        {
            get { return Control.IsChecked; }
            set { Control.IsChecked = value; }
        }

        public object Content
        {
            get { return Control.Content; }
            set { Control.Content = value; }
        }

        protected override Type ControlType
        {
            get { return typeof(RadToggleButton); }
        }

        internal new RadToggleButton Control
        {
            get { return (RadToggleButton)base.Control; }
        }
    }

    /// <summary>
    /// A button that alternates through several states.
    /// </summary>
    public class StateButton : Button
    {
        public StateButton()
        {
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (CurrentState.IsNullOrEmpty())
            {
                CurrentState = States == null ? null : States.FirstOrDefault();
            }
        }

        private int stateIndex;

        public event EventHandler<EventArgs> StateChanged;

        protected void OnStateChanged()
        {
            if (StateChanged != null)
            {
                StateChanged(this, new EventArgs());
            }
        }

        private string[] states;
        public string[] States
        {
            get { return states; }
            set
            {
                states = value;
                if (value != null) CurrentState = States.FirstOrDefault();
            }
        }

        public string CurrentState
        {
            get { return States == null || States.Length <= stateIndex ? null : States[stateIndex]; }
            set
            {
                if (States == null) return;
                var index = States.ToList().IndexOf(value);
                if (index >= 0)
                {
                    bool changed = stateIndex != index;
                    stateIndex = index;
                    Content = States[stateIndex];
                    if (changed) OnStateChanged();
                }
            }
        }

        protected override void OnClick()
        {
            base.OnClick();

            if (States.IsNotNullOrEmpty())
            {
                if (stateIndex == States.Length - 1)
                {
                    stateIndex = 0;
                }
                else
                {
                    stateIndex++;
                }
                Content = States[stateIndex];

                OnStateChanged();
            }
        }
    }

    /// <summary>
    ///   A DatePicker.
    /// </summary>
    public class DatePicker : WrappedControl
    {
        protected override Type ControlType
        {
            get { return typeof(RadDatePicker); }
        }

        protected new RadDatePicker Control
        {
            get { return (RadDatePicker)base.Control; }
        }

        public DatePicker()
        {
            // ReSharper disable RedundantNameQualifier
            Control.SelectionChanged += (sender, e) => SelectionChanged(this, new System.Windows.Controls.SelectionChangedEventArgs(
                                                                                  // ReSharper restore RedundantNameQualifier
                                                                                  RadDateTimePicker.SelectionChangedEvent, e.RemovedItems, e.AddedItems));
        }

        public event SelectionChangedEventHandler SelectionChanged = delegate { };

        public DateTime? SelectedValue
        {
            get { return Control.SelectedValue; }
            set { Control.SelectedValue = value; }
        }
    }

    /// <summary>
    /// A data grid view.
    /// </summary>
    public class GridView : WrappedControl
    {
        protected override Type ControlType
        {
            get { return typeof(RadGridView); }
        }

        protected new RadGridView Control
        {
            get { return (RadGridView)base.Control; }
        }

        public GridView()
        {
            Control.EditTriggers = GridViewEditTriggers.CellClick;
            Control.ShowGroupPanel = false;
            IsFilteringAllowed = false;
            ExpandHeirarchyOnClick = true;
            RowIndicatorVisibility = Visibility.Collapsed;
            Columns = new ObservableCollection<GridViewColumn>();
            Columns.CollectionChanged += OnColumnsChanged;
            Relations = new ObservableCollection<GridViewRelation>();
            Relations.CollectionChanged += OnRelationsChanged;
            AutoGenerateRelations = true;
            AutoGenerateColumns = true;

            Control.SetValue(ScrollViewer.HorizontalScrollBarVisibilityProperty, ScrollBarVisibility.Disabled);
            Control.ColumnWidth = new GridViewLength(200, GridViewLengthUnitType.Star, 200, 200);
            Control.AutoGenerateColumns = false;
            Control.DataLoading += OnDataLoading;
            Control.DataLoaded += OnDataLoaded;
            Control.SelectionChanged += OnSelectionChanged;
            Control.PreviewMouseLeftButtonDown += OnMouseLeftButtonDown;
            Control.PreparedCellForEdit += OnPreparedCellForEdit;
        }

        void OnPreparedCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e)
        {
            var comboBox = e.EditingElement as RadComboBox;
            if (comboBox != null)
            {
                comboBox.SelectionChanged -= OnComboBoxSelectionChanged;
                comboBox.SelectionChanged += OnComboBoxSelectionChanged;
            }
        }

        void OnComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cell = ((UIElement)e.OriginalSource).ParentOfType<GridViewCell>();
            if (cell != null) cell.CommitEdit();
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ExpandHeirarchyOnClick)
            {
                var frameworkElement = e.OriginalSource as FrameworkElement;

                // auto expand/collapse on click of expandable read only row
                if (frameworkElement != null)
                {
                    var cell = frameworkElement.ParentOfType<GridViewCell>();
                    var row = frameworkElement.ParentOfType<GridViewRow>();

                    if (row != null && cell != null && row.IsExpandable && (IsReadOnly || cell.Column.IsReadOnly) && (row.ChildDataControls.Count == 0 || row.ChildDataControls.Any(cc => cc.Items.Count > 0)))
                    {
                        row.IsExpanded = !row.IsExpanded;
                    }
                }
            }
        }

        private void OnSelectionChanged(object sender, SelectionChangeEventArgs e)
        {
            SelectionChanged(this, new EventArgs());
        }

        private void OnDataLoading(object sender, GridViewDataLoadingEventArgs e)
        {
            var gridViewDataControl = (GridViewDataControl)sender;

            if (gridViewDataControl.ParentRow == null) return;

            gridViewDataControl.DataLoading -= OnDataLoading;
            gridViewDataControl.DataLoaded -= OnDataLoaded;
            gridViewDataControl.DataLoading += OnDataLoading;
            gridViewDataControl.DataLoaded += OnDataLoaded;

            gridViewDataControl.ShowGroupPanel = false;
            gridViewDataControl.IsFilteringAllowed = IsFilteringAllowed;
            gridViewDataControl.RowIndicatorVisibility = RowIndicatorVisibility;
            gridViewDataControl.IsReadOnly = IsReadOnly;
            gridViewDataControl.EditTriggers = GridViewEditTriggers.CellClick;
            gridViewDataControl.SetValue(ScrollViewer.HorizontalScrollBarVisibilityProperty, ScrollBarVisibility.Disabled);

            var relation = Relations.First(r => r.ParentPropertyName == gridViewDataControl.TableDefinition.Relation.Name);
            if (relation.Columns.Length != 0)
            {
                gridViewDataControl.AutoGenerateColumns = false;
                UpdateColumns(gridViewDataControl.Columns, relation.Columns);
            }

            if (gridViewDataControl.ParentRow.IsExpanded && e.ItemsSource.As<IList>().IfNotNull(i => i.Count, -1) == 0)
            {
                gridViewDataControl.ParentRow.IsExpanded = false;
            }
        }

        private void OnDataLoaded(object sender, EventArgs e)
        {
            ((GridViewDataControl)sender).Columns.OfType<GridViewDataColumn>().ToList().ForEach(UpdateCheckBoxColumnTemplate);
        }

        private void UpdateCheckBoxColumnTemplate(GridViewDataColumn column)
        {
            if (IsReadOnly || !new[] { typeof(bool), typeof(bool?) }.Contains(column.DataType)) return;

            var template =
                string.Format(@"<DataTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"" xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"" xmlns:telerik=""clr-namespace:Telerik.Windows.Controls;assembly=Telerik.Windows.Controls"">
    <CheckBox telerik:StyleManager.Theme=""{0}"">
        <CheckBox.IsChecked>{1}</CheckBox.IsChecked>
    </CheckBox>
</DataTemplate>", Telerik.Windows.Controls.StyleManager.ApplicationTheme, XamlWriter.Save(column.DataMemberBinding));

            using (var ms = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(template)))
            {
                column.CellTemplate = (DataTemplate)XamlReader.Load(ms);
            }
            column.IsReadOnly = true;
        }

        private void OnRelationsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Control.ChildTableDefinitions.Where(i => !Relations.Any(r => r.ParentPropertyName == i.Relation.Name)).ToList().ForEach(i => Control.ChildTableDefinitions.Remove(i));

            Relations.Where(r => !Control.ChildTableDefinitions.Any(i => i.Relation.Name == r.ParentPropertyName)).ToList().ForEach(i => Control.ChildTableDefinitions.Add(new GridViewTableDefinition { Relation = new PropertyRelation(i.ParentPropertyName) { Name = i.ParentPropertyName } }));
        }

        private void OnColumnsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null && e.NewItems != null) e.OldItems.OfType<GridViewColumn>().Except(e.NewItems.OfType<GridViewColumn>()).ForEach(c => c.PropertyChanged -= OnColumnPropertyChanged);
            UpdateColumns(Control.Columns, Columns);
        }

        private void OnColumnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var column = (GridViewColumn)sender;
            UpdateColumn(column, (GridViewBoundColumnBase)Control.Columns[column.Name]);
        }

        private void UpdateColumns(IList<Telerik.Windows.Controls.GridViewColumn> radGridViewColumns, IEnumerable<GridViewColumn> gridViewColumns)
        {
            radGridViewColumns.Where(i => !gridViewColumns.Any(c => c.Name == i.UniqueName)).ToList().ForEach(i => radGridViewColumns.Remove(i));

            radGridViewColumns.AddRangeWithSinglePropertyChangedNotification(gridViewColumns.Where(c => !radGridViewColumns.Any(i => i.UniqueName == c.Name)).ToList().Select(CreateRadColumn));
        }

        private GridViewBoundColumnBase CreateRadColumn(GridViewColumn column)
        {
            column.PropertyChanged -= OnColumnPropertyChanged;

            GridViewBoundColumnBase radColumn;
            if (column is GridViewComboBoxColumn)
            {
                var comboBoxColumn = (GridViewComboBoxColumn)column;
                var radComboBoxColumn =
                    new Telerik.Windows.Controls.
                        GridViewComboBoxColumn
                        {
                            ItemsSource = comboBoxColumn.ItemsSource,
                            DisplayMemberPath = comboBoxColumn.DisplayMemberPath,
                            SelectedValueMemberPath = comboBoxColumn.SelectedValueMemberPath
                        };
                var style = new Style(typeof(RadComboBox));
                style.Setters.Add(new Setter(RadComboBox.OpenDropDownOnFocusProperty, true));
                radComboBoxColumn.EditorStyle = style;

                radColumn = radComboBoxColumn;
            }
            else
            {
                radColumn = new GridViewDataColumn();
            }

            radColumn.EditTriggers = GridViewEditTriggers.CellClick;
            radColumn.TextWrapping = TextWrapping.Wrap;

            UpdateColumn(column, radColumn);

            column.PropertyChanged += OnColumnPropertyChanged;

            return radColumn;
        }

        private void UpdateColumn(GridViewColumn column, GridViewBoundColumnBase radGridViewColumn)
        {
            radGridViewColumn.UniqueName = column.Name;
            radGridViewColumn.Header = column.Header;
            radGridViewColumn.DataMemberBinding = column.Binding;
            radGridViewColumn.IsReadOnly = column.IsReadOnly;
            radGridViewColumn.Width = column.Width.HasValue
                                          ? new GridViewLength(column.Width.Value)
                                          : new GridViewLength(1, GridViewLengthUnitType.Star);
        }

        public bool AutoGenerateColumns { get; set; }

        public bool AutoGenerateRelations { get; set; }

        public bool ExpandHeirarchyOnClick { get; set; }

        public object ItemsSource
        {
            get { return Control.ItemsSource; }
            set
            {
                bool changed = Control.ItemsSource != value;
                Control.ItemsSource = value;
                if (changed)
                {
                    Regenerate();
                }
            }
        }

        private void Regenerate()
        {
            if ((AutoGenerateColumns || AutoGenerateRelations) && ItemsSource != null)
            {
                var manualColumnIndices = Columns.Where(c => !c.IsAutoGenerated).Select((c, i) => new { Column = c, Index = i }).ToList();
                var manualRelationIndices = Relations.Where(r => !r.IsAutoGenerated).Select((r, i) => new { Relation = r, Index = i }).ToList();

                Columns.Where(i => i.IsAutoGenerated).ToList().ForEach(i => Columns.Remove(i));
                Relations.Where(i => i.IsAutoGenerated).ToList().ForEach(i => Relations.Remove(i));

                var elementType = ItemsSource.GetType().FindElementType();

                if (AutoGenerateColumns)
                {
                    GenerateColumns(elementType).ToList().ForEach(i => Columns.Add(i));
                }
                if (AutoGenerateRelations)
                {
                    GenerateRelations(elementType).ToList().ForEach(i => Relations.Add(i));
                }

                manualColumnIndices.ForEach(i =>
                {
                    var radColumn = Control.Columns[i.Column.Name];
                    radColumn.DisplayIndex = Math.Min(Control.Columns.Count - 1, i.Index);
                });

                manualRelationIndices.ForEach(i =>
                {
                    var radRelation = Control.ChildTableDefinitions.FirstOrDefault(td => td.Relation.Name == i.Relation.ParentPropertyName);
                    if (radRelation != null)
                    {
                        Control.ChildTableDefinitions.Remove(radRelation);
                        var index = Math.Min(Control.ChildTableDefinitions.Count - 1, i.Index);
                        if (index == Control.ChildTableDefinitions.Count)
                        {
                            Control.ChildTableDefinitions.Add(radRelation);
                        }
                        else
                        {
                            Control.ChildTableDefinitions.Insert(index, radRelation);
                        }
                    }
                });
            }
        }

        private IEnumerable<GridViewColumn> GenerateColumns(Type type)
        {
            return
                from c in
                    (from p in type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                     where !typeof(IEnumerable).IsAssignableFrom(p.PropertyType) || p.PropertyType == typeof(string)
                     orderby p.GetAttribute<DisplayAttribute>().IfNotNull(a => a.Order, 0)
                     select new GridViewColumn(p) { IsAutoGenerated = true })
                where !Columns.Any(i => i.Name == c.Name)
                select c;
        }

        private IEnumerable<GridViewRelation> GenerateRelations(Type type)
        {
            return
                from r in
                    (from p in type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                     where typeof(IEnumerable).IsAssignableFrom(p.PropertyType) && p.PropertyType != typeof(string)
                     select new GridViewRelation(p.Name, GenerateColumns(p.PropertyType.FindElementType()), GenerateRelations(p.PropertyType.FindElementType())) { IsAutoGenerated = true })
                where !Relations.Any(i => i.ParentPropertyName == r.ParentPropertyName)
                select r;
        }

        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }
        public ObservableCollection<object> SelectedItems
        {
            get { return Control.SelectedItems; }
        }

        public event EventHandler SelectionChanged = delegate { };

        public bool IsReadOnly
        {
            get { return Control.IsReadOnly; }
            set { Control.IsReadOnly = value; }
        }

        public bool IsFilteringAllowed
        {
            get { return Control.IsFilteringAllowed; }
            set { Control.IsFilteringAllowed = value; }
        }

        public Visibility RowIndicatorVisibility
        {
            get { return Control.RowIndicatorVisibility; }
            set { Control.RowIndicatorVisibility = value; }
        }

        public ObservableCollection<GridViewColumn> Columns { get; private set; }

        public ObservableCollection<GridViewRelation> Relations { get; private set; }
    }

    /// <summary>
    /// Represents a heirarchical relation in a grid view.
    /// </summary>
    public class GridViewRelation
    {
        public bool IsAutoGenerated { get; internal set; }

        public string ParentPropertyName { get; private set; }

        public GridViewColumn[] Columns { get; private set; }

        public GridViewRelation[] Relations { get; private set; }

        public GridViewRelation(string parentPropertyName, IEnumerable<GridViewColumn> columns, IEnumerable<GridViewRelation> relations = null)
        {
            ParentPropertyName = parentPropertyName;
            Columns = columns.ToArray();
            Relations = (relations ?? new GridViewRelation[0]).ToArray();
        }
    }

    /// <summary>
    /// Represents a column in a grid view with a combobox.
    /// </summary>
    public class GridViewComboBoxColumn : GridViewColumn
    {
        public IEnumerable ItemsSource { get; private set; }
        public string DisplayMemberPath { get; private set; }
        public string SelectedValueMemberPath { get; private set; }

        public GridViewComboBoxColumn(MemberInfo member, IEnumerable itemsSource, string displayMemberPath, string selectedValueMemberPath)
            : base(member)
        {
            ItemsSource = itemsSource;
            DisplayMemberPath = displayMemberPath;
            SelectedValueMemberPath = selectedValueMemberPath;
        }

        public GridViewComboBoxColumn(Binding binding, object header, string name, bool isReadOnly, IEnumerable itemsSource, string displayMemberPath, string selectedValueMemberPath)
            : base(binding, header, name, isReadOnly)
        {
            ItemsSource = itemsSource;
            DisplayMemberPath = displayMemberPath;
            SelectedValueMemberPath = selectedValueMemberPath;
        }
    }

    /// <summary>
    /// Represents a column in a grid view.
    /// </summary>
    public class GridViewColumn : INotifyPropertyChanged
    {
        public bool IsAutoGenerated { get; internal set; }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                var isChanged = name != value;
                name = value;
                if (isChanged) PropertyChanged(this, new PropertyChangedEventArgs("Name"));
            }
        }

        private object header;
        public object Header
        {
            get { return header; }
            set
            {
                var isChanged = header != value;
                header = value;
                if (isChanged) PropertyChanged(this, new PropertyChangedEventArgs("Header"));
            }
        }

        private double? width;
        public double? Width
        {
            get { return width; }
            set
            {
                var isChanged = width != value;
                width = value;
                if (isChanged) PropertyChanged(this, new PropertyChangedEventArgs("Width"));
            }
        }

        private Binding binding;
        public Binding Binding
        {
            get { return binding; }
            set
            {
                var isChanged = binding != value;
                binding = value;
                if (isChanged) PropertyChanged(this, new PropertyChangedEventArgs("Binding"));
            }
        }

        private bool isReadOnly;
        public bool IsReadOnly
        {
            get { return isReadOnly; }
            set
            {
                var isChanged = isReadOnly != value;
                isReadOnly = value;
                if (isChanged) PropertyChanged(this, new PropertyChangedEventArgs("IsReadOnly"));
            }
        }

        public static IEnumerable<GridViewColumn> For<T>(IEnumerable<T> source, params Expression<Func<T, object>>[] memberExpressions)
        {
            return For(memberExpressions);
        }

        public static IEnumerable<GridViewColumn> For<T>(params Expression<Func<T, object>>[] memberExpressions)
        {
            foreach (var memberExpression in memberExpressions)
            {
                MemberExpression body = null;
                if (memberExpression.Body is MemberExpression)
                {
                    body = (MemberExpression)memberExpression.Body;
                }
                else if (memberExpression.Body is UnaryExpression)
                {
                    body = ((UnaryExpression)memberExpression.Body).Operand as MemberExpression;
                }

                if (body == null) throw new InvalidOperationException("Expression body should be a member expression.");

                yield return new GridViewColumn(body.Member) { Binding = new Binding(body.ToString().Split('.').Skip(1).Join(".")) };
            }
        }

        public GridViewColumn(MemberInfo member)
            : this(
                new Binding(member.Name), member.GetDisplayName(),
            member.Name,
                member.GetAttributes(typeof(ReadOnlyAttribute)).OfType<ReadOnlyAttribute>().Select(i => i.IsReadOnly).FirstOrDefault() || (member is PropertyInfo && !((PropertyInfo)member).CanWrite))
        {


        }

        public GridViewColumn(string bindingPath)
            : this(new Binding(bindingPath))
        {

        }

        public GridViewColumn(Binding binding, object header = null, string name = null, bool isReadOnly = false)
        {
            Binding = binding;
            Header = header ?? name ?? binding.Path.Path;
            Name = name ?? binding.Path.Path;
            IsReadOnly = isReadOnly;
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
    }

    /// <summary>
    /// A Tab Control
    /// </summary>
    public class TabControl : WrappedControl
    {
        public TabControl()
        {
            Control.SelectionChanged += (sender, e) => SelectionChanged(this, e);
        }

        protected override Type ControlType
        {
            get { return typeof(RadTabControl); }
        }

        protected new RadTabControl Control
        {
            get { return (RadTabControl)base.Control; }
        }

        public event RoutedEventHandler SelectionChanged = delegate { };

        public Orientation TabOrientation
        {
            get { return Control.TabOrientation; }
            set { Control.TabOrientation = value; }
        }

        public Dock TabStripPlacement
        {
            get { return Control.TabStripPlacement; }
            set { Control.TabStripPlacement = value; }
        }

        public IEnumerable ItemsSource
        {
            get { return Control.ItemsSource; }
            set { Control.ItemsSource = value; }
        }

        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }
    }

    /// <summary>
    ///   A WPF RadioButton with additional functionality.
    /// </summary>
    public class RadioButton : WrappedControl
    {
        protected override Type ControlType
        {
            get { return typeof(RadRadioButton); }
        }

        protected new RadRadioButton Control
        {
            get { return (RadRadioButton)base.Control; }
        }

        public RadioButton()
        {
            Control.Click += OnClick;
        }

        private void OnClick(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Control.IsChecked.HasValue && Control.IsChecked.Value && AllowUncheck)
            {
                e.Handled = true;
                Control.IsChecked = false;
            }
        }

        public bool AllowUncheck { get; set; }

        public string GroupName
        {
            get { return Control.GroupName; }
            set { Control.GroupName = value; }
        }

        public bool? IsChecked
        {
            get { return Control.IsChecked; }
            set { Control.IsChecked = value; }
        }

        public event RoutedEventHandler Checked
        {
            add { Control.Checked += value; }
            remove { Control.Checked -= value; }
        }

        public event RoutedEventHandler Unchecked
        {
            add { Control.Unchecked += value; }
            remove { Control.Unchecked -= value; }
        }

        public object Content
        {
            get { return Control.Content; }
            set { Control.Content = value; }
        }
    }

    /// <summary>
    /// A WPF ComboBox.
    /// </summary>
    public class ComboBox : WrappedControl
    {
        protected override Type ControlType
        {
            get { return typeof(RadComboBox); }
        }

        protected new RadComboBox Control
        {
            get { return (RadComboBox)base.Control; }
        }

        public ComboBox()
        {
            Control.SelectionChanged += (sender, e) => SelectionChanged(this, new System.Windows.Controls.SelectionChangedEventArgs(
System.Windows.Controls.Primitives.Selector.SelectionChangedEvent,
e.RemovedItems, e.AddedItems));
        }

        public IEnumerable ItemsSource
        {
            get { return Control.ItemsSource; }
            set { Control.ItemsSource = value; }
        }

        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }

        public object SelectedValue
        {
            get { return Control.SelectedValue; }
            set { Control.SelectedValue = value; }
        }

        public string SelectedValuePath
        {
            get { return Control.SelectedValuePath; }
            set { Control.SelectedValuePath = value; }
        }

        public int SelectedIndex
        {
            get { return Control.SelectedIndex; }
            set { Control.SelectedIndex = value; }
        }

        public string DisplayMemberPath
        {
            get { return Control.DisplayMemberPath; }
            set { Control.DisplayMemberPath = value; }
        }

        public event SelectionChangedEventHandler SelectionChanged = delegate { };
    }

    /// <summary>
    /// An item displayed by a TreeView.
    /// </summary>
    public class TreeViewItem
    {
        /// <summary>
        /// Gets or sets the header to display.
        /// </summary>
        /// <value>The header.</value>
        public object Header { get; set; }

        /// <summary>
        /// Gets or sets the items source for child items.
        /// </summary>
        /// <value>The items source.</value>
        public IEnumerable ItemsSource { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is expanded.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is expanded; otherwise, <c>false</c>.
        /// </value>
        public bool IsExpanded { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelected { get; set; }
    }

    /// <summary>
    /// A WPF TreeView.
    /// </summary>
    public class TreeView : WrappedControl
    {
        public TreeView()
        {
            Control.SelectionChanged += (sender, e) => SelectionChanged(this, new System.Windows.Controls.SelectionChangedEventArgs(
System.Windows.Controls.Primitives.Selector.SelectionChangedEvent,
e.RemovedItems, e.AddedItems));

            Control.PreviewExpanded += (sender, e) => PreviewExpanded(this, e);


            Control.PreviewCollapsed += (sender, e) => PreviewCollapsed(this, e);

            Control.Expanded += (sender, e) => Expanded(this, e);

            Control.Collapsed += (sender, e) => Collapsed(this, e);
        }

        protected override Type ControlType
        {
            get { return typeof(RadTreeView); }
        }

        protected new RadTreeView Control
        {
            get { return (RadTreeView)base.Control; }
        }

        public IEnumerable ItemsSource
        {
            get { return Control.ItemsSource; }
            set { Control.ItemsSource = value; }
        }

        public object SelectedItem
        {
            get { return Control.SelectedItem; }
            set { Control.SelectedItem = value; }
        }

        public object SelectedValue
        {
            get { return Control.SelectedValue; }
        }

        public string SelectedValuePath
        {
            get { return Control.SelectedValuePath; }
            set { Control.SelectedValuePath = value; }
        }

        public string DisplayMemberPath
        {
            get { return Control.DisplayMemberPath; }
            set { Control.DisplayMemberPath = value; }
        }

        public void Expand(string path, string separator = null)
        {
            if (separator == null) Control.ExpandItemByPath(path);
            else Control.ExpandItemByPath(path, separator);
        }

        public void ExpandAll()
        {
            Control.ExpandAll();
        }

        public bool IsExpandOnDoubleClickEnabled
        {
            get { return Control.IsExpandOnDblClickEnabled; }
            set { Control.IsExpandOnDblClickEnabled = value; }
        }

        public bool IsExpandOnSingleClickEnabled
        {
            get { return Control.IsExpandOnSingleClickEnabled; }
            set { Control.IsExpandOnSingleClickEnabled = value; }
        }

        public void CollapseAll()
        {
            Control.CollapseAll();
        }

        public event SelectionChangedEventHandler SelectionChanged = delegate { };

        public event EventHandler Collapsed = delegate { };

        public event EventHandler Expanded = delegate { };

        public event EventHandler PreviewCollapsed = delegate { };

        public event EventHandler PreviewExpanded = delegate { };
    }
}