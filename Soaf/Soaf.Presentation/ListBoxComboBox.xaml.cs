﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Soaf.Collections;
using Soaf.Validation;
using Telerik.Windows.Controls;

// ReSharper disable CheckNamespace

namespace Soaf.Presentation.Controls
// ReSharper restore CheckNamespace
{
    public partial class ListBoxComboBox
    {
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(ListBoxComboBox), new PropertyMetadata(null));

        public static readonly DependencyProperty DisplayMemberPathProperty =
            DependencyProperty.Register("DisplayMemberPath", typeof(string), typeof(ListBoxComboBox), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ListBoxComboBox), new PropertyMetadata(string.Empty));

        // Using a DependencyProperty as the backing store for DefaultText.  This enables animation, styling, binding, etc... 
        public static readonly DependencyProperty DefaultTextProperty =
            DependencyProperty.Register("DefaultText", typeof(string), typeof(ListBoxComboBox), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty DefaultTextFontStyleProperty =
            DependencyProperty.Register("DefaultTextFontStyle", typeof(FontStyle), typeof(ListBoxComboBox), new PropertyMetadata(FontStyles.Normal));

        public static readonly DependencyProperty DefaultTextBorderBrushProperty =
            DependencyProperty.Register("DefaultTextBorderBrush", typeof(Brush), typeof(ListBoxComboBox), new PropertyMetadata(new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFd6d4d4"))));

        public static readonly DependencyProperty DefaultTextFontColorProperty =
            DependencyProperty.Register("DefaultTextFontColor", typeof(Brush), typeof(ListBoxComboBox), new PropertyMetadata(new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF808080"))));

        public static readonly DependencyProperty SelectAllButtonContentProperty =
            DependencyProperty.Register("SelectAllButtonContent", typeof(string), typeof(ListBoxComboBox), new PropertyMetadata("Select All"));

        public static readonly DependencyProperty SelectAllButtonContentOnCheckedProperty =
            DependencyProperty.Register("SelectAllButtonContentOnChecked", typeof(string), typeof(ListBoxComboBox), new PropertyMetadata("Deselect All"));

        public static readonly DependencyProperty SelectAllButtonIsCheckedProperty =
            DependencyProperty.Register("SelectAllButtonIsChecked", typeof(bool), typeof(ListBoxComboBox), new PropertyMetadata(false));

        public static readonly DependencyProperty IsSelectAllTextDisplayedOnSelectionProperty =
            DependencyProperty.Register("SelectAllTextIsDisplayedOnSelection", typeof(bool), typeof(ListBoxComboBox), new PropertyMetadata(false));

        public static readonly DependencyProperty SelectAllButtonCommandProperty =
            DependencyProperty.Register("SelectAllButtonCommand", typeof(ICommand), typeof(ListBoxComboBox), new PropertyMetadata(null));

        public static readonly DependencyProperty SelectionModeProperty =
            DependencyProperty.Register("SelectionMode", typeof(SelectionMode), typeof(ListBoxComboBox), new PropertyMetadata(SelectionMode.Multiple, OnSelectionModeChanged));

        public static readonly DependencyProperty IsSelectAllButtonVisibleProperty =
            DependencyProperty.Register("IsSelectAllButtonVisible", typeof(bool), typeof(ListBoxComboBox), new PropertyMetadata(true));

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(ListBoxComboBox), new PropertyMetadata(null, OnPropertySelectedItemChanged));

        public static readonly DependencyProperty IsDropDownOpenProperty =
            DependencyProperty.Register("IsDropDownOpen", typeof(bool), typeof(ListBoxComboBox), new PropertyMetadata(false));

        public static readonly DependencyProperty ListBoxItemWidthProperty =
            DependencyProperty.Register("ListBoxItemWidth", typeof(double?), typeof(ListBoxComboBox));

        public SelectionChangedEventHandler SelectionChanged;
        private ListBox listBox;

        public ListBoxComboBox()
        {
            InitializeComponent();
            SelectionMode = SelectionMode.Multiple;
            ActiveHelper = new MultipleSelectListBoxComboBoxHelper(this);
            IsTabStop = true;
        }

        /// <summary>
        /// Gets or sets the IListBoxComboBoxHelper that aids in MultiSelect or Single select behavior.
        /// </summary>
        public IListBoxComboBoxHelper ActiveHelper { get; set; }

        /// <summary>
        /// Gets or sets the width of each list box item.
        /// If no value set, the items take the width of the ListBoxComboBox instance.
        /// </summary>
        public double? ListBoxItemWidth
        {
            get { return (double?)GetValue(ListBoxItemWidthProperty); }
            set { SetValue(ListBoxItemWidthProperty, value); }
        }

        /// <summary> 
        ///Gets or sets a collection used to generate the content of the ComboBox 
        /// </summary> 
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        /// <summary> 
        ///Gets or sets a collection of selected items.
        /// </summary> 
        public IList SelectedItems
        {
            get { return ActiveHelper.SelectedItems; }
        }

        /// <summary>
        /// Gets or sets the Selected Item for Single Select Mode.
        /// </summary>
        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set
            {
                if (SelectionMode == SelectionMode.Single)
                {
                    SetValue(SelectedItemProperty, value);
                }
                else
                {
                    throw new Exception("SelectedItem is for use in Single Select Mode.  Please refer to the \"SelectedItemsBehavior\".");
                }
            }
        }

        public ICommand SelectAllButtonCommand
        {
            get { return GetValue(SelectAllButtonCommandProperty) as ICommand; }
            set { SetValue(SelectAllButtonCommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets the SelectionMode.
        /// </summary>
        /// <remarks>
        /// Currently supports single and multiple selection modes.  'Extended' selection mode
        /// may produce unexpected behavior.
        /// </remarks>
        public SelectionMode SelectionMode
        {
            get { return (SelectionMode)GetValue(SelectionModeProperty); }
            set { SetValue(SelectionModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets whether the drop down is open.
        /// </summary>
        public bool IsDropDownOpen
        {
            get { return (bool)GetValue(IsDropDownOpenProperty); }
            set { SetValue(IsDropDownOpenProperty, value); }
        }

        /// <summary> 
        ///Gets or sets the display member path for the items.
        /// </summary> 
        public string DisplayMemberPath
        {
            get { return (string)GetValue(DisplayMemberPathProperty); }
            set { SetValue(DisplayMemberPathProperty, value); }
        }

        /// <summary> 
        ///Gets or sets the text displayed in the ComboBox 
        /// </summary> 
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        /// <summary> 
        ///Gets or sets the text displayed in the ComboBox if there are no selected items 
        /// </summary> 
        public string DefaultText
        {
            get { return (string)GetValue(DefaultTextProperty); }
            set { SetValue(DefaultTextProperty, value); }
        }

        /// <summary>
        /// Gets or sets the FontStyle of the text displayed in the ComboBox
        /// </summary>
        public FontStyle DefaultTextFontStyle
        {
            get { return (FontStyle)GetValue(DefaultTextFontStyleProperty); }
            set { SetValue(DefaultTextFontStyleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the content displayed in the "Select All" button.
        /// </summary>
        public string SelectAllButtonContent
        {
            get { return (string)GetValue(SelectAllButtonContentProperty); }
            set { SetValue(SelectAllButtonContentProperty, value); }
        }

        /// <summary>
        /// Gets or sets the content displayed in the "Select All" button when it is checked.
        /// If not set, the content does not change.
        /// </summary>
        public string SelectAllButtonContentOnChecked
        {
            get { return (string)GetValue(SelectAllButtonContentOnCheckedProperty); }
            set { SetValue(SelectAllButtonContentOnCheckedProperty, value); }
        }

        /// <summary>
        /// Gets or sets whether the "Select All" button is checked.
        /// </summary>
        public bool SelectAllButtonIsChecked
        {
            get { return (bool)GetValue(SelectAllButtonIsCheckedProperty); }
            set
            {
                if (SelectionMode != SelectionMode.Single)
                {
                    SetValue(SelectAllButtonIsCheckedProperty, value);
                }
                else
                {
                    throw new Exception("Cannot use \"Select All\" button in Single Select Mode.");
                }
            }
        }

        /// <summary>
        /// Gets or sets whether the content of the "Select All" button is displayed
        /// as the selected text when all items are selected.
        /// </summary>
        public bool SelectAllTextIsDisplayedOnSelection
        {
            get { return (bool)GetValue(IsSelectAllTextDisplayedOnSelectionProperty); }
            set
            {
                if (SelectionMode != SelectionMode.Single)
                {
                    SetValue(IsSelectAllTextDisplayedOnSelectionProperty, value);
                }
                else
                {
                    throw new Exception("Cannot use \"Select All\" button in Single Select Mode.");
                }
            }
        }

        /// <summary>
        /// Gets or sets the border brush of the main ComboBox where the default text is displayed.
        /// </summary>
        /// <remarks>
        /// The regular BorderBrush property will set the BorderBrush of the dropdown list box.
        /// </remarks>
        public Brush DefaultTextBorderBrush
        {
            get { return (Brush)GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }

        /// <summary>
        /// Gets or sets the font color for the default text
        /// </summary>
        public Brush DefaultTextFontColor
        {
            get { return (Brush)GetValue(DefaultTextFontColorProperty); }
            set { SetValue(DefaultTextFontColorProperty, value); }
        }

        public bool IsSelectAllButtonVisible
        {
            get { return (bool)GetValue(IsSelectAllButtonVisibleProperty); }
            set { SetValue(IsSelectAllButtonVisibleProperty, value); }
        }

        private static void OnSelectionModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue.Equals(SelectionMode.Single))
            {
                var listboxComboBox = (ListBoxComboBox)d;
                listboxComboBox.IsSelectAllButtonVisible = false;
            }
        }

        private void OnLoaded(object sender, EventArgs e)
        {
            if (ListBoxItemWidth == null) { ListBoxItemWidth = ActualWidth; }
            if (SelectionMode != SelectionMode.Single) return;

            var helper = new SingleSelectListBoxComboBoxHelper(this);
            ActiveHelper = helper;
            helper.RefreshSelectedItem();
        }

        public void OnListBoxLoaded(object sender, RoutedEventArgs e)
        {
            listBox = (ListBox)sender;
            listBox.SelectionMode = SelectionMode;

            ActiveHelper.OnListBoxLoaded(sender, e);
            if (IsDropDownOpen)
            {
                var item = listBox.ChildrenOfType<ListBoxItem>().FirstOrDefault();
                if (item != null) item.Focus();
                listBox.KeyDown += OnListBoxKeyDown;
            }
        }

        private void OnListBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                MainComboBox.Focus();
            }
        }

        private static void OnPropertySelectedItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var listBoxComboBox = (ListBoxComboBox)d;
            if (listBoxComboBox.ActiveHelper is SingleSelectListBoxComboBoxHelper)
            {
                ((SingleSelectListBoxComboBoxHelper)listBoxComboBox.ActiveHelper).RefreshSelectedItem();
            }
        }

        public void OnListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var handler = SelectionChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
            if (e.Handled) return;

            if (listBox.ItemsSource != null)
            {
                ActiveHelper.OnListBoxSelectionChanged(sender, e);
            }
        }

        private void OnItemLoaded(object sender, RoutedEventArgs e)
        {
            var control = (ContentControl)sender;
            control.SetBinding(ContentProperty, new Binding(DisplayMemberPath));
        }

        public void OnSelectAllChecked(object sender, RoutedEventArgs e)
        {
            var multipleSelectListBoxComboBoxHelper = ActiveHelper as MultipleSelectListBoxComboBoxHelper;
            if (multipleSelectListBoxComboBoxHelper == null) return;

            multipleSelectListBoxComboBoxHelper.OnSelectAllChecked(sender, e);

            var command = SelectAllButtonCommand;
            if (command != null) command.Execute(true);
        }

        public void OnSelectAllUnchecked(object sender, RoutedEventArgs e)
        {
            var multipleSelectListBoxComboBoxHelper = ActiveHelper as MultipleSelectListBoxComboBoxHelper;
            if (multipleSelectListBoxComboBoxHelper == null) return;

            multipleSelectListBoxComboBoxHelper.OnSelectAllUnchecked(sender, e);

            var command = SelectAllButtonCommand;
            if (command != null) command.Execute(false);
        }

        public void OnSelectAllKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            MainComboBox.Focus();
            if (e.Key == Key.Tab)
            {
                if (!Keyboard.Modifiers.Equals(ModifierKeys.Shift))
                {
                    MainComboBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                }
                else
                {
                    MainComboBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
                }
            }
        }

        public event EventHandler DropDownOpened
        {
            add { MainComboBox.DropDownOpened += value; }
            remove { MainComboBox.DropDownOpened -= value; }
        }

        public event EventHandler DropDownClosed
        {
            add { MainComboBox.DropDownClosed += value; }
            remove { MainComboBox.DropDownClosed -= value; }
        }
    }

    #region Helper classes

    public interface IListBoxComboBoxHelper
    {
        IList SelectedItems { get; }
        void OnListBoxLoaded(object sender, RoutedEventArgs e);
        void OnListBoxSelectionChanged(object sender, SelectionChangedEventArgs e);
    }

    public class MultipleSelectListBoxComboBoxHelper : IListBoxComboBoxHelper
    {
        private readonly ListBoxComboBox parent;
        private readonly List<object> itemsToRestore = new List<object>();
        private readonly ObservableCollection<object> selectedItems = new ObservableCollection<object>();
        private bool isSelectedItemsChanging;
        private bool isSelectingAll;
        private ListBox listBox;
        private string originalSelectAllButtonContent;
        [UsedImplicitly]
        private PropertyChangeNotifier itemsSourceNotifier;

        public MultipleSelectListBoxComboBoxHelper(ListBoxComboBox listBoxComboBox)
        {
            parent = listBoxComboBox;
            selectedItems.CollectionChanged += OnSelectedItemsChanged;

            itemsSourceNotifier = new PropertyChangeNotifier(listBoxComboBox, ListBoxComboBox.ItemsSourceProperty).AddValueChanged(OnItemsSourceChanged);
        }

        #region IListBoxComboBoxHelper Members

        public void OnListBoxLoaded(object sender, RoutedEventArgs e)
        {
            listBox = (ListBox)sender;

            originalSelectAllButtonContent = parent.SelectAllButtonContent;

            if (parent.SelectAllButtonIsChecked)
            {
                parent.SelectAllButtonIsChecked = false;
                parent.SelectAllButtonIsChecked = true;
            }
            else
            {
                listBox.SelectedItems.AddRangeWithSinglePropertyChangedNotification(selectedItems);
            }

            SetText();
        }

        public void OnListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isSelectedItemsChanging) return;

            isSelectedItemsChanging = true;

            foreach (object i in listBox.SelectedItems.OfType<object>().ToArray())
            {
                if (!selectedItems.Contains(i))
                {
                    selectedItems.Add(i);
                }
            }

            foreach (object i in selectedItems.OfType<object>().ToArray())
            {
                if (!listBox.SelectedItems.Contains(i))
                {
                    selectedItems.Remove(i);
                }
            }

            if (selectedItems.Count == listBox.ItemsSource.Cast<object>().Count() && !parent.SelectAllButtonIsChecked)
            {
                parent.SelectAllButtonIsChecked = true;
            }

            isSelectedItemsChanging = false;

            if (selectedItems.Count != listBox.ItemsSource.Cast<object>().Count() && parent.SelectAllButtonIsChecked && !isSelectingAll)
            {
                itemsToRestore.AddRange(selectedItems);
                parent.SelectAllButtonIsChecked = false;
            }

            SetText();
        }

        public IList SelectedItems
        {
            get { return selectedItems; }
        }


        #endregion

        private void OnItemsSourceChanged(object sender, EventArgs e)
        {
            // Allow the SelectAllButton to be checked AFTER ItemSource is set.
            if (parent.SelectAllButtonIsChecked)
            {
                parent.SelectAllButtonIsChecked = false;
                parent.SelectAllButtonIsChecked = true;
            }

            SetText();
        }

        private void OnSelectedItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (isSelectedItemsChanging || parent.ItemsSource == null || listBox == null) return;

            isSelectedItemsChanging = true;

            foreach (object i in listBox.SelectedItems.OfType<object>().ToArray())
            {
                if (!selectedItems.Contains(i))
                {
                    listBox.SelectedItems.Remove(i);
                }
            }

            foreach (object i in selectedItems.OfType<object>().ToArray())
            {
                if (!listBox.SelectedItems.Contains(i))
                {
                    listBox.SelectedItems.Add(i);
                }
            }

            if (selectedItems.Count == parent.ItemsSource.Cast<object>().Count() && !parent.SelectAllButtonIsChecked)
            {
                parent.SelectAllButtonIsChecked = true;
            }

            isSelectedItemsChanging = false;

            if (selectedItems.Count != parent.ItemsSource.Cast<object>().Count() && parent.SelectAllButtonIsChecked && !isSelectingAll)
            {
                itemsToRestore.AddRange(selectedItems);
                parent.SelectAllButtonIsChecked = false;
            }

            SetText();
        }

        /// <summary> 
        ///Set the text property of this control (bound to the ContentPresenter of the ComboBox) 
        /// </summary> 
        private void SetText()
        {
            if (parent.SelectAllTextIsDisplayedOnSelection && parent.ItemsSource != null && selectedItems.Count == parent.ItemsSource.Cast<object>().Count())
            {
                parent.Text = parent.SelectAllButtonContent;
            }
            else
            {
                parent.Text = parent.ItemsSource != null &&
                               selectedItems != null &&
                               selectedItems.Count > 0 ? parent.ItemsSource.OfType<object>().
                                                             Where(i => selectedItems.Contains(i)).
                                                             Select(i => new Binding(parent.DisplayMemberPath) { Source = i }.Evaluate().IfNotNull(x => x.ToString())).
                                                             Join()
                                   : parent.DefaultText;
            }

            // set DefaultText if nothing else selected 
            if (string.IsNullOrEmpty(parent.Text))
            {
                parent.Text = parent.DefaultText;
            }
        }

        [UsedImplicitly]
        public void OnSelectAllChecked(object sender, RoutedEventArgs e)
        {
            if (parent.ItemsSource == null) return;

            isSelectingAll = true;
            foreach (object item in parent.ItemsSource)
            {
                if (!selectedItems.Contains(item))
                {
                    selectedItems.Add(item);
                }
            }

            if (parent.SelectAllButtonContentOnChecked != null)
            {
                parent.SelectAllButtonContent = parent.SelectAllButtonContentOnChecked;
            }

            isSelectingAll = false;
        }

        [UsedImplicitly]
        public void OnSelectAllUnchecked(object sender, RoutedEventArgs e)
        {
            if (selectedItems.IsNotNullOrEmpty())
            {
                selectedItems.Clear();
            }

            if (itemsToRestore.Any())
            {
                selectedItems.AddRangeWithSinglePropertyChangedNotification(itemsToRestore);
                itemsToRestore.Clear();
            }

            parent.SelectAllButtonContent = originalSelectAllButtonContent;
        }
    }

    public class SingleSelectListBoxComboBoxHelper : IListBoxComboBoxHelper
    {
        private readonly ListBoxComboBox parent;
        private ListBox listBox;

        public SingleSelectListBoxComboBoxHelper(ListBoxComboBox listBoxComboBox)
        {
            parent = listBoxComboBox;
        }

        #region IListBoxComboBoxHelper Members

        public void OnListBoxLoaded(object sender, RoutedEventArgs e)
        {
            listBox = (ListBox)sender;
            RefreshSelectedItem();
        }

        public void OnListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            parent.SelectedItem = listBox.SelectedItem;
            SetText();
            parent.IsDropDownOpen = false;
        }

        public IList SelectedItems
        {
            get { return new List<object> { parent.SelectedItem }; }
        }

        #endregion

        public void RefreshSelectedItem()
        {
            if (listBox != null)
            {
                listBox.SelectedItem = parent.SelectedItem;
                SetText();
            }
        }

        private void SetText()
        {
            if (parent.SelectedItem != null)
            {
                var displayMemberPathBinding = new Binding(parent.DisplayMemberPath) { Source = parent.SelectedItem };
                parent.Text = displayMemberPathBinding.Evaluate().IfNotNull(x => x.ToString());
            }
            else
            {
                parent.Text = parent.DefaultText;
            }
        }
    }

    #endregion

    #region Converters

    public class SingleSelectModeToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var selectionMode = (SelectionMode)value;
            return (selectionMode == SelectionMode.Single) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class DefaultTextFontColorConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values != null && values.Count() == 3)
            {
                var defaultTextFontColor = values[0] as Brush;
                var defaultText = values[1] as string;
                var text = values[2] as string;

                return defaultText == text ? defaultTextFontColor : Brushes.Black;
            }

            return Brushes.Black;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class ShowSelectionBackgroundConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values != null && values.Count() == 2)
            {
                if (!(bool)values[0] && (bool)values[1]) return true;
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class ShowWhiteTextConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
            {
                return Brushes.White;
            }
            return Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    #endregion
}