﻿using System;
using System.Windows;
using System.Windows.Input;

// ReSharper disable CheckNamespace

namespace Soaf.Presentation.Controls
// ReSharper restore CheckNamespace
{
    /// <summary>
    ///   Interaction logic for DialogButtonsControl.xaml
    /// </summary>
    public partial class DialogButtonsControl
    {
        /// <summary>
        ///   Gets or sets the bound interaction context.
        /// </summary>
        public static readonly DependencyProperty InteractionContextProperty = DependencyProperty.Register("InteractionContext", typeof(IInteractionContext), typeof(DialogButtonsControl), new PropertyMetadata(null));

        public static readonly DependencyProperty DialogButtonsProperty = DependencyProperty.Register("DialogButtons", typeof(DialogButtons), typeof(DialogButtonsControl), new PropertyMetadata(DialogButtons.Cancel, OnDialogButtonsChanged));

        public static readonly DependencyProperty HasDefaultButtonProperty = DependencyProperty.Register("HasDefaultButton", typeof(bool), typeof(DialogButtonsControl), new PropertyMetadata(true));

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(ICommand), typeof(DialogButtonsControl), new PropertyMetadata(null));

        public DialogButtonsControl()
        {
            InitializeComponent();

            OkButton.Command = Soaf.Presentation.Command.Create(() => ProcessDialogButton(true), () => CanExecuteCommand(true));
            CancelButton.Command = Soaf.Presentation.Command.Create(() => ProcessDialogButton(false), () => CanExecuteCommand(false));
            ApplyButton.Command = Soaf.Presentation.Command.Create(() => ProcessDialogButton(true, false), () => CanExecuteCommand(true));
            YesButton.Command = Soaf.Presentation.Command.Create(() => ProcessDialogButton(true), () => CanExecuteCommand(true));
            NoButton.Command = Soaf.Presentation.Command.Create(() => ProcessDialogButton(false), () => CanExecuteCommand(false));

            KeyDown += OnKeyDown;
        }

        public DialogButtons DialogButtons
        {
            get { return (DialogButtons)GetValue(DialogButtonsProperty); }
            set { SetValue(DialogButtonsProperty, value); }
        }

        public bool HasDefaultButton
        {
            get { return (bool)GetValue(HasDefaultButtonProperty); }
            set { SetValue(HasDefaultButtonProperty, value); }
        }

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        /// <summary>
        ///   Gets or sets the bound interaction context.
        /// </summary>
        /// <value> The interaction context. </value>
        public IInteractionContext InteractionContext
        {
            get { return (IInteractionContext)GetValue(InteractionContextProperty); }
            set { SetValue(InteractionContextProperty, value); }
        }

        private bool CanExecuteCommand(bool value)
        {
            return Command == null || Command.CanExecute(value);
        }

        private static void OnDialogButtonsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dialogButtons = (DialogButtons)e.NewValue;
            var control = (DialogButtonsControl)d;
            control.OkButton.Visibility = dialogButtons.HasFlag(DialogButtons.Ok) ? Visibility.Visible : Visibility.Collapsed;
            control.CancelButton.Visibility = dialogButtons.HasFlag(DialogButtons.Cancel) ? Visibility.Visible : Visibility.Collapsed;
            control.ApplyButton.Visibility = dialogButtons.HasFlag(DialogButtons.Apply) ? Visibility.Visible : Visibility.Collapsed;
            control.YesButton.Visibility = dialogButtons.HasFlag(DialogButtons.Yes) ? Visibility.Visible : Visibility.Collapsed;
            control.NoButton.Visibility = dialogButtons.HasFlag(DialogButtons.No) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (HasDefaultButton && (DialogButtons.HasFlag(DialogButtons.Ok) || DialogButtons.HasFlag(DialogButtons.Yes))) ProcessDialogButton(true);
        }

        public event EventHandler<EventArgs<bool?>> DialogButtonClicked;

        private void ProcessDialogButton(bool value, bool complete = true)
        {
            if (Command != null) Command.Execute(value);

            DialogButtonClicked.Fire(this, value);

            if (InteractionContext != null && complete) InteractionContext.Complete(value);
        }
    }
}