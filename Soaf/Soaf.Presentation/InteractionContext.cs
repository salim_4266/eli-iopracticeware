﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using Soaf.ComponentModel;

namespace Soaf.Presentation
{
    public interface IInteractionContext<T> : IInteractionContext
    {
        [Dependency]
        T Context { get; set; }
    }

    internal class InteractionContext<T> : InteractionContext, IInteractionContext<T>
    {
        public T Context { get; set; }
    }

    /// <summary>
    ///   A default implementation of IInteractionContext.
    /// </summary>
    [CreationContextLifestyle]
    internal class InteractionContext : IInteractionContext
    {
        private bool isBusy;
        private object status;
        private bool completeOnNotBusy;
        private readonly IMessenger defaultMessenger;
        private readonly ObservableCollection<object> busyComponents;

        public InteractionContext()
        {
            Messenger = defaultMessenger = new Messenger();
            busyComponents = new ObservableCollection<object>();
            busyComponents.CollectionChanged += delegate { IsBusy = busyComponents.Count > 0; };
        }

        #region IInteractionContext Members

        public bool IsComplete { get; private set; }

        public bool? DialogResult { get; private set; }

        public bool IsBusy
        {
            get { return isBusy; }
            private set
            {
                bool isChanged = isBusy != value;
                isBusy = value;
                if (isChanged) PropertyChanged.Fire(this, "IsBusy");

                if (completeOnNotBusy && !IsComplete)
                {
                    IsComplete = true;

                    AfterCompleted(this, new EventArgs<bool?>(DialogResult));

                    Dispose();
                }
            }
        }

        public object Status
        {
            get { return status; }
            set
            {
                bool isChanged = status != value;
                status = value;
                if (isChanged) PropertyChanged.Fire(this, "Status");
            }
        }

        public IList<object> BusyComponents
        {
            get { return busyComponents; }
        }

        public bool Complete(bool? dialogResult)
        {
            if (IsComplete) throw new InvalidOperationException("The interaction has already completed.");

            #region helper func completeFunc - holds the body of work

            Func<bool> completeFunc = () =>
                                          {
                                              DialogResult = dialogResult;

                                              var cancelArgs = new CancelEventArgs();
                                              Completing(this, cancelArgs);
                                              if (cancelArgs.Cancel) return false;

                                              Completed(this, new EventArgs<bool?>(DialogResult));

                                              if (IsBusy)
                                              {
                                                  // wait until not busy to complete
                                                  completeOnNotBusy = true;
                                                  return false;
                                              }

                                              IsComplete = true;

                                              AfterCompleted(this, new EventArgs<bool?>(DialogResult));

                                              Dispose();

                                              return true;
                                          };

            #endregion

            if (Application.Current.Dispatcher.Thread.ManagedThreadId != Dispatcher.CurrentDispatcher.Thread.ManagedThreadId)
            {
                bool returnValue = false;
                var dispatcherOperation = Application.Current.Dispatcher.BeginInvoke(() => returnValue = completeFunc());
                dispatcherOperation.Wait();
                return returnValue;
            }
            return completeFunc();
        }

        public event EventHandler<CancelEventArgs> Completing = delegate { };
        public event EventHandler<EventArgs<bool?>> Completed = delegate { };

        public virtual IMessenger Messenger { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public event EventHandler<EventArgs<bool?>> AfterCompleted = delegate { };

        public void Dispose()
        {
            defaultMessenger.Dispose();
        }
    }
}