﻿using System;
using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Soaf.Presentation.Controls
{
    /// <summary>
    ///   Interaction logic for OrderableListBox.xaml
    /// </summary>
    public partial class OrderableListBox : UserControl
    {
        public OrderableListBox()
        {
            InitializeComponent();

            ListBox.SelectionChanged += OnSelectionChanged;

            UpButton.IsEnabled = false;
            DownButton.IsEnabled = false;
        }

        public IList ItemsSource
        {
            get { return (IList)ListBox.ItemsSource; }
            set { ListBox.ItemsSource = value; }
        }

        public object SelectedItem
        {
            get { return ListBox.SelectedItem; }
            set { ListBox.SelectedItem = value; }
        }

        public object SelectedValue
        {
            get { return ListBox.SelectedValue; }
            set { ListBox.SelectedValue = value; }
        }

        public string SelectedValuePath
        {
            get { return ListBox.SelectedValuePath; }
            set { ListBox.SelectedValuePath = value; }
        }

        public int SelectedIndex
        {
            get { return ListBox.SelectedIndex; }
            set { ListBox.SelectedIndex = value; }
        }

        public string DisplayMemberPath
        {
            get { return ListBox.DisplayMemberPath; }
            set { ListBox.DisplayMemberPath = value; }
        }

        private void OnDownClick(object sender, RoutedEventArgs e)
        {
            object item = ListBox.SelectedItem;
            if (item != null && ItemsSource != null && item != ItemsSource.OfType<object>().LastOrDefault())
            {
                int index = ItemsSource.IndexOf(item) + 1;
                ItemsSource.Remove(item);

                ItemsSource.Insert(index, item);

                Refresh();
                
                ListBox.SelectedItem = item;

                OnOrderChanged();
            }
        }

        private void OnUpClick(object sender, RoutedEventArgs e)
        {
            object item = ListBox.SelectedItem;
            if (item != null && ItemsSource != null && item != ItemsSource.OfType<object>().FirstOrDefault())
            {
                int index = ItemsSource.IndexOf(item) - 1;
                ItemsSource.Remove(item);

                ItemsSource.Insert(index, item);

                Refresh();

                ListBox.SelectedItem = item;

                OnOrderChanged();                
            }
        }

        private void Refresh()
        {
            ListBox.Items.Refresh();
            UpdateButtonsEnabled();
        }

        public event EventHandler<EventArgs> OrderChanged;

        private void OnOrderChanged()
        {
            if (OrderChanged != null)
            {
                OrderChanged(this, new EventArgs());
            }
        }

        public event EventHandler<SelectionChangedEventArgs> SelectionChanged;

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, e);
            }

            UpdateButtonsEnabled();
        }

        private void UpdateButtonsEnabled()
        {
            UpButton.IsEnabled = false;
            DownButton.IsEnabled = false;

            if (ListBox.ItemsSource != null && ListBox.ItemsSource.OfType<object>().Count() > 0 && ListBox.SelectedItem != null)
            {
                if (ListBox.SelectedItem != ListBox.ItemsSource.OfType<object>().Last())
                {
                    DownButton.IsEnabled = true;
                }
                if (ListBox.SelectedItem != ListBox.ItemsSource.OfType<object>().First())
                {
                    UpButton.IsEnabled = true;
                }
            }
        }
    }
}