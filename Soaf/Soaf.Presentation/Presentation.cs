﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Transactions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Logging;
using Soaf.Presentation;
using Soaf.Presentation.Interop;
using Soaf.Reflection;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Data.DataForm;
using BackgroundWorker = Soaf.ComponentModel.BackgroundWorker;
using IWindow = Soaf.Presentation.IWindow;
using System.Resources;
using System.Threading.Tasks;
using Application = System.Windows.Application;
using Binding = System.Windows.Data.Binding;
using ButtonBase = System.Windows.Controls.Primitives.ButtonBase;
using Control = System.Windows.Controls.Control;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;
using TextBox = System.Windows.Controls.TextBox;
using UserControl = System.Windows.Controls.UserControl;

[assembly: Component(typeof(WindowInteractionManager), typeof(IInteractionManager))]
[assembly: Component(typeof(InteractionContext), typeof(IInteractionContext))]
[assembly: Component(typeof(InteractionContext<>), typeof(IInteractionContext<>), typeof(InteractionContext))]
[assembly: Component(typeof(Soaf.Presentation.Window), typeof(IWindow))]

[assembly: XmlnsDefinition("http://soaf/presentation", "Soaf.Presentation")]
[assembly: XmlnsDefinition("http://soaf/presentation", "Soaf.Presentation.Controls")]

namespace Soaf.Presentation
{
    public static class Binder
    {
        private static readonly DependencyProperty Property = DependencyProperty.RegisterAttached("Property", typeof(Object), typeof(DependencyObject), new PropertyMetadata(null));

        public static Object GetValue(Object container, String expression)
        {
            var binding = new Binding(expression) { Source = container };
            var o = new BindingDependencyObject();
            BindingOperations.SetBinding(o, Property, binding);
            return o.GetValue(Property);
        }

        public static void SetValue(object target, string expression, object value)
        {
            var binding = new Binding(expression) { Source = target, Mode = BindingMode.TwoWay };
            var o = new BindingDependencyObject();
            BindingOperations.SetBinding(o, Property, binding);
            o.SetValue(Property, value);
        }

        #region Nested type: BindingDependencyObject

        private class BindingDependencyObject : DependencyObject
        {
        }

        #endregion
    }

    /// <summary>
    /// Extensions for presentation framework.
    /// </summary>
    public static partial class Presentation
    {
        /// <summary>
        /// Sets the InteractionContext as busy.
        /// </summary>
        /// <param name="interactionContext">The interaction context.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public static IInteractionContext SetBusy(this IInteractionContext interactionContext, bool value, object status = null)
        {
            if (value) interactionContext.BusyComponents.Add(true);
            if (interactionContext.BusyComponents.Count == 1 || status != null) interactionContext.Status = status;

            return interactionContext;
        }

        public static IEnumerable<DependencyObject> Parents(this UIElement element)
        {
            DependencyObject current = VisualTreeHelper.GetParent(element);
            while (current != null)
            {
                yield return current;
                current = current.As<UIElement>().IfNotNull(fe => VisualTreeHelper.GetParent(fe));
            }
        }

        /// <summary>
        ///   Performs the specified loadable's load actions in parallels
        /// </summary>
        /// <param name = "loadable">The loadable.</param>
        public static void Load(this ILoadable loadable)
        {
            if (loadable.LoadActions == null) return;

            loadable.LoadActions.ForAllInParallel(a => a());
        }
    }

    /// <summary>
    /// Represents a wrapped item that can be selected.
    /// </summary>
    public interface ISelectableItem
    {
        object Item { get; set; }
        bool IsSelected { get; set; }
    }

    /// <summary>
    /// Represents a wrapped item that can be selected.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SelectableItem<T> : ISelectableItem, INotifyPropertyChanged, IEditableObjectExtended, ICustomTypeDescriptor
    {
        private bool isSelected;

        public SelectableItem(T item)
        {
            Item = item;
        }

        public T Item
        {
            get
            {
                var item = ((ISelectableItem)this).Item;
                return item == null ? default(T) : (T)item;
            }
            set
            {
                var propertyChanged = Item as INotifyPropertyChanged;
                if (propertyChanged != null) propertyChanged.PropertyChanged -= OnItemPropertyChanged;

                bool isChanged = !Equals(Item, value);
                ((ISelectableItem)this).Item = value;

                propertyChanged = value as INotifyPropertyChanged;
                if (propertyChanged != null) propertyChanged.PropertyChanged += OnItemPropertyChanged;

                if (isChanged) OnPropertyChanged("Item");
            }
        }

        private void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged.Fire(Item, e.PropertyName);
        }

        private void OnPropertyChanged(string name)
        {
            PropertyChanged.Fire(this, name);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        #endregion

        #region ISelectableItem Members

        object ISelectableItem.Item { get; set; }

        [Description("")]
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                bool isChanged = value != isSelected;
                isSelected = value;
                if (isChanged) OnPropertyChanged("IsSelected");
            }
        }

        #endregion

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(Item);
        }

        public string GetClassName()
        {
            return TypeDescriptor.GetClassName(Item);
        }

        public string GetComponentName()
        {
            return TypeDescriptor.GetComponentName(Item);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(Item);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(Item);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(Item);
        }

        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(Item, editorBaseType);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(Item);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(Item, attributes);
        }

        public PropertyDescriptorCollection GetProperties()
        {
            return new PropertyDescriptorCollection(
                TypeDescriptor.GetProperties(Item).OfType<PropertyDescriptor>().Select(p => new BasicPropertyDescriptor(p, null, c => c.As<SelectableItem<T>>().IfNotDefault(i => i.Item as object) ?? c))
                .Concat(TypeDescriptor.GetProperties(this).OfType<PropertyDescriptor>())
                .ToArray());
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return new PropertyDescriptorCollection(TypeDescriptor.GetProperties(Item, attributes).OfType<PropertyDescriptor>()
                .Select(p => new BasicPropertyDescriptor(p, null, c => c.As<SelectableItem<T>>().IfNotDefault(i => i.Item as object) ?? c))
                .Concat(TypeDescriptor.GetProperties(this, attributes).OfType<PropertyDescriptor>()).ToArray());
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return TypeDescriptor.GetProperties(Item).Contains(pd) ? Item as object : this;
        }

        public void AcceptChanges()
        {
            Item.As<IChangeTracking>().IfNotNull(i => i.AcceptChanges());
        }

        public bool IsChanged
        {
            get { return Item.As<IChangeTracking>().IfNotNull(i => i.IsChanged); }
        }

        public bool IsEditing
        {
            get { return Item.As<IEditableObjectExtended>().IfNotNull(i => i.IsEditing); }
        }

        public bool CascadeEditingEventsToChildren
        {
            get { return Item.As<IEditableObjectExtended>().IfNotNull(i => i.CascadeEditingEventsToChildren); }
            set { Item.As<IEditableObjectExtended>().IfNotNull(i => i.CascadeEditingEventsToChildren = value); }
        }

        public void BeginEdit()
        {
            Item.As<IEditableObject>().IfNotNull(i => i.BeginEdit());
        }

        public void EndEdit()
        {
            Item.As<IEditableObject>().IfNotNull(i => i.EndEdit());
        }

        public void CancelEdit()
        {
            Item.As<IEditableObject>().IfNotNull(i => i.CancelEdit());
        }

        public void BeginNamedEdit(string name)
        {
            Item.As<IEditableObjectExtended>().IfNotNull(i => i.BeginNamedEdit(name));
        }

        public void EndNamedEdit(string name)
        {
            Item.As<IEditableObjectExtended>().IfNotNull(i => i.EndNamedEdit(name));
        }

        public void CancelNamedEdit(string name)
        {
            Item.As<IEditableObjectExtended>().IfNotNull(i => i.CancelNamedEdit(name));
        }

        public Dictionary<string, object> GetOriginalValues()
        {
            return Item.As<IEditableObjectExtended>().IfNotNull(i => i.GetOriginalValuesForNamedEdit(string.Empty));
        }

        public Dictionary<string, object> GetOriginalValuesForNamedEdit(string name)
        {
            return Item.As<IEditableObjectExtended>().IfNotNull(i => i.GetOriginalValuesForNamedEdit(name));
        }

        public static implicit operator T(SelectableItem<T> i)
        {
            return i.Item;
        }

        public static implicit operator SelectableItem<T>(T i)
        {
            return new SelectableItem<T>(i);
        }

        public static bool operator ==(SelectableItem<T> x, object y)
        {
            return Equals(x.IfNotDefault(i => i.Item), y.As<SelectableItem<T>>().IfNotDefault(i => i.Item as object) ?? y);
        }

        public static bool operator !=(SelectableItem<T> x, object y)
        {
            return !Equals(x.IfNotDefault(i => i.Item), y.As<SelectableItem<T>>().IfNotDefault(i => i.Item as object) ?? y);
        }

        public override bool Equals(object obj)
        {
            return Item.Equals(obj.As<SelectableItem<T>>().IfNotDefault(i => i.Item as object) ?? obj);
        }

        public override int GetHashCode()
        {
            return Item.GetHashCode();
        }
    }

    public static class Selectable
    {
        public static IEnumerable<SelectableItem<T>> AsSelectableItems<T>(this IEnumerable<T> source)
        {
            foreach (var item in source)
            {
                yield return new SelectableItem<T>(item);
            }
        }

        public static IEnumerable<T> AsItems<T>(this IEnumerable<SelectableItem<T>> source)
        {
            foreach (var item in source)
            {
                yield return item.Item;
            }
        }

        public static SelectableItem<T> For<T>(T source)
        {
            return new SelectableItem<T>(source);
        }
    }

    internal class ValueDependencyObject : DependencyObject
    {
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(ValueDependencyObject), new PropertyMetadata(null));

        public object Value
        {
            get { return GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
    }

    /// <summary>
    /// Extension/helper methods.
    /// </summary>
    public static partial class Presentation
    {
        public static object Evaluate(this Binding binding)
        {
            var d = new ValueDependencyObject();
            BindingOperations.SetBinding(d, ValueDependencyObject.ValueProperty, binding);
            return d.Value;
        }

        public static Dispatcher GetDispatcher(this Application application)
        {
            return application.Dispatcher;
        }
    }

    /// <summary>
    ///   Manages applicaton themes and styles.
    /// </summary>
    public static class StyleManager
    {
        internal static readonly ResourceDictionary StyleResources;

        static StyleManager()
        {
            using (new TimedScope(s => Debug.WriteLine("Initialized StyleManager in {0}.".FormatWith(s))))
            {
                Telerik.Windows.Controls.StyleManager.ApplicationTheme = new Windows8Theme();
                // force load of Telerik assemblies
                new[] { typeof(RadTabControl), typeof(RadGridView) }.ForEach(delegate { });
                var styleResources = typeof(StyleManager).Assembly.GetXamlResourceUris().FirstOrDefault(u => u.OriginalString.EndsWith("StyleResources.xaml", StringComparison.OrdinalIgnoreCase));
                if (styleResources != null)
                {
                    StyleResources = (ResourceDictionary)Application.LoadComponent(styleResources);
                    Application.Current.Resources.MergedDictionaries.Add(StyleResources);
                }
            }
        }

        public static void Initialize()
        {

        }

        /// <summary>
        /// Gets the uris of all xaml resources.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static IEnumerable<Uri> GetXamlResourceUris(this Assembly assembly)
        {
            var uris = new List<Uri>();
            var resourcesName = "{0}.g.resources".FormatWith(assembly.FullName.Split(',').First().Trim());
            if (!assembly.GetManifestResourceNames().Contains(resourcesName)) return new Uri[0];

            using (var s = assembly.GetManifestResourceStream(resourcesName).EnsureNotDefault("Could not load resource {0}.".FormatWith(resourcesName)))
            using (var reader = new ResourceReader(s))
            {
                foreach (DictionaryEntry entry in reader)
                {
                    var key = ((string)entry.Key);
                    if (key.EndsWith(".baml")) key = key.Substring(0, key.Length - 4) + "xaml";
                    if (key.EndsWith(".xaml"))
                    {
                        var uri = new Uri("/{0};component/{1}".FormatWith(assembly.FullName, key), UriKind.Relative);
                        uris.Add(uri);
                    }
                }
            }
            return uris.ToArray();
        }
    }

    /// <summary>
    ///   Defines a type that has actions to load itself.
    /// </summary>
    public interface ILoadable
    {
        IEnumerable<Action> LoadActions { get; }
    }

    public class PresentationBackgroundWorker : BackgroundWorker
    {
        private bool setStatus;

        public PresentationBackgroundWorker(object owner)
        {
            Owner = owner;
        }

        public object Owner { get; private set; }

        public override void RunWorkerAsync(object argument = null, bool cancelAndRunAgainIfBusy = true, bool propogateTransactions = true)
        {
            Application.Current.MainWindow.Dispatcher.BeginInvoke(() =>
            {
                var interactionContextAware = Owner as IInteractionContextAware;
                if (interactionContextAware != null && interactionContextAware.InteractionContext != null)
                {
                    if (interactionContextAware.InteractionContext.Status == null)
                    {
                        interactionContextAware.InteractionContext.Status = "Loading...";
                        setStatus = true;
                    }
                    interactionContextAware.InteractionContext.BusyComponents.Add(this);
                }
                base.RunWorkerAsync(argument, cancelAndRunAgainIfBusy, propogateTransactions);
            });
        }

        protected override void OnRunWorkerCompleted(RunWorkerCompletedEventArgs e)
        {
            var interactionContextAware = Owner as IInteractionContextAware;
            if (!RunAgain && interactionContextAware != null && interactionContextAware.InteractionContext != null)
            {
                interactionContextAware.InteractionContext.BusyComponents.Remove(this);

                if (setStatus && !interactionContextAware.InteractionContext.IsBusy) interactionContextAware.InteractionContext.Status = null;
            }

            base.OnRunWorkerCompleted(e);
        }
    }

    /// <summary>
    /// Helper/extension methods for Interactions.
    /// </summary>
    public static class WindowInteractionExtensions
    {
        /// <summary>
        /// Shows a modal window for the specified arguments.
        /// </summary>
        /// <param name="interactionManager">The interaction manager.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="autoEvaluateOwnerIfNotSet">if set to <c>true</c> [automatic evaluate owner if not set].</param>
        /// <returns></returns>
        public static bool? ShowModal(this IInteractionManager interactionManager, InteractionArguments arguments, bool autoEvaluateOwnerIfNotSet = true)
        {
            var windowInteractionArguments = arguments.As<WindowInteractionArguments>().EnsureNotDefault("Arguments must be WindowInteractionArguments.");
            windowInteractionArguments.IsModal = true;

            // Auto evaluate owner if not set already and requested
            var activeWindowHandle = arguments.Owner == null && autoEvaluateOwnerIfNotSet
                ? WindowInteropUtility.User32.GetActiveWindow()
                : IntPtr.Zero;

            if (activeWindowHandle != IntPtr.Zero)
            {
                arguments.Owner = GetValidOwner(activeWindowHandle);
            }

            interactionManager.Show(arguments);

            return arguments.InteractionContext.DialogResult;
        }

        /// <summary>
        /// Transforms window handle into a valid owner for window.
        /// </summary>
        /// <param name="windowHandle">The window handle.</param>
        /// <returns></returns>
        private static object GetValidOwner(IntPtr windowHandle)
        {
            object owner = null;
            var hwndSource = HwndSource.FromHwnd(windowHandle);
            // Ensure we never make window modal to loading window. Once loading window closes this modal window will close too
            if (hwndSource != null && hwndSource.RootVisual is Window.LoadingWindow)
            {
                var loadingWindowOwnerHandle = WindowInteropUtility.User32.GetWindow(windowHandle, WindowInteropUtility.Enums.GetWindow_Cmd.GW_OWNER);
                if (loadingWindowOwnerHandle != IntPtr.Zero)
                {
                    owner = GetValidOwner(loadingWindowOwnerHandle);
                }
            }
            else
            {
                owner = hwndSource
                        ?? NativeWindow.FromHandle(windowHandle)
                        ?? (object)new WindowInteropUtility.Win32WindowHandleWrapper(windowHandle);
            }
            return owner;
        }
    }

    /// <summary>
    /// Interaction arguments for a window.
    /// </summary>
    [ContentProperty("Content")]
    public class WindowInteractionArguments : InteractionArguments
    {
        public WindowInteractionArguments()
        {
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            WindowName = "ParentWindow";
            CanClose = true;
        }

        /// <summary>
        /// Gets or sets the dialog buttons.
        /// </summary>
        /// <value>
        /// The dialog buttons.
        /// </value>
        public DialogButtons? DialogButtons { get; set; }

        /// <summary>
        /// Gets or sets the resize mode.
        /// </summary>
        /// <value>
        /// The resize mode.
        /// </value>
        public ResizeMode ResizeMode { get; set; }

        /// <summary>
        /// Gets or sets the state of the window.
        /// </summary>
        /// <value>
        /// The state of the window.
        /// </value>
        public WindowState WindowState { get; set; }

        /// <summary>
        /// Gets or sets the name of the window.
        /// Defaults to "ParentWindow".
        /// </summary>
        public string WindowName { get; set; }

        /// <summary>
        /// Gets or sets the window startup location. Default is CenterOwner.
        /// </summary>
        /// <value>
        /// The window startup location.
        /// </value>
        public WindowStartupLocation WindowStartupLocation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show a modal window.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is modal; otherwise, <c>false</c>.
        /// </value>
        public bool IsModal { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance can close.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance can close; otherwise, <c>false</c>.
        /// </value>
        public bool CanClose { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance should be created as a siblining window
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance should hide the current active window; otherwise, <c>false</c>.
        /// </value>
        public bool HideActiveWindow { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the window frame style to display
        /// </summary>
        public WindowFrameStyle WindowFrameStyle { get; set; }
    }

    /// <summary>
    ///   A default implementation of IInteractionManager that uses Windows.
    /// </summary>
    [Singleton]
    internal class WindowInteractionManager : IInteractionManager
    {
        private readonly Func<IWindow> createWindow;
        private readonly HashSet<IWindow> windowsNotClosed = new HashSet<IWindow>();

        public WindowInteractionManager(Func<IWindow> createWindow)
        {
            StyleManager.Initialize();

            this.createWindow = createWindow;
            Application.Current.Dispatcher.BeginInvoke((Action)(() => Application.Current.Exit += OnApplicationExit));
        }

        private void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if (e.Cancel) return;

            var window = (IWindow)sender;

            window.Closing -= OnWindowClosing;

            IInteractionContext context = window.InteractionContext;
            windowsNotClosed.Remove(window);

            if (context != null && !context.IsComplete)
            {
                bool cancel = !context.Complete(false);
                if (cancel)
                {
                    e.Cancel = true;
                    windowsNotClosed.Add(window);
                    window.Closing += OnWindowClosing;
                    return;
                }
            }
            window.Content = null;
        }

        private void OnApplicationExit(object sender, ExitEventArgs e)
        {
            windowsNotClosed.ForEach(c => c.Close());
            windowsNotClosed.Clear();
        }


        [DebuggerNonUserCode]
        private static ContentControl Owner
        {
            get
            {
                try
                {
                    if (!Application.Current.Dispatcher.CheckAccess()) return null;

                    // Find currently active window
                    var window = Application.Current.Windows
                        .OfType<System.Windows.Window>()
                        .LastOrDefault(IsValidOwnerWindow);

                    // If window not found -> try to assign Main Window
                    if (window == null
                        && IsValidOwnerWindow(Application.Current.MainWindow))
                    {
                        window = Application.Current.MainWindow;
                    }

                    // Do not use loading window as owner (use it's owner instead)
                    if (window is Window.LoadingWindow)
                    {
                        window = window.Owner;
                    }

                    return window;
                }
                catch
                {
                    return null;
                }
            }
        }

        private static bool IsValidOwnerWindow(System.Windows.Window window)
        {
            return window != null && window.CheckAccess() && window.IsActive && window.IsVisible && window.IsLoaded;
        }

        #region IInteractionManager Members

        public void Alert(AlertArguments arguments)
        {
            if (arguments.Owner == null) arguments.Owner = Owner;

            if (Alerting != null)
            {
                var eventArgs = new InteractionEventArgs<AlertArguments>(arguments);
                Alerting(this, eventArgs);
                if (eventArgs.Handled) return;
            }

            var content = arguments.Content;

            #region showAlert action

            Action showAlert = () =>
                {
                    if (arguments.Content is DataTemplate)
                    {
                        content = new ContentPresenter { ContentTemplate = arguments.Content as DataTemplate };
                    }

                    RadWindow.Alert(new DialogParameters
                    {
                        Owner = arguments.Owner as ContentControl,
                        Header = arguments.Header ?? GetHeader(arguments.Content),
                        Content = content,
                        ContentStyle = (Style)StyleManager.StyleResources["Dialog"],
                        Opened = (sender, e) =>
                        {
                            var w = ((RadWindow)sender);
                            w.BringToFront();
                            w.IsTopmost = true;
                        },
                        Closed = (sender, e) =>
                        {
                            var w = ((RadWindow)sender);
                            w.Content = null;
                        }
                    });
                };

            #endregion

            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => showAlert());
            }
            else
            {
                showAlert();
            }
        }

        /// <summary>
        /// Returns a header for context
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        private object GetHeader(object content)
        {
            if (content != null)
            {
                var property = content.GetType().GetProperty("Header", BindingFlags.Instance | BindingFlags.Public);
                if (property != null && property.CanRead && property.GetIndexParameters().Length == 0)
                {
                    var header = property.GetValue(content, null);
                    return header;
                }
            }

            // Return an empty string Header
            return " ";
        }

        public event EventHandler<InteractionEventArgs<AlertArguments>> Alerting;

        public PromptResult Prompt(PromptArguments arguments)
        {
            if (arguments.Owner == null) arguments.Owner = Owner;

            if (Prompting != null)
            {
                var eventArgs = new InteractionEventArgs<PromptArguments, PromptResult>(arguments);
                Prompting(this, eventArgs);
                if (eventArgs.Handled) return eventArgs.Result;
            }



            var content = arguments.Content;

            #region

            Func<PromptResult> showPrompt = () =>
                {
                    PromptResult result = null;
                    if (arguments.Content is DataTemplate)
                    {
                        content = new ContentPresenter { ContentTemplate = arguments.Content as DataTemplate };
                    }

                    RadWindow.Prompt(new DialogParameters
                    {
                        Owner = arguments.Owner as ContentControl,
                        Header = arguments.Header ?? GetHeader(arguments.Content),
                        Content = content,
                        ContentStyle = (Style)StyleManager.StyleResources["Dialog"],
                        DefaultPromptResultValue = arguments.DefaultInput,
                        Closed = (sender, e) =>
                        {
                            result = new PromptResult(e.DialogResult, e.PromptResult);
                            var w = ((RadWindow)sender);
                            w.Content = null;
                        },
                        Opened = (sender, e) =>
                        {
                            var w = ((RadWindow)sender);
                            w.BringToFront();
                            w.IsTopmost = true;
                        }
                    });

                    return result;
                };

            #endregion

            PromptResult promptResult = null;
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => promptResult = showPrompt());
            }
            else
            {
                promptResult = showPrompt();
            }
            return promptResult;
        }

        public event EventHandler<InteractionEventArgs<PromptArguments, PromptResult>> Prompting;

        public bool? Confirm(ConfirmationArguments arguments)
        {
            if (arguments.Owner == null) arguments.Owner = Owner;

            if (Confirming != null)
            {
                var eventArgs = new InteractionEventArgs<ConfirmationArguments, bool?>(arguments);
                Confirming(this, eventArgs);
                if (eventArgs.Handled) return eventArgs.Result;
            }



            object okContent = arguments.OkButtonContent ?? (arguments.Buttons == ConfirmButtons.YesNo ? "Yes" : null);
            object cancelContent = arguments.CancelButtonContent ?? (arguments.Buttons == ConfirmButtons.YesNo ? "No" : null);

            var content = arguments.Content;

            #region showConfirm

            Func<bool?> showConfirm = () =>
                {
                    bool? result = null;
                    if (arguments.Content is DataTemplate)
                    {
                        content = new ContentPresenter { ContentTemplate = arguments.Content as DataTemplate };
                    }

                    RadWindow.Confirm(new DialogParameters
                    {
                        Owner = arguments.Owner as ContentControl,
                        Header = arguments.Header ?? GetHeader(arguments.Content),
                        Content = content,
                        OkButtonContent = okContent,
                        CancelButtonContent = cancelContent,
                        Closed = (sender, e) =>
                        {
                            result = e.DialogResult;
                            var w = ((RadWindow)sender);
                            w.Content = null;
                        },
                        Opened = (sender, e) =>
                        {
                            var w = ((RadWindow)sender);
                            w.BringToFront();
                            w.IsTopmost = true;
                        }
                    });

                    return result;
                };

            #endregion

            bool? confirmResult = null;
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => confirmResult = showConfirm());
            }
            else
            {
                confirmResult = showConfirm();
            }
            return confirmResult;
        }

        public event EventHandler<InteractionEventArgs<ConfirmationArguments, bool?>> Confirming;

        public void DisplayException(ExceptionInteractionArguments arguments)
        {
            if (arguments.Owner == null) arguments.Owner = Owner;

            if (DisplayingException != null)
            {
                var eventArgs = new HandledEventArgs<ExceptionInteractionArguments>(arguments);
                DisplayingException(this, eventArgs);
                if (eventArgs.Handled) return;
            }

            var errorTextBox = new TextBox
            {
                IsReadOnly = true,
                Text = arguments.Exception.ToString(),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Left,
                VerticalContentAlignment = VerticalAlignment.Top,
                MaxWidth = 360,
                Height = 150
            };


            errorTextBox.MouseDoubleClick += delegate { errorTextBox.SelectAll(); };

            var panel = new StackPanel();
            panel.Children.Add(new TextBlock { Text = arguments.Message, Padding = new Thickness(0, 0, 0, 12), TextWrapping = TextWrapping.Wrap });
            panel.Children.Add(new RadExpander { Content = errorTextBox, Header = new TextBlock { HorizontalAlignment = HorizontalAlignment.Left, Text = "Details..." } });
            var dialogParameters = new DialogParameters
            {
                Owner = arguments.Owner,
                Header = "An error has occurred",
                Content = new ContentControl { MaxWidth = 400, MinHeight = 80, Content = panel },
                Opened = (sender, e) =>
                {
                    var w = ((RadWindow)sender);
                    w.BringToFront();
                    w.IsTopmost = true;
                }
            };
            RadWindow.Alert(dialogParameters);
        }

        public event EventHandler<HandledEventArgs<ExceptionInteractionArguments>> DisplayingException;

        public void Show(InteractionArguments arguments)
        {
            if (arguments.Owner == null) arguments.Owner = Owner;

            var windowInteractionArguments = arguments.As<WindowInteractionArguments>().EnsureNotDefault("Arguments must be WindowInteractionArguments.");

            var eventArgs = new InteractionEventArgs<InteractionArguments>(arguments);

            #region showAction

            Action showAction = () =>
                {
                    Showing.Fire(this, eventArgs);
                    if (eventArgs.Handled) return;

                    if (windowInteractionArguments.HideActiveWindow)
                    {
                        var activeWindow = windowsNotClosed.FirstOrDefault(w => w.IsActive);
                        if (activeWindow != null) activeWindow.Hide();
                    }

                    var window = BuildWindow(windowInteractionArguments);

                    if (windowInteractionArguments.IsModal) window.ShowModal();
                    else window.Show();
                };

            #endregion

            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => showAction());
            }
            else
            {
                showAction();
            }
        }

        public event EventHandler<InteractionEventArgs<InteractionArguments>> Showing;

        #endregion

        private IWindow BuildWindow(WindowInteractionArguments arguments)
        {
            var window = createWindow();

            if (arguments.InteractionContext != null) window.InteractionContext = arguments.InteractionContext;

            if (arguments.InteractionContext == null) arguments.InteractionContext = window.InteractionContext = (window.InteractionContext ?? ServiceProvider.Current.GetService<IInteractionContext>());

            var content = arguments.Content;
            if (content is DataTemplate)
            {
                content = new ContentPresenter { ContentTemplate = arguments.Content as DataTemplate };
            }
            else if (content is Type)
            {
                content = ServiceProvider.Current.GetService((Type)content);
            }

            window.Content = content;
            window.Header = arguments.Header ?? GetHeader(arguments.Content);
            window.Owner = arguments.Owner;
            window.DialogButtons = arguments.DialogButtons;
            window.ResizeMode = arguments.ResizeMode;
            window.CanClose = arguments.CanClose;
            window.WindowState = arguments.WindowState;
            window.WindowStartupLocation = arguments.WindowStartupLocation;
            window.WindowFrameStyle = arguments.WindowFrameStyle;
            window.Name = arguments.WindowName;

            window.Closing += OnWindowClosing;

            arguments.InteractionContext.CastTo<InteractionContext>().AfterCompleted += delegate
            {
                if (windowsNotClosed.Remove(window))
                {
                    var fe = window as FrameworkElement;
                    if (fe == null || fe.Dispatcher.CheckAccess())
                    {
                        window.Close();
                    }
                    else
                    {
                        fe.Dispatcher.BeginInvoke(window.Close);
                    }
                }
            };

            windowsNotClosed.Add(window);

            return window;
        }
    }

    /// <summary>
    /// Binds IsEnabled to whether or not the specified Command can execute.
    /// </summary>
    public class CommandCanExecuteBehavior : Behavior<Control>
    {
        public CommandCanExecuteBehavior()
        {
            CommandManager.RequerySuggested += OnCommandManagerRequerySuggested;
        }

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(ICommand), typeof(CommandCanExecuteBehavior), new PropertyMetadata(null, OnCommandChanged));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register("CommandParameter", typeof(object), typeof(CommandCanExecuteBehavior), new PropertyMetadata(null));

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        private static void OnCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((CommandCanExecuteBehavior)d).OnCommandChanged(e.OldValue as ICommand, e.NewValue as ICommand);
        }

        private void OnCommandChanged(ICommand oldValue, ICommand newValue)
        {
            if (AssociatedObject != null) AssociatedObject.IsEnabled = Command != null && Command.CanExecute(CommandParameter);

            if (oldValue != null) oldValue.CanExecuteChanged -= OnCanExecuteChanged;
            if (newValue != null) newValue.CanExecuteChanged += OnCanExecuteChanged;
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            UpdateEnabled();
        }

        private void OnCommandManagerRequerySuggested(object sender, EventArgs e)
        {
            UpdateEnabled();
        }

        private void OnCanExecuteChanged(object sender, EventArgs e)
        {
            UpdateEnabled();
        }

        private void UpdateEnabled()
        {
            if (AssociatedObject != null) AssociatedObject.IsEnabled = Command != null && Command.CanExecute(CommandParameter);
        }
    }

    /// <summary>
    /// An abstract base class for a command wrapper.
    /// </summary>
    public abstract class CommandWrapper : Freezable, ICommand
    {
        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(ICommand), typeof(CommandWrapper), null);

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public abstract void Execute(object parameter);

        public bool CanExecute(object parameter)
        {
            return Command == null || Command.CanExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        protected override Freezable CreateInstanceCore()
        {
            return this;
        }
    }

    /// <summary>
    ///   Displays a confirmation window.
    /// </summary>
    public class ConfirmCommand : CommandWrapper
    {
        public static readonly DependencyProperty ContentProperty = DependencyProperty.Register("Content", typeof(object), typeof(ConfirmCommand), new PropertyMetadata(null));
        public static readonly DependencyProperty CancelCommandProperty = DependencyProperty.Register("CancelCommand", typeof(ICommand), typeof(ConfirmCommand), new PropertyMetadata(null));
        public static readonly DependencyProperty OkButtonContentProperty = DependencyProperty.Register("OkButtonContent", typeof(object), typeof(ConfirmCommand), new PropertyMetadata(null));
        public static readonly DependencyProperty CancelButtonContentProperty = DependencyProperty.Register("CancelButtonContent", typeof(object), typeof(ConfirmCommand), new PropertyMetadata(null));

        public object Content
        {
            get { return GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public object CancelButtonContent
        {
            get { return GetValue(CancelButtonContentProperty); }
            set { SetValue(CancelButtonContentProperty, value); }
        }

        public object OkButtonContent
        {
            get { return GetValue(OkButtonContentProperty); }
            set { SetValue(OkButtonContentProperty, value); }
        }

        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }


        public override void Execute(object parameter)
        {
            Content.EnsureNotDefault("Content is required.");

            ContentControl owner = Application.Current.Windows.OfType<System.Windows.Window>().FirstOrDefault(w => w.IsActive);
            bool disabled = false;
            if (owner != null && owner.IsEnabled)
            {
                owner.IsEnabled = false;
                disabled = true;
            }

            bool? result = InteractionManager.Current.Confirm(Content, CancelButtonContent, OkButtonContent, owner: owner);

            if (Command != null && result == true)
            {
                Command.Execute(parameter);
            }
            else if (CancelCommand != null && result != true)
            {
                CancelCommand.Execute(parameter);
            }

            if (owner != null && disabled) owner.IsEnabled = true;
        }
    }

    /// <summary>
    /// Opens a file browser.
    /// </summary>
    public class OpenFileCommand : CommandWrapper
    {
        public string Filter { get; set; }

        public int FilterIndex { get; set; }

        public bool Multiselect { get; set; }

        public bool AddExtension { get; set; }

        public bool CheckFileExists { get; set; }

        public bool CheckPathExists { get; set; }

        public string DefaultExtension { get; set; }

        public bool DereferenceLinks { get; set; }

        public string FileName { get; set; }

        public string InitialDirectory { get; set; }

        public bool ReadOnlyChecked { get; set; }

        public bool ShowReadOnly { get; set; }

        public bool ValidateNames { get; set; }

        public string Title { get; set; }

        public override void Execute(object parameter)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = Filter;
            dialog.FilterIndex = FilterIndex;
            dialog.Multiselect = Multiselect;
            dialog.AddExtension = AddExtension;
            dialog.CheckFileExists = CheckFileExists;
            dialog.CheckPathExists = CheckPathExists;
            dialog.DefaultExt = DefaultExtension;
            dialog.DereferenceLinks = DereferenceLinks;
            dialog.FileName = FileName;
            dialog.InitialDirectory = InitialDirectory;
            dialog.ReadOnlyChecked = ReadOnlyChecked;
            dialog.ShowReadOnly = ShowReadOnly;
            dialog.Title = Title;
            dialog.ValidateNames = ValidateNames;

            var owner = Application.Current.Windows.OfType<System.Windows.Window>().FirstOrDefault(w => w.IsActive);
            bool? result = dialog.ShowDialog(owner);
            FileInfo[] files = dialog.FileNames.Select(n => new FileInfo(n)).ToArray();

            if (Command != null && result.GetValueOrDefault(false))
            {
                Command.Execute(Tuple.Create(files, parameter));
            }

        }
    }

    /// <summary>
    /// Shows a save file dialog.
    /// </summary>
    public class SaveFileCommand : CommandWrapper
    {
        public string Filter { get; set; }

        public int FilterIndex { get; set; }

        public string DefaultExtension { get; set; }

        public bool AddExtension { get; set; }

        public bool CheckFileExists { get; set; }

        public bool CheckPathExists { get; set; }

        public bool DereferenceLinks { get; set; }

        public string FileName { get; set; }

        public string InitialDirectory { get; set; }

        public bool ValidateNames { get; set; }

        public string Title { get; set; }

        public bool CreatePrompt { get; set; }

        public bool OverwritePrompt { get; set; }

        public static readonly DependencyProperty FileNameProperty =
       DependencyProperty.RegisterAttached("FileName", typeof(string), typeof(SaveFileCommand), new PropertyMetadata(OnFileNameChanged));

        public static string GetFileName(DependencyObject dependencyObject)
        {
            return (string)dependencyObject.GetValue(FileNameProperty);
        }

        public static void SetFileName(DependencyObject dependencyObject, string fileName)
        {
            dependencyObject.SetValue(FileNameProperty, fileName);
        }

        private static void OnFileNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var saveFileCommand = (SaveFileCommand)d;
            if (e.NewValue != null && !string.IsNullOrEmpty(e.NewValue.ToString()))
            {
                saveFileCommand.FileName = (string)e.NewValue;
            }
        }

        public override void Execute(object parameter)
        {
            var dialog = new SaveFileDialog();
            dialog.Filter = Filter;
            dialog.FilterIndex = FilterIndex;
            dialog.DefaultExt = DefaultExtension;
            dialog.AddExtension = AddExtension;
            dialog.CheckFileExists = CheckFileExists;
            dialog.CheckPathExists = CheckPathExists;
            dialog.DereferenceLinks = DereferenceLinks;
            dialog.FileName = FileName;
            dialog.InitialDirectory = InitialDirectory;
            dialog.Title = Title;
            dialog.ValidateNames = ValidateNames;
            dialog.CreatePrompt = CreatePrompt;
            dialog.OverwritePrompt = OverwritePrompt;

            var owner = Application.Current.Windows.OfType<System.Windows.Window>().FirstOrDefault(w => w.IsActive);
            bool? result = dialog.ShowDialog(owner);

            if (Command != null && result.GetValueOrDefault(false))
            {
                Command.Execute(Tuple.Create(dialog.FileName, parameter));
            }
        }

    }

    /// <summary>
    ///   Displays a prompt window.
    /// </summary>
    public class PromptCommand : CommandWrapper
    {
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(PromptCommand), new PropertyMetadata(null));

        public static readonly DependencyProperty DefaultInputProperty = DependencyProperty.Register("DefaultInput", typeof(string), typeof(PromptCommand), new PropertyMetadata(null));

        public bool ProceedOnCancel { get; set; }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string DefaultInput
        {
            get { return (string)GetValue(DefaultInputProperty); }
            set { SetValue(DefaultInputProperty, value); }
        }

        public override void Execute(object parameter)
        {
            Text.EnsureNotDefault("Text is required.");

            ContentControl owner = Application.Current.Windows.OfType<System.Windows.Window>().FirstOrDefault(w => w.IsActive);
            bool disabled = false;
            if (owner != null && owner.IsEnabled)
            {
                owner.IsEnabled = false;
                disabled = true;
            }

            PromptResult result = InteractionManager.Current.Prompt(Text, defaultInput: DefaultInput, owner: owner);

            if (Command != null && ((result.DialogResult.HasValue && result.DialogResult.Value) || ProceedOnCancel))
            {
                Command.Execute(Tuple.Create(result.Input, parameter));
            }

            if (owner != null && disabled) owner.IsEnabled = true;
        }
    }

    /// <summary>
    ///   Displays an alert window.
    /// </summary>
    public class AlertCommand : CommandWrapper
    {
        public static readonly DependencyProperty ContentProperty = DependencyProperty.Register("Content", typeof(object), typeof(AlertCommand), new PropertyMetadata(null));
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(AlertCommand), new PropertyMetadata(null));

        public object Content
        {
            get { return GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public override void Execute(object parameter)
        {
            if (Content == null && Text == null)
            {
                throw new InvalidOperationException("Content or Text must be supplied");
            }

            ContentControl owner = Application.Current.Windows.OfType<System.Windows.Window>().FirstOrDefault(w => w.IsActive);
            bool disabled = false;
            if (owner != null && owner.IsEnabled)
            {
                owner.IsEnabled = false;
                disabled = true;
            }
            if (Content != null)
            {
                InteractionManager.Current.Alert(Content, owner: owner);
            }
            else
            {
                InteractionManager.Current.Alert(Text, owner: owner);
            }

            if (Command != null) Command.Execute(parameter);

            if (owner != null && disabled) owner.IsEnabled = true;
        }
    }



    /// <summary>
    /// Displays a new view using the current InteractionManager.
    /// </summary>
    public class InteractionCommand : CommandWrapper
    {
        /// <summary>
        /// The view to display.
        /// </summary>
        public static readonly DependencyProperty ArgumentsProperty = DependencyProperty.Register("Arguments", typeof(InteractionArguments), typeof(InteractionCommand), new PropertyMetadata(null));

        /// <summary>
        /// The arguments for the interaction.
        /// </summary>
        /// <value>
        /// The view.
        /// </value>
        public InteractionArguments Arguments
        {
            get { return (InteractionArguments)GetValue(ArgumentsProperty); }
            set { SetValue(ArgumentsProperty, value); }
        }

        public override void Execute(object parameter)
        {
            Arguments.EnsureNotDefault("Arguments are required.");

            ContentControl owner = Application.Current.Windows.OfType<System.Windows.Window>().FirstOrDefault(w => w.IsActive);

            bool disabled = false;
            if (owner != null && owner.IsEnabled && Arguments.As<WindowInteractionArguments>().IfNotNull(a => a.IsModal))
            {
                owner.IsEnabled = false;
                disabled = true;
            }

            var interactionContent = Arguments.InteractionContext;

            InteractionManager.Current.Show(Arguments);

            if (Command != null) Command.Execute(parameter);

            if (owner != null && disabled) owner.IsEnabled = true;

            Arguments.InteractionContext = interactionContent;
        }
    }

    /// <summary>
    /// A command that operates off of a delegate.
    /// </summary>
    public class DelegateCommand : ICommand
    {
        // Fields
        private readonly Predicate<object> canExecute;
        private readonly Action<object> execute;

        // Events

        // Methods
        public DelegateCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        public DelegateCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("execute");
            }
            this.execute = execute;
            this.canExecute = canExecute;
        }

        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            if (canExecute != null)
            {
                return canExecute(parameter);
            }
            return true;
        }

        public void Execute(object parameter)
        {
            execute(parameter);
        }

        #endregion

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }

    /// <summary>
    /// Helpers for commands.
    /// </summary>
    public static class Command
    {
        /// <summary>
        /// Creates a command from the specified action.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action">The action.</param>
        /// <param name="canExecute">The can execute.</param>
        /// <returns></returns>
        public static ICommand Create<T>(Action<T> action, Func<T, bool> canExecute = null)
        {
            return new DelegateCommand(o => action(TryConvert<T>(o)), canExecute != null ? (Predicate<object>)(o => canExecute(TryConvert<T>(o))) : null);
        }

        [DebuggerNonUserCode]
        private static T TryConvert<T>(object o)
        {
            T result;
            o.TryConvertTo(out result);
            return result;
        }

        /// <summary>
        /// Creates a command from the specified action.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="canExecute">The can execute.</param>
        /// <returns></returns>
        public static ICommand Create(Action action, Func<bool> canExecute = null)
        {
            return new DelegateCommand(o => action(), canExecute != null ? (Predicate<object>)(o => canExecute()) : null);
        }

        /// <summary>
        /// Executes the action async.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="getInteractionContext">The get interaction context.</param>
        /// <param name="status">The status.</param>
        /// <param name="supportsCancellation">if set to <c>true</c> [supports cancellation].</param>
        /// <param name="useStaThread"> </param>
        public static ICommand ExecuteAsync(this Action action, Func<IInteractionContext> getInteractionContext = null, object status = null, bool supportsCancellation = false, bool useStaThread = false)
        {
            ICommand command;
            if (getInteractionContext != null)
                command = Create(action).Async(getInteractionContext, useStaThread, status, supportsCancellation);
            else
                command = Create(action).Async();

            command.Execute();

            return command;
        }

        /// <summary>
        /// Returns an asynchronously executing wrapper for the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="onBegin">The on begin.</param>
        /// <param name="onComplete">The on complete.</param>
        /// <param name="asyncIf"> </param>
        /// <param name="useStaThread"> </param>
        /// <returns></returns>
        public static ICommand Async(this ICommand command, bool useStaThread = false, Action<ICommand> onBegin = null, Action<ICommand> onComplete = null, Func<bool> asyncIf = null)
        {
            return AsyncCommand.Create(command, onBegin, onComplete, asyncIf, useStaThread);
        }

        /// <summary>
        /// Returns an asynchronously executing wrapper for the specified command. Sets the interaction context to busy when executing.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="getInteractionContext">The get interaction context.</param>
        /// <param name="status">The status. Default is "Loading..."</param>
        /// <param name="supportsCancellation">if set to <c>true</c> [supports cancellation].</param>
        /// <param name="onBegin">The on begin.</param>
        /// <param name="onComplete">The on complete.</param>
        /// <param name="asyncIf"> </param>
        /// <param name="useStaThread"> </param>
        /// <returns></returns>
        public static ICommand Async(this ICommand command, Func<IInteractionContext> getInteractionContext, bool useStaThread = false, object status = null, bool supportsCancellation = false, Action<ICommand> onBegin = null, Action<ICommand> onComplete = null, Func<bool> asyncIf = null)
        {
            if (status == null) status = "Loading...";

            AsyncCommand asyncCommand = null;
            if (supportsCancellation)
            {
                // ReSharper disable PossibleNullReferenceException
                // ReSharper disable AccessToModifiedClosure
                var cancel = Create(() => asyncCommand.Cancel());
                // ReSharper restore AccessToModifiedClosure
                // ReSharper restore PossibleNullReferenceException
                status = new BusyStatusWithCancel { Status = status, Cancel = cancel };

            }

            asyncCommand = AsyncCommand.Create(command,
                                               c =>
                                               {
                                                   if (onBegin != null) onBegin(c);
                                                   var interactionContext = getInteractionContext();
                                                   if (interactionContext != null)
                                                   {
                                                       if (!interactionContext.IsBusy) interactionContext.Status = status;
                                                       interactionContext.BusyComponents.Add(command);
                                                   }
                                               },
                                               c =>
                                               {
                                                   var interactionContext = getInteractionContext();
                                                   if (interactionContext != null)
                                                   {
                                                       interactionContext.BusyComponents.Remove(command);
                                                       if (!interactionContext.IsBusy) interactionContext.Status = null;
                                                   }
                                                   if (onComplete != null) onComplete(c);
                                               }, asyncIf, useStaThread);

            return asyncCommand;
        }

        /// <summary>
        /// Make input command, which is used as implementation for View.Closing, asynchronous
        /// </summary>
        /// <param name="command"></param>
        /// <param name="getInteractionContext"></param>
        /// <returns></returns>
        public static ICommand AsyncForClosing(this ICommand command, Func<IInteractionContext> getInteractionContext)
        {
            // Interaction context must be provided (otherwise we can't force final closing)
            if (getInteractionContext == null) throw new ArgumentNullException("getInteractionContext");

            // Setup a variable for closure which will be shared between async and delegate command
            var hasBeenCancelled = false;
            var isClosingRequestedByAsyncCommand = false;

            // Convert input command into async command with special handling to closing event
            var asyncCommand = command.Async(getInteractionContext, onComplete: c =>
                {
                    // Cancelled? -> nothing to do
                    if (hasBeenCancelled) return;

                    // Proceed with original intention of closing and with previous dialog result
                    var context = getInteractionContext();
                    isClosingRequestedByAsyncCommand = true;
                    context.Complete(context.DialogResult);
                });

            // Prepare synchronous command which will cancel closing while async command is executing
            return Create<Action>(cancelAction =>
                {
                    // Are we closing after completing async command?
                    if (isClosingRequestedByAsyncCommand)
                    {
                        isClosingRequestedByAsyncCommand = false;
                        return;
                    }

                    // Cancel original closing
                    cancelAction();

                    // Run original command with new way of cancelling closing
                    Action cancelFinalClosing = () => hasBeenCancelled = true;
                    asyncCommand.Execute(cancelFinalClosing);
                }, asyncCommand.CanExecute);
        }

        /// <summary>
        /// Executes the specified command with no parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        public static void Execute(this ICommand command)
        {
            command.Execute(null);
        }

        /// <summary>
        /// Determines if the the specified command can execute with no parameter.
        /// </summary>
        /// <param name="command">The command.</param>
        public static bool CanExecute(this ICommand command)
        {
            return command.CanExecute(null);
        }

        /// <summary>
        /// Returns composite command that executes several other commands.
        /// </summary>
        /// <param name="commands">The commands.</param>
        /// <returns></returns>
        public static ICommand Composite(params ICommand[] commands)
        {
            return new CompositeCommand(commands, null);
        }

        /// <summary>
        /// Returns composite command that executes several other commands.
        /// </summary>
        /// <param name="canExecute"></param>
        /// <param name="commands"></param>
        /// <returns></returns>
        public static ICommand Composite(Func<IEnumerable<ICommand>, object, bool> canExecute, params ICommand[] commands)
        {
            return new CompositeCommand(commands, canExecute);
        }

        /// <summary>
        /// Returns composite command that executes several other commands.
        /// </summary>
        /// <param name="runIfAnyCanExecute"></param>
        /// <param name="commands"></param>        
        /// <returns></returns>
        public static ICommand Composite(bool runIfAnyCanExecute, params ICommand[] commands)
        {
            return new CompositeCommand(commands, runIfAnyCanExecute);
        }

        /// <summary>
        /// Asynchronously delays specified command execution by X milliseconds.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="timeoutInMilliseconds">The timeout in milliseconds.</param>
        /// <returns></returns>
        public static ICommand Delay(this ICommand command, int timeoutInMilliseconds)
        {
            return new DelayedCommand(command, timeoutInMilliseconds);
        }
    }

    /// <summary>
    /// A template for a busy message that supports cancellations.
    /// </summary>
    public class BusyStatusWithCancel : INotifyPropertyChanged
    {
        private ICommand cancel;
        public ICommand Cancel
        {
            get { return cancel; }
            set
            {
                bool isChanged = cancel != value;
                cancel = value;
                if (isChanged) PropertyChanged.Fire(this, () => Cancel);
            }
        }

        private object status;
        public object Status
        {
            get { return status; }
            set
            {
                bool isChanged = status != value;
                status = value;
                if (isChanged) PropertyChanged.Fire(this, () => Status);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    /// <summary>
    /// A command that executes after a certain timeout.
    /// </summary>
    public class DelayedCommand : ICommand
    {
        private readonly ICommand command;
        private readonly int timeoutInMilliseconds;
        CancellationTokenSource waitToken;

        public DelayedCommand(ICommand command, int timeoutInMilliseconds)
        {
            this.command = command;
            this.timeoutInMilliseconds = timeoutInMilliseconds;
        }

        public void Execute(object parameter)
        {
            // Cancel previous execution attempt before adding new one
            if (waitToken != null && !waitToken.IsCancellationRequested)
            {
                waitToken.Cancel();
            }

            // Create new cancellation token
            waitToken = new CancellationTokenSource();

            Task.Factory.StartNew(p =>
            {
                var theToken = (CancellationToken)p;

                // Wait until execution
                theToken.WaitHandle.WaitOne(timeoutInMilliseconds);

                if (theToken.IsCancellationRequested) return;

                // Actually execute command
                command.Execute(parameter);

            }, waitToken.Token, waitToken.Token);
        }

        public bool CanExecute(object parameter)
        {
            return command.CanExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { command.CanExecuteChanged += value; }
            remove { command.CanExecuteChanged -= value; }
        }
    }

    /// <summary>
    /// A composite command that executes several other commands.
    /// </summary>
    public class CompositeCommand : ICommand
    {
        private readonly IEnumerable<ICommand> commands;
        private readonly Func<IEnumerable<ICommand>, object, bool> canExecute;

        public CompositeCommand(IEnumerable<ICommand> commands, Func<IEnumerable<ICommand>, object, bool> canExecute)
        {
            this.commands = commands.ToArray();
            this.canExecute = canExecute ?? ((c, p) => c.All(i => i.CanExecute(p)));
        }

        public CompositeCommand(IEnumerable<ICommand> commands, bool runIfAnyCanExecute)
            : this(commands, runIfAnyCanExecute ? (new Func<IEnumerable<ICommand>, object, bool>((c, p) => c.Any(i => i.CanExecute(p)))) : null)
        {
        }

        public void Execute(object parameter)
        {
            foreach (var command in commands.Where(c => c.CanExecute(parameter)))
            {
                command.Execute(parameter);
            }

            OnCanExecuteChanged();
        }

        public bool CanExecute(object parameter)
        {
            return canExecute(commands, parameter);
        }

        private void OnCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        public event EventHandler CanExecuteChanged
        {
            add { commands.ForEach(c => c.CanExecuteChanged += value); }
            remove { commands.ForEach(c => c.CanExecuteChanged -= value); }
        }
    }

    /// <summary>
    /// A command that wraps another command and executes asychronously.
    /// </summary>
    public sealed class AsyncCommand : ICommand
    {
        private readonly ICommand command;
        private readonly Action<ICommand> onBegin;
        private readonly Action<ICommand> onComplete;

        private readonly Func<bool> asyncIf;
        private Thread workerThread;
        private BackgroundWorker worker;
        private Thread staThread;

        private readonly bool useStaThread;
        private bool isBusy;

        private bool aborted = false;
        /// <summary>
        /// Gets a value indicating whether this command is currently running in the background.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is running; otherwise, <c>false</c>.
        /// </value>
        public bool IsBusy
        {
            get { return isBusy; }
        }

        /// <summary>
        /// Creates an asynchronous command for the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="onBegin">The on begin.</param>
        /// <param name="onComplete">The on complete.</param>
        /// <param name="asyncIf"> </param>
        /// <param name="useStaThread"> </param>
        /// <returns></returns>
        public static AsyncCommand Create(ICommand command, Action<ICommand> onBegin = null, Action<ICommand> onComplete = null, Func<bool> asyncIf = null, bool useStaThread = false)
        {
            var asyncCommand = new AsyncCommand(command, onBegin, onComplete, asyncIf, useStaThread);
            return asyncCommand;
        }

        private AsyncCommand(ICommand command, Action<ICommand> onBegin, Action<ICommand> onComplete, Func<bool> asyncIf, bool useStaThread = false)
        {
            this.command = command;
            this.onBegin = onBegin;
            this.onComplete = onComplete;
            this.useStaThread = useStaThread;

            this.asyncIf = asyncIf ?? (() => Application.Current.GetDispatcher().Thread.ManagedThreadId == Dispatcher.CurrentDispatcher.Thread.ManagedThreadId);
            worker = new BackgroundWorker();
            worker.DoWork += OnDoWork;
            worker.RunWorkerCompleted += OnRunWorkerCompleted;
        }

        private void OnDoWork(object sender, DoWorkEventArgs e)
        {
            workerThread = Thread.CurrentThread;
            if (useStaThread)
            {
                Exception ex = null;
                staThread = new Thread(s =>
                    {
                        try
                        {
                            command.Execute(e.Argument);
                        }
                        catch (Exception exception)
                        {
                            ex = exception;
                        }
                    });
                staThread.SetApartmentState(ApartmentState.STA);
                staThread.Start();
                staThread.Join();
                if (ex != null)
                {
                    ex.Rethrow();
                }
            }
            else
            {
                command.Execute(e.Argument);
            }
        }

        private void OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                try
                {
                    if (onComplete != null) onComplete(command);
                }
                finally
                {
                    isBusy = false;
                    if (e.Error != null && !aborted)
                    {
                        throw new Exception("The asynchronous command encountered an error.", e.Error);
                    }
                }
            }
            finally
            {
                OnCanExecuteChanged();
            }
        }

        public void Execute(object parameter)
        {
            aborted = false;
            var executeAsync = asyncIf();
            isBusy = true;

            if (onBegin != null) onBegin(command);

            OnCanExecuteChanged();

            if (executeAsync) worker.RunWorkerAsync(parameter);
            else
            {
                command.Execute(parameter);
                isBusy = false;
                try
                {
                    if (onComplete != null) onComplete(command);
                }
                finally
                {
                    OnCanExecuteChanged();
                }
            }
        }

        /// <summary>
        /// Aborts the async thread..
        /// </summary>
        public void Cancel()
        {
            var workerThread = this.workerThread;
            if (worker.IsBusy && workerThread != null)
            {
                try
                {
                    workerThread.Abort();
                    if (staThread != null) staThread.Abort();
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {
                }
                finally
                {
                    aborted = true;

                    ((IDisposable)worker).Dispose();

                    worker = new BackgroundWorker();
                    worker.DoWork += OnDoWork;
                    worker.RunWorkerCompleted += OnRunWorkerCompleted;

                    isBusy = false;

                    try { if (onComplete != null) onComplete(command); }
                    finally
                    {
                        OnCanExecuteChanged();
                    }
                }

            }
        }

        public bool CanExecute(object parameter)
        {
            return !isBusy && !worker.IsBusy && command.CanExecute(parameter);
        }

        private void OnCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        public event EventHandler CanExecuteChanged
        {
            add { command.CanExecuteChanged += value; }
            remove { command.CanExecuteChanged -= value; }
        }
    }

    /// <summary>
    ///   Denotes that a type supports invoking attributed properties or methods using the application dispatcher.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class SupportsDispatcherThreadAttribute : ConcernAttribute
    {
        /// <summary>
        /// Gets or sets a value indicating whether to suppress on suppress property changed scope. If SuppressPropertyChangedScope.Current is not null and this value is true, Dispatcher behavior will be disabled. Default is true.
        /// </summary>
        /// <value>
        /// <c>true</c> if [suppress on suppress property changed scope]; otherwise, <c>false</c>.
        /// </value>
        public bool SuppressOnSuppressPropertyChangedScope { get; set; }

        public SupportsDispatcherThreadAttribute()
            : base(typeof(DispatcherThreadBehavior), 1000)
        {
        }
    }


    /// <summary>
    /// Denotes that a member should be invoked using a dispatcher.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
    public class DispatcherThreadAttribute : Attribute
    {
    }

    /// <summary>
    /// Behavior to invoke dispatcher on method/property access.
    /// </summary>
    internal class DispatcherThreadBehavior : AttributedMemberInterceptor
    {
        private static readonly InterceptSettersFilter SettersFilter = new InterceptSettersFilter();

        private readonly IEnumerable<Type> attributeTypes = new[] { typeof(DispatcherThreadAttribute) };
        private bool suppressOnSuppressPropertyChangedScope;
        private bool hasInitialized;
        private readonly object syncRoot = new object();

        public override IEnumerable<Type> AttributeTypes
        {
            get { return attributeTypes; }
        }

        public override void Intercept(IInvocation invocation)
        {
            ApplyConcern(invocation.Target);

            if (suppressOnSuppressPropertyChangedScope && SuppressPropertyChangedScope.Current != null)
            {
                invocation.Proceed();
                return;
            }

            Dispatcher dispatcher = null;

            if (Application.Current != null) dispatcher = Application.Current.Dispatcher;

            if (dispatcher == null || dispatcher.CheckAccess()) invocation.Proceed();

            else DoDispatcherInvoke(invocation, dispatcher);

        }

        public override bool ShouldIntercept(Type type, MethodInfo method)
        {
            // We are interested only in setters
            return base.ShouldIntercept(type, method) && SettersFilter.ShouldIntercept(type, method);
        }

        private static void DoDispatcherInvoke(IInvocation invocation, Dispatcher dispatcher)
        {
            var transaction = Transaction.Current;
            dispatcher.Invoke(() =>
            {
                using (new DependentTransactionScope(transaction, true))
                {
                    invocation.Proceed();
                }
            });
        }

        private void ApplyConcern(object value)
        {
            lock (syncRoot)
            {
                if (hasInitialized) return;
                hasInitialized = true;

                var attribute = value.GetType().GetAttribute<SupportsDispatcherThreadAttribute>();
                suppressOnSuppressPropertyChangedScope = attribute == null || attribute.SuppressOnSuppressPropertyChangedScope;
            }
        }
    }

    /// <summary>
    /// Constructs the specified type using the service provider.
    /// </summary>
    public class ServiceExtension : MarkupExtension
    {
        [ConstructorArgument("type")]
        public Type Type { get; private set; }

        public ServiceExtension(Type type)
        {
            Type = type;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Type == null)
            {
                throw new Exception("Type required.");
            }

            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                return Activator.CreateInstance(Type);
            }

            var instance = ServiceProvider.Current.GetService(Type);
            return instance;
        }
    }

    /// <summary>
    /// An interface for a view model that includes default behavior support.
    /// </summary>
    [SupportsDispatcherThread(SuppressOnSuppressPropertyChangedScope = true)]
    [SupportsNotifyPropertyChanged(notifyAnyPropertyChanged: true, notifyChildPropertyChanged: true)]
    public interface IViewModel
    {

    }

    /// <summary>
    /// An interface for a view context (a root view model for a view) that includes default behavior support.
    /// </summary>
    public interface IViewContext : IViewModel, IInteractionContextAware
    {
    }

    public interface IDetermineSaveStatus
    {
        bool IsInAcceptableSaveState { get; }
    }

    /// <summary>
    /// Converts enum values to a specified attribute type and property value.
    /// </summary>
    public class EnumAttributeConverter : IValueConverter
    {
        public Type AttributeType { get; set; }

        public string PropertyName { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            AttributeType.EnsureNotDefault("Must specify AttributeType.");

            if (!(value is Enum)) return null;

            var e = (Enum)value;

            var attribute = e.GetAttribute(AttributeType);

            if (attribute == null) return null;

            if (PropertyName == null) return attribute.ToString();

            var propertyInfo = attribute.GetType().GetProperty(PropertyName);

            if (propertyInfo == null) return attribute.ToString();

            return propertyInfo.GetValue(attribute, null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A behavior to support binding and synchronizing a selected items collection.
    /// </summary>
    public class SelectedItemsBehavior : Behavior<Control>
    {
        private IList ControlSelectedItems
        {
            get
            {
                if (AssociatedObject != null)
                {
                    var property = AssociatedObject.GetType().GetProperty("SelectedItems");
                    if (property == null) throw new InvalidOperationException("Could not find SelectedItems property on control.");
                    var collection = property.GetValue(AssociatedObject, null) as IList;
                    if (collection == null) throw new InvalidOperationException("Could not find collection in SelectedItems on control.");
                    return collection;
                }
                return null;
            }
        }

        public IList SelectedItems
        {
            get { return (IList)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(INotifyCollectionChanged), typeof(SelectedItemsBehavior), new PropertyMetadata(OnSelectedItemsPropertyChanged));


        public object ValidationNotifier
        {
            get { return GetValue(ValidationNotifierProperty); }
            set { SetValue(ValidationNotifierProperty, value); }
        }

        public static readonly DependencyProperty ValidationNotifierProperty =
            DependencyProperty.Register("ValidationNotifier", typeof(object), typeof(SelectedItemsBehavior));

        // used as a data item to bind to the AssociatedObject's Arbitrary Validation Binding.
        private readonly object boundValidationProxyDataElement = new object();

        private BindingExpression AssociatedObjectBindingExpression
        {
            get { return BindingOperations.GetBindingExpression(AssociatedObject, ValidationNotifierProperty); }
        }

        private BindingExpression SelectedItemsBindingExpression
        {
            get { return BindingOperations.GetBindingExpression(this, SelectedItemsProperty); }
        }

        private static bool isTransferring;

        private static void OnSelectedItemsPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs args)
        {
            var selectedItemsBehavior = target as SelectedItemsBehavior;
            if (selectedItemsBehavior == null) return;

            Transfer(args.NewValue as IList, selectedItemsBehavior.ControlSelectedItems, true, true);

            var collection = args.NewValue as INotifyCollectionChanged;
            if (collection != null)
            {
                collection.CollectionChanged += new NotifyCollectionChangedEventHandler(selectedItemsBehavior.OnContextSelectedItemsCollectionChanged).MakeWeak<NotifyCollectionChangedEventHandler>();
            }

            selectedItemsBehavior.SyncBindingValidation();
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            if (SelectedItems == null)
            {
                var expression = BindingOperations.GetBindingExpression(this, SelectedItemsProperty);
                if (expression != null && expression.DataItem != null && expression.ParentBinding != null && expression.ParentBinding.Path != null && expression.ParentBinding.Path.Path.IsNotNullOrEmpty())
                {
                    var property = expression.DataItem.GetType().GetProperty(expression.ParentBinding.Path.Path);
                    if (property != null && property.CanWrite)
                    {
                        var elementType = property.PropertyType.FindElementType();
                        if (elementType != null)
                        {
                            var collectionType = typeof(ObservableCollection<>).MakeGenericType(elementType);
                            if (property.PropertyType.IsAssignableFrom(collectionType))
                            {
                                SelectedItems = (IList)collectionType.CreateInstance();
                            }
                        }
                    }
                }
            }

            Transfer(SelectedItems, ControlSelectedItems, true, true);

            var collection = ControlSelectedItems as INotifyCollectionChanged;
            if (collection != null)
            {
                collection.CollectionChanged += new NotifyCollectionChangedEventHandler(OnControlSelectedItemsCollectionChanged).MakeWeak<NotifyCollectionChangedEventHandler>();
            }

            var arbitraryBindingForValidation = new Binding { Mode = BindingMode.OneWay, NotifyOnValidationError = true, ValidatesOnDataErrors = true, Source = boundValidationProxyDataElement };
            AssociatedObject.SetBinding(ValidationNotifierProperty, arbitraryBindingForValidation);

            SyncBindingValidation();
        }

        // when a notifyCollectionChanged event has been fired on the source collection
        private void OnContextSelectedItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Transfer(e, SelectedItems, ControlSelectedItems);
            SyncBindingValidation();
        }

        // when the UI control's 'Selected Items' have been modified by user interaction (target changed)
        private void OnControlSelectedItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Transfer(e, ControlSelectedItems, SelectedItems);
            SyncBindingValidation();
        }

        private void Transfer(NotifyCollectionChangedEventArgs e, IEnumerable source, IList target)
        {
            if (e.Action == NotifyCollectionChangedAction.Add) source = e.NewItems;
            else if (e.Action == NotifyCollectionChangedAction.Remove) source = e.OldItems;

            Transfer(source, target,
                     !new[] { NotifyCollectionChangedAction.Add, NotifyCollectionChangedAction.Remove }.Contains(e.Action),
                     e.Action != NotifyCollectionChangedAction.Remove);
        }

        public static void Transfer(IEnumerable source, IList target, bool clearTarget, bool add)
        {
            if (source == null || target == null || isTransferring || (clearTarget && source.OfType<object>().SequenceEqual(target.OfType<object>())))
                return;

            isTransferring = true;

            if (clearTarget) target.Clear();

            foreach (var o in source)
            {
                if (add)
                    target.Add(o);
                else
                    target.Remove(o);
            }

            isTransferring = false;
        }

        // get any binding errors that occured on the SelectedItemsProperty attached property and add them to a 
        // default property on the associated control so the control knows there is a validation error
        private void SyncBindingValidation()
        {
            if (!CheckCanSyncBindingValidation()) return;

            if (!SelectedItemsBindingExpression.HasError)
            {
                System.Windows.Controls.Validation.ClearInvalid(AssociatedObjectBindingExpression);
            }
            else if (SelectedItemsBindingExpression.ValidationError != null)
            {
                // mark the binding attached to the control as invalid and set it's validation error to the validation error of the SelectedItems
                System.Windows.Controls.Validation.MarkInvalid(AssociatedObjectBindingExpression, SelectedItemsBindingExpression.ValidationError);
            }
        }

        private bool CheckCanSyncBindingValidation()
        {
            if (isTransferring || AssociatedObject == null || SelectedItemsProperty == null
                || SelectedItemsBindingExpression == null || SelectedItemsBindingExpression.Status != BindingStatus.Active
                || SelectedItemsBindingExpression.ParentBinding == null || !SelectedItemsBindingExpression.ParentBinding.ValidatesOnDataErrors
                || AssociatedObjectBindingExpression == null) return false;

            return true;
        }
    }

    /// <summary>
    /// Auto-scrolls a textbox to the end whenever it changes.
    /// </summary>
    public class AutoScrollTextBoxBehavior : Behavior<TextBox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.TextChanged += OnTextChanged;
        }

        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            AssociatedObject.ScrollToEnd();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.TextChanged -= OnTextChanged;
        }
    }

    /// <summary>
    /// Only auto generates data fields for properties with the Display attribute.
    /// </summary>
    public class AutoGenerateDisplayFieldsBehavior : Behavior<RadDataForm>
    {
        public UpdateSourceTrigger UpdateSourceTrigger { get; set; }

        public BindingMode BindingMode { get; set; }

        public object TargetNullValue { get; set; }

        public bool ValidatesOnDataErrors { get; set; }

        public bool ValidatesOnExceptions { get; set; }

        public AutoGenerateDisplayFieldsBehavior()
        {
            TargetNullValue = string.Empty;
            BindingMode = BindingMode.TwoWay;
            UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            ValidatesOnDataErrors = true;
            ValidatesOnExceptions = true;
        }

        protected override void OnAttached()
        {
            AssociatedObject.AutoGenerateFields = true;
            AssociatedObject.AutoGeneratingField += OnAutoGeneratingField;
            base.OnAttached();
        }

        private void OnAutoGeneratingField(object sender, AutoGeneratingFieldEventArgs e)
        {
            if (AssociatedObject.CurrentItem == null || e.DataField.DataMemberBinding == null || e.DataField.DataMemberBinding.Path == null) return;

            var propertyInfo = AssociatedObject.CurrentItem.GetType().GetProperty(e.DataField.DataMemberBinding.Path.Path);

            if (propertyInfo != null && !propertyInfo.HasAttribute<DisplayAttribute>())
            {
                e.Cancel = true;
            }
            else
            {
                e.DataField.DataMemberBinding.Mode = BindingMode;
                e.DataField.DataMemberBinding.UpdateSourceTrigger = UpdateSourceTrigger;
                e.DataField.DataMemberBinding.TargetNullValue = TargetNullValue;
                e.DataField.DataMemberBinding.ValidatesOnDataErrors = ValidatesOnDataErrors;
                e.DataField.DataMemberBinding.ValidatesOnExceptions = ValidatesOnExceptions;
            }
        }

        protected override void OnDetaching()
        {
            AssociatedObject.AutoGeneratingField -= OnAutoGeneratingField;
            AssociatedObject.AutoGenerateFields = false;
        }
    }

    public class ClickBehavior : Behavior<FrameworkElement>
    {
        protected override void OnAttached()
        {
            AssociatedObject.MouseLeftButtonDown += (s, e) => AssociatedObject.CaptureMouse();
            AssociatedObject.MouseLeftButtonUp += (s, e) =>
            {
                if (!AssociatedObject.IsMouseCaptured) return;
                AssociatedObject.ReleaseMouseCapture();
                if (AssociatedObject.InputHitTest(e.GetPosition(AssociatedObject)) != null)
                    AssociatedObject.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
            };
        }
    }

    public class DoubleClickBehavior : Behavior<FrameworkElement>
    {
        private bool isDoubleClicking;

        protected override void OnAttached()
        {
            AssociatedObject.MouseLeftButtonDown += (s, e) =>
            {
                isDoubleClicking = false;
                if (e.ClickCount != 2) return;
                isDoubleClicking = true;
                AssociatedObject.CaptureMouse();
            };
            AssociatedObject.MouseLeftButtonUp += (s, e) =>
            {
                if (!AssociatedObject.IsMouseCaptured) return;
                AssociatedObject.ReleaseMouseCapture();
                if (AssociatedObject.InputHitTest(e.GetPosition(AssociatedObject)) != null && isDoubleClicking)
                {
                    AssociatedObject.RaiseEvent(new RoutedEventArgs(Control.MouseDoubleClickEvent));
                    isDoubleClicking = false;
                }
            };

        }
    }

    public static class FocusBehavior
    {
        public static readonly DependencyProperty FocusOnLoadProperty =
            DependencyProperty.RegisterAttached(
                "FocusOnLoad",
                typeof(bool),
                typeof(Control),
                new PropertyMetadata(false, OnFocusOnLoadPropertyChanged));

        public static bool GetFocusOnLoad(Control control)
        {
            return (bool)control.GetValue(FocusOnLoadProperty);
        }

        public static void SetFocusOnLoad(Control control, bool value)
        {
            control.SetValue(FocusOnLoadProperty, value);
        }

        static void OnFocusOnLoadPropertyChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var control = obj as Control;
            if (control == null || !(args.NewValue is bool))
            {
                return;
            }

            if ((bool)args.NewValue)
            {
                control.Loaded += (sender, e) => control.Focus();
            }
        }
    }

    /// <summary>
    /// A strongly typed view.
    /// </summary>
    public class View : UserControl, IInteractionContextAware, INotifyPropertyChanged
    {
        private bool isFirstLoad = true;
        private bool isLoadPending;

        static View()
        {
            StyleManager.Initialize();
        }

        public View() : this(null) { }

        public View(Type viewContextType)
        {
            ViewContextType = viewContextType ?? typeof(object);
            InheritInteractionContext = true;

            Loaded += (sender, e) =>
            {
                InitializeInteractionContextPropertyBinding(this);
                PropertyChanged.Fire(this, () => ParentInteractionContextAware);

                if (isFirstLoad && Load != null && Load.CanExecute(null)) Load.Execute(null);
                else if (isFirstLoad) isLoadPending = true;
                isFirstLoad = false;
            };
        }

        /// <summary>
        /// Gets the DataContext casted as an object of type 'TContext'.
        /// If it cannot be casted to TContext, throws an InvalidCastException.
        /// </summary>
        /// <typeparam name="TContext">The Type of DataContext to get.</typeparam>
        /// <returns></returns>
        public TContext GetDataContext<TContext>() where TContext : class
        {
            var castedDataContext = DataContext as TContext;
            if (castedDataContext == null)
            {
                throw new InvalidCastException(string.Format("Could not cast DataContext of Type {0} to Type {1}.", DataContext.GetType().FullName, typeof(TContext).FullName));
            }
            return castedDataContext;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public static readonly DependencyProperty ViewContextTypeProperty = DependencyProperty.Register("ViewContextType", typeof(Type), typeof(View), new PropertyMetadata(OnViewContextTypeChanged));

        /// <summary>
        /// Gets or sets the ViewContext type.
        /// </summary>
        public virtual Type ViewContextType
        {
            get { return (Type)GetValue(ViewContextTypeProperty); }
            set { SetValue(ViewContextTypeProperty, value); }
        }

        private static void OnViewContextTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = d as View;
            UpdateDataContext(view);
        }

        public static readonly DependencyProperty LoadProperty = DependencyProperty.Register("Load", typeof(ICommand), typeof(View), new PropertyMetadata(null, OnLoadChanged));

        private static void OnLoadChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = (View)d;
            var command = e.NewValue as ICommand;
            if (view.isLoadPending && command != null)
            {
                if (command.CanExecute()) command.Execute();
                view.isLoadPending = false;
            }
        }

        /// <summary>
        /// Gets or sets the load command.
        /// </summary>
        /// <value>
        /// The load.
        /// </value>
        public ICommand Load
        {
            get { return (ICommand)GetValue(LoadProperty); }
            set { SetValue(LoadProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register("Header", typeof(object), typeof(View), new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets the header for this view.
        /// </summary>
        /// <value>
        /// The header.
        /// </value>
        public object Header
        {
            get { return GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        /// <summary>
        /// Gets or sets the interaction context.
        /// </summary>
        public static DependencyProperty InteractionContextProperty = DependencyProperty.Register("InteractionContext", typeof(IInteractionContext), typeof(View), new PropertyMetadata(OnInteractionContextChanged));

        /// <summary>
        /// Gets or sets the interaction context.
        /// </summary>
        /// <value>
        /// The interaction context.
        /// </value>
        public IInteractionContext InteractionContext
        {
            get { return GetValue(InteractionContextProperty) as IInteractionContext; }
            set { SetValue(InteractionContextProperty, value); }
        }

        public static DependencyProperty ClosingProperty = DependencyProperty.Register("Closing", typeof(ICommand), typeof(View), null);

        /// <summary>
        /// Gets or sets the closing command.
        /// </summary>
        public ICommand Closing
        {
            get { return GetValue(ClosingProperty) as ICommand; }
            set { SetValue(ClosingProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to auto create the data context type.
        /// </summary>
        public static DependencyProperty AutoCreateDataContextProperty = DependencyProperty.Register("AutoCreateDataContext", typeof(bool), typeof(View), new PropertyMetadata(false, OnAutoCreateDataContextChanged));

        private static void OnAutoCreateDataContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = (View)d;
            UpdateDataContext(view);
        }

        private static void UpdateDataContext(View view)
        {
            if (view.AutoCreateDataContext
                && view.ViewContextType != typeof(object) // Wait until both AutoCreateDataContext and ViewContextType are set
                && !DesignerProperties.GetIsInDesignMode(new ContentControl()))
            {
                var dc = ServiceProvider.Current.GetService(typeof(Func<,>).MakeGenericType(typeof(IInteractionContext), view.ViewContextType)).CastTo<Delegate>().DynamicInvoke(view.InteractionContext);
                view.DataContext = null;
                view.DataContext = dc;

                // DataBinding isn't guaranteed...and sometimes it isn't happening right away
                // So force it to happen as soon as possible
                view.Dispatcher.BeginInvoke(new Action(() => { }), DispatcherPriority.DataBind);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to auto create the data context type.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [auto resolve data context]; otherwise, <c>false</c>.
        /// </value>
        public bool AutoCreateDataContext
        {
            get { return (bool)GetValue(AutoCreateDataContextProperty); }
            set { SetValue(AutoCreateDataContextProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to inherit an interaction context defined by a visual parent.
        /// </summary>
        public static DependencyProperty InheritInteractionContextProperty = DependencyProperty.Register("InheritInteractionContext", typeof(bool), typeof(View), new PropertyMetadata(false, OnInheritInteractionContextChanged));

        private static void OnInheritInteractionContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = ((View)d);
            InitializeInteractionContextPropertyBinding(view);
        }

        private static void InitializeInteractionContextPropertyBinding(View view)
        {
            if (view.GetBindingExpression(InheritInteractionContextProperty) == null)
            {
                view.SetBinding(InteractionContextProperty, view.InheritInteractionContext
                    ? new Binding("ParentInteractionContextAware.InteractionContext") { Source = view, Mode = BindingMode.OneWay }
                    : new Binding("InteractionContext") { Source = view, Mode = BindingMode.OneTime });
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to inherit an interaction context defined by a visual parent.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [auto resolve data context]; otherwise, <c>false</c>.
        /// </value>
        public bool InheritInteractionContext
        {
            get { return (bool)GetValue(InheritInteractionContextProperty); }
            set { SetValue(InheritInteractionContextProperty, value); }
        }


        private static void OnInteractionContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = (View)d;
            if (e.OldValue != null)
            {
                e.OldValue.CastTo<IInteractionContext>().Completing -= new EventHandler<CancelEventArgs>(view.OnInteractionContextCompleting).MakeWeak();
            }
            if (e.NewValue != null)
            {
                e.NewValue.CastTo<IInteractionContext>().Completing += new EventHandler<CancelEventArgs>(view.OnInteractionContextCompleting).MakeWeak();
            }

            var userControl = d as UserControl;
            if (userControl != null)
            {
                var interactionContextAware = userControl.DataContext as IInteractionContextAware;
                if (interactionContextAware != null) interactionContextAware.InteractionContext = e.NewValue as IInteractionContext;
            }
        }

        private void OnInteractionContextCompleting(object sender, CancelEventArgs e)
        {
            if (Closing != null)
            {
                Action cancelClosingAction = () => e.Cancel = true;
                Closing.Execute(cancelClosingAction);
            }
        }

        public IInteractionContextAware ParentInteractionContextAware
        {
            get { return this.GetParents().OfType<IInteractionContextAware>().FirstOrDefault(); }
        }
    }

    /// <summary>
    /// A strongly typed view.
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public class View<TContext> : View
    {
        public View()
            : base(typeof(TContext))
        {
        }

        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        /// <value>
        /// The view model.
        /// </value>
        public new TContext DataContext
        {
            get
            {
                return (TContext)base.DataContext;
            }
            set
            {
                base.DataContext = value;
            }
        }
    }

    /// <summary>
    /// Listens for property changes on a dependency object using a weak reference.
    /// </summary>
    public sealed class PropertyChangeNotifier :
DependencyObject,
IDisposable
    {
        #region Member Variables
        private readonly WeakReference propertySource;
        #endregion // Member Variables

        #region Constructor
        public PropertyChangeNotifier(DependencyObject propertySource, string path)
            : this(propertySource, new PropertyPath(path))
        {
        }
        public PropertyChangeNotifier(DependencyObject propertySource, DependencyProperty property)
            : this(propertySource, new PropertyPath(property))
        {
        }
        public PropertyChangeNotifier(DependencyObject propertySource, PropertyPath property)
        {
            if (null == propertySource)
                throw new ArgumentNullException("propertySource");
            if (null == property)
                throw new ArgumentNullException("property");
            this.propertySource = new WeakReference(propertySource);
            var binding = new Binding { Path = property, Mode = BindingMode.OneWay, Source = propertySource };
            BindingOperations.SetBinding(this, ValueProperty, binding);
        }
        #endregion // Constructor

        #region PropertySource
        public DependencyObject PropertySource
        {
            get
            {
                try
                {
                    // note, it is possible that accessing the target property
                    // will result in an exception so i’ve wrapped this check
                    // in a try catch
                    return propertySource.IsAlive
                    ? propertySource.Target as DependencyObject
                    : null;
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion // PropertySource

        #region Value
        /// <summary>
        /// Identifies the <see cref="Value"/> dependency property
        /// </summary>
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value",
        typeof(object), typeof(PropertyChangeNotifier), new PropertyMetadata(null, OnPropertyChanged));

        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var notifier = (PropertyChangeNotifier)d;
            if (null != notifier.ValueChanged)
                notifier.ValueChanged(notifier, EventArgs.Empty);
        }

        /// <summary>
        /// Returns/sets the value of the property
        /// </summary>
        /// <seealso cref="ValueProperty"/>
        [Description("Returns/sets the value of the property")]
        [Category("Behavior")]
        [Bindable(true)]
        public object Value
        {
            get
            {
                return GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }
        #endregion //Value

        #region Events
        public event EventHandler ValueChanged;
        #endregion // Events

        #region IDisposable Members
        public void Dispose()
        {
            BindingOperations.ClearBinding(this, ValueProperty);
        }
        #endregion

        public PropertyChangeNotifier AddValueChanged(EventHandler valueChanged)
        {
            ValueChanged += valueChanged;
            return this;
        }

        public PropertyChangeNotifier RemoveValueChanged(EventHandler valueChanged)
        {
            ValueChanged -= valueChanged;
            return this;
        }
    }

    /// <summary>
    /// Interface that identifies a type which supports LoadArguments.
    /// </summary>
    public interface IHasLoadArguments<TLoadArguments> where TLoadArguments : class
    {
        TLoadArguments LoadArguments { get; set; }
    }

    /// <summary>
    /// Interface that identifies a type which supports ReturnArguments.
    /// </summary>
    public interface IHasReturnArguments<TReturnArguments> where TReturnArguments : class
    {
        TReturnArguments ReturnArguments { get; }
    }

    /// <summary>
    /// A manager component that supports showing a view using specified LoadArguments and returning specified ReturnArguments
    /// </summary>
    [Singleton]
    public class ViewManager : Freezable
    {
        private static readonly MethodInfo ViewMethod = Reflector.GetMember<ViewManager>(vm => vm.View<object, object>(null, null)).As<MethodInfo>().EnsureNotDefault().GetGenericMethodDefinition();

        /// <summary>
        /// The content for the displayed view. May be a FrameworkElement, DataTemplate or Type.
        /// </summary>
        public static readonly DependencyProperty ViewProperty = DependencyProperty.Register("Content", typeof(object), typeof(ViewManager), new PropertyMetadata(null), v => v is FrameworkElement || v is DataTemplate || v is Type || v == null);

        /// <summary>
        /// The content for the displayed view. May be a FrameworkElement, DataTemplate or Type.
        /// </summary>
        /// <value>The content.</value>
        public object Content
        {
            get { return GetValue(ViewProperty); }
            set { SetValue(ViewProperty, value); }
        }
        /// <summary>
        /// Shows the view with the specified load arguments.
        /// </summary>
        /// <returns>
        /// Any return arguments
        /// </returns>
        public object View()
        {
            return View(null);
        }

        /// <summary>
        /// Shows the view with the specified load arguments.
        /// </summary>
        /// <param name="loadArguments">The load arguments.</param>
        /// <returns>Any return arguments</returns>
        public object View(object loadArguments)
        {
            var view = CreateView();

            if (view == null) return null;

            var loadArgumentsType = loadArguments == null ? typeof(object) : loadArguments.GetType();

            Func<object, Type> getReturnArgumentsType = o => o == null ? null :
                o.GetType().GetInterfaces().Where(i => i.EqualsGenericTypeFor(typeof(IHasReturnArguments<>))).Select(t => t.GetGenericArguments()[0]).FirstOrDefault();

            var returnArgumentsType = getReturnArgumentsType(view.DataContext) ?? getReturnArgumentsType(view) ?? typeof(object);

            var returnArguments = ViewMethod.MakeGenericMethod(loadArgumentsType, returnArgumentsType).Invoke(this, new[] { view, loadArguments });

            return returnArguments;
        }

        /// <summary>
        /// Shows the view with the specified load arguments.
        /// </summary>
        /// <typeparam name="TLoadArguments">The type of the T load arguments.</typeparam>
        /// <typeparam name="TReturnArguments">The type of the T return arguments.</typeparam>
        /// <param name="loadArguments">The load arguments.</param>
        /// <returns></returns>
        public TReturnArguments View<TLoadArguments, TReturnArguments>(TLoadArguments loadArguments = null)
            where TLoadArguments : class
            where TReturnArguments : class
        {
            var view = CreateView();

            if (view == null) { return null; }

            return View<TLoadArguments, TReturnArguments>(view, loadArguments);
        }

        private TReturnArguments View<TLoadArguments, TReturnArguments>(FrameworkElement view, TLoadArguments loadArguments = null)
            where TLoadArguments : class
            where TReturnArguments : class
        {
            var hasLoadArguments = view.DataContext as IHasLoadArguments<TLoadArguments> ?? view as IHasLoadArguments<TLoadArguments>;

            if (hasLoadArguments != null && loadArguments != null)
            {
                hasLoadArguments.LoadArguments = loadArguments;
            }

            var windowArguments = new WindowInteractionArguments { Content = view, ResizeMode = ResizeMode.CanResize };

            InteractionManager.Current.Show(windowArguments);

            var hasReturnArguments = view.DataContext as IHasReturnArguments<TReturnArguments> ?? view as IHasReturnArguments<TReturnArguments>;

            var returnArguments = hasReturnArguments == null ? null : hasReturnArguments.ReturnArguments;

            return returnArguments;
        }

        /// <summary>
        /// Creates the view by looking at the data type.
        /// </summary>
        /// <returns></returns>
        private FrameworkElement CreateView()
        {
            var view = Content;

            if (view is DataTemplate) view = new ContentControl { ContentTemplate = (DataTemplate)view }.ChildrenOfType<FrameworkElement>().FirstOrDefault();
            else if (view is Type) view = Activator.CreateInstance((Type)view);

            return view as FrameworkElement;
        }

        protected override Freezable CreateInstanceCore()
        {
            return this;
        }
    }

    /// <summary>
    /// A special ICommand that allows binding to the return value of the method and also supports calling methods on automatically resolved instances.
    /// </summary>
    public class CallMethodCommand : Freezable, ICommand
    {
        public static readonly DependencyProperty MethodNameProperty = DependencyProperty.Register("MethodName", typeof(string), typeof(CallMethodCommand), new PropertyMetadata(null));

        public string MethodName
        {
            get
            {
                return (string)GetValue(MethodNameProperty);
            }
            set
            {
                SetValue(MethodNameProperty, value);
            }
        }

        public static readonly DependencyProperty MethodParameterProperty = DependencyProperty.Register("MethodParameter", typeof(object), typeof(CallMethodCommand), new PropertyMetadata(null));

        public object MethodParameter
        {
            get
            {
                return GetValue(MethodParameterProperty);
            }
            set
            {
                SetValue(MethodParameterProperty, value);
            }
        }

        public static readonly DependencyProperty TargetTypeProperty = DependencyProperty.Register("TargetType", typeof(Type), typeof(CallMethodCommand), new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets the type of the target. If set, will resolve the target type and invoke the method on that target instead of using the default TargetedTriggerAction.Target.
        /// </summary>
        /// <value>The type of the target.</value>
        public Type TargetType
        {
            get
            {
                return (Type)GetValue(TargetTypeProperty);
            }
            set
            {
                SetValue(TargetTypeProperty, value);
            }
        }

        public static readonly DependencyProperty ReturnValueProperty = DependencyProperty.Register("ReturnValue", typeof(object), typeof(CallMethodCommand), new FrameworkPropertyMetadata { BindsTwoWayByDefault = true });

        /// <summary>
        /// Gets or sets the return value of the called method. Will set to a new value each time the method is called.
        /// </summary>
        /// <value>The return value.</value>
        public object ReturnValue
        {
            get
            {
                return GetValue(ReturnValueProperty);
            }
            set
            {
                SetValue(ReturnValueProperty, value);
            }
        }

        public static readonly DependencyProperty TargetProperty = DependencyProperty.Register("Target", typeof(object), typeof(CallMethodCommand), new FrameworkPropertyMetadata { BindsTwoWayByDefault = true });

        /// <summary>
        /// Gets or sets the return value of the called method. Will set to a new value each time the method is called.
        /// </summary>
        /// <value>The return value.</value>
        public object Target
        {
            get
            {
                return GetValue(TargetProperty);
            }
            set
            {
                SetValue(TargetProperty, value);
            }
        }


        public event EventHandler CanExecuteChanged = delegate { };

        public void Execute(object parameter)
        {
            var arguments = MethodParameter == null ? new object[0] : new[] { MethodParameter };

            var target = TargetType == null ? Target : ServiceProvider.Current.GetService(TargetType);

            ReturnValue = target.Invoke(MethodName, arguments);
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        protected override Freezable CreateInstanceCore()
        {
            return this;
        }
    }
}