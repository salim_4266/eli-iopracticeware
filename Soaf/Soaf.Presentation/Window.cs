﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using Soaf.Presentation.Controls;
using Soaf.Presentation.Interop;
using Soaf.Reflection;
using Telerik.Windows.Controls;
using Application = System.Windows.Application;
using Binding = System.Windows.Data.Binding;
using Button = System.Windows.Controls.Button;
using Control = System.Windows.Forms.Control;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using Panel = System.Windows.Controls.Panel;

namespace Soaf.Presentation
{
    /// <summary>
    ///     A default window implementation.
    /// </summary>
    [TemplatePart(Name = "PART_Close")]
    [TemplatePart(Name = "PART_Maximize")]
    [TemplatePart(Name = "PART_Minimize")]
    public class Window : System.Windows.Window, IWindow
    {
        protected const string PartClose = "PART_Close";
        protected const string PartMaximize = "PART_Maximize";
        protected const string PartMinimize = "PART_Minimize";

        public static readonly DependencyProperty IsModalProperty = DependencyProperty.Register(
            "IsModal", typeof (bool), typeof (Window), new PropertyMetadata(ComponentDispatcher.IsThreadModal));

        public static readonly DependencyProperty WindowFrameStyleProperty = DependencyProperty.Register(
            "WindowFrameStyle", typeof (WindowFrameStyle), typeof (Window), new PropertyMetadata(WindowFrameStyle.Normal, OnWindowFrameStylePropertyChanged));

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof (object), typeof (Window), new PropertyMetadata(null, OnHeaderPropertyChangedCallback));

        public static readonly DependencyProperty CanCloseProperty =
            DependencyProperty.Register("CanClose", typeof (bool), typeof (Window), new PropertyMetadata(true, OnCanClosePropertyChanged));

        private readonly ContentControl contentControl;
        private readonly Panel contentLayoutPanel;
        private readonly DialogButtonsControl dialogButtonsControl;
        private DialogButtons? dialogButtons;
        private IInteractionContext interactionContext;

        private bool isMousePositionStartupLocation;
        private bool isContentChanging;
        private LoadingWindow loadingWindow;

        static Window()
        {
            StyleManager.Initialize();
        }

        public Window()
        {
            // Calculate owner
            System.Windows.Window owner = Application.Current.Windows
                                                     .OfType<System.Windows.Window>()
                                                     .FirstOrDefault(i => i.IsActive)
                                          ?? Application.Current.MainWindow;

            // Ensure we are not making Loading Window an owner of this window, since
            // when Loading Window will close -> this Window will forcibly be closed too
            if (owner is LoadingWindow)
            {
                owner = owner.Owner;
            }

            // Actually set owner if it's not us
            if (!Equals(owner, this))
            {
                Owner = owner;
            }

            // We want the window to size to contents and then make contents size to window (see OnWindowLoaded)
            // TODO: later we probably would want to preserve user's setting after window has actually been loaded
            SizeToContent = SizeToContent.WidthAndHeight;

            // Subscribe to events
            Loaded += OnWindowLoaded;
            var resizeModeNotifier = new PropertyChangeNotifier(this, ResizeModeProperty);
            resizeModeNotifier.AddValueChanged(OnResizeModePropertyChanged);

            // Build core layout
            contentLayoutPanel = BuildContentLayoutContainer(out dialogButtonsControl, out contentControl);
            Content = contentLayoutPanel;
        }

        public new virtual event EventHandler<CancelEventArgs> Closing;
        public new virtual event EventHandler<EventArgs> Closed;
        public event PropertyChangedEventHandler PropertyChanged;

        private Panel BuildContentLayoutContainer(out DialogButtonsControl buttonsControl, out ContentControl contentControl)
        {
            // Define layout
            var panel = new Grid();
            panel.ColumnDefinitions.Add(new ColumnDefinition());
            panel.RowDefinitions.Add(new RowDefinition());
            // A row for dialog buttons control which can hide and show
            panel.RowDefinitions.Add(new RowDefinition {Height = GridLength.Auto});

            // Content control
            contentControl = new ContentControl();

            // Create buttons control
            buttonsControl = new DialogButtonsControl
                                 {
                                     BorderBrush = new SolidColorBrush(Colors.White),
                                     BorderThickness = new Thickness(0, 1, 0, 0),
                                     VerticalAlignment = VerticalAlignment.Bottom,
                                     VerticalContentAlignment = VerticalAlignment.Bottom,
                                     HorizontalAlignment = HorizontalAlignment.Stretch,
                                     HorizontalContentAlignment = HorizontalAlignment.Stretch,
                                     Visibility = Visibility.Collapsed // Hidden by default
                                 };

            buttonsControl.DialogButtonClicked += OnDialogButtonsControlButtonClicked;

            // Place controls on a panel
            contentControl.SetValue(Grid.RowProperty, 0);
            panel.Children.Add(contentControl);
            buttonsControl.SetValue(Grid.RowProperty, 1);
            panel.Children.Add(buttonsControl);

            return panel;
        }

        private void OnDialogButtonsControlButtonClicked(object sender, EventArgs<bool?> args)
        {
            InteractionContext.IfNotNull(i => i.Complete(args.Value));
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            RefreshIsModal();

            // Locate the actual contents of the ContentControl
            var control = contentControl
                .ChildrenOfType<ContentPresenter>()
                .First()
                .ChildrenOfType<FrameworkElement>()
                .FirstOrDefault();

            // We had window first size to to contents, so now let's make contents size back to Window
            if (control != null)
            {
                control.ClearValue(HeightProperty);
                control.ClearValue(WidthProperty);

                SizeToContent = SizeToContent.Manual;
                if (WindowState != WindowState.Maximized)
                {
                    Size preferredSize = MeasureOverride(new Size(ActualWidth, ActualHeight));

                    Width = preferredSize.Width > 0 && control is WindowContentWindowsFormsHost 
                        ? preferredSize.Width 
                        : ActualWidth;
                    Height = preferredSize.Height > 0 && control is WindowContentWindowsFormsHost 
                        ? preferredSize.Height 
                        : ActualHeight;

                    Width = ActualWidth;
                    Height = ActualHeight;
                }
            } 

            if (!HasLoaded)
            {
                // Our trick with making sizing to content first and then sizing content back to window
                // causes Window to be misaligned according to startup location set, so fixing it
                if (WindowStartupLocation == System.Windows.WindowStartupLocation.CenterScreen 
                    || (WindowStartupLocation == System.Windows.WindowStartupLocation.CenterOwner && Owner == null))
                {
                    Left = (SystemParameters.WorkArea.Width - Width) / 2 + SystemParameters.WorkArea.Left;
                    Top = (SystemParameters.WorkArea.Height - (Height + 30)) / 2 + SystemParameters.WorkArea.Top;
                }
                else if (WindowStartupLocation == System.Windows.WindowStartupLocation.CenterOwner)
                {
                    Left = Math.Max(0, Owner.Left - ((Width - Owner.Width) / 2));
                    Top = Math.Max(0, Owner.Top - (((Height + 30) - Owner.Height) / 2));
                }
                // See: http://www.formatexception.com/2008/09/wpf-and-mouseposition/
                // Position Window manually when it is expected in MouseLocation which WPF doesn't support out of box
                else if (isMousePositionStartupLocation 
                    && WindowStartupLocation == System.Windows.WindowStartupLocation.Manual)
                {
                    var positionSource = Owner as UIElement 
                        ?? Application.Current.Windows
                        .OfType<System.Windows.Window>()
                        .LastOrDefault(w => w.IsActive) 
                        ?? Application.Current.MainWindow;

                    Point position = positionSource.PointToScreen(Mouse.GetPosition(positionSource));

                    var screen = Screen.FromPoint(new System.Drawing.Point((int)position.X, (int)position.Y));

                    // Prevent window from being cut-off on any side
                    if (ActualHeight + position.Y > screen.Bounds.Bottom)
                    {
                        Top = screen.Bounds.Bottom - ActualHeight;
                    }
                    else if (ActualHeight + position.Y < screen.Bounds.Top)
                    {
                        Top = screen.Bounds.Top;
                    }
                    else
                    {
                        Top = position.Y;
                    }

                    if (ActualWidth + position.X > screen.Bounds.Right)
                    {
                        Left = screen.Bounds.Right - ActualWidth;
                    }
                    else if (ActualWidth + position.X < screen.Bounds.Left)
                    {
                        Left = screen.Bounds.Left;
                    }
                    else
                    {
                        Left = position.X;
                    }
                }
            }

            HasLoaded = true;
        }

        protected virtual void OnMinimize(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        protected virtual void OnMaximize(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState == WindowState.Maximized 
                ? WindowState.Normal 
                : WindowState.Maximized;
        }

        protected virtual void OnClose(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            // Handle actions for template parts
            var minimizeButton = GetChildControl<Button>(PartMinimize);
            if (minimizeButton != null)
            {
                minimizeButton.Click += OnMinimize;
            }
            var maximizeButton = GetChildControl<Button>(PartMaximize);
            if (maximizeButton != null)
            {
                maximizeButton.Click += OnMaximize;
            }
            var closeButton = GetChildControl<Button>(PartClose);
            if (closeButton != null)
            {
                closeButton.Click += OnClose;
            }

            // Perform updates to UI parts
            UpdateCloseButtonState();
            UpdateResizeMode();
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            // Ensure proper Min/Max size is provided when switching monitors
            WindowInteropUtility.ProvideCorrectMinMaxSize(this);

            RefreshIsModal();
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            if (!IsInitialized)
            {
                base.OnContentChanged(oldContent, newContent);
            }
            else if (!isContentChanging)
            {
                isContentChanging = true;

                // Wrap newly set Content inside our layout panel
                contentControl.Content = newContent is Control
                                              ? new WindowContentWindowsFormsHost
                                                    {
                                                        Child = (Control) newContent
                                                    }
                                              : newContent;

                // Reset the content value back to our panel
                Content = contentLayoutPanel;

                isContentChanging = false;
            }
        }

        protected override void OnClosed(EventArgs args)
        {
            Closed.Fire(this, new EventArgs());

            // To prevent memory leak, unsubscribe from InteractionContext
            // The InteractionContext is used after Window is closed to read DialogResult, so we can't null it here
            if (interactionContext != null)
            {
                // Ensure we will not leave busy indicator showing
                HideBusyIndicator();

                interactionContext.PropertyChanged -= OnInteractionContextPropertyChanged;
            }

            base.OnClosed(args);
        }

        protected override void OnClosing(CancelEventArgs args)
        {
            var closingArgs = new CancelEventArgs();
            Closing.Fire(this, closingArgs);
            args.Cancel = closingArgs.Cancel;

            base.OnClosing(args);
        }

        private void OnResizeModePropertyChanged(object sender, EventArgs e)
        {
            UpdateResizeMode();
        }

        private static void OnHeaderPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var target = (Window)dependencyObject;

            // Update title if header represents simple string
            var newValue = dependencyPropertyChangedEventArgs.NewValue as string;
            if (newValue != null && !string.IsNullOrEmpty(newValue))
            {
                target.Title = newValue;
            }
        }

        private static void OnCanClosePropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var target = (Window)dependencyObject;
            target.UpdateCloseButtonState();
        }

        private static void OnWindowFrameStylePropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var target = (Window) dependencyObject;

            switch (target.WindowFrameStyle)
            {
                case WindowFrameStyle.None:
                    {
                        target.ShowInTaskbar = false;
                        target.Topmost = true;
                        break;
                    }
                case WindowFrameStyle.Normal:
                case WindowFrameStyle.Child:
                    {
                        target.ShowInTaskbar = true;
                        target.Topmost = false;
                        break;
                    }
                default:
                    throw new Exception("WindowFrameStyle {0} is not currently supported by Soaf's Window".FormatWith(target.WindowFrameStyle));
            }
        }

        private void RefreshIsModal()
        {
            IsModal = ComponentDispatcher.IsThreadModal;
        }

        private void UpdateCloseButtonState()
        {
            SetButtonVisibility(PartClose, CanClose);
        }

        private void UpdateResizeMode()
        {
            switch (ResizeMode)
            {
                case ResizeMode.NoResize:
                    SetButtonVisibility(PartMaximize, false);
                    SetButtonVisibility(PartMinimize, false);
                    break;
                case ResizeMode.CanMinimize:
                    SetButtonVisibility(PartMaximize, false);
                    SetButtonVisibility(PartMinimize, true);
                    break;
                case ResizeMode.CanResizeWithGrip:
                case ResizeMode.CanResize:
                    SetButtonVisibility(PartMaximize, true);
                    SetButtonVisibility(PartMinimize, true);
                    break;
            }
        }

        protected void SetButtonVisibility(string partName, bool value, bool hideInsteadOfColllapse = false)
        {
            var button = GetChildControl<Button>(partName);
            if (button != null)
            {
                button.Visibility = value 
                    ? Visibility.Visible 
                    : hideInsteadOfColllapse 
                    ? Visibility.Hidden
                    : Visibility.Collapsed;
            }
        }

        protected T GetChildControl<T>(string ctrlName) where T : DependencyObject
        {
            return GetTemplateChild(ctrlName) as T;
        }

        private void OnInteractionContextPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != Reflector.GetMember<IInteractionContext>(i => i.IsBusy).Name)
                return;

            Action action = delegate
                                {
                                    var context = sender.CastTo<IInteractionContext>();
                                    if (context.IsBusy) ShowBusyIndicator();
                                    else HideBusyIndicator();
                                };
            if (CheckAccess()) action();
            else Dispatcher.Invoke(action);
        }

        private void ShowBusyIndicator()
        {
            if (loadingWindow != null && loadingWindow.IsVisible) return;

            if (!IsLoaded)
            {
                throw new Exception("Cannot show BusyIndicator when parent window has not been loaded");
            }

            try
            {
                loadingWindow = new LoadingWindow
                                     {
                                         Owner = this,
                                         AllowsTransparency = true,
                                         Background = new SolidColorBrush(Colors.Transparent),
                                         WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner,
                                         WindowStyle = WindowStyle.None,
                                         ShowInTaskbar = false,
                                         Width = 181,
                                         Height = 100
                                     };
            }
            catch (Exception ex)
            {
                throw new Exception("Error occurred when displaying the BusyIndicator", ex);
            }

            var busyIndicator = new RadBusyIndicator {IsBusy = true, VerticalAlignment = VerticalAlignment.Stretch, VerticalContentAlignment = VerticalAlignment.Stretch, HorizontalAlignment = HorizontalAlignment.Stretch, HorizontalContentAlignment = HorizontalAlignment.Stretch};

            BindingOperations.SetBinding(busyIndicator, RadBusyIndicator.IsBusyProperty, new Binding("InteractionContext.IsBusy") {Source = this});
            BindingOperations.SetBinding(busyIndicator, RadBusyIndicator.BusyContentProperty, new Binding("InteractionContext.Status") {Source = this});

            loadingWindow.Content = busyIndicator;

            Dispatcher.BeginInvoke(new Action(() =>
                                                  {
                                                      if (loadingWindow == null) return;
                                                      loadingWindow.ShowDialog();
                                                  }));
        }

        private void HideBusyIndicator()
        {
            if (loadingWindow == null) return;

            loadingWindow.Close();

            loadingWindow = null;
        }

        #region IWindow Members

        public bool HasLoaded { get; private set; }

        /// <summary>
        /// Specifies whether window can be closed
        /// </summary>
        public bool CanClose
        {
            get { return (bool)GetValue(CanCloseProperty); }
            set { SetValue(CanCloseProperty, value); }
        }

        /// <summary>
        /// Header
        /// </summary>
        public virtual object Header
        {
            get { return GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        public WindowFrameStyle WindowFrameStyle
        {
            get { return (WindowFrameStyle) GetValue(WindowFrameStyleProperty); }
            set { SetValue(WindowFrameStyleProperty, value); }
        }

        public bool IsModal
        {
            get { return (bool) GetValue(IsModalProperty); }
            set { SetValue(IsModalProperty, value); }
        }

        public virtual DialogButtons? DialogButtons
        {
            get { return dialogButtons; }
            set
            {
                if (Equals(value, dialogButtons)) return;

                dialogButtons = value;

                if (value.HasValue)
                {
                    dialogButtonsControl.DialogButtons = dialogButtons.Value;
                }

                // If dialog buttons were provided -> expand the row which holds the control
                dialogButtonsControl.Visibility = value == null
                                                       ? Visibility.Collapsed
                                                       : Visibility.Visible;

                PropertyChanged.Fire(this, () => DialogButtons);
            }
        }

        /// <summary>
        ///     Gets or sets the size.
        /// </summary>
        /// <value>
        ///     The size.
        /// </value>
        public virtual Size Size
        {
            get { return new Size(Width, Height); }
            set
            {
                if (Equals(value, new Size(Width, Height))) return;

                if (!double.IsNaN(value.Width)) Width = value.Width;
                if (!double.IsNaN(value.Height)) Height = value.Height;

                PropertyChanged.Fire(this, () => Size);
            }
        }

        public virtual IInteractionContext InteractionContext
        {
            get { return interactionContext; }
            set
            {
                if (Equals(value, interactionContext)) return;

                // Unsubscribe existing interaction context
                if (interactionContext != null)
                {
                    interactionContext.PropertyChanged -= OnInteractionContextPropertyChanged;
                }

                interactionContext = value;

                // Re-subscribe on new context
                if (interactionContext != null)
                {
                    interactionContext.PropertyChanged += OnInteractionContextPropertyChanged;
                }

                PropertyChanged.Fire(this, () => InteractionContext);

                // Handle showing of busy indicator
                if (interactionContext != null && interactionContext.IsBusy)
                {
                    ShowBusyIndicator();
                }
                else
                {
                    HideBusyIndicator();
                }
            }
        }

        public virtual bool? ShowModal()
        {
            return ShowDialog();
        }

        /// <summary>
        /// To keep using the dependency property defined in base class -> we simply provide an explicit interface implementation
        /// </summary>
        WindowStartupLocation IWindow.WindowStartupLocation
        {
            get { return (WindowStartupLocation)(int)WindowStartupLocation; }
            set
            {
                if (value == Soaf.Presentation.WindowStartupLocation.MousePosition)
                {
                    isMousePositionStartupLocation = true;
                    WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                }
                else
                {
                    isMousePositionStartupLocation = false;
                    WindowStartupLocation = (System.Windows.WindowStartupLocation)(int)value;
                }
            }
        }

        object IWindow.Owner
        {
            get { return Owner; }
            set
            {
                if (value is System.Windows.Window)
                {
                    Owner = (System.Windows.Window) value;
                }
                else if (value is System.Windows.Forms.IWin32Window)
                {
                    var interopHelper = new WindowInteropHelper(this);
                    interopHelper.Owner = ((System.Windows.Forms.IWin32Window)value).Handle;
                }
                else if (value is System.Windows.Interop.IWin32Window)
                {
                    var interopHelper = new WindowInteropHelper(this);
                    interopHelper.Owner = ((System.Windows.Interop.IWin32Window)value).Handle;
                }
            }
        }

        #endregion

        /// <summary>
        /// A special inherited class to differentiate Loading window from other windows
        /// </summary>
        public class LoadingWindow : System.Windows.Window
        {}
    }
}