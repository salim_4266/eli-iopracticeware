﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Reflection;
using Telerik.Windows.Controls;
using Application = System.Windows.Application;
using Binding = System.Windows.Data.Binding;
using Brush = System.Drawing.Brush;
using Color = System.Drawing.Color;
using Control = System.Windows.Forms.Control;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;
using Point = System.Windows.Point;
using Size = System.Windows.Size;

[assembly: Component(typeof(NavigableInteractionManager), typeof(INavigableInteractionManager))]
[assembly: Component(typeof(ViewModelTypeDescriptionProvider), Initialize = true)]


namespace Soaf.Presentation
{


    /// <summary>
    /// An adorner that displays a busy indicator.
    /// </summary>
    public class BusyAdorner : Adorner
    {
        #region BusyScope enum

        public enum BusyScope
        {
            Window,
            Element
        }

        #endregion

        public static DependencyProperty IsBusyProperty = DependencyProperty.RegisterAttached("IsBusy", typeof(bool), typeof(UIElement), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, (d, e) => { if (d is UIElement && e.NewValue is bool) SetBusy(d as UIElement, ((bool)e.NewValue)); }));
        public static DependencyProperty StatusProperty = DependencyProperty.RegisterAttached("Status", typeof(string), typeof(UIElement), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, (d, e) => { if (d is UIElement) SetStatus(d as UIElement, (e.NewValue as string)); }));
        public static DependencyProperty ScopeProperty = DependencyProperty.RegisterAttached("Scope", typeof(BusyScope), typeof(UIElement), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, (d, e) => { if (d is UIElement) SetScope(d as UIElement, ((BusyScope)e.NewValue)); }));

        private readonly RadBusyIndicator busyIndicator;

        public BusyAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
            busyIndicator = new RadBusyIndicator { IsBusy = true };
            AddVisualChild(busyIndicator);
        }

        protected override int VisualChildrenCount
        {
            get { return 1; }
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            busyIndicator.Arrange(new Rect(finalSize));
            return base.ArrangeOverride(finalSize);
        }

        protected override Visual GetVisualChild(int index)
        {
            return index == 0 ? busyIndicator : base.GetVisualChild(index);
        }

        public static void SetIsBusy(UIElement element, bool value)
        {
            element.SetValue(IsBusyProperty, value);
        }

        public static bool GetIsBusy(UIElement element)
        {
            return (bool)element.GetValue(IsBusyProperty);
        }

        private static void SetBusy(UIElement element, bool value)
        {
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(GetScope(element) == BusyScope.Element ? element : element.Parents().OfType<Visual>().Last());

            if (adornerLayer != null)
            {
                BusyAdorner adorner;
                switch (value)
                {
                    case true:
                        adorner = adornerLayer.FindChildByType<BusyAdorner>();
                        if (adorner == null)
                        {
                            adorner = new BusyAdorner(element);
                            adornerLayer.Add(adorner);
                        }
                        break;
                    case false:
                        adorner = adornerLayer.FindChildByType<BusyAdorner>();
                        if (adorner != null)
                        {
                            adornerLayer.Remove(adorner);
                        }
                        break;
                }
            }
        }

        public static void SetStatus(UIElement element, string value)
        {
            if (GetStatus(element) != value && value != null)
            {
                element.SetValue(StatusProperty, value);

                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(GetScope(element) == BusyScope.Element ? element : element.Parents().OfType<Visual>().Last());

                if (adornerLayer != null)
                {
                    var adorner = adornerLayer.FindChildByType<BusyAdorner>();
                    if (adorner != null)
                    {
                        adorner.busyIndicator.BusyContent = value;
                    }
                }
            }
        }

        public static string GetStatus(UIElement element)
        {
            return (string)element.GetValue(StatusProperty);
        }

        public static void SetScope(UIElement element, BusyScope value)
        {
            element.SetValue(ScopeProperty, value);
        }

        public static BusyScope GetScope(UIElement element)
        {
            return (BusyScope)element.GetValue(ScopeProperty);
        }
    }

    /// <summary>
    /// Presentation extension/helper methods.
    /// </summary>
    public static partial class Presentation
    {
        private static object syncRoot = new object();

        /// <summary>
        /// DoEvents replacement for WPF
        /// </summary>
        /// <param name="application"></param>
        public static void DoEvents(this Application application)
        {
            application.Dispatcher.Invoke(DispatcherPriority.Background, new Action(delegate { }));
        }

        /// <summary>
        /// Loads component (multi-threaded safe way).
        /// </summary>
        /// <typeparam name="TComponent">The type of the component.</typeparam>
        /// <param name="application">The application.</param>
        /// <param name="component">The component.</param>
        /// <param name="resourceLocator">The resource locator.</param>
        public static void SafeLoadComponent<TComponent>(this Application application, TComponent component, Uri resourceLocator)
        {
            // See https://connect.microsoft.com/VisualStudio/feedback/details/758055/application-loadcomponent-is-not-thread-safe
            lock (syncRoot)
            {
                Application.LoadComponent(component, resourceLocator);
            }
        }

        /// <summary>
        /// Loads component (multi-threaded safe way).
        /// </summary>
        /// <typeparam name="TComponent">The type of the component.</typeparam>
        /// <param name="application">The application.</param>
        /// <param name="resourceLocator">The resource locator.</param>
        /// <returns></returns>
        public static TComponent SafeLoadComponent<TComponent>(this Application application, Uri resourceLocator)
        {
            // See https://connect.microsoft.com/VisualStudio/feedback/details/758055/application-loadcomponent-is-not-thread-safe
            lock (syncRoot)
            {
                return (TComponent)Application.LoadComponent(resourceLocator);
            }
        }

        /// <summary>
        /// Determines whether component is present.
        /// </summary>
        /// <param name="application">The application.</param>
        /// <param name="resourceLocator">The resource locator.</param>
        /// <returns></returns>
        public static bool IsComponentPresent(this Application application, Uri resourceLocator)
        {
            lock (syncRoot)
            {
                var resouceInfo = Application.GetResourceStream(resourceLocator);
                return resouceInfo != null;
            }
        }

        /// <summary>
        /// Build full uri for the specified relative component path.
        /// </summary>
        /// <param name="sourceAssembly">The source assembly (where xaml is located).</param>
        /// <param name="relativeComponentPath">The relative component path (as appears in solution, like: '/Views/Default/Dic.xaml').</param>
        /// <returns></returns>
        public static Uri BuildComponentUri(this Assembly sourceAssembly, string relativeComponentPath)
        {
            var componentUri = string.Format("/{0};component{1}", sourceAssembly.GetName().Name, relativeComponentPath);
            return new Uri(componentUri, UriKind.RelativeOrAbsolute);
        }

        /// <summary>
        /// Short hand to retrieve current application dispatcher.
        /// </summary>
        /// <param name="viewContext">The view context.</param>
        /// <returns></returns>
        public static Dispatcher Dispatcher(this IViewContext viewContext)
        {
            return Application.Current.Dispatcher;
        }

        /// <summary>
        /// Calls Invoke on the dispatcher with an action.
        /// </summary>
        /// <param name="dispatcher">The dispatcher.</param>
        /// <param name="action">The action.</param>
        /// <param name="priority">The priority.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        public static object Invoke(this Dispatcher dispatcher, Action action, DispatcherPriority priority = DispatcherPriority.Normal, params object[] args)
        {
            return dispatcher.Invoke(action, priority, args);
        }

        /// <summary>
        /// Calls BeginOnvoke on the dispatcher with an action.
        /// </summary>
        /// <param name="dispatcher">The dispatcher.</param>
        /// <param name="action">The action.</param>
        /// <param name="priority">The priority.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        public static DispatcherOperation BeginInvoke(this Dispatcher dispatcher, Action action, DispatcherPriority priority = DispatcherPriority.Normal, params object[] args)
        {
            return dispatcher.BeginInvoke(action, priority, args);
        }

        /// <summary>
        /// Shows a loading indicator.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="content">The content.</param>
        public static void ShowBusyIndicator(this Control control, string content = null)
        {
            control.Enabled = false;
            ElementHost host = control.Controls.OfType<ElementHost>().FirstOrDefault(i => i.Name == "BusyIndicator");
            if (host != null) return;
            var busyIndicator = new RadBusyIndicator();
            if (content != null) busyIndicator.BusyContent = content;
            host = new ElementHost { Child = busyIndicator, Name = "BusyIndicator", Anchor = AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right, Width = 181, Height = 100 };
            using (host.CreateGraphics())
            {
            }
            host.Left = (control.Width - host.Width) / 2;
            host.Top = (control.Height - host.Height) / 2;
            host.BackColor = control.BackColor;
            busyIndicator.IsBusy = true;
            control.Controls.Add(host);
            host.BringToFront();
        }

        /// <summary>
        /// Hides a loading indicator if currently displayed
        /// </summary>
        /// <param name="control">The control.</param>
        public static void HideBusyIndicator(this Control control)
        {
            ElementHost host = control.Controls.OfType<ElementHost>().FirstOrDefault(i => i.Name == "BusyIndicator");
            if (host != null)
            {
                control.Controls.Remove(host);
                host.Dispose();
                control.Enabled = true;
            }
        }

        /// <summary>
        /// Converts the WPF color to a GDI color.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static Color ToGdiColor(this System.Windows.Media.Color source)
        {
            return Color.FromArgb(source.A, source.R, source.G, source.B);
        }

        /// <summary>
        /// Toes the GDI brush.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="rectangle">The rectangle.</param>
        /// <returns></returns>
        public static Brush ToGdiBrush(this System.Windows.Media.Brush source, Rectangle rectangle = default(Rectangle))
        {
            if (source is SolidColorBrush)
            {
                return new SolidBrush(((SolidColorBrush)source).Color.ToGdiColor());
            }
            if (source is LinearGradientBrush)
            {
                var lgb = (LinearGradientBrush)source;
                return new System.Drawing.Drawing2D.LinearGradientBrush(rectangle, lgb.GradientStops.First().Color.ToGdiColor(), lgb.GradientStops.Last().Color.ToGdiColor(), 90);
            }
            return null;
        }

        /// <summary>
        /// Converts the GDI color to a WPF color.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static System.Windows.Media.Color ToWpfColor(this Color source)
        {
            return System.Windows.Media.Color.FromArgb(source.A, source.R, source.G, source.B);
        }

        /// <summary>
        /// Converts the GDI icon to a WPF ImageSource.
        /// </summary>
        /// <param name="icon">The icon.</param>
        /// <returns></returns>
        public static ImageSource ToImageSource(this Icon icon)
        {
            var ms = new MemoryStream();
            icon.Save(ms);
            var ibd = new IconBitmapDecoder(ms, BitmapCreateOptions.None, BitmapCacheOption.Default);
            return ibd.Frames[0];
        }

        /// <summary>
        /// Converts the GDI Bitmap to a WPF BitmapSource.
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns></returns>
        public static BitmapSource ToBitmapSource(this Bitmap bitmap, int? width = null, int? height = null)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(bitmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty,
                                                         BitmapSizeOptions.FromWidthAndHeight(width ?? bitmap.Width, height ?? bitmap.Height));
        }

        /// <summary>
        /// Converts the mouse button event args to the Windows Forms equivalent.
        /// </summary>
        /// <param name="args">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        internal static MouseEventArgs ToMouseEventArgs(this MouseButtonEventArgs args)
        {
            IInputElement outerContainer = args.Source.As<FrameworkElement>().IfNotNull(fe => fe.Parents().OfType<IInputElement>().LastOrDefault());
            Point position = outerContainer == null ? default(Point) : args.GetPosition(outerContainer);
            return new MouseEventArgs(args.ChangedButton.TranslateEnum<MouseButtons>(), args.ClickCount, (int)position.X, (int)position.Y, 0);
        }
    }

    /// <summary>
    /// Hosts the main content of a Window whent the content is a Windows Forms Control.
    /// </summary>
    internal class WindowContentWindowsFormsHost : WindowsFormsHost
    {
        private Window window;

        public WindowContentWindowsFormsHost()
        {
            Loaded += OnLoaded;
            ChildChanged += OnChildChanged;
            VerticalAlignment = VerticalAlignment.Stretch;
            HorizontalAlignment = HorizontalAlignment.Stretch;
        }

        private void OnChildChanged(object sender, ChildChangedEventArgs e)
        {
            if (Child != null && !Child.Is<Panel>())
            {
                Background = new SolidColorBrush(Child.BackColor != Color.Transparent ? Colors.White : Colors.Transparent);

                var panel = new Panel(Child);
                Child = panel;
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (window != null)
            {
                window.Closed -= OnWindowClosed;
            }

            window = this.GetVisualParent<Window>();

            window.Closed += OnWindowClosed;
        }

        private void OnWindowClosed(object sender, EventArgs e)
        {
            Dispose();
        }

        private class Panel : Control
        {
            private readonly Control child;
            private readonly System.Drawing.Size childSize;

            public Panel(Control child)
            {
                this.child = child;
                childSize = child.Size;
                Controls.Add(child);
                child.Dock = DockStyle.Fill;
            }

            protected override System.Drawing.Size DefaultSize
            {
                get { return childSize; }
            }

            public override System.Drawing.Size GetPreferredSize(System.Drawing.Size proposedSize)
            {
                if (child.PreferredSize.Width == 0 || child.PreferredSize.Height == 0)
                {
                    return new System.Drawing.Size(childSize.Width, childSize.Height);
                }
                return child.GetPreferredSize(proposedSize);
            }
        }
    }

    /// <summary>
    /// When placed into an element's Resources collection the 
    /// spy's Element property returns that containing element.
    /// Use the  NameScopeSource attached property to bridge an
    /// element's NameScope to other elements.
    /// </summary>
    public class ElementReference
        : Freezable
    {
        #region Element

        DependencyObject element;

        public DependencyObject Element
        {
            get
            {
                if (element == null)
                {
                    var prop =
                        typeof(Freezable).GetProperty(
                        "InheritanceContext",
                        BindingFlags.Instance | BindingFlags.NonPublic
                        );

                    element = prop.GetValue(this, null) as DependencyObject;

                    if (element != null)
                        Freeze();
                }
                return element;
            }
        }

        #endregion // Element

        #region NameScopeSource

        public static ElementReference GetNameScopeSource(
            DependencyObject obj)
        {
            return (ElementReference)obj.GetValue(NameScopeSourceProperty);
        }

        public static void SetNameScopeSource(
            DependencyObject obj, ElementReference value)
        {
            obj.SetValue(NameScopeSourceProperty, value);
        }

        public static readonly DependencyProperty
            NameScopeSourceProperty =
            DependencyProperty.RegisterAttached(
            "NameScopeSource",
            typeof(ElementReference),
            typeof(ElementReference),
            new UIPropertyMetadata(
                null, OnNameScopeSourceChanged));

        static void OnNameScopeSourceChanged(
            DependencyObject depObj,
            DependencyPropertyChangedEventArgs e)
        {
            var source = e.NewValue as ElementReference;
            if (source == null || source.Element == null)
                return;

            INameScope scope =
                NameScope.GetNameScope(source.Element);
            if (scope == null)
                return;

            depObj.Dispatcher.BeginInvoke(
                DispatcherPriority.Normal,
                (Action)(() => NameScope.SetNameScope(depObj, scope)));
        }

        #endregion // NameScopeSource

        #region CreateInstanceCore

        protected override Freezable CreateInstanceCore()
        {
            // We are required to override this abstract method.
            throw new NotSupportedException();
        }

        #endregion // CreateInstanceCore
    }

    public static class LoadingWindow
    {
        private static Tuple<RadBusyIndicator, System.Windows.Window> GetLoadingWindow()
        {
            var busyIndicator = new RadBusyIndicator
            {
                IsBusy = true,
                VerticalAlignment = VerticalAlignment.Stretch,
                VerticalContentAlignment = VerticalAlignment.Stretch,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                HorizontalContentAlignment = HorizontalAlignment.Stretch
            };

            var loadingWindow = new Window.LoadingWindow
            {
                Content = busyIndicator,
                AllowsTransparency = true,
                Background = new SolidColorBrush(Colors.Transparent),
                WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner,
                WindowStyle = WindowStyle.None,
                ResizeMode = ResizeMode.NoResize,
                ShowInTaskbar = false,
                Width = 181,
                Height = 100,
                Topmost = true
            };

            loadingWindow.ContentRendered += delegate
            {
                // don't force in front of all other apps once loaded
                loadingWindow.Topmost = false;
                loadingWindow.Activate();
            };

            return new Tuple<RadBusyIndicator, System.Windows.Window>(busyIndicator, loadingWindow);
        }

        public static void Run(Action action)
        {
            var busyIndicatorAndWindow = GetLoadingWindow();
            var busyIndicator = busyIndicatorAndWindow.Item1;
            var window = busyIndicatorAndWindow.Item2;

            Command.Create(action).Async(false, null, i =>
            {
                // Normally you have to invoke the following onto the dispatcher thread,
                // but Command.Async uses a BackgroundWorker which automates it for us
                busyIndicator.IsBusy = false;
                window.Close();
            }).Execute();

            window.ShowDialog();
        }

        public static TResult Run<TResult>(Func<TResult> function)
        {
            var busyIndicatorAndWindow = GetLoadingWindow();
            var busyIndicator = busyIndicatorAndWindow.Item1;
            var window = busyIndicatorAndWindow.Item2;
            TResult result = default(TResult);

            Command.Create(() => result = function()).Async(false, null, i =>
            {
                // Normally you have to invoke the following onto the dispatcher thread,
                // but Command.Async uses a BackgroundWorker which automates it for us
                busyIndicator.IsBusy = false;
                window.Close();
            }).Execute();

            window.ShowDialog();

            return result;
        }
    }

    public class DataContextReference : Freezable
    {
        /// <summary>
        /// Dependency property to store current or user specified context reference
        /// </summary>
        /// <remarks>
        /// FrameworkElement.Resources are outside Visual/Logical tree, so DataContext is not inheritable there and is always "Default"/null
        /// 
        /// </remarks>
        public static readonly DependencyProperty DataContextProperty =
            DependencyProperty.Register("DataContext", typeof(object),
                typeof(DataContextReference),
                new PropertyMetadata(null, null, OnCoerceDataContext));

        public DataContextReference()
        {
            // Use Binding to copy DataContext inherited/set on FrameworkElement. User can override this by setting Binding explicitly
            BindingOperations.SetBinding(this, DataContextProperty, new Binding());

            IsSynchronizedWithCurrentItem = true;
        }

        /// <summary>
        ///   Gets/sets whether the spy will return the CurrentItem of the 
        ///   ICollectionView that wraps the data context, assuming it is
        ///   a collection of some sort. If the data context is not a 
        ///   collection, this property has no effect. 
        ///   The default value is true.
        /// </summary>
        public bool IsSynchronizedWithCurrentItem { get; set; }

        public object DataContext
        {
            get { return GetValue(DataContextProperty); }
            set { SetValue(DataContextProperty, value); }
        }

        // Borrow the DataContext dependency property from FrameworkElement.

        private static object OnCoerceDataContext(DependencyObject depObj, object value)
        {
            var reference = depObj as DataContextReference;
            if (reference == null)
                return value;

            if (reference.IsSynchronizedWithCurrentItem)
            {
                ICollectionView view = CollectionViewSource.GetDefaultView(value);
                if (view != null)
                    return view.CurrentItem;
            }

            return value;
        }

        protected override Freezable CreateInstanceCore()
        {
            // We are required to override this abstract method.
            throw new NotImplementedException();
        }
    }


    /// <summary>
    /// Markup extension to provide display name.
    /// </summary>
    public class DisplayNameExtension : MarkupExtension
    {
        public DisplayNameExtension()
        {
        }

        public DisplayNameExtension(string propertyName)
        {
            PropertyName = propertyName;
        }

        public Type Type { get; set; }

        public string PropertyName { get; set; }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            // (This code has zero tolerance)
            PropertyInfo property = Type.GetProperty(PropertyName);
            if (property == null) return string.Empty;
            string displayName = property.GetDisplayName();
            return displayName;
        }
    }

    /// <summary>
    /// Markup extension to provide format string.
    /// </summary>
    public class DataFormatStringExtension : MarkupExtension
    {
        public DataFormatStringExtension()
        {
        }

        public DataFormatStringExtension(string propertyName)
        {
            PropertyName = propertyName;
        }

        public Type Type { get; set; }

        public string PropertyName { get; set; }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            PropertyInfo property = Type.GetProperty(PropertyName);
            if (property == null) return string.Empty;
            return property.GetAttribute<DisplayFormatAttribute>().IfNotNull(a => a.DataFormatString);
        }
    }

    public class ViewModelTypeDescriptionProvider : TypeDescriptionProvider
    {
        private static readonly TypeDescriptionProvider DefaultObjectProvider;

        static ViewModelTypeDescriptionProvider()
        {
            DefaultObjectProvider = TypeDescriptor.GetProvider(typeof(object));
            TypeDescriptor.AddProviderTransparent(new ViewModelTypeDescriptionProvider(), typeof(object));
        }

        public ViewModelTypeDescriptionProvider() { }

        public ViewModelTypeDescriptionProvider(Type type)
            : base(TypeDescriptor.GetProvider(type))
        {
        }

        public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
        {
            if (!instance.Is<IViewModel>() && !objectType.Is<IViewModel>()
                && !instance.Is<EventArgs>() && !objectType.Is<EventArgs>()
                ) return DefaultObjectProvider.GetTypeDescriptor(objectType, instance);

            return new ViewModelTypeDescriptor(DefaultObjectProvider.GetTypeDescriptor(objectType, null));
        }

        private class ViewModelTypeDescriptor : CustomTypeDescriptor
        {
            private readonly PropertyDescriptorCollection properties;

            public ViewModelTypeDescriptor(ICustomTypeDescriptor parent)
                : base(parent)
            {
                properties = new PropertyDescriptorCollection(base.GetProperties().OfType<PropertyDescriptor>().Select(pd => new ViewModelPropertyDescriptor(pd)).ToArray());
            }

            public override PropertyDescriptorCollection GetProperties()
            {
                return properties;
            }

            public override PropertyDescriptorCollection GetProperties(Attribute[] attributes)
            {
                return new PropertyDescriptorCollection(GetProperties().OfType<PropertyDescriptor>().Where(pd => pd.Attributes.OfType<Attribute>().Any(attributes.Contains)).ToArray());
            }
        }

        private class ViewModelPropertyDescriptor : BasicPropertyDescriptor
        {
            public ViewModelPropertyDescriptor(PropertyDescriptor descriptor, IEnumerable<Attribute> attributes = null, Func<object, object> componentSelector = null)
                : base(descriptor, attributes, componentSelector)
            {
            }

        }
    }


    /// <summary>
    /// Interface that defines methods for displaying navigable content
    /// </summary>
    public interface INavigableInteractionManager
    {
        /// <summary>
        /// Shows the specified arguments.
        /// If no navigation window is present, a new navigation window is created.  
        /// Otherwise the current navigation window is reused.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        void Show(NavigableInteractionArguments arguments);

        /// <summary>
        /// Shows the previous NavigableInteractionArgument for the current navigation window.
        /// </summary>
        void ShowPrevious();

        /// <summary>
        /// Occurs when showing.
        /// </summary>
        event EventHandler<InteractionEventArgs<InteractionArguments>> Showing;
    }

    /// <summary>
    /// An implementation of INavigableInteractionManager that enables views to be navigable.
    /// </summary>
    public class NavigableInteractionManager : INavigableInteractionManager
    {
        #region Navigation Window Helper

        /// <summary>
        /// Class to help track and cleanup navigation windows
        /// </summary>
        private class NavigationWindowHelper : IDisposable
        {
            private readonly IInteractionContext interactionContext;
            private IWindow window;

            public NavigationWindowHelper(IInteractionContext interactionContext, NavigationViewContext navigationViewContext, IWindow window, object owner)
            {
                this.interactionContext = interactionContext;
                PreviousArguments = new Stack<NavigableInteractionArguments>();
                interactionContext.CastTo<InteractionContext>().AfterCompleted += OnAfterCompleted;

                NavigationViewContext = navigationViewContext;
                Window = window;
                WindowOwner = owner;
            }

            public void Dispose()
            {
                Dispose(true);
            }

            // ReSharper disable UnusedParameter.Local
            // Following disposable pattern
            private void Dispose(bool isDisposing)
            // ReSharper restore UnusedParameter.Local
            {
                if (Window != null)
                {
                    Window.Close();
                }
                interactionContext.CastTo<InteractionContext>().AfterCompleted -= OnAfterCompleted;
            }

            public void Show(NavigableInteractionArguments arguments)
            {
                Show(arguments, true);
            }

            public void ShowPrevious()
            {
                var args = PreviousArguments.Pop();

                var originalDirection = args.Direction;
                args.Direction = args.Direction == Direction.Backward ? Direction.Forward : Direction.Backward;

                var reconstructedContent = Activator.CreateInstance(args.Content.GetType());
                var view = args.Content as View;
                if (view != null) { ((View)reconstructedContent).DataContext = view.DataContext; }
                args.Content = reconstructedContent;

                Show(args, false);

                // The args will be saved as the currentNavigableArguments, so we need to set back the originalDirection
                args.Direction = originalDirection;
            }

            private object GetHeader(object content)
            {
                if (content != null)
                {
                    var property = content.GetType().GetProperty("Header", BindingFlags.Instance | BindingFlags.Public);
                    if (property != null && property.CanRead && property.GetIndexParameters().Length == 0)
                    {
                        return property.GetValue(content, null);
                    }
                }

                return " ";
            }

            private void Show(NavigableInteractionArguments arguments, bool trackArguments)
            {
                var dpWindow = window as DependencyObject;
                if (dpWindow != null)
                {
                    BindingOperations.ClearBinding(dpWindow, System.Windows.Window.TitleProperty);
                    BindingOperations.SetBinding(dpWindow, System.Windows.Window.TitleProperty, new Binding("Header") { Source = arguments.Content });
                }
                else
                {
                    window.Header = arguments.Header ?? GetHeader(arguments.Content);
                }

                if (NavigationViewContext.CurrentContent == null)
                {
                    NavigationViewContext.CurrentContent = arguments.Content;
                }
                else if (arguments.Direction == Direction.Forward)
                {
                    NavigationViewContext.NextAnimation.Execute(arguments.Content);
                }
                else
                {
                    NavigationViewContext.PreviousAnimation.Execute(arguments.Content);
                }

                if (trackArguments)
                {
                    if (CurrentArgument != null)
                    {
                        PreviousArguments.Push(CurrentArgument);
                    }
                }
                CurrentArgument = arguments;

                if (!window.HasLoaded) { window.ShowModal(); }
            }

            private void OnAfterCompleted(object sender, EventArgs<bool?> args)
            {
                if (window != null)
                {
                    var windowElement = window as FrameworkElement;
                    if (windowElement == null || windowElement.Dispatcher.CheckAccess())
                    {
                        window.Close();
                    }
                    else
                    {
                        windowElement.Dispatcher.BeginInvoke(window.Close);
                    }
                    Window = null;
                    NavigationViewContext = null;
                    CurrentArgument = null;
                    PreviousArguments.Clear();
                }
            }

            private void OnWindowClosing(object sender, CancelEventArgs e)
            {
                if (e.Cancel) return;

                var window = (IWindow)sender;

                window.Closing -= new EventHandler<CancelEventArgs>(OnWindowClosing).MakeWeak();

                IInteractionContext context = window.InteractionContext;
                Window = null;

                if (context != null && !context.IsComplete)
                {
                    bool cancel = !context.Complete(false);
                    if (cancel)
                    {
                        e.Cancel = true;
                        window.Closing += new EventHandler<CancelEventArgs>(OnWindowClosing).MakeWeak();
                        Window = window;
                        return;
                    }
                }

                PreviousArguments.Clear();
                NavigationViewContext = null;
                CurrentArgument = null;
                Window = null;
            }

            /// <summary>
            /// Gets or sets the owner.
            /// Can be used to identify the NavigationWindowHelper instance.
            /// (Each instance should have a unique owner.)
            /// </summary>
            // ReSharper disable once MemberCanBePrivate.Local
            // ReSharper disable once UnusedAutoPropertyAccessor.Local
            public object WindowOwner { get; set; }

            /// <summary>
            /// Gets or sets the window.
            /// </summary>
            private IWindow Window
            {
                get { return window; }
                set
                {
                    if (window != null)
                    {
                        window.Closing -= new EventHandler<CancelEventArgs>(OnWindowClosing).MakeWeak();
                    }
                    window = value;
                    if (window != null)
                    {
                        window.Closing += new EventHandler<CancelEventArgs>(OnWindowClosing).MakeWeak();
                    }
                }
            }

            /// <summary>
            /// Gets or sets the navigation view context
            /// </summary>
            private NavigationViewContext NavigationViewContext { get; set; }

            /// <summary>
            /// Gets or sets the current NavigableInteractionArguments
            /// </summary>
            private NavigableInteractionArguments CurrentArgument { get; set; }

            /// <summary>
            /// Gets or sets the previous NavigableInteractionArguments.
            /// </summary>
            private Stack<NavigableInteractionArguments> PreviousArguments { get; set; }
        }
        #endregion

        private readonly Func<IWindow> createWindow;
        private readonly Func<IInteractionContext> createInteractionContext;
        private static NavigationWindowHelper navigationWindowHelper;

        public NavigableInteractionManager(Func<IWindow> createWindow, Func<IInteractionContext> createInteractionContext)
        {
            this.createWindow = createWindow;
            this.createInteractionContext = createInteractionContext;
            StyleManager.Initialize();
            Application.Current.Dispatcher.BeginInvoke((Action)(() => Application.Current.Exit += OnApplicationExit));
        }

        private void OnApplicationExit(object sender, ExitEventArgs e)
        {
            if (navigationWindowHelper != null)
            {
                navigationWindowHelper.Dispose();
                navigationWindowHelper = null;
            }
        }

        private IWindow BuildWindow(NavigableInteractionArguments arguments)
        {
            #region helper func getHeader

            Func<object, object> getHeader = content =>
            {
                if (content != null)
                {
                    var property = content.GetType().GetProperty("Header", BindingFlags.Instance | BindingFlags.Public);
                    if (property != null && property.CanRead && property.GetIndexParameters().Length == 0)
                    {
                        return property.GetValue(content, null);
                    }
                }

                return " ";
            };

            #endregion

            var window = createWindow();

            if (arguments.InteractionContext != null) window.InteractionContext = arguments.InteractionContext;

            if (arguments.InteractionContext == null) arguments.InteractionContext = window.InteractionContext = (window.InteractionContext ?? createInteractionContext());

            window.Header = arguments.Header ?? getHeader(arguments.Content);
            window.Owner = arguments.Owner;
            window.ResizeMode = arguments.ResizeMode;
            window.CanClose = true;
            window.DialogButtons = null;
            window.WindowState = arguments.WindowState;
            window.WindowStartupLocation = arguments.StartupLocation;
            window.WindowFrameStyle = arguments.WindowFrameStyle;
            window.Closed += delegate
                {
                    if (navigationWindowHelper != null)
                    {
                        navigationWindowHelper.Dispose();
                        navigationWindowHelper = null;
                    }
                };
            return window;
        }

        public void Show(NavigableInteractionArguments arguments)
        {
            if (arguments.Owner == null) arguments.Owner = Owner;

            var eventArgs = new InteractionEventArgs<InteractionArguments>(arguments);
            Showing.Fire(this, eventArgs);
            if (eventArgs.Handled) return;

            if (navigationWindowHelper == null)
            {
                var window = BuildWindow(arguments);
                var navigationView = new NavigationView();
                window.Content = navigationView;
                var viewContext = navigationView.DataContext.EnsureType<NavigationViewContext>();
                navigationWindowHelper = new NavigationWindowHelper(arguments.InteractionContext, viewContext, window, arguments.Owner);
            }

            navigationWindowHelper.Show(arguments);
        }

        public void ShowPrevious()
        {
            if (navigationWindowHelper != null)
            {
                navigationWindowHelper.ShowPrevious();
            }
        }

        public event EventHandler<InteractionEventArgs<InteractionArguments>> Showing;

        [DebuggerNonUserCode]
        private static ContentControl Owner
        {
            get
            {
                #region helper func isValidOwnerWindow

                Func<System.Windows.Window, bool> isValidOwnerWindow = window => window != null && window.CheckAccess() && window.IsActive && window.IsVisible && window.IsLoaded;

                #endregion

                try
                {
                    if (!Application.Current.Dispatcher.CheckAccess()) return null;

                    var window = Application.Current.Windows.OfType<System.Windows.Window>().LastOrDefault(isValidOwnerWindow);

                    if (window == null && isValidOwnerWindow(Application.Current.MainWindow)) window = Application.Current.MainWindow;

                    return window;
                }
                catch
                {
                    return null;
                }
            }
        }
    }

    /// <summary>
    /// Interaction arguments for the INavigableInteractionManager that provides a "navigation direction".
    /// </summary>
    public class NavigableInteractionArguments : InteractionArguments
    {
        public NavigableInteractionArguments()
        {
            WindowState = WindowState.Normal;
            StartupLocation = WindowStartupLocation.CenterScreen;
            WindowFrameStyle = WindowFrameStyle.Child;
            ResizeMode = ResizeMode.NoResize;
        }

        /// <summary>
        /// Gets or sets the direction of the navigation
        /// </summary>
        public Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets the ResizeMode
        /// </summary>
        public ResizeMode ResizeMode { get; set; }

        /// <summary>
        /// Gets or sets the initial WindowState
        /// </summary>
        public WindowState WindowState { get; set; }

        /// <summary>
        /// Gets or sets the initial window startup location
        /// </summary>
        public WindowStartupLocation StartupLocation { get; set; }

        /// <summary>
        /// Gets or sets the window frame style
        /// </summary>
        public WindowFrameStyle WindowFrameStyle { get; set; }
    }

    /// <summary>
    /// An enumeration of values that represent the direction a navigation may take.
    /// </summary>
    public enum Direction
    {
        Forward,
        Backward
    }
}