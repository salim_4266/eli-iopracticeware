﻿using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Transactions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Data;
using ADODB;

namespace Soaf.Testing
{

    [TestClass]
    public class ServiceTests
    {
        private const string ConnectionString = @"Data Source=localhost;Initial Catalog=Test1;Integrated Security=True";
        private TransactionScope transactionScope;

        [TestInitialize]
        public void OnTestInitialize()
        {
            Monitor.Enter(Common.SyncRoot);

            PrepAuthentication.CreateUser();
            PrepAuthentication.LogIn("ServiceTests", "ServiceTests");
            Creator();
            transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10));
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            transactionScope.Dispose();
            Destroyer();
            PrepAuthentication.LogOut();
            PrepAuthentication.DeleteUser();

            Monitor.Exit(Common.SyncRoot);
        }

        #region Test IAdoService

        [TestMethod]
        public void TestAdoServiceExecuteCommandDbDataReaderText()
        {
            var adoService = Common.ServiceProvider.GetService<IAdoService>();

            var argument = CreateCommandArgument("SELECT * FROM ServiceTest WHERE SomeId = 2");
            var serverResult = adoService.ExecuteCommandDbDataReader(argument);

            var table = serverResult.Result;
            Assert.IsTrue(table.Rows.Count == 1);
            Assert.IsTrue(table.Rows[0]["Name"].Equals("Test"));
        }

        [TestMethod]
        public void TestAdoServiceExecuteCommandDbDataReaderStoredProcedure()
        {
            var adoService = Common.ServiceProvider.GetService<IAdoService>();

            var argument1 = CreateCommandArgument("SELECT * FROM ServiceTest WHERE Name = 'Test'");
            var serverResult1 = adoService.ExecuteCommandDbDataReader(argument1);

            var table1 = serverResult1.Result;
            var value1 = (int)table1.Rows[0]["SomeId"];
            Assert.IsTrue(value1 == 2);

            var argument2 = CreateCommandArgument("sp_ServiceTestChangeId", CommandType.StoredProcedure);
            var parameterOne = new AdoServiceParameterExecutionArgument { ParameterName = "@SomeId", Value = 2, Direction = ParameterDirection.Input.ToString() };
            var parameterTwo = new AdoServiceParameterExecutionArgument { ParameterName = "@SomeNewId", Value = 7, Direction = ParameterDirection.Input.ToString() };
            argument2.Parameters.Add(parameterOne);
            argument2.Parameters.Add(parameterTwo);

            adoService.ExecuteCommandDbDataReader(argument2);

            var serverResult2 = adoService.ExecuteCommandDbDataReader(argument1);

            var table2 = serverResult2.Result;
            var value2 = (int)table2.Rows[0]["SomeId"];
            Assert.IsTrue(value2 == 7);
        }

        [TestMethod]
        public void TestAdoServiceExecuteCommandNonQueryText()
        {
            var adoService = Common.ServiceProvider.GetService<IAdoService>();
            
            var argument = CreateCommandArgument("DELETE FROM ServiceTest");
            var serverResult = adoService.ExecuteCommandNonQuery(argument);
            Assert.IsTrue(serverResult.Result == 2);
        }

        [TestMethod]
        public void TestAdoServiceExecuteCommandNonQueryStoredProcedure()
        {
            var adoService = Common.ServiceProvider.GetService<IAdoService>();

            var argument = CreateCommandArgument("sp_ServiceTestChangeId", CommandType.StoredProcedure);

            var parameterOne = new AdoServiceParameterExecutionArgument { ParameterName = "@SomeId", Value = 2, Direction = ParameterDirection.Input.ToString() };
            var parameterTwo = new AdoServiceParameterExecutionArgument { ParameterName = "@SomeNewId", Value = 7, Direction = ParameterDirection.Input.ToString() };
            argument.Parameters.Add(parameterOne);
            argument.Parameters.Add(parameterTwo);

            var serverResult = adoService.ExecuteCommandNonQuery(argument);
            Assert.IsTrue(serverResult.Result == 1);
        }

        [TestMethod]
        public void TestAdoServiceExecuteCommandScalarText()
        {
            var adoService = Common.ServiceProvider.GetService<IAdoService>();

            var argument = CreateCommandArgument("SELECT COUNT(*) FROM ServiceTest");
            var serverResult = adoService.ExecuteCommandScalar(argument);

            var count = serverResult.Result;
            Assert.IsTrue((int)count == 2);
        }

        [TestMethod]
        public void TestAdoServiceExecuteCommandScalarStoredProcedure()
        {
            var adoService = Common.ServiceProvider.GetService<IAdoService>();

            var argument = CreateCommandArgument("sp_ServiceTestCountItems", CommandType.StoredProcedure);
            var serverResult = adoService.ExecuteCommandScalar(argument);
            var count = serverResult.Result;
            Assert.IsTrue((int)count == 2);
        }

        #endregion

        #region Test IAdoComService

        [TestMethod]
        public void TestAdoComServiceAddNew()
        {
            var adoComService = Common.ServiceProvider.GetService<IAdoComService>();

            Recordset rs1 = adoComService.OpenRecordset("SELECT * FROM ServiceTest", ConnectionString);
            Assert.IsTrue(rs1.RecordCount == 2);

            rs1.AddNew();
            rs1.Fields["SomeId"].Value = "7";
            rs1.Fields["Name"].Value = "New";

            var rs2 = adoComService.UpdateRecordset(rs1, ConnectionString);

            Assert.IsTrue(rs2.RecordCount == 3);

            var rs3 = adoComService.OpenRecordset("SELECT * FROM ServiceTest", ConnectionString);
            Assert.IsTrue(rs3.RecordCount == 3);

        }

        [TestMethod]
        public void TestAdoComServiceUpdateRecordsetDeleting()
        {
            var adoComService = Common.ServiceProvider.GetService<IAdoComService>();

            var rs1 = adoComService.OpenRecordset("SELECT * FROM ServiceTest", ConnectionString);
            Assert.IsTrue(rs1.RecordCount == 2);

            rs1.MoveFirst();
            rs1.Delete();

            var rs2 = adoComService.UpdateRecordset(rs1, ConnectionString);
            Assert.IsTrue(rs2.RecordCount == 1);

            var rs3 = adoComService.OpenRecordset("SELECT * FROM ServiceTest", ConnectionString);
            Assert.IsTrue(rs3.RecordCount == 1);
        }

        [TestMethod]
        public void TestAdoComServiceUpdateRecordsetUpdating()
        {
            var adoComService = Common.ServiceProvider.GetService<IAdoComService>();

            var rs1 = adoComService.OpenRecordset("SELECT * FROM ServiceTest", ConnectionString);
            Assert.IsTrue(rs1.RecordCount == 2);

            rs1.MoveFirst();
            rs1.Fields["Name"].Value = "New";

            var rs2 = adoComService.UpdateRecordset(rs1, ConnectionString);
            Assert.IsTrue(rs2.RecordCount == 2);

            rs2.MoveFirst();
            Assert.IsTrue(rs2.Fields["Name"].Value == "New");

            var rs3 = adoComService.OpenRecordset("SELECT * FROM ServiceTest", ConnectionString);
            Assert.IsTrue(rs3.RecordCount == 2);

            rs3.MoveFirst();
            Assert.IsTrue(rs3.Fields["Name"].Value == "New");
        }

        [TestMethod]
        public void TestAdoComServiceOpenRecordset()
        {
            var adoComService = Common.ServiceProvider.GetService<IAdoComService>();
            var rs = adoComService.OpenRecordset("SELECT * FROM ServiceTest", ConnectionString);
            Assert.IsTrue(rs.RecordCount == 2);
        }

        [TestMethod]
        public void TestAdoComServiceExecuteSql()
        {
            var adoComService = Common.ServiceProvider.GetService<IAdoComService>();

            var rs1 = adoComService.OpenRecordset("SELECT * FROM ServiceTest WHERE Name = 'Service'", ConnectionString);
            Assert.IsTrue(rs1.RecordCount == 1);
            rs1.MoveFirst();
            Assert.IsTrue(rs1.Fields[0].Value == 1);

            adoComService.OpenRecordset("UPDATE ServiceTest SET SomeId = 7 WHERE SomeId = 1", ConnectionString);

            var rs2 = adoComService.OpenRecordset("SELECT * FROM ServiceTest WHERE Name = 'Service'", ConnectionString);
            Assert.IsTrue(rs2.RecordCount == 1);
            rs2.MoveFirst();
            Assert.IsTrue(rs2.Fields[0].Value == 7);
        }

        #endregion

        [TestMethod]
        public void TestTestContactService()
        {
            var service = Common.ServiceProvider.GetService<ITestContactService>();
            var contacts = service.GetContacts();
            Assert.IsNotNull(contacts);
            Assert.IsTrue(contacts.Count() == 1000);
            int i = 0;
            Assert.IsTrue(contacts.All(c => c.GetType().Assembly.IsDynamic && c.StateViewModel != null && c.StateViewModel.GetType().Assembly.IsDynamic &&
                                            c.Name == "Contact" + i++ && c.LastName == "Smith"));
        }

        #region Prep Test Database

        private void Creator()
        {
            var adoService = Common.ServiceProvider.GetService<IAdoService>();

            // Create table, add two records and store proc
            var argument = CreateCommandArgument(SqlArgumentTableCreate);
            adoService.ExecuteCommandNonQuery(argument);

            argument.CommandText = SqlArgumentInsertOne;
            adoService.ExecuteCommandNonQuery(argument);

            argument.CommandText = SqlArgumentInsertTwo;
            adoService.ExecuteCommandNonQuery(argument);

            argument.CommandText = SqlArgumentStoreProcOneCreate;
            adoService.ExecuteCommandNonQuery(argument);

            argument.CommandText = SqlArgumentStoreProcTwoCreate;
            adoService.ExecuteCommandNonQuery(argument);
        }

        private void Destroyer()
        {
            var adoService = Common.ServiceProvider.GetService<IAdoService>();

            // Create table, add two records and store proc
            var argument = CreateCommandArgument(SqlArgumentTableCreate);

            argument.CommandText = SqlArgumentStoreProcTwoDrop;
            adoService.ExecuteCommandNonQuery(argument);

            argument.CommandText = SqlArgumentStoreProcOneDrop;
            adoService.ExecuteCommandNonQuery(argument);

            argument.CommandText = SqlArgumentTableDrop;
            adoService.ExecuteCommandNonQuery(argument);
        }

        private AdoServiceCommandExecutionArgument CreateCommandArgument(string commandText = null, CommandType commandType = CommandType.Text, UpdateRowSource updateRowSource = UpdateRowSource.None)
        {
            var argument = new AdoServiceCommandExecutionArgument
            {
                ConnectionString = ConnectionString,
                ConnectionTimeout = 180,
                Database = "Test1",
                CommandText = commandText,
                CommandTimeout = 180,
                CommandType = commandType.ToString(),
                UpdatedRowSource = updateRowSource.ToString()
            };
            return argument;
        }

        private const string SqlArgumentTableCreate = "CREATE TABLE ServiceTest(SomeId INT NOT NULL, Name VARCHAR(50) NOT NULL)";
        private const string SqlArgumentTableDrop = "DROP TABLE ServiceTest";
        private const string SqlArgumentInsertOne = "INSERT INTO ServiceTest VALUES(1, 'Service')";
        private const string SqlArgumentInsertTwo = "INSERT INTO ServiceTest VALUES(2, 'Test')";
        private const string SqlArgumentStoreProcOneCreate = @"
                                                                CREATE PROCEDURE sp_ServiceTestChangeId
	                                                                @SomeId INT,
	                                                                @SomeNewId INT
                                                                AS
                                                                BEGIN
                                                                    UPDATE ServiceTest SET SomeId = @SomeNewId WHERE SomeId = @SomeId
                                                                END
                                                              ";
        private const string SqlArgumentStoreProcOneDrop = @"DROP PROCEDURE sp_ServiceTestChangeId";
        private const string SqlArgumentStoreProcTwoCreate = @"
                                                                    CREATE PROCEDURE sp_ServiceTestCountItems
                                                                AS
                                                                BEGIN
                                                                    SELECT COUNT(SomeId) FROM ServiceTest
                                                                END
                                                             ";
        private const string SqlArgumentStoreProcTwoDrop = @"DROP PROCEDURE sp_ServiceTestCountItems";

        #endregion
    }
}