﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Transactions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Data;

namespace Soaf.Testing
{
    [TestClass]
    public class DataExtensionsTests
    {
        public const string ConnectionString = @"Data Source=localhost;Initial Catalog=Test1;Integrated Security=True";
        private TransactionScope transactionScope;

        [TestInitialize]
        public void OnTestInitialize()
        {
            Monitor.Enter(Common.SyncRoot);
            PrepAuthentication.CreateUser();
            PrepAuthentication.LogIn("ServiceTests", "ServiceTests");
            Creator();
            transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10));
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            transactionScope.Dispose();
            Destroyer();
            PrepAuthentication.LogOut();
            PrepAuthentication.DeleteUser();
            Monitor.Exit(Common.SyncRoot);
        }

        [TestMethod]
        public void TestAdoServiceCommandExecuteDbDataReaderText()
        {
            var adoServiceCommand = CreateCommand("SELECT * FROM ServiceTest WHERE SomeId = 2");
            var result = adoServiceCommand.ExecuteReader();
            var table = new DataTable("Test");
            table.Load(result);
            Assert.IsTrue(table.Rows.Count == 1);
            Assert.IsTrue(table.Rows[0]["Name"].Equals("Test"));
        }

        [TestMethod]
        public void TestAdoServiceCommandExecuteDbDataReaderStoredProcedure()
        {
            var adoServiceConnection = CreateConnection();

            var adoServiceCommand1 = CreateCommand("SELECT * FROM ServiceTest WHERE Name = 'Test'", CommandType.Text, adoServiceConnection);
            var result1 = adoServiceCommand1.ExecuteReader();
            var table1 = new DataTable("Test");
            table1.Load(result1);
            var value1 = (int)table1.Rows[0]["SomeId"];
            Assert.IsTrue(value1 == 2);

            var adoServiceCommand2 = CreateCommand("sp_ServiceTestChangeId", CommandType.StoredProcedure, adoServiceConnection);
            var parameterOne = new AdoServiceParameter { DbType = DbType.Int32, Direction = ParameterDirection.Input, ParameterName = "@SomeId", Value = 2 };
            var parameterTwo = new AdoServiceParameter { DbType = DbType.Int32, Direction = ParameterDirection.Input, ParameterName = "@SomeNewId", Value = 7 };
            adoServiceCommand2.Parameters.Add(parameterOne);
            adoServiceCommand2.Parameters.Add(parameterTwo);
            adoServiceCommand2.ExecuteReader();

            var result2 = adoServiceCommand1.ExecuteReader();
            var table2 = new DataTable("Test");
            table2.Load(result2);
            var value2 = (int)table2.Rows[0]["SomeId"];
            Assert.IsTrue(value2 == 7);
        }

        [TestMethod]
        public void TestAdoServiceCommandExecuteNonQueryText()
        {
            var adoServiceCommand = CreateCommand("DELETE FROM ServiceTest");
            var result = adoServiceCommand.ExecuteNonQuery();
            Assert.IsTrue(result == 2);
        }

        [TestMethod]
        public void TestAdoServiceCommandExecuteNonQueryStoredProcedure()
        {
            var adoServiceCommand = CreateCommand("sp_ServiceTestChangeId", CommandType.StoredProcedure);
            var parameterOne = new AdoServiceParameter {DbType = DbType.Int32, Direction = ParameterDirection.Input, ParameterName = "@SomeId", Value = 2};
            var parameterTwo = new AdoServiceParameter {DbType = DbType.Int32, Direction = ParameterDirection.Input, ParameterName = "@SomeNewId", Value = 7};
            adoServiceCommand.Parameters.Add(parameterOne);
            adoServiceCommand.Parameters.Add(parameterTwo);

            var result = adoServiceCommand.ExecuteNonQuery();
            Assert.IsTrue(result == 1);
        }

        [TestMethod]
        public void TestAdoServiceCommandExecuteScalarText()
        {
            var adoServiceCommand = CreateCommand("SELECT COUNT(*) FROM ServiceTest");
            var result = adoServiceCommand.ExecuteScalar();
            Assert.IsTrue((int)result == 2);
        }

        [TestMethod]
        public void TestAdoServiceCommandExecuteScalarStoredProcedure()
        {
            var adoServiceCommand = CreateCommand("sp_ServiceTestCountItems", CommandType.StoredProcedure);
            var result = adoServiceCommand.ExecuteScalar();
            Assert.IsTrue((int)result == 2);
        }

        #region Prep Test Database

        private AdoServiceCommand CreateCommand(string commandText = null, CommandType commandType = CommandType.Text, AdoServiceConnection adoServiceConnection = null, UpdateRowSource updateRowSource = UpdateRowSource.None)
        {
            // Init ado service connection if not set
            if (adoServiceConnection == null)
            {
                adoServiceConnection = CreateConnection();
            }

            var command = new AdoServiceCommand
            {
                Connection = adoServiceConnection,
                CommandTimeout = 180,
                CommandText = commandText,
                CommandType = commandType,
                UpdatedRowSource = updateRowSource
            };
            return command;
        }

        private AdoServiceConnection CreateConnection()
        {
            var builder = new SqlConnectionStringBuilder(ConnectionString) { ConnectTimeout = 180 };

            var adoServiceConnection = new AdoServiceConnection();
            adoServiceConnection.ConnectionString = builder.ConnectionString;
            return adoServiceConnection;
        }

        private void Creator()
        {
            // Create table, add two records and store proc
            var command = CreateCommand();

            command.CommandText = SqlArgumentTableCreate;
            command.ExecuteNonQuery();

            command.CommandText = SqlArgumentInsertOne;
            command.ExecuteNonQuery();

            command.CommandText = SqlArgumentInsertTwo;
            command.ExecuteNonQuery();

            command.CommandText = SqlArgumentStoreProcOneCreate;
            command.ExecuteNonQuery();

            command.CommandText = SqlArgumentStoreProcTwoCreate;
            command.ExecuteNonQuery();
        }

        private void Destroyer()
        {
            var command = CreateCommand();
            command.CommandText = SqlArgumentStoreProcTwoDrop;
            command.ExecuteNonQuery();

            command.CommandText = SqlArgumentStoreProcOneDrop;
            command.ExecuteNonQuery();

            command.CommandText = SqlArgumentTableDrop;
            command.ExecuteNonQuery();
        }

        private const string SqlArgumentTableCreate = "CREATE TABLE ServiceTest(SomeId INT NOT NULL, Name VARCHAR(50) NOT NULL)";
        private const string SqlArgumentTableDrop = "DROP TABLE ServiceTest";
        private const string SqlArgumentInsertOne = "INSERT INTO ServiceTest VALUES(1, 'Service')";
        private const string SqlArgumentInsertTwo = "INSERT INTO ServiceTest VALUES(2, 'Test')";
        private const string SqlArgumentStoreProcOneCreate = @"
                                                                CREATE PROCEDURE sp_ServiceTestChangeId
	                                                                @SomeId INT,
	                                                                @SomeNewId INT
                                                                AS
                                                                BEGIN
                                                                    UPDATE ServiceTest SET SomeId = @SomeNewId WHERE SomeId = @SomeId
                                                                END
                                                              ";
        private const string SqlArgumentStoreProcOneDrop = @"DROP PROCEDURE sp_ServiceTestChangeId";
        private const string SqlArgumentStoreProcTwoCreate = @"
                                                                    CREATE PROCEDURE sp_ServiceTestCountItems
                                                                AS
                                                                BEGIN
                                                                    SELECT COUNT(SomeId) FROM ServiceTest
                                                                END
                                                             ";
        private const string SqlArgumentStoreProcTwoDrop = @"DROP PROCEDURE sp_ServiceTestCountItems";

        #endregion
    }
}
