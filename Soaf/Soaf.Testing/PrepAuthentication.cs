﻿using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Data;
using Soaf.EntityFramework;

namespace Soaf.Testing
{
    public static class PrepAuthentication
    {
        /// <summary>
        /// An authenticated user cookie raw value. Allows performing web requests bypassing the RemoteServiceBehavior.
        /// Call LogIn method with <para>refreshAuthenticatedUserCookie</para> set to <code>true</code>
        /// </summary>
        public static string AuthenticatedUserCookie = string.Empty;

        private const string SqlTestUserCreate = @"INSERT INTO [UserRepository].[dbo].[Users]
                                               ([Id]
                                               ,[UserName]
                                               ,[Email]
                                               ,[Password]
                                               ,[PasswordQuestion]
                                               ,[PasswordAnswer]
                                               ,[IsApproved]
                                               ,[LastActivityDate]
                                               ,[LastLoginDate]
                                               ,[LastPasswordChangedDate]
                                               ,[CreationDate]
                                               ,[IsOnline]
                                               ,[IsLockedOut]
                                               ,[LastLockedOutDate]
                                               ,[FailedPasswordAttemptCount]
                                               ,[FailedPasswordAttemptWindowStart]
                                               ,[FailedPasswordAnswerAttemptCount]
                                               ,[FailedPasswordAnswerAttemptWindowStart]
                                               ,[Comment])
                                         VALUES
                                               (NEWID()
                                               ,'ServiceTests'
                                               ,'ServiceTests@ServiceTests.com'
                                               ,'ServiceTests'
                                               ,'ServiceTests'
                                               ,'ServiceTests'
                                               ,1
                                               ,GETDATE()
                                               ,GETDATE()
                                               ,GETDATE()
                                               ,GETDATE()
                                               ,0
                                               ,0
                                               ,GETDATE()
                                               ,0
                                               ,GETDATE()
                                               ,0
                                               ,GETDATE()
                                               ,'ServiceTests')
                                           ";
        private const string SqlTestUserDrop = "DELETE FROM Users WHERE UserName = 'ServiceTests'";

        public static void CreateUser()
        {
            var factory = Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.First(f => f.RepositoryTypeName == "Soaf.Security.IUserRepository, Soaf");
            var context = new ObjectContext(factory.ConnectionString);
            context.Connection.Open();
            using (var connection = new SqlConnection(context.Connection.CastTo<EntityConnection>().StoreConnection.ConnectionString))
            {
                connection.RunScript(SqlTestUserCreate);
            }
        }

        public static void DeleteUser()
        {
            var factory = Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.First(f => f.RepositoryTypeName == "Soaf.Security.IUserRepository, Soaf");
            var context = new ObjectContext(factory.ConnectionString);
            context.Connection.Open();
            using (var connection = new SqlConnection(context.Connection.CastTo<EntityConnection>().StoreConnection.ConnectionString))
            {
                connection.RunScript(SqlTestUserDrop);
            }
        }

        public static void LogIn(string username, string password, bool refreshAuthenticatedUserCookie = false)
        {
            var authenticationService = Common.ServiceProvider.GetService<Security.IAuthenticationService>();
            authenticationService.Login(username, password, null, true);

            var membershipUser = System.Web.Security.Membership.GetUser(username, false);
            Assert.IsNotNull(membershipUser);
            Assert.IsTrue(membershipUser.IsOnline);

            if (!refreshAuthenticatedUserCookie) return;

            // Retrieve a cookie which can be used to authenticate calls made bypassing the RemoteServiceBehavior
            var authenticationClient = new WebAuthenticationService.AuthenticationServiceClient();
            using (new OperationContextScope(authenticationClient.InnerChannel))
            {
                authenticationClient.Login(username, password, null, true);

                // Extract the cookie embedded in the received web service response
                // and stores it locally
                var response = (HttpResponseMessageProperty) OperationContext.Current.IncomingMessageProperties[HttpResponseMessageProperty.Name];
                AuthenticatedUserCookie = response.Headers["Set-Cookie"];
            }
        }

        public static void LogOut()
        {
            var authenticationService = Common.ServiceProvider.GetService<Security.IAuthenticationService>();
            System.Web.Security.MembershipUser membershipUser = System.Web.Security.Membership.GetUser("ServiceTests", false);
            Assert.IsNotNull(membershipUser);
            Assert.IsTrue(membershipUser.IsOnline);
            authenticationService.Logout();

            // Cleanup separately retrieved authentication cookie as well
            if (!string.IsNullOrEmpty(AuthenticatedUserCookie))
            {
                var authenticationClient = new WebAuthenticationService.AuthenticationServiceClient();
                authenticationClient.Logout();
            }

            var factory = Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.First(f => f.RepositoryTypeName == "Soaf.Security.IUserRepository, Soaf");
            var context = new ObjectContext(factory.ConnectionString);
            context.Connection.Open();
            using (var connection = new SqlConnection(context.Connection.CastTo<EntityConnection>().StoreConnection.ConnectionString))
            {
                var isOnline = connection.Execute<bool>("SELECT IsOnline FROM [UserRepository].[dbo].[Users] WHERE UserName = '{0}'".FormatWith(membershipUser.UserName));
                Assert.IsFalse(isOnline);
            }
            
        }
    }
}
