﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Data;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.TypeSystem;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using TestApplication1.Model;
using DescriptionAttribute = System.ComponentModel.DescriptionAttribute;

namespace Soaf.Testing
{
    [TestClass]
    public class CoreTests
    {
        [TestInitialize]
        public void OnTestInitialize()
        {
            Monitor.Enter(Common.SyncRoot);

            PrepAuthentication.CreateUser();
            PrepAuthentication.LogIn("ServiceTests", "ServiceTests");
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            Common.ServiceProvider.GetService<ICache>().Clear();
            
            PrepAuthentication.LogOut();
            PrepAuthentication.DeleteUser();

            Monitor.Exit(Common.SyncRoot);
        }

        /// <summary>
        ///   Tests basic authorization.
        /// </summary>
        [TestMethod]
        public void TestAuthorization()
        {
            var principalContext = Common.ServiceProvider.GetService<IPrincipalContext>();
            var unauthorizedPrincipal = new GenericPrincipal(new GenericIdentity(string.Empty, true));
            principalContext.Principal = unauthorizedPrincipal;

            try
            {
                Common.ServiceProvider.GetService<AuthorizingModel>();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(UnauthorizedAccessException));
            }

            var userPrincipal = new GenericPrincipal(new GenericIdentity("User", true), "User");
            principalContext.Principal = userPrincipal;

            var instance = Common.ServiceProvider.GetService<AuthorizingModel>();
            Assert.IsNotNull(instance);

            try
            {
                instance.DoAdminWork();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(UnauthorizedAccessException));
            }
            string result = instance.DoAdminOrSystemWorkWithReturn();
            Assert.IsNull(result);

            var systemPrincipal = new GenericPrincipal(new GenericIdentity("System", true, id: Guid.NewGuid()), "User");
            principalContext.Principal = systemPrincipal;

            instance = Common.ServiceProvider.GetService<AuthorizingModel>();
            Assert.IsNotNull(instance);

            try
            {
                instance.DoAdminWork();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(UnauthorizedAccessException));
            }

            result = instance.DoAdminOrSystemWorkWithReturn();
            Assert.IsTrue(result == "Hello");

            var adminPrincipal = new GenericPrincipal(new GenericIdentity("User", true), "Administrator", "User");
            principalContext.Principal = adminPrincipal;

            instance = Common.ServiceProvider.GetService<AuthorizingModel>();
            Assert.IsNotNull(instance);

            instance.DoAdminWork();

            principalContext.Principal = userPrincipal;
            result = instance.DoAdminOrSystemWorkWithReturn();
            Assert.IsNull(result);
        }

        /// <summary>
        ///   Tests that attributes marked as Inerhited = false are respected when getting attributes.
        /// </summary>
        [TestMethod]
        public void TestNonInheritedAttribute()
        {
            var attribute = typeof(InheritedAttributeObject).GetAttribute<NonInheritedTestAttribute>();
            Assert.IsNull(attribute);

            attribute = typeof(InheritedAttributeObject).GetMethod("Execute").GetAttribute<NonInheritedTestAttribute>();
            Assert.IsNull(attribute);
        }

        /// <summary>
        ///   Tests the validating object annotated with ValidationAttributes on properties and methods.
        /// </summary>
        [TestMethod]
        public void TestValidatingObject()
        {
            ValidatingObject instance = Common.ServiceProvider.GetService<ValidatingObject>();

            instance.ValidatingProperty1 = "Hello";

            Assert.AreEqual(instance.ValidatingProperty1, "Hello");

            try
            {
                instance.ValidatingProperty1 = null;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex.InnerException, typeof(ValidationException));
            }

            try
            {
                instance.ValidatingProperty1 = string.Empty;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex.InnerException, typeof(ValidationException));
            }

            instance.ValidatingMethod1("Hello");

            try
            {
                instance.ValidatingMethod1(null);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex.InnerException, typeof(ValidationException));
            }

            try
            {
                instance.ValidatingMethod1(string.Empty);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex.InnerException, typeof(ValidationException));
            }

            instance.ValidatingMethod2(1);

            try
            {
                instance.ValidatingMethod2(2);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex.InnerException, typeof(ValidationException));
            }

            // test Validate method
            instance = new ValidatingObject();

            var results = instance.Validate();

            Assert.IsTrue(results.Count == 1);
        }


        /// <summary>
        ///   Tests the validating object annotated with ValidationAttributes on properties and methods.
        /// </summary>
        [TestMethod]
        public void TestDataErrorInfoObject()
        {
            var instance = Common.ServiceProvider.GetService<DataErrorInfoObject>();

            instance.ValidatingProperty1 = "Hello";

            Assert.AreEqual(instance.ValidatingProperty1, "Hello");

            instance.ValidatingProperty1 = null;
            Assert.IsFalse(instance.CastTo<IDataErrorInfo>()["ValidatingProperty1"].IsNullOrEmpty());
            Assert.IsFalse(instance.CastTo<IDataErrorInfo>().Error.IsNullOrEmpty());

            instance.ValidatingProperty1 = "Hello";
            Assert.IsTrue(instance.CastTo<IDataErrorInfo>()["ValidatingProperty1"] == null);
            Assert.IsTrue(instance.CastTo<IDataErrorInfo>().Error == null);

            instance.ValidatingProperty1 = string.Empty;
            Assert.IsFalse(instance.CastTo<IDataErrorInfo>()["ValidatingProperty1"].IsNullOrEmpty());
            Assert.IsFalse(instance.CastTo<IDataErrorInfo>().Error.IsNullOrEmpty());
        }

        /// <summary>
        ///   Tests that the object marked with HasDefaults gets the correct default values on resolution.
        /// </summary>
        [TestMethod]
        public void TestHasDefaultsObject()
        {
            HasDefaultsObject instance = Common.ServiceProvider.GetService<HasDefaultsObject>();

            Assert.IsTrue(instance.StringValue == "Hello");

            Assert.IsTrue(instance.IntValue == 1);

            Assert.IsTrue(instance.IntValue2 == 0);

            Assert.IsTrue(instance.ObjectList != null && instance.ObjectList.Count == 0 && instance.ObjectList.GetType() == typeof(List<object>));
        }

        /// <summary>
        ///   Tests the notifying object for implementing INotifyPropertyChanged automatically and for notifying on attributed properties changed.
        /// </summary>
        [TestMethod]
        public void TestNotifyingObject()
        {
            NotifyingObject instance = Common.ServiceProvider.GetService<NotifyingObject>();

            Assert.IsTrue(instance is INotifyPropertyChanged);

            var notifyPropertyChanged = instance as INotifyPropertyChanged;

            Assert.IsNotNull(notifyPropertyChanged);

            var propertiesChanged = new List<string>();

            // ReSharper disable PossibleNullReferenceException
            notifyPropertyChanged.PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged);
            // ReSharper restore PossibleNullReferenceException

            instance.Property1 = "Hello";

            Assert.IsTrue(propertiesChanged.SequenceEqual(new[] { "Property1", "Property2" }));
        }

        /// <summary>
        ///   Tests the notifying object support for DependsOn using a complex property path (with dots).
        /// </summary>
        [TestMethod]
        public void TestNotifyingObjectDependsOnComplexPath()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingObjectWithChild>();

            var notifyPropertyChanged = instance as INotifyPropertyChanged;

            Assert.IsNotNull(notifyPropertyChanged);

            var propertiesChanged = new List<string>();

            // ReSharper disable PossibleNullReferenceException
            notifyPropertyChanged.PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged);
            // ReSharper restore PossibleNullReferenceException

            instance.Child.Property1 = "Hello";

            Assert.IsTrue(propertiesChanged.SequenceEqual(new[] { "Child.Property1", "DependentProperty" }));
        }

        /// <summary>
        ///   Tests the notifying object support for a regular property that is NOT a dependency on an object that supports both NotifyAll and NotifyChild.
        /// </summary>
        [TestMethod]
        public void TestNotifyingObjectOnNonDependencyNonComplexPath()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingObjectWithChild>();

            var notifyPropertyChanged = instance as INotifyPropertyChanged;

            Assert.IsNotNull(notifyPropertyChanged);

            var propertiesChanged = new List<string>();

            // ReSharper disable PossibleNullReferenceException
            notifyPropertyChanged.PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged);
            // ReSharper restore PossibleNullReferenceException

            instance.RegularProperty = "Hello";

            Assert.IsTrue(propertiesChanged.SequenceEqual(new[] { "RegularProperty", "DependsOnRegularProperty" }));
        }

        /// <summary>
        ///   Tests the notifying object support for DependsOn using a complex property path (with dots). Uses
        /// a type that has NotifyOnChildPropertyChanges set to false.
        /// </summary>
        [TestMethod]
        public void TestNotifyingObjectDependsOnComplexPathWithoutChildPropertyChanges()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingObjectWithChildDependency>();

            Assert.IsTrue(instance is INotifyPropertyChanged);

            var propertiesChanged = new List<string>();
            instance.CastTo<INotifyPropertyChanged>().PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged);

            instance.Child.Property1 = "Hello";
            instance.Child.Property2 = "Test property2 Change";
            instance.Child = Common.ServiceProvider.GetService<NotifyingAnyPropertyChangedObject>();

            Assert.AreEqual(5, propertiesChanged.Count);
            Assert.IsTrue(propertiesChanged.SequenceEqual(new[] { 
                "DependentProperty", "DependentProperty2", 
                "Child", "DependentProperty", "DependentProperty2" }));
        }

        /// <summary>
        /// Tests the view context DependsOn functionality in a common use case.
        /// </summary>
        [TestMethod]
        public void TestViewContextComplexDependsOn()
        {
            var viewContext = Common.ServiceProvider.GetService<TestViewContext>();
            for (int i = 0; i < 10; i++)
            {
                var child = Common.ServiceProvider.GetService<TestChildViewModel>();
                child.Name = "Name" + i;
                viewContext.ViewModel.Children.Add(child);
            }

            var propertyChanges = new List<string>();
            viewContext.CastTo<INotifyPropertyChanged>().PropertyChanged += Common.CollectPropertyChangesTo(propertyChanges);

            viewContext.ViewModel.Children.Last().Name += "_New";

            Assert.IsTrue(propertyChanges.SequenceEqual(new[] { "ViewModel.Children.Item.Name", "CalculatedProperty" }));
            propertyChanges.Clear();

            var propertyChanges2 = new List<string>();
            viewContext.ViewModel.CastTo<INotifyPropertyChanged>().PropertyChanged += Common.CollectPropertyChangesTo(propertyChanges2);
            viewContext.ViewModel.Name = "NewName";

            Assert.IsTrue(propertyChanges.SequenceEqual(new[] { "ViewModel.Name", "ViewModel.CalculatedProperty1", "ViewModel.CalculatedProperty2" }));
            Assert.IsTrue(propertyChanges2.SequenceEqual(new[] { "Name", "CalculatedProperty1", "CalculatedProperty2" }));
        }

        /// <summary>
        ///   Tests the notifying object for implementing INotifyPropertyChanged automatically and for notifying on any properties changed.
        /// </summary>
        [TestMethod]
        public void TestNotifyingAnyPropertyChangedObject()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingAnyPropertyChangedObject>();

            var notifyPropertyChanged = instance as INotifyPropertyChanged;

            Assert.IsNotNull(notifyPropertyChanged);

            var propertiesChanged = new List<string>();

            notifyPropertyChanged.PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged);

            instance.Property1 = "Hello";

            Assert.IsTrue(propertiesChanged.Count == 1 && propertiesChanged.SequenceEqual(new[] { "Property1" }));
        }

        /// <summary>
        /// Tests the notifying child property changed object.
        /// </summary>
        [TestMethod]
        public void TestNotifyingChildPropertyChangedObject()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingObjectWithChild>();

            var child = Common.ServiceProvider.GetService<NotifyingAnyPropertyChangedObject>();

            instance.Child = child;

            var propertiesChanged = new List<string>();

            ((INotifyPropertyChanged)instance).PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged, instance);

            child.Property1 = "Hello";

            Assert.IsTrue(propertiesChanged.SequenceEqual(new[] { "Child.Property1", "DependentProperty" }));
        }

        /// <summary>
        /// Tests the notifying child property changed object.
        /// </summary>
        [TestMethod]
        public void TestNotifyingDependentPropertyChangedViaIgnorePropertyChangesProperty()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingObjectWithChild>();
            var propertiesChanged = new List<string>();

            ((INotifyPropertyChanged)instance).PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged, instance);

            instance.IgnoredProperty = "Ignored?";

            Assert.IsTrue(propertiesChanged.SequenceEqual(new[] { "DependsOnIgnoredProperty" }));
        }

        /// <summary>
        /// Tests the notifying child collection property changed object with updating property.
        /// </summary>
        [TestMethod]
        public void TestNotifyingChildCollectionPropertyChangedObjectWithUpdatingProperty()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingObjectWithChild>();

            var child = Common.ServiceProvider.GetService<NotifyingAnyPropertyChangedObject>();

            instance.Children.Add(child);

            var propertiesChanged = new List<string>();

            ((INotifyPropertyChanged)instance).PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged, instance);

            child.Property2 = "Hello";

            Assert.IsTrue(propertiesChanged.SequenceEqual(new[] { "Children.Item.Property2" }));
        }

        /// <summary>
        /// Tests the notifying child collection property changed object with setting new collection.
        /// </summary>
        [TestMethod]
        public void TestNotifyingChildCollectionPropertyChangedObjectWithSettingNewCollection()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingObjectWithChild>();

            var child = Common.ServiceProvider.GetService<NotifyingAnyPropertyChangedObject>();

            instance.Children.Add(child);

            var propertiesChanged = new List<string>();

            ((INotifyPropertyChanged)instance).PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged, instance);

            instance.Children = new ObservableCollection<NotifyingAnyPropertyChangedObject>();

            child.Property2 = "Hello";

            Assert.IsTrue(propertiesChanged.Count == 1 && propertiesChanged.SequenceEqual(new[] { "Children" }));
        }

        /// <summary>
        /// Tests the notifying child collection property changed object with remove collection item.
        /// </summary>
        [TestMethod]
        public void TestNotifyingChildCollectionPropertyChangedObjectWithRemoveCollectionItem()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingObjectWithChild>();

            var child = Common.ServiceProvider.GetService<NotifyingAnyPropertyChangedObject>();

            instance.Children.Add(child);

            var propertiesChanged = new List<string>();

            ((INotifyPropertyChanged)instance).PropertyChanged += Common.CollectPropertyChangesTo(propertiesChanged, instance);

            // Triggers "Count" and "Item[]" property changed notifications
            instance.Children.Remove(child);

            // Triggers "Property2" change notification
            child.Property2 = "Hello";

            // The root object should report Children changed 2 times
            Assert.IsTrue(propertiesChanged.SequenceEqual(new [] {"Children.Count", "Children.Item[]"}));
        }

        /// <summary>
        ///   Tests the begin/cancel/end functionality of an editable object.
        /// </summary>
        [TestMethod]
        public void TestEditableObject()
        {
            var instance = Common.ServiceProvider.GetService<EditableObject>();
            var notTrackedPropertyValue = Common.ServiceProvider.GetService<EditableObject>();

            // Confirm initial values
            Assert.IsTrue(instance.Property1 == "Hello1");
            Assert.IsTrue(instance.Property2 == "Hello2");
            Assert.IsTrue(instance.NotTrackedProperty == null);

            // Confirm instance implements required interface
            var editableObject = instance as IEditableObjectExtended;
            Assert.IsNotNull(editableObject);
            Assert.IsFalse(editableObject.IsChanged);

            // Initiate edits
            // ReSharper disable PossibleNullReferenceException
            editableObject.BeginNamedEdit("Test");
            editableObject.BeginEdit();
            // ReSharper restore PossibleNullReferenceException

            // Check that excluded property from change tracking doesn't change the changed state
            instance.NotTrackedProperty = notTrackedPropertyValue;
            Assert.IsFalse(editableObject.IsChanged);

            // Modify property of not tracked property -> it should not make owner Changed
            instance.NotTrackedProperty.Property1 = "NotTrackedProperty-2";
            Assert.IsTrue(notTrackedPropertyValue.CastTo<IEditableObjectExtended>().IsChanged);
            Assert.IsFalse(notTrackedPropertyValue.CastTo<IEditableObjectExtended>().IsEditing);
            Assert.IsFalse(editableObject.IsChanged);

            // Ensure values get modified properly
            instance.Property1 = "Hello1-2";
            instance.Property2 = "Hello2-2";
            Assert.IsTrue(instance.Property1 == "Hello1-2");
            Assert.IsTrue(instance.Property2 == "Hello2-2");

            // Check cancel edit
            editableObject.CancelEdit();
            Assert.IsFalse(editableObject.IsChanged);
            Assert.IsTrue(instance.Property1 == "Hello1");
            Assert.IsTrue(instance.Property2 == "Hello2");
            Assert.IsTrue(instance.NotTrackedProperty == notTrackedPropertyValue);
            Assert.IsTrue(instance.NotTrackedProperty.Property1 == "NotTrackedProperty-2");

            // Check begin edit
            editableObject.BeginEdit();
            instance.Property1 = "Hello1-2";
            Assert.IsTrue(editableObject.IsChanged);

            editableObject.EndEdit();
            Assert.IsTrue(editableObject.IsChanged);

            editableObject.CancelEdit();
            instance.Property2 = "Hello2-2";
            Assert.IsTrue(instance.Property1 == "Hello1-2");
            Assert.IsTrue(instance.Property2 == "Hello2-2");

            // Verify change tracking
            Assert.IsTrue(editableObject.IsChanged);
            editableObject.AcceptChanges();
            Assert.IsFalse(editableObject.IsChanged);

            // Verify after cancelling named edit we are back at original values
            editableObject.CancelNamedEdit("Test");
            Assert.IsTrue(instance.Property1 == "Hello1");
            Assert.IsTrue(instance.Property2 == "Hello2");

            // Verify it is changed again, since last accept changes
            Assert.IsTrue(editableObject.IsChanged);

            // Verify can alter dynamically implemented property
            Assert.IsTrue(editableObject.CascadeEditingEventsToChildren);
            editableObject.CascadeEditingEventsToChildren = false;
            Assert.IsFalse(editableObject.CascadeEditingEventsToChildren);
        }

        /// <summary>
        ///   Tests that transient components are garbage collected.
        /// </summary>
        [TestMethod]
        public void TestComponentGarbageCollection()
        {
            object instance = Common.ServiceProvider.GetService<SimpleObject>();
            var reference = new WeakReference(instance);
            // ReSharper disable RedundantAssignment
            instance = null;
            // ReSharper restore RedundantAssignment

            GC.Collect();

            Assert.IsTrue(!reference.IsAlive && reference.Target == null);

            var instanceResolver = Common.ServiceProvider.GetService<Func<SimpleObject>>();
            instance = instanceResolver();

            reference = new WeakReference(instance);

            Assert.IsTrue(reference.IsAlive && reference.Target != null);

            // ReSharper disable RedundantAssignment
            instance = null;
            // ReSharper restore RedundantAssignment

            GC.Collect();

            Assert.IsTrue(!reference.IsAlive && reference.Target == null);

            reference = GetDisposedDisposableReference<DisposableObject>();

            GC.Collect();

            Assert.IsTrue(!reference.IsAlive && reference.Target == null);

            instance = Common.ServiceProvider.GetService<DisposableSingleton>();
            object instance2 = Common.ServiceProvider.GetService<DisposableSingleton>();
            Assert.AreSame(instance, instance2);

            reference = new WeakReference(instance);
            instance.CastTo<DisposableSingleton>().Dispose();

            // ReSharper disable RedundantAssignment
            instance = null;
            instance2 = null;
            // ReSharper restore RedundantAssignment

            GC.Collect();

            Assert.IsTrue(!reference.IsAlive && reference.Target == null);

            var perThread = Common.ServiceProvider.GetService<PerThreadObject>();
            var perThread2 = Common.ServiceProvider.GetService<PerThreadObject>();
            Assert.AreSame(perThread, perThread2);

            instance = Common.ServiceProvider.GetService<DisposablePerThreadObject>();
            instance2 = Common.ServiceProvider.GetService<DisposablePerThreadObject>();
            Assert.AreSame(instance, instance2);

            reference = new WeakReference(instance);
            instance.CastTo<DisposablePerThreadObject>().Dispose();

            // ReSharper disable RedundantAssignment
            instance = null;
            instance2 = null;
            // ReSharper restore RedundantAssignment

            GC.Collect();

            Assert.IsTrue(!reference.IsAlive && reference.Target == null);

            perThread2 = Common.ServiceProvider.GetService<PerThreadObject>();
            Assert.AreSame(perThread, perThread2);

            instance = Common.ServiceProvider.GetService<DisposableSingleton>();
            instance.CastTo<IDisposable>().Dispose();
            instance2 = Common.ServiceProvider.GetService<DisposableSingleton>();
            Assert.AreNotSame(instance, instance2);

            instance = Common.ServiceProvider.GetService<DisposablePerThreadObject>();
            instance.CastTo<IDisposable>().Dispose();
            instance2 = Common.ServiceProvider.GetService<DisposablePerThreadObject>();
            Assert.AreNotSame(instance, instance2);

            IUnitOfWork work;
            using (work = Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Create())
            {

            }
            Assert.AreNotSame(work, Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Current);

            instance = Common.ServiceProvider.GetService<ScopedObject>();
            reference = new WeakReference(instance);

            // ReSharper disable RedundantAssignment
            instance = null;
            // ReSharper restore RedundantAssignment

            GC.Collect();

            Assert.IsTrue(!reference.IsAlive && reference.Target == null);
        }

        private WeakReference GetDisposedDisposableReference<T>() where T : IDisposable
        {
            using (var disposable = Common.ServiceProvider.GetService<T>())
            {
                return new WeakReference(disposable);
            }
        }

        [TestMethod]
        public void TestWeakDelegatesUnregisterCallbacks()
        {
            var testHandler = new TestWeakDelegateCanRemove();
            int calledNumTimes = 0;

            Action<EventHandler> fnUnregister = weakDelegateRef =>
            {
                calledNumTimes++;
                testHandler.TestEvent -= weakDelegateRef;
            };

            // Subscribe to event with methods from instances we won't hold strong reference to
            Action subscribeToEvent = () =>
                {
                    // Use one handler to subscribe twice
                    EventHandler eventHandler = new TestWeakDelegateCanRemove().ShouldNotExecute;
                    testHandler.TestEvent += WeakDelegate.MakeWeak<EventHandler>(
                        eventHandler, fnUnregister);
                    testHandler.TestEvent += WeakDelegate.MakeWeak<EventHandler>(
                        eventHandler, fnUnregister);

                    // And another from different instance
                    testHandler.TestEvent += WeakDelegate.MakeWeak<EventHandler>(
                        new TestWeakDelegateCanRemove().ShouldNotExecute, fnUnregister);
                };
            // Call it in a different function, so that Debugger doesn't hold any references
            subscribeToEvent();

            Assert.AreEqual(3, testHandler.GetTestEventSubscribersCount());

            GC.Collect(GC.MaxGeneration);
            GC.WaitForFullGCComplete();

            // Unsubscribe will only execute if we attempt firing the event
            testHandler.FireTestEvent();

            // Every unsubscribe callback should have been called
            Assert.AreEqual(3, calledNumTimes);
            // Verify that event doesn't have any subscribers
            Assert.AreEqual(0, testHandler.GetTestEventSubscribersCount());
        }

        [TestMethod]
        public void TestWeakDelegatesCanUnsubscribeWithEvents()
        {
            var testHandler = new TestWeakDelegateCanRemove();

            testHandler.TestEvent += WeakDelegate.MakeWeak<EventHandler>(testHandler.ShouldNotExecute);
            testHandler.TestEvent += WeakDelegate.MakeWeak<EventHandler>(testHandler.ShouldExecute);
            testHandler.TestEvent -= WeakDelegate.MakeWeak<EventHandler>(testHandler.ShouldNotExecute);

            testHandler.FireTestEvent();
        }

        [TestMethod]
        public void TestWeakDelegateRemoveInstanceHandler()
        {
            int i = 0;
            // make sure the methods are instance methods by using a closure
            EventHandler shouldNotExecute = delegate { if (i != 0) throw new Exception(); };
            EventHandler shouldExecute = delegate { if (i != 0) throw new Exception(); };

            // Add Weak,Weak/Remove Weak
            var a = WeakDelegate.MakeWeak<EventHandler>(shouldNotExecute);
            a = (EventHandler)Delegate.Combine(a, WeakDelegate.MakeWeak<EventHandler>(shouldExecute));
            a = (EventHandler)WeakDelegate.Remove(a, WeakDelegate.MakeWeak<EventHandler>(shouldNotExecute));
            var invocationList = a.GetInvocationList();
            if (invocationList.Length != 1
                || invocationList[0].Method.Name == "ShouldNotExecute")
            {
                throw new InvalidOperationException("Delegate did not remove");
            }

            // Add Regular,Weak/Remove Weak
            a = shouldNotExecute;
            a = (EventHandler)Delegate.Combine(a, WeakDelegate.MakeWeak<EventHandler>(shouldExecute));
            a = (EventHandler)WeakDelegate.Remove(a, WeakDelegate.MakeWeak<EventHandler>(shouldNotExecute));
            invocationList = a.GetInvocationList();
            if (invocationList.Length != 1
                || invocationList[0].Method.Name == "ShouldNotExecute")
            {
                throw new InvalidOperationException("Delegate did not remove");
            }


            // Add Regular,Weak/Remove Regular
            a = shouldNotExecute;
            a = (EventHandler)Delegate.Combine(a, WeakDelegate.MakeWeak<EventHandler>(shouldExecute));
            a = (EventHandler)WeakDelegate.Remove(a, shouldNotExecute);
            invocationList = a.GetInvocationList();
            if (invocationList.Length != 1
                || invocationList[0].Method.Name == "ShouldNotExecute")
            {
                throw new InvalidOperationException("Delegate did not remove");
            }


            // Add Weak,Weak/Remove Regular
            a = WeakDelegate.MakeWeak<EventHandler>(shouldNotExecute);
            a = (EventHandler)Delegate.Combine(a, WeakDelegate.MakeWeak<EventHandler>(shouldExecute));
            a = (EventHandler)WeakDelegate.Remove(a, shouldNotExecute);
            invocationList = a.GetInvocationList();
            if (invocationList.Length != 1
                || invocationList[0].Method.Name == "ShouldNotExecute")
            {
                throw new InvalidOperationException("Delegate did not remove");
            }

        }

        /// <summary>
        ///   Tests that type and member attributes are inherited.
        /// </summary>
        [TestMethod]
        public void TestAttributeInheritance()
        {
            Type type = typeof(InheritedAttributeObject);
            Assert.IsTrue(type.HasAttribute<SupportsCachingAttribute>());
            Assert.IsTrue(type.GetMethod("Execute").HasAttribute<CacheAttribute>());
        }

        /// <summary>
        ///   Tests that an IInitializable object gets Initialized.
        /// </summary>
        [TestMethod]
        public void TestInitializableObject()
        {
            InitializableObject instance = Common.ServiceProvider.GetService<InitializableObject>();
            Assert.IsTrue(instance.IsInitialized);
        }

        /// <summary>
        ///   Tests that an IInitializable object gets Initialized only once.
        /// </summary>
        [TestMethod]
        public void TestInitializableObjectInitializedOnce()
        {
            InitializableObject instance1 = Common.ServiceProvider.GetService<InitializableObject>();
            InitializableObject instance2 = Common.ServiceProvider.GetService<InitializableObject>();
            Assert.AreEqual(instance1, instance2);
            Assert.IsTrue(instance1.IsInitialized && instance2.IsInitialized);
        }

        [TestMethod]
        public void TestOperationCaching()
        {
            const string testString = "Arg1";
            var testGuid = Guid.NewGuid();
            var testTuple = new Tuple<string, int>("S1", 2);

            //// Batch 1. Verify instance method calls a cached properly

            var testObject1 = Common.ServiceProvider.GetService<AllowSingleMethodCallObject>();
            var testObject2 = Common.ServiceProvider.GetService<AllowSingleMethodCallObject>();
            var testResult11 = testObject1.ExecuteInstanceCached(s => s.GetRandomNumber(testString, testGuid, testTuple));
            var testResult21 = testObject2.ExecuteInstanceCached(s => s.GetRandomNumber(testString, testGuid, testTuple));
            var testResult12 = testObject1.ExecuteInstanceCached(s => s.GetRandomNumber(testString, testGuid, testTuple));
            var testResult22 = testObject2.ExecuteInstanceCached(s => s.GetRandomNumber(testString, testGuid, testTuple));

            Assert.AreNotEqual(testResult11, testResult21); // Should get different random results
            // Cached returned same values and no exception thrown
            Assert.AreEqual(testResult11, testResult12);
            Assert.AreEqual(testResult21, testResult22);

            //// Batch 2. Verify extension method calls are cached properly

            testObject1 = Common.ServiceProvider.GetService<AllowSingleMethodCallObject>();
            testObject2 = Common.ServiceProvider.GetService<AllowSingleMethodCallObject>();
            testResult11 = testObject1.ExecuteInstanceCached(s => s.GetRandomNumberWithExtension(testString));
            testResult21 = testObject2.ExecuteInstanceCached(s => s.GetRandomNumberWithExtension(testString));
            testResult12 = testObject1.ExecuteInstanceCached(s => s.GetRandomNumberWithExtension(testString));
            testResult22 = testObject2.ExecuteInstanceCached(s => s.GetRandomNumberWithExtension(testString));

            Assert.AreNotEqual(testResult11, testResult21); // Should get different random results
            // Cached returned same values and no exception thrown
            Assert.AreEqual(testResult11, testResult12);
            Assert.AreEqual(testResult21, testResult22);

            //// Batch 3. Verify non-instance specific caching

            testObject1 = Common.ServiceProvider.GetService<AllowSingleMethodCallObject>();
            testObject2 = Common.ServiceProvider.GetService<AllowSingleMethodCallObject>();
            testResult11 = testObject1.ExecuteCached(s => s.GetRandomNumber(testString, testGuid, testTuple));
            testResult21 = testObject2.ExecuteCached(s => s.GetRandomNumber(testString, testGuid, testTuple));
            Assert.AreEqual(testResult11, testResult21);

            testObject1 = Common.ServiceProvider.GetService<AllowSingleMethodCallObject>();
            testObject2 = Common.ServiceProvider.GetService<AllowSingleMethodCallObject>();
            testResult12 = testObject1.ExecuteCached(s => s.GetRandomNumberWithExtension(testString));
            testResult22 = testObject2.ExecuteCached(s => s.GetRandomNumberWithExtension(testString));
            Assert.AreEqual(testResult12, testResult22);
        }

        /// <summary>
        ///   Tests that a caching object has non-virtual members tagged throws an exception.
        /// </summary>
        [TestMethod]
        public void TestNonVirtualInvalidCachingObject()
        {
            try
            {
                Common.ServiceProvider.GetService<NonVirtualInvalidCachingObject>();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex.InnerException, typeof(InvalidOperationException));
                return;
            }

            Assert.Fail();
        }

        /// <summary>
        ///   Tests that a caching object has non-public or non-protected members tagged throws an exception.
        /// </summary>
        [TestMethod]
        public void TestNotAccessibleCachingObject()
        {
            try
            {
                Common.ServiceProvider.GetService<NotAccessibleInvalidCachingObject>();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex.InnerException, typeof(InvalidOperationException));
            }
        }

        /// <summary>
        ///   Tests a caching method with no parameters.
        /// </summary>
        [TestMethod]
        public void TestCachingMethod()
        {
            CachingObject cachingObject = Common.ServiceProvider.GetService<CachingObject>();

            object result1 = cachingObject.CachingMethod();

            object result2 = cachingObject.CachingMethod();

            Assert.AreEqual(result1, result2);
        }

        /// <summary>
        ///   Tests a generic caching method with parameters.
        /// </summary>
        [TestMethod]
        public void TestGenericCachingMethod()
        {
            var value = new object();

            CachingObject cachingObject = Common.ServiceProvider.GetService<CachingObject>();

            object result1 = cachingObject.GenericCachingMethod(1);

            object result2 = cachingObject.GenericCachingMethod<object>(1);

            object result3 = cachingObject.GenericCachingMethod(2);

            object result4 = cachingObject.GenericCachingMethod<object>(1);

            object result5 = cachingObject.GenericCachingMethod(1);

            object result6 = cachingObject.GenericCachingMethod(value);

            object result7 = cachingObject.GenericCachingMethod(value);

            Assert.AreNotSame(result1, result2);

            Assert.AreNotSame(result1, result3);

            Assert.AreSame(result2, result4);

            Assert.AreSame(result6, result7);

            Assert.AreSame(result1, result5);
        }

        /// <summary>
        ///   Tests a caching property.
        /// </summary>
        [TestMethod]
        public void TestCachingProperty()
        {
            var cachingObject = Common.ServiceProvider.GetService<CachingObject>();

            object result1 = cachingObject.CachingProperty;

            object result2 = cachingObject.CachingProperty;

            Assert.AreEqual(result1, result2);
        }

        /// <summary>
        ///   Tests a non cached property.
        /// </summary>
        [TestMethod]
        public void TestNonCachingProperty()
        {
            CachingObject cachingObject = Common.ServiceProvider.GetService<CachingObject>();

            object result1 = cachingObject.NonCachingProperty;

            object result2 = cachingObject.NonCachingProperty;

            Assert.AreNotEqual(result1, result2);
        }

        /// <summary>
        ///   Tests that caching is applied on inherited types.
        /// </summary>
        [TestMethod]
        public void TestCachingInheritance()
        {
            ChildCachingObject cachingObject = Common.ServiceProvider.GetService<ChildCachingObject>();

            const string parameter = "hello";

            object refArgument1 = null;

            object outArgument1;

            object result1 = cachingObject.CachingMethodWithParameters(parameter, ref refArgument1, out outArgument1);

            object refArgument2 = null;

            object outArgument2;

            object result2 = cachingObject.CachingMethodWithParameters(parameter, ref refArgument2, out outArgument2);

            Assert.AreEqual(result1, result2);

            object result3 = cachingObject.ChildCachingMethod();

            object result4 = cachingObject.ChildCachingMethod();

            Assert.AreEqual(result3, result4);
        }

        /// <summary>
        ///   Tests the caching methods with parameters and ref and out arguments.
        /// </summary>
        [TestMethod]
        public void TestCachingMethodWithParameters()
        {
            var cachingObject = Common.ServiceProvider.GetService<CachingObject>();

            string parameter = "hello";

            object refArgument1 = null;

            object outArgument1;

            object result1 = cachingObject.CachingMethodWithParameters(parameter, ref refArgument1, out outArgument1);

            object refArgument2 = null;

            object outArgument2;

            object result2 = cachingObject.CachingMethodWithParameters(parameter, ref refArgument2, out outArgument2);

            Assert.AreEqual(result1, result2);

            Assert.IsNotNull(refArgument1);

            Assert.IsNotNull(refArgument2);

            Assert.AreEqual(refArgument1, refArgument2);

            Assert.AreEqual(outArgument1, outArgument2);

            parameter = "hello2";

            object refArgument3 = null;

            object outArgument3;

            object result3 = cachingObject.CachingMethodWithParameters(parameter, ref refArgument3, out outArgument3);

            object refArgument4 = null;

            object outArgument4;

            object result4 = cachingObject.CachingMethodWithParameters(parameter, ref refArgument4, out outArgument4);

            Assert.AreEqual(result3, result4);

            Assert.IsNotNull(refArgument3);

            Assert.IsNotNull(refArgument4);

            Assert.AreEqual(refArgument1, refArgument2);

            Assert.AreEqual(outArgument3, outArgument4);

            Assert.AreNotEqual(result1, result3);

            Assert.AreNotEqual(refArgument1, refArgument3);

            Assert.AreNotEqual(outArgument1, outArgument3);
        }


        /// <summary>
        ///   Tests that a component registered as a singleton to multiple services resolves correctly.
        /// </summary>
        [TestMethod]
        public void TestMultipleServiceComponent()
        {
            var instance1 = Common.ServiceProvider.GetService<IInterface1>();
            var instance2 = Common.ServiceProvider.GetService<IInterface2>();
            Assert.AreSame(instance1, instance2);
        }

        /// <summary>
        ///   Tests that synchronizable object with a synchronized property is synchronized successfully via the SupportsSynchronizationAttribute and SynchronizedAttribute.
        /// </summary>
        [TestMethod]
        public void TestSynchronizableObject()
        {
            var instance = Common.ServiceProvider.GetService<TestSynchronizableObject>();
            object value = instance.SynchronizedProperty;
            Assert.IsNull(value);
            Assert.IsTrue(instance.SynchronizedSuccessfully);
        }

        /// <summary>
        ///   Tests resolution of generic components registered only as a generic template.
        /// </summary>
        [TestMethod]
        public void TestGenericResolution()
        {
            var instance = Common.ServiceProvider.GetService<ObservableCollection<object>>();
            Assert.IsNotNull(instance);
        }

        /// <summary>
        ///   Tests resolution of ConcreteType which is registered for some interface, but not for itself
        /// </summary>
        [TestMethod]
        public void TestResolutionOfConcreteTypeRegisteredForItsInterface()
        {
            var instance = Common.ServiceProvider.GetService<MultipleInterfaceClass>();
            Assert.IsNotNull(instance);
        }

        /// <summary>
        ///   Tests that required property dependencies are respected.
        /// </summary>
        [TestMethod]
        public void TestMissingPropertyDependency()
        {
            try
            {
                Common.ServiceProvider.GetService<MissingPropertyDependencyTestObject>();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ResolutionException));
            }
        }

        /// <summary>
        ///   Tests that optional constructor dependencies are respected.
        /// </summary>
        [TestMethod]
        public void TestOptionalConstructorDependency()
        {
            var instance = Common.ServiceProvider.GetService<OptionalConstructorDependencyObject>();
            Assert.IsNotNull(instance);
            Assert.IsNull(instance.Test);
        }

        [TestMethod]
        public void TestBasicTypeGeneration()
        {
            var metaType = new MetaType { Members = new { Prop1 = "Hello", Prop2 = 1, Prop3 = Guid.NewGuid() }.GetType().GetProperties().Select(pi => pi.ToMetaProperty().CastTo<MetaMember>()).ToList() };
            var type = metaType.ToType();
            Assert.IsNotNull(type);
            Assert.IsTrue(type.GetProperties().Length == 3);
        }

        [TestMethod]
        public void TestDataContractTypeGeneration()
        {
            var dataContract = typeof(TestApplication1.Model.Test1Model.Test1Contact).ToDataContractType();

            Assert.IsTrue(dataContract.HasAttribute<DataContractAttribute>());

            Assert.IsTrue(dataContract.GetProperties().All(p => p.HasAttribute<DataMemberAttribute>()));
        }

        /// <summary>
        /// Tests read only support for IIsReadOnly.
        /// </summary>
        [TestMethod]
        public void TestReadOnlySupport()
        {
            var instance = Common.ServiceProvider.GetService<SimpleObject>();
            instance.Value2 = "test";
            Assert.IsTrue((string)instance.Value2 == "test");
            instance.IsReadOnly = true;
            Assert.IsTrue(instance.IsReadOnly);
            try
            {
                instance.Value2 = "test2";
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is InvalidOperationException);
            }
            instance.IsReadOnly = false;
            instance.Value2 = "test3";
            Assert.IsTrue((string)instance.Value2 == "test3");
        }

        private IMessenger GetMessenger()
        {
            return new Messenger();
        }

        /// <summary>
        ///   Tests the message bus subscription process.
        /// </summary>
        [TestMethod]
        public void TestMessengerSubscription()
        {
            bool handled = false;
            var message = new TestMessage();
            Guid originalId = message.Id;
            IMessenger messenger = GetMessenger();

            object token = messenger.Subscribe<TestMessage>((sender, t, m) =>
            {
                if (handled) Assert.Fail("Should not have handled the message twice.");
                Assert.AreEqual(message, m);
                Assert.AreEqual(messenger, sender);
                messenger.Subscribe<object>(delegate { });
                handled = true;
            }, m => m.Id == originalId);

            messenger.Publish(message);

            // change id to test filter
            message.Id = Guid.NewGuid();

            messenger.Publish(message);

            // test unsubscription
            messenger.Unsubscribe(token);

            message.Id = originalId;

            messenger.Publish(message);

            if (!handled) Assert.Fail("Message was not handled.");
        }

        /// <summary>
        ///   Tests message subscriptions that use weak references.
        /// </summary>
        [TestMethod]
        public void TestMessengerWeakReferenceSubscription()
        {
            IMessenger messenger = GetMessenger();

            bool handled = false;

            MessageHandler<TestMessage> handler = (b, t, m) => { handled = true; };

            messenger.Subscribe(handler, useWeakReference: true);

            var reference = new WeakReference(handler);

            Assert.IsTrue(reference.IsAlive);

            messenger.Publish(new TestMessage());

            // let GC
            handler = null;

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            Assert.IsNull(handler);
            // ReSharper restore ConditionIsAlwaysTrueOrFalse

            // force scavenge
            messenger.CastTo<Messenger>().ScavengingTimer.Change(0, 5000);

            Thread.Sleep(200);

            GC.Collect();

            Assert.IsFalse(reference.IsAlive);

            Assert.IsTrue(handled);
        }

        /// <summary>
        /// Tests the unit of work attribute behavior.
        /// </summary>
        [TestMethod]
        public void TestUnitOfWorkAttribute()
        {
            var instance = Common.ServiceProvider.GetService<ITestObjectWithUnitOfWork>();
            IUnitOfWork work1, work2;
            instance.Test(out work1, out work2);
            Assert.AreSame(work1, work2);
        }

        /// <summary>
        /// Tests mapping various objects to one another.
        /// </summary>
        [TestMethod]
        public void TestMapping()
        {
            var contact = new Contact
            {
                FirstName = "John",
                LastName = "Doe",
                Addresses = new[]
                                                  {
                                                      new Address {Line1 = "1 Main St.", City = "Podunk", State = "NY", ZipCode = "10000"}
                                                  }
            };

            var map = new ContactToContactViewModelMap();
            var mapper = new Mapper<Contact, ContactViewModel>(map);

            var viewModel = new ContactViewModel();
            mapper.Map(contact, viewModel);

            Common.ServiceProvider.GetService<Func<TimeSpan, IMapper<Contact, ContactViewModel>>>()(TimeSpan.FromMinutes(20));

            Assert.IsTrue(viewModel.StateViewModel.GetType().Assembly.IsDynamic);
            Assert.IsTrue(viewModel.StateViewModel.Name == "NY");

            Assert.IsTrue(viewModel.FirstName == "John" && viewModel.LastName == "Doe" && viewModel.Street == "1 Main St." && viewModel.City == "Podunk" && viewModel.State == "NY" && viewModel.ZipCode == "10000");

            var attributes = TypeDescriptor.GetAttributes(viewModel);
            Assert.IsTrue(attributes.OfType<DescriptionAttribute>().Count() == 1 && attributes.OfType<DescriptionAttribute>().First().Description == "FirstName");

            var properties = TypeDescriptor.GetProperties(viewModel).OfType<PropertyDescriptor>().ToArray();

            var streetProperty = properties.FirstOrDefault(pd => pd.Name == "Street");
            Assert.IsNotNull(streetProperty);

            Assert.IsTrue(streetProperty.Attributes.OfType<DescriptionAttribute>().Count() == 1 && streetProperty.Attributes.OfType<DescriptionAttribute>().First().Description == "Street");

            var firstNameProperty = properties.FirstOrDefault(pd => pd.Name == "FirstName");
            Assert.IsNotNull(firstNameProperty);

            Assert.IsTrue(firstNameProperty.Attributes.OfType<DescriptionAttribute>().Count() == 1 && firstNameProperty.Attributes.OfType<DescriptionAttribute>().First().Description == "First Name");

            var reverseMapper = new Mapper<ContactViewModel, Contact>(new ContactViewModelToContactMap());
            var newContact = new Contact();

            reverseMapper.Map(viewModel, newContact);

            Assert.IsTrue(newContact.FirstName == viewModel.FirstName && newContact.LastName == viewModel.LastName && newContact.Addresses.Count == 1);

            var address = newContact.Addresses.First();

            Assert.IsTrue(address.Line1 == viewModel.Street && address.City == viewModel.City && address.State == viewModel.State && address.ZipCode == viewModel.ZipCode);
        }

        /// <summary>
        /// Tests a scoped lifestyle.
        /// </summary>
        [TestMethod]
        public void TestScopedLifestyle()
        {
            ScopedOrTransientType transient1;
            ScopedOrSingletonType singleton1;
            using (Common.ServiceProvider.GetService<IScope>())
            {
                transient1 = Common.ServiceProvider.GetService<ScopedOrTransientType>();
                var transient2 = Common.ServiceProvider.GetService<ScopedOrTransientType>();
                var cvm1 = Common.ServiceProvider.GetService<ContactViewModel>();
                var cvm2 = Common.ServiceProvider.GetService<ContactViewModel>();
                singleton1 = Common.ServiceProvider.GetService<ScopedOrSingletonType>();
                var singleton2 = Common.ServiceProvider.GetService<ScopedOrSingletonType>();

                Assert.AreSame(transient1, transient2);
                Assert.AreSame(singleton1, singleton2);
                Assert.AreNotSame(cvm1, cvm2);
            }

            // should act like transients outside scope
            var transient3 = Common.ServiceProvider.GetService<ScopedOrTransientType>();
            var transient4 = Common.ServiceProvider.GetService<ScopedOrTransientType>();
            Assert.AreNotSame(transient1, transient3);
            Assert.AreNotSame(transient3, transient4);

            // should act like singletons outside scope
            var singleton3 = Common.ServiceProvider.GetService<ScopedOrSingletonType>();
            var singleton4 = Common.ServiceProvider.GetService<ScopedOrSingletonType>();
            Assert.AreNotSame(singleton1, singleton3);
            Assert.AreSame(singleton3, singleton4);
        }

        /// <summary>
        /// Tests objects whose lifestyles are bound to creation context.
        /// </summary>
        [TestMethod]
        public void TestObjectsBoundToCreationContext()
        {
            var o = Common.ServiceProvider.GetService<BoundToCreationContextObjectOwner>();
            var o2 = Common.ServiceProvider.GetService<BoundToCreationContextObjectOwner>();

            Assert.AreNotSame(o, o2);

            Assert.AreNotSame(o.BoundToCreationContextObject, o2.BoundToCreationContextObject);

            Assert.AreSame(o.BoundToCreationContextObject.Child, o.BoundToCreationContextObject2);

            Assert.AreSame(o2.BoundToCreationContextObject.Child, o2.BoundToCreationContextObject2);
        }

        /// <summary>
        /// Tests creating a logger instance.
        /// </summary>
        [TestMethod]
        public void TestLogger()
        {
            var logManager = Common.ServiceProvider.GetService<ILogManager>();
            var logger = logManager.GetCurrentTypeLogger();
            Assert.IsNotNull(logger);
        }

        /// <summary>
        /// Tests auto mapper using XML deserialization.
        /// </summary>
        [TestMethod]
        public void TestAutoMapperXmlDeserialization()
        {
            var instance = Common.ServiceProvider.GetService<NotifyingObjectWithChild>();
            instance.Child = Common.ServiceProvider.GetService<NotifyingAnyPropertyChangedObject>();
            instance.Child.Property1 = "Hello";
            instance.Child.Property2 = "Hello2";

            instance.Children.Add(Common.ServiceProvider.GetService<NotifyingAnyPropertyChangedObject>());
            instance.Children.Add(Common.ServiceProvider.GetService<NotifyingAnyPropertyChangedObject>());

            for (int i = 0; i < instance.Children.Count; i++)
            {
                instance.Children[i].Property1 = "Child " + i;
            }

            var xml = instance.ToXml();

            var newInstance = xml.FromXml<NotifyingObjectWithChild>(true);
            Assert.IsNotNull(newInstance);
            Assert.IsTrue(newInstance is INotifyPropertyChanged);

            bool propertyChanged = false;
            newInstance.CastTo<INotifyPropertyChanged>().PropertyChanged += delegate { propertyChanged = true; };

            newInstance.Child.Property1 = "Hello3";
            Assert.IsTrue(propertyChanged);

            for (int i = 0; i < newInstance.Children.Count; i++)
            {
                Assert.IsTrue(newInstance.Children[i].Property1 == "Child " + i);
            }

            Assert.IsTrue(newInstance.Children.All(c => c.GetType().Assembly.IsDynamic));
        }

        /// <summary>
        /// Tests lazy components.
        /// </summary>
        [TestMethod]
        public void TestLazy()
        {
            var lazy = Common.ServiceProvider.GetService<Lazy<TestObjectWithUnitOfWork>>();
            Assert.IsFalse(lazy.IsValueCreated);
            Assert.IsNotNull(lazy.Value);
            Assert.IsTrue(lazy.IsValueCreated);
        }

        /// <summary>
        /// Tests using InitializeWith as a replacement for object initializers in resolved types.
        /// </summary>
        [TestMethod]
        public void TestInitializeWith()
        {
            var contact = Common.ServiceProvider.GetService<Func<ContactViewModel>>().InitializeWith(() => new ContactViewModel
                                                                                                     {
                                                                                                         City = "New York",
                                                                                                         FirstName = "John",
                                                                                                         LastName = "Smith",
                                                                                                         State = "NY",
                                                                                                         ZipCode = "11111"
                                                                                                     })();

            Assert.IsTrue(contact.City == "New York");
            Assert.IsTrue(contact.FirstName == "John");
            Assert.IsTrue(contact.LastName == "Smith");
            Assert.IsTrue(contact.State == "NY");
            Assert.IsTrue(contact.ZipCode == "11111");
        }


        [TestMethod]
        public void TestEvaluatorExtensionMethodCalls()
        {
            var item = new { Person = new { Name = "Jeff" } };
            var list = new List<object>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(item);
            }

            var myEnumerables = Evaluator.Current.Evaluate("Batch(6).SelectMany(it).Select(Person.Name)", list);

            Assert.IsTrue(myEnumerables is IEnumerable<object> && myEnumerables.CastTo<IEnumerable<object>>().OfType<string>().Count() == 10 && myEnumerables.CastTo<IEnumerable<object>>().OfType<string>().All(s => s == "Jeff"));

            var filteredEnumerables = Evaluator.Current.Evaluate("Where(it=\"Jeff\")", myEnumerables);

            Assert.IsTrue(filteredEnumerables is IEnumerable<object> && filteredEnumerables.CastTo<IEnumerable<object>>().OfType<string>().Count() == 10 && filteredEnumerables.CastTo<IEnumerable<object>>().OfType<string>().All(s => s == "Jeff"));
        }

        [TestMethod]
        public void TestEvaluatorInstanceMethodCalls()
        {
            var item = new object();
            var list = new List<object>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(item);
            }

            var result = Evaluator.Current.Evaluate("Count.ToString()", list);

            Assert.IsTrue((string)result == "10");
        }

        [TestMethod]
        public void TestEvaluatorComparisons()
        {
            var item = new { Person = new { Name = "Jeff", Id = 123 } };
            var list = new List<object>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(item);
            }

            var result = Evaluator.Current.Evaluate("First().Person.Id > 100 ? \"true\" : \"false\"", list);

            Assert.IsTrue((string)result == "true");

            var result2 = Evaluator.Current.Evaluate("First().Person.Id < 100 ? \"true\" : \"false\"", list);

            Assert.IsTrue((string)result2 == "false");

            var result3 = Evaluator.Current.Evaluate("First().Person.Id = 100 ? \"true\" : \"false\"", list);

            Assert.IsTrue((string)result3 == "false");

            var result4 = Evaluator.Current.Evaluate("First().Person.Id != 100 and First().Person.Id > 100 + 20 ? \"true\" : \"false\"", list);

            Assert.IsTrue((string)result4 == "true");

            var result5 = Evaluator.Current.Evaluate("First().Person.Id > 120 or First().Person.Id < 130 + 20 ? \"true\" : \"false\"", list);

            Assert.IsTrue((string)result5 == "true");

            var result6 = Evaluator.Current.Evaluate("First().Person.Id > 120 and First().Person.Id > 130 + 20 ? \"true\" : \"false\"", list);

            Assert.IsTrue((string)result6 == "false");
        }

        [TestMethod]
        public void TestMemberAccessString()
        {
            Assert.AreEqual("City", Reflector.MemberPath<Contact>(c => c.City));
        }

        [TestMethod]
        public void TestSyncToTargetCollection()
        {
            var sourceCollection = new List<ContactViewModel>
                                       {
                                           new ContactViewModel { FirstName = "FirstName1", City = "Updated"},
                                           new ContactViewModel { FirstName = "FirstName2", City = "Updated" },
                                           new ContactViewModel { FirstName = "FirstName3", City = "Updated" }
                                       };
            var targetCollection = new FixupCollection<Contact>
                                       {
                                           new Contact { FirstName = "FirstName4" },
                                           new Contact { FirstName = "FirstName2" },
                                           new Contact { FirstName = "FirstName1" },
                                           new Contact { FirstName = "FirstName5" },
                                           new Contact { FirstName = "FirstName3" }
                                       };

            // Count number of events during synchronization
            var removalsCount = 0;
            var setsCount = 0;
            targetCollection.ItemSet += (sender, args) => setsCount++;
            targetCollection.ItemRemoved += (sender, args) => removalsCount++;

            // Sync them
            sourceCollection.SyncToTargetCollection(targetCollection,
                (model, contact) => model.FirstName == contact.FirstName,
                (viewModel, contact) =>
                {
                    contact.FirstName = viewModel.FirstName;
                    contact.City = viewModel.City;
                });

            // Validate
            Assert.AreEqual(3, targetCollection.Count);
            Assert.IsTrue(targetCollection.All(p => p.City == "Updated"));
            for (int i = 0; i < targetCollection.Count; i++)
            {
                // Ensure order is valid
                Assert.AreSame(sourceCollection[i].FirstName, targetCollection[i].FirstName);
            }

            // Validate that minimum number of events fired upon sync
            Assert.IsTrue(setsCount == 0);
            Assert.IsTrue(removalsCount == 2);
        }

        [TestMethod]
        public void TestCollectionViewFactoryBehaviorOnExtendedObservableCollection()
        {
            var collection = new ExtendedObservableCollection<ContactViewModel>();

            // Returns ListCollectionView with our fix under normal conditions
            Assert.IsInstanceOfType(collection.CreateView(), typeof(WeakListCollectionView));

            // When bound to grid -> uses QueryableCollectionView
            var theGrid = new RadGridView();
            theGrid.ItemsSource = collection;
            Assert.IsInstanceOfType(theGrid.Items.SourceCollection, typeof(QueryableCollectionView));

            // Let's ensure our collection wasn't wrapped into something else
            var sourceCollection = (QueryableCollectionView)theGrid.Items.SourceCollection;
            Assert.AreSame(collection, sourceCollection.SourceCollection);
        }

        [TestMethod]
        public void TestXmlDeserializerResolvesCollectionIntoWritableCollection()
        {
            var serializedSubject = (new TestICollectionXmlDeserialization
                                        {
                                            CollectionProperty = new Collection<string> { "one", "two" }
                                        }).ToXml();

            var deserializedResult = serializedSubject.FromXml<TestICollectionXmlDeserialization>();

            // Ensure it is writable (without CustomDataContractResolver it would have deserialized it into string[] which is read-only)
            Assert.AreEqual(true, !deserializedResult.CollectionProperty.IsReadOnly);
        }

        [TestMethod]
        public void TestDbViewQueryingOptimizationScope()
        {
            // We don't want this test to run over the wire, but need to use AdoService
            var connectionStringRepository = Common.ServiceProvider.GetService<IConnectionStringRepository>();
            var adoService = new AdoService(connectionStringRepository);

            var executeQuery = new Action(() =>
            {
                var argument = new AdoServiceCommandExecutionArgument
                {
                    ConnectionString = DataExtensionsTests.ConnectionString,
                    ConnectionTimeout = 180,
                    Database = "Test2",
                    CommandText = @"
SELECT *
FROM [dbo].[Contacts] c
JOIN [dbo].[ContactCountries] cc ON
    cc.ContactId = c.Id
JOIN [dbo].[ContactPhoneNumbers] cpn ON
    cpn.ContactId = c.Id
",
                    CommandTimeout = 180,
                    CommandType = CommandType.Text.ToString(),
                    UpdatedRowSource = UpdateRowSource.None.ToString()
                };
                var serverResult = adoService.ExecuteCommandDbDataReader(argument);
                Assert.AreEqual(1104, serverResult.Result.Rows.Count);
            });

            var executeQueryWithinScope = new Action<bool>(sharedMode =>
            {
                using (new DbViewQueryingOptimizationScope(sharedMode, TimeSpan.FromMinutes(1)))
                {
                    executeQuery();
                }
            });

            // Prepare shared scope execution
            var sharedOptimizationScope = new DbViewQueryingOptimizationScope();
            sharedOptimizationScope.DetachScopeFromThread(Thread.CurrentThread);
            var executeQueryWithinSharedScope = new Action(() =>
            {
                sharedOptimizationScope.AttachScopeToThread(Thread.CurrentThread);
                executeQuery();
                sharedOptimizationScope.DetachScopeFromThread(Thread.CurrentThread);
            });

            var testActions = new[]
            {
                // 1 outside of scope
                executeQuery, 
                
                // 2 with sharing on
                () => executeQueryWithinScope(true), 
                () => executeQueryWithinScope(true),

                // 1 in non-shared scope
                () => executeQueryWithinScope(false), 

                // 3 thread sharing 1 scope
                executeQueryWithinSharedScope,
                executeQueryWithinSharedScope,
                executeQueryWithinSharedScope

            };

            // Run All test actions in parallel
            testActions.AsParallel()
                .WithDegreeOfParallelism(testActions.Length)
                .ForAll(a => a());

            // Cleanup shared scope
            sharedOptimizationScope.Dispose();
        }

        [TestMethod]
        public void TestCallingServicesWithNonEnumeratedEnumerables()
        {
            var filters = new ContactServiceFilter {NameFilter = "Contact10"}
                .CreateEnumerable()
                .OrderBy(c => c.NameFilter);

            var contactService = ServiceProvider.Current.GetService<ITestContactService>();
            var contacts = contactService.GetContacts(filters);
            Assert.IsTrue(contacts is IList<ContactViewModel>);

            var clonedFilters = (IEnumerable<ContactServiceFilter>)ServiceProvider.Current.GetService<ICloner>()
                .Clone(filters, typeof(IEnumerable<ContactServiceFilter>), typeof(IEnumerable<ContactServiceFilter>));

            Assert.IsTrue(clonedFilters is IList<ContactServiceFilter>);
        }
    }
}