﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Caching;
using Soaf.Data;
using Soaf.EntityFramework;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Validation;

namespace Soaf.Testing
{
    [TestClass]
    public class CoreExtensionsTests
    {
        [TestInitialize]
        public void OnTestInitialize()
        {
            Monitor.Enter(Common.SyncRoot);

            PrepAuthentication.CreateUser();
            PrepAuthentication.LogIn("ServiceTests", "ServiceTests");
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            Common.ServiceProvider.GetService<ICache>().Clear();
            
            PrepAuthentication.LogOut();
            PrepAuthentication.DeleteUser();

            Monitor.Exit(Common.SyncRoot);
        }

        /// <summary>
        /// Tests the metadata type attribute.
        /// </summary>
        [TestMethod]
        public void TestMetadataTypeAttribute()
        {
            var description = typeof(SimpleObject).GetProperty("CreatedDateTime").GetAttribute<System.ComponentModel.DescriptionAttribute>();

            Assert.IsNotNull(description);

            Assert.IsTrue(description.Description == "Created");
        }

        [TestMethod]
        public void TestLogging()
        {
            var listener = new TestTraceListener();
            Trace.Listeners.Add(listener);

            var logger = Common.ServiceProvider.GetService<ILogger>();
            logger.Log("No severity test");
            logger.Log("Information test");
            logger.Log("Warning test", Severity.Warning);
            logger.Log("Error test", Severity.Error);

            Assert.IsTrue(listener.MessageCount == 4);
        }

        /// <summary>
        /// Tests non query sql statements.
        /// </summary>
        [TestMethod]
        public void TestSqlConnectionExecuteNonQuery()
        {
            using (var connection = new EntityConnection(Common.ServiceProvider
               .GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.First().ConnectionString).StoreConnection)
            {
                connection.Execute("DECLARE @t table(x int)");
            }
        }

        /// <summary>
        /// Tests sql statements that return datatables.
        /// </summary>
        [TestMethod]
        public void TestSqlConnectionExecuteDataTable()
        {
            using (var connection = new EntityConnection(Common.ServiceProvider
               .GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.First().ConnectionString).StoreConnection)
            {
                var dt = connection.Execute<DataTable>("SELECT * FROM sys.tables");
                Assert.IsTrue(dt.Rows.Count > 0 && dt.Columns.Count > 0);
            }
        }

        /// <summary>
        /// Tests sql statements that returns scalar values.
        /// </summary>
        [TestMethod]
        public void TestSqlConnectionExecuteScalar()
        {
            using (var connection = new EntityConnection(Common.ServiceProvider
               .GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.First().ConnectionString).StoreConnection)
            {
                var count = connection.Execute<int>("SELECT COUNT(*) FROM sys.tables");
                Assert.IsTrue(count > 0);
            }
        }

        private class TestTraceListener : TraceListener
        {
            public int MessageCount;

            private readonly StringBuilder sb = new StringBuilder();

            public override void Write(string message)
            {
                sb.Append(message);
            }

            public override void WriteLine(string message)
            {
                sb.AppendLine(message);
                MessageCount++;
            }
        }

        private class SimpleObjectMetadata
        {
            [System.ComponentModel.Description("Created"), UsedImplicitly]
            public DateTime CreatedDateTime { get; set; }
        }

        [MetadataType(typeof(SimpleObjectMetadata))]
        private class SimpleObject
        {
            [UsedImplicitly]
            public DateTime CreatedDateTime { get; set; }
        }
    }
}
