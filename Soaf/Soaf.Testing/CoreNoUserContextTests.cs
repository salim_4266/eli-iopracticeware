﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Reflection;
using Soaf.Threading;
using Soaf.Web.Client;

namespace Soaf.Testing
{
    [TestClass]
    public class CoreNoUserContextTests
    {
        [TestInitialize]
        public void OnTestInitialize()
        {
            Monitor.Enter(Common.SyncRoot);
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            Common.ServiceProvider.GetService<ICache>().Clear();
            Monitor.Exit(Common.SyncRoot);
        }

        [TestMethod]
        public void TestCloningOfPrimitiveCollections()
        {
            var source = new List<Guid> {Guid.NewGuid(), Guid.NewGuid()};
            var cloner = Common.ServiceProvider.GetService<ICloner>();
            var cloned = (IList<Guid>)cloner.Clone(source.Where(s => s != null), typeof(IEnumerable<Guid>), typeof(IEnumerable<Guid>));
            Assert.IsTrue(source.SequenceEqual(cloned));
        }

        [TestMethod]
        public void TestCloningOfViewModels()
        {
            ServiceModel.AddKnowntype(typeof(List<CloneTestViewModel>));
            ServiceModel.AddKnowntype(typeof(List<CloneTestNoDependencyViewModel>));
            ServiceProvider.Current.GetService<CloneTestViewModel>();
            ServiceProvider.Current.GetService<CloneTestNoDependencyViewModel>();

            // Get basic cloning speed metrics
            var baseCloningSource = new List<CloneTestNoDependencyViewModel>();
            for (int i = 0; i < 10000; i++)
            {
                baseCloningSource.Add(new CloneTestNoDependencyViewModel { TestStringProperty = "Test String " + i });
            }
            var baseCloningStart = DateTime.Now;
            baseCloningSource.Clone();
            var baseCloningSpeed = DateTime.Now - baseCloningStart;

            // Get metrics for cloning with dependency properties injection
            var source = new List<CloneTestViewModel>();
            for (int i = 0; i < 10000; i++)
            {
                source.Add(new CloneTestViewModel {CollectionProperty = new ExtendedObservableCollection<int>(), TestStringProperty = "Test String " + i});
            }
            var start = DateTime.Now;
            var cloned = source.Clone();
            var elapsed = DateTime.Now - start;
            Assert.AreEqual(10000, cloned.Count);
            Assert.AreSame(typeof(ExtendedObservableCollection<int>), cloned[100].CollectionProperty.GetType());
            Assert.AreSame(typeof(ExtendedObservableCollection<int>), cloned[100].CollectionProperty2.GetType());
            Assert.AreSame(typeof(Collection<int>), cloned[100].CollectionProperty3.GetType());
            Assert.AreSame(typeof(List<int>), cloned[100].CollectionProperty4.GetType());
            Assert.AreSame(typeof(List<int>), cloned[100].CollectionProperty5.GetType());

            var dependencyInjectionTimePerProperty = (elapsed - baseCloningSpeed).TotalMilliseconds / 5;

            Assert.IsTrue(dependencyInjectionTimePerProperty <= 350, "Dependency injection must be below 350ms per 10000 items. Current " + dependencyInjectionTimePerProperty);
        }

        [TestMethod]
        public void TestWaitExtensionMethodPerformance()
        {
            var start = DateTime.Now;
            for (int i = 0; i < 50; i++)
            {
                int signalReadyIn = 3;
                Tasks.Wait(() => --signalReadyIn == 0, TimeSpan.FromMilliseconds(500));
            }
            var elapsed = DateTime.Now - start;
            Assert.IsTrue(elapsed <= TimeSpan.FromMilliseconds(50 * 11 * 3), "Wait delay increased. New value: " + elapsed);
        }

        [TestMethod]
        public void TestLimitedConcurrencyScheduler()
        {
            const int activeTaskSize = 4;
            const int tasksCount = 20;
            
            var executedTasks = new ConcurrentBag<int>();
            var onCompleteExecutions = new ConcurrentBag<int>();
            var taskQueue = new TaskQueue(activeTaskSize, 2000);
            
            var completionHandles = new WaitHandle[tasksCount];
            for (int i = 0; i < tasksCount; i++)
            {
                var manualResetEvent = new ManualResetEvent(false);
                completionHandles[i] = manualResetEvent;
                int taskIndex = i;
                taskQueue.Enqueue(() =>
                {
                    executedTasks.Add(Thread.CurrentThread.ManagedThreadId);
                    Thread.Sleep(100);
                    
                }, () =>
                {
                    onCompleteExecutions.Add(taskIndex);
                    manualResetEvent.Set();
                });
            }

            Task.Factory.StartNew(() =>
            {
                WaitHandle.WaitAll(completionHandles);
            }).Wait();


            Assert.AreEqual(activeTaskSize, executedTasks.Distinct().Count(), "Expected to run in parallel on at least 4 different threads");
            Assert.AreEqual(tasksCount, onCompleteExecutions.Distinct().Count());
        }

        [TestMethod]
        public void TestSynchronizedDictionaryPerformance()
        {
            const int maxParallelism = 4;
            var target = new SynchronizedDictionary<int, string>();
            var expected = new ConcurrentDictionary<int, string>();

            // Warm up
            target.GetOrAddValue(-10000, v => "TestMe");
            expected.GetOrAdd(-10000, v => "TestMe");

            var start = DateTime.Now;
            Enumerable.Range(0, 10000)
                .AsParallel()
                .WithDegreeOfParallelism(maxParallelism)
                .ForAll(i =>
                {
                    var val1 = target.GetOrAddValue(10000, v => "TestMe");
                    var val2 = target.GetOrAddValue(i, v => "TestMe");
                    Assert.AreEqual(val1, val2);
                });
            var elapsedTarget = DateTime.Now - start;
            
            start = DateTime.Now;
            Enumerable.Range(0, 10000)
                .AsParallel()
                .WithDegreeOfParallelism(maxParallelism)
                .ForAll(i =>
                {
                    var val1 = expected.GetOrAdd(10000, v => "TestMe");
                    var val2 = expected.GetOrAdd(i, v => "TestMe");
                    Assert.AreEqual(val1, val2);
                });
            var elapsedExample = DateTime.Now - start;

            var slowness = elapsedTarget.TotalMilliseconds / Math.Max(elapsedExample.TotalMilliseconds, 1);
            Assert.IsTrue(slowness > 2 && slowness <= 100, "Not expected slowness: " + slowness);
        }

        [TestMethod]
        public void TestRefreshableCachedValue()
        {
            var testSubject = new RefreshableCachedValue<int>();
            var staticDependency = new object();
            const int maxParallelism = 4;
            testSubject.GetOrRefreshValue(() => maxParallelism, staticDependency);

            var start = DateTime.Now;
            for (int i = 0; i < 10000; i++)
            {
                testSubject.GetOrRefreshValue(() => maxParallelism, staticDependency);
            }
            var elapsed = DateTime.Now - start;
            Assert.IsTrue(elapsed <= TimeSpan.FromMilliseconds(250), "Retrieving cached value should be very fast. Current time:" + elapsed);

            Enumerable.Range(0, 1000)
                .AsParallel()
                .WithDegreeOfParallelism(maxParallelism)
                .ForAll(i =>
                {
                    // Check we get right value with static dependency
                    var value = testSubject.GetOrRefreshValue(() =>
                    {
                        throw new InvalidOperationException("Value refresh is not expected");
                    }, staticDependency);
                    Assert.AreEqual(maxParallelism, value);

                    // Check we get right value with changed dependency
                    var changedDependency = new object();
                    value = testSubject.GetOrRefreshValue(() => i, "I'm static", changedDependency);
                    Assert.AreEqual(i, value);
                    
                    // Check new value is not retrieved when already cached
                    value = testSubject.GetOrRefreshValue(() =>
                    {
                        throw new InvalidOperationException("Value refresh is not expected");
                    }, "I'm static", changedDependency);
                    Assert.AreEqual(i, value);
                });

            // Disposing instance should drop the associated cache
            testSubject.Dispose();
            var called = false;
            testSubject.GetOrRefreshValue(() =>
            {
                called = true;
                return maxParallelism;
            }, staticDependency);

            Assert.IsTrue(called);
        }

        [DataContract]
        public class CloneTestViewModel : IViewModel
        {
            [DataMember] 
            public virtual string TestStringProperty { get; set; }

            [DataMember]
            public virtual string TestStringProperty1 { get; set; }

            [DataMember]
            public virtual string TestStringProperty2 { get; set; }

            [DataMember, Dependency]
            public virtual ObservableCollection<int> CollectionProperty { get; set; }

            [DataMember, Dependency]
            public virtual ExtendedObservableCollection<int> CollectionProperty2 { get; set; }

            [DataMember, Dependency]
            public virtual Collection<int> CollectionProperty3 { get; set; }

            [DataMember, Dependency]
            public virtual IList<int> CollectionProperty4 { get; set; }

            [DataMember, Dependency]
            public virtual List<int> CollectionProperty5 { get; set; }
        }

        [DataContract]
        public class CloneTestNoDependencyViewModel : IViewModel
        {
            [DataMember]
            public virtual string TestStringProperty { get; set; }

            [DataMember]
            public virtual string TestStringProperty1 { get; set; }

            [DataMember]
            public virtual string TestStringProperty2 { get; set; }
        }
    }
}
