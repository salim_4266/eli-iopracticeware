﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Presentation;
using Soaf.Security;
using Soaf.Testing;
using Soaf.Threading;
using TestApplication1.Model;

[assembly: Component(typeof(TestObjectWithUnitOfWork), typeof(ITestObjectWithUnitOfWork))]
[assembly: Component(typeof(MultipleInterfaceClass), typeof(IInterface1), typeof(IInterface2))]
[assembly: Component(typeof(TestContactService))]
[assembly: Component(typeof(ContactToContactViewModelMap), AddAllServices = true)]

namespace Soaf.Testing
{
    public interface ITestContactService
    {
        IEnumerable<ContactViewModel> GetContacts();

        IEnumerable<ContactViewModel> GetContacts(int id);

        IEnumerable<ContactViewModel> GetContacts(IEnumerable<ContactServiceFilter> filters);
    }

    public class TestContactService : ITestContactService
    {
        private readonly Func<ContactViewModel> createContact;

        public TestContactService(Func<ContactViewModel> createContact)
        {
            this.createContact = createContact;
        }

        public virtual IEnumerable<ContactViewModel> GetContacts()
        {
            var results = new List<ContactViewModel>();
            for (int i = 0; i < 1000; i++)
            {
                results.Add(CreateContact(i));
            }
            return results;
        }

        public virtual IEnumerable<ContactViewModel> GetContacts(int id)
        {
            return GetContacts();
        }

        public IEnumerable<ContactViewModel> GetContacts(IEnumerable<ContactServiceFilter> filters)
        {
            return GetContacts().Where(c => filters.Any(f => f.NameFilter == c.Name));
        }

        private ContactViewModel CreateContact(int id)
        {
            var contact = createContact();
            contact.Name = "Contact" + id;
            contact.FirstName = "John";
            contact.LastName = "Smith";
            contact.Street = "291 Broadyway";
            contact.City = "New York";
            contact.State = "NY";
            contact.ZipCode = "10007";
            contact.StateViewModel.Name = "Hawaii";

            return contact;
        }
    }

    [DataContract]
    public class ContactServiceFilter
    {
        [DataMember]
        public virtual string NameFilter { get; set; }
    }

    [ScopedLifestyle]
    public class ScopedOrTransientType
    {
    }

    [ScopedLifestyle(LifestyleType.Singleton)]
    public class ScopedOrSingletonType
    {
    }

    [CreationContextLifestyle]
    public class BoundToCreationContextObject
    {
        [Dependency]
        public BoundToCreationContextObject2 Child { get; set; }
    }

    [CreationContextLifestyle]
    public class BoundToCreationContextObject2
    {
    }

    public class BoundToCreationContextObjectOwner
    {
        public BoundToCreationContextObject BoundToCreationContextObject { get; set; }
        public BoundToCreationContextObject2 BoundToCreationContextObject2 { get; set; }

        public BoundToCreationContextObjectOwner(BoundToCreationContextObject boundToCreationContextObject, BoundToCreationContextObject2 boundToCreationContextObject2)
        {
            BoundToCreationContextObject = boundToCreationContextObject;
            BoundToCreationContextObject2 = boundToCreationContextObject2;
        }
    }

    /// <summary>
    /// A test map for Contact to ContactViewModel.
    /// </summary>
    public class ContactToContactViewModelMap : IMap<Contact, ContactViewModel>
    {
        private readonly IMapMember<Contact, ContactViewModel>[] members;

        public ContactToContactViewModelMap()
        {

            members = this.CreateMembers(c => new ContactViewModel
                                                  {
                                                      StateViewModel = FilterAddress(c.Addresses)
                                                          .Select(a => Mapper.Factory.CreateMapper<Address, StateViewModel>(i =>
                                                                                                                            new StateViewModel
                                                                                                                                {
                                                                                                                                    Name = i.State
                                                                                                                                }).Map(a)).FirstOrDefault(),
                                                      LastName = c.LastName,
                                                      Street = FilterAddress(c.Addresses).Select(a => a.Line1).FirstOrDefault(),
                                                      City = FilterAddress(c.Addresses).Select(a => a.City).FirstOrDefault(),
                                                      State = FilterAddress(c.Addresses).Select(a => a.State).FirstOrDefault(),
                                                      ZipCode = FilterAddress(c.Addresses).Select(a => a.ZipCode).FirstOrDefault(),
                                                  }, true).Concat(
                                                      new[] { this.CreateMember(c => c.FirstName, cvm => cvm.FirstName, true) }).ToArray();
        }

        private static IEnumerable<Address> FilterAddress(IEnumerable<Address> source)
        {
            return source.Where(i => i.State == "NY");
        }

        public IEnumerable<IMapMember<Contact, ContactViewModel>> Members
        {
            get { return members; }
        }
    }

    /// <summary>
    /// A test map for ContactViewModel to Contact.
    /// </summary>
    public class ContactViewModelToContactMap : IMap<ContactViewModel, Contact>
    {
        private readonly IMapMember<ContactViewModel, Contact>[] members;

        public ContactViewModelToContactMap()
        {
            members = this
                .CreateMembers(cvm =>
                               new Contact
                               {
                                   FirstName = cvm.FirstName,
                                   LastName = cvm.LastName,
                               }).Concat(new[]
                                                 {
                                                     this.CreateMember(cvm => cvm.State, c => GetNYAddress(c).State),
                                                     this.CreateMember(cvm => cvm.Street, c => GetNYAddress(c).Line1),
                                                     this.CreateMember(cvm => cvm.City, c => GetNYAddress(c).City),
                                                     this.CreateMember(cvm => cvm.ZipCode, c => GetNYAddress(c).ZipCode)
                                                 })
                .ToArray();
        }

        private static Address GetNYAddress(Contact contact)
        {
            if (contact.Addresses == null) contact.Addresses = new List<Address>();

            var address = contact.Addresses.FirstOrDefault(a => a.State == "NY");
            if (address == null)
            {
                address = new Address();
                contact.Addresses.Add(address);
            }
            return address;
        }

        public IEnumerable<IMapMember<ContactViewModel, Contact>> Members
        {
            get { return members; }
        }
    }

    [DataContract]
    public class BaseViewModel
    {
        [DataMember]
        public string Name { get; set; }
    }

    [Description("FirstName")]
    [SupportsNotifyPropertyChanged(true, true)]
    [DataContract]
    public class ContactViewModel : BaseViewModel
    {
        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [Description("Street")]
        [DataMember]
        public string Street { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        [Dependency]
        public StateViewModel StateViewModel { get; set; }
    }


    [SupportsNotifyPropertyChanged]
    [DataContract]
    public class StateViewModel
    {
        [DataMember]
        public string Name { get; set; }
    }

    public interface ITestObjectWithUnitOfWork
    {
        void Test(out IUnitOfWork work1, out IUnitOfWork work2);
    }

    [SupportsUnitOfWork]
    public abstract class TestObjectWithUnitOfWork : ITestObjectWithUnitOfWork
    {
        private readonly IUnitOfWorkProvider unitOfWorkProvider;

        public TestObjectWithUnitOfWork(IUnitOfWorkProvider unitOfWorkProvider)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
        }

        /// <summary>
        /// Tests the specified work1.
        /// </summary>
        /// <param name="work1">The work1.</param>
        /// <param name="work2">The work2.</param>
        [UnitOfWork(CreateNew = true)]
        public virtual void Test(out IUnitOfWork work1, out IUnitOfWork work2)
        {
            work1 = unitOfWorkProvider.Current;

            work2 = unitOfWorkProvider.GetUnitOfWork();
        }
    }

    public class TestMessage
    {
        public Guid Id = Guid.NewGuid();
    }

    [SupportsAuthorization]
    [AuthorizeRoleOrName(Roles = new[] { "User" })]
    public abstract class AuthorizingModel
    {
        [AuthorizeRoleOrName(Roles = new[] { "Administrator" })]
        public virtual void DoAdminWork()
        {
        }

        [AuthorizeRoleOrName(Roles = new[] { "Administrator" }, IdentityNames = new[] { "System" }, UnauthorizedBehavior = UnauthorizedBehavior.SkipInvocation)]
        public virtual string DoAdminOrSystemWorkWithReturn()
        {
            return "Hello";
        }
    }

    public interface ITest
    {
        void Do();
    }

    public class MissingPropertyDependencyTestObject
    {
        [Dependency]
        public ITest Test { get; set; }
    }

    public class OptionalConstructorDependencyObject
    {
        public ITest Test { get; set; }

        public OptionalConstructorDependencyObject(ITest test = null)
        {
            Test = test;
        }
    }

    [SupportsSynchronization]
    public class TestSynchronizableObject
    {
        public bool SynchronizedSuccessfully { get; private set; }

        [Synchronized]
        public virtual object SynchronizedProperty
        {
            get
            {
                var t = new Thread(() =>
                                       {
                                           if (!Monitor.TryEnter(typeof(TestSynchronizableObject).GetProperty("SynchronizedProperty").GetGetMethod(true)))
                                           {
                                               SynchronizedSuccessfully = true;
                                           }
                                       });

                t.Start();

                t.Join();

                return null;
            }
        }
    }

    [AttributeUsage(AttributeTargets.All, Inherited = false)]
    public class NonInheritedTestAttribute : Attribute
    {
    }

    [SupportsValidation]
    public class ValidatingObject
    {
        [Required]
        public virtual string ValidatingProperty1 { get; set; }

        public virtual void ValidatingMethod1([Required] string value)
        {
        }

        public virtual int ValidatingMethod2([Range(0, 1)] int value)
        {
            return value;
        }
    }


    [SupportsDataErrorInfo]
    public class DataErrorInfoObject
    {
        [Required]
        public virtual string ValidatingProperty1 { get; set; }

        public virtual void ValidatingMethod1([Required] string value)
        {
        }

        public virtual int ValidatingMethod2([Range(0, 1)] int value)
        {
            return value;
        }
    }

    [SupportsCaching]
    public class CachingObject
    {
        [Cache]
        public virtual object CachingProperty
        {
            get { return new object(); }
        }

        public virtual object NonCachingProperty
        {
            get { return new object(); }
        }

        [Cache]
        public virtual object CachingMethod()
        {
            return new object();
        }

        [Cache]
        public virtual object GenericCachingMethod<T>(T input) where T : new()
        {
            return new object();
        }

        [Cache]
        // ReSharper disable RedundantAssignment
        public virtual object CachingMethodWithParameters(string value, ref object refArguments, out object outArgument)
        // ReSharper restore RedundantAssignment
        {
            refArguments = new object();

            outArgument = new object();

            return new object();
        }
    }

    public class ChildCachingObject : CachingObject
    {
        [Cache]
        public virtual object ChildCachingMethod()
        {
            return new object();
        }
    }

    [SupportsCaching]
    public class NonVirtualInvalidCachingObject
    {
        [Cache]
        public object CachingProperty
        {
            get { return new object(); }
        }
    }

    [SupportsCaching]
    public class NotAccessibleInvalidCachingObject
    {
        [Cache]
        // ReSharper disable UnusedMember.Local
        protected object CachingProperty
        {
            get { return new object(); }
        }

        // ReSharper restore UnusedMember.Local
    }

    [Singleton]
    public class InitializableObject : IInitializable
    {
        public bool IsInitialized { get; private set; }

        #region IInitializable Members

        public virtual void Initialize()
        {
            if (IsInitialized)
            {
                throw new InvalidOperationException();
            }
            IsInitialized = true;
        }

        #endregion
    }

    [SupportsCaching]
    [NonInheritedTest]
    public interface IInheritedAttributeInterface
    {
        [Cache]
        [NonInheritedTest]
        void Execute();
    }

    [Singleton]
    public class InheritedAttributeObject : IInheritedAttributeInterface
    {
        #region IInheritedAttributeInterface Members

        public void Execute()
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public interface IInterface1 { }

    public interface IInterface2 { }

    [Singleton]
    public class MultipleInterfaceClass : IInterface1, IInterface2
    {

    }

    [SupportsDefaults]
    public class HasDefaultsObject
    {
        [DefaultValue("Hello")]
        public string StringValue { get; set; }

        [DefaultValue(1)]
        public int IntValue { get; set; }

        [DefaultValue(null)]
        public int IntValue2 { get; set; }

        [DefaultValue(null)]
        public List<object> ObjectList { get; set; }
    }

    [SupportsValidation]
    public class DisposableObject : IDisposable
    {
        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            IsDisposed = true;
        }
    }

    [Singleton]
    [Disposable]
    public class DisposableSingleton : IDisposable
    {
        public virtual void Dispose()
        {
        }
    }

    [ScopedLifestyle]
    public class ScopedObject : IDisposable
    {
        public virtual void Dispose()
        {
        }
    }

    [CallContextLifestyle]
    [Disposable]
    public class DisposablePerThreadObject : IDisposable
    {
        public virtual void Dispose()
        {
        }
    }

    [CallContextLifestyle]
    public class PerThreadObject
    {
    }

    [SupportsCaching]
    [SupportsValidation]
    public class SimpleObject : IIsReadOnly
    {
        [Cache]
        public virtual object Value { get; set; }

        public virtual object Value2 { get; set; }

        public bool IsReadOnly { get; set; }
    }

    [SupportsNotifyPropertyChanged]
    public class NotifyingObject
    {
        private string property2;

        [NotifyPropertyChanged]
        public virtual string Property1 { get; set; }

        [DependsOn("Property1")]
        public virtual string Property2
        {
            get { return property2 + Property1; }
            set { property2 = value; }
        }
    }

    [SupportsNotifyPropertyChanged(true)]
    [DataContract]
    public class NotifyingAnyPropertyChangedObject : INotifyPropertyChanged
    {
        [DataMember]
        public virtual string Property1 { get; set; }

        [DataMember]
        public virtual string Property2 { get; set; }

        #region INotifyPropertyChanged Members

        public virtual event PropertyChangedEventHandler PropertyChanged = delegate { };

        #endregion
    }

    [SupportsNotifyPropertyChanged(true, true)]
    [DataContract]
    public class NotifyingObjectWithChild
    {
        private string dependentProperty;
        private string dependsOnRegularProperty;

        public NotifyingObjectWithChild()
        {
            Children = new ExtendedObservableCollection<NotifyingAnyPropertyChangedObject>();
        }

        [DataMember]
        [Dependency]
        public virtual NotifyingAnyPropertyChangedObject Child { get; set; }

        [DataMember]
        public virtual ObservableCollection<NotifyingAnyPropertyChangedObject> Children { get; set; }

        [DependsOn("Child.Property1")]
        public virtual string DependentProperty
        {
            get { return dependentProperty + Child.IfNotNull(c => c.Property1); }
            set { dependentProperty = value; }
        }

        [DependsOn("RegularProperty")]
        public virtual string DependsOnRegularProperty
        {
            get { return dependsOnRegularProperty + RegularProperty; }
            set { dependsOnRegularProperty = value; }
        }

        [DataMember]
        public virtual string RegularProperty { get; set; }

        [DependsOn("IgnoredProperty")]
        public virtual string DependsOnIgnoredProperty 
        {
            get { return IgnoredProperty + "_IDependOnIt"; }
        }

        [DataMember]
        [IgnorePropertyChanges]
        public virtual string IgnoredProperty { get; set; }
    }

    [SupportsNotifyPropertyChanged(true)]
    [DataContract]
    public class NotifyingObjectWithChildDependency
    {
        private string dependentProperty;
        private string dependentProperty2;

        public NotifyingObjectWithChildDependency()
        {
            Children = new ExtendedObservableCollection<NotifyingAnyPropertyChangedObject>();
        }

        [DataMember]
        [Dependency]
        public virtual NotifyingAnyPropertyChangedObject Child { get; set; }

        [DataMember]
        public virtual ObservableCollection<NotifyingAnyPropertyChangedObject> Children { get; set; }

        [DependsOn("Child.Property1")]
        public string DependentProperty
        {
            get { return dependentProperty + Child.IfNotNull(c => c.Property1); }
            set { dependentProperty = value; }
        }

        [DependsOn("Child.Property2")]
        public string DependentProperty2
        {
            get { return dependentProperty2 + Child.IfNotNull(c => c.Property2); }
            set { dependentProperty2 = value; }
        }
    }

    [EditableObject]
    public class EditableObject
    {
        public EditableObject()
        {
            property1 = "Hello1";
            property2 = "Hello2";
        }

        private string property1;
        public virtual string Property1
        {
            get { return property1; }
            set { property1 = value; }
        }

        private string property2;
        public virtual string Property2
        {
            get { return property2; }
            set { property2 = value; }
        }

        [IgnoreChangeTracking]
        public virtual EditableObject NotTrackedProperty { get; set; }
    }

    public class TestViewContext : IViewContext
    {
        [DependsOn("ViewModel.Children.Item.Name")]
        public virtual string CalculatedProperty
        {
            get { return ViewModel.Children.Select(c => c.Name).Join(); }
        }

        [Dependency]
        public virtual TestViewModel ViewModel { get; set; }

        public IInteractionContext InteractionContext { get; set; }
    }

    public class TestViewModel : IViewModel
    {
        public virtual string Name { get; set; }

        [DependsOn("Name")]
        public virtual string CalculatedProperty1 { get { return Name + "_Calc"; } }

        [DependsOn("Name")]
        public virtual string CalculatedProperty2 { get { return Name + "_Calc"; } }

        [Dependency]
        public virtual ObservableCollection<TestChildViewModel> Children { get; set; }
    }

    public class TestChildViewModel : IViewModel
    {
        public virtual string Name { get; set; }
    }

    public class TestWeakDelegateCanRemove
    {
        public event EventHandler TestEvent;

        public void FireTestEvent()
        {
            if (TestEvent != null)
            {
                TestEvent(this, EventArgs.Empty);
            }
        }

        public int GetTestEventSubscribersCount()
        {
            return TestEvent == null ? 0 : TestEvent.GetInvocationList().Length;
        }

        public void ShouldExecute(object sender, EventArgs e)
        {

        }

        public void ShouldNotExecute(object sender, EventArgs e)
        {
            throw new InvalidOperationException("This should not execute");
        }
    }

    [DataContract]
    public class TestICollectionXmlDeserialization
    {
        [DataMember]
        public ICollection<string> CollectionProperty { get; set; }
    }

    public class AllowSingleMethodCallObject
    {
        private bool calledOnce;
        private static readonly Random RandomGen = new Random();

        public int GetRandomNumber(string arg1, Guid arg2, Tuple<string, int> arg3)
        {
            if (calledOnce) throw new InvalidOperationException("Can be called only once");
            calledOnce = true;

            return RandomGen.Next();
        }
    }

    public static class TestModelExtensions
    {
        public static int GetRandomNumberWithExtension(this AllowSingleMethodCallObject target, string arg1)
        {
            return target.GetRandomNumber(arg1, Guid.Empty, null);
        }
    }
}