﻿using System;
using System.Linq;
using System.Threading;
using System.Transactions;
using System.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Caching;
using Soaf.ComponentModel;
using Soaf.Collections;

namespace Soaf.Testing
{
    /// <summary>
    ///This is a test class for RolesTest and is intended
    ///to contain all RolesTest Unit Tests
    ///</summary>
    [TestClass]
    public class RolesTest
    {
        private TransactionScope transactionScope;

        [TestInitialize]
        public void OnTestInitialize()
        {
            Monitor.Enter(Common.SyncRoot);

            PrepAuthentication.CreateUser();
            PrepAuthentication.LogIn("ServiceTests", "ServiceTests");

            // force Core.Extended assembly reference
// ReSharper disable once ObjectCreationAsStatement
            new Security.MembershipProvider();

            if (Transaction.Current != null || Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Current != null) throw new InvalidOperationException();
            transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10));
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            transactionScope.Dispose();
            Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Current.IfNotNull(c => c.Dispose());
            Common.ServiceProvider.GetService<ICache>().Clear();
            
            PrepAuthentication.LogOut();
            PrepAuthentication.DeleteUser();

            Monitor.Exit(Common.SyncRoot);
        }

        private static MembershipUser CreateUser(string username = null)
        {
            if (username == null) username = "test{0}@test.com".FormatWith(new Random().Next());

            string password = "Password1";
            string email = "test@test.com";
            string passwordQuestion = "What is your mother's maiden name?";
            string passwordAnswer = "Johnson";
            MembershipCreateStatus status;
            MembershipCreateStatus statusExpected = MembershipCreateStatus.Success;
            MembershipUser createdUser = Membership.CreateUser(username, password, email, passwordQuestion, passwordAnswer, true, Guid.NewGuid(), out status);
            Assert.AreEqual(statusExpected, status);
            return createdUser;
        }

        /// <summary>
        ///A test for AddUserToRole
        ///</summary>
        [TestMethod]
        public void AddUserToRoleTest()
        {
            MembershipUser user = CreateUser();
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Assert.IsNotNull(user.UserName);
            Roles.AddUserToRole(user.UserName, roleName);
            Assert.IsTrue(Roles.IsUserInRole(user.UserName, roleName));
        }

        /// <summary>
        ///A test for AddUserToRoles
        ///</summary>
        [TestMethod]
        public void AddUserToRolesTest()
        {
            MembershipUser user = CreateUser();
            var roleNames = new[] { "Test Role 1", "Test Role 2" };
            roleNames.ForEach(r => Roles.CreateRole(r));
            Roles.AddUserToRoles(user.UserName, roleNames);
            roleNames.ForEach(r => Assert.IsTrue(Roles.GetRolesForUser(user.UserName).Contains(r)));
        }

        /// <summary>
        ///A test for AddUsersToRole
        ///</summary>
        [TestMethod]
        public void AddUsersToRoleTest()
        {
            MembershipUser user1 = CreateUser();
            MembershipUser user2 = CreateUser("test2@test.com");
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Roles.AddUsersToRole(new[] { user1, user2 }.Select(i => i.UserName).ToArray(), roleName);
            Assert.IsTrue(Roles.IsUserInRole(user1.UserName, roleName));
            Assert.IsTrue(Roles.IsUserInRole(user2.UserName, roleName));
        }

        /// <summary>
        ///A test for AddUsersToRoles
        ///</summary>
        [TestMethod]
        public void AddUsersToRolesTest()
        {
            MembershipUser user1 = CreateUser();
            MembershipUser user2 = CreateUser("test2@test.com");
            var roleNames = new[] { "Test Role 1", "Test Role 2" };
            roleNames.ForEach(r => Roles.CreateRole(r));
            Roles.AddUsersToRoles(new[] { user1, user2 }.Select(i => i.UserName).ToArray(), roleNames);
            roleNames.ForEach(r => new[] { user1, user2 }.ForEach(u => Assert.IsTrue(Roles.IsUserInRole(u.UserName, r))));
        }

        /// <summary>
        ///A test for CreateRole
        ///</summary>
        [TestMethod]
        public void CreateRoleTest()
        {
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Assert.IsTrue(Roles.RoleExists(roleName));
        }

        /// <summary>
        ///A test for DeleteRole
        ///</summary>
        [TestMethod]
        public void DeleteRoleTest()
        {
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Assert.IsTrue(Roles.RoleExists(roleName));
            Roles.DeleteRole(roleName);
            Assert.IsFalse(Roles.RoleExists(roleName));
        }

        /// <summary>
        ///A test for DeleteRole
        ///</summary>
        [TestMethod]
        public void DeletePopulatedRoleTest()
        {
            MembershipUser user = CreateUser();
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Assert.IsNotNull(user.UserName);
            Roles.AddUserToRole(user.UserName, roleName);
            try
            {
                Roles.DeleteRole(roleName, true);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(InvalidOperationException));
            }
        }

        /// <summary>
        ///A test for FindUsersInRole
        ///</summary>
        [TestMethod]
        public void FindUsersInRoleTest()
        {
            MembershipUser user = CreateUser();
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Roles.AddUserToRole(user.UserName, roleName);
            string[] usersInRole = Roles.FindUsersInRole(roleName, user.UserName.Substring(1, user.UserName.Length - 3).ToLower());
            Assert.IsTrue(usersInRole.Length == 1);
        }

        /// <summary>
        ///A test for GetAllRoles
        ///</summary>
        [TestMethod]
        public void GetAllRolesTest()
        {
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Assert.IsTrue(Roles.GetAllRoles().Single() == roleName);
        }

        /// <summary>
        ///A test for GetRolesForUser
        ///</summary>
        [TestMethod]
        public void GetRolesForUserTest()
        {
            MembershipUser user = CreateUser();
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Roles.AddUserToRole(user.UserName, roleName);
            Assert.IsTrue(Roles.GetRolesForUser(user.UserName).Single() == roleName);
        }

        /// <summary>
        ///A test for GetUsersInRole
        ///</summary>
        [TestMethod]
        public void GetUsersInRoleTest()
        {
            MembershipUser user = CreateUser();
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Assert.IsNotNull(user.UserName);
            Roles.AddUserToRole(user.UserName, roleName);
            string[] usersInRole = Roles.GetUsersInRole(roleName);
            Assert.IsTrue(usersInRole.Single() == user.UserName);
        }


        /// <summary>
        ///A test for RemoveUserFromRole
        ///</summary>
        [TestMethod]
        public void RemoveUserFromRoleTest()
        {
            MembershipUser user = CreateUser();
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Roles.AddUserToRole(user.UserName, roleName);
            Assert.IsTrue(Roles.IsUserInRole(user.UserName, roleName));
            Roles.RemoveUserFromRole(user.UserName, roleName);
            Assert.IsFalse(Roles.IsUserInRole(user.UserName, roleName));
        }

        /// <summary>
        ///A test for RemoveUserFromRoles
        ///</summary>
        [TestMethod]
        public void RemoveUserFromRolesTest()
        {
            MembershipUser user = CreateUser();
            var roleNames = new[] { "Test Role", "Test Role 2" };
            roleNames.ForEach(r => Roles.CreateRole(r));
            Roles.AddUserToRoles(user.UserName, roleNames);
            roleNames.ForEach(r => Assert.IsTrue(Roles.IsUserInRole(user.UserName, r)));
            Roles.RemoveUserFromRoles(user.UserName, roleNames);
            roleNames.ForEach(r => Assert.IsFalse(Roles.IsUserInRole(user.UserName, r)));
        }

        /// <summary>
        ///A test for RemoveUsersFromRole
        ///</summary>
        [TestMethod]
        public void RemoveUsersFromRoleTest()
        {
            MembershipUser user1 = CreateUser();
            MembershipUser user2 = CreateUser("test2@test.com");
            string roleName = "Test Role";
            Roles.CreateRole(roleName);
            Roles.AddUsersToRole(new[] { user1, user2 }.Select(i => i.UserName).ToArray(), roleName);
            Assert.IsTrue(Roles.IsUserInRole(user1.UserName, roleName));
            Assert.IsTrue(Roles.IsUserInRole(user2.UserName, roleName));
            Roles.RemoveUsersFromRole(new[] { user1, user2 }.Select(i => i.UserName).ToArray(), roleName);
            Assert.IsFalse(Roles.IsUserInRole(user1.UserName, roleName));
            Assert.IsFalse(Roles.IsUserInRole(user2.UserName, roleName));
        }

        /// <summary>
        ///A test for RoleExists
        ///</summary>
        [TestMethod]
        public void RoleExistsTest()
        {
            string roleName = "Test Role";
            if (Roles.RoleExists(roleName))
            {
                Roles.DeleteRole(roleName);
            }
            Roles.CreateRole(roleName);
            Assert.IsTrue(Roles.RoleExists(roleName));
        }
    }
}