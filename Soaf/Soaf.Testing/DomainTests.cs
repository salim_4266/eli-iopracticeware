﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Objects;
using System.Data.Services.Client;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Caching;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Data;
using Soaf.Domain;
using Soaf.IO;
using Soaf.Linq;
using Soaf.Linq.Expressions;
using Soaf.EntityFramework;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Testing.Test1RepositoryService;
using Soaf.Web.Client;
using Soaf.Web.Services;
using TestApplication1.Model;
using TestApplication1.Model.Test1Model;
using TestApplication1.Model.Test2Model;
using System.Transactions;
using Contact = TestApplication1.Model.Contact;
using ObjectStateEntry = Soaf.ComponentModel.ObjectStateEntry;
using Test1Address = TestApplication1.Model.Test1Model.Test1Address;
using Test1Contact = TestApplication1.Model.Test1Model.Test1Contact;
using Test1ContactAddress = TestApplication1.Model.Test1Model.Test1ContactAddress;
using Test1Country = TestApplication1.Model.Test1Model.Test1Country;
using Test1Repository = TestApplication1.Model.Test1Model.Test1Repository;
using Test1State = TestApplication1.Model.Test1Model.Test1State;

namespace Soaf.Testing
{
    [TestClass]
    public class DomainTests
    {
        private TransactionScope transactionScope;

        /// <summary>
        ///   Called when [test initialize]. Creates the test databases.
        /// </summary>
        [TestInitialize]
        public void OnTestInitialize()
        {
            Monitor.Enter(Common.SyncRoot);
            PrepAuthentication.CreateUser();
            PrepAuthentication.LogIn("ServiceTests", "ServiceTests", true);

            if (Transaction.Current != null || Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Current != null) throw new InvalidOperationException();
            transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10));

            var systemPrincipal = new GenericPrincipal(new GenericIdentity("System", true, id: Guid.NewGuid()), "User");
            Thread.CurrentPrincipal = systemPrincipal;
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            transactionScope.Dispose();
            Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Current.IfNotNull(c => c.Dispose());
            Common.ServiceProvider.GetService<ICache>().Clear();

            PrepAuthentication.LogOut();
            PrepAuthentication.DeleteUser();

            Monitor.Exit(Common.SyncRoot);
        }

        /// <summary>
        ///   Tests that query inclusions eager load data correctly.
        /// </summary>
        [TestMethod]
        public void TestQueryInclusions()
        {
            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf =>
            {
                ocf.ProxyCreationEnabled = true;
            });

            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.LazyLoadingEnabled = false);

            List<Test2Contact> results = (from contact in Common.ServiceProvider.GetService<ITest2Repository>().Test2Contacts.Include(c => c.ContactAddresses.Select(ca => ca.Address))
                                          where
                                          contact.LastName == "Brennan" && contact.ContactAddresses.Count > 0
                                          select contact).
            ToList();

            Assert.IsNotNull(results);

            Assert.IsTrue(results.Count > 0);

            Assert.IsTrue(ObjectContext.GetKnownProxyTypes().Contains(results[0].GetType()));

            Assert.IsTrue(results[0].ContactAddresses.Count > 0);

            Assert.IsNotNull(results[0].ContactAddresses.First().Address);
        }

        /// <summary>
        ///   Tests expression composition using the And extension method to combine raw lambda expressions.
        /// </summary>
        [TestMethod]
        public void TestExpressionComposition()
        {
            IQueryable<Contact> query = Common.ServiceProvider.GetService<IFederatedTestRepository>().Contacts;

            Expression<Func<Contact, bool>> lastNameCriteria = query.CreatePredicate(c => c.LastName.Contains("Brennan"));
            Expression<Func<Contact, bool>> firstNameCriteria = query.CreatePredicate(c => c.LastName.Contains("d"));

            query = query.Where(lastNameCriteria.And(firstNameCriteria));

            List<Contact> results = query.ToList();

            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count > 0);
        }

        /// <summary>
        /// Tests a query with inline method calls.
        /// </summary>
        [TestMethod]
        public void TestQueryWithInlineMethodCalls()
        {
            IQueryable<Contact> query = Common.ServiceProvider.GetService<ITest1Repository>().Contacts;

            var results1 = query.Where(i => i.FirstName.Contains("a") && HasLastName().Invoke(i, "Brennan")).ToList();
            Assert.IsNotNull(results1);
            Assert.IsTrue(results1.Count > 0);

            var results2 = query.Where(i => i.FirstName.Contains("a") && HasLastName().Compile()(i, "Brennan")).ToList();
            Assert.IsNotNull(results2);
            Assert.IsTrue(results2.Count > 0);

        }

        private Expression<Func<Contact, string, bool>> HasLastName()
        {
            return (c, name) => c.LastName == name;
        }

        /// <summary>
        ///   Tests the method queryable that takes parameters.
        /// </summary>
        [TestMethod]
        public void TestMethodBasedQueryable()
        {
            var query = Common.ServiceProvider.GetService<IFederatedTestRepository>().GetContactInfoByFirstNameAndLastName("d", "Brennan");

            List<Contact> results = query.ToList();

            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count > 0);
        }

        /// <summary>
        ///   Tests predicate expression composition with Or, And, and Not expressions.
        /// </summary>
        [TestMethod]
        public void TestCriteriaBuilder()
        {
            IQueryable<Test2Contact> query = Common.ServiceProvider.GetService<ITest2Repository>().Test2Contacts;

            Expression<Func<Test2Contact, bool>> criteria1 = query.CreatePredicate(c => c.FirstName != "John");
            criteria1 = criteria1.Not();

            Expression<Func<Test2Contact, bool>> criteria2 = query.CreatePredicate(c => c.FirstName != "Joe");
            criteria2 = criteria2.Not();

            List<Test2Contact> resultOr = query.Where(criteria1.Or(criteria2)).ToList();
            Assert.IsTrue(resultOr.Count > 0);

            List<Test2Contact> resultAnd = query.Where(criteria1.And(criteria2)).ToList();
            // can't have two different FirstNames...
            Assert.IsTrue(resultAnd.Count == 0);
        }

        /// <summary>
        ///   Tests that lazy loading without a context doesn't load.
        /// </summary>
        [TestMethod]
        public void TestOutOfContextLazyLoading()
        {
            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf =>
                                                                                                           {
                                                                                                               ocf.LazyLoadingEnabled = true;
                                                                                                               ocf.ProxyCreationEnabled = true;
                                                                                                           });

            Test2Contact contact = Common.ServiceProvider.GetService<ITest2Repository>().Test2Contacts.Where(c => c.ContactAddresses.Count > 0).Take(100).First();
            Assert.IsTrue(contact.ContactAddresses.Count == 0);
        }

        /// <summary>
        ///   Tests that lazy loading in a data context succeeds.
        /// </summary>
        [TestMethod]
        public void TestLazyLoading()
        {
            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWork())
            {
                var repository = Common.ServiceProvider.GetService<ITest2Repository>();
                repository.IsLazyLoadingEnabled = true;
                Test2Contact contact = repository.Test2Contacts.Where(c => c.ContactAddresses.Count > 0).Take(100).First();
                Assert.IsTrue(contact.ContactAddresses.ToArray().Length > 0);
            }
        }

        /// <summary>
        ///   Tests a query based on a stored procedure.
        /// </summary>
        [TestMethod]
        public void TestStoredProcedureQuery()
        {
            List<GetContactsByName_Result> results = Common.ServiceProvider.GetService<ITest2Repository>().GetContactsByName("d", "Brennan").ToList();
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count > 0);
        }


        /// <summary>
        ///   Tests that a proxy entity is returned when resolution is requested for an EF entity type and that a regular non proxy entity is returned when ProxyCreationEnabled is false.
        /// </summary>
        [TestMethod]
        public void TestNewProxyEntityResolution()
        {
            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = true);

            Test1Contact contact = Common.ServiceProvider.GetService<ITest1Repository>().CreateTest1Contact();
            Assert.IsTrue(typeof(Test1Contact) != contact.GetType() && ObjectContext.GetKnownProxyTypes().Contains(contact.GetType()));

            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = false);

            contact = Common.ServiceProvider.GetService<ITest1Repository>().CreateTest1Contact();
            Assert.IsTrue(typeof(Test1Contact) == contact.GetType());
        }

        /// <summary>
        /// Tests a query with no criteria.
        /// </summary>
        [TestMethod]
        public void TestQueryWithNoCriteria()
        {
            var results = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts.ToList();
            Assert.IsTrue(results.Count == 1000);
        }

        /// <summary>
        /// Tests the query with only a Take statement.
        /// </summary>
        [TestMethod]
        public void TestQueryWithTakeOnly()
        {
            var results = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts.Take(100).ToList();
            Assert.IsTrue(results.Count == 100);
        }

        /// <summary>
        ///   Tests saving of new entities.
        /// </summary>
        [TestMethod]
        public void TestSavingExistingEntities()
        {
            Test1Contact contact;

            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = true);

            contact = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts.Include(c => c.ContactAddresses.Select(ca => ca.Address))
                .OrderBy(c => c.Id).Skip(5).First();

            contact.LastName = "ALFKI";
            contact.ContactAddresses.First().Address.City = "NYC";

            Common.ServiceProvider.GetService<ITest1Repository>().Save(contact);

            Test1Contact contactRefetched = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts.OrderBy(c => c.Id).Skip(5).First();
            Assert.IsTrue(contactRefetched.LastName == "ALFKI");
            Assert.IsTrue(contact.ContactAddresses.First().Address.City == "NYC");
        }

        /// <summary>
        ///   Tests saving of new entities.
        /// </summary>
        [TestMethod]
        public void TestSavingNewEntities()
        {
            var contact = new Test1Contact { FirstName = "John", LastName = "ALFKI" };
            var address = new Test1Address { Address1 = "1 Blue Way", City = "Podunk", Country = new Test1Country { Name = "Far Far Away" }, State = new Test1State { Code = "ABC", Name = "The State" } };
            contact.ContactAddresses.Add(new Test1ContactAddress { Address = address });

            Common.ServiceProvider.GetService<ITest1Repository>().Save(contact);

            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = false);

            Test1Contact contactRefetched = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts
                .Include(c => c.ContactAddresses.SelectMany(ca => new object[] { ca.Address.Country, ca.Address.State }))
                .Single(c => c.LastName == "ALFKI");
            Assert.IsNotNull(contactRefetched);

            var comparison = new Objects.ObjectComparison();
            comparison.Compare(contact, contactRefetched);

            Assert.AreNotSame(contact, contactRefetched);

            Assert.IsTrue(comparison.Differences.Count == 0, "Compare failed: " + comparison.DifferencesString);
        }


        /// <summary>
        ///   Tests bulk saving of new entities (SQL Server 2005)
        /// </summary>
        [TestMethod]
        public void TestSavingEntitiesInBulk90()
        {
            TestSavingEntitiesInBulkUsingCompatibility(90);
        }

        /// <summary>
        ///   Tests bulk saving of new entities (SQL Server 2008 and up)
        /// </summary>
        [TestMethod]
        public void TestSavingEntitiesInBulk100AndUp()
        {
            TestSavingEntitiesInBulkUsingCompatibility(100);
        }


        private void TestSavingEntitiesInBulkUsingCompatibility(int compatibilityLevel)
        {
            // We cannot alter database compatibility within transaction
            transactionScope.Dispose();
            
            var result = Common.ServiceProvider.GetService<IAdoService>().ExecuteCommandScalar(new AdoServiceCommandExecutionArgument
            {
                ConnectionString = DataExtensionsTests.ConnectionString,
                CommandText = @"DECLARE @Sql nvarchar(150)
SET @Sql = 'SELECT compatibility_level
FROM sys.databases
WHERE database_id = ' + CONVERT(nvarchar, DB_ID())
EXEC(@Sql)"
            });

            // Can only try lower compatibility level
            var lowerCompatibility = result.Result.CastTo<int>() > compatibilityLevel;

            if (lowerCompatibility)
            {
                Common.ServiceProvider.GetService<IAdoService>().ExecuteCommandNonQuery(new AdoServiceCommandExecutionArgument
                {
                    ConnectionString = DataExtensionsTests.ConnectionString,
                    CommandText = @"DECLARE @Sql nvarchar(150)
SET @Sql = 'ALTER DATABASE ' + DB_NAME() + '
SET COMPATIBILITY_LEVEL = " + compatibilityLevel + @";'
EXEC(@Sql)"
                });
            }

            // Execute within transaction
            transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10));
            TestSavingEntitiesInBulk();
            transactionScope.Dispose();

            if (lowerCompatibility)
            {
                Common.ServiceProvider.GetService<IAdoService>().ExecuteCommandNonQuery(new AdoServiceCommandExecutionArgument
                {
                    ConnectionString = DataExtensionsTests.ConnectionString,
                    CommandText = @"DECLARE @Sql nvarchar(150)
SET @Sql = 'ALTER DATABASE ' + DB_NAME() + '
SET COMPATIBILITY_LEVEL = " + result.Result + @";'
EXEC(@Sql)"
                });
            }
        }

        private void TestSavingEntitiesInBulk()
        {
            var contacts = new List<Test1Contact>();
            IObjectStateEntry[] objectStateEntries;

            using (var work = Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWorkScope())
            {
                var country = new Test1Country { Name = "Far Far Away" };
                var state = new Test1State { Code = "ABC", Name = "The State" };
                for (int i = 0; i < 100; i++)
                {
                    var contact = new Test1Contact { FirstName = i.ToString(), LastName = "ALFKI" };
                    var address1 = new Test1Address { Address1 = "1 Blue Way_1_" + i, City = "Podunk", Country = country, State = state };
                    var address2 = new Test1Address { Address1 = "1 Blue Way", City = "Podunk", Country = country, State = state };
                    contact.ContactAddresses.Add(new Test1ContactAddress { Address = address1 });
                    contact.ContactAddresses.Add(new Test1ContactAddress { Address = address2 });

                    contacts.Add(contact);
                }

                // Test bulk insert
                contacts.ForEach(Common.ServiceProvider.GetService<ITest1Repository>().Save);
                Common.ServiceProvider.GetService<IRepositoryService>().Persist(work.UnitOfWork.ObjectStateEntries.ToArray(), typeof(ITest1Repository).FullName, true);

                // Test bulk update
                foreach (var contact in contacts)
                {
                    contact.ContactAddresses.ElementAt(1).Address.Address1 = "1 Blue Way_2_" + contact.FirstName;
                }
                work.UnitOfWork.ObjectStateEntries.ToArray().ForEach(e => e.State = ObjectState.Modified);
                Common.ServiceProvider.GetService<IRepositoryService>().Persist(work.UnitOfWork.ObjectStateEntries.ToArray(), typeof(ITest1Repository).FullName, true);

                // Get entries for later bulk delete test
                Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = false);
                objectStateEntries = work.UnitOfWork.ObjectStateEntries.Select(e => new ObjectStateEntry(e.Object)).ToArray();
            }

            var ids = contacts.Select(i => i.Id).ToArray();
            var contactsRefetched = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts
                .Include(c => c.ContactAddresses.SelectMany(ca => new object[] { ca.Address.Country, ca.Address.State }))
                .Where(c => ids.Contains(c.Id)).ToArray();

            Assert.AreEqual(100, contactsRefetched.Length);
            Assert.AreEqual(200, contactsRefetched.SelectMany(c => c.ContactAddresses).Count());

            // Ensure all relations were properly setup
            Assert.IsTrue(contactsRefetched
                .SelectMany(c => c.ContactAddresses.Select(ca => new { c.FirstName, ca.Address.Address1 }))
                .GroupBy(ca => ca.FirstName)
                .All(g => g.Count() == 2 && g.Any(ca => ca.Address1.EndsWith("_1_" + g.Key)) && g.Any(ca => ca.Address1.EndsWith("_2_" + g.Key))), "Each contact must have matching addresses");

            // Test bulk delete
            objectStateEntries.ForEach(s => s.State = ObjectState.Deleted);
            Common.ServiceProvider.GetService<IRepositoryService>().Persist(objectStateEntries, typeof(ITest1Repository).FullName, true);

            contactsRefetched = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts
                .Include(c => c.ContactAddresses.SelectMany(ca => new object[] { ca.Address.Country, ca.Address.State }))
                .Where(c => ids.Contains(c.Id)).ToArray();
            Assert.IsTrue(contactsRefetched.Length == 0);
        }

        /// <summary>
        ///   Tests saving of new proxy entities.
        /// </summary>
        [TestMethod]
        public void TestSavingNewProxyEntities()
        {
            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = true);

            Test1Contact contact;
            using (var work = Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWork())
            {
                contact = Common.ServiceProvider.GetService<ITest1Repository>().CreateTest1Contact();
                Assert.IsTrue(typeof(Test1Contact) != contact.GetType() && ObjectContext.GetKnownProxyTypes().Contains(contact.GetType()));
                contact.FirstName = "John";
                contact.LastName = "ALFKI";

                Common.ServiceProvider.GetService<ITest1Repository>().Save(contact);

                work.AcceptChanges();
            }
            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWork())
            {
                Test1Contact contactRefetched = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts.FirstOrDefault(c => c.LastName == "ALFKI" && c.FirstName == "John");

                Objects.ObjectComparison comparison = EntityFramework.EntityFramework.CreateEntityFrameworkObjectComparison();
                comparison.Compare(contact, contactRefetched);

                Assert.AreNotSame(contact, contactRefetched);

                Assert.IsTrue(comparison.Differences.Count == 0, "Compare failed: " + comparison.DifferencesString);
            }
        }

        /// <summary>
        ///   Tests saving of a graph with proxy and non-proxy entities in a single graph.
        /// </summary>
        [TestMethod]
        public void TestSavingGraphWithProxyAndNonProxyEntities()
        {
            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = true);

            Test1Contact contact;

            contact = Common.ServiceProvider.GetService<ITest1Repository>().CreateTest1Contact();
            contact.FirstName = "John";
            contact.LastName = "ALFKI";

            Test1Contact[] contacts;

            Test1Address address = Common.ServiceProvider.GetService<ITest1Repository>().Test1Addresses.First();
            Assert.IsTrue(address.GetType() != typeof(Test1Address));
            var contactAddress = new Test1ContactAddress { Address = address };
            contact.ContactAddresses.Add(contactAddress);

            var contact2 = new Test1Contact { FirstName = "John", LastName = "ALFKI2" };
            contact2.ContactAddresses.Add(Common.ServiceProvider.GetService<ITest1Repository>().CreateTest1ContactAddress().With(i => i.Address = Common.ServiceProvider.GetService<ITest1Repository>().Test1Addresses.OrderBy(a => a.Id).Skip(1).First()));

            contacts = new[] { contact, contact2 };

            contacts.ForEach(c => Common.ServiceProvider.GetService<ITest1Repository>().Save(c));

            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = false);

            Test1Contact[] contactsRefetched = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts
                .Include(c => c.ContactAddresses.Select(ca => ca.Address))
                .Where(c => c.LastName == "ALFKI" || c.LastName == "ALFKI2").ToArray();

            Objects.ObjectComparison comparison = EntityFramework.EntityFramework.CreateEntityFrameworkObjectComparison(true);

            comparison.Compare(contacts, contactsRefetched);

            Assert.IsTrue(comparison.Differences.Count == 0, "Compare failed: " + comparison.DifferencesString);
        }


        /// <summary>
        ///   Tests saving of entities in a graph with new and non-new entities.
        /// </summary>
        [TestMethod]
        public void TestSavingGraphWithNewAndNonNewEntities()
        {
            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = true);

            var newContacts = new List<Test1Contact>();

            Func<int, Test1Contact> testContactCreator = i =>
                                                             {
                                                                 Test1Contact contact = Common.ServiceProvider.GetService<ITest1Repository>().CreateTest1Contact();
                                                                 contact.FirstName = "John";
                                                                 contact.LastName = "ALFKI" + i;
                                                                 var address = new Test1Address { Address1 = "1 Blue Way" + i, City = "Podunk", Country = new Test1Country { Name = "Far Far Away" }, State = new Test1State { Code = "ABC", Name = "The State" } };
                                                                 contact.ContactAddresses.Add(new Test1ContactAddress { Address = address });
                                                                 return contact;
                                                             };

            for (int i = 0; i < 10; i++)
            {
                newContacts.Add(testContactCreator(i));
            }

            List<Test1Contact> existingNonProxyContacts;
            List<Test1Contact> existingProxyContacts;

            Func<IQueryable<Test1Contact>> existingContactsQuery;

            existingContactsQuery = () => Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts.Include(c => c.ContactAddresses.Select(a => a.Address)).OrderBy(c => c.Id);
            existingProxyContacts = existingContactsQuery().Skip(75).Take(25).ToList();
            foreach (Test1Address address in existingProxyContacts.Select(c => c.ContactAddresses.First().Address))
            {
                address.City = "New York City";
            }

            existingNonProxyContacts = existingContactsQuery().Skip(25).Take(25).ToList();
            foreach (Test1Address address in existingNonProxyContacts.Select(c => c.ContactAddresses.First().Address))
            {
                address.City = "New York City 2";
            }

            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = false);

            for (int i = 0; i < 10; i++)
            {
                newContacts.Add(testContactCreator(i));
            }

            Assert.IsTrue(newContacts.Any(c => ObjectContext.GetKnownProxyTypes().Contains(c.GetType())));
            Assert.IsTrue(newContacts.Any(c => c.GetType() == typeof(Test1Contact)));

            Test1Contact[] contacts = existingNonProxyContacts.Concat(existingProxyContacts).Concat(newContacts).ToArray();

            using (var work = Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWork())
            {
                contacts.ForEach(Common.ServiceProvider.GetService<ITest1Repository>().Save);
                work.AcceptChanges();
            }
            using (Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWork())
            {
                var newContactsRefetched = Common.ServiceProvider.GetService<ITest1Repository>().Test1Contacts.Include(c => c.ContactAddresses.Select(ca => new { ca.Address.Country, ca.Address.State }))
                    .Where(c => c.LastName.StartsWith("ALFKI")).ToArray();

                Test1Contact[] contactsRefetched = existingContactsQuery().Skip(25).Take(25).ToArray().Concat(existingContactsQuery().Skip(75).Take(25).ToList()).Concat(newContactsRefetched).ToArray();

                Objects.ObjectComparison comparison = EntityFramework.EntityFramework.CreateEntityFrameworkObjectComparison(true);
                comparison.Compare(contacts, contactsRefetched);

                Assert.IsTrue(comparison.Differences.Count == 0, "Compare failed: " + comparison.DifferencesString);
            }
        }

        /// <summary>
        /// Tests deletion.
        /// </summary>
        [TestMethod]
        public void TestDelete()
        {
            var test1Repository = Common.ServiceProvider.GetService<ITest1Repository>();

            var contact = test1Repository.Test1Contacts.Include(c => c.ContactAddresses.Select(ca => ca.Address)).First();
            var contactId = contact.Id;
            var addressId = contact.ContactAddresses.First().AddressId;
            contact.ContactAddresses.ToList().ForEach(test1Repository.Delete);
            test1Repository.Delete(contact);
            var address = test1Repository.Test1Addresses.FirstOrDefault(a => a.Id == addressId);
            test1Repository.Save(address);
            Assert.IsNotNull(address);
            contact = test1Repository.Test1Contacts.FirstOrDefault(c => c.Id == contactId);
            Assert.IsNull(contact);
        }

        /// <summary>
        /// Tests that deleting an entity which has a depending related entity throws an error.
        /// </summary>
        [TestMethod]
        public void TestDeleteForeignKeyConstraintError()
        {
            var repository = Common.ServiceProvider.GetService<ITest1Repository>();
            using (var work = Common.ServiceProvider.GetService<IUnitOfWorkProvider>().GetUnitOfWork())
            {
                var contact = repository.Test1ContactAddresses.Include(ca => ca.Contact).First().Contact;

                try
                {
                    repository.Delete(contact);
                    work.AcceptChanges();

                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
                }
            }
        }

        /// <summary>
        /// Tests repository service construction and method accuracy.
        /// </summary>
        [TestMethod]
        public void TestRepositoryServiceConstruction()
        {
            Type serviceType = Common.ServiceProvider.GetService<IRepositoryServiceFactory>().GetServiceType(typeof(ITest1Repository));

            Assert.IsNotNull(serviceType);

            Assert.IsTrue(serviceType.HasAttribute<ServiceContractAttribute>());
            Assert.IsTrue(serviceType.GetAttribute<ServiceContractAttribute>().Name == "Test1Repository");
            Assert.IsTrue(serviceType.GetAttribute<ServiceContractAttribute>().Namespace == "http://TestApplication1/Model/Test1Model");

            foreach (var propertyInfo in typeof(ITest1Repository).GetProperties().Where(p => p.PropertyType.EqualsGenericTypeFor(typeof(IQueryable<>))))
            {
                var serviceMethod = serviceType.GetMethod("Get{0}".FormatWith(propertyInfo.Name), new[] { typeof(string), typeof(string), typeof(string) });
                Assert.IsNotNull(serviceMethod);
                Assert.IsTrue(serviceMethod.GetParameters().Length == 3);
                Assert.IsTrue(serviceMethod.GetParameters()[0].Name == "criteria");
                Assert.IsTrue(serviceMethod.GetParameters()[1].Name == "ordering");
                Assert.IsTrue(serviceMethod.GetParameters()[2].Name == "inclusions");
                Assert.IsTrue(serviceMethod.ReturnType.FindElementType().Name == propertyInfo.PropertyType.FindElementType().Name + "Contract_IsReference_WithKey");
            }

            foreach (var methodInfo in typeof(ITest1Repository).GetMethods().Where(m => !m.IsSpecialName && m.ReturnType.EqualsGenericTypeFor(typeof(IQueryable<>))))
            {
                var serviceMethod = serviceType.GetMethod(methodInfo.Name);
                Assert.IsNotNull(serviceMethod);
                Assert.IsTrue(serviceMethod.GetParameters().Select(p => new { p.Name, p.ParameterType })
                                  .SequenceEqual(
                                      methodInfo.GetParameters().Select(p => new { p.Name, p.ParameterType })
                                          .Concat(new[] { new { Name = "criteria", ParameterType = typeof(string) }, new { Name = "ordering", ParameterType = typeof(string) }, new { Name = "inclusions", ParameterType = typeof(string) } })));

                Assert.IsTrue(serviceMethod.ReturnType.FindElementType().Name == methodInfo.ReturnType.GetQueryableElementType().Name + "Contract_IsReference_WithKey");
            }

            foreach (var propertyInfo in typeof(ITest1Repository).GetProperties().Where(p => !p.PropertyType.EqualsGenericTypeFor(typeof(IQueryable<>))))
            {
                var serviceMethod = serviceType.GetMethod("Get{0}".FormatWith(propertyInfo.Name));
                Assert.IsNotNull(serviceMethod);
                Assert.IsTrue(serviceMethod.GetParameters().Length == 0);
                Assert.IsTrue(serviceMethod.ReturnType == propertyInfo.PropertyType);
            }

            foreach (var methodInfo in typeof(ITest1Repository).GetMethods().Where(m => !m.IsSpecialName && !m.ReturnType.EqualsGenericTypeFor(typeof(IQueryable<>))))
            {
                MethodInfo serviceMethod;

                if (Domain.Domain.IsSaveOrDeleteMethod(methodInfo))
                {
                    serviceMethod = serviceType.GetMethod("{0}{1}".FormatWith(methodInfo.Name, methodInfo.GetParameters()[0].ParameterType.Name));
                }
                else
                {
                    serviceMethod = serviceType.GetMethod(methodInfo.Name);
                }

                Assert.IsNotNull(serviceMethod);
                Assert.IsTrue(serviceMethod.GetParameters().Select(p => new { p.Name, TypeName = p.ParameterType.GetAttribute<DataContractAttribute>().Name })
                                  .SequenceEqual(
                                      methodInfo.GetParameters().Select(p => new { p.Name, TypeName = p.ParameterType.Name })));

                if (serviceMethod.ReturnType != typeof(void))
                {
                    Assert.IsTrue(serviceMethod.ReturnType.Name == methodInfo.ReturnType.Name + "Contract_IsReference_WithKey");
                }
            }
        }

        /// <summary>
        /// Tests the operations on a repository service.
        /// </summary>
        [TestMethod]
        public void TestRepositoryServiceOperations()
        {
            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf => ocf.ProxyCreationEnabled = true);

            var serviceType = Common.ServiceProvider.GetService<IRepositoryServiceFactory>().GetServiceType(typeof(ITest1Repository));
            var instance = Common.ServiceProvider.GetService(serviceType).CastTo<dynamic>();

            var test1Contacts = instance.GetTest1Contacts("LastName.Contains(\"a\")", "LastName", "ContactAddresses.Address.Country");
            Assert.IsTrue(test1Contacts != null && test1Contacts.Length > 0);

            var test1Contact = instance.CreateTest1Contact();
            test1Contact.LastName = "ALFKITestRepositoryServiceOperations";
            test1Contact.FirstName = "John";
            instance.SaveTest1Contact(test1Contact);

            test1Contacts = instance.GetTest1Contacts("LastName == \"ALFKITestRepositoryServiceOperations\"", "LastName", "ContactAddresses.Address.Country");
            Assert.IsTrue(test1Contacts.Length == 1);
            test1Contact = test1Contacts[0];

            instance.DeleteTest1Contact(test1Contact);

            test1Contacts = instance.GetTest1Contacts("LastName == \"ALFKITestRepositoryServiceOperations\"", "LastName", "ContactAddresses.Address.Country");
            Assert.IsTrue(test1Contacts.Length == 0);
        }

        /// <summary>
        /// Tests the remote operations on a repository service.
        /// </summary>
        [TestMethod]
        public void TestRemoteRepositoryServiceOperations()
        {
            //var client = new Test1RepositoryClient();
            var serviceType = Common.ServiceProvider.GetService<IRepositoryServiceFactory>().GetServiceType(typeof(ITest1Repository));
            var client = Common.ServiceProvider.GetService(serviceType).CastTo<dynamic>();

            var contacts = client.GetTest1Contacts("LastName.Contains(\"a\")", "LastName", "ContactAddresses.Address.Country");
            Assert.IsTrue(contacts != null && contacts.Length > 0);
            Assert.IsNotNull(contacts[0].ContactAddresses[0].Address.Country);
        }

        [TestMethod]
        public void TestGenericRepository()
        {
            var repository = Common.ServiceProvider.GetService<IRepository<Test1Contact>>();
            var results = repository.Query().ToArray();
            Assert.IsTrue(Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Current == null);
            Assert.IsTrue(results.Length > 0);
            results[0].FirstName = "Joe";
            repository.Save(results[0]);
            results = repository.Query().ToArray();
            Assert.IsTrue(results[0].FirstName == "Joe");
        }

        [TestMethod]
        public void TestRepositoryGarbageCollection()
        {
            WeakReference reference;
            IUnitOfWork work;
            using (work = Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Create())
            {
                var contacts = Common.ServiceProvider.GetService<ITest1Repository>().Contacts.ToArray();
                Assert.IsNotNull(contacts);
                contacts = null;
                Assert.IsNull(contacts);
                reference = new WeakReference(work.CastTo<UnitOfWork>().Components[typeof(Test1Repository)].CastTo<EntityFrameworkRepositoryProvider>());
                Assert.IsTrue(reference.IsAlive);
            }
            work = null;
            Assert.IsNull(work);

            GC.Collect();
            GC.Collect();

            Assert.IsFalse(reference.IsAlive);
        }

        [TestMethod]
        public void TestQueryableFactoryLoad()
        {
            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf =>
            {
                ocf.ProxyCreationEnabled = false;
                ocf.LazyLoadingEnabled = false;
            });

            var repository = Common.ServiceProvider.GetService<ITest2Repository>();
            List<Test2Contact> results = (from contact in repository.Test2Contacts
                                          where
                                          contact.LastName == "Brennan" && contact.ContactAddresses.Count > 0
                                          select contact).
            ToList();

            Assert.IsNotNull(results);

            Assert.IsTrue(results.Count > 0);

            Assert.IsTrue(!ObjectContext.GetKnownProxyTypes().Contains(results[0].GetType()));

            Queryables.GetQueryableFactory(repository).Load(results,
                                                           i => i.Select(c => c.ContactAddresses.Where(ca => ca.Id > 0).Select(ca => ca.Address)),
                                                           i => i.Select(c => c.ContactAddresses.Select(ca => ca.Address)));

            Assert.IsTrue(results[0].ContactAddresses.Count > 0);

            Assert.IsNotNull(results[0].ContactAddresses.First().Address);
        }

        /// <summary>
        /// Tests remote operations on a repository service.
        /// </summary>
        [TestMethod]
        public void TestServiceStringQuery()
        {
            var client = new Test1RepositoryClient();

            using (new OperationContextScope(client.InnerChannel))
            {
                SetAuthenticationCookieInCurrentOperationContext();
                var result = client.GetTest1Contacts("LastName.Contains(\"A\")", "LastName", "ContactAddresses.Address.Country");
                Assert.IsTrue(result != null && result.Count > 0);
                Assert.IsTrue(result != null && result.Count == 204);
                Assert.IsNotNull(result[0].ContactAddresses.First().Address.Country);
            }
        }

        /// <summary>
        /// Tests remote update operations on a repository service.
        /// </summary>
        [TestMethod]
        public void TestRepositorySave()
        {
            // So we revert changes, since transaction is not carried over in this case
            List<Test1RepositoryService.Test1Contact> original;

            List<Test1RepositoryService.Test1Contact> result;

            var client = new Test1RepositoryClient();
            using (new OperationContextScope(client.InnerChannel))
            {
                SetAuthenticationCookieInCurrentOperationContext();

                // Fetch
                original = client.GetTest1Contacts("LastName.Contains(\"A\")", "LastName", "ContactAddresses.Address.Country");

                result = original.Clone();
                Assert.IsTrue(result != null && result.Count > 0);
                Assert.IsTrue(result != null && result.Count == 204);
                Assert.IsNotNull(result[0].ContactAddresses.First().Address.Country);

                // Update
                result.ForEach(c => c.LastName = "ALFKI");
                result.ForEach(c => c.ContactAddresses.First().Address.Country.Name = "ALFKI");
                client.SaveAllTest1Contact(result);
            }

            client = new Test1RepositoryClient();
            using (new OperationContextScope(client.InnerChannel))
            {
                SetAuthenticationCookieInCurrentOperationContext();

                // Verify update
                result = client.GetTest1Contacts("LastName.Contains(\"A\")", "LastName", "ContactAddresses.Address.Country");
                
                // Revert changes
                client.SaveAllTest1Contact(original);
                
                Assert.IsTrue(result.Count == 204
                              && result.All(c => c.LastName == "ALFKI")
                              && result.All(c => c.ContactAddresses.First().Address.Country.Name == "ALFKI"));
            }
        }

        /// <summary>
        /// Tests remote REST query operations on a repository service.
        /// </summary>
        [TestMethod]
        public void TestRepositoryRestQuery()
        {
            var webClient = new WebClient();
            webClient.Headers.Add(HttpRequestHeader.Cookie, PrepAuthentication.AuthenticatedUserCookie);

            var result = webClient.OpenRead(new Uri(@"http://localhost/Soaf.Testing.Web/Services/rest/Test1Repository/GetTest1Contacts?criteria=LastName=""Breiter""&ordering=&inclusions=ContactAddresses.Address"));
            var expected = result.GetString();
            Assert.AreEqual(expected, @"<ArrayOfTest1Contact xmlns=""http://schemas.datacontract.org/2004/07/TestApplication1.Model.Test1Model"" xmlns:i=""http://www.w3.org/2001/XMLSchema-instance""><Test1Contact z:Id=""i1"" xmlns:z=""http://schemas.microsoft.com/2003/10/Serialization/""><ContactAddresses><Test1ContactAddress z:Id=""i2""><Address z:Id=""i3""><Address1>Ferdinandstrasse 29-33</Address1><Address2 i:nil=""true""/><City>Hamburg</City><ContactAddresses><Test1ContactAddress z:Ref=""i2""/></ContactAddresses><Country i:nil=""true""/><CountryId>1082</CountryId><Id>123340</Id><State i:nil=""true""/><StateId i:nil=""true""/><Zip>20095</Zip></Address><AddressId>123340</AddressId><Contact z:Ref=""i1""/><ContactId>60935</ContactId><Id>31012</Id></Test1ContactAddress></ContactAddresses><FirstName>Finn</FirstName><Id>60935</Id><LastName>Breiter</LastName><NonEntitiesCollectionReturningNull i:nil=""true""/></Test1Contact></ArrayOfTest1Contact>");
        }

        [TestMethod]
        public void TestRepositoryODataOperations()
        {
            // Try fetching data
            var client = new Test1RepositoryODataService.Test1Repository(new Uri(@"http://localhost/Soaf.Testing.Web/Services/OData/Test1Repository"));
            client.SendingRequest += (sender, args) =>
                                         {
                                             // Provide authentication information
                                             args.RequestHeaders[HttpRequestHeader.Cookie] = PrepAuthentication.AuthenticatedUserCookie;
                                         };

            var query = client.Test1Contacts
                .Expand("ContactAddresses/Address")
                .Where(c => c.LastName.Contains("A"))
                .CastTo<DataServiceQuery<Test1RepositoryODataService.Test1Contact>>();

            var results = query.Execute().IfNotNull(r => r.ToArray());
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Length == 204);
            Assert.IsTrue(results[0].ContactAddresses.Count > 0 && results[0].ContactAddresses[0].Address != null);

            // Modify and try save changes
            results.ForEach(c => c.ContactAddresses.First().Address.City = "ALFKI");
            var response = client.SaveChanges();
            Assert.IsTrue(response.All(i => i.Error == null));

            // Refetch saved data and verify
            var query2 = client.Test1Contacts
                .Expand("ContactAddresses/Address")
                .Where(c => c.LastName.Contains("A"))
                .CastTo<DataServiceQuery<Test1RepositoryODataService.Test1Contact>>();

            var results2 = query2.Execute().IfNotNull(r => r.ToArray());
            Assert.IsNotNull(results2);
            Assert.IsTrue(results2.Length == 204);
            Assert.IsTrue(results2.All(i => i.ContactAddresses.First().Address.City == "ALFKI"));
        }

        /// <summary>
        /// Sets cookie when OperationContextScope is initialized
        /// </summary>
        private static void SetAuthenticationCookieInCurrentOperationContext()
        {
            // Embeds the extracted cookie in the next web service request
            // Note that we manually have to create the request object since
            // since it doesn't exist yet at this stage 
            var request = new HttpRequestMessageProperty();
            request.Headers[HttpRequestHeader.Cookie] = PrepAuthentication.AuthenticatedUserCookie;
            OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = request;
        }


        [TestMethod]
        public void TestProtobufCloner()
        {
            Common.ServiceProvider.GetService<IEntityFrameworkRepositoryProviderFactory>().ObjectContextFactories.ForEach(ocf =>
            {
                ocf.ProxyCreationEnabled = true;
            });

            ServiceModel.Initialize();
            ServiceModel.AddKnownTypeHierarchy(typeof(Test1Contact), null, true);

            var repository = Common.ServiceProvider.GetService<ITest1Repository>();

            var collection = new List<Test1Contact>();
            for (int i = 0; i < 10; i++)
            {
                collection.Add(repository.CreateTest1Contact());
            }

            Assert.IsTrue(collection.All(i => i.GetType().Assembly.IsDynamic && i.GetType().FullName.StartsWith("System.Data.Entity.DynamicProxies.")));

            var cloner = new ProtobufCloner(new DataContractSerializerCloner());
            var clonedCollection = cloner.Clone(collection, typeof(ICollection<Test1Contact>), null) as IEnumerable<Test1Contact>;

            Assert.IsNotNull(clonedCollection);
            Assert.AreEqual(10, clonedCollection.Count());
            Assert.IsTrue(clonedCollection.All(i => i != null));
            Assert.IsTrue(collection.All(i => i.GetType().Assembly.IsDynamic));
        }
    }
}