﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soaf.Data;
using Soaf.Domain;
using Soaf.EntityFramework;
using Soaf.IO;
using Soaf.Reflection;
using Soaf.Web.Client;
using TestApplication1.Model;
using File = System.IO.File;
using Resources = Soaf.Testing.Properties.Resources;

namespace Soaf.Testing
{

    [TestClass]
    public class Common
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool Wow64RevertWow64FsRedirection(IntPtr ptr);

        public static readonly object SyncRoot = new object();

        static Common()
        {
            ServiceProvider = Bootstrapper.Run<IServiceProvider>(new AssemblyRepository(new[] { typeof(Common).Assembly, typeof(IFederatedTestRepository).Assembly, typeof(RemoteServiceBehavior).Assembly }));

            EnableDtcNetworkAccess();

            var iisreset = new Process { StartInfo = { FileName = "iisreset.exe", UseShellExecute = false, CreateNoWindow = true } };
            iisreset.Start();
            iisreset.WaitForExit();

            ServiceProvider
                .GetService<IEntityFrameworkRepositoryProviderFactory>()
                .ObjectContextFactories
                .ForEach(f =>
                             {
                                 try
                                 {
                                     Stream stream = f.RepositoryType.Assembly.GetManifestResourceStream("{0}.{1}{2}".FormatWith(f.RepositoryType.Namespace, f.RepositoryType.GetAttribute<RepositoryAttribute>().Name, "Setup.sql"));
                                     CreateAndInitializeDatabase(f.GetObjectContext(), stream == null ? null : stream.GetString());
                                 }
                                 catch (Exception ex)
                                 {
                                     throw new InvalidOperationException("Path: " + f.RepositoryType.Assembly.Location, ex);
                                 }
                             });
        }

        public static IServiceProvider ServiceProvider { get; private set; }

        /// <summary>
        ///   Enables DTC network access.
        /// </summary>
        private static void EnableDtcNetworkAccess()
        {
            File.WriteAllText("MSDTC_Security.reg", Resources.MSDTC_Security);

            var oldWowRedirect = new IntPtr(0);
            if (Environment.Is64BitOperatingSystem)
                Wow64DisableWow64FsRedirection(ref oldWowRedirect);

            dynamic regeditProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "regedit",
                    Arguments = "/s \"" + Path.GetFullPath("MSDTC_Security.reg") + "\"",
                    CreateNoWindow = true,
                    UseShellExecute = false
                }
            };
            regeditProcess.Start();
            regeditProcess.WaitForExit();

            var stopProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "net",
                    Arguments = "stop msdtc",
                    CreateNoWindow = true,
                    UseShellExecute = false
                }
            };
            stopProcess.Start();
            stopProcess.WaitForExit();

            var startProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "net",
                    Arguments = "start msdtc",
                    CreateNoWindow = true,
                    UseShellExecute = false
                }
            };
            startProcess.Start();
            startProcess.WaitForExit();

            if (Environment.Is64BitOperatingSystem)
                Wow64RevertWow64FsRedirection(oldWowRedirect);

        }


        /// <summary>
        ///   Creates the and initialize a test Entity Framework model's database.
        /// </summary>
        /// <param name="objectContext"> The object context. </param>
        /// <param name="sql"> The SQL. </param>
        public static void CreateAndInitializeDatabase(ObjectContext objectContext, string sql)
        {
            if (objectContext.DatabaseExists())
            {
                objectContext.DeleteDatabase();
            }
            if (sql == null)
            {
                objectContext.CreateDatabase();
            }
            else
            {
                objectContext.Connection.Close();
                DbConnection connection = objectContext.Connection.CastTo<EntityConnection>().StoreConnection;
                var builder = new SqlConnectionStringBuilder(connection.ConnectionString) { InitialCatalog = "master" };
                using (var sqlConnection = new SqlConnection(builder.ToString()))
                {
                    sqlConnection.Open();
                    SqlCommand command = sqlConnection.CreateCommand();
                    foreach (string line in sql.Split(new[] { "\nGO" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        try
                        {
                            command.CommandText = line;
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            throw new InvalidOperationException("Can't run: " + line, ex);
                        }
                    }
                }
            }
            using (var sqlConnection = new SqlConnection(((EntityConnection)objectContext.Connection).StoreConnection.ConnectionString))
            {
                sqlConnection.EnsureMachineAccountAccessToSqlServerDatabase(@"NT AUTHORITY\Network Service");
                sqlConnection.EnsureMachineAccountAccessToSqlServerDatabase(@"IIS APPPOOL\ASP.NET V4.0 Integrated");
                sqlConnection.EnsureMachineAccountAccessToSqlServerDatabase(@"IIS APPPOOL\ASP.NET V4.0");
                sqlConnection.EnsureMachineAccountAccessToSqlServerDatabase(@"IIS APPPOOL\DefaultAppPool");
                sqlConnection.EnsureMachineAccountAccessToSqlServerDatabase(@"{0}\{1}$".FormatWith(Environment.UserDomainName, Environment.MachineName));
            }
        }

        /// <summary>
        /// Allows to collect property changes filtering out cascading property change notifications
        /// </summary>
        /// <param name="propertiesChanged"></param>
        /// <param name="expectedSenderAssert">If provided, asserts that sender is equal to specified object</param>
        /// <returns></returns>
        public static PropertyChangedEventHandler CollectPropertyChangesTo(IList<string> propertiesChanged, object expectedSenderAssert = null)
        {
            return (sender, e) =>
            {
                if (expectedSenderAssert != null)
                {
                    Assert.AreSame(expectedSenderAssert, sender);
                }
                propertiesChanged.Add(e.PropertyName);
            };
        }

        [AssemblyInitialize]
        public static void OnAssemblyInitialize(TestContext testContext)
        {
        }
    }
}