﻿using System.Linq;
using System.Threading;
using System.Transactions;
using System.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Soaf.Caching;
using Soaf.ComponentModel;

namespace Soaf.Testing
{


    /// <summary>
    ///This is a test class for MembershipTest and is intended
    ///to contain all MembershipTest Unit Tests
    ///</summary>
    [TestClass]
    public class MembershipTest
    {
        private TransactionScope transactionScope;

        [TestInitialize]
        public void OnTestInitialize()
        {
            Monitor.Enter(Common.SyncRoot);
            PrepAuthentication.CreateUser();
            PrepAuthentication.LogIn("ServiceTests", "ServiceTests");

            if (Transaction.Current != null || Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Current != null) throw new InvalidOperationException();
            transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10));
        }

        [TestCleanup]
        public void OnTestCleanup()
        {
            transactionScope.Dispose();
            Common.ServiceProvider.GetService<IUnitOfWorkProvider>().Current.IfNotNull(c => c.Dispose());
            Common.ServiceProvider.GetService<ICache>().Clear();
            
            PrepAuthentication.LogOut();
            PrepAuthentication.DeleteUser();

            Monitor.Exit(Common.SyncRoot);
        }

        /// <summary>
        ///A test for CreateUser
        ///</summary>
        [TestMethod]
        public void CreateUserTest()
        {
            var createdUser = CreateUser();

            var user = Membership.GetUser(createdUser.UserName);
            Assert.IsNotNull(user);
            Assert.AreEqual(user.UserName, createdUser.UserName);
        }

        private static MembershipUser CreateUser()
        {
            string username = "test@test.com";
            string password = "Password1";
            string email = "test@test.com";
            string passwordQuestion = "What is your mother's maiden name?";
            string passwordAnswer = "Johnson";
            MembershipCreateStatus status;
            var statusExpected = MembershipCreateStatus.Success;
            var createdUser = Membership.CreateUser(username, password, email, passwordQuestion, passwordAnswer, true, Guid.NewGuid(), out status);
            Assert.AreEqual(statusExpected, status);
            Assert.IsNotNull(createdUser);
            return createdUser;
        }

        /// <summary>
        ///A test for DeleteUser
        ///</summary>
        [TestMethod]
        public void DeleteUserTest()
        {
            var user = CreateUser();
            Assert.IsNotNull(user.UserName);
            Membership.DeleteUser(user.UserName);
            user = Membership.GetUser(user.UserName);
            Assert.IsNull(user);
        }

        /// <summary>
        ///A test for FindUsersByEmail
        ///</summary>
        [TestMethod]
        public void FindUsersByEmailTest()
        {
            var user = CreateUser();
            string emailToMatch = user.Email.Substring(1, user.Email.Length - 2).ToUpper();
            int totalRecords;
            user = Membership.FindUsersByEmail(emailToMatch, 0, 100, out totalRecords).OfType<MembershipUser>().FirstOrDefault();
            Assert.IsNotNull(user);
            Assert.IsTrue(totalRecords == 1);
            Assert.IsTrue(user.Email.ToLower().Contains(emailToMatch.ToLower()));
        }

        /// <summary>
        ///A test for FindUsersByName
        ///</summary>
        [TestMethod]
        public void FindUsersByNameTest()
        {
            var user = CreateUser();
            string toMatch = user.UserName.Substring(1, user.UserName.Length - 2).ToUpper();
            int totalRecords;
            user = Membership.FindUsersByEmail(toMatch, 0, 100, out totalRecords).OfType<MembershipUser>().FirstOrDefault();
            Assert.IsNotNull(user);
            Assert.IsTrue(totalRecords == 1);
            Assert.IsTrue(user.UserName.ToLower().Contains(toMatch.ToLower()));
        }

        /// <summary>
        ///A test for GetAllUsers
        ///</summary>
        [TestMethod]
        public void GetAllUsersTest()
        {
            var user = CreateUser();
            int pageIndex = 0;
            int pageSize = 100;
            int totalRecords;
            var users = Membership.GetAllUsers(pageIndex, pageSize, out totalRecords);
            Assert.AreEqual(totalRecords, 2);
            Assert.IsNotNull(users.OfType<MembershipUser>().Select(u => u.UserName).FirstOrDefault(u => u.Equals(user.UserName, StringComparison.OrdinalIgnoreCase)));
        }

        /// <summary>
        ///A test for GetNumberOfUsersOnline
        ///</summary>
        [TestMethod]
        public void GetNumberOfUsersOnlineTest()
        {
            var user = CreateUser();
            var online = Membership.GetNumberOfUsersOnline();
            Assert.IsTrue(online == 1);
            Membership.GetUser(user.UserName, true);
            online = Membership.GetNumberOfUsersOnline();
            Assert.IsTrue(online == 2);
        }

        /// <summary>
        ///A test for GetUser
        ///</summary>
        [TestMethod]
        public void GetUserByKeyTest()
        {
            var user = CreateUser();
            Assert.IsNotNull(user);
            if (user.ProviderUserKey != null) user = Membership.GetUser(user.ProviderUserKey);
            Assert.IsNotNull(user);
        }

        /// <summary>
        ///A test for ValidateUser
        ///</summary>
        [TestMethod]
        public void ValidateUserTest()
        {
            using (new TransactionScope(TransactionScopeOption.Suppress))
            {
                var user = CreateUser();
                var isValid = Membership.ValidateUser(user.UserName, "Password1");
                Assert.IsTrue(isValid);
                Membership.DeleteUser(user.UserName);
                user = Membership.GetUser(user.UserName);
                Assert.IsNull(user);
            }
        }
    }
}
