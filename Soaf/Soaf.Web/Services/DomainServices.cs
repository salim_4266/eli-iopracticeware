﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.Web.Hosting;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Domain;
using Soaf.Linq;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Threading;
using Soaf.TypeSystem;
using Soaf.Web.Services;

[assembly: Component(typeof(RepositoryServiceFactory), typeof(IRepositoryServiceFactory), Initialize = true)]
[assembly: Component(typeof(RepositoryServiceGenerator), typeof(IRepositoryServiceGenerator))]
[assembly: Component(typeof(RepositoryServiceBehavior))]
[assembly: Component(typeof(RepositoryServicesConfiguration), typeof(IRepositoryServicesConfiguration), Priority = -1)]

namespace Soaf.Web.Services
{
    [Singleton]
    public interface IRepositoryServicesConfiguration
    {
        bool ServicesEnabled { get; }
    }

    internal sealed class RepositoryServicesConfiguration : IRepositoryServicesConfiguration
    {
        public bool ServicesEnabled { get; set; }
    }

    /// <summary>
    ///   Provides types that are services for a repository type.
    /// </summary>
    internal interface IRepositoryServiceFactory : IInitializable
    {
        Type GetServiceType(Type repositoryType);
    }

    /// <summary>
    ///   Provides types that are services for a repository type.
    /// </summary>
    [Singleton]
    [SupportsSynchronization]
    internal class RepositoryServiceFactory : IRepositoryServiceFactory
    {
        private readonly IComponentRegistry componentRegistry;
        private readonly IRepositoryServiceGenerator repositoryServiceGenerator;
        private readonly IDictionary<Type, Type> repositoryServices = new Dictionary<Type, Type>().Synchronized();
        private readonly IRepositoryServicesConfiguration configuration;

        public RepositoryServiceFactory(IRepositoryServiceGenerator repositoryServiceGenerator, IComponentRegistry componentRegistry, IRepositoryServicesConfiguration configuration)
        {
            this.repositoryServiceGenerator = repositoryServiceGenerator;
            this.componentRegistry = componentRegistry;
            this.configuration = configuration;
        }

        #region IRepositoryServiceFactory Members

        [Synchronized]
        public virtual Type GetServiceType(Type repositoryType)
        {
            if (!repositoryServiceGenerator.IsSupported)
            {
                throw new NotSupportedException();
            }

            Type result;
            if (!repositoryServices.TryGetValue(repositoryType, out result))
            {
                repositoryServices[repositoryType] = result = repositoryServiceGenerator.GenerateServiceType(repositoryType);
                // add to the registry
                componentRegistry.GetComponentRegistration(result, null);
            }
            return result;
        }

        #endregion

        public void Initialize()
        {
            if (!repositoryServiceGenerator.IsSupported)
            {
                return;
            }

            if (HostingEnvironment.IsHosted && configuration.ServicesEnabled)
            {
                foreach (Type type in componentRegistry.ComponentRegistrations.Select(r => r.Type).Concat(componentRegistry.ComponentRegistrations.SelectMany(r => r.For ?? new Type[0])).WhereNotDefault()
                    .Where(t => t.HasAttribute<RepositoryAttribute>(false, false)).ToArray())
                {
                    GetServiceType(type);
                }
            }
        }
    }

    /// <summary>
    ///   Generates dynamic types that are repository services.
    /// </summary>
    internal interface IRepositoryServiceGenerator
    {
        bool IsSupported { get; }
        Type GenerateServiceType(Type repositoryType);
    }

    /// <summary>
    ///   Generates dynamic types that are repository services.
    /// </summary>
    [Singleton]
    internal class RepositoryServiceGenerator : IRepositoryServiceGenerator
    {
        #region IRepositoryServiceGenerator Members

        /// <summary>
        ///   Builds a dynamic service based on the repository properties and methods.
        /// </summary>
        /// <param name = "repositoryType">Type of the repository.</param>
        /// <returns></returns>
        public Type GenerateServiceType(Type repositoryType)
        {
            using (new TimedScope(s => Debug.WriteLine("Created domain service for {0} in {1}.".FormatWith(repositoryType.Name, s))))
            {
                if (!IsSupported)
                {
                    throw new NotSupportedException();
                }

                var queryStringConverter = new QueryStringConverter();

                var repositoryAttribute = repositoryType.GetAttribute<RepositoryAttribute>();

                var operationNames = new HashSet<string>();

                IEnumerable<MemberInfo> members = repositoryType.GetMembers(true, BindingFlags.Public | BindingFlags.Instance);

                var serviceMetaType = new MetaType { Name = "Soaf.Web.Services.RepositoryServices.{0}".FormatWith(repositoryAttribute.Name ?? repositoryType.Name) };
                foreach (MethodInfo methodInfo in members.OfType<MethodInfo>().Where(m => !m.IsSpecialName && m.ReturnType.EqualsGenericTypeFor(typeof(IQueryable<>))))
                {
                    MetaMethod metaMethod = methodInfo.ToMetaMethod();
                    if (operationNames.Add(metaMethod.Name))
                    {
                        metaMethod.IsAbstract = false;
                        metaMethod.ReturnType = methodInfo.ReturnType.GetQueryableElementType().MakeEnumerableType().ToMetaType().ToDataContractType();
                        metaMethod.Attributes.Add(new RepositoryServiceMethodAttribute(methodInfo.Name, methodInfo.GetParameters().Select(p => p.ParameterType.AssemblyQualifiedName).ToArray()) { IsQueryable = true });
                        metaMethod.Attributes.Add(new OperationContractAttribute());

                        metaMethod.Parameters.Add(new MetaParameter { Name = "criteria", ParameterType = typeof(string).ToMetaType() });
                        metaMethod.Parameters.Add(new MetaParameter { Name = "ordering", ParameterType = typeof(string).ToMetaType() });
                        metaMethod.Parameters.Add(new MetaParameter { Name = "inclusions", ParameterType = typeof(string).ToMetaType() });

                        if (methodInfo.GetParameterTypes().All(queryStringConverter.CanConvert))
                        {
                            metaMethod.Attributes.Add(new WebGetAttribute());
                        }
                        else
                        {
                            metaMethod.Attributes.Add(new WebInvokeAttribute());
                        }

                        serviceMetaType.Members.Add(metaMethod);
                    }
                }

                foreach (PropertyInfo propertyInfo in members.OfType<PropertyInfo>().Where(p => p.PropertyType.EqualsGenericTypeFor(typeof(IQueryable<>))))
                {
                    var metaMethod = new MetaMethod { Name = "Get{0}".FormatWith(propertyInfo.Name) };
                    if (operationNames.Add(metaMethod.Name))
                    {
                        metaMethod.IsAbstract = false;
                        metaMethod.ReturnType = propertyInfo.PropertyType.GetQueryableElementType().MakeEnumerableType().ToMetaType().ToDataContractType();
                        metaMethod.Attributes.Add(new RepositoryServiceMethodAttribute(propertyInfo.GetGetMethod(true).Name) { IsQueryable = true });
                        metaMethod.Attributes.Add(new OperationContractAttribute());
                        metaMethod.Attributes.Add(new WebGetAttribute());

                        metaMethod.Parameters.Add(new MetaParameter { Name = "criteria", ParameterType = typeof(string).ToMetaType() });
                        metaMethod.Parameters.Add(new MetaParameter { Name = "ordering", ParameterType = typeof(string).ToMetaType() });
                        metaMethod.Parameters.Add(new MetaParameter { Name = "inclusions", ParameterType = typeof(string).ToMetaType() });

                        serviceMetaType.Members.Add(metaMethod);
                    }
                }

                foreach (MethodInfo methodInfo in members.OfType<MethodInfo>().Where(m => !m.IsSpecialName && !m.ReturnType.EqualsGenericTypeFor(typeof(IQueryable<>))))
                {
                    MetaMethod metaMethod = methodInfo.ToMetaMethod();

                    if (Domain.Domain.IsSaveOrDeleteMethod(methodInfo, Domain.Domain.SaveMethodName) || Domain.Domain.IsSaveOrDeleteMethod(methodInfo, Domain.Domain.DeleteMethodName))
                    {
                        metaMethod.Name = metaMethod.Name + metaMethod.Parameters.First().ParameterType.Name;

                        var manyMetaMethod = new MetaMethod { Name = metaMethod.Name.Replace("Save", "SaveAll").Replace("Delete", "DeleteAll") };
                        manyMetaMethod.Parameters.Add(new MetaParameter { Name = "items", ParameterType = methodInfo.GetParameters()[0].ParameterType.MakeEnumerableType().ToMetaType().ToDataContractType() });
                        manyMetaMethod.Attributes.Add(new RepositoryServiceMethodAttribute(methodInfo.Name, methodInfo.GetParameters().Select(p => p.ParameterType.AssemblyQualifiedName).ToArray()) { IsUpdateOrDeleteMany = true });
                        manyMetaMethod.Attributes.Add(new OperationContractAttribute());
                        manyMetaMethod.Attributes.Add(new WebInvokeAttribute());

                        if (operationNames.Add(manyMetaMethod.Name))
                        {
                            serviceMetaType.Members.Add(manyMetaMethod);
                        }
                    }

                    if (operationNames.Add(metaMethod.Name))
                    {
                        metaMethod.IsAbstract = false;
                        metaMethod.Attributes.Add(new RepositoryServiceMethodAttribute(methodInfo.Name, methodInfo.GetParameters().Select(p => p.ParameterType.AssemblyQualifiedName).ToArray()));
                        metaMethod.Attributes.Add(new OperationContractAttribute());

                        if (methodInfo.GetParameterTypes().All(queryStringConverter.CanConvert))
                        {
                            metaMethod.Attributes.Add(new WebGetAttribute());
                        }
                        else
                        {
                            metaMethod.Attributes.Add(new WebInvokeAttribute());
                        }

                        metaMethod.Parameters.ForEach(p => p.ParameterType = p.ParameterType.ToDataContractType());
                        if (metaMethod.ReturnType != null)
                        {
                            metaMethod.ReturnType = metaMethod.ReturnType.ToDataContractType();
                        }

                        serviceMetaType.Members.Add(metaMethod);
                    }
                }

                foreach (PropertyInfo propertyInfo in members.OfType<PropertyInfo>().Where(p => !p.PropertyType.EqualsGenericTypeFor(typeof(IQueryable<>))))
                {
                    var metaMethod = new MetaMethod { Name = "Get{0}".FormatWith(propertyInfo.Name), ReturnType = propertyInfo.PropertyType.ToMetaType() };

                    if (operationNames.Add(metaMethod.Name))
                    {
                        metaMethod.Attributes.Add(new RepositoryServiceMethodAttribute(propertyInfo.Name));
                        metaMethod.Attributes.Add(new OperationContractAttribute());
                        metaMethod.Attributes.Add(new WebGetAttribute());
                        metaMethod.ReturnType = metaMethod.ReturnType.ToDataContractType();
                        serviceMetaType.Members.Add(metaMethod);
                    }
                }

                serviceMetaType.Attributes.Add(new RepositoryServiceAttribute { RepositoryType = repositoryType });

                var serviceContractAttribute = new ServiceContractAttribute { Name = repositoryAttribute.Name ?? repositoryType.Name, Namespace = repositoryAttribute.Namespace ?? "http://{0}".FormatWith((repositoryType.Namespace ?? repositoryType.Name).Replace(".", "/")) };
                serviceMetaType.Attributes.Add(serviceContractAttribute);

                var aspNetCompatibilityRequirementsAttribute = new AspNetCompatibilityRequirementsAttribute { RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed };
                serviceMetaType.Attributes.Add(aspNetCompatibilityRequirementsAttribute);

                return serviceMetaType.ToType();
            }
        }

        public bool IsSupported
        {
            get
            {
                return new[] { typeof(QueryStringConverter), typeof(WebGetAttribute), typeof(WebInvokeAttribute), typeof(OperationContractAttribute), typeof(AspNetCompatibilityRequirementsAttribute) }
                    .All(i => i != null);
            }
        }

        #endregion
    }

    /// <summary>
    ///   Denotes that a method in a repository service is a queryable method and has a predicate and ordering string criteria.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    internal class RepositoryServiceMethodAttribute : Attribute
    {
        public RepositoryServiceMethodAttribute(string repositoryMemberName, params string[] repositoryMemberParameterTypes)
        {
            RepositoryMethodName = repositoryMemberName;
            RepositoryMethodParameterTypes = repositoryMemberParameterTypes;
        }

        public string RepositoryMethodName { get; set; }

        public string[] RepositoryMethodParameterTypes { get; set; }

        public bool IsQueryable { get; set; }

        public bool IsUpdateOrDeleteMany { get; set; }
    }

    /// <summary>
    ///   Denotes that a type is a repository service and should behave as such.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    internal class RepositoryServiceAttribute : ConcernAttribute
    {
        public RepositoryServiceAttribute()
            : base(typeof(RepositoryServiceBehavior))
        {
        }

        public Type RepositoryType { get; set; }
    }

    /// <summary>
    ///   Responsible for providing the dynamic behavior in repository services.
    /// </summary>
    [SupportsSynchronization]
    internal class RepositoryServiceBehavior : IInterceptor
    {
        private readonly ICloner cloner;
        private readonly IServiceProvider serviceProvider;
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private object repository;
        private Type repositoryType;

        public RepositoryServiceBehavior(IServiceProvider serviceProvider, IUnitOfWorkProvider unitOfWorkProvider, ICloner cloner)
        {
            this.serviceProvider = serviceProvider;
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.cloner = cloner;
        }

        #region IInterceptor Members

        /// <summary>
        ///   Intercepts the specified invocation. Calls the underlying repository instance methods.
        /// </summary>
        /// <param name = "invocation">The invocation.</param>
        public void Intercept(IInvocation invocation)
        {
            EnsureRepository(invocation.Target);

            var attribute = invocation.Method.GetAttribute<RepositoryServiceMethodAttribute>();

            MethodInfo method = repositoryType.GetMembers(true, BindingFlags.Public | BindingFlags.Instance).OfType<MethodInfo>().First(m => m.Name == attribute.RepositoryMethodName && m.GetParameterTypes().SequenceEqual(attribute.RepositoryMethodParameterTypes.Select(p => p.ToType())));

            ParameterInfo[] parameters = method.GetParameters();
            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                object argument = invocation.Arguments[i];
                if (argument != null)
                {
                    Type typeToMatch = attribute.IsUpdateOrDeleteMany ? typeof(IEnumerable<>).MakeGenericType(parameter.ParameterType) : parameter.ParameterType;
                    if (!argument.Is(typeToMatch))
                    {
                        invocation.Arguments[i] = cloner.Clone(argument, typeToMatch);
                    }
                }
            }

            if (attribute.IsQueryable)
            {
                var result = method.GetInvoker()(repository, invocation.Arguments.Take(invocation.Arguments.Length - 3).ToArray()) as IQueryable;

                if (result != null)
                {
                    var predicate = invocation.Arguments[invocation.Arguments.Length - 3] as string;
                    var ordering = invocation.Arguments[invocation.Arguments.Length - 2] as string;
                    var inclusions = invocation.Arguments[invocation.Arguments.Length - 1] as string;
                    if (predicate.IsNotNullOrEmpty())
                    {
                        result = result.Where(predicate);
                    }
                    if (ordering.IsNotNullOrEmpty())
                    {
                        result = result.OrderBy(ordering);
                    }
                    if (inclusions.IsNotNullOrEmpty())
                    {
                        var parsedInclusions = new List<QueryInclusion>();
                        foreach (string inclusion in inclusions.Split(',').Select(i => i.Trim()).WhereNotDefault())
                        {
                            if (inclusion.IsNotNullOrEmpty())
                            {
                                parsedInclusions.Add(Queryables.ParseQueryInclusion(result.ElementType, inclusion));
                            }
                        }
                        result = result.Include(parsedInclusions.ToArray());
                    }

                    invocation.ReturnValue = result.ToInferredElementTypeList();
                }
                else
                {
                    invocation.ReturnValue = null;
                }
            }
            else
            {
                if (attribute.IsUpdateOrDeleteMany)
                {
                    var items = invocation.Arguments[0].CastTo<IEnumerable>();
                    bool isNew;
                    using (IUnitOfWork work = unitOfWorkProvider.GetUnitOfWork(out isNew))
                    {
                        foreach (object item in items)
                        {
                            method.GetInvoker()(repository, item);
                        }
                        work.AcceptChanges();
                    }
                    invocation.ReturnValue = null;
                }
                else
                {
                    object result = method.GetInvoker()(repository, invocation.Arguments.ToArray());

                    invocation.ReturnValue = result;
                }
            }

            if (invocation.ReturnValue != null && !invocation.ReturnValue.GetType().Is(invocation.Method.ReturnType))
            {
                Type sourceType = invocation.ReturnValue.GetType();
                if (ObjectContext.GetKnownProxyTypes().Contains(sourceType))
                {
                    sourceType = sourceType.BaseType;
                }
                invocation.ReturnValue = cloner.Clone(invocation.ReturnValue, invocation.Method.ReturnType, sourceType);
            }
        }

        #endregion

        [Synchronized]
        protected virtual void EnsureRepository(object target)
        {
            repositoryType = repositoryType ?? target.GetType().GetAttribute<RepositoryServiceAttribute>().RepositoryType;
            repository = repository ?? serviceProvider.GetService(repositoryType);
        }
    }
}