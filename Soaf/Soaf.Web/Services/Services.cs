﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Services;
using System.Diagnostics;
using System.IdentityModel.Claims;
using System.IdentityModel.Policy;
using System.Linq;
using System.Reflection;
using System.Security.Authentication;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading;
using System.Transactions;
using System.Web;
using System.Web.ApplicationServices;
using System.Web.Routing;
using System.Xml;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Reflection;
using Soaf.Security;
using Soaf.Web.Client;
using Soaf.Web.Services;
using Soaf.TypeSystem;
using AuthenticationService = System.Web.ApplicationServices.AuthenticationService;
using GenericIdentity = Soaf.Security.GenericIdentity;
using GenericPrincipal = Soaf.Security.GenericPrincipal;
using RequestContext = System.Web.Routing.RequestContext;

[assembly: Component(typeof(ServiceRouteGenerator), typeof(IServiceRouteGenerator))]

namespace Soaf.Web.Services
{
    /// <summary>
    ///   Generates ServiceRoutes for all ServiceContracts.
    /// </summary>
    public interface IServiceRouteGenerator
    {
        IEnumerable<ServiceRoute> GetServiceRoutes();
        void AddServiceRoutes();
    }

    /// <summary>
    ///   Generates ServiceRoutes for all ServiceContracts.
    /// </summary>    
    [Singleton]
    internal class ServiceRouteGenerator : IServiceRouteGenerator
    {
        private readonly Func<Type, ServiceHostFactoryWrapper<ServiceHostFactory>> standardServiceHostFactoryResolver;
        private readonly IComponentRegistry componentRegistry;
        private readonly Func<Type, ServiceHostFactoryWrapper<DataServiceHostFactory>> oDataServiceHostFactoryResolver;
        private readonly IConfigurationContainer configurationContainer;
        private readonly Func<Type, ServiceHostFactoryWrapper<WebScriptServiceHostFactory>> webScriptServiceHostFactoryResolver;
        private readonly Func<Type, ServiceHostFactoryWrapper<WebServiceHostFactory>> webServiceHostFactoryResolver;

        public ServiceRouteGenerator(IComponentRegistry componentRegistry,
                                     Func<Type, ServiceHostFactoryWrapper<ServiceHostFactory>> standardServiceHostFactoryResolver,
                                     Func<Type, ServiceHostFactoryWrapper<WebServiceHostFactory>> webServiceHostFactoryResolver,
                                     Func<Type, ServiceHostFactoryWrapper<WebScriptServiceHostFactory>> webScriptServiceHostFactoryResolver,
                                     Func<Type, ServiceHostFactoryWrapper<DataServiceHostFactory>> oDataServiceHostFactoryResolver,
                                     IConfigurationContainer configurationContainer
            )
        {
            this.componentRegistry = componentRegistry;
            this.standardServiceHostFactoryResolver = standardServiceHostFactoryResolver;
            this.webServiceHostFactoryResolver = webServiceHostFactoryResolver;
            this.webScriptServiceHostFactoryResolver = webScriptServiceHostFactoryResolver;
            this.oDataServiceHostFactoryResolver = oDataServiceHostFactoryResolver;
            this.configurationContainer = configurationContainer;
        }

        #region IServiceRouteGenerator Members

        public IEnumerable<System.ServiceModel.Activation.ServiceRoute> GetServiceRoutes()
        {
            var routes = new List<ServiceRoute>();

            var registeredServices = componentRegistry.ComponentRegistrations
                .SelectMany(r => r.Type
                    .GetTypeBaseTypesAndInterfaces()
                    .Select(t => new
                        {
                            r.Type,
                            Service = r.For
                                // TODO: this will be updated when RemoteServiceEnumerator service is created to identify all services
                                .OrderByDescending(ft => ft.IsAbstract) // Interface/Abstract is more likely to be service
                                .FirstOrDefault()
                                ?? r.Type,
                            Contract = t.GetAttribute<ServiceContractAttribute>(false, false),
                            IsRemoteServiceApplied = r.Concerns.Any(c => c is RemoteServiceAttribute)
                                || r.Type.HasAttribute<RemoteServiceAttribute>()
                        }))
                .Where(i => i.Contract != null
                    || i.IsRemoteServiceApplied
                    || HasServiceConfiguration(i.Service.FullName))
                .Distinct()
                .ToArray();

            foreach (var item in registeredServices)
            {
                var contract = item.Contract;
                var type = item.Type;
                var service = item.Service;
                if (contract == null)
                {
                    type = service = CreateServiceContractType(service);
                    contract = type.GetAttribute<ServiceContractAttribute>();
                }

                foreach (var method in type.GetMethods())
                {
                    ServiceModel.AddServiceMethodKnownTypes(method);
                }

                if (type.IsGenericTypeFor(typeof(System.Data.Services.DataService<>)))
                {
                    routes.Add(new ServiceRoute("Services/OData/{0}".FormatWith(contract.Name ?? type.Name), oDataServiceHostFactoryResolver(service), type));
                }
                else
                {
                    routes.Add(new ServiceRoute("Services/{0}".FormatWith(contract.Name ?? type.Name), standardServiceHostFactoryResolver(service), type));
                    routes.Add(new ServiceRoute("Services/REST/{0}".FormatWith(contract.Name ?? type.Name), webServiceHostFactoryResolver(service), type));
                    routes.Add(new ServiceRoute("Services/AJAX/{0}".FormatWith(contract.Name ?? type.Name), webScriptServiceHostFactoryResolver(service), type));
                }
            }

            AddApplicationServicesRoutes(routes);

            return routes;
        }

        private static Type CreateServiceContractType(Type type)
        {
            var metaType = type.CreateServiceContractMetaType(false);
            metaType.Attributes.Add(new AspNetCompatibilityRequirementsAttribute { RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed });
            metaType.Attributes.Add(new ProxyAttribute { ProxiedType = type });
            metaType.Attributes.Add(new ServiceBehaviorAttribute { TransactionTimeout = "00:20:00" });
            foreach (var method in metaType.Members.OfType<MetaMethod>())
            {
                method.Attributes.Add(new OperationBehaviorAttribute { TransactionAutoComplete = true, TransactionScopeRequired = true });
            }
            return metaType.ToType();
        }

        private bool HasServiceConfiguration(string name)
        {
            var servicesSection = configurationContainer.Configuration.GetSection("system.serviceModel/services") as ServicesSection;

            if (servicesSection != null)
            {
                var result = (from service in servicesSection.Services.OfType<ServiceElement>()
                              where service.Name == name
                              select service).Any();
                return result;
            }
            return false;
        }

        public void AddServiceRoutes()
        {
            IEnumerable<System.ServiceModel.Activation.ServiceRoute> serviceRoutes = GetServiceRoutes();
            serviceRoutes.ForEach(r => RouteTable.Routes.Add(r));
        }

        #endregion

        /// <summary>
        /// Adds the application services routes.
        /// </summary>
        /// <param name="routes">The routes.</param>
        private void AddApplicationServicesRoutes(List<ServiceRoute> routes)
        {
            routes.Add(new ServiceRoute("Services/AuthenticationService", standardServiceHostFactoryResolver(typeof(AuthenticationService)), typeof(WebAuthenticationService)));
            routes.Add(new ServiceRoute("Services/RoleService", standardServiceHostFactoryResolver(typeof(RoleService)), typeof(RoleService)));
            routes.Add(new ServiceRoute("Services/ProfileService", standardServiceHostFactoryResolver(typeof(ProfileService)), typeof(ProfileService)));
        }

        #region Nested type: ServiceRoute

        private class ServiceRoute : System.ServiceModel.Activation.ServiceRoute
        {
            public ServiceRoute(string routePrefix, ServiceHostFactoryBase serviceHostFactory, Type serviceType)
                : base(routePrefix, serviceHostFactory, serviceType)
            {
            }

            public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
            {
                return null;
            }
        }

        #endregion
    }

    /// <summary>
    ///   Creates ServiceHost instances. Applies any default behaviors and searches assemblies when type cannot be found.
    /// </summary>
    internal class ServiceHostFactoryWrapper<TServiceHostFactory> : System.ServiceModel.Activation.ServiceHostFactory
        where TServiceHostFactory : System.ServiceModel.Activation.ServiceHostFactory
    {
        private readonly TServiceHostFactory innerServiceHostFactory;
        private readonly Func<Type, InstanceProvider> instanceProviderResolver;
        private readonly Type componentServiceType;

        public ServiceHostFactoryWrapper(Func<Type, InstanceProvider> instanceProviderResolver, Func<TServiceHostFactory> innerServiceHostFactoryResolver, Type componentServiceType)
        {
            this.instanceProviderResolver = instanceProviderResolver;
            this.componentServiceType = componentServiceType;
            innerServiceHostFactory = innerServiceHostFactoryResolver();
        }

        public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
        {
            Type type = constructorString.ToType();
            if (type != null)
            {
                return CreateServiceHost(type, baseAddresses);
            }

            return innerServiceHostFactory.CreateServiceHost(constructorString, baseAddresses);
        }

        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            ServiceHost serviceHost = Objects.FullAccessDynamicWrapper.For(innerServiceHostFactory).CreateServiceHost(serviceType, baseAddresses);

            if (!serviceHost.Is<DataServiceHost>())
            {
                serviceHost.AddDefaultEndpoints();
                serviceHost.Description.Behaviors.Add(new AddressFilterBehavior());
            }
            serviceHost.Description.Behaviors.Add(new ErrorHandlerBehavior());
            serviceHost.Description.Behaviors.Add(instanceProviderResolver(componentServiceType));

            serviceHost.Authorization.PrincipalPermissionMode = PrincipalPermissionMode.Custom;
            serviceHost.Authorization.ServiceAuthorizationManager = new HttpContextPrincipalAuthorizationManager();


            return serviceHost;
        }
    }

    internal class HttpContextPrincipalAuthorizationManager : ServiceAuthorizationManager
    {
        protected override ReadOnlyCollection<IAuthorizationPolicy> GetAuthorizationPolicies(OperationContext operationContext)
        {
            return new List<IAuthorizationPolicy> { new PrincipalAuthorizationPolicy() }.AsReadOnly();
        }
    }

    public class PrincipalAuthorizationPolicy : IAuthorizationPolicy
    {
        #region IAuthorizationPolicy Members

        public ClaimSet Issuer
        {
            get { return ClaimSet.System; }
        }

        public string Id
        {
            get { return PrincipalContext.Current.Principal.IfNotNull(p => p.Identity.IfNotNull(i => i.Name)) ?? string.Empty; }
        }

        public bool Evaluate(EvaluationContext evaluationContext, ref object state)
        {
            var principal = HttpContext.Current.EnsureNotDefault("HttpContext should not be null upon AuthorizationPolicy.Exucute.")
                .User.EnsureNotDefault("HttpContext.Current.User should not be null upon AuthorizationPolicy.Exucute.");

            evaluationContext.AddClaimSet(this, new DefaultClaimSet(Claim.CreateNameClaim(principal.Identity.Name)));
            evaluationContext.Properties["Identities"] = new List<IIdentity>(new[] { principal.Identity });
            evaluationContext.Properties["Principal"] = principal;
            PrincipalContext.Current.Principal = principal;
            return true;
        }

        #endregion
    }

    /// <summary>
    /// Automatically Traces any errors encountered during operation invocation.
    /// </summary>
    internal class ErrorHandlerBehavior : IServiceBehavior, IErrorHandler
    {
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {

        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {

        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            serviceHostBase.ChannelDispatchers.OfType<ChannelDispatcher>().ForEach(cd => cd.ErrorHandlers.Add(this));
        }

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
        }

        public bool HandleError(Exception error)
        {
            #region helper function getCurrentTransactionInfo

            Func<string> getCurrentTransactionInfo = () =>
                {
                    const string formatStringPrefix = "Current Transaction: ";
                    if (Transaction.Current == null)
                    {
                        return formatStringPrefix + "null";
                    }
                    var distributedId = "{0} (DistributedIdentifier) {1}".FormatWith(formatStringPrefix, Transaction.Current.TransactionInformation.DistributedIdentifier);
                    var localId = "{0} (LocalIdentifer) {1}".FormatWith(formatStringPrefix, Transaction.Current.TransactionInformation.LocalIdentifier);
                    var status = "{0} (Status) {1}".FormatWith(formatStringPrefix, Transaction.Current.TransactionInformation.Status.ToString());
                    var creationTime = "{0} (CreationTime) {1}".FormatWith(formatStringPrefix, Transaction.Current.TransactionInformation.CreationTime);
                    return new[] { distributedId, localId, status, creationTime }.Join(Environment.NewLine);
                };

            #endregion

            Trace.TraceError(new[] { error.ToString(), getCurrentTransactionInfo() }.Join(Environment.NewLine));
            return false;
        }
    }

    /// <summary>
    /// A custom ServiceHostFactory that provides a BasicHttp ServiceHost with additional functionality.
    /// </summary>
    internal class ServiceHostFactory : System.ServiceModel.Activation.ServiceHostFactory
    {
        protected override System.ServiceModel.ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            return new ServiceHost(serviceType, baseAddresses);
        }

        #region Nested type: ServiceHost

        private class ServiceHost : System.ServiceModel.ServiceHost
        {
            public ServiceHost(Type serviceType, Uri[] baseAddresses)
                : base(serviceType, baseAddresses)
            {
            }

            protected override void OnOpening()
            {
                Description.Endpoints.Select(e => e.Contract).Distinct().ForEach(c => c.Behaviors.Add(new DataContractSerializerOperationBehavior()));

                base.OnOpening();
            }

            public override ReadOnlyCollection<ServiceEndpoint> AddDefaultEndpoints()
            {
                var endpoints = base.AddDefaultEndpoints().ToList();

                if (!endpoints.Any(ep => ep.Binding is WSHttpBinding))
                {
                    foreach (var ep in endpoints.ToArray())
                    {
                        var wsHttpBinding = new WSHttpBinding { TransactionFlow = true };
                        if (ep.Address.Uri.Scheme == Uri.UriSchemeHttps)
                        {
                            wsHttpBinding.Security.Mode = SecurityMode.Transport;
                            wsHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                        }
                        else
                        {
                            wsHttpBinding.Security.Mode = SecurityMode.None;
                        }

                        endpoints.Add(AddServiceEndpoint(ep.Contract.ContractType, wsHttpBinding, string.Empty, new Uri("/WsHttp", UriKind.RelativeOrAbsolute)));
                    }
                }

                foreach (var endpoint in endpoints.ToArray())
                {
                    endpoints.Add(AddBinaryEndpoint(endpoint));
                    endpoints.Add(AddProtobufEndpoint(endpoint));
                }

                ApplyCommonEndpointsConfiguration(endpoints);

                return endpoints.ToList().AsReadOnly();
            }

            private void ApplyCommonEndpointsConfiguration(List<ServiceEndpoint> endpoints)
            {
                foreach (var endpoint in endpoints)
                {
                    // Breakdown binding for configuration
                    var elements = endpoint.Binding.CreateBindingElements().ToArray();

                    SetMaxValues(elements);
                    SetTransportStreamed(elements);

                    // Re-construct binding
                    endpoint.Binding = new CustomBinding(elements)
                                       {
                                           CloseTimeout = TimeSpan.FromMinutes(20),
                                           OpenTimeout = TimeSpan.FromMinutes(20),
                                           ReceiveTimeout = TimeSpan.FromMinutes(20),
                                           SendTimeout = TimeSpan.FromMinutes(20)
                                       };
                }

                endpoints
                    .SelectMany(ep => ep.Contract.Operations)
                    .Distinct()
                    .ForEach(o => o.Behaviors.Add(new TransactionSuppressingOperationInvokerBehavior()));

                endpoints
                   .SelectMany(ep => ep.Contract.Operations)
                   .Distinct()
                   .ForEach(o => o.Behaviors.Add(new PrincipalVerifyingOperationInvokerBehavior()));

                endpoints.ForEach(ep => ep.Behaviors.Add(new ContextDataInitializationBehavior()));
            }

            /// <summary>
            /// Enables streaming for supported transport elements
            /// </summary>
            /// <param name="bindingElements"></param>
            private void SetTransportStreamed(IEnumerable<BindingElement> bindingElements)
            {
                var httpTransportElement = bindingElements.OfType<HttpTransportBindingElement>().FirstOrDefault();
                if (httpTransportElement != null)
                {
                    // TODO: switch to .NET 4.5 for optimal results under IIS (see http://blogs.microsoft.co.il/idof/2012/01/17/whats-new-in-wcf-45-improved-streaming-in-iis-hosting/) 
                    httpTransportElement.TransferMode = TransferMode.Streamed;
                }
            }

            /// <summary>
            /// Applies max transport and encoding values.
            /// </summary>
            /// <param name="elements">Endpoint binding elements.</param>
            private void SetMaxValues(BindingElement[] elements)
            {
                var transportElement = elements.OfType<TransportBindingElement>().FirstOrDefault();
                if (transportElement != null)
                {
                    transportElement.MaxBufferPoolSize = int.MaxValue;
                    transportElement.MaxReceivedMessageSize = int.MaxValue;
                }

                var textMessageEncodingBindingElement = elements.OfType<TextMessageEncodingBindingElement>().FirstOrDefault();
                if (textMessageEncodingBindingElement != null)
                {
                    textMessageEncodingBindingElement.MaxReadPoolSize = int.MaxValue;
                    textMessageEncodingBindingElement.MaxWritePoolSize = int.MaxValue;
                    textMessageEncodingBindingElement.ReaderQuotas = XmlDictionaryReaderQuotas.Max;
                }

                var binaryMessageEncodingBindingElement = elements.OfType<BinaryMessageEncodingBindingElement>().FirstOrDefault();
                if (binaryMessageEncodingBindingElement != null)
                {
                    binaryMessageEncodingBindingElement.MaxReadPoolSize = int.MaxValue;
                    binaryMessageEncodingBindingElement.MaxSessionSize = int.MaxValue;
                    binaryMessageEncodingBindingElement.MaxWritePoolSize = int.MaxValue;
                    binaryMessageEncodingBindingElement.ReaderQuotas = XmlDictionaryReaderQuotas.Max;
                }
            }

            /// <summary>
            /// Adds an endpoint identical to the specified endpoint, but using binary message encoding and at the address /Binary.
            /// </summary>
            /// <param name="endpoint"></param>
            /// <returns></returns>
            private ServiceEndpoint AddBinaryEndpoint(ServiceEndpoint endpoint)
            {
                var bindingElements = endpoint.Binding.CreateBindingElements().Select(e => e.Clone()).ToList();

                var encodingBindingElement = bindingElements.OfType<MessageEncodingBindingElement>().FirstOrDefault();

                if (encodingBindingElement == null)
                    bindingElements.Add(CreateBinaryEncodingBindingElement());
                else
                    bindingElements.Replace(encodingBindingElement, CreateBinaryEncodingBindingElement());

                var binaryEndpoint = AddServiceEndpoint(endpoint.Contract.ContractType, new CustomBinding(bindingElements), string.Empty, new Uri(endpoint.ListenUri + "/Binary", UriKind.RelativeOrAbsolute));

                return binaryEndpoint;
            }

            private BindingElement CreateBinaryEncodingBindingElement()
            {
                return new BinaryMessageEncodingBindingElement { ReaderQuotas = XmlDictionaryReaderQuotas.Max, MaxReadPoolSize = int.MaxValue, MaxSessionSize = int.MaxValue, MaxWritePoolSize = int.MaxValue };
            }

            /// <summary>
            /// Adds an endpoint identical to the specified endpoint, but using protobuf message encoding and at the address /Protobuf
            /// </summary>
            /// <param name="endpoint"></param>
            /// <returns></returns>
            private ServiceEndpoint AddProtobufEndpoint(ServiceEndpoint endpoint)
            {
                ProtoBuf.Meta.RuntimeTypeModel.Default.InferTagFromNameDefault = true;
                ProtoBuf.Meta.RuntimeTypeModel.Default.AutoAddProtoContractTypesOnly = true;

                var protobufEndpoint = AddServiceEndpoint(endpoint.Contract.ContractType, new CustomBinding(endpoint.Binding.CreateBindingElements()), string.Empty, new Uri(endpoint.ListenUri + "/Protobuf", UriKind.RelativeOrAbsolute));

                protobufEndpoint.Behaviors.Add(new ProtoEndpointBehavior());

                return protobufEndpoint;
            }
        }

        #endregion
    }

    /// <summary>
    /// A custom factory for creating WebServiceHosts with additional functionality.
    /// </summary>
    internal class WebServiceHostFactory : System.ServiceModel.Activation.WebServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            return new WebServiceHost(serviceType, baseAddresses);
        }

        #region Nested type: WebServiceHost

        /// <summary>
        /// A custom WebServiceHost that does not automatically add a BasicHttpEndpoint
        /// </summary>
        private class WebServiceHost : System.ServiceModel.Web.WebServiceHost
        {
            public WebServiceHost(Type serviceType, Uri[] baseAddresses)
                : base(serviceType, baseAddresses)
            {
            }

            public override ReadOnlyCollection<ServiceEndpoint> AddDefaultEndpoints()
            {
                ReadOnlyCollection<ServiceEndpoint> result = base.AddDefaultEndpoints();

                result.ForEach(i =>
                {
                    var securityMode = i.Address.Uri.Scheme == Uri.UriSchemeHttps
                                            ? WebHttpSecurityMode.Transport
                                            : WebHttpSecurityMode.None;
                    i.Binding = new WebHttpBinding(securityMode);
                });

                return result;
            }

            protected override void OnOpening()
            {
                Description.Endpoints.ForEach(ep => ep.Behaviors.Add(new WebHttpBehavior()));
                base.OnOpening();
            }

            #region Nested type: WebHttpBehavior

            private class WebHttpBehavior : System.ServiceModel.Description.WebHttpBehavior
            {
                public WebHttpBehavior()
                {
                    AutomaticFormatSelectionEnabled = true;
                    HelpEnabled = true;
                }
            }

            #endregion
        }

        #endregion
    }

    /// <summary>
    /// Applies a MatchAllMessageFilter to all endpoints. Equivalent to setting [ServiceBehavior(AddressFilterMode=AddressFilterMode.Any)].
    /// </summary>
    internal class AddressFilterBehavior : IServiceBehavior
    {
        #region IServiceBehavior Members

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            serviceHostBase.ChannelDispatchers.OfType<ChannelDispatcher>()
                .SelectMany(d => d.Endpoints).ForEach(ep =>
                                                      ep.AddressFilter = new MatchAllMessageFilter());
        }

        #endregion
    }

    /// <summary>
    /// Instantiates service instances using the Resolver, so they support dependency injection and proxying.
    /// </summary>
    internal class InstanceProvider : IInstanceProvider, IContractBehavior, IServiceBehavior
    {
        private readonly Func<object> activator;
        private object instance;

        public InstanceProvider(Type type, IServiceProvider activator)
        {
            this.activator = () => activator.GetService(type);
        }

        #region IContractBehavior Members

        public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        {
        }

        public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        {
            dispatchRuntime.InstanceProvider = this;
        }

        public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        #endregion

        #region IInstanceProvider Members

        public object GetInstance(InstanceContext instanceContext)
        {
            return instance ?? (instance = activator());
        }

        public object GetInstance(InstanceContext instanceContext, Message message)
        {
            return GetInstance(instanceContext);
        }

        public void ReleaseInstance(InstanceContext instanceContext, object instance)
        {
            this.instance = null;
        }

        #endregion

        #region IServiceBehavior Members

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            serviceDescription.Endpoints.Select(ep => ep.Contract).Distinct().ForEach(c => c.Behaviors.Add(this));
        }

        #endregion
    }

    internal class ContextDataInitializationBehavior : IDispatchMessageInspector, IEndpointBehavior
    {
        public void Validate(ServiceEndpoint endpoint)
        {
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(this);
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            ContextData.LoadFromOperationContext();

            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            ContextData.Clear();
        }
    }


    /// <summary>
    /// Verifies HttpContext.Current.User matches Thread.CurrentPrincipal.
    /// </summary>
    internal class PrincipalVerifyingOperationInvokerBehavior : IOperationBehavior
    {
        public void Validate(OperationDescription operationDescription)
        {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            dispatchOperation.Invoker = new PrincipalVerifyingOperationInvoker(dispatchOperation.Invoker);
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }
    }

    /// <summary>
    /// Verifies HttpContext.Current.User matches Thread.CurrentPrincipal.
    /// </summary>
    internal class PrincipalVerifyingOperationInvoker : IOperationInvoker
    {
        private readonly IOperationInvoker innerInvoker;

        public PrincipalVerifyingOperationInvoker(IOperationInvoker innerInvoker)
        {
            this.innerInvoker = innerInvoker;
        }

        public object[] AllocateInputs()
        {
            return innerInvoker.AllocateInputs();
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            ValidatePrincipal(instance);

            return innerInvoker.Invoke(instance, inputs, out outputs);
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            ValidatePrincipal(instance);

            return innerInvoker.InvokeBegin(instance, inputs, callback, state);
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            ValidatePrincipal(instance);

            return innerInvoker.InvokeEnd(instance, out outputs, result);
        }

        public bool IsSynchronous
        {
            get { return innerInvoker.IsSynchronous; }
        }

        private static void ValidatePrincipal(object instance)
        {
            try
            {
                var serviceName = instance == null ? "Service null" : "Service " + instance.GetType().FullName;
                var warningMessage = string.Format("{0} Principal is not valid. Details: {1}",
                    serviceName, Environment.NewLine);


                if (
                    // 1. Nothing should be null by the time we invoke operation
                    HttpContext.Current == null
                    || HttpContext.Current.User == null
                    || HttpContext.Current.User.Identity == null
                    || Thread.CurrentPrincipal == null
                    || Thread.CurrentPrincipal.Identity == null
                    // 2. Principal must be same object (we ensure that in PrincipalAuthorizationPolicy class)
                    || !ReferenceEquals(HttpContext.Current.User, Thread.CurrentPrincipal))
                {
                    throw new AuthenticationException(warningMessage + DescribeCurrentContextsValues());
                }
            }
            catch (AuthenticationException)
            {
                // Rethrow authentication exceptions
                throw;
            }
            catch (Exception ex)
            {
                // Log any other types of errors
                Trace.TraceError(ex.ToString());
            }
        }

        /// <summary>
        /// Describes passed instance of a principal
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="principal"></param>
        /// <returns></returns>
        private static string DescribePrincipal(string prefix, IPrincipal principal)
        {
            var result = new StringBuilder();
            if (principal == null)
            {
                result.AppendLine(prefix + ": null");
            }
            else if (principal.Identity == null)
            {
                result.AppendLine(prefix + ".Identity: null");
            }
            else
            {
                result.AppendLine(string.Format("{0}.Identity.IsAuthenticated: {1}", prefix, principal.Identity.IsAuthenticated));
                result.AppendLine(string.Format("{0}.Identity.Name: {1}", prefix, principal.Identity.Name));
                result.AppendLine(string.Format("{0}.Identity.AuthenticationType: {1}", prefix, principal.Identity.AuthenticationType));
                result.AppendLine(string.Format("{0}.Identity (Type): {1}", prefix, principal.Identity.GetType().FullName));
            }

            return result.ToString();
        }

        /// <summary>
        /// Describes current values of both contexts
        /// </summary>
        /// <returns></returns>
        private static string DescribeCurrentContextsValues()
        {
            var result = new StringBuilder();
            var httpUser = HttpContext.Current.IfNotNull(c => c.User);
            var currentPrincipal = Thread.CurrentPrincipal;

            // Describe http context user
            result.AppendLine(HttpContext.Current == null 
                ? "HttpContext.Current: null"
                : DescribePrincipal("HttpContext.Current.User", httpUser));

            // Describe thread current principal
            result.AppendLine(DescribePrincipal("Thread.CurrentPrincipal", currentPrincipal));

            // Check whether reference equals
            result.AppendFormat("ReferenceEquals(HttpContext.Current.User, Thread.CurrentPrincipal): {0}",
                ReferenceEquals(httpUser, currentPrincipal));

            return result.ToString();
        }
    }


    /// <summary>
    /// Suppresses transactions for non DTC operations.
    /// </summary>
    internal class TransactionSuppressingOperationInvokerBehavior : IOperationBehavior
    {
        public void Validate(OperationDescription operationDescription)
        {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            dispatchOperation.Invoker = new TransactionSuppressingOperationInvoker(dispatchOperation.Invoker);
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }
    }

    /// <summary>
    /// Suppresses transactions for non DTC operations.
    /// </summary>
    internal class TransactionSuppressingOperationInvoker : IOperationInvoker
    {
        private readonly IOperationInvoker innerInvoker;

        public TransactionSuppressingOperationInvoker(IOperationInvoker innerInvoker)
        {
            this.innerInvoker = innerInvoker;
        }

        public object[] AllocateInputs()
        {
            return innerInvoker.AllocateInputs();
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            if (Transaction.Current != null && Transaction.Current.TransactionInformation.DistributedIdentifier == default(Guid))
            {
                using (new TransactionScope(TransactionScopeOption.Suppress))
                {
                    return innerInvoker.Invoke(instance, inputs, out outputs);
                }
            }
            return innerInvoker.Invoke(instance, inputs, out outputs);
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            if (Transaction.Current != null && Transaction.Current.TransactionInformation.DistributedIdentifier == default(Guid))
            {
                using (new TransactionScope(TransactionScopeOption.Suppress))
                {
                    return innerInvoker.InvokeBegin(instance, inputs, callback, state);
                }
            }
            return innerInvoker.InvokeBegin(instance, inputs, callback, state);
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            if (Transaction.Current != null && Transaction.Current.TransactionInformation.DistributedIdentifier == default(Guid))
            {
                using (new TransactionScope(TransactionScopeOption.Suppress))
                {
                    return innerInvoker.InvokeEnd(instance, out outputs, result);
                }
            }
            return innerInvoker.InvokeEnd(instance, out outputs, result);
        }

        public bool IsSynchronous
        {
            get { return innerInvoker.IsSynchronous; }
        }
    }

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceContract(Namespace = "http://asp.net/ApplicationServices/v200", Name = "AuthenticationService")]
    [ServiceBehavior(Namespace = "http://asp.net/ApplicationServices/v200", InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, TransactionTimeout = "00:20:00", ReleaseServiceInstanceOnTransactionComplete = false)]
    public class WebAuthenticationService : Security.AuthenticationService
    {
        private readonly AuthenticationService msAuthenticationService;

        public WebAuthenticationService()
        {
            msAuthenticationService = (AuthenticationService)typeof(AuthenticationService).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public, null, Type.EmptyTypes, null).Invoke(new object[0]);
        }

        [OperationContract]
        [OperationBehavior(TransactionScopeRequired = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        public override bool IsLoggedIn()
        {
            return base.IsLoggedIn() && msAuthenticationService.IsLoggedIn();
        }

        [OperationContract]
        [OperationBehavior(TransactionScopeRequired = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        public override bool Login(string username, string password, string customCredential, bool isPersistent)
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null && !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(username));
            }

            return base.Login(username, password, customCredential, isPersistent) && msAuthenticationService.Login(username, password, customCredential, isPersistent);
        }

        [OperationContract]
        [OperationBehavior(TransactionScopeRequired = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        public override void Logout()
        {
            HttpContext.Current.Cache.Remove(UserPrincipalBootstrapper.CacheKeyPrefix + HttpContext.Current.User.Identity.Name);
            base.Logout();
            msAuthenticationService.Logout();
        }

        [OperationContract]
        [OperationBehavior(TransactionScopeRequired = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        public override bool ValidateUser(string username, string password, string customCredential)
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null && !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(username));
            }

            return base.ValidateUser(username, password, customCredential) && msAuthenticationService.ValidateUser(username, password, customCredential);
        }

        [OperationContract]
        [OperationBehavior(TransactionScopeRequired = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        public override bool ValidateAssemblyNameAndVersion(string assemblyFullName)
        {
            try
            {
                var asm = Assembly.Load(assemblyFullName);
                var name = new AssemblyName(assemblyFullName);
                return asm != null && asm.GetName().Version.ToString().Equals(name.Version.ToString(), StringComparison.OrdinalIgnoreCase);
            }
            catch
            {
                return false;
            }
        }
    }
}