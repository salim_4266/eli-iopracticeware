﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Data.Services.Providers;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web.Hosting;
using Soaf.Collections;
using Soaf.ComponentModel;
using Soaf.Domain;
using Soaf.Logging;
using Soaf.Reflection;
using Soaf.Threading;
using Soaf.TypeSystem;
using Soaf.Validation;
using Soaf.Web.Services;

[assembly: Component(typeof(RepositoryODataServiceFactory), typeof(IRepositoryODataServiceFactory), Initialize = true)]

namespace Soaf.Web.Services
{
    /// <summary>
    /// Metadata for context of type T.
    /// </summary>
    internal class DataServiceProviderMetadata<T>
    {
        private readonly IDictionary<Type, ResourceType> resourceTypeMap = new Dictionary<Type, ResourceType>();

        private readonly List<ResourceSet> resourceSets = new List<ResourceSet>();
        private readonly List<ServiceOperation> serviceOperations = new List<ServiceOperation>();

        public DataServiceProviderMetadata()
        {
            ContainerName = typeof(T).GetAttribute<RepositoryAttribute>().IfNotNull(i => i.Name);
            if (ContainerName.IsNullOrEmpty())
            {
                ContainerName = typeof(T).GetAttribute<ServiceContractAttribute>().IfNotNull(i => i.Name);
            }
            if (ContainerName.IsNullOrEmpty())
            {
                ContainerName = typeof(T).Name;
            }

            ContainerNamespace = typeof(T).GetAttribute<RepositoryAttribute>().IfNotNull(i => i.Namespace);
            if (ContainerNamespace.IsNullOrEmpty())
            {
                ContainerNamespace = typeof(T).GetAttribute<ServiceContractAttribute>().IfNotNull(i => i.Namespace);
            }
            if (ContainerNamespace.IsNullOrEmpty())
            {
                ContainerNamespace = typeof(T).Namespace;
            }

            AddResourceSets();
            AddServiceOperations();

            resourceSets.ForEach(r => r.ResourceType.PropertiesDeclaredOnThisType.OfType<AssociationSetResourceProperty>().ForEach(p => CreateAssociation(r, p)));

            resourceSets.ForEach(r => r.SetReadOnly());
            ResourceTypes.ForEach(r => r.SetReadOnly());
            ResourceTypes.SelectMany(r => r.Properties).ForEach(p => p.SetReadOnly());
            serviceOperations.ForEach(o => o.SetReadOnly());
        }

        public string ContainerName { get; private set; }
        public string ContainerNamespace { get; private set; }

        public IEnumerable<ResourceType> ResourceTypes
        {
            get { return resourceTypeMap.Values; }
        }

        public IEnumerable<ResourceSet> ResourceSets
        {
            get { return resourceSets.ToList().AsReadOnly(); }
        }

        public IEnumerable<ServiceOperation> ServiceOperations
        {
            get { return serviceOperations.ToList().AsReadOnly(); }
        }

        /// <summary>
        /// Adds the service operations for all custom methods.
        /// </summary>
        private void AddServiceOperations()
        {
            typeof(T)
                .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(m =>
                       !m.IsSpecialName && m.DeclaringType == typeof(T) &&
                       !Domain.Domain.IsSaveOrDeleteMethod(m, Domain.Domain.SaveMethodName) && !Domain.Domain.IsSaveOrDeleteMethod(m, Domain.Domain.DeleteMethodName) &&
                       (!typeof(T).HasAttribute<ServiceContractAttribute>() || m.HasAttribute<OperationContractAttribute>()))
                .Select(GetServiceOperation).WhereNotDefault().ToList().ForEach(i => serviceOperations.Add(i));
        }

        /// <summary>
        /// Adds the resource sets for all IQueryable properties.
        /// </summary>
        private void AddResourceSets()
        {
            GetQueryableProperties().Select(i => new { i.Name, ResourceType = GetResourceType(i.PropertyType.GetGenericArguments().First()) })
                .Where(i => i.ResourceType != null)
                .Select(i => new ResourceSet(i.Name, i.ResourceType)).ToList().ForEach(i => resourceSets.Add(i));
        }

        /// <summary>
        /// Creates the service operation for the method specified.
        /// </summary>
        /// <param name="methodInfo">The method info.</param>
        /// <returns></returns>
        private ServiceOperation GetServiceOperation(MethodInfo methodInfo)
        {
            if (methodInfo.ReturnType.IsEnum) return null;

            string name = methodInfo.GetAttribute<OperationContractAttribute>().IfNotNull(i => i.Name);
            if (name.IsNullOrEmpty())
            {
                name = methodInfo.Name;
            }

            ServiceOperationResultKind resultKind = ServiceOperationResultKind.DirectValue;
            if (methodInfo.ReturnType.IsGenericTypeFor(typeof(IQueryable<>)))
            {
                resultKind = ServiceOperationResultKind.QueryWithMultipleResults;
            }
            else if (methodInfo.ReturnType.IsGenericTypeFor(typeof(IEnumerable<>)))
            {
                resultKind = ServiceOperationResultKind.Enumeration;
            }
            else if (methodInfo.ReturnType == typeof(void))
            {
                resultKind = ServiceOperationResultKind.Void;
            }

            ResourceType resultType = GetPrimitiveResourceType(methodInfo.ReturnType) ?? GetResourceType(new[] { ServiceOperationResultKind.Enumeration, ServiceOperationResultKind.QueryWithMultipleResults }.Contains(resultKind) ? methodInfo.ReturnType.FindElementType() : methodInfo.ReturnType);
            if (resultType == null) return null;

            ResourceSet resultSet = ResourceSets.FirstOrDefault(i => i.ResourceType == resultType);
            string operationHttpMethod = GetOperationHttpMethod(methodInfo);
            var operation = new MethodServiceOperation(name, resultKind, resultType, resultSet, operationHttpMethod, GetServiceOperationParameters(methodInfo), methodInfo);
            return operation;
        }

        private static ResourceType GetPrimitiveResourceType(Type type)
        {
            var resourceType = ResourceType.GetPrimitiveResourceType(type);
            return resourceType;
        }

        /// <summary>
        /// Gets the operation HTTP method. Chooses GET by default unless WebInvoke attribute is present.
        /// </summary>
        /// <param name="methodInfo">The method info.</param>
        /// <returns></returns>
        private string GetOperationHttpMethod(MethodInfo methodInfo)
        {
            var webInvokeAttribute = methodInfo.GetAttribute<WebInvokeAttribute>();
            if (webInvokeAttribute != null)
            {
                return "POST";
            }
            return "GET";
        }

        /// <summary>
        /// Gets the service operation parameters for the specified method.
        /// </summary>
        /// <param name="methodInfo">The method info.</param>
        /// <returns></returns>
        private IEnumerable<ServiceOperationParameter> GetServiceOperationParameters(MethodInfo methodInfo)
        {
            return methodInfo.GetParameters()
                .Select(pi => new { pi.Name, ResourceType = GetPrimitiveResourceType(pi.ParameterType) ?? GetResourceType(pi.ParameterType) })
                .Where(i => i.ResourceType != null)
                .Select(i => new ServiceOperationParameter(i.Name, i.ResourceType));
        }

        /// <summary>
        /// Gets the queryable properties on context of type T.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<PropertyInfo> GetQueryableProperties()
        {
            return typeof(T).GetProperties().Where(i => i.CanRead && i.GetIndexParameters().Length == 0 && i.PropertyType.EqualsGenericTypeFor(typeof(IQueryable<>)));
        }

        /// <summary>
        /// Gets the queryable property element types on context of type T.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Type> GetQueryablePropertyElementTypes()
        {
            return GetQueryableProperties().Select(pi => pi.PropertyType.GetGenericArguments().First());
        }

        /// <summary>
        /// Gets or creates a ResourceType for the system Type specified..
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private ResourceType GetResourceType(Type type)
        {
            if (type.IsValueType) return null;

            ResourceType resourceType;

            if (resourceTypeMap.TryGetValue(type, out resourceType))
            {
                return resourceType;
            }

            string name = type.GetAttribute<DataContractAttribute>().IfNotNull(a => a.Name);
            if (name.IsNullOrEmpty())
            {
                name = type.Name;
            }

            string ns = type.GetAttribute<DataContractAttribute>().IfNotNull(a => a.Namespace).IfNotNull(s => s.Replace("http://", string.Empty).Replace("/", "."));
            if (ns.IsNullOrEmpty())
            {
                ns = type.Namespace;
            }

            resourceType = new ResourceType(type, GetQueryablePropertyElementTypes().Contains(type) ? ResourceTypeKind.EntityType : ResourceTypeKind.ComplexType,
                                            type.BaseType == typeof(object) || type.BaseType == null ? null : GetResourceType(type.BaseType),
                                            ns, name, type.IsAbstract);

            resourceTypeMap[type] = resourceType;

            GetResourceProperties(resourceType).WhereNotDefault().ForEach(resourceType.AddProperty);

            return resourceType;
        }

        /// <summary>
        /// Creates the resource properties for a resource type based on the system type's public properties.
        /// </summary>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        private IEnumerable<ResourceProperty> GetResourceProperties(ResourceType resourceType)
        {
            return resourceType.InstanceType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).Where(pi => pi.CanRead && pi.GetGetMethod().GetBaseDefinition() == pi.GetGetMethod()).Select(pi => GetResourceProperty(pi, resourceType)).WhereNotDefault();
        }

        /// <summary>
        /// Creates a resource property for the specified PropertyInfo.
        /// </summary>
        /// <param name="pi">The pi.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        private ResourceProperty GetResourceProperty(PropertyInfo pi, ResourceType resourceType)
        {
            if (pi.PropertyType.IsEnum) return null;

            ResourcePropertyKind kind;
            ResourceType propertyResourceType = GetResourcePropertyResourceType(pi, resourceType, out kind);

            if (propertyResourceType == null) return null;

            if (kind == ResourcePropertyKind.ResourceSetReference || kind == ResourcePropertyKind.ResourceReference)
            {
                // WCF Data Services does not currently support navigation properties on derived types.
                if (resourceType.BaseType != null) return null;

                return new AssociationSetResourceProperty(pi.Name, kind, propertyResourceType);
            }
            return new ResourceProperty(pi.Name, kind, propertyResourceType);
        }

        /// <summary>
        /// Creates an association for a ResourceProperty that models an association.
        /// </summary>
        /// <param name="resourceSet">The resource set.</param>
        /// <param name="resourceProperty">The resource property.</param>
        private void CreateAssociation(ResourceSet resourceSet, AssociationSetResourceProperty resourceProperty)
        {
            ResourceSet targetResourceSet = ResourceSets.First(i => i.ResourceType == resourceProperty.ResourceType);

            resourceProperty.ResourceAssociationSet = new ResourceAssociationSet(
                string.Format("{0}_{1}",
                              resourceSet.Name,
                              resourceProperty.Name),
                new ResourceAssociationSetEnd(
                    resourceSet,
                    resourceSet.ResourceType,
                    resourceProperty),
                new ResourceAssociationSetEnd(
                    targetResourceSet,
                    targetResourceSet.ResourceType,
                    null));
        }

        /// <summary>
        /// Gets the ResourceType corresponding to a PropertyInfo.
        /// </summary>
        /// <param name="pi">The pi.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <param name="kind">The kind.</param>
        /// <returns></returns>
        private ResourceType GetResourcePropertyResourceType(PropertyInfo pi, ResourceType resourceType, out ResourcePropertyKind kind)
        {
            ResourceType propertyResourceType = GetPrimitiveResourceType(pi.PropertyType);
            if (propertyResourceType != null)
            {
                kind = ResourcePropertyKind.Primitive;
                if (resourceType.ResourceTypeKind == ResourceTypeKind.EntityType && pi.DeclaringType != null && (IsPropertyKeyProperty(pi) || !pi.DeclaringType.GetProperties().Any(IsPropertyKeyProperty)))
                {
                    kind |= ResourcePropertyKind.Key;
                }
            }
            else if (pi.PropertyType.IsGenericTypeFor(typeof(IEnumerable<>)) && GetPrimitiveResourceType(pi.PropertyType.FindElementType()) == null)
            {
                propertyResourceType = GetResourceType(pi.PropertyType.FindElementType());
                kind = propertyResourceType.ResourceTypeKind == ResourceTypeKind.EntityType ? ResourcePropertyKind.ResourceSetReference : ResourcePropertyKind.ComplexType;
            }
            else
            {
                propertyResourceType = GetResourceType(pi.PropertyType);
                kind = propertyResourceType == null || propertyResourceType.ResourceTypeKind == ResourceTypeKind.ComplexType ? ResourcePropertyKind.ComplexType : ResourcePropertyKind.ResourceReference;
            }

            return propertyResourceType;
        }

        /// <summary>
        /// Determines whether the PropertyInfo should be treated as a key property kind.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <returns>
        ///   <c>true</c> if [is property key property] [the specified property]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsPropertyKeyProperty(PropertyInfo property)
        {
            if (property.DeclaringType.GetAttribute<DataServiceKeyAttribute>().IfNotNull(a => a.KeyNames.Contains(property.Name))) return true;

            if (GetPrimitiveResourceType(property.PropertyType) != null && !property.PropertyType.IsGenericType)
            {
                if (property.DeclaringType != null && property.Name.Equals(property.DeclaringType.Name + "id", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
                if (property.Name.Equals("id", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// A special type of ResourceProperty that is a ResourceSet association.
    /// </summary>
    internal class AssociationSetResourceProperty : ResourceProperty
    {
        public AssociationSetResourceProperty([NotNull] string name, ResourcePropertyKind kind, [NotNull] ResourceType propertyResourceType)
            : base(name, kind, propertyResourceType)
        {
        }

        public ResourceAssociationSet ResourceAssociationSet { get; set; }
    }

    /// <summary>
    /// A service operation that represents a system MethodInfo on the context of type T.
    /// </summary>
    internal class MethodServiceOperation : ServiceOperation
    {
        public MethodServiceOperation([NotNull] string name, ServiceOperationResultKind resultKind, ResourceType resultType, ResourceSet resultSet, [NotNull] string method, IEnumerable<ServiceOperationParameter> parameters, MethodInfo methodInfo)
            : base(name, resultKind, resultType, resultSet, method, parameters)
        {
            MethodInfo = methodInfo;
        }

        public MethodInfo MethodInfo { get; private set; }
    }

    /// <summary>
    /// A custom IDataServiceQueryProvider compatible with Soaf Repositories and other constructs.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class DataServiceProvider<T> : IDataServiceQueryProvider, IDataServiceMetadataProvider, IDataServiceUpdateProvider
    {
        public static readonly DataServiceProviderMetadata<T> Metadata = new DataServiceProviderMetadata<T>();

        private readonly HashSet<object> forDelete = new HashSet<object>();
        private readonly HashSet<object> forSave = new HashSet<object>();
        private readonly IUnitOfWorkProvider unitOfWorkProvider;

        public DataServiceProvider(IUnitOfWorkProvider unitOfWorkProvider)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
        }

        #region IDataServiceMetadataProvider Members

        public bool TryResolveResourceSet(string name, out ResourceSet resourceSet)
        {
            resourceSet = Metadata.ResourceSets.FirstOrDefault(i => i.Name == name);
            return resourceSet != null;
        }

        public ResourceAssociationSet GetResourceAssociationSet(ResourceSet resourceSet, ResourceType resourceType, ResourceProperty resourceProperty)
        {
            return
                // WCF Data Services does not currently support navigation properties on derived types.
                !resourceSet.ResourceType.PropertiesDeclaredOnThisType.Contains(resourceProperty) ? null :
                resourceProperty.As<AssociationSetResourceProperty>().IfNotNull(i => i.ResourceAssociationSet);
        }

        public bool TryResolveResourceType(string name, out ResourceType resourceType)
        {
            resourceType = Types.FirstOrDefault(i => i.Name == name);
            return resourceType != null;
        }

        public IEnumerable<ResourceType> GetDerivedTypes(ResourceType resourceType)
        {
            return Types.Where(i => i.BaseType == resourceType);
        }

        public bool HasDerivedTypes(ResourceType resourceType)
        {
            return Types.Any(i => i.BaseType == resourceType);
        }

        public bool TryResolveServiceOperation(string name, out ServiceOperation serviceOperation)
        {
            serviceOperation = ServiceOperations.FirstOrDefault(i => i.Name == name);
            return serviceOperation != null;
        }

        public string ContainerNamespace
        {
            get { return Metadata.ContainerNamespace; }
        }

        public string ContainerName
        {
            get { return Metadata.ContainerName; }
        }

        public IEnumerable<ResourceSet> ResourceSets
        {
            get { return Metadata.ResourceSets; }
        }

        public IEnumerable<ResourceType> Types
        {
            get { return Metadata.ResourceTypes; }
        }

        public IEnumerable<ServiceOperation> ServiceOperations
        {
            get { return Metadata.ServiceOperations; }
        }

        #endregion

        #region IDataServiceQueryProvider Members

        public object CurrentDataSource { get; set; }

        public object GetOpenPropertyValue(object target, string propertyName)
        {
            return target.GetType().GetProperty(propertyName).GetValue(target, null);
        }

        public IEnumerable<KeyValuePair<string, object>> GetOpenPropertyValues(object target)
        {
            return target.GetType().GetProperties().Where(pi => pi.CanRead && pi.GetIndexParameters().Length == 0)
                .Select(pi => new KeyValuePair<string, object>(pi.Name, pi.GetValue(target, null)));
        }

        public object GetPropertyValue(object target, ResourceProperty resourceProperty)
        {
            return target.GetType().GetProperty(resourceProperty.Name).GetValue(target, null);
        }

        public IQueryable GetQueryRootForResourceSet(ResourceSet resourceSet)
        {
            return (IQueryable)CurrentDataSource.GetType().GetProperty(resourceSet.Name).GetValue(CurrentDataSource, null);
        }

        public ResourceType GetResourceType(object target)
        {
            Type type = target.GetType().FindElementType() ?? target.GetType();
            return Metadata.ResourceTypes.Single(t => t.InstanceType == (type.Assembly.IsDynamic ? type.BaseType : type));
        }

        public object InvokeServiceOperation(ServiceOperation serviceOperation, object[] parameters)
        {
            return serviceOperation.CastTo<MethodServiceOperation>().MethodInfo.Invoke(CurrentDataSource, parameters);
        }

        public bool IsNullPropagationRequired
        {
            get { return true; }
        }

        #endregion

        #region IDataServiceUpdateProvider Members

        public object CreateResource(string containerName, string fullTypeName)
        {
            return fullTypeName.ToType().CreateInstance();
        }

        public object GetResource(IQueryable query, string fullTypeName)
        {
            object resource = null;

            foreach (object o in query)
            {
                if (resource != null)
                {
                    throw new Exception("Expected a single response.");
                }
                resource = o;
            }

            // fullTypeName can be null for deletes
            if (fullTypeName != null && resource != null && resource.GetType() != Type.GetType(fullTypeName))
                throw new Exception("Unexpected type for resource.");

            return resource;
        }

        public object ResetResource(object resource)
        {
            using (var work = unitOfWorkProvider.Create())
            {
                CurrentDataSource.CastTo<dynamic>().Save(resource);
                work.RejectChanges();
            }
            return resource;
        }

        public void SetValue(object targetResource, string propertyName, object propertyValue)
        {
            PropertyInfo pi = targetResource.GetType().GetProperty(propertyName);
            if (pi == null)
                throw new Exception("Can't find property {0}.".FormatWith(propertyName));
            pi.SetValue(targetResource, propertyValue, null);
        }

        public object GetValue(object targetResource, string propertyName)
        {
            PropertyInfo pi = targetResource.GetType().GetProperty(propertyName);
            if (pi == null)
                throw new Exception("Can't find property {0}.".FormatWith(propertyName));
            return pi.GetValue(targetResource, null);
        }

        public void SetReference(object targetResource, string propertyName, object propertyValue)
        {
            SetValue(targetResource, propertyName, propertyValue);
        }

        public void AddReferenceToCollection(object targetResource, string propertyName, object resourceToBeAdded)
        {
            PropertyInfo pi = targetResource.GetType().GetProperty(propertyName);
            if (pi == null)
                throw new Exception("Can't find property {0}.".FormatWith(propertyName));
            var collection = (IList)pi.GetValue(targetResource, null);
            collection.Add(resourceToBeAdded);
        }

        public void RemoveReferenceFromCollection(object targetResource, string propertyName, object resourceToBeRemoved)
        {
            PropertyInfo pi = targetResource.GetType().GetProperty(propertyName);
            if (pi == null)
                throw new Exception("Can't find property {0}".FormatWith(propertyName));
            var collection = (IList)pi.GetValue(targetResource, null);
            collection.Remove(resourceToBeRemoved);
        }

        public void DeleteResource(object targetResource)
        {
            forDelete.Add(targetResource);
        }

        public void SaveChanges()
        {
            using (var work = unitOfWorkProvider.Create())
            {
                forSave.ForEach(i => CurrentDataSource.CastTo<dynamic>().Save(i));
                forDelete.ForEach(i => CurrentDataSource.CastTo<dynamic>().Delete(i));
                work.AcceptChanges();
            }
        }

        public object ResolveResource(object resource)
        {
            return resource;
        }

        public void ClearChanges()
        {
            forSave.Clear();
            forDelete.Clear();
        }

        public void SetConcurrencyValues(object resourceCookie, bool? checkForEquality, IEnumerable<KeyValuePair<string, object>> concurrencyValues)
        {
        }

        #endregion
    }

    /// <summary>
    /// A DataService that uses the custom Soaf providers.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DataService<T> : System.Data.Services.DataService<T>, IServiceProvider
    {
        private readonly Func<T> dataSourceResolver;
        private readonly DataServiceProvider<T> provider;

        internal DataService(Func<T> dataSourceResolver, DataServiceProvider<T> provider)
        {
            this.dataSourceResolver = dataSourceResolver;
            this.provider = provider;
        }

        #region IServiceProvider Members

        public object GetService(Type serviceType)
        {
            if (new[] { typeof(IDataServiceMetadataProvider), typeof(IDataServiceQueryProvider), typeof(IDataServiceUpdateProvider) }.Contains(serviceType))
            {
                return provider;
            }

            return null;
        }

        #endregion

        public static void InitializeService(DataServiceConfiguration config)
        {
            config.SetEntitySetAccessRule("*", EntitySetRights.All);
            config.SetServiceOperationAccessRule("*", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V2;
            config.DataServiceBehavior.AcceptProjectionRequests = true;
            config.DataServiceBehavior.AcceptCountRequests = true;
        }

        protected override T CreateDataSource()
        {
            return dataSourceResolver();
        }
    }


    /// <summary>
    ///   Provides types that are OData services for a repository type.
    /// </summary>
    internal interface IRepositoryODataServiceFactory : IInitializable
    {
        Type GetServiceType(Type repositoryType);
    }

    /// <summary>
    ///   Provides types that are OData services for a repository type.
    /// </summary>
    [Singleton]
    [SupportsSynchronization]
    internal class RepositoryODataServiceFactory : IRepositoryODataServiceFactory
    {
        private readonly IComponentRegistry componentRegistry;
        private readonly IRepositoryServicesConfiguration configuration;
        private readonly IDictionary<Type, Type> repositoryServices = new Dictionary<Type, Type>().Synchronized();

        public RepositoryODataServiceFactory(IComponentRegistry componentRegistry, IRepositoryServicesConfiguration configuration)
        {
            this.componentRegistry = componentRegistry;
            this.configuration = configuration;
        }

        #region IRepositoryODataServiceFactory Members

        [Synchronized]
        public virtual Type GetServiceType(Type repositoryType)
        {
            Type result;
            if (!repositoryServices.TryGetValue(repositoryType, out result))
            {
                using (new TimedScope(s => Debug.WriteLine("Created OData service for {0} in {1}.".FormatWith(repositoryType.Name, s))))
                {
                    var repositoryAttribute = repositoryType.GetAttribute<RepositoryAttribute>();

                    var metaType = new MetaType { Name = "Soaf.Web.Services.RepositoryServices.OData.{0}".FormatWith(repositoryAttribute.Name ?? repositoryType.Name), BaseType = typeof(DataService<>).MakeGenericType(repositoryType).ToMetaType() };
                    metaType.Members.Add(typeof(DataService<>).MakeGenericType(repositoryType).GetConstructors(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).First().ToMetaConstructor());

                    repositoryServices[repositoryType] = result = metaType.ToType();

                    // add to the registry
                    componentRegistry.GetComponentRegistration(result, null);
                }
            }
            return result;
        }

        #endregion

        public void Initialize()
        {
            if (HostingEnvironment.IsHosted && configuration.ServicesEnabled)
            {
                foreach (Type type in componentRegistry.ComponentRegistrations.Select(r => r.Type).Concat(componentRegistry.ComponentRegistrations.SelectMany(r => r.For ?? new Type[0])).WhereNotDefault()
                    .Where(t => t.HasAttribute<RepositoryAttribute>(false, false)).ToArray())
                {
                    GetServiceType(type);
                }
            }
        }
    }
}