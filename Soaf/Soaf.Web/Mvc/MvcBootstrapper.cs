﻿using System.Web.Mvc;
using System.Web.Routing;
using Soaf.ComponentModel;

namespace Soaf.Web.Mvc
{
    [Singleton]
    public class MvcBootstrapper
    {
        private readonly ControllerFactory controllerFactory;

        public bool RegisterAllAreas { get; set; }

        public MvcBootstrapper(ControllerFactory controllerFactory)
        {
            this.controllerFactory = controllerFactory;
            RegisterAllAreas = true;
        }

        public void Initialize()
        {
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            if (RegisterAllAreas) AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterDefaultRoutes(RouteTable.Routes);
        }

        private static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        private static void RegisterDefaultRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*allaspx}", new { allaspx = @".*\.aspx(/.*)?" });

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
                new { controller = "^(?!Services).*" }
                );
        }
    }
}
