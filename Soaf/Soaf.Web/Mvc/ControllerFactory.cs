﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Routing;

namespace Soaf.Web.Mvc
{
    /// <summary>
    /// Resolves controllers for injection and interception instead of creating them using System.Activator (via DefaultControllerFactory).
    /// </summary>
    public class ControllerFactory : DefaultControllerFactory
    {
        private readonly IServiceProvider serviceProvider;

        public ControllerFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        [DebuggerNonUserCode]
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null) return base.GetControllerInstance(requestContext, controllerType);
            return serviceProvider.GetService(controllerType) as IController;
        }
    }
}