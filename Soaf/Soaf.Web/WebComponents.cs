﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.ServiceModel;
using System.Transactions;
using System.Web;
using System.Web.Caching;
using System.Web.Configuration;
using System.Web.Hosting;
using Soaf.ComponentModel;
using Soaf.Configuration;
using Soaf.Security;
using Soaf.Web;
using Soaf.Web.Mvc;
using Soaf.Web.Services;

[assembly: Component(typeof(WebPrincipalContext), typeof(IPrincipalContext), Priority = -1)]
[assembly: Component(typeof(WebConfigurationContainer), typeof(IConfigurationContainer), Priority = -1)]

namespace Soaf.Web
{
    /// <summary>
    /// Components available for consumption by web applications.
    /// </summary>
    public class WebComponents
    {
        public WebComponents(IServiceRouteGenerator serviceRouteGenerator, MvcBootstrapper mvcBootstrapper, UserPrincipalBootstrapper userPrincipalBootstrapper)
        {
            UserPrincipalBootstrapper = userPrincipalBootstrapper;
            ServiceRouteGenerator = serviceRouteGenerator;
            MvcBootstrapper = mvcBootstrapper;
        }

        public UserPrincipalBootstrapper UserPrincipalBootstrapper { get; private set; }
        public IServiceRouteGenerator ServiceRouteGenerator { get; private set; }
        public MvcBootstrapper MvcBootstrapper { get; private set; }

        public void InitializeAll()
        {
            MvcBootstrapper.Initialize();
            ServiceRouteGenerator.AddServiceRoutes();
        }
    }

    /// <summary>
    /// Replaces the HttpContext's IPrincipal with a UserPrincipal for the current identity's username.
    /// </summary>
    public class UserPrincipalBootstrapper
    {
        private readonly IPrincipalContext principalContext;
        private readonly IUserRepository userRepository;

        internal const string CacheKeyPrefix = "UserPrincipalCache__";

        public UserPrincipalBootstrapper(IPrincipalContext principalContext, IUserRepository userRepository)
        {
            this.principalContext = principalContext;
            this.userRepository = userRepository;
        }

        private void OnAuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current != null && HttpContext.Current.User is UserPrincipal)
            {
                throw new InvalidOperationException("UserPrincpial was not cleaned up from a previous use.");
            }

            if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var cacheKey = CacheKeyPrefix + HttpContext.Current.User.Identity.Name;

                var userPrincipal = HttpContext.Current.Cache[cacheKey] as UserPrincipal;

                if (userPrincipal == null)
                {
                    using (var ts = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }))
                    {
                        var user = userRepository.Users.FirstOrDefault(u => u.UserName == HttpContext.Current.User.Identity.Name);
                        if (user != null)
                        {
                            userPrincipal = new UserPrincipal(new UserIdentity(user, true, string.Empty));
                            HttpContext.Current.Cache.Add(cacheKey, userPrincipal, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(10), CacheItemPriority.Default, null);
                        }
                        else
                        {
                            throw new SecurityException("User {0} not found.".FormatWith(HttpContext.Current.User.Identity.Name));
                        }
                        ts.Complete();
                    }
                }

                principalContext.Principal = userPrincipal;

            }
        }

        public void Initialize()
        {
            var application = HttpContext.Current.ApplicationInstance;
            if (application == null) throw new NotSupportedException();
            application.PostAuthenticateRequest += OnAuthenticateRequest;
        }
    }

    /// <summary>
    /// An IPrincipalContext that uses the HttpContext.User if available.
    /// </summary>
    internal class WebPrincipalContext : PrincipalContext
    {
        public override System.Security.Principal.IPrincipal Principal
        {
            get
            {
                if (HttpContext.Current != null) return HttpContext.Current.User;
                return base.Principal;
            }
            set
            {
                if (HttpContext.Current != null) HttpContext.Current.User = value;
                base.Principal = value;
            }
        }
    }


    [Singleton]
    public class WebConfigurationContainer : ExeConfigurationContainer
    {
        private readonly Lazy<System.Configuration.Configuration> webConfiguration = Lazy.For(() => WebConfigurationManager.OpenWebConfiguration("~"));

        public override System.Configuration.Configuration Configuration
        {
            get { return HttpContext.Current == null ? base.Configuration : webConfiguration.Value; }
        }
    }
}