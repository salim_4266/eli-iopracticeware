param($server)
# Add to Server Manager

# $server = "rds0x.iopracticeware.com"

get-process ServerManager | stop-process �force

$file = get-item "$env:USERPROFILE\AppData\Roaming\Microsoft\Windows\ServerManager\ServerList.xml"

copy-item �path $file �destination $file-backup �force

$xml = [xml] (get-content $file )

$newserver = @($xml.ServerList.ServerInfo)[0].clone()

$newserver.name = $server

$newserver.lastUpdateTime = �0001-01-01T00:00:00�

$newserver.status = �2�

$xml.ServerList.AppendChild($newserver)

$xml.Save($file.FullName)

start-process �filepath $env:SystemRoot\System32\ServerManager.exe �WindowStyle Maximized

# Add to RAP
Import-Module ActiveDirectory
ADD-ADGroupMember �RDS Servers� �members ($server.Split('.')[0] + �$�)