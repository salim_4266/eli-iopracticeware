# Configure DNS
$wmi = Get-WmiObject Win32_NetworkAdapterConfiguration
$wmi.SetDNSDomain('iopracticeware.com')
$dns = "10.1.1.50", "10.1.1.60"
$wmi.SetDNSServerSearchOrder($dns)
$class = [wmiclass]'Win32_NetworkAdapterConfiguration'
$suffixes = "iopracticeware.com", "us-east-1.ec2-utilities.amazonaws.com", "ec2-utilities.amazonaws.com", "ec2.internal", "compute-1.internal"
$class.SetDNSSuffixSearchOrder($suffixes)

foreach ($adapter in Get-WmiObject -class win32_networkadapter -filter "NetConnectionStatus = 2")
{
    $name = $adapter.Name
    Get-NetAdapterBinding -InterfaceDescription $name | Select-Object Name,DisplayName,ComponentID
    Disable-NetAdapterBinding -InterfaceDescription $name -ComponentID ms_tcpip6
}

# Install IO
Copy-Item "\\rds01\C$\Program Files (x86)\IO Practiceware" "C:\Program Files (x86)\IO Practiceware" -Recurse -Force
Start-Process "C:\Program Files (x86)\IO Practiceware\IO Practiceware Client Setup.exe" -ArgumentList "-passive -norestart" -Wait

# Join Domain
# Can't use SecureString file becuase machine cryptography will change as joined to domain
$username = "iopracticeware\Administrator"
$password = "reT&*325" | ConvertTo-SecureString -asPlainText -Force
$credential = new-object -typename System.Management.Automation.PSCredential `
         -argumentlist $username, $password
$newName = "rds-" + (Get-NetIPAddress)[0].IPAddress.Replace(':','').Replace('%','').Substring(0,8)
Add-Computer -DomainName iopracticeware.com -OUPath "OU=RDS,DC=iopracticeware,DC=com" -Credential $credential -NewName $newName


# Add RD Session Host to the IO Practiceware Collection after restart (since we just joined the domain
$taskName = "ScheduleAddRDSessionHost"
$taskPath = "\IO Practiceware\"
$action = New-ScheduledTaskAction �Execute "PowerShell.exe" -Argument "-ExecutionPolicy Unrestricted -File C:\ScheduleAddRDSessionHost.ps1"
$trigger = New-ScheduledTaskTrigger -AtStartup
Unregister-ScheduledTask -TaskName $taskName -TaskPath $taskPath -Confirm:$false -ErrorAction:SilentlyContinue
# Can't use SecureString becuase machine cryptography will change once joined to domain
Register-ScheduledTask �TaskName $taskName -TaskPath $taskPath -Action $action �Trigger $trigger �User "NT AUTHORITY\SYSTEM" | Out-Null


Restart-Computer -Force