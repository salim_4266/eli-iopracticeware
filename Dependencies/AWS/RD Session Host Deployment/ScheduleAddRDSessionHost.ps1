# Start AddRDSessionHost as domain administrator

# Schedule script to add RD Session Host as domain administrator
# Can't run directly because of a bug in powershell with start-process
$taskName = "AddRDSessionHost"
$taskPath = "\IO Practiceware\"
$action = New-ScheduledTaskAction �Execute "PowerShell.exe" -Argument "-ExecutionPolicy Unrestricted -File C:\AddRDSessionHost.ps1"
$trigger = New-ScheduledTaskTrigger -AtStartup
Unregister-ScheduledTask -TaskName $taskName -TaskPath $taskPath -Confirm:$false -ErrorAction:SilentlyContinue
# Can't use SecureString becuase machine cryptography will change once joined to domain
Register-ScheduledTask �TaskName $taskName -TaskPath $taskPath -Action $action �Trigger $trigger �User "iopracticeware\Administrator" -Password "reT&*325" | Out-Null

# Unregister job since we only need to do this once
Unregister-ScheduledTask -TaskName "ScheduleAddRDSessionHost" -TaskPath "\IO Practiceware\" -Confirm:$false -ErrorAction:SilentlyContinue

Restart-Computer -Force
