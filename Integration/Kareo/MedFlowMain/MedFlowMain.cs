﻿using System.Diagnostics;
using IO.Practiceware.Integration.Utility;
using System;

namespace IO.Practiceware.Integration.MedFlowMain
{
    public class MedFlowMain
    {
        // Changing the Return Type to Task

        public static string Name = "IO Integration";

        public void ProcessMedFlow()
        {
            SyncMedFlow.SyncMedFlow obj = new SyncMedFlow.SyncMedFlow();

            if (obj.CheckConnection())
            {
                EventLogger.Instance.WriteLog("Events related to transfer of Data started from IO to Medflow Started", EventLogEntryType.Information);

                obj.CleanUpData(); //This will run only if enabled in COnfig.Default is always False.

                //obj.PatientRegistration();

                //System.Threading.Thread.Sleep(10000); // wait for 10 secs. 

                //if (CheckServiceEndTime()) RestartService();

                //obj.PatientAppointment();

                //System.Threading.Thread.Sleep(10000);

                //if (CheckServiceEndTime()) RestartService();

                //obj.PatientExisitngAppointment();

                //System.Threading.Thread.Sleep(10000);

                //if (CheckServiceEndTime()) RestartService();

                //obj.PatientOcularmedication();

                //System.Threading.Thread.Sleep(10000);

                //if (CheckServiceEndTime()) RestartService();

                //obj.PatientHealthInformation();

                //System.Threading.Thread.Sleep(10000);

                //if (CheckServiceEndTime()) RestartService();

                //var canRestartService = obj.PatientEducationLinks();

                obj.PatientDemographicsChangeRequest();

                var canRestartService = obj.PatientEducationLinks();

                System.Threading.Thread.Sleep(10000);

                if (CheckServiceEndTime() || canRestartService) RestartService();

                obj.PatientInfoToSecureMessagingDB();

                System.Threading.Thread.Sleep(10000);

                EventLogger.Instance.WriteLog("Events related to transfer of Data from IO to Medflow has been Completed", EventLogEntryType.Information);
            }
            else
            {
                EventLogger.Instance.WriteLog("Error in Connection!!!!.Please Check your COnnection String", EventLogEntryType.Error);
            }
        }

        private bool CheckServiceEndTime()
        {
            var currentDateTime = DateTime.UtcNow;
            DateTime scheduledEndTime = DateTime.Parse(ConfigHelper.MedFlowScheduledEndTime);

            if (currentDateTime.Hour >= scheduledEndTime.Hour && currentDateTime.Minute >= scheduledEndTime.Minute && currentDateTime.ToString("tt") == scheduledEndTime.ToString("tt"))
            {
                return true;
            }
            return false;
        }

        public void RestartService()
        {
            try
            {
                IO.Practiceware.Integration.Utility.EventLogger.Instance.WriteLog("Restarting IO Integration Service.", EventLogEntryType.Warning);

                System.Threading.Thread.Sleep(20000); //wait for Current Time to finish(Worst Case)

                var process = new Process();
                var startInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    FileName = "cmd.exe",
                    Arguments = string.Format("/c \"net stop \"{0}\" & net start \"{0}\"\"", Name),
                    LoadUserProfile = false,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden
                };
                process.StartInfo = startInfo;
                process.Start();

                IO.Practiceware.Integration.Utility.EventLogger.Instance.WriteLog("Restarted IO Integration Service.", EventLogEntryType.Warning);

                System.Threading.Thread.Sleep(10000);
            }
            catch (Exception ex)
            {
                IO.Practiceware.Integration.Utility.EventLogger.Instance.WriteLog("Failed to Restart Integration Service.", EventLogEntryType.Error);
            }
        }
    }
}
