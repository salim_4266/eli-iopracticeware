﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Data;
using IO.Practiceware.Integration.MedFlowEntities;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.IO;
using IO.Practiceware.Integration.Utility;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;
namespace IO.Practiceware.Integration.MedFlowServices
{
    public class MedFlowService
    {
        //ToDo: Wrapper methods to API calls.


        //Instance for using this Class
        private static MedFlowService _instance;

        public bool FlagforReturnedUrl = false;

        public static MedFlowService Instance
        {
            get { return _instance ?? (_instance = new MedFlowService()); }
        }

        #region Functions for Sending XML Document using Medflow Web Api


        public void GetMyWebApi(Object parameterList, bool flagforUrl = true, string MessageId = "", string _connectionString = "")
        {

            // Using WebClient

            if (flagforUrl)
            {
                var webApiobject = parameterList as ParameterList;

                var _parametertosend = new ParameterList
                {
                    ClientKey = webApiobject.ClientKey,
                    ActionTypeLookupTypeId = webApiobject.ActionTypeLookupTypeId,
                    MessageData = webApiobject.MessageData,
                    ProcessedDate = DateTime.Now,
                    ExternalId = webApiobject.ExternalId,
                    PatientId = webApiobject.PatientId,
                    IsActive = webApiobject.IsActive
                };

                var _otherparameter = new ParameterList
                {
                    PatientId = webApiobject.PatientId,
                    IsActive = webApiobject.IsActive
                };


                string result = string.Empty;
                var _jsontobesent = Newtonsoft.Json.JsonConvert.SerializeObject(_parametertosend);
                string _medflowurl = ConfigurationManager.AppSettings["MedflowAPIBaseUrl"].ToString() + "api/PortalMedflowIntegration/PostPushToPortal";
 
                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;

                try
                {
                    result = webclient.UploadString(_medflowurl, "POST", _jsontobesent);
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();
                    if (result == "success")
                    {
                        if (MessageId != string.Empty && webApiobject.ActionTypeLookupTypeId == 8)
                        {

                        }

                        // For Deployment
                        using (SqlConnection con = new SqlConnection(_connectionString))
                        {
                            if (con.State == ConnectionState.Closed)
                                con.Open();
                            using (SqlCommand cmd = con.CreateCommand()) //For Successful Insertion
                            {
                                cmd.CommandText = "Insert into Medflow.PortalQueueXMLResponse (MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive,OriginalId)";
                                cmd.CommandText += "VALUES('" + _parametertosend.MessageData.Replace("'", "") + "','" + _parametertosend.ActionTypeLookupTypeId + "','" + _parametertosend.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + _otherparameter.PatientId + "','" + _otherparameter.IsActive + "','" + _parametertosend.ExternalId + "')";
                                cmd.CommandType = CommandType.Text;
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    else
                    {
                        //For Deployment
                        using (SqlConnection con = new SqlConnection(_connectionString))
                        {
                            if (con.State == ConnectionState.Closed)
                                con.Open();
                            using (SqlCommand cmd = con.CreateCommand())  // For Inserting Exception
                            {
                                cmd.CommandText = "Insert into Medflow.PortalQueueXMLException (MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive,ExternalId,Exception)";
                                cmd.CommandText += "VALUES('" + _parametertosend.MessageData.Replace("'", "") + "','" + _parametertosend.ActionTypeLookupTypeId + "','" + _parametertosend.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + _otherparameter.PatientId + "','" + _otherparameter.IsActive + "','" + _parametertosend.ExternalId + "','Fail')";
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
                catch (WebException Exception)
                {
                    EventLogger.Instance.WriteLog("Exception Occured while Accessing WebAPI.Refer Exception Table for Details.", EventLogEntryType.Error);
                    string responsetext = string.Empty;
                    if (Exception.Response != null)
                    {
                        var responsstream = Exception.Response.GetResponseStream();
                        if (responsstream != null)
                        {
                            using (var reader = new StreamReader(responsstream))
                            {
                                responsetext = reader.ReadToEnd();
                                //For Deployment
                                using (SqlConnection con = new SqlConnection(_connectionString))
                                {
                                    if (con.State == ConnectionState.Closed)
                                        con.Open();
                                    using (SqlCommand cmd = con.CreateCommand())  //for Inserting Web Exceptions
                                    {
                                        cmd.CommandText = "Insert into Medflow.PortalQueueXMLException (MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive,ExternalId,Exception)";
                                        cmd.CommandText += "VALUES('" + _parametertosend.MessageData.Replace("'", "") + "','" + _parametertosend.ActionTypeLookupTypeId + "','" + _parametertosend.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + _otherparameter.PatientId + "','" + _otherparameter.IsActive + "','" + _parametertosend.ExternalId + "','" + responsetext + "')";
                                        cmd.ExecuteNonQuery();

                                    }

                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.Instance.WriteLog("There's a Exception while accessing the Medflow Service: - " + ex.Message, EventLogEntryType.Error);
                }
            }
            else
            {
                var webApiobject = parameterList as ParameterList;

                string result = string.Empty;
                var _jsontobesent = Newtonsoft.Json.JsonConvert.SerializeObject(parameterList);

                string _medflowurl = ConfigurationManager.AppSettings["MedflowAPIBaseUrl"].ToString() + "api/PortalMedflowIntegration/PostPushToPortal";

                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;

                try
                {
                    result = webclient.UploadString(_medflowurl, "POST", _jsontobesent);


                    result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();

                    if (result == "success")
                    {
                        //For Deployment
                        using (SqlConnection con = new SqlConnection(_connectionString))
                        {
                            if (con.State == ConnectionState.Closed)
                                con.Open();
                            using (SqlCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = "Insert into Medflow.PortalQueueXMLResponse (MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive,OriginalId)";
                                cmd.CommandText += "VALUES('" + webApiobject.MessageData.Replace("'", "") + "','" + webApiobject.ActionTypeLookupTypeId + "','" + webApiobject.ProcessedDate + "','" + webApiobject.PatientId + "','" + webApiobject.IsActive + "','" + webApiobject.ExternalId + "')";
                                cmd.CommandType = CommandType.Text;
                                cmd.ExecuteNonQuery();
                            }

                        }
                    }
                    else
                    {
                        // For Deployment
                        using (SqlConnection con = new SqlConnection(_connectionString))
                        {
                            if (con.State == ConnectionState.Closed) con.Open();
                            using (SqlCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = "Insert into Medflow.PortalQueueXMLException (MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive,ExternalId,Exception)";
                                cmd.CommandText += "VALUES('" + webApiobject.MessageData.Replace("'", "") + "','" + webApiobject.ActionTypeLookupTypeId + "','" + webApiobject.ProcessedDate + "','" + webApiobject.PatientId + "','" + webApiobject.IsActive + "','" + webApiobject.ExternalId + "','Fail')";
                                cmd.ExecuteNonQuery();

                            }
                        }
                    }
                }

                catch (WebException Exception)
                {
                    string responsetext = string.Empty;

                    if (Exception.Response != null)
                    {
                        var responsstream = Exception.Response.GetResponseStream();
                        if (responsstream != null)
                        {
                            using (var reader = new StreamReader(responsstream))
                            {
                                responsetext = reader.ReadToEnd();

                                //For Deployment
                                using (SqlConnection con = new SqlConnection(_connectionString))
                                {
                                    if (con.State == ConnectionState.Closed)
                                        con.Open();
                                    using (SqlCommand cmd = con.CreateCommand())
                                    {
                                        cmd.CommandText = "Insert into Medflow.PortalQueueXMLException (MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive,ExternalId,Exception)";
                                        cmd.CommandText += "VALUES('" + webApiobject.MessageData.Replace("'", "") + "','" + webApiobject.ActionTypeLookupTypeId + "','" + webApiobject.ProcessedDate + "','" + webApiobject.PatientId + "','" + webApiobject.IsActive + "','" + webApiobject.ExternalId + "','" + responsetext + "')";
                                        cmd.ExecuteNonQuery();
                                    }

                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.Instance.WriteLog("There's a Exception while accessing the Medflow Service: - " + ex.Message, EventLogEntryType.Error);
                }
            }
        }


        public string GetPatientEducationLinkForDiagnosis(string diagnosisCode)
        {
            string medlineUrlforDiagnosis = "https://apps2.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.90&mainSearchCriteria.v.c=" + diagnosisCode;
            string medLinkUrl = string.Empty;
            medLinkUrl = medlineUrlforDiagnosis;
            return medLinkUrl;
        }


        public string GetPatientEducationLinkForLionicCodes(string lionicCode)
        {
            string medlineUrlforLabCodes = "https://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.1&mainSearchCriteria.v.c=" + lionicCode + "&informationRecipient.languageCode.c=en";
            string medLinkUrl = string.Empty;
            medLinkUrl = medlineUrlforLabCodes;
            return medLinkUrl;
        }

        public string GetPatientEducationLinkForMedicationCode(string medicationString)
        {
            string medlineUrlforMedicationCode = "https://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.69&mainSearchCriteria.v.dn=" + medicationString + "&informationRecipient.languageCode.c=en";
            string medLinkUrl = string.Empty;
            medLinkUrl = medlineUrlforMedicationCode;
            return medLinkUrl;
        }


        public string InsertVDTInformationfromMVE()
        {
            return "return";
        }

        #endregion

    }
}
