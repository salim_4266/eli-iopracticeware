﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class InvoiceDTO
    {
        public int InvoiceId { get; set; }
        public int PatientId { get; set; }
        public int AppointmentId { get; set; }
        public int ResourceId { get; set; }
        public int ServiceLocationId { get; set; }
        public int PracticeId { get; set; }
        public ICDMode Mode { get; set; }
        public List<BillingServiceDTO> BillingServices { get; set; }

        public int ExternalSystemPatientId { get; set; }
        public int ExternalSystemAppointmentId { get; set; }
        public int ExternalSystemProviderId { get; set; }
        public int ExternalSystemServiceLocationId { get; set; }
        public int ExternalSystemEncounterId { get; set; }
        public int ExternalSystemPracticeId { get; set; }
    }

}
