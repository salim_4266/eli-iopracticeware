﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public enum Gender
    {
        Other = 4,
        Male = 3,
        Female = 2,
        Unknown = 1,
    }

    public enum ICDMode
    {
        ICD9 = 0,
        ICD10 = 1
    }

   
}
