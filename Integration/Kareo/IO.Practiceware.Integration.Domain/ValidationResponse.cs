﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class ValidationResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
