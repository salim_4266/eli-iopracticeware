﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class ExternalSystemPatientDTO
    {
        public int PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int PracticeId { get; set; }
        public string PracticeName { get; set; }
    }
}
