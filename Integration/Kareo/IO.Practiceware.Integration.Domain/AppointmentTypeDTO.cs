﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
   public class AppointmentTypeDTO
    {
        public int AppTypeId { get; set; }
        public string AppointmentType { get; set; }
    }
}
