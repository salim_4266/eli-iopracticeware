﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class ExternalSystemAppointmentDTO
    {
        public int AppointmentId { get; set; }
        public int PatientId { get; set; }
        public int PracticeId { get; set; }
        public string ConfirmationStatus { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<string> Resources { get; set; }
        public string ServiceLocationName { get; set; }
        public string AppointmentReason { get; set; }
        public string AppointmentType { get; set; }

        public int IO_PatientId { get; set; }
        public int IO_ResourceId { get; set; }
        public int IO_ServiceLocationId { get; set; }
        public int IO_AppointmentId { get; set; }
        public int IO_AppointmentTypeId { get; set; }
    }
}
