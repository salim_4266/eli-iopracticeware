﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class ExternalSystemPracticeDTO
    {
        public int ExternalSystemPracticeId { get; set; }
        public string PracticeName { get; set; }
        public string PracticeType { get; set; }
        public string PracticeAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string TaxId { get; set; }
        public string Email { get; set; }
        public string LocationReference { get; set; }
        public string BillingOffice { get; set; }
        public string NPI { get; set; }
        public string Fax { get; set; }

        public int IO_PracticeId { get; set; }
    }
}
