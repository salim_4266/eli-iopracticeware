﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class EncounterDTO
    {
        public int EncounterId { get; set; }
        public int EncounterStatusId { get; set; }
        public int EncounterTypeId { get; set; }
        public int PatientId { get; set; }
        public int ServiceLocationId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }
}
