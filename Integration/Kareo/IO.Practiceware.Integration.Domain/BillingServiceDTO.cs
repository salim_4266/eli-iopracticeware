﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class BillingServiceDTO
    {
        public int BillingServiceId { get; set; }
        public DateTime? DateOfService { get; set; }
        public string ServiceComment { get; set; }
        public string ProcedureCode { get; set; }
        public string Modifier1 { get; set; }
        public string Modifier2 { get; set; }
        public string Modifier3 { get; set; }
        public string Modifier4 { get; set; }
        public string DiagnosisCode1 { get; set; }
        public string DiagnosisCode2 { get; set; }
        public string DiagnosisCode3 { get; set; }
        public string DiagnosisCode4 { get; set; }
        public decimal Units { get; set; }
        public decimal UnitCharge { get; set; }
        public decimal TotalAllowed { get; set; }
    }
}
