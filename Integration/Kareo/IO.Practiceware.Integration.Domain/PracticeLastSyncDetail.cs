﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class PracticeLastSyncDetail
    {
        public int ExternalSystemPracticeId { get; set; }
        public DateTime? LastSyncTime { get; set; }
        public List<ExternalSystemServiceLocationDTO> ExternalSystemServiceLocationList { get; set; }
        public List<ExternalSystemProviderDTO> ExternalSystemProviderList { get; set; }
    }
}


