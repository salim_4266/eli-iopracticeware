﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class ExternalSystemServiceLocationDTO
    {

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string BillingName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string FaxPhone { get; set; }
        public string FaxPhoneExt { get; set; }
        public int ExternalSystemServiceLocationId { get; set; }
        public string NPI { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string PhoneExt { get; set; }
        public string PlaceOfService { get; set; }
        public int PracticeID { get; set; }
        public string PracticeName { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public int IO_ServiceLocationId { get; set; }
        public int IO_PracticeId { get; set; }
    }
}
