﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Domain
{
    public class ExternalSystemProviderDTO
    {
        public bool Active { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string BillingType { get; set; }
        public string EmailAddress { get; set; }
        public string Fax { get; set; }
        public string FaxExt { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public string HomePhone { get; set; }
        public string HomePhoneExt { get; set; }
        public int ExternalSystemProviderId { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string MobilePhone { get; set; }
        public string MobilePhoneExt { get; set; }
        public string NationalProviderIdentifier { get; set; }
        public int PracticeID { get; set; }
        public string PracticeName { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string Type { get; set; }
        public string WorkPhone { get; set; }
        public string WorkPhoneExt { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }

        public int IO_ProviderId { get; set; }
        public int IO_PracticeId { get; set; }
    }
}

