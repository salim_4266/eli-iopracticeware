﻿using IO.Practiceware.Integration.Domain;
using IO.Practiceware.Integration.KareoServices.KareoAPI;
using IO.Practiceware.Integration.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.KareoServices
{
    public interface IKareoService
    {
        /// <summary>
        /// retruns Kareo Practices
        /// </summary>
        /// <returns></returns>
        List<ExternalSystemPracticeDTO> GetPractices();

        /// <summary>
        ///  returns Kareo Patient Data
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        ExternalSystemPatientDTO GetPatient(int patientId);

        /// <summary>
        /// returns Kareo Appointment Data for a particular date
        /// </summary>
        /// <param name="practiceName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        List<ExternalSystemAppointmentDTO> GetAppointments(string practiceName, DateTime startDate, DateTime endDate, out string errorMessage);

        /// <summary>
        /// returns Kareo Service Locations 
        /// </summary>
        /// <param name="practiceId"></param>
        /// <returns></returns>
        List<ExternalSystemServiceLocationDTO> GetServiceLocations(int practiceId);

        /// <summary>
        /// returns Kareo Providers
        /// </summary>
        /// <param name="practiceName"></param>
        /// <returns></returns>
        List<ExternalSystemProviderDTO> GetProviders(string practiceName);

        /// <summary>
        /// return Kareo Provider
        /// </summary>
        /// <param name="practiceName"></param>
        /// <param name="providerName"></param>
        /// <returns></returns>
        ExternalSystemProviderDTO GetProvider(string practiceName, string providerName);

        /// <summary>
        /// creates an Encounter in Kareo for an IO Invoice
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        int CreateEncounter(InvoiceDTO invoice);

        /// <summary>
        /// checks if Applications is connected to Kareo Server
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        bool IsConnected(out string errorMessage);
    }

    public class KareoService : IKareoService
    {

        private KareoServicesClient _service;
        private Func<PatientData, ExternalSystemPatientDTO> _patientMapper;
        private Func<PracticeData, ExternalSystemPracticeDTO> _practiceMapper;
        private Func<string, Gender> _getGender;
        private Func<AppointmentData, ExternalSystemAppointmentDTO> _appointmentMapper;
        private Func<ProviderData, ExternalSystemProviderDTO> _providerMapper;
        private Func<ServiceLocationData, ExternalSystemServiceLocationDTO> _serviceLocationMapper;

        private ProviderFieldsToReturn _providerFieldsToReturn;
        private AppointmentFieldsToReturn _appointmentFieldsToReturn;
        private ServiceLocationFieldsToReturn _serviceLocationFieldsToReturn;
        private PracticeFieldsToReturn _practiceFieldsToReturn;

        public KareoService()
        {
            _service = new KareoServicesClient();
            InitMappers();
            InitFieldsToReturn();
        }

        private void InitMappers()
        {
            _getGender = (gender) =>
            {
                if (!string.IsNullOrEmpty(gender))
                {
                    gender = gender.ToLower();
                }

                if (gender == "male" || gender == "m")
                    return Gender.Male;
                else if (gender == "female" || gender == "f")
                    return Gender.Female;
                else
                    return Gender.Unknown;
            };


            _patientMapper = (patient) =>
            {
                return new ExternalSystemPatientDTO
                {
                    PatientId = Convert.ToInt32(patient.ID),
                    FirstName = patient.FirstName,
                    LastName = patient.LastName,
                    Gender = _getGender(patient.Gender),
                    DateOfBirth = !string.IsNullOrEmpty(patient.DOB) ? patient.DOB.ToMMDDYYYY() : (DateTime?)null,
                    PracticeId = Convert.ToInt32(patient.PracticeId),
                    PracticeName = patient.PracticeName
                };
            };

            _practiceMapper = (practice) =>
            {
                var tmpPracticeAddress = string.Format("{0} ,{1}", practice.PracticeAddressLine1, practice.PracticeAddressLine2);
                tmpPracticeAddress = tmpPracticeAddress.Length > 35 ? tmpPracticeAddress.Substring(0, 35) : tmpPracticeAddress;

                return new ExternalSystemPracticeDTO
                {
                    BillingOffice = !string.IsNullOrWhiteSpace(practice.BillingContactFullName) ? "T" : "F",
                    City = practice.PracticeCity,
                    Email = practice.Email,
                    LocationReference = string.Format("{0}_{1}", practice.ID, practice.PracticeCity),
                    NPI = practice.NPI,
                    Fax = practice.FaxExt + practice.Fax,
                    Phone = practice.PhoneExt + practice.Phone,
                    PracticeAddress = tmpPracticeAddress,
                    IO_PracticeId = 0,
                    ExternalSystemPracticeId = Convert.ToInt32(practice.ID),
                    PracticeName = practice.PracticeName,
                    PracticeType = "P",
                    State = practice.PracticeState,
                    TaxId = practice.TaxID,
                    Zip = practice.PracticeZipCode
                };
            };

            _providerMapper = (item) =>
            {
                return new ExternalSystemProviderDTO
                {
                    Active = item.Active == "True" ? true : false,
                    AddressLine1 = item.AddressLine1,
                    AddressLine2 = item.AddressLine2,
                    BillingType = item.BillingType,
                    EmailAddress = item.EmailAddress,
                    Fax = item.Fax,
                    FaxExt = item.FaxExt,
                    FirstName = item.FirstName,
                    FullName = item.FullName,
                    HomePhone = item.HomePhone,
                    HomePhoneExt = item.HomePhoneExt,
                    ExternalSystemProviderId = Convert.ToInt32(item.ID),
                    LastName = item.LastName,
                    MiddleName = item.MiddleName,
                    MobilePhone = item.MobilePhone,
                    MobilePhoneExt = item.MobilePhoneExt,
                    NationalProviderIdentifier = item.NationalProviderIdentifier,
                    PracticeID = Convert.ToInt32(item.PracticeID),
                    PracticeName = item.PracticeName,
                    Prefix = item.Prefix,
                    Suffix = item.Suffix,
                    Type = item.Type,
                    WorkPhone = item.WorkPhone,
                    WorkPhoneExt = item.WorkPhoneExt,
                    ZipCode = item.ZipCode,
                    City = item.City,
                    Country = item.Country,
                    State = item.State
                };
            };


            _serviceLocationMapper = (item) =>
            {
                return new ExternalSystemServiceLocationDTO
                {
                    AddressLine1 = item.AddressLine1,
                    AddressLine2 = item.AddressLine2,
                    BillingName = item.BillingName,
                    City = item.City,
                    Country = item.Country,
                    FaxPhone = item.FaxPhone,
                    FaxPhoneExt = item.FaxPhoneExt,
                    ExternalSystemServiceLocationId = Convert.ToInt32(item.ID),
                    NPI = item.NPI,
                    Name = item.Name,
                    Phone = item.Phone,
                    PhoneExt = item.PhoneExt,
                    PlaceOfService = item.PlaceOfService,
                    PracticeID = Convert.ToInt32(item.PracticeID),
                    PracticeName = item.PracticeName,
                    State = item.State,
                    ZipCode = item.ZipCode,
                };
            };


            _appointmentMapper = (appt) =>
            {
                List<string> resourceList = new List<string>();
                for (int i = 1; i <= 10; i++)
                {
                    string res = appt.GetType().GetProperty("ResourceName" + i).GetValue(appt, null).ToString();
                    if (!string.IsNullOrWhiteSpace(res))
                    {
                        resourceList.Add(res);
                    }
                }

                return new ExternalSystemAppointmentDTO
                {

                    AppointmentId = appt.ID.GetNumericValue(),
                    PatientId = Convert.ToInt32(appt.PatientID),
                    PracticeId = Convert.ToInt32(appt.PracticeID),
                    ConfirmationStatus = appt.ConfirmationStatus,
                    StartDate = appt.StartDate.ToMMDDYYYYHHMMSSTT(),
                    EndDate = !string.IsNullOrEmpty(appt.EndDate) ? appt.EndDate.ToMMDDYYYYHHMMSSTT() : (DateTime?)null,
                    ServiceLocationName = appt.ServiceLocationName,
                    Resources = resourceList,
                    AppointmentReason = appt.AppointmentReason1,
                    AppointmentType = appt.Type
                };

            };
        }

        private void InitFieldsToReturn()
        {
            _providerFieldsToReturn = new ProviderFieldsToReturn
             {
                 Active = true,
                 AddressLine1 = true,
                 AddressLine2 = true,
                 BillingType = true,
                 EmailAddress = true,
                 Fax = true,
                 FaxExt = true,
                 FirstName = true,
                 FullName = true,
                 HomePhone = true,
                 HomePhoneExt = true,
                 ID = true,
                 LastName = true,
                 MiddleName = true,
                 MobilePhone = true,
                 MobilePhoneExt = true,
                 NationalProviderIdentifier = true,
                 PracticeID = true,
                 PracticeName = true,
                 Prefix = true,
                 Suffix = true,
                 Type = true,
                 WorkPhone = true,
                 WorkPhoneExt = true,
                 ZipCode = true,
                 City = true,
                 Country = true,
                 State = true
             };

            _appointmentFieldsToReturn = new AppointmentFieldsToReturn
                 {
                     ID = true,
                     PatientID = true,
                     PracticeID = true,
                     ConfirmationStatus = true,
                     StartDate = true,
                     EndDate = true,
                     ServiceLocationName = true,
                     ResourceName1 = true,
                     ResourceName2 = true,
                     ResourceName3 = true,
                     ResourceName4 = true,
                     ResourceName5 = true,
                     ResourceName6 = true,
                     ResourceName7 = true,
                     ResourceName8 = true,
                     ResourceName9 = true,
                     ResourceName10 = true,
                     Type = true,
                     AppointmentReason1 = true
                 };

            _serviceLocationFieldsToReturn = new ServiceLocationFieldsToReturn
              {
                  AddressLine1 = true,
                  AddressLine2 = true,
                  BillingName = true,
                  City = true,
                  Country = true,
                  FaxPhone = true,
                  FaxPhoneExt = true,
                  ID = true,
                  NPI = true,
                  Name = true,
                  Phone = true,
                  PhoneExt = true,
                  PlaceOfService = true,
                  PracticeID = true,
                  PracticeName = true,
                  State = true,
                  ZipCode = true
              };

            _practiceFieldsToReturn = new PracticeFieldsToReturn
               {
                   BillingContactFullName = true,
                   PracticeCity = true,
                   Email = true,
                   ID = true,
                   NPI = true,
                   FaxExt = true,
                   Fax = true,
                   PhoneExt = true,
                   Phone = true,
                   PracticeName = true,
                   PracticeState = true,
                   TaxID = true,
                   PracticeZipCode = true,
                   PracticeAddressLine1 = true,
                   PracticeAddressLine2 = true,
               };
        }

        public ExternalSystemPatientDTO GetPatient(int patientId)
        {
            ExternalSystemPatientDTO patient = null;
            try
            {
                GetPatientReq request = new GetPatientReq();
                request.RequestHeader = ApiHelper.GetRequestHeader();
                request.Filter = new SinglePatientFilter()
                {
                    PatientID = patientId
                };

                GetPatientResp response = _service.GetPatient(request);
                if (response.Patient != null)
                {
                    patient = _patientMapper(response.Patient);
                }
            }
            catch (Exception ex)
            {
            }
            return patient;

        }

        public List<ExternalSystemAppointmentDTO> GetAppointments(string practiceName, DateTime startDate, DateTime endDate, out string errorMessage)
        {
            errorMessage = string.Empty;

            int timeZoneOffsetFromGMT = (DateTime.Now.ToCustomerMachineTime() - DateTime.UtcNow).Hours;

            GetAppointmentsReq request = new GetAppointmentsReq();
            request.Filter = new AppointmentFilter
            {
                PracticeName = practiceName,
                StartDate = startDate.ToString("MM/dd/yyyy"),
                EndDate = endDate.ToString("MM/dd/yyyy"),
                TimeZoneOffsetFromGMT = timeZoneOffsetFromGMT.ToString()
            };

            request.Fields = _appointmentFieldsToReturn;

            request.RequestHeader = ApiHelper.GetRequestHeader();

            List<ExternalSystemAppointmentDTO> appointmentList = new List<ExternalSystemAppointmentDTO>();

            GetAppointmentsResp response = _service.GetAppointments(request);

            // check if AppointmentId has some value. Some times it remains empty for currupted record
            List<AppointmentData> appintmentData = response.Appointments != null ?
                response.Appointments.Where(x => !string.IsNullOrWhiteSpace(x.ID)).ToList()
                : new List<AppointmentData>();


            foreach (var item in appintmentData)
            {
                appointmentList.Add(_appointmentMapper(item));
            }

            return appointmentList;
        }

        public List<ExternalSystemPracticeDTO> GetPractices()
        {
            GetPracticesReq request = new GetPracticesReq();
            request.RequestHeader = ApiHelper.GetRequestHeader();
            request.Filter = new PracticeFilter
            {
                PracticeName = string.Empty
            };
            request.Fields = _practiceFieldsToReturn;

            GetPracticesResp response = _service.GetPractices(request);

            List<ExternalSystemPracticeDTO> practiceList = new List<ExternalSystemPracticeDTO>();

            if (response.Practices != null)
            {
                foreach (var item in response.Practices)
                {
                    practiceList.Add(_practiceMapper(item));
                }
            }
            return practiceList;

        }

        public List<ExternalSystemServiceLocationDTO> GetServiceLocations(int practiceId)
        {
            GetServiceLocationsReq request = new GetServiceLocationsReq();
            request.RequestHeader = ApiHelper.GetRequestHeader();
            request.Filter = new ServiceLocationFilter
            {
                PracticeID = practiceId.ToString()
            };

            request.Fields = _serviceLocationFieldsToReturn;

            GetServiceLocationsResp response = _service.GetServiceLocations(request);
            List<ExternalSystemServiceLocationDTO> serviceLocationList = new List<ExternalSystemServiceLocationDTO>();

            if (response.ServiceLocations != null)
            {
                foreach (var item in response.ServiceLocations)
                {

                    serviceLocationList.Add(_serviceLocationMapper(item));
                }
            }
            return serviceLocationList;
        }

        public List<ExternalSystemProviderDTO> GetProviders(string practiceName)
        {
            GetProvidersReq request = new GetProvidersReq();
            request.RequestHeader = ApiHelper.GetRequestHeader();
            request.Filter = new ProviderFilter
            {
                PracticeName = practiceName,
                Type = "Normal Provider"
            };
            request.Fields = _providerFieldsToReturn;

            GetProvidersResp response = _service.GetProviders(request);

            List<ExternalSystemProviderDTO> providerList = new List<ExternalSystemProviderDTO>();

            if (response.Providers != null)
            {
                foreach (var item in response.Providers)
                {

                    providerList.Add(_providerMapper(item));
                }
            }
            return providerList;
        }

        public ExternalSystemProviderDTO GetProvider(string practiceName, string providerName)
        {
            GetProvidersReq request = new GetProvidersReq();
            request.RequestHeader = ApiHelper.GetRequestHeader();
            request.Filter = new ProviderFilter
            {
                PracticeName = practiceName,
                FullName = providerName
            };
            request.Fields = _providerFieldsToReturn;

            GetProvidersResp response = _service.GetProviders(request);

            ExternalSystemProviderDTO provider = null;

            if (response.Providers != null && response.Providers.Count() > 0 && !string.IsNullOrWhiteSpace(response.Providers[0].ID))
            {
                provider = _providerMapper(response.Providers[0]);
            }
            return provider;
        }

        public int CreateEncounter(InvoiceDTO invoice)
        {

            // Create the encounter to insert.
            EncounterCreate newEncounter = new EncounterCreate();
            newEncounter.ServiceStartDate = DateTime.Now.ToKareoServerTime();
            newEncounter.PostDate = DateTime.Now.ToKareoServerTime();

            // Set the practice we want to add this encounter to
            PracticeIdentifierReq practice = new PracticeIdentifierReq();
            practice.PracticeID = invoice.ExternalSystemPracticeId;

            AppointmentIdentifierReq appointment = new AppointmentIdentifierReq();
            appointment.AppointmentID = invoice.ExternalSystemAppointmentId;

            // Create the patient for the encounter
            PatientIdentifierReq patient = new PatientIdentifierReq();
            patient.PatientID = invoice.ExternalSystemPatientId;

            // Set the service location
            EncounterServiceLocation location = new EncounterServiceLocation();
            location.LocationID = invoice.ExternalSystemServiceLocationId;

            // Create the patient case for the encounter
            //PatientCaseIdentifierReq encounterCase = new PatientCaseIdentifierReq();
            //encounterCase.CaseName = "Secondary Case"; //todo

            // Create the referring provider for the encounter
            ProviderIdentifierDetailedReq provider = new ProviderIdentifierDetailedReq();
            provider.ProviderID = invoice.ExternalSystemProviderId;

            List<ServiceLineReq> serviceLineList = new List<ServiceLineReq>();

            foreach (var billingService in invoice.BillingServices)
            {
                ServiceLineReq serviceLine = new ServiceLineReq();
                serviceLine.ServiceStartDate = billingService.DateOfService;
                serviceLine.ProcedureCode = billingService.ProcedureCode;
                serviceLine.Units = Convert.ToDouble(billingService.Units);
                serviceLine.UnitCharge = 0.00;
                // as per discussion with craig, we do not need to send charges from IO to Kareo
                // If we send change amount to Kareo then it would mean the client would need to maintain the charge amounts in IO.
                //serviceLine.UnitCharge = Convert.ToDouble(billingService.UnitCharge);

                if (!string.IsNullOrWhiteSpace(billingService.DiagnosisCode1))
                    serviceLine.DiagnosisCode1 = billingService.DiagnosisCode1;

                if (!string.IsNullOrWhiteSpace(billingService.DiagnosisCode2))
                    serviceLine.DiagnosisCode2 = billingService.DiagnosisCode2;

                if (!string.IsNullOrWhiteSpace(billingService.DiagnosisCode3))
                    serviceLine.DiagnosisCode3 = billingService.DiagnosisCode3;

                if (!string.IsNullOrWhiteSpace(billingService.DiagnosisCode4))
                    serviceLine.DiagnosisCode4 = billingService.DiagnosisCode4;

                if (!string.IsNullOrWhiteSpace(billingService.Modifier1))
                    serviceLine.ProcedureModifier1 = billingService.Modifier1;

                if (!string.IsNullOrWhiteSpace(billingService.Modifier2))
                    serviceLine.ProcedureModifier2 = billingService.Modifier2;

                if (!string.IsNullOrWhiteSpace(billingService.Modifier3))
                    serviceLine.ProcedureModifier3 = billingService.Modifier3;

                if (!string.IsNullOrWhiteSpace(billingService.Modifier4))
                    serviceLine.ProcedureModifier4 = billingService.Modifier4;

                serviceLineList.Add(serviceLine);
            }


            // Make sure you set the encounter's practice, service location, patient, case, and
            // provider (and any other objects you create relating to the encounter)
            newEncounter.Practice = practice;
            newEncounter.Appointment = appointment;
            newEncounter.ServiceLocation = location;
            newEncounter.Patient = patient;
            //newEncounter.Case = encounterCase;
            //newEncounter.ReferringProvider = provider;
            newEncounter.RenderingProvider = provider;
            newEncounter.ServiceLines = serviceLineList.ToArray();

            // Create the create encounter request object
            CreateEncounterReq request = new CreateEncounterReq();
            request.RequestHeader = ApiHelper.GetRequestHeader();
            request.Encounter = newEncounter;

            // Call the Create Encounter method
            CreateEncounterResp response = _service.CreateEncounter(request);

            ValidationResponse validateResponse = ApiHelper.ValidateResponse(response);
            if (validateResponse.Success)
            {
                return response.EncounterID;
            }
            else
            {
                throw new Exception(validateResponse.ErrorMessage);
            }
        }

        public bool IsConnected(out string errorMessage)
        {
            errorMessage = string.Empty;
            bool isConnected = false;
            try
            {
                GetPracticesReq request = new GetPracticesReq();
                request.RequestHeader = ApiHelper.GetRequestHeader();
                request.Filter = new PracticeFilter
                {
                    PracticeName = string.Empty
                };

                GetPracticesResp response = _service.GetPractices(request);
                ValidationResponse validateResponse = ApiHelper.ValidateResponse(response);
                if (validateResponse.Success && response.Practices != null && response.Practices.ToList().Count > 0)
                {
                    isConnected = true;
                }
                else
                {
                    isConnected = false;
                    errorMessage = "Practices Not Found";
                }
            }
            catch (Exception ex)
            {
                isConnected = false;
                errorMessage = ex.Message;

            }
            return isConnected;

        }
    }
}
