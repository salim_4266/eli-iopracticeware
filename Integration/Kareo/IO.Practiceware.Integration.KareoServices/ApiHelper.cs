﻿using IO.Practiceware.Integration.Domain;
using IO.Practiceware.Integration.KareoServices.KareoAPI;
using IO.Practiceware.Integration.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.KareoServices
{
    public class ApiHelper
    {

        /// <summary>
        /// Create the request header that is required for every call to the Kareo Service 
        /// </summary>
        /// <returns></returns>
        public static RequestHeader GetRequestHeader()
        {
            RequestHeader requestHeader = new RequestHeader();
            requestHeader.CustomerKey = ConfigHelper.ApiCustomerKey;
            requestHeader.User = ConfigHelper.ApiUsername;
            requestHeader.Password = ConfigHelper.ApiPassword;
            return requestHeader;
        }


        public static ValidationResponse ValidateResponse(ResponseBase response)
        {
            ValidationResponse validationResponse = new ValidationResponse();
            validationResponse.Success = true;

            if (response.ErrorResponse.IsError)
            {
                validationResponse.Success = false;
                validationResponse.ErrorMessage = response.ErrorResponse.ErrorMessage;
            }
            if (!response.SecurityResponse.SecurityResultSuccess)
            {
                validationResponse.Success = false;
                validationResponse.ErrorMessage = response.SecurityResponse.SecurityResult;
            }
            return validationResponse;
        }
    }
}

