﻿using IO.Practiceware.Integration.Utility.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Utility
{
    /// <summary>
    /// this class contains properties for all the keys specified in app.config file
    /// </summary>
    public class ConfigHelper
    {
        public static string ApiClientVersion
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.ApiClientVersion);
            }
        }

        public static string ApiCustomerKey
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.ApiCustomerKey);
            }
        }

        public static string ApiUsername
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.ApiUsername);
            }
        }

        public static string ApiPassword
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.ApiPassword);
            }
        }

        public static string ConnectionString
        {
            get
            {
                return GeneralHelper.ConnectionString(AppSettingsKeys.ConnectionString);
            }
        }

        public static bool TestMode
        {
            get
            {
                return Convert.ToBoolean(GeneralHelper.AppSetting(AppSettingsKeys.TestMode));
            }
        }

        public static string SyncInterval
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.SyncInterval);
            }
        }

        public static string KareoApiTimeZone
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.KareoApiTimeZone);
            }
        }

        public static string CustomerTimeZone
        {
            get
            {
                return GeneralHelper.AppSetting(AppSettingsKeys.CustomerTimeZone);
            }
        }

    }
}
