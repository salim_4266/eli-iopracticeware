﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Utility
{
    public class GeneralHelper
    {
        /// <summary>
        /// returns App Settings value for a key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string AppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }

        /// <summary>
        /// retruns Connection String value for a key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ToString();
        }

    }
}

