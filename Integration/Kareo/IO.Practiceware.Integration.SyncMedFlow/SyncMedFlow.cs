﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using IO.Practiceware.Integration.MedFlowEntities;
using System.Xml.Schema;
using System.Xml;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using IO.Practiceware.Integration.MedFlowServices;
using System.Text.RegularExpressions;
using IO.Practiceware.Integration.Utility;
using System.Threading;
using System.Net.Http;
using System.Net;


namespace IO.Practiceware.Integration.SyncMedFlow
{
    public class SyncMedFlow
    {
        //For Local Checking
        //private string _con = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        private int CommandTimeout
        {
            get
            {
                var _commandTimeout = ConfigurationManager.AppSettings["CommandTimeout"] != null ? ConfigurationManager.AppSettings["CommandTimeout"].ToString() : "300";
                return Convert.ToInt32(_commandTimeout);
            }
        }

        public bool CheckConnection()
        {
            bool isConOpen = true;
            for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
            {
                string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (Exception ex)
                    {
                        isConOpen = false;
                        EventLogger.Instance.WriteLog("Connection Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Information);
                    }
                }
            }
            return isConOpen;
        }

        #region Live Methods


        /*************************************** PatientRegistration Functions (ActionType Look Up id - 14 ) starts here *******/

        public bool PatientRegistration()
        {
            EventLogger.Instance.WriteLog("PatientRegistration Event Started.", EventLogEntryType.Information);
            bool isSuccess = false;
            for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
            {
                string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                string _ClientKey = ConfigurationManager.ConnectionStrings[temp].Name.ToString();
                bool isLoopActive = true;
                int PatientTransfered = 0;
                while (isLoopActive)
                {
                    PatientTransfered = 0;
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        try
                        {
                            string MessageData = string.Empty;
                            conn.Open();
                            using (SqlCommand cmd = conn.CreateCommand())
                            {
                                cmd.CommandText = "SELECT Top (500) p.Id,ISNULL(FirstName,'') as FirstName,ISNULL(LastName,'') as LastName, GenderId, DateOfBirth, PatientStatusId, ISNULL(pea.Value,'') as Value, p.ActivationCode FROM Model.Patients p (nolock) Left Join Model.PatientEmailAddresses pea (nolock)  ON p.id = pea.PatientId where p.LastName not like '%Test%' And p.GenderId is not null AND p.DateOfBirth IS NOT NULL and not exists (select 1 from medflow.POrtalQueueXmlResponse where PatientId = p.Id AND ActionTypeLookupId = 14)  order by p.Id ";
                                cmd.CommandTimeout = CommandTimeout;

                                SqlDataAdapter da = new SqlDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                da.Fill(dt);
                                //Check count and then go
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        PatientActivationCode act = new PatientActivationCode();
                                        if (string.IsNullOrEmpty(Convert.ToString(dr["ActivationCode"])))
                                        {
                                            act.Code = Guid.NewGuid().ToString().GetHashCode().ToString("x");

                                            try
                                            {
                                                cmd.CommandText = "";
                                                cmd.CommandText = " UPDATE model.Patients SET ActivationCode = '" + act.Code + "'  WHERE Id = " + Convert.ToString(dr["Id"]);
                                                cmd.CommandType = CommandType.Text;
                                                cmd.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                EventLogger.Instance.WriteLog("Patient  Activation code save failed for patient '" + dr["Id"].ToString() + "'with Exception-" + ex.Message, EventLogEntryType.Error);
                                                isSuccess = false;
                                            }

                                        }
                                        else
                                        {
                                            act.Code = Convert.ToString(dr["ActivationCode"]);
                                        }
                                        act.ExpiryDate = DateTime.Now.AddMonths(12);

                                        PatientActivationMessageData obj = new PatientActivationMessageData();
                                        obj.FirstName = dr["FirstName"].ToString();
                                        obj.LastName = dr["LastName"].ToString();
                                        obj.PatientAccount = dr["Id"].ToString();
                                        obj.Dob = Convert.ToDateTime(dr["DateOfBirth"].ToString());
                                        obj.EmailAddress = dr["Value"].ToString();
                                        if (dr["GenderId"].ToString() == "4")
                                            obj.Gender = "Unknown";
                                        else
                                            obj.Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dr["GenderId"].ToString()));

                                        obj.ActivationCode = act;

                                        PortalQueueXml obj2 = new PortalQueueXml
                                        {
                                            PatientId = Convert.ToInt32(dr["Id"].ToString()),
                                            ActionTypeLookupId =
                                                (int)
                                                    ((ActionTypeLookUps)
                                                        Enum.Parse(typeof(ActionTypeLookUps),
                                                            ActionTypeLookUps.PatientActivation.ToString())),
                                            ProcessedDate = DateTime.Now,
                                            IsActive = Convert.ToBoolean(Convert.ToInt32(dr["PatientStatusId"].ToString())),
                                            MessageData = obj
                                        };

                                        MessageData = GenerateXmlForPatient(obj);
                                        //String Encrypted_MessageData = EncryptDecrypt.Encrypt(MessageData);

                                        //no need to insert into Encounterid For Patient Activation
                                        try
                                        {
                                            cmd.CommandText = "";
                                            cmd.CommandText = "INSERT INTO  Medflow.PortalQueueXML(MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive)";
                                            cmd.CommandText += "VALUES('" + MessageData.Replace("'", "") + "','" + obj2.ActionTypeLookupId + "','" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + obj2.PatientId + "','" + obj2.IsActive + "')";
                                            cmd.CommandText += " SELECT SCOPE_IDENTITY() ";
                                            cmd.CommandType = CommandType.Text;
                                            obj2.ExternalId = Convert.ToInt32(cmd.ExecuteScalar());
                                        }
                                        catch
                                        {
                                            EventLogger.Instance.WriteLog("PatientRegistration -> Patient Registration Failed While Inserting Row in PortalQueue for patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                            isSuccess = false;
                                        }

                                        //function Call for Sending the Data to Medflow using Web IOPW Web Api.

                                        var parameters = new ParameterList { ClientKey = _ClientKey, ActionTypeLookupTypeId = obj2.ActionTypeLookupId, MessageData = MessageData, ProcessedDate = obj2.ProcessedDate, ExternalId = obj2.ExternalId, PatientId = obj2.PatientId, IsActive = 1 };

                                        try
                                        {
                                            MedFlowService.Instance.GetMyWebApi(parameters,true, "", _connectionString);

                                            cmd.CommandText = " SELECT TOP 1 * from Medflow.PortalQueueXMLResponse where PatientId= '" + obj2.PatientId + "' AND ActionTypeLookupId = '" + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                            SqlDataAdapter dapatient = new SqlDataAdapter(cmd);
                                            DataTable dtPatient = new DataTable();
                                            dapatient.Fill(dtPatient);
                                            if (dtPatient.Rows.Count > 0)
                                            {
                                                PatientTransfered = PatientTransfered + 1;
                                                EventLogger.Instance.WriteLog("PatientRegistration Success.", EventLogEntryType.Information);
                                                PatientDemographicsInformation(obj2.PatientId, _connectionString, _ClientKey);
                                            }
                                            else
                                            {
                                                EventLogger.Instance.WriteLog("PatientRegistration Failed for Patient" + obj2.PatientId, EventLogEntryType.Error);
                                            }
                                        }
                                        catch
                                        {
                                            EventLogger.Instance.WriteLog("PatientRegistration -> Patient Registration Failed for patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                            isSuccess = false;
                                        }
                                    }
                                }
                                else
                                {
                                    isLoopActive = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            EventLogger.Instance.WriteLog("PatientRegistration Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                            isSuccess = false;
                        }
                    }
                    if (PatientTransfered == 0)
                    {
                        isLoopActive = false;
                    }
                }
            }
            EventLogger.Instance.WriteLog("PatientRegistration Event Completed.", EventLogEntryType.Information);
            return isSuccess;

        }

        public bool PatientInfoToSecureMessagingDB()
        {
            EventLogger.Instance.WriteLog("Sending patient Info to secure messaging Event has been  Started.", EventLogEntryType.Information);
            bool isSuccess = false;
            for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
            {
                string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                string _ClientKey = ConfigurationManager.ConnectionStrings[temp].Name.ToString();
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    try
                    {

                        string MessageData = string.Empty;
                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {

                            //code change after only patients with email Address needs to be sent 

                            cmd.CommandText = "";
                            cmd.CommandText = "SELECT  p.Id,ISNULL(FirstName,'') AS FirstName,ISNULL(LastName,'') AS LastName,ISNULL(MiddleName,'') AS MiddleName,GenderId,DateOfBirth FROM model.Patients p (nolock)";
                            cmd.CommandText += " WHERE p.LastName NOT LIKE '%Test%' AND p.ActivationCode IS NOT NULL AND p.GenderId is not null AND p.DateOfBirth IS NOT NULL";
                            cmd.CommandText += " AND NOT EXISTS (select 1 from medflow.PortalQueueXML where PatientId = p.Id AND ActionTypeLookupId=27) ";
                            cmd.CommandText += " ORDER BY p.Id ";
                            cmd.CommandTimeout = CommandTimeout;

                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            da.Fill(dt);


                            //Check count and then go
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    try
                                    {
                                        var patientDemo = new PatientDemographicsInfo();
                                        patientDemo.PatientAccount = Convert.ToInt32(dr["Id"]);
                                        patientDemo.FirstName = dr["FirstName"].ToString();
                                        patientDemo.LastName = dr["LastName"].ToString();
                                        patientDemo.MiddleName = dr["MiddleName"].ToString();
                                        patientDemo.DOB = Convert.ToDateTime(dr["DateOfBirth"].ToString());
                                        patientDemo.Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dr["GenderId"].ToString()));
                                        patientDemo.ClientKey = _ClientKey;
                                        SendPatientInfoSecureMessaging(patientDemo, _connectionString);
                                    }
                                    catch (Exception ex)
                                    {
                                        EventLogger.Instance.WriteLog("Error Occured for Patient " + dr["Id"].ToString() + "         " + ex.Message, EventLogEntryType.Error);
                                    }
                                }
                            }
                        }
                        EventLogger.Instance.WriteLog("End of Sending patient Info to secure messaging Event.", EventLogEntryType.Information);
                    }

                    catch (Exception e)
                    {
                        isSuccess = false;
                        EventLogger.Instance.WriteLog("Sending patient info to SecureMessaging DB failed\n" + e.Message + "\n" + e.StackTrace, EventLogEntryType.Error);
                    }
                }
            }
            return isSuccess;
        }

        private void SendPatientInfoSecureMessaging(PatientDemographicsInfo info, string _connectionString)
        {
            string result = string.Empty;
            var _jsontobesent = Newtonsoft.Json.JsonConvert.SerializeObject(info);
            string _iourl = ConfigurationManager.AppSettings["IOPWAPIBaseUrl"].ToString() + "api/Medflow/InsertPatientDemographicsInfo";

            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;

            try
            {
                result = webclient.UploadString(_iourl, "POST", _jsontobesent);
                result = Newtonsoft.Json.JsonConvert.DeserializeObject(result).ToString();
                if (result == "success")
                {
                    using (SqlConnection con = new SqlConnection(_connectionString))
                    {
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (SqlCommand cmd = con.CreateCommand()) //For Successful Insertion
                        {
                            cmd.CommandText = "Insert into Medflow.PortalQueueXML(MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive)";
                            cmd.CommandText += "VALUES('Patient Info For Secure Messaging','27','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + info.PatientAccount + "','1')";
                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                EventLogger.Instance.WriteLog("Error Occrured In Secure Messaging " + e.InnerException + e.Message, EventLogEntryType.Error);
            }
        }

        public bool PatientDemographicsChangeRequest()
        {
            bool isSuccess = false;
            EventLogger.Instance.WriteLog("PatientDemographicsChangeRequest Event Started.", EventLogEntryType.Information);
            for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
            {
                EventLogger.Instance.WriteLog("PatientDemographicsChangeRequest Event Started For '" + ConfigurationManager.ConnectionStrings[temp].Name.ToString() + "' Client Key", EventLogEntryType.Information);
            
                string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                string _ClientKey = ConfigurationManager.ConnectionStrings[temp].Name.ToString();
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    try
                    {
                        string MessageData = string.Empty;
                        string Encrypted_MessageData = string.Empty;
                        int ExternalId = 0;
                        string processedDate = "";
                        string TempPatientId = "";
                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = "Select distinct Top(1000) a.KeyValueNumeric, MAX(p.PatientStatusId) as PatientStatusId, MAX(a.AuditDateTime) as AuditDateTime From [dbo].[AuditEntries] a with (nolock) inner join [dbo].[AuditEntryChanges] b with (nolock) on a.Id = b.AuditEntryId and a.ChangeTypeId <> 0 and a.ObjectName in ('model.Patients', 'model.PatientAddresses') and b.ColumnName != 'ActivationCode' inner join model.Patients p on p.Id = a.KeyValueNumeric and p.LastName not like '%Test%' and P.GenderId is not null and p.DateOfBirth is not null and P.ActivationCode is not null and p.ActivationCode <> '' and a.AuditDateTime > ISNULL((Select top(1) ProcessedDate from medflow.POrtalQueueXmlResponse c with (nolock) where c.ActionTypeLookupId = 25 and c.PatientId = a.KeyValueNumeric order by ProcessedDate Desc),(Select top(1) ProcessedDate from medflow.POrtalQueueXmlResponse c with (nolock) where c.ActionTypeLookupId = 23 and c.PatientId = a.KeyValueNumeric order by ProcessedDate Desc)) and Cast(a.AuditDateTime as Date) > '2017-01-01' group by a.KeyValueNumeric";
                            cmd.CommandTimeout = CommandTimeout;
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            da.Fill(dt);

                            //Check count and then go
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    processedDate = dr["AuditDateTime"].ToString();
                                    TempPatientId = dr["KeyValueNumeric"].ToString();
                                    string _selectQry = "SELECT distinct  p.FirstName, p.LastName, MiddleName, p.Id, p.DateOfBirth, p.GenderId, pe.Value, r.Name AS Race, (Case	When r.Name = 'American Indian or Alaska Native' Then '1002-5' When r.Name = 'Asian' Then '2028-9' When r.Name = 'Black or African American' Then '2054-5' When r.Name = 'Native Hawaiian or Other Pacific Islander' Then '2076-8' When r.Name = 'White' Then '2106-3' Else '' END) AS RaceCode, (Case when cmte.Id =1 then 'Mail' When cmte.Id = 6 Then 'Unknown' Else 'Phone' End) AS PreferredCommunication, l.Name AS    PreferredLanguage, l.Id as PreferredLanguageCode, E.Name as Ethnicity, (Case	when e.Name = 'Hispanic or Latino' Then '2135-2' ELSE '0000' END) as EthnicityCode FROM model.Patients p join dbo.PatientDemographics pd on p.Id = pd.PatientId LEFT join  model.PatientEmailAddresses pe on  p.id = pe.PatientId  left join   model.PatientCommunicationPreferences pcp on pcp.PatientId = p.Id left join model.CommunicationMethodType_Enumeration cmte on cmte.Id = pcp.CommunicationMethodTypeId and cmte.Id in(1,6,5) left Join model.PatientRace pr on pr.Patients_Id = p.Id left join model.Races r on r.Id = pr.Races_Id left join model.Languages l on l.id = p.LanguageId left join  model.Ethnicities e on e.Id = p.EthnicityId Where p.id = " + TempPatientId;
                                    cmd.CommandText = _selectQry;
                                    cmd.CommandType = CommandType.Text;
                                    SqlDataAdapter patientAdapter = new SqlDataAdapter(cmd);
                                    DataTable patientTable = new DataTable();
                                    patientAdapter.Fill(patientTable);
                                    PatientDemographics objPatient = new PatientDemographics();
                                    if (patientTable.Rows.Count > 0)
                                    {
                                        objPatient.PatientId = TempPatientId;
                                        objPatient.FirstName = patientTable.Rows[0]["FirstName"].ToString();
                                        objPatient.LastName = patientTable.Rows[0]["LastName"].ToString();
                                        if (patientTable.Rows[0]["GenderId"].ToString() == "4")
                                            objPatient.Gender = "Unknown";
                                        else
                                            objPatient.Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(patientTable.Rows[0]["GenderId"].ToString()));
                                        objPatient.DOB = patientTable.Rows[0]["DateOfBirth"].ToString();
                                        objPatient.Ethnicity = patientTable.Rows[0]["Ethnicity"].ToString();
                                        objPatient.EthnicityCode = patientTable.Rows[0]["EthnicityCode"].ToString();
                                        objPatient.MiddleName = patientTable.Rows[0]["MiddleName"].ToString();
                                        objPatient.PreferredCommunication = patientTable.Rows[0]["PreferredCommunication"].ToString();
                                        objPatient.PreferredLanguage = patientTable.Rows[0]["PreferredLanguage"].ToString();
                                        objPatient.PreferredLanguageCode = patientTable.Rows[0]["PreferredLanguageCode"].ToString();
                                        objPatient.Race = patientTable.Rows[0]["Race"].ToString();
                                        objPatient.RaceCode = patientTable.Rows[0]["RaceCode"].ToString();
                                        objPatient.EmailAddress = patientTable.Rows[0]["Value"].ToString();
                                        objPatient.Addresses = GetPatientAddresses(TempPatientId, _connectionString);
                                        objPatient.PhoneNumber = GetPatientPhoneNumbers(TempPatientId, _connectionString);
                                    }

                                    MessageData = GenerateXmlForPatientDemographicsChangeRequest(objPatient);
                                    try
                                    {
                                        cmd.CommandText = "";
                                        cmd.CommandText = "INSERT INTO Medflow.PortalQueueXML(MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive)";
                                        cmd.CommandText += "VALUES('" + MessageData.Replace("'", "") + "', '25', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + TempPatientId + "', '" + Convert.ToInt16(dr["PatientStatusId"]) + "')";
                                        cmd.CommandText += " SELECT SCOPE_IDENTITY() ";
                                        cmd.CommandType = CommandType.Text;
                                        ExternalId = Convert.ToInt32(cmd.ExecuteScalar());
                                    }
                                    catch
                                    {
                                        EventLogger.Instance.WriteLog("PatientRegistration -> Patient Registration Failed While Inserting Row in PortalQueue for patient '" + TempPatientId + "'", EventLogEntryType.Error);
                                        isSuccess = false;
                                    }

                                    //function Call for Sending the Data to Medflow using Web IOPW Web Api.
                                    var parameters = new ParameterList { ClientKey = _ClientKey, ActionTypeLookupTypeId = 25, MessageData = MessageData, ProcessedDate = Convert.ToDateTime(processedDate).AddMinutes(5.0), ExternalId = ExternalId, PatientId = Convert.ToInt32(TempPatientId), IsActive = 1 };

                                    try
                                    {
                                        MedFlowService.Instance.GetMyWebApi(parameters, true, "", _connectionString);
                                        cmd.CommandText = " SELECT TOP 1 * from Medflow.PortalQueueXMLResponse where PatientId= '" + TempPatientId + "' AND ActionTypeLookupId = '25' AND ProcessedDate = '" + Convert.ToDateTime(processedDate).AddMinutes(5.0) + "'";
                                        SqlDataAdapter dapatient = new SqlDataAdapter(cmd);
                                        DataTable dtPatient = new DataTable();
                                        dapatient.Fill(dtPatient);
                                        if (dtPatient.Rows.Count > 0)
                                        {
                                            EventLogger.Instance.WriteLog("PatientDemographicsChangeRequest Success. for PatientId " + TempPatientId, EventLogEntryType.Information);
                                        }
                                        else
                                        {
                                            EventLogger.Instance.WriteLog("PatientDemographicsChangeRequest Failed for Patient " + TempPatientId, EventLogEntryType.Error);
                                        }
                                    }
                                    catch
                                    {
                                        EventLogger.Instance.WriteLog("PatientDemographicsChangeRequest -> Patient Registration Failed for patient '" + TempPatientId + "'", EventLogEntryType.Error);
                                        isSuccess = false;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogger.Instance.WriteLog("PatientDemographicsChangeRequest Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                        isSuccess = false;
                    }

                }
            }
            EventLogger.Instance.WriteLog("PatientDemographicsChangeRequest Event Completed.", EventLogEntryType.Information);
            return isSuccess;
        }

        public IList<PatientAddress> GetPatientAddresses(string PatientId, string _connectionString)
        {
            IList<PatientAddress> objPatientAddress = new List<PatientAddress>();
            string _selectQry = "Select ISNULL(Line1,'') as Line1, ISNULL(Line2,'') as Line2, ISNULL(PostalCode,'') as PostalCode, ISNULL(a.City,'') as City, ISNULL(C.Name, '') as State, ISNULL(b.Name,'') as AddressType From Model.PatientAddresses a inner join model.PatientAddressTypes b on a.PatientAddressTypeId = b.Id inner join [model].[StateOrProvinces] c on C.Id = a.StateOrProvinceId and a.PatientId = " + PatientId;
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand(_selectQry, conn);
                    SqlDataAdapter AddressAdapter = new SqlDataAdapter(cmd);
                    DataTable AddressTable = new DataTable();
                    AddressAdapter.Fill(AddressTable);
                    if (AddressTable.Rows.Count > 0)
                    {
                        foreach (DataRow rdr in AddressTable.Rows)
                        {
                            PatientAddress objTemp = new PatientAddress()
                            {
                                City = rdr["City"].ToString(),
                                Line1 = rdr["Line1"].ToString(),
                                Line2 = rdr["Line2"].ToString(),
                                State = rdr["State"].ToString(),
                                Type = rdr["AddressType"].ToString(),
                                ZipCode = rdr["PostalCode"].ToString()
                            };
                            objPatientAddress.Add(objTemp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("GetPatientAddresses Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }
            return objPatientAddress;
        }

        public IList<PatientPhoneNumber> GetPatientPhoneNumbers(string PatientId, string _connectionString)
        {
            IList<PatientPhoneNumber> objPatientNumbers = new List<PatientPhoneNumber>();
            string _selectQry = "Select PatientPhoneNumberTypeId, ExchangeAndSuffix From Model.PatientPhoneNumbers PPN inner join model.Patients P on PPN.PatientId = P.Id and P.Id = " + PatientId;
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand(_selectQry, conn);
                    SqlDataAdapter TempAdapter = new SqlDataAdapter(cmd);
                    DataTable PhoneTable = new DataTable();
                    TempAdapter.Fill(PhoneTable);
                    foreach (DataRow rdr in PhoneTable.Rows)
                    {
                        PatientPhoneNumber objTemp = new PatientPhoneNumber()
                        {
                            Type = rdr["PatientPhoneNumberTypeId"].ToString(),
                            Number = rdr["ExchangeAndSuffix"].ToString(),
                        };
                        objPatientNumbers.Add(objTemp);
                    }
                }
            }
            catch (Exception ee)
            {

            }
            return objPatientNumbers;
        }

        public string GenerateXmlForPatient(PatientActivationMessageData pam)
        {
            string xml = string.Empty;
            try
            {
                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument xmlDoc = new XmlDocument();
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

                XmlElement documentElement = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);

                XmlSchema schema = new XmlSchema();
                schema.Namespaces.Add("xsi", xsi);
                schema.Namespaces.Add("xsd", xsd);
                xmlDoc.Schemas.Add(schema);

                XmlElement schemaNode = xmlDoc.CreateElement("Xml");
                schemaNode.SetAttribute("xmlns:xsi", xsi);
                schemaNode.SetAttribute("xmlns:xsd", xsd);
                xmlDoc.AppendChild(schemaNode);

                XmlNode rootNode = xmlDoc.CreateElement("Type");
                rootNode.InnerText = "Patient Activation";
                schemaNode.AppendChild(rootNode);

                XmlNode patient = xmlDoc.CreateElement("Patient");
                schemaNode.AppendChild(patient);

                XmlNode firstName = xmlDoc.CreateElement("FirstName");
                firstName.InnerText = Convert.ToString(pam.FirstName);
                patient.AppendChild(firstName);
                XmlNode lastName = xmlDoc.CreateElement("LastName");
                lastName.InnerText = Convert.ToString(pam.LastName);
                patient.AppendChild(lastName);
                XmlNode middleName = xmlDoc.CreateElement("MiddleName");
                middleName.InnerText = Convert.ToString(pam.MiddleName);
                patient.AppendChild(middleName);
                XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
                patientAccount.InnerText = Convert.ToString(pam.PatientAccount);
                patient.AppendChild(patientAccount);
                XmlNode dob = xmlDoc.CreateElement("DOB");
                dob.InnerText = Convert.ToString(pam.Dob.ToString("MM/dd/yyyy"));
                patient.AppendChild(dob);
                XmlNode gender = xmlDoc.CreateElement("Gender");
                gender.InnerText = Convert.ToString(pam.Gender);
                patient.AppendChild(gender);
                XmlNode emailAddress = xmlDoc.CreateElement("EmailAddress");
                emailAddress.InnerText = Convert.ToString(pam.EmailAddress);
                patient.AppendChild(emailAddress);

                XmlNode activation = xmlDoc.CreateElement("Activation");
                schemaNode.AppendChild(activation);

                XmlNode code = xmlDoc.CreateElement("Code");
                code.InnerText = Convert.ToString(pam.ActivationCode.Code);
                activation.AppendChild(code);
                XmlNode expiryDate = xmlDoc.CreateElement("ExpiryDate");
                expiryDate.InnerText = Convert.ToString(pam.ActivationCode.ExpiryDate.ToString("MM/dd/yyyy"));
                activation.AppendChild(expiryDate);
                xml = xmlDoc.InnerXml;
                EventLogger.Instance.WriteLog("PatientRegistration -> Xml generation successfully for patient '" + pam.PatientAccount + "'", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("PatientRegistration -> Xml generation failed for patient '" + pam.PatientAccount + "'  \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }
            return xml;
        }
        /************************PatientRegistration Functions (ActionType Look Up id - 14 ) Ends here**************************/

        public string GenerateXmlForPatientDemographicsChangeRequest(PatientDemographics Pat)
        {
            string xml = string.Empty;
            try
            {
                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument xmlDoc = new XmlDocument();
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

                XmlElement documentElement = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);

                XmlSchema schema = new XmlSchema();
                schema.Namespaces.Add("xsi", xsi);
                schema.Namespaces.Add("xsd", xsd);
                xmlDoc.Schemas.Add(schema);

                XmlElement schemaNode = xmlDoc.CreateElement("Xml");
                schemaNode.SetAttribute("xmlns:xsi", xsi);
                schemaNode.SetAttribute("xmlns:xsd", xsd);
                xmlDoc.AppendChild(schemaNode);

                XmlNode rootNode = xmlDoc.CreateElement("Type");
                rootNode.InnerText = "Update Demographics information";
                schemaNode.AppendChild(rootNode);

                XmlNode patient = xmlDoc.CreateElement("Patient");
                schemaNode.AppendChild(patient);

                XmlNode firstName = xmlDoc.CreateElement("FirstName");
                firstName.InnerText = Convert.ToString(Pat.FirstName);
                patient.AppendChild(firstName);

                XmlNode lastName = xmlDoc.CreateElement("LastName");
                lastName.InnerText = Convert.ToString(Pat.LastName);
                patient.AppendChild(lastName);

                XmlNode middleName = xmlDoc.CreateElement("MiddleName");
                middleName.InnerText = Convert.ToString(Pat.MiddleName);
                patient.AppendChild(middleName);

                XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
                patientAccount.InnerText = Convert.ToString(Pat.PatientId);
                patient.AppendChild(patientAccount);

                XmlNode dob = xmlDoc.CreateElement("DOB");
                dob.InnerText = Convert.ToDateTime(Pat.DOB).ToString("MM/dd/yyyy");
                patient.AppendChild(dob);

                XmlNode gender = xmlDoc.CreateElement("Gender");
                gender.InnerText = Convert.ToString(Pat.Gender);
                patient.AppendChild(gender);

                XmlNode emailAddress = xmlDoc.CreateElement("EmailAddress");
                emailAddress.InnerText = Convert.ToString(Pat.EmailAddress);
                patient.AppendChild(emailAddress);

                XmlNode Addresses = xmlDoc.CreateElement("Addresses");
                patient.AppendChild(Addresses);

                foreach (PatientAddress pAddress in Pat.Addresses)
                {
                    XmlNode Address = xmlDoc.CreateElement("Address");
                    Addresses.AppendChild(Address);

                    XmlNode Type = xmlDoc.CreateElement("Type");
                    Type.InnerText = Convert.ToString(pAddress.Type);
                    Address.AppendChild(Type);

                    XmlNode Line1 = xmlDoc.CreateElement("Line1");
                    Line1.InnerText = Convert.ToString(pAddress.Line1);
                    Address.AppendChild(Line1);

                    XmlNode Line2 = xmlDoc.CreateElement("Line2");
                    Line2.InnerText = Convert.ToString(pAddress.Line2);
                    Address.AppendChild(Line2);

                    XmlNode ZipCode = xmlDoc.CreateElement("ZipCode");
                    ZipCode.InnerText = Convert.ToString(pAddress.ZipCode);
                    Address.AppendChild(ZipCode);

                    XmlNode City = xmlDoc.CreateElement("City");
                    City.InnerText = Convert.ToString(pAddress.City);
                    Address.AppendChild(City);

                    XmlNode State = xmlDoc.CreateElement("State");
                    State.InnerText = Convert.ToString(pAddress.State);
                    Address.AppendChild(State);
                }

                XmlNode PhoneNumbers = xmlDoc.CreateElement("PhoneNumbers");
                patient.AppendChild(PhoneNumbers);

                foreach (PatientPhoneNumber phoneNumber in Pat.PhoneNumber)
                {
                    XmlNode Phone = xmlDoc.CreateElement("Phone");
                    PhoneNumbers.AppendChild(Phone);

                    XmlNode Type = xmlDoc.CreateElement("Type");
                    if (phoneNumber.Type == "2")
                    {
                        Type.InnerText = "Work";
                    }
                    else if (phoneNumber.Type == "3")
                    {
                        Type.InnerText = "Cell";
                    }
                    else if (phoneNumber.Type == "7")
                    {
                        Type.InnerText = "Home";
                    }
                    else if (phoneNumber.Type == "12")
                    {
                        Type.InnerText = "Cell";
                    }
                    else if (phoneNumber.Type == "14")
                    {
                        Type.InnerText = "Fax";
                    }
                    else if (phoneNumber.Type == "15")
                    {
                        Type.InnerText = "Cell";
                    }
                    else if (phoneNumber.Type == "16")
                    {
                        Type.InnerText = "Home";
                    }
                    else if (phoneNumber.Type == "17")
                    {
                        Type.InnerText = "Home";
                    }
                    Phone.AppendChild(Type);

                    XmlNode Number = xmlDoc.CreateElement("Number");
                    Number.InnerText = Convert.ToString(phoneNumber.Number);
                    Phone.AppendChild(Number);
                }

                XmlNode Race = xmlDoc.CreateElement("Race");
                Race.InnerText = Convert.ToString(Pat.Race);
                patient.AppendChild(Race);

                XmlNode RaceCode = xmlDoc.CreateElement("RaceCode");
                RaceCode.InnerText = Convert.ToString(Pat.RaceCode);
                patient.AppendChild(RaceCode);

                XmlNode PreferredCommunication = xmlDoc.CreateElement("PreferredCommunication");
                PreferredCommunication.InnerText = Convert.ToString(Pat.PreferredCommunication);
                patient.AppendChild(PreferredCommunication);

                XmlNode PreferredLanguage = xmlDoc.CreateElement("PreferredLanguage");
                PreferredLanguage.InnerText = Convert.ToString(Pat.PreferredLanguage);
                patient.AppendChild(PreferredLanguage);

                XmlNode PreferredLanguageCode = xmlDoc.CreateElement("PreferredLanguageCode");
                PreferredLanguageCode.InnerText = Convert.ToString(Pat.PreferredLanguageCode);
                patient.AppendChild(PreferredLanguageCode);

                XmlNode Ethnicity = xmlDoc.CreateElement("Ethnicity");
                Ethnicity.InnerText = Convert.ToString(Pat.Ethnicity);
                patient.AppendChild(Ethnicity);

                XmlNode EthnicityCode = xmlDoc.CreateElement("EthnicityCode");
                EthnicityCode.InnerText = Convert.ToString(Pat.EthnicityCode);
                patient.AppendChild(EthnicityCode);

                XmlNode Image = xmlDoc.CreateElement("Image");
                patient.AppendChild(Image);

                xml = xmlDoc.InnerXml;
                EventLogger.Instance.WriteLog("PatientRegistration -> Xml generation successfully for patient '" + Pat.PatientId + "'", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("PatientRegistration -> Xml generation failed for patient '" + Pat.PatientId + "'  \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }
            return xml;
        }


        /************************PatientAppointment Functions (ActionType Look Up id - 16 ) starts here*************************/
        public bool PatientAppointment()
        {
            bool isSuccess = true;
            EventLogger.Instance.WriteLog(" Patient Appointment Information Event Started.", EventLogEntryType.Information);
            for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
            {
                string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                string _ClientKey = ConfigurationManager.ConnectionStrings[temp].Name.ToString();
                EventLogger.Instance.WriteLog(" Patient Appointment Information Event Started For Key." + _ClientKey, EventLogEntryType.Information);
                int PatientTransfered = 0;
                bool isLoopActive = true;
                while (isLoopActive)
                {
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        try
                        {
                            string MessageData = string.Empty;
                            conn.Open();
                            using (SqlCommand cmd = conn.CreateCommand())
                            {
                                cmd.CommandText = "SELECT Distinct Top(100) p.Id,p.FirstName,p.LastName,p.MiddleName,p.GenderId,p.DateOfBirth FROM model.Patients p INNER join model.Encounters e on p.id= e.PatientId Inner join Medflow.PortalQueueXML pe on  p.id = pe.PatientId AND pe.ActionTypeLookupId  in (14, 23) Where p.LastName not like '%Test%' and NOT EXISTS (SELECT 1 from MedFlow.PortalQueueXML where EncounterId = e.Id AND ActionTypeLookupId=16) order by p.Id";
                                cmd.CommandTimeout = CommandTimeout;

                                SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                                DataTable dtPatientInformation = new DataTable();
                                da1.Fill(dtPatientInformation);

                                //How to Check here before insertion.


                                if (dtPatientInformation.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtPatientInformation.Rows)
                                    {
                                        PatientActivationCode act = new PatientActivationCode();

                                        PatientActivationMessageData obj = new PatientActivationMessageData
                                        {
                                            FirstName = dr["FirstName"].ToString(),
                                            LastName = dr["LastName"].ToString(),
                                            MiddleName = dr["MiddleName"].ToString(),
                                            PatientAccount = dr["Id"].ToString(),
                                            Dob = Convert.ToDateTime(dr["DateOfBirth"].ToString()),
                                            Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dr["GenderId"].ToString()))
                                        };

                                        cmd.CommandText = "Select pat.Id, pat.FirstName, pat.LastName, pat.GenderId, Pat.DateOfBirth, appmt.Appointmentid, appmt.ResourceId1 as ProviderId,AppType.AppointmentType as Description, Enc.StartDateTime as Date,sc.Name as Facility from dbo.appointments appmt join model.Encounters Enc on appmt.PatientId = Enc.PatientId and appmt.Encounterid = enc.id join model.Patients pat	on appmt.PatientId = pat.Id  join dbo.appointmentType AppType on	AppType.AppTypeId = appmt.AppTypeId left join model.ServiceLocationCodes sc on appmt.resourceid2 = sc.Id where appmt.PatientId = @PatientID and appmt.ScheduleStatus <> 'F' Order by Date asc";

                                        cmd.Parameters.Add("@PatientID", SqlDbType.NVarChar).Value = dr["Id"].ToString();

                                        SqlDataAdapter daAppointments = new SqlDataAdapter(cmd);
                                        DataTable dtAppointments = new DataTable();
                                        daAppointments.Fill(dtAppointments);
                                        cmd.Parameters.Clear();

                                        // For Converting a Datatable to List

                                        if (dtAppointments.Rows.Count > 0)
                                        {
                                            PortalQueueXml obj2 = new PortalQueueXml();
                                            obj2.PatientId = Convert.ToInt32(dr["Id"].ToString());
                                            obj2.ActionTypeLookupId = (int)((ActionTypeLookUps)Enum.Parse(typeof(ActionTypeLookUps), ActionTypeLookUps.Appointments.ToString()));
                                            obj2.ProcessedDate = DateTime.Now;
                                            obj2.ExternalId = 2;

                                            List<PatientActivationMessageData> AppointmentList = new List<PatientActivationMessageData>();

                                            for (int i = 0; i < dtAppointments.Rows.Count; i++)
                                            {
                                                PatientActivationMessageData App = new PatientActivationMessageData();
                                                App.FirstName = dtAppointments.Rows[i]["FirstName"].ToString();
                                                App.LastName = dtAppointments.Rows[i]["LastName"].ToString();
                                                App.Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dtAppointments.Rows[i]["GenderId"].ToString()));
                                                App.PatientAccount = dtAppointments.Rows[i]["Id"].ToString();
                                                App.Dob = Convert.ToDateTime(dtAppointments.Rows[i]["DateOfBirth"].ToString());
                                                App.Appointmentid = Convert.ToInt32(dtAppointments.Rows[i]["Appointmentid"]);
                                                App.ProviderId = dtAppointments.Rows[i]["ProviderId"].ToString();
                                                App.ExternalId = dtAppointments.Rows[i]["Appointmentid"].ToString();
                                                App.Description = dtAppointments.Rows[i]["Description"].ToString();
                                                App.AppointmentDate = Convert.ToDateTime(dtAppointments.Rows[i]["Date"]);
                                                App.FacilityName = dtAppointments.Rows[i]["Facility"].ToString();
                                                App.Status = "booked";
                                                AppointmentList.Add(App);

                                                // Insert into Portal Queue XML
                                                cmd.CommandText = "";
                                                cmd.CommandText = "INSERT INTO  Medflow.PortalQueueXML(ActionTypeLookupId,ProcessedDate,PatientId,IsActive,EncounterId)";
                                                cmd.CommandText += "VALUES('" + obj2.ActionTypeLookupId + "','" + obj2.ProcessedDate.ToString() + "','" + obj2.PatientId + "','" + 1 + "','" + dtAppointments.Rows[i]["Appointmentid"].ToString() + "')";
                                                cmd.CommandText += " SELECT SCOPE_IDENTITY()";
                                                cmd.CommandType = CommandType.Text;
                                                obj2.ExternalId = Convert.ToInt32(cmd.ExecuteScalar());
                                            }
                                            MessageData = GenerateXmlForPatientAppointment(AppointmentList);
                                            try
                                            {
                                                //Finally update the Table with the MessageData Value
                                                cmd.CommandText = "";
                                                cmd.CommandText = "UPDATE Medflow.PortalQueueXML SET MessageData = '" + MessageData.Replace("'", "") + "'  WHERE Patientid = '" + obj2.PatientId + "' and ActionTypeLookupId = " + obj2.ActionTypeLookupId;
                                                cmd.CommandType = CommandType.Text;
                                                cmd.ExecuteNonQuery();

                                                EventLogger.Instance.WriteLog("PatientAppointment -> Patient Appointment created for patient '" + obj2.PatientId + "'", EventLogEntryType.Information);

                                            }
                                            catch (Exception ex)
                                            {
                                                EventLogger.Instance.WriteLog("Patient Appointment Failed for patient '" + obj2.PatientId + "' due to the Exception- " + ex.Message, EventLogEntryType.Error);
                                            }

                                            //sending patient appointment Data to Medflow.

                                            var Parameters = new ParameterList
                                            {
                                                ClientKey = _ClientKey,
                                                ActionTypeLookupTypeId = obj2.ActionTypeLookupId,
                                                MessageData = MessageData,
                                                ProcessedDate = obj2.ProcessedDate,
                                                ExternalId = obj2.ExternalId,
                                                PatientId = obj2.PatientId,
                                                IsActive = 1
                                            };

                                            try
                                            {
                                                MedFlowService.Instance.GetMyWebApi(Parameters, true, "", _connectionString);

                                                cmd.Parameters.Clear();
                                                cmd.CommandText = " SELECT TOP 1 * from Medflow.PortalQueueXMLResponse where PatientId= '" + obj2.PatientId + "' AND ActionTypeLookupId = '" + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString() + "'";
                                                SqlDataAdapter dapatient = new SqlDataAdapter(cmd);
                                                DataTable dtPatient = new DataTable();
                                                dapatient.Fill(dtPatient);
                                                if (dtPatient.Rows.Count > 0)
                                                {
                                                    PatientTransfered = PatientTransfered + 1;
                                                    EventLogger.Instance.WriteLog("PatientAppointment -> Patient Appointment Insertion is Successful.", EventLogEntryType.Information);
                                                    isSuccess = true;
                                                }
                                                else
                                                {
                                                    cmd.Parameters.Clear();
                                                    cmd.CommandText = "";
                                                    cmd.CommandText = "DELETE FROM Medflow.PortalQueueXML WHERE Patientid = '" + obj2.PatientId + "' and ActionTypeLookupId = " + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString() + "'";
                                                    cmd.CommandType = CommandType.Text;
                                                    cmd.ExecuteNonQuery();
                                                    EventLogger.Instance.WriteLog("PatientAppointment -> Patient Appointment Insertion Failed for PatientId" + obj2.PatientId, EventLogEntryType.Error);
                                                }
                                            }
                                            catch
                                            {
                                                cmd.Parameters.Clear();
                                                cmd.CommandText = "";
                                                cmd.CommandText = "DELETE FROM Medflow.PortalQueueXML WHERE Patientid = '" + obj2.PatientId + "' and ActionTypeLookupId = " + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString() + "'";
                                                cmd.CommandType = CommandType.Text;
                                                cmd.ExecuteNonQuery();
                                                EventLogger.Instance.WriteLog("PatientAppointment -> Patient Appointment Failed for a Patient During Accessing MedFlow API '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                            }
                                        }
                                        else
                                        {
                                            isSuccess = false;
                                        }
                                    }
                                }
                                else
                                {
                                    isLoopActive = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            EventLogger.Instance.WriteLog(" PatientAppointment -> Patient Appointment Details Insertion Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                        }
                    }
                    if (PatientTransfered == 0)
                    {
                        isLoopActive = false;
                    }
                }
            }
            EventLogger.Instance.WriteLog("End of Patient Appointment Information Event.", EventLogEntryType.Information);
            return isSuccess;
        }


        /************************PatientAppointment Functions (ActionType Look Up id - 16 ) starts here*************************/
        public bool PatientExisitngAppointment()
        {
            bool isSuccess = true;
            EventLogger.Instance.WriteLog("Patient Existing Appointment Update Information Event Started", EventLogEntryType.Information);
            for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
            {
                string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                string _ClientKey = ConfigurationManager.ConnectionStrings[temp].Name.ToString();
                EventLogger.Instance.WriteLog("Patient Existing Appointment Update Information Event Started For Key." + _ClientKey, EventLogEntryType.Information);
                int PatientTransfered = 0;
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    try
                    {
                        string MessageData = string.Empty;
                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = "Select distinct top(1000) (Select PatientId From dbo.Appointments Where AppointmentId = a.KeyValueNumeric) as PatientId, a.AuditDateTime as RecordDate From dbo.AuditEntries a Where a.ChangeTypeId = 1 and a.ObjectName = 'model.Appointments' and Cast(a.AuditDateTime as Date) > '2017-01-01' and a.AuditDateTime > (Select Top(1) ProcessedDate From medflow.PortalQueueXml Where EncounterId = Cast(a.KeyValueNumeric as int) and ActionTypeLookUpId = 16 Order by ProcessedDate Desc) Order by PatientId";
                            cmd.CommandTimeout = CommandTimeout;

                            SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                            DataTable dtPatientInformation = new DataTable();
                            da1.Fill(dtPatientInformation);

                            if (dtPatientInformation.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dtPatientInformation.Rows)
                                {
                                    string PatientId = dr["PatientId"].ToString();
                                    DateTime Record_DateTime = Convert.ToDateTime(dr["RecordDate"]);

                                    cmd.CommandText = "Select pat.Id as PatientId, pat.FirstName, pat.LastName, pat.MiddleName, pat.GenderId, Pat.DateOfBirth, appmt.Appointmentid, appmt.ResourceId1 as ProviderId,AppType.AppointmentType as Description, Enc.StartDateTime as Date,sc.Name as Facility from dbo.appointments appmt join model.Encounters Enc on appmt.PatientId = Enc.PatientId and appmt.Encounterid = enc.id join model.Patients pat	on appmt.PatientId = pat.Id  join dbo.appointmentType AppType on	AppType.AppTypeId = appmt.AppTypeId left join model.ServiceLocationCodes sc on appmt.resourceid2 = sc.Id where appmt.PatientId = '" + PatientId + "' and appmt.ScheduleStatus <> 'F' Order by Date asc";

                                    SqlDataAdapter daAppointments = new SqlDataAdapter(cmd);
                                    DataTable dtAppointments = new DataTable();
                                    daAppointments.Fill(dtAppointments);
                                    cmd.Parameters.Clear();

                                    PatientActivationMessageData obj = new PatientActivationMessageData();

                                    // For Converting a Datatable to List

                                    if (dtAppointments.Rows.Count > 0)
                                    {
                                        obj.FirstName = dtAppointments.Rows[0]["FirstName"].ToString();
                                        obj.LastName = dtAppointments.Rows[0]["LastName"].ToString();
                                        obj.MiddleName = dtAppointments.Rows[0]["MiddleName"].ToString();
                                        obj.PatientAccount = dtAppointments.Rows[0]["PatientId"].ToString();
                                        obj.Dob = Convert.ToDateTime(dtAppointments.Rows[0]["DateOfBirth"].ToString());
                                        obj.Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dtAppointments.Rows[0]["GenderId"].ToString()));

                                        PortalQueueXml obj2 = new PortalQueueXml();
                                        obj2.PatientId = Convert.ToInt32(dtAppointments.Rows[0]["PatientId"].ToString());
                                        obj2.ActionTypeLookupId = (int)((ActionTypeLookUps)Enum.Parse(typeof(ActionTypeLookUps), ActionTypeLookUps.Appointments.ToString()));
                                        obj2.ProcessedDate = DateTime.Now;
                                        obj2.ExternalId = 2;

                                        List<PatientActivationMessageData> AppointmentList = new List<PatientActivationMessageData>();
                                        for (int i = 0; i < dtAppointments.Rows.Count; i++)
                                        {
                                            PatientActivationMessageData App = new PatientActivationMessageData();
                                            App.FirstName = dtAppointments.Rows[i]["FirstName"].ToString();
                                            App.LastName = dtAppointments.Rows[i]["LastName"].ToString();
                                            App.Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dtAppointments.Rows[i]["GenderId"].ToString()));
                                            App.PatientAccount = dtAppointments.Rows[i]["PatientId"].ToString();
                                            App.Dob = Convert.ToDateTime(dtAppointments.Rows[i]["DateOfBirth"].ToString());
                                            App.Appointmentid = Convert.ToInt32(dtAppointments.Rows[i]["Appointmentid"]);
                                            App.ProviderId = dtAppointments.Rows[i]["ProviderId"].ToString();
                                            App.ExternalId = dtAppointments.Rows[i]["Appointmentid"].ToString();
                                            App.Description = dtAppointments.Rows[i]["Description"].ToString();
                                            App.AppointmentDate = Convert.ToDateTime(dtAppointments.Rows[i]["Date"]);
                                            App.FacilityName = dtAppointments.Rows[i]["Facility"].ToString();
                                            App.Status = "Booked";
                                            AppointmentList.Add(App);

                                            // Insert into Portal Queue XML
                                            cmd.CommandText = "";
                                            cmd.CommandText = "INSERT INTO  Medflow.PortalQueueXML(ActionTypeLookupId,ProcessedDate,PatientId,IsActive,EncounterId)";
                                            cmd.CommandText += "VALUES('" + obj2.ActionTypeLookupId + "','" + DateTime.Now.ToString() + "','" + obj2.PatientId + "','" + 1 + "','" + dtAppointments.Rows[0]["Appointmentid"].ToString() + "')";
                                            cmd.CommandText += " SELECT SCOPE_IDENTITY()";
                                            cmd.CommandType = CommandType.Text;
                                            obj2.ExternalId = Convert.ToInt32(cmd.ExecuteScalar());
                                        }

                                        MessageData = GenerateXmlForPatientAppointment(AppointmentList);
                                        try
                                        {
                                            //Finally update the Table with the MessageData Value
                                            cmd.CommandText = "";
                                            cmd.CommandText = "UPDATE Medflow.PortalQueueXML SET MessageData = '" + MessageData.Replace("'", "") + "', ProcessedDate = '" + Record_DateTime.AddMinutes(15).ToString() + "' WHERE Patientid = '" + obj2.PatientId + "' and ActionTypeLookupId = " + obj2.ActionTypeLookupId;
                                            cmd.CommandType = CommandType.Text;
                                            cmd.ExecuteNonQuery();

                                            EventLogger.Instance.WriteLog("PatientAppointment -> Patient Appointment created for patient '" + obj2.PatientId + "'", EventLogEntryType.Information);

                                        }
                                        catch (Exception ex)
                                        {
                                            EventLogger.Instance.WriteLog("Patient Appointment Failed for patient '" + obj2.PatientId + "' due to the Exception- " + ex.Message, EventLogEntryType.Error);
                                        }

                                        //sending patient appointment Data to Medflow.

                                        var Parameters = new ParameterList
                                        {
                                            ClientKey = _ClientKey,
                                            ActionTypeLookupTypeId = obj2.ActionTypeLookupId,
                                            MessageData = MessageData,
                                            ProcessedDate = obj2.ProcessedDate,
                                            ExternalId = obj2.ExternalId,
                                            PatientId = obj2.PatientId,
                                            IsActive = 1
                                        };

                                        try
                                        {
                                            MedFlowService.Instance.GetMyWebApi(Parameters, true, "", _connectionString);

                                            cmd.Parameters.Clear();
                                            cmd.CommandText = " SELECT TOP 1 * from Medflow.PortalQueueXMLResponse where PatientId= '" + obj2.PatientId + "' AND ActionTypeLookupId = '" + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                            SqlDataAdapter dapatient = new SqlDataAdapter(cmd);
                                            DataTable dtPatient = new DataTable();
                                            dapatient.Fill(dtPatient);
                                            if (dtPatient.Rows.Count > 0)
                                            {
                                                PatientTransfered = PatientTransfered + 1;
                                                EventLogger.Instance.WriteLog("PatientAppointment -> Patient Appointment Insertion is Successful.", EventLogEntryType.Information);
                                                isSuccess = true;
                                            }
                                            else
                                            {
                                                cmd.Parameters.Clear();
                                                cmd.CommandText = "";
                                                cmd.CommandText = "DELETE FROM Medflow.PortalQueueXML WHERE Patientid = '" + obj2.PatientId + "' and ActionTypeLookupId = " + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                                cmd.CommandType = CommandType.Text;
                                                cmd.ExecuteNonQuery();
                                                EventLogger.Instance.WriteLog("PatientAppointment -> Patient Appointment Insertion Failed for PatientId" + obj2.PatientId, EventLogEntryType.Error);
                                            }
                                        }
                                        catch
                                        {
                                            cmd.Parameters.Clear();
                                            cmd.CommandText = "";
                                            cmd.CommandText = "DELETE FROM Medflow.PortalQueueXML WHERE Patientid = '" + obj2.PatientId + "' and ActionTypeLookupId = " + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                            cmd.CommandType = CommandType.Text;
                                            cmd.ExecuteNonQuery();
                                            EventLogger.Instance.WriteLog("PatientAppointment -> Patient Appointment Failed for a Patient During Accessing MedFlow API '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                        }
                                    }
                                    else
                                    {
                                        isSuccess = false;
                                    }
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogger.Instance.WriteLog(" PatientAppointment -> Patient Appointment Details Insertion Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                    }
                }
            }
            EventLogger.Instance.WriteLog("Patient Existing Appointment Update Information Event Completed", EventLogEntryType.Information);
            return isSuccess;
        }


        public string GenerateXmlForPatientAppointment(List<PatientActivationMessageData> appointmentList)
        {
            string xml = string.Empty;
            try
            {
                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument xmlDoc = new XmlDocument();
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

                XmlElement documentElement = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);

                XmlSchema schema = new XmlSchema();
                schema.Namespaces.Add("xsi", xsi);
                schema.Namespaces.Add("xsd", xsd);
                xmlDoc.Schemas.Add(schema);


                XmlElement schemaNode = xmlDoc.CreateElement("Xml");
                schemaNode.SetAttribute("xmlns:xsi", xsi);
                schemaNode.SetAttribute("xmlns:xsd", xsd);
                xmlDoc.AppendChild(schemaNode);

                XmlNode rootNode = xmlDoc.CreateElement("Type");
                rootNode.InnerText = "Patient Appointments";
                schemaNode.AppendChild(rootNode);

                //// Patient Part Starts here
                XmlNode patient = xmlDoc.CreateElement("Patient");
                schemaNode.AppendChild(patient);

                XmlNode firstName = xmlDoc.CreateElement("FirstName");
                firstName.InnerText = Convert.ToString(appointmentList[0].FirstName);
                patient.AppendChild(firstName);

                XmlNode lastName = xmlDoc.CreateElement("LastName");
                lastName.InnerText = Convert.ToString(appointmentList[0].LastName);
                patient.AppendChild(lastName);


                XmlNode middleName = xmlDoc.CreateElement("MiddleName");
                middleName.InnerText = Convert.ToString(appointmentList[0].MiddleName);
                patient.AppendChild(middleName);


                XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
                patientAccount.InnerText = Convert.ToString(appointmentList[0].PatientAccount);
                patient.AppendChild(patientAccount);


                XmlNode dob = xmlDoc.CreateElement("DOB");
                dob.InnerText = Convert.ToString(appointmentList[0].Dob.ToString("MM/dd/yyyy"));
                patient.AppendChild(dob);

                XmlNode gender = xmlDoc.CreateElement("Gender");
                gender.InnerText = Convert.ToString(appointmentList[0].Gender);
                patient.AppendChild(gender);

                ////Patient part ends here 

                //// for Appointments

                //Parent Node - Appointments
                XmlNode appointments = xmlDoc.CreateElement("Appointments");

                schemaNode.AppendChild(appointments);

                for (var i = 0; i < appointmentList.Count; i++)
                {
                    //For Child node - Appointment

                    XmlNode appointment = xmlDoc.CreateElement("Appointment");
                    schemaNode.AppendChild(appointment);

                    // Binding Elements to  Child nodes
                    XmlNode date = xmlDoc.CreateElement("Date");
                    date.InnerText = Convert.ToString(appointmentList[i].AppointmentDate, CultureInfo.CurrentCulture);
                    appointment.AppendChild(date);

                    XmlNode providerId = xmlDoc.CreateElement("ProviderId");
                    providerId.InnerText = Convert.ToString(appointmentList[i].ProviderId);
                    appointment.AppendChild(providerId);

                    XmlNode externalId = xmlDoc.CreateElement("ExternalId");
                    externalId.InnerText = Convert.ToString(appointmentList[i].Appointmentid);
                    appointment.AppendChild(externalId);

                    XmlNode description = xmlDoc.CreateElement("Description");
                    description.InnerText = Convert.ToString(appointmentList[i].Description);
                    appointment.AppendChild(description);

                    XmlNode facilityName = xmlDoc.CreateElement("FacilityName");
                    facilityName.InnerText = Convert.ToString(appointmentList[i].FacilityName);
                    appointment.AppendChild(facilityName);

                    XmlNode status = xmlDoc.CreateElement("Status");
                    status.InnerText = Convert.ToString(appointmentList[i].Status);
                    appointment.AppendChild(status);

                    appointments.AppendChild(appointment);
                }
                xml = xmlDoc.InnerXml;
                EventLogger.Instance.WriteLog("PatientAppointment ->Created xml for PatientAppointments for patient '" + appointmentList[0].PatientAccount + "'", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("PatientAppointment -> Appointment Xml generation failed for patient '" + appointmentList[0].PatientAccount + "'  \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            return xml;
        }


        /************************PatientRegistration Functions (ActionType Look Up id - 16 ) Ends here****************************/


        /************************PatientHealthInformation Functions (ActionType Look Up id - 8 ) Starts here************************/

        public bool PatientHealthInformation()
        {
            bool isSuccess = true;
            EventLogger.Instance.WriteLog("Patient Health Information Event Started.", EventLogEntryType.Information);
            for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
            {
                string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                string _ClientKey = ConfigurationManager.ConnectionStrings[temp].Name.ToString();
                var MessageId = string.Empty;
                int phInfoId = 0;
                bool isloopOn = true;
                int PatientHealthRecordTransfered = 0;
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    try
                    {
                        var externalSystemName = ConfigurationManager.AppSettings["ExternalSystem"] != null ? ConfigurationManager.AppSettings["ExternalSystem"].ToString() : "OMEDIX";
                        string MessageData = string.Empty;
                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            string strSql = " IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_NAME = N'ProcessedPatientHealthInformation' AND Table_Schema= N'MedFlow') BEGIN CREATE TABLE [MedFlow].[ProcessedPatientHealthInformation] (ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),ExternalSystemMessageId UNIQUEIDENTIFIER NOT NULL,PatientId BIGINT NOT NULL) END ";
                            cmd.CommandText = strSql;
                            cmd.Parameters.Clear();
                            cmd.ExecuteNonQuery();
                        }
                        while (isloopOn)
                        {
                            PatientHealthRecordTransfered = 0;
                            using (SqlCommand cmd = conn.CreateCommand())
                            {
                                string sqlStr = "Select top(1000) p.Id,FirstName,LastName,MiddleName,GenderId,DateOfBirth,PatientStatusId,esm.Id as MessageId, esm.Value as CCD,esm.Description,esm.CreatedDateTime as Date from model.ExternalSystemMessages esm " +
                                " INNER JOIN model.ExternalSystemMessagePracticeRepositoryEntities esmpe on esmpe.ExternalSystemMessageId = esm.Id " +
                                " INNER JOIN model.ExternalSystemExternalSystemMessageTypes esesm on esesm.Id = esm.ExternalSystemExternalSystemMessageTypeId " +
                                " INNER JOIN model.ExternalSystemMessageTypes esmt on esmt.Id = esesm.ExternalSystemMessageTypeId " +
                                " INNER JOIN model.PracticeRepositoryEntities pe on pe.Id = esmpe.PracticeRepositoryEntityId " +
                                " INNER JOIN model.Patients p on p.Id = esmpe.PracticeRepositoryEntityKey " +
                                " INNER JOIN Medflow.PortalQueueXML pqx on  pqx.PatientId = p.Id " +
                                " WHERE esm.ExternalSystemMessageProcessingStateId IN(1,3) AND esmt.Id IN(28,29) AND pe.Id IN(3) " +
                                " AND ((esm.Value IS NOT NULL OR RTRIM(LTRIM(esm.Value)) <> '') AND (RTRIM((LTRIM(esm.Error))) IS NULL OR RTRIM((LTRIM(esm.Error))) = '' OR RTRIM((LTRIM(esm.Error))) LIKE 'NO HANDLER%')) " +
                                " AND esm.ProcessingAttemptCount <= 10 AND pqx.ActionTypeLookupId = 14  AND esesm.ExternalSystemId IN(SELECT ID FROM model.ExternalSystems WHERE Name ='" + externalSystemName + "') " +
                                " AND NOT EXISTS (SELECT ExternalSystemMessageId FROM [MedFlow].[ProcessedPatientHealthInformation] WHERE PatientId = p.Id AND ExternalSystemMessageId = esm.Id )  " +
                                " ORDER BY esm.CREATEDDATETIME ASC ";

                                cmd.CommandText = sqlStr;
                                cmd.CommandTimeout = CommandTimeout;
                                SqlDataAdapter da = new SqlDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                da.Fill(dt);

                                EventLogger.Instance.WriteLog("Total Number of Patient Health Information to be Migrated are " + dt.Rows.Count, EventLogEntryType.Information);
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        DataRow patientHealthinformationDataRow = dr;
                                        var xmlString = patientHealthinformationDataRow["CCD"] != null ? patientHealthinformationDataRow["CCD"].ToString() : string.Empty;
                                        if (!string.IsNullOrEmpty(xmlString))
                                        {
                                            var patientId = patientHealthinformationDataRow["Id"].ToString();
                                            PatientActivationMessageData obj = new PatientActivationMessageData
                                            {
                                                FirstName = patientHealthinformationDataRow["FirstName"].ToString(),
                                                LastName = patientHealthinformationDataRow["LastName"].ToString(),
                                                MiddleName = patientHealthinformationDataRow["MiddleName"].ToString(),
                                                PatientAccount = patientId,
                                                Dob = Convert.ToDateTime(patientHealthinformationDataRow["DateOfBirth"].ToString()),
                                                Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(patientHealthinformationDataRow["GenderId"].ToString()))
                                            };

                                            cmd.CommandText = " Select TOP 1 * from Model.Appointments ap INNER JOIN model.Encounters e on e.Id= ap.EncounterId WHERE e.PatientId = " + patientId + " AND e.EncounterStatusId IN(5,6,7) ORDER BY DATETIME DESC ";
                                            SqlDataAdapter daEncounterDet = new SqlDataAdapter(cmd);
                                            DataTable dtEncounterDet = new DataTable();
                                            daEncounterDet.Fill(dtEncounterDet);

                                            string providerId = "0";
                                            string maxAppointmentDate = string.Empty;
                                            if (dtEncounterDet != null && dtEncounterDet.Rows.Count > 0)
                                            {
                                                DataRow drProvider = dtEncounterDet.Rows[0];
                                                providerId = drProvider["UserId"] != null ? drProvider["UserId"].ToString() : "0";
                                                maxAppointmentDate = drProvider["DateTime"] != null ? drProvider["DateTime"].ToString() : string.Empty;
                                            }

                                            MessageId = patientHealthinformationDataRow["MessageId"].ToString();

                                            // Extracting Data after this node

                                            var finalXmlString = xmlString.Substring(xmlString.IndexOf(@"<ClinicalDocument"));

                                            // replacing '&' to 'and' in Clinical Document XML
                                            string finalClincalText = Regex.Replace(finalXmlString, "&amp;", "and");

                                            //ConvertintoBase64String(PatientHealthinformationDataRow["CCD"].ToString()); //<?xml-stylesheet type="text/xsl" href="CDA.xsl"?>                                     
                                            obj.Xmldata = Base64Encode(finalClincalText);

                                            obj.Date = Convert.ToDateTime(maxAppointmentDate == string.Empty ? patientHealthinformationDataRow["Date"].ToString() : maxAppointmentDate);
                                            obj.ProviderId = providerId;

                                            PortalQueueXml obj2 = new PortalQueueXml
                                            {
                                                PatientId = Convert.ToInt32(patientId),
                                                ActionTypeLookupId =
                                                    (int)
                                                        ((ActionTypeLookUps)
                                                            Enum.Parse(typeof(ActionTypeLookUps),
                                                                ActionTypeLookUps.ProtectedHealthInformation.ToString())),
                                                ProcessedDate = DateTime.Now,
                                                IsActive =
                                                    Convert.ToBoolean(Convert.ToInt32(patientHealthinformationDataRow["PatientStatusId"].ToString())),
                                                ExternalId = 1,
                                                MessageData = obj
                                            };
                                            // Sending as 1

                                            MessageData = GenerateXmlForPatientHealthInformation(obj); // GenerateXMLForSecureMessages
                                            //String Encrypted_MessageData = EncryptDecrypt.Encrypt(MessageData);

                                            try
                                            {
                                                cmd.CommandText = "";
                                                cmd.CommandText = "INSERT INTO  Medflow.PortalQueueXML(MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive)";
                                                cmd.CommandText += "VALUES('" + MessageData.Replace("'", "") + "','" + obj2.ActionTypeLookupId + "','" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + obj2.PatientId + "','" + obj2.IsActive + "')";
                                                cmd.CommandText += " SELECT SCOPE_IDENTITY()";
                                                cmd.CommandType = CommandType.Text;
                                                obj2.ExternalId = Convert.ToInt32(cmd.ExecuteScalar());

                                                EventLogger.Instance.WriteLog("Patient Health Information Created for PatientId '" + patientId + "'", EventLogEntryType.Information);
                                            }
                                            catch
                                            {
                                                EventLogger.Instance.WriteLog("Patient Health Information Creation Failed for PatientId  While Inserting Row in PortalQueueXml'" + patientId + "'", EventLogEntryType.Error);
                                            }

                                            var parameters = new ParameterList { ClientKey = _ClientKey, ActionTypeLookupTypeId = obj2.ActionTypeLookupId, MessageData = MessageData, ProcessedDate = obj2.ProcessedDate, ExternalId = obj2.ExternalId, PatientId = obj2.PatientId, IsActive = 1 };
                                            try
                                            {
                                                MedFlowService.Instance.GetMyWebApi(parameters, true, MessageId, _connectionString);

                                                using (SqlCommand cmd1 = conn.CreateCommand()) //For Successful Insertion
                                                {
                                                    cmd1.CommandText = " IF EXISTS( SELECT TOP 1 * FROM Medflow.PortalQueueXMLResponse WHERE ActionTypeLookupId = '" + parameters.ActionTypeLookupTypeId + "' AND ProcessedDate= '" + parameters.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "' AND PatientId = '" + parameters.PatientId + "') " +
                                                                        " BEGIN INSERT INTO [MedFlow].[ProcessedPatientHealthInformation](PatientId,ExternalSystemMessageId) VALUES('" + parameters.PatientId + "','" + MessageId + "'); SELECT SCOPE_IDENTITY(); END ELSE BEGIN  SELECT 0; END ";
                                                    cmd1.CommandType = CommandType.Text;
                                                    phInfoId = Convert.ToInt32(cmd1.ExecuteScalar());
                                                    if (phInfoId > 0)
                                                    {
                                                        PatientHealthRecordTransfered = PatientHealthRecordTransfered + 1;
                                                        EventLogger.Instance.WriteLog("Patient Health Information Generated Successfully.", EventLogEntryType.Information);
                                                    }
                                                    else
                                                    {
                                                        EventLogger.Instance.WriteLog("Patient Health Information Generated Failed for Patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                                    }
                                                }
                                            }
                                            catch
                                            {
                                                EventLogger.Instance.WriteLog("Patient Health Information Failed for Patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    isloopOn = false;
                                }
                            }
                            if (PatientHealthRecordTransfered == 0)
                            {
                                isloopOn = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogger.Instance.WriteLog("Patient Health Information Generation Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                    }
                }
            }
            EventLogger.Instance.WriteLog("End of Patient Health Information Event.", EventLogEntryType.Information);
            return isSuccess;
        }

        public string GenerateXmlForPatientHealthInformation(PatientActivationMessageData obj)
        {
            string xml = string.Empty;
            try
            {
                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument xmlDoc = new XmlDocument();
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

                XmlElement documentElement = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);

                XmlSchema schema = new XmlSchema();
                schema.Namespaces.Add("xsi", xsi);
                schema.Namespaces.Add("xsd", xsd);
                xmlDoc.Schemas.Add(schema);

                XmlElement schemaNode = xmlDoc.CreateElement("Xml");
                schemaNode.SetAttribute("xmlns:xsi", xsi);
                schemaNode.SetAttribute("xmlns:xsd", xsd);
                xmlDoc.AppendChild(schemaNode);

                XmlNode rootNode = xmlDoc.CreateElement("Type");
                rootNode.InnerText = "Patient Health Information";
                schemaNode.AppendChild(rootNode);

                XmlNode patient = xmlDoc.CreateElement("Patient");
                schemaNode.AppendChild(patient);

                XmlNode firstName = xmlDoc.CreateElement("FirstName");
                firstName.InnerText = Convert.ToString(obj.FirstName);
                patient.AppendChild(firstName);
                XmlNode lastName = xmlDoc.CreateElement("LastName");
                lastName.InnerText = Convert.ToString(obj.LastName);
                patient.AppendChild(lastName);
                XmlNode middleName = xmlDoc.CreateElement("MiddleName");
                middleName.InnerText = Convert.ToString(obj.MiddleName);
                patient.AppendChild(middleName);
                XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
                patientAccount.InnerText = Convert.ToString(obj.PatientAccount);
                patient.AppendChild(patientAccount);
                XmlNode dob = xmlDoc.CreateElement("DOB");
                dob.InnerText = Convert.ToString(obj.Dob.ToString("MM/dd/yyyy"));
                patient.AppendChild(dob);
                XmlNode gender = xmlDoc.CreateElement("Gender");
                gender.InnerText = Convert.ToString(obj.Gender);
                patient.AppendChild(gender);

                // Patient Health Information Node Starts here -->

                XmlNode patientHealthInformation = xmlDoc.CreateElement("PatientHealthInformation");
                schemaNode.AppendChild(patientHealthInformation);

                XmlNode date = xmlDoc.CreateElement("Date");
                date.InnerText = obj.Date.ToString("yyyy-MM-dd");
                patientHealthInformation.AppendChild(date);

                XmlNode xmldata = xmlDoc.CreateElement("xmldata");
                xmldata.InnerText = Convert.ToString(obj.Xmldata);
                patientHealthInformation.AppendChild(xmldata);

                XmlNode providerId = xmlDoc.CreateElement("ProviderId");
                providerId.InnerText = Convert.ToString(obj.ProviderId);
                patientHealthInformation.AppendChild(providerId);

                xml = xmlDoc.InnerXml;
                EventLogger.Instance.WriteLog("PatientHealthInformation -> Xml generation successfully for patient '" + obj.PatientAccount + "'", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("Xml generation failed for patient '" + obj.PatientAccount + "'  \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }
            return xml;
        }

        /************************PatientHealthInformation Functions (ActionType Look Up id - 8 ) Ends here**************************/


        /************************PatientOcularmedication Functions (ActionType Look Up id - 20 ) Starts here************************/

        public bool PatientOcularmedication()
        {
            string _insertQry = "";
            string _deleteQry = "";
            bool isSuccess = false;
            for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
            {
                string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                string _ClientKey = ConfigurationManager.ConnectionStrings[temp].Name.ToString();
                _insertQry = "";
                _deleteQry = "";
                EventLogger.Instance.WriteLog(" Patient Ocular Medication Information Event Started.", EventLogEntryType.Information);
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    try
                    {
                        string MessageData = string.Empty;
                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {

                            cmd.CommandText = " Select Distinct p.Id, e.Id As EncounterId FROM model.Patients p ";
                            cmd.CommandText += " INNER join model.Encounters e on e.PatientId = p.Id ";
                            cmd.CommandText += " INNER join dbo.PatientClinical pc on PC.appointmentId = e.Id";
                            cmd.CommandText += " INNER join Medflow.PortalQueueXML pe on  pe.EncounterId = e.Id AND pe.ActionTypeLookupId = 16";
                            cmd.CommandText += " where	p.LastName not like '%Test%' AND PC.ClinicalType = 'A' AND PC.FindingDetail LIKE '%RX-8%' and PC.DrawFileName != '' and PC.FindingDetail is not null ";
                            cmd.CommandText += " AND NOT EXISTS  (select 1 from medflow.PortalQueueXML where  ActionTypeLookupId= 20 AND pe.EncounterId = EncounterId) ";
                            cmd.CommandTimeout = CommandTimeout;

                            SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                            DataTable dtPatientInformation = new DataTable();
                            da1.Fill(dtPatientInformation);

                            if (dtPatientInformation.Rows.Count > 0)
                            {

                                foreach (DataRow dr in dtPatientInformation.Rows)
                                {
                                    _insertQry = "";
                                    _deleteQry = "";

                                    var encounterId = dr["EncounterId"].ToString();

                                    // SP replaced with Inline Sql
                                    cmd.CommandText = "";
                                    cmd.CommandText = "Select PC.AppointmentId AS EncounterId,pc.ClinicalId As ExternalId,";  // ClinicalId - Ocular Medication code in EMR
                                    cmd.CommandText += " CASE WHEN FindingDetail like 'RX-8/%-2/%' THEN Replace (Replace(SUBSTRING(findingdetail, 6, charindex('-2/', findingdetail) - 6),'&','and'), '+','Plus') ELSE '' END AS DrugName,";
                                    cmd.CommandText += " CASE WHEN FindingDetail like 'RX-8/%-2/%' THEN dbo.ExtractTextValue('(',SUBSTRING(FindingDetail,(CHARINDEX('-2/', FindingDetail) + 3),LEN(FindingDetail)),')') END AS SIG,";
                                    cmd.CommandText += " CASE WHEN FindingDetail like 'RX-8/%-3/%' THEN dbo.ExtractTextValue('(',SUBSTRING(FindingDetail,(CHARINDEX('-3/', FindingDetail) + 3),LEN(FindingDetail)),')') END AS Strength,";
                                    cmd.CommandText += " CASE Status  when 'A' then 'Active' when 'D' then 'Discontinue' end as Status,";
                                    cmd.CommandText += " P.FirstName,P.lastName,P.MiddleName,P.ID as 'PatientAccount',DateofBirth,GenderId,ap.ResourceId1 as 'ProviderId',p.PatientStatusId,";
                                    cmd.CommandText += " e.StartDateTime As 'WrittenDate',e.EndDateTime As 'LastFillDate'";
                                    cmd.CommandText += " from dbo.PatientClinical PC (nolock) join model.Patients P (nolock)";
                                    cmd.CommandText += " on PC.Patientid = P.Id join model.Encounters E (nolock) on E.Id = PC.AppointmentId";
                                    cmd.CommandText += " and E.PatientId = PC.PatientId";
                                    cmd.CommandText += " join dbo.Appointments ap (nolock) on ap.AppointmentId = e.Id";
                                    cmd.CommandText += " where	PC.ClinicalType = 'A' AND PC.FindingDetail LIKE '%RX-8%' and PC.AppointmentId = @EncounterId";
                                    cmd.CommandText += " and PC.DrawFileName != '' and PC.FindingDetail is not null order by ExternalId";

                                    cmd.Parameters.Add("@EncounterId", SqlDbType.NVarChar).Value = encounterId;

                                    SqlDataAdapter daOcularMedications = new SqlDataAdapter(cmd);
                                    DataTable dtOcularMedications = new DataTable();
                                    daOcularMedications.Fill(dtOcularMedications);
                                    cmd.Parameters.Clear();

                                    if (dtOcularMedications.Rows.Count > 0)
                                    {
                                        PatientActivationCode act = new PatientActivationCode();
                                        PatientActivationMessageData obj = new PatientActivationMessageData();
                                        obj.FirstName = dtOcularMedications.Rows[0]["FirstName"].ToString();
                                        obj.LastName = dtOcularMedications.Rows[0]["LastName"].ToString();
                                        obj.MiddleName = dtOcularMedications.Rows[0]["MiddleName"].ToString();
                                        obj.PatientAccount = dtOcularMedications.Rows[0]["PatientAccount"].ToString();
                                        obj.Dob = Convert.ToDateTime(dtOcularMedications.Rows[0]["DateofBirth"].ToString());
                                        obj.Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dtOcularMedications.Rows[0]["GenderId"].ToString()));

                                        // For Converting a Datatable to List

                                        List<PatientActivationMessageData> ocularMedicationList = new List<PatientActivationMessageData>();

                                        for (int i = 0; i < dtOcularMedications.Rows.Count; i++)
                                        {

                                            PatientActivationMessageData App = new PatientActivationMessageData();
                                            App.FirstName = dtOcularMedications.Rows[i]["FirstName"].ToString();
                                            App.LastName = dtOcularMedications.Rows[i]["LastName"].ToString();
                                            App.MiddleName = dtOcularMedications.Rows[i]["MiddleName"].ToString();
                                            App.PatientAccount = dtOcularMedications.Rows[i]["PatientAccount"].ToString();
                                            App.Dob = Convert.ToDateTime(dtOcularMedications.Rows[i]["DateofBirth"].ToString());
                                            App.Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dtOcularMedications.Rows[i]["GenderId"].ToString()));
                                            App.ExternalId = dtOcularMedications.Rows[i]["ExternalId"].ToString();
                                            App.DrugName = dtOcularMedications.Rows[i]["DrugName"].ToString();
                                            App.Strength = dtOcularMedications.Rows[i]["Strength"].ToString();
                                            App.Sig = dtOcularMedications.Rows[i]["SIG"].ToString();
                                            App.Refill = 8;
                                            App.AdditionalInstruction = "";
                                            App.WrittenDate = Convert.ToDateTime(dtOcularMedications.Rows[i]["WrittenDate"].ToString()); ;
                                            App.LastFillDate = Convert.ToDateTime(dtOcularMedications.Rows[i]["LastFillDate"].ToString()); ;
                                            App.ProviderId = dtOcularMedications.Rows[i]["ProviderId"].ToString();
                                            App.Status = dtOcularMedications.Rows[i]["Status"].ToString();

                                            ocularMedicationList.Add(App);
                                        }

                                        PortalQueueXml obj2 = new PortalQueueXml
                                        {
                                            PatientId = Convert.ToInt32(dr["Id"].ToString()),
                                            ActionTypeLookupId =
                                                (int)
                                                    ((ActionTypeLookUps)
                                                        Enum.Parse(typeof(ActionTypeLookUps),
                                                            ActionTypeLookUps.OcularMedications.ToString())),
                                            ProcessedDate = DateTime.Now,
                                            IsActive =
                                                Convert.ToBoolean(
                                                    Convert.ToInt32(
                                                        dtOcularMedications.Rows[0]["PatientStatusId"].ToString())),
                                            MessageData = obj
                                        };

                                        MessageData = GenerateXmlForPatientOcularMedication(ocularMedicationList);
                                        //string Encrypted_MessageData = EncryptDecrypt.Encrypt(MessageData);

                                        try
                                        {
                                            _insertQry = "";
                                            _insertQry = "INSERT INTO  Medflow.PortalQueueXML(MessageData, ActionTypeLookupId, ProcessedDate, PatientId, IsActive, EncounterId) Values ('" + MessageData.Replace("'", "") + "','" + obj2.ActionTypeLookupId + "','" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + obj2.PatientId + "','" + obj2.IsActive + "','" + encounterId + "');";
                                            cmd.CommandText = _insertQry + " SELECT SCOPE_IDENTITY()";
                                            cmd.CommandType = CommandType.Text;
                                            obj2.ExternalId = Convert.ToInt32(cmd.ExecuteScalar());
                                        }
                                        catch
                                        {
                                            EventLogger.Instance.WriteLog("Patient Ocular Medication Failed for Encounter while Inserting '" + encounterId + "'", EventLogEntryType.Error);
                                        }

                                        var parameters = new ParameterList
                                        {
                                            ClientKey = _ClientKey,
                                            ActionTypeLookupTypeId = obj2.ActionTypeLookupId,
                                            MessageData = MessageData,
                                            ProcessedDate = obj2.ProcessedDate,
                                            ExternalId = obj2.ExternalId,
                                            PatientId = obj2.PatientId,
                                            IsActive = 1
                                        };

                                        try
                                        {
                                            MedFlowService.Instance.GetMyWebApi(parameters, true, "", _connectionString);

                                            cmd.Parameters.Clear();
                                            cmd.CommandText = " SELECT TOP 1 * from Medflow.PortalQueueXMLResponse where PatientId= '" + obj2.PatientId + "' AND ActionTypeLookupId = '" + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                            SqlDataAdapter dapatient = new SqlDataAdapter(cmd);
                                            DataTable dtPatient = new DataTable();
                                            dapatient.Fill(dtPatient);
                                            if (dtPatient.Rows.Count > 0)
                                            {
                                                EventLogger.Instance.WriteLog("Patient Ocular Medication is Successful.", EventLogEntryType.Information);
                                                isSuccess = true;
                                            }
                                            else
                                            {
                                                cmd.Parameters.Clear();
                                                _deleteQry = "";
                                                _deleteQry = "DELETE FROM Medflow.PortalQueueXML WHERE Patientid = '" + obj2.PatientId + "' and ActionTypeLookupId = " + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                                cmd.CommandText = _deleteQry;
                                                cmd.CommandType = CommandType.Text;
                                                cmd.ExecuteNonQuery();
                                                EventLogger.Instance.WriteLog("Patient Ocular Medication Failed for Encounter " + encounterId, EventLogEntryType.Error);
                                            }
                                        }
                                        catch
                                        {
                                            cmd.Parameters.Clear();
                                            cmd.CommandText = "";
                                            _deleteQry = "";
                                            _deleteQry = "DELETE FROM Medflow.PortalQueueXML WHERE Patientid = '" + obj2.PatientId + "' and ActionTypeLookupId = " + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                            cmd.CommandText = _deleteQry;
                                            cmd.CommandType = CommandType.Text;
                                            cmd.ExecuteNonQuery();
                                            EventLogger.Instance.WriteLog("Patient Ocular Medication Failed for patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                        }
                                    }
                                }
                            }
                        }
                        return isSuccess;
                    }
                    catch (Exception ex)
                    {
                        EventLogger.Instance.WriteLog("Patient Ocular Medication Insertion Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                    }
                }
            }
            EventLogger.Instance.WriteLog("End of Patient Ocular Medication Information Event.", EventLogEntryType.Information);
            return isSuccess;
        }

        public string GenerateXmlForPatientOcularMedication(List<PatientActivationMessageData> appointmentList)
        {
            string xml = string.Empty;
            try
            {
                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument xmlDoc = new XmlDocument();
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

                XmlElement documentElement = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);

                XmlSchema schema = new XmlSchema();
                schema.Namespaces.Add("xsi", xsi);
                schema.Namespaces.Add("xsd", xsd);
                xmlDoc.Schemas.Add(schema);


                XmlElement schemaNode = xmlDoc.CreateElement("Xml");
                schemaNode.SetAttribute("xmlns:xsi", xsi);
                schemaNode.SetAttribute("xmlns:xsd", xsd);
                xmlDoc.AppendChild(schemaNode);

                XmlNode rootNode = xmlDoc.CreateElement("Type");
                rootNode.InnerText = "Ocular Medications";
                schemaNode.AppendChild(rootNode);

                //// Patient Part Starts here
                XmlNode patient = xmlDoc.CreateElement("Patient");
                schemaNode.AppendChild(patient);

                XmlNode firstName = xmlDoc.CreateElement("FirstName");
                firstName.InnerText = Convert.ToString(appointmentList[0].FirstName);
                patient.AppendChild(firstName);

                XmlNode lastName = xmlDoc.CreateElement("LastName");
                lastName.InnerText = Convert.ToString(appointmentList[0].LastName);
                patient.AppendChild(lastName);


                XmlNode middleName = xmlDoc.CreateElement("MiddleName");
                middleName.InnerText = Convert.ToString(appointmentList[0].MiddleName);
                patient.AppendChild(middleName);


                XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
                patientAccount.InnerText = Convert.ToString(appointmentList[0].PatientAccount);
                patient.AppendChild(patientAccount);


                XmlNode dob = xmlDoc.CreateElement("DOB");
                dob.InnerText = Convert.ToString(appointmentList[0].Dob.ToString("MM/dd/yyyy"));
                patient.AppendChild(dob);

                XmlNode gender = xmlDoc.CreateElement("Gender");
                gender.InnerText = Convert.ToString(appointmentList[0].Gender);
                patient.AppendChild(gender);


                ////Patient part ends here 

                //// for Ocularmedications

                //Parent Node - Appointments
                XmlNode ocularMedications = xmlDoc.CreateElement("OcularMedications");

                schemaNode.AppendChild(ocularMedications);

                foreach (PatientActivationMessageData t in appointmentList)
                {
                    //For Child node - Appointment

                    XmlNode ocularMedication = xmlDoc.CreateElement("OcularMedication");
                    schemaNode.AppendChild(ocularMedication);

                    // Binding Elements to  Child nodes
                    XmlNode externalId = xmlDoc.CreateElement("ExternalId");
                    externalId.InnerText = Convert.ToString(t.ExternalId);
                    ocularMedication.AppendChild(externalId);

                    XmlNode drugName = xmlDoc.CreateElement("DrugName");
                    drugName.InnerText = Convert.ToString(t.DrugName);
                    ocularMedication.AppendChild(drugName);

                    XmlNode strength = xmlDoc.CreateElement("Strength");
                    strength.InnerText = Convert.ToString(t.Strength);
                    ocularMedication.AppendChild(strength);

                    XmlNode sig = xmlDoc.CreateElement("SIG");
                    sig.InnerText = Convert.ToString(t.Sig);
                    ocularMedication.AppendChild(sig);

                    XmlNode refill = xmlDoc.CreateElement("Refill");
                    refill.InnerText = Convert.ToString(t.Refill);
                    ocularMedication.AppendChild(refill);

                    XmlNode additionalInstruction = xmlDoc.CreateElement("AdditionalInstruction");
                    additionalInstruction.InnerText = Convert.ToString(t.AdditionalInstruction);
                    ocularMedication.AppendChild(additionalInstruction);

                    XmlNode writtenDate = xmlDoc.CreateElement("WrittenDate");
                    writtenDate.InnerText = Convert.ToString(t.WrittenDate, CultureInfo.CurrentCulture);
                    ocularMedication.AppendChild(writtenDate);

                    XmlNode lastFillDate = xmlDoc.CreateElement("LastFillDate");
                    lastFillDate.InnerText = Convert.ToString(t.LastFillDate, CultureInfo.CurrentCulture);
                    ocularMedication.AppendChild(lastFillDate);

                    XmlNode providerId = xmlDoc.CreateElement("ProviderId");
                    providerId.InnerText = Convert.ToString(t.ProviderId);
                    ocularMedication.AppendChild(providerId);

                    XmlNode status = xmlDoc.CreateElement("Status");
                    status.InnerText = Convert.ToString(t.Status);
                    ocularMedication.AppendChild(status);

                    ocularMedications.AppendChild(ocularMedication);
                }
                xml = xmlDoc.InnerXml;
                EventLogger.Instance.WriteLog("PatientOcularMedication -> Created xml for Patient Ocular Medication for patient '" + appointmentList[0].PatientAccount + "'", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("PatientOcularMedication -> Xml generation failed for patient '" + appointmentList[0].PatientAccount + "'  \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }
            return xml;
        }

        /********************************PatientOcularmedication Functions (ActionType Look Up id - 20 ) Ends here*******************/


        /************************PatientDemographicsInformation Functions (ActionType Look Up id - 23 ) Starts here************************/
        public bool PatientDemographicsInformation(int patientId, string _connectionString, string _clientKey)
        {
            bool isSuccess = false;
            EventLogger.Instance.WriteLog("PatientDemographicsInformation Started.", EventLogEntryType.Information);

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                try
                {


                    string MessageData = string.Empty;
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {

                        cmd.CommandText = "";
                        cmd.CommandText = "select p.Id FROM Model.Patients p inner join Medflow.PortalQueueXML portxml on p.Id = portxml.PatientId and portxml.ActionTypeLookupId = 14 ";
                        cmd.CommandText += " where p.Id ='" + patientId + "'";
                        cmd.CommandText += " AND NOT EXISTS (select 1 from Medflow.PortalQueueXMLResponse where PatientId = '" + patientId + "'  and ActionTypeLookupId = 23 )";
                        cmd.CommandTimeout = CommandTimeout;

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                cmd.CommandText = " ";
                                cmd.CommandText = "SELECT distinct  p.FirstName,p.LastName,MiddleName,p.Id,p.DateOfBirth,p.GenderId,pe.Value,";
                                cmd.CommandText += "r.Name AS Race,";
                                cmd.CommandText += " (Case	When r.Name = 'American Indian or Alaska Native' Then '1002-5'";
                                cmd.CommandText += " When r.Name = 'Asian' Then '2028-9' ";
                                cmd.CommandText += " When r.Name = 'Black or African American' Then '2054-5' ";
                                cmd.CommandText += " When r.Name = 'Native Hawaiian or Other Pacific Islander' Then '2076-8'";
                                cmd.CommandText += " When r.Name = 'White' Then '2106-3'";
                                cmd.CommandText += " Else ''";
                                cmd.CommandText += " END) AS RaceCode,";
                                cmd.CommandText += " (Case when cmte.Id =1 then 'Mail'";
                                cmd.CommandText += " when cmte.Id =6 Then 'SMS'";
                                cmd.CommandText += " Else 'Phone'";
                                cmd.CommandText += " End	) AS PreferredCommunication,";
                                cmd.CommandText += " l.Name AS PreferredLanguage, l.Id as PreferredLanguageCode, E.Name as Ethnicity,";
                                cmd.CommandText += " (Case	when e.Name = 'Hispanic or Latino' Then '2135-2'";
                                cmd.CommandText += " ELSE '0000'";
                                cmd.CommandText += " END";
                                cmd.CommandText += " ) as EthnicityCode";
                                cmd.CommandText += " FROM model.Patients p join dbo.PatientDemographics pd on p.Id = pd.PatientId";
                                cmd.CommandText += " LEFT join  model.PatientEmailAddresses pe on  p.id = pe.PatientId";
                                cmd.CommandText += " left join model.PatientCommunicationPreferences pcp on pcp.PatientId = p.Id";
                                cmd.CommandText += " left join model.CommunicationMethodType_Enumeration cmte on cmte.Id = pcp.CommunicationMethodTypeId and cmte.Id in(1,6,5)";
                                cmd.CommandText += " left Join model.PatientRace pr on pr.Patients_Id = p.Id";
                                cmd.CommandText += " left join model.Races r on r.Id = pr.Races_Id";

                                cmd.CommandText += " left join model.Languages l on l.id = p.LanguageId";
                                cmd.CommandText += " left join model.Ethnicities e on e.Id = p.EthnicityId";
                                cmd.CommandText += " where p.id= @PatientID";

                                cmd.CommandText += " select (case when pat.Id =1 then 'Home'";
                                cmd.CommandText += " else 'Work'";
                                cmd.CommandText += " END)  Type,pa.Line1,pa.Line2,pa.PostalCode As ZipCode,pd.City,State";
                                cmd.CommandText += " from dbo.PatientDemographics pd";
                                cmd.CommandText += " join model.PatientAddresses pa on pa.PatientId = pd.patientid";
                                cmd.CommandText += " left join model.PatientAddressTypes pat on pat.Id = pa.PatientAddressTypeId";
                                cmd.CommandText += " where pd.PatientId = @PatientID";

                                cmd.CommandText += " select (";
                                cmd.CommandText += " case when  ppn.PatientPhoneNumberTypeId = 2 THEN 'Work'";
                                cmd.CommandText += " when  ppn.PatientPhoneNumberTypeId = 3 THEN 'Cell'";
                                cmd.CommandText += " when  ppn.PatientPhoneNumberTypeId = 14 THEN 'Fax'";
                                cmd.CommandText += " when  ppn.PatientPhoneNumberTypeId = 7 THEN 'Home'";
                                cmd.CommandText += " else 'Home'";
                                cmd.CommandText += " END";
                                cmd.CommandText += " )As  Type,";
                                cmd.CommandText += " ppn.ExchangeAndSuffix as Number,ppn.AreaCode";
                                cmd.CommandText += " From dbo.PatientDemographics pd";
                                cmd.CommandText += " join model.PatientPhoneNumbers ppn on ppn.PatientId = pd.PatientId and ppn.PatientPhoneNumberTypeId in(2,3,7,14)";
                                cmd.CommandText += " where pd.PatientId = @PatientID";

                                cmd.Parameters.Add("@PatientID", SqlDbType.NVarChar).Value = dr["Id"].ToString();

                                SqlDataAdapter daAddress = new SqlDataAdapter(cmd);
                                DataSet demographicsInformation = new DataSet();
                                daAddress.Fill(demographicsInformation);
                                cmd.Parameters.Clear();
                                //Fetch basic detail
                                if (demographicsInformation != null)
                                {
                                    PatientActivationMessageData obj = new PatientActivationMessageData();
                                    obj.FirstName = demographicsInformation.Tables[0].Rows[0]["FirstName"].ToString();
                                    obj.LastName = demographicsInformation.Tables[0].Rows[0]["LastName"].ToString();
                                    obj.MiddleName = demographicsInformation.Tables[0].Rows[0]["MiddleName"].ToString();
                                    obj.PatientAccount = demographicsInformation.Tables[0].Rows[0]["Id"].ToString();
                                    obj.Dob = Convert.ToDateTime(demographicsInformation.Tables[0].Rows[0]["DateOfBirth"].ToString());
                                    obj.EmailAddress = demographicsInformation.Tables[0].Rows[0]["Value"].ToString();
                                    if (demographicsInformation.Tables[0].Rows[0]["GenderId"].ToString() == "4")
                                        obj.Gender = "Unknown";
                                    else
                                        obj.Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(demographicsInformation.Tables[0].Rows[0]["GenderId"].ToString()));
                                    obj.Race = demographicsInformation.Tables[0].Rows[0]["Race"].ToString();
                                    obj.RaceCode = demographicsInformation.Tables[0].Rows[0]["RaceCode"].ToString();
                                    obj.PreferredCommunication = demographicsInformation.Tables[0].Rows[0]["PreferredCommunication"].ToString();
                                    obj.PreferredLanguage = demographicsInformation.Tables[0].Rows[0]["PreferredLanguage"].ToString();
                                    obj.PreferredLanguageCode = demographicsInformation.Tables[0].Rows[0]["PreferredLanguageCode"].ToString();
                                    obj.Ethnicity = demographicsInformation.Tables[0].Rows[0]["Ethnicity"].ToString();
                                    obj.EthnicityCode = demographicsInformation.Tables[0].Rows[0]["EthnicityCode"].ToString();
                                    obj.Image = ""; //Need to disscuss

                                    // Creating list for address

                                    List<PatientAddress> addressList = new List<PatientAddress>();

                                    for (int i = 0; i < demographicsInformation.Tables[1].Rows.Count; i++)
                                    {

                                        PatientAddress add = new PatientAddress
                                        {
                                            Type = demographicsInformation.Tables[1].Rows[i]["Type"].ToString(),
                                            Line1 = demographicsInformation.Tables[1].Rows[i]["Line1"].ToString(),
                                            Line2 = demographicsInformation.Tables[1].Rows[i]["Line2"].ToString(),
                                            ZipCode = demographicsInformation.Tables[1].Rows[i]["ZipCode"].ToString(),
                                            City = demographicsInformation.Tables[1].Rows[i]["City"].ToString(),
                                            State = demographicsInformation.Tables[1].Rows[i]["State"].ToString()
                                        };

                                        addressList.Add(add);
                                    }


                                    // Creating list for Phone Number

                                    List<PatientPhoneNumber> phoneList = new List<PatientPhoneNumber>();

                                    for (int i = 0; i < demographicsInformation.Tables[2].Rows.Count; i++)
                                    {

                                        PatientPhoneNumber phone = new PatientPhoneNumber
                                        {
                                            Type = demographicsInformation.Tables[2].Rows[i]["Type"].ToString(),
                                            Number = demographicsInformation.Tables[2].Rows[i]["Number"].ToString()
                                        };

                                        phoneList.Add(phone);
                                    }
                                    PortalQueueXml obj2 = new PortalQueueXml
                                    {
                                        PatientId = Convert.ToInt32(dr["Id"].ToString()),
                                        ActionTypeLookupId =
                                            (int)
                                                ((ActionTypeLookUps)
                                                    Enum.Parse(typeof(ActionTypeLookUps),
                                                        ActionTypeLookUps.DemographicsDump.ToString())),
                                        ProcessedDate = DateTime.Now,
                                        MessageData = obj
                                    };

                                    MessageData = GenerateXmlForPatientDemographics(obj, addressList, phoneList);
                                    //String Encrypted_MessageData = EncryptDecrypt.Encrypt(MessageData);

                                    try
                                    {
                                        cmd.CommandText = "";
                                        cmd.CommandText = "INSERT INTO  Medflow.PortalQueueXML(MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive)";
                                        cmd.CommandText += " VALUES('" + MessageData.Replace("'", "") + "','" + obj2.ActionTypeLookupId + "','" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + obj2.PatientId + "','" + 1 + "')";
                                        cmd.CommandText += " SELECT SCOPE_IDENTITY()";
                                        cmd.CommandType = CommandType.Text;
                                        obj2.ExternalId = Convert.ToInt32(cmd.ExecuteScalar());
                                    }
                                    catch
                                    {
                                        EventLogger.Instance.WriteLog("PatientDemographicsInformation -> Patient demographics Failed for patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                    }

                                    var parameters = new ParameterList { ClientKey = _clientKey, ActionTypeLookupTypeId = obj2.ActionTypeLookupId, MessageData = MessageData, ProcessedDate = obj2.ProcessedDate, ExternalId = obj2.ExternalId, PatientId = obj2.PatientId, IsActive = 1 };
                                    try
                                    {
                                        MedFlowService.Instance.GetMyWebApi(parameters, true, "", _connectionString);
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = " SELECT TOP 1 * from Medflow.PortalQueueXMLResponse where PatientId= '" + obj2.PatientId + "' AND ActionTypeLookupId = '" + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                        SqlDataAdapter dapatient = new SqlDataAdapter(cmd);
                                        DataTable dtPatient = new DataTable();
                                        dapatient.Fill(dtPatient);
                                        if (dtPatient.Rows.Count > 0)
                                        {
                                            isSuccess = true;
                                            EventLogger.Instance.WriteLog("PatientDemographicsInformation-> PatientDemographicsInformation created for patient '" + obj2.PatientId + "'", EventLogEntryType.Information);
                                        }
                                        else
                                        {
                                            isSuccess = false;
                                            EventLogger.Instance.WriteLog("PatientDemographicsInformation-> Xml was not Processed.PatientDemographicsInformation Failed for patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                        }
                                    }
                                    catch
                                    {
                                        isSuccess = false;
                                        EventLogger.Instance.WriteLog("PatientDemographicsInformation-> PatientDemographicsInformation Failed for patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    EventLogger.Instance.WriteLog("PatientDemographicsInformation Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
            return isSuccess;
        }




        public string GenerateXmlForPatientDemographics(PatientActivationMessageData pam, List<PatientAddress> addresstList, List<PatientPhoneNumber> phoneList)
        {
            string xml = string.Empty;
            try
            {
                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument xmlDoc = new XmlDocument();
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

                XmlElement documentElement = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);

                XmlSchema schema = new XmlSchema();
                schema.Namespaces.Add("xsi", xsi);
                schema.Namespaces.Add("xsd", xsd);
                xmlDoc.Schemas.Add(schema);


                XmlElement schemaNode = xmlDoc.CreateElement("Xml");
                schemaNode.SetAttribute("xmlns:xsi", xsi);
                schemaNode.SetAttribute("xmlns:xsd", xsd);
                xmlDoc.AppendChild(schemaNode);

                XmlNode rootNode = xmlDoc.CreateElement("Type");
                rootNode.InnerText = "Patient Demographics";
                schemaNode.AppendChild(rootNode);

                //// Patient Part Starts here
                XmlNode patient = xmlDoc.CreateElement("Patient");
                schemaNode.AppendChild(patient);

                XmlNode firstName = xmlDoc.CreateElement("FirstName");
                firstName.InnerText = Convert.ToString(pam.FirstName);
                patient.AppendChild(firstName);

                XmlNode lastName = xmlDoc.CreateElement("LastName");
                lastName.InnerText = Convert.ToString(pam.LastName);
                patient.AppendChild(lastName);


                XmlNode middleName = xmlDoc.CreateElement("MiddleName");
                middleName.InnerText = Convert.ToString(pam.MiddleName);
                patient.AppendChild(middleName);


                XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
                patientAccount.InnerText = Convert.ToString(pam.PatientAccount);
                patient.AppendChild(patientAccount);


                XmlNode dob = xmlDoc.CreateElement("DOB");
                dob.InnerText = Convert.ToString(pam.Dob.ToString("MM/dd/yyyy"));
                patient.AppendChild(dob);

                XmlNode gender = xmlDoc.CreateElement("Gender");
                gender.InnerText = Convert.ToString(pam.Gender);
                patient.AppendChild(gender);

                XmlNode emailAddress = xmlDoc.CreateElement("EmailAddress");
                emailAddress.InnerText = Convert.ToString(pam.EmailAddress);
                patient.AppendChild(emailAddress);

                ////Patient part ends here 

                //// for Address


                XmlNode addresses = xmlDoc.CreateElement("Addresses");
                patient.AppendChild(addresses);


                foreach (PatientAddress t in addresstList)
                {
                    //For Parent Node - Appointment

                    XmlNode address = xmlDoc.CreateElement("Address");
                    addresses.AppendChild(address);

                    XmlNode type = xmlDoc.CreateElement("Type");
                    type.InnerText = Convert.ToString(t.Type);
                    address.AppendChild(type);

                    XmlNode line1 = xmlDoc.CreateElement("Line1");
                    line1.InnerText = Convert.ToString(t.Line1);
                    address.AppendChild(line1);

                    XmlNode line2 = xmlDoc.CreateElement("Line2");
                    line2.InnerText = Convert.ToString(t.Line2);
                    address.AppendChild(line2);

                    XmlNode zipCode = xmlDoc.CreateElement("ZipCode");
                    zipCode.InnerText = Convert.ToString(t.ZipCode);
                    address.AppendChild(zipCode);

                    XmlNode city = xmlDoc.CreateElement("City");
                    city.InnerText = Convert.ToString(t.City);
                    address.AppendChild(city);

                    XmlNode state = xmlDoc.CreateElement("State");
                    state.InnerText = Convert.ToString(t.State);
                    address.AppendChild(state);
                }

                //// for Phone Number


                XmlNode phoneNumbers = xmlDoc.CreateElement("PhoneNumbers");
                patient.AppendChild(phoneNumbers);


                foreach (PatientPhoneNumber t in phoneList)
                {
                    //For Parent Node - Appointment

                    XmlNode phone = xmlDoc.CreateElement("Phone");
                    phoneNumbers.AppendChild(phone);

                    XmlNode type = xmlDoc.CreateElement("Type");
                    type.InnerText = Convert.ToString(t.Type);
                    phone.AppendChild(type);

                    XmlNode number = xmlDoc.CreateElement("Number");
                    number.InnerText = Convert.ToString(t.Number);
                    phone.AppendChild(number);
                }

                XmlNode race = xmlDoc.CreateElement("Race");
                race.InnerText = Convert.ToString(pam.Race);
                patient.AppendChild(race);

                XmlNode raceCode = xmlDoc.CreateElement("RaceCode");
                raceCode.InnerText = Convert.ToString(pam.RaceCode);
                patient.AppendChild(raceCode);


                //XmlNode preferredCommunication = xmlDoc.CreateElement("PreferredCommunication");
                //preferredCommunication.InnerText = Convert.ToString(pam.PreferredCommunication);
                //patient.AppendChild(preferredCommunication);


                //XmlNode preferredLanguage = xmlDoc.CreateElement("PreferredLanguage");
                //preferredLanguage.InnerText = Convert.ToString(pam.PreferredLanguage);
                //patient.AppendChild(preferredLanguage);


                //XmlNode preferredLanguageCode = xmlDoc.CreateElement("PreferredLanguageCode");
                //preferredLanguageCode.InnerText = Convert.ToString(pam.PreferredLanguageCode);
                //patient.AppendChild(preferredLanguageCode);

                //XmlNode ethnicity = xmlDoc.CreateElement("Ethnicity");
                //ethnicity.InnerText = Convert.ToString(pam.Ethnicity);
                //patient.AppendChild(ethnicity);

                //XmlNode ethnicityCode = xmlDoc.CreateElement("EthnicityCode");
                //ethnicityCode.InnerText = Convert.ToString(pam.EthnicityCode);
                //patient.AppendChild(ethnicityCode);

                //XmlNode image = xmlDoc.CreateElement("Image");
                //image.InnerText = Convert.ToString(pam.Image);
                //patient.AppendChild(image);

                xml = xmlDoc.InnerXml;

                EventLogger.Instance.WriteLog("PatientDemographics -> Patient's Demographics Created XML for Patient '" + pam.PatientAccount + "'", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("PatientDemographics -> Patient's Demographics Creation failed for Patient '" + pam.PatientAccount + "'  \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            return xml;

        }

        /********************************PatientDemographicsInformation Functions (ActionType Look Up id - 23) Ends here********************/


        /*************************************** Patient Education Link Functions starts here ******************************************/


        public bool PatientEducationLinks()
        {
            EventLogger.Instance.WriteLog("Patient Education Link Event Started.", EventLogEntryType.Information);
            for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
            {
                string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                string _ClientKey = ConfigurationManager.ConnectionStrings[temp].Name.ToString();

                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    try
                    {
                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            string strSql = " IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_NAME = N'ProcessedPatientEducationLinks' AND Table_Schema='MedFlow')  " +
                                            " BEGIN CREATE TABLE [MedFlow].[ProcessedPatientEducationLinks] (ID INT NOT NULL PRIMARY KEY IDENTITY(1,1),EncounterId BIGINT NOT NULL," +
                                            " PatientId BIGINT NOT NULL,HasDiagnosisLink BIT NOT NULL DEFAULT(0),HasMedicationLink BIT NOT NULL DEFAULT(0),HasLabCodeLink BIT NOT NULL DEFAULT(0)) END ";
                            cmd.CommandText = strSql;
                            cmd.ExecuteNonQuery();

                            //Add an Extra column on the table
                            strSql = " IF NOT EXISTS (SELECT * FROM  sys.columns WHERE  object_id = OBJECT_ID(N'[MedFlow].[ProcessedPatientEducationLinks]') AND name = 'LinkedId') BEGIN ALTER TABLE MedFLow.ProcessedPatientEducationLinks ADD LinkedId BIGINT NULL END  ";
                            cmd.CommandText = strSql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogger.Instance.WriteLog("Error Occured While Creating Table for PatientEducationLink  ->   " + ex.Message, EventLogEntryType.Error);
                    }
                }

                PatientEducationLinkWithDiagnosis(_connectionString, _ClientKey);
                System.Threading.Thread.Sleep(10000); // wait for 10 secs.

                if (CheckServiceEndTime()) return true;

                PatientEducationLinkwithLabCode(_connectionString, _ClientKey);
                System.Threading.Thread.Sleep(10000);
                if (CheckServiceEndTime()) return true;

                PatientEducationLinkwithMedication(_connectionString, _ClientKey);
                System.Threading.Thread.Sleep(10000);
            }
            EventLogger.Instance.WriteLog("End of Patient Education Link Event.", EventLogEntryType.Information);
            return false;
        }

        /***** 1. Method for Patient Link Education - Diagnosis URL*******/

        public bool PatientEducationLinkWithDiagnosis(string _connectionString, string _clientKey)
        {

            bool isSuccess = false;
            EventLogger.Instance.WriteLog("Patient Education Link with Diagnosis Code Started.", EventLogEntryType.Information);
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                try
                {
                    string MessageData = string.Empty;
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {

                        cmd.CommandText = "";
                        cmd.CommandText = "Select distinct  p.Id,FirstName,LastName,MiddleName,GenderId,DateOfBirth,PatientStatusId,PC.FindingDetailIcd10,app.ResourceId1, e.Id as EncounterId, pc.ClinicalId   FROM model.Patients p";
                        cmd.CommandText += " INNER join model.Encounters e on p.id= e.PatientId";
                        cmd.CommandText += " INNER JOIN dbo.Appointments app on app.AppointmentId = e.Id";
                        cmd.CommandText += " INNER join Medflow.PortalQueueXML pe on  pe.EncounterId = e.Id AND pe.ActionTypeLookupId = 16";
                        cmd.CommandText += " INNER join dbo.PatientClinicalIcd10 PC on e.Id = PC.AppointmentId";
                        cmd.CommandText += " WHERE	p.LastName not like '%Test%' AND PC.ClinicalType = 'B'";
                        cmd.CommandText += " AND  NOT EXISTS (SELECT LinkedId from MedFlow.ProcessedPatientEducationLinks WHERE HasDiagnosisLink = 1 AND LinkedId = pc.ClinicalId AND pe.PatientId= PatientId AND pe.EncounterId = EncounterId) ";
                        cmd.CommandTimeout = CommandTimeout;


                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {

                                PatientActivationMessageData obj = new PatientActivationMessageData
                                {
                                    FirstName = dr["FirstName"].ToString(),
                                    LastName = dr["LastName"].ToString(),
                                    PatientAccount = dr["Id"].ToString(),
                                    Dob = Convert.ToDateTime(dr["DateOfBirth"].ToString()),
                                    Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dr["GenderId"].ToString())),
                                    FindingDetailIcd10 = dr["FindingDetailIcd10"].ToString(),
                                    PatientEducationDate = DateTime.Now.ToString("MM/dd/yyyy"),
                                    ProviderId = dr["ResourceId1"].ToString()
                                };

                                var encounterId = dr["EncounterId"].ToString();
                                var clinicalId = dr["ClinicalId"].ToString();

                                obj.MedlineUrl = MedFlowService.Instance.GetPatientEducationLinkForDiagnosis(obj.FindingDetailIcd10);


                                PortalQueueXml obj2 = new PortalQueueXml
                                {
                                    PatientId = Convert.ToInt32(dr["Id"].ToString()),
                                    ActionTypeLookupId =
                                        (int)
                                            ((ActionTypeLookUps)
                                                Enum.Parse(typeof(ActionTypeLookUps),
                                                    ActionTypeLookUps.PatientEducationLink.ToString())),
                                    ProcessedDate = DateTime.Now,
                                    IsActive = Convert.ToBoolean(Convert.ToInt32(dr["PatientStatusId"].ToString())),
                                    MessageData = obj
                                };

                                MessageData = GenerateXmLforPatientEducationLinkWithDiagnosis(obj);
                                //String Encrypted_MessageData = EncryptDecrypt.Encrypt(MessageData);

                                try
                                {
                                    cmd.CommandText = "";
                                    cmd.CommandText = "INSERT INTO  Medflow.PortalQueueXML(MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive, EncounterId)";
                                    cmd.CommandText += "VALUES('" + MessageData.Replace("'", "") + "','" + obj2.ActionTypeLookupId + "','" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + obj2.PatientId + "','" + obj2.IsActive + "','" + encounterId + "')";
                                    cmd.CommandText += " SELECT SCOPE_IDENTITY()";
                                    cmd.CommandType = CommandType.Text;
                                    obj2.ExternalId = Convert.ToInt32(cmd.ExecuteScalar());
                                }
                                catch
                                {
                                    EventLogger.Instance.WriteLog("Patient Education Link for Diagnosis URL failed for Patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                    isSuccess = false;
                                }

                                var parameters = new ParameterList { ClientKey = _clientKey, ActionTypeLookupTypeId = obj2.ActionTypeLookupId, MessageData = MessageData, ProcessedDate = obj2.ProcessedDate, ExternalId = obj2.ExternalId, PatientId = obj2.PatientId, IsActive = 1 };

                                try
                                {
                                    MedFlowService.Instance.GetMyWebApi(parameters, MedFlowService.Instance.FlagforReturnedUrl, "", _connectionString);

                                    isSuccess = true;
                                    cmd.Parameters.Clear();
                                    cmd.CommandText = "SELECT 1 from Medflow.PortalQueueXMLResponse WHERE PatientId= '" + obj2.PatientId + "' AND ActionTypeLookupId = '" + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                    SqlDataAdapter dapatient = new SqlDataAdapter(cmd);
                                    DataTable dtPatient = new DataTable();
                                    dapatient.Fill(dtPatient);
                                    if (dtPatient.Rows.Count > 0)
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "  IF NOT EXISTS(SELECT TOP 1 * FROM [MedFlow].[ProcessedPatientEducationLinks] WHERE PatientID = " + obj2.PatientId + " AND EncounterId = " + encounterId + " AND HasDiagnosisLink = 1 AND LinkedId= " + clinicalId + ") " +
                                                          " BEGIN INSERT INTO [MedFlow].[ProcessedPatientEducationLinks](EncounterId,PatientId,HasDiagnosisLink,HasMedicationLink,HasLabCodeLink,LinkedId) " +
                                                          " VALUES(" + encounterId + "," + obj2.PatientId + ",1,0,0," + clinicalId + ") END ";
                                        cmd.ExecuteNonQuery();
                                        EventLogger.Instance.WriteLog("Patient Education Link generation for Diagnosis URL Success.", EventLogEntryType.Information);
                                        isSuccess = true;
                                    }
                                    else
                                    {
                                        EventLogger.Instance.WriteLog("Patient Education Link generation for Diagnosis URL Failed for Encounter" + encounterId, EventLogEntryType.Error);
                                    }
                                }
                                catch
                                {
                                    EventLogger.Instance.WriteLog("Patient Education Link generation with Diagnosis failed for patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                    isSuccess = false;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.Instance.WriteLog("Patient Education Link generation with Diagnosis failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                    isSuccess = false;
                }
            }
            EventLogger.Instance.WriteLog("End of PatientEducationLink Event.", EventLogEntryType.Information);

            return isSuccess;
        }

        public string GenerateXmLforPatientEducationLinkWithDiagnosis(PatientActivationMessageData pelData)
        {
            string xml = string.Empty;
            try
            {
                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument xmlDoc = new XmlDocument();
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

                XmlElement documentElement = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);

                XmlSchema schema = new XmlSchema();
                schema.Namespaces.Add("xsi", xsi);
                schema.Namespaces.Add("xsd", xsd);
                xmlDoc.Schemas.Add(schema);


                XmlElement schemaNode = xmlDoc.CreateElement("Xml");
                schemaNode.SetAttribute("xmlns:xsi", xsi);
                schemaNode.SetAttribute("xmlns:xsd", xsd);
                xmlDoc.AppendChild(schemaNode);

                XmlNode rootNode = xmlDoc.CreateElement("Type");
                rootNode.InnerText = "Patient Education Link";
                schemaNode.AppendChild(rootNode);


                XmlNode patient = xmlDoc.CreateElement("Patient");
                schemaNode.AppendChild(patient);

                XmlNode firstName = xmlDoc.CreateElement("FirstName");
                firstName.InnerText = Convert.ToString(pelData.FirstName);
                patient.AppendChild(firstName);
                XmlNode lastName = xmlDoc.CreateElement("LastName");
                lastName.InnerText = Convert.ToString(pelData.LastName);
                patient.AppendChild(lastName);
                XmlNode middleName = xmlDoc.CreateElement("MiddleName");
                middleName.InnerText = Convert.ToString(pelData.MiddleName);
                patient.AppendChild(middleName);
                XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
                patientAccount.InnerText = Convert.ToString(pelData.PatientAccount);
                patient.AppendChild(patientAccount);
                XmlNode dob = xmlDoc.CreateElement("DOB");
                dob.InnerText = Convert.ToString(pelData.Dob.ToString("MM/dd/yyyy"));
                patient.AppendChild(dob);
                XmlNode gender = xmlDoc.CreateElement("Gender");
                gender.InnerText = Convert.ToString(pelData.Gender);
                patient.AppendChild(gender);

                // Patient Information Ends Here!

                // Patient Education Link Part starts Here!

                XmlNode patientEducationDocument = xmlDoc.CreateElement("PatientEducationDocument");
                schemaNode.AppendChild(patientEducationDocument);

                XmlNode date = xmlDoc.CreateElement("Date");
                date.InnerText = Convert.ToString(pelData.PatientEducationDate);
                patientEducationDocument.AppendChild(date);

                XmlNode data = xmlDoc.CreateElement("Data");
                data.InnerText = Convert.ToString(pelData.MedlineUrl);
                patientEducationDocument.AppendChild(data);


                XmlNode providerId = xmlDoc.CreateElement("ProviderId");
                providerId.InnerText = Convert.ToString(pelData.ProviderId);
                patientEducationDocument.AppendChild(providerId);


                xml = xmlDoc.InnerXml;
                EventLogger.Instance.WriteLog("PatientEducationLinkWithDiagnosis -> Xml generation successfully for patient '" + pelData.PatientAccount + "'", EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("PatientEducationLinkWithDiagnosis -> Xml generation failed for patient '" + pelData.PatientAccount + "'  \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            return xml;
        }



        /***** 2. Method for Patient Link Enducation - Lab Code ***********/

        public bool PatientEducationLinkwithLabCode(string _connectionString, string _clientKey)
        {

            bool isSuccess = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                try
                {
                    EventLogger.Instance.WriteLog("PatientEducationLinkwithLabCode Started.", EventLogEntryType.Information);
                    string MessageData = string.Empty;
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "";
                        cmd.CommandText = "Select distinct p.Id,FirstName,LastName,MiddleName,GenderId,DateOfBirth,PatientStatusId,ISNULL(Obr.OrderCode,'') as 'OrderCode',ap.ResourceId1, e.Id AS EncounterId, lrep.ReportID As ReportId  FROM model.Patients p";
                        cmd.CommandText += " join model.Encounters e on p.id= e.PatientId";
                        cmd.CommandText += " join dbo.Appointments ap on ap.AppointmentId = e.Id";
                        cmd.CommandText += " INNER join Medflow.PortalQueueXML pe on  pe.EncounterId = e.Id AND pe.ActionTypeLookupId = 16";
                        cmd.CommandText += " inner join dbo.HL7_LabReports lrep on lrep.PatientID = p.Id";
                        cmd.CommandText += " inner join dbo.HL7_Observation Obr on Obr.ReportID = lrep.ReportID";
                        cmd.CommandText += " WHERE p.LastName not like '%Test%'  AND NOT EXISTS (SELECT LinkedId from MedFlow.ProcessedPatientEducationLinks WHERE HasLabCodeLink = 1 AND EncounterId = pe.EncounterId AND PatientId = pe.PatientId AND LinkedId = lrep.ReportID) ";
                        cmd.CommandText += " group by  p.Id,FirstName,LastName,MiddleName,GenderId,DateOfBirth,PatientStatusId,Obr.OrderCode,ap.ResourceId1, e.Id,lrep.ReportID";

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                PatientActivationMessageData obj = new PatientActivationMessageData
                                {
                                    FirstName = dr["FirstName"].ToString(),
                                    LastName = dr["LastName"].ToString(),
                                    PatientAccount = dr["Id"].ToString(),
                                    Dob = Convert.ToDateTime(dr["DateOfBirth"].ToString()),
                                    Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dr["GenderId"].ToString())),
                                    OrderCode = dr["OrderCode"].ToString(),
                                    PatientEducationDate = DateTime.Now.ToString("MM/dd/yyyy"),
                                    ProviderId = dr["ResourceId1"].ToString()
                                };

                                var encounterId = dr["EncounterId"].ToString();
                                var reportId = dr["ReportId"].ToString();

                                obj.MedlineUrl = MedFlowService.Instance.GetPatientEducationLinkForLionicCodes(obj.OrderCode);

                                //obj.MedlineURLNew = Regex.Replace(obj.MedlineURL, "&", "k");

                                PortalQueueXml obj2 = new PortalQueueXml
                                {
                                    PatientId = Convert.ToInt32(dr["Id"].ToString()),
                                    ActionTypeLookupId =
                                        (int)
                                            ((ActionTypeLookUps)
                                                Enum.Parse(typeof(ActionTypeLookUps),
                                                    ActionTypeLookUps.PatientEducationLink.ToString())),
                                    ProcessedDate = DateTime.Now,
                                    IsActive = Convert.ToBoolean(Convert.ToInt32(dr["PatientStatusId"].ToString())),
                                    MessageData = obj
                                };

                                MessageData = GenerateXmLforPatientEducationLinkwithLabCode(obj);
                                //String Encrypted_MessageData = EncryptDecrypt.Encrypt(MessageData);

                                try
                                {
                                    cmd.CommandText = "";
                                    cmd.CommandText = "INSERT INTO  Medflow.PortalQueueXML(MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive, EncounterId)";
                                    cmd.CommandText += "VALUES('" + MessageData.Replace("'", "") + "','" + obj2.ActionTypeLookupId + "','" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + obj2.PatientId + "','" + obj2.IsActive + "','" + encounterId + "')";
                                    cmd.CommandText += " SELECT SCOPE_IDENTITY()";
                                    cmd.CommandType = CommandType.Text;
                                    obj2.ExternalId = Convert.ToInt32(cmd.ExecuteScalar());
                                }
                                catch
                                {
                                    EventLogger.Instance.WriteLog("PatientEducationLinkwithLabCode -> Failed for Encounter '" + encounterId + "'", EventLogEntryType.Error);
                                    isSuccess = false;
                                }
                                var parameters = new ParameterList { ClientKey = _clientKey, ActionTypeLookupTypeId = obj2.ActionTypeLookupId, MessageData = MessageData, ProcessedDate = obj2.ProcessedDate, ExternalId = obj2.ExternalId, PatientId = obj2.PatientId, IsActive = 1 };

                                try
                                {
                                    MedFlowService.Instance.GetMyWebApi(parameters, MedFlowService.Instance.FlagforReturnedUrl, "", _connectionString);
                                    cmd.Parameters.Clear();
                                    cmd.CommandText = " SELECT TOP 1 * from Medflow.PortalQueueXMLResponse where PatientId= '" + obj2.PatientId + "' AND ActionTypeLookupId = '" + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                    SqlDataAdapter dapatient = new SqlDataAdapter(cmd);
                                    DataTable dtPatient = new DataTable();
                                    dapatient.Fill(dtPatient);
                                    if (dtPatient.Rows.Count > 0)
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "  IF NOT EXISTS(SELECT TOP 1 * FROM [MedFlow].[ProcessedPatientEducationLinks] WHERE PatientID = " + obj2.PatientId + " AND EncounterId = " + encounterId + " AND HasLabCodeLink = 1  AND LinkedId = " + reportId + ") " +
                                                          " BEGIN INSERT INTO [MedFlow].[ProcessedPatientEducationLinks](EncounterId,PatientId,HasDiagnosisLink,HasMedicationLink,HasLabCodeLink,LinkedId) " +
                                                          " VALUES(" + encounterId + "," + obj2.PatientId + ",0,0,1," + reportId + ") END ";
                                        cmd.ExecuteNonQuery();

                                        EventLogger.Instance.WriteLog("PatientEducationLinkwithLabCode Success.", EventLogEntryType.Information);
                                        isSuccess = true;
                                    }
                                    else
                                    {
                                        EventLogger.Instance.WriteLog("PatientEducationLinkwithLabCode Failed for Encounter " + encounterId, EventLogEntryType.Error);
                                    }
                                }
                                catch
                                {
                                    EventLogger.Instance.WriteLog("PatientEducationLinkwithLabCode Failed for patient '" + obj2.PatientId + "'", EventLogEntryType.Error);
                                    isSuccess = false;
                                }
                            }
                        }
                    }
                }

                catch (Exception ex)
                {

                    EventLogger.Instance.WriteLog("Patient Education link with Medication Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                    isSuccess = false;
                }
            }


            return isSuccess;


        }

        public string GenerateXmLforPatientEducationLinkwithLabCode(PatientActivationMessageData pelData)
        {
            string xml = string.Empty;
            try
            {

                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument xmlDoc = new XmlDocument();
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

                XmlElement documentElement = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);

                XmlSchema schema = new XmlSchema();
                schema.Namespaces.Add("xsi", xsi);
                schema.Namespaces.Add("xsd", xsd);
                xmlDoc.Schemas.Add(schema);


                XmlElement schemaNode = xmlDoc.CreateElement("Xml");
                schemaNode.SetAttribute("xmlns:xsi", xsi);
                schemaNode.SetAttribute("xmlns:xsd", xsd);
                xmlDoc.AppendChild(schemaNode);

                XmlNode rootNode = xmlDoc.CreateElement("Type");
                rootNode.InnerText = "Patient Education Link";
                schemaNode.AppendChild(rootNode);


                XmlNode patient = xmlDoc.CreateElement("Patient");
                schemaNode.AppendChild(patient);

                XmlNode firstName = xmlDoc.CreateElement("FirstName");
                firstName.InnerText = Convert.ToString(pelData.FirstName);
                patient.AppendChild(firstName);
                XmlNode lastName = xmlDoc.CreateElement("LastName");
                lastName.InnerText = Convert.ToString(pelData.LastName);
                patient.AppendChild(lastName);
                XmlNode middleName = xmlDoc.CreateElement("MiddleName");
                middleName.InnerText = Convert.ToString(pelData.MiddleName);
                patient.AppendChild(middleName);
                XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
                patientAccount.InnerText = Convert.ToString(pelData.PatientAccount);
                patient.AppendChild(patientAccount);
                XmlNode dob = xmlDoc.CreateElement("DOB");
                dob.InnerText = Convert.ToString(pelData.Dob.ToString("MM/dd/yyyy"));
                patient.AppendChild(dob);
                XmlNode gender = xmlDoc.CreateElement("Gender");
                gender.InnerText = Convert.ToString(pelData.Gender);
                patient.AppendChild(gender);

                // Patient Information Ends Here!

                // Patient Education Link Part starts Here!

                XmlNode patientEducationDocument = xmlDoc.CreateElement("PatientEducationDocument");
                schemaNode.AppendChild(patientEducationDocument);

                XmlNode date = xmlDoc.CreateElement("Date");
                date.InnerText = Convert.ToString(pelData.PatientEducationDate);
                patientEducationDocument.AppendChild(date);

                XmlNode data = xmlDoc.CreateElement("Data");
                data.InnerText = Convert.ToString(pelData.MedlineUrl);
                patientEducationDocument.AppendChild(data);


                XmlNode providerId = xmlDoc.CreateElement("ProviderId");
                providerId.InnerText = Convert.ToString(pelData.ProviderId);
                patientEducationDocument.AppendChild(providerId);


                xml = xmlDoc.InnerXml;
                EventLogger.Instance.WriteLog("PatientEducationLinkwithLabCode -> Xml generation successfully for patient '" + pelData.PatientAccount + "'", EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("PatientEducationLinkwithLabCode -> Xml generation failed for patient '" + pelData.PatientAccount + "'  \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            return xml;
        }


        /****** 3. Method for Patient Link Enducation - Medication URL *******/

        public bool PatientEducationLinkwithMedication(string _connectionString, string _clientKey)
        {

            bool isSuccess = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                try
                {
                    EventLogger.Instance.WriteLog("Patient Education Link with Medication URL Started.", EventLogEntryType.Information);
                    string MessageData = string.Empty;
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {

                        cmd.CommandText = "";
                        cmd.CommandText = " Select distinct p.Id,FirstName,LastName,MiddleName,GenderId,DateOfBirth,PatientStatusId,ap.ResourceId1, Replace (Replace(SUBSTRING(findingdetail, 6, charindex('-2/', findingdetail) - 6),'&','and'), '+','Plus') as DrugName, e.Id AS EncounterId,pc.ClinicalId   FROM model.Patients p";
                        cmd.CommandText += " inner join model.Encounters e on p.id= e.PatientId";
                        cmd.CommandText += " inner join dbo.Appointments ap on ap.AppointmentId = e.Id";
                        cmd.CommandText += " INNER join Medflow.PortalQueueXML pe on  pe.EncounterId = e.Id AND pe.ActionTypeLookupId = 16";
                        cmd.CommandText += " inner join dbo.PatientClinical pc on pc.appointmentId = e.Id";
                        cmd.CommandText += " and pc.AppointmentId = e.Id";
                        cmd.CommandText += " and pc.ClinicalType = 'A' and pc.FindingDetail like '%RX-8%' ";
                        cmd.CommandText += " AND p.LastName not like '%Test%' AND NOT EXISTS (SELECT LinkedId from MedFlow.ProcessedPatientEducationLinks WHERE HasMedicationLink = 1 AND EncounterId = pe.EncounterId AND PatientId = pe.PatientId AND LinkedId= pc.ClinicalId) ";
                        cmd.CommandText += " GROUP BY  p.Id,FirstName,LastName,MiddleName,GenderId,DateOfBirth,PatientStatusId,FindingDetail,ap.ResourceId1, e.Id,pc.ClinicalId ";


                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                PatientActivationMessageData obj = new PatientActivationMessageData
                                {
                                    FirstName = dr["FirstName"].ToString(),
                                    LastName = dr["LastName"].ToString(),
                                    PatientAccount = dr["Id"].ToString(),
                                    Dob = Convert.ToDateTime(dr["DateOfBirth"].ToString()),
                                    Gender = Enum.GetName(typeof(Gender), Convert.ToInt32(dr["GenderId"].ToString())),
                                    DrugName = dr["DrugName"].ToString(),
                                    PatientEducationDate = DateTime.Now.ToString("MM/dd/yyyy"),
                                    ProviderId = dr["ResourceId1"].ToString()
                                };


                                var encounterId = dr["EncounterId"].ToString();
                                var clinicalId = dr["ClinicalId"].ToString();
                                obj.MedlineUrl = MedFlowService.Instance.GetPatientEducationLinkForMedicationCode(obj.DrugName);

                                //  obj.MedlineURLNew = Regex.Replace(obj.MedlineURL, "&", "k");

                                PortalQueueXml obj2 = new PortalQueueXml
                                {
                                    PatientId = Convert.ToInt32(dr["Id"].ToString()),
                                    ActionTypeLookupId =
                                        (int)
                                            ((ActionTypeLookUps)
                                                Enum.Parse(typeof(ActionTypeLookUps),
                                                    ActionTypeLookUps.PatientEducationLink.ToString())),
                                    ProcessedDate = DateTime.Now,
                                    IsActive = Convert.ToBoolean(Convert.ToInt32(dr["PatientStatusId"].ToString())),
                                    MessageData = obj
                                };

                                MessageData = GenerateXmLforPatientEducationLinkwithMedication(obj);
                                //string Encrypted_MessageData = EncryptDecrypt.Encrypt(MessageData);

                                try
                                {
                                    cmd.CommandText = "";
                                    cmd.CommandText = "INSERT INTO  Medflow.PortalQueueXML(MessageData,ActionTypeLookupId,ProcessedDate,PatientId,IsActive, EncounterId)";
                                    cmd.CommandText += " VALUES('" + MessageData.Replace("'", "") + "','" + obj2.ActionTypeLookupId + "','" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + obj2.PatientId + "','" + obj2.IsActive + "','" + encounterId + "')";
                                    cmd.CommandText += " SELECT SCOPE_IDENTITY()";
                                    cmd.CommandType = CommandType.Text;
                                    obj2.ExternalId = Convert.ToInt32(cmd.ExecuteScalar());
                                }
                                catch
                                {
                                    EventLogger.Instance.WriteLog("PatientEducationLinkwithMedication -> Patient Education Link with Medication URL Failed for Encounter while Inserting in PortalQueue '" + encounterId + "'", EventLogEntryType.Error);
                                    isSuccess = false;
                                }

                                var parameters = new ParameterList { ClientKey = _clientKey, ActionTypeLookupTypeId = obj2.ActionTypeLookupId, MessageData = MessageData, ProcessedDate = obj2.ProcessedDate, ExternalId = obj2.ExternalId, PatientId = obj2.PatientId, IsActive = 1 };

                                try
                                {
                                    MedFlowService.Instance.GetMyWebApi(parameters, MedFlowService.Instance.FlagforReturnedUrl, "", _connectionString);

                                    cmd.Parameters.Clear();
                                    cmd.CommandText = " SELECT TOP 1 * from Medflow.PortalQueueXMLResponse where PatientId= '" + obj2.PatientId + "' AND ActionTypeLookupId = '" + obj2.ActionTypeLookupId + "' AND ProcessedDate = '" + obj2.ProcessedDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                    SqlDataAdapter dapatient = new SqlDataAdapter(cmd);
                                    DataTable dtPatient = new DataTable();
                                    dapatient.Fill(dtPatient);
                                    if (dtPatient.Rows.Count > 0)
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "  IF NOT EXISTS(SELECT TOP 1 * FROM [MedFlow].[ProcessedPatientEducationLinks] WHERE PatientID = " + obj2.PatientId + " AND EncounterId = " + encounterId + " AND HasMedicationLink= 1 AND LinkedId= " + clinicalId + ") " +
                                                          " BEGIN INSERT INTO [MedFlow].[ProcessedPatientEducationLinks](EncounterId,PatientId,HasDiagnosisLink,HasMedicationLink,HasLabCodeLink,LinkedId) " +
                                                          " VALUES(" + encounterId + "," + obj2.PatientId + ",0,1,0," + clinicalId + ") END ";
                                        cmd.ExecuteNonQuery();

                                        EventLogger.Instance.WriteLog("PatientEducationLinkwithMedication -> Patient Education Link with Medication Success.", EventLogEntryType.Information);
                                        isSuccess = true;
                                    }
                                    else
                                    {
                                        EventLogger.Instance.WriteLog("PatientEducationLinkwithMedication -> Patient Education Link with Medication Failed for Encounter.Couldn't find Processed row" + encounterId, EventLogEntryType.Error);
                                    }
                                }
                                catch
                                {
                                    EventLogger.Instance.WriteLog(" PatientEducationLinkwithMedication -> Patient Education Link with Medication Failed for Encounter '" + encounterId + "'", EventLogEntryType.Error);
                                    isSuccess = false;
                                }
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    EventLogger.Instance.WriteLog("Patient Education Link with Medication Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                    isSuccess = false;
                }
            }
            return isSuccess;
        }

        public string GenerateXmLforPatientEducationLinkwithMedication(PatientActivationMessageData pelData)
        {
            string xml = string.Empty;
            try
            {
                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument xmlDoc = new XmlDocument();
                var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", null, null);

                XmlElement documentElement = xmlDoc.DocumentElement;
                xmlDoc.InsertBefore(xmlDeclaration, documentElement);

                XmlSchema schema = new XmlSchema();
                schema.Namespaces.Add("xsi", xsi);
                schema.Namespaces.Add("xsd", xsd);
                xmlDoc.Schemas.Add(schema);


                XmlElement schemaNode = xmlDoc.CreateElement("Xml");
                schemaNode.SetAttribute("xmlns:xsi", xsi);
                schemaNode.SetAttribute("xmlns:xsd", xsd);
                xmlDoc.AppendChild(schemaNode);

                XmlNode rootNode = xmlDoc.CreateElement("Type");
                rootNode.InnerText = "Patient Education Link";
                schemaNode.AppendChild(rootNode);


                XmlNode patient = xmlDoc.CreateElement("Patient");
                schemaNode.AppendChild(patient);

                XmlNode firstName = xmlDoc.CreateElement("FirstName");
                firstName.InnerText = Convert.ToString(pelData.FirstName);
                patient.AppendChild(firstName);
                XmlNode lastName = xmlDoc.CreateElement("LastName");
                lastName.InnerText = Convert.ToString(pelData.LastName);
                patient.AppendChild(lastName);
                XmlNode middleName = xmlDoc.CreateElement("MiddleName");
                middleName.InnerText = Convert.ToString(pelData.MiddleName);
                patient.AppendChild(middleName);
                XmlNode patientAccount = xmlDoc.CreateElement("PatientAccount");
                patientAccount.InnerText = Convert.ToString(pelData.PatientAccount);
                patient.AppendChild(patientAccount);
                XmlNode dob = xmlDoc.CreateElement("DOB");
                dob.InnerText = Convert.ToString(pelData.Dob.ToString("MM/dd/yyyy"));
                patient.AppendChild(dob);
                XmlNode gender = xmlDoc.CreateElement("Gender");
                gender.InnerText = Convert.ToString(pelData.Gender);
                patient.AppendChild(gender);

                // Patient Information Ends Here!

                // Patient Education Link Part starts Here!

                XmlNode patientEducationDocument = xmlDoc.CreateElement("PatientEducationDocument");
                schemaNode.AppendChild(patientEducationDocument);

                XmlNode date = xmlDoc.CreateElement("Date");
                date.InnerText = Convert.ToString(pelData.PatientEducationDate);
                patientEducationDocument.AppendChild(date);

                XmlNode data = xmlDoc.CreateElement("Data");
                data.InnerText = Convert.ToString(pelData.MedlineUrl);
                patientEducationDocument.AppendChild(data);


                XmlNode providerId = xmlDoc.CreateElement("ProviderId");
                providerId.InnerText = Convert.ToString(pelData.ProviderId);
                patientEducationDocument.AppendChild(providerId);


                xml = xmlDoc.InnerXml;
                EventLogger.Instance.WriteLog("PatientEducationLinkwithMedication -> Xml generation successfully for patient '" + pelData.PatientAccount + "'", EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("PatientEducationLinkwithMedication -> Xml generation failed for patient '" + pelData.PatientAccount + "'  \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            return xml;
        }


        /********************************************************* Patient Education Link Functions ends here ****************************/


        public void GetVDTInformationforMUCalculator()
        {
            EventLogger.Instance.WriteLog("Get VDT information from MVE Environment", EventLogEntryType.Information);
            MedFlowServices.MedFlowService.Instance.InsertVDTInformationfromMVE();
        }


        #endregion


        public void CleanUpData()
        {
            try
            {
                for (int temp = 1; temp < ConfigurationManager.ConnectionStrings.Count; temp++)
                {
                    string _connectionString = ConfigurationManager.ConnectionStrings[temp].ConnectionString.ToString();
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        string MessageData = string.Empty;
                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            if (ConfigHelper.CleanHealthInformation)
                            {
                                //Clean PatientHealthInformation
                                cmd.CommandText = " DELETE FROM MedFlow.ProcessedPatientHealthInformation ";
                                cmd.ExecuteNonQuery();
                            }
                            if (ConfigHelper.CleanEducationInformation)
                            {
                                //Clean EducationLinks
                                cmd.CommandText = " DELETE FROM MedFlow.ProcessedPatientEducationLinks ";
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private bool CheckServiceEndTime()
        {
            var currentDateTime = DateTime.UtcNow;
            DateTime scheduledEndTime = DateTime.Parse(ConfigHelper.MedFlowScheduledEndTime);

            if (currentDateTime.Hour >= scheduledEndTime.Hour && currentDateTime.Minute >= scheduledEndTime.Minute && currentDateTime.ToString("tt") == scheduledEndTime.ToString("tt"))
            {
                return true;
            }
            return false;
        }

        //  For Encoding into Base 64 string from String
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        //For Decoding

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }


    }

}
