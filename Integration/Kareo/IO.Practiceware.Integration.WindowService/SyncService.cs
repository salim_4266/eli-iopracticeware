﻿using IO.Practiceware.Integration.Main;
using IO.Practiceware.Integration.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace IO.Practiceware.Integration.WindowService
{
    public partial class SyncService : ServiceBase
    {
        private bool isConnected = false;
        private bool requiredTablesExists = false;

        public SyncService()
        {
            InitializeComponent();
            this.AutoLog = false;
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {

            EventLogger.Instance.WriteLog("Sync Service Started.", EventLogEntryType.Information);
            
            SyncMain syncMain = new SyncMain();
            isConnected = syncMain.CheckConnection();
            if (isConnected)
            {
                requiredTablesExists = syncMain.CreateRequiredTablesIfNotExist();
            }
#if DEBUG
            if (isConnected && requiredTablesExists)
            {
                syncMain.StartSync();
            }
#else
            // Set up a timer to trigger every minute.
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = GetInterval();
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
#endif

        }

        private void OnTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (isConnected && requiredTablesExists)
            {
                SyncMain syncMain = new SyncMain();
                syncMain.StartSync();
            }
        }

        protected override void OnStop()
        {
            EventLogger.Instance.WriteLog("Sync Service Stopped.", EventLogEntryType.Information);
        }

        /// <summary>
        /// return milliseconds
        /// </summary>
        /// <returns></returns>
        public double GetInterval()
        {
            int minutes = 10;
            try
            {
                minutes = Convert.ToInt32(ConfigHelper.SyncInterval);
            }
            catch (Exception e)
            {
            }
            return TimeSpan.FromMinutes(minutes).TotalMilliseconds;
        }
    }
}
