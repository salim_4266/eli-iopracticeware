﻿using IO.Practiceware.Integration.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using IO.Practiceware.Integration.Utility;
using System.IO;
using System.Reflection;

namespace IO.Practiceware.Integration.Services
{
    public interface IIOService
    {
        /// <summary>
        /// creates a Patient in IO for an External System Patient
        /// </summary>
        /// <param name="patient"></param>
        /// <returns></returns>
        int CreatePatient(ExternalSystemPatientDTO patient);

        /// <summary>
        /// updates patient in IO for an External System Patient
        /// </summary>
        /// <param name="patient"></param>
        void UpdatePatient(ExternalSystemPatientDTO patient);

        /// <summary>
        /// creates appointment Type in IO for an External System AppointmentType
        /// </summary>
        /// <param name="appointmentType"></param>
        /// <returns></returns>
        int CreateAppointmentType(AppointmentTypeDTO appointmentType);

        /// <summary>
        /// returns matched Appointment Type
        /// </summary>
        /// <param name="appointmentType"></param>
        /// <returns></returns>
        AppointmentTypeDTO GetAppointmentType(string appointmentType);

        /// <summary>
        /// creates a Provider in IO for an External System Provider
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        int CreateResource(ExternalSystemProviderDTO resource);

        /// <summary>
        /// creates an appointment in IO for an External System Appointment
        /// </summary>
        /// <param name="appointment"></param>
        /// <returns></returns>
        int CreateAppointment(ExternalSystemAppointmentDTO appointment);

        /// <summary>
        ///  creates a practice in IO for an External System Practice
        /// </summary>
        /// <param name="practice"></param>
        /// <returns></returns>
        int CreatePractice(ExternalSystemPracticeDTO practice);

        /// <summary>
        /// updates a practice in IO for an External System Practice
        /// </summary>
        /// <param name="practice"></param>
        void UpdatePractice(ExternalSystemPracticeDTO practice);

        /// <summary>
        /// checks if resource already exist in IO
        /// </summary>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        int CheckIfResourceExistInIO(string resourceName);

        /// <summary>
        /// get a unique password for Resource, which has not been used yet
        /// </summary>
        /// <returns></returns>
        string GetAvailablePasswordForResource();

        /// <summary>
        /// creates a service location in IO for an External System Service Location
        /// </summary>
        /// <param name="serviceLocation"></param>
        /// <returns></returns>
        int CreateServiceLocation(ExternalSystemServiceLocationDTO serviceLocation);

        /// <summary>
        /// update some basic settings in IO ie. No validations are required in CheckIn and CheckOut
        /// </summary>
        void SetApplicationConfigurationSetting();

        /// <summary>
        /// returns a list of invoice ids whose encouter is not created yet in External System
        /// </summary>
        /// <returns></returns>
        List<int> GetInvoiceIdListWhoseEncounterIsNotCreatedYet();

        /// <summary>
        /// retruns IO Invoice
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        InvoiceDTO GetInvoiceDetail(int invoiceId);

        /// <summary>
        /// retruns IO Encounter
        /// </summary>
        /// <param name="encounterid"></param>
        /// <returns></returns>
        EncounterDTO GetEncounter(int encounterid);

        /// <summary>
        /// updates appointment in IO for an External System Appointment
        /// </summary>
        /// <param name="externalAppointment"></param>
        void UpdateAppointment(ExternalSystemAppointmentDTO externalAppointment);

        /// <summary>
        /// checks if application is connected to IO server
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        bool IsConnected(out string errorMessage);

        /// <summary>
        /// adds additional permission to a providers ( ie. Bill Invoice, View Appointment Tab etc)
        /// </summary>
        /// <param name="userId"></param>
        void AddNecessaryPermissionsToProvider(int userId);

        /// <summary>
        /// creates required mapping tables
        /// </summary>
        /// <returns></returns>
        void CreateRequiredTables();

    }

    public class IOService : IIOService
    {
        private SqlConnection _connection;

        /// <summary>
        /// you must have to pass a sql connection which is in open mode to access functions of this class
        /// </summary>
        /// <param name="connection"></param>
        public IOService(SqlConnection connection)
        {
            _connection = connection;
        }

        public int CreatePatient(ExternalSystemPatientDTO patient)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO model.Patients (FirstName,LastName,GenderId,DateOfBirth,PatientStatusId,IsClinical,IsHipaaConsentSigned,IsCollectionLetterSuppressed,PaymentOfBenefitsToProviderId) ";
                cmd.CommandText += "Values (@FirstName,@LastName,@GenderId,@DateOfBirth,@PatientStatusId,@IsClinical,@IsHipaaConsentSigned,@IsCollectionLetterSuppressed,@PaymentOfBenefitsToProviderId)";
                cmd.CommandText += "SELECT CAST(scope_identity() AS int)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@FirstName", patient.FirstName);
                cmd.Parameters.AddWithValue("@LastName", patient.LastName);
                cmd.Parameters.AddWithValue("@GenderId", patient.Gender);
                cmd.Parameters.AddWithValue("@DateOfBirth", patient.DateOfBirth);
                cmd.Parameters.AddWithValue("@PatientStatusId", 1); // from enum
                cmd.Parameters.AddWithValue("@IsClinical", 1);
                cmd.Parameters.AddWithValue("@IsHipaaConsentSigned", 1);
                cmd.Parameters.AddWithValue("@IsCollectionLetterSuppressed", 0);
                cmd.Parameters.AddWithValue("@PaymentOfBenefitsToProviderId", 1); // from enum 
                return (int)cmd.ExecuteScalar();
            }
        }

        public void UpdatePatient(ExternalSystemPatientDTO patient)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {

                cmd.CommandText = "UPDATE model.Patients SET FirstName=@FirstName,LastName=@LastName,GenderId=@GenderId,DateOfBirth=@DateOfBirth WHERE Id=@Id";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@Id", patient.PatientId);
                cmd.Parameters.AddWithValue("@FirstName", patient.FirstName);
                cmd.Parameters.AddWithValue("@LastName", patient.LastName);
                cmd.Parameters.AddWithValue("@GenderId", patient.Gender);
                cmd.Parameters.AddWithValue("@DateOfBirth", patient.DateOfBirth);
                cmd.ExecuteNonQuery();
            }
        }

        public int CreateAppointmentType(AppointmentTypeDTO appointmentType)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO dbo.AppointmentType (AppointmentType, Question, Duration, DurationOther, Recursion, ResourceId1, ResourceId2, ResourceId3, ResourceId4, ResourceId5, ResourceId6, ResourceId7, ResourceId8, DrRequired, PcRequired, Status, Period, Rank, OtherRank, RecallFreq, RecallMax, Category, FirstVisitIndicator, AvailableInPlan, EMBasis, AppointmentCategoryId)  ";
                cmd.CommandText += "VALUES (@AppointmentType,@Question,@Duration,@DurationOther,@Recursion,@ResourceId1,@ResourceId2,@ResourceId3,@ResourceId4,@ResourceId5,@ResourceId6,@ResourceId7,@ResourceId8,@DrRequired,@PcRequired,@Status,@Period,@Rank,@OtherRank,@RecallFreq,@RecallMax,@Category,@FirstVisitIndicator,@AvailableInPlan,@EMBasis,@AppointmentCategoryId) ";
                cmd.CommandText += "SELECT CAST(scope_identity() AS int)";
                cmd.Parameters.AddWithValue("@AppointmentType", appointmentType.AppointmentType);
                cmd.Parameters.AddWithValue("@Question", "First Visit");
                cmd.Parameters.AddWithValue("@Duration", 0);
                cmd.Parameters.AddWithValue("@DurationOther", 0);
                cmd.Parameters.AddWithValue("@Recursion", 0);
                cmd.Parameters.AddWithValue("@ResourceId1", 0);
                cmd.Parameters.AddWithValue("@ResourceId2", 0);
                cmd.Parameters.AddWithValue("@ResourceId3", 0);
                cmd.Parameters.AddWithValue("@ResourceId4", 0);
                cmd.Parameters.AddWithValue("@ResourceId5", 0);
                cmd.Parameters.AddWithValue("@ResourceId6", 0);
                cmd.Parameters.AddWithValue("@ResourceId7", 0);
                cmd.Parameters.AddWithValue("@ResourceId8", 0);
                cmd.Parameters.AddWithValue("@DrRequired", "N");
                cmd.Parameters.AddWithValue("@PcRequired", "N");
                cmd.Parameters.AddWithValue("@Status", "A");
                cmd.Parameters.AddWithValue("@Period", string.Empty);
                cmd.Parameters.AddWithValue("@Rank", 0);
                cmd.Parameters.AddWithValue("@OtherRank", 0);
                cmd.Parameters.AddWithValue("@RecallFreq", 0);
                cmd.Parameters.AddWithValue("@RecallMax", 0);
                cmd.Parameters.AddWithValue("@Category", string.Empty);
                cmd.Parameters.AddWithValue("@FirstVisitIndicator", "N");
                cmd.Parameters.AddWithValue("@AvailableInPlan", "T");
                cmd.Parameters.AddWithValue("@EMBasis", string.Empty);
                cmd.Parameters.AddWithValue("@AppointmentCategoryId", DBNull.Value);
                return (int)cmd.ExecuteScalar();
            }
        }

        public AppointmentTypeDTO GetAppointmentType(string appointmentTypeName)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM dbo.AppointmentType WHERE AppointmentType=@AppointmentType";
                cmd.Parameters.AddWithValue("@AppointmentType", appointmentTypeName);

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        AppointmentTypeDTO appointmentType = new AppointmentTypeDTO
                        {
                            AppTypeId = Convert.ToInt32(ds.Tables[0].Rows[0]["AppTypeId"]),
                            AppointmentType = ds.Tables[0].Rows[0]["AppointmentType"].ToString()
                        };
                        return appointmentType;
                    }
                    else
                    {
                        return null;
                    }
                }

            }
        }

        public int CreateResource(ExternalSystemProviderDTO resource)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText += "INSERT INTO Resources ";
                cmd.CommandText += "(ResourceName, ResourceType, ResourceDescription, ResourcePid, ResourcePermissions, ResourceColor, ResourceOverLoad, ResourceCost, ResourceSpecialty, ";
                cmd.CommandText += "ResourceTaxId, ResourceAddress, ResourceSuite, ResourceCity, ResourceState, ResourceZip, ResourcePhone, ResourceEmail, ServiceCode, ResourceSSN, ";
                cmd.CommandText += "ResourceDEA, ResourceLicence, ResourceScheduleTemplate2, ResourceUPIN, ResourceScheduleTemplate1, ResourceScheduleTemplate3, PracticeId, StartTime1, ";
                cmd.CommandText += "EndTime1, StartTime2, EndTime2, StartTime3, EndTime3, StartTime4, EndTime4, StartTime5, EndTime5, StartTime6, EndTime6, StartTime7, EndTime7, Vacation, ";
                cmd.CommandText += "Status, ResourceLastName, ResourceFirstName, SignatureFile, PlaceOfService, Billable, GoDirect, NPI, ResourceMI, ResourceSuffix, ResourcePrefix, IsLoggedIn, ";
                cmd.CommandText += "FileServer, HexColor, OrdinalId) ";
                cmd.CommandText += "VALUES (@ResourceName,@ResourceType,@ResourceDescription,@ResourcePid,@ResourcePermissions,@ResourceColor,@ResourceOverLoad,@ResourceCost,@ResourceSpecialty, ";
                cmd.CommandText += "@ResourceTaxId,@ResourceAddress,@ResourceSuite,@ResourceCity,@ResourceState,@ResourceZip,@ResourcePhone,@ResourceEmail,@ServiceCode,@ResourceSSN,@ResourceDEA, ";
                cmd.CommandText += "@ResourceLicence,@ResourceScheduleTemplate2,@ResourceUPIN,@ResourceScheduleTemplate1,@ResourceScheduleTemplate3,@PracticeId,@StartTime1, ";
                cmd.CommandText += "@EndTime1,@StartTime2,@EndTime2,@StartTime3,@EndTime3,@StartTime4,@EndTime4,@StartTime5,@EndTime5,@StartTime6,@EndTime6,@StartTime7,@EndTime7, ";
                cmd.CommandText += "@Vacation,@Status,@ResourceLastName,@ResourceFirstName,@SignatureFile,@PlaceOfService,@Billable,@GoDirect,@NPI,@ResourceMI,@ResourceSuffix, ";
                cmd.CommandText += "@ResourcePrefix,@IsLoggedIn,@FileServer,@HexColor,@OrdinalId)";
                cmd.CommandText += "SELECT CAST(scope_identity() AS int)";

                cmd.Parameters.AddWithValue("@ResourceName", resource.FullName);
                cmd.Parameters.AddWithValue("@ResourceType", "D"); // 'D' for Doctor
                cmd.Parameters.AddWithValue("@ResourceDescription", resource.FullName);
                cmd.Parameters.AddWithValue("@ResourcePid", GetAvailablePasswordForResource());
                cmd.Parameters.AddWithValue("@ResourcePermissions", "11011111110111100110111110000000111001111111111111101110000000111101100000000000000000000000000000011111111111000000000000000000");
                cmd.Parameters.AddWithValue("@ResourceColor", 0);
                cmd.Parameters.AddWithValue("@ResourceOverLoad", 0);
                cmd.Parameters.AddWithValue("@ResourceCost", 0);
                cmd.Parameters.AddWithValue("@ResourceSpecialty", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceTaxId", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceAddress", resource.AddressLine1 + ", " + resource.AddressLine2);
                cmd.Parameters.AddWithValue("@ResourceSuite", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceCity", resource.City);
                cmd.Parameters.AddWithValue("@ResourceState", resource.State);
                cmd.Parameters.AddWithValue("@ResourceZip", resource.ZipCode);
                cmd.Parameters.AddWithValue("@ResourcePhone", resource.HomePhone);
                cmd.Parameters.AddWithValue("@ResourceEmail", resource.EmailAddress);
                cmd.Parameters.AddWithValue("@ServiceCode", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceSSN", DBNull.Value);
                cmd.Parameters.AddWithValue("@ResourceDEA", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceLicence", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceScheduleTemplate2", 0);
                cmd.Parameters.AddWithValue("@ResourceUPIN", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceScheduleTemplate1", 0);
                cmd.Parameters.AddWithValue("@ResourceScheduleTemplate3", 0);
                cmd.Parameters.AddWithValue("@PracticeId", resource.IO_PracticeId);
                cmd.Parameters.AddWithValue("@StartTime1", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime1", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime2", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime2", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime3", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime3", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime4", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime4", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime5", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime5", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime6", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime6", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime7", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime7", string.Empty);
                cmd.Parameters.AddWithValue("@Vacation", string.Empty);
                cmd.Parameters.AddWithValue("@Status", "A");
                cmd.Parameters.AddWithValue("@ResourceLastName", resource.LastName);
                cmd.Parameters.AddWithValue("@ResourceFirstName", resource.FirstName);
                cmd.Parameters.AddWithValue("@SignatureFile", string.Empty);
                cmd.Parameters.AddWithValue("@PlaceOfService", string.Empty);
                cmd.Parameters.AddWithValue("@Billable", "Y");
                cmd.Parameters.AddWithValue("@GoDirect", "N");
                cmd.Parameters.AddWithValue("@NPI", resource.NationalProviderIdentifier);
                cmd.Parameters.AddWithValue("@ResourceMI", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceSuffix", resource.Suffix);
                cmd.Parameters.AddWithValue("@ResourcePrefix", resource.Prefix);
                cmd.Parameters.AddWithValue("@IsLoggedIn", 0);
                cmd.Parameters.AddWithValue("@FileServer", DBNull.Value);
                cmd.Parameters.AddWithValue("@HexColor", "FFFFFF");
                cmd.Parameters.AddWithValue("@OrdinalId", 1);

                return (int)cmd.ExecuteScalar();
            }
        }

        public int CreateAppointment(ExternalSystemAppointmentDTO appointment)
        {
            int appointmentStatus = appointment.ConfirmationStatus == "Cancelled" ? 14 : 1; // enum EncounterStatus -> 14 for Cancel, 1 for Pending
            int encounterId = 0;
            int appointmentId = 0;
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT model.Encounters(PatientId,EncounterTypeId, EncounterStatusId, ServiceLocationId, InsuranceTypeId, EncounterStatusChangeReasonId, ";
                cmd.CommandText += "EncounterStatusChangeComment, ConfirmationStatusId, StartDateTime, EndDateTime) ";
                cmd.CommandText += "VALUES (@PatientId,@EncounterTypeId, @EncounterStatusId, @ServiceLocationId, @InsuranceTypeId,@EncounterStatusChangeReasonId, ";
                cmd.CommandText += "@EncounterStatusChangeComment, @ConfirmationStatusId, @StartDateTime, @EndDateTime)";
                cmd.CommandText += "SELECT CAST(scope_identity() AS int)";
                cmd.Parameters.AddWithValue("@PatientId", appointment.IO_PatientId);
                cmd.Parameters.AddWithValue("@EncounterTypeId", 1); // enum EncounterTypeId - Clinician Encounter
                cmd.Parameters.AddWithValue("@EncounterStatusId", appointmentStatus);
                cmd.Parameters.AddWithValue("@ServiceLocationId", appointment.IO_ServiceLocationId);
                cmd.Parameters.AddWithValue("@InsuranceTypeId", 1); // enum InsuranceType - MajorMedical
                cmd.Parameters.AddWithValue("@EncounterStatusChangeReasonId", DBNull.Value); // will be NULL
                cmd.Parameters.AddWithValue("@EncounterStatusChangeComment", DBNull.Value); // wii be NULL
                cmd.Parameters.AddWithValue("@ConfirmationStatusId", DBNull.Value); // will be NULL
                cmd.Parameters.AddWithValue("@StartDateTime", new DateTime(1753, 01, 01));
                cmd.Parameters.AddWithValue("@EndDateTime", new DateTime(1753, 01, 01));
                encounterId = (int)cmd.ExecuteScalar();
            }

            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT model.Appointments (DateTime, AppointmentTypeId, EncounterId, Comment, OrderDate, DisplayOnCalendar, RoomId, EquipmentId, UserId, __EntityType__) ";
                cmd.CommandText += "VALUES (@DateTime, @AppointmentTypeId, @EncounterId, @Comment, @OrderDate, @DisplayOnCalendar, @RoomId, @EquipmentId, @UserId, @__EntityType__)";
                cmd.CommandText += "SELECT CAST(scope_identity() AS int)";
                cmd.Parameters.AddWithValue("@DateTime", appointment.StartDate);
                cmd.Parameters.AddWithValue("@AppointmentTypeId", appointment.IO_AppointmentTypeId);
                cmd.Parameters.AddWithValue("@EncounterId", encounterId);
                cmd.Parameters.AddWithValue("@Comment", string.Empty);
                cmd.Parameters.AddWithValue("@OrderDate", DBNull.Value); // will be NULL
                cmd.Parameters.AddWithValue("@DisplayOnCalendar", true);
                cmd.Parameters.AddWithValue("@RoomId", DBNull.Value); // will be NULL
                cmd.Parameters.AddWithValue("@EquipmentId", DBNull.Value);// will be NULL
                cmd.Parameters.AddWithValue("@UserId", appointment.IO_ResourceId);
                cmd.Parameters.AddWithValue("@__EntityType__", "UserAppointment");
                appointmentId = (int)cmd.ExecuteScalar();
            }
            return appointmentId;
        }

        public int CreatePractice(ExternalSystemPracticeDTO practice)
        {
            string startTime = "05:30 AM";
            string endTime = "11:00 PM";
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO PracticeName ";
                cmd.CommandText += " (PracticeType, PracticeName, PracticeAddress, PracticeSuite, PracticeCity, PracticeState, PracticeZip, PracticePhone, PracticeOtherPhone, PracticeFax, ";
                cmd.CommandText += "PracticeEmail, PracticeTaxId, PracticeOtherId, StartTime1, EndTime1, StartTime2, EndTime2, StartTime3, EndTime3, StartTime4, EndTime4, StartTime5, EndTime5, ";
                cmd.CommandText += "StartTime6, EndTime6, StartTime7, EndTime7, Vacation, LoginType, LocationReference, BillingOffice, CatArray1, CatArray2, CatArray3, CatArray4, CatArray5, ";
                cmd.CommandText += "CatArray6, CatArray7, CatArray8, PracticeNPI, PracticeTaxonomy, HexColor)  ";
                cmd.CommandText += "VALUES (@PracticeType,@PracticeName,@PracticeAddress,@PracticeSuite,@PracticeCity,@PracticeState,@PracticeZip,@PracticePhone,@PracticeOtherPhone,@PracticeFax,@PracticeEmail,@PracticeTaxId, ";
                cmd.CommandText += "@PracticeOtherId,@StartTime1,@EndTime1,@StartTime2,@EndTime2,@StartTime3,@EndTime3,@StartTime4,@EndTime4,@StartTime5,@EndTime5,@StartTime6,@EndTime6,@StartTime7,@EndTime7,@Vacation, ";
                cmd.CommandText += "@LoginType,@LocationReference,@BillingOffice,@CatArray1,@CatArray2,@CatArray3,@CatArray4,@CatArray5,@CatArray6,@CatArray7,@CatArray8,@PracticeNPI,@PracticeTaxonomy,@HexColor) ";
                cmd.CommandText += "SELECT CAST(scope_identity() AS int)";

                cmd.Parameters.AddWithValue("@PracticeType", "P");
                cmd.Parameters.AddWithValue("@PracticeName", practice.PracticeName);
                cmd.Parameters.AddWithValue("@PracticeAddress", practice.PracticeAddress);
                cmd.Parameters.AddWithValue("@PracticeSuite", string.Empty);
                cmd.Parameters.AddWithValue("@PracticeCity", practice.City);
                cmd.Parameters.AddWithValue("@PracticeState", practice.State);
                cmd.Parameters.AddWithValue("@PracticeZip", practice.Zip);
                cmd.Parameters.AddWithValue("@PracticePhone", practice.Phone);
                cmd.Parameters.AddWithValue("@PracticeOtherPhone", string.Empty);
                cmd.Parameters.AddWithValue("@PracticeFax", practice.Fax);
                cmd.Parameters.AddWithValue("@PracticeEmail", practice.Email);
                cmd.Parameters.AddWithValue("@PracticeTaxId", practice.TaxId);
                cmd.Parameters.AddWithValue("@PracticeOtherId", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime1", startTime);
                cmd.Parameters.AddWithValue("@EndTime1", endTime);
                cmd.Parameters.AddWithValue("@StartTime2", startTime);
                cmd.Parameters.AddWithValue("@EndTime2", endTime);
                cmd.Parameters.AddWithValue("@StartTime3", startTime);
                cmd.Parameters.AddWithValue("@EndTime3", endTime);
                cmd.Parameters.AddWithValue("@StartTime4", startTime);
                cmd.Parameters.AddWithValue("@EndTime4", endTime);
                cmd.Parameters.AddWithValue("@StartTime5", startTime);
                cmd.Parameters.AddWithValue("@EndTime5", endTime);
                cmd.Parameters.AddWithValue("@StartTime6", startTime);
                cmd.Parameters.AddWithValue("@EndTime6", endTime);
                cmd.Parameters.AddWithValue("@StartTime7", startTime);
                cmd.Parameters.AddWithValue("@EndTime7", endTime);
                cmd.Parameters.AddWithValue("@Vacation", string.Empty);
                cmd.Parameters.AddWithValue("@LoginType", "1");
                cmd.Parameters.AddWithValue("@LocationReference", practice.LocationReference);
                cmd.Parameters.AddWithValue("@BillingOffice", practice.BillingOffice);
                cmd.Parameters.AddWithValue("@CatArray1", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray2", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray3", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray4", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray5", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray6", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray7", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray8", string.Empty);
                cmd.Parameters.AddWithValue("@PracticeNPI", practice.NPI);
                cmd.Parameters.AddWithValue("@PracticeTaxonomy", string.Empty);
                cmd.Parameters.AddWithValue("@HexColor", "FFFFFF");

                return (int)cmd.ExecuteScalar();
            }
        }

        public void UpdatePractice(ExternalSystemPracticeDTO practice)
        {
            string startTime = "05:30 AM";
            string endTime = "11:00 PM";
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE PracticeName ";
                cmd.CommandText += "SET PracticeType = @PracticeType, PracticeName = @PracticeName, PracticeAddress = @PracticeAddress, PracticeSuite = @PracticeSuite, PracticeCity = @PracticeCity, ";
                cmd.CommandText += "PracticeState = @PracticeState, PracticeZip = @PracticeZip, PracticePhone = @PracticePhone, PracticeOtherPhone = @PracticeOtherPhone, ";
                cmd.CommandText += "PracticeFax = @PracticeFax, PracticeEmail = @PracticeEmail, PracticeTaxId = @PracticeTaxId, PracticeOtherId = @PracticeOtherId, StartTime1 = @StartTime1, ";
                cmd.CommandText += "EndTime1 = @EndTime1, StartTime2 = @StartTime2, EndTime2 = @EndTime2, StartTime3 = @StartTime3, EndTime3 = @EndTime3, StartTime4 = @StartTime4, ";
                cmd.CommandText += "EndTime4 = @EndTime4, StartTime5 = @StartTime5, EndTime5 = @EndTime5, StartTime6 = @StartTime6, EndTime6 = @EndTime6, StartTime7 = @StartTime7, ";
                cmd.CommandText += "EndTime7 = @EndTime7, Vacation = @Vacation, LoginType = @LoginType, LocationReference = @LocationReference, BillingOffice = @BillingOffice, ";
                cmd.CommandText += "CatArray1 = @CatArray1, CatArray2 = @CatArray2, CatArray3 = @CatArray3, CatArray4 = @CatArray4, CatArray5 = @CatArray5, CatArray6 = @CatArray6, ";
                cmd.CommandText += "CatArray7 = @CatArray7, CatArray8 = @CatArray8, PracticeNPI = @PracticeNPI, PracticeTaxonomy = @PracticeTaxonomy, HexColor = @HexColor ";
                cmd.CommandText += "WHERE PracticeId=@PracticeId";

                cmd.Parameters.AddWithValue("@PracticeId", practice.IO_PracticeId);
                cmd.Parameters.AddWithValue("@PracticeType", "P");
                cmd.Parameters.AddWithValue("@PracticeName", practice.PracticeName);
                cmd.Parameters.AddWithValue("@PracticeAddress", practice.PracticeAddress);
                cmd.Parameters.AddWithValue("@PracticeSuite", string.Empty);
                cmd.Parameters.AddWithValue("@PracticeCity", practice.City);
                cmd.Parameters.AddWithValue("@PracticeState", practice.State);
                cmd.Parameters.AddWithValue("@PracticeZip", practice.Zip);
                cmd.Parameters.AddWithValue("@PracticePhone", practice.Phone);
                cmd.Parameters.AddWithValue("@PracticeOtherPhone", string.Empty);
                cmd.Parameters.AddWithValue("@PracticeFax", practice.Fax);
                cmd.Parameters.AddWithValue("@PracticeEmail", practice.Email);
                cmd.Parameters.AddWithValue("@PracticeTaxId", practice.TaxId);
                cmd.Parameters.AddWithValue("@PracticeOtherId", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime1", startTime);
                cmd.Parameters.AddWithValue("@EndTime1", endTime);
                cmd.Parameters.AddWithValue("@StartTime2", startTime);
                cmd.Parameters.AddWithValue("@EndTime2", endTime);
                cmd.Parameters.AddWithValue("@StartTime3", startTime);
                cmd.Parameters.AddWithValue("@EndTime3", endTime);
                cmd.Parameters.AddWithValue("@StartTime4", startTime);
                cmd.Parameters.AddWithValue("@EndTime4", endTime);
                cmd.Parameters.AddWithValue("@StartTime5", startTime);
                cmd.Parameters.AddWithValue("@EndTime5", endTime);
                cmd.Parameters.AddWithValue("@StartTime6", startTime);
                cmd.Parameters.AddWithValue("@EndTime6", endTime);
                cmd.Parameters.AddWithValue("@StartTime7", startTime);
                cmd.Parameters.AddWithValue("@EndTime7", endTime);
                cmd.Parameters.AddWithValue("@Vacation", string.Empty);
                cmd.Parameters.AddWithValue("@LoginType", "1");
                cmd.Parameters.AddWithValue("@LocationReference", practice.LocationReference);
                cmd.Parameters.AddWithValue("@BillingOffice", practice.BillingOffice);
                cmd.Parameters.AddWithValue("@CatArray1", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray2", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray3", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray4", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray5", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray6", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray7", string.Empty);
                cmd.Parameters.AddWithValue("@CatArray8", string.Empty);
                cmd.Parameters.AddWithValue("@PracticeNPI", practice.NPI);
                cmd.Parameters.AddWithValue("@PracticeTaxonomy", string.Empty);
                cmd.Parameters.AddWithValue("@HexColor", "FFFFFF");

                cmd.ExecuteNonQuery();
            }
        }

        public int CheckIfResourceExistInIO(string resourceName)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT Id FROM model.Users WHERE UserName = @UserName";
                cmd.Parameters.AddWithValue("@UserName", resourceName.Trim());
                object result = cmd.ExecuteScalar();
                return result != null ? Convert.ToInt32(result) : -1;
            }
        }

        public string GetAvailablePasswordForResource()
        {
            Random random = new Random();

            bool isDuplicate = true;
            int newUniquePassword = 0;

            while (isDuplicate)
            {
                int newPassword = random.Next(1000, 9999);

                using (SqlCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT ResourcePid FROM dbo.Resources WHERE ResourcePid=@ResourcePid";
                    cmd.Parameters.AddWithValue("@ResourcePid", newPassword.ToString());
                    object result = cmd.ExecuteScalar();
                    isDuplicate = result != null ? true : false;
                }
                if (!isDuplicate)
                    newUniquePassword = newPassword;
            }
            return newUniquePassword.ToString();
        }

        public int CreateServiceLocation(ExternalSystemServiceLocationDTO serviceLocation)
        {

            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText += "INSERT INTO Resources ";
                cmd.CommandText += "(ResourceName, ResourceType, ResourceDescription, ResourcePid, ResourcePermissions, ResourceColor, ResourceOverLoad, ResourceCost, ResourceSpecialty, ";
                cmd.CommandText += "ResourceTaxId, ResourceAddress, ResourceSuite, ResourceCity, ResourceState, ResourceZip, ResourcePhone, ResourceEmail, ServiceCode, ResourceSSN, ";
                cmd.CommandText += "ResourceDEA, ResourceLicence, ResourceScheduleTemplate2, ResourceUPIN, ResourceScheduleTemplate1, ResourceScheduleTemplate3, PracticeId, StartTime1, ";
                cmd.CommandText += "EndTime1, StartTime2, EndTime2, StartTime3, EndTime3, StartTime4, EndTime4, StartTime5, EndTime5, StartTime6, EndTime6, StartTime7, EndTime7, Vacation, ";
                cmd.CommandText += "Status, ResourceLastName, ResourceFirstName, SignatureFile, PlaceOfService, Billable, GoDirect, NPI, ResourceMI, ResourceSuffix, ResourcePrefix, IsLoggedIn, ";
                cmd.CommandText += "FileServer, HexColor, OrdinalId) ";
                cmd.CommandText += "VALUES (@ResourceName,@ResourceType,@ResourceDescription,@ResourcePid,@ResourcePermissions,@ResourceColor,@ResourceOverLoad,@ResourceCost,@ResourceSpecialty, ";
                cmd.CommandText += "@ResourceTaxId,@ResourceAddress,@ResourceSuite,@ResourceCity,@ResourceState,@ResourceZip,@ResourcePhone,@ResourceEmail,@ServiceCode,@ResourceSSN,@ResourceDEA, ";
                cmd.CommandText += "@ResourceLicence,@ResourceScheduleTemplate2,@ResourceUPIN,@ResourceScheduleTemplate1,@ResourceScheduleTemplate3,@PracticeId,@StartTime1, ";
                cmd.CommandText += "@EndTime1,@StartTime2,@EndTime2,@StartTime3,@EndTime3,@StartTime4,@EndTime4,@StartTime5,@EndTime5,@StartTime6,@EndTime6,@StartTime7,@EndTime7, ";
                cmd.CommandText += "@Vacation,@Status,@ResourceLastName,@ResourceFirstName,@SignatureFile,@PlaceOfService,@Billable,@GoDirect,@NPI,@ResourceMI,@ResourceSuffix, ";
                cmd.CommandText += "@ResourcePrefix,@IsLoggedIn,@FileServer,@HexColor,@OrdinalId)";
                cmd.CommandText += "SELECT CAST(scope_identity() AS int)";

                cmd.Parameters.AddWithValue("@ResourceName", serviceLocation.Name.Truncate(16));
                cmd.Parameters.AddWithValue("@ResourceType", "R");
                cmd.Parameters.AddWithValue("@ResourceDescription", serviceLocation.Name);
                cmd.Parameters.AddWithValue("@ResourcePid", string.Empty);
                cmd.Parameters.AddWithValue("@ResourcePermissions", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceColor", 0);
                cmd.Parameters.AddWithValue("@ResourceOverLoad", 0);
                cmd.Parameters.AddWithValue("@ResourceCost", 0);
                cmd.Parameters.AddWithValue("@ResourceSpecialty", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceTaxId", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceAddress", string.Format("{0}, {1}", serviceLocation.AddressLine1, serviceLocation.AddressLine2).Truncate(35));
                cmd.Parameters.AddWithValue("@ResourceSuite", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceCity", serviceLocation.City);
                cmd.Parameters.AddWithValue("@ResourceState", serviceLocation.State);
                cmd.Parameters.AddWithValue("@ResourceZip", serviceLocation.ZipCode.Truncate(10));
                cmd.Parameters.AddWithValue("@ResourcePhone", serviceLocation.Phone);
                cmd.Parameters.AddWithValue("@ResourceEmail", string.Empty);
                cmd.Parameters.AddWithValue("@ServiceCode", "02"); // 01- Office, 02- Other, 03- Other Office
                cmd.Parameters.AddWithValue("@ResourceSSN", DBNull.Value);
                cmd.Parameters.AddWithValue("@ResourceDEA", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceLicence", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceScheduleTemplate2", 0);
                cmd.Parameters.AddWithValue("@ResourceUPIN", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceScheduleTemplate1", 0);
                cmd.Parameters.AddWithValue("@ResourceScheduleTemplate3", 0);
                cmd.Parameters.AddWithValue("@PracticeId", serviceLocation.IO_PracticeId);
                cmd.Parameters.AddWithValue("@StartTime1", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime1", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime2", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime2", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime3", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime3", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime4", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime4", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime5", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime5", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime6", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime6", string.Empty);
                cmd.Parameters.AddWithValue("@StartTime7", string.Empty);
                cmd.Parameters.AddWithValue("@EndTime7", string.Empty);
                cmd.Parameters.AddWithValue("@Vacation", string.Empty);
                cmd.Parameters.AddWithValue("@Status", "A");
                cmd.Parameters.AddWithValue("@ResourceLastName", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceFirstName", string.Empty);
                cmd.Parameters.AddWithValue("@SignatureFile", string.Empty);
                cmd.Parameters.AddWithValue("@PlaceOfService", 11);
                cmd.Parameters.AddWithValue("@Billable", "N"); // 'N' for Service Location Resources
                cmd.Parameters.AddWithValue("@GoDirect", "N");//  'N' for Service Location Resources
                cmd.Parameters.AddWithValue("@NPI", serviceLocation.NPI.Truncate(10));
                cmd.Parameters.AddWithValue("@ResourceMI", string.Empty);
                cmd.Parameters.AddWithValue("@ResourceSuffix", string.Empty);
                cmd.Parameters.AddWithValue("@ResourcePrefix", string.Empty);
                cmd.Parameters.AddWithValue("@IsLoggedIn", 0);
                cmd.Parameters.AddWithValue("@FileServer", DBNull.Value);
                cmd.Parameters.AddWithValue("@HexColor", "FFFFFF");
                cmd.Parameters.AddWithValue("@OrdinalId", 1);

                int newResourceId = (int)cmd.ExecuteScalar();

                // Note: refer OnServiceLocationInsert trigger for this
                // Id for Service Locations is maintained like this in view for 'R' type resources
                int newServiceLocationId = 110000000 + newResourceId;
                return newServiceLocationId;
            }
        }

        public void SetApplicationConfigurationSetting()
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "Update dbo.practiceinterfaceconfiguration SET FieldValue='F' WHERE FieldReference IN('MUCHECKIN','MUON')";
                cmd.ExecuteNonQuery();
            }
        }

        public List<int> GetInvoiceIdListWhoseEncounterIsNotCreatedYet()
        {
            List<int> invoiceList = new List<int>();

            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT Inv.Id as InvoiceId from dbo.ExternalSystemAppointmentMapping AS ApptMapping ";
                cmd.CommandText += "JOIN model.Invoices as Inv on ApptMapping.IOAppointmentId = Inv.EncounterId ";
                cmd.CommandText += "JOIN model.Encounters as enc ON  enc.Id = Inv.EncounterId ";
                cmd.CommandText += "LEFT JOIN  dbo.ExternalSystemInvoiceMapping AS InvMapping on Inv.Id = InvMapping.IOInvoiceId ";
                cmd.CommandText += "WHERE InvMapping.IOInvoiceId IS NULL ";

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        invoiceList.Add(Convert.ToInt32(item["InvoiceId"]));
                    }
                }

            }
            return invoiceList;
        }

        public InvoiceDTO GetInvoiceDetail(int invoiceId)
        {
            InvoiceDTO invoice = new InvoiceDTO();

            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText += "SELECT inv.Id as InvoiceId, patientMapping.IOPatientId, patientMapping.ExternalSystemPatientId, apptMapping.IOAppointmentId, ";
                cmd.CommandText += "apptMapping.ExternalSystemAppointmentId ,providerMapping.IOResourceId, providerMapping.ExternalSystemProviderId, ";
                cmd.CommandText += "locationMapping.IOServiceLocationId, locationMapping.ExternalSystemServiceLocationId , practiceMapping.IOPracticeId, practiceMapping.ExternalSystemPracticeId ";
                cmd.CommandText += "FROM model.Invoices as inv ";
                cmd.CommandText += "JOIN model.Encounters as enc on inv.EncounterId  = enc.Id ";
                cmd.CommandText += "JOIN model.Appointments as appt on  enc.Id = appt.Id ";
                cmd.CommandText += "JOIN dbo.ExternalSystemPatientMapping as patientMapping on enc.PatientId = patientMapping.IOPatientId ";
                cmd.CommandText += "JOIN dbo.ExternalSystemAppointmentMapping as apptMapping on enc.Id = apptMapping.IOAppointmentId ";
                cmd.CommandText += "JOIN dbo.ExternalSystemProviderMapping as providerMapping on appt.UserId = providerMapping.IOResourceId ";
                cmd.CommandText += "JOIN dbo.ExternalSystemServiceLocationMapping as locationMapping on enc.ServiceLocationId = locationMapping.IOServiceLocationId ";
                cmd.CommandText += "JOIN dbo.Resources as res on res.ResourceId = appt.UserId ";
                cmd.CommandText += "JOIN dbo.ExternalSystemPracticeMapping as practiceMapping on res.PracticeId =  practiceMapping.IOPracticeId ";
                cmd.CommandText += "WHERE inv.Id = @InvoiceId";
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dsInvoiceInfo = new DataSet();
                da.Fill(dsInvoiceInfo);

                if (dsInvoiceInfo.Tables[0].Rows.Count > 0)
                {
                    var dataRow = dsInvoiceInfo.Tables[0].Rows[0];
                    invoice.InvoiceId = Convert.ToInt32(dataRow["InvoiceId"]);
                    invoice.PatientId = Convert.ToInt32(dataRow["IOPatientId"]);
                    invoice.AppointmentId = Convert.ToInt32(dataRow["IOAppointmentId"]);
                    invoice.ResourceId = Convert.ToInt32(dataRow["IOResourceId"]);
                    invoice.ServiceLocationId = Convert.ToInt32(dataRow["IOServiceLocationId"]);
                    invoice.PracticeId = Convert.ToInt32(dataRow["IOPracticeId"]);
                    invoice.ExternalSystemPatientId = Convert.ToInt32(dataRow["ExternalSystemPatientId"]);
                    invoice.ExternalSystemAppointmentId = Convert.ToInt32(dataRow["ExternalSystemAppointmentId"]);
                    invoice.ExternalSystemProviderId = Convert.ToInt32(dataRow["ExternalSystemProviderId"]);
                    invoice.ExternalSystemServiceLocationId = Convert.ToInt32(dataRow["ExternalSystemServiceLocationId"]);
                    invoice.ExternalSystemPracticeId = Convert.ToInt32(dataRow["ExternalSystemPracticeId"]);
                }
            }
            if (invoice.InvoiceId > 0)
            {
                bool isICD10 = HasInvoiceICD10Linking(invoice.InvoiceId);
                invoice.Mode = isICD10 ? ICDMode.ICD10 : ICDMode.ICD9;

                invoice.BillingServices = GetBillingServiceList(invoiceId, invoice.Mode);
            }

            return invoice.InvoiceId > 0 ? invoice : null;
        }

        private List<BillingServiceDTO> GetBillingServiceList(int invoiceId, ICDMode mode)
        {
            DataSet dsBillingServices = new DataSet();
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT i.Id as InvoiceId,bs.Id as BillingServiceId, es.Code, bs.PatientBillDate,  es.Description, bs.Unit, bs.UnitCharge, bs.UnitAllowableExpense ";
                cmd.CommandText += "FROM model.Invoices as i ";
                cmd.CommandText += "INNER JOIN model.BillingServices AS bs ON i.Id= bs.InvoiceId ";
                cmd.CommandText += "INNER JOIN  model.EncounterServices as es on bs.EncounterServiceId = es.Id ";
                cmd.CommandText += "WHERE i.Id = @InvoiceId";
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                da1.Fill(dsBillingServices);
            }

            List<BillingServiceDTO> billingServiceList = new List<BillingServiceDTO>();

            if (dsBillingServices != null && dsBillingServices.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in dsBillingServices.Tables[0].Rows)
                {
                    BillingServiceDTO billingService = new BillingServiceDTO
                    {
                        BillingServiceId = Convert.ToInt32(item["BillingServiceId"]),
                        DateOfService = item["PatientBillDate"] != DBNull.Value ? Convert.ToDateTime(item["PatientBillDate"]) : (DateTime?)null,
                        ProcedureCode = Convert.ToString(item["Code"]),
                        ServiceComment = item["Description"].ToString(),
                        Units = item["Unit"] != DBNull.Value ? Convert.ToDecimal(item["Unit"]) : 1,
                        UnitCharge = item["UnitCharge"] != DBNull.Value ? Convert.ToDecimal(item["UnitCharge"]) : 0,
                    };
                    billingService.TotalAllowed = billingService.Units * billingService.UnitCharge;

                    List<string> diagList = GetBillingDiagnosisList(billingService.BillingServiceId, mode);
                    List<string> modifierList = GetServiceModifierList(billingService.BillingServiceId);

                    int diagCount = 1;
                    foreach (var diagnosis in diagList)
                    {
                        if (diagCount <= 4)
                        {
                            if (diagCount == 1)
                                billingService.DiagnosisCode1 = diagnosis;
                            else if (diagCount == 2)
                                billingService.DiagnosisCode2 = diagnosis;
                            else if (diagCount == 3)
                                billingService.DiagnosisCode3 = diagnosis;
                            else if (diagCount == 4)
                                billingService.DiagnosisCode4 = diagnosis;
                            diagCount++;
                        }
                    }

                    int modifierCount = 1;
                    foreach (var modifier in modifierList)
                    {
                        if (modifierCount <= 4)
                        {
                            if (modifierCount == 1)
                                billingService.Modifier1 = modifier;
                            else if (modifierCount == 2)
                                billingService.Modifier2 = modifier;
                            else if (modifierCount == 3)
                                billingService.Modifier3 = modifier;
                            else if (modifierCount == 4)
                                billingService.Modifier4 = modifier;
                            modifierCount++;
                        }
                    }

                    billingServiceList.Add(billingService);
                }
            }
            return billingServiceList;
        }

        private List<string> GetServiceModifierList(int billingServiceId)
        {
            DataSet dsServiceModifiers = new DataSet();
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT sm.Code FROM [model].[BillingServiceModifiers] AS bsm ";
                cmd.CommandText += "JOIN [model].[ServiceModifiers] AS sm  ON bsm.ServiceModifierId = sm.Id WHERE bsm.BillingServiceId= @BillingServiceId";

                cmd.Parameters.AddWithValue("@BillingServiceId", billingServiceId);

                SqlDataAdapter da3 = new SqlDataAdapter(cmd);
                da3.Fill(dsServiceModifiers);
            }

            List<string> serviceModifierList = new List<string>();

            if (dsServiceModifiers != null & dsServiceModifiers.Tables.Count > 0)
            {
                foreach (DataRow item in dsServiceModifiers.Tables[0].Rows)
                {
                    serviceModifierList.Add(Convert.ToString(item["Code"]));
                }
            }
            return serviceModifierList;
        }

        private List<string> GetBillingDiagnosisList(int billingServiceId, ICDMode mode)
        {
            DataSet dsBillingDiagnosis = new DataSet();

            if (mode == ICDMode.ICD9)
            {
                using (SqlCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT ex.ExternalSystemEntityKey as DiagnosisCode FROM  model.BillingServiceBillingDiagnosis as bb ";
                    cmd.CommandText += "JOIN model.BillingDiagnosis b on bb.BillingDiagnosisId = b.Id ";
                    cmd.CommandText += "JOIN  model.ExternalSystemEntityMappings as ex on b.ExternalSystemEntityMappingId = ex.Id ";
                    cmd.CommandText += "WHERE bb.BillingServiceId= @BillingServiceId";
                    cmd.Parameters.AddWithValue("@BillingServiceId", billingServiceId);

                    SqlDataAdapter da2 = new SqlDataAdapter(cmd);
                    da2.Fill(dsBillingDiagnosis);
                }
            }
            else
            {
                using (SqlCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT bd.DiagnosisCode AS DiagnosisCode FROM model.BillingServiceBillingDiagnosis AS bsbd ";
                    cmd.CommandText += "JOIN model.BillingDiagnosisIcd10 AS bd ON bsbd.BillingDiagnosisId = bd.Id ";
                    cmd.CommandText += "WHERE bsbd.BillingServiceId=@BillingServiceId ";
                    cmd.Parameters.AddWithValue("@BillingServiceId", billingServiceId);

                    SqlDataAdapter da2 = new SqlDataAdapter(cmd);
                    da2.Fill(dsBillingDiagnosis);
                }
            }

            List<string> billingDiagnosisList = new List<string>();

            if (dsBillingDiagnosis != null & dsBillingDiagnosis.Tables.Count > 0)
            {
                foreach (DataRow item in dsBillingDiagnosis.Tables[0].Rows)
                {
                    billingDiagnosisList.Add(Convert.ToString(item["DiagnosisCode"]));
                }
            }
            return billingDiagnosisList;
        }

        public void UpdateAppointment(ExternalSystemAppointmentDTO externalAppointment)
        {
            EncounterDTO encounter = GetEncounter(externalAppointment.IO_AppointmentId);
            if (encounter != null)
            {
                int encounterStatusId = encounter.EncounterStatusId;
                List<int> cancelStatusList = new List<int>();

                // in EncounterStatus_Enumeration Table 
                // all these values results in Appointment Cancellation
                for (int i = 8; i <= 14; i++)
                {
                    cancelStatusList.Add(i);
                }

                bool isCancelled = cancelStatusList.IndexOf(encounterStatusId) > -1 ? true : false;

                if (isCancelled == false && externalAppointment.ConfirmationStatus == "Cancelled")
                {
                    // Case: appointment is cancelled in kareo but not in IO

                    // cancel appointment in IO as well
                    using (SqlCommand cmd = _connection.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE model.Encounters SET EncounterStatusId = @EncounterStatusId WHERE (Id = @Id)";
                        cmd.Parameters.AddWithValue("@EncounterStatusId", 14); // 14 for Cancel Appointment
                        cmd.Parameters.AddWithValue("@Id", externalAppointment.IO_AppointmentId);
                        cmd.ExecuteNonQuery();
                    }
                }

                // also check for apointment date
                if (externalAppointment.StartDate != encounter.StartDateTime)
                {
                    using (SqlCommand cmd = _connection.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE model.Encounters SET StartDateTime = @StartDateTime, EndDateTime = @EndDateTime WHERE (Id = @Id)";
                        cmd.Parameters.AddWithValue("@StartDateTime", externalAppointment.StartDate);
                        cmd.Parameters.AddWithValue("@EndDateTime", externalAppointment.StartDate.AddHours(1));
                        cmd.Parameters.AddWithValue("@Id", externalAppointment.IO_AppointmentId);
                        cmd.ExecuteNonQuery();
                    }
                }

            }
        }

        public EncounterDTO GetEncounter(int encounterid)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT * from model.Encounters WHERE Id=@Encounterid";
                cmd.Parameters.AddWithValue("@Encounterid", encounterid);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                DataRow drow = ds.Tables[0].Rows[0];

                EncounterDTO enc = new EncounterDTO
                {
                    EncounterStatusId = Convert.ToInt32(drow["EncounterStatusId"]),
                    EncounterTypeId = Convert.ToInt32(drow["EncounterTypeId"]),
                    EndDateTime = Convert.ToDateTime(drow["EndDateTime"]),
                    EncounterId = Convert.ToInt32(drow["Id"]),
                    PatientId = Convert.ToInt32(drow["PatientId"]),
                    ServiceLocationId = Convert.ToInt32(drow["ServiceLocationId"]),
                    StartDateTime = Convert.ToDateTime(drow["StartDateTime"])
                };

                return enc;
            }
        }

        private bool HasInvoiceICD10Linking(int invoiceId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT COUNT(*) FROM model.BillingDiagnosisIcd10 WHERE InvoiceId=@InvoiceId";
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);
                int count = (int)cmd.ExecuteScalar();
                return count > 0;
            }
        }

        public bool IsConnected(out string errorMessage)
        {
            errorMessage = string.Empty;
            bool isConnected = false;
            try
            {
                using (SqlCommand cmd = _connection.CreateCommand())
                {
                    cmd.CommandText += "SELECT * FROM model.Users";
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    isConnected = (ds.Tables[0].Rows.Count > 0) ? true : false;
                }
            }
            catch (Exception ex)
            {
                isConnected = false;
                errorMessage = ex.Message;
            }
            return isConnected;
        }


        public void AddNecessaryPermissionsToProvider(int userId)
        {
            // all IO permissions, these IDs can be found in model.Permissions
            List<int> ioPermissions = GetAllPermissions();

            foreach (var permissionId in ioPermissions)
            {
                bool isDenied;
                bool exist = CheckIfPermissionExist(userId, permissionId, out isDenied);

                if (exist)
                {
                    if (isDenied)
                    {
                        // assign again if permission is denied
                        using (SqlCommand cmd = _connection.CreateCommand())
                        {
                            cmd.CommandText = "UPDATE model.UserPermissions SET IsDenied=@IsDenied WHERE UserId=@UserId AND PermissionId=@PermissionId";
                            cmd.Parameters.AddWithValue("@IsDenied", 0);
                            cmd.Parameters.AddWithValue("@UserId", userId);
                            cmd.Parameters.AddWithValue("@PermissionId", permissionId);
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    // if not exist, assign permission
                    using (SqlCommand cmd = _connection.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO model.UserPermissions (IsDenied,UserId,PermissionId) VALUES (@IsDenied,@UserId,@PermissionId)";
                        cmd.Parameters.AddWithValue("@IsDenied", 0);
                        cmd.Parameters.AddWithValue("@UserId", userId);
                        cmd.Parameters.AddWithValue("@PermissionId", permissionId);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        private bool CheckIfPermissionExist(int userId, int permissionId, out bool isDenied)
        {
            bool exist;
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT TOP 1 * FROM model.UserPermissions WHERE UserId=@UserId AND PermissionId=@PermissionId";
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@PermissionId", permissionId);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    isDenied = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsDenied"]);
                    exist = true;
                }
                else
                {
                    isDenied = true;
                    exist = false;
                }
            }
            return exist;
        }

        private List<int> GetAllPermissions()
        {
            List<int> permissionList = new List<int>();
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "select * from model.permissions";

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        permissionList.Add(Convert.ToInt32(item["Id"]));
                    }
                }
            }
            return permissionList;
        }

        public void CreateRequiredTables()
        {
            try
            {
                string path = string.Empty;

#if DEBUG
                DirectoryInfo dir = new DirectoryInfo(Directory.GetCurrentDirectory());
                path = dir.Parent.Parent.Parent.FullName + "\\IO.Practiceware.Integration.Main\\Scripts\\MappingTables.sql";
#else
                path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\MappingTables.sql";

#endif
                if (File.Exists(path))
                {
                    string script = File.ReadAllText(path);
                    using (SqlCommand cmd = _connection.CreateCommand())
                    {
                        cmd.CommandText = script;
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    throw new Exception("File does not exist at following location :" + path);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

