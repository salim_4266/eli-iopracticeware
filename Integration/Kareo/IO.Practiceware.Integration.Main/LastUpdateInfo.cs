﻿using IO.Practiceware.Integration.Domain;
using IO.Practiceware.Integration.KareoServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Main
{
    /// <summary>
    /// Singleton Class that contains method to check wheather Practices, ServiceLocations and Providers should be synced before 24 hours or not
    /// </summary>
    public class LastUpdateInfo
    {
        private static LastUpdateInfo instance;
        private static LastUpdateInfo lastUpdateInfo;

        private DateTime? lastSyncTimeForPractices { get; set; }
        private List<ExternalSystemPracticeDTO> cachedExternalPractices { get; set; }
        private List<AppointmentTypeDTO> cachedExternalSystemAppointmentTypes { get; set; }

        private List<PracticeLastSyncDetail> cachedPracticeDetailList { get; set; }

        private LastUpdateInfo(List<int> practiceIds)
        {
            cachedPracticeDetailList = new List<PracticeLastSyncDetail>();
            foreach (var practiceId in practiceIds)
            {
                cachedPracticeDetailList.Add(new PracticeLastSyncDetail
                {
                    ExternalSystemPracticeId = practiceId,
                    LastSyncTime = null,
                    ExternalSystemProviderList = null,
                    ExternalSystemServiceLocationList = null
                });
            }
        }

        public static LastUpdateInfo Instance
        {
            get
            {
                if (instance == null)
                {
                    IKareoService kareoService = new KareoService();
                    List<int> practiceIds = kareoService.GetPractices().Select(x => x.ExternalSystemPracticeId).ToList();
                    instance = new LastUpdateInfo(practiceIds);
                }
                return instance;
            }
        }


        #region  normal 1 day sync

        /// <summary>
        /// this method tell us whether we should sync practices or use cached one
        /// this out parameter returns cached practices
        /// but only use these cached data when method return false
        /// </summary>
        /// <param name="externalSystemPracticeList"></param>
        /// <returns></returns>
        public bool CanSync(out List<ExternalSystemPracticeDTO> externalSystemPracticeList, out  List<AppointmentTypeDTO> externalSystemAppointmentTypes)
        {
            externalSystemPracticeList = cachedExternalPractices;
            externalSystemAppointmentTypes = cachedExternalSystemAppointmentTypes;
            return CheckIf24HourCompleted(lastSyncTimeForPractices);
        }

        /// <summary>
        /// this method updates sync time and practices
        /// </summary>
        /// <param name="externalSystemPracticeList"></param>
        public void SetSync(List<ExternalSystemPracticeDTO> externalSystemPracticeList, List<AppointmentTypeDTO> externalSystemAppointmentTypes)
        {
            cachedExternalPractices = externalSystemPracticeList;
            cachedExternalSystemAppointmentTypes = externalSystemAppointmentTypes;
            lastSyncTimeForPractices = DateTime.Now;
        }

        #endregion

        #region normal 1 day sync ( practice wise)

        /// <summary>
        /// this method tell us whether we should sync service locations and providers or use cached one
        /// these out parameter return cached service locations and cached providers
        /// but only use these cached data when method return false
        /// </summary>
        /// <param name="practiceId"></param>
        /// <param name="serviceLocationList"></param>
        /// <param name="providerList"></param>
        /// <returns></returns>
        public bool CanSync(int practiceId, out List<ExternalSystemServiceLocationDTO> serviceLocationList, out List<ExternalSystemProviderDTO> providerList)
        {
            // these our parameter values will be used when this fuction return false
            // in that case it will be returning cached values
            PracticeLastSyncDetail practice = cachedPracticeDetailList.First(x => x.ExternalSystemPracticeId == practiceId);
            serviceLocationList = practice.ExternalSystemServiceLocationList;
            providerList = practice.ExternalSystemProviderList;

            return CheckIf24HourCompleted(practice.LastSyncTime);
        }

        /// <summary>
        /// this method updates sync time, service locations  and providers
        /// </summary>
        /// <param name="practiceId"></param>
        /// <param name="serviceLocationList"></param>
        /// <param name="providerList"></param>
        public void SetSync(int practiceId, List<ExternalSystemServiceLocationDTO> serviceLocationList, List<ExternalSystemProviderDTO> providerList)
        {
            PracticeLastSyncDetail practice = cachedPracticeDetailList.First(x => x.ExternalSystemPracticeId == practiceId);
            practice.ExternalSystemServiceLocationList = serviceLocationList;
            practice.ExternalSystemProviderList = providerList;
            practice.LastSyncTime = DateTime.Now;
        }

        #endregion

        private int GetTimeDiffernceInHour(DateTime timeToSubtract, DateTime timeFromSubtract)
        {
            return Convert.ToInt32(timeFromSubtract.Subtract(timeToSubtract).TotalHours);
        }

        private bool CheckIf24HourCompleted(DateTime? time)
        {
            if (time.HasValue)
            {
                return GetTimeDiffernceInHour(time.Value, DateTime.Now) > 24;
            }
            return true;
        }
    }

}


