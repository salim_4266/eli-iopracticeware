﻿
IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id=OBJECT_ID(N'dbo.ExternalSystemPatientMapping') AND type = N'U')
BEGIN
	CREATE TABLE dbo.ExternalSystemPatientMapping
	(
	Id INT IDENTITY(1,1) NOT NULL,
	IOPatientId INT NOT NULL,
	ExternalSystemPatientId INT NOT NULL,
	CreatedOn DateTime NOT NULL DEFAULT GetDate()
	CONSTRAINT PK_ExternalSystemPatientMappingId PRIMARY KEY (Id)
	)
END

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id=OBJECT_ID(N'dbo.ExternalSystemPracticeMapping') AND type = N'U')
BEGIN
	CREATE TABLE dbo.ExternalSystemPracticeMapping
	(
	Id INT IDENTITY(1,1) NOT NULL,
	IOPracticeId INT NOT NULL,
	ExternalSystemPracticeId INT NOT NULL,
	CreatedOn DateTime NOT NULL DEFAULT GetDate()
	CONSTRAINT PK_ExternalSystemPracticeMapping PRIMARY KEY (Id)
	)
END

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id=OBJECT_ID(N'dbo.ExternalSystemAppointmentMapping') AND type = N'U')
BEGIN
	CREATE TABLE dbo.ExternalSystemAppointmentMapping
	(
	Id INT IDENTITY(1,1) NOT NULL,
	IOAppointmentId INT NOT NULL,
	ExternalSystemAppointmentId INT NOT NULL,
	CreatedOn DateTime NOT NULL DEFAULT GetDate()
	CONSTRAINT PK_ExternalSystemAppointmentMappingId PRIMARY KEY (Id)
	)
END

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id=OBJECT_ID(N'dbo.ExternalSystemServiceLocationMapping') AND type = N'U')
BEGIN
	CREATE TABLE dbo.ExternalSystemServiceLocationMapping
	(
	Id INT IDENTITY(1,1) NOT NULL,
	IOServiceLocationId INT NOT NULL,
	ExternalSystemServiceLocationId INT NOT NULL,
	CreatedOn DateTime NOT NULL DEFAULT GetDate()
	CONSTRAINT PK_ExternalSystemServiceLocationMappingId PRIMARY KEY (Id)
	)
END

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id=OBJECT_ID(N'dbo.ExternalSystemProviderMapping') AND type = N'U')
BEGIN
	CREATE TABLE dbo.ExternalSystemProviderMapping
	(
	Id INT IDENTITY(1,1) NOT NULL,
	IOResourceId INT NOT NULL,
	ExternalSystemProviderId INT NOT NULL,
	CreatedOn DateTime NOT NULL DEFAULT GetDate()
	CONSTRAINT PK_ExternalSystemProviderMappingId PRIMARY KEY (Id)
	)
END

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id=OBJECT_ID(N'dbo.ExternalSystemInvoiceMapping') AND type = N'U')
BEGIN
	CREATE TABLE dbo.ExternalSystemInvoiceMapping
	(
	Id INT IDENTITY(1,1) NOT NULL,
	IOInvoiceId INT NOT NULL,
	ExternalSystemEncounterId INT NOT NULL,
	CreatedOn DateTime NOT NULL DEFAULT GetDate()
	CONSTRAINT PK_ExternalSystemInvoiceMappingId PRIMARY KEY (Id)
	)
END