﻿using IO.Practiceware.Integration.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace IO.Practiceware.Integration.Main
{
    public interface IIOKareoMapping
    {

        /// <summary>
        /// returns -1 if not exist else return respective IO PatientId
        /// </summary>
        /// <param name="kareoPatientId"></param>
        /// <returns></returns>
        int CheckIfPatientExistInIO(int externalSystemPatientId);
        void CreatePatientMapping(int ioPatientId, int externalSystemPatientId);

        /// <summary>
        /// returns -1 if not exist else return respective IO PracticeId
        /// </summary>
        /// <param name="kareoPracticeId"></param>
        /// <returns></returns>
        int CheckIfPracticeExistInIO(int externalSystemPracticeId);
        void CreatePracticeMapping(int ioPracticeId, int externalSystemPracticeId);

        /// <summary>
        /// returns -1 if not exist else return respective IO AppointmentId
        /// </summary>
        /// <param name="externalSystemAppointmentId"></param>
        /// <returns></returns>
        int CheckIfAppointmentExistInIO(int externalSystemAppointmentId);
        void CreateAppointmentMapping(int ioAppointmentId, int externalSystemAppointmentId);

        int CheckIfServiceLocationExistInIO(int externalSystemServiceLocationId);
        void CreateServiceLocationMapping(int ioPracticeId, int externalSystemServiceLocationId);

        int CheckIfProviderExistInIO(int externalSystemProviderId);
        void CreateProviderMapping(int ioResourceId, int externalSystemProviderId);

        int CheckIfInvoiceExistInIO(int externalSystemEncounterId);
        void CreateInvoiceMapping(int ioInvoiceId, int externalSystemEncounterId);
    }

    public class IOKareoMapping : IIOKareoMapping
    {
        private SqlConnection _connection;

        /// <summary>
        ///  you must have to pass a sql connection which is in open mode to access functions of this class
        /// </summary>
        /// <param name="connection"></param>
        public IOKareoMapping(SqlConnection connection)
        {
            _connection = connection;
        }


        #region Patient Mapping

        public void CreatePatientMapping(int ioPatientId, int externalSystemPatientId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO dbo.ExternalSystemPatientMapping (IOPatientId,ExternalSystemPatientId) Values (@IOPatientId,@ExternalSystemPatientId)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@IOPatientId", ioPatientId);
                cmd.Parameters.AddWithValue("@ExternalSystemPatientId", externalSystemPatientId);
                cmd.ExecuteNonQuery();
            }
        }


        public int CheckIfPatientExistInIO(int externalSystemPatientId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT IOPatientId FROM dbo.ExternalSystemPatientMapping WHERE ExternalSystemPatientId=@ExternalSystemPatientId";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ExternalSystemPatientId", externalSystemPatientId);
                object result = cmd.ExecuteScalar();
                return result != null ? Convert.ToInt32(result) : -1;
            }
        }

        #endregion

        #region Practice Mapping

        public int CheckIfPracticeExistInIO(int externalSystemPracticeId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT IOPracticeId FROM dbo.ExternalSystemPracticeMapping WHERE ExternalSystemPracticeId=@ExternalSystemPracticeId";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ExternalSystemPracticeId", externalSystemPracticeId);
                object result = cmd.ExecuteScalar();
                return result != null ? Convert.ToInt32(result) : -1;
            }
        }

        public void CreatePracticeMapping(int ioPracticeId, int externalSystemPracticeId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO dbo.ExternalSystemPracticeMapping (IOPracticeId,ExternalSystemPracticeId) Values (@IOPracticeId,@ExternalSystemPracticeId)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@IOPracticeId", ioPracticeId);
                cmd.Parameters.AddWithValue("@ExternalSystemPracticeId", externalSystemPracticeId);
                cmd.ExecuteNonQuery();
            }
        }

        #endregion

        #region Appointment Mapping

        public int CheckIfAppointmentExistInIO(int externalSystemAppointmentId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT IOAppointmentId FROM dbo.ExternalSystemAppointmentMapping WHERE ExternalSystemAppointmentId=@ExternalSystemAppointmentId";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ExternalSystemAppointmentId", externalSystemAppointmentId);
                object result = cmd.ExecuteScalar();
                return result != null ? Convert.ToInt32(result) : -1;
            }
        }

        public void CreateAppointmentMapping(int ioAppointmentId, int externalSystemAppointmentId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO dbo.ExternalSystemAppointmentMapping (IOAppointmentId,ExternalSystemAppointmentId) Values (@IOAppointmentId,@ExternalSystemAppointmentId)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@IOAppointmentId", ioAppointmentId);
                cmd.Parameters.AddWithValue("@ExternalSystemAppointmentId", externalSystemAppointmentId);
                cmd.ExecuteNonQuery();
            }
        }

        #endregion

        #region Service Location Mapping

        public int CheckIfServiceLocationExistInIO(int externalSystemServiceLocationId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT IOServiceLocationId FROM dbo.ExternalSystemServiceLocationMapping WHERE ExternalSystemServiceLocationId=@ExternalSystemServiceLocationId";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ExternalSystemServiceLocationId", externalSystemServiceLocationId);
                object result = cmd.ExecuteScalar();
                return result != null ? Convert.ToInt32(result) : -1;
            }
        }

        public void CreateServiceLocationMapping(int ioServiceLocationId, int externalSystemServiceLocationId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO dbo.ExternalSystemServiceLocationMapping (IOServiceLocationId,ExternalSystemServiceLocationId) Values (@IOServiceLocationId,@ExternalSystemServiceLocationId)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@IOServiceLocationId", ioServiceLocationId);
                cmd.Parameters.AddWithValue("@ExternalSystemServiceLocationId", externalSystemServiceLocationId);
                cmd.ExecuteNonQuery();
            }
        }

        #endregion

        #region Provider Mapping

        public int CheckIfProviderExistInIO(int externalSystemProviderId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT IOResourceId FROM dbo.ExternalSystemProviderMapping WHERE ExternalSystemProviderId=@ExternalSystemProviderId";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ExternalSystemProviderId", externalSystemProviderId);
                object result = cmd.ExecuteScalar();
                return result != null ? Convert.ToInt32(result) : -1;
            }
        }

        public void CreateProviderMapping(int ioResourceId, int externalSystemProviderId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO dbo.ExternalSystemProviderMapping (IOResourceId,ExternalSystemProviderId) Values (@IOResourceId,@ExternalSystemProviderId)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@IOResourceId", ioResourceId);
                cmd.Parameters.AddWithValue("@ExternalSystemProviderId", externalSystemProviderId);
                cmd.ExecuteNonQuery();
            }
        }

        #endregion


        #region Invoice Mapping

        public int CheckIfInvoiceExistInIO(int externalSystemEncounterId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "SELECT IOInvoiceId FROM dbo.ExternalSystemInvoiceMapping WHERE ExternalSystemEncounterId=@ExternalSystemEncounterId";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ExternalSystemEncounterId", externalSystemEncounterId);
                object result = cmd.ExecuteScalar();
                return result != null ? Convert.ToInt32(result) : -1;
            }
        }

        public void CreateInvoiceMapping(int ioInvoiceId, int externalSystemEncounterId)
        {
            using (SqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO dbo.ExternalSystemInvoiceMapping (IOInvoiceId,ExternalSystemEncounterId) Values (@IOInvoiceId,@ExternalSystemEncounterId)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@IOInvoiceId", ioInvoiceId);
                cmd.Parameters.AddWithValue("@ExternalSystemEncounterId", externalSystemEncounterId);
                cmd.ExecuteNonQuery();
            }
        } 

        #endregion
    }
}
