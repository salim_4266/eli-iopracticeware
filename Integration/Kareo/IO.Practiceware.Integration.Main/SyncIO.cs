﻿using IO.Practiceware.Integration.KareoServices;
using IO.Practiceware.Integration.Services;
using IO.Practiceware.Integration.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Transactions;
using IO.Practiceware.Integration.Domain;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace IO.Practiceware.Integration.Main
{
    public interface ISyncIO
    {
        /// <summary>
        /// Start synchronization of Kareo Appointments with IO
        /// </summary>
        /// <param name="appointmentStartDate"></param>
        /// <param name="appointmentEndDate"></param>
        void SyncAppointments(DateTime appointmentStartDate, DateTime appointmentEndDate);

        /// <summary>
        /// starts synchronization of IO Invoices with Kareo Encounters
        /// </summary>
        void SyncInvoices();

        /// <summary>
        /// checks connections to both IO and Kareo servers
        /// </summary>
        /// <returns></returns>
        bool CheckConnection();

        /// <summary>
        /// creates required mapping tables
        /// </summary>
        /// <returns></returns>
        void ConfigureMappingTables();
    }

    public class SyncIO : ISyncIO
    {
        private IKareoService _kareoService;
        private IIOService _ioService;
        private IIOKareoMapping _ioKareoMapper;
        private SqlConnection _connection;

        string testPracticeName = "Test";
        bool isTestMode = ConfigHelper.TestMode;

        public SyncIO(SqlConnection connection)
        {
            _connection = connection;

            _kareoService = new KareoService();
            _ioService = new IOService(_connection);
            _ioKareoMapper = new IOKareoMapping(_connection);
        }

        #region Appointment Syncing Section

        public void SyncAppointments(DateTime appointmentStartDate, DateTime appointmentEndDate)
        {
            List<string> invalidProviders = new List<string>();

            List<ExternalSystemPracticeDTO> externalSystemPracticeList;
            List<AppointmentTypeDTO> externalSystemAppointmentTypes;

            // if CanSync return false then  out parameter will contain cached practice list
            if (LastUpdateInfo.Instance.CanSync(out externalSystemPracticeList, out externalSystemAppointmentTypes))
            {
                // sync practices
                EventLogger.Instance.WriteLog("Practice Sync Started.", EventLogEntryType.Information);
                externalSystemPracticeList = SyncPractices();
                EventLogger.Instance.WriteLog("Practice Sync Completed.", EventLogEntryType.Information);

                // sync appointment types
                EventLogger.Instance.WriteLog("Appointment Types Sync Started.", EventLogEntryType.Information);
                externalSystemAppointmentTypes = SyncAppointmentTypes();
                EventLogger.Instance.WriteLog("Appointment Types Sync Completed.", EventLogEntryType.Information);

                // updates practices
                LastUpdateInfo.Instance.SetSync(externalSystemPracticeList, externalSystemAppointmentTypes);
            }

            externalSystemPracticeList.ForEach(practice =>
            {
                List<ExternalSystemServiceLocationDTO> externalSystemServiceLocationList;
                List<ExternalSystemProviderDTO> externalSystemProviderList;


                if (LastUpdateInfo.Instance.CanSync(practice.ExternalSystemPracticeId, out externalSystemServiceLocationList, out externalSystemProviderList))
                {
                    EventLogger.Instance.WriteLog("Service Location Sync Started.\nIO PracticeId =" + practice.IO_PracticeId + "\nKareo PracticeId =" + practice.ExternalSystemPracticeId + "\nKareo Practice Name =" + practice.PracticeName, EventLogEntryType.Information);
                    externalSystemServiceLocationList = SyncPracticeServiceLocations(practice.ExternalSystemPracticeId, practice.IO_PracticeId);
                    EventLogger.Instance.WriteLog("Service Location Sync Completed.\nIO PracticeId =" + practice.IO_PracticeId + "\nKareo PracticeId =" + practice.ExternalSystemPracticeId + "\nKareo Practice Name =" + practice.PracticeName, EventLogEntryType.Information);

                    EventLogger.Instance.WriteLog("Providers Sync Started.\nIO PracticeId =" + practice.IO_PracticeId + "\nKareo PracticeId =" + practice.ExternalSystemPracticeId + "\nKareo Practice Name =" + practice.PracticeName, EventLogEntryType.Information);
                    externalSystemProviderList = SyncProviders(practice.PracticeName, practice.IO_PracticeId);
                    EventLogger.Instance.WriteLog("Providers Sync Completed.\nIO PracticeId =" + practice.IO_PracticeId + "\nKareo PracticeId =" + practice.ExternalSystemPracticeId + "\nKareo Practice Name =" + practice.PracticeName, EventLogEntryType.Information);

                    // updates service locations and providers
                    LastUpdateInfo.Instance.SetSync(practice.ExternalSystemPracticeId, externalSystemServiceLocationList, externalSystemProviderList);
                }

                string errorMessage;
                List<ExternalSystemAppointmentDTO> externalSystemAppointments = _kareoService.GetAppointments(practice.PracticeName, appointmentStartDate, appointmentEndDate, out errorMessage);
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    EventLogger.Instance.WriteLog("Could Not Load Appointments.\nError Message = " + errorMessage, EventLogEntryType.Information);
                }

                EventLogger.Instance.WriteLog("Appointment Sync Started.\nIO PracticeId =" + practice.IO_PracticeId + "\nKareo PracticeId =" + practice.ExternalSystemPracticeId + "\nKareo Practice Name =" + practice.PracticeName, EventLogEntryType.Information);

                EventLogger.Instance.WriteLog("Appointments To Sync :" + externalSystemAppointments.Count + "\nPractice Name =" + practice.PracticeName + "\nStart Date =" + appointmentEndDate.ToString() + "\nEnd Date=" + appointmentEndDate.ToString(), EventLogEntryType.Information);

                // loop through all Appointments
                foreach (var appointment in externalSystemAppointments)
                {
                    try
                    {
                        // check if patient exist in IO, if not then create
                        int ioPatientId = _ioKareoMapper.CheckIfPatientExistInIO(appointment.PatientId);
                        if (ioPatientId < 0)
                        {
                            ExternalSystemPatientDTO basicInfo = _kareoService.GetPatient(appointment.PatientId);
                            if (basicInfo != null)
                            {
                                ioPatientId = _ioService.CreatePatient(basicInfo);
                                _ioKareoMapper.CreatePatientMapping(ioPatientId, Convert.ToInt32(basicInfo.PatientId));
                            }
                        }

                        int ioResourceId = 0;
                        if (appointment.Resources.Count > 0)
                        {
                            string resourceName = appointment.Resources.First();
                            ExternalSystemProviderDTO apptProvider = externalSystemProviderList.FirstOrDefault(x => x.FullName == resourceName);
                            if (apptProvider == null && invalidProviders.FirstOrDefault(x => x == resourceName) == null)
                            {
                                apptProvider = _kareoService.GetProvider(practice.PracticeName, resourceName);
                                if (apptProvider != null)
                                {
                                    apptProvider.IO_PracticeId = practice.IO_PracticeId;
                                    apptProvider.IO_ProviderId = _ioService.CreateResource(apptProvider);
                                    _ioKareoMapper.CreateProviderMapping(apptProvider.IO_ProviderId, apptProvider.ExternalSystemProviderId);
                                }
                                else
                                {
                                    invalidProviders.Add(resourceName);
                                }
                            }

                            ioResourceId = apptProvider != null ? apptProvider.IO_ProviderId : 0;
                        }

                        int ioServiceLocationId = 0;
                        if (externalSystemServiceLocationList.Count > 0)
                        {
                            ExternalSystemServiceLocationDTO apptServiceLocation = externalSystemServiceLocationList.FirstOrDefault(x => x.Name == appointment.ServiceLocationName);
                            ioServiceLocationId = apptServiceLocation != null ? apptServiceLocation.IO_ServiceLocationId : 0;
                        }

                        int ioAppointmentTypeId = 0;
                        if (!string.IsNullOrWhiteSpace(appointment.AppointmentReason))
                        {
                            // By default, We are considering Kareo AppointmentReason as AppointmentType in IO
                            AppointmentTypeDTO appointmentType = _ioService.GetAppointmentType(appointment.AppointmentReason);
                            if (appointmentType == null)
                            {
                                appointmentType = new AppointmentTypeDTO
                                {
                                    AppointmentType = appointment.AppointmentReason
                                };
                                appointmentType.AppTypeId = _ioService.CreateAppointmentType(appointmentType);
                            }
                            ioAppointmentTypeId = appointmentType.AppTypeId;
                        }
                        else
                        {
                            // If we do not get any Appointment Reason, then we will consider Kareo AppointmentType as AppointmentType in IO
                            if (externalSystemAppointmentTypes != null)
                            {
                                AppointmentTypeDTO appointmentType = externalSystemAppointmentTypes.FirstOrDefault(x => x.AppointmentType == appointment.AppointmentType);
                                if (appointmentType != null)
                                {
                                    ioAppointmentTypeId = appointmentType.AppTypeId;
                                }
                            }
                        }

                        if (ioPatientId == 0 || ioResourceId == 0 || ioServiceLocationId == 0)
                        {
                            EventLogger.Instance.WriteLog("Appointment not created in IO for Kareo Appointment Id" + appointment.AppointmentId + ". \nIO PatientId =" + ioPatientId + "\nIO ResourceId = " + ioResourceId + "\nIO ServiceLocationId = " + ioServiceLocationId + "\nKareo Practice Name =" + practice.PracticeName, EventLogEntryType.Error);
                            continue;
                        }

                        // check if appointment exist in IO, if not then create
                        appointment.IO_AppointmentId = _ioKareoMapper.CheckIfAppointmentExistInIO(appointment.AppointmentId);
                        if (appointment.IO_AppointmentId < 0)
                        {
                            appointment.IO_PatientId = ioPatientId;
                            appointment.IO_ResourceId = ioResourceId;
                            appointment.IO_ServiceLocationId = ioServiceLocationId;
                            appointment.IO_AppointmentTypeId = ioAppointmentTypeId;
                            appointment.IO_AppointmentId = _ioService.CreateAppointment(appointment);
                            _ioKareoMapper.CreateAppointmentMapping(appointment.IO_AppointmentId, appointment.AppointmentId);
                            EventLogger.Instance.WriteLog("Appointment Created in IO.\nIO PatientId =" + ioPatientId + "\nKareo PatientId =" + appointment.PatientId + "\nIO AppointmentId =" + appointment.IO_AppointmentId + "\nKareo AppointmentId =" + appointment.AppointmentId + "\nKareo Practice Name =" + practice.PracticeName, EventLogEntryType.Information);
                        }
                        else
                        {
                            _ioService.UpdateAppointment(appointment);
                            EventLogger.Instance.WriteLog("Appointment Updated in IO.\nIO PatientId =" + ioPatientId + "\nKareo PatientId =" + appointment.PatientId + "\nIO AppointmentId =" + appointment.IO_AppointmentId + "\nKareo AppointmentId =" + appointment.AppointmentId + "\nKareo Practice Name =" + practice.PracticeName, EventLogEntryType.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogger.Instance.WriteLog("Appointment Creation In IO Failed For Kareo Appointment Id " + appointment.AppointmentId + "\nKareo Practice Name" + practice.PracticeName + "\nMessage =" + ex.Message + ".\nStack Trace" + ex.StackTrace, EventLogEntryType.Error);
                    }
                }
                EventLogger.Instance.WriteLog("Appointment Sync Completed.\nIO PracticeId =" + practice.IO_PracticeId + "\nKaero PracticeId =" + practice.ExternalSystemPracticeId + "\nKareo Practice Name =" + practice.PracticeName, EventLogEntryType.Information);

            });
        }

        private List<ExternalSystemPracticeDTO> SyncPractices()
        {
            List<ExternalSystemPracticeDTO> externalSystemPracticeList = _kareoService.GetPractices();

            List<ExternalSystemPracticeDTO> testPractices = externalSystemPracticeList.Where(x => x.PracticeName.ToLower().Contains(testPracticeName.ToLower())).ToList();
            List<ExternalSystemPracticeDTO> originalPractices = externalSystemPracticeList.Where(x => !testPractices.Contains(x)).ToList();

            if (isTestMode)
            {
                // remove original practices from list
                foreach (var item in originalPractices)
                {
                    externalSystemPracticeList.Remove(item);
                }
            }
            else
            {
                // remove test practices from list
                foreach (var item in testPractices)
                {
                    externalSystemPracticeList.Remove(item);
                }
            }

            foreach (var item in externalSystemPracticeList)
            {
                item.IO_PracticeId = _ioKareoMapper.CheckIfPracticeExistInIO(item.ExternalSystemPracticeId);
                if (item.IO_PracticeId < 0)
                {
                    // create practice and mapping in IO
                    item.IO_PracticeId = _ioService.CreatePractice(item);
                    _ioKareoMapper.CreatePracticeMapping(item.IO_PracticeId, item.ExternalSystemPracticeId);

                    EventLogger.Instance.WriteLog("Practice Created in IO.\nIO PracticeId =" + item.IO_PracticeId + "\nKareo PracticeId =" + item.ExternalSystemPracticeId + "\nPractice Name =" + item.PracticeName, EventLogEntryType.Information);

                }
            }
            return externalSystemPracticeList;
        }

        private List<AppointmentTypeDTO> SyncAppointmentTypes()
        {
            //Note: There are two types of AppointmentTypes in Kareo - Patient and Other

            // create Patient appointment type
            AppointmentTypeDTO patientApptType = _ioService.GetAppointmentType("Patient");
            if (patientApptType == null)
            {
                patientApptType = new AppointmentTypeDTO
                {
                    AppointmentType = "Patient"
                };
                patientApptType.AppTypeId = _ioService.CreateAppointmentType(patientApptType);
            }

            // create Other appointment type
            AppointmentTypeDTO otherApptType = _ioService.GetAppointmentType("Other");
            if (otherApptType == null)
            {
                otherApptType = new AppointmentTypeDTO
                {
                    AppointmentType = "Other"
                };
                otherApptType.AppTypeId = _ioService.CreateAppointmentType(otherApptType);
            }

            List<AppointmentTypeDTO> appointmentTypeList = new List<AppointmentTypeDTO>();
            appointmentTypeList.Add(patientApptType);
            appointmentTypeList.Add(otherApptType);
            return appointmentTypeList;
        }

        private List<ExternalSystemServiceLocationDTO> SyncPracticeServiceLocations(int externalSystemPracticeId, int ioPracticeId)
        {
            List<ExternalSystemServiceLocationDTO> externalSystemServiceLocations = _kareoService.GetServiceLocations(externalSystemPracticeId);
            foreach (var item in externalSystemServiceLocations)
            {
                // to avoid duplicacy in records
                if (externalSystemServiceLocations.Where(x => x.Name == item.Name).Count() > 1)
                {
                    item.Name = item.ExternalSystemServiceLocationId + "_" + item.Name;
                }

                item.IO_PracticeId = ioPracticeId;
                item.IO_ServiceLocationId = _ioKareoMapper.CheckIfServiceLocationExistInIO(item.ExternalSystemServiceLocationId);
                if (item.IO_ServiceLocationId < 0)
                {
                    // create service location and mapping in IO
                    item.IO_ServiceLocationId = _ioService.CreateServiceLocation(item);
                    _ioKareoMapper.CreateServiceLocationMapping(item.IO_ServiceLocationId, item.ExternalSystemServiceLocationId);

                    EventLogger.Instance.WriteLog("Service Locations Created in IO.\nIO ServiceLocationId =" + item.IO_ServiceLocationId + "\nKareo ServiceLocationId =" + item.ExternalSystemServiceLocationId, EventLogEntryType.Information);
                }
            }
            return externalSystemServiceLocations;
        }

        private List<ExternalSystemProviderDTO> SyncProviders(string externalSystemPracticeName, int ioPracticeId)
        {
            List<ExternalSystemProviderDTO> extSystemProviders = _kareoService.GetProviders(externalSystemPracticeName);
            foreach (var item in extSystemProviders)
            {
                item.IO_PracticeId = ioPracticeId;
                item.IO_ProviderId = _ioKareoMapper.CheckIfProviderExistInIO(item.ExternalSystemProviderId);
                if (item.IO_ProviderId < 0)
                {
                    // create practice and mapping in IO
                    item.IO_ProviderId = _ioService.CreateResource(item);
                    _ioKareoMapper.CreateProviderMapping(item.IO_ProviderId, item.ExternalSystemProviderId);
                    _ioService.AddNecessaryPermissionsToProvider(item.IO_ProviderId);

                    EventLogger.Instance.WriteLog("Provider Created in IO.\nIO ProviderId =" + item.IO_ProviderId + "\nKareo ProviderId =" + item.ExternalSystemProviderId, EventLogEntryType.Information);
                }
            }
            return extSystemProviders;
        }

        #endregion

        #region Invoice Syncing Section

        public void SyncInvoices()
        {
            List<int> unsyncedInvoices = _ioService.GetInvoiceIdListWhoseEncounterIsNotCreatedYet();
            foreach (var invoiceId in unsyncedInvoices)
            {
                try
                {
                    InvoiceDTO invoice = _ioService.GetInvoiceDetail(invoiceId);

                    // select only those billing services which has atleast one Diagnosis Code
                    if (invoice.BillingServices.Count > 0)
                    {
                        invoice.BillingServices = invoice.BillingServices.Where(x => !string.IsNullOrWhiteSpace(x.DiagnosisCode1)).ToList();
                    }

                    if (invoice.BillingServices.Count > 0)
                    {
                        int externalSystemEncounterId = _kareoService.CreateEncounter(invoice);
                        _ioKareoMapper.CreateInvoiceMapping(invoiceId, externalSystemEncounterId);

                        EventLogger.Instance.WriteLog("Encounter Created in Kareo.\nIO InvoiceId =" + invoice.InvoiceId + "\nKareo EncounterId =" + invoice.ExternalSystemEncounterId, EventLogEntryType.Information);
                    }
                    else
                    {
                        EventLogger.Instance.WriteLog("Encounter Creation Failed in Kareo. Billing Service Count is 0 in IO..\nIO InvoiceId =" + invoice.InvoiceId + "\nKareo EncounterId =" + invoice.ExternalSystemEncounterId, EventLogEntryType.Information);
                    }
                }
                catch (Exception ex)
                {
                    EventLogger.Instance.WriteLog("Encounter Creation In Kareo Failed For IO InvoiceId " + invoiceId + ".\n" + ex.Message + ".\n\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
        }
        #endregion


        public bool CheckConnection()
        {
            string ioErrMsg;
            bool ioConnection = _ioService.IsConnected(out ioErrMsg);

            string kareoErrMsg;
            bool kareoConnection = _kareoService.IsConnected(out kareoErrMsg);
            if (!ioConnection)
            {
                EventLogger.Instance.WriteLog("IO Database Connection Failed.\n" + ioErrMsg, EventLogEntryType.Error);
            }
            else
            {
                EventLogger.Instance.WriteLog("IO Database Connection Succeeded.", EventLogEntryType.Information);
            }

            if (!kareoConnection)
            {
                EventLogger.Instance.WriteLog("Kareo Connection Failed.\n" + kareoErrMsg, EventLogEntryType.Error);
            }
            else
            {
                EventLogger.Instance.WriteLog("Kareo Connection Succeeded.", EventLogEntryType.Information);
            }
            return (ioConnection && kareoConnection) ? true : false;
        }

        public void ConfigureMappingTables()
        {
            _ioService.CreateRequiredTables();
            EventLogger.Instance.WriteLog("Mapping Tables Configured Successfully.", EventLogEntryType.Information);
        }

    }
}


