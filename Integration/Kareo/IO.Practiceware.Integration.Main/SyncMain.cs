﻿using IO.Practiceware.Integration.Domain;
using IO.Practiceware.Integration.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Transactions;

namespace IO.Practiceware.Integration.Main
{
    public class SyncMain
    {
        /// <summary>
        /// this method start Syncs
        /// </summary>
        public void StartSync()
        {

            EventLogger.Instance.WriteLog("Syncing Started.", EventLogEntryType.Information);

            #region Appointemnts Syncing
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new System.TimeSpan(0, 15, 0)))
                {
                    using (SqlConnection conn = new SqlConnection(ConfigHelper.ConnectionString))
                    {
                        conn.Open();
                        ISyncIO syncIO = new SyncIO(conn);
                        syncIO.SyncAppointments(DateTime.Now.ToCustomerMachineTime(), DateTime.Now.ToCustomerMachineTime());
                        scope.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogger.Instance.WriteLog("Error In Appointment Syncing. Syncing Failed.\n" + ex.Message + ".\n" + ex.StackTrace, EventLogEntryType.Error);
            }

            #endregion

            EventLogger.Instance.WriteLog("Encounter Syncing Started.", EventLogEntryType.Information);

            #region Invoice Syncing
            using (SqlConnection conn = new SqlConnection(ConfigHelper.ConnectionString))
            {
                try
                {
                    conn.Open();
                    ISyncIO syncIO = new SyncIO(conn);
                    syncIO.SyncInvoices();
                }
                catch (Exception ex)
                {
                    EventLogger.Instance.WriteLog("Error In Encounter Syncing. Syncing Failed.\n" + ex.Message + ".\n" + ex.StackTrace, EventLogEntryType.Error);
                }
            }
            #endregion

            EventLogger.Instance.WriteLog("Encounter Syncing Completed.", EventLogEntryType.Information);
            EventLogger.Instance.WriteLog("Syncing Completed.", EventLogEntryType.Information);
        }


        public bool CheckConnection()
        {
            using (SqlConnection conn = new SqlConnection(ConfigHelper.ConnectionString))
            {
                try
                {
                    conn.Open();
                    ISyncIO syncIO = new SyncIO(conn);
                    return syncIO.CheckConnection();
                }
                catch (Exception ex)
                {
                    EventLogger.Instance.WriteLog("Connection Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Information);
                    return false;
                }
            }
        }

        public bool CreateRequiredTablesIfNotExist()
        {
            using (SqlConnection conn = new SqlConnection(ConfigHelper.ConnectionString))
            {
                try
                {
                    conn.Open();
                    ISyncIO syncIO = new SyncIO(conn);
                    syncIO.ConfigureMappingTables();
                    return true;
                }
                catch (Exception ex)
                {
                    EventLogger.Instance.WriteLog("Mapping Tables Configuration Failed. \n" + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Information);
                    return false;
                }
            }
        }
    }
}
