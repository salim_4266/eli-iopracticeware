REM Checking whether IO's client is installed
if not exist "%LocalAppData%\IO Practiceware\IO.Practiceware.Interop.dll" goto EndScriptNotInstalled

REM Going to installation directory to perform .NET and native COM registrations
cd "%LocalAppData%\IO Practiceware"

REM Btn32a20.ocx is deployed with Prerequisites installer. Skipping its registration
regsvr32.exe /s IOConnectedDevices.dll
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe IO.Practiceware.Interop.dll /tlb:IO.Practiceware.Interop.tlb /codebase

%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe LabReport.dll /tlb:LabReport.tlb /codebase
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe EPLib.dll /tlb:EPLib.tlb /codebase
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe ClinicalDecisions.dll /tlb:ClinicalDecisions.tlb /codebase
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe GrowthCharts.dll /tlb:GrowthCharts.tlb /codebase
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe Immunizations.dll /tlb:Immunizations.tlb /codebase
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe Surveillance.dll /tlb:Surveillance.tlb /codebase
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe EmergencyAccess.dll /tlb:EmergencyAccess.tlb /codebase
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe ClinicalReports.dll /tlb:ClinicalReports.tlb /codebase
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\RegAsm.exe ClinicalIntegration.dll /tlb:ClinicalIntegration.tlb /codebase


REM Register all Dlls in GAC (don't care about failures)
for %%I in (*.dll) do "%programfiles%\Microsoft SDKs\Windows\v7.1\Bin\NETFX 4.0 Tools\gacutil" -i %%I

exit /b 0

:EndScriptNotInstalled
echo Please, install IO Practiceware Client Setup.exe first
pause
exit /b 0