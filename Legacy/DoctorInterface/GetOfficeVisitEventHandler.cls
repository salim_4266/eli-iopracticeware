VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "GetOfficeVisitEventHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IO_Practiceware_Interop.IEventHandler

Private m_SelectedDiagnosis As String
Private Sub IEventHandler_OnEvent(ByVal sender As Variant, ByVal e As Variant)
    Dim ProcCode As String
    
    m_SelectedDiagnosis = e.SelectedDiagnosis
    
    'Reload the Billed Diagnosis
    Call frmSetProcedures.LoadBilledDiagnosesFromFile(m_SelectedDiagnosis)
    
    'Then Recalculate Office visit
    ProcCode = ReCalculateOfficeVisit
    
    Call RetainOfficeVisit(ProcCode)
    
    Call BuildCodingCluesArguments
    Exit Sub
End Sub

Private Function ReCalculateOfficeVisit() As String
On Error GoTo lReCalculateOfficeVisit_Error

Dim ETemp As String, OTemp As String, EMBasis As String
Dim ALevel As Integer, OLevel As Integer
Dim Temp As String, RemoveOn As Boolean
Dim RS As Recordset, InsurerId As Long
Dim ProcedureEM As String


'We have Stored Procedure which gets EmBasis Info from dbo.AppointmetType.So,Assign Directly
EMBasis = frmSystems1.EMBasis

If EMBasis = "" Then Exit Function

'if it's PostOp Proc no need of checking Eye Proc.
If (EMBasis = "99024") Then
    ProcedureEM = "99024"
    OTemp = ""
Else
    Call frmInquiry.GetAnalyticalLevel(ALevel, OLevel)
    If Not (frmSystems1.IsPostOpOn) Then
        ETemp = Trim(str(Val(EMBasis) + ALevel))
    Else
        ETemp = "99024"
    End If
    If (OLevel = 0) Then
        OLevel = 1
    End If
    OTemp = "9200" + Trim(str(OLevel))
    If (Mid(ETemp, 3, 2) = "24") Then
        OTemp = "9204" + Trim(str(OLevel))
    ElseIf (Mid(ETemp, 3, 2) = "21") Then
        OTemp = "9201" + Trim(str(OLevel))
    End If
    
    ProcedureEM = ETemp
    
    'For now implementing Pricing Part from vb6.As we dont have model Table Structure
    'Get InsurerId
    Dim RetPatientFinancialService As PatientFinancialService
    Set RetPatientFinancialService = New PatientFinancialService
    RetPatientFinancialService.InsuredPatientId = globalExam.iPatientId
    Set RS = RetPatientFinancialService.GetPatientPrimaryInsurer
    If (Not RS Is Nothing And RS.RecordCount > 0) Then
        InsurerId = RS("InsurerId")
    End If
    Set RetPatientFinancialService = Nothing
    
    If (frmSetEMProcedure.IsOCodeValid(InsurerId, ETemp, OTemp, Temp, RemoveOn)) Then
        If (Trim(Temp) <> "") Then
            ProcedureEM = Temp
        End If
    End If
End If
    ReCalculateOfficeVisit = ProcedureEM
    Exit Function
    
lReCalculateOfficeVisit_Error:
End Function

Public Function RetainOfficeVisit(ByVal Code As String) As Boolean
On Error GoTo lRetainOfficeVisit_Error
Dim FileNum As Integer
Dim TheFile As String

FileNum = FreeFile

TheFile = DoctorInterfaceDirectory + "OfficeVisit" + "_" + Trim(str(globalExam.iAppointmentId)) + "_" + Trim(str(globalExam.iPatientId)) + ".txt"

If (FM.IsFileThere(TheFile)) Then
    FM.Kill TheFile
End If
If Not (FM.IsFileThere(TheFile)) Then
    FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(FileNum)
    Print #FileNum, Code
    FM.CloseFile CLng(FileNum)
End If
Exit Function
lRetainOfficeVisit_Error:
End Function
Private Function IsNewDiagnosis(ADiag As String, PatId As Long) As Boolean
Dim RetCln As PatientClinical
IsNewDiagnosis = True
If (PatId > 0) And (Trim(ADiag) <> "") Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalType = "B"
    RetCln.PatientId = PatId
    RetCln.Findings = ADiag
    RetCln.AppointmentId = 0
    RetCln.Symptom = ""
    RetCln.ImageDescriptor = ""
    RetCln.ImageInstructions = ""
    RetCln.ClinicalDraw = ""
    RetCln.ClinicalStatus = ""
    If (RetCln.FindPatientClinical > 0) Then
        IsNewDiagnosis = False
    End If
    Set RetCln = Nothing
End If
End Function
Private Function BuildCodingCluesArguments()
On Error GoTo lBuildCodingCluesArguments_Error

Dim StandardExamComWrapper As New comWrapper
Dim viewManager As Object
Dim loadArguments As Object
Dim ORankingOn As Boolean
Dim RankingVerifier As Boolean
Dim ODOn As Boolean, OSOn As Boolean
Dim AResult As Boolean, TheHigh As Boolean
Dim PatId As Long
Dim Ranking As Integer
Dim MRanking As Integer
Dim ARanking(3) As Integer
Dim RankingTemp As Integer
Dim i As Integer, ACnt As Integer
Dim ThePO As Integer, ACnt1 As Integer
Dim Temp As String, Temp1 As String
Dim TheTime As String, TheType As String
Dim TheText As String, TheQues As String
Dim TheOrder As String, TheParty As String
Dim ADiag As String, ADiag1 As String, AEye As String
Dim ResultLevel As Integer
Dim ResultLevelOpth As Integer
Dim TypeOfHistory As String
Dim EmLevelQualified As String
Dim EyeLevelQualified As String


ResultLevel = 0
ResultLevelOpth = 0
PatId = frmSystems1.GetPatientId
ORankingOn = False
RankingVerifier = False

StandardExamComWrapper.RethrowExceptions = False
Call StandardExamComWrapper.Create(StandardExamViewManagerType, emptyArgs)
Set viewManager = StandardExamComWrapper.Instance

Call StandardExamComWrapper.Create("IO.Practiceware.Presentation.ViewModels.StandardExam.CodingCluesLoadArguments", emptyArgs)
Set loadArguments = StandardExamComWrapper.Instance

'Type of history
TypeOfHistory = frmSystems1.GetHxReview
loadArguments.TypeOfHistory = TypeOfHistory
loadArguments.PHX = False
loadArguments.Ros = False
If (frmSystems1.lstOHX.ListCount > 1) Or (frmSystems1.lstFMX.ListCount > 1) Or (frmSystems1.lstMHX.ListCount > 1) Then
    loadArguments.PHX = True
    loadArguments.Ros = True
End If
ARanking(1) = 0
If (TypeOfHistory = "Problem Focused") Then
    ARanking(1) = 1
ElseIf (TypeOfHistory = "Expanded Problem Focused") Then
    ARanking(1) = 2
ElseIf (TypeOfHistory = "Detailed") Then
    ARanking(1) = 3
ElseIf (TypeOfHistory = "Comprehensive") Then
    ARanking(1) = 4
End If
loadArguments.ReasonForVisit = False
If (frmSystems1.lstHPI.ListCount > 0) Then
    If (Len(frmSystems1.lstHPI.List(0)) > 4) Then
        loadArguments.ReasonForVisit = True
    End If
End If
loadArguments.HPI = False
If (frmSystems1.lstCC.ListCount > 1) Then
    loadArguments.HPI = True
End If
Ranking = 0
loadArguments.VisualAcuity = False
loadArguments.GeneralMedicalEvaluation = False
loadArguments.IOP = False
i = 1
Do Until Not (frmSystems1.GetTestText(i, TheText, TheQues, TheOrder, TheParty, TheTime, TheHigh, TheType, ThePO))
    If (Trim(TheText) <> "") Then
        If (TheOrder = "03A") Or (TheOrder = "02A") Then
                loadArguments.VisualAcuity = True
                Ranking = Ranking + 1
        ElseIf (TheOrder = "26A") Then
            loadArguments.IOP = True
            Ranking = Ranking + 1
        ElseIf (TheOrder = "01A") Then
            loadArguments.GeneralMedicalEvaluation = True
            Ranking = Ranking + 1
        End If
    End If
    i = i + 1
Loop
Temp = "B"
Temp1 = ""
loadArguments.Pupils = False
GoSub VerifySystem
If (AResult) Then
    loadArguments.Pupils = True
    Ranking = Ranking + 1
End If
Temp = "F"
Temp1 = "X"
loadArguments.Conj = False
GoSub VerifySystem
If (AResult) Then
    loadArguments.Conj = True
    Ranking = Ranking + 1
End If
Temp = "A"
Temp1 = ""
loadArguments.Adnexa = False
GoSub VerifySystem
If (AResult) Then
    loadArguments.Adnexa = True
    Ranking = Ranking + 1
End If
Temp = "G"
Temp1 = "X"
loadArguments.Cornea = False
GoSub VerifySystem
If (AResult) Then
    loadArguments.Cornea = True
    Ranking = Ranking + 1
End If
Temp = "D"
Temp1 = ""
loadArguments.CVF = False
GoSub VerifySystem
If (AResult) Then
    loadArguments.CVF = True
    Ranking = Ranking + 1
End If
Temp = "KL"
Temp1 = "Y"
loadArguments.OnVit = False
GoSub VerifySystem
If (AResult) Then
    loadArguments.OnVit = True
    Ranking = Ranking + 1
End If

Temp = "H"
Temp1 = "X"
loadArguments.Ac = False
GoSub VerifySystem
If (AResult) Then
    loadArguments.Ac = True
    Ranking = Ranking + 1
End If

Temp = "J"
Temp1 = "X"
loadArguments.Lens = False
GoSub VerifySystem
If (AResult) Then
    loadArguments.Lens = True
    Ranking = Ranking + 1
End If

Temp = "MNP"
Temp1 = "Y"
loadArguments.FundusExam = False
GoSub VerifySystem
If (AResult) Then
    loadArguments.FundusExam = True
    Ranking = Ranking + 1
End If
Temp = "None"
ARanking(2) = 0
If (Ranking >= 12) Then
    Temp = "Comprehensive"
    ARanking(2) = 4
ElseIf (Ranking >= 9) And (Ranking <= 11) Then
    Temp = "Detailed"
    ARanking(2) = 3
ElseIf (Ranking >= 6) And (Ranking <= 8) Then
    Temp = "Expanded Problem Focused"
    ARanking(2) = 2
ElseIf (Ranking > 0) Then
    Temp = "Problem Focused"
    ARanking(2) = 1
End If

'Type of Exam
loadArguments.TypeOfExam = Temp

MRanking = 0
Temp1 = "RX"
loadArguments.ManagedMedications = False
GoSub GetActions
If (ACnt < 1) Then
    If (frmSystems1.lstMeds.ListCount > 1) Then
        ACnt = frmSystems1.lstMeds.ListCount - 1
    End If
End If
If (ACnt > 0) Then
    loadArguments.ManagedMedications = True
    MRanking = MRanking + 1
    ORankingOn = True
End If
Temp1 = "OFFICE PROCEDURES"
GoSub GetActions
loadArguments.ProcedureOrdered = "0"
If (ACnt > 0) Then
    loadArguments.ProcedureOrdered = str(ACnt)
    MRanking = MRanking + (2 * ACnt)
    ORankingOn = True
End If
loadArguments.TestOrdered = "0"
Temp1 = "ORDER A TEST"
GoSub GetActions
If (ACnt > 0) Then
    loadArguments.TestOrdered = str(ACnt)
    MRanking = MRanking + (2 * ACnt)
    ORankingOn = True
End If
If Not (ORankingOn) Then
    Temp1 = "SCHEDULE SURGERY"
    GoSub GetActions
    If (ACnt > 0) Then
        ORankingOn = True
    End If
End If
ACnt = 0
ACnt1 = 0
For i = 1 To frmSetProcedures.TotalBilledDiagnosis
    If (frmSetProcedures.GetBilledDiagnosis(i, ADiag, ADiag1, AEye, Temp, Temp1, TheHigh)) Then
        If (IsNewDiagnosis(ADiag, PatId)) Then
            ACnt = ACnt + 1
        Else
            ACnt1 = ACnt1 + 1
        End If
    End If
Next i
loadArguments.Newdiagnosis = "0"
If (ACnt > 0) Then
    loadArguments.Newdiagnosis = str(ACnt)
    MRanking = MRanking + (2 * ACnt)
    ORankingOn = True
End If
loadArguments.ChronicDiagnosis = "0"
If (ACnt1 > 0) Then
    loadArguments.ChronicDiagnosis = str(ACnt1)
    MRanking = MRanking + (2 * ACnt1)
End If
Temp = "Any"
ARanking(3) = 2
If (MRanking >= 9) Then
    Temp = "High Complexity"
    ARanking(3) = 5
ElseIf (MRanking >= 6) And (MRanking <= 8) Then
    Temp = "Moderate Complexity"
    ARanking(3) = 4
ElseIf (MRanking >= 2) And (MRanking <= 5) Then
    Temp = "Low Complexity"
    ARanking(3) = 3
End If
'Medical Decision Making
loadArguments.MedicalDecisionMaking = Temp

EmLevelQualified = ""
EyeLevelQualified = ""
' Analysis is such that the lowest of the first two, coupled with the third
RankingTemp = ARanking(1)
If (ARanking(2) < ARanking(1)) Then
    RankingTemp = ARanking(2)
End If
ResultLevel = 0
EmLevelQualified = "Level 1"
If (RankingTemp <> 0) Then
    ResultLevel = 1
    EmLevelQualified = "Level 2"
    If (RankingTemp >= 3) Then
        If (ARanking(3) = 3) Then
            ResultLevel = 2
            EmLevelQualified = "Level 3"
        ElseIf (ARanking(3) > 3) Then
            ResultLevel = 3
            EmLevelQualified = "Level 4"
        End If
    End If
    If (ARanking(1) = 3) And (ResultLevel > 2) Then
        ResultLevel = 2
        EmLevelQualified = "Level 3"
    ElseIf (ARanking(1) = 1) And (ResultLevel > 1) Then
        ResultLevel = 1
        EmLevelQualified = "Level 2"
    ElseIf (ARanking(1) = 2) And (ResultLevel > 1) Then
        ResultLevel = 1
        EmLevelQualified = "Level 2"
    End If
End If
ResultLevelOpth = 0
EyeLevelQualified = "Does Not Qualify"
If (ORankingOn) Then
    If (RankingTemp >= 2) Then
        If (ARanking(3) >= 2) And (ARanking(3) < 4) Then
            ResultLevelOpth = 2
            EyeLevelQualified = "Level 2"
        ElseIf (ARanking(3) >= 4) Then
            ResultLevelOpth = 4
            EyeLevelQualified = "Level 4"
        End If
    Else
        ResultLevelOpth = 2
        EyeLevelQualified = "Level 2"
    End If
    If ((ARanking(1) = 3) Or (ARanking(1) = 2)) And (ResultLevelOpth > 2) Then
        ResultLevelOpth = 2
        EyeLevelQualified = "Level 2"
    End If
End If
loadArguments.EmLevelQualified = EmLevelQualified
loadArguments.EyeLevelQualified = EyeLevelQualified

viewManager.CodingCluesLoadArguments = loadArguments
Exit Function
GetActions:
    ACnt = 0
    i = 1
    While (frmEvaluation.GetActionEvaluation(i, Temp, TheHigh, AResult))
        If (InStrPS(UCase(Temp), Temp1) > 0) Then
            If (UCase(Temp1) = "RX") Then
                If (InStrPS(Temp, "P-7") <> 0) Then
                    ACnt = ACnt + 1
                End If
            Else
                ACnt = ACnt + 1
            End If
        End If
        i = i + 1
    Wend
    Return
VerifySystem:
    ODOn = False
    OSOn = False
    AResult = False
    If (Trim(Temp) <> "") Then
        For i = 0 To frmSystems1.lstOD.ListCount - 1
            If (InStrPS(Temp, Mid(frmSystems1.lstOD.List(i), 71, 1)) <> 0) Then
                ODOn = True
                Exit For
            ElseIf (InStrPS(Temp1, Mid(frmSystems1.lstOD.List(i), 71, 1)) <> 0) Then
                ODOn = True
                Exit For
            End If
        Next i
        For i = 0 To frmSystems1.lstOS.ListCount - 1
            If (InStrPS(Temp, Mid(frmSystems1.lstOS.List(i), 71, 1)) <> 0) Then
                OSOn = True
                Exit For
            ElseIf (InStrPS(Temp1, Mid(frmSystems1.lstOS.List(i), 71, 1)) <> 0) Then
                OSOn = True
                Exit For
            End If
        Next i
        If (ODOn) Or (OSOn) Then
            AResult = True
        End If
    End If
    Return
lBuildCodingCluesArguments_Error:
    LogError "GetOfficeVisitEventHandler", "BuildCosingCluesArguments", Err.Number, Err.Description
End Function

