VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmMarcoInterface 
   BackColor       =   &H00A95911&
   Caption         =   "Interface"
   ClientHeight    =   6735
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6960
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   6735
   ScaleWidth      =   6960
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtScript 
      Height          =   5055
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   4
      Top             =   1560
      Width           =   6735
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   6960
      Top             =   120
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdGetLab 
      Height          =   855
      Left            =   2400
      TabIndex        =   3
      Top             =   600
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmMarcoInterface.frx":0000
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   6960
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   0   'False
      RThreshold      =   128
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   855
      Left            =   4680
      TabIndex        =   2
      Top             =   600
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmMarcoInterface.frx":01EA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   855
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "frmMarcoInterface.frx":03C9
   End
   Begin VB.Label lblStatus 
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Initializing..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6735
   End
End
Attribute VB_Name = "frmMarcoInterface"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------------------------------------------------
' Handshaking variables
    ' miCurrentState: 0-Free, 10-Receiving data, 20-Requesting to send data, 21-Sending data,
    '   22-Requesting to stop sending data
Dim miCurrentState As Integer

    ' Buffer to store messages that are being received
Dim mstrReadBuffer As String

    ' Buffer to store messages that need to be sent
Dim mstrWriteBuffer As String
    ' Buffer to store message descriptions that need to be sent
Dim mstrWriteDescBuffer As String
    ' Current message trying to be sent
Dim mstrWriteMessage As String
    ' Current message description trying to be sent
Dim mstrWriteMessageDesc As String

'----------------------------------------------------------------------------------------------------------------------
' Setup parameters for comm port - comes from ini file
    ' Comm port to use
Dim mstrCommPort As String
    ' Comm settings to use (ex: 9600,n,8,1)
Dim mstrCommSetting As String
    ' Equipment type that we are talking to.  Used when requesting or clearing data
Dim mstrEquipmentType As String

'----------------------------------------------------------------------------------------------------------------------
' Constants for parsing/building messages
Dim strSOH As String
Dim strSTX As String
Dim strETB As String
Dim strEOT As String
Dim strCR As String
Dim strLF As String

'----------------------------------------------------------------------------------------------------------------------
' SUB: ParseINIFile
'
' Parses the Interface.INI file for setup parameters
'
' IN:   No input parameters - location of file is assumed to be the current directory, and is hardcoded.
'
' OUT:  The following global variabels are set:
'       mstrCommPort - Comm Port to use for communications
'       mstrCommSetting - Comm port settings (baud rate, etc.)
'       mstrRequestDataType - Equipment type that we are talking to.  Used when requesting or clearing data
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub ParseINIFile()
    ' Determine the name of the ini file to read
    Dim strIniFile As String
    strIniFile = ".\Interface.INI"
    
    ' Determine the comm port
    mstrCommPort = ReadINIFile(strIniFile, "Interface", "CommPort", "1")
    
    ' Determine the comm settings
    Dim strJunk As String
    mstrCommSetting = ReadINIFile(strIniFile, "Interface", "BaudRate", "9600")
    strJunk = LCase(Left$(ReadINIFile(strIniFile, "Interface", "Parity", "o"), 1))
    If Len(strJunk) <= 0 Then strJunk = "o"
    mstrCommSetting = mstrCommSetting & "," & strJunk
    mstrCommSetting = mstrCommSetting & "," & ReadINIFile(strIniFile, "Interface", "DataBits", "8")
    mstrCommSetting = mstrCommSetting & "," & ReadINIFile(strIniFile, "Interface", "StopBits", "1")
    
    mstrEquipmentType = ReadINIFile(strIniFile, "Interface", "EquipmentType", "RK")
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  SetupCommPort
'
' Prepares the comm port for communications
'
' IN:   Uses the following global variables:
'       mstrCommPort - Comm Port to use for communications
'       mstrCommSetting - Comm port settings (baud rate, etc.)
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub SetupCommPort()
    On Error Resume Next
    
    Me.MSComm1.CommPort = mstrCommPort
    Me.MSComm1.Settings = mstrCommSetting
    Me.MSComm1.DTREnable = False
    Me.MSComm1.InputMode = comInputModeText
    Me.MSComm1.InputLen = 1
    Me.MSComm1.RThreshold = 1
    
    Me.MSComm1.PortOpen = True
    If Err <> 0 Then
        Dim strError As String
        strError = "The communications port (" & mstrCommPort & ") could not be initialized."
        MsgBox strError
        Unload Me
        Exit Sub
    End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: InitializeVars
'
' Sets the constants used to parse/build messages
'
' IN:   None
'
' OUT:  The following global variables are set:
'       strSOH
'       strSTX
'       strETB
'       strEOT
'       strCR
'       strLF
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub InitializeVars()
    strSOH = Chr(1)
    strSTX = Chr(2)
    strETB = Chr(23)
    strEOT = Chr(4)
    strCR = Chr(13)
    strLF = Chr(10)
    
    SetCurrentState 0
    
    mstrReadBuffer = ""
    mstrWriteBuffer = ""
    mstrWriteDescBuffer = ""
    mstrWriteMessage = ""
    mstrWriteMessageDesc = ""

End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: ReadData
'
' Reads all commands from the comm port, performs logic as necessary
'
' IN:   Uses the following global variables:
'       mstrReadBuffer - Stores incoming commands here, and then pulls the completed commands off of this
'
' OUT:  Uses the following global variables:
'       mstrReadBuffer - If a command was incomplete, it is left in this variable
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub ReadData()
    ' Read data from buffer
    If IsMissing(mstrReadBuffer) Then mstrReadBuffer = ""
    
    Dim strChar As String
    Dim strMessage As String
    While (MSComm1.InBufferCount > 0)
        strChar = MSComm1.Input
        If (strChar = strCR) Then
        ElseIf (strChar = strLF) Then
        ElseIf (strChar = strEOT) Then
            strMessage = mstrReadBuffer
            mstrReadBuffer = ""
            If Len(strMessage) > 0 Then HandleMessageFromEquip (strMessage)
        Else
            mstrReadBuffer = mstrReadBuffer & strChar
        End If
    Wend
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  HandleMessageFromEquip
'
' Performs logic for a single message from the equipment
'
' IN:   strMessage
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub HandleMessageFromEquip(strMessage As String)
    ' Skip leading char(1) - quit if none
    If Left$(strMessage, 1) <> strSOH Then Exit Sub
    strMessage = Right$(strMessage, Len(strMessage) - 1)
    
    ' Parse out command type - quit if none
    Dim strMessageType As String
    strMessageType = ParseData(strMessage, strSTX)
    
    If strMessageType = "C**" Then
        HandleCommandFromEquip strMessage
    ElseIf Left$(strMessageType, 1) = "D" Then
        strMessageType = Right$(strMessageType, Len(strMessageType) - 1)
        HandleDataFromEquip strMessageType, strMessage
    Else
    End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  HandleCommandFromEquip
'
' Performs logic for a single command from the equipment
'
' IN:   strCommand
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub HandleCommandFromEquip(strCommand As String)
    Dim strCommandType As String
    strCommandType = ParseData(strCommand, strETB)
    If strCommandType = "RS" Then
        RequestDataFromEquip
    End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: HandleDataFromEquip
'
' Equipment has sent data.  Parse it into data parts
'
' IN:   strEquipType - Type of equipment sending data (RM, KM, LM)
'       strData - Data sent.
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub HandleDataFromEquip(strEquipType As String, strData As String)
    While Len(strData) > 0
        ' If it starts with a strSOH, then it is a new command
        If Left$(strData, 1) = strSOH Then
            HandleMessageFromEquip strData
            strData = ""
        Else
            Dim strDataPart As String
            strDataPart = ParseData(strData, strETB)
            HandleDataPartFromEquip strEquipType, strDataPart
        End If
    Wend
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: HandleDataPartFromEquip
'
' Equipment has sent a data part.  Parse and store it
'   Not coded - need to determine how to store data
'
' IN:   strEquipType - Type of equipment sending data (RM, KM, LM)
'       strDataPart - Data part sent.
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub HandleDataPartFromEquip(strEquipType As String, strDataPart As String)
    Me.txtScript = Me.txtScript & "From " & strEquipType & ": " & strDataPart & strCR & strLF
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  SendData
'
' Place message in send buffer.  Request to send it if possible.
'
' IN:   strMessage - Message to send (note that leading SOH and trailing EOT should
'           not be included)
'       Uses the following global variables:
'           mstrWriteBuffer - Buffer for sending data
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
Private Sub SendData(strMessage As String, strMessageDesc As String)
    mstrWriteBuffer = mstrWriteBuffer & strMessage & strEOT
    mstrWriteDescBuffer = mstrWriteDescBuffer & strMessageDesc & strEOT
    RequestSend
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  RequestSend
'
' If we have data to send, and the line is available, request that the equipment accept data now
'
' IN:   None
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
Private Sub RequestSend()
    If Len(mstrWriteMessage) = 0 And Len(mstrWriteBuffer) > 0 Then
        mstrWriteMessage = ParseData(mstrWriteBuffer, strEOT)
        mstrWriteMessageDesc = ParseData(mstrWriteDescBuffer, strEOT)
    End If
    If Len(mstrWriteMessage) > 0 And miCurrentState = 0 Then
        SetCurrentState 20
        Timer1.Enabled = True
        MSComm1.DTREnable = True
    End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  SendData
'
' We now have the line for sending data - send one message
'
' IN:   Uses the following global variables
'           mstrWriteData
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
Private Sub SendDataNow()
    If Len(mstrWriteMessage) > 0 Then Me.MSComm1.Output = strSOH & mstrWriteMessage & strEOT
    Me.txtScript = Me.txtScript & "To: " & mstrWriteMessage & strCR & strLF
    mstrWriteMessage = ""
End Sub

'----------------------------------------------------------------------------------------------------------------------
' FUNCTION: ParseData
'
' Pulls the first chunk off of a longer string
'
' IN:   strCommandIn - String to parse
'       strDelimiter - Delimiter use
'       blnRequireDelimiter - if true, and the delimiter is not found, then the function returns
'           an empty string.  If false, and the delimiter is not found, then the function returns
'           the entire string.
'
' OUT:  strCommandIn - The first section is removed
'       Function returns the first section
'----------------------------------------------------------------------------------------------------------------------
'
Private Function ParseData(strData As String, strDelimiter As String, Optional blnRequireDelimiter As Boolean) As String
    Dim lJunk As Long
    lJunk = InStrPS(strData, strDelimiter)
    If (lJunk >= 1) Then
        ParseData = Left$(strData, lJunk - 1)
        strData = Right(strData, Len(strData) - lJunk)
    ElseIf (blnRequireDelimiter) Then
        ParseData = ""
    Else
        ParseData = strData
        strData = ""
    End If
    
End Function

'----------------------------------------------------------------------------------------------------------------------
' SUB: RequestDataFromEquip
'
' We need to retrieve data from the equipment - send command to equipment to send it
'
' IN:   None
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub RequestDataFromEquip()
    ' Request the data from the equipment
    Dim strMessage As String
    strMessage = "C" & mstrEquipmentType & strSTX & "SD" & strETB
    SendData strMessage, "Data request"
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: ClearDataFromEquip
'
' Clear equipment memory
'
' IN:   None
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub ClearDataFromEquip()
    ' Request the data from the equipment
    Dim strMessage As String
    strMessage = "C" & mstrEquipmentType & strSTX & "CL" & strETB
    SendData strMessage, "Clear"
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: SetCurrentState
'
' Set the current handshaking state
'
' IN:   iNextState
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub SetCurrentState(iNextState As Integer)
    miCurrentState = iNextState
    Select Case miCurrentState
        Case 0
            Me.lblStatus = "Waiting..."
        Case 10
            Me.lblStatus = "Receiving..."
        Case 20
            Me.lblStatus = "Starting to send " & mstrWriteMessageDesc & "..."
        Case 21
            Me.lblStatus = "Sending " & mstrWriteMessageDesc & "..."
        Case 22
            Me.lblStatus = "Finished sending " & mstrWriteMessageDesc & "..."
    End Select
End Sub

'----------------------------------------------------------------------------------------------------------------------
'----------------------------------------------------------------------------------------------------------------------
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub Form_Load()
    ParseINIFile
    SetupCommPort
    InitializeVars
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    
    Me.MSComm1.PortOpen = False
End Sub

Private Sub MSComm1_OnComm()
    Select Case MSComm1.CommEvent
        ' Errors
        Case comEventBreak      ' A Break was received.
        Case comEventFrame      ' Framing Error
        Case comEventOverrun    ' Data Lost.
        Case comEventRxOver     ' Receive buffer overflow.
        Case comEventRxParity   ' Parity Error.
        Case comEventTxFull     ' Transmit buffer full.
        Case comEventDCB        ' Unexpected error retrieving DCB]
        
        ' Events
        Case comEvCD            ' Change in the CD line.
        Case comEvCTS           ' Change in the CTS line.
        Case comEvDSR           ' Change in the DSR line.
            DSRStateChanged
        Case comEvRing          ' Change in the Ring Indicator.
        Case comEvReceive       ' Received RThreshold # of chars.
            ' We have received enough characters that we should transfer the buffer and look for data
            ReadData
        Case comEvSend          ' There are SThreshold number of characters in the transmit buffer.
        Case comEvEOF           ' An EOF charater was found in the input stream
        
    End Select
End Sub

Private Sub DSRStateChanged()
    Select Case miCurrentState
        Case 0
            If MSComm1.DSRHolding Then
                ' Equipment is trying to send data
                MSComm1.DTREnable = True
                mstrReadBuffer = ""
                SetCurrentState 10
            End If
        Case 10
            If Not MSComm1.DSRHolding Then
                ' Equipment is done sending data
                MSComm1.DTREnable = False
                ReadData
                SetCurrentState 0
            End If
        Case 20
            ' We just requested to send data
            If MSComm1.DSRHolding Then
                ' Equipment said go ahead
                Timer1.Enabled = False
                SetCurrentState 21
                SendDataNow
                SetCurrentState 22
                
                'Tell equipment we are done
                MSComm1.DTREnable = False
            End If
        Case 22
            ' We have just finished sending data
            If Not MSComm1.DSRHolding Then
                ' Equipment acknowledged - put back to free state
                SetCurrentState 0
            End If
    End Select
    
    RequestSend
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    If (miCurrentState = 20) Then
        ' Timed out on send - cancel attempt for now
        MSComm1.DTREnable = False
        SetCurrentState 0

        mstrWriteBuffer = ""
        mstrWriteDescBuffer = ""
        mstrWriteMessage = ""
        mstrWriteMessageDesc = ""
        
        ' Display message
        frmEventMsgs.Header = "Communications with the lab equipment could not be established."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.OtherText = ""
        frmEventMsgs.Show 1
    End If
End Sub

Private Sub cmdGetLab_Click()
    RequestDataFromEquip
End Sub

Private Sub cmdClear_Click()
    ClearDataFromEquip
End Sub

Private Sub cmdDone_Click()
    Unload Me
End Sub
