VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmDoctorSignIn 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Sign In"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdOne 
      Height          =   990
      Left            =   5880
      TabIndex        =   1
      Top             =   1680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTwo 
      Height          =   990
      Left            =   7560
      TabIndex        =   2
      Top             =   1680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":01DC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFour 
      Height          =   990
      Left            =   5880
      TabIndex        =   4
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":03B8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFive 
      Height          =   990
      Left            =   7560
      TabIndex        =   5
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":0594
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSeven 
      Height          =   990
      Left            =   5880
      TabIndex        =   7
      Top             =   3840
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":0770
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEight 
      Height          =   990
      Left            =   7560
      TabIndex        =   8
      Top             =   3840
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":094C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdZero 
      Height          =   990
      Left            =   7560
      TabIndex        =   10
      Top             =   4920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":0B28
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdThree 
      Height          =   990
      Left            =   9240
      TabIndex        =   3
      Top             =   1680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":0D04
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSix 
      Height          =   990
      Left            =   9240
      TabIndex        =   6
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":0EE0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNine 
      Height          =   990
      Left            =   9240
      TabIndex        =   9
      Top             =   3840
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":10BC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9240
      TabIndex        =   0
      Top             =   4920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":1298
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   5880
      TabIndex        =   11
      Top             =   4920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":1477
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   1680
      TabIndex        =   12
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":1658
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLogout 
      Height          =   990
      Left            =   3960
      TabIndex        =   13
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorSignIn.frx":1837
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "****"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   48
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1080
      Left            =   1590
      TabIndex        =   15
      Top             =   3840
      Visible         =   0   'False
      Width           =   1740
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H0077742D&
      Caption         =   "Please Enter Your Personal Id Number"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1215
      Left            =   360
      TabIndex        =   14
      Top             =   2640
      Width           =   5055
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmDoctorSignIn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public UserName As String
Public Permission As String
Public UserId As Long
Public IsValid As Boolean

Private ThePid As String
Private CurrentValue As String
Private CurrentEntry As Integer
Private MaxEntry As Integer
Private PreventDoubleFire As Boolean

Private Sub cmdDelete_Click()
If (CurrentEntry < 2) Then
    Exit Sub
End If
CurrentEntry = CurrentEntry - 1
Label1.Caption = String$(CurrentEntry - 1, "*")
If (CurrentEntry < 2) Then
    Label1.Visible = False
End If
CurrentValue = Left$(CurrentValue, CurrentEntry - 1)
End Sub

Private Sub cmdDone_Click()
If (Len(CurrentValue) <> 4) Then
    CurrentEntry = 1
    CurrentValue = ""
    ThePid = ""
    Label1.Visible = False
    Label1.Caption = ""
Else
    ThePid = CurrentValue
    IsValid = ApplValidateLogin(ThePid, UserId, UserName, Permission)
    Unload frmDoctorSignIn
End If
End Sub

Private Sub cmdExit_Click()
CurrentEntry = 1
CurrentValue = ""
ThePid = ""
Label1.Caption = ""
Unload frmDoctorSignIn
End Sub

Private Sub cmdLogout_Click()
UserName = ""
Permission = 0
IsValid = True
Unload frmDoctorSignIn
End Sub

Private Sub cmdZero_Click()
Call EnterPIDKey("0")
End Sub

Private Sub cmdOne_Click()
Call EnterPIDKey("1")
End Sub

Private Sub cmdTwo_Click()
Call EnterPIDKey("2")
End Sub

Private Sub cmdThree_Click()
Call EnterPIDKey("3")
End Sub

Private Sub cmdFour_Click()
Call EnterPIDKey("4")
End Sub

Private Sub cmdFive_Click()
Call EnterPIDKey("5")
End Sub

Private Sub cmdSix_Click()
Call EnterPIDKey("6")
End Sub

Private Sub cmdSeven_Click()
Call EnterPIDKey("7")
End Sub

Private Sub cmdEight_Click()
Call EnterPIDKey("8")
End Sub

Private Sub cmdNine_Click()
Call EnterPIDKey("9")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If (47 < KeyAscii And KeyAscii < 58) Then ' Make sure the key pressed is a number 0-9
    If (PreventDoubleFire) Then 'HACK: For some reason this event fires twice for each click.
        PreventDoubleFire = False
    Else
        Call EnterPIDKey(Chr(KeyAscii))
        PreventDoubleFire = True
    End If
ElseIf (KeyAscii = 13) Then
    Call cmdDone_Click
ElseIf (KeyAscii = 8) Then ' If Backspace is pressed delete an entry
    If (PreventDoubleFire) Then
        PreventDoubleFire = False
    Else
        Call cmdDelete_Click
        PreventDoubleFire = True
    End If
End If
End Sub

Private Sub EnterPIDKey(PIDKey As String)
If (CurrentEntry > MaxEntry) Then
    Exit Sub
End If
CurrentValue = CurrentValue + PIDKey
Label1.Caption = String$(CurrentEntry, "*")
Label1.Visible = True
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub Form_Load()
IsValid = False
Label1.Caption = ""
CurrentEntry = 1
ThePid = ""
CurrentValue = ""
MaxEntry = 4
If (Trim$(UserName) <> "") Then
    cmdLogout.Visible = True
Else
    cmdLogout.Visible = False
End If
PreventDoubleFire = False
End Sub

Private Function ApplValidateLogin(PID As String, UserId As Long, UserName As String, Perm As String) As Boolean
Dim RetrieveResource As SchedulerResource
UserId = 0
UserName = ""
Perm = ""
Set RetrieveResource = New SchedulerResource
RetrieveResource.ResourcePid = PID
ApplValidateLogin = RetrieveResource.ResourceLoginVerification
If (ApplValidateLogin) Then
    UserName = RetrieveResource.ResourceType + Trim$(Str$(RetrieveResource.ResourceId)) + "-" + Trim$(RetrieveResource.ResourceDescription)
    Perm = RetrieveResource.ResourcePermissions
    UserId = RetrieveResource.ResourceId
End If
Set RetrieveResource = Nothing
End Function

