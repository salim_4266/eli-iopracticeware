VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmNumericPad 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9885
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12945
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "NumericPad.frx":0000
   ScaleHeight     =   9885
   ScaleWidth      =   12945
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdDoc 
      Height          =   990
      Left            =   8520
      TabIndex        =   57
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":36C9
   End
   Begin VB.TextBox txtComment 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   240
      MaxLength       =   48
      TabIndex        =   66
      Top             =   7680
      Visible         =   0   'False
      Width           =   4575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt20 
      Height          =   990
      Left            =   10200
      TabIndex        =   43
      Top             =   6120
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":38BD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt19 
      Height          =   990
      Left            =   10200
      TabIndex        =   44
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":3A9E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt18 
      Height          =   990
      Left            =   10200
      TabIndex        =   45
      Top             =   3720
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":3C7F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt17 
      Height          =   990
      Left            =   10200
      TabIndex        =   46
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":3E60
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOne 
      Height          =   990
      Left            =   240
      TabIndex        =   0
      Top             =   4440
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":4041
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTwo 
      Height          =   990
      Left            =   1440
      TabIndex        =   1
      Top             =   4440
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":4219
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFour 
      Height          =   990
      Left            =   240
      TabIndex        =   2
      Top             =   3360
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":43F1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFive 
      Height          =   990
      Left            =   1440
      TabIndex        =   3
      Top             =   3360
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":45C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSeven 
      Height          =   990
      Left            =   240
      TabIndex        =   4
      Top             =   2280
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":47A1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEight 
      Height          =   990
      Left            =   1440
      TabIndex        =   5
      Top             =   2280
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":4979
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdZero 
      Height          =   990
      Left            =   1440
      TabIndex        =   6
      Top             =   5520
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":4B51
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdThree 
      Height          =   990
      Left            =   2640
      TabIndex        =   7
      Top             =   4440
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":4D29
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSix 
      Height          =   990
      Left            =   2640
      TabIndex        =   8
      Top             =   3360
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":4F01
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNine 
      Height          =   990
      Left            =   2640
      TabIndex        =   9
      Top             =   2280
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":50D9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   3840
      TabIndex        =   10
      Top             =   5520
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":52B1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   240
      TabIndex        =   11
      Top             =   5520
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":5490
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMinus 
      Height          =   990
      Left            =   3840
      TabIndex        =   12
      Top             =   1200
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":5671
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPlus 
      Height          =   975
      Left            =   3840
      TabIndex        =   13
      Top             =   2280
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":5849
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPoint 
      Height          =   990
      Left            =   2640
      TabIndex        =   14
      Top             =   5520
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":5A21
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLesser 
      Height          =   990
      Left            =   240
      TabIndex        =   17
      Top             =   6600
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":5BF9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdGreater 
      Height          =   990
      Left            =   3480
      TabIndex        =   18
      Top             =   6600
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":5DD5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDivide 
      Height          =   990
      Left            =   1440
      TabIndex        =   34
      Top             =   1200
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":5FB1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMultiply 
      Height          =   990
      Left            =   2640
      TabIndex        =   35
      Top             =   1200
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":6189
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdColon 
      Height          =   990
      Left            =   240
      TabIndex        =   41
      Top             =   1200
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":6361
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   3840
      TabIndex        =   42
      Top             =   4440
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":6539
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOU 
      Height          =   855
      Left            =   8520
      TabIndex        =   48
      Top             =   360
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":6714
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOS 
      Height          =   855
      Left            =   6840
      TabIndex        =   50
      Top             =   360
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":68F1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOD 
      Height          =   855
      Left            =   5160
      TabIndex        =   49
      Top             =   360
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":6ACE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDefault 
      Height          =   990
      Left            =   1800
      TabIndex        =   59
      Top             =   6600
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":6CAB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMinutes 
      Height          =   990
      Left            =   6840
      TabIndex        =   53
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":6E87
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNextVisit 
      Height          =   990
      Left            =   6840
      TabIndex        =   55
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":7069
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHours 
      Height          =   990
      Left            =   6840
      TabIndex        =   54
      Top             =   1320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":7251
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDays 
      Height          =   990
      Left            =   5160
      TabIndex        =   19
      Top             =   1320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":7431
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMonths 
      Height          =   990
      Left            =   5160
      TabIndex        =   20
      Top             =   3720
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":7610
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdYears 
      Height          =   990
      Left            =   5160
      TabIndex        =   21
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":77F1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEarly 
      Height          =   990
      Left            =   5160
      TabIndex        =   52
      Top             =   6120
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":79D1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAny 
      Height          =   990
      Left            =   6840
      TabIndex        =   51
      Top             =   6120
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":7BC1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdWeeks 
      Height          =   990
      Left            =   5160
      TabIndex        =   32
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":7DB0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt7 
      Height          =   990
      Left            =   6840
      TabIndex        =   28
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":7F90
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt1 
      Height          =   990
      Left            =   5160
      TabIndex        =   22
      Top             =   1320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":8170
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt2 
      Height          =   990
      Left            =   5160
      TabIndex        =   23
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":8350
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt3 
      Height          =   990
      Left            =   5160
      TabIndex        =   24
      Top             =   3720
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":8530
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt4 
      Height          =   990
      Left            =   5160
      TabIndex        =   25
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":8710
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt5 
      Height          =   990
      Left            =   5160
      TabIndex        =   26
      Top             =   6120
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":88F0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt6 
      Height          =   990
      Left            =   6840
      TabIndex        =   27
      Top             =   1320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":8AD0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt9 
      Height          =   990
      Left            =   6840
      TabIndex        =   30
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":8CB0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt10 
      Height          =   990
      Left            =   6840
      TabIndex        =   31
      Top             =   6120
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":8E90
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt21 
      Height          =   990
      Left            =   5160
      TabIndex        =   61
      Top             =   7320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":9071
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt22 
      Height          =   990
      Left            =   6840
      TabIndex        =   62
      Top             =   7320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":9252
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt23 
      Height          =   990
      Left            =   8520
      TabIndex        =   63
      Top             =   7320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":9433
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt24 
      Height          =   990
      Left            =   10200
      TabIndex        =   64
      Top             =   7320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":9614
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNDA 
      Height          =   990
      Left            =   8520
      TabIndex        =   60
      Top             =   3720
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":97F5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt13 
      Height          =   990
      Left            =   8520
      TabIndex        =   38
      Top             =   3720
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":99E3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStat 
      Height          =   990
      Left            =   6840
      TabIndex        =   33
      Top             =   3720
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":9BC4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt8 
      Height          =   990
      Left            =   6840
      TabIndex        =   29
      Top             =   3720
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":9DA3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurrVisit 
      Height          =   990
      Left            =   8520
      TabIndex        =   56
      Top             =   1320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":9F83
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt11 
      Height          =   990
      Left            =   8520
      TabIndex        =   36
      Top             =   1320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":A170
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdThisVisit 
      Height          =   990
      Left            =   8520
      TabIndex        =   65
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":A351
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt14 
      Height          =   990
      Left            =   8520
      TabIndex        =   39
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":A53A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdInc 
      Height          =   990
      Left            =   3480
      TabIndex        =   67
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":A71B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDec 
      Height          =   990
      Left            =   240
      TabIndex        =   68
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":A8FB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSetAppt 
      Height          =   990
      Left            =   10200
      TabIndex        =   69
      Top             =   1320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":AADB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSPI 
      Height          =   750
      Left            =   960
      TabIndex        =   70
      Top             =   8040
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1323
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":ACC3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   870
      Left            =   10200
      TabIndex        =   71
      Top             =   360
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":AEB2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt12 
      Height          =   990
      Left            =   8520
      TabIndex        =   37
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":B098
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLTR 
      Height          =   990
      Left            =   3840
      TabIndex        =   72
      Top             =   3360
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":B279
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPRN 
      Height          =   990
      Left            =   8520
      TabIndex        =   73
      Top             =   6120
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":B453
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt16 
      Height          =   990
      Left            =   10200
      TabIndex        =   47
      Top             =   1320
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":B631
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt15 
      Height          =   990
      Left            =   8520
      TabIndex        =   40
      Top             =   6120
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "NumericPad.frx":B812
   End
   Begin VB.Frame frPack 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   8535
      Left            =   5160
      TabIndex        =   74
      Top             =   360
      Visible         =   0   'False
      Width           =   6615
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   0
         Left            =   0
         TabIndex        =   75
         Top             =   0
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":B9F3
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   1
         Left            =   0
         TabIndex        =   76
         Top             =   960
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":BBD1
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   2
         Left            =   0
         TabIndex        =   77
         Top             =   1920
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":BDB2
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   3
         Left            =   0
         TabIndex        =   78
         Top             =   2880
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":BF90
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   4
         Left            =   0
         TabIndex        =   79
         Top             =   3840
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":C172
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   5
         Left            =   0
         TabIndex        =   80
         Top             =   4800
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":C356
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   6
         Left            =   0
         TabIndex        =   81
         Top             =   5760
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":C53A
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   7
         Left            =   0
         TabIndex        =   82
         Top             =   6720
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":C719
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   8
         Left            =   0
         TabIndex        =   83
         Top             =   7680
         Visible         =   0   'False
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":C8F8
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   9
         Left            =   1680
         TabIndex        =   84
         Top             =   0
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":CAD3
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   10
         Left            =   1680
         TabIndex        =   85
         Top             =   960
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":CCB9
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   11
         Left            =   1680
         TabIndex        =   86
         Top             =   1920
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":CE98
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   12
         Left            =   1680
         TabIndex        =   87
         Top             =   2880
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":D076
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   13
         Left            =   1680
         TabIndex        =   88
         Top             =   3840
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":D258
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   14
         Left            =   1680
         TabIndex        =   89
         Top             =   4800
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":D436
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   15
         Left            =   1680
         TabIndex        =   90
         Top             =   5760
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":D616
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   16
         Left            =   1680
         TabIndex        =   91
         Top             =   6720
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":D7F8
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   17
         Left            =   1680
         TabIndex        =   92
         Top             =   7680
         Visible         =   0   'False
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":D9DC
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   18
         Left            =   3360
         TabIndex        =   93
         Top             =   0
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":DBB7
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   19
         Left            =   3360
         TabIndex        =   94
         Top             =   960
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":DD9C
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   20
         Left            =   3360
         TabIndex        =   95
         Top             =   1920
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":DF84
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   21
         Left            =   3360
         TabIndex        =   96
         Top             =   2880
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":E16F
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   22
         Left            =   3360
         TabIndex        =   97
         Top             =   3840
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":E357
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   23
         Left            =   3360
         TabIndex        =   98
         Top             =   4800
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":E536
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   24
         Left            =   3360
         TabIndex        =   99
         Top             =   5760
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":E717
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   25
         Left            =   3360
         TabIndex        =   100
         Top             =   6720
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":E8F6
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   26
         Left            =   3360
         TabIndex        =   101
         Top             =   7680
         Visible         =   0   'False
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":EADC
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   27
         Left            =   5040
         TabIndex        =   102
         Top             =   0
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":ECB7
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   28
         Left            =   5040
         TabIndex        =   103
         Top             =   960
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":EE99
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   29
         Left            =   5040
         TabIndex        =   104
         Top             =   1920
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":F07E
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   30
         Left            =   5040
         TabIndex        =   105
         Top             =   2880
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":F25F
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   31
         Left            =   5040
         TabIndex        =   106
         Top             =   3840
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":F442
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   32
         Left            =   5040
         TabIndex        =   107
         Top             =   4800
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":F62E
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   33
         Left            =   5040
         TabIndex        =   108
         Top             =   5760
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":F80D
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   34
         Left            =   5040
         TabIndex        =   109
         Top             =   6720
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":F9EC
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdpack 
         Height          =   855
         Index           =   35
         Left            =   5040
         TabIndex        =   110
         Top             =   7680
         Visible         =   0   'False
         Width           =   1575
         _Version        =   131072
         _ExtentX        =   2778
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "NumericPad.frx":FBCB
      End
   End
   Begin VB.Label lblHelp 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "FieldName"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   855
      Left            =   240
      TabIndex        =   58
      Top             =   8040
      Visible         =   0   'False
      Width           =   4620
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblField 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "FieldName"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   240
      TabIndex        =   16
      Top             =   120
      Width           =   1125
   End
   Begin VB.Label lblResult 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   615
      Left            =   240
      TabIndex        =   15
      Top             =   480
      Width           =   4695
   End
End
Attribute VB_Name = "frmNumericPad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Used for Numeric Pad
Public NumPad_Result As String
Public NumPad_HelpField As String
Public NumPad_Field As String
Public NumPad_Min As Double
Public NumPad_Max As Double
Public NumPad_Default As Double
Public NumPad_ForceDecimal As Boolean
Public NumPad_Quit As Boolean
Public NumPad_Increment As String
Public NumPad_Limit As Integer

' Used for Time Frame Trigger
Public NumPad_TimeFrame As String
Public NumPad_TimeFrameOn As Boolean
Public NumPad_NoValue As Boolean

' Clear Previous Value (Quantifiers)
Public ClearValueOn As Boolean

' Used Appointment Category Trigger
Public NumPad_ACategoryOn As Boolean

' Used for Eye Trigger
Public NumPad_EyesOn As Boolean
Public SetApptOn As Boolean

' Use Comments
Public NumPad_CommentOn As Boolean
Public NumPad_Comments As String

' Activate Option Buttons
Public NumPad_DisplayFull As Boolean
Public NumPad_Choice1 As String
Public NumPad_Choice2 As String
Public NumPad_Choice3 As String
Public NumPad_Choice4 As String
Public NumPad_Choice5 As String
Public NumPad_Choice6 As String
Public NumPad_Choice7 As String
Public NumPad_Choice8 As String
Public NumPad_Choice9 As String
Public NumPad_Choice10 As String
Public NumPad_Choice11 As String
Public NumPad_Choice12 As String
Public NumPad_Choice13 As String
Public NumPad_Choice14 As String
Public NumPad_Choice15 As String
Public NumPad_Choice16 As String
Public NumPad_Choice17 As String
Public NumPad_Choice18 As String
Public NumPad_Choice19 As String
Public NumPad_Choice20 As String
Public NumPad_Choice21 As String
Public NumPad_Choice22 As String
Public NumPad_Choice23 As String
Public NumPad_Choice24 As String
Public NumPad_ChoiceOn1 As Boolean
Public NumPad_ChoiceOn2 As Boolean
Public NumPad_ChoiceOn3 As Boolean
Public NumPad_ChoiceOn4 As Boolean
Public NumPad_ChoiceOn5 As Boolean
Public NumPad_ChoiceOn6 As Boolean
Public NumPad_ChoiceOn7 As Boolean
Public NumPad_ChoiceOn8 As Boolean
Public NumPad_ChoiceOn9 As Boolean
Public NumPad_ChoiceOn10 As Boolean
Public NumPad_ChoiceOn11 As Boolean
Public NumPad_ChoiceOn12 As Boolean
Public NumPad_ChoiceOn13 As Boolean
Public NumPad_ChoiceOn14 As Boolean
Public NumPad_ChoiceOn15 As Boolean
Public NumPad_ChoiceOn16 As Boolean
Public NumPad_ChoiceOn17 As Boolean
Public NumPad_ChoiceOn18 As Boolean
Public NumPad_ChoiceOn19 As Boolean
Public NumPad_ChoiceOn20 As Boolean
Public NumPad_ChoiceOn21 As Boolean
Public NumPad_ChoiceOn22 As Boolean
Public NumPad_ChoiceOn23 As Boolean
Public NumPad_ChoiceOn24 As Boolean

Public NumPad_frPack As String

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private AllowMinusFree As Boolean
Private TheEye As String
Private NoValue As Boolean
Private NoValueButton As Integer
Private CurrentValue As String
Private CurrentEntry As Integer

Private Function IsNoValueOn()
IsNoValueOn = False
If (NoValue) Then
    If (NoValueButton = 1) And (NumPad_ChoiceOn1) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 2) And (NumPad_ChoiceOn2) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 3) And (NumPad_ChoiceOn3) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 4) And (NumPad_ChoiceOn4) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 5) And (NumPad_ChoiceOn5) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 6) And (NumPad_ChoiceOn6) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 7) And (NumPad_ChoiceOn7) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 8) And (NumPad_ChoiceOn8) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 9) And (NumPad_ChoiceOn9) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 10) And (NumPad_ChoiceOn10) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 11) And (NumPad_ChoiceOn11) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 12) And (NumPad_ChoiceOn12) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 13) And (NumPad_ChoiceOn13) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 14) And (NumPad_ChoiceOn14) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 15) And (NumPad_ChoiceOn15) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 16) And (NumPad_ChoiceOn16) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 17) And (NumPad_ChoiceOn17) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 18) And (NumPad_ChoiceOn18) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 19) And (NumPad_ChoiceOn19) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 20) And (NumPad_ChoiceOn20) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 21) And (NumPad_ChoiceOn21) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 22) And (NumPad_ChoiceOn22) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 23) And (NumPad_ChoiceOn23) Then
        IsNoValueOn = True
    ElseIf (NoValueButton = 24) And (NumPad_ChoiceOn24) Then
        IsNoValueOn = True
    End If
End If
End Function

Private Sub cmdClear_Click()
ClearValueOn = True
End Sub

Private Sub cmdDefault_Click()
CurrentValue = Trim(cmdDefault.Text)
lblResult.Caption = CurrentValue
End Sub

Private Sub cmdLTR_Click()
frmAlphaPad.NumPad_Field = "Enter Letters"
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.Show 1
If Not (frmAlphaPad.NumPad_Quit) Then
    If (Trim(frmAlphaPad.NumPad_Result) <> "") Then
        CurrentValue = CurrentValue + Trim(frmAlphaPad.NumPad_Result)
        CurrentEntry = CurrentEntry + Len(Trim(frmAlphaPad.NumPad_Result))
        lblResult.Caption = CurrentValue
    End If
End If
End Sub

Private Sub cmdpack_Click(Index As Integer)
Dim i As Integer
For i = 0 To 35
    If i = Index Then
        cmdpack(i).BackColor = ButtonSelectionColor
        cmdpack(i).ForeColor = TextSelectionColor
        NumPad_frPack = cmdpack(i).Text
    Else
        cmdpack(i).BackColor = BaseBackColor
        cmdpack(i).ForeColor = BaseTextColor
    End If
Next
End Sub

Private Sub cmdPRN_Click()
If (cmdPRN.BackColor = BaseBackColor) Then
    cmdPRN.BackColor = ButtonSelectionColor
    cmdPRN.ForeColor = TextSelectionColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "PRN"
Else
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdQuit_Click()
NumPad_Increment = ""
NumPad_HelpField = ""
NumPad_Default = 0
NumPad_Quit = True
NumPad_Result = ""
Unload frmNumericPad
End Sub

Private Sub cmdDelete_Click()
If (CurrentEntry < 2) Then
    Exit Sub
End If
CurrentEntry = CurrentEntry - 1
CurrentValue = Left(CurrentValue, CurrentEntry - 1)
lblResult.Caption = CurrentValue
End Sub

Private Sub cmdDone_Click()
NumPad_HelpField = ""
If (NumPad_Default = -9999) Then
    NumPad_Result = "1"
    CurrentValue = "1"
End If
If (Trim(txtComment.Text) <> "") Then
    NumPad_Comments = Trim(txtComment.Text)
End If
NumPad_Default = 0
If (IsNoValueOn) Then
    NumPad_Result = "-"
Else
    NumPad_Result = CurrentValue
    If (NumPad_ForceDecimal) Then
        If (InStrPS(CurrentValue, ".") = 0) Then
            CurrentValue = CurrentValue + ".00"
        End If
    End If
    If (NumPad_EyesOn) Then
        NumPad_Result = TheEye + " " + CurrentValue
    End If
    If (NumPad_TimeFrameOn) Then
        If (Trim(NumPad_Result) = "") And (Trim(NumPad_TimeFrame) <> "") Then
            NumPad_Result = "-"
        End If
    End If
    If (NumPad_Max > NumPad_Min) Then
        If (InStrPS("<>", Left(NumPad_Result, 1)) = 0) Then
            If (NumPad_Max < Val(Trim(NumPad_Result))) Then
                frmEventMsgs.Header = "Value too high"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                lblResult.Caption = ""
                NumPad_Result = ""
                CurrentEntry = 1
                CurrentValue = ""
                Exit Sub
            End If
            If (NumPad_Min > Val(Trim(NumPad_Result))) Then
                frmEventMsgs.Header = "Value too low"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                lblResult.Caption = ""
                NumPad_Result = ""
                CurrentEntry = 1
                CurrentValue = ""
                Exit Sub
            End If
        End If
    End If
End If

If Mid(lblField.Caption, 1, 31) = "Pills Count or Package Size For" Then
    Dim tst As Integer
    If Not NumPad_Result = "" Then tst = tst + 1
    If Not NumPad_frPack = "" Then tst = tst + 1
    If tst = 1 Then
        frmEventMsgs.Header = "Both a quantity and package type are required to use this area."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
End If

NumPad_Increment = ""
Unload frmNumericPad
End Sub

Private Sub cmdAppt1_Click()
If (cmdAppt1.BackColor = BaseBackColor) Then
    cmdAppt1.BackColor = ButtonSelectionColor
    cmdAppt1.ForeColor = TextSelectionColor
    NumPad_ChoiceOn1 = True
Else
    cmdAppt1.BackColor = BaseBackColor
    cmdAppt1.ForeColor = BaseTextColor
    NumPad_ChoiceOn1 = False
End If
End Sub

Private Sub cmdAppt2_Click()
If (cmdAppt2.BackColor = BaseBackColor) Then
    cmdAppt2.BackColor = ButtonSelectionColor
    cmdAppt2.ForeColor = TextSelectionColor
    NumPad_ChoiceOn2 = True
Else
    cmdAppt2.BackColor = BaseBackColor
    cmdAppt2.ForeColor = BaseTextColor
    NumPad_ChoiceOn2 = False
End If
End Sub

Private Sub cmdAppt3_Click()
If (cmdAppt3.BackColor = BaseBackColor) Then
    cmdAppt3.BackColor = ButtonSelectionColor
    cmdAppt3.ForeColor = TextSelectionColor
    NumPad_ChoiceOn3 = True
Else
    cmdAppt3.BackColor = BaseBackColor
    cmdAppt3.ForeColor = BaseTextColor
    NumPad_ChoiceOn3 = False
End If
End Sub

Private Sub cmdAppt4_Click()
If (cmdAppt4.BackColor = BaseBackColor) Then
    cmdAppt4.BackColor = ButtonSelectionColor
    cmdAppt4.ForeColor = TextSelectionColor
    NumPad_ChoiceOn4 = True
Else
    cmdAppt4.BackColor = BaseBackColor
    cmdAppt4.ForeColor = BaseTextColor
    NumPad_ChoiceOn4 = False
End If
End Sub

Private Sub cmdAppt5_Click()
If (cmdAppt5.BackColor = BaseBackColor) Then
    cmdAppt5.BackColor = ButtonSelectionColor
    cmdAppt5.ForeColor = TextSelectionColor
    NumPad_ChoiceOn5 = True
Else
    cmdAppt5.BackColor = BaseBackColor
    cmdAppt5.ForeColor = BaseTextColor
    NumPad_ChoiceOn5 = False
End If
End Sub

Private Sub cmdAppt6_Click()
If (cmdAppt6.BackColor = BaseBackColor) Then
    cmdAppt6.BackColor = ButtonSelectionColor
    cmdAppt6.ForeColor = TextSelectionColor
    NumPad_ChoiceOn6 = True
Else
    cmdAppt6.BackColor = BaseBackColor
    cmdAppt6.ForeColor = BaseTextColor
    NumPad_ChoiceOn6 = False
End If
End Sub

Private Sub cmdAppt7_Click()
If (cmdAppt7.BackColor = BaseBackColor) Then
    cmdAppt7.BackColor = ButtonSelectionColor
    cmdAppt7.ForeColor = TextSelectionColor
    NumPad_ChoiceOn7 = True
Else
    cmdAppt7.BackColor = BaseBackColor
    cmdAppt7.ForeColor = BaseTextColor
    NumPad_ChoiceOn7 = False
End If
End Sub

Private Sub cmdAppt8_Click()
If (cmdAppt8.BackColor = BaseBackColor) Then
    cmdAppt8.BackColor = ButtonSelectionColor
    cmdAppt8.ForeColor = TextSelectionColor
    NumPad_ChoiceOn8 = True
Else
    cmdAppt8.BackColor = BaseBackColor
    cmdAppt8.ForeColor = BaseTextColor
    NumPad_ChoiceOn8 = False
End If
End Sub

Private Sub cmdAppt9_Click()
If (cmdAppt9.BackColor = BaseBackColor) Then
    cmdAppt9.BackColor = ButtonSelectionColor
    cmdAppt9.ForeColor = TextSelectionColor
    NumPad_ChoiceOn9 = True
Else
    cmdAppt9.BackColor = BaseBackColor
    cmdAppt9.ForeColor = BaseTextColor
    NumPad_ChoiceOn9 = False
End If
End Sub

Private Sub cmdAppt10_Click()
If (cmdAppt10.BackColor = BaseBackColor) Then
    cmdAppt10.BackColor = ButtonSelectionColor
    cmdAppt10.ForeColor = TextSelectionColor
    NumPad_ChoiceOn10 = True
Else
    cmdAppt10.BackColor = BaseBackColor
    cmdAppt10.ForeColor = BaseTextColor
    NumPad_ChoiceOn10 = False
End If
End Sub

Private Sub cmdAppt11_Click()
If (cmdAppt11.BackColor = BaseBackColor) Then
    cmdAppt11.BackColor = ButtonSelectionColor
    cmdAppt11.ForeColor = TextSelectionColor
    NumPad_ChoiceOn11 = True
Else
    cmdAppt11.BackColor = BaseBackColor
    cmdAppt11.ForeColor = BaseTextColor
    NumPad_ChoiceOn11 = False
End If
End Sub

Private Sub cmdAppt12_Click()
If (cmdAppt12.BackColor = BaseBackColor) Then
    cmdAppt12.BackColor = ButtonSelectionColor
    cmdAppt12.ForeColor = TextSelectionColor
    NumPad_ChoiceOn12 = True
Else
    cmdAppt12.BackColor = BaseBackColor
    cmdAppt12.ForeColor = BaseTextColor
    NumPad_ChoiceOn12 = False
End If
End Sub

Private Sub cmdAppt13_Click()
If (cmdAppt13.BackColor = BaseBackColor) Then
    cmdAppt13.BackColor = ButtonSelectionColor
    cmdAppt13.ForeColor = TextSelectionColor
    NumPad_ChoiceOn13 = True
Else
    cmdAppt13.BackColor = BaseBackColor
    cmdAppt13.ForeColor = BaseTextColor
    NumPad_ChoiceOn13 = False
End If
End Sub

Private Sub cmdAppt14_Click()
If (cmdAppt14.BackColor = BaseBackColor) Then
    cmdAppt14.BackColor = ButtonSelectionColor
    cmdAppt14.ForeColor = TextSelectionColor
    NumPad_ChoiceOn14 = True
Else
    cmdAppt14.BackColor = BaseBackColor
    cmdAppt14.ForeColor = BaseTextColor
    NumPad_ChoiceOn14 = False
End If
End Sub

Private Sub cmdAppt15_Click()
If (cmdAppt15.BackColor = BaseBackColor) Then
    cmdAppt15.BackColor = ButtonSelectionColor
    cmdAppt15.ForeColor = TextSelectionColor
    NumPad_ChoiceOn15 = True
Else
    cmdAppt15.BackColor = BaseBackColor
    cmdAppt15.ForeColor = BaseTextColor
    NumPad_ChoiceOn15 = False
End If
End Sub

Private Sub cmdAppt16_Click()
If (cmdAppt16.BackColor = BaseBackColor) Then
    cmdAppt16.BackColor = ButtonSelectionColor
    cmdAppt16.ForeColor = TextSelectionColor
    NumPad_ChoiceOn16 = True
Else
    cmdAppt16.BackColor = BaseBackColor
    cmdAppt16.ForeColor = BaseTextColor
    NumPad_ChoiceOn16 = False
End If
End Sub

Private Sub cmdAppt17_Click()
If (cmdAppt17.BackColor = BaseBackColor) Then
    cmdAppt17.BackColor = ButtonSelectionColor
    cmdAppt17.ForeColor = TextSelectionColor
    NumPad_ChoiceOn17 = True
Else
    cmdAppt17.BackColor = BaseBackColor
    cmdAppt17.ForeColor = BaseTextColor
    NumPad_ChoiceOn17 = False
End If
End Sub

Private Sub cmdAppt18_Click()
If (cmdAppt18.BackColor = BaseBackColor) Then
    cmdAppt18.BackColor = ButtonSelectionColor
    cmdAppt18.ForeColor = TextSelectionColor
    NumPad_ChoiceOn18 = True
Else
    cmdAppt18.BackColor = BaseBackColor
    cmdAppt18.ForeColor = BaseTextColor
    NumPad_ChoiceOn18 = False
End If
End Sub

Private Sub cmdAppt19_Click()
If (cmdAppt19.BackColor = BaseBackColor) Then
    cmdAppt19.BackColor = ButtonSelectionColor
    cmdAppt19.ForeColor = TextSelectionColor
    NumPad_ChoiceOn19 = True
Else
    cmdAppt19.BackColor = BaseBackColor
    cmdAppt19.ForeColor = BaseTextColor
    NumPad_ChoiceOn19 = False
End If
End Sub

Private Sub cmdAppt20_Click()
If (cmdAppt20.BackColor = BaseBackColor) Then
    cmdAppt20.BackColor = ButtonSelectionColor
    cmdAppt20.ForeColor = TextSelectionColor
    NumPad_ChoiceOn20 = True
Else
    cmdAppt20.BackColor = BaseBackColor
    cmdAppt20.ForeColor = BaseTextColor
    NumPad_ChoiceOn20 = False
End If
End Sub

Private Sub cmdAppt21_Click()
If (cmdAppt21.BackColor = BaseBackColor) Then
    cmdAppt21.BackColor = ButtonSelectionColor
    cmdAppt21.ForeColor = TextSelectionColor
    NumPad_ChoiceOn21 = True
Else
    cmdAppt21.BackColor = BaseBackColor
    cmdAppt21.ForeColor = BaseTextColor
    NumPad_ChoiceOn21 = False
End If
End Sub

Private Sub cmdAppt22_Click()
If (cmdAppt22.BackColor = BaseBackColor) Then
    cmdAppt22.BackColor = ButtonSelectionColor
    cmdAppt22.ForeColor = TextSelectionColor
    NumPad_ChoiceOn22 = True
Else
    cmdAppt22.BackColor = BaseBackColor
    cmdAppt22.ForeColor = BaseTextColor
    NumPad_ChoiceOn22 = False
End If
End Sub

Private Sub cmdAppt23_Click()
If (cmdAppt23.BackColor = BaseBackColor) Then
    cmdAppt23.BackColor = ButtonSelectionColor
    cmdAppt23.ForeColor = TextSelectionColor
    NumPad_ChoiceOn23 = True
Else
    cmdAppt23.BackColor = BaseBackColor
    cmdAppt23.ForeColor = BaseTextColor
    NumPad_ChoiceOn23 = False
End If
End Sub

Private Sub cmdAppt24_Click()
If (cmdAppt24.BackColor = BaseBackColor) Then
    cmdAppt24.BackColor = ButtonSelectionColor
    cmdAppt24.ForeColor = TextSelectionColor
    NumPad_ChoiceOn24 = True
Else
    cmdAppt24.BackColor = BaseBackColor
    cmdAppt24.ForeColor = BaseTextColor
    NumPad_ChoiceOn24 = False
End If
End Sub

Private Sub cmdNextVisit_Click()
If (cmdNextVisit.BackColor = BaseBackColor) Then
    cmdNextVisit.BackColor = ButtonSelectionColor
    cmdNextVisit.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "NextVisit"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdCurrVisit_Click()
If (cmdCurrVisit.BackColor = BaseBackColor) Then
    cmdCurrVisit.BackColor = ButtonSelectionColor
    cmdCurrVisit.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = Trim(cmdCurrVisit.Text)
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdDoc_Click()
If (cmdDoc.BackColor = BaseBackColor) Then
    cmdDoc.BackColor = ButtonSelectionColor
    cmdDoc.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    NumPad_TimeFrame = "Ongoing"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdDays_Click()
If (cmdDays.BackColor = BaseBackColor) Then
    cmdDays.BackColor = ButtonSelectionColor
    cmdDays.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "Days"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdNDA_Click()
If (cmdNDA.BackColor = BaseBackColor) Then
    cmdNDA.BackColor = ButtonSelectionColor
    cmdNDA.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "Next Available Date"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdSetAppt_Click()
If (cmdSetAppt.BackColor = BaseBackColor) Then
    cmdSetAppt.BackColor = ButtonSelectionColor
    cmdSetAppt.ForeColor = TextSelectionColor
    SetApptOn = True
Else
    cmdSetAppt.BackColor = BaseBackColor
    cmdSetAppt.ForeColor = BaseTextColor
    SetApptOn = False
End If
End Sub

Private Sub cmdSPI_Click()
frmSelectDialogue.InsurerSelected = ""
Call frmSelectDialogue.BuildSelectionDialogue("SpecialInstructions")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    txtComment.Text = UCase(frmSelectDialogue.Selection)
    txtComment.Visible = True
End If
End Sub

Private Sub cmdThisVisit_Click()
If (cmdThisVisit.BackColor = BaseBackColor) Then
    cmdThisVisit.BackColor = ButtonSelectionColor
    cmdThisVisit.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "This Visit"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdWeeks_Click()
If (cmdWeeks.BackColor = BaseBackColor) Then
    cmdWeeks.BackColor = ButtonSelectionColor
    cmdWeeks.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "Weeks"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdMonths_Click()
If (cmdMonths.BackColor = BaseBackColor) Then
    cmdMonths.BackColor = ButtonSelectionColor
    cmdMonths.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "Months"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdYears_Click()
If (cmdYears.BackColor = BaseBackColor) Then
    cmdYears.BackColor = ButtonSelectionColor
    cmdYears.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "Years"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdStat_Click()
If (cmdStat.BackColor = BaseBackColor) Then
    cmdStat.BackColor = ButtonSelectionColor
    cmdStat.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "STAT"
Else
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdAny_Click()
If (cmdStat.BackColor = BaseBackColor) Then
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = ButtonSelectionColor
    cmdAny.ForeColor = TextSelectionColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    NumPad_TimeFrame = Trim(cmdAny.Text)
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdEarly_Click()
If (cmdStat.BackColor = BaseBackColor) Then
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = ButtonSelectionColor
    cmdEarly.ForeColor = TextSelectionColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = Trim(cmdEarly.Text)
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdHours_Click()
If (cmdStat.BackColor = BaseBackColor) Then
    cmdHours.BackColor = ButtonSelectionColor
    cmdHours.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = ButtonSelectionColor
    cmdEarly.ForeColor = TextSelectionColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "Hours"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdMinutes_Click()
If (cmdStat.BackColor = BaseBackColor) Then
    cmdMinutes.BackColor = ButtonSelectionColor
    cmdMinutes.ForeColor = TextSelectionColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = ButtonSelectionColor
    cmdEarly.ForeColor = TextSelectionColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = "Minutes"
Else
    cmdThisVisit.BackColor = BaseBackColor
    cmdThisVisit.ForeColor = BaseTextColor
    cmdPRN.BackColor = BaseBackColor
    cmdPRN.ForeColor = BaseTextColor
    cmdNDA.BackColor = BaseBackColor
    cmdNDA.ForeColor = BaseTextColor
    cmdStat.BackColor = BaseBackColor
    cmdStat.ForeColor = BaseTextColor
    cmdYears.BackColor = BaseBackColor
    cmdYears.ForeColor = BaseTextColor
    cmdMonths.BackColor = BaseBackColor
    cmdMonths.ForeColor = BaseTextColor
    cmdDays.BackColor = BaseBackColor
    cmdDays.ForeColor = BaseTextColor
    cmdWeeks.BackColor = BaseBackColor
    cmdWeeks.ForeColor = BaseTextColor
    cmdHours.BackColor = BaseBackColor
    cmdHours.ForeColor = BaseTextColor
    cmdMinutes.BackColor = BaseBackColor
    cmdMinutes.ForeColor = BaseTextColor
    cmdAny.BackColor = BaseBackColor
    cmdAny.ForeColor = BaseTextColor
    cmdEarly.BackColor = BaseBackColor
    cmdEarly.ForeColor = BaseTextColor
    cmdNextVisit.BackColor = BaseBackColor
    cmdNextVisit.ForeColor = BaseTextColor
    cmdCurrVisit.BackColor = BaseBackColor
    cmdCurrVisit.ForeColor = BaseTextColor
    cmdDoc.BackColor = BaseBackColor
    cmdDoc.ForeColor = BaseTextColor
    NumPad_TimeFrame = ""
End If
End Sub

Private Sub cmdOD_Click()
If (cmdOD.BackColor = BaseBackColor) Then
    cmdOD.BackColor = ButtonSelectionColor
    cmdOD.ForeColor = TextSelectionColor
    cmdOS.BackColor = BaseBackColor
    cmdOS.ForeColor = BaseTextColor
    cmdOU.BackColor = BaseBackColor
    cmdOU.ForeColor = BaseTextColor
    TheEye = "OD"
Else
    cmdOD.BackColor = BaseBackColor
    cmdOD.ForeColor = BaseTextColor
    cmdOS.BackColor = BaseBackColor
    cmdOS.ForeColor = BaseTextColor
    cmdOU.BackColor = BaseBackColor
    cmdOU.ForeColor = BaseTextColor
    TheEye = ""
End If
End Sub

Private Sub cmdOS_Click()
If (cmdOS.BackColor = BaseBackColor) Then
    cmdOS.BackColor = ButtonSelectionColor
    cmdOS.ForeColor = TextSelectionColor
    cmdOD.BackColor = BaseBackColor
    cmdOD.ForeColor = BaseTextColor
    cmdOU.BackColor = BaseBackColor
    cmdOU.ForeColor = BaseTextColor
    TheEye = "OS"
Else
    cmdOD.BackColor = BaseBackColor
    cmdOD.ForeColor = BaseTextColor
    cmdOS.BackColor = BaseBackColor
    cmdOS.ForeColor = BaseTextColor
    cmdOU.BackColor = BaseBackColor
    cmdOU.ForeColor = BaseTextColor
    TheEye = ""
End If
End Sub

Private Sub cmdOU_Click()
If (cmdOU.BackColor = BaseBackColor) Then
    cmdOU.BackColor = ButtonSelectionColor
    cmdOU.ForeColor = TextSelectionColor
    cmdOS.BackColor = BaseBackColor
    cmdOS.ForeColor = BaseTextColor
    cmdOD.BackColor = BaseBackColor
    cmdOD.ForeColor = BaseTextColor
    TheEye = "OU"
Else
    cmdOD.BackColor = BaseBackColor
    cmdOD.ForeColor = BaseTextColor
    cmdOS.BackColor = BaseBackColor
    cmdOS.ForeColor = BaseTextColor
    cmdOU.BackColor = BaseBackColor
    cmdOU.ForeColor = BaseTextColor
    TheEye = ""
End If
End Sub

Private Sub cmdGreater_Click()
CurrentValue = "+" + Trim(Mid(cmdGreater.Text, 3, Len(cmdGreater.Text) - 2))
lblResult.Caption = CurrentValue
End Sub

Private Sub cmdLesser_Click()
CurrentValue = "-" + Trim(Mid(cmdLesser.Text, 3, Len(cmdLesser.Text) - 2))
lblResult.Caption = CurrentValue
End Sub

Private Sub cmdColon_Click()
CurrentValue = CurrentValue + ":"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdMultiply_Click()
CurrentValue = CurrentValue + "*"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdPlus_Click()
CurrentValue = CurrentValue + "+"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdMinus_Click()
If (CurrentEntry = 1) Then
    CurrentValue = CurrentValue + "-"
    lblResult.Caption = CurrentValue
    CurrentEntry = CurrentEntry + 1
ElseIf (AllowMinusFree) Then
    CurrentValue = CurrentValue + "-"
    lblResult.Caption = CurrentValue
    CurrentEntry = CurrentEntry + 1
End If
End Sub

Private Sub cmdPoint_Click()
CurrentValue = CurrentValue + "."
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdDivide_Click()
CurrentValue = CurrentValue + "/"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdZero_Click()
CurrentValue = CurrentValue + "0"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdInc_Click()
Dim UTemp As String
Dim Temp As String
Dim IncCnt As Single
Temp = Trim(lblResult.Caption)
IncCnt = Val(Trim(cmdInc.Text))
Temp = Trim(str(Val(Temp) + IncCnt))
If (Mid(Temp, 1, 1) = "-") Then
    Call DisplayDollarAmount(Mid(Temp, 2, Len(Temp) - 1), UTemp)
    UTemp = "-" + Trim(UTemp)
Else
    Call DisplayDollarAmount(Temp, UTemp)
    UTemp = "+" + Trim(UTemp)
End If
lblResult.Caption = UTemp
CurrentValue = UTemp
End Sub

Private Sub cmdDec_Click()
Dim UTemp As String
Dim Temp As String
Dim IncCnt As Single
Temp = Trim(lblResult.Caption)
IncCnt = Val(Trim(cmdDec.Text))
Temp = Trim(str(Val(Temp) + IncCnt))
If (Mid(Temp, 1, 1) = "-") Then
    Call DisplayDollarAmount(Mid(Temp, 2, Len(Temp) - 1), UTemp)
    UTemp = "-" + Trim(UTemp)
Else
    Call DisplayDollarAmount(Temp, UTemp)
    UTemp = "+" + Trim(UTemp)
End If
lblResult.Caption = UTemp
CurrentValue = UTemp
End Sub

Private Sub cmdOne_Click()
CurrentValue = CurrentValue + "1"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdTwo_Click()
CurrentValue = CurrentValue + "2"
lblResult = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdThree_Click()
CurrentValue = CurrentValue + "3"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdFour_Click()
CurrentValue = CurrentValue + "4"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdFive_Click()
CurrentValue = CurrentValue + "5"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdSix_Click()
CurrentValue = CurrentValue + "6"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdSeven_Click()
CurrentValue = CurrentValue + "7"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdEight_Click()
CurrentValue = CurrentValue + "8"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdNine_Click()
CurrentValue = CurrentValue + "9"
lblResult.Caption = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub Form_Load()
Dim BoldOn As Boolean
AllowMinusFree = False
TheEye = ""
SetApptOn = False
ClearValueOn = False
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdOne.BackColor
BaseTextColor = cmdOne.ForeColor
NumPad_Quit = False
lblHelp.Caption = ""
lblHelp.Visible = False
If (Trim(NumPad_HelpField) <> "") Then
    lblHelp.Caption = NumPad_HelpField
    lblHelp.Visible = True
End If
NumPad_Comments = ""
cmdSPI.Visible = False
cmdClear.Visible = False
If (InStrPS(UCase(Trim(NumPad_Field)), "QUANTIFIER") > 0) Then
    cmdClear.Visible = True
End If
txtComment.Text = ""
txtComment.Visible = False
If (NumPad_CommentOn) Then
    txtComment.Visible = True
    If (UCase(Trim(NumPad_Field)) = "ORDER A TEST") Then
        cmdSPI.Visible = True
    End If
    If (UCase(Trim(NumPad_Field)) = "OFFICE PROCEDURES") Then
        cmdSPI.Visible = True
    End If
    If (UCase(Trim(NumPad_Field)) = "SCHEDULE SURGERY") Then
        cmdSPI.Visible = True
    End If
End If
If (NumPad_TimeFrameOn) Then
    cmdCurrVisit.Visible = True
    cmdNextVisit.Visible = True
    cmdDoc.Visible = True
    cmdHours.Visible = True
    cmdMinutes.Visible = True
    cmdThisVisit.Visible = True
    cmdSetAppt.Visible = False
    If (UCase(NumPad_Field) = "OFFICE PROCEDURES") Then
        cmdSetAppt.Visible = True
    End If
    If (UCase(NumPad_Field) = "ORDER A TEST") Then
        cmdSetAppt.Visible = True
    End If
    If (UCase(Trim(NumPad_Field)) = "SCHEDULE SURGERY") Then
        cmdSetAppt.Visible = True
    End If
    cmdNDA.Visible = True
    cmdAny.Visible = True
    cmdEarly.Visible = True
    cmdDays.Visible = True
    cmdWeeks.Visible = True
    cmdMonths.Visible = True
    cmdYears.Visible = True
    cmdPRN.Visible = True
    cmdStat.Visible = True
    cmdPlus.Visible = False
    cmdMinus.Visible = False
    cmdMultiply.Visible = False
    cmdDivide.Visible = False
    cmdPoint.Visible = False
    cmdColon.Visible = False
End If
If (NumPad_EyesOn) Then
    cmdOD.Visible = True
    cmdOS.Visible = True
    cmdOU.Visible = True
End If
BoldOn = False
If (Left(NumPad_Field, 6) = "Dosage") Then
    BoldOn = True
End If
frmNumericPad.Width = 12000
frmNumericPad.Height = 9000
NumPad_ChoiceOn1 = False
NumPad_ChoiceOn2 = False
NumPad_ChoiceOn3 = False
NumPad_ChoiceOn4 = False
NumPad_ChoiceOn5 = False
NumPad_ChoiceOn6 = False
NumPad_ChoiceOn7 = False
NumPad_ChoiceOn8 = False
NumPad_ChoiceOn9 = False
NumPad_ChoiceOn10 = False
NumPad_ChoiceOn11 = False
NumPad_ChoiceOn12 = False
NumPad_ChoiceOn13 = False
NumPad_ChoiceOn14 = False
NumPad_ChoiceOn15 = False
NumPad_ChoiceOn16 = False
NumPad_ChoiceOn17 = False
NumPad_ChoiceOn18 = False
NumPad_ChoiceOn19 = False
NumPad_ChoiceOn20 = False
NumPad_ChoiceOn21 = False
NumPad_ChoiceOn22 = False
NumPad_ChoiceOn23 = False
NumPad_ChoiceOn24 = False
If (Trim(NumPad_Choice1) <> "") Then
    cmdAppt1.Text = NumPad_Choice1
    cmdAppt1.Visible = True
    If (InStrPS(cmdAppt1.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 1
    End If
End If
If (Trim(NumPad_Choice2) <> "") Then
    cmdAppt2.Text = NumPad_Choice2
    cmdAppt2.Visible = True
    If (InStrPS(cmdAppt2.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 2
    End If
End If
If (Trim(NumPad_Choice3) <> "") Then
    cmdAppt3.Text = NumPad_Choice3
    cmdAppt3.Visible = True
    If (InStrPS(cmdAppt3.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 3
    End If
End If
If (Trim(NumPad_Choice4) <> "") Then
    cmdAppt4.Text = NumPad_Choice4
    cmdAppt4.Visible = True
    If (InStrPS(cmdAppt4.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 4
    End If
End If
If (Trim(NumPad_Choice5) <> "") Then
    cmdAppt5.Text = NumPad_Choice5
    cmdAppt5.Visible = True
    If (InStrPS(cmdAppt5.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 5
    End If
End If
If (Trim(NumPad_Choice6) <> "") Then
    cmdAppt6.Text = NumPad_Choice6
    cmdAppt6.Visible = True
    If (InStrPS(cmdAppt6.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 6
    End If
End If
If (Trim(NumPad_Choice7) <> "") Then
    cmdAppt7.Text = NumPad_Choice7
    cmdAppt7.Visible = True
    If (InStrPS(cmdAppt7.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 7
    End If
End If
If (Trim(NumPad_Choice8) <> "") Then
    cmdAppt8.Text = NumPad_Choice8
    cmdAppt8.Visible = True
    If (InStrPS(cmdAppt8.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 8
    End If
End If
If (Trim(NumPad_Choice9) <> "") Then
    cmdAppt9.Text = NumPad_Choice9
    cmdAppt9.Visible = True
    If (InStrPS(cmdAppt9.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 9
    End If
End If
If (Trim(NumPad_Choice10) <> "") Then
    cmdAppt10.Text = NumPad_Choice10
    cmdAppt10.Visible = True
    If (InStrPS(cmdAppt10.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 10
    End If
End If
If (Trim(NumPad_Choice11) <> "") Then
    cmdAppt11.Text = NumPad_Choice11
    cmdAppt11.Visible = True
    If (InStrPS(cmdAppt11.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 11
    End If
    cmdAppt11.FontBold = BoldOn
End If
If (Trim(NumPad_Choice12) <> "") Then
    cmdAppt12.Text = NumPad_Choice12
    cmdAppt12.Visible = True
    If (InStrPS(cmdAppt12.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 12
    End If
    cmdAppt12.FontBold = BoldOn
End If
If (Trim(NumPad_Choice13) <> "") Then
    cmdAppt13.Text = NumPad_Choice13
    cmdAppt13.Visible = True
    If (InStrPS(cmdAppt13.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 13
    End If
    cmdAppt13.FontBold = BoldOn
End If
If (Trim(NumPad_Choice14) <> "") Then
    cmdAppt14.Text = NumPad_Choice14
    cmdAppt14.Visible = True
    If (InStrPS(cmdAppt14.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 14
    End If
    cmdAppt14.FontBold = BoldOn
End If
If (Trim(NumPad_Choice15) <> "") Then
    cmdAppt15.Text = NumPad_Choice15
    cmdAppt15.Visible = True
    If (InStrPS(cmdAppt15.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 15
    End If
    cmdAppt15.FontBold = BoldOn
End If
If (Trim(NumPad_Choice16) <> "") Then
    cmdAppt16.Text = NumPad_Choice16
    cmdAppt16.Visible = True
    If (InStrPS(cmdAppt16.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 16
    End If
    cmdAppt16.FontBold = BoldOn
End If
If (Trim(NumPad_Choice17) <> "") Then
    cmdAppt17.Text = NumPad_Choice17
    cmdAppt17.Visible = True
    If (InStrPS(cmdAppt17.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 17
    End If
    cmdAppt17.FontBold = BoldOn
End If
If (Trim(NumPad_Choice18) <> "") Then
    cmdAppt18.Text = NumPad_Choice18
    cmdAppt18.Visible = True
    If (InStrPS(cmdAppt18.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 18
    End If
    cmdAppt18.FontBold = BoldOn
End If
If (Trim(NumPad_Choice19) <> "") Then
    cmdAppt19.Text = NumPad_Choice19
    cmdAppt19.Visible = True
    If (InStrPS(cmdAppt19.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 19
    End If
    cmdAppt19.FontBold = BoldOn
End If
If (Trim(NumPad_Choice20) <> "") Then
    If (Left(NumPad_Choice20, 1) = "~") Then
        NumPad_Choice20 = Mid(NumPad_Choice20, 2, Len(NumPad_Choice20) - 1)
        cmdAppt20.FontUnderline = True
        cmdAppt20.FontBold = True
    End If
    cmdAppt20.Text = NumPad_Choice20
    cmdAppt20.Visible = True
    If (InStrPS(cmdAppt20.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 20
    End If
    cmdAppt20.FontBold = BoldOn
End If
If (Trim(NumPad_Choice21) <> "") Then
    If (Left(NumPad_Choice21, 1) = "~") Then
        NumPad_Choice21 = Mid(NumPad_Choice21, 2, Len(NumPad_Choice21) - 1)
        cmdAppt21.FontUnderline = True
        cmdAppt21.FontBold = True
    End If
    cmdAppt21.Text = NumPad_Choice21
    cmdAppt21.Visible = True
    If (InStrPS(cmdAppt21.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 21
    End If
    cmdAppt21.FontBold = BoldOn
End If
If (Trim(NumPad_Choice22) <> "") Then
    If (Left(NumPad_Choice22, 1) = "~") Then
        NumPad_Choice22 = Mid(NumPad_Choice22, 2, Len(NumPad_Choice22) - 1)
        cmdAppt22.FontUnderline = True
        cmdAppt22.FontBold = True
    End If
    cmdAppt22.Text = NumPad_Choice22
    cmdAppt22.Visible = True
    If (InStrPS(cmdAppt22.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 22
    End If
    cmdAppt22.FontBold = BoldOn
End If
If (Trim(NumPad_Choice23) <> "") Then
    If (Left(NumPad_Choice23, 1) = "~") Then
        NumPad_Choice23 = Mid(NumPad_Choice23, 2, Len(NumPad_Choice23) - 1)
        cmdAppt23.FontUnderline = True
        cmdAppt23.FontBold = True
    End If
    cmdAppt23.Text = NumPad_Choice23
    cmdAppt23.Visible = True
    If (InStrPS(cmdAppt23.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 23
    End If
    cmdAppt23.FontBold = BoldOn
End If
If (Trim(NumPad_Choice24) <> "") Then
    If (Left(NumPad_Choice24, 1) = "~") Then
        NumPad_Choice24 = Mid(NumPad_Choice24, 2, Len(NumPad_Choice24) - 1)
        cmdAppt24.FontUnderline = True
        cmdAppt24.FontBold = True
    End If
    cmdAppt24.Text = NumPad_Choice24
    cmdAppt24.Visible = True
    If (InStrPS(cmdAppt24.Text, "Ignore") <> 0) Then
        NoValue = True
        NoValueButton = 24
    End If
    cmdAppt24.FontBold = BoldOn
End If
If (NumPad_ACategoryOn) Then
    cmdAppt1.Text = "Short"
    cmdAppt1.Visible = True
    cmdAppt2.Text = "Intermediate"
    cmdAppt2.Visible = True
    cmdAppt3.Text = "Long"
    cmdAppt3.Visible = True
    cmdAppt4.Text = "Extended"
    cmdAppt4.Visible = True
    cmdAppt5.Text = "W"
    cmdAppt5.Visible = True
    cmdAppt6.Text = "X"
    cmdAppt6.Visible = True
    cmdAppt7.Text = "Y"
    cmdAppt7.Visible = True
    cmdAppt8.Text = "Z"
    cmdAppt8.Visible = True
    cmdAppt9.Text = "Clear Slot"
    cmdAppt9.Visible = True
    cmdAppt10.Text = "A"
    cmdAppt10.Visible = True
    cmdAppt11.Text = "B"
    cmdAppt11.Visible = True
    cmdAppt12.Text = "C"
    cmdAppt12.Visible = True
    cmdAppt13.Text = "D"
    cmdAppt13.Visible = True
    cmdAppt14.Text = "M"
    cmdAppt14.Visible = True
    cmdAppt15.Text = "N"
    cmdAppt15.Visible = True
    cmdAppt16.Text = "O"
    cmdAppt16.Visible = True
    cmdAppt17.Text = "P"
    cmdAppt17.Visible = True
End If
lblResult.Caption = ""
CurrentEntry = 1
NumPad_Result = ""
CurrentValue = ""
lblField.Caption = NumPad_Field
lblField.Visible = True
If (NumPad_Max <> NumPad_Min) Then
    cmdGreater.Visible = True
    cmdGreater.Text = "> " + Trim(str(NumPad_Max))
Else
    cmdGreater.Visible = False
End If
If (NumPad_Max <> NumPad_Min) Then
    cmdLesser.Visible = True
    cmdLesser.Text = "< " + Trim(str(NumPad_Min))
Else
    cmdLesser.Visible = False
End If
If (NumPad_Default = -9999) Then
'Kill the pad
    cmdOne.Visible = False
    cmdTwo.Visible = False
    cmdThree.Visible = False
    cmdFour.Visible = False
    cmdFive.Visible = False
    cmdSix.Visible = False
    cmdSeven.Visible = False
    cmdEight.Visible = False
    cmdNine.Visible = False
    cmdZero.Visible = False
    cmdLesser.Visible = False
    cmdGreater.Visible = False
    cmdDefault.Visible = False
    cmdPlus.Visible = False
    cmdMinus.Visible = False
    cmdColon.Visible = False
    cmdDivide.Visible = False
    cmdMultiply.Visible = False
    cmdDelete.Visible = False
    cmdPoint.Visible = False
ElseIf (NumPad_Default <> 0) Then
    cmdDefault.Visible = True
    cmdDefault.Text = Trim(str(NumPad_Default))
Else
    cmdDefault.Visible = False
End If
If (InStrPS(NumPad_Field, "Complaint") <> 0) Then
    AllowMinusFree = True
End If
If (InStrPS(NumPad_Field, "Quantifier") <> 0) Then
    AllowMinusFree = True
End If
cmdInc.Visible = False
cmdDec.Visible = False
If (Trim(NumPad_Increment) <> "") Then
    cmdInc.Text = "+" + Trim(NumPad_Increment)
    cmdInc.Visible = True
    cmdDec.Text = "-" + Trim(NumPad_Increment)
    cmdDec.Visible = True
End If
End Sub

Public Sub SetDisplay(sTitle As String, Optional sChoice1 As String = "", Optional sChoice2 As String = "", Optional sChoice3 As String = "", Optional sChoice4 As String = "", Optional sChoice5 As String = "", _
    Optional sChoice6 As String = "", Optional sChoice7 As String = "", Optional sChoice8 As String = "", Optional sChoice9 As String = "", Optional sChoice10 As String = "", Optional sChoice11 As String = "", Optional sChoice12 As String = "", _
    Optional sChoice13 As String = "", Optional sChoice14 As String = "", Optional sChoice15 As String = "", Optional sChoice16 As String = "", Optional sChoice17 As String = "", Optional sChoice18 As String = "", Optional sChoice19 As String = "", _
    Optional sChoice20 As String = "", Optional sChoice21 As String = "", Optional sChoice22 As String = "", Optional sChoice23 As String = "", Optional sChoice24 As String = "")
    NumPad_Result = ""
    NumPad_Field = sTitle
    NumPad_ChoiceOn1 = False
    NumPad_Choice1 = sChoice1
    NumPad_ChoiceOn2 = False
    NumPad_Choice2 = sChoice2
    NumPad_ChoiceOn3 = False
    NumPad_Choice3 = sChoice3
    NumPad_ChoiceOn4 = False
    NumPad_Choice4 = sChoice4
    NumPad_ChoiceOn5 = False
    NumPad_Choice5 = sChoice5
    NumPad_ChoiceOn6 = False
    NumPad_Choice6 = sChoice6
    NumPad_ChoiceOn7 = False
    NumPad_Choice7 = sChoice7
    NumPad_ChoiceOn8 = False
    NumPad_Choice8 = sChoice8
    NumPad_ChoiceOn9 = False
    NumPad_Choice9 = sChoice9
    NumPad_ChoiceOn10 = False
    NumPad_Choice10 = sChoice10
    NumPad_ChoiceOn11 = False
    NumPad_Choice11 = sChoice11
    NumPad_ChoiceOn12 = False
    NumPad_Choice12 = sChoice12
    NumPad_ChoiceOn13 = False
    NumPad_Choice13 = sChoice13
    NumPad_ChoiceOn14 = False
    NumPad_Choice14 = sChoice14
    NumPad_ChoiceOn15 = False
    NumPad_Choice15 = sChoice15
    NumPad_ChoiceOn16 = False
    NumPad_Choice16 = sChoice16
    NumPad_ChoiceOn17 = False
    NumPad_Choice17 = sChoice17
    NumPad_ChoiceOn18 = False
    NumPad_Choice18 = sChoice18
    NumPad_ChoiceOn19 = False
    NumPad_Choice19 = sChoice19
    NumPad_ChoiceOn20 = False
    NumPad_Choice20 = sChoice20
    NumPad_ChoiceOn21 = False
    NumPad_Choice21 = sChoice21
    NumPad_ChoiceOn22 = False
    NumPad_Choice22 = sChoice22
    NumPad_ChoiceOn23 = False
    NumPad_Choice23 = sChoice23
    NumPad_ChoiceOn24 = False
    NumPad_Choice24 = sChoice24
End Sub

Public Function GetResultString() As String
Dim sResult As String
    If Trim$(NumPad_Result) <> "" Then
        sResult = Trim$(NumPad_Result) & " "
    End If
    If NumPad_ChoiceOn1 Then
        sResult = sResult & NumPad_Choice1 & " "
    End If
    If NumPad_ChoiceOn2 Then
        sResult = sResult & NumPad_Choice2 & " "
    End If
    If NumPad_ChoiceOn3 Then
        sResult = sResult & NumPad_Choice3 & " "
    End If
    If NumPad_ChoiceOn4 Then
        sResult = sResult & NumPad_Choice4 & " "
    End If
    If NumPad_ChoiceOn5 Then
        sResult = sResult & NumPad_Choice5 & " "
    End If
    If NumPad_ChoiceOn6 Then
        sResult = sResult & NumPad_Choice6 & " "
    End If
    If NumPad_ChoiceOn7 Then
        sResult = sResult & NumPad_Choice7 & " "
    End If
    If NumPad_ChoiceOn8 Then
        sResult = sResult & NumPad_Choice8 & " "
    End If
    If NumPad_ChoiceOn9 Then
        sResult = sResult & NumPad_Choice9 & " "
    End If
    If NumPad_ChoiceOn11 Then
        sResult = sResult & NumPad_Choice11 & " "
    End If
    If NumPad_ChoiceOn12 Then
        sResult = sResult & NumPad_Choice12 & " "
    End If
    If NumPad_ChoiceOn13 Then
        sResult = sResult & NumPad_Choice13 & " "
    End If
    If NumPad_ChoiceOn14 Then
        sResult = sResult & NumPad_Choice14 & " "
    End If
    If NumPad_ChoiceOn15 Then
        sResult = sResult & NumPad_Choice15 & " "
    End If
    If NumPad_ChoiceOn16 Then
        sResult = sResult & NumPad_Choice16 & " "
    End If
    If NumPad_ChoiceOn17 Then
        sResult = sResult & NumPad_Choice17 & " "
    End If
    If NumPad_ChoiceOn18 Then
        sResult = sResult & NumPad_Choice18 & " "
    End If
    If NumPad_ChoiceOn19 Then
        sResult = sResult & NumPad_Choice19 & " "
    End If
    If NumPad_ChoiceOn20 Then
        sResult = sResult & NumPad_Choice20 & " "
    End If
    If NumPad_ChoiceOn21 Then
        sResult = sResult & NumPad_Choice21 & " "
    End If
    If NumPad_ChoiceOn22 Then
        sResult = sResult & NumPad_Choice22 & " "
    End If
    If NumPad_ChoiceOn23 Then
        sResult = sResult & NumPad_Choice23 & " "
    End If
    If NumPad_ChoiceOn24 Then
        sResult = sResult & NumPad_Choice24 & " "
    End If
    GetResultString = Trim$(sResult)
End Function


Private Sub lblResult_Change()

If NumPad_Limit > 0 Then
    If Len(lblResult.Caption) > NumPad_Limit Then
       lblResult.Caption = Mid(lblResult.Caption, 1, NumPad_Limit)
       CurrentValue = lblResult.Caption
       CurrentEntry = NumPad_Limit
    End If
End If
End Sub

Public Sub DisableButtons(lst As String)
Dim btn As Control
Dim txt As String
For Each btn In Me
    txt = getText(btn)
    If Not txt = "" Then
        txt = "<" & txt & ">"
        If InStrPS(1, lst, txt) > 0 Then
            btn.Enabled = False
        End If
    End If
Next

End Sub

Private Function getText(cntl As Control) As String
On Error GoTo EndFunction
getText = cntl.Text
Exit Function

EndFunction:
getText = ""
End Function
Public Function FrmClose()
 Call cmdQuit_Click
 Unload Me
End Function

