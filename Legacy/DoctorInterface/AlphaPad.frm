VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmAlphaPad 
   BackColor       =   &H00505050&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9435
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12420
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "AlphaPad.frx":0000
   ScaleHeight     =   9435
   ScaleWidth      =   12420
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtResult 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   1080
      Width           =   6495
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   8760
      TabIndex        =   1
      Top             =   6360
      Width           =   2295
      _Version        =   131072
      _ExtentX        =   4048
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   9840
      TabIndex        =   2
      Top             =   5280
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":38A8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSpace 
      Height          =   975
      Left            =   3840
      TabIndex        =   3
      Top             =   6360
      Width           =   4815
      _Version        =   131072
      _ExtentX        =   8493
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":3A89
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   240
      TabIndex        =   4
      Top             =   6360
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":3C69
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQ 
      Height          =   990
      Left            =   240
      TabIndex        =   6
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":3E44
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdW 
      Height          =   990
      Left            =   1320
      TabIndex        =   7
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":401C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdE 
      Height          =   990
      Left            =   2400
      TabIndex        =   8
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":41F4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdY 
      Height          =   990
      Left            =   5640
      TabIndex        =   9
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":43CC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdR 
      Height          =   990
      Left            =   3480
      TabIndex        =   10
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":45A4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdT 
      Height          =   990
      Left            =   4560
      TabIndex        =   11
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":477C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdO 
      Height          =   990
      Left            =   8880
      TabIndex        =   12
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":4954
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdI 
      Height          =   990
      Left            =   7800
      TabIndex        =   13
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":4B2C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdU 
      Height          =   990
      Left            =   6720
      TabIndex        =   14
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":4D04
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdP 
      Height          =   990
      Left            =   9960
      TabIndex        =   15
      Top             =   3120
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":4EDC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdA 
      Height          =   990
      Left            =   720
      TabIndex        =   16
      Top             =   4200
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":50B4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdS 
      Height          =   990
      Left            =   1800
      TabIndex        =   17
      Top             =   4200
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":528C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdD 
      Height          =   990
      Left            =   2880
      TabIndex        =   18
      Top             =   4200
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":5464
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdH 
      Height          =   990
      Left            =   6120
      TabIndex        =   19
      Top             =   4200
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":563C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdF 
      Height          =   990
      Left            =   3960
      TabIndex        =   20
      Top             =   4200
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":5814
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdG 
      Height          =   990
      Left            =   5040
      TabIndex        =   21
      Top             =   4200
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":59EC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdL 
      Height          =   990
      Left            =   9360
      TabIndex        =   22
      Top             =   4200
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":5BC4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdK 
      Height          =   990
      Left            =   8280
      TabIndex        =   23
      Top             =   4200
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":5D9C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdJ 
      Height          =   990
      Left            =   7200
      TabIndex        =   24
      Top             =   4200
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":5F74
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdZ 
      Height          =   990
      Left            =   1200
      TabIndex        =   25
      Top             =   5280
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":614C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdX 
      Height          =   990
      Left            =   2280
      TabIndex        =   26
      Top             =   5280
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":6324
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdC 
      Height          =   990
      Left            =   3360
      TabIndex        =   27
      Top             =   5280
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":64FC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdN 
      Height          =   990
      Left            =   6600
      TabIndex        =   28
      Top             =   5280
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":66D4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdV 
      Height          =   990
      Left            =   4440
      TabIndex        =   29
      Top             =   5280
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":68AC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdB 
      Height          =   990
      Left            =   5520
      TabIndex        =   30
      Top             =   5280
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":6A84
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdM 
      Height          =   990
      Left            =   7680
      TabIndex        =   31
      Top             =   5280
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":6C5C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCapsOn 
      Height          =   975
      Left            =   2280
      TabIndex        =   33
      Top             =   6360
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":6E34
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd1 
      Height          =   990
      Left            =   240
      TabIndex        =   34
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":7013
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd2 
      Height          =   990
      Left            =   1320
      TabIndex        =   35
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":71EB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd3 
      Height          =   990
      Left            =   2400
      TabIndex        =   36
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":73C3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd4 
      Height          =   990
      Left            =   3480
      TabIndex        =   37
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":759B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd5 
      Height          =   990
      Left            =   4560
      TabIndex        =   38
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":7773
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd6 
      Height          =   990
      Left            =   5640
      TabIndex        =   39
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":794B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd7 
      Height          =   990
      Left            =   6720
      TabIndex        =   40
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":7B23
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd8 
      Height          =   990
      Left            =   7800
      TabIndex        =   41
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":7CFB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd9 
      Height          =   990
      Left            =   8880
      TabIndex        =   42
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":7ED3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd10 
      Height          =   990
      Left            =   9960
      TabIndex        =   43
      Top             =   2040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":80AB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEqual 
      Height          =   990
      Left            =   9960
      TabIndex        =   44
      Top             =   960
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AlphaPad.frx":8283
   End
   Begin VB.Label lblLegend 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "legend"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1335
      Left            =   6960
      TabIndex        =   45
      Top             =   480
      Visible         =   0   'False
      Width           =   2805
   End
   Begin VB.Label lblCapsOn 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Caps On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   3120
      TabIndex        =   32
      Top             =   360
      Width           =   1140
   End
   Begin VB.Label lblField 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "FieldName"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   240
      TabIndex        =   5
      Top             =   720
      Width           =   1215
   End
End
Attribute VB_Name = "frmAlphaPad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Used for Alpha Pad
Public NumPad_Result As String
Public NumPad_Field As String
Public NumPad_Quit As Boolean
Public NumPad_DisplayFull As Boolean

Private CapsOn As Boolean
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long
Private CurrentValue As String
Private CurrentEntry As Integer

Private Sub cmd1_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("1")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmd2_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("2")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmd3_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("3")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmd4_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("4")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmd5_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("5")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmd6_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("6")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmd7_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("7")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmd8_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("8")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmd9_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("9")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmd10_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("0")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdCapsOn_Click()
CapsOn = Not (CapsOn)
If (CapsOn) Then
    lblCapsOn.Caption = "CAPS ON"
    cmdCapsOn.Text = "CAPS OFF"
Else
    lblCapsOn.Caption = "CAPS OFF"
    cmdCapsOn.Text = "CAPS ON"
End If
End Sub

Private Sub cmdEqual_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("=")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1

End Sub

Private Sub cmdQuit_Click()
NumPad_Quit = True
NumPad_Result = ""
Unload frmAlphaPad
End Sub

Private Sub cmdDelete_Click()
If (CurrentEntry > 1) Then
    CurrentEntry = CurrentEntry - 1
    CurrentValue = Left(CurrentValue, CurrentEntry - 1)
    txtResult.Text = CurrentValue
End If
End Sub

Private Sub cmdDone_Click()
NumPad_Quit = False
NumPad_Result = CurrentValue
If (Trim(CurrentValue) <> Trim(txtResult.Text)) Then
    NumPad_Result = Trim(txtResult.Text)
End If
Unload frmAlphaPad
End Sub

Private Sub cmdSpace_Click()
CurrentValue = CurrentValue + " "
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdA_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("A")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdB_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("B")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdC_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("C")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdD_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("D")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdE_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("E")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdF_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("F")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdG_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("G")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdH_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("H")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdI_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("I")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdJ_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("J")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdK_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("K")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdL_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("L")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdM_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("M")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdN_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("N")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdO_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("O")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdP_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("P")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdQ_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("Q")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdR_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("R")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdS_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("S")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdT_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("T")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdU_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("U")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdV_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("V")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdW_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("W")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdX_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("X")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdY_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("Y")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub cmdZ_Click()
CurrentValue = CurrentValue + SetDisplayCharacter("Z")
txtResult.Text = CurrentValue
CurrentEntry = CurrentEntry + 1
End Sub

Private Sub Form_Load()
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdA.BackColor
BaseTextColor = cmdA.ForeColor
NumPad_Quit = False
CapsOn = True
lblCapsOn.Caption = "CAPS ON"
cmdCapsOn.Text = "CAPS OFF"
frmAlphaPad.Width = 12000
frmAlphaPad.Height = 9000
txtResult.Text = ""
CurrentEntry = 1
NumPad_Result = ""
CurrentValue = ""
lblField.Caption = NumPad_Field
End Sub

Private Function SetDisplayCharacter(TheChar As String) As String
Dim AChar As String
SetDisplayCharacter = TheChar
If (Asc(TheChar) >= 65) And (Asc(TheChar) < 92) Then
    If Not (CapsOn) Then
        AChar = Chr(Asc(TheChar) + 32)
        SetDisplayCharacter = AChar
    End If
End If
End Function
Private Sub txtResult_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    Call cmdDone_Click
End If
End Sub
Public Sub FrmClose()
Call cmdQuit_Click
Unload Me
End Sub
