VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmRxHistory 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9825
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   13005
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "RxHistory.frx":0000
   ScaleHeight     =   9825
   ScaleWidth      =   13005
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSFlexGridLib.MSFlexGrid lstsort 
      Height          =   2775
      Left            =   1920
      TabIndex        =   8
      Top             =   2880
      Visible         =   0   'False
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   4895
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   -2147483643
      GridColor       =   8388608
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox lstZoom 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5820
      ItemData        =   "RxHistory.frx":36C9
      Left            =   960
      List            =   "RxHistory.frx":36CB
      TabIndex        =   7
      Top             =   2400
      Visible         =   0   'False
      Width           =   7575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10080
      TabIndex        =   0
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxHistory.frx":36CD
   End
   Begin fpBtnAtlLibCtl.fpBtn btnInfo 
      Height          =   990
      Left            =   6120
      TabIndex        =   9
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   0   'False
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxHistory.frx":38A8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpr 
      Height          =   990
      Left            =   7560
      TabIndex        =   1
      Top             =   7920
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxHistory.frx":3A83
   End
   Begin MSFlexGridLib.MSFlexGrid lstHx 
      Height          =   4335
      Left            =   240
      TabIndex        =   3
      Top             =   720
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   7646
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   -2147483643
      GridColor       =   8388608
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid lstHxN 
      Height          =   2415
      Left            =   240
      TabIndex        =   4
      Top             =   5400
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   4260
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   -2147483643
      GridColor       =   8388608
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Ocular Drugs"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   240
      TabIndex        =   6
      Top             =   480
      Width           =   1410
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Non-Ocular Drugs"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   240
      TabIndex        =   5
      Top             =   5040
      Width           =   1920
   End
   Begin VB.Label lblHx 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Drug History for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   240
      TabIndex        =   2
      Top             =   120
      Width           =   1830
   End
End
Attribute VB_Name = "frmRxHistory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public ActivityId As Long
Public EditPrevOn As Boolean
Public Drug_Name As String
Public MedId As Integer
Private PatientId As Long
Private m_SortColumnName As String
Private m_SortColumn As Integer
Private m_SortOrder As SortSettings

Private Sub btnInfo_Click()
 Dim RetActivity As PracticeActivity
 Dim ResourceId As Long
 Set RetActivity = New PracticeActivity
 RetActivity.ActivityId = ActivityId
 If (RetActivity.RetrieveActivity) Then
    ResourceId = RetActivity.ActivityResourceId
 End If
 Set RetActivity = Nothing
 Dim ClinicalManager As New ClinicalIntegration.frmInfo
 ClinicalManager.PatientId = PatientId
 ClinicalManager.IsMedList = True
 ClinicalManager.DbObject = IdbConnection
 ClinicalManager.ResourceId = CStr(ResourceId)
 ClinicalManager.AppointmentId = globalExam.iAppointmentId
 Dim OrderString As String
 Dim RS As Recordset
 Dim strArr() As String
 Dim RxCUI As String
 strArr = Split(Drug_Name, " ")
 RxCUI = strArr(0)
 ClinicalManager.DrugName = RxCUI
 OrderString = "Select count (rx.RxNorm) as Count from dbo.patientclinical pc inner join [dbo].[FDBtoRxNorm1] rx on pc.drawfilename = rx.FDBId where clinicaltype = 'A' and pc.findingdetail like 'Rx-8/" + RxCUI + "%'"
 Set RS = GetRS(OrderString)
 If RS.RecordCount > 0 And RS("Count") > 0 Then
   ClinicalManager.ShowDialog
 Else
   MsgBox "Information not Found"
 End If
 btnInfo.Enabled = False
End Sub

Private Sub cmdDone_Click()
    CurrentAction = ""
    Unload frmRxHistory
End Sub

Private Sub cmdImpr_Click()
Dim LDate As String
Dim ApptId As Long
Dim PatId As Long
Dim RetActivity As PracticeActivity
Dim RetrievalActivity As PracticeActivity
PatId = PatientId
ApptId = 0
Set RetActivity = New PracticeActivity
RetActivity.ActivityId = ActivityId
If (RetActivity.RetrieveActivity) Then
    LDate = RetActivity.ActivityDate
    Set RetrievalActivity = New PracticeActivity
    RetrievalActivity.ActivityDate = LDate
    RetrievalActivity.ActivityPatientId = PatId
    RetrievalActivity.ActivityStatus = "DH"
    If (RetrievalActivity.RetrievePreviousActivity) Then
        ApptId = RetrievalActivity.ActivityAppointmentId
        ActivityId = RetrievalActivity.ActivityId
    End If
    Set RetrievalActivity = Nothing
End If
Set RetActivity = Nothing
If (ApptId > 0) And (PatId > 0) And (ActivityId > 0) Then
    frmImpression.EditPreviousOn = False
    frmImpression.ActivityId = ActivityId
    frmImpression.PatientId = PatId
    frmImpression.AppointmentId = ApptId
    If (frmImpression.LoadImpression(True)) Then
        frmImpression.Show 1
    Else
        frmEventMsgs.Header = "No Impressions/Plans"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Public Function LoadHistoryRx(PatId As Long, HstOn As Boolean) As Boolean
Dim DumpOn As Boolean
Dim OverrideOn As Boolean
Dim ProceedOn As Boolean
Dim u As Integer
Dim Y As Integer
Dim g As Integer
Dim CntOcu As Integer, CntNon As Integer
Dim z As Integer, z1 As Integer
Dim i As Integer, k As Integer, j As Integer
Dim TodayDate As String
Dim NowDate As String
Dim DisplayText As String
Dim TestDate1 As String
Dim TestDate2 As String
Dim YDate As String
Dim OtherNote As String
Dim Temp As String, Ref As String
Dim ANote As String, ARef As String
Dim TheStat As String, PrsOn As String
Dim TheDate As String, TheEye As String
Dim TheDrug As String, TheFreq As String
Dim RetPat As Patient
Dim RetNote As PatientNotes
Dim RetCln As PatientClinical
Dim RetAppt As SchedulerAppointment
Dim RetAct As PracticeActivity
Dim fCDrugRx As CDrugRx

LoadHistoryRx = False
TodayDate = ""
Call FormatTodaysDate(TodayDate, False)
PatientId = PatId
lstHx.Clear
DisplayText = "Date      " + vbTab _
            + "Eye" + vbTab _
            + "Drug" + Space(17) + vbTab _
            + "Status " + vbTab _
            + "P" + vbTab _
            + "Frequency" + Space(56)
lstHx.FormatString = DisplayText
lstHx.Rows = 2
CntOcu = 0
DisplayText = "Date      " + vbTab _
            + "Drug" + Space(21) + vbTab _
            + "Status " + vbTab _
            + "Frequency" + Space(56)
lstHxN.FormatString = DisplayText
lstHxN.Rows = 2
CntNon = 0
If (PatId > 0) Then
    Temp = ""
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        Temp = Trim(RetPat.FirstName) + " " + Trim(RetPat.LastName)
    End If
    Set RetPat = Nothing
    lblHx.Caption = "Drug History for " + Temp
    Temp = ""
    Set RetCln = New PatientClinical
    RetCln.PatientId = PatId
    RetCln.AppointmentId = 0
    RetCln.ClinicalType = "A"
    RetCln.EyeContext = ""
    RetCln.Findings = ""
    RetCln.Symptom = ""
    RetCln.ImageDescriptor = ""
    RetCln.ImageInstructions = ""
    If (RetCln.FindPatientClinicalReverse > 0) Then
        i = 1
        While (RetCln.SelectPatientClinical(i))
            If (Left(RetCln.Findings, 2) = "RX") Then
                Ref = Trim(RetCln.Findings)
                DisplayText = Space(95)
                k = InStrPS(Ref, "-1/")
                If (k < 1) Then
                    k = InStrPS(Ref, "-8/")
                Else
                    Call ReplaceCharacters(Ref, "-8/", " ")
                End If
                j = InStrPS(Ref, "-2/")
                If (j > 0) And (j > k) Then
                    TheDrug = Trim(Mid(Ref, k + 3, (j - 3) - k))
                Else
                    If (j > 1) Then
                        TheDrug = Trim(Left(Ref, j - 1))
                    End If
                End If
                
                ProceedOn = True
                NowDate = ""
                Set RetAppt = New SchedulerAppointment
                RetAppt.AppointmentId = RetCln.AppointmentId
                If (RetAppt.RetrieveSchedulerAppointment) Then
                    NowDate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
                End If
                Set RetAppt = Nothing
                
                
                
                
                
''''                TheFreq = ""
''''                k = InStrPS(Ref, "-2/")
''''                j = InStrPS(Ref, "-3/")
''''                If (j > 0) And (k > 0) Then
''''                    Temp = Trim(Mid(Ref, k + 3, (j - 3) - k))
''''                    z = InStrPS(Temp, "[Last")
''''                    If (z > 0) Then
''''                        z1 = InStrPS(z, Temp, "]")
''''                        If (z1 > 0) Then
''''                            Temp = Left(Temp, z - 1) + Mid(Temp, z + 1, Len(Temp) - z)
''''                        End If
''''                    End If
''''                    If (InStrPS(Temp, "(OU)") > 0) Then
''''                        TheEye = "OU"
''''                        Call ReplaceCharacters(Temp, "(OU)", "~")
''''                    ElseIf (InStrPS(Temp, "(OS)") > 0) Then
''''                        TheEye = "OS"
''''                        Call ReplaceCharacters(Temp, "(OS)", "~")
''''                    ElseIf (InStrPS(Temp, "(OD)") > 0) Then
''''                        TheEye = "OD"
''''                        Call ReplaceCharacters(Temp, "(OD)", "~")
''''                    End If
''''                    Call StripCharacters(Temp, "~")
''''                    TheFreq = Trim(Temp)
''''                End If
''''
''''                k = InStrPS(Ref, "-3/")
''''                If (j > 0) And (k > 0) Then
''''                    j = InStrPS(Ref, "-4/")
''''                    Temp = Trim(Mid(Ref, k + 3, (j - 3) - k))
''''                    z = InStrPS(Temp, "[Last")
''''                    If (z > 0) Then
''''                        z1 = InStrPS(z, Temp, "]")
''''                        If (z1 > 0) Then
''''                            Temp = Left(Temp, z - 1) + Mid(Temp, z + 1, Len(Temp) - z)
''''                        End If
''''                    End If
''''                    TheFreq = Trim(Temp)
''''                End If
                
                Set fCDrugRx = New CDrugRx
                fCDrugRx.LoadRxFromDB Ref
                
                TheEye = DisplayEyeRef(fCDrugRx.EyeRef)
                TheFreq = fCDrugRx.Frequency
                
                
' Get Any Notes
                ANote = ""
                Set RetNote = New PatientNotes
                RetNote.NotesAppointmentId = RetCln.AppointmentId
                RetNote.NotesPatientId = RetCln.PatientId
                RetNote.NotesSystem = TheDrug
                If (RetNote.FindNotes > 0) Then
                    u = 1
                    While (RetNote.SelectNotes(u))
                        If Not RetNote.NotesCommentOn Then
                            ANote = ANote + " *"
                        End If
                        ANote = ANote + " " _
                              + Trim(RetNote.NotesText1) + " " _
                              + Trim(RetNote.NotesText2) + " " _
                              + Trim(RetNote.NotesText3) + " " _
                              + Trim(RetNote.NotesText4)
                        ANote = Trim(ANote)
                        u = u + 1
                    Wend
                End If
                Set RetNote = Nothing
                
                If (TheFreq = "(AS PRESCRIBED)") Then
                    TheFreq = ANote
                Else
                    TheFreq = Trim(TheFreq) + " " + ANote
                End If
                TheFreq = Trim(TheFreq)
                
'                OtherNote = ""
'                k = InStrPS(Ref, "-5/")
'                If (k > 0) Then
'                    j = InStrPS(Ref, "-6/")
'                    If (j > 0) Then
'                        OtherNote = Mid(Ref, k + 3, (j - 1) - (k + 2))
'                    End If
'                End If
'                TheFreq = Trim(Trim(TheFreq) + " " + OtherNote)
'                If (Len(TheFreq) < 66) Then
'                    TheFreq = TheFreq + Space(65 - Len(TheFreq))
'                End If
                    
                PrsOn = " "
                If (InStrPS(Ref, "P-7") > 0) Then
                    PrsOn = "Y"
                End If
    
                If (Trim(RetCln.ImageDescriptor) <> "") Then
                    For z = 1 To Len(Trim(RetCln.ImageDescriptor)) Step 11
                        TheStat = "Start  "
                        If (Mid(RetCln.ImageDescriptor, z, 1) = "R") Then
                            TheStat = "Refill "
                        ElseIf (Mid(RetCln.ImageDescriptor, z, 1) = "D") Then
                            TheStat = "Disc   "
                        ElseIf (Mid(RetCln.ImageDescriptor, z, 1) = "P") Then
                            TheStat = "Pdisc  "
                        ElseIf (Mid(RetCln.ImageDescriptor, z, 1) = "C") Then
                            TheStat = "Rvisd  "
                        ElseIf (Mid(RetCln.ImageDescriptor, z, 1) = "X") Then
                            TheStat = "Deleted"
                        ElseIf (Mid(RetCln.ImageDescriptor, z, 1) = "K") Then
                            TheStat = "Cont   "
                        ElseIf (Mid(RetCln.ImageDescriptor, z, 1) = "!") Then
                            TheStat = "Active "
                        ElseIf (Mid(RetCln.ImageDescriptor, z, 1) = "^") Then
                            TheStat = "Cont   "
                        End If
                        TheDate = Mid(RetCln.ImageDescriptor, z + 1, 10)
                        If (z + 11 >= Len(Trim(RetCln.ImageDescriptor))) Then
                            If (Mid(RetCln.ImageDescriptor, z, 1) = "!") And (TheDate = NowDate) Then
                                TheStat = "Start  "
                            End If
                        End If
                        GoSub SetRx
                        If (z + 10 = Len(Trim(RetCln.ImageDescriptor))) Then
                            If (TheDate <> NowDate) Then
                                TheStat = "Start  "
                                TheDate = NowDate
                                GoSub SetRx
                            End If
                        End If
                    Next z
                Else
                    TheDate = NowDate
                    TheStat = "Start  "
                    GoSub SetRx
                End If
            End If
            i = i + 1
        Wend
    End If
    Set RetCln = Nothing
End If
' Sort then Clear Duplicate Dates
Call SortByColumn(0, True)
If (CntOcu > 1) Then
    For i = lstHx.Rows - 1 To 1 Step -1
        If (lstHx.TextMatrix(i, 0) = lstHx.TextMatrix(i - 1, 0)) Then
            lstHx.TextMatrix(i, 0) = ""
        End If
    Next i
End If
SortByColumn_6 lstHx, 2

' Other List
Call SortByColumn(0, False)
If (CntNon > 1) Then
    For i = lstHxN.Rows - 1 To 1 Step -1
        If (lstHxN.TextMatrix(i, 0) = lstHxN.TextMatrix(i - 1, 0)) Then
            lstHxN.TextMatrix(i, 0) = ""
        End If
    Next i
End If
SortByColumn_6 lstHxN, 1



'cmdImpr.Visible = True
'If (CurrentAction <> "") Then
'    cmdImpr.Visible = False
'End If
If (CurrentAction = "--") Then
    If (ActivityId > 0) Then
        NowDate = ""
        Set RetAct = New PracticeActivity
        RetAct.ActivityId = ActivityId
        If (RetAct.RetrieveActivity) Then
            NowDate = Mid(RetAct.ActivityDate, 5, 2) + "/" + Mid(RetAct.ActivityDate, 7, 2) + "/" + Left(RetAct.ActivityDate, 4)
        End If
        Set RetAct = Nothing
        If (NowDate <> "") Then
            DumpOn = False
            g = lstHxN.Rows - 1
            For u = 1 To g
                If (u > g) Then
                    Exit For
                End If
                If (lstHxN.TextMatrix(u, 0) = NowDate) Or ((DumpOn) And (lstHxN.TextMatrix(u, 0) = "")) Then
                    DumpOn = False
                Else
                    DumpOn = True
                End If
                If (DumpOn) Then
                    If u < g Then
                        lstHxN.RemoveItem u
                    End If
                    u = u - 1
                    g = g - 1
                    If (u <= g) Then
                        Exit For
                    End If
                End If
            Next u
            g = lstHx.Rows - 1
            For u = 1 To g
                If (u > g) Then
                    Exit For
                End If
                If (lstHx.TextMatrix(u, 0) = NowDate) Or (Not (DumpOn) And (lstHx.TextMatrix(u, 0) = "")) Then
                    DumpOn = False
                Else
                    DumpOn = True
                End If
                If (DumpOn) Then
                    If u < g Then
                        lstHx.RemoveItem u
                    End If
                    u = u - 1
                    g = g - 1
                    If (u >= g) Then
                        Exit For
                    End If
                End If
            Next u
        End If
    End If
End If
LoadHistoryRx = True
Exit Function
SetRx:
    YDate = TheDate
    Call FormatTodaysDate(YDate, False)
    TestDate1 = Mid(YDate, 7, 4) + Mid(YDate, 1, 2) + Mid(YDate, 4, 2)
    TestDate2 = Mid(TodayDate, 7, 4) + Mid(TodayDate, 1, 2) + Mid(TodayDate, 4, 2)
    If (EditPrevOn) Or (TestDate2 > TestDate1) Then
        If (TheDate = "") Then
            TheDate = YDate
            TheDate = Mid(YDate, 5, 2) + "/" + Mid(YDate, 7, 2) + "/" + Left(YDate, 4)
        End If
        'If (IsDrugOkay(TheDrug) = "O") Or (PrsOn = "Y") Then
        If (IsDrugOkay_6(TheDrug) = "O") Or (PrsOn = "Y") Then
            If (TheStat = "Start  ") And (PrsOn <> "Y") Then
                TheStat = "Active "
            End If
            If (TheStat = "Active ") And (PrsOn = "Y") And (NowDate = Mid(TheDate, 5, 2) + "/" + Mid(TheDate, 7, 2) + "/" + Mid(TheDate, 1, 4)) Then
                TheStat = "Start  "
            End If
            If (TheStat = "Unknown") Then
                TheStat = "       "
                TheFreq = " "
            End If
            DisplayText = TheDate + vbTab _
                        + TheEye + vbTab _
                        + TheDrug + vbTab _
                        + TheStat + vbTab _
                        + PrsOn + vbTab _
                        + TheFreq + vbTab
'
' Need to override the current active Rx with the
' previous status. The previous Rx record holds the
' status and date of the change which needs to be
' displayed forward to the current RX.
'
            OverrideOn = False
            For Y = 1 To lstHx.Rows - 1
                If (TheDate = lstHx.TextMatrix(Y, 0)) And (TheDrug = lstHx.TextMatrix(Y, 2)) And (TheEye = lstHx.TextMatrix(Y, 1)) And ((Trim(TheFreq) = Trim(lstHx.TextMatrix(Y, 5))) Or (Trim(TheStat) = "Rvisd")) Then
                    lstHx.TextMatrix(Y, 3) = TheStat
                    OverrideOn = True
                    Exit For
                End If
            Next Y
            If (Not OverrideOn) Then
                CntOcu = CntOcu + 1
                lstHx.AddItem DisplayText, CntOcu
            End If
        Else
            If (TheStat = "Start  ") Or (TheStat = "       ") Then
                TheStat = "Active "
            End If
            If (TheStat = "Unknown") Then
                TheStat = "       "
                TheFreq = " "
            End If
            DisplayText = TheDate + vbTab _
                        + TheDrug + vbTab _
                        + TheStat + vbTab _
                        + TheFreq + vbTab
'
' Need to override the current active Rx with the
' previous status. The previous Rx record holds the
' status and date of the change which needs to be
' displayed forward to the current RX.
'
            OverrideOn = False
            For Y = 1 To lstHxN.Rows - 1
                If (TheDate = lstHxN.TextMatrix(Y, 0)) And (TheDrug = lstHxN.TextMatrix(Y, 2)) And (TheEye = lstHxN.TextMatrix(Y, 1)) Then
                    lstHxN.TextMatrix(Y, 2) = TheStat
                    OverrideOn = True
                    Exit For
                End If
            Next Y
            If (Not OverrideOn) Then
                CntNon = CntNon + 1
                lstHxN.AddItem DisplayText, CntNon
            End If
        End If
    End If
    Return
End Function

Private Sub SortByColumn(ByVal sort_column As Integer, WhichOne As Boolean)
Dim i As Integer
Dim Temp As String
If (sort_column = 0) Then
    If (WhichOne) Then
        lstHx.Visible = False
        lstHx.Refresh
        If (InStrPS(lstHx.TextMatrix(0, sort_column), "Date") > 0) Then
            For i = 1 To lstHx.Rows - 1
                Temp = lstHx.TextMatrix(i, sort_column)
                If (Trim(Temp) <> "") And (InStrPS(Temp, "/") > 0) Then
                    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
                End If
                lstHx.TextMatrix(i, sort_column) = Temp
                Temp = lstHx.TextMatrix(i, sort_column + 1)
                If (Temp = "OS") Then
                    Temp = "OC"
                End If
                lstHx.TextMatrix(i, sort_column + 1) = Temp
            Next i
        End If
        lstHx.Row = 0
        lstHx.RowSel = 0
        lstHx.col = sort_column + 1
        lstHx.ColSel = sort_column
        m_SortOrder = flexSortStringDescending
        lstHx.Sort = m_SortOrder
        If (m_SortColumn <> sort_column) And (m_SortColumn > 0) Then
            If (m_SortColumnName <> "") Then
                lstHx.TextMatrix(0, m_SortColumn) = m_SortColumnName
            End If
            m_SortColumnName = lstHx.TextMatrix(0, sort_column)
        ElseIf (Trim(m_SortColumnName) = "") Then
            m_SortColumnName = lstHx.TextMatrix(0, sort_column)
        End If
        m_SortColumn = sort_column
        If (InStrPS(m_SortColumnName, "Date") > 0) Then
            For i = 1 To lstHx.Rows - 1
                Temp = lstHx.TextMatrix(i, sort_column)
                If (Trim(Temp) <> "") And (IsNumeric(Temp)) Then
                    Temp = Mid(Temp, 5, 2) + "/" + Mid(Temp, 7, 2) + "/" + Left(Temp, 4)
                    lstHx.TextMatrix(i, sort_column) = Temp
                End If
                Temp = lstHx.TextMatrix(i, sort_column + 1)
                If (Temp = "OC") Then
                    Temp = "OS"
                End If
                lstHx.TextMatrix(i, sort_column + 1) = Temp
            Next i
        End If
        lstHx.Visible = True
    Else
        lstHxN.Visible = False
        lstHxN.Refresh
        If (InStrPS(lstHxN.TextMatrix(0, sort_column), "Date") > 0) Then
            For i = 1 To lstHxN.Rows - 1
                Temp = lstHxN.TextMatrix(i, sort_column)
                If (Trim(Temp) <> "") And (InStrPS(Temp, "/") > 0) Then
                    Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
                End If
                lstHxN.TextMatrix(i, sort_column) = Temp
            Next i
        End If
        lstHxN.Row = 0
        lstHxN.RowSel = 0
        lstHxN.col = sort_column + 1
        lstHxN.ColSel = sort_column
        m_SortOrder = flexSortStringDescending
        lstHxN.Sort = m_SortOrder
        If (m_SortColumn <> sort_column) And (m_SortColumn > 0) Then
            If (m_SortColumnName <> "") Then
                lstHxN.TextMatrix(0, m_SortColumn) = m_SortColumnName
            End If
            m_SortColumnName = lstHxN.TextMatrix(0, sort_column)
        ElseIf (Trim(m_SortColumnName) = "") Then
            m_SortColumnName = lstHxN.TextMatrix(0, sort_column)
        End If
        m_SortColumn = sort_column
        If (InStrPS(m_SortColumnName, "Date") > 0) Then
            For i = 1 To lstHxN.Rows - 1
                Temp = lstHxN.TextMatrix(i, sort_column)
                If (Trim(Temp) <> "") And (IsNumeric(Temp)) Then
                    Temp = Mid(Temp, 5, 2) + "/" + Mid(Temp, 7, 2) + "/" + Left(Temp, 4)
                    lstHxN.TextMatrix(i, sort_column) = Temp
                End If
            Next i
        End If
        lstHxN.Visible = True
    End If
End If
End Sub

Private Function IsDrugOkay(TheName As String) As String
Dim RetDrug As DiagnosisMasterDrugs
IsDrugOkay = ""
If (Trim(TheName) <> "") Then
    If (Mid(TheName, Len(TheName), 1) = "*") Then
        TheName = Left(TheName, Len(TheName) - 1)
    End If
    Set RetDrug = New DiagnosisMasterDrugs
    RetDrug.DrugName = Trim(TheName)
    If (RetDrug.FindDrugbyName > 0) Then
        If (RetDrug.SelectDrug(1)) Then
            IsDrugOkay = Trim(RetDrug.DrugSpecialtyType)
        End If
    End If
    Set RetDrug = Nothing
End If
End Function

Private Function IsDrugOkay_6(TheName As String) As String
IsDrugOkay_6 = ""
If (Trim(TheName) <> "") Then
    If (Mid(TheName, Len(TheName), 1) = "*") Then
        TheName = Left(TheName, Len(TheName) - 1)
    End If
    
    Dim clsDrug As New CDrug
    clsDrug.DisplayName = TheName
    If clsDrug.SelectDrug(1) Then
        If clsDrug.Focus Then IsDrugOkay_6 = "O"
    Else
        Dim RetDrug As DiagnosisMasterDrugs
        Set RetDrug = New DiagnosisMasterDrugs
        RetDrug.DrugName = Trim(TheName)
        If (RetDrug.FindDrugbyName > 0) Then
            If (RetDrug.SelectDrug(1)) Then
                IsDrugOkay_6 = Trim(RetDrug.DrugSpecialtyType)
            End If
        End If
        Set RetDrug = Nothing
    End If
End If
End Function


Private Sub lstHx_Click()
Dim i As Integer
Dim Temp As String
Dim CurrentRow As Integer
Dim CurrentCol As Integer
CurrentRow = lstHx.RowSel
CurrentCol = lstHx.ColSel
If (CurrentRow > 0) And (CurrentCol = 5) Then
    lstZoom.Clear
    Temp = Trim(lstHx.TextMatrix(CurrentRow, 5))
    If (Trim(Temp) <> "") Then
        For i = 1 To Len(Temp) Step 60
            lstZoom.AddItem Trim(Mid(Temp, i, 60))
        Next i
        If (lstZoom.ListCount > 0) Then
            lstZoom.Visible = True
        End If
    End If
ElseIf (CurrentRow > 0) And (CurrentCol >= 0) Then
    Temp = Trim(lstHx.TextMatrix(CurrentRow, 2))
    Drug_Name = Temp
    btnInfo.Enabled = True
End If
End Sub

Private Sub lstHxN_Click()
Dim i As Integer
Dim Temp As String
Dim CurrentRow As Integer
Dim CurrentCol As Integer
CurrentRow = lstHxN.RowSel
CurrentCol = lstHxN.ColSel
If (CurrentRow > 0) And (CurrentCol = 4) Then
    lstZoom.Clear
    Temp = Trim(lstHxN.TextMatrix(CurrentRow, 4))
    If (Trim(Temp) <> "") Then
        For i = 1 To Len(Temp) Step 60
            lstZoom.AddItem Trim(Mid(Temp, i, 60))
        Next i
        If (lstZoom.ListCount > 0) Then
            lstZoom.Visible = True
        End If
    End If
ElseIf (CurrentRow > 0) And (CurrentCol >= 0) Then
Temp = Trim(lstHxN.TextMatrix(CurrentRow, 1))
Drug_Name = Temp
btnInfo.Enabled = True
End If
End Sub

Private Sub SortByColumn_6(lst As MSFlexGrid, drug As Integer)
Dim r As Integer
Dim c As Integer

Dim r2 As Integer

lstsort.Rows = 0
For r = 1 To lst.Rows - 1
    If IsDate(lst.TextMatrix(r, 0)) Then
        If lstsort.Rows > 0 Then
            GoSub SortDrug
        End If
    End If
    If Not lst.TextMatrix(r, 2) = "" Then
        lstsort.Rows = lstsort.Rows + 1
        For c = 1 To lst.Cols - 1
            lstsort.TextMatrix(lstsort.Rows - 1, c) = lst.TextMatrix(r, c)
        Next
    End If
Next
r = r - 1
GoSub SortDrug

Exit Sub
SortDrug:
lstsort.col = drug
lstsort.Sort = flexSortGenericAscending

For r2 = 0 To lstsort.Rows - 1
    For c = 1 To lst.Cols - 1
        lst.TextMatrix(r - lstsort.Rows + r2, c) = lstsort.TextMatrix(r2, c)
    Next
Next

lstsort.Rows = 0
Return
End Sub

Private Sub lstZoom_Click()
    lstZoom.Visible = False
    lstZoom.Clear
End Sub

Private Sub lstZoom_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    lstZoom.Visible = False
    lstZoom.Clear
End Sub
Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
 End Function
