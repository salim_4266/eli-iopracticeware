VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmLetters 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9105
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12060
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "Letters.frx":0000
   ScaleHeight     =   9105
   ScaleWidth      =   12060
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt1 
      Height          =   870
      Left            =   375
      TabIndex        =   0
      Top             =   960
      Width           =   2040
      _Version        =   131072
      _ExtentX        =   3598
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt2 
      Height          =   855
      Left            =   360
      TabIndex        =   1
      Top             =   1920
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":38AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt3 
      Height          =   855
      Left            =   360
      TabIndex        =   2
      Top             =   2880
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":3A8B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt4 
      Height          =   855
      Left            =   360
      TabIndex        =   3
      Top             =   3840
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":3C6C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   990
      Left            =   2640
      TabIndex        =   4
      Top             =   6720
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":3E4D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRefDr 
      Height          =   1575
      Left            =   5160
      TabIndex        =   5
      Top             =   960
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":402C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPCPDr 
      Height          =   1455
      Left            =   5160
      TabIndex        =   6
      Top             =   2640
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   2566
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":422A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLater 
      Height          =   990
      Left            =   5160
      TabIndex        =   7
      Top             =   4200
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":4411
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect 
      Height          =   990
      Left            =   5160
      TabIndex        =   8
      Top             =   5280
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":45F8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   5160
      TabIndex        =   9
      Top             =   6720
      Width           =   1560
      _Version        =   131072
      _ExtentX        =   2752
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":47E5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10200
      TabIndex        =   10
      Top             =   6720
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":49C4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   990
      Left            =   360
      TabIndex        =   13
      Top             =   6720
      Width           =   2040
      _Version        =   131072
      _ExtentX        =   3598
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":4BA3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt5 
      Height          =   855
      Left            =   360
      TabIndex        =   14
      Top             =   4800
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":4D86
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt6 
      Height          =   855
      Left            =   360
      TabIndex        =   15
      Top             =   5760
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":4F67
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt7 
      Height          =   855
      Left            =   2640
      TabIndex        =   16
      Top             =   960
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":5148
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt8 
      Height          =   855
      Left            =   2640
      TabIndex        =   17
      Top             =   1920
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":5329
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt9 
      Height          =   855
      Left            =   2640
      TabIndex        =   18
      Top             =   2880
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":550A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt10 
      Height          =   855
      Left            =   2640
      TabIndex        =   19
      Top             =   3840
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":56EB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt11 
      Height          =   855
      Left            =   2640
      TabIndex        =   20
      Top             =   4800
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":58CD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt12 
      Height          =   855
      Left            =   2640
      TabIndex        =   21
      Top             =   5760
      Width           =   2055
      _Version        =   131072
      _ExtentX        =   3625
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":5AAF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCCRefDr 
      Height          =   1575
      Left            =   8520
      TabIndex        =   22
      Top             =   960
      Visible         =   0   'False
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":5C91
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCCPCPDr 
      Height          =   1455
      Left            =   8520
      TabIndex        =   23
      Top             =   2640
      Visible         =   0   'False
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   2566
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":5E94
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCCLater 
      Height          =   990
      Left            =   8520
      TabIndex        =   24
      Top             =   4200
      Visible         =   0   'False
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":6080
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCCSelect 
      Height          =   990
      Left            =   8520
      TabIndex        =   25
      Top             =   5280
      Visible         =   0   'False
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":626C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCCPat 
      Height          =   990
      Left            =   6960
      TabIndex        =   26
      Top             =   6720
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Letters.frx":645E
   End
   Begin VB.Label lblField2 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Select choices, then press DONE"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3360
      TabIndex        =   12
      Top             =   480
      Width           =   3765
   End
   Begin VB.Label lblField1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Send consultation letter to whom ?"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   2880
      TabIndex        =   11
      Top             =   120
      Width           =   5415
   End
End
Attribute VB_Name = "frmLetters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public LetterName As String
Public LetterDestination As String
Public LetterPCP As String
Public LetterPCPId As Long
Public LetterRef As String
Public LetterRefId As Long
Public LetterOtherId As Long
Public LetterDestinationCCpat As String
Public LetterDestinationCCpcp As String
Public LetterCCPCP As String
Public LetterCCPCPId As Long
Public LetterDestinationCCref As String
Public LetterCCRef As String
Public LetterCCRefId As Long
Public LetterDestinationCCoth As String
Public LetterCCOtherId As Long

Private CCOn As Boolean
Private SetBackColor As Long
Private SetForeColor As Long
Private BaseBackColor As Long
Private BaseForeColor As Long
Private DisplayType As String
Private CurrentIndex As Integer
Private TotalAppts As Long

Private Sub ClearAppt(IType As Boolean)
cmdAppt1.BackColor = BaseBackColor
cmdAppt1.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt1.Text = ""
    cmdAppt1.Visible = False
End If
cmdAppt2.BackColor = BaseBackColor
cmdAppt2.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt2.Text = ""
    cmdAppt2.Visible = False
End If
cmdAppt3.BackColor = BaseBackColor
cmdAppt3.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt3.Text = ""
    cmdAppt3.Visible = False
End If
cmdAppt4.BackColor = BaseBackColor
cmdAppt4.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt4.Text = ""
    cmdAppt4.Visible = False
End If
cmdAppt5.BackColor = BaseBackColor
cmdAppt5.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt5.Text = ""
    cmdAppt5.Visible = False
End If
cmdAppt6.BackColor = BaseBackColor
cmdAppt6.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt6.Text = ""
    cmdAppt6.Visible = False
End If
cmdAppt7.BackColor = BaseBackColor
cmdAppt7.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt7.Text = ""
    cmdAppt7.Visible = False
End If
cmdAppt8.BackColor = BaseBackColor
cmdAppt8.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt8.Text = ""
    cmdAppt8.Visible = False
End If
cmdAppt9.BackColor = BaseBackColor
cmdAppt9.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt9.Text = ""
    cmdAppt9.Visible = False
End If
cmdAppt10.BackColor = BaseBackColor
cmdAppt10.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt10.Text = ""
    cmdAppt10.Visible = False
End If
cmdAppt11.BackColor = BaseBackColor
cmdAppt11.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt11.Text = ""
    cmdAppt11.Visible = False
End If
cmdAppt12.BackColor = BaseBackColor
cmdAppt12.ForeColor = BaseForeColor
If (IType) Then
    cmdAppt12.Text = ""
    cmdAppt12.Visible = False
End If
End Sub

Private Sub cmdCCLater_Click()
If (cmdCCLater.BackColor = BaseBackColor) Then
    LetterDestinationCCpcp = "-"
    LetterDestinationCCref = "-"
    LetterDestinationCCoth = "-"
    cmdCCLater.BackColor = SetBackColor
    cmdCCLater.ForeColor = SetForeColor
    cmdCCSelect.Text = "CC - Select a Physician"
    cmdCCSelect.BackColor = BaseBackColor
    cmdCCSelect.ForeColor = BaseForeColor
    cmdCCPCPDr.BackColor = BaseBackColor
    cmdCCPCPDr.ForeColor = BaseForeColor
    cmdCCRefDr.BackColor = BaseBackColor
    cmdCCRefDr.ForeColor = BaseForeColor
Else
    LetterDestinationCCpcp = ""
    LetterDestinationCCref = ""
    LetterDestinationCCoth = ""
    cmdCCLater.BackColor = BaseBackColor
    cmdCCLater.ForeColor = BaseForeColor
End If
End Sub

Private Sub cmdCCPat_Click()
If (cmdCCPat.BackColor = BaseBackColor) Then
    LetterDestinationCCpat = "CCPAT"
    cmdCCPat.BackColor = SetBackColor
    cmdCCPat.ForeColor = SetForeColor
Else
    LetterDestinationCCpat = ""
    cmdCCPat.BackColor = BaseBackColor
    cmdCCPat.ForeColor = BaseForeColor
End If
End Sub

Private Sub cmdCCPCPDr_Click()
Dim i As Integer
If (cmdCCPCPDr.BackColor = BaseBackColor) Then
    i = InStrPS(cmdCCPCPDr.Text, ":")
    LetterDestinationCCpcp = "(" + Trim(cmdCCPCPDr.Tag) + ")"
    cmdCCPCPDr.BackColor = SetBackColor
    cmdCCPCPDr.ForeColor = SetForeColor
    cmdCCSelect.Text = "Select a Physician"
    cmdCCSelect.BackColor = BaseBackColor
    cmdCCSelect.ForeColor = BaseForeColor
    cmdCCLater.BackColor = BaseBackColor
    cmdCCLater.ForeColor = BaseForeColor
Else
    LetterDestinationCCpcp = ""
    cmdCCPCPDr.BackColor = BaseBackColor
    cmdCCPCPDr.ForeColor = BaseForeColor
End If
End Sub

Private Sub cmdCCRefDr_Click()
Dim i As Integer
If (cmdCCRefDr.BackColor = BaseBackColor) Then
    i = InStrPS(cmdCCRefDr.Text, ":")
    LetterDestinationCCref = "(" + Trim(cmdCCRefDr.Tag) + ")"
    cmdCCRefDr.BackColor = SetBackColor
    cmdCCRefDr.ForeColor = SetForeColor
    cmdCCSelect.Text = "CC - Select a Physician"
    cmdCCSelect.BackColor = BaseBackColor
    cmdCCSelect.ForeColor = BaseForeColor
    cmdCCLater.BackColor = BaseBackColor
    cmdCCLater.ForeColor = BaseForeColor
Else
    LetterDestinationCCref = ""
    cmdCCRefDr.BackColor = BaseBackColor
    cmdCCRefDr.ForeColor = BaseForeColor
End If
End Sub

Private Sub cmdCCSelect_Click()
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.NumPad_Field = "Physician Name"
frmAlphaPad.NumPad_Result = ""
frmAlphaPad.NumPad_Quit = False
frmAlphaPad.Show 1
If Not (frmAlphaPad.NumPad_Quit) Then
    FListSelect.ApptPeriod = ""
    FListSelect.ApptTypeId = 0
    FListSelect.ApptTypeName = Trim(frmAlphaPad.NumPad_Result)
    If (FListSelect.LoadAppt("Q", True)) Then
        FListSelect.Show 1
        If (Trim(FListSelect.ApptTypeName) <> "") Then
            LetterDestinationCCoth = "(" + Trim(str(FListSelect.ApptTypeId)) + ")"
            cmdCCSelect.Text = "CC - Select a Physician: " + Trim(FListSelect.ApptTypeName) + " " + LetterDestinationCCoth
            cmdCCSelect.BackColor = SetBackColor
            cmdCCSelect.ForeColor = SetForeColor
            cmdCCLater.BackColor = BaseBackColor
            cmdCCLater.ForeColor = BaseForeColor
        Else
            LetterDestinationCCoth = ""
            cmdCCSelect.Text = "CC - Select a Physician"
            cmdCCSelect.BackColor = BaseBackColor
            cmdCCSelect.ForeColor = BaseForeColor
        End If
    End If
End If
End Sub

Private Sub cmdQuit_Click()
LetterName = ""
LetterDestination = ""
LetterDestinationCCpcp = ""
LetterDestinationCCref = ""
LetterDestinationCCpat = ""
LetterDestinationCCoth = ""
Unload frmLetters
End Sub

Private Sub cmdDone_Click()
If (Trim(LetterName) <> "") Then
    If (Trim(LetterDestination) = "") Then
        LetterDestination = "-"
    End If
End If
Unload frmLetters
End Sub

Private Sub cmdNext_Click()
If (CurrentIndex > TotalAppts) Then
    CurrentIndex = 1
End If
Call LoadLetters(DisplayType, False)
End Sub

Private Sub cmdPrev_Click()
CurrentIndex = CurrentIndex - 24
If (CurrentIndex < 1) Then
    CurrentIndex = 1
End If
Call LoadLetters(DisplayType, False)
End Sub

Private Sub cmdLater_Click()
If (cmdLater.BackColor = BaseBackColor) Then
    LetterDestination = "-"
    cmdLater.BackColor = SetBackColor
    cmdLater.ForeColor = SetForeColor
    cmdSelect.Text = "Select a Physician"
    cmdSelect.BackColor = BaseBackColor
    cmdSelect.ForeColor = BaseForeColor
    cmdPCPDr.BackColor = BaseBackColor
    cmdPCPDr.ForeColor = BaseForeColor
    cmdRefDr.BackColor = BaseBackColor
    cmdRefDr.ForeColor = BaseForeColor
Else
    LetterDestination = ""
    cmdLater.BackColor = BaseBackColor
    cmdLater.ForeColor = BaseForeColor
End If
End Sub

Private Sub cmdRefDr_Click()
Dim i As Integer
If (cmdRefDr.BackColor = BaseBackColor) Then
    i = InStrPS(cmdRefDr.Text, ":")
    LetterDestination = Trim(Mid(cmdRefDr.Text, i + 1, Len(cmdRefDr.Text) - i)) + "(" + Trim(str(LetterRefId)) + ")"
    cmdRefDr.BackColor = SetBackColor
    cmdRefDr.ForeColor = SetForeColor
    cmdSelect.Text = "Select a Physician"
    cmdSelect.BackColor = BaseBackColor
    cmdSelect.ForeColor = BaseForeColor
    cmdPCPDr.BackColor = BaseBackColor
    cmdPCPDr.ForeColor = BaseForeColor
    cmdLater.BackColor = BaseBackColor
    cmdLater.ForeColor = BaseForeColor
    
Else
    LetterDestination = ""
    cmdRefDr.BackColor = BaseBackColor
    cmdRefDr.ForeColor = BaseForeColor
End If
End Sub

Private Sub cmdPCPDr_Click()
Dim i As Integer
If (cmdPCPDr.BackColor = BaseBackColor) Then
    i = InStrPS(cmdPCPDr.Text, ":")
    LetterDestination = Trim(Mid(cmdPCPDr.Text, i + 1, Len(cmdPCPDr.Text) - i)) + "(" + Trim(str(LetterPCPId)) + ")"
    cmdPCPDr.BackColor = SetBackColor
    cmdPCPDr.ForeColor = SetForeColor
    cmdSelect.Text = "Select a Physician"
    cmdSelect.BackColor = BaseBackColor
    cmdSelect.ForeColor = BaseForeColor
    cmdRefDr.BackColor = BaseBackColor
    cmdRefDr.ForeColor = BaseForeColor
    cmdLater.BackColor = BaseBackColor
    cmdLater.ForeColor = BaseForeColor
Else
    LetterDestination = ""
    cmdPCPDr.BackColor = BaseBackColor
    cmdPCPDr.ForeColor = BaseForeColor
End If
End Sub

Private Sub cmdAppt1_Click()
Call SetItem(cmdAppt1)
End Sub

Private Sub cmdAppt2_Click()
Call SetItem(cmdAppt2)
End Sub

Private Sub cmdAppt3_Click()
Call SetItem(cmdAppt3)
End Sub

Private Sub cmdAppt4_Click()
Call SetItem(cmdAppt4)
End Sub

Private Sub cmdAppt5_Click()
Call SetItem(cmdAppt5)
End Sub

Private Sub cmdAppt6_Click()
Call SetItem(cmdAppt6)
End Sub

Private Sub cmdAppt7_Click()
Call SetItem(cmdAppt7)
End Sub

Private Sub cmdAppt8_Click()
Call SetItem(cmdAppt8)
End Sub

Private Sub cmdAppt9_Click()
Call SetItem(cmdAppt9)
End Sub

Private Sub cmdAppt10_Click()
Call SetItem(cmdAppt10)
End Sub

Private Sub cmdAppt11_Click()
Call SetItem(cmdAppt11)
End Sub

Private Sub cmdAppt12_Click()
Call SetItem(cmdAppt12)
End Sub

Private Sub SetItem(AButton As fpBtn)
If (AButton.BackColor = BaseBackColor) Then
    Call ClearAppt(False)
    AButton.BackColor = SetBackColor
    AButton.ForeColor = SetForeColor
    LetterName = Trim(AButton.Text)
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseForeColor
    LetterName = ""
End If
End Sub

Public Function LoadLetters(TheType As String, Init As Boolean) As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim DisplayText As String
Dim DontCare As Boolean
Dim GoodDr As Boolean
Dim RetCode As PracticeCodes
Dim RetrieveReferral As PracticeVendors
LoadLetters = False
CCOn = True
DisplayType = TheType
If (Init) Then
'    LetterPCP = ""
'    LetterPCPId = 0
'    LetterRef = ""
'    LetterRefId = 0
'    LetterDestinationCCpcp = ""
'    LetterCCPCP = ""
'    LetterCCPCPId = 0
'    LetterDestinationCCref = ""
'    LetterCCRef = ""
'    LetterCCRefId = 0
    LetterName = ""
    LetterDestination = ""
    LetterDestinationCCoth = ""
    LetterCCOtherId = 0
    LetterOtherId = 0
    LetterDestinationCCpat = ""
    CurrentIndex = 1
    'BaseBackColor = cmdAppt1.BackColor
    BaseBackColor = 12688201
    BaseForeColor = cmdAppt1.ForeColor
    SetBackColor = 14745312
    SetForeColor = 0
End If
LetterOtherId = 0
cmdDone.Visible = True
Call ClearAppt(True)
cmdPCPDr.Visible = False
cmdRefDr.Visible = False
cmdCCPCPDr.Visible = False
cmdCCRefDr.Visible = False
cmdCCSelect.Visible = CCOn
cmdCCPat.Visible = CCOn
cmdCCLater.Visible = CCOn
lblField1.Caption = "Send referral letter to whom ?"
If (DisplayType = "C") Then
    lblField1.Caption = "Send consultation letter to whom ?"
End If
If (Trim(LetterPCP) <> "") Then
    cmdPCPDr.Visible = True
    cmdPCPDr.Text = "PCP: " + Trim(LetterPCP)
    cmdPCPDr.Tag = Trim(str(LetterPCPId))
    cmdCCPCPDr.Visible = CCOn
    cmdCCPCPDr.Text = "CC - PCP: " + Trim(LetterPCP)
    cmdCCPCPDr.Tag = Trim(str(LetterPCPId))
End If
If (Trim(LetterRef) <> "") Then
    cmdRefDr.Visible = True
    cmdRefDr.Text = "Referring Dr: " + Trim(LetterRef)
    cmdRefDr.Tag = Trim(str(LetterRefId))
    cmdCCRefDr.Visible = CCOn
    cmdCCRefDr.Text = "CC - Referring Dr: " + Trim(LetterRef)
    cmdCCRefDr.Tag = Trim(str(LetterRefId))
End If
' Letters To Select From
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "ReferralLetters"
If (DisplayType = "C") Then
    RetCode.ReferenceType = "ConsultationLetters"
End If
TotalAppts = RetCode.FindCode
If (TotalAppts > 0) Then
    LoadLetters = True
    i = CurrentIndex
    ButtonCnt = 1
    While (RetCode.SelectCode(i)) And (ButtonCnt < 13)
        DisplayText = Trim(RetCode.ReferenceCode)
        If (ButtonCnt = 1) Then
            cmdAppt1.Text = DisplayText
            cmdAppt1.Tag = "0"
            cmdAppt1.Visible = True
        ElseIf (ButtonCnt = 2) Then
            cmdAppt2.Text = DisplayText
            cmdAppt2.Tag = "0"
            cmdAppt2.Visible = True
        ElseIf (ButtonCnt = 3) Then
            cmdAppt3.Text = DisplayText
            cmdAppt3.Tag = "0"
            cmdAppt3.Visible = True
        ElseIf (ButtonCnt = 4) Then
            cmdAppt4.Text = DisplayText
            cmdAppt4.Tag = "0"
            cmdAppt4.Visible = True
        ElseIf (ButtonCnt = 5) Then
            cmdAppt5.Text = DisplayText
            cmdAppt5.Tag = "0"
            cmdAppt5.Visible = True
        ElseIf (ButtonCnt = 6) Then
            cmdAppt6.Text = DisplayText
            cmdAppt6.Tag = "0"
            cmdAppt6.Visible = True
        ElseIf (ButtonCnt = 7) Then
            cmdAppt7.Text = DisplayText
            cmdAppt7.Tag = "0"
            cmdAppt7.Visible = True
        ElseIf (ButtonCnt = 8) Then
            cmdAppt8.Text = DisplayText
            cmdAppt8.Tag = "0"
            cmdAppt8.Visible = True
        ElseIf (ButtonCnt = 9) Then
            cmdAppt9.Text = DisplayText
            cmdAppt9.Tag = "0"
            cmdAppt9.Visible = True
        ElseIf (ButtonCnt = 10) Then
            cmdAppt10.Text = DisplayText
            cmdAppt10.Tag = "0"
            cmdAppt10.Visible = True
        ElseIf (ButtonCnt = 11) Then
            cmdAppt11.Text = DisplayText
            cmdAppt11.Tag = "0"
            cmdAppt11.Visible = True
        ElseIf (ButtonCnt = 12) Then
            cmdAppt12.Text = DisplayText
            cmdAppt12.Tag = "0"
            cmdAppt12.Visible = True
        End If
        i = i + 1
        ButtonCnt = ButtonCnt + 1
    Wend
    CurrentIndex = i
End If
Set RetCode = Nothing
cmdNext.Visible = False
cmdPrev.Visible = False
If (TotalAppts > 12) Then
    cmdNext.Visible = True
    cmdPrev.Visible = True
End If
End Function

Private Sub cmdSelect_Click()
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.NumPad_Field = "Physician Name"
frmAlphaPad.NumPad_Result = ""
frmAlphaPad.NumPad_Quit = False
frmAlphaPad.Show 1
If Not (frmAlphaPad.NumPad_Quit) Then
    FListSelect.ApptPeriod = ""
    FListSelect.ApptTypeId = 0
    FListSelect.ApptTypeName = Trim(frmAlphaPad.NumPad_Result)
    If (FListSelect.LoadAppt("Q", True)) Then
        FListSelect.Show 1
        If (Trim(FListSelect.ApptTypeName) <> "") Then
        
            Dim VenID As Long
            VenID = Trim(str(FListSelect.ApptTypeId))
            Dim VenName As String
            Dim RetVen As New PracticeVendors
            RetVen.VendorId = VenID
            If (RetVen.RetrieveVendor) Then
                VenName = Trim(RetVen.VendorName)
            End If
            Set RetVen = Nothing

            'LetterDestination = Trim(FListSelect.ApptTypeName) + "(" + Trim(Str(FListSelect.ApptTypeId)) + ")"
            LetterDestination = VenName & "(" & VenID & ")"
            cmdSelect.Text = "Select a Physician: " + LetterDestination
            cmdSelect.BackColor = SetBackColor
            cmdSelect.ForeColor = SetForeColor
' Clear up others
            cmdLater.BackColor = BaseBackColor
            cmdLater.ForeColor = BaseForeColor
            cmdPCPDr.BackColor = BaseBackColor
            cmdPCPDr.ForeColor = BaseForeColor
            cmdRefDr.BackColor = BaseBackColor
            cmdRefDr.ForeColor = BaseForeColor
        Else
            LetterDestination = ""
            cmdSelect.Text = "Select a Physician"
            cmdSelect.BackColor = BaseBackColor
            cmdSelect.ForeColor = BaseForeColor
        End If
    End If
End If
End Sub
Public Function FrmClose()
 Call cmdQuit_Click
 Unload Me
End Function
