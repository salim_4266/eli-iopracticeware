VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{C1AD690C-829F-4862-9CA2-61B9A6A815E4}#1.0#0"; "twnpro3.dll"
Begin VB.Form frmProcessScan 
   BackColor       =   &H00505050&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Process Scan"
   ClientHeight    =   2925
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6450
   ForeColor       =   &H00A39B6D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "ProcessScan.frx":0000
   ScaleHeight     =   2925
   ScaleWidth      =   6450
   StartUpPosition =   2  'CenterScreen
   Begin fpBtnAtlLibCtl.fpBtn cmdStart 
      Height          =   1695
      Left            =   4320
      TabIndex        =   0
      Top             =   1080
      Width           =   1995
      _Version        =   131072
      _ExtentX        =   3519
      _ExtentY        =   2990
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ProcessScan.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   1695
      Left            =   120
      TabIndex        =   1
      Top             =   1080
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   2990
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ProcessScan.frx":3911
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMulti 
      Height          =   1695
      Left            =   2160
      TabIndex        =   3
      Top             =   1080
      Width           =   1995
      _Version        =   131072
      _ExtentX        =   3519
      _ExtentY        =   2990
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ProcessScan.frx":3B44
   End
   Begin TWNPRO3LibCtl.TwainPRO TwainPRO1 
      Left            =   240
      Top             =   120
      ErrStr          =   "6FN900S0GEP-IB003APWTP"
      ErrCode         =   1512253461
      ErrInfo         =   116168247
      _cx             =   847
      _cy             =   847
      Caption         =   ""
      ForeColor       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ProductName     =   ""
      ProductFamily   =   ""
      Manufacturer    =   ""
      VersionInfo     =   ""
      MaxImages       =   -1
      ShowUI          =   -1  'True
      SaveJPGLumFactor=   32
      SaveJPGChromFactor=   36
      SaveJPGSubSampling=   2
      SaveJPGProgressive=   0   'False
      PICPassword     =   ""
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveTIFCompression=   0
      SaveMultiPage   =   0   'False
      CaptionLeft     =   0
      CaptionTop      =   0
      CaptionWidth    =   0
      CaptionHeight   =   0
      ShadowText      =   -1  'True
      ClipCaption     =   0   'False
      HAlign          =   0
      VAlign          =   0
      EnableExtCaps   =   -1  'True
      CloseOnCancel   =   -1  'True
      TransferMode    =   0
   End
   Begin VB.Label lblPrint 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Scanning FileName"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   2025
      TabIndex        =   2
      Top             =   240
      Visible         =   0   'False
      Width           =   2325
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmProcessScan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public QuitOn As Boolean
Public ScanFileName As String
Private MultiOn As Boolean
Private ImageCount As Integer
Private CurrentCount As Integer
Private ScanningInProgress As Boolean

Private Sub cmdQuit_Click()
On Error GoTo UI_ErrorHandler
QuitOn = True
If (ScanningInProgress) Then
    Call TwainPRO1.CloseSession
End If
Unload frmProcessScan
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
    Unload frmProcessScan
End Sub

Private Sub cmdMulti_Click()
On Error GoTo UI_ErrorHandler
Dim TempName As String
Dim StartOn As Boolean
Call TwainPRO1.OpenDSM
Call TwainPRO1.SetDataSource(0)
TwainPRO1.SaveMultiPage = True
ScanFileName = Left(ScanFileName, Len(ScanFileName) - 3) + "tif"
TwainPRO1.SaveTIFCompression = TWTIF_CCITTFAX4
StartOn = False
MultiOn = True
QuitOn = False
ScanningInProgress = True
While (ScanningInProgress)
    lblPrint.Caption = "Scanning"
    lblPrint.Visible = True
    DoEvents
    If Not (StartOn) Then
        StartOn = True
        Call TwainPRO1.StartSession
    End If
    If (TwainPRO1.PendingXfers = -1) Then
        ScanningInProgress = False
    End If
Wend
Call TwainPRO1.CloseSession
TempName = ScanFileName
Call ConvertTIFToPDF(TempName)
Unload frmProcessScan
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
    If (ScanningInProgress) Then
        ScanningInProgress = False
        If (StartOn) Then
            StartOn = False
            Call TwainPRO1.CloseSession
            If (ImageCount < 1) Then
                QuitOn = True
            End If
        Else
            Call TwainPRO1.CloseSession
            QuitOn = True
        End If
        TempName = ScanFileName
        Call ConvertTIFToPDF(TempName)
    End If
    Unload frmProcessScan
End Sub

Private Sub cmdStart_Click()
On Error GoTo UI_ErrorHandler
Dim StartOn As Boolean
Dim TempName As String
Call TwainPRO1.OpenDSM
Call TwainPRO1.SetDataSource(0)
TwainPRO1.SaveMultiPage = True
StartOn = False
QuitOn = False
MultiOn = False
TwainPRO1.SaveMultiPage = True
If (InStrPS(UCase(ScanFileName), ".TIF") <> 0) Then
    MultiOn = True
    TwainPRO1.SaveTIFCompression = TWTIF_CCITTFAX4
End If
ScanningInProgress = True
While (ScanningInProgress)
    lblPrint.Caption = "Scanning"
    lblPrint.Visible = True
    CurrentCount = CurrentCount + 1
    DoEvents
    If Not (StartOn) Then
        StartOn = True
        Call TwainPRO1.StartSession
    End If
    If (TwainPRO1.PendingXfers = -1) Then
        ScanningInProgress = False
    ElseIf (ImageCount < 1) Then
        ScanningInProgress = False
        QuitOn = True
    ElseIf (CurrentCount <> ImageCount) Then
        ScanningInProgress = False
        QuitOn = True
    End If
Wend
Call TwainPRO1.CloseSession
TempName = ScanFileName
Call ConvertTIFToPDF(TempName)
Unload frmProcessScan
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
    If (ScanningInProgress) Then
        ScanningInProgress = False
        If (StartOn) Then
            StartOn = False
            Call TwainPRO1.CloseSession
            If (ImageCount < 1) Then
                QuitOn = True
            End If
        Else
            Call TwainPRO1.CloseSession
            QuitOn = True
        End If
        TempName = ScanFileName
        Call ConvertTIFToPDF(TempName)
    End If
    Unload frmProcessScan
End Sub

Private Sub Form_Load()
QuitOn = False
lblPrint.Caption = "Ready to Scan "
lblPrint.Visible = True
ScanningInProgress = False
ImageCount = 0
CurrentCount = 0
MultiOn = False
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
QuitOn = False
lblPrint.Caption = "Ready to Scan "
lblPrint.Visible = True
If (ScanningInProgress) Then
    Call TwainPRO1.CloseSession
End If
ScanningInProgress = False
ImageCount = 0
CurrentCount = 0
MultiOn = False
End Sub

Private Sub TwainPRO1_PostScan(Cancel As Boolean)
Dim k As Integer
Dim ImgCnt As String
Dim TempFile As String
TempFile = ScanFileName
If (MultiOn) Then
    TwainPRO1.SaveTIFCompression = TWTIF_CCITTFAX4
    Call TwainPRO1.SaveFile(ScanFileName)
    If (TwainPRO1.PendingXfers = -1) Or (Cancel) Then
        ScanningInProgress = False
    End If
Else
    If (ImageCount > 0) Then
        ImgCnt = Trim(str(ImageCount))
        If (ImageCount < 10) Then
            ImgCnt = "0" + ImgCnt
        End If
        k = InStrPS(ScanFileName, ".")
        If (k > 0) Then
            TempFile = Left(ScanFileName, k - 1) + "-" + ImgCnt + Mid(ScanFileName, k, Len(ScanFileName) - (k - 1))
        End If
    End If
    Call TwainPRO1.SaveFile(TempFile)
    If (TwainPRO1.PendingXfers = -1) Or (Cancel) Then
        ScanningInProgress = False
    End If
    ImageCount = ImageCount + 1
End If
End Sub

Private Sub TwainPRO1_PreScan(Cancel As Boolean)
If (TwainPRO1.PendingXfers = -1) Or (Cancel) Then
    ScanningInProgress = False
End If
End Sub

Private Sub ConvertTIFToPDF(FileName As String)
Dim i As Integer
Dim MyCnt As Integer
Dim CurrentFileSize As Long
Dim NewFile As String
If (InStrPS(UCase(FileName), ".TIF") > 0) Then
    i = InStrPS(FileName, ".")
    If (i > 0) Then
        NewFile = Left(FileName, i) + "pdf"
    Else
        i = Len(FileName)
        NewFile = FileName + ".pdf"
    End If
    If (Dir(NewFile) <> "") Then
        FM.Kill NewFile
    End If
    If (Dir(NewFile) = "") Then
'
' calculate wait time, convert factor is 20 seconds per meg
'
        CurrentFileSize = FM.GetFileLength(FileName)
        CurrentFileSize = (CurrentFileSize / 1024000) * 10
        If (CurrentFileSize < 0) Then
            CurrentFileSize = 100
        End If
' it is global now
        pdf.ConvertTiffToPDF FileName, NewFile
'
' got to sleep to let it do its thing until the pdf file is built
'
        MyCnt = 0
        While (Dir(NewFile) = "") And (MyCnt < CurrentFileSize)
            lblPrint.Caption = "Converting TIF to PDF" + str(MyCnt) + " of" + str(CurrentFileSize)
            lblPrint.Visible = True
            DoEvents
            Call DoSleep(2000)
            MyCnt = MyCnt + 1
        Wend
' it is a global now
        If (Dir(NewFile) <> "") Then
            FM.Kill FileName
            FileName = NewFile
        End If
' add an additional sleep to make sure the Headers get updated
        Call DoSleep(4000)
        ScanFileName = FileName
    End If
End If
End Sub
