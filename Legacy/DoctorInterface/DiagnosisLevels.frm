VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmDiagnosisLevels 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9225
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12315
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "DiagnosisLevels.frx":0000
   ScaleHeight     =   9225
   ScaleWidth      =   12315
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdPrevDx 
      Height          =   720
      Left            =   3360
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1270
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis1 
      Height          =   840
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1482
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":48DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis10 
      Height          =   855
      Left            =   6240
      TabIndex        =   10
      Top             =   3840
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":5AF5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiffDiag 
      Height          =   990
      Left            =   240
      TabIndex        =   14
      Top             =   7800
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":6D10
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   990
      Left            =   1800
      TabIndex        =   15
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":6EF8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10320
      TabIndex        =   19
      Top             =   7800
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":70D4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis2 
      Height          =   840
      Left            =   240
      TabIndex        =   2
      Top             =   1920
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1482
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":72AF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis3 
      Height          =   840
      Left            =   240
      TabIndex        =   3
      Top             =   2880
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1482
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":84C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis4 
      Height          =   840
      Left            =   240
      TabIndex        =   4
      Top             =   3840
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1482
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":96E3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis6 
      Height          =   840
      Left            =   240
      TabIndex        =   6
      Top             =   5760
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1482
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":A8FD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis7 
      Height          =   840
      Left            =   6240
      TabIndex        =   7
      Top             =   960
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1482
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":BB17
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis8 
      Height          =   840
      Left            =   6240
      TabIndex        =   8
      Top             =   1920
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1482
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":CD31
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis5 
      Height          =   840
      Left            =   240
      TabIndex        =   5
      Top             =   4800
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1482
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":DF4B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis9 
      Height          =   840
      Left            =   6240
      TabIndex        =   9
      Top             =   2880
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1482
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":F165
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStandardDrill 
      Height          =   855
      Left            =   240
      TabIndex        =   13
      Top             =   6720
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":1037F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis11 
      Height          =   855
      Left            =   6240
      TabIndex        =   11
      Top             =   4800
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":10566
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNegative 
      Height          =   990
      Left            =   8640
      TabIndex        =   18
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":11781
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuantifier 
      Height          =   990
      Left            =   4680
      TabIndex        =   16
      Top             =   7800
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":11979
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther 
      Height          =   990
      Left            =   3000
      TabIndex        =   17
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":11B5A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRO 
      Height          =   990
      Left            =   6240
      TabIndex        =   22
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":11D47
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHO 
      Height          =   990
      Left            =   7440
      TabIndex        =   23
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":11F26
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   855
      Left            =   6240
      TabIndex        =   12
      Top             =   6720
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":12107
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis12 
      Height          =   855
      Left            =   6240
      TabIndex        =   24
      Top             =   5760
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DiagnosisLevels.frx":122DE
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "System"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   240
      TabIndex        =   21
      Top             =   480
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "OD"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   240
      TabIndex        =   20
      Top             =   120
      Width           =   435
   End
End
Attribute VB_Name = "frmDiagnosisLevels"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public FormOn As Boolean
Public JumpAction As Boolean
Public PatientId As Long
Public AppointmentId As Long
Public ActivityId As Long
Public MedicalSystem As String
Public Diagnosis As String
Public DiagnosisLevel As Integer
Public DiagnosisId As Long
Public SystemReference As String
Public NoteOn As Boolean
Public FavoriteOn As Boolean
Public NegFindings As Boolean
Public QuantifierOn As Boolean
Public HistoryOfOn As Boolean
Public RuleOutOn As Boolean
Public DrawAllowed As Boolean

Private Const LevelTag As String = "* "
Private Const NotBillTag As String = "  (NB)"
Private Const UnspecTag As String = "  (NS)"
Private DiagSelectedOn As Boolean
Private OrgPicture As IPictureDisp
Private LocalSysRef As String
Private TheDiagnosis As String
Private CurrentActiveDiag As Integer
Private TotalOtherFound As Integer
Private CurrentLevel As Integer
Private OriginalDiagnosis As String
Private CurrentIndex As Integer
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub TurnOthersOff(Display As Boolean)
cmdDiagnosis1.BackColor = BaseBackColor
cmdDiagnosis1.ForeColor = BaseTextColor
cmdDiagnosis1.Visible = Display
cmdDiagnosis2.BackColor = BaseBackColor
cmdDiagnosis2.ForeColor = BaseTextColor
cmdDiagnosis2.Visible = Display
cmdDiagnosis3.BackColor = BaseBackColor
cmdDiagnosis3.ForeColor = BaseTextColor
cmdDiagnosis3.Visible = Display
cmdDiagnosis4.BackColor = BaseBackColor
cmdDiagnosis4.ForeColor = BaseTextColor
cmdDiagnosis4.Visible = Display
cmdDiagnosis5.BackColor = BaseBackColor
cmdDiagnosis5.ForeColor = BaseTextColor
cmdDiagnosis5.Visible = Display
cmdDiagnosis6.BackColor = BaseBackColor
cmdDiagnosis6.ForeColor = BaseTextColor
cmdDiagnosis6.Visible = Display
cmdDiagnosis7.BackColor = BaseBackColor
cmdDiagnosis7.ForeColor = BaseTextColor
cmdDiagnosis7.Visible = Display
cmdDiagnosis8.BackColor = BaseBackColor
cmdDiagnosis8.ForeColor = BaseTextColor
cmdDiagnosis8.Visible = Display
cmdDiagnosis9.BackColor = BaseBackColor
cmdDiagnosis9.ForeColor = BaseTextColor
cmdDiagnosis9.Visible = Display
cmdDiagnosis10.BackColor = BaseBackColor
cmdDiagnosis10.ForeColor = BaseTextColor
cmdDiagnosis10.Visible = Display
cmdDiagnosis11.BackColor = BaseBackColor
cmdDiagnosis11.ForeColor = BaseTextColor
cmdDiagnosis11.Visible = Display
cmdDiagnosis12.BackColor = BaseBackColor
cmdDiagnosis12.ForeColor = BaseTextColor
cmdDiagnosis12.Visible = Display
End Sub

Private Sub cmdDiagnosis1_Click()
Call DoDiagnosis(cmdDiagnosis1, 1)
End Sub

Private Sub cmdDiagnosis2_Click()
Call DoDiagnosis(cmdDiagnosis2, 2)
End Sub

Private Sub cmdDiagnosis3_Click()
Call DoDiagnosis(cmdDiagnosis3, 3)
End Sub

Private Sub cmdDiagnosis4_Click()
Call DoDiagnosis(cmdDiagnosis4, 4)
End Sub

Private Sub cmdDiagnosis5_Click()
Call DoDiagnosis(cmdDiagnosis5, 5)
End Sub

Private Sub cmdDiagnosis6_Click()
Call DoDiagnosis(cmdDiagnosis6, 6)
End Sub

Private Sub cmdDiagnosis7_Click()
Call DoDiagnosis(cmdDiagnosis7, 7)
End Sub

Private Sub cmdDiagnosis8_Click()
Call DoDiagnosis(cmdDiagnosis8, 8)
End Sub

Private Sub cmdDiagnosis9_Click()
Call DoDiagnosis(cmdDiagnosis9, 9)
End Sub

Private Sub cmdDiagnosis10_Click()
Call DoDiagnosis(cmdDiagnosis10, 10)
End Sub

Private Sub cmdDiagnosis11_Click()
Call DoDiagnosis(cmdDiagnosis11, 11)
End Sub

Private Sub cmdDiagnosis12_Click()
Call DoDiagnosis(cmdDiagnosis12, 12)
End Sub

Private Sub DoDiagnosis(AButton As fpBtn, BRef As Integer)
Dim ASystem As String
Dim ADiag As String
Dim ALevel As Integer
Dim w, q As Integer
Call TriggerDiagnosis(AButton)
If (AButton.BackColor <> BaseBackColor) Then
    Call TurnOthersOff(True)
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    ASystem = MedicalSystem
    q = InStrPS(AButton.Tag, "/")
    If (q = 0) Then
        Call TriggerDiagnosis(AButton)
        Exit Sub
    End If
    w = InStrPS(q + 1, AButton.Tag, "/")
    If (w = 0) Then
        Exit Sub
    End If
    ALevel = Val(Trim(AButton.ToolTipText))
    ADiag = Mid(AButton.Tag, w + 1, Len(AButton.Tag) - w)
    If (ALevel = 0) Then
        TheDiagnosis = ADiag
        DiagnosisId = Val(Left(AButton.Tag, q - 1))
        CurrentActiveDiag = BRef
        SystemReference = Label2.Caption
        If (NegFindings) Then
            Call cmdDone_Click
        ElseIf (QuantifierOn) Then
            Call cmdDone_Click
        ElseIf FavoriteOn Then
            Call DoDraw(False)
            Call cmdDone_Click
        Else
            Call DoDraw(False)
        End If
        Exit Sub
    End If
    ALevel = ALevel + 1
    If (CheckNextLevel(ASystem, ADiag, ALevel)) Then
        DiagSelectedOn = False
        Diagnosis = ADiag
        DiagnosisLevel = ALevel
        TheDiagnosis = ADiag
        DiagnosisId = Val(Left(AButton.Tag, q - 1))
        CurrentActiveDiag = BRef
        SystemReference = Label2.Caption
        cmdPrevDx.Text = "Touch here to select finding:" + vbCrLf + Mid(AButton.Text, 2, Len(AButton.Text) - 1)
        Set cmdPrevDx.picture = AButton.picture
        cmdPrevDx.Tag = Trim(str(DiagnosisId))
        cmdPrevDx.Visible = False
        If (ALevel <> 1) Then
            cmdPrevDx.Visible = True
        End If
        CurrentIndex = 1
        If (FavoriteOn) Then
            FavoriteOn = False
            If (NegFindings) Then
                If (Trim(TheDiagnosis) <> "") Then
                    DiagSelectedOn = True
                End If
                Call cmdDone_Click
            ElseIf (QuantifierOn) Then
                If (Trim(TheDiagnosis) <> "") Then
                    DiagSelectedOn = True
                End If
                Call cmdDone_Click
            ElseIf Not (LoadDiagnosisLevels(False)) Then
                FavoriteOn = True
                Call DoDraw(False)
                Call cmdDone_Click
            End If
            FavoriteOn = True
        ElseIf (NegFindings) Then
            If (Trim(TheDiagnosis) <> "") Then
                DiagSelectedOn = True
            End If
            Call cmdDone_Click
        ElseIf (QuantifierOn) Then
            If (Trim(TheDiagnosis) <> "") Then
                DiagSelectedOn = True
            End If
            Call cmdDone_Click
        Else
            If Not (LoadDiagnosisLevels(False)) Then
                Call DoDraw(False)
                Call cmdDone_Click
            End If
        End If
    Else
        DiagSelectedOn = True
        CurrentActiveDiag = BRef
        SystemReference = Label2.Caption
        TheDiagnosis = ADiag
        DiagnosisId = Val(Left(AButton.Tag, q - 1))
        If Not (NegFindings) And Not (QuantifierOn) Then
            Call DoDraw(False)
        End If
        Call cmdDone_Click
    End If
End If
End Sub

Private Sub cmdDiffDiag_Click()
Call TurnOthersOff(True)
Diagnosis = ""
TheDiagnosis = ""
DiagnosisLevel = 1
CurrentIndex = 1
cmdPrevDx.Visible = False
Call LoadDiagnosisLevels(False)
End Sub

Private Sub cmdDone_Click()
If (FavoriteOn) Then
    If (Trim(TheDiagnosis) <> Diagnosis) And (Trim(TheDiagnosis) <> "") Then
        FavoriteOn = True
        Diagnosis = TheDiagnosis
    Else
        FavoriteOn = False
    End If
Else
    If (Trim(TheDiagnosis) <> Diagnosis) Then
        Diagnosis = TheDiagnosis
    End If
End If
If Not (DiagSelectedOn) Then
    Diagnosis = ""
End If
Unload frmDiagnosisLevels
FormOn = False
End Sub

Private Sub DoDraw(OtherOn As Boolean)
Dim q As Integer
Dim ADiag As Long
Dim ADraw As String
Dim RDraw As String
Dim TheFile As String
Dim ImageRef As String
Dim PrimaryId As Long
Dim LocalDiagnosis As String
ADiag = 0
ADraw = ""
JumpAction = False
PrimaryId = 0
If (OtherOn) Then
    If (Trim(frmSearch.Selection) <> "") Then
        PrimaryId = Val(Mid(frmSearch.Selection, 76, Len(frmSearch.Selection) - 75))
    End If
ElseIf (CurrentActiveDiag = 1) Then
    q = InStrPS(cmdDiagnosis1.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis1.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 2) Then
    q = InStrPS(cmdDiagnosis2.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis2.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 3) Then
    q = InStrPS(cmdDiagnosis3.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis3.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 4) Then
    q = InStrPS(cmdDiagnosis4.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis4.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 5) Then
    q = InStrPS(cmdDiagnosis5.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis5.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 6) Then
    q = InStrPS(cmdDiagnosis6.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis6.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 7) Then
    q = InStrPS(cmdDiagnosis7.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis7.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 8) Then
    q = InStrPS(cmdDiagnosis8.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis8.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 9) Then
    q = InStrPS(cmdDiagnosis9.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis9.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 10) Then
    q = InStrPS(cmdDiagnosis10.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis10.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 11) Then
    q = InStrPS(cmdDiagnosis11.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis11.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = 12) Then
    q = InStrPS(cmdDiagnosis12.Tag, "/")
    If (q > 1) Then
        PrimaryId = Val(Left(cmdDiagnosis12.Tag, q - 1))
    End If
ElseIf (CurrentActiveDiag = -1) Then
    If (Trim(cmdPrevDx.Tag) <> "") Then
        PrimaryId = Val(Trim(cmdPrevDx.Tag))
    End If
End If
If (PrimaryId > 0) Then
    If (DrawAllowed) Then
        frmDrawNew.InitOn = True
        frmDrawNew.SystemReference = LocalSysRef
        frmDrawNew.Diagnosis = TheDiagnosis
        ADiag = PrimaryId
        frmDrawNew.DiagnosisId = ADiag
        frmDrawNew.DiagnosisHOOn = False
        frmDrawNew.DiagnosisROOn = False
        frmDrawNew.DiagnosisNFOn = False
        frmDrawNew.DiagnosisQuant = ""
        frmDrawNew.EyeContext = Label1.Caption
        frmDrawNew.PatientId = PatientId
        frmDrawNew.AppointmentId = AppointmentId
        frmDrawNew.DrawFileName = ""
        LocalDiagnosis = TheDiagnosis
        Call ReplaceCharacters(LocalDiagnosis, ".", "-")
        TheFile = DoctorInterfaceDirectory + "I" + Label1.Caption + LocalDiagnosis + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        frmDrawNew.StoreResults = TheFile
        If (Trim(ADraw) = "") Then
            Call frmSystems1.GetDrawFile(TheFile, ADraw)
            Call frmSystems1.GetDiagFromFile(TheFile, ADiag)
            frmDrawNew.DiagnosisId = ADiag
        End If
        frmDrawNew.DrawFileName = Trim(ADraw)
        If (Trim(RDraw) = "") Then
            Call frmSystems1.GetReplaceFile(TheFile, RDraw)
        End If
        frmDrawNew.ReplaceBase = RDraw
        frmDrawNew.FirstTimeForImage = Not FM.IsFileThere(TheFile)
        If (frmDrawNew.LoadDraw(True)) Then
            frmDrawNew.FormOn = True
            frmDiagnosisLevels.Enabled = False
            frmDrawNew.Show 1
            
            If Not (frmDrawNew.HomeOn) Then
                frmDiagnosisLevels.Enabled = True
                frmDiagnosisLevels.ZOrder 0
                If (Not NoteOn) Then
                    NoteOn = frmDrawNew.NoteOn
                End If
                ADraw = Trim(frmDrawNew.DrawFileName)
                Call frmSystems1.PurgeDrawFileNoBrushs(TheFile, ADraw)
            Else
                TheDiagnosis = ""
            End If
            JumpAction = frmDrawNew.JumpOn
        End If
    End If
End If
End Sub

Private Sub cmdHO_Click()
HistoryOfOn = Not (HistoryOfOn)
If (HistoryOfOn) Then
    cmdHO.BackColor = ButtonSelectionColor
    cmdHO.ForeColor = TextSelectionColor
Else
    cmdHO.BackColor = BaseBackColor
    cmdHO.ForeColor = BaseTextColor
End If
End Sub

Private Sub cmdMore_Click()
If (cmdMore.Text = "Start Over") Then
    CurrentIndex = 1
End If
If (FavoriteOn) Then
    If (DiagnosisLevel > 1) Then
        FavoriteOn = False
        Call LoadDiagnosisLevels(False)
        FavoriteOn = True
    Else
        Call LoadDiagnosisFavorites
    End If
Else
    Call LoadDiagnosisLevels(False)
End If
End Sub

Private Sub cmdNegative_Click()
NegFindings = Not (NegFindings)
If (NegFindings) Then
    cmdNegative.BackColor = ButtonSelectionColor
    cmdNegative.ForeColor = TextSelectionColor
Else
    cmdNegative.BackColor = BaseBackColor
    cmdNegative.ForeColor = BaseTextColor
End If
End Sub

Private Sub cmdNotes_Click()
frmNotes.NoteId = 0
frmNotes.NoteOn = False
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.SystemReference = LocalSysRef
frmNotes.SetTo = "C"
frmNotes.EyeContext = Trim(Label1.Caption)
frmNotes.MaintainOn = True
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    NoteOn = frmNotes.NoteOn
End If
End Sub

Private Function LoadDiagnosisFavorites() As Boolean
Dim i As Integer
Dim ButtonMax As Integer
Dim ButtonCnt As Integer
Dim TotalFavs As Integer
Dim BLevel As String, ILevel As String
Dim ULevel As Boolean, AImage As Boolean
Dim AName As String, AOtherDiag As String
Dim DiagClass As String
Dim AId As Long, ALevel As Integer
Dim RetFav As PracticeFavorites
LoadDiagnosisFavorites = False
LocalSysRef = SystemReference
NegFindings = False
QuantifierOn = False
If (MedicalSystem = "") Then
    Exit Function
End If
ButtonMax = 12
cmdOther.Visible = False
cmdDiagnosis11.Visible = False
cmdDiffDiag.Visible = False
cmdStandardDrill.Visible = True
Set RetFav = New PracticeFavorites
RetFav.FavoritesSystem = MedicalSystem
RetFav.FavoritesDiagnosis = ""
RetFav.FavoritesRank = 0
TotalFavs = RetFav.FindFavorites
If (TotalFavs > 0) Then
    Call ClearButtons
    i = CurrentIndex
    ButtonCnt = 1
    While (RetFav.SelectFavorites(i)) And (ButtonCnt < ButtonMax + 1)
        If (GetDiagnosis(RetFav.FavoritesSystem, RetFav.FavoritesDiagnosis, AName, AOtherDiag, AId, ALevel, AImage, ULevel, BLevel)) Then
            ILevel = ""
            If (ULevel) Then
                ILevel = LevelTag
            End If
            DiagClass = ""
            If (BLevel = "U") Then
                DiagClass = UnspecTag
            ElseIf (BLevel = "F") Then
                DiagClass = NotBillTag
            End If
            If (ButtonCnt = 1) Then
                Call SetButtonPicture(cmdDiagnosis1, AImage)
                cmdDiagnosis1.Text = ILevel + AName + DiagClass
                cmdDiagnosis1.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis1.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                Call SetButtonPicture(cmdDiagnosis2, AImage)
                cmdDiagnosis2.Text = ILevel + AName + DiagClass
                cmdDiagnosis2.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis2.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                Call SetButtonPicture(cmdDiagnosis3, AImage)
                cmdDiagnosis3.Text = ILevel + AName + DiagClass
                cmdDiagnosis3.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis3.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                Call SetButtonPicture(cmdDiagnosis4, AImage)
                cmdDiagnosis4.Text = ILevel + AName + DiagClass
                cmdDiagnosis4.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis4.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                Call SetButtonPicture(cmdDiagnosis5, AImage)
                cmdDiagnosis5.Text = ILevel + AName + DiagClass
                cmdDiagnosis5.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis5.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                Call SetButtonPicture(cmdDiagnosis6, AImage)
                cmdDiagnosis6.Text = ILevel + AName + DiagClass
                cmdDiagnosis6.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis6.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                Call SetButtonPicture(cmdDiagnosis7, AImage)
                cmdDiagnosis7.Text = ILevel + AName + DiagClass
                cmdDiagnosis7.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis7.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                Call SetButtonPicture(cmdDiagnosis8, AImage)
                cmdDiagnosis8.Text = ILevel + AName + DiagClass
                cmdDiagnosis8.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis8.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                Call SetButtonPicture(cmdDiagnosis9, AImage)
                cmdDiagnosis9.Text = ILevel + AName + DiagClass
                cmdDiagnosis9.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis9.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                Call SetButtonPicture(cmdDiagnosis10, AImage)
                cmdDiagnosis10.Text = ILevel + AName + DiagClass
                cmdDiagnosis10.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis10.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                Call SetButtonPicture(cmdDiagnosis10, AImage)
                cmdDiagnosis11.Text = ILevel + AName + DiagClass
                cmdDiagnosis11.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis11.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                Call SetButtonPicture(cmdDiagnosis10, AImage)
                cmdDiagnosis12.Text = ILevel + AName + DiagClass
                cmdDiagnosis12.Tag = Trim(str(AId)) + "/" + AOtherDiag + "/" + RetFav.FavoritesDiagnosis
                cmdDiagnosis12.ToolTipText = Trim(str(ALevel))
                cmdDiagnosis12.Visible = True
            End If
            ButtonCnt = ButtonCnt + 1
        End If
        i = i + 1
        CurrentIndex = i
    Wend
    LoadDiagnosisFavorites = True
End If
If (TotalFavs > ButtonMax) Then
    cmdMore.Visible = True
    cmdMore.Text = "More Favorites"
    If (CurrentIndex > TotalFavs) Then
        cmdMore.Text = "Start Over"
    End If
Else
    cmdMore.Visible = False
End If
Set RetFav = Nothing
End Function

Public Function LoadDiagnosisLevels(Init As Boolean) As Boolean
Dim i As Integer
Dim Temp As String
Dim AImage As Boolean
Dim ILevel As String
Dim DiagClass As String
Dim ButtonMax As Integer
Dim ButtonCnt As Integer
Dim TotalDiags As Integer
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
LoadDiagnosisLevels = False
NoteOn = False
NegFindings = False
QuantifierOn = False
HistoryOfOn = False
cmdHO.BackColor = BaseBackColor
cmdHO.ForeColor = BaseTextColor
RuleOutOn = False
cmdRO.BackColor = BaseBackColor
cmdRO.ForeColor = BaseTextColor
cmdDiffDiag.Enabled = True
DiagSelectedOn = False
If (MedicalSystem = "") Then
    Exit Function
End If
If (Init) Then
    Set OrgPicture = cmdDiagnosis1.picture
End If
If (FavoriteOn) Then
    frmDiagnosisLevels.BackColor = &HFF8080
    Label1.BackColor = &HFF8080
    Label2.BackColor = &HFF8080
    LoadDiagnosisLevels = LoadDiagnosisFavorites
    Exit Function
End If
frmDiagnosisLevels.BackColor = &HA95911
Label1.BackColor = &HA95911
Label2.BackColor = &HA95911
cmdOther.Visible = True
cmdQuantifier.Visible = DrawAllowed
cmdNotes.Visible = DrawAllowed
cmdNegative.Visible = DrawAllowed
cmdDiffDiag.Visible = DrawAllowed
cmdStandardDrill.Visible = DrawAllowed
ButtonMax = 12
cmdDiagnosis11.Visible = True
cmdDiffDiag.Visible = True
cmdStandardDrill.Visible = False
Set RetrieveDiagnosis = New DiagnosisMasterPrimary
RetrieveDiagnosis.PrimaryLevel = DiagnosisLevel
RetrieveDiagnosis.PrimaryRank = 0
RetrieveDiagnosis.PrimaryBilling = False
RetrieveDiagnosis.PrimarySystem = MedicalSystem
If (DiagnosisLevel < 2) Then
    RetrieveDiagnosis.PrimaryDiagnosis = ""
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = Diagnosis
Else
    RetrieveDiagnosis.PrimaryDiagnosis = Diagnosis
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = ""
End If
CurrentLevel = DiagnosisLevel
TotalDiags = RetrieveDiagnosis.FindPrimary
If (TotalDiags > 0) Then
    Call ClearButtons
    i = CurrentIndex
    ButtonCnt = 1
    While (RetrieveDiagnosis.SelectPrimary(i)) And (ButtonCnt < ButtonMax + 1)
        AImage = False
        If (Trim(RetrieveDiagnosis.PrimaryImage1) <> "") Then
            AImage = True
        End If
        ILevel = ""
        If (RetrieveDiagnosis.PrimaryNextLevelOn) Then
            ILevel = LevelTag
        End If
        DiagClass = ""
        If (RetrieveDiagnosis.PrimaryUnspecifiedOn) Then
            DiagClass = UnspecTag
        ElseIf Not (RetrieveDiagnosis.PrimaryBilling) Then
            DiagClass = NotBillTag
        End If
        If (ButtonCnt = 1) Then
            Call SetButtonPicture(cmdDiagnosis1, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis1.Text = ILevel + Temp + DiagClass
            cmdDiagnosis1.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis1.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis1.Visible = True
        ElseIf (ButtonCnt = 2) Then
            Call SetButtonPicture(cmdDiagnosis2, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis2.Text = ILevel + Temp + DiagClass
            cmdDiagnosis2.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis2.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis2.Visible = True
        ElseIf (ButtonCnt = 3) Then
            Call SetButtonPicture(cmdDiagnosis3, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis3.Text = ILevel + Temp + DiagClass
            cmdDiagnosis3.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis3.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis3.Visible = True
        ElseIf (ButtonCnt = 4) Then
            Call SetButtonPicture(cmdDiagnosis4, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis4.Text = ILevel + Temp + DiagClass
            cmdDiagnosis4.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis4.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis4.Visible = True
        ElseIf (ButtonCnt = 5) Then
            Call SetButtonPicture(cmdDiagnosis5, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis5.Text = ILevel + Temp + DiagClass
            cmdDiagnosis5.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis5.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis5.Visible = True
        ElseIf (ButtonCnt = 6) Then
            Call SetButtonPicture(cmdDiagnosis6, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis6.Text = ILevel + Temp + DiagClass
            cmdDiagnosis6.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis6.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis6.Visible = True
        ElseIf (ButtonCnt = 7) Then
            Call SetButtonPicture(cmdDiagnosis7, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis7.Text = ILevel + Temp + DiagClass
            cmdDiagnosis7.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis7.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis7.Visible = True
        ElseIf (ButtonCnt = 8) Then
            Call SetButtonPicture(cmdDiagnosis8, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis8.Text = ILevel + Temp + DiagClass
            cmdDiagnosis8.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis8.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis8.Visible = True
        ElseIf (ButtonCnt = 9) Then
            Call SetButtonPicture(cmdDiagnosis9, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis9.Text = ILevel + Temp + DiagClass
            cmdDiagnosis9.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis9.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis9.Visible = True
        ElseIf (ButtonCnt = 10) Then
            Call SetButtonPicture(cmdDiagnosis10, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis10.Text = ILevel + Temp + DiagClass
            cmdDiagnosis10.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis10.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis10.Visible = True
        ElseIf (ButtonCnt = 11) Then
            Call SetButtonPicture(cmdDiagnosis11, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis11.Text = ILevel + Temp + DiagClass
            cmdDiagnosis11.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis11.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis11.Visible = True
        ElseIf (ButtonCnt = 12) Then
            Call SetButtonPicture(cmdDiagnosis12, AImage)
            Temp = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                Temp = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            cmdDiagnosis12.Text = ILevel + Temp + DiagClass
            cmdDiagnosis12.Tag = Trim(str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis12.ToolTipText = Trim(str(RetrieveDiagnosis.PrimaryLevel))
            cmdDiagnosis12.Visible = True
        End If
        i = i + 1
        ButtonCnt = ButtonCnt + 1
        CurrentIndex = i
    Wend
    LoadDiagnosisLevels = True
End If
If (TotalDiags > ButtonMax) Then
    cmdMore.Visible = True
    cmdMore.Text = "More Findings"
    If (CurrentIndex > TotalDiags) Then
        cmdMore.Text = "Start Over"
    End If
Else
    cmdMore.Visible = False
End If
Set RetrieveDiagnosis = Nothing
End Function

Private Sub cmdOther_Click()
Dim DisplayStart As String
Dim DisplayType As String
Dim iLeftParen As Long
Dim iRightParen As Long
DisplayStart = ""
DisplayType = ""
frmEventMsgs.Header = "Dx Display Ordering"
frmEventMsgs.AcceptText = "Alpha"
frmEventMsgs.RejectText = "Numeric"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    DisplayType = "A"
ElseIf (frmEventMsgs.Result = 2) Then
    DisplayType = "N"
End If
If (Trim(DisplayType) <> "") Then
    Call GetStartingValue(DisplayStart, DisplayType)
    If (Trim(DisplayStart) <> "") Then
        frmSearch.DiagOn = True
        frmSearch.FilterBillable = False
        frmSearch.FilterSpecial = False
        frmSearch.Selection = ""
        If (frmSearch.LoadAvailableSearch(DisplayStart, DisplayType)) Then
            frmSearch.Show 1
            If (Trim(frmSearch.Selection) <> "") Then
                TheDiagnosis = Trim(Mid(frmSearch.Selection, 1, 10))
                iLeftParen = InStrRev(frmSearch.Selection, "(")
                If iLeftParen > 0 Then
                    iRightParen = InStrPS(iLeftParen, frmSearch.Selection, ")")
                    If iRightParen > 0 Then
                        LocalSysRef = Mid$(frmSearch.Selection, iLeftParen + 1, iRightParen - iLeftParen - 1)
                    End If
                End If
                DiagnosisId = Val(Mid(frmSearch.Selection, 76, Len(frmSearch.Selection) - 75))
                Call DoDraw(True)
                DiagSelectedOn = True
                Call cmdDone_Click
            End If
        End If
    End If
End If
End Sub

Private Sub cmdPrevDx_Click()
DiagSelectedOn = True
CurrentActiveDiag = -1
If (NegFindings) Then
    Call cmdDone_Click
ElseIf (QuantifierOn) Then
    Call cmdDone_Click
Else
    Call DoDraw(False)
    Call cmdDone_Click
End If
End Sub

Private Sub cmdQuantifier_Click()
QuantifierOn = Not (QuantifierOn)
If (QuantifierOn) Then
    cmdQuantifier.BackColor = ButtonSelectionColor
    cmdQuantifier.ForeColor = TextSelectionColor
Else
    cmdQuantifier.BackColor = BaseBackColor
    cmdQuantifier.ForeColor = BaseTextColor
End If
End Sub

Private Sub cmdRO_Click()
RuleOutOn = Not (RuleOutOn)
If (RuleOutOn) Then
    cmdRO.BackColor = ButtonSelectionColor
    cmdRO.ForeColor = TextSelectionColor
Else
    cmdRO.BackColor = BaseBackColor
    cmdRO.ForeColor = BaseTextColor
End If
End Sub

Private Sub cmdStandardDrill_Click()
TheDiagnosis = "-"
Diagnosis = TheDiagnosis
FavoriteOn = False
Unload frmDiagnosisLevels
FormOn = False
End Sub

Private Sub Form_Load()
JumpAction = False
CurrentIndex = 1
TheDiagnosis = ""
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdDiagnosis1.BackColor
BaseTextColor = cmdDiagnosis1.ForeColor
End Sub

Private Sub ClearButtons()
cmdDiagnosis1.BackColor = BaseBackColor
cmdDiagnosis1.ForeColor = BaseTextColor
cmdDiagnosis1.Text = ""
cmdDiagnosis1.Tag = ""
cmdDiagnosis1.ToolTipText = ""
cmdDiagnosis1.Visible = False
cmdDiagnosis2.BackColor = BaseBackColor
cmdDiagnosis2.ForeColor = BaseTextColor
cmdDiagnosis2.Text = ""
cmdDiagnosis2.Tag = ""
cmdDiagnosis2.ToolTipText = ""
cmdDiagnosis2.Visible = False
cmdDiagnosis3.BackColor = BaseBackColor
cmdDiagnosis3.ForeColor = BaseTextColor
cmdDiagnosis3.Text = ""
cmdDiagnosis3.Tag = ""
cmdDiagnosis3.ToolTipText = ""
cmdDiagnosis3.Visible = False
cmdDiagnosis4.BackColor = BaseBackColor
cmdDiagnosis4.ForeColor = BaseTextColor
cmdDiagnosis4.Text = ""
cmdDiagnosis4.Tag = ""
cmdDiagnosis4.ToolTipText = ""
cmdDiagnosis4.Visible = False
cmdDiagnosis5.BackColor = BaseBackColor
cmdDiagnosis5.ForeColor = BaseTextColor
cmdDiagnosis5.Text = ""
cmdDiagnosis5.Tag = ""
cmdDiagnosis5.ToolTipText = ""
cmdDiagnosis5.Visible = False
cmdDiagnosis6.BackColor = BaseBackColor
cmdDiagnosis6.ForeColor = BaseTextColor
cmdDiagnosis6.Text = ""
cmdDiagnosis6.Tag = ""
cmdDiagnosis6.ToolTipText = ""
cmdDiagnosis6.Visible = False
cmdDiagnosis7.BackColor = BaseBackColor
cmdDiagnosis7.ForeColor = BaseTextColor
cmdDiagnosis7.Text = ""
cmdDiagnosis7.Tag = ""
cmdDiagnosis7.ToolTipText = ""
cmdDiagnosis7.Visible = False
cmdDiagnosis8.BackColor = BaseBackColor
cmdDiagnosis8.ForeColor = BaseTextColor
cmdDiagnosis8.Text = ""
cmdDiagnosis8.Tag = ""
cmdDiagnosis8.ToolTipText = ""
cmdDiagnosis8.Visible = False
cmdDiagnosis9.BackColor = BaseBackColor
cmdDiagnosis9.ForeColor = BaseTextColor
cmdDiagnosis9.Text = ""
cmdDiagnosis9.Tag = ""
cmdDiagnosis9.ToolTipText = ""
cmdDiagnosis9.Visible = False
cmdDiagnosis10.BackColor = BaseBackColor
cmdDiagnosis10.ForeColor = BaseTextColor
cmdDiagnosis10.Text = ""
cmdDiagnosis10.Tag = ""
cmdDiagnosis10.ToolTipText = ""
cmdDiagnosis10.Visible = False
cmdDiagnosis11.BackColor = BaseBackColor
cmdDiagnosis11.ForeColor = BaseTextColor
cmdDiagnosis11.Text = ""
cmdDiagnosis11.Tag = ""
cmdDiagnosis11.ToolTipText = ""
cmdDiagnosis11.Visible = False
cmdDiagnosis12.BackColor = BaseBackColor
cmdDiagnosis12.ForeColor = BaseTextColor
cmdDiagnosis12.Text = ""
cmdDiagnosis12.Tag = ""
cmdDiagnosis12.ToolTipText = ""
cmdDiagnosis12.Visible = False
End Sub

Public Sub SetDiagnosisLabel()
Dim IImage As String
Dim ADiagnosis As DiagnosisMasterPrimary
If (MedicalSystem <> "") Then
    Set ADiagnosis = New DiagnosisMasterPrimary
    ADiagnosis.PrimarySystem = MedicalSystem
    ADiagnosis.PrimaryRank = 0
    ADiagnosis.PrimaryBilling = False
    If (Trim(Diagnosis) <> "") Then
        ADiagnosis.PrimaryLevel = 2
        ADiagnosis.PrimaryDiagnosis = Diagnosis
        ADiagnosis.PrimaryNextLevelDiagnosis = ""
    Else
        ADiagnosis.PrimaryLevel = 1
        ADiagnosis.PrimaryDiagnosis = ""
        ADiagnosis.PrimaryNextLevelDiagnosis = Diagnosis
    End If
    If (ADiagnosis.FindPrimary > 0) Then
        If (ADiagnosis.SelectPrimary(1)) Then
            If (Trim(ADiagnosis.PrimaryLingo) <> "") Then
                cmdPrevDx.Text = "Touch here to select this finding: " + vbCrLf + Trim(ADiagnosis.PrimaryLingo)
            Else
                cmdPrevDx.Text = "Touch here to select this finding: " + vbCrLf + Trim(ADiagnosis.PrimaryName)
            End If
            cmdPrevDx.Tag = Trim(str(ADiagnosis.PrimaryId))
            If (Trim(ADiagnosis.PrimaryImage1) <> "") Then
                Call SetButtonPicture(cmdPrevDx, True)
            Else
                Call SetButtonPicture(cmdPrevDx, False)
            End If
        End If
    End If
    Label2.Caption = frmSystems1.SystemReference
    Label2.Visible = True
    Label2.Tag = ""
    cmdPrevDx.Visible = False
    If (ADiagnosis.PrimaryLevel <> 1) Then
        cmdPrevDx.Visible = True
    End If
    Set ADiagnosis = Nothing
End If
End Sub

Private Function CheckNextLevel(TheSystem As String, TheDiag As String, TheLevel As Integer)
Dim ADiagnosis As DiagnosisMasterPrimary
CheckNextLevel = False
If (TheSystem <> "") And (Trim(TheDiag) <> "") Then
    Set ADiagnosis = New DiagnosisMasterPrimary
    ADiagnosis.PrimarySystem = TheSystem
    ADiagnosis.PrimaryLevel = TheLevel
    ADiagnosis.PrimaryRank = 0
    ADiagnosis.PrimaryBilling = False
    If (TheLevel = 1) Then
        ADiagnosis.PrimaryDiagnosis = ""
        ADiagnosis.PrimaryNextLevelDiagnosis = TheDiag
    Else
        ADiagnosis.PrimaryDiagnosis = TheDiag
        ADiagnosis.PrimaryNextLevelDiagnosis = ""
    End If
    CheckNextLevel = ADiagnosis.IsNextLevelOn
    Set ADiagnosis = Nothing
End If
End Function

Private Sub TriggerDiagnosis(AButton As fpBtn)
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    DiagnosisId = 0
    TheDiagnosis = ""
    CurrentActiveDiag = 0
End If
End Sub

Private Function GetDiagnosis(TheSys As String, TheDiag As String, TheName As String, TheOtherDiag As String, TheId As Long, TheLevel As Integer, TheImage As Boolean, NLevel As Boolean, GLevel As String)
Dim ADiagnosis As DiagnosisMasterPrimary
GetDiagnosis = False
TheId = 0
TheName = ""
GLevel = ""
NLevel = False
TheImage = False
If (Trim(TheDiag) <> "") Then
    Set ADiagnosis = New DiagnosisMasterPrimary
    ADiagnosis.PrimarySystem = TheSys
    ADiagnosis.PrimaryDiagnosis = ""
    ADiagnosis.PrimaryNextLevelDiagnosis = TheDiag
    ADiagnosis.PrimaryLevel = 0
    ADiagnosis.PrimaryRank = 0
    ADiagnosis.PrimaryBilling = False
    If (ADiagnosis.FindPrimary > 0) Then
        If (ADiagnosis.SelectPrimary(1)) Then
            GetDiagnosis = True
            TheId = ADiagnosis.PrimaryId
            TheName = Trim(ADiagnosis.PrimaryName)
            If (Trim(ADiagnosis.PrimaryLingo) <> "") Then
                TheName = Trim(ADiagnosis.PrimaryLingo)
            End If
            TheOtherDiag = ADiagnosis.PrimaryDiagnosis
            TheLevel = ADiagnosis.PrimaryLevel
            NLevel = ADiagnosis.PrimaryNextLevelOn
            TheImage = False
            If (Trim(ADiagnosis.PrimaryImage1) <> "") And (Trim(ADiagnosis.PrimaryImage1) <> "9990000") Then
                TheImage = True
            End If
            If (ADiagnosis.PrimaryUnspecifiedOn) Then
                GLevel = "U"
            ElseIf Not (ADiagnosis.PrimaryBilling) Then
                GLevel = "F"
            End If
        End If
    End If
    Set ADiagnosis = Nothing
End If
End Function

Private Function GetStartingValue(TheText As String, TheType As String) As Boolean
TheText = ""
GetStartingValue = True
If (TheType = "N") Then
    frmNumericPad.NumPad_Field = "Starting with Diagnosis Number"
    frmNumericPad.NumPad_Result = ""
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_ChoiceOn1 = False
    frmNumericPad.NumPad_Choice1 = ""
    frmNumericPad.NumPad_ChoiceOn2 = False
    frmNumericPad.NumPad_Choice2 = ""
    frmNumericPad.NumPad_ChoiceOn3 = False
    frmNumericPad.NumPad_Choice3 = ""
    frmNumericPad.NumPad_ChoiceOn4 = False
    frmNumericPad.NumPad_Choice4 = ""
    frmNumericPad.NumPad_ChoiceOn5 = False
    frmNumericPad.NumPad_Choice5 = ""
    frmNumericPad.NumPad_ChoiceOn6 = False
    frmNumericPad.NumPad_Choice6 = ""
    frmNumericPad.NumPad_ChoiceOn7 = False
    frmNumericPad.NumPad_Choice7 = ""
    frmNumericPad.NumPad_ChoiceOn8 = False
    frmNumericPad.NumPad_Choice8 = ""
    frmNumericPad.NumPad_ChoiceOn9 = False
    frmNumericPad.NumPad_Choice9 = ""
    frmNumericPad.NumPad_ChoiceOn10 = False
    frmNumericPad.NumPad_Choice10 = ""
    frmNumericPad.NumPad_ChoiceOn11 = False
    frmNumericPad.NumPad_Choice11 = ""
    frmNumericPad.NumPad_ChoiceOn12 = False
    frmNumericPad.NumPad_Choice12 = ""
    frmNumericPad.NumPad_ChoiceOn13 = False
    frmNumericPad.NumPad_Choice13 = ""
    frmNumericPad.NumPad_ChoiceOn14 = False
    frmNumericPad.NumPad_Choice14 = ""
    frmNumericPad.NumPad_ChoiceOn15 = False
    frmNumericPad.NumPad_Choice15 = ""
    frmNumericPad.NumPad_ChoiceOn16 = False
    frmNumericPad.NumPad_Choice16 = ""
    frmNumericPad.NumPad_ChoiceOn17 = False
    frmNumericPad.NumPad_Choice17 = ""
    frmNumericPad.NumPad_ChoiceOn18 = False
    frmNumericPad.NumPad_Choice18 = ""
    frmNumericPad.NumPad_ChoiceOn19 = False
    frmNumericPad.NumPad_Choice19 = ""
    frmNumericPad.NumPad_ChoiceOn20 = False
    frmNumericPad.NumPad_Choice20 = ""
    frmNumericPad.NumPad_ChoiceOn21 = False
    frmNumericPad.NumPad_Choice21 = ""
    frmNumericPad.NumPad_ChoiceOn22 = False
    frmNumericPad.NumPad_Choice22 = ""
    frmNumericPad.NumPad_ChoiceOn23 = False
    frmNumericPad.NumPad_Choice23 = ""
    frmNumericPad.NumPad_ChoiceOn24 = False
    frmNumericPad.NumPad_Choice24 = ""
    frmNumericPad.Show 1
    If Not (frmNumericPad.NumPad_Quit) Then
        TheText = Trim(frmNumericPad.NumPad_Result)
    Else
        TheText = ""
    End If
ElseIf (TheType = "A") Then
    frmAlphaPad.NumPad_Field = "Starting with Diagnosis Alpha"
    frmAlphaPad.NumPad_Result = ""
    frmAlphaPad.NumPad_DisplayFull = True
    frmAlphaPad.Show 1
    If Not (frmAlphaPad.NumPad_Quit) Then
        TheText = Trim(frmAlphaPad.NumPad_Result)
        TheText = "*" + TheText
    Else
        TheText = ""
    End If
End If
End Function

Private Function GetPrimaryDiagnosis(Id As String, TheName As String, BillOn As Boolean, AId As Long) As Boolean
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
GetPrimaryDiagnosis = False
TheName = ""
If (Id <> "") And (Len(Id) > 1) Then
    Set RetrieveDiagnosis = New DiagnosisMasterPrimary
    RetrieveDiagnosis.PrimaryDiagnosis = ""
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = Id
    RetrieveDiagnosis.PrimaryLevel = 0
    RetrieveDiagnosis.PrimaryRank = 0
    RetrieveDiagnosis.PrimaryBilling = BillOn
    RetrieveDiagnosis.PrimaryDiscipline = ""
    If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
        If (RetrieveDiagnosis.SelectPrimary(1)) Then
            TheName = Trim(RetrieveDiagnosis.PrimaryName)
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                TheName = Trim(RetrieveDiagnosis.PrimaryLingo)
            End If
            AId = RetrieveDiagnosis.PrimaryId
            GetPrimaryDiagnosis = True
        End If
    End If
    Set RetrieveDiagnosis = Nothing
End If
End Function

Private Sub SetButtonPicture(AButton As fpBtn, TheAction As Boolean)
If (TheAction) Then
    Set AButton.picture = OrgPicture
Else
    Set AButton.picture = Nothing
End If
End Sub
Public Function FrmClose()
 Call cmdDone_Click
  Unload Me
End Function
