VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmTestResults 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "TestResults.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtZoom 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3495
      Left            =   2040
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   32
      Top             =   840
      Visible         =   0   'False
      Width           =   8055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify9 
      Height          =   480
      Left            =   11400
      TabIndex        =   41
      Top             =   3360
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify8 
      Height          =   480
      Left            =   11400
      TabIndex        =   40
      Top             =   2280
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":4993
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify7 
      Height          =   480
      Left            =   11400
      TabIndex        =   33
      Top             =   1200
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":5C5D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify5 
      Height          =   480
      Left            =   7440
      TabIndex        =   34
      Top             =   2280
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":6F27
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify6 
      Height          =   480
      Left            =   7440
      TabIndex        =   35
      Top             =   3360
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":81F1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify4 
      Height          =   480
      Left            =   7440
      TabIndex        =   36
      Top             =   1200
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":94BB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify2 
      Height          =   480
      Left            =   3600
      TabIndex        =   37
      Top             =   2280
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":A785
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify3 
      Height          =   480
      Left            =   3600
      TabIndex        =   38
      Top             =   3360
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":BA4F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify1 
      Height          =   480
      Left            =   3600
      TabIndex        =   39
      Top             =   1200
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":CD19
   End
   Begin VB.TextBox txtTest9 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1095
      Left            =   7920
      MultiLine       =   -1  'True
      TabIndex        =   29
      Top             =   2760
      Width           =   3975
   End
   Begin VB.TextBox txtSummary 
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   28
      Top             =   120
      Width           =   2535
   End
   Begin VB.TextBox txtSum1 
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2640
      MultiLine       =   -1  'True
      TabIndex        =   27
      Top             =   120
      Width           =   615
   End
   Begin VB.TextBox txtSum2 
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   6360
      MultiLine       =   -1  'True
      TabIndex        =   26
      Top             =   120
      Width           =   3735
   End
   Begin VB.TextBox txtGen 
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   3240
      MultiLine       =   -1  'True
      TabIndex        =   25
      Top             =   120
      Width           =   3135
   End
   Begin VB.Timer Timer1 
      Interval        =   30000
      Left            =   11400
      Top             =   120
   End
   Begin VB.TextBox txtTest7 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1215
      Left            =   7920
      MultiLine       =   -1  'True
      TabIndex        =   23
      Top             =   480
      Width           =   3975
   End
   Begin VB.TextBox txtTest8 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1095
      Left            =   7920
      MultiLine       =   -1  'True
      TabIndex        =   22
      Top             =   1680
      Width           =   3975
   End
   Begin VB.TextBox txtTest5 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1095
      Left            =   4080
      MultiLine       =   -1  'True
      TabIndex        =   21
      Top             =   1680
      Width           =   3855
   End
   Begin VB.TextBox txtTest6 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1095
      Left            =   4080
      MultiLine       =   -1  'True
      TabIndex        =   20
      Top             =   2760
      Width           =   3855
   End
   Begin VB.TextBox txtTest4 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1215
      Left            =   4080
      MultiLine       =   -1  'True
      TabIndex        =   19
      Top             =   480
      Width           =   3855
   End
   Begin VB.TextBox txtTest3 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1095
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   18
      Top             =   2760
      Width           =   3975
   End
   Begin VB.TextBox txtTest2 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1095
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   17
      Top             =   1680
      Width           =   3975
   End
   Begin VB.TextBox txtTest1 
      BackColor       =   &H00EFEBE3&
      BeginProperty Font 
         Name            =   "MS Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1215
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   16
      Top             =   480
      Width           =   3975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   7800
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":DFE3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   1095
      Left            =   9720
      TabIndex        =   1
      Top             =   7800
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":E216
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1095
      Left            =   10800
      TabIndex        =   2
      Top             =   7800
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":E44A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest1 
      Height          =   870
      Left            =   120
      TabIndex        =   3
      Top             =   3960
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":E67D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest2 
      Height          =   870
      Left            =   120
      TabIndex        =   4
      Top             =   4920
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":E85D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest3 
      Height          =   870
      Left            =   120
      TabIndex        =   5
      Top             =   5880
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":EA3D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest4 
      Height          =   870
      Left            =   120
      TabIndex        =   6
      Top             =   6840
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":EC1D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest5 
      Height          =   870
      Left            =   4080
      TabIndex        =   7
      Top             =   3960
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":EDFD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest6 
      Height          =   870
      Left            =   4080
      TabIndex        =   8
      Top             =   4920
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":EFDD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest7 
      Height          =   870
      Left            =   4080
      TabIndex        =   9
      Top             =   5880
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":F1BD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest8 
      Height          =   870
      Left            =   4080
      TabIndex        =   10
      Top             =   6840
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":F39D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest9 
      Height          =   870
      Left            =   8040
      TabIndex        =   11
      Top             =   3960
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":F57D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest10 
      Height          =   870
      Left            =   8040
      TabIndex        =   12
      Top             =   4920
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":F75D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest11 
      Height          =   870
      Left            =   8040
      TabIndex        =   13
      Top             =   5880
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":F93E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrevTest 
      Height          =   870
      Left            =   8040
      TabIndex        =   14
      Top             =   6840
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":FB1F
   End
   Begin VB.TextBox txtTime 
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   10080
      MaxLength       =   2560
      MultiLine       =   -1  'True
      TabIndex        =   24
      Top             =   120
      Width           =   1815
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   1095
      Left            =   3360
      TabIndex        =   30
      Top             =   7800
      Visible         =   0   'False
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":FD06
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNextTest 
      Height          =   870
      Left            =   9960
      TabIndex        =   31
      Top             =   6840
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":FEF2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAllTests 
      Height          =   1095
      Left            =   8400
      TabIndex        =   42
      Top             =   7800
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":100D9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   1095
      Left            =   4680
      TabIndex        =   15
      Top             =   7800
      Visible         =   0   'False
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":102C0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrcTests 
      Height          =   1095
      Left            =   7320
      TabIndex        =   43
      Top             =   7800
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":104A8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMyTests 
      Height          =   1095
      Left            =   6000
      TabIndex        =   44
      Top             =   7800
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":1068C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExternal 
      Height          =   1095
      Left            =   2280
      TabIndex        =   45
      Top             =   7800
      Width           =   1005
      _Version        =   131072
      _ExtentX        =   1773
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestResults.frx":10874
   End
End
Attribute VB_Name = "frmTestResults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public FormOn As Boolean
Public ActivityId As Long
Public ChangeRequest As Boolean
Public PatientId As Long
Public QuestionSet As String
Public AppointmentId As Long
Public ExitFast As Boolean
Public ModalSetting As Integer

Private ResourceId As Long
Private PracticeViewOn As Boolean
Private FavoriteTestViewOn As Boolean
Private CurrentLine(9) As Integer
Private CurrentIndex As Integer
Private ClinicalClass As DynamicClass
Private TotalTestResults As Integer
Private TestResultsIndex As Integer

Private FirstElementOnPage As Integer
Private LastElementOnPage As Integer

Private Sub cmdDone_Click()
Set ClinicalClass = Nothing
Unload frmTestResults
FormOn = False
End Sub

Private Sub cmdExternal_Click()
Dim GlassesOn As Boolean
Dim ContOn As Boolean
Dim InitialPull As Boolean
Dim FrmId As Long, RscId As Long
Dim X As Integer
Dim g As Integer, f As Integer
Dim u As Integer
Dim Ques As String, OrdId As String
Dim AName As String, TestTime As String
Dim APiece1(50) As String
Dim APiece2(50) As String
Dim APiece3(50) As String
Dim AContent() As String
Dim MyFile As String
Dim AFileName As String
Dim AllFiles(4) As String
Dim AllOrders(4) As String
Dim CurrCnt(4) As Integer
Dim CurODEyeSet(4) As Boolean
Dim CurOSEyeSet(4) As Boolean
Dim iFreeFile As Long
Dim DL As IOConnectedDevices.ConnectedDevices

    On Error GoTo lcmdExternal_Click_Error

GlassesOn = False
InitialPull = True
For f = 1 To 4
    CurODEyeSet(f) = True
    CurOSEyeSet(f) = True
Next
f = 0
Erase CurrCnt
Erase AContent
Erase AllFiles
Erase AllOrders

Dim intSleepTime As Integer

intSleepTime = CInt(CheckConfigCollection("SLEEPTIME", "20"))

Set DL = New IOConnectedDevices.ConnectedDevices
If CheckConfigCollection("TESTDEVICEON") = "T" Then    ' Loopback Function
    Call DL.SetDummyData
Else
    ContOn = True
    Do Until Not (ContOn)
        Call DL.GetDeviceData
        DoEvents
        For u = 1 To intSleepTime
            Call DoSleep(1000)
            If (DL.resultStatus <> 1) Then
                Exit For
            End If
        Next u
        If (DL.resultStatus = 0) Then
            ContOn = False
        Else
            frmEventMsgs.Header = "Data Not Received, Try Again ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Yes"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result <> 2) Then
                ContOn = False
            End If
        End If
    Loop
End If
If (DL.resultStatus = 0) Then
' OD
    Call PinpointError("frmTestResults", "tests " + Trim(str(DL.numTestsDone)))
    For u = 1 To DL.numTestsDone
        InitialPull = True
        AContent = DL.DeviceOutput(u)
        For g = 0 To UBound(AContent, 1) - 1
            APiece1(g) = AContent(g, 0)
            APiece2(g) = AContent(g, 1)
            Call ReplaceCharacters(APiece2(g), vbCrLf, " ")
            If (InStrPS(1, "[RTPLA][RT80][RT70][RT60][RT50][RT400][RT40][RT30][RT25][RT200][RT20][RT-2][RT150][RT15][RT100][RT-1][RT+2][RT+1][RML20400][RMERR][RM800][RM80][RM70][RM60][RM50][RM400][RM40][RM30][RM25][RM200][RM20][RM150][RM15][RM100][NT80][NT70][NT60][NT50][NT400][NT40][NT30][NT-3][NT25][NT200][NT20][NT-2][NT150][NT15][NT100][NT-1][NT+3][NT+2][NT+1]", APiece1(g))) Then
                If (APiece2(g) = "1") Then
                    Call PinpointError("frmTestResults", "OD Element Found at " + Trim(str(u)) + " within " + Trim(str(g)))
                    GoSub PostODTestData
                End If
            Else
                Call PinpointError("frmTestResults", "OD Element Found at " + Trim(str(u)) + " within " + Trim(str(g)))
                GoSub PostODTestData
            End If
        Next g
    Next u
' OS
    For u = 1 To DL.numTestsDone
        AContent = DL.DeviceOutput(u)
        For g = 0 To UBound(AContent, 1) - 1
            APiece1(g) = AContent(g, 0)
            APiece3(g) = AContent(g, 2)
            Call ReplaceCharacters(APiece3(g), vbCrLf, " ")
            If (InStrPS(1, "[RTPLA][RT80][RT70][RT60][RT50][RT400][RT40][RT30][RT25][RT200][RT20][RT-2][RT150][RT15][RT100][RT-1][RT+2][RT+1][RML20400][RMERR][RM800][RM80][RM70][RM60][RM50][RM400][RM40][RM30][RM25][RM200][RM20][RM150][RM15][RM100][NT80][NT70][NT60][NT50][NT400][NT40][NT30][NT-3][NT25][NT200][NT20][NT-2][NT150][NT15][NT100][NT-1][NT+3][NT+2][NT+1]", APiece1(g))) Then
                If (APiece3(g) = "1") Then
                    Call PinpointError("frmTestResults", "OS Element Found at " + Trim(str(u)) + " within " + Trim(str(g)))
                    GoSub PostOSTestData
                End If
            Else
                Call PinpointError("frmTestResults", "OS Element Found at " + Trim(str(u)) + " within " + Trim(str(g)))
                GoSub PostOSTestData
            End If
        Next g
    Next u
    For u = 1 To f
        If (Trim(AllFiles(u)) <> "") Then
            If (RecapTest(Trim(AllFiles(u)))) Then
                Call PushList(Trim(AllFiles(u)))
            Else
                Call frmSystems1.ClearTestResults(AllOrders(u))
            End If
        End If
    Next u
    Call frmSystems1.DisplayTestResults(True)
    TestResultsIndex = 1
    Call ViewTestResults(TestResultsIndex)
Else
' Class ConnectedDevices: The class is called ConnectedDevices in dll called IOConnectedDevices.
' integer resultStatus: There is a public variable resultStatus which is set to 1 initially.
' Statuses are: 0 - Done
'     1 - No Data Read
'     2 - Comm Port Unavailable
'     3 - Device Not Configured
    If (DL.resultStatus = 2) Then
        frmEventMsgs.Header = "Data not received: Error - Comm Port Not Available"
    ElseIf (DL.resultStatus = 3) Then
        frmEventMsgs.Header = "Data not received: Error - Device Not Configured"
    Else
        frmEventMsgs.Header = "Data not received: Error = " + Trim(str(DL.resultStatus))
    End If
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Set DL = Nothing
Exit Sub

PostODTestData:
    If (Trim(APiece1(g)) <> "") Then
        If (GetTestName(APiece1(g), FrmId, Ques, OrdId)) Then
            If (InitialPull) Then
'                If (OrdId = "07A") Then
'                    frmEventMsgs.Header = "Is this a new pair ?"
'                    frmEventMsgs.AcceptText = ""
'                    frmEventMsgs.RejectText = "Yes"
'                    frmEventMsgs.CancelText = "No"
'                    frmEventMsgs.Other0Text = ""
'                    frmEventMsgs.Other1Text = ""
'                    frmEventMsgs.Other2Text = ""
'                    frmEventMsgs.Other3Text = ""
'                    frmEventMsgs.Other4Text = ""
'                    frmEventMsgs.Show 1
'                    If (frmEventMsgs.Result = 2) Then
'                        GlassesOn = True
'                    End If
'                End If
                AFileName = GetTestFileName(OrdId)
                MyFile = AFileName
                If (Dir(MyFile) <> "") Then
                    FM.Kill AFileName
                End If
                InitialPull = False
            End If
            AFileName = GetTestFileName(OrdId)
            MyFile = AFileName
            If (Dir(MyFile) = "") Then
                f = f + 1
                AllFiles(f) = AFileName
                AllOrders(f) = OrdId
                iFreeFile = FreeFile
                FM.OpenFile AFileName, FileOpenMode.Output, FileAccess.ReadWrite, CLng(iFreeFile)
                Print #iFreeFile, "QUESTION=/" + Trim(Ques)
                Call FormatTimeNow(TestTime)
                RscId = UserLogin.iId
                AName = UserLogin.sNameShort
                If (Trim(AName) <> "") Then
                    Print #iFreeFile, "TIME=" + TestTime + " (" + AName + ")"
                Else
                    Print #iFreeFile, "TIME=" + TestTime
                End If
                FM.CloseFile CLng(iFreeFile)
                If (CurrCnt(f) < 0) Then
                    CurrCnt(f) = 0
                End If
                CurrCnt(f) = CurrCnt(f) + 1
                If (SetDeviceRButton(APiece1(g), FrmId, CurrCnt(f), AFileName)) Then
                    Call PinpointError("frmTestResults", "Content OD " + APiece1(g) + " " + APiece2(g))
                End If
            Else
                X = -1
                For f = 1 To 4
                    If (AllFiles(f) = AFileName) Then
                        X = f
                        Exit For
                    End If
                Next f
                If (X <> -1) Then
                    f = X
                End If
            End If
            If (CurODEyeSet(f)) Then
                If (SetDeviceEye(APiece1(g), "OD", FrmId, AFileName)) Then
                    Call PinpointError("frmTestResults", "Eye OD " + APiece1(g) + " " + APiece2(g))
                End If
                CurODEyeSet(f) = False
            End If
            If (SetDeviceData(APiece1(g), APiece2(g), FrmId, AFileName)) Then
                Call PinpointError("frmTestResults", "Wrote OD " + APiece1(g) + " " + APiece2(g))
            End If
        End If
    End If
    Return
PostOSTestData:
    If (Trim(APiece1(g)) <> "") Then
        If (GetTestName(APiece1(g), FrmId, Ques, OrdId)) Then
            If (InitialPull) Then
'                If (OrdId = "07A") Then
'                    frmEventMsgs.Header = "Is this a new pair ?"
'                    frmEventMsgs.AcceptText = ""
'                    frmEventMsgs.RejectText = "Yes"
'                    frmEventMsgs.CancelText = "No"
'                    frmEventMsgs.Other0Text = ""
'                    frmEventMsgs.Other1Text = ""
'                    frmEventMsgs.Other2Text = ""
'                    frmEventMsgs.Other3Text = ""
'                    frmEventMsgs.Other4Text = ""
'                    frmEventMsgs.Show 1
'                    If (frmEventMsgs.Result = 2) Then
'                        GlassesOn = True
'                    End If
'                End If
                AFileName = GetTestFileName(OrdId)
                MyFile = AFileName
                If (Dir(MyFile) <> "") Then
                    FM.Kill AFileName
                End If
                InitialPull = False
            End If
            AFileName = GetTestFileName(OrdId)
            MyFile = AFileName
            If (Dir(MyFile) = "") Then
                f = f + 1
                AllFiles(f) = AFileName
                AllOrders(f) = OrdId
                iFreeFile = FreeFile
                FM.OpenFile AFileName, FileOpenMode.Output, FileAccess.ReadWrite, CLng(iFreeFile)
                Print #iFreeFile, "QUESTION=/" + Trim(Ques)
                Call FormatTimeNow(TestTime)
                RscId = UserLogin.iId
                AName = UserLogin.sNameShort
                If (Trim(AName) <> "") Then
                    Print #iFreeFile, "TIME=" + TestTime + " (" + AName + ")"
                Else
                    Print #iFreeFile, "TIME=" + TestTime
                End If
                FM.CloseFile CLng(iFreeFile)
                If (CurrCnt(f) < 0) Then
                    CurrCnt(f) = 0
                End If
                CurrCnt(f) = CurrCnt(f) + 1
                Call SetDeviceRButton(APiece1(g), FrmId, CurrCnt(f), AFileName)
            Else
                X = -1
                For f = 1 To 4
                    If (AllFiles(f) = AFileName) Then
                        X = f
                        Exit For
                    End If
                Next f
                If (X <> -1) Then
                    f = X
                End If
            End If
            If (CurOSEyeSet(f)) Then
                Call SetDeviceEye(APiece1(g), "OS", FrmId, AFileName)
                CurOSEyeSet(f) = False
            End If
            Call SetDeviceData(APiece1(g), APiece3(g), FrmId, AFileName)
            Call PinpointError("frmTestResults", "Wrote OS " + APiece1(g) + " " + APiece3(g))
        End If
    End If
    Return

lcmdExternal_Click_Error:

    LogError "frmTestResults", "cmdExternal_Click", Err, Err.Description
End Sub

Private Sub cmdHome_Click()
ExitFast = True
Set ClinicalClass = Nothing
Unload frmTestResults
FormOn = False
End Sub

Private Sub cmdMore_Click()
'Dim j As Integer
'If (TestResultsIndex < 1) Then
'    TestResultsIndex = 1
'End If
'TestResultsIndex = TestResultsIndex + 9
'j = frmSystems1.GetTotalTests
'If (TestResultsIndex > j) Then
'    TestResultsIndex = 1
'End If
'Call ViewTestResults(TestResultsIndex)
'If (j > 9) Then
'    cmdMore.Visible = True
'    cmdPrev.Visible = True
'Else
'    cmdMore.Visible = False
'    cmdPrev.Visible = False
'End If

Call ViewTestResults(LastElementOnPage + 1)

End Sub

Private Sub cmdAllTests_Click()
CurrentIndex = 1
Call ExtractTests
PracticeViewOn = False
FavoriteTestViewOn = False
End Sub

Private Sub cmdPrcTests_Click()
CurrentIndex = 1
FavoriteTestViewOn = False
PracticeViewOn = ExtractTestsbyPractice
If Not (PracticeViewOn) Then
    PracticeViewOn = False
    FavoriteTestViewOn = False
    Call ExtractTests
End If
End Sub

Private Sub cmdMyTests_Click()
CurrentIndex = 1
PracticeViewOn = False
FavoriteTestViewOn = ExtractTestsbyFavorites
If Not (FavoriteTestViewOn) Then
    PracticeViewOn = False
    FavoriteTestViewOn = False
    Call ExtractTests
End If
End Sub

Private Sub cmdPrev_Click()
'Dim j As Integer
'TestResultsIndex = TestResultsIndex - 18
'If (TestResultsIndex < 1) Then
'    TestResultsIndex = 1
'End If
'Call ViewTestResults(TestResultsIndex)
'j = frmSystems1.GetTotalTests
'If (j > 9) Then
'    cmdMore.Visible = True
'    cmdPrev.Visible = True
'Else
'    cmdMore.Visible = False
'    cmdPrev.Visible = False
'End If
'
Dim i As Integer
Dim Cntr As Integer
Dim AHigh As Boolean
Dim ThePO As Integer
Dim TheType As String
Dim TheText As String
Dim TheOrder As String
Dim TheQuestion As String
Dim TheTime As String
Dim TheParty As String
Dim ATests As Integer

For i = LastElementOnPage To 1 Step -1
    Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
    If Not SkipTest(TheQuestion) Then
        Cntr = Cntr + 1
    End If
    If Cntr = 9 Then Exit For
Next

Call ViewTestResults(i)
End Sub

Private Sub cmdMagnify1_Click()
If (txtZoom.Visible) Then
    txtZoom.Visible = False
    txtZoom.Text = ""
    cmdMagnify1.Visible = IsZoomOn(txtTest1)
    cmdMagnify2.Visible = IsZoomOn(txtTest2)
    cmdMagnify3.Visible = IsZoomOn(txtTest3)
    cmdMagnify4.Visible = IsZoomOn(txtTest4)
    cmdMagnify5.Visible = IsZoomOn(txtTest5)
    cmdMagnify6.Visible = IsZoomOn(txtTest6)
    cmdMagnify7.Visible = IsZoomOn(txtTest7)
    cmdMagnify8.Visible = IsZoomOn(txtTest8)
    cmdMagnify9.Visible = IsZoomOn(txtTest9)
Else
    txtZoom.Text = txtTest1.Text
    txtZoom.Visible = True
    cmdMagnify1.Visible = True
    cmdMagnify2.Visible = False
    cmdMagnify3.Visible = False
    cmdMagnify4.Visible = False
    cmdMagnify5.Visible = False
    cmdMagnify6.Visible = False
    cmdMagnify7.Visible = False
    cmdMagnify8.Visible = False
    cmdMagnify9.Visible = False
End If
End Sub

Private Sub cmdMagnify2_Click()
If (txtZoom.Visible) Then
    txtZoom.Visible = False
    txtZoom.Text = ""
    cmdMagnify1.Visible = IsZoomOn(txtTest1)
    cmdMagnify2.Visible = IsZoomOn(txtTest2)
    cmdMagnify3.Visible = IsZoomOn(txtTest3)
    cmdMagnify4.Visible = IsZoomOn(txtTest4)
    cmdMagnify5.Visible = IsZoomOn(txtTest5)
    cmdMagnify6.Visible = IsZoomOn(txtTest6)
    cmdMagnify7.Visible = IsZoomOn(txtTest7)
    cmdMagnify8.Visible = IsZoomOn(txtTest8)
    cmdMagnify9.Visible = IsZoomOn(txtTest9)
Else
    txtZoom.Text = txtTest2.Text
    txtZoom.Visible = True
    cmdMagnify1.Visible = False
    cmdMagnify2.Visible = True
    cmdMagnify3.Visible = False
    cmdMagnify4.Visible = False
    cmdMagnify5.Visible = False
    cmdMagnify6.Visible = False
    cmdMagnify7.Visible = False
    cmdMagnify8.Visible = False
    cmdMagnify9.Visible = False
End If
End Sub

Private Sub cmdMagnify3_Click()
If (txtZoom.Visible) Then
    txtZoom.Visible = False
    txtZoom.Text = ""
    cmdMagnify1.Visible = IsZoomOn(txtTest1)
    cmdMagnify2.Visible = IsZoomOn(txtTest2)
    cmdMagnify3.Visible = IsZoomOn(txtTest3)
    cmdMagnify4.Visible = IsZoomOn(txtTest4)
    cmdMagnify5.Visible = IsZoomOn(txtTest5)
    cmdMagnify6.Visible = IsZoomOn(txtTest6)
    cmdMagnify7.Visible = IsZoomOn(txtTest7)
    cmdMagnify8.Visible = IsZoomOn(txtTest8)
    cmdMagnify9.Visible = IsZoomOn(txtTest9)
Else
    txtZoom.Text = txtTest3.Text
    txtZoom.Visible = True
    cmdMagnify1.Visible = False
    cmdMagnify2.Visible = False
    cmdMagnify3.Visible = True
    cmdMagnify4.Visible = False
    cmdMagnify5.Visible = False
    cmdMagnify6.Visible = False
    cmdMagnify7.Visible = False
    cmdMagnify8.Visible = False
    cmdMagnify9.Visible = False
End If
End Sub

Private Sub cmdMagnify4_Click()
If (txtZoom.Visible) Then
    txtZoom.Visible = False
    txtZoom.Text = ""
    cmdMagnify1.Visible = IsZoomOn(txtTest1)
    cmdMagnify2.Visible = IsZoomOn(txtTest2)
    cmdMagnify3.Visible = IsZoomOn(txtTest3)
    cmdMagnify4.Visible = IsZoomOn(txtTest4)
    cmdMagnify5.Visible = IsZoomOn(txtTest5)
    cmdMagnify6.Visible = IsZoomOn(txtTest6)
    cmdMagnify7.Visible = IsZoomOn(txtTest7)
    cmdMagnify8.Visible = IsZoomOn(txtTest8)
    cmdMagnify9.Visible = IsZoomOn(txtTest9)
Else
    txtZoom.Text = txtTest4.Text
    txtZoom.Visible = True
    cmdMagnify1.Visible = False
    cmdMagnify2.Visible = False
    cmdMagnify3.Visible = False
    cmdMagnify4.Visible = True
    cmdMagnify5.Visible = False
    cmdMagnify6.Visible = False
    cmdMagnify7.Visible = False
    cmdMagnify8.Visible = False
    cmdMagnify9.Visible = False
End If
End Sub

Private Sub cmdMagnify5_Click()
If (txtZoom.Visible) Then
    txtZoom.Visible = False
    txtZoom.Text = ""
    cmdMagnify1.Visible = IsZoomOn(txtTest1)
    cmdMagnify2.Visible = IsZoomOn(txtTest2)
    cmdMagnify3.Visible = IsZoomOn(txtTest3)
    cmdMagnify4.Visible = IsZoomOn(txtTest4)
    cmdMagnify5.Visible = IsZoomOn(txtTest5)
    cmdMagnify6.Visible = IsZoomOn(txtTest6)
    cmdMagnify7.Visible = IsZoomOn(txtTest7)
    cmdMagnify8.Visible = IsZoomOn(txtTest8)
    cmdMagnify9.Visible = IsZoomOn(txtTest9)
Else
    txtZoom.Text = txtTest5.Text
    txtZoom.Visible = True
    cmdMagnify1.Visible = False
    cmdMagnify2.Visible = False
    cmdMagnify3.Visible = False
    cmdMagnify4.Visible = False
    cmdMagnify5.Visible = True
    cmdMagnify6.Visible = False
    cmdMagnify7.Visible = False
    cmdMagnify8.Visible = False
    cmdMagnify9.Visible = False
End If
End Sub

Private Sub cmdMagnify6_Click()
If (txtZoom.Visible) Then
    txtZoom.Visible = False
    txtZoom.Text = ""
    cmdMagnify1.Visible = IsZoomOn(txtTest1)
    cmdMagnify2.Visible = IsZoomOn(txtTest2)
    cmdMagnify3.Visible = IsZoomOn(txtTest3)
    cmdMagnify4.Visible = IsZoomOn(txtTest4)
    cmdMagnify5.Visible = IsZoomOn(txtTest5)
    cmdMagnify6.Visible = IsZoomOn(txtTest6)
    cmdMagnify7.Visible = IsZoomOn(txtTest7)
    cmdMagnify8.Visible = IsZoomOn(txtTest8)
    cmdMagnify9.Visible = IsZoomOn(txtTest9)
Else
    txtZoom.Text = txtTest6.Text
    txtZoom.Visible = True
    cmdMagnify1.Visible = False
    cmdMagnify2.Visible = False
    cmdMagnify3.Visible = False
    cmdMagnify4.Visible = False
    cmdMagnify5.Visible = False
    cmdMagnify6.Visible = True
    cmdMagnify7.Visible = False
    cmdMagnify8.Visible = False
    cmdMagnify9.Visible = False
End If
End Sub

Private Sub cmdMagnify7_Click()
If (txtZoom.Visible) Then
    txtZoom.Visible = False
    txtZoom.Text = ""
    cmdMagnify1.Visible = IsZoomOn(txtTest1)
    cmdMagnify2.Visible = IsZoomOn(txtTest2)
    cmdMagnify3.Visible = IsZoomOn(txtTest3)
    cmdMagnify4.Visible = IsZoomOn(txtTest4)
    cmdMagnify5.Visible = IsZoomOn(txtTest5)
    cmdMagnify6.Visible = IsZoomOn(txtTest6)
    cmdMagnify7.Visible = IsZoomOn(txtTest7)
    cmdMagnify8.Visible = IsZoomOn(txtTest8)
    cmdMagnify9.Visible = IsZoomOn(txtTest9)
Else
    txtZoom.Text = txtTest7.Text
    txtZoom.Visible = True
    cmdMagnify1.Visible = False
    cmdMagnify2.Visible = False
    cmdMagnify3.Visible = False
    cmdMagnify4.Visible = False
    cmdMagnify5.Visible = False
    cmdMagnify6.Visible = False
    cmdMagnify7.Visible = True
    cmdMagnify8.Visible = False
    cmdMagnify9.Visible = False
End If
End Sub

Private Sub cmdMagnify8_Click()
If (txtZoom.Visible) Then
    txtZoom.Visible = False
    txtZoom.Text = ""
    cmdMagnify1.Visible = IsZoomOn(txtTest1)
    cmdMagnify2.Visible = IsZoomOn(txtTest2)
    cmdMagnify3.Visible = IsZoomOn(txtTest3)
    cmdMagnify4.Visible = IsZoomOn(txtTest4)
    cmdMagnify5.Visible = IsZoomOn(txtTest5)
    cmdMagnify6.Visible = IsZoomOn(txtTest6)
    cmdMagnify7.Visible = IsZoomOn(txtTest7)
    cmdMagnify8.Visible = IsZoomOn(txtTest8)
    cmdMagnify9.Visible = IsZoomOn(txtTest9)
Else
    txtZoom.Text = txtTest8.Text
    txtZoom.Visible = True
    cmdMagnify1.Visible = False
    cmdMagnify2.Visible = False
    cmdMagnify3.Visible = False
    cmdMagnify4.Visible = False
    cmdMagnify5.Visible = False
    cmdMagnify6.Visible = False
    cmdMagnify7.Visible = False
    cmdMagnify8.Visible = True
    cmdMagnify9.Visible = False
End If
End Sub

Private Sub cmdMagnify9_Click()
If (txtZoom.Visible) Then
    txtZoom.Visible = False
    txtZoom.Text = ""
    cmdMagnify1.Visible = IsZoomOn(txtTest1)
    cmdMagnify2.Visible = IsZoomOn(txtTest2)
    cmdMagnify3.Visible = IsZoomOn(txtTest3)
    cmdMagnify4.Visible = IsZoomOn(txtTest4)
    cmdMagnify5.Visible = IsZoomOn(txtTest5)
    cmdMagnify6.Visible = IsZoomOn(txtTest6)
    cmdMagnify7.Visible = IsZoomOn(txtTest7)
    cmdMagnify8.Visible = IsZoomOn(txtTest8)
    cmdMagnify9.Visible = IsZoomOn(txtTest9)
Else
    txtZoom.Text = txtTest9.Text
    txtZoom.Visible = True
    cmdMagnify1.Visible = False
    cmdMagnify2.Visible = False
    cmdMagnify3.Visible = False
    cmdMagnify4.Visible = False
    cmdMagnify5.Visible = False
    cmdMagnify6.Visible = False
    cmdMagnify7.Visible = False
    cmdMagnify8.Visible = False
    cmdMagnify9.Visible = True
End If
End Sub

Private Sub cmdNextTest_Click()
If (CurrentIndex > TotalTestResults) Then
    CurrentIndex = 1
End If
If (FavoriteTestViewOn) Then
    Call ExtractTestsbyFavorites
ElseIf (PracticeViewOn) Then
    Call ExtractTestsbyPractice
Else
    Call ExtractTests
End If
End Sub

Private Sub cmdPrevTest_Click()
CurrentIndex = CurrentIndex - 22
If (CurrentIndex < 1) Then
    CurrentIndex = TotalTestResults - 10
End If
If (FavoriteTestViewOn) Then
    Call ExtractTestsbyFavorites
ElseIf (PracticeViewOn) Then
    Call ExtractTestsbyPractice
Else
    Call ExtractTests
End If
End Sub

Private Sub cmdNotes_Click()
frmNotes.NoteId = 0
frmNotes.SystemReference = ""
frmNotes.EyeContext = ""
frmNotes.NoteOn = False
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.SetTo = "C"
frmNotes.MaintainOn = True
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    If (frmNotes.NoteOn) Then
        Call LoadSummaryInfo(ModalSetting)
    End If
End If
End Sub

Private Function LoadExamInfo() As Boolean
txtSummary.Text = frmSystems1.txtSummary
txtSum1.Text = frmSystems1.txtSum1
txtSum2.Text = frmSystems1.txtSum2
txtGen.Text = frmSystems1.txtGen
txtSummary.Locked = True
txtSum1.Locked = True
txtSum2.Locked = True
txtGen.Locked = True
ResourceId = UserLogin.iId
PracticeViewOn = False
FavoriteTestViewOn = ExtractTestsbyFavorites
If Not (FavoriteTestViewOn) Then
    Call ExtractTests
End If
Call Timer1_Timer
Call ViewTestResults(1)
txtSummary.Locked = True
LoadExamInfo = True
End Function

Public Function LoadSummaryInfo(MSet As Integer) As Boolean
TestResultsIndex = 1
ModalSetting = MSet
LoadSummaryInfo = LoadExamInfo
End Function

Private Function ViewTestResults(startPoint As Integer) As Boolean
Dim i As Integer
Dim AHigh As Boolean
Dim NoteText As String
Dim ThePO As Integer
Dim TheType As String
Dim TheText As String
Dim TheOrder As String
Dim TheQuestion As String
Dim TheTime As String
Dim TheParty As String
Dim ATests As Integer
TheText = ""
TheOrder = ""
TheQuestion = ""
txtTest1.Text = ""
txtTest2.Text = ""
txtTest3.Text = ""
txtTest4.Text = ""
txtTest5.Text = ""
txtTest6.Text = ""
txtTest7.Text = ""
txtTest8.Text = ""
txtTest9.Text = ""
ATests = frmSystems1.TotalEnteredTests
i = startPoint
'-------------------------------------------
step1:
If (i > ATests) Then
    Exit Function
End If
Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
If SkipTest(TheQuestion) Then
    i = i + 1
    GoTo step1
End If
NoteText = ""
Call AddNotesToTest(PatientId, AppointmentId, TheOrder, NoteText)
txtTest1.Locked = False
If (Trim(NoteText) <> "") Then
    txtTest1.Text = TheText + Chr(13) + Chr(10) + NoteText + Chr(13) + Chr(10) + TheParty
Else
    txtTest1.Text = TheText + Chr(13) + Chr(10) + TheParty
End If
txtTest1.Tag = TheQuestion + "_" + TheOrder
CurrentLine(1) = 1
If (Trim(txtTest1.Text) <> "") And (txtTest1.Text <> Chr(13) + Chr(10)) Then
    cmdMagnify1.Visible = True
Else
    cmdMagnify1.Visible = False
End If

FirstElementOnPage = i

i = i + 1
'-------------------------------------------
step2:
If (i > ATests) Then
    Exit Function
End If
Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
If SkipTest(TheQuestion) Then
    i = i + 1
    GoTo step2
End If
NoteText = ""
Call AddNotesToTest(PatientId, AppointmentId, TheOrder, NoteText)
txtTest2.Locked = False
If (Trim(NoteText) <> "") Then
    txtTest2.Text = TheText + Chr(13) + Chr(10) + NoteText + Chr(13) + Chr(10) + TheParty
Else
    txtTest2.Text = TheText + Chr(13) + Chr(10) + TheParty
End If
txtTest2.Tag = TheQuestion + "_" + TheOrder
CurrentLine(2) = 1
If (Trim(txtTest2.Text) <> "") And (txtTest2.Text <> Chr(13) + Chr(10)) Then
    cmdMagnify2.Visible = True
Else
    cmdMagnify2.Visible = False
End If
i = i + 1
'-------------------------------------------
step3:
If (i > ATests) Then
    Exit Function
End If
Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
If SkipTest(TheQuestion) Then
    i = i + 1
    GoTo step3
End If
NoteText = ""
Call AddNotesToTest(PatientId, AppointmentId, TheOrder, NoteText)
txtTest3.Locked = False
If (Trim(NoteText) <> "") Then
    txtTest3.Text = TheText + Chr(13) + Chr(10) + NoteText + Chr(13) + Chr(10) + TheParty
Else
    txtTest3.Text = TheText + Chr(13) + Chr(10) + TheParty
End If
txtTest3.Tag = TheQuestion + "_" + TheOrder
CurrentLine(3) = 1
If (Trim(txtTest3.Text) <> "") And (txtTest3.Text <> Chr(13) + Chr(10)) Then
    cmdMagnify3.Visible = True
Else
    cmdMagnify3.Visible = False
End If
i = i + 1
'-------------------------------------------
step4:
If (i > ATests) Then
    Exit Function
End If
Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
If SkipTest(TheQuestion) Then
    i = i + 1
    GoTo step4
End If
NoteText = ""
Call AddNotesToTest(PatientId, AppointmentId, TheOrder, NoteText)
txtTest4.Locked = False
If (Trim(NoteText) <> "") Then
    txtTest4.Text = TheText + Chr(13) + Chr(10) + NoteText + Chr(13) + Chr(10) + TheParty
Else
    txtTest4.Text = TheText + Chr(13) + Chr(10) + TheParty
End If
txtTest4.Tag = TheQuestion + "_" + TheOrder
CurrentLine(4) = 1
If (Trim(txtTest4.Text) <> "") And (txtTest4.Text <> Chr(13) + Chr(10)) Then
    cmdMagnify4.Visible = True
Else
    cmdMagnify4.Visible = False
End If
i = i + 1
'-------------------------------------------
step5:
If (i > ATests) Then
    Exit Function
End If
Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
If SkipTest(TheQuestion) Then
    i = i + 1
    GoTo step5
End If
NoteText = ""
Call AddNotesToTest(PatientId, AppointmentId, TheOrder, NoteText)
txtTest5.Locked = False
If (Trim(NoteText) <> "") Then
    txtTest5.Text = TheText + Chr(13) + Chr(10) + NoteText + Chr(13) + Chr(10) + TheParty
Else
    txtTest5.Text = TheText + Chr(13) + Chr(10) + TheParty
End If
txtTest5.Tag = TheQuestion + "_" + TheOrder
CurrentLine(5) = 1
If (Trim(txtTest5.Text) <> "") And (txtTest5.Text <> Chr(13) + Chr(10)) Then
    cmdMagnify5.Visible = True
Else
    cmdMagnify5.Visible = False
End If
i = i + 1
'-------------------------------------------
step6:
If (i > ATests) Then
    Exit Function
End If
Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
If SkipTest(TheQuestion) Then
    i = i + 1
    GoTo step6
End If
NoteText = ""
Call AddNotesToTest(PatientId, AppointmentId, TheOrder, NoteText)
txtTest6.Locked = False
If (Trim(NoteText) <> "") Then
    txtTest6.Text = TheText + Chr(13) + Chr(10) + NoteText + Chr(13) + Chr(10) + TheParty
Else
    txtTest6.Text = TheText + Chr(13) + Chr(10) + TheParty
End If
txtTest6.Tag = TheQuestion + "_" + TheOrder
CurrentLine(6) = 1
If (Trim(txtTest6.Text) <> "") And (txtTest6.Text <> Chr(13) + Chr(10)) Then
    cmdMagnify6.Visible = True
Else
    cmdMagnify6.Visible = False
End If
i = i + 1
'-------------------------------------------
step7:
If (i > ATests) Then
    Exit Function
End If
Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
If SkipTest(TheQuestion) Then
    i = i + 1
    GoTo step7
End If
NoteText = ""
Call AddNotesToTest(PatientId, AppointmentId, TheOrder, NoteText)
txtTest7.Locked = False
If (Trim(NoteText) <> "") Then
    txtTest7.Text = TheText + Chr(13) + Chr(10) + NoteText + Chr(13) + Chr(10) + TheParty
Else
    txtTest7.Text = TheText + Chr(13) + Chr(10) + TheParty
End If
txtTest7.Tag = TheQuestion + "_" + TheOrder
CurrentLine(7) = 1
If (Trim(txtTest7.Text) <> "") And (txtTest7.Text <> Chr(13) + Chr(10)) Then
    cmdMagnify7.Visible = True
Else
    cmdMagnify7.Visible = False
End If
i = i + 1
'-------------------------------------------
step8:
If (i > ATests) Then
    Exit Function
End If
Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
If SkipTest(TheQuestion) Then
    i = i + 1
    GoTo step8
End If
NoteText = ""
Call AddNotesToTest(PatientId, AppointmentId, TheOrder, NoteText)
txtTest8.Locked = False
If (Trim(NoteText) <> "") Then
    txtTest8.Text = TheText + Chr(13) + Chr(10) + NoteText + Chr(13) + Chr(10) + TheParty
Else
    txtTest8.Text = TheText + Chr(13) + Chr(10) + TheParty
End If
txtTest8.Tag = TheQuestion + "_" + TheOrder
CurrentLine(8) = 1
If (Trim(txtTest8.Text) <> "") And (txtTest8.Text <> Chr(13) + Chr(10)) Then
    cmdMagnify8.Visible = True
Else
    cmdMagnify8.Visible = False
End If
i = i + 1
'-------------------------------------------
step9:
If (i > ATests) Then
    Exit Function
End If
Call frmSystems1.GetTestText(i, TheText, TheQuestion, TheOrder, TheParty, TheTime, AHigh, TheType, ThePO)
If SkipTest(TheQuestion) Then
    i = i + 1
    GoTo step9
End If
NoteText = ""
Call AddNotesToTest(PatientId, AppointmentId, TheOrder, NoteText)
txtTest9.Locked = False
If (Trim(NoteText) <> "") Then
    txtTest9.Text = TheText + Chr(13) + Chr(10) + NoteText + Chr(13) + Chr(10) + TheParty
Else
    txtTest9.Text = TheText + Chr(13) + Chr(10) + TheParty
End If
txtTest9.Tag = TheQuestion + "_" + TheOrder
CurrentLine(9) = 1
If (Trim(txtTest9.Text) <> "") And (txtTest9.Text <> Chr(13) + Chr(10)) Then
    cmdMagnify9.Visible = True
Else
    cmdMagnify9.Visible = False
End If
i = i + 1
'-------------------------------------------

If (i > ATests) Then
    Exit Function
End If

LastElementOnPage = i - 1

If (frmSystems1.TotalEnteredTests > 9) Then
    cmdMore.Visible = True
    cmdPrev.Visible = True
Else
    cmdMore.Visible = False
    cmdPrev.Visible = False
End If
End Function

Private Function SkipTest(inp As String) As Boolean

Dim i As Integer
Dim q(10) As String
'q(0) = "GLASSES"
q(1) = "CONTACT LENSES"
q(2) = "RETINOSCOPY"
'q(3) = "AUTOREFRACTION"
q(4) = "MANIFEST REFRACTION"
q(5) = "SPEC RX"
q(6) = "CL RX"
q(7) = "CL TRIAL 1"
q(8) = "CL TRIAL 2"
q(9) = "CL ORDER TRIAL"
q(10) = "DILATED REFRACTION"

For i = 0 To UBound(q)
    If inp = q(i) Then
        SkipTest = True
        Exit For
    End If
Next

End Function



Private Sub ClearButtons()
cmdTest1.Text = ""
cmdTest1.Tag = ""
cmdTest1.Visible = False
cmdTest2.Text = ""
cmdTest2.Tag = ""
cmdTest2.Visible = False
cmdTest3.Text = ""
cmdTest3.Tag = ""
cmdTest3.Visible = False
cmdTest4.Text = ""
cmdTest4.Tag = ""
cmdTest4.Visible = False
cmdTest5.Text = ""
cmdTest5.Tag = ""
cmdTest5.Visible = False
cmdTest6.Text = ""
cmdTest6.Tag = ""
cmdTest6.Visible = False
cmdTest7.Text = ""
cmdTest7.Tag = ""
cmdTest7.Visible = False
cmdTest8.Text = ""
cmdTest8.Tag = ""
cmdTest8.Visible = False
cmdTest9.Text = ""
cmdTest9.Tag = ""
cmdTest9.Visible = False
cmdTest10.Text = ""
cmdTest10.Tag = ""
cmdTest10.Visible = False
cmdTest11.Text = ""
cmdTest11.Tag = ""
cmdTest11.Visible = False
End Sub

Private Function ExtractTests() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
ExtractTests = True
Set ClinicalClass = New DynamicClass
ClinicalClass.QuestionParty = "T"
ClinicalClass.QuestionSet = "First Visit"
TotalTestResults = ClinicalClass.FindClassFormsbyName
If (TotalTestResults > 0) Then
    Call ClearButtons
    ButtonCnt = 1
    i = CurrentIndex
    While (ClinicalClass.SelectClassForm(i)) And (ButtonCnt < 12)
        If (ButtonCnt = 1) Then
            cmdTest1.Text = ClinicalClass.Question
            cmdTest1.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest1.Visible = True
        ElseIf (ButtonCnt = 2) Then
            cmdTest2.Text = ClinicalClass.Question
            cmdTest2.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest2.Visible = True
        ElseIf (ButtonCnt = 3) Then
            cmdTest3.Text = ClinicalClass.Question
            cmdTest3.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest3.Visible = True
        ElseIf (ButtonCnt = 4) Then
            cmdTest4.Text = ClinicalClass.Question
            cmdTest4.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest4.Visible = True
        ElseIf (ButtonCnt = 5) Then
            cmdTest5.Text = ClinicalClass.Question
            cmdTest5.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest5.Visible = True
        ElseIf (ButtonCnt = 6) Then
            cmdTest6.Text = ClinicalClass.Question
            cmdTest6.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest6.Visible = True
        ElseIf (ButtonCnt = 7) Then
            cmdTest7.Text = ClinicalClass.Question
            cmdTest7.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest7.Visible = True
        ElseIf (ButtonCnt = 8) Then
            cmdTest8.Text = ClinicalClass.Question
            cmdTest8.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest8.Visible = True
        ElseIf (ButtonCnt = 9) Then
            cmdTest9.Text = ClinicalClass.Question
            cmdTest9.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest9.Visible = True
        ElseIf (ButtonCnt = 10) Then
            cmdTest10.Text = ClinicalClass.Question
            cmdTest10.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest10.Visible = True
        ElseIf (ButtonCnt = 11) Then
            cmdTest11.Text = ClinicalClass.Question
            cmdTest11.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
            cmdTest11.Visible = True
        End If
        ButtonCnt = ButtonCnt + 1
        i = i + 1
        CurrentIndex = i
    Wend
End If
Set ClinicalClass = Nothing
If (TotalTestResults > 11) Then
    cmdPrevTest.Visible = True
    cmdNextTest.Visible = True
Else
    cmdPrevTest.Visible = False
    cmdNextTest.Visible = False
End If
End Function

Private Function ExtractTestsbyFavorites() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim ClinicalClass As DynamicClass
Dim RetFav As PracticeFavorites
ExtractTestsbyFavorites = False
Set ClinicalClass = New DynamicClass
Set RetFav = New PracticeFavorites
RetFav.FavoritesSystem = "<" + Trim(str(ResourceId))
RetFav.FavoritesDiagnosis = ""
TotalTestResults = RetFav.FindFavorites
If (TotalTestResults > 0) Then
    ExtractTestsbyFavorites = True
    Call ClearButtons
    i = CurrentIndex
    ButtonCnt = 1
    While (RetFav.SelectFavorites(i)) And (ButtonCnt < 12)
        ClinicalClass.ClassId = Val(Trim(RetFav.FavoritesDiagnosis))
        If (ClinicalClass.RetrieveClass) Then
            If (ButtonCnt = 1) Then
                cmdTest1.Text = ClinicalClass.Question
                cmdTest1.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdTest2.Text = ClinicalClass.Question
                cmdTest2.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdTest3.Text = ClinicalClass.Question
                cmdTest3.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdTest4.Text = ClinicalClass.Question
                cmdTest4.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdTest5.Text = ClinicalClass.Question
                cmdTest5.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdTest6.Text = ClinicalClass.Question
                cmdTest6.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdTest7.Text = ClinicalClass.Question
                cmdTest7.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdTest8.Text = ClinicalClass.Question
                cmdTest8.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdTest9.Text = ClinicalClass.Question
                cmdTest9.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdTest10.Text = ClinicalClass.Question
                cmdTest10.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdTest11.Text = ClinicalClass.Question
                cmdTest11.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest11.Visible = True
            End If
            ButtonCnt = ButtonCnt + 1
        End If
        i = i + 1
        CurrentIndex = i
    Wend
End If
Set ClinicalClass = Nothing
Set RetFav = Nothing
If (TotalTestResults > 11) Then
    cmdPrevTest.Visible = True
    cmdNextTest.Visible = True
Else
    cmdPrevTest.Visible = False
    cmdNextTest.Visible = False
End If
End Function

Private Function ExtractTestsbyPractice() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim ClinicalClass As DynamicClass
Dim RetFav As PracticeFavorites
ExtractTestsbyPractice = False
Set ClinicalClass = New DynamicClass
Set RetFav = New PracticeFavorites
RetFav.FavoritesSystem = "<" + Trim(str(0))
RetFav.FavoritesDiagnosis = ""
TotalTestResults = RetFav.FindFavorites
If (TotalTestResults < 1) Then
    RetFav.FavoritesSystem = "<" + Trim(str(0))
    RetFav.FavoritesDiagnosis = Chr(1)
    TotalTestResults = RetFav.FindFavorites
End If
If (TotalTestResults > 0) Then
    ExtractTestsbyPractice = True
    Call ClearButtons
    i = CurrentIndex
    ButtonCnt = 1
    While (RetFav.SelectFavorites(i)) And (ButtonCnt < 12)
        ClinicalClass.ClassId = Val(Trim(RetFav.FavoritesDiagnosis))
        If (ClinicalClass.RetrieveClass) Then
            If (ButtonCnt = 1) Then
                cmdTest1.Text = ClinicalClass.Question
                cmdTest1.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdTest2.Text = ClinicalClass.Question
                cmdTest2.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdTest3.Text = ClinicalClass.Question
                cmdTest3.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdTest4.Text = ClinicalClass.Question
                cmdTest4.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdTest5.Text = ClinicalClass.Question
                cmdTest5.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdTest6.Text = ClinicalClass.Question
                cmdTest6.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdTest7.Text = ClinicalClass.Question
                cmdTest7.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdTest8.Text = ClinicalClass.Question
                cmdTest8.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdTest9.Text = ClinicalClass.Question
                cmdTest9.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdTest10.Text = ClinicalClass.Question
                cmdTest10.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdTest11.Text = ClinicalClass.Question
                cmdTest11.Tag = ClinicalClass.Question + "_" + ClinicalClass.QuestionOrder
                cmdTest11.Visible = True
            End If
            ButtonCnt = ButtonCnt + 1
        End If
        i = i + 1
        CurrentIndex = i
    Wend
End If
Set ClinicalClass = Nothing
Set RetFav = Nothing
If (TotalTestResults > 11) Then
    cmdPrevTest.Visible = True
    cmdNextTest.Visible = True
Else
    cmdPrevTest.Visible = False
    cmdNextTest.Visible = False
End If
End Function

Private Sub cmdTest1_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest1.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest1.Tag, q + 1, Len(cmdTest1.Tag) - q)
    Question = Trim(Left(cmdTest1.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest2_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest2.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest2.Tag, q + 1, Len(cmdTest2.Tag) - q)
    Question = Trim(Left(cmdTest2.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest3_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest3.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest3.Tag, q + 1, Len(cmdTest3.Tag) - q)
    Question = Trim(Left(cmdTest3.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest4_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest4.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest4.Tag, q + 1, Len(cmdTest4.Tag) - q)
    Question = Trim(Left(cmdTest4.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest5_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest5.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest5.Tag, q + 1, Len(cmdTest5.Tag) - q)
    Question = Trim(Left(cmdTest5.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest6_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest6.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest6.Tag, q + 1, Len(cmdTest6.Tag) - q)
    Question = Trim(Left(cmdTest6.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest7_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest7.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest7.Tag, q + 1, Len(cmdTest7.Tag) - q)
    Question = Trim(Left(cmdTest7.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest8_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest8.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest8.Tag, q + 1, Len(cmdTest8.Tag) - q)
    Question = Trim(Left(cmdTest8.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest9_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest9.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest9.Tag, q + 1, Len(cmdTest9.Tag) - q)
    Question = Trim(Left(cmdTest9.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest10_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest10.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest10.Tag, q + 1, Len(cmdTest10.Tag) - q)
    Question = Trim(Left(cmdTest10.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Private Sub cmdTest11_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(cmdTest11.Tag, "_")
If (q <> 0) Then
    Temp = Mid(cmdTest11.Tag, q + 1, Len(cmdTest11.Tag) - q)
    Question = Trim(Left(cmdTest11.Tag, q - 1))
    Call DoTest(Question, Temp, False, False)
End If
End Sub

Public Function DoTest(Question As String, FileId As String, EditSet As Boolean, LocalTrigger As Boolean) As Boolean


Select Case FileId
Case "07A", "08A", "09A", "09D", "10A", "13A", "07R", "07S", "07T", "07U", "09G"
    Call frmSystems1.ShowRefract
    DoTest = True
    Exit Function
End Select


Dim Sex As String
Dim Race As String
Dim TheFile As String
Dim ProceedOn As Boolean
Dim EditType As Boolean
Dim RetiredOn As Boolean
Dim iFreeFile As Long
If (Trim(FileId) = "") Then
    Exit Function
End If
TheFile = GetTestFileName(FileId)
ProceedOn = True
EditType = GetQuestionEditType(Question, RetiredOn)
If (RetiredOn) Then
    frmEventMsgs.Header = "Retired Test, Cannot Edit"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Function
End If
If (EditType) Then
    frmTestEntry.ActivityId = ActivityId
    frmTestEntry.PatientId = PatientId
    frmTestEntry.AppointmentId = AppointmentId
    frmTestEntry.ChangeOn = False
    frmTestEntry.StoreResults = TheFile
    frmTestEntry.Question = UCase(Question)
    frmTestEntry.FormOn = True
    frmTestEntry.ModalSetting = 1
    frmTestEntry.Show 1
    If (Not LocalTrigger) Then
        frmTestResults.Enabled = False
    End If
Else
    If Not (FM.IsFileThere(TheFile)) And Not (EditSet) Then
        iFreeFile = FreeFile
        FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(iFreeFile)
        Print #iFreeFile, "QUESTION=\" + UCase(Trim(Question))
        FM.CloseFile CLng(iFreeFile)
        EditSet = True
    ElseIf (InStrPS(TheFile, "72A") <> 0) Then
        If (FM.IsFileThere(TheFile + "-Current")) Then
            FM.Kill TheFile + "-Current"
        End If
        If (FM.IsFileThere(TheFile)) Then
            FM.Name TheFile, TheFile + "-Current"
            iFreeFile = FreeFile
            FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(iFreeFile)
            Print #iFreeFile, "QUESTION=\" + UCase(Trim(Question))
            FM.CloseFile CLng(iFreeFile)
            EditSet = True
        End If
    End If
    frmEditTests.StoreResults = TheFile
    frmEditTests.Question = UCase(Question)
    frmEditTests.PatientId = PatientId
    frmEditTests.AppointmentId = AppointmentId
    frmEditTests.FormOn = True
    frmEditTests.Show 1
    If (frmEditTests.QuitOn) Then
        ProceedOn = False
        If (InStrPS(TheFile, "72A") <> 0) Then
            If (FM.IsFileThere(TheFile + "-Current")) Then
                If (FM.IsFileThere(TheFile)) Then
                    FM.Kill TheFile
                End If
                FM.Name TheFile + "-Current", TheFile
            ElseIf (FM.IsFileThere(TheFile)) Then
                FM.Kill TheFile
            End If
        End If
    End If
End If
frmTestResults.ZOrder 0
If Not (LocalTrigger) Then
    frmTestResults.Enabled = True
    If (frmTestEntry.FormColor <> 0) Then
        frmTestResults.BackColor = frmTestEntry.FormColor
    End If
End If
If (ProceedOn) Then
    If (RecapTest(TheFile)) Then
        Call PushList(TheFile)
        Call InsertLog("FrmTestResults.frm", "Captured Vital Sign " & UCase(Question) & " Information for Patient " & PatientId & " ", LoginCatg.ExamElementsInsert, "", "", 0, globalExam.iAppointmentId)
    Else
        Call frmSystems1.ClearTestResults(FileId)
    End If
    If (RaceTest(TheFile, Race, Sex)) Then
        Call PostRace(Race, Sex)
    End If
    Call frmSystems1.DisplayTestResults(True)
    If Not (LocalTrigger) Then
        TestResultsIndex = 1
        Call ViewTestResults(TestResultsIndex)
        frmTestResults.ZOrder 0
    End If
    DoTest = True
End If
End Function

Private Sub Form_Load()
CurrentIndex = 1
ExitFast = False
End Sub

Public Sub PushList(FileName As String)
On Error GoTo UI_ErrorHandler
Dim TheRecord As String
Dim ODOn As Boolean
Dim DoRecapCheck As Boolean
Dim z1 As Integer, z2 As Integer
Dim w As Integer, s As Integer
Dim q As Integer, r As Integer
Dim a As Integer, k As Integer
Dim Y As Integer, z As Integer
Dim FileNum As Integer
Dim MultiTestIndicator As String
Dim PadValue As String
Dim Question As String
Dim TheValue As String
Dim TestTime As String
Dim LocalText As String
Dim LocalQuestion As String
Dim LocalTestName As String
Dim UTemp As String
Dim Recap() As String
Dim PadOrderResult() As String
Dim CurrentLevel As Integer
Dim Level1 As Integer
Dim TestNameLoc As Integer
Dim TestWideName As String
Dim WhereTestNameIs As Integer
Dim ControlLevel1 As DynamicControls
Level1 = 0
FileNum = 0
WhereTestNameIs = 0
Erase Recap
LocalTestName = ""
TestTime = ""
Erase PadOrderResult
ODOn = False
DoRecapCheck = True
FileNum = FreeFile
FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
Do Until (EOF(FileNum))
    Line Input #FileNum, TheRecord
    If (InStrPS(TheRecord, "QUESTION=") <> 0) Then
        Question = Trim(Mid(TheRecord, 10, Len(TheRecord) - 9))
        If (UCase(Trim(Question)) = "/GENERAL MEDICAL OBSERVATION") Then
            DoRecapCheck = False
        End If
        If (UCase(Trim(Question)) = "/GENERAL IMPRESSIONS") Then
            DoRecapCheck = False
        End If
        If (UCase(Trim(Question)) = "/BLOOD COUNT") Then
            DoRecapCheck = False
        End If
    ElseIf (InStrPS(TheRecord, "TIME=") <> 0) Then
        TestTime = Trim(Mid(TheRecord, 6, Len(TheRecord) - 5))
    Else
        r = InStrPS(TheRecord, "=")
        If (r = 0) Then
            r = Len(TheRecord) + 1
        End If
        r = r - 1
        w = r + 3
        Y = InStrPS(w, TheRecord, " ")
        If (Y = 0) Then
            Y = Len(TheRecord) + 1
        End If
        If (Val(Trim(Mid(TheRecord, w, Y - w))) > 0) Then
            Set ControlLevel1 = New DynamicControls
            ControlLevel1.ControlId = Val(Trim(Mid(TheRecord, w, Y - w)))
            ControlLevel1.ControlName = ""
            ControlLevel1.IEChoiceName = ""
            If (ControlLevel1.RetrieveControl) Then
                MultiTestIndicator = ""
                PadValue = ""
                TheValue = ""
                If (ControlLevel1.ControlType = "N") Or (ControlLevel1.ControlType = "S") Then
                    Y = InStrPS(w, TheRecord, "<")
                    If (Y <> 0) Then
                        z = InStrPS(w, TheRecord, ">")
                        If (z <> 0) Then
                            PadValue = "@" + Mid(TheRecord, Y + 1, (z - 1) - Y)
                        End If
                    End If
                ElseIf (ControlLevel1.ControlType = "R") Then
                    MultiTestIndicator = ""
                End If
                Level1 = Level1 + 1
                ReDim Preserve PadOrderResult(Level1)
                ReDim Preserve Recap(Level1)
                If (Trim(ControlLevel1.DoctorLingo) <> "") Then
                    If (ControlLevel1.DecimalDefault = "T") And (ControlLevel1.ControlType = "N") Then
                        PadOrderResult(Level1) = Mid(PadValue, 2, Len(PadValue) - 1)
                        PadValue = "@##"
                    End If
                    If ControlLevel1.ControlId = "14905" Then
                        Y = InStrPS(w, TheRecord, "<")
                        If (Y <> 0) Then
                        z = InStrPS(w, TheRecord, ">")
                            If (z <> 0) Then
                                Recap(Level1) = "BMI:" & Mid(TheRecord, Y + 1, (z - 1) - Y) + "," + " "
                            End If
                        End If
                    Else
                        Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.DoctorLingo) + " " + PadValue
                    End If
                    Call GetPadValue(Recap(Level1), TheValue)
                    Call SetPadValue(Recap(Level1), TheValue)
                    s = InStrPS(Recap(Level1), "/Time/")
                    If (s > 0) Then
                        Recap(Level1) = Left(Recap(Level1), s) + TestTime + Mid(Recap(Level1), s + 5, Len(Recap(Level1)) - (s + 4))
                    End If
                    s = InStrPS(Recap(Level1), ":^OD")
                    z1 = InStrPS(Recap(Level1), ":/")
                    z2 = InStrPS(Recap(Level1), ":^")
                    If (s <> 0) Then
                        WhereTestNameIs = Level1
                        LocalTestName = Trim(Left(Recap(Level1), s - 1))
                        Call ReplaceCharacters(LocalTestName, "%", " ")
                        LocalTestName = Trim(LocalTestName)
                        ODOn = True
                    ElseIf (z1 <> 0) Then
                        WhereTestNameIs = Level1
                        LocalTestName = Trim(Left(Recap(Level1), z1 - 1))
                        s = InStrPS(Recap(Level1), "^OD")
                        If (s <> 0) Then
                            ODOn = True
                        End If
                    ElseIf (z2 <> 0) Then
                        If (Not (DoRecapCheck)) Then
                            WhereTestNameIs = Level1
                            LocalTestName = Trim(Left(Recap(Level1), z2 - 1))
                            ODOn = True
                        End If
                    Else
                        If (Not DoRecapCheck) Then
                            If (Trim(LocalTestName) = "") And (WhereTestNameIs = 0) Then
                                WhereTestNameIs = 1
                                LocalTestName = Trim(Mid(Question, 2, Len(Question) - 1)) + ":"
                            End If
                        End If
                    End If
                Else
                    Recap(Level1) = "-"
                End If
                If (DoRecapCheck) Then
                    If (Trim(LocalTestName) = "") Then
                        LocalTestName = Mid(Question, 2, Len(Question) - 1)
                        UTemp = LocalTestName
                        Call StripCharacters(UTemp, " ")
                        If (InStrPS(UCase(Recap(Level1)), UCase(LocalTestName)) = 0) And (InStrPS(UCase(Recap(Level1)), UCase(UTemp)) = 0) Then
                            Recap(Level1) = LocalTestName + ":" + Recap(Level1)
                            WhereTestNameIs = Level1
                        End If
                    End If
                    s = InStrPS(UCase(Recap(Level1)), UCase(LocalTestName))
                    If (s <> 0) And (Trim(LocalTestName) <> "") Then
                        s = InStrPS(Recap(Level1), "^OS")
                        If (s <> 0) And (ODOn) Then
                            Recap(Level1) = Mid(Recap(Level1), s, Len(Recap(Level1)) - (s - 1))
                        Else
                            If (WhereTestNameIs > 0) And (WhereTestNameIs <> Level1) Then
                                s = InStrPS(Recap(WhereTestNameIs), ":")
                                If (s <> 0) Then
                                    TestWideName = Trim(Recap(Level1))
                                    k = InStrPS(Recap(Level1), ":")
                                    If (k <> 0) Then
                                        TestWideName = Trim(Mid(Recap(Level1), s + 1, Len(Recap(Level1)) - s))
                                    End If
                                    If (InStrPS(Recap(WhereTestNameIs), TestWideName) = 0) Then
                                        Recap(WhereTestNameIs) = Left(Recap(WhereTestNameIs), s) + TestWideName + Mid(Recap(WhereTestNameIs), s + 1, Len(Recap(WhereTestNameIs)) - s)
                                    End If
                                    Recap(Level1) = "-"
                                End If
                            End If
                        End If
                    End If
                Else
                    s = InStrPS(UCase(Recap(WhereTestNameIs)), UCase(LocalTestName))
                    If (s = 0) Then
                        Recap(Level1) = LocalTestName + Recap(Level1)
                    End If
                End If
            End If
            Set ControlLevel1 = Nothing
        End If
    End If
Loop
FM.CloseFile CLng(FileNum)

' Build Recap Values
FM.OpenFile FileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
For s = 1 To Level1
    If (Trim(Recap(s)) <> "") And (Trim(Recap(s)) <> "-") Then
' Put in any switch for PadOrderResults here
        If (Trim(PadOrderResult(s)) <> "") Then
            z = s
            Call StripCharacters(Recap(s), "#")
            Print #FileNum, "Recap=" + Recap(s)
            s = s + 1
            Print #FileNum, "Recap=" + Recap(s)
            Print #FileNum, "Recap=" + PadOrderResult(z)
        Else
            Print #FileNum, "Recap=" + Recap(s)
        End If
    End If
Next s

FM.CloseFile CLng(FileNum)
Exit Sub
UI_ErrorHandler:
    If (FileNum <> 0) Then
        FM.CloseFile CLng(FileNum)
    End If
    Set ControlLevel1 = Nothing
    Resume LeaveFast
LeaveFast:
End Sub

Public Function RecapTest(TheFile As String) As Boolean
Dim TheRecord As String
Dim FileNum1 As Integer
RecapTest = False
If (FM.IsFileThere(TheFile)) Then
    RecapTest = True
    FileNum1 = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum1)
    Do Until (EOF(FileNum1))
        Line Input #FileNum1, TheRecord
        If (InStrPS(TheRecord, "Recap=") <> 0) Then
            RecapTest = False
        End If
    Loop
    FM.CloseFile CLng(FileNum1)
End If
End Function

Public Function RaceTest(TheFile As String, TheRace As String, TheSex As String) As Boolean
Dim q As Integer
Dim OtherText As String
Dim Temp As String
Dim TheRecord As String
Dim TheQues As String
Dim FileNum1 As Integer
RaceTest = False
TheRace = ""
TheSex = ""
OtherText = ""
If (FM.IsFileThere(TheFile)) Then
    FileNum1 = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum1)
    Do Until (EOF(FileNum1))
        Line Input #FileNum1, TheRecord
        q = InStrPS(TheRecord, "QUESTION=")
        If (q <> 0) Then
            TheQues = Mid(TheRecord, q + 9, Len(TheRecord) - (q + 8))
            If (UCase(Trim(TheQues)) = "/GENERAL MEDICAL OBSERVATION") Then
                RaceTest = True
            End If
        ElseIf (RaceTest) Then
            If (Left(TheRecord, 6) = "Recap=") Then
                Temp = Trim(Mid(TheRecord, 7, Len(TheRecord) - 6))
                If (Temp = "M,") Or (Temp = "F,") Then
                    TheSex = Left(Temp, 1)
                ElseIf (Len(Temp) > 5) Then
                    If (Left(Temp, 5) = "Gen:^") Then
                        TheRace = Mid(Temp, 6, 1)
                    Else
                        Call StripCharacters(Temp, "^")
                        Call StripCharacters(Temp, "~")
                        OtherText = OtherText + Temp
                    End If
                Else
                    Call StripCharacters(Temp, "^")
                    Call StripCharacters(Temp, "~")
                    OtherText = OtherText + Temp
                End If
            End If
        End If
    Loop
    FM.CloseFile CLng(FileNum1)
    frmSystems1.txtGen.Text = OtherText
    txtGen.Text = OtherText
End If
End Function

Private Sub PostRace(TheRace As String, TheSex As String)
Dim RetrievalPatient As Patient
If (TheRace <> "") And (PatientId > 0) Then
    Set RetrievalPatient = New Patient
    RetrievalPatient.PatientId = PatientId
    If (RetrievalPatient.RetrievePatient) Then
        RetrievalPatient.Origin = UCase(Left(TheRace, 1))
        RetrievalPatient.Gender = UCase(Left(TheSex, 1))
        Call RetrievalPatient.ApplyPatient
        txtSum1.Text = RetrievalPatient.Origin + "," + RetrievalPatient.Gender
    End If
    Set RetrievalPatient = Nothing
    Call Timer1_Timer
End If
End Sub

Private Sub Timer1_Timer()
Dim oLocalDateTime As New CIODateTime
    On Error GoTo lTimer1_Timer_Error

    oLocalDateTime.MyDateTime = Now
    txtTime.Locked = False
    txtTime.Text = oLocalDateTime.GetDisplayDate(False) & " " & oLocalDateTime.GetDisplayTime
    txtTime.Locked = True
    Set oLocalDateTime = Nothing

    Exit Sub

lTimer1_Timer_Error:

    LogError "frmTestResults", "Timer1_Timer", Err, Err.Description
End Sub

Private Sub txtTest1_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(txtTest1.Tag, "_")
If (q <> 0) Then
    Temp = Mid(txtTest1.Tag, q + 1, Len(txtTest1.Tag) - q)
    Question = Trim(Left(txtTest1.Tag, q - 1))
    Call DoTest(Question, Temp, True, False)
End If
End Sub

Private Sub txtTest1_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub txtTest2_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(txtTest2.Tag, "_")
If (q <> 0) Then
    Temp = Mid(txtTest2.Tag, q + 1, Len(txtTest2.Tag) - q)
    Question = Trim(Left(txtTest2.Tag, q - 1))
    Call DoTest(Question, Temp, True, False)
End If
End Sub

Private Sub txtTest2_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub txtTest3_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(txtTest3.Tag, "_")
If (q <> 0) Then
    Temp = Mid(txtTest3.Tag, q + 1, Len(txtTest3.Tag) - q)
    Question = Trim(Left(txtTest3.Tag, q - 1))
    Call DoTest(Question, Temp, True, False)
End If
End Sub

Private Sub txtTest3_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub txtTest4_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(txtTest4.Tag, "_")
If (q <> 0) Then
    Temp = Mid(txtTest4.Tag, q + 1, Len(txtTest4.Tag) - q)
    Question = Trim(Left(txtTest4.Tag, q - 1))
    Call DoTest(Question, Temp, True, False)
End If
End Sub

Private Sub txtTest4_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub txtTest5_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(txtTest5.Tag, "_")
If (q <> 0) Then
    Temp = Mid(txtTest5.Tag, q + 1, Len(txtTest5.Tag) - q)
    Question = Trim(Left(txtTest5.Tag, q - 1))
    Call DoTest(Question, Temp, True, False)
End If
End Sub

Private Sub txtTest5_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub txtTest6_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(txtTest6.Tag, "_")
If (q <> 0) Then
    Temp = Mid(txtTest6.Tag, q + 1, Len(txtTest6.Tag) - q)
    Question = Trim(Left(txtTest6.Tag, q - 1))
    Call DoTest(Question, Temp, True, False)
End If
End Sub

Private Sub txtTest6_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub txtTest7_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(txtTest7.Tag, "_")
If (q <> 0) Then
    Temp = Mid(txtTest7.Tag, q + 1, Len(txtTest7.Tag) - q)
    Question = Trim(Left(txtTest7.Tag, q - 1))
    Call DoTest(Question, Temp, True, False)
End If
End Sub

Private Sub txtTest7_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub txtTest8_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(txtTest8.Tag, "_")
If (q <> 0) Then
    Temp = Mid(txtTest8.Tag, q + 1, Len(txtTest8.Tag) - q)
    Question = Trim(Left(txtTest8.Tag, q - 1))
    Call DoTest(Question, Temp, True, False)
End If
End Sub

Private Sub txtTest8_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub txtTest9_Click()
Dim Question As String, Temp As String
Dim q As Integer
q = InStrPS(txtTest9.Tag, "_")
If (q <> 0) Then
    Temp = Mid(txtTest9.Tag, q + 1, Len(txtTest9.Tag) - q)
    Question = Trim(Left(txtTest9.Tag, q - 1))
    Call DoTest(Question, Temp, True, False)
End If
End Sub

Private Sub txtTest9_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub

Private Sub AddNotesToTest(PatId As Long, ApptId As Long, TheOrder As String, TheText As String)
Dim i As Integer
Dim Temp As String
Dim RetNotes As PatientNotes
Temp = "T" + Trim(TheOrder)
TheText = ""
If (PatId > 0) And (ApptId > 0) Then
    Set RetNotes = New PatientNotes
    RetNotes.NotesPatientId = PatientId
    RetNotes.NotesAppointmentId = AppointmentId
    RetNotes.NotesType = "C"
    RetNotes.NotesSystem = Temp
    If (RetNotes.FindNotes > 0) Then
        i = 1
        While (RetNotes.SelectNotes(i))
            If (Trim(RetNotes.NotesText1) <> "") Then
                TheText = TheText + Trim(RetNotes.NotesText1)
            End If
            If (Trim(RetNotes.NotesText2) <> "") Then
                TheText = TheText + Trim(RetNotes.NotesText2)
            End If
            If (Trim(RetNotes.NotesText3) <> "") Then
                TheText = TheText + Trim(RetNotes.NotesText3)
            End If
            If (Trim(RetNotes.NotesText4) <> "") Then
                TheText = TheText + Trim(RetNotes.NotesText4)
            End If
            i = i + 1
        Wend
    End If
    Set RetNotes = Nothing
End If
End Sub

Private Function GetQuestionEditType(TheQues As String, ROn As Boolean) As Boolean
Dim RetCls As DynamicClass
Dim RetFrm As DynamicForms
GetQuestionEditType = False
ROn = False
Set RetFrm = New DynamicForms
RetFrm.Question = TheQues
If (RetFrm.RetrieveForm) Then
    GetQuestionEditType = RetFrm.EditType
    Set RetCls = New DynamicClass
    RetCls.Question = TheQues
    RetCls.QuestionParty = "Y"
    RetCls.QuestionOrder = ""
    If (RetCls.FindClassFormsbyName > 0) Then
        ROn = True
    End If
    Set RetCls = Nothing
End If
Set RetFrm = Nothing
End Function

Private Function IsZoomOn(ATest As TextBox) As Boolean
IsZoomOn = False
If (Len(Trim(ATest.Text)) > 6) Then
    IsZoomOn = True
End If
End Function

Private Sub txtZoom_Click()
txtZoom.Visible = False
txtZoom.Text = ""
cmdMagnify1.Visible = IsZoomOn(txtTest1)
cmdMagnify2.Visible = IsZoomOn(txtTest2)
cmdMagnify3.Visible = IsZoomOn(txtTest3)
cmdMagnify4.Visible = IsZoomOn(txtTest4)
cmdMagnify5.Visible = IsZoomOn(txtTest5)
cmdMagnify6.Visible = IsZoomOn(txtTest6)
cmdMagnify7.Visible = IsZoomOn(txtTest7)
cmdMagnify8.Visible = IsZoomOn(txtTest8)
cmdMagnify9.Visible = IsZoomOn(txtTest9)
End Sub

Private Function GetTestName(ADeviceRef As String, AFormId As Long, AQuestion As String, AOrder As String) As Boolean
Dim RetCtl As DynamicControls
Dim RetFrm As DynamicForms
Dim RetQue As DynamicClass
GetTestName = False
AFormId = 0
AQuestion = ""
AOrder = ""
If (Trim(ADeviceRef) <> "") Then
    Set RetCtl = New DynamicControls
    RetCtl.ICDAlias4 = ADeviceRef
    If (RetCtl.FindControlbyReference > 0) Then
        If (RetCtl.SelectControl(1)) Then
            AFormId = RetCtl.FormId
            If (AFormId > 0) Then
                Set RetFrm = New DynamicForms
                RetFrm.FormId = AFormId
                If (RetFrm.RetrieveForm) Then
                    If (RetFrm.SelectForm(1)) Then
                        AQuestion = Trim(RetFrm.Question)
                        Set RetQue = Nothing
                        Set RetQue = New DynamicClass
                        RetQue.QuestionSet = "First Visit"
                        RetQue.Question = AQuestion
                        RetQue.QuestionParty = "T"
                        If (RetQue.FindQuestionForms > 0) Then
                            If (RetQue.SelectClassForm(1)) Then
                                AOrder = RetQue.QuestionOrder
                            End If
                        End If
                        Set RetQue = Nothing
                    End If
                End If
                Set RetFrm = Nothing
            End If
        End If
        Set RetCtl = Nothing
        GetTestName = True
    End If
End If
End Function

Private Function SetDeviceData(ARef As String, AData As String, AFrmId As Long, AFileName As String) As Boolean
Dim ARec As String
Dim RetCtl As DynamicControls
Dim iFreeFile As Long
SetDeviceData = False
If (Trim(ARef) <> "") And (AFrmId > 0) And (Trim(AFileName) <> "") Then
    Set RetCtl = New DynamicControls
    RetCtl.ICDAlias4 = ARef
    If (RetCtl.FindControlbyReference > 0) Then
        If (RetCtl.SelectControl(1)) Then
            Call ReplaceCharacters(AData, "+ ", "+")
            Call ReplaceCharacters(AData, "- ", "-")
            iFreeFile = FreeFile
            FM.OpenFile AFileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(iFreeFile)
            ARec = "*" + Trim(RetCtl.ControlName) + "=T" + Trim(str(RetCtl.ControlId)) + " <" + AData + ">"
            Print #iFreeFile, ARec
            FM.CloseFile CLng(iFreeFile)
            SetDeviceData = True
        End If
    End If
    Set RetCtl = Nothing
End If
End Function

Private Function SetDeviceEye(ARef As String, AEye As String, AFrmId As Long, AFileName As String) As Boolean
Dim PostEye As Boolean
Dim p As Integer
Dim ARec As String
Dim RetCtl As DynamicControls
Dim iFreeFile As Long
SetDeviceEye = False
If (Trim(ARef) <> "") And (AFrmId > 0) And (Trim(AFileName) <> "") Then
    PostEye = False
    Set RetCtl = New DynamicControls
    RetCtl.FormId = AFrmId
    If (RetCtl.FindControl > 0) Then
        p = 1
        Do Until Not (RetCtl.SelectControl(p))
            If (RetCtl.ControlType = "B") Then
                If (InStrPS(RetCtl.ControlName, AEye) > 0) Then
                    iFreeFile = FreeFile
                    FM.OpenFile AFileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(iFreeFile)
                    ARec = "*" + Trim(RetCtl.ControlName) + "=T" + Trim(str(RetCtl.ControlId)) + " <*" + Trim(RetCtl.ControlName) + ">"
                    Print #iFreeFile, ARec
                    FM.CloseFile CLng(iFreeFile)
                    PostEye = True
                    SetDeviceEye = True
                    Exit Do
                End If
            End If
            p = p + 1
        Loop
        If Not (PostEye) Then
            iFreeFile = FreeFile
            FM.OpenFile AFileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(iFreeFile)
            ARec = "*=T0 <*>"
            Print #iFreeFile, ARec
            FM.CloseFile CLng(iFreeFile)
            SetDeviceEye = True
        End If
    End If
    Set RetCtl = Nothing
End If
End Function

Private Function SetDeviceRButton(ARef As String, AFrmId As Long, TstId As Integer, AFileName As String) As Boolean
Dim p As Integer
Dim m As Integer
Dim ARec As String
Dim iFreeFile As Long
Dim RetCtl As DynamicControls
SetDeviceRButton = False
If (Trim(ARef) <> "") And (AFrmId > 0) And (Trim(AFileName) <> "") Then
    m = 0
    Set RetCtl = New DynamicControls
    RetCtl.FormId = AFrmId
    If (RetCtl.FindControl > 0) Then
        p = 1
        Do Until Not (RetCtl.SelectControl(p))
            If (RetCtl.ControlType = "R") Then
                m = m + 1
                If (m = TstId) Then
                    iFreeFile = FreeFile
                    FM.OpenFile AFileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(iFreeFile)
                    ARec = "*" + Trim(RetCtl.ControlName) + "=T" + Trim(str(RetCtl.ControlId)) + " <*" + Trim(RetCtl.ControlName) + ">"
                    Print #iFreeFile, ARec
                    FM.CloseFile CLng(iFreeFile)
                    SetDeviceRButton = True
                    Exit Do
                End If
            End If
            p = p + 1
        Loop
    End If
    Set RetCtl = Nothing
End If
End Function
Public Function FrmClose()
 Call cmdHome_Click
End Function

