VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form frmMarcoInterface 
   BackColor       =   &H00A95911&
   Caption         =   "Test Capture"
   ClientHeight    =   6735
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7020
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   6735
   ScaleWidth      =   7020
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstType 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "MarcoInterface.frx":0000
      Left            =   2640
      List            =   "MarcoInterface.frx":000A
      TabIndex        =   11
      Top             =   2640
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.ListBox lstPort 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "MarcoInterface.frx":002B
      Left            =   120
      List            =   "MarcoInterface.frx":0035
      TabIndex        =   10
      Top             =   1560
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.ListBox lstStop 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "MarcoInterface.frx":0045
      Left            =   5280
      List            =   "MarcoInterface.frx":0052
      TabIndex        =   9
      Top             =   1560
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.ListBox lstData 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "MarcoInterface.frx":006B
      Left            =   4080
      List            =   "MarcoInterface.frx":0078
      TabIndex        =   8
      Top             =   1560
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstParity 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "MarcoInterface.frx":0090
      Left            =   2640
      List            =   "MarcoInterface.frx":009D
      TabIndex        =   7
      Top             =   1560
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.ListBox lstBaud 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      ItemData        =   "MarcoInterface.frx":00B2
      Left            =   1200
      List            =   "MarcoInterface.frx":00CB
      TabIndex        =   6
      Top             =   1560
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   120
      Top             =   600
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdGetLab 
      Height          =   855
      Left            =   1800
      TabIndex        =   3
      Top             =   600
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "MarcoInterface.frx":00FA
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   600
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   0   'False
      RThreshold      =   128
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   855
      Left            =   5160
      TabIndex        =   2
      Top             =   600
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "MarcoInterface.frx":02E4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   855
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "MarcoInterface.frx":04C3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdChange 
      Height          =   855
      Left            =   3480
      TabIndex        =   5
      Top             =   600
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "MarcoInterface.frx":06AD
   End
   Begin VB.TextBox txtScript 
      Height          =   5055
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   4
      Top             =   1560
      Width           =   6735
   End
   Begin VB.Label lblStatus 
      BackColor       =   &H00999900&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Initializing..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6735
   End
End
Attribute VB_Name = "frmMarcoInterface"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public EquipmentType As String
Private Const gstrNULL$ = ""         'Empty string
Private Const gintMAX_SIZE% = 255    'Maximum buffer size

'----------------------------------------------------------------------------------------------------------------------
' Handshaking variables
    ' miCurrentState: 0-Free, 10-Receiving data, 20-Requesting to send data, 21-Sending data,
    '   22-Requesting to stop sending data
Private miCurrentState As Integer
    ' Buffer to store messages that are being received
Private mstrReadBuffer As String
    ' Buffer to store messages that need to be sent
Private mstrWriteBuffer As String
    ' Buffer to store message descriptions that need to be sent
Private mstrWriteDescBuffer As String
    ' Current message trying to be sent
Private mstrWriteMessage As String
    ' Current message description trying to be sent
Private mstrWriteMessageDesc As String

'----------------------------------------------------------------------------------------------------------------------
' Setup parameters for comm port - comes from ini file
    ' Comm port to use
Private mstrCommPort As String
    ' Comm settings to use (ex: 9600,n,8,1)
Private mstrCommSetting As String
    ' Equipment type that we are talking to.  Used when requesting or clearing data
Private mstrEquipmentType As String
Private RcvOnly As Boolean
'----------------------------------------------------------------------------------------------------------------------
' Constants for parsing/building messages
Private strSOH As String
Private strSTX As String
Private strETB As String
Private strEOT As String
Private strCR As String
Private strLF As String

'----------------------------------------------------------------------------------------------------------------------
' SUB: InitializeVars
'
' Sets the constants used to parse/build messages
'
' IN:   None
'
' OUT:  The following global variables are set:
'       strSOH
'       strSTX
'       strETB
'       strEOT
'       strCR
'       strLF
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub InitializeVars()
strSOH = Chr(1)
strSTX = Chr(2)
strETB = Chr(23)
strEOT = Chr(4)
strCR = Chr(13)
strLF = Chr(10)
Call SetCurrentState(0)
mstrReadBuffer = ""
mstrWriteBuffer = ""
mstrWriteDescBuffer = ""
mstrWriteMessage = ""
mstrWriteMessageDesc = ""
End Sub

Private Sub ParseINIFile(DefaultOn As Boolean)
RcvOnly = False
mstrEquipmentType = "RK"
If (DefaultOn) Then
    mstrCommPort = "1"
    mstrCommSetting = "9600"                        ' Baud Rate
    mstrCommSetting = mstrCommSetting & "," & "o"   ' Parity
    mstrCommSetting = mstrCommSetting & "," & "8"   ' DataBits
    mstrCommSetting = mstrCommSetting & "," & "1"   ' StopBits
    If (EquipmentType = "ARK-2000") Then
        mstrEquipmentType = "RK"
        RcvOnly = False
    ElseIf (EquipmentType = "VL-3000") Then
        mstrEquipmentType = "RS"
        RcvOnly = True
    End If
Else
    If (lstPort.ListIndex > -1) Then
        mstrCommPort = Mid(lstPort.List(lstPort.ListIndex), 4, 1)
    Else
        mstrCommPort = "1"
    End If
    If (lstBaud.ListIndex > -1) Then
        mstrCommSetting = lstBaud.List(lstBaud.ListIndex)
    Else
        mstrCommSetting = "9600"
    End If
    If (lstParity.ListIndex > -1) Then
        mstrCommSetting = mstrCommSetting & "," & Left(lstParity.List(lstParity.ListIndex), 1)
    Else
        mstrCommSetting = mstrCommSetting & "," & "o"
    End If
    If (lstData.ListIndex > -1) Then
        If (lstData.ListIndex = 0) Then
            mstrCommSetting = mstrCommSetting & ","
        Else
            mstrCommSetting = mstrCommSetting & "," & Left(lstData.List(lstData.ListIndex), 1)
        End If
    Else
        mstrCommSetting = mstrCommSetting & "," & "8"
    End If
    If (lstStop.ListIndex > -1) Then
        If (lstStop.ListIndex = 0) Then
            mstrCommSetting = mstrCommSetting & ","
        Else
            mstrCommSetting = mstrCommSetting & "," & Left(lstStop.List(lstStop.ListIndex), 1)
        End If
    Else
        mstrCommSetting = mstrCommSetting & "," & "1"
    End If
    If (lstType.ListIndex > -1) Then
        mstrEquipmentType = Left(lstType.List(lstType.ListIndex), 2)
        If (mstrEquipmentType = "RK") Then
            RcvOnly = False
        ElseIf (mstrEquipmentType = "RS") Then
            RcvOnly = True
        End If
    Else
        If (EquipmentType = "ARK-2000") Then
            mstrEquipmentType = "RK"
            RcvOnly = False
        ElseIf (EquipmentType = "VL-3000") Then
            mstrEquipmentType = "RS"
            RcvOnly = True
        End If
    End If
End If
If (RcvOnly) Then
    cmdClear.Enabled = False
    cmdGetLab.Enabled = False
End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  SetupCommPort
'
' Prepares the comm port for communications
'
' IN:   Uses the following global variables:
'       mstrCommPort - Comm Port to use for communications
'       mstrCommSetting - Comm port settings (baud rate, etc.)
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Function SetupCommPort() As Boolean
On Error Resume Next
SetupCommPort = False
MSComm1.CommPort = mstrCommPort
MSComm1.Settings = mstrCommSetting
'MSComm1.DTREnable = False
'MSComm1.InputMode = comInputModeText
MSComm1.InputLen = 1
MSComm1.RThreshold = 1
MSComm1.PortOpen = True
If (Err <> 0) And (Err <> 8005) Then
    frmEventMsgs.Header = "The communication ports could not be initialized."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
SetupCommPort = True
End Function

'----------------------------------------------------------------------------------------------------------------------
' SUB: ReadData
'
' Reads all commands from the comm port, performs logic as necessary
'
' IN:   Uses the following global variables:
'       mstrReadBuffer - Stores incoming commands here, and then pulls the completed commands off of this
'
' OUT:  Uses the following global variables:
'       mstrReadBuffer - If a command was incomplete, it is left in this variable
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub ReadData()
Dim strChar As String
Dim strMessage As String
' Read data from buffer
If IsMissing(mstrReadBuffer) Then
    mstrReadBuffer = ""
End If
While (MSComm1.InBufferCount > 0)
    strChar = MSComm1.Input
    If (strChar = strCR) Then
    
    ElseIf (strChar = strLF) Then
    
    ElseIf (strChar = strEOT) Then
        strMessage = mstrReadBuffer
        mstrReadBuffer = ""
        If Len(strMessage) > 0 Then
            Call HandleMessageFromEquip(strMessage)
        End If
    Else
        mstrReadBuffer = mstrReadBuffer & strChar
    End If
Wend
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  HandleMessageFromEquip
'
' Performs logic for a single message from the equipment
'
' IN:   strMessage
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub HandleMessageFromEquip(strMessage As String)
Dim strMessageType As String
' Skip leading char(1) - quit if none
If Left(strMessage, 1) = strSOH Then
    strMessage = Right(strMessage, Len(strMessage) - 1)
' Parse out command type - quit if none
    strMessageType = ParseData(strMessage, strSTX)
    If strMessageType = "C**" Then
        Call HandleCommandFromEquip(strMessage)
    ElseIf Left(strMessageType, 1) = "D" Then
        strMessageType = Right(strMessageType, Len(strMessageType) - 1)
        Call HandleDataFromEquip(strMessageType, strMessage)
    End If
End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  HandleCommandFromEquip
'
' Performs logic for a single command from the equipment
'
' IN:   strCommand
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub HandleCommandFromEquip(strCommand As String)
Dim strCommandType As String
strCommandType = ParseData(strCommand, strETB)
If strCommandType = "RS" Then
    Call RequestDataFromEquip
End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: HandleDataFromEquip
'
' Equipment has sent data.  Parse it into data parts
'
' IN:   strEquipType - Type of equipment sending data (RM, KM, LM)
'       strData - Data sent.
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub HandleDataFromEquip(strEquipType As String, strData As String)
Dim strDataPart As String
While Len(strData) > 0
    ' If it starts with a strSOH, then it is a new command
    If Left(strData, 1) = strSOH Then
        Call HandleMessageFromEquip(strData)
        strData = ""
    Else
        strDataPart = ParseData(strData, strETB)
        Call HandleDataPartFromEquip(strEquipType, strDataPart)
    End If
Wend
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: HandleDataPartFromEquip
'
' Equipment has sent a data part.  Parse and store it
'   Not coded - need to determine how to store data
'
' IN:   strEquipType - Type of equipment sending data (RM, KM, LM)
'       strDataPart - Data part sent.
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub HandleDataPartFromEquip(strEquipType As String, strDataPart As String)
txtScript.Text = txtScript.Text & "From " & strEquipType & ": " & strDataPart & strCR & strLF
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  SendData
'
' Place message in send buffer.  Request to send it if possible.
'
' IN:   strMessage - Message to send (note that leading SOH and trailing EOT should
'           not be included)
'       Uses the following global variables:
'           mstrWriteBuffer - Buffer for sending data
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
Private Sub SendData(strMessage As String, strMessageDesc As String)
mstrWriteBuffer = mstrWriteBuffer & strMessage & strEOT
mstrWriteDescBuffer = mstrWriteDescBuffer & strMessageDesc & strEOT
Call RequestSend
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  RequestSend
'
' If we have data to send, and the line is available, request that the equipment accept data now
'
' IN:   None
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
Private Sub RequestSend()
If Len(mstrWriteMessage) = 0 And Len(mstrWriteBuffer) > 0 Then
    mstrWriteMessage = ParseData(mstrWriteBuffer, strEOT)
    mstrWriteMessageDesc = ParseData(mstrWriteDescBuffer, strEOT)
End If
If Len(mstrWriteMessage) > 0 And miCurrentState = 0 Then
    Call SetCurrentState(20)
    Timer1.Enabled = True
    MSComm1.DTREnable = True
End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB:  SendData
'
' We now have the line for sending data - send one message
'
' IN:   Uses the following global variables
'           mstrWriteData
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
Private Sub SendDataNow()
If Len(mstrWriteMessage) > 0 Then
    MSComm1.Output = strSOH & mstrWriteMessage & strEOT
End If
txtScript = txtScript & "To: " & mstrWriteMessage & strCR & strLF
mstrWriteMessage = ""
End Sub

'----------------------------------------------------------------------------------------------------------------------
' FUNCTION: ParseData
'
' Pulls the first chunk off of a longer string
'
' IN:   strCommandIn - String to parse
'       strDelimiter - Delimiter use
'       blnRequireDelimiter - if true, and the delimiter is not found, then the function returns
'           an empty string.  If false, and the delimiter is not found, then the function returns
'           the entire string.
'
' OUT:  strCommandIn - The first section is removed
'       Function returns the first section
'----------------------------------------------------------------------------------------------------------------------
'
Private Function ParseData(strData As String, strDelimiter As String, Optional blnRequireDelimiter As Boolean) As String
Dim lJunk As Long
lJunk = InStrPS(strData, strDelimiter)
If (lJunk >= 1) Then
    ParseData = Left(strData, lJunk - 1)
    strData = Right(strData, Len(strData) - lJunk)
ElseIf (blnRequireDelimiter) Then
    ParseData = ""
Else
    ParseData = strData
    strData = ""
End If
End Function

'----------------------------------------------------------------------------------------------------------------------
' SUB: RequestDataFromEquip
'
' We need to retrieve data from the equipment - send command to equipment to send it
'
' IN:   None
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub RequestDataFromEquip()
Dim strMessage As String
If Not (RcvOnly) Then
    strMessage = "C" & mstrEquipmentType & strSTX & "SD" & strETB
    Call SendData(strMessage, "Data request")
End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: ClearDataFromEquip
'
' Clear equipment memory
'
' IN:   None
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub ClearDataFromEquip()
Dim strMessage As String
If Not (RcvOnly) Then
    strMessage = "C" & mstrEquipmentType & strSTX & "CL" & strETB
    Call SendData(strMessage, "Clear")
End If
End Sub

'----------------------------------------------------------------------------------------------------------------------
' SUB: SetCurrentState
'
' Set the current handshaking state
'
' IN:   iNextState
'
' OUT:  None
'----------------------------------------------------------------------------------------------------------------------
'
Private Sub SetCurrentState(iNextState As Integer)
miCurrentState = iNextState
Select Case miCurrentState
    Case 0
        lblStatus = "Waiting..."
    Case 10
        lblStatus = "Receiving..."
    Case 20
        lblStatus = "Starting to send " & mstrWriteMessageDesc & "..."
    Case 21
        lblStatus = "Sending " & mstrWriteMessageDesc & "..."
    Case 22
        lblStatus = "Finished sending " & mstrWriteMessageDesc & "..."
    Case 23
        lblStatus = "Finished receiving... "
End Select
End Sub

Private Sub cmdChange_Click()
If (cmdChange.Text = "Device Settings") Then
    txtScript.Visible = False
    lstPort.Visible = True
    lstBaud.Visible = True
    lstParity.Visible = True
    lstData.Visible = True
    lstStop.Visible = True
    lstType.Visible = True
    cmdChange.Text = "Close"
Else
    frmEventMsgs.Header = "Apply Settings ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        Call ParseINIFile(False)
        Call SetupCommPort
        Call InitializeVars
    End If
    txtScript.Visible = True
    lstPort.Visible = False
    lstBaud.Visible = False
    lstParity.Visible = False
    lstData.Visible = False
    lstStop.Visible = False
    lstType.Visible = False
    cmdChange.Text = "Device Settings"
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
On Error Resume Next
Me.MSComm1.PortOpen = False
End Sub

Private Sub MSComm1_OnComm()
Select Case MSComm1.CommEvent
    Case comEventBreak      ' A Break was received.
    Case comEventFrame      ' Framing Error
    Case comEventOverrun    ' Data Lost.
    Case comEventRxOver     ' Receive buffer overflow.
    Case comEventRxParity   ' Parity Error.
    Case comEventTxFull     ' Transmit buffer full.
    Case comEventDCB        ' Unexpected error retrieving DCB]
    Case comEvCD            ' Change in the CD line.
    Case comEvCTS           ' Change in the CTS line.
    Case comEvDSR           ' Change in the DSR line.
        Call DSRStateChanged
    Case comEvRing          ' Change in the Ring Indicator.
    Case comEvReceive       ' Received RThreshold # of chars.
        Call ReadData       ' We have received enough characters that we should transfer the buffer and look for data
    Case comEvSend          ' There are SThreshold number of characters in the transmit buffer.
    Case comEvEOF           ' An EOF charater was found in the input stream
End Select
End Sub

Private Sub DSRStateChanged()
Select Case miCurrentState
    Case 0
        If MSComm1.DSRHolding Then           ' Equipment is trying to send data
            MSComm1.DTREnable = True
            mstrReadBuffer = ""
            Call SetCurrentState(10)
        End If
    Case 10
        If Not MSComm1.DSRHolding Then      ' Equipment is done sending data
            MSComm1.DTREnable = False
            Call ReadData
            If (RcvOnly) Then
                Call SetCurrentState(23)
            Else
                Call SetCurrentState(20)
            End If
        End If
    Case 20                                 ' We just requested to send data
        If MSComm1.DSRHolding Then          ' Equipment said go ahead
            Timer1.Enabled = False
            Call SetCurrentState(21)
            Call SendDataNow
            Call SetCurrentState(22)        'Tell equipment we are done
            MSComm1.DTREnable = False
        End If
    Case 22                                 ' We have just finished sending data
        If Not MSComm1.DSRHolding Then      ' Equipment acknowledged - put back to free state
            Call SetCurrentState(0)
        End If
    Case 24                                 ' We have just finished receiving data
        If Not MSComm1.DSRHolding Then      ' Equipment acknowledged - put back to free state
            Call SetCurrentState(0)
        End If
    End Select
    If Not (RcvOnly) Then
        Call RequestSend
    End If
End Sub

Private Sub Timer1_Timer()
Timer1.Enabled = False
If (miCurrentState = 20) Then
    MSComm1.DTREnable = False
    Call SetCurrentState(0)
    mstrWriteBuffer = ""
    mstrWriteDescBuffer = ""
    mstrWriteMessage = ""
    mstrWriteMessageDesc = ""
    frmEventMsgs.Header = "Communications with the lab equipment could not be established."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdGetLab_Click()
Call RequestDataFromEquip
End Sub

Private Sub cmdClear_Click()
Call ClearDataFromEquip
End Sub

Private Sub cmdDone_Click()
Unload frmMarcoInterface
End Sub

Public Function LoadMachine() As Boolean
LoadMachine = False
Call ParseINIFile(True)
If (SetupCommPort) Then
    Call InitializeVars
    LoadMachine = True
End If
End Function

Private Sub txtScript_DblClick()
frmEventMsgs.Header = "Write Out Contents ?"
frmEventMsgs.AcceptText = "Write"
frmEventMsgs.RejectText = "Append"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    FM.OpenFile SchedulerInterfaceDirectory + "DevicePost.txt", FileOpenMode.Output, FileAccess.ReadWrite, CLng(101)
    Print #101, Trim(txtScript.Text)
    FM.CloseFile CLng(101)
    txtScript.SetFocus
    SendKeys "{End}"
ElseIf (frmEventMsgs.Result = 2) Then
    FM.OpenFile SchedulerInterfaceDirectory + "DevicePost.txt", FileOpenMode.Append, FileAccess.ReadWrite, CLng(101)
    Print #101, Trim(txtScript.Text)
    FM.CloseFile CLng(101)
    txtScript.SetFocus
    SendKeys "{End}"
End If
End Sub
