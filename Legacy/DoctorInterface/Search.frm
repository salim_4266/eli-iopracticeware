VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmSearch 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12360
   ForeColor       =   &H00A39B6D&
   LinkTopic       =   "Form1"
   Picture         =   "Search.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12360
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10200
      TabIndex        =   0
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx10 
      Height          =   1095
      Left            =   4080
      TabIndex        =   1
      Top             =   5280
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":38FC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx9 
      Height          =   1095
      Left            =   4080
      TabIndex        =   2
      Top             =   4080
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":3ADB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx8 
      Height          =   1095
      Left            =   4080
      TabIndex        =   3
      Top             =   2880
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":3CB9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx7 
      Height          =   1095
      Left            =   4080
      TabIndex        =   4
      Top             =   1680
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":3E97
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx6 
      Height          =   1095
      Left            =   4080
      TabIndex        =   5
      Top             =   480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":4075
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx11 
      Height          =   1095
      Left            =   8040
      TabIndex        =   6
      Top             =   480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":4253
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx12 
      Height          =   1095
      Left            =   8040
      TabIndex        =   7
      Top             =   1680
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":4432
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx13 
      Height          =   1095
      Left            =   8040
      TabIndex        =   8
      Top             =   2880
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":4611
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx14 
      Height          =   1095
      Left            =   8040
      TabIndex        =   9
      Top             =   4080
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":47F0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx15 
      Height          =   1095
      Left            =   8040
      TabIndex        =   10
      Top             =   5280
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":49CF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx2 
      Height          =   1095
      Left            =   120
      TabIndex        =   11
      Top             =   1680
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":4BAE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx3 
      Height          =   1095
      Left            =   120
      TabIndex        =   12
      Top             =   2880
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":4D8C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx4 
      Height          =   1095
      Left            =   120
      TabIndex        =   13
      Top             =   4080
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":4F6A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx5 
      Height          =   1095
      Left            =   120
      TabIndex        =   14
      Top             =   5280
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":5148
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   1095
      Left            =   120
      TabIndex        =   15
      Top             =   6480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":5326
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreRx 
      Height          =   1095
      Left            =   8040
      TabIndex        =   16
      Top             =   6480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":5509
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx1 
      Height          =   1095
      Left            =   120
      TabIndex        =   17
      Top             =   480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":56F1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect 
      Height          =   1095
      Left            =   4080
      TabIndex        =   19
      Top             =   6480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Search.frx":58CF
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Assign An Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   360
      TabIndex        =   18
      Top             =   120
      Width           =   1860
   End
End
Attribute VB_Name = "frmSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DiagOn As Boolean
Public FilterBillable As Boolean
Public FilterSpecial As Boolean
Public Selection As String

Private TotalFound As Integer
Private TheName As String
Private DisplayType As String
Private DisplayStart As String
Private OtherICDAccessType As String
Private CurrentIndex As Integer

Private Sub cmdDone_Click()
If (Trim(TheName) <> "") Then
    Selection = Trim(TheName)
End If
Unload frmSearch
End Sub

Private Sub cmdRx1_Click()
Call TriggerRx(cmdRx1)
End Sub

Private Sub cmdRx2_Click()
Call TriggerRx(cmdRx2)
End Sub

Private Sub cmdRx3_Click()
Call TriggerRx(cmdRx3)
End Sub

Private Sub cmdRx4_Click()
Call TriggerRx(cmdRx4)
End Sub

Private Sub cmdRx5_Click()
Call TriggerRx(cmdRx5)
End Sub

Private Sub cmdRx6_Click()
Call TriggerRx(cmdRx6)
End Sub

Private Sub cmdRx7_Click()
Call TriggerRx(cmdRx7)
End Sub

Private Sub cmdRx8_Click()
Call TriggerRx(cmdRx8)
End Sub

Private Sub cmdRx9_Click()
Call TriggerRx(cmdRx9)
End Sub

Private Sub cmdRx10_Click()
Call TriggerRx(cmdRx10)
End Sub

Private Sub cmdRx11_Click()
Call TriggerRx(cmdRx11)
End Sub

Private Sub cmdRx12_Click()
Call TriggerRx(cmdRx12)
End Sub

Private Sub cmdRx13_Click()
Call TriggerRx(cmdRx13)
End Sub

Private Sub cmdRx14_Click()
Call TriggerRx(cmdRx14)
End Sub

Private Sub cmdRx15_Click()
Call TriggerRx(cmdRx15)
End Sub

Private Sub cmdMoreRx_Click()
CurrentIndex = CurrentIndex + 1
If (CurrentIndex > TotalFound) Then
    CurrentIndex = 1
End If
If (DiagOn) Then
    Call LoadOtherDiags
Else
    Call LoadOtherProcedure
End If
End Sub

Private Sub cmdPrev_Click()
CurrentIndex = CurrentIndex - 30
If (CurrentIndex < 1) Then
    CurrentIndex = 1
End If
If (DiagOn) Then
    Call LoadOtherDiags
Else
    Call LoadOtherProcedure
End If
End Sub

Private Sub cmdSelect_Click()
frmEventMsgs.Header = "Display Ordering"
frmEventMsgs.AcceptText = "Alpha"
frmEventMsgs.RejectText = "Numeric"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
If (DiagOn) Then
    frmEventMsgs.Other0Text = "Type"
End If
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    DisplayType = "A"
    Call GetStartingValue(DisplayStart, DisplayType)
    If (Trim(DisplayStart) = "") Then
        Exit Sub
    End If
ElseIf (frmEventMsgs.Result = 2) Then
    DisplayType = "N"
    Call GetStartingValue(DisplayStart, DisplayType)
    If (Trim(DisplayStart) = "") Then
        Exit Sub
    End If
ElseIf (frmEventMsgs.Result = 3) Then
    frmEventMsgs.Header = "Display ICD by Type"
    frmEventMsgs.AcceptText = "All"
    frmEventMsgs.RejectText = "OPHT"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        OtherICDAccessType = ""
    ElseIf (frmEventMsgs.Result = 2) Then
        OtherICDAccessType = "OPHT"
    Else
        Exit Sub
    End If
    frmEventMsgs.Header = "Display Ordering"
    frmEventMsgs.AcceptText = "Alpha"
    frmEventMsgs.RejectText = "Numeric"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        DisplayType = "A"
        Call GetStartingValue(DisplayStart, DisplayType)
        If (DisplayStart = "") Then
            Exit Sub
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        DisplayType = "N"
        Call GetStartingValue(DisplayStart, DisplayType)
        If (DisplayStart = "") Then
            Exit Sub
        End If
    Else
        Exit Sub
    End If
Else
    Exit Sub
End If
CurrentIndex = 1
If (DiagOn) Then
    Call LoadOtherDiags
Else
    Call LoadOtherProcedure
End If
End Sub

Private Sub TriggerRx(AButton As fpBtn)
If (FilterBillable) Then
    If (AButton.ToolTipText = "X") Then
        frmEventMsgs.Header = "Diagnosis is NOT Billable."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
End If
TheName = Space(75)
Mid(TheName, 1, Len(Trim(AButton.Text))) = Trim(AButton.Text)
TheName = TheName + Trim(AButton.Tag)
Call cmdDone_Click
End Sub

Private Sub ClearSearch()
cmdRx1.Visible = False
cmdRx2.Visible = False
cmdRx3.Visible = False
cmdRx4.Visible = False
cmdRx5.Visible = False
cmdRx6.Visible = False
cmdRx7.Visible = False
cmdRx8.Visible = False
cmdRx9.Visible = False
cmdRx10.Visible = False
cmdRx11.Visible = False
cmdRx12.Visible = False
cmdRx13.Visible = False
cmdRx14.Visible = False
cmdRx15.Visible = False
End Sub

Public Function LoadAvailableSearch(TheText As String, TheType As String) As Boolean
LoadAvailableSearch = False
Selection = ""
TheName = ""
TotalFound = 0
CurrentIndex = 1
OtherICDAccessType = ""
DisplayStart = TheText
DisplayType = TheType
If (DiagOn) Then
    Label1.Caption = "Search for a Diagnosis"
    LoadAvailableSearch = LoadOtherDiags
Else
    Label1.Caption = "Search for a Procedure"
    LoadAvailableSearch = LoadOtherProcedure
End If
Exit Function
UI_ErrorHandler:
    LoadAvailableSearch = False
    Call PinpointError("frmSearch", Error(Err))
    Resume LeaveFast
LeaveFast:
End Function

Private Function LoadOtherDiags() As Boolean
Dim q As Integer
Dim p As Integer
Dim MyCnt As Integer
Dim Cnt As Integer
Dim PostIt As Boolean
Dim ButtonCnt As Integer
Dim LimDiag As String
Dim ATag As String
Dim ANons As String
Dim ABill As String
Dim ABilling As String
Dim ASpec As String
Dim sSystem As String
Dim eesES As EEyeSystem
Dim DisplayText As String
Dim RetrieveDiag As New DiagnosisMasterPrimary
Cnt = 0
LoadOtherDiags = False
Call ClearSearch
LimDiag = DisplayStart
If (Trim(DisplayType) = "") Then
    DisplayType = "A"
End If
If (Trim(DisplayStart) = "") Then
    DisplayStart = Chr(1)
End If
MyCnt = 0
ButtonCnt = 0
p = CurrentIndex
RetrieveDiag.PrimarySystem = ""
RetrieveDiag.PrimaryName = ""
RetrieveDiag.PrimaryDiagnosis = ""
RetrieveDiag.PrimaryNextLevelDiagnosis = ""
RetrieveDiag.PrimaryLingo = ""
RetrieveDiag.PrimaryRank = 0
RetrieveDiag.PrimaryLevel = 0
RetrieveDiag.PrimaryDiscipline = OtherICDAccessType
RetrieveDiag.PrimaryBilling = False
If (DisplayType = "A") Then
    RetrieveDiag.PrimaryLingo = DisplayStart
    TotalFound = RetrieveDiag.FindRealPrimarybyDiagnosisFiltered
Else
    RetrieveDiag.PrimaryNextLevelDiagnosis = DisplayStart
    TotalFound = RetrieveDiag.FindRealPrimarybyDiagnosisNumericFiltered
End If
If (TotalFound > 0) Then
    ButtonCnt = 1
    Do Until Not (RetrieveDiag.SelectPrimary(p)) Or (ButtonCnt > 15)
        ABill = " (NB)"
        If (RetrieveDiag.PrimaryBilling) Then
            ABill = ""
        End If
        If (FilterSpecial) Then
            If (Left(LimDiag, 1) <> "*") Then
                If (DisplayType <> "A") Then
                    PostIt = IsDiagnosisOk(Trim(RetrieveDiag.PrimaryNextLevelDiagnosis), "", LimDiag)
                Else
                    PostIt = IsDiagnosisOk(Trim(RetrieveDiag.PrimaryNextLevelDiagnosis), Trim(RetrieveDiag.PrimaryLingo), LimDiag)
                End If
            Else
                PostIt = True
            End If
        Else
            PostIt = True
        End If
        If (PostIt) Then
            DisplayText = RetrieveDiag.PrimaryNextLevelDiagnosis + Space(10 - Len(RetrieveDiag.PrimaryNextLevelDiagnosis))
            If (Left(cmdRx1.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx2.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx3.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx4.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx5.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx6.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx7.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx8.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx9.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx10.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx11.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx12.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx13.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx14.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx15.Text, 10) = DisplayText) Then
                PostIt = False
            End If
            If (PostIt) Then
                MyCnt = MyCnt + 1
                If (Trim(RetrieveDiag.PrimaryLingo) <> "") And (DisplayType <> "A") Then
                    DisplayText = DisplayText + Left(Trim(RetrieveDiag.PrimaryLingo), 60)
                Else
                    If (Len(Trim(RetrieveDiag.PrimaryLingo)) > 60) Then
                        DisplayText = DisplayText + Left(Trim(RetrieveDiag.PrimaryLingo), 60)
                    Else
                        DisplayText = DisplayText + Trim(RetrieveDiag.PrimaryLingo)
                    End If
                End If
                ABilling = ""
                If Not (RetrieveDiag.PrimaryBilling) Then
                    ABilling = " (NB)"
                End If
                ASpec = ""
                If (RetrieveDiag.PrimaryUnspecifiedOn) Then
                    ASpec = " (NS)"
                End If
                eesES = GetEyeSystem(RetrieveDiag.PrimarySystem)
                DisplayText = DisplayText + ABill + ASpec & "   (" & GetSystemName(eesES) & ")"
                ATag = Trim(str(RetrieveDiag.PrimaryId))
                If (ButtonCnt = 1) Then
                    cmdRx1.Text = DisplayText
                    cmdRx1.Tag = ATag
                    cmdRx1.ToolTipText = ABill
                    cmdRx1.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 2) Then
                    cmdRx2.Text = DisplayText
                    cmdRx2.Tag = ATag
                    cmdRx2.ToolTipText = ABill
                    cmdRx2.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 3) Then
                    cmdRx3.Text = DisplayText
                    cmdRx3.Tag = ATag
                    cmdRx3.ToolTipText = ABill
                    cmdRx3.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 4) Then
                    cmdRx4.Text = DisplayText
                    cmdRx4.Tag = ATag
                    cmdRx4.ToolTipText = ABill
                    cmdRx4.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 5) Then
                    cmdRx5.Text = DisplayText
                    cmdRx5.Tag = ATag
                    cmdRx5.ToolTipText = ABill
                    cmdRx5.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 6) Then
                    cmdRx6.Text = DisplayText
                    cmdRx6.Tag = ATag
                    cmdRx6.ToolTipText = ABill
                    cmdRx6.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 7) Then
                    cmdRx7.Text = DisplayText
                    cmdRx7.Tag = ATag
                    cmdRx7.ToolTipText = ABill
                    cmdRx7.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 8) Then
                    cmdRx8.Text = DisplayText
                    cmdRx8.Tag = ATag
                    cmdRx8.ToolTipText = ABill
                    cmdRx8.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 9) Then
                    cmdRx9.Text = DisplayText
                    cmdRx9.Tag = ATag
                    cmdRx9.ToolTipText = ABill
                    cmdRx9.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 10) Then
                    cmdRx10.Text = DisplayText
                    cmdRx10.Tag = ATag
                    cmdRx10.ToolTipText = ABill
                    cmdRx10.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 11) Then
                    cmdRx11.Text = DisplayText
                    cmdRx11.Tag = ATag
                    cmdRx11.ToolTipText = ABill
                    cmdRx11.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 12) Then
                    cmdRx12.Text = DisplayText
                    cmdRx12.Tag = ATag
                    cmdRx12.ToolTipText = ABill
                    cmdRx12.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 13) Then
                    cmdRx13.Text = DisplayText
                    cmdRx13.Tag = ATag
                    cmdRx13.ToolTipText = ABill
                    cmdRx13.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 14) Then
                    cmdRx14.Text = DisplayText
                    cmdRx14.Tag = ATag
                    cmdRx14.ToolTipText = ABill
                    cmdRx14.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 15) Then
                    cmdRx15.Text = DisplayText
                    cmdRx15.Tag = ATag
                    cmdRx15.ToolTipText = ABill
                    cmdRx15.Visible = True
                    Cnt = 1
                End If
                ButtonCnt = ButtonCnt + 1
            End If
        End If
        p = p + 1
    Loop
End If
CurrentIndex = p
If (Cnt > 0) Then
    LoadOtherDiags = True
End If
If (TotalFound > 15) And ((MyCnt = 15) Or (CurrentIndex < 16)) Then
    cmdMoreRx.Visible = True
    cmdMoreRx.Text = "Next Dx"
    cmdPrev.Visible = True
    cmdPrev.Text = "Previous Dx"
Else
    cmdMoreRx.Visible = False
    cmdPrev.Visible = False
End If
cmdSelect.Visible = True
Set RetrieveDiag = Nothing
End Function

Private Function IsDiagnosisOk(TheDiag As String, TheName As String, LimitDiag As String) As Boolean
IsDiagnosisOk = True
If (Trim(LimitDiag) <> "") Then
    If (UCase(LimitDiag) < UCase(Left(TheName, Len(LimitDiag)))) Then
        IsDiagnosisOk = False
        Exit Function
    End If
End If
If (Left(TheDiag, 1) = "Y") Or (Left(TheDiag, 1) = "W") Or (Left(TheDiag, 1) = "L") Or _
   (Left(TheDiag, 1) = "M") Or (Left(TheDiag, 1) = "X") Or (Left(TheDiag, 1) = "P") Then
    IsDiagnosisOk = False
End If
End Function

Private Function GetStartingValue(TheText As String, TheType As String) As Boolean
TheText = ""
GetStartingValue = True
If (TheType = "N") Then
    If (DiagOn) Then
        frmNumericPad.NumPad_Field = "Starting with Diagnosis Number"
    Else
        frmNumericPad.NumPad_Field = "Starting with Procedure Number"
    End If
    frmNumericPad.NumPad_Result = ""
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_ChoiceOn1 = False
    frmNumericPad.NumPad_Choice1 = ""
    frmNumericPad.NumPad_ChoiceOn2 = False
    frmNumericPad.NumPad_Choice2 = ""
    frmNumericPad.NumPad_ChoiceOn3 = False
    frmNumericPad.NumPad_Choice3 = ""
    frmNumericPad.NumPad_ChoiceOn4 = False
    frmNumericPad.NumPad_Choice4 = ""
    frmNumericPad.NumPad_ChoiceOn5 = False
    frmNumericPad.NumPad_Choice5 = ""
    frmNumericPad.NumPad_ChoiceOn6 = False
    frmNumericPad.NumPad_Choice6 = ""
    frmNumericPad.NumPad_ChoiceOn7 = False
    frmNumericPad.NumPad_Choice7 = ""
    frmNumericPad.NumPad_ChoiceOn8 = False
    frmNumericPad.NumPad_Choice8 = ""
    frmNumericPad.NumPad_ChoiceOn9 = False
    frmNumericPad.NumPad_Choice9 = ""
    frmNumericPad.NumPad_ChoiceOn10 = False
    frmNumericPad.NumPad_Choice10 = ""
    frmNumericPad.NumPad_ChoiceOn11 = False
    frmNumericPad.NumPad_Choice11 = ""
    frmNumericPad.NumPad_ChoiceOn12 = False
    frmNumericPad.NumPad_Choice12 = ""
    frmNumericPad.NumPad_ChoiceOn13 = False
    frmNumericPad.NumPad_Choice13 = ""
    frmNumericPad.NumPad_ChoiceOn14 = False
    frmNumericPad.NumPad_Choice14 = ""
    frmNumericPad.NumPad_ChoiceOn15 = False
    frmNumericPad.NumPad_Choice15 = ""
    frmNumericPad.NumPad_ChoiceOn16 = False
    frmNumericPad.NumPad_Choice16 = ""
    frmNumericPad.NumPad_ChoiceOn17 = False
    frmNumericPad.NumPad_Choice17 = ""
    frmNumericPad.NumPad_ChoiceOn18 = False
    frmNumericPad.NumPad_Choice18 = ""
    frmNumericPad.NumPad_ChoiceOn19 = False
    frmNumericPad.NumPad_Choice19 = ""
    frmNumericPad.NumPad_ChoiceOn20 = False
    frmNumericPad.NumPad_Choice20 = ""
    frmNumericPad.NumPad_ChoiceOn21 = False
    frmNumericPad.NumPad_Choice21 = ""
    frmNumericPad.NumPad_ChoiceOn22 = False
    frmNumericPad.NumPad_Choice22 = ""
    frmNumericPad.NumPad_ChoiceOn23 = False
    frmNumericPad.NumPad_Choice23 = ""
    frmNumericPad.NumPad_ChoiceOn24 = False
    frmNumericPad.NumPad_Choice24 = ""
    frmNumericPad.Show 1
    If Not (frmAlphaPad.NumPad_Quit) Then
        TheText = Trim(frmNumericPad.NumPad_Result)
    Else
        TheText = ""
    End If
ElseIf (TheType = "A") Then
    If (DiagOn) Then
        frmAlphaPad.NumPad_Field = "Starting with Diagnosis Alpha"
    Else
        frmAlphaPad.NumPad_Field = "Starting with Procedure Alpha"
    End If
    frmAlphaPad.NumPad_Result = ""
    frmAlphaPad.NumPad_DisplayFull = True
    frmAlphaPad.Show 1
    If Not (frmAlphaPad.NumPad_Quit) Then
        TheText = Trim(frmAlphaPad.NumPad_Result)
    Else
        TheText = ""
    End If
End If
End Function

Private Function LoadOtherProcedure() As Boolean
Dim p As Integer
Dim ButtonCnt As Integer
Dim AId As Long
Dim ATag As String
Dim ADiag As String
Dim ADesc As String
Dim DisplayText As String
Dim RetrieveProcedure As DiagnosisMasterProcedures
Set RetrieveProcedure = New DiagnosisMasterProcedures
LoadOtherProcedure = False
Call ClearSearch
If (Trim(DisplayType) = "") Then
    DisplayType = "N"
End If
If (Trim(DisplayStart) = "") Then
    DisplayStart = Chr(1)
End If
p = CurrentIndex
RetrieveProcedure.ProcedureCPT = DisplayStart
RetrieveProcedure.ProcedureDiagnosis = ""
RetrieveProcedure.ProcedureRank = 0
RetrieveProcedure.ProcedureLinkType = ""
If (DisplayType = "N") Then
    TotalFound = RetrieveProcedure.FindProcedurebyNumericDistinct
Else
    TotalFound = RetrieveProcedure.FindProcedurebyAlphaDistinct
End If
If (TotalFound > 0) Then
    ButtonCnt = 1
    While (RetrieveProcedure.SelectProcedureDistinct(p)) And (ButtonCnt < 16)
        If (GetProcedure(RetrieveProcedure.ProcedureCPT, ADiag, ADesc, AId)) Then
            DisplayText = Trim(RetrieveProcedure.ProcedureCPT) + Space(10 - Len(10 - Len(Trim(RetrieveProcedure.ProcedureCPT))))
            DisplayText = DisplayText + ADesc
            ATag = Trim(str(AId))
            If (ButtonCnt = 1) Then
                cmdRx1.Text = DisplayText
                cmdRx1.Tag = ATag
                cmdRx1.ToolTipText = AId
                cmdRx1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdRx2.Text = DisplayText
                cmdRx2.Tag = ATag
                cmdRx2.ToolTipText = AId
                cmdRx2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdRx3.Text = DisplayText
                cmdRx3.Tag = ATag
                cmdRx3.ToolTipText = AId
                cmdRx3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdRx4.Text = DisplayText
                cmdRx4.Tag = ATag
                cmdRx4.ToolTipText = AId
                cmdRx4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdRx5.Text = DisplayText
                cmdRx5.Tag = ATag
                cmdRx5.ToolTipText = AId
                cmdRx5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdRx6.Text = DisplayText
                cmdRx6.Tag = ATag
                cmdRx6.ToolTipText = AId
                cmdRx6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdRx7.Text = DisplayText
                cmdRx7.Tag = ATag
                cmdRx7.ToolTipText = AId
                cmdRx7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdRx8.Text = DisplayText
                cmdRx8.Tag = ATag
                cmdRx8.ToolTipText = AId
                cmdRx8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdRx9.Text = DisplayText
                cmdRx9.Tag = ATag
                cmdRx9.ToolTipText = AId
                cmdRx9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdRx10.Text = DisplayText
                cmdRx10.Tag = ATag
                cmdRx10.ToolTipText = AId
                cmdRx10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdRx11.Text = DisplayText
                cmdRx11.Tag = ATag
                cmdRx11.ToolTipText = AId
                cmdRx11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdRx12.Text = DisplayText
                cmdRx12.Tag = ATag
                cmdRx12.ToolTipText = AId
                cmdRx12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdRx13.Text = DisplayText
                cmdRx13.Tag = ATag
                cmdRx13.ToolTipText = AId
                cmdRx13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdRx14.Text = DisplayText
                cmdRx14.Tag = ATag
                cmdRx14.ToolTipText = AId
                cmdRx14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdRx15.Text = DisplayText
                cmdRx15.Tag = ATag
                cmdRx15.ToolTipText = AId
                cmdRx15.Visible = True
            End If
            LoadOtherProcedure = True
            ButtonCnt = ButtonCnt + 1
        End If
        p = p + 1
    Wend
End If
CurrentIndex = p
If (TotalFound > 15) Then
    cmdMoreRx.Visible = True
    cmdMoreRx.Text = "Next"
    cmdPrev.Visible = True
    cmdPrev.Text = "Previous"
Else
    cmdMoreRx.Visible = False
    cmdPrev.Visible = False
End If
cmdSelect.Visible = True
Set RetrieveProcedure = Nothing
End Function

Private Function GetProcedure(ACPT As String, ADiag As String, ADesc As String, AId As Long) As Boolean
Dim RetProc As DiagnosisMasterProcedures
GetProcedure = False
ADiag = ""
ADesc = ""
AId = 0
If (Trim(ACPT) <> "") Then
    Set RetProc = New DiagnosisMasterProcedures
    RetProc.ProcedureCPT = Trim(ACPT)
    RetProc.ProcedureRank = -2
    RetProc.ProcedureDiagnosis = ""
    RetProc.ProcedureLinkType = ""
    If (RetProc.FindProcedure > 0) Then
        If (RetProc.SelectProcedure(1)) Then
            ADiag = Trim(RetProc.ProcedureDiagnosis)
            ADesc = Trim(RetProc.ProcedureName)
            AId = RetProc.ProcedureId
            GetProcedure = True
        End If
    End If
    Set RetProc = Nothing
End If
End Function
Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
End Function

