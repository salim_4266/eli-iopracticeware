VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmSelectQuestion 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "SelectQuestion.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdOtherDx 
      Height          =   975
      Left            =   2160
      TabIndex        =   10
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImmunizations 
      Height          =   975
      Left            =   5880
      TabIndex        =   40
      Top             =   7920
      Visible         =   0   'False
      Width           =   2415
      _Version        =   131072
      _ExtentX        =   4260
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":38AD
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000001&
      Height          =   975
      Left            =   480
      TabIndex        =   37
      Top             =   7920
      Width           =   7575
      Begin fpBtnAtlLibCtl.fpBtn cmdAllergy 
         Height          =   975
         Left            =   5280
         TabIndex        =   38
         Top             =   0
         Width           =   2175
         _Version        =   131072
         _ExtentX        =   3836
         _ExtentY        =   1720
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "SelectQuestion.frx":3A95
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx16 
      Height          =   1095
      Left            =   9000
      TabIndex        =   32
      Top             =   600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":3C84
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx17 
      Height          =   1095
      Left            =   9000
      TabIndex        =   33
      Top             =   1800
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":3E63
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx18 
      Height          =   1095
      Left            =   9000
      TabIndex        =   34
      Top             =   3000
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":4042
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx19 
      Height          =   1095
      Left            =   9000
      TabIndex        =   35
      Top             =   4200
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":4221
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx20 
      Height          =   1095
      Left            =   9000
      TabIndex        =   36
      Top             =   5400
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":4400
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10080
      TabIndex        =   7
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":45DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   975
      Left            =   8400
      TabIndex        =   9
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":47BE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNewSearch 
      Height          =   1095
      Left            =   6120
      TabIndex        =   14
      Top             =   6600
      Visible         =   0   'False
      Width           =   5535
      _Version        =   131072
      _ExtentX        =   9763
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":499E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   1095
      Left            =   3240
      TabIndex        =   15
      Top             =   6600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":4B83
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   1095
      Left            =   480
      TabIndex        =   16
      Top             =   6600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":4D66
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx10 
      Height          =   1095
      Left            =   3240
      TabIndex        =   17
      Top             =   5400
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":4F49
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx9 
      Height          =   1095
      Left            =   3240
      TabIndex        =   18
      Top             =   4200
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":5128
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx8 
      Height          =   1095
      Left            =   3240
      TabIndex        =   19
      Top             =   3000
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":5306
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx7 
      Height          =   1095
      Left            =   3240
      TabIndex        =   20
      Top             =   1800
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":54E4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx6 
      Height          =   1095
      Left            =   3240
      TabIndex        =   21
      Top             =   600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":56C2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx11 
      Height          =   1095
      Left            =   6120
      TabIndex        =   22
      Top             =   600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":58A0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx12 
      Height          =   1095
      Left            =   6120
      TabIndex        =   23
      Top             =   1800
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":5A7F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx13 
      Height          =   1095
      Left            =   6120
      TabIndex        =   24
      Top             =   3000
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":5C5E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx14 
      Height          =   1095
      Left            =   6120
      TabIndex        =   25
      Top             =   4200
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":5E3D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx15 
      Height          =   1095
      Left            =   6120
      TabIndex        =   26
      Top             =   5400
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":601C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx1 
      Height          =   1095
      Left            =   480
      TabIndex        =   27
      Top             =   600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":61FB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx2 
      Height          =   1095
      Left            =   480
      TabIndex        =   28
      Top             =   1800
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":63D9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx3 
      Height          =   1095
      Left            =   480
      TabIndex        =   29
      Top             =   3000
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":65B7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx4 
      Height          =   1095
      Left            =   480
      TabIndex        =   30
      Top             =   4200
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":6795
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx5 
      Height          =   1095
      Left            =   480
      TabIndex        =   31
      Top             =   5400
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":6973
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt5 
      Height          =   1575
      Left            =   5880
      TabIndex        =   12
      Top             =   840
      Visible         =   0   'False
      Width           =   5775
      _Version        =   131072
      _ExtentX        =   10186
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":6B51
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt7 
      Height          =   1575
      Left            =   5880
      TabIndex        =   6
      Top             =   4200
      Visible         =   0   'False
      Width           =   5775
      _Version        =   131072
      _ExtentX        =   10186
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":6D31
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt6 
      Height          =   1575
      Left            =   5880
      TabIndex        =   5
      Top             =   2520
      Visible         =   0   'False
      Width           =   5775
      _Version        =   131072
      _ExtentX        =   10186
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":6F11
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt8 
      Height          =   1575
      Left            =   5880
      TabIndex        =   4
      Top             =   5880
      Visible         =   0   'False
      Width           =   5775
      _Version        =   131072
      _ExtentX        =   10186
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":70F1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt3 
      Height          =   1575
      Left            =   480
      TabIndex        =   2
      Top             =   4200
      Visible         =   0   'False
      Width           =   5175
      _Version        =   131072
      _ExtentX        =   9128
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":72D1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt2 
      Height          =   1575
      Left            =   480
      TabIndex        =   1
      Top             =   2520
      Visible         =   0   'False
      Width           =   5175
      _Version        =   131072
      _ExtentX        =   9128
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":74B1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt1 
      Height          =   1575
      Left            =   480
      TabIndex        =   0
      Top             =   840
      Visible         =   0   'False
      Width           =   5175
      _Version        =   131072
      _ExtentX        =   9128
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":7691
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt4 
      Height          =   1575
      Left            =   480
      TabIndex        =   3
      Top             =   5880
      Visible         =   0   'False
      Width           =   5175
      _Version        =   131072
      _ExtentX        =   9128
      _ExtentY        =   2778
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12688201
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SelectQuestion.frx":7871
   End
   Begin VB.Label lblPat 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   240
      TabIndex        =   39
      Top             =   240
      Width           =   1515
   End
   Begin VB.Label lblSys 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Select Diagnosis"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   6120
      TabIndex        =   11
      Top             =   240
      Visible         =   0   'False
      Width           =   1785
   End
   Begin VB.Label lblResult 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Select Question"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   9480
      TabIndex        =   8
      Top             =   240
      Width           =   1710
   End
   Begin VB.Label lblDrug 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Select Drug"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   6120
      TabIndex        =   13
      Top             =   240
      Visible         =   0   'False
      Width           =   1260
   End
End
Attribute VB_Name = "frmSelectQuestion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public QuestionFamily As Integer
Public QuestionSet As String
Public Question As String
Public QuestionOrder As String
Public SystemReference As String
Public AppointmentId As Long
Public PatientId As Long
Public NoteOn As Boolean

Private Diagnosis As String
Private DiagnosisId As Long
Private DisplayStart As String
Private DisplayType As String
Private TotalOtherDiags As Integer
Private TotalCCs As Integer
Private CurrentOtherIndex As Integer
Private CurrentOtherIndexPrev As Integer
Private CurrentCCIndex As Integer

Private ComplaintDoDate As Boolean
Private DiagOn As Boolean
Private DiagDetails As String
Private CurrentText As String
Private CurrentOrder As String

Private ComplaintOn As Boolean
Private ComplaintType As String
Private Complaint As String

Private Sub cmdAllergy_Click()
'===============================================================================================
Dim TotalQuestions As Boolean
Dim ClinicalClass As New DynamicClass
Dim TheFile As String
Dim OrgAction  As Boolean

ClinicalClass.QuestionParty = "P"
ClinicalClass.QuestionSet = "No Visit"
ClinicalClass.Question = "ALLERGIES ALL"
ClinicalClass.QuestionOrder = "66C"
TotalQuestions = ClinicalClass.RetrieveClassbyQuestion
If Not (TotalQuestions) Then
    ClinicalClass.QuestionParty = "X"
    TotalQuestions = ClinicalClass.RetrieveClassbyQuestion
End If
If (TotalQuestions) Then
    TheFile = DoctorInterfaceDirectory + ClinicalClass.QuestionParty + ClinicalClass.QuestionOrder + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
    OrgAction = FM.IsFileThere(TheFile)
    If Not (OrgAction) Then
        If Not frmSystems1.EditPreviousOn Then
            Call frmSystems1.PGetPatientResponses(ClinicalClass.Question, TheFile)
        End If
    End If
End If
'================================================================================================
frmAllergy.Question = ClinicalClass.Question
frmAllergy.TheFile = TheFile
frmAllergy.lblPat.Caption = lblPat.Caption
Set ClinicalClass = Nothing
If frmAllergy.LoadAllergy Then
    frmAllergy.Show 1
End If
End Sub

Private Sub cmdAppt1_Click()
CurrentText = cmdAppt1.Text
CurrentOrder = cmdAppt1.Tag
If (QuestionFamily <> 10) Then
    Call cmdDone_Click
Else
    CurrentCCIndex = 1
    Call LoadChiefComplaints(CurrentText)
End If
End Sub

Private Sub cmdAppt2_Click()
CurrentText = cmdAppt2.Text
CurrentOrder = cmdAppt2.Tag
If (QuestionFamily <> 10) Then
    Call cmdDone_Click
Else
    CurrentCCIndex = 1
    Call LoadChiefComplaints(CurrentText)
End If
End Sub

Private Sub cmdAppt3_Click()
CurrentText = cmdAppt3.Text
CurrentOrder = cmdAppt3.Tag
If (QuestionFamily <> 10) Then
    Call cmdDone_Click
Else
    CurrentCCIndex = 1
    Call LoadChiefComplaints(CurrentText)
End If
End Sub

Private Sub cmdAppt4_Click()
CurrentText = cmdAppt4.Text
CurrentOrder = cmdAppt4.Tag
If (QuestionFamily <> 10) Then
    Call cmdDone_Click
Else
    CurrentCCIndex = 1
    Call LoadChiefComplaints(CurrentText)
End If
End Sub

Private Sub cmdAppt5_Click()
CurrentText = cmdAppt5.Text
CurrentOrder = cmdAppt5.Tag
If (QuestionFamily <> 10) Then
    Call cmdDone_Click
Else
    CurrentCCIndex = 1
    Call LoadChiefComplaints(CurrentText)
End If
End Sub

Private Sub cmdAppt6_Click()
CurrentText = cmdAppt6.Text
CurrentOrder = cmdAppt6.Tag
If (QuestionFamily <> 10) Then
    Call cmdDone_Click
Else
    CurrentCCIndex = 1
    Call LoadChiefComplaints(CurrentText)
End If
End Sub

Private Sub cmdAppt7_Click()
CurrentText = cmdAppt7.Text
CurrentOrder = cmdAppt7.Tag
If (QuestionFamily <> 10) Then
    Call cmdDone_Click
Else
    CurrentCCIndex = 1
    Call LoadChiefComplaints(CurrentText)
End If
End Sub

Private Sub cmdAppt8_Click()
CurrentText = cmdAppt8.Text
CurrentOrder = cmdAppt8.Tag
If (QuestionFamily <> 10) Then
    Call cmdDone_Click
Else
    CurrentCCIndex = 1
    Call LoadChiefComplaints(CurrentText)
End If
End Sub

Private Sub cmdDone_Click()
If (lblSys.Visible) Then
    Call DoDiagnosis
    Call ClearRxs
Else
    If (Trim(CurrentText) <> "") Then
        If (QuestionFamily <> 10) Then
            Question = CurrentText
            QuestionOrder = CurrentOrder
        Else
            Question = CurrentText
            QuestionOrder = "97A"
        End If
    End If
    Unload frmSelectQuestion
End If
End Sub

Public Function LoadQuestionSelection() As Boolean
Dim j As Integer
Dim i As Integer
Dim RetrieveQuestion As DynamicClass
LoadQuestionSelection = False
DiagOn = False
ComplaintOn = False
Question = ""
CurrentText = ""
CurrentOrder = ""
CurrentOtherIndex = 1
CurrentOtherIndexPrev = 1
NoteOn = False
Set RetrieveQuestion = New DynamicClass
RetrieveQuestion.QuestionFamily = QuestionFamily
RetrieveQuestion.QuestionSet = QuestionSet
If (RetrieveQuestion.FindQuestionbyFamily > 0) Then
    i = 1
    LoadQuestionSelection = True
    While (RetrieveQuestion.SelectClassForm(i))
        If (i = 1) Then
            cmdAppt1.Text = RetrieveQuestion.Question
            cmdAppt1.Tag = RetrieveQuestion.QuestionOrder
            cmdAppt1.Visible = True
        ElseIf (i = 2) Then
            cmdAppt2.Text = RetrieveQuestion.Question
            cmdAppt2.Tag = RetrieveQuestion.QuestionOrder
            cmdAppt2.Visible = True
        ElseIf (i = 3) Then
            cmdAppt3.Text = RetrieveQuestion.Question
            cmdAppt3.Tag = RetrieveQuestion.QuestionOrder
            cmdAppt3.Visible = True
        ElseIf (i = 4) Then
            cmdAppt4.Text = RetrieveQuestion.Question
            cmdAppt4.Tag = RetrieveQuestion.QuestionOrder
            cmdAppt4.Visible = True
        ElseIf (i = 5) Then
            cmdAppt5.Text = RetrieveQuestion.Question
            cmdAppt5.Tag = RetrieveQuestion.QuestionOrder
            cmdAppt5.Visible = True
        ElseIf (i = 6) Then
            cmdAppt6.Text = RetrieveQuestion.Question
            cmdAppt6.Tag = RetrieveQuestion.QuestionOrder
            cmdAppt6.Visible = True
        ElseIf (i = 7) Then
            cmdAppt7.Text = RetrieveQuestion.Question
            cmdAppt7.Tag = RetrieveQuestion.QuestionOrder
            cmdAppt7.Visible = True
        ElseIf (i = 8) Then
            cmdAppt8.Text = RetrieveQuestion.Question
            cmdAppt8.Tag = RetrieveQuestion.QuestionOrder
            cmdAppt8.Visible = True
        End If
        i = i + 1
    Wend
End If
If (QuestionFamily = 3) Or (QuestionFamily = 4) Then
    If (QuestionFamily = 3) Then
        RetrieveQuestion.QuestionFamily = 2
    End If
    If (QuestionFamily = 4) Then
        RetrieveQuestion.QuestionFamily = 7
    End If
    RetrieveQuestion.QuestionSet = QuestionSet
    If (RetrieveQuestion.FindQuestionbyFamily > 0) Then
        j = 1
        LoadQuestionSelection = True
        While (RetrieveQuestion.SelectClassForm(j))
            If (i = 1) Then
                cmdAppt1.Text = RetrieveQuestion.Question
                cmdAppt1.Tag = RetrieveQuestion.QuestionOrder
                cmdAppt1.Visible = True
            ElseIf (i = 2) Then
                cmdAppt2.Text = RetrieveQuestion.Question
                cmdAppt2.Tag = RetrieveQuestion.QuestionOrder
                cmdAppt2.Visible = True
            ElseIf (i = 3) Then
                cmdAppt3.Text = RetrieveQuestion.Question
                cmdAppt3.Tag = RetrieveQuestion.QuestionOrder
                cmdAppt3.Visible = True
            ElseIf (i = 4) Then
                cmdAppt4.Text = RetrieveQuestion.Question
                cmdAppt4.Tag = RetrieveQuestion.QuestionOrder
                cmdAppt4.Visible = True
            ElseIf (i = 5) Then
                cmdAppt5.Text = RetrieveQuestion.Question
                cmdAppt5.Tag = RetrieveQuestion.QuestionOrder
                cmdAppt5.Visible = True
            ElseIf (i = 6) Then
                cmdAppt6.Text = RetrieveQuestion.Question
                cmdAppt6.Tag = RetrieveQuestion.QuestionOrder
                cmdAppt6.Visible = True
            ElseIf (i = 7) Then
                cmdAppt7.Text = RetrieveQuestion.Question
                cmdAppt7.Tag = RetrieveQuestion.QuestionOrder
                cmdAppt7.Visible = True
            ElseIf (i = 8) Then
                cmdAppt8.Text = RetrieveQuestion.Question
                cmdAppt8.Tag = RetrieveQuestion.QuestionOrder
                cmdAppt8.Visible = True
            End If
            i = i + 1
            j = j + 1
        Wend
    End If
End If
Set RetrieveQuestion = Nothing
cmdOtherDx.Visible = False
If (QuestionFamily <> 9) And (QuestionFamily <> 10) Then
    cmdOtherDx.Visible = True
End If
If QuestionFamily = 9 Then
    cmdAllergy.Visible = True
Else
    cmdAllergy.Visible = False
End If
lblResult.Caption = "Select Question"
If (QuestionFamily = 10) Then
    lblResult.Caption = "Select CC"
    If CheckConfigCollection("RVON") = "T" Then
        lblResult.Caption = "Select RV"
    End If
End If
End Function

Private Function LoadOtherDiags() As Boolean
'REFACTOR: LoadOtherDiags here and frmSearch are probably equal.
Dim p As Integer
Dim Cnt As Integer
Dim PostIt As Boolean
Dim ButtonCnt As Integer
Dim LimDiag As String
Dim ATag As String
Dim DisplayText As String
Dim RetrieveDiag As New DiagnosisMasterPrimary
Cnt = 0
LoadOtherDiags = False
Call ClearRxs
lblSys.Visible = True
LimDiag = DisplayStart
If (Trim(DisplayType) = "") Then
    DisplayType = "A"
End If
If (Trim(DisplayStart) = "") Then
    DisplayStart = Chr(1)
End If
ButtonCnt = 0
CurrentOtherIndexPrev = CurrentOtherIndex
p = CurrentOtherIndex
RetrieveDiag.PrimarySystem = ""
RetrieveDiag.PrimaryName = ""
RetrieveDiag.PrimaryDiagnosis = ""
RetrieveDiag.PrimaryNextLevelDiagnosis = ""
RetrieveDiag.PrimaryRank = 0
RetrieveDiag.PrimaryLevel = 0
RetrieveDiag.PrimaryDiscipline = ""
RetrieveDiag.PrimaryBilling = False
If (DisplayType = "A") Then
    RetrieveDiag.PrimaryLingo = "*" & DisplayStart
    TotalOtherDiags = RetrieveDiag.FindRealPrimarybyDiagnosis
Else
    RetrieveDiag.PrimaryNextLevelDiagnosis = DisplayStart
    TotalOtherDiags = RetrieveDiag.FindRealPrimarybyDiagnosisNumeric
End If
If (TotalOtherDiags > 0) Then
    ButtonCnt = 1
    Do Until Not (RetrieveDiag.SelectPrimary(p)) Or (ButtonCnt > 20)
        If (Left(DisplayStart, 1) <> "*") Then
            If (DisplayType <> "A") Then
                PostIt = IsDiagnosisOk(Trim(RetrieveDiag.PrimaryNextLevelDiagnosis), LimDiag, True)
            Else
                PostIt = IsDiagnosisOk(Trim(RetrieveDiag.PrimaryLingo), LimDiag, False)
            End If
        Else
            PostIt = True
        End If
        If (PostIt) Then
            DisplayText = RetrieveDiag.PrimaryNextLevelDiagnosis + Space(10 - Len(RetrieveDiag.PrimaryNextLevelDiagnosis))
            If (Left(cmdRx1.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx2.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx3.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx4.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx5.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx6.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx7.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx8.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx9.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx10.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx11.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx12.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx13.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx14.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx15.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx16.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx17.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx18.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx19.Text, 10) = DisplayText) Then
                PostIt = False
            ElseIf (Left(cmdRx20.Text, 10) = DisplayText) Then
                PostIt = False
            End If
            If (PostIt) Then
                If (Trim(RetrieveDiag.PrimaryLingo) <> "") And (DisplayType <> "A") Then
                    DisplayText = DisplayText + Left(Trim(RetrieveDiag.PrimaryLingo), 50)
                Else
                    If (Len(Trim(RetrieveDiag.PrimaryName)) > 50) Then
                        DisplayText = DisplayText + Left(Trim(RetrieveDiag.PrimaryName), 50)
                    Else
                        DisplayText = DisplayText + Trim(RetrieveDiag.PrimaryName)
                    End If
                End If
                ATag = Trim(str(RetrieveDiag.PrimaryId))
                If (ButtonCnt = 1) Then
                    cmdRx1.Text = DisplayText
                    cmdRx1.Tag = ATag
                    cmdRx1.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 2) Then
                    cmdRx2.Text = DisplayText
                    cmdRx2.Tag = ATag
                    cmdRx2.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 3) Then
                    cmdRx3.Text = DisplayText
                    cmdRx3.Tag = ATag
                    cmdRx3.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 4) Then
                    cmdRx4.Text = DisplayText
                    cmdRx4.Tag = ATag
                    cmdRx4.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 5) Then
                    cmdRx5.Text = DisplayText
                    cmdRx5.Tag = ATag
                    cmdRx5.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 6) Then
                    cmdRx6.Text = DisplayText
                    cmdRx6.Tag = ATag
                    cmdRx6.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 7) Then
                    cmdRx7.Text = DisplayText
                    cmdRx7.Tag = ATag
                    cmdRx7.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 8) Then
                    cmdRx8.Text = DisplayText
                    cmdRx8.Tag = ATag
                    cmdRx8.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 9) Then
                    cmdRx9.Text = DisplayText
                    cmdRx9.Tag = ATag
                    cmdRx9.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 10) Then
                    cmdRx10.Text = DisplayText
                    cmdRx10.Tag = ATag
                    cmdRx10.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 11) Then
                    cmdRx11.Text = DisplayText
                    cmdRx11.Tag = ATag
                    cmdRx11.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 12) Then
                    cmdRx12.Text = DisplayText
                    cmdRx12.Tag = ATag
                    cmdRx12.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 13) Then
                    cmdRx13.Text = DisplayText
                    cmdRx13.Tag = ATag
                    cmdRx13.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 14) Then
                    cmdRx14.Text = DisplayText
                    cmdRx14.Tag = ATag
                    cmdRx14.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 15) Then
                    cmdRx15.Text = DisplayText
                    cmdRx15.Tag = ATag
                    cmdRx15.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 16) Then
                    cmdRx16.Text = DisplayText
                    cmdRx16.Tag = ATag
                    cmdRx16.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 17) Then
                    cmdRx17.Text = DisplayText
                    cmdRx17.Tag = ATag
                    cmdRx17.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 18) Then
                    cmdRx18.Text = DisplayText
                    cmdRx18.Tag = ATag
                    cmdRx18.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 19) Then
                    cmdRx19.Text = DisplayText
                    cmdRx19.Tag = ATag
                    cmdRx19.Visible = True
                    Cnt = 1
                ElseIf (ButtonCnt = 20) Then
                    cmdRx20.Text = DisplayText
                    cmdRx20.Tag = ATag
                    cmdRx20.Visible = True
                    Cnt = 1
                End If
            End If
            If (PostIt) Then
                ButtonCnt = ButtonCnt + 1
            End If
        Else
            Exit Do
        End If
        p = p + 1
    Loop
End If
CurrentOtherIndex = p
If (Cnt > 0) Then
    LoadOtherDiags = True
    DiagOn = True
End If
If (TotalOtherDiags > 20) Then
    cmdNext.Visible = True
    cmdNext.Text = "Next Dx"
    cmdPrev.Visible = True
    cmdPrev.Text = "Previous Dx"
Else
    cmdNext.Visible = False
    cmdPrev.Visible = False
End If
cmdNewSearch.Visible = True
Set RetrieveDiag = Nothing
End Function

Private Function LoadChiefComplaints(IType As String) As Boolean
Dim p As Integer
Dim Cnt As Integer
Dim ButtonCnt As Integer
Dim ATag As String
Dim DisplayText As String
Dim RetCode As PracticeCodes
Set RetCode = New PracticeCodes
LoadChiefComplaints = False
Cnt = 0
ComplaintType = IType
ComplaintDoDate = False
Call ClearRxs
ComplaintDoDate = False
If (InStrPS(IType, "PO") <> 0) Then
    ComplaintDoDate = True
End If
ButtonCnt = 0
p = CurrentCCIndex
RetCode.ReferenceType = "CHIEFCOMPLAINTS"
RetCode.ReferenceAlternateCode = IType
TotalCCs = RetCode.FindAlternateCode
If (TotalCCs > 0) Then
    ButtonCnt = 1
    Do Until Not (RetCode.SelectCode(p)) Or (ButtonCnt > 20)
        DisplayText = Trim(RetCode.ReferenceCode)
        ATag = ""
        If (ButtonCnt = 1) Then
            cmdRx1.Text = DisplayText
            cmdRx1.Tag = ATag
            cmdRx1.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 2) Then
            cmdRx2.Text = DisplayText
            cmdRx2.Tag = ATag
            cmdRx2.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 3) Then
            cmdRx3.Text = DisplayText
            cmdRx3.Tag = ATag
            cmdRx3.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 4) Then
            cmdRx4.Text = DisplayText
            cmdRx4.Tag = ATag
            cmdRx4.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 5) Then
            cmdRx5.Text = DisplayText
            cmdRx5.Tag = ATag
            cmdRx5.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 6) Then
            cmdRx6.Text = DisplayText
            cmdRx6.Tag = ATag
            cmdRx6.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 7) Then
            cmdRx7.Text = DisplayText
            cmdRx7.Tag = ATag
            cmdRx7.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 8) Then
            cmdRx8.Text = DisplayText
            cmdRx8.Tag = ATag
            cmdRx8.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 9) Then
            cmdRx9.Text = DisplayText
            cmdRx9.Tag = ATag
            cmdRx9.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 10) Then
            cmdRx10.Text = DisplayText
            cmdRx10.Tag = ATag
            cmdRx10.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 11) Then
            cmdRx11.Text = DisplayText
            cmdRx11.Tag = ATag
            cmdRx11.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 12) Then
            cmdRx12.Text = DisplayText
            cmdRx12.Tag = ATag
            cmdRx12.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 13) Then
            cmdRx13.Text = DisplayText
            cmdRx13.Tag = ATag
            cmdRx13.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 14) Then
            cmdRx14.Text = DisplayText
            cmdRx14.Tag = ATag
            cmdRx14.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 15) Then
            cmdRx15.Text = DisplayText
            cmdRx15.Tag = ATag
            cmdRx15.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 16) Then
            cmdRx16.Text = DisplayText
            cmdRx16.Tag = ATag
            cmdRx16.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 17) Then
            cmdRx17.Text = DisplayText
            cmdRx17.Tag = ATag
            cmdRx17.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 18) Then
            cmdRx18.Text = DisplayText
            cmdRx18.Tag = ATag
            cmdRx18.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 19) Then
            cmdRx19.Text = DisplayText
            cmdRx19.Tag = ATag
            cmdRx19.Visible = True
            Cnt = 1
        ElseIf (ButtonCnt = 20) Then
            cmdRx20.Text = DisplayText
            cmdRx20.Tag = ATag
            cmdRx20.Visible = True
            Cnt = 1
        End If
        ButtonCnt = ButtonCnt + 1
        p = p + 1
    Loop
End If
Set RetCode = Nothing
CurrentCCIndex = p
If (Cnt > 0) Then
    LoadChiefComplaints = True
    ComplaintOn = True
    cmdAppt1.Visible = False
    cmdAppt2.Visible = False
    cmdAppt3.Visible = False
    cmdAppt4.Visible = False
    cmdAppt5.Visible = False
    cmdAppt6.Visible = False
    cmdAppt7.Visible = False
    cmdAppt8.Visible = False
    If (TotalCCs > 20) Then
        cmdNext.Visible = True
        cmdNext.Text = "Next"
        cmdPrev.Visible = True
        cmdPrev.Text = "Prev"
    Else
        cmdNext.Visible = False
        cmdPrev.Visible = False
    End If
End If
End Function

Private Sub cmdImmunizations_Click()
Dim objHelper As New Immunizations.DbHelper
objHelper.DbObject = IdbConnection
Dim objImm As New Immunizations.frmImmunizations
objImm.PatientId = PatientId
objImm.EncounterId = AppointmentId
objImm.UserId = UserLogin.iId
objImm.ShowDialog
End Sub

Private Sub cmdNewSearch_Click()
    Call cmdOtherDx_Click
End Sub

Private Sub cmdNotes_Click()
frmNotes.NoteId = 0
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.SystemReference = Trim(str(QuestionFamily))
frmNotes.SetTo = "C"
If (QuestionFamily = 8) Then
    frmNotes.SetTo = "R"
    frmNotes.SystemReference = ""
End If
frmNotes.EyeContext = ""
frmNotes.NoteOn = False
frmNotes.MaintainOn = True
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    frmSelectQuestion.NoteOn = frmNotes.NoteOn
    If frmNotes.NoteOn = True Then
        Call InsertLog("FrmSelectQuestion.frm", "Added New Notes For Patient " & PatientId & "", LoginCatg.PatientNotes, "", "", 0, globalExam.iAppointmentId)
    End If
End If
End Sub

Private Sub cmdPrev_Click()
If (DiagOn) Then
    CurrentOtherIndex = CurrentOtherIndexPrev
    CurrentOtherIndex = CurrentOtherIndex - 23
    If (CurrentOtherIndex < 1) Then
        CurrentOtherIndex = 1
    End If
    If Not (LoadOtherDiags) Then
        CurrentOtherIndex = 1
        CurrentOtherIndexPrev = 1
        Call LoadOtherDiags
    End If
ElseIf (ComplaintOn) Then
    If ((CurrentCCIndex - 1) Mod 20 > 0) Then
        CurrentCCIndex = CurrentCCIndex - ((CurrentCCIndex - 1) Mod 20) - 20
    Else
        CurrentCCIndex = CurrentCCIndex - 40
    End If
    If (CurrentCCIndex > TotalCCs) Then
        CurrentCCIndex = 1
    ElseIf (CurrentCCIndex < 1) Then
        CurrentCCIndex = (Int(TotalCCs / 20) * 20) + 1
    End If
    Call LoadChiefComplaints(ComplaintType)
End If
End Sub

Private Sub cmdNext_Click()
If (DiagOn) Then
    CurrentOtherIndex = CurrentOtherIndex + 1
    If (CurrentOtherIndex > TotalOtherDiags) Then
        CurrentOtherIndex = 1
    End If
    If Not (LoadOtherDiags) Then
        CurrentOtherIndex = 1
        CurrentOtherIndexPrev = 1
        Call LoadOtherDiags
    End If
ElseIf (ComplaintOn) Then
    If (CurrentCCIndex > TotalCCs) Then
        CurrentCCIndex = 1
    End If
    Call LoadChiefComplaints(ComplaintType)
End If
End Sub

Private Sub cmdOtherDx_Click()
frmEventMsgs.Header = "Display Ordering"
frmEventMsgs.AcceptText = "Alpha"
frmEventMsgs.RejectText = "Numeric"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    DisplayType = "A"
ElseIf (frmEventMsgs.Result = 2) Then
    DisplayType = "N"
Else
    Exit Sub
End If
    GetStartingValue DisplayStart, DisplayType
    If DisplayStart = "" Then
        Exit Sub
    End If
    frmSearch.DiagOn = True
    frmSearch.FilterBillable = False
    frmSearch.FilterSpecial = False
    frmSearch.Selection = False
    If DisplayType = "A" Then
        DisplayStart = "*" & DisplayStart
    End If
    If frmSearch.LoadAvailableSearch(DisplayStart, DisplayType) Then
        frmSearch.Show 1
        If Trim$(frmSearch.Selection) <> "" Then
            Diagnosis = Trim$(Mid$(frmSearch.Selection, 1, 10))
            DiagnosisId = Val(Mid$(frmSearch.Selection, 76, Len(frmSearch.Selection) - 75))
            DoDiagnosis
        End If
    End If
End Sub

Private Sub DoDiagnosis()
Dim FileNum As Long
Dim ButtonName As String
Dim StoreResults As String
If (Trim(Diagnosis) <> "") Then
    Call SetDxDetails(DiagnosisId, DiagDetails)
    StoreResults = DoctorInterfaceDirectory + "P98A" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
    FileNum = FreeFile
    If Not (FM.IsFileThere(StoreResults)) Then
        FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
        Print #FileNum, "QUESTION=" + "/Other Diagnosis"
        FM.CloseFile CLng(FileNum)
    End If
    ButtonName = "NEWDIAG:" + Trim(Diagnosis) + "=T" + Trim(str(DiagnosisId)) + " <" + Trim(Diagnosis) + ">" + "(" + Trim(str(QuestionFamily)) + ") [" + DiagDetails + "]"
    If Not (IsDiagDuplicate(ButtonName, StoreResults)) Then
        FM.OpenFile StoreResults, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
        Print #FileNum, ButtonName
        FM.CloseFile CLng(FileNum)
        CurrentText = "Other Diagnosis"
        CurrentOrder = "98A"
    End If
    Diagnosis = ""
End If
cmdPrev.Visible = False
cmdNext.Visible = False
cmdNewSearch.Visible = False
lblSys.Visible = False
cmdNotes.Visible = True
cmdOtherDx.Visible = True
lblResult.Visible = True
If (Trim(cmdAppt1.Tag) <> "") Then
    cmdAppt1.Visible = True
End If
If (Trim(cmdAppt2.Tag) <> "") Then
    cmdAppt2.Visible = True
End If
If (Trim(cmdAppt3.Tag) <> "") Then
    cmdAppt3.Visible = True
End If
If (Trim(cmdAppt4.Tag) <> "") Then
    cmdAppt4.Visible = True
End If
If (Trim(cmdAppt5.Tag) <> "") Then
    cmdAppt5.Visible = True
End If
If (Trim(cmdAppt6.Tag) <> "") Then
    cmdAppt6.Visible = True
End If
If (Trim(cmdAppt7.Tag) <> "") Then
    cmdAppt7.Visible = True
End If
If (Trim(cmdAppt8.Tag) <> "") Then
    cmdAppt8.Visible = True
End If
End Sub

Private Function IsDiagDuplicate(TheText As String, TheFile As String) As Boolean
Dim Rec As String
Dim AFile As Long
IsDiagDuplicate = False
If (FM.IsFileThere(TheFile)) Then
    AFile = 501
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(AFile)
    Do Until (EOF(AFile))
        Line Input #AFile, Rec
        If (Trim(Rec) = Trim(TheText)) Then
            IsDiagDuplicate = True
            Exit Do
        End If
    Loop
    FM.CloseFile CLng(AFile)
End If
End Function

Private Function GetStartingValue(TheText As String, TheType As String) As Boolean
TheText = ""
GetStartingValue = True
If (TheType = "N") Then
    frmNumericPad.NumPad_Field = "Starting with Diagnosis Number"
    frmNumericPad.NumPad_Result = ""
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_ChoiceOn1 = False
    frmNumericPad.NumPad_Choice1 = ""
    frmNumericPad.NumPad_ChoiceOn2 = False
    frmNumericPad.NumPad_Choice2 = ""
    frmNumericPad.NumPad_ChoiceOn3 = False
    frmNumericPad.NumPad_Choice3 = ""
    frmNumericPad.NumPad_ChoiceOn4 = False
    frmNumericPad.NumPad_Choice4 = ""
    frmNumericPad.NumPad_ChoiceOn5 = False
    frmNumericPad.NumPad_Choice5 = ""
    frmNumericPad.NumPad_ChoiceOn6 = False
    frmNumericPad.NumPad_Choice6 = ""
    frmNumericPad.NumPad_ChoiceOn7 = False
    frmNumericPad.NumPad_Choice7 = ""
    frmNumericPad.NumPad_ChoiceOn8 = False
    frmNumericPad.NumPad_Choice8 = ""
    frmNumericPad.NumPad_ChoiceOn9 = False
    frmNumericPad.NumPad_Choice9 = ""
    frmNumericPad.NumPad_ChoiceOn10 = False
    frmNumericPad.NumPad_Choice10 = ""
    frmNumericPad.NumPad_ChoiceOn11 = False
    frmNumericPad.NumPad_Choice11 = ""
    frmNumericPad.NumPad_ChoiceOn12 = False
    frmNumericPad.NumPad_Choice12 = ""
    frmNumericPad.NumPad_ChoiceOn13 = False
    frmNumericPad.NumPad_Choice13 = ""
    frmNumericPad.NumPad_ChoiceOn14 = False
    frmNumericPad.NumPad_Choice14 = ""
    frmNumericPad.NumPad_ChoiceOn15 = False
    frmNumericPad.NumPad_Choice15 = ""
    frmNumericPad.NumPad_ChoiceOn16 = False
    frmNumericPad.NumPad_Choice16 = ""
    frmNumericPad.NumPad_ChoiceOn17 = False
    frmNumericPad.NumPad_Choice17 = ""
    frmNumericPad.NumPad_ChoiceOn18 = False
    frmNumericPad.NumPad_Choice18 = ""
    frmNumericPad.NumPad_ChoiceOn19 = False
    frmNumericPad.NumPad_Choice19 = ""
    frmNumericPad.NumPad_ChoiceOn20 = False
    frmNumericPad.NumPad_Choice20 = ""
    frmNumericPad.NumPad_ChoiceOn21 = False
    frmNumericPad.NumPad_Choice21 = ""
    frmNumericPad.NumPad_ChoiceOn22 = False
    frmNumericPad.NumPad_Choice22 = ""
    frmNumericPad.NumPad_ChoiceOn23 = False
    frmNumericPad.NumPad_Choice23 = ""
    frmNumericPad.NumPad_ChoiceOn24 = False
    frmNumericPad.NumPad_Choice24 = ""
    frmNumericPad.Show 1
    If Not (frmAlphaPad.NumPad_Quit) Then
        TheText = Trim(frmNumericPad.NumPad_Result)
    Else
        TheText = ""
    End If
ElseIf (TheType = "A") Then
    frmAlphaPad.NumPad_Field = "Starting with Diagnosis Alpha"
    frmAlphaPad.NumPad_Result = ""
    frmAlphaPad.NumPad_DisplayFull = True
    frmAlphaPad.Show 1
    If Not (frmAlphaPad.NumPad_Quit) Then
        TheText = Trim(frmAlphaPad.NumPad_Result)
    Else
        TheText = ""
    End If
End If
End Function

Private Function IsDiagnosisOk(TheDiag As String, LimitDiag As String, DiagTestOn As Boolean) As Boolean
IsDiagnosisOk = True
If (Trim(LimitDiag) <> "") Then
    If (UCase(LimitDiag) < UCase(Left(TheDiag, Len(LimitDiag)))) Then
        IsDiagnosisOk = False
        Exit Function
    End If
End If
If (DiagTestOn) Then
    If (Left(TheDiag, 1) = "Y") Or (Left(TheDiag, 1) = "W") Or _
       (Left(TheDiag, 1) = "M") Or (Left(TheDiag, 1) = "X") Or (Left(TheDiag, 1) = "P") Then
        IsDiagnosisOk = False
    End If
End If
End Function

Private Sub SetDxDetails(TheRef As Long, TheDetails As String)
Dim TheName As String
Dim RetDiag As DiagnosisMasterPrimary
TheDetails = ""
If (TheRef > 0) Then
    Set RetDiag = New DiagnosisMasterPrimary
    RetDiag.PrimaryId = TheRef
    If (RetDiag.RetrievePrimary) Then
        TheName = Trim(RetDiag.PrimaryLingo)
        frmNumericPad.NumPad_Result = ""
        frmNumericPad.NumPad_Field = "Last Date " + Trim(TheName) + " occurred"
        frmNumericPad.NumPad_Max = 0
        frmNumericPad.NumPad_Min = 0
        frmNumericPad.NumPad_EyesOn = True
        frmNumericPad.NumPad_CommentOn = False
        frmNumericPad.NumPad_TimeFrameOn = False
        frmNumericPad.NumPad_TimeFrame = ""
        frmNumericPad.NumPad_DisplayFull = True
        frmNumericPad.NumPad_ChoiceOn1 = False
        frmNumericPad.NumPad_Choice1 = ""
        frmNumericPad.NumPad_ChoiceOn2 = False
        frmNumericPad.NumPad_Choice2 = ""
        frmNumericPad.NumPad_ChoiceOn3 = False
        frmNumericPad.NumPad_Choice3 = ""
        frmNumericPad.NumPad_ChoiceOn4 = False
        frmNumericPad.NumPad_Choice4 = ""
        frmNumericPad.NumPad_ChoiceOn5 = False
        frmNumericPad.NumPad_Choice5 = ""
        frmNumericPad.NumPad_ChoiceOn6 = False
        frmNumericPad.NumPad_Choice6 = ""
        frmNumericPad.NumPad_ChoiceOn7 = False
        frmNumericPad.NumPad_Choice7 = ""
        frmNumericPad.NumPad_ChoiceOn8 = False
        frmNumericPad.NumPad_Choice8 = ""
        frmNumericPad.NumPad_ChoiceOn9 = False
        frmNumericPad.NumPad_Choice9 = ""
        frmNumericPad.NumPad_ChoiceOn10 = False
        frmNumericPad.NumPad_Choice10 = ""
        frmNumericPad.NumPad_ChoiceOn11 = False
        frmNumericPad.NumPad_Choice11 = ""
        frmNumericPad.NumPad_ChoiceOn12 = False
        frmNumericPad.NumPad_Choice12 = ""
        frmNumericPad.NumPad_ChoiceOn13 = False
        frmNumericPad.NumPad_Choice13 = ""
        frmNumericPad.NumPad_ChoiceOn14 = False
        frmNumericPad.NumPad_Choice14 = ""
        frmNumericPad.NumPad_ChoiceOn15 = False
        frmNumericPad.NumPad_Choice15 = ""
        frmNumericPad.NumPad_ChoiceOn16 = False
        frmNumericPad.NumPad_Choice16 = ""
        frmNumericPad.NumPad_ChoiceOn17 = False
        frmNumericPad.NumPad_Choice17 = ""
        frmNumericPad.NumPad_ChoiceOn18 = False
        frmNumericPad.NumPad_Choice18 = ""
        frmNumericPad.NumPad_ChoiceOn19 = False
        frmNumericPad.NumPad_Choice19 = ""
        frmNumericPad.NumPad_ChoiceOn20 = False
        frmNumericPad.NumPad_Choice20 = ""
        frmNumericPad.NumPad_ChoiceOn21 = False
        frmNumericPad.NumPad_Choice21 = ""
        frmNumericPad.NumPad_ChoiceOn22 = False
        frmNumericPad.NumPad_Choice22 = ""
        frmNumericPad.NumPad_ChoiceOn23 = False
        frmNumericPad.NumPad_Choice23 = ""
        frmNumericPad.NumPad_ChoiceOn24 = False
        frmNumericPad.NumPad_Choice24 = ""
        frmNumericPad.Show 1
        If Not (frmNumericPad.NumPad_Quit) And (Trim(frmNumericPad.NumPad_Result) <> "") Then
            TheDetails = Trim(frmNumericPad.NumPad_Result)
        End If
    End If
    Set RetDiag = Nothing
End If
End Sub

Private Sub SetupComplaint(IType As String)
Dim FileNum As Long
Dim StoreResults As String
If (Trim(IType) <> "") Then
    StoreResults = DoctorInterfaceDirectory + "P97A" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
    FileNum = FreeFile
    If (FM.IsFileThere(StoreResults)) Then
        FM.OpenFile StoreResults, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
    Else
        FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
    End If
    Print #FileNum, "QUESTION=/CHIEFCOMPLAINTS"
    Print #FileNum, "CC:" + Complaint
    FM.CloseFile CLng(FileNum)
End If
cmdPrev.Visible = False
cmdNext.Visible = False
cmdNewSearch.Visible = False
lblSys.Visible = False
cmdNotes.Visible = True
cmdOtherDx.Visible = True
lblResult.Visible = True
If (Trim(cmdAppt1.Tag) <> "") Then
    cmdAppt1.Visible = True
End If
If (Trim(cmdAppt2.Tag) <> "") Then
    cmdAppt2.Visible = True
End If
If (Trim(cmdAppt3.Tag) <> "") Then
    cmdAppt3.Visible = True
End If
If (Trim(cmdAppt4.Tag) <> "") Then
    cmdAppt4.Visible = True
End If
If (Trim(cmdAppt5.Tag) <> "") Then
    cmdAppt5.Visible = True
End If
If (Trim(cmdAppt6.Tag) <> "") Then
    cmdAppt6.Visible = True
End If
If (Trim(cmdAppt7.Tag) <> "") Then
    cmdAppt7.Visible = True
End If
If (Trim(cmdAppt8.Tag) <> "") Then
    cmdAppt8.Visible = True
End If
End Sub

Private Sub ClearRxs()
cmdRx1.Visible = False
cmdRx2.Visible = False
cmdRx3.Visible = False
cmdRx4.Visible = False
cmdRx5.Visible = False
cmdRx6.Visible = False
cmdRx7.Visible = False
cmdRx8.Visible = False
cmdRx9.Visible = False
cmdRx10.Visible = False
cmdRx11.Visible = False
cmdRx12.Visible = False
cmdRx13.Visible = False
cmdRx14.Visible = False
cmdRx15.Visible = False
cmdRx16.Visible = False
cmdRx17.Visible = False
cmdRx18.Visible = False
cmdRx19.Visible = False
cmdRx20.Visible = False
cmdPrev.Visible = False
cmdNext.Visible = False
cmdNewSearch.Visible = False
cmdRx1.Text = "Rx1"
cmdRx2.Text = "Rx2"
cmdRx3.Text = "Rx3"
cmdRx4.Text = "Rx4"
cmdRx5.Text = "Rx5"
cmdRx6.Text = "Rx6"
cmdRx7.Text = "Rx7"
cmdRx8.Text = "Rx8"
cmdRx9.Text = "Rx9"
cmdRx10.Text = "Rx10"
cmdRx11.Text = "Rx11"
cmdRx12.Text = "Rx12"
cmdRx13.Text = "Rx13"
cmdRx14.Text = "Rx14"
cmdRx15.Text = "Rx15"
End Sub

Private Sub TriggerOthers(ABut As fpBtn)
Dim ADate As String
If (DiagOn) Then
    Diagnosis = Trim(Trim(ABut.Text))
    DiagnosisId = Val(Trim(ABut.Tag))
    Call cmdDone_Click
ElseIf (ComplaintOn) Then
    Complaint = Trim(ABut.Text)
    frmEventMsgs.Header = "Eye ?"
    frmEventMsgs.AcceptText = "OD"
    frmEventMsgs.RejectText = "OS"
    frmEventMsgs.CancelText = "OU"
    frmEventMsgs.Other0Text = "NA"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        Complaint = Complaint + " <OD>"
    ElseIf (frmEventMsgs.Result = 2) Then
        Complaint = Complaint + " <OS>"
    ElseIf (frmEventMsgs.Result = 3) Then
        Complaint = Complaint + " <NA>"
    ElseIf (frmEventMsgs.Result = 4) Then
        Complaint = Complaint + " <OU>"
    End If
    If (ComplaintDoDate) Then
        Call RequestDate(ADate)
        If (Trim(ADate) <> "") Then
            Complaint = Complaint + " <" + ADate + ">"
        End If
    End If
    Call SetupComplaint(ComplaintType)
    Call ClearRxs
    Call cmdDone_Click
End If
End Sub

Private Sub cmdRx1_Click()
Call TriggerOthers(cmdRx1)
End Sub

Private Sub cmdRx2_Click()
Call TriggerOthers(cmdRx2)
End Sub

Private Sub cmdRx3_Click()
Call TriggerOthers(cmdRx3)
End Sub

Private Sub cmdRx4_Click()
Call TriggerOthers(cmdRx4)
End Sub

Private Sub cmdRx5_Click()
Call TriggerOthers(cmdRx5)
End Sub

Private Sub cmdRx6_Click()
Call TriggerOthers(cmdRx6)
End Sub

Private Sub cmdRx7_Click()
Call TriggerOthers(cmdRx7)
End Sub

Private Sub cmdRx8_Click()
Call TriggerOthers(cmdRx8)
End Sub

Private Sub cmdRx9_Click()
Call TriggerOthers(cmdRx9)
End Sub

Private Sub cmdRx10_Click()
Call TriggerOthers(cmdRx10)
End Sub

Private Sub cmdRx11_Click()
Call TriggerOthers(cmdRx11)
End Sub

Private Sub cmdRx12_Click()
Call TriggerOthers(cmdRx12)
End Sub

Private Sub cmdRx13_Click()
Call TriggerOthers(cmdRx13)
End Sub

Private Sub cmdRx14_Click()
Call TriggerOthers(cmdRx14)
End Sub

Private Sub cmdRx15_Click()
Call TriggerOthers(cmdRx15)
End Sub

Private Sub cmdRx16_Click()
Call TriggerOthers(cmdRx16)
End Sub

Private Sub cmdRx17_Click()
Call TriggerOthers(cmdRx17)
End Sub

Private Sub cmdRx18_Click()
Call TriggerOthers(cmdRx18)
End Sub

Private Sub cmdRx19_Click()
Call TriggerOthers(cmdRx19)
End Sub

Private Sub cmdRx20_Click()
Call TriggerOthers(cmdRx20)
End Sub

Private Sub Form_Load()
Complaint = ""
ComplaintType = ""
ComplaintDoDate = False
End Sub

Private Function RequestDate(TheDate As String) As Boolean
RequestDate = False
TheDate = ""
frmNumericPad.NumPad_Result = ""
frmNumericPad.NumPad_Field = "Enter a date, or just press Done"
frmNumericPad.NumPad_Max = 0
frmNumericPad.NumPad_Min = 0
frmNumericPad.NumPad_Default = 0
frmNumericPad.NumPad_ForceDecimal = False
frmNumericPad.NumPad_HelpField = ""
frmNumericPad.NumPad_EyesOn = False
frmNumericPad.NumPad_CommentOn = False
frmNumericPad.NumPad_TimeFrameOn = False
frmNumericPad.NumPad_DisplayFull = True
frmNumericPad.NumPad_Choice1 = "AM"
frmNumericPad.NumPad_Choice2 = "before"
frmNumericPad.NumPad_Choice3 = "approx"
frmNumericPad.NumPad_Choice4 = "since"
frmNumericPad.NumPad_Choice5 = ""
frmNumericPad.NumPad_Choice6 = "PM"
frmNumericPad.NumPad_Choice7 = "Hour"
frmNumericPad.NumPad_Choice8 = "Min"
frmNumericPad.NumPad_Choice9 = "ago"
frmNumericPad.NumPad_Choice10 = ""
frmNumericPad.NumPad_Choice11 = "Days"
frmNumericPad.NumPad_Choice12 = "Weeks"
frmNumericPad.NumPad_Choice13 = "Months"
frmNumericPad.NumPad_Choice14 = ""
frmNumericPad.NumPad_Choice15 = ""
frmNumericPad.NumPad_Choice16 = ""
frmNumericPad.NumPad_Choice17 = ""
frmNumericPad.NumPad_Choice18 = ""
frmNumericPad.NumPad_Choice19 = ""
frmNumericPad.NumPad_Choice20 = ""
frmNumericPad.NumPad_Choice21 = ""
frmNumericPad.NumPad_Choice22 = ""
frmNumericPad.NumPad_Choice23 = ""
frmNumericPad.NumPad_Choice24 = ""
frmNumericPad.Show 1
If Not (frmNumericPad.NumPad_Quit) Then
    If (Trim(frmNumericPad.NumPad_Result) <> "") Then
        TheDate = Trim(frmNumericPad.NumPad_Result)
        If (frmNumericPad.NumPad_ChoiceOn1) Then
            TheDate = TheDate + " (AM) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn2) Then
            TheDate = TheDate + " (before) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn3) Then
            TheDate = TheDate + " (approx) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn4) Then
            TheDate = TheDate + " (since) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn6) Then
            TheDate = TheDate + " (PM) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn7) Then
            TheDate = TheDate + " (Hour) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn8) Then
            TheDate = TheDate + " (Min) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn9) Then
            TheDate = TheDate + " (ago) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn11) Then
            TheDate = TheDate + " (Days) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn12) Then
            TheDate = TheDate + " (Weeks) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn13) Then
            TheDate = TheDate + " (Months) "
        End If
    End If
End If
End Function
