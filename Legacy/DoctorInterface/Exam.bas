Attribute VB_Name = "MExam"
Option Explicit

Public Const MAX_EYE_SYSTEMS As Long = 22
Public Const FILE_NAME_SEPARATOR As String = "_"
Public Const TEST_FILE_EXT As String = ".txt"

Public Enum EEyeSystem
    eesExternal = 1
    eesPupils = 2
    eesEOM = 3
    eesVFConf = 4
    eesLidsLacrimal = 5
    eesConjSclera = 6
    eesCornea = 7
    eesAC = 8
    eesIris = 9
    eesLens = 10
    eesVitreous = 11
    eesONGlauc = 12
    eesBV = 13
    eesMacula = 14
    eesRetinaChoroid = 15
    eesPeriphRetina = 16
    eesSystemic = 17
    eesVisionProblems = 18
    eesSLE = 19
    eesFundus = 20
    eesSlitLampExam = 21
    eesFundusExam = 22
End Enum

Public Property Get globalExam() As privateCExam
Static oExam As privateCExam
    If oExam Is Nothing Then
        Set oExam = New privateCExam
    End If
    Set globalExam = oExam
End Property

Public Function GetSystemName(eesSystem As EEyeSystem) As String
Dim asName(MAX_EYE_SYSTEMS) As String
    On Error GoTo lGetSystemName_Error

    asName(1) = "External"
    asName(2) = "Pupils"
    asName(3) = "EOM"
    asName(4) = "VF Conf"
    asName(5) = "Lids Lacrimal"
    asName(6) = "Conj Sclera"
    asName(7) = "Cornea"
    asName(8) = "AC"
    asName(9) = "Iris"
    asName(10) = "Lens"
    asName(11) = "Vitreous"
    asName(12) = "ON   Glauc"
    asName(13) = "BV"
    asName(14) = "Macula"
    asName(15) = "Retina Choroid"
    asName(16) = "Periph Retina"
    asName(17) = "Systemic"
    asName(18) = "Vision Problems"
    asName(19) = "SLE"
    asName(20) = "Fundus"
    asName(21) = "Slit Lamp Exam"
    asName(22) = "Fundus Exam"
    GetSystemName = asName(eesSystem)

    Exit Function

lGetSystemName_Error:
    If eesSystem = 26 Then
        ' REFACTOR: Do nothing - this is a "Z" system, which means archived in our DM currently.
    Else
        LogError "MExam", "GetSystemName", Err, Err.Description, "System Number: " & Str$(eesSystem)
    End If
End Function

Public Function GetSystemShortName(eesSystem As EEyeSystem) As String
Dim asName(MAX_EYE_SYSTEMS) As String

    On Error GoTo lGetSystemShortName_Error

    asName(1) = "Ext"
    asName(2) = "Pupils"
    asName(3) = "EOM"
    asName(4) = "VF Conf"
    asName(5) = "Lid/Lac"
    asName(6) = "Cnj/Scl"
    asName(7) = "Cornea"
    asName(8) = "AC"
    asName(9) = "Iris"
    asName(10) = "Lens"
    asName(11) = "Fun"
    asName(12) = "Fun"
    asName(13) = "Fun"
    asName(14) = "Fun"
    asName(15) = "Fun"
    asName(16) = "Fun"
    asName(17) = "Systmc"
    asName(18) = "VA/Prob"
    asName(19) = "SLE"
    asName(20) = "Fundus"
    asName(21) = "SLE"
    asName(22) = "Fundus"
    GetSystemShortName = asName(eesSystem)

    Exit Function

lGetSystemShortName_Error:

    LogError "MExam", "GetSystemShortName", Err, Err.Description
    
End Function

Public Function GetEyeSystem(sPrimarySystem As String) As EEyeSystem
    On Error GoTo lGetEyeSystem_Error

    GetEyeSystem = Asc(Trim$(sPrimarySystem)) - 64

    Exit Function

lGetEyeSystem_Error:

    LogError "MExam", "GetEyeSystem", Err, Err.Description
    
End Function

Public Function GetTestFileName(sTestNumber As String) As String
    GetTestFileName = DoctorInterfaceDirectory & "T" & sTestNumber & FILE_NAME_SEPARATOR & Trim$(Str$(globalExam.iAppointmentId)) & FILE_NAME_SEPARATOR & Trim$(Str$(globalExam.iPatientId)) & TEST_FILE_EXT
    
    Dim FileId As String
    
    Select Case sTestNumber
    Case "EvalActions"
        FileId = sTestNumber
    Case Else
        FileId = "T" & sTestNumber
    End Select
    
    GetTestFileName = DoctorInterfaceDirectory & FileId & FILE_NAME_SEPARATOR & _
    Trim$(Str$(globalExam.iAppointmentId)) & FILE_NAME_SEPARATOR & _
    Trim$(Str$(globalExam.iPatientId)) & TEST_FILE_EXT

End Function
