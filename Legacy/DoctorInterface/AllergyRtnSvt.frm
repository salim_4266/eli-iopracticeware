VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form AllergyRtnSvt 
   BackColor       =   &H8000000D&
   BorderStyle     =   0  'None
   Caption         =   "Select Reactions, Severity"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12195
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12195
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FrmReactions 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   9375
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   12735
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv1 
         Height          =   615
         Left            =   6120
         TabIndex        =   3
         Top             =   480
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":0000
      End
      Begin fpBtnAtlLibCtl.fpBtn FpBtnRc2 
         Height          =   615
         Left            =   3720
         TabIndex        =   4
         Top             =   1200
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":022F
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc1 
         Height          =   615
         Left            =   3720
         TabIndex        =   5
         Top             =   480
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":046E
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv4 
         Height          =   615
         Left            =   6120
         TabIndex        =   6
         Top             =   2640
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":06AE
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc4 
         Height          =   615
         Left            =   3720
         TabIndex        =   7
         Top             =   2640
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":08EB
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc3 
         Height          =   615
         Left            =   3720
         TabIndex        =   8
         Top             =   1920
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":0B1E
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc6 
         Height          =   615
         Left            =   3720
         TabIndex        =   9
         Top             =   4080
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":0D51
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc5 
         Height          =   615
         Left            =   3720
         TabIndex        =   10
         Top             =   3360
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":0F85
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc8 
         Height          =   615
         Left            =   3720
         TabIndex        =   11
         Top             =   5520
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":11B8
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc7 
         Height          =   615
         Left            =   3720
         TabIndex        =   12
         Top             =   4800
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":13F2
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc9 
         Height          =   615
         Left            =   3720
         TabIndex        =   13
         Top             =   6240
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":1625
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc11 
         Height          =   615
         Left            =   3720
         TabIndex        =   14
         Top             =   7680
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":1856
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc10 
         Height          =   615
         Left            =   3720
         TabIndex        =   15
         Top             =   6960
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":1A86
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv5 
         Height          =   615
         Left            =   6120
         TabIndex        =   16
         Top             =   3360
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":1CB9
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtn1 
         Height          =   855
         Left            =   8880
         TabIndex        =   19
         Top             =   8040
         Width           =   1965
         _Version        =   131072
         _ExtentX        =   3466
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":1EEA
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv3 
         Height          =   615
         Left            =   6120
         TabIndex        =   20
         Top             =   1920
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":2119
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv2 
         Height          =   615
         Left            =   6120
         TabIndex        =   21
         Top             =   1200
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":234C
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtn4 
         Height          =   855
         Left            =   840
         TabIndex        =   22
         Top             =   8040
         Width           =   1965
         _Version        =   131072
         _ExtentX        =   3466
         _ExtentY        =   1508
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AllergyRtnSvt.frx":2587
      End
      Begin VB.Label Label3 
         BackColor       =   &H00C19B49&
         BackStyle       =   0  'Transparent
         Caption         =   "Reactions"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   4200
         TabIndex        =   18
         Top             =   120
         Width           =   1860
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label2 
         BackColor       =   &H00C19B49&
         BackStyle       =   0  'Transparent
         Caption         =   "Severity"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   6480
         TabIndex        =   17
         Top             =   120
         Width           =   1860
         WordWrap        =   -1  'True
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtn2 
      Height          =   615
      Left            =   840
      TabIndex        =   0
      Top             =   -2640
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AllergyRtnSvt.frx":27B6
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtn3 
      Height          =   615
      Left            =   4320
      TabIndex        =   1
      Top             =   -2640
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AllergyRtnSvt.frx":29E8
   End
End
Attribute VB_Name = "AllergyRtnSvt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
' Refactor: take out all calls to set messages individually.  Remove string variables.
Public Reaction As String
Public Severity As String

Private Sub EnableRctSvrtBt()
    fpBtnRc1.Enabled = True
    FpBtnRc2.Enabled = True
    fpBtnRc3.Enabled = True
    fpBtnRc4.Enabled = True
    fpBtnRc5.Enabled = True
    fpBtnRc6.Enabled = True
    fpBtnRc7.Enabled = True
    fpBtnRc8.Enabled = True
    fpBtnRc9.Enabled = True
    fpBtnRc10.Enabled = True
    fpBtnRc11.Enabled = True
    fpBtnSv1.Enabled = True
    fpBtnSv2.Enabled = True
    fpBtnSv3.Enabled = True
    fpBtnSv4.Enabled = True
    fpBtnSv5.Enabled = True
End Sub
Private Sub form_load()
Call EnableRctSvrtBt
Reaction = "None"
Severity = "None"
End Sub


Private Sub fpBtn1_Click()
Unload Me
End Sub

Private Sub fpBtn4_Click()
Reaction = "None"
Severity = "None"
Unload Me
End Sub

Private Sub fpBtnRc1_Click()
If (Reaction <> "" And Reaction <> "None") Then
    
Else
    Reaction = fpBtnRc1.Text
    fpBtnRc1.Enabled = False
End If
End Sub

Private Sub FpBtnRc2_Click()
If (Reaction <> "" And Reaction <> "None") Then
    
Else
Reaction = FpBtnRc2.Text
FpBtnRc2.Enabled = False
End If
End Sub

Private Sub fpBtnRc3_Click()
If (Reaction <> "None" And Reaction <> "") Then
    
Else
Reaction = fpBtnRc3.Text
fpBtnRc3.Enabled = False
End If
End Sub

Private Sub fpBtnRc4_Click()
If (Reaction <> "None" And Reaction <> "") Then
    
Else
Reaction = fpBtnRc4.Text
fpBtnRc4.Enabled = False
End If
End Sub

Private Sub fpBtnRc5_Click()
If (Reaction <> "None" And Reaction <> "") Then
    
Else
Reaction = fpBtnRc5.Text
fpBtnRc5.Enabled = False
End If
End Sub

Private Sub fpBtnRc6_Click()
If (Reaction <> "None" And Reaction <> "") Then
    
Else
Reaction = fpBtnRc6.Text
fpBtnRc6.Enabled = False
End If
End Sub

Private Sub fpBtnRc7_Click()
If (Reaction <> "None" And Reaction <> "") Then
    
Else
Reaction = fpBtnRc7.Text
fpBtnRc7.Enabled = False
End If
End Sub

Private Sub fpBtnRc8_Click()
If (Reaction <> "None" And Reaction <> "") Then
    
Else
Reaction = fpBtnRc8.Text
fpBtnRc8.Enabled = False
End If
End Sub

Private Sub fpBtnRc9_Click()
If (Reaction <> "None" And Reaction <> "") Then
    
Else
Reaction = fpBtnRc9.Text
fpBtnRc9.Enabled = False
End If
End Sub
Private Sub fpBtnRc10_Click()
If (Reaction <> "None" And Reaction <> "") Then
    
Else
Reaction = fpBtnRc10.Text
fpBtnRc10.Enabled = False
End If
End Sub

Private Sub fpBtnRc11_Click()
If (Reaction <> "None" And Reaction <> "") Then
    
Else
Reaction = fpBtnRc11.Text
fpBtnRc11.Enabled = False
End If
End Sub

Private Sub fpBtnSv1_Click()
If (Severity <> "None" And Severity <> "") Then
    
Else
Severity = fpBtnSv1.Text
fpBtnSv1.Enabled = False
End If
End Sub

Private Sub fpBtnSv2_Click()
If (Severity <> "None" And Severity <> "") Then
    
Else
Severity = fpBtnSv2.Text
fpBtnSv2.Enabled = False
End If
End Sub

Private Sub fpBtnSv3_Click()
If (Severity <> "None" And Severity <> "") Then
    
Else
Severity = fpBtnSv3.Text
fpBtnSv3.Enabled = False
End If
End Sub

Private Sub fpBtnSv4_Click()
If (Severity <> "None" And Severity <> "") Then
    
Else
Severity = fpBtnSv4.Text
fpBtnSv4.Enabled = False
End If
End Sub

Private Sub fpBtnSv5_Click()
If (Severity <> "None" And Severity <> "") Then
    
Else
Severity = fpBtnSv5.Text
fpBtnSv5.Enabled = False
End If
End Sub

