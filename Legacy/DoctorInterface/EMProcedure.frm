VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmEMProcedure 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure8 
      Height          =   1095
      Left            =   3840
      TabIndex        =   0
      Top             =   2880
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure6 
      Height          =   1095
      Left            =   3840
      TabIndex        =   1
      Top             =   480
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":01DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure1 
      Height          =   1095
      Left            =   360
      TabIndex        =   2
      Top             =   480
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":03BE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure2 
      Height          =   1095
      Left            =   360
      TabIndex        =   3
      Top             =   1680
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":059D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure3 
      Height          =   1095
      Left            =   360
      TabIndex        =   4
      Top             =   2880
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":077C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure4 
      Height          =   1095
      Left            =   360
      TabIndex        =   5
      Top             =   4080
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":095B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure11 
      Height          =   1095
      Left            =   7320
      TabIndex        =   6
      Top             =   480
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":0B3A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure12 
      Height          =   1095
      Left            =   7320
      TabIndex        =   7
      Top             =   1680
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":0D1A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure13 
      Height          =   1095
      Left            =   7320
      TabIndex        =   8
      Top             =   2880
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":0EFA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure14 
      Height          =   1095
      Left            =   7320
      TabIndex        =   9
      Top             =   4080
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":10DA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure15 
      Height          =   1095
      Left            =   7320
      TabIndex        =   10
      Top             =   5280
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":12BA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure9 
      Height          =   1095
      Left            =   3840
      TabIndex        =   11
      Top             =   4080
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":149A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure10 
      Height          =   1095
      Left            =   3840
      TabIndex        =   12
      Top             =   5280
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":1679
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReviewSystems 
      Height          =   975
      Left            =   8520
      TabIndex        =   13
      Top             =   7920
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":1859
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   14
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":1A99
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   975
      Left            =   1800
      TabIndex        =   15
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":1CCC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRooms 
      Height          =   975
      Left            =   5160
      TabIndex        =   16
      Top             =   7920
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":1F00
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10200
      TabIndex        =   17
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":213C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreCPTs 
      Height          =   1095
      Left            =   360
      TabIndex        =   18
      Top             =   6480
      Width           =   10935
      _Version        =   131072
      _ExtentX        =   19288
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":236F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure5 
      Height          =   1095
      Left            =   360
      TabIndex        =   19
      Top             =   5280
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":2553
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure7 
      Height          =   1095
      Left            =   3840
      TabIndex        =   20
      Top             =   1680
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EMProcedure.frx":2732
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Evaluation and  Management  Procedure for Billing"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0FFFF&
      Height          =   330
      Left            =   360
      TabIndex        =   21
      Top             =   120
      Width           =   7065
   End
End
Attribute VB_Name = "frmEMProcedure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public AppointmentId As Long
Public ActivityId As Long
Public ResourceName As String
Public NavigationAction As Long

Private ProcedureEM As String
Private MaxEMProcedures As Integer
Private CurrentIndex As Integer
Private CurrentCPTIndex As Integer

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub TurnCPTs(IType As Boolean)
cmdProcedure1.Enabled = IType
cmdProcedure2.Enabled = IType
cmdProcedure3.Enabled = IType
cmdProcedure4.Enabled = IType
cmdProcedure5.Enabled = IType
cmdProcedure6.Enabled = IType
cmdProcedure7.Enabled = IType
cmdProcedure8.Enabled = IType
cmdProcedure9.Enabled = IType
cmdProcedure10.Enabled = IType
cmdProcedure11.Enabled = IType
cmdProcedure12.Enabled = IType
cmdProcedure13.Enabled = IType
cmdProcedure14.Enabled = IType
cmdProcedure15.Enabled = IType
End Sub

Private Sub ClearCPTs()
cmdProcedure1.Visible = False
cmdProcedure1.BackColor = BaseBackColor
cmdProcedure1.ForeColor = BaseTextColor
cmdProcedure2.Visible = False
cmdProcedure2.BackColor = BaseBackColor
cmdProcedure2.ForeColor = BaseTextColor
cmdProcedure3.Visible = False
cmdProcedure3.BackColor = BaseBackColor
cmdProcedure3.ForeColor = BaseTextColor
cmdProcedure4.Visible = False
cmdProcedure4.BackColor = BaseBackColor
cmdProcedure4.ForeColor = BaseTextColor
cmdProcedure5.Visible = False
cmdProcedure5.BackColor = BaseBackColor
cmdProcedure5.ForeColor = BaseTextColor
cmdProcedure6.Visible = False
cmdProcedure6.BackColor = BaseBackColor
cmdProcedure6.ForeColor = BaseTextColor
cmdProcedure7.Visible = False
cmdProcedure7.BackColor = BaseBackColor
cmdProcedure7.ForeColor = BaseTextColor
cmdProcedure8.Visible = False
cmdProcedure8.BackColor = BaseBackColor
cmdProcedure8.ForeColor = BaseTextColor
cmdProcedure9.Visible = False
cmdProcedure9.BackColor = BaseBackColor
cmdProcedure9.ForeColor = BaseTextColor
cmdProcedure10.Visible = False
cmdProcedure10.BackColor = BaseBackColor
cmdProcedure10.ForeColor = BaseTextColor
cmdProcedure11.Visible = False
cmdProcedure11.BackColor = BaseBackColor
cmdProcedure11.ForeColor = BaseTextColor
cmdProcedure12.Visible = False
cmdProcedure12.BackColor = BaseBackColor
cmdProcedure12.ForeColor = BaseTextColor
cmdProcedure13.Visible = False
cmdProcedure13.BackColor = BaseBackColor
cmdProcedure13.ForeColor = BaseTextColor
cmdProcedure14.Visible = False
cmdProcedure14.BackColor = BaseBackColor
cmdProcedure14.ForeColor = BaseTextColor
cmdProcedure15.Visible = False
cmdProcedure15.BackColor = BaseBackColor
cmdProcedure15.ForeColor = BaseTextColor
End Sub

Private Sub cmdProcedure1_Click()
Call TriggerProcedure(cmdProcedure1)
End Sub

Private Sub cmdProcedure2_Click()
Call TriggerProcedure(cmdProcedure2)
End Sub

Private Sub cmdProcedure3_Click()
Call TriggerProcedure(cmdProcedure3)
End Sub

Private Sub cmdProcedure4_Click()
Call TriggerProcedure(cmdProcedure4)
End Sub

Private Sub cmdProcedure5_Click()
Call TriggerProcedure(cmdProcedure5)
End Sub

Private Sub cmdProcedure6_Click()
Call TriggerProcedure(cmdProcedure6)
End Sub

Private Sub cmdProcedure7_Click()
Call TriggerProcedure(cmdProcedure7)
End Sub

Private Sub cmdProcedure8_Click()
Call TriggerProcedure(cmdProcedure8)
End Sub

Private Sub cmdProcedure9_Click()
Call TriggerProcedure(cmdProcedure9)
End Sub

Private Sub cmdProcedure10_Click()
Call TriggerProcedure(cmdProcedure10)
End Sub

Private Sub cmdProcedure11_Click()
Call TriggerProcedure(cmdProcedure11)
End Sub

Private Sub cmdProcedure12_Click()
Call TriggerProcedure(cmdProcedure12)
End Sub

Private Sub cmdProcedure13_Click()
Call TriggerProcedure(cmdProcedure13)
End Sub

Private Sub cmdProcedure14_Click()
Call TriggerProcedure(cmdProcedure14)
End Sub

Private Sub cmdProcedure15_Click()
Call TriggerProcedure(cmdProcedure15)
End Sub

Private Sub cmdHome_Click()
NavigationAction = 99
Unload frmEMProcedure
End Sub

Private Sub cmdMoreCPTs_Click()
CurrentIndex = CurrentIndex + 1
If (CurrentIndex >= MaxEMProcedures) Then
    CurrentIndex = 1
End If
Call LoadEMProcedure(False)
End Sub

Private Sub cmdDone_Click()
NavigationAction = 1
Unload frmEMProcedure
End Sub

Private Sub cmdNotes_Click()
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.ResourceName = ResourceName
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Public Function LoadEMProcedure(Init As Boolean) As Boolean
Dim j As Integer
Dim i As Integer
Dim ButtonCnt As Integer
Dim TheDisplayName As String
Dim TheTag As String
Dim RetrieveProcedure As DiagnosisMasterProcedures
Set RetrieveProcedure = New DiagnosisMasterProcedures
LoadEMProcedure = False
If (Init) Then
    MaxEMProcedures = 0
    CurrentIndex = 1
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = cmdProcedure1.BackColor
    BaseTextColor = cmdProcedure1.ForeColor
    ProcedureEM = ""
End If
Call ClearCPTs
i = CurrentIndex
ButtonCnt = 1
RetrieveProcedure.ProcedureCPT = Chr(1)
RetrieveProcedure.ProcedureRank = 1
MaxEMProcedures = RetrieveProcedure.FindProcedurebyEM
If (MaxEMProcedures > 0) Then
    While (RetrieveProcedure.SelectProcedure(i)) And (ButtonCnt < 16)
        TheDisplayName = Trim(RetrieveProcedure.ProcedureCPT) + ":EM" + "-" + Trim(RetrieveProcedure.ProcedureName)
        TheTag = RetrieveProcedure.ProcedureId
        If Not (IsEMPresent(TheDisplayName, ButtonCnt)) Then
            Call SetButton(ButtonCnt, TheDisplayName, TheTag)
            ButtonCnt = ButtonCnt + 1
        End If
        i = i + 1
    Wend
End If
CurrentIndex = i
LoadEMProcedure = True
If (CurrentIndex < MaxEMProcedures) Then
    cmdMoreCPTs.Text = "More Procedures"
    cmdMoreCPTs.Visible = True
Else
    If (MaxEMProcedures > 15) Then
        cmdMoreCPTs.Text = "Beginning of Procedure List"
        cmdMoreCPTs.Visible = True
    Else
        cmdMoreCPTs.Visible = False
    End If
End If
Set RetrieveProcedure = Nothing
End Function

Private Sub cmdReviewSystems_Click()
NavigationAction = 2
CurrentIndex = 0
Unload frmEMProcedure
End Sub

Private Function IsEMPresent(ALabel As String, Cnt As Integer) As Boolean
Dim i As Integer, j As Integer
Dim TestProc As String
Dim Proc As String
IsEMPresent = False
i = InStrPS(ALabel, ":")
If (i <> 0) Then
    Proc = Left(ALabel, i - 1)
Else
    Exit Function
End If
For i = 1 To Cnt
    TestProc = ""
    If (i = 1) Then
        j = InStrPS(cmdProcedure1.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure1.Text, j - 1)
        End If
    ElseIf (i = 2) Then
        j = InStrPS(cmdProcedure2.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure2.Text, j - 1)
        End If
    ElseIf (i = 3) Then
        j = InStrPS(cmdProcedure3.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure3.Text, j - 1)
        End If
    ElseIf (i = 4) Then
        j = InStrPS(cmdProcedure4.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure4.Text, j - 1)
        End If
    ElseIf (i = 5) Then
        j = InStrPS(cmdProcedure5.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure5.Text, j - 1)
        End If
    ElseIf (i = 6) Then
        j = InStrPS(cmdProcedure6.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure6.Text, j - 1)
        End If
    ElseIf (i = 7) Then
        j = InStrPS(cmdProcedure7.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure7.Text, j - 1)
        End If
    ElseIf (i = 8) Then
        j = InStrPS(cmdProcedure8.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure8.Text, j - 1)
        End If
    ElseIf (i = 9) Then
        j = InStrPS(cmdProcedure9.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure9.Text, j - 1)
        End If
    ElseIf (i = 10) Then
        j = InStrPS(cmdProcedure10.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure10.Text, j - 1)
        End If
    ElseIf (i = 11) Then
        j = InStrPS(cmdProcedure11.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure11.Text, j - 1)
        End If
    ElseIf (i = 12) Then
        j = InStrPS(cmdProcedure12.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure12.Text, j - 1)
        End If
    ElseIf (i = 13) Then
        j = InStrPS(cmdProcedure13.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure13.Text, j - 1)
        End If
    ElseIf (i = 14) Then
        j = InStrPS(cmdProcedure14.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure14.Text, j - 1)
        End If
    ElseIf (i = 15) Then
        j = InStrPS(cmdProcedure15.Text, ":")
        If (j <> 0) Then
            TestProc = Left(cmdProcedure15.Text, j - 1)
        End If
    End If
    If (TestProc = Proc) Then
        IsEMPresent = True
        Exit For
    End If
Next i
End Function

Private Sub cmdRooms_Click()
Dim q, RcvId As Long
q = InStrPS(ResourceName, "-")
If (q <> 0) Then
    RcvId = Val(Mid(ResourceName, 2, q - 2))
    frmRoomActivity.TheReceiverId = RcvId
    frmRoomActivity.ResourceName = ResourceName
    frmRoomActivity.Show 1
    If (frmRoomActivity.ResourceName <> ResourceName) Then
        ResourceName = frmRoomActivity.ResourceName
    End If
    If (Trim(ResourceName) = "") Then
        Call cmdHome_Click
    End If
End If
End Sub

Private Sub TriggerProcedure(AButton As fpBtn)
Dim i As Integer
If (AButton.BackColor = BaseBackColor) Then
    i = InStrPS(AButton.Text, ":")
    If (i <> 0) Then
        ProcedureEM = Left(AButton.Text, i - 1)
        AButton.BackColor = ButtonSelectionColor
        AButton.ForeColor = TextSelectionColor
        Call TurnCPTs(False)
        AButton.Enabled = True
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    ProcedureEM = ""
    Call TurnCPTs(True)
End If
End Sub

Private Sub SetButton(Ref As Integer, ADisplayName As String, ATag As String)
If (Ref = 1) Then
    cmdProcedure1.Text = ADisplayName
    cmdProcedure1.Tag = ATag
    cmdProcedure1.Visible = True
ElseIf (Ref = 2) Then
    cmdProcedure2.Text = ADisplayName
    cmdProcedure2.Tag = ATag
    cmdProcedure2.Visible = True
ElseIf (Ref = 3) Then
    cmdProcedure3.Text = ADisplayName
    cmdProcedure3.Tag = ATag
    cmdProcedure3.Visible = True
ElseIf (Ref = 4) Then
    cmdProcedure4.Text = ADisplayName
    cmdProcedure4.Tag = ATag
    cmdProcedure4.Visible = True
ElseIf (Ref = 5) Then
    cmdProcedure5.Text = ADisplayName
    cmdProcedure5.Tag = ATag
    cmdProcedure5.Visible = True
ElseIf (Ref = 6) Then
    cmdProcedure6.Text = ADisplayName
    cmdProcedure6.Tag = ATag
    cmdProcedure6.Visible = True
ElseIf (Ref = 7) Then
    cmdProcedure7.Text = ADisplayName
    cmdProcedure7.Tag = ATag
    cmdProcedure7.Visible = True
ElseIf (Ref = 8) Then
    cmdProcedure8.Text = ADisplayName
    cmdProcedure8.Tag = ATag
    cmdProcedure8.Visible = True
ElseIf (Ref = 9) Then
    cmdProcedure9.Text = ADisplayName
    cmdProcedure9.Tag = ATag
    cmdProcedure9.Visible = True
ElseIf (Ref = 10) Then
    cmdProcedure10.Text = ADisplayName
    cmdProcedure10.Tag = ATag
    cmdProcedure10.Visible = True
ElseIf (Ref = 11) Then
    cmdProcedure11.Text = ADisplayName
    cmdProcedure11.Tag = ATag
    cmdProcedure11.Visible = True
ElseIf (Ref = 12) Then
    cmdProcedure12.Text = ADisplayName
    cmdProcedure12.Tag = ATag
    cmdProcedure12.Visible = True
ElseIf (Ref = 13) Then
    cmdProcedure13.Text = ADisplayName
    cmdProcedure13.Tag = ATag
    cmdProcedure13.Visible = True
ElseIf (Ref = 14) Then
    cmdProcedure14.Text = ADisplayName
    cmdProcedure14.Tag = ATag
    cmdProcedure14.Visible = True
ElseIf (Ref = 15) Then
    cmdProcedure15.Text = ADisplayName
    cmdProcedure15.Tag = ATag
    cmdProcedure15.Visible = True
End If
End Sub

Public Function GetEMProcedure(AProcedure As String) As Boolean
GetEMProcedure = False
If (Trim(ProcedureEM) <> "") Then
    AProcedure = ProcedureEM
    GetEMProcedure = True
End If
End Function
