VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmPalette 
   BackColor       =   &H00505050&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Select Palette"
   ClientHeight    =   4545
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6075
   ClipControls    =   0   'False
   ForeColor       =   &H00808000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Palette.frx":0000
   ScaleHeight     =   4545
   ScaleWidth      =   6075
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1080
      Top             =   2400
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.ComboBox lstZones 
      Height          =   315
      Left            =   240
      TabIndex        =   14
      Top             =   1200
      Visible         =   0   'False
      Width           =   4935
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd1 
      Height          =   975
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MouseIcon       =   "Palette.frx":36C9
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15329769
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":382B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd2 
      Height          =   975
      Left            =   960
      TabIndex        =   4
      Top             =   480
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15329769
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":3B90
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd3 
      Height          =   975
      Left            =   1680
      TabIndex        =   5
      Top             =   480
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15329769
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":3DB8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdYellow 
      Height          =   975
      Left            =   3120
      TabIndex        =   0
      Top             =   2400
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":3FE0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRed 
      Height          =   975
      Left            =   3120
      TabIndex        =   1
      Top             =   1440
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":420F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdWhite 
      Height          =   975
      Left            =   4560
      TabIndex        =   2
      Top             =   480
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":443A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrown 
      Height          =   975
      Left            =   3840
      TabIndex        =   6
      Top             =   2400
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":4665
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBlue 
      Height          =   975
      Left            =   3840
      TabIndex        =   7
      Top             =   1440
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":4890
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdGreen 
      Height          =   975
      Left            =   4560
      TabIndex        =   8
      Top             =   1440
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":4ABB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOrange 
      Height          =   975
      Left            =   4560
      TabIndex        =   9
      Top             =   2400
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":4CE6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBlack 
      Height          =   975
      Left            =   3840
      TabIndex        =   10
      Top             =   480
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":4F11
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPurple 
      Height          =   975
      Left            =   3120
      TabIndex        =   11
      Top             =   480
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":5138
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   4920
      TabIndex        =   12
      Top             =   3480
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":5363
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   13
      Top             =   3480
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":5592
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOtherColor 
      Height          =   975
      Left            =   240
      TabIndex        =   15
      Top             =   2040
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":57C1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLBlue 
      Height          =   975
      Left            =   5280
      TabIndex        =   19
      Top             =   480
      Width           =   675
      _Version        =   131072
      _ExtentX        =   1191
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Palette.frx":59F0
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Any Color"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   18
      Top             =   1680
      Width           =   930
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Line Width"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   17
      Top             =   120
      Width           =   1035
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Medicare Colors"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   3120
      TabIndex        =   16
      Top             =   120
      Width           =   1545
   End
End
Attribute VB_Name = "frmPalette"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ZoneDescription As String
Public ColorSelected As Long
Public LineSelected As Long

Public Function LoadPalette(Init As Integer, zz As String) As Boolean
Dim u As Integer
Dim RetCodes As PracticeCodes
ColorSelected = -1
LineSelected = -1
ZoneDescription = ""
lstZones.Clear
LoadPalette = True
If (Init = 0) Then
    frmPalette.Caption = "Select Palette"
    Label1.Visible = True
    Label2.Visible = True
    Label3.Visible = True
    cmd1.Visible = True
    cmd2.Visible = True
    cmd3.Visible = True
    cmdOtherColor.Visible = True
    cmdRed.Visible = True
    cmdLBlue.Visible = True
    cmdBlue.Visible = True
    cmdBrown.Visible = True
    cmdBlack.Visible = True
    cmdGreen.Visible = True
    cmdWhite.Visible = True
    cmdOrange.Visible = True
    cmdPurple.Visible = True
    cmdYellow.Visible = True
    lstZones.Visible = False
Else
    frmPalette.Caption = "Select Zone Description"
    Label1.Visible = False
    Label2.Visible = False
    Label3.Visible = False
    cmd1.Visible = False
    cmd2.Visible = False
    cmd3.Visible = False
    cmdOtherColor.Visible = False
    cmdRed.Visible = False
    cmdLBlue.Visible = False
    cmdBlue.Visible = False
    cmdBrown.Visible = False
    cmdBlack.Visible = False
    cmdGreen.Visible = False
    cmdWhite.Visible = False
    cmdOrange.Visible = False
    cmdPurple.Visible = False
    cmdYellow.Visible = False
    Set RetCodes = New PracticeCodes
    RetCodes.ReferenceType = "Zones" + zz + "Alternatives"
    If (RetCodes.FindCode > 0) Then
        u = 1
        While (RetCodes.SelectCode(u))
            lstZones.AddItem Trim(RetCodes.ReferenceCode)
            u = u + 1
        Wend
    End If
    Set RetCodes = Nothing
    lstZones.Visible = True
End If
End Function

Private Sub cmdDone_Click()
Unload frmPalette
End Sub

Private Sub cmdHome_Click()
ColorSelected = -1
LineSelected = -1
ZoneDescription = ""
Unload frmPalette
End Sub

Private Sub cmd1_Click()
LineSelected = 2
End Sub

Private Sub cmd2_Click()
LineSelected = 6
End Sub

Private Sub cmd3_Click()
LineSelected = 10
End Sub

Private Sub cmdGreen_Click()
ColorSelected = cmdGreen.BackColor
End Sub

Private Sub cmdLBlue_Click()
ColorSelected = cmdLBlue.BackColor
End Sub

Private Sub cmdOtherColor_Click()
CommonDialog1.CancelError = True
On Error GoTo UI_ErrorHandler
CommonDialog1.Flags = &H1&
CommonDialog1.ShowColor
cmdOtherColor.BackColor = CommonDialog1.Color
ColorSelected = cmdOtherColor.BackColor
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub cmdPurple_Click()
ColorSelected = cmdPurple.BackColor
End Sub

Private Sub cmdYellow_Click()
ColorSelected = cmdYellow.BackColor
End Sub

Private Sub cmdRed_Click()
ColorSelected = cmdRed.BackColor
End Sub

Private Sub cmdOrange_Click()
ColorSelected = cmdOrange.BackColor
End Sub

Private Sub cmdBlack_Click()
ColorSelected = cmdBlack.BackColor
End Sub

Private Sub cmdBlue_Click()
ColorSelected = cmdBlue.BackColor
End Sub

Private Sub cmdBrown_Click()
ColorSelected = cmdBrown.BackColor
End Sub

Private Sub cmdWhite_Click()
ColorSelected = cmdWhite.BackColor
End Sub

Private Sub lstZones_Click()
If (lstZones.ListIndex >= 0) Then
    ZoneDescription = Trim(lstZones.List(lstZones.ListIndex))
End If
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
