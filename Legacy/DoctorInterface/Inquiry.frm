VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmInquiry 
   BackColor       =   &H00505050&
   Caption         =   "Reason for Selection"
   ClientHeight    =   7425
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5355
   ClipControls    =   0   'False
   ForeColor       =   &H00E0E0E0&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Inquiry.frx":0000
   ScaleHeight     =   7425
   ScaleWidth      =   5355
   StartUpPosition =   2  'CenterScreen
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   870
      Left            =   1920
      TabIndex        =   0
      Top             =   6480
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Inquiry.frx":36C9
   End
   Begin VB.Line Line3 
      X1              =   120
      X2              =   5280
      Y1              =   720
      Y2              =   720
   End
   Begin VB.Label lblResultsOpth 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Eye Code Qualified:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   29
      Top             =   360
      Width           =   1890
   End
   Begin VB.Label lblResults 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "E + M Code Qualified:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   28
      Top             =   120
      Width           =   2070
   End
   Begin VB.Label lblRx 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Managed Meds:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   27
      Top             =   6240
      Width           =   1395
   End
   Begin VB.Label lblProcs 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Procedures Ordered:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   26
      Top             =   5880
      Width           =   1800
   End
   Begin VB.Label lblTests 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Tests Ordered:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   25
      Top             =   5520
      Width           =   1290
   End
   Begin VB.Label lblNewD 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "New Diagnosis:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   24
      Top             =   5160
      Width           =   1350
   End
   Begin VB.Label lblDiag 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Chronic Diagnosis:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   23
      Top             =   4800
      Width           =   1635
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Medical Decision Making:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   22
      Top             =   4440
      Width           =   2430
   End
   Begin VB.Label lblMDMResult 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "MDM Results"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2880
      TabIndex        =   21
      Top             =   4440
      Width           =   1200
   End
   Begin VB.Line Line2 
      X1              =   120
      X2              =   5280
      Y1              =   1800
      Y2              =   1800
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   5280
      Y1              =   4320
      Y2              =   4320
   End
   Begin VB.Label lblRos 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "ROS"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   2160
      TabIndex        =   20
      Top             =   1200
      Width           =   420
   End
   Begin VB.Label lblHx 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "PHX"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   2160
      TabIndex        =   19
      Top             =   1440
      Width           =   375
   End
   Begin VB.Label lblFun 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Fundus Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   18
      Top             =   3960
      Width           =   1380
   End
   Begin VB.Label lblLens 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Lens Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   2880
      TabIndex        =   17
      Top             =   3720
      Width           =   1155
   End
   Begin VB.Label lblAC 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "AC Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   2880
      TabIndex        =   16
      Top             =   3480
      Width           =   1005
   End
   Begin VB.Label lblAd 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Adnexa Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   15
      Top             =   3240
      Width           =   1395
   End
   Begin VB.Label lblON 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "ON/Vit Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   14
      Top             =   3720
      Width           =   1320
   End
   Begin VB.Label lblCornea 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Cornea Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   13
      Top             =   3480
      Width           =   1350
   End
   Begin VB.Label lblHPI 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "HPI Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   12
      Top             =   1440
      Width           =   1050
   End
   Begin VB.Label lblCC 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "CC/RV Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   11
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Label lblExResult 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Exam Results"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1800
      TabIndex        =   10
      Top             =   1920
      Width           =   1275
   End
   Begin VB.Label lblHxResult 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Hx Results"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1680
      TabIndex        =   9
      Top             =   840
      Width           =   990
   End
   Begin VB.Label lblGMed 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Gen Med Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   2760
      TabIndex        =   8
      Top             =   2280
      Width           =   1530
   End
   Begin VB.Label lblIOP 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "IOP Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   7
      Top             =   2520
      Width           =   1065
   End
   Begin VB.Label lblVA 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "VA Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   6
      Top             =   2280
      Width           =   1005
   End
   Begin VB.Label lblConj 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Conj Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   2880
      TabIndex        =   5
      Top             =   3000
      Width           =   1125
   End
   Begin VB.Label lblVCF 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "VF Conf Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   2880
      TabIndex        =   4
      Top             =   3240
      Width           =   1440
   End
   Begin VB.Label lblPupils 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Pupils Present"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   3
      Top             =   3000
      Width           =   1275
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Type of Exam:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   2
      Top             =   1920
      Width           =   1365
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      Caption         =   "Type of History:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   1470
   End
End
Attribute VB_Name = "frmInquiry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ResultLevel As Integer
Public ResultLevelOpth As Integer

Private Sub cmdDone_Click()
Unload frmInquiry
End Sub

Private Function ActivatePresence() As Boolean
Dim ORankingOn As Boolean
Dim RankingVerifier As Boolean
Dim ODOn As Boolean, OSOn As Boolean
Dim AResult As Boolean, TheHigh As Boolean
Dim PatId As Long
Dim Ranking As Integer
Dim MRanking As Integer
Dim ARanking(3) As Integer
Dim RankingTemp As Integer
Dim i As Integer, ACnt As Integer
Dim ThePO As Integer, ACnt1 As Integer
Dim Temp As String, Temp1 As String
Dim TheTime As String, TheType As String
Dim TheText As String, TheQues As String
Dim TheOrder As String, TheParty As String
Dim ADiag As String, ADiag1 As String, AEye As String
ResultLevel = 0
ResultLevelOpth = 0
ActivatePresence = True
PatId = frmSystems1.GetPatientId
ORankingOn = False
RankingVerifier = False
lblHxResult.Caption = frmSystems1.GetHxReview
lblHx.Caption = "PHX "
lblHx.ForeColor = 255
lblRos.Caption = "ROS "
lblRos.ForeColor = 255
If (frmSystems1.lstOHX.ListCount > 1) Or (frmSystems1.lstFMX.ListCount > 1) Or (frmSystems1.lstMHX.ListCount > 1) Then
    lblHx.Caption = "PHX " + frmSystems1.GetHxReview
    lblHx.ForeColor = 0
    lblRos.Caption = "ROS " + frmSystems1.GetHxReview
    lblRos.ForeColor = 0
End If
ARanking(1) = 0
If (lblHxResult.Caption = "Problem Focused") Then
    ARanking(1) = 1
ElseIf (lblHxResult.Caption = "Expanded Problem Focused") Then
    ARanking(1) = 2
ElseIf (lblHxResult.Caption = "Detailed") Then
    ARanking(1) = 3
ElseIf (lblHxResult.Caption = "Comprehensive") Then
    ARanking(1) = 4
End If
lblCC.Visible = True
lblCC.Caption = "CC/RV Not Present"
lblCC.ForeColor = 255
If (frmSystems1.lstHPI.ListCount > 0) Then
    If (Len(frmSystems1.lstHPI.List(0)) > 4) Then
        lblCC.Caption = "CC/RV Present"
        lblCC.ForeColor = 0
    End If
End If
lblHPI.Visible = True
lblHPI.Caption = "HPI Not Present"
lblHPI.ForeColor = 255
If (frmSystems1.lstCC.ListCount > 1) Then
    lblHPI.Caption = "HPI Present"
    lblHPI.ForeColor = 0
End If
If (lblCC.ForeColor = 0) And (lblHPI.ForeColor = 0) And (Trim(lblHxResult.Caption) <> "") And (Trim(lblHxResult.Caption) <> "None") Then
    RankingVerifier = True
End If
Ranking = 0
lblVA.Visible = True
lblVA.Caption = "VA Not Present"
lblVA.ForeColor = 255
lblIOP.Visible = True
lblIOP.Caption = "IOP Not Present"
lblIOP.ForeColor = 255
lblGMed.Visible = True
lblGMed.Caption = "Gen Med Not Present"
lblGMed.ForeColor = 255
i = 1
Do Until Not (frmSystems1.GetTestText(i, TheText, TheQues, TheOrder, TheParty, TheTime, TheHigh, TheType, ThePO))
    If (Trim(TheText) <> "") Then
        If (TheOrder = "03A") Or (TheOrder = "02A") Then
            If (lblVA.ForeColor <> 0) Then
                lblVA.Caption = "VA Present"
                lblVA.ForeColor = 0
                Ranking = Ranking + 1
            End If
        ElseIf (TheOrder = "26A") Then
            lblIOP.Caption = "IOP Present"
            lblIOP.ForeColor = 0
            Ranking = Ranking + 1
        ElseIf (TheOrder = "01A") Then
            lblGMed.Caption = "Gen Med Present"
            lblGMed.ForeColor = 0
            Ranking = Ranking + 1
        End If
    End If
    i = i + 1
Loop
lblPupils.Visible = True
lblPupils.Caption = "Pupils Not Present"
lblPupils.ForeColor = 255
Temp = "B"
Temp1 = ""
GoSub VerifySystem
If (AResult) Then
    lblPupils.Caption = "Pupils Present"
    lblPupils.ForeColor = 0
    Ranking = Ranking + 1
End If
lblConj.Visible = True
lblConj.Caption = "Conj Not Present"
lblConj.ForeColor = 255
Temp = "F"
Temp1 = "X"
GoSub VerifySystem
If (AResult) Then
    lblConj.Caption = "Conj Present"
    lblConj.ForeColor = 0
    Ranking = Ranking + 1
End If
lblAd.Visible = True
lblAd.Caption = "Adnexa Not Present"
lblAd.ForeColor = 255
Temp = "A"
Temp1 = ""
GoSub VerifySystem
If (AResult) Then
    lblAd.Caption = "Adnexa Present"
    lblAd.ForeColor = 0
    Ranking = Ranking + 1
End If
lblCornea.Visible = True
lblCornea.Caption = "Cornea Not Present"
lblCornea.ForeColor = 255
Temp = "G"
Temp1 = "X"
GoSub VerifySystem
If (AResult) Then
    lblCornea.Caption = "Cornea Present"
    lblCornea.ForeColor = 0
    Ranking = Ranking + 1
End If
lblVCF.Visible = True
lblVCF.Caption = "CVF Not Present"
lblVCF.ForeColor = 255
Temp = "D"
Temp1 = ""
GoSub VerifySystem
If (AResult) Then
    lblVCF.Caption = "CVF Present"
    lblVCF.ForeColor = 0
    Ranking = Ranking + 1
End If
lblON.Visible = True
lblON.Caption = "ON/Vit Not Present"
lblON.ForeColor = 255
Temp = "KL"
Temp1 = "Y"
GoSub VerifySystem
If (AResult) Then
    lblON.Caption = "ON/Vit Present"
    lblON.ForeColor = 0
    Ranking = Ranking + 1
End If

lblAC.Visible = True
lblAC.Caption = "AC Not Present"
lblAC.ForeColor = 255
Temp = "H"
Temp1 = "X"
GoSub VerifySystem
If (AResult) Then
    lblAC.Caption = "AC Present"
    lblAC.ForeColor = 0
    Ranking = Ranking + 1
End If

lblLens.Visible = True
lblLens.Caption = "Lens Not Present"
lblLens.ForeColor = 255
Temp = "J"
Temp1 = "X"
GoSub VerifySystem
If (AResult) Then
    lblLens.Caption = "Lens Present"
    lblLens.ForeColor = 0
    Ranking = Ranking + 1
End If

lblFun.Visible = True
lblFun.Caption = "Fundus Not Present"
lblFun.ForeColor = 255
Temp = "MNP"
Temp1 = "Y"
GoSub VerifySystem
If (AResult) Then
    lblFun.Caption = "Fundus Present"
    lblFun.ForeColor = 0
    Ranking = Ranking + 1
End If
Temp = "None"
ARanking(2) = 0
If (Ranking >= 12) Then
    Temp = "Comprehensive"
    ARanking(2) = 4
ElseIf (Ranking >= 9) And (Ranking <= 11) Then
    Temp = "Detailed"
    ARanking(2) = 3
ElseIf (Ranking >= 6) And (Ranking <= 8) Then
    Temp = "Expanded Problem Focused"
    ARanking(2) = 2
ElseIf (Ranking > 0) Then
    Temp = "Problem Focused"
    ARanking(2) = 1
End If
lblExResult.Caption = Temp

MRanking = 0
Temp1 = "RX"
GoSub GetActions
lblRx.Caption = "Managed Meds: No"
If (ACnt < 1) Then
    If (frmSystems1.lstMeds.ListCount > 1) Then
        ACnt = frmSystems1.lstMeds.ListCount - 1
    End If
End If
If (ACnt > 0) Then
    lblRx.Caption = "Managed Meds: Yes"
    MRanking = MRanking + 1
    ORankingOn = True
End If
Temp1 = "OFFICE PROCEDURES"
GoSub GetActions
lblProcs.Caption = "Procedures Ordered: 0"
If (ACnt > 0) Then
    lblProcs.Caption = "Procedures Ordered: " + Trim(str(ACnt))
    MRanking = MRanking + (2 * ACnt)
    ORankingOn = True
End If
Temp1 = "ORDER A TEST"
GoSub GetActions
lblTests.Caption = "Tests Ordered: 0"
If (ACnt > 0) Then
    lblTests.Caption = "Tests Ordered: " + Trim(str(ACnt))
    MRanking = MRanking + (2 * ACnt)
    ORankingOn = True
End If
If Not (ORankingOn) Then
    Temp1 = "SCHEDULE SURGERY"
    GoSub GetActions
    If (ACnt > 0) Then
        ORankingOn = True
    End If
End If
ACnt = 0
ACnt1 = 0
For i = 1 To frmSetProcedures.TotalBilledDiagnosis
    If (frmSetProcedures.GetBilledDiagnosis(i, ADiag, ADiag1, AEye, Temp, Temp1, TheHigh)) Then
        If (IsNewDiagnosis(ADiag, PatId)) Then
            ACnt = ACnt + 1
        Else
            ACnt1 = ACnt1 + 1
        End If
    End If
Next i
lblNewD.Caption = "New Diagnosis: 0"
If (ACnt > 0) Then
    lblNewD.Caption = "New Diagnosis: " + Trim(str(ACnt))
    MRanking = MRanking + (2 * ACnt)
    ORankingOn = True
End If
lblDiag.Caption = "Chronic Diagnosis: 0"
If (ACnt1 > 0) Then
    lblDiag.Caption = "Chronic Diagnosis: " + Trim(str(ACnt1))
    MRanking = MRanking + (2 * ACnt1)
End If
Temp = "Any"
ARanking(3) = 2
If (MRanking >= 9) Then
    Temp = "High Complexity"
    ARanking(3) = 5
ElseIf (MRanking >= 6) And (MRanking <= 8) Then
    Temp = "Moderate Complexity"
    ARanking(3) = 4
ElseIf (MRanking >= 2) And (MRanking <= 5) Then
    Temp = "Low Complexity"
    ARanking(3) = 3
End If
lblMDMResult.Caption = Temp

' Analysis is such that the lowest of the first two, coupled with the third
RankingTemp = ARanking(1)
If (ARanking(2) < ARanking(1)) Then
    RankingTemp = ARanking(2)
End If
ResultLevel = 0
lblResults.Caption = "E + M Code Qualified: Level 1"
If (RankingTemp <> 0) Then
    ResultLevel = 1
    lblResults.Caption = "E + M Code Qualified: Level 2"
    If (RankingTemp >= 3) Then
        If (ARanking(3) = 3) Then
            ResultLevel = 2
            lblResults.Caption = "E + M Code Qualified: Level 3"
        ElseIf (ARanking(3) > 3) Then
            ResultLevel = 3
            lblResults.Caption = "E + M Code Qualified: Level 4"
        End If
    End If
    If (ARanking(1) = 3) And (ResultLevel > 2) Then
        ResultLevel = 2
        lblResults.Caption = "E + M Code Qualified: Level 3"
    ElseIf (ARanking(1) = 1) And (ResultLevel > 1) Then
        ResultLevel = 1
        lblResults.Caption = "E + M Code Qualified: Level 2"
    ElseIf (ARanking(1) = 2) And (ResultLevel > 1) Then
        ResultLevel = 1
        lblResults.Caption = "E + M Code Qualified: Level 2"
    End If
End If
ResultLevelOpth = 0
lblResultsOpth.Caption = "Eye Code Qualified: Does Not Qualify"
If (ORankingOn) Then
    If (RankingTemp >= 2) Then
        If (ARanking(3) >= 2) And (ARanking(3) < 4) Then
            ResultLevelOpth = 2
            lblResultsOpth.Caption = "Eye Code Qualified: Level 2"
        ElseIf (ARanking(3) >= 4) Then
            ResultLevelOpth = 4
            lblResultsOpth.Caption = "Eye Code Qualified: Level 4"
        End If
    Else
        ResultLevelOpth = 2
        lblResultsOpth.Caption = "Eye Code Qualified: Level 2"
    End If
    If ((ARanking(1) = 3) Or (ARanking(1) = 2)) And (ResultLevelOpth > 2) Then
        ResultLevelOpth = 2
        lblResultsOpth.Caption = "Eye Code Qualified: Level 2"
    End If
End If
Exit Function
GetActions:
    ACnt = 0
    i = 1
    While (frmEvaluation.GetActionEvaluation(i, Temp, TheHigh, AResult))
        If (InStrPS(UCase(Temp), Temp1) > 0) Then
            If (UCase(Temp1) = "RX") Then
                If (InStrPS(Temp, "P-7") <> 0) Then
                    ACnt = ACnt + 1
                End If
            Else
                ACnt = ACnt + 1
            End If
        End If
        i = i + 1
    Wend
    Return
VerifySystem:
    ODOn = False
    OSOn = False
    AResult = False
    If (Trim(Temp) <> "") Then
        For i = 0 To frmSystems1.lstOD.ListCount - 1
            If (InStrPS(Temp, Mid(frmSystems1.lstOD.List(i), 71, 1)) <> 0) Then
                ODOn = True
                Exit For
            ElseIf (InStrPS(Temp1, Mid(frmSystems1.lstOD.List(i), 71, 1)) <> 0) Then
                ODOn = True
                Exit For
            End If
        Next i
        For i = 0 To frmSystems1.lstOS.ListCount - 1
            If (InStrPS(Temp, Mid(frmSystems1.lstOS.List(i), 71, 1)) <> 0) Then
                OSOn = True
                Exit For
            ElseIf (InStrPS(Temp1, Mid(frmSystems1.lstOS.List(i), 71, 1)) <> 0) Then
                OSOn = True
                Exit For
            End If
        Next i
        If (ODOn) Or (OSOn) Then
            AResult = True
        End If
    End If
    Return
End Function

Private Sub Form_Activate()
Call ActivatePresence
End Sub

Public Function GetAnalyticalLevel(EMLevel As Integer, OPLevel As Integer) As Boolean
Dim ORankingOn As Boolean
Dim RankingVerifier As Boolean
Dim ODOn As Boolean, OSOn As Boolean
Dim AResult As Boolean, TheHigh As Boolean
Dim PatId As Long
Dim Ranking As Integer
Dim MRanking As Integer
Dim ARanking(3) As Integer
Dim RankingTemp As Integer
Dim i As Integer, ACnt As Integer
Dim ThePO As Integer, ACnt1 As Integer
Dim Temp As String, Temp1 As String
Dim TheTime As String, TheType As String
Dim TheText As String, TheQues As String
Dim TheOrder As String, TheParty As String
Dim ADiag As String, ADiag1 As String, AEye As String
GetAnalyticalLevel = False
PatId = frmSystems1.GetPatientId
ORankingOn = False
RankingVerifier = False
Temp = frmSystems1.GetHxReview
ARanking(1) = 0
If (Temp = "Problem Focused") Then
    ARanking(1) = 1
ElseIf (Temp = "Expanded Problem Focused") Then
    ARanking(1) = 2
ElseIf (Temp = "Detailed") Then
    ARanking(1) = 3
ElseIf (Temp = "Comprehensive") Then
    ARanking(1) = 4
End If
TheHigh = False
If (frmSystems1.lstHPI.ListCount > 0) Then
    If (Len(frmSystems1.lstHPI.List(0)) > 4) Then
        TheHigh = True
    End If
End If
AResult = False
If (frmSystems1.lstCC.ListCount > 1) Then
    AResult = True
End If

i = 1
TheHigh = False
Ranking = 0
Do Until Not (frmSystems1.GetTestText(i, TheText, TheQues, TheOrder, TheParty, TheTime, TheHigh, TheType, ThePO))
    If (Trim(TheText) <> "") Then
        If (TheOrder = "03A") Or (TheOrder = "02A") Then
            If Not (TheHigh) Then
                Ranking = Ranking + 1
                TheHigh = True
            End If
        ElseIf (TheOrder = "26A") Then
            Ranking = Ranking + 1
        ElseIf (TheOrder = "01A") Then
            Ranking = Ranking + 1
        End If
    End If
    i = i + 1
Loop
Temp = "B"
Temp1 = ""
GoSub VerifySystem
If (AResult) Then
    Ranking = Ranking + 1
End If
Temp = "F"
Temp1 = "X"
GoSub VerifySystem
If (AResult) Then
    Ranking = Ranking + 1
End If
Temp = "A"
Temp1 = ""
GoSub VerifySystem
If (AResult) Then
    Ranking = Ranking + 1
End If
Temp = "G"
Temp1 = "X"
GoSub VerifySystem
If (AResult) Then
    Ranking = Ranking + 1
End If
Temp = "D"
Temp1 = ""
GoSub VerifySystem
If (AResult) Then
    Ranking = Ranking + 1
End If
Temp = "KL"
Temp1 = "Y"
GoSub VerifySystem
If (AResult) Then
    Ranking = Ranking + 1
End If

Temp = "H"
Temp1 = "X"
GoSub VerifySystem
If (AResult) Then
    Ranking = Ranking + 1
End If

Temp = "J"
Temp1 = "X"
GoSub VerifySystem
If (AResult) Then
    Ranking = Ranking + 1
End If

Temp = "MNP"
Temp1 = "Y"
GoSub VerifySystem
If (AResult) Then
    Ranking = Ranking + 1
End If
ARanking(2) = 0
If (Ranking >= 12) Then
    ARanking(2) = 4
ElseIf (Ranking >= 9) And (Ranking <= 11) Then
    ARanking(2) = 3
ElseIf (Ranking >= 6) And (Ranking <= 8) Then
    ARanking(2) = 2
ElseIf (Ranking > 0) Then
    ARanking(2) = 1
End If

MRanking = 0
Temp1 = "RX"
GoSub GetActions
If (ACnt < 1) Then
    If (frmSystems1.lstMeds.ListCount > 1) Then
        ACnt = frmSystems1.lstMeds.ListCount - 1
    End If
End If
If (ACnt > 0) Then
    MRanking = MRanking + 1
    ORankingOn = True
End If
Temp1 = "OFFICE PROCEDURES"
GoSub GetActions
If (ACnt > 0) Then
    MRanking = MRanking + (2 * ACnt)
    ORankingOn = True
End If
Temp1 = "ORDER A TEST"
GoSub GetActions
If (ACnt > 0) Then
    MRanking = MRanking + (2 * ACnt)
    ORankingOn = True
End If
If Not (ORankingOn) Then
    Temp1 = "SCHEDULE SURGERY"
    GoSub GetActions
    If (ACnt > 0) Then
        ORankingOn = True
    End If
End If
For i = 1 To frmSetProcedures.TotalBilledDiagnosis
    If (frmSetProcedures.GetBilledDiagnosis(i, ADiag, ADiag1, AEye, Temp, Temp1, TheHigh)) Then
        If (IsNewDiagnosis(ADiag, PatId)) Then
            ORankingOn = True
            MRanking = MRanking + 2
        Else
            MRanking = MRanking + 2
        End If
    End If
Next i

ARanking(3) = 2
If (MRanking >= 9) Then
    ARanking(3) = 5
ElseIf (MRanking >= 6) And (MRanking <= 8) Then
    ARanking(3) = 4
ElseIf (MRanking >= 2) And (MRanking <= 5) Then
    ARanking(3) = 3
End If

' Analysis is such that the lowest of the first two, coupled with the third
RankingTemp = ARanking(1)
If (ARanking(2) < ARanking(1)) Then
    RankingTemp = ARanking(2)
End If
ResultLevel = 0
If (RankingTemp <> 0) Then
    ResultLevel = 1
    If (RankingTemp >= 3) Then
        If (ARanking(3) = 3) Then
            ResultLevel = 2
        ElseIf (ARanking(3) > 3) Then
            ResultLevel = 3
        End If
    End If
    If (ARanking(1) = 3) And (ResultLevel > 2) Then
        ResultLevel = 2
    ElseIf (ARanking(1) = 1) And (ResultLevel > 1) Then
        ResultLevel = 1
    ElseIf (ARanking(1) = 2) And (ResultLevel > 1) Then
        ResultLevel = 1
    End If
End If
ResultLevelOpth = 0
If (ORankingOn) Then
    If (RankingTemp >= 2) Then
        If (ARanking(3) >= 2) And (ARanking(3) < 4) Then
            ResultLevelOpth = 2
        ElseIf (ARanking(3) >= 4) Then
            ResultLevelOpth = 4
        End If
    Else
        ResultLevelOpth = 2
    End If
    If ((ARanking(1) = 3) Or (ARanking(1) = 2)) And (ResultLevelOpth > 2) Then
        ResultLevelOpth = 2
    End If
End If
EMLevel = ResultLevel
OPLevel = ResultLevelOpth
GetAnalyticalLevel = True
Exit Function
GetActions:
    ACnt = 0
    i = 1
    While (frmEvaluation.GetActionEvaluation(i, Temp, TheHigh, AResult))
        If (InStrPS(UCase(Temp), Temp1) > 0) Then
            If (Temp1 = "RX") Then
                If (InStrPS(Temp, "P-7") <> 0) Then
                    ACnt = ACnt + 1
                End If
            Else
                ACnt = ACnt + 1
            End If
        End If
        i = i + 1
    Wend
    Return
VerifySystem:
    ODOn = False
    OSOn = False
    AResult = False
    If (Trim(Temp) <> "") Then
        For i = 0 To frmSystems1.lstOD.ListCount - 1
            If (InStrPS(Temp, Mid(frmSystems1.lstOD.List(i), 71, 1)) <> 0) Then
                ODOn = True
                Exit For
            ElseIf (InStrPS(Temp1, Mid(frmSystems1.lstOD.List(i), 71, 1)) <> 0) Then
                ODOn = True
                Exit For
            End If
        Next i
        For i = 0 To frmSystems1.lstOS.ListCount - 1
            If (InStrPS(Temp, Mid(frmSystems1.lstOS.List(i), 71, 1)) <> 0) Then
                OSOn = True
                Exit For
            ElseIf (InStrPS(Temp1, Mid(frmSystems1.lstOS.List(i), 71, 1)) <> 0) Then
                OSOn = True
                Exit For
            End If
        Next i
        If (ODOn) Or (OSOn) Then
            AResult = True
        End If
    End If
    Return
End Function

Public Function GetAnalyticalPricing(InsId As Long, EMProc As String, OPProc As String, ResultProc As String, RemoveOPTHOn As Boolean) As Boolean
Dim i As Integer
Dim u As Integer
Dim ThePO As Integer
Dim OPTHOn As Boolean
Dim T360On As Boolean
Dim TheHigh As Boolean
Dim TheType As String
Dim fIgnoreOphthalmoscopy As Boolean
Dim AMod As String, ALnk As String
Dim ADiag As String, AProc As String
Dim TheOrd As String, TheP As String
Dim TheT As String, OPTHProc As String
Dim TheText As String, TheQues As String
Dim EMPrice As Single, OPPrice As Single, OPTHPrice As Single
Dim RetSrv As Service
Dim RetFee As PracticeFeeSchedules
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
GetAnalyticalPricing = False
ResultProc = ""
OPTHOn = False
RemoveOPTHOn = False
fIgnoreOphthalmoscopy = True
EMPrice = 0
OPPrice = 0
OPTHPrice = 0
OPTHProc = ""
' If Ocode = Y then the Ophthalmoscopy doesn't matter.
Set RetPatientFinancialService = New PatientFinancialService
RetPatientFinancialService.InsurerId = InsId
Set RS = RetPatientFinancialService.RetrieveInsurer
If (Not RS Is Nothing And RS.RecordCount > 0) Then
    fIgnoreOphthalmoscopy = True
End If
Set RetPatientFinancialService = Nothing
If (Trim(EMProc) <> "") And (Trim(OPProc) <> "") Then
    'If Not fIgnoreOphthalmoscopy Then
        For i = 1 To frmSetProcedures.TotalBillingProcedure
            If (frmSetProcedures.GetBillingProcedure(i, AProc, ADiag, AMod, ALnk, TheHigh)) Then
                If (AProc = "92225") Or (AProc = "92226") Then
                    OPTHProc = AProc
                    OPTHOn = True
                End If
            End If
        Next i
        If (OPTHOn) Then
            T360On = False
            i = 1
            Do Until Not (frmSystems1.GetTestText(i, TheText, TheQues, TheOrd, TheP, TheT, TheHigh, TheType, ThePO))
                If (TheOrd = "70A") Then
                    If (InStrPS(TheText, "360") > 0) Then
                        T360On = True
                    End If
                    Exit Do
                End If
                i = i + 1
            Loop
        End If
        If (OPTHProc <> "") Then
            Set RetSrv = New Service
            RetSrv.Service = OPTHProc
            If (RetSrv.RetrieveService) Then
                OPTHPrice = RetSrv.ServiceFee
            End If
            Set RetSrv = Nothing
        End If
        If InsId > 0 Then
            If (OPTHProc <> "") Then
                Set RetFee = New PracticeFeeSchedules
                RetFee.SchPlanId = InsId
                RetFee.SchService = OPTHProc
                RetFee.SchLocId = -1
                RetFee.SchDoctorId = -1
                If (RetFee.FindServicebyInsurer > 0) Then
                    u = 1
                    Do Until Not (RetFee.SelectService(u))
                        If (RetFee.SchService = OPProc) Then
                            If (RetFee.SchAdjustmentAllowed > 0) Then
                                OPTHPrice = RetFee.SchAdjustmentAllowed
                                Exit Do
                            End If
                        End If
                        u = u + 1
                    Loop
                End If
                Set RetFee = Nothing
            End If
        End If
    'End If

' Test for highest fee
    Set RetSrv = New Service
    RetSrv.Service = EMProc
    If (RetSrv.RetrieveService) Then
        EMPrice = RetSrv.ServiceFee
    End If
    Set RetSrv = Nothing
    Set RetSrv = New Service
    RetSrv.Service = OPProc
    If (RetSrv.RetrieveService) Then
        OPPrice = RetSrv.ServiceFee
    End If
    Set RetSrv = Nothing
    
    If (InsId > 0) Then
        If (EMProc <> "") Then
            Set RetFee = New PracticeFeeSchedules
            RetFee.SchPlanId = InsId
            RetFee.SchService = EMProc
            RetFee.SchLocId = -1
            RetFee.SchDoctorId = -1
            If (RetFee.FindServicebyInsurer > 0) Then
                u = 1
                Do Until Not (RetFee.SelectService(u))
                    If (RetFee.SchService = EMProc) Then
                        If (RetFee.SchAdjustmentAllowed > 0) Then
                            EMPrice = RetFee.SchAdjustmentAllowed
                            Exit Do
                        End If
                    End If
                    u = u + 1
                Loop
            End If
            Set RetFee = Nothing
        End If
        
        If (OPProc <> "") Then
            Set RetFee = New PracticeFeeSchedules
            RetFee.SchPlanId = InsId
            RetFee.SchService = OPProc
            RetFee.SchLocId = -1
            RetFee.SchDoctorId = -1
            If (RetFee.FindServicebyInsurer > 0) Then
                u = 1
                Do Until Not (RetFee.SelectService(u))
                    If (RetFee.SchService = OPProc) Then
                        If (RetFee.SchAdjustmentAllowed > 0) Then
                            OPPrice = RetFee.SchAdjustmentAllowed
                            Exit Do
                        End If
                    End If
                    u = u + 1
                Loop
            End If
            Set RetFee = Nothing
        End If
    
    End If
'
' If you use an Ophthalmic code, you can not bill an
' Ophthalmoscopy (92225, or 92226) unless the ophthalmoscopy
' includes a '360 indirect ophthalmoscopy' (one of the buttons
' in the ophthalmoscopy exam element).
'
' If there is an ophthalmoscopy present but it's not a
' '360 indirect' compare the sum of the E&M code plus
' the ophthalmoscopy vs. the amount of the Ophthalmic
' code without the ophthalmoscopy.
'
' If there is an ophthalmoscopy present and it is a
' '360 indirect', compare the sum of the E&M code plus
' the ophthalmoscopy with the sum of the ophthalmic
' code plus the ophthalmoscopy.
    If fIgnoreOphthalmoscopy Then
        If (EMPrice >= OPPrice) Then
            ResultProc = EMProc
        Else
            ResultProc = OPProc
        End If
    Else
        If (OPTHOn) And (Not (T360On)) Then
            If (EMPrice + OPTHPrice >= OPPrice) Then
                ResultProc = EMProc
            Else
                ResultProc = OPProc
                RemoveOPTHOn = True
            End If
        Else
            If (EMPrice >= OPPrice) Then
                ResultProc = EMProc
            Else
                ResultProc = OPProc
            End If
        End If
    End If
End If
GetAnalyticalPricing = True
End Function

Private Function IsNewDiagnosis(ADiag As String, PatId As Long) As Boolean
Dim RetCln As PatientClinical
IsNewDiagnosis = True
If (PatId > 0) And (Trim(ADiag) <> "") Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalType = "B"
    RetCln.PatientId = PatId
    RetCln.Findings = ADiag
    RetCln.AppointmentId = 0
    RetCln.Symptom = ""
    RetCln.ImageDescriptor = ""
    RetCln.ImageInstructions = ""
    RetCln.ClinicalDraw = ""
    RetCln.ClinicalStatus = ""
    If (RetCln.FindPatientClinical > 0) Then
        IsNewDiagnosis = False
    End If
    Set RetCln = Nothing
End If
End Function
Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
End Function

