VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmEditPI 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9345
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12090
   ForeColor       =   &H00B60000&
   LinkTopic       =   "Form1"
   Picture         =   "EditPI.frx":0000
   ScaleHeight     =   9345
   ScaleWidth      =   12090
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Frame FrmReactions 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   8415
      Left            =   6960
      TabIndex        =   54
      Top             =   -120
      Visible         =   0   'False
      Width           =   5055
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv2 
         Height          =   615
         Left            =   2400
         TabIndex        =   55
         Top             =   1200
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":36C9
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv1 
         Height          =   615
         Left            =   2400
         TabIndex        =   56
         Top             =   480
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":3904
      End
      Begin fpBtnAtlLibCtl.fpBtn FpBtnRc2 
         Height          =   615
         Left            =   0
         TabIndex        =   57
         Top             =   1200
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":3B33
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc1 
         Height          =   615
         Left            =   0
         TabIndex        =   58
         Top             =   480
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":3D72
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv4 
         Height          =   615
         Left            =   2400
         TabIndex        =   61
         Top             =   2640
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":3FB2
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv3 
         Height          =   615
         Left            =   2400
         TabIndex        =   62
         Top             =   1920
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":41EF
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc4 
         Height          =   615
         Left            =   0
         TabIndex        =   63
         Top             =   2640
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":4422
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc3 
         Height          =   615
         Left            =   0
         TabIndex        =   64
         Top             =   1920
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":4655
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc6 
         Height          =   615
         Left            =   0
         TabIndex        =   65
         Top             =   4080
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":4888
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc5 
         Height          =   615
         Left            =   0
         TabIndex        =   66
         Top             =   3360
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":4ABC
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc8 
         Height          =   615
         Left            =   0
         TabIndex        =   67
         Top             =   5520
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":4CEF
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc7 
         Height          =   615
         Left            =   0
         TabIndex        =   68
         Top             =   4800
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":4F29
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc9 
         Height          =   615
         Left            =   0
         TabIndex        =   69
         Top             =   6240
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":515C
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc11 
         Height          =   615
         Left            =   0
         TabIndex        =   70
         Top             =   7680
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":538D
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnRc10 
         Height          =   615
         Left            =   0
         TabIndex        =   71
         Top             =   6960
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":55BD
      End
      Begin fpBtnAtlLibCtl.fpBtn fpBtnSv5 
         Height          =   615
         Left            =   2400
         TabIndex        =   72
         Top             =   3360
         Width           =   2325
         _Version        =   131072
         _ExtentX        =   4101
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   11373126
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "EditPI.frx":57F0
      End
      Begin VB.Label Label2 
         BackColor       =   &H00C19B49&
         BackStyle       =   0  'Transparent
         Caption         =   "Severity"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   2400
         TabIndex        =   60
         Top             =   240
         Width           =   1860
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C19B49&
         BackStyle       =   0  'Transparent
         Caption         =   "Reactions"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   120
         TabIndex        =   59
         Top             =   240
         Width           =   1860
         WordWrap        =   -1  'True
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBye 
      Height          =   735
      Left            =   2520
      TabIndex        =   51
      Top             =   8260
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":5A21
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3a 
      Height          =   615
      Left            =   6960
      TabIndex        =   23
      Top             =   360
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":5C59
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2a 
      Height          =   615
      Left            =   4200
      TabIndex        =   14
      Top             =   360
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":5E8B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1e 
      Height          =   615
      Left            =   120
      TabIndex        =   9
      Top             =   3240
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":60BD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1d 
      Height          =   615
      Left            =   120
      TabIndex        =   8
      Top             =   2520
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":62EF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1c 
      Height          =   615
      Left            =   120
      TabIndex        =   7
      Top             =   1800
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":6521
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1b 
      Height          =   615
      Left            =   120
      TabIndex        =   6
      Top             =   1080
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":6753
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   735
      Left            =   10320
      TabIndex        =   0
      Top             =   8260
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":6985
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNo 
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   8260
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":6BB8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1a 
      Height          =   615
      Left            =   120
      TabIndex        =   5
      Top             =   360
      Visible         =   0   'False
      Width           =   4035
      _Version        =   131072
      _ExtentX        =   7117
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":6DEB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1f 
      Height          =   615
      Left            =   120
      TabIndex        =   10
      Top             =   3960
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":701D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1g 
      Height          =   615
      Left            =   120
      TabIndex        =   11
      Top             =   4680
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":724F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1h 
      Height          =   615
      Left            =   120
      TabIndex        =   12
      Top             =   5400
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":7481
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1i 
      Height          =   615
      Left            =   120
      TabIndex        =   13
      Top             =   6120
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":76B3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2b 
      Height          =   615
      Left            =   4200
      TabIndex        =   15
      Top             =   1080
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":78E5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2c 
      Height          =   615
      Left            =   4200
      TabIndex        =   16
      Top             =   1800
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":7B17
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2d 
      Height          =   615
      Left            =   4200
      TabIndex        =   17
      Top             =   2520
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":7D49
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2e 
      Height          =   615
      Left            =   4200
      TabIndex        =   18
      Top             =   3240
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":7F7B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2f 
      Height          =   615
      Left            =   4200
      TabIndex        =   19
      Top             =   3960
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":81AD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2g 
      Height          =   615
      Left            =   4200
      TabIndex        =   20
      Top             =   4680
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":83DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2h 
      Height          =   615
      Left            =   4200
      TabIndex        =   21
      Top             =   5400
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":8611
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2i 
      Height          =   615
      Left            =   4200
      TabIndex        =   22
      Top             =   6120
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":8843
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3b 
      Height          =   615
      Left            =   6960
      TabIndex        =   24
      Top             =   1080
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":8A75
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3c 
      Height          =   615
      Left            =   6960
      TabIndex        =   25
      Top             =   1800
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":8CA7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3d 
      Height          =   615
      Left            =   6960
      TabIndex        =   26
      Top             =   2520
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":8ED9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3e 
      Height          =   615
      Left            =   6960
      TabIndex        =   27
      Top             =   3240
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":910B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3f 
      Height          =   615
      Left            =   6960
      TabIndex        =   28
      Top             =   3960
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":933D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3g 
      Height          =   615
      Left            =   6960
      TabIndex        =   29
      Top             =   4680
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":956F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3h 
      Height          =   615
      Left            =   6960
      TabIndex        =   30
      Top             =   5400
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":97A1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3i 
      Height          =   615
      Left            =   6960
      TabIndex        =   31
      Top             =   6120
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":99D3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2j 
      Height          =   615
      Left            =   4200
      TabIndex        =   32
      Top             =   6840
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":9C05
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2k 
      Height          =   615
      Left            =   4200
      TabIndex        =   33
      Top             =   7560
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":9E37
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel2l 
      Height          =   615
      Left            =   4200
      TabIndex        =   34
      Top             =   8280
      Visible         =   0   'False
      Width           =   2685
      _Version        =   131072
      _ExtentX        =   4736
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":A069
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3j 
      Height          =   615
      Left            =   6960
      TabIndex        =   35
      Top             =   6840
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":A29B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3k 
      Height          =   615
      Left            =   6960
      TabIndex        =   36
      Top             =   7560
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":A4CD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel3l 
      Height          =   615
      Left            =   6960
      TabIndex        =   37
      Top             =   8280
      Visible         =   0   'False
      Width           =   2565
      _Version        =   131072
      _ExtentX        =   4524
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":A6FF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4a 
      Height          =   615
      Left            =   9600
      TabIndex        =   39
      Top             =   360
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":A931
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4b 
      Height          =   615
      Left            =   9600
      TabIndex        =   40
      Top             =   1080
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":AB63
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4c 
      Height          =   615
      Left            =   9600
      TabIndex        =   41
      Top             =   1800
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":AD95
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4d 
      Height          =   615
      Left            =   9600
      TabIndex        =   42
      Top             =   2520
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":AFC7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdlevel4e 
      Height          =   615
      Left            =   9600
      TabIndex        =   43
      Top             =   3240
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":B1F9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4f 
      Height          =   615
      Left            =   9600
      TabIndex        =   44
      Top             =   3960
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":B42B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4g 
      Height          =   615
      Left            =   9600
      TabIndex        =   45
      Top             =   4680
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":B65D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4h 
      Height          =   615
      Left            =   9600
      TabIndex        =   46
      Top             =   5400
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":B88F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4i 
      Height          =   615
      Left            =   9600
      TabIndex        =   47
      Top             =   6120
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":BAC1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4j 
      Height          =   615
      Left            =   9600
      TabIndex        =   48
      Top             =   6840
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":BCF3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel4k 
      Height          =   615
      Left            =   9600
      TabIndex        =   49
      Top             =   7560
      Visible         =   0   'False
      Width           =   2325
      _Version        =   131072
      _ExtentX        =   4101
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":BF25
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1j 
      Height          =   615
      Left            =   120
      TabIndex        =   52
      Top             =   6840
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":C157
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLevel1k 
      Height          =   615
      Left            =   120
      TabIndex        =   53
      Top             =   7560
      Visible         =   0   'False
      Width           =   4005
      _Version        =   131072
      _ExtentX        =   7064
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EditPI.frx":C389
   End
   Begin VB.Label lblBox 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Box"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   3720
      TabIndex        =   50
      Top             =   120
      Width           =   360
   End
   Begin VB.Label lblLevel4 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Form Question"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   9600
      TabIndex        =   38
      Top             =   120
      Visible         =   0   'False
      Width           =   2295
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblLevel3 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Form Question"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   6960
      TabIndex        =   4
      Top             =   120
      Visible         =   0   'False
      Width           =   2535
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblLevel2 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Form Question"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   4200
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   2655
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblLevel1 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Form Question"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   3540
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmEditPI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Option Explicit
Public FormOn As Boolean
Public StoreResults As String
Public UpdateType As String
Public ItemToReplace As String
Public Question As String
Public QuestionFamily As Long
Public BoxName As String
Public StoreresultOrgValidate As String

Public AReaction  As String
Public ASeverity  As String
Public IsLvlTwSlctd As Boolean
Public IsLvlOneSlctd As Boolean
Private Type References
    ElementOn As Boolean
    ElementPosted As Boolean
    FormId As Long
    ControlId As Long
    ControlName As String
    ControlNest As String
    ControlLingo As String
    Question As String
    DateTrigger As String
    DateTriggerOn As Boolean
    DrugOn As Boolean
    DrugsAlias(4) As String
End Type

Private SelectionLevel1(20) As References
Private SelectionLevel2(20) As References
Private SelectionLevel3(20) As References
Private SelectionLevel4(20) As References
Private SelectionLingo(20) As String

Private Const FSlash As String = "/"
Public AllBtnName As String
Private RefToField As String
Private SetDate As String
Private StoreResultsOrg As String
Private TheLink As String
Private QuestionId(4) As Integer
Private MaxQuestions(4) As Integer
Private LevelAt As Integer
Private ReloadForm As Boolean
Private TriggerOnDone(4) As Boolean
Private CurrentFormId As Long
Private CurrentSecondFormId As Long
Private CurrentThirdFormId As Long
Private MaxLevel1 As Integer
Private MaxLevel2 As Integer
Private MaxLevel3 As Integer
Private MaxLevel4 As Integer

Private BaseBackColor As Long
Private BaseForeColor As Long
Private BackSelectionColor As Long
Private TextSelectionColor As Long
Private TheForm As DynamicForms
Private TheControl As DynamicControls
Private TheSecondaryForm As DynamicSecondaryForms
Private TheSecondaryControl As DynamicSecondaryControls
Private TheThirdForm As DynamicThirdForms
Private TheThirdControl As DynamicThirdControls
Private TheFourthForm As DynamicFourthForms
Private TheFourthControl As DynamicFourthControls

Private Sub cmdApply_Click()
If (LevelAt = 1) Then
    If (Trim(StoreResultsOrg) <> "") Then
        If (FM.IsFileThere(StoreResultsOrg)) Then
            If (UpdateType = "A") Then
                Call MergeFiles(StoreResultsOrg, StoreResults)
            ElseIf (UpdateType = "R") Then
                Call ReplaceFiles(StoreResultsOrg, StoreResults, ItemToReplace)
            End If
            If (FM.IsFileThere(StoreResults)) Then
                FM.Kill StoreResultsOrg
            Else
                FM.Kill StoreResults
                FM.Name StoreResultsOrg, StoreResults
            End If
        End If
    End If
    Unload frmEditPI
    FormOn = False
ElseIf (LevelAt = 2) Then
    If (MaxQuestions(2) > 1) And (QuestionId(2) < MaxQuestions(2)) Then
        LevelAt = 2
        Call ResetLevelText(2)
        Call TurnOffOthers(3)
        If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(2))) Then
            Call TurnOn(2)
        End If
    Else
        LevelAt = 1
        ' write the code here to stop hiding the level 2 of P66C
        If (InStrPS(StoreResults, "P69A") <> 0) Then
            '
        Else
        Call TurnOffOthers(1)
        End If
    End If
ElseIf (LevelAt = 3) Then
    If (MaxQuestions(3) > 1) And (QuestionId(3) < MaxQuestions(3)) Then
        LevelAt = 3
        Call ResetLevelText(3)
        Call TurnOffOthers(4)
        If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(3))) Then
            Call TurnOn(3)
        End If
    Else
        LevelAt = 2
        If (MaxQuestions(2) > 1) And (QuestionId(2) < MaxQuestions(2)) Then
            Call ResetLevelText(2)
            Call TurnOffOthers(3)
            If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(2))) Then
                Call TurnOn(2)
            End If
        Else
            LevelAt = 1
            Call TurnOffOthers(1)
        End If
    End If
ElseIf (LevelAt = 4) Then
    If (MaxQuestions(4) > 1) And (QuestionId(4) < MaxQuestions(4)) Then
        LevelAt = 4
        Call ResetLevelText(4)
        If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(4))) Then
            Call TurnOn(4)
        End If
    Else
        LevelAt = 3
        If (MaxQuestions(3) > 1) And (QuestionId(3) < MaxQuestions(3)) Then
            Call ResetLevelText(3)
            Call TurnOffOthers(4)
            If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(3))) Then
                Call TurnOn(3)
            End If
        Else
            LevelAt = 2
            If (MaxQuestions(2) > 1) And (QuestionId(2) < MaxQuestions(2)) Then
                Call ResetLevelText(2)
                Call TurnOffOthers(3)
                If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(2))) Then
                    Call TurnOn(2)
                End If
            Else
                LevelAt = 1
                Call TurnOffOthers(1)
            End If
        End If
    End If
End If
End Sub

Private Sub cmdBye_Click()
If (Trim(StoreResultsOrg) <> "") Then
    If (FM.IsFileThere(StoreResultsOrg)) Then
        If (FM.IsFileThere(StoreResults)) Then
            FM.Kill StoreResultsOrg
        Else
            FM.Kill StoreResults
            FM.Name StoreResultsOrg, StoreResults
        End If
    End If
End If
Unload frmEditPI
FormOn = False
End Sub

Private Sub cmdNo_Click()
If (FM.IsFileThere(StoreResults)) Then
    FM.Kill StoreResults
End If
If (Trim(StoreResultsOrg) <> "") Then
        If (InStrPS(StoreResultsOrg, "P66A") <> 0 Or InStrPS(StoreResultsOrg, "P69A")) Then
            If (FM.IsFileThere(StoreresultOrgValidate)) And (Not (FM.IsFileThere(StoreResults))) Then
                FM.Name StoreresultOrgValidate, StoreResults
        End If
        Else
            If (FM.IsFileThere(StoreResultsOrg)) And (Not (FM.IsFileThere(StoreResults))) Then
                FM.Name StoreResultsOrg, StoreResults
        End If
        End If
    End If
Unload frmEditPI
FormOn = False
End Sub

Private Sub TurnEnableOn(ALevel As Integer)
If (ALevel = 1) Then
    cmdLevel1a.Enabled = True
    cmdLevel1b.Enabled = True
    cmdLevel1c.Enabled = True
    cmdLevel1d.Enabled = True
    cmdLevel1e.Enabled = True
    cmdLevel1f.Enabled = True
    cmdLevel1g.Enabled = True
    cmdLevel1h.Enabled = True
    cmdLevel1i.Enabled = True
    cmdLevel1j.Enabled = True
    cmdLevel1k.Enabled = True
    cmdLevel4a.Enabled = True
    cmdLevel4b.Enabled = True
    cmdLevel4c.Enabled = True
    cmdLevel4d.Enabled = True
    cmdlevel4e.Enabled = True
    cmdLevel4f.Enabled = True
    cmdLevel4g.Enabled = True
    cmdLevel4h.Enabled = True
    cmdLevel4i.Enabled = True
    cmdLevel4j.Enabled = True
    cmdLevel4k.Enabled = True
ElseIf (ALevel = 2) Then
    cmdLevel2a.Enabled = True
    cmdLevel2b.Enabled = True
    cmdLevel2c.Enabled = True
    cmdLevel2d.Enabled = True
    cmdLevel2e.Enabled = True
    cmdLevel2f.Enabled = True
    cmdLevel2g.Enabled = True
    cmdLevel2h.Enabled = True
    cmdLevel2i.Enabled = True
    cmdLevel2j.Enabled = True
    cmdLevel2k.Enabled = True
    cmdLevel2l.Enabled = True
ElseIf (ALevel = 3) Then
    cmdLevel3a.Enabled = True
    cmdLevel3b.Enabled = True
    cmdLevel3c.Enabled = True
    cmdLevel3d.Enabled = True
    cmdLevel3e.Enabled = True
    cmdLevel3f.Enabled = True
    cmdLevel3g.Enabled = True
    cmdLevel3h.Enabled = True
    cmdLevel3i.Enabled = True
    cmdLevel3j.Enabled = True
    cmdLevel3k.Enabled = True
    cmdLevel3l.Enabled = True
ElseIf (ALevel = 4) Then
    cmdLevel4a.Enabled = True
    cmdLevel4b.Enabled = True
    cmdLevel4c.Enabled = True
    cmdLevel4d.Enabled = True
    cmdlevel4e.Enabled = True
    cmdLevel4f.Enabled = True
    cmdLevel4g.Enabled = True
    cmdLevel4h.Enabled = True
    cmdLevel4i.Enabled = True
    cmdLevel4j.Enabled = True
    cmdLevel4k.Enabled = True
End If
End Sub

Private Sub TurnOn(ALevel As Integer)
If (ALevel = 1) Then
    If (Left(cmdLevel1a.Text, 3) <> "Lev") Then
        cmdLevel1a.Visible = True
    End If
    If (Left(cmdLevel1b.Text, 3) <> "Lev") Then
        cmdLevel1b.Visible = True
    End If
    If (Left(cmdLevel1c.Text, 3) <> "Lev") Then
        cmdLevel1c.Visible = True
    End If
    If (Left(cmdLevel1d.Text, 3) <> "Lev") Then
        cmdLevel1d.Visible = True
    End If
    If (Left(cmdLevel1e.Text, 3) <> "Lev") Then
        cmdLevel1e.Visible = True
    End If
    If (Left(cmdLevel1f.Text, 3) <> "Lev") Then
        cmdLevel1f.Visible = True
    End If
    If (Left(cmdLevel1g.Text, 3) <> "Lev") Then
        cmdLevel1g.Visible = True
    End If
    If (Left(cmdLevel1h.Text, 3) <> "Lev") Then
        cmdLevel1h.Visible = True
    End If
    If (Left(cmdLevel1i.Text, 3) <> "Lev") Then
        cmdLevel1i.Visible = True
    End If
    If (Left(cmdLevel1j.Text, 3) <> "Lev") Then
        cmdLevel1j.Visible = True
    End If
    If (Left(cmdLevel1k.Text, 3) <> "Lev") Then
        cmdLevel1k.Visible = True
    End If
    Call TurnEnableOn(1)
ElseIf (ALevel = 2) Then
    If (Left(cmdLevel2a.Text, 3) <> "Lev") Then
        cmdLevel2a.Visible = True
    End If
    If (Left(cmdLevel2b.Text, 3) <> "Lev") Then
        cmdLevel2b.Visible = True
    End If
    If (Left(cmdLevel2c.Text, 3) <> "Lev") Then
        cmdLevel2c.Visible = True
    End If
     If (Left(cmdLevel2d.Text, 3) <> "Lev") Then
       cmdLevel2d.Visible = True
    End If
    If (Left(cmdLevel2e.Text, 3) <> "Lev") Then
        cmdLevel2e.Visible = True
    End If
    If (Left(cmdLevel2f.Text, 3) <> "Lev") Then
        cmdLevel2f.Visible = True
    End If
    If (Left(cmdLevel2g.Text, 3) <> "Lev") Then
        cmdLevel2g.Visible = True
    End If
    If (Left(cmdLevel2h.Text, 3) <> "Lev") Then
        cmdLevel2h.Visible = True
    End If
    If (Left(cmdLevel2i.Text, 3) <> "Lev") Then
        cmdLevel2i.Visible = True
    End If
    If (Left(cmdLevel2j.Text, 3) <> "Lev") Then
        cmdLevel2j.Visible = True
    End If
    If (Left(cmdLevel2k.Text, 3) <> "Lev") Then
        cmdLevel2k.Visible = True
    End If
    If (Left(cmdLevel2l.Text, 3) <> "Lev") Then
        cmdLevel2l.Visible = True
    End If
    Call TurnEnableOn(2)
ElseIf (ALevel = 3) Then
    If (Left(cmdLevel3a.Text, 3) <> "Lev") Then
        cmdLevel3a.Visible = True
    End If
    If (Left(cmdLevel3b.Text, 3) <> "Lev") Then
        cmdLevel3b.Visible = True
    End If
    If (Left(cmdLevel3c.Text, 3) <> "Lev") Then
        cmdLevel3c.Visible = True
    End If
    If (Left(cmdLevel3d.Text, 3) <> "Lev") Then
        cmdLevel3d.Visible = True
    End If
    If (Left(cmdLevel3e.Text, 3) <> "Lev") Then
        cmdLevel3e.Visible = True
    End If
    If (Left(cmdLevel3f.Text, 3) <> "Lev") Then
        cmdLevel3f.Visible = True
    End If
    If (Left(cmdLevel3g.Text, 3) <> "Lev") Then
        cmdLevel3g.Visible = True
    End If
    If (Left(cmdLevel3h.Text, 3) <> "Lev") Then
        cmdLevel3h.Visible = True
    End If
    If (Left(cmdLevel3i.Text, 3) <> "Lev") Then
        cmdLevel3i.Visible = True
    End If
    If (Left(cmdLevel3j.Text, 3) <> "Lev") Then
        cmdLevel3j.Visible = True
    End If
    If (Left(cmdLevel3k.Text, 3) <> "Lev") Then
        cmdLevel3k.Visible = True
    End If
    If (Left(cmdLevel3l.Text, 3) <> "Lev") Then
        cmdLevel3l.Visible = True
    End If
    Call TurnEnableOn(3)
ElseIf (ALevel = 4) Then
    If (Left(cmdLevel4a.Text, 3) <> "Lev") Then
        cmdLevel4a.Visible = True
    End If
    If (Left(cmdLevel4b.Text, 3) <> "Lev") Then
        cmdLevel4b.Visible = True
    End If
    If (Left(cmdLevel4c.Text, 3) <> "Lev") Then
        cmdLevel4c.Visible = True
    End If
    If (Left(cmdLevel4d.Text, 3) <> "Lev") Then
        cmdLevel4d.Visible = True
    End If
    If (Left(cmdlevel4e.Text, 3) <> "Lev") Then
        cmdlevel4e.Visible = True
    End If
    If (Left(cmdLevel4f.Text, 3) <> "Lev") Then
        cmdLevel4f.Visible = True
    End If
    If (Left(cmdLevel4g.Text, 3) <> "Lev") Then
        cmdLevel4g.Visible = True
    End If
    If (Left(cmdLevel4h.Text, 3) <> "Lev") Then
        cmdLevel4h.Visible = True
    End If
    If (Left(cmdLevel4i.Text, 3) <> "Lev") Then
        cmdLevel4i.Visible = True
    End If
    If (Left(cmdLevel4j.Text, 3) <> "Lev") Then
        cmdLevel4j.Visible = True
    End If
    If (Left(cmdLevel4k.Text, 3) <> "Lev") Then
        cmdLevel4k.Visible = True
    End If
    Call TurnEnableOn(4)
End If
End Sub

Private Sub ResetLevelText(ALevel As Integer)
If (ALevel = 2) Then
    cmdLevel2a.BackColor = BaseBackColor
    cmdLevel2a.ForeColor = BaseForeColor
    cmdLevel2a.Text = "Level 2"
    cmdLevel2b.BackColor = BaseBackColor
    cmdLevel2b.ForeColor = BaseForeColor
    cmdLevel2b.Text = "Level 2"
    cmdLevel2c.BackColor = BaseBackColor
    cmdLevel2c.ForeColor = BaseForeColor
    cmdLevel2c.Text = "Level 2"
    cmdLevel2d.BackColor = BaseBackColor
    cmdLevel2d.ForeColor = BaseForeColor
    cmdLevel2d.Text = "Level 2"
    cmdLevel2e.BackColor = BaseBackColor
    cmdLevel2e.ForeColor = BaseForeColor
    cmdLevel2e.Text = "Level 2"
    cmdLevel2f.BackColor = BaseBackColor
    cmdLevel2f.ForeColor = BaseForeColor
    cmdLevel2f.Text = "Level 2"
    cmdLevel2g.BackColor = BaseBackColor
    cmdLevel2g.ForeColor = BaseForeColor
    cmdLevel2g.Text = "Level 2"
    cmdLevel2h.BackColor = BaseBackColor
    cmdLevel2h.ForeColor = BaseForeColor
    cmdLevel2h.Text = "Level 2"
    cmdLevel2i.BackColor = BaseBackColor
    cmdLevel2i.ForeColor = BaseForeColor
    cmdLevel2i.Text = "Level 2"
    cmdLevel2j.BackColor = BaseBackColor
    cmdLevel2j.ForeColor = BaseForeColor
    cmdLevel2j.Text = "Level 2"
    cmdLevel2k.BackColor = BaseBackColor
    cmdLevel2k.ForeColor = BaseForeColor
    cmdLevel2k.Text = "Level 2"
    cmdLevel2l.BackColor = BaseBackColor
    cmdLevel2l.ForeColor = BaseForeColor
    cmdLevel2l.Text = "Level 2"
ElseIf (ALevel = 3) Then
    cmdLevel3a.BackColor = BaseBackColor
    cmdLevel3a.ForeColor = BaseForeColor
    cmdLevel3a.Text = "Level 3"
    cmdLevel3b.BackColor = BaseBackColor
    cmdLevel3b.ForeColor = BaseForeColor
    cmdLevel3b.Text = "Level 3"
    cmdLevel3c.BackColor = BaseBackColor
    cmdLevel3c.ForeColor = BaseForeColor
    cmdLevel3c.Text = "Level 3"
    cmdLevel3d.BackColor = BaseBackColor
    cmdLevel3d.ForeColor = BaseForeColor
    cmdLevel3d.Text = "Level 3"
    cmdLevel3e.BackColor = BaseBackColor
    cmdLevel3e.ForeColor = BaseForeColor
    cmdLevel3e.Text = "Level 3"
    cmdLevel3f.BackColor = BaseBackColor
    cmdLevel3f.ForeColor = BaseForeColor
    cmdLevel3f.Text = "Level 3"
    cmdLevel3g.BackColor = BaseBackColor
    cmdLevel3g.ForeColor = BaseForeColor
    cmdLevel3g.Text = "Level 3"
    cmdLevel3h.BackColor = BaseBackColor
    cmdLevel3h.ForeColor = BaseForeColor
    cmdLevel3h.Text = "Level 3"
    cmdLevel3i.BackColor = BaseBackColor
    cmdLevel3i.ForeColor = BaseForeColor
    cmdLevel3i.Text = "Level 3"
    cmdLevel3j.BackColor = BaseBackColor
    cmdLevel3j.ForeColor = BaseForeColor
    cmdLevel3j.Text = "Level 3"
    cmdLevel3k.BackColor = BaseBackColor
    cmdLevel3k.ForeColor = BaseForeColor
    cmdLevel3k.Text = "Level 3"
    cmdLevel3l.BackColor = BaseBackColor
    cmdLevel3l.ForeColor = BaseForeColor
    cmdLevel3l.Text = "Level 3"
ElseIf (ALevel = 4) Then
    cmdLevel4a.BackColor = BaseBackColor
    cmdLevel4a.ForeColor = BaseForeColor
    cmdLevel4a.Text = "Level 4"
    cmdLevel4b.BackColor = BaseBackColor
    cmdLevel4b.ForeColor = BaseForeColor
    cmdLevel4b.Text = "Level 4"
    cmdLevel4c.BackColor = BaseBackColor
    cmdLevel4c.ForeColor = BaseForeColor
    cmdLevel4c.Text = "Level 4"
    cmdLevel4d.BackColor = BaseBackColor
    cmdLevel4d.ForeColor = BaseForeColor
    cmdLevel4d.Text = "Level 4"
    cmdlevel4e.BackColor = BaseBackColor
    cmdlevel4e.ForeColor = BaseForeColor
    cmdlevel4e.Text = "Level 4"
    cmdLevel4f.BackColor = BaseBackColor
    cmdLevel4f.ForeColor = BaseForeColor
    cmdLevel4f.Text = "Level 4"
    cmdLevel4g.BackColor = BaseBackColor
    cmdLevel4g.ForeColor = BaseForeColor
    cmdLevel4g.Text = "Level 4"
    cmdLevel4h.BackColor = BaseBackColor
    cmdLevel4h.ForeColor = BaseForeColor
    cmdLevel4h.Text = "Level 4"
    cmdLevel4i.BackColor = BaseBackColor
    cmdLevel4i.ForeColor = BaseForeColor
    cmdLevel4i.Text = "Level 4"
    cmdLevel4j.BackColor = BaseBackColor
    cmdLevel4j.ForeColor = BaseForeColor
    cmdLevel4j.Text = "Level 4"
    cmdLevel4k.BackColor = BaseBackColor
    cmdLevel4k.ForeColor = BaseForeColor
    cmdLevel4k.Text = "Level 4"
End If
End Sub

Private Sub TurnOffOthers(ALevel As Integer)
If (ALevel = 1) Then
    lblLevel2.Visible = False
    lblLevel3.Visible = False
    lblLevel4.Visible = False
    cmdLevel2a.Visible = False
    cmdLevel2b.Visible = False
    cmdLevel2c.Visible = False
    cmdLevel2d.Visible = False
    cmdLevel2e.Visible = False
    cmdLevel2f.Visible = False
    cmdLevel2g.Visible = False
    cmdLevel2h.Visible = False
    cmdLevel2i.Visible = False
    cmdLevel2j.Visible = False
    cmdLevel2k.Visible = False
    cmdLevel2l.Visible = False
    cmdLevel3a.Visible = False
    cmdLevel3b.Visible = False
    cmdLevel3c.Visible = False
    cmdLevel3d.Visible = False
    cmdLevel3e.Visible = False
    cmdLevel3f.Visible = False
    cmdLevel3g.Visible = False
    cmdLevel3h.Visible = False
    cmdLevel3i.Visible = False
    cmdLevel3j.Visible = False
    cmdLevel3k.Visible = False
    cmdLevel3l.Visible = False
    cmdLevel4a.Visible = False
    cmdLevel4b.Visible = False
    cmdLevel4c.Visible = False
    cmdLevel4d.Visible = False
    cmdlevel4e.Visible = False
    cmdLevel4f.Visible = False
    cmdLevel4g.Visible = False
    cmdLevel4h.Visible = False
    cmdLevel4i.Visible = False
    cmdLevel4j.Visible = False
    cmdLevel4k.Visible = False
ElseIf (ALevel = 2) Then
    cmdLevel1a.Visible = False
    cmdLevel1b.Visible = False
    cmdLevel1c.Visible = False
    cmdLevel1d.Visible = False
    cmdLevel1e.Visible = False
    cmdLevel1f.Visible = False
    cmdLevel1g.Visible = False
    cmdLevel1h.Visible = False
    cmdLevel1i.Visible = False
    cmdLevel1j.Visible = False
    cmdLevel1k.Visible = False
    cmdLevel3a.Visible = False
    cmdLevel3b.Visible = False
    cmdLevel3c.Visible = False
    cmdLevel3d.Visible = False
    cmdLevel3e.Visible = False
    cmdLevel3f.Visible = False
    cmdLevel3g.Visible = False
    cmdLevel3h.Visible = False
    cmdLevel3i.Visible = False
    cmdLevel3j.Visible = False
    cmdLevel3k.Visible = False
    cmdLevel3l.Visible = False
    cmdLevel4a.Visible = False
    cmdLevel4b.Visible = False
    cmdLevel4c.Visible = False
    cmdLevel4d.Visible = False
    cmdlevel4e.Visible = False
    cmdLevel4f.Visible = False
    cmdLevel4g.Visible = False
    cmdLevel4h.Visible = False
    cmdLevel4i.Visible = False
    cmdLevel4j.Visible = False
    cmdLevel4k.Visible = False
    lblLevel3.Visible = False
    lblLevel4.Visible = False
ElseIf (ALevel = 3) Then
    cmdLevel2a.Visible = False
    cmdLevel2b.Visible = False
    cmdLevel2c.Visible = False
    cmdLevel2d.Visible = False
    cmdLevel2e.Visible = False
    cmdLevel2f.Visible = False
    cmdLevel2g.Visible = False
    cmdLevel2h.Visible = False
    cmdLevel2i.Visible = False
    cmdLevel2j.Visible = False
    cmdLevel2k.Visible = False
    cmdLevel2l.Visible = False
    cmdLevel4a.Visible = False
    cmdLevel4b.Visible = False
    cmdLevel4c.Visible = False
    cmdLevel4d.Visible = False
    cmdlevel4e.Visible = False
    cmdLevel4f.Visible = False
    cmdLevel4g.Visible = False
    cmdLevel4h.Visible = False
    cmdLevel4i.Visible = False
    cmdLevel4j.Visible = False
    cmdLevel4k.Visible = False
    lblLevel4.Visible = False
ElseIf (ALevel = 4) Then
    cmdLevel3a.Visible = False
    cmdLevel3b.Visible = False
    cmdLevel3c.Visible = False
    cmdLevel3d.Visible = False
    cmdLevel3e.Visible = False
    cmdLevel3f.Visible = False
    cmdLevel3g.Visible = False
    cmdLevel3h.Visible = False
    cmdLevel3i.Visible = False
    cmdLevel3j.Visible = False
    cmdLevel3k.Visible = False
    cmdLevel3l.Visible = False
End If
End Sub

Private Sub TurnOff(ALevel As Integer)
If (ALevel = 1) Then
    Call ResetLevelText(1)
    lblLevel1.Visible = False
    cmdLevel1a.Visible = False
    cmdLevel1b.Visible = False
    cmdLevel1c.Visible = False
    cmdLevel1d.Visible = False
    cmdLevel1e.Visible = False
    cmdLevel1f.Visible = False
    cmdLevel1g.Visible = False
    cmdLevel1h.Visible = False
    cmdLevel1i.Visible = False
    cmdLevel1j.Visible = False
    cmdLevel1k.Visible = False
ElseIf (ALevel = 2) Then
    Call ResetLevelText(2)
    lblLevel2.Visible = False
    cmdLevel2a.Visible = False
    cmdLevel2b.Visible = False
    cmdLevel2c.Visible = False
    cmdLevel2d.Visible = False
    cmdLevel2e.Visible = False
    cmdLevel2f.Visible = False
    cmdLevel2g.Visible = False
    cmdLevel2h.Visible = False
    cmdLevel2i.Visible = False
    cmdLevel2j.Visible = False
    cmdLevel2k.Visible = False
    cmdLevel2l.Visible = False
ElseIf (ALevel = 3) Then
    Call ResetLevelText(3)
    lblLevel3.Visible = False
    cmdLevel3a.Visible = False
    cmdLevel3b.Visible = False
    cmdLevel3c.Visible = False
    cmdLevel3d.Visible = False
    cmdLevel3e.Visible = False
    cmdLevel3f.Visible = False
    cmdLevel3g.Visible = False
    cmdLevel3h.Visible = False
    cmdLevel3i.Visible = False
    cmdLevel3j.Visible = False
    cmdLevel3k.Visible = False
    cmdLevel3l.Visible = False
ElseIf (ALevel = 4) Then
    Call ResetLevelText(4)
    lblLevel4.Visible = False
    cmdLevel4a.Visible = False
    cmdLevel4b.Visible = False
    cmdLevel4c.Visible = False
    cmdLevel4d.Visible = False
    cmdlevel4e.Visible = False
    cmdLevel4f.Visible = False
    cmdLevel4g.Visible = False
    cmdLevel4h.Visible = False
    cmdLevel4i.Visible = False
    cmdLevel4j.Visible = False
    cmdLevel4k.Visible = False
End If
End Sub

Private Sub TriggerNextLevelChoices(ALevel As Integer, TheFormId As Long, TheQuestion As String, TrigId As Integer, ALink As String)
Dim i As Integer
If (ALevel = 2) Then
    MaxLevel2 = 0
    Erase SelectionLevel2
    Call TurnOff(2)
    Call TurnOff(3)
    Call TurnOff(4)
    Set TheSecondaryControl = New DynamicSecondaryControls
    TheSecondaryControl.Language = ""
    TheSecondaryControl.SecondaryFormId = TheFormId
    If (TheSecondaryControl.FindControl > 0) Then
        i = 1
        Do Until Not (TheSecondaryControl.SelectControl(i))
            If (TheSecondaryControl.ControlType = "B") Then
                MaxLevel2 = MaxLevel2 + 1
                Call PlantSelection2(TheSecondaryControl, MaxLevel2, 2, TheQuestion, TrigId, ALink)
            End If
            i = i + 1
        Loop
    End If
    Set TheSecondaryControl = Nothing
ElseIf (ALevel = 3) Then
    MaxLevel3 = 0
    Erase SelectionLevel3
    Call TurnOff(3)
    Call TurnOff(4)
    Set TheThirdControl = New DynamicThirdControls
    TheThirdControl.Language = ""
    TheThirdControl.ThirdFormId = TheFormId
    If (TheThirdControl.FindControl > 0) Then
        i = 1
        Do Until Not (TheThirdControl.SelectControl(i))
            If (TheThirdControl.ControlType = "B") Then
                MaxLevel3 = MaxLevel3 + 1
                Call PlantSelection3(TheThirdControl, MaxLevel3, 3, TheQuestion, TrigId, ALink)
            End If
            i = i + 1
        Loop
    End If
    Set TheThirdControl = Nothing
ElseIf (ALevel = 4) Then
    MaxLevel4 = 0
    Erase SelectionLevel4
    Call TurnOff(4)
    Set TheFourthControl = New DynamicFourthControls
    TheFourthControl.Language = ""
    TheFourthControl.FourthFormId = TheFormId
    If (TheFourthControl.FindControl > 0) Then
        i = 1
        Do Until Not (TheFourthControl.SelectControl(i))
            If (TheFourthControl.ControlType = "B") Then
                MaxLevel4 = MaxLevel4 + 1
                Call PlantSelection4(TheFourthControl, MaxLevel4, 4, TheQuestion, TrigId, ALink)
            End If
            i = i + 1
        Loop
    End If
    Set TheFourthControl = Nothing
End If
End Sub

Private Sub TriggerLevel(ALevel As Integer, Acmd As fpBtn, AQuestion As Integer, TheTrigger As Boolean, TrigId As Integer)
Dim k As Integer
Dim Temp As String
Dim CtlName As String
TheTrigger = False
If (ALevel = 2) Then
    CurrentSecondFormId = 0
    If (Len(Acmd.ToolTipText) > 0) Then
        k = InStrPS(Acmd.ToolTipText, "-")
        If (k > 0) Then
            TriggerOnDone(2) = False
            Temp = Left(Acmd.ToolTipText, k - 1)
            CtlName = Trim(Mid(Acmd.ToolTipText, k + 1, Len(Acmd.ToolTipText) - k))
            TheLink = CtlName
            Set TheSecondaryForm = New DynamicSecondaryForms
            TheSecondaryForm.FormId = CurrentFormId
            TheSecondaryForm.ControlName = CtlName
            TheSecondaryForm.QuestionOrder = ""
            If (TheSecondaryForm.RetrieveForm) Then
                k = AQuestion
                MaxQuestions(2) = TheSecondaryForm.ClinicalTotalForms
                Do Until Not (TheSecondaryForm.SelectForm(k))
                    TriggerOnDone(2) = True
                    'here showing the frame for noting reactions and severity
                    If (InStrPS(StoreResults, "P69A") <> 0) Then
                        Call EnableRctSvrtBt
                        FrmReactions.Visible = True
                        AReaction = "None"
                        ASeverity = "None"
                    End If
                    CurrentSecondFormId = TheSecondaryForm.FormId
                    Call TriggerNextLevelChoices(2, TheSecondaryForm.SecondaryFormId, TheSecondaryForm.Question, TrigId, TheLink)
                    Exit Do
                    k = k + 1
                Loop
            End If
            Set TheSecondaryForm = Nothing
            If (k > 0) Then
                TheTrigger = True
                Call TurnOn(ALevel - 1)
                Acmd.Visible = True
                Acmd.Enabled = False
            Else
                Call cmdApply_Click
            End If
        End If
    End If
ElseIf (ALevel = 3) Then
    CurrentThirdFormId = 0
    If (Len(Acmd.ToolTipText) > 0) Then
        k = InStrPS(Acmd.ToolTipText, "-")
        If (k > 0) Then
            TriggerOnDone(3) = False
            Temp = Left(Acmd.ToolTipText, k - 1)
            CtlName = Trim(Mid(Acmd.ToolTipText, 4, Len(Acmd.ToolTipText) - 3))
            k = 0
            Set TheThirdForm = New DynamicThirdForms
            TheThirdForm.FormId = CurrentFormId
            TheThirdForm.ControlName = Trim(CtlName)
            TheThirdForm.QuestionOrder = ""
            If (TheThirdForm.RetrieveForm) Then
                k = AQuestion
                MaxQuestions(3) = TheThirdForm.ClinicalTotalForms
                Do Until Not (TheThirdForm.SelectForm(k))
                    TriggerOnDone(3) = True
                    CurrentThirdFormId = TheThirdForm.FormId
                    Call TriggerNextLevelChoices(3, TheThirdForm.ThirdFormId, TheThirdForm.Question, TrigId, TheLink)
                    Exit Do
                    k = k + 1
                Loop
            End If
            Set TheThirdForm = Nothing
            If (k > 0) Then
                TheTrigger = True
                Call TurnOn(ALevel)
                Acmd.Visible = True
                Acmd.Enabled = False
            Else
                Call cmdApply_Click
            End If
        End If
    End If
ElseIf (ALevel = 4) Then
    If (Len(Acmd.ToolTipText) > 0) Then
        k = InStrPS(Acmd.ToolTipText, "-")
        If (k > 0) Then
            TriggerOnDone(4) = False
            Temp = Left(Acmd.ToolTipText, k - 1)
            CtlName = Trim(Mid(Acmd.ToolTipText, 4, Len(Acmd.ToolTipText) - 3))
            k = 0
            Set TheFourthForm = New DynamicFourthForms
            TheFourthForm.FormId = CurrentFormId
            TheFourthForm.ControlName = CtlName
            TheFourthForm.QuestionOrder = ""
            If (TheFourthForm.RetrieveForm) Then
                k = AQuestion
                Do Until Not (TheFourthForm.SelectForm(k))
                    TriggerOnDone(4) = True
                    Call TriggerNextLevelChoices(4, TheFourthForm.FourthFormId, TheFourthForm.Question, TrigId, TheLink)
                    Exit Do
                    k = k + 1
                Loop
            End If
            Set TheFourthForm = Nothing
            If (k > 0) Then
                TheTrigger = True
                Call TurnOffOthers(ALevel)
                Call TurnOn(ALevel - 1)
                Acmd.Visible = True
                Acmd.Enabled = False
            Else
                Call TurnOffOthers(ALevel)
                Call cmdApply_Click
            End If
        End If
    End If
End If
End Sub

Private Function PlantSelection1(AControl As DynamicControls, AIdx As Integer, ALevel As Integer, AQuestion As String) As Boolean
Dim Temp As String
Dim TempA As String
Dim Acmd As fpBtn
If (ALevel = 1) Then
    If (AIdx = 1) Then
        Set Acmd = cmdLevel1a
    ElseIf (AIdx = 2) Then
        Set Acmd = cmdLevel1b
    ElseIf (AIdx = 3) Then
        Set Acmd = cmdLevel1c
    ElseIf (AIdx = 4) Then
        Set Acmd = cmdLevel1d
    ElseIf (AIdx = 5) Then
        Set Acmd = cmdLevel1e
    ElseIf (AIdx = 6) Then
        Set Acmd = cmdLevel1f
    ElseIf (AIdx = 7) Then
        Set Acmd = cmdLevel1g
    ElseIf (AIdx = 8) Then
        Set Acmd = cmdLevel1h
    ElseIf (AIdx = 9) Then
        Set Acmd = cmdLevel1i
    ElseIf (AIdx = 10) Then
        Set Acmd = cmdLevel1j
    ElseIf (AIdx = 11) Then
        Set Acmd = cmdLevel1k
    Else
        Exit Function
    End If
    Temp = ""
    If (AIdx < 10) Then
        Temp = Temp + "0" + Chr(48 + AIdx)
    Else
        Temp = Temp + Trim(str(AIdx))
    End If
    TempA = Trim(AControl.DoctorLingo)
    Call ReplaceCharacters(TempA, "^", " ")
    Call ReplaceCharacters(TempA, "-", " ")
    If (Trim(TempA) = "") Then
        TempA = Trim(AControl.ControlText)
    End If
    Call ReplaceCharacters(TempA, "^", " ")
    Call ReplaceCharacters(TempA, "-", " ")
    TempA = Trim(TempA)
    Acmd.BackColor = BaseBackColor
    Acmd.ForeColor = BaseForeColor
    Acmd.Text = TempA
    Acmd.Tag = Trim(str(AControl.ControlId))
    Acmd.ToolTipText = Temp + "-" + Trim(AControl.ControlName)
    Acmd.Visible = True
    Acmd.Enabled = True
    SelectionLevel1(AIdx).ElementOn = False
    SelectionLevel1(AIdx).Question = AQuestion
    SelectionLevel1(AIdx).FormId = AControl.FormId
    SelectionLevel1(AIdx).ControlId = AControl.ControlId
    SelectionLevel1(AIdx).ControlName = AControl.ControlName
    SelectionLevel1(AIdx).ControlLingo = Trim(AControl.DoctorLingo)
    SelectionLevel1(AIdx).DrugOn = AControl.DrugQuestion
    SelectionLevel1(AIdx).DrugsAlias(1) = Trim(AControl.ICDAlias1)
    SelectionLevel1(AIdx).DrugsAlias(2) = Trim(AControl.ICDAlias2)
    SelectionLevel1(AIdx).DrugsAlias(3) = Trim(AControl.ICDAlias3)
    SelectionLevel1(AIdx).DrugsAlias(4) = Trim(AControl.ICDAlias4)
    If (AControl.ICDAlias3 = "D") Then
        SelectionLevel1(AIdx).DateTrigger = ""
        SelectionLevel1(AIdx).DateTriggerOn = True
    End If
End If
End Function

Private Function PlantSelection2(AControl As DynamicSecondaryControls, AIdx As Integer, ALevel As Integer, AQuestion As String, ALevelIdx As Integer, BLink As String) As Boolean
Dim i As Integer
Dim Temp As String
Dim TempA As String
Dim TheNest As String
Dim Acmd As fpBtn
If (ALevel = 2) Then
    If (AIdx = 1) Then
        Set Acmd = cmdLevel2a
    ElseIf (AIdx = 2) Then
        Set Acmd = cmdLevel2b
    ElseIf (AIdx = 3) Then
        Set Acmd = cmdLevel2c
    ElseIf (AIdx = 4) Then
        Set Acmd = cmdLevel2d
    ElseIf (AIdx = 5) Then
        Set Acmd = cmdLevel2e
    ElseIf (AIdx = 6) Then
        Set Acmd = cmdLevel2f
    ElseIf (AIdx = 7) Then
        Set Acmd = cmdLevel2g
    ElseIf (AIdx = 8) Then
        Set Acmd = cmdLevel2h
    ElseIf (AIdx = 9) Then
        Set Acmd = cmdLevel2i
    ElseIf (AIdx = 10) Then
        Set Acmd = cmdLevel2j
    ElseIf (AIdx = 11) Then
        Set Acmd = cmdLevel2k
    ElseIf (AIdx = 12) Then
        Set Acmd = cmdLevel2l
    Else
        Exit Function
    End If
    Temp = ""
    If (AIdx < 10) Then
        Temp = Temp + "0" + Chr(48 + AIdx)
    Else
        Temp = Temp + Trim(str(AIdx))
    End If
    TheNest = Trim(BLink)
    TempA = Trim(AControl.DoctorLingo)
    Call ReplaceCharacters(TempA, "^", " ")
    Call ReplaceCharacters(TempA, "-", " ")
    If (Trim(TempA) = "") Then
        TempA = Trim(AControl.ControlText)
    End If
    Call ReplaceCharacters(TempA, "^", " ")
    Call ReplaceCharacters(TempA, "-", " ")
    TempA = Trim(TempA)
    Acmd.BackColor = BaseBackColor
    Acmd.ForeColor = BaseForeColor
    Acmd.Text = TempA
    Acmd.Tag = Trim(str(AControl.SecondaryControlId))
    Acmd.ToolTipText = Temp + "-" + Trim(AControl.ControlName)
    Acmd.Visible = True
    Acmd.Enabled = True
    SelectionLevel2(AIdx).ElementOn = False
    SelectionLevel2(AIdx).Question = AQuestion
    SelectionLevel2(AIdx).FormId = AControl.SecondaryFormId
    SelectionLevel2(AIdx).ControlId = AControl.SecondaryControlId
    SelectionLevel2(AIdx).ControlName = AControl.ControlName
    SelectionLevel2(AIdx).ControlLingo = Trim(AControl.DoctorLingo)
    SelectionLevel2(AIdx).DrugOn = AControl.DrugQuestion
    SelectionLevel2(AIdx).DrugsAlias(1) = Trim(AControl.ICDAlias1)
    SelectionLevel2(AIdx).DrugsAlias(2) = Trim(AControl.ICDAlias2)
    SelectionLevel2(AIdx).DrugsAlias(3) = Trim(AControl.ICDAlias3)
    SelectionLevel2(AIdx).DrugsAlias(4) = Trim(AControl.ICDAlias4)
    SelectionLevel2(AIdx).ControlNest = TheNest
    If (AControl.ICDAlias3 = "D") Then
        SelectionLevel2(AIdx).DateTrigger = ""
        SelectionLevel2(AIdx).DateTriggerOn = True
    End If
    lblLevel2.Caption = Trim(SelectionLevel2(AIdx).Question)
    lblLevel2.Visible = True
End If
End Function

Private Function PlantSelection3(AControl As DynamicThirdControls, AIdx As Integer, ALevel As Integer, AQuestion As String, ALevelIdx As Integer, BLink As String) As Boolean
Dim i As Integer
Dim Temp As String
Dim TempA As String
Dim Acmd As fpBtn
Dim TheNest As String
If (ALevel = 3) Then
    If (AIdx = 1) Then
        Set Acmd = cmdLevel3a
    ElseIf (AIdx = 2) Then
        Set Acmd = cmdLevel3b
    ElseIf (AIdx = 3) Then
        Set Acmd = cmdLevel3c
    ElseIf (AIdx = 4) Then
        Set Acmd = cmdLevel3d
    ElseIf (AIdx = 5) Then
        Set Acmd = cmdLevel3e
    ElseIf (AIdx = 6) Then
        Set Acmd = cmdLevel3f
    ElseIf (AIdx = 7) Then
        Set Acmd = cmdLevel3g
    ElseIf (AIdx = 8) Then
        Set Acmd = cmdLevel3h
    ElseIf (AIdx = 9) Then
        Set Acmd = cmdLevel3i
    ElseIf (AIdx = 10) Then
        Set Acmd = cmdLevel3j
    ElseIf (AIdx = 11) Then
        Set Acmd = cmdLevel3k
    ElseIf (AIdx = 12) Then
        Set Acmd = cmdLevel3l
    Else
        Exit Function
    End If
    Temp = ""
    If (AIdx < 10) Then
        Temp = Temp + "0" + Chr(48 + AIdx)
    Else
        Temp = Temp + Trim(str(AIdx))
    End If
'    TheNest = Trim(SelectionLevel1(ALevelIdx).ControlName)
    TheNest = Trim(BLink)
    TempA = Trim(AControl.DoctorLingo)
    Call ReplaceCharacters(TempA, "^", " ")
    Call ReplaceCharacters(TempA, "-", " ")
    If (Trim(TempA) = "") Then
        TempA = Trim(AControl.ControlText)
    End If
    TempA = Trim(TempA)
    Call ReplaceCharacters(TempA, "^", " ")
    Call ReplaceCharacters(TempA, "-", " ")
    Acmd.BackColor = BaseBackColor
    Acmd.ForeColor = BaseForeColor
    Acmd.Text = TempA
    Acmd.Tag = Trim(str(AControl.ThirdControlId))
    Acmd.ToolTipText = Temp + "-" + Trim(AControl.ControlName)
    Acmd.Visible = True
    Acmd.Enabled = True
    SelectionLevel3(AIdx).ElementOn = False
    SelectionLevel3(AIdx).Question = AQuestion
    SelectionLevel3(AIdx).FormId = AControl.ThirdFormId
    SelectionLevel3(AIdx).ControlId = AControl.ThirdControlId
    SelectionLevel3(AIdx).ControlName = AControl.ControlName
    SelectionLevel3(AIdx).ControlLingo = Trim(AControl.DoctorLingo)
    SelectionLevel3(AIdx).DrugOn = AControl.DrugQuestion
    SelectionLevel3(AIdx).DrugsAlias(1) = Trim(AControl.ICDAlias1)
    SelectionLevel3(AIdx).DrugsAlias(2) = Trim(AControl.ICDAlias2)
    SelectionLevel3(AIdx).DrugsAlias(3) = Trim(AControl.ICDAlias3)
    SelectionLevel3(AIdx).DrugsAlias(4) = Trim(AControl.ICDAlias4)
    SelectionLevel3(AIdx).ControlNest = TheNest
    If (AControl.ICDAlias3 = "D") Then
        SelectionLevel3(AIdx).DateTrigger = ""
        SelectionLevel3(AIdx).DateTriggerOn = True
    End If
    lblLevel3.Caption = Trim(SelectionLevel3(AIdx).Question)
    lblLevel3.Visible = True
End If
End Function

Private Function PlantSelection4(AControl As DynamicFourthControls, AIdx As Integer, ALevel As Integer, AQuestion As String, ALevelIdx As Integer, BLink As String) As Boolean
Dim i As Integer
Dim Temp As String
Dim TempA As String
Dim TheNest As String
Dim Acmd As fpBtn
If (ALevel = 4) Then
    If (AIdx = 1) Then
        Set Acmd = cmdLevel4a
    ElseIf (AIdx = 2) Then
        Set Acmd = cmdLevel4b
    ElseIf (AIdx = 3) Then
        Set Acmd = cmdLevel4c
    ElseIf (AIdx = 4) Then
        Set Acmd = cmdLevel4d
    ElseIf (AIdx = 5) Then
        Set Acmd = cmdlevel4e
    ElseIf (AIdx = 6) Then
        Set Acmd = cmdLevel4f
    ElseIf (AIdx = 7) Then
        Set Acmd = cmdLevel4g
    ElseIf (AIdx = 8) Then
        Set Acmd = cmdLevel4h
    ElseIf (AIdx = 9) Then
        Set Acmd = cmdLevel4i
    ElseIf (AIdx = 10) Then
        Set Acmd = cmdLevel4j
    ElseIf (AIdx = 11) Then
        Set Acmd = cmdLevel4k
    Else
        Exit Function
    End If
    Temp = ""
    If (AIdx < 10) Then
        Temp = Temp + "0" + Chr(48 + AIdx)
    Else
        Temp = Temp + Trim(str(AIdx))
    End If
    TheNest = Trim(BLink)
    TempA = Trim(AControl.DoctorLingo)
    Call ReplaceCharacters(TempA, "^", " ")
    Call ReplaceCharacters(TempA, "-", " ")
    If (Trim(TempA) = "") Then
        TempA = Trim(AControl.ControlText)
    End If
    Call ReplaceCharacters(TempA, "^", " ")
    Call ReplaceCharacters(TempA, "-", " ")
    TempA = Trim(TempA)
    Acmd.BackColor = BaseBackColor
    Acmd.ForeColor = BaseForeColor
    Acmd.Text = TempA
    Acmd.Tag = Trim(str(AControl.FourthControlId))
    Acmd.ToolTipText = Temp + "-" + Trim(AControl.ControlName)
    Acmd.Visible = True
    Acmd.Enabled = True
    SelectionLevel4(AIdx).ElementOn = False
    SelectionLevel4(AIdx).Question = AQuestion
    SelectionLevel4(AIdx).FormId = AControl.FourthFormId
    SelectionLevel4(AIdx).ControlId = AControl.FourthControlId
    SelectionLevel4(AIdx).ControlName = AControl.ControlName
    SelectionLevel4(AIdx).ControlLingo = Trim(AControl.DoctorLingo)
    SelectionLevel4(AIdx).DrugOn = AControl.DrugQuestion
    SelectionLevel4(AIdx).DrugsAlias(1) = Trim(AControl.ICDAlias1)
    SelectionLevel4(AIdx).DrugsAlias(2) = Trim(AControl.ICDAlias2)
    SelectionLevel4(AIdx).DrugsAlias(3) = Trim(AControl.ICDAlias3)
    SelectionLevel4(AIdx).DrugsAlias(4) = Trim(AControl.ICDAlias4)
    SelectionLevel4(AIdx).ControlNest = TheNest
    If (AControl.ICDAlias3 = "D") Then
        SelectionLevel4(AIdx).DateTrigger = ""
        SelectionLevel4(AIdx).DateTriggerOn = True
    End If
    lblLevel4.Caption = Trim(SelectionLevel4(AIdx).Question)
    lblLevel4.Visible = True
End If
End Function

Private Function DynamicEntry(Question As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Integer
DynamicEntry = True
LevelAt = 1
MaxLevel1 = 0
TheLink = ""
SetDate = ""
TriggerOnDone(1) = False
TriggerOnDone(2) = False
TriggerOnDone(3) = False
TriggerOnDone(4) = False
MaxQuestions(1) = 0
MaxQuestions(2) = 0
MaxQuestions(3) = 0
MaxQuestions(4) = 0
BaseBackColor = cmdNo.BackColor
BaseForeColor = cmdNo.ForeColor
BackSelectionColor = cmdApply.BackColor
TextSelectionColor = cmdApply.ForeColor
cmdApply.Visible = True
cmdNo.Visible = True
lblBox.Caption = Trim(BoxName)
If (BoxName = "CC") Then
    lblBox.Caption = "HPI"
ElseIf (BoxName = "HPI") Then
    lblBox.Caption = "CC"
End If
Set TheForm = New DynamicForms
Set TheControl = New DynamicControls
TheForm.Question = Trim(Question)
If (TheForm.RetrieveForm) And (TheForm.FormId > 0) Then
    CurrentFormId = TheForm.FormId
    TheControl.Language = ""
    TheControl.FormId = TheForm.FormId
    If (TheControl.FindControl > 0) Then
        i = 1
        While (TheControl.SelectControl(i))
            If Not (TheControl.IgnoreIt) Then
                If (TheControl.ControlType = "B") Then
                    MaxLevel1 = MaxLevel1 + 1
                    Call PlantSelection1(TheControl, MaxLevel1, 1, Question)
                End If
            End If
            i = i + 1
        Wend
        Call TurnOffOthers(1)
    End If
Else
    DynamicEntry = False
    ReloadForm = False
End If
Set TheForm = Nothing
Set TheControl = Nothing
Exit Function
UI_ErrorHandler:
    Set TheForm = Nothing
    Set TheControl = Nothing
    DynamicEntry = False
    Call PinpointError("DynamicEntry", Err.Source + " " + Err.Description)
    Resume LeaveFast
LeaveFast:
End Function

Private Sub form_load()
On Error GoTo UI_ErrorHandler
Dim MyFile As String
    Dim FileNum As Integer
    LevelAt = 1
ReloadForm = False
Erase SelectionLevel1
Erase SelectionLevel2
Erase SelectionLevel3
Erase SelectionLevel4
Erase SelectionLingo
StoreResultsOrg = ""
If (Trim(StoreResults) <> "") Then
    StoreResultsOrg = StoreResults + "org"
    MyFile = Dir(StoreResults)
    If (MyFile <> "") Then
        FM.FileCopy StoreResults, StoreResultsOrg
        StoreresultOrgValidate = StoreResults + "orgvalidate"
        FM.FileCopy StoreResults, StoreresultOrgValidate
        ReloadForm = True
    End If
End If
FileNum = FreeFile
FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
FM.CloseFile CLng(FileNum)
lblLevel1.Caption = Question
If Not (DynamicEntry(Question)) Then
    Unload frmEditPI
    FM.Kill (StoreResults)
End If
Exit Sub
UI_ErrorHandler:
    If (Err = 53) Then
        Resume Next
    End If
    Call PinpointError("EditPI", Err.Source + " " + Err.Description)
    Unload frmEditPI
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub TriggerCheckBox(Acmd As fpBtn)
If IsLvlTwSlctd = True And InStrPS(StoreResults, "P69A") <> 0 And Val(Mid(Acmd.Name, 9, 1)) = 2 Then
    Exit Sub
End If
If InStrPS(StoreResults, "P66A") <> 0 And Val(Mid(Acmd.Name, 9, 1)) = 1 Then
    Acmd.Enabled = False
    If AllBtnName <> "" Then
        EnableBtnLvlOneAlg (AllBtnName)
    End If
    AllBtnName = Acmd.Name
End If
Dim Lv1 As Integer
Dim Temp As String
Dim AChoice As Integer
Dim TheLevel As Integer
Dim TriggerOn As Boolean
Lv1 = Val(Left(Acmd.ToolTipText, 2))
If (Lv1 > 0) Then
    If (Acmd.BackColor = BaseBackColor) Then
        TheLevel = Val(Mid(Acmd.Name, 9, 1))
        LevelAt = TheLevel
        If (TheLevel = 1) Then
            SelectionLevel1(Lv1).ElementOn = True
            SelectionLevel1(Lv1).ElementPosted = False
            If InStrPS(StoreResults, "P69A") <> 0 Then
                IsLvlTwSlctd = False
            End If
        ElseIf (TheLevel = 2) Then
            If InStrPS(StoreResults, "P69A") <> 0 Then
                IsLvlTwSlctd = True
            End If
            SelectionLevel2(Lv1).ElementOn = True
            SelectionLevel2(Lv1).ElementPosted = False
        ElseIf (TheLevel = 3) Then
            SelectionLevel3(Lv1).ElementOn = True
            SelectionLevel3(Lv1).ElementPosted = False
        ElseIf (TheLevel = 4) Then
            SelectionLevel4(Lv1).ElementOn = True
            SelectionLevel4(Lv1).ElementPosted = False
        End If
        Acmd.BackColor = BackSelectionColor
        If (TheLevel < 4) Then
            If (TheLevel = 1) Then
                FrmReactions.Visible = False
                AReaction = "None"
                ASeverity = "None"
                If (SelectionLevel1(Lv1).DateTriggerOn) Then
                    Call DoDateEntry(SelectionLevel1(Lv1).DateTrigger)
                End If
            ElseIf (TheLevel = 2) Then
                If (SelectionLevel2(Lv1).DateTriggerOn) Then
                    Call DoDateEntry(SelectionLevel2(Lv1).DateTrigger)
                End If
            ElseIf (TheLevel = 3) Then
                If (SelectionLevel3(Lv1).DateTriggerOn) Then
                    Call DoDateEntry(SelectionLevel3(Lv1).DateTrigger)
                End If
            End If
            QuestionId(TheLevel + 1) = 1
            Call PostToFileAndTrigger(TheLevel)
            Call TriggerLevel(TheLevel + 1, Acmd, QuestionId(TheLevel + 1), TriggerOn, Lv1)
            If (InStrPS(StoreResults, "P69A") <> 0 And TheLevel = 2) Then
                Acmd.Enabled = False
            End If
        ElseIf (TheLevel = 4) Then
            If (SelectionLevel4(Lv1).DateTriggerOn) Then
                Call DoDateEntry(SelectionLevel4(Lv1).DateTrigger)
            End If
            Call PostToFileAndTrigger(TheLevel)
            Call TriggerLevel(TheLevel, Acmd, QuestionId(TheLevel), TriggerOn, Lv1)
        End If
    Else
        LevelAt = 0
        QuestionId(TheLevel) = 1
        If (TheLevel = 1) Then
            SelectionLevel1(Lv1).ElementOn = False
            SelectionLevel1(Lv1).ElementPosted = False
        ElseIf (TheLevel = 2) Then
            SelectionLevel2(Lv1).ElementOn = False
            SelectionLevel2(Lv1).ElementPosted = False
        ElseIf (TheLevel = 3) Then
            SelectionLevel3(Lv1).ElementOn = False
            SelectionLevel3(Lv1).ElementPosted = False
        ElseIf (TheLevel = 4) Then
            SelectionLevel4(Lv1).ElementOn = False
            SelectionLevel4(Lv1).ElementPosted = False
        End If
    End If
End If
End Sub

Private Sub cmdLevel1a_Click()
Call TriggerCheckBox(cmdLevel1a)
End Sub

Private Sub cmdLevel1b_Click()
Call TriggerCheckBox(cmdLevel1b)
End Sub

Private Sub cmdLevel1c_Click()
Call TriggerCheckBox(cmdLevel1c)
End Sub

Private Sub cmdLevel1d_Click()
Call TriggerCheckBox(cmdLevel1d)
End Sub

Private Sub cmdLevel1e_Click()
Call TriggerCheckBox(cmdLevel1e)
End Sub

Private Sub cmdLevel1f_Click()
Call TriggerCheckBox(cmdLevel1f)
End Sub

Private Sub cmdLevel1g_Click()
Call TriggerCheckBox(cmdLevel1g)
End Sub

Private Sub cmdLevel1h_Click()
Call TriggerCheckBox(cmdLevel1h)
End Sub

Private Sub cmdLevel1i_Click()
Call TriggerCheckBox(cmdLevel1i)
End Sub

Private Sub cmdLevel1j_Click()
Call TriggerCheckBox(cmdLevel1j)
End Sub

Private Sub cmdLevel1k_Click()
Call TriggerCheckBox(cmdLevel1k)
End Sub

Private Sub cmdLevel2a_Click()
If IsLvlOneSlctd Then
    
End If
Call TriggerCheckBox(cmdLevel2a)
End Sub

Private Sub cmdLevel2b_Click()
If IsLvlOneSlctd Then
    
End If
Call TriggerCheckBox(cmdLevel2b)
End Sub

Private Sub cmdLevel2c_Click()
 Call TriggerCheckBox(cmdLevel2c)
End Sub

Private Sub cmdLevel2d_Click()
Call TriggerCheckBox(cmdLevel2d)
End Sub

Private Sub cmdLevel2e_Click()
Call TriggerCheckBox(cmdLevel2e)
End Sub

Private Sub cmdLevel2f_Click()
Call TriggerCheckBox(cmdLevel2f)
End Sub

Private Sub cmdLevel2g_Click()
Call TriggerCheckBox(cmdLevel2g)
End Sub

Private Sub cmdLevel2h_Click()
Call TriggerCheckBox(cmdLevel2h)
End Sub

Private Sub cmdLevel2i_Click()
Call TriggerCheckBox(cmdLevel2i)
End Sub

Private Sub cmdLevel2j_Click()
Call TriggerCheckBox(cmdLevel2j)
End Sub

Private Sub cmdLevel2k_Click()
Call TriggerCheckBox(cmdLevel2k)
End Sub

Private Sub cmdLevel2l_Click()
Call TriggerCheckBox(cmdLevel2l)
End Sub

Private Sub cmdLevel3a_Click()
Call TriggerCheckBox(cmdLevel3a)
End Sub

Private Sub cmdLevel3b_Click()
Call TriggerCheckBox(cmdLevel3b)
End Sub

Private Sub cmdLevel3c_Click()
Call TriggerCheckBox(cmdLevel3c)
End Sub

Private Sub cmdLevel3d_Click()
Call TriggerCheckBox(cmdLevel3d)
End Sub

Private Sub cmdLevel3e_Click()
Call TriggerCheckBox(cmdLevel3e)
End Sub

Private Sub cmdLevel3f_Click()
Call TriggerCheckBox(cmdLevel3f)
End Sub

Private Sub cmdLevel3g_Click()
Call TriggerCheckBox(cmdLevel3g)
End Sub

Private Sub cmdLevel3h_Click()
Call TriggerCheckBox(cmdLevel3h)
End Sub

Private Sub cmdLevel3i_Click()
Call TriggerCheckBox(cmdLevel3i)
End Sub

Private Sub cmdLevel3j_Click()
Call TriggerCheckBox(cmdLevel3j)
End Sub

Private Sub cmdLevel3k_Click()
Call TriggerCheckBox(cmdLevel3k)
End Sub

Private Sub cmdLevel3l_Click()
Call TriggerCheckBox(cmdLevel3l)
End Sub

Private Sub cmdLevel4a_Click()
Call TriggerCheckBox(cmdLevel4a)
End Sub

Private Sub cmdLevel4b_Click()
Call TriggerCheckBox(cmdLevel4b)
End Sub

Private Sub cmdLevel4c_Click()
Call TriggerCheckBox(cmdLevel4c)
End Sub

Private Sub cmdLevel4d_Click()
Call TriggerCheckBox(cmdLevel4d)
End Sub

Private Sub cmdLevel4e_Click()
Call TriggerCheckBox(cmdlevel4e)
End Sub

Private Sub cmdLevel4f_Click()
Call TriggerCheckBox(cmdLevel4f)
End Sub

Private Sub cmdLevel4g_Click()
Call TriggerCheckBox(cmdLevel4g)
End Sub

Private Sub cmdLevel4h_Click()
Call TriggerCheckBox(cmdLevel4h)
End Sub

Private Sub cmdLevel4i_Click()
Call TriggerCheckBox(cmdLevel4i)
End Sub

Private Sub cmdLevel4j_Click()
Call TriggerCheckBox(cmdLevel4j)
End Sub

Private Sub cmdLevel4k_Click()
Call TriggerCheckBox(cmdLevel4k)
End Sub

Private Function NextCurrentLevelQuestion(TheLevel As Integer, TheQuestion As Integer) As Boolean
Dim Trig As Boolean
Dim Lv1 As Integer
Dim cmdBx As fpBtn
Dim ALevel As Integer
NextCurrentLevelQuestion = False
TheQuestion = TheQuestion + 1
ALevel = TheLevel - 1
If (ALevel < 1) Then
    ALevel = 1
End If
Trig = False
If (ALevel = 1) Then
    If Not (cmdLevel1a.Enabled) Then
        Set cmdBx = cmdLevel1a
        Trig = True
    End If
    If Not (cmdLevel1b.Enabled) Then
        Set cmdBx = cmdLevel1b
        Trig = True
    End If
    If Not (cmdLevel1c.Enabled) Then
        Set cmdBx = cmdLevel1c
        Trig = True
    End If
    If Not (cmdLevel1d.Enabled) Then
        Set cmdBx = cmdLevel1d
        Trig = True
    End If
    If Not (cmdLevel1e.Enabled) Then
        Set cmdBx = cmdLevel1e
        Trig = True
    End If
    If Not (cmdLevel1f.Enabled) Then
        Set cmdBx = cmdLevel1f
        Trig = True
    End If
    If Not (cmdLevel1g.Enabled) Then
        Set cmdBx = cmdLevel1g
        Trig = True
    End If
    If Not (cmdLevel1h.Enabled) Then
        Set cmdBx = cmdLevel1h
        Trig = True
    End If
    If Not (cmdLevel1i.Enabled) Then
        Set cmdBx = cmdLevel1i
        Trig = True
    End If
    If Not (cmdLevel1j.Enabled) Then
        Set cmdBx = cmdLevel1j
        Trig = True
    End If
    If Not (cmdLevel1k.Enabled) Then
        Set cmdBx = cmdLevel1k
        Trig = True
    End If
ElseIf (ALevel = 2) Then
    If Not (cmdLevel2a.Enabled) Then
        Set cmdBx = cmdLevel2a
        Trig = True
    End If
    If Not (cmdLevel2b.Enabled) Then
        Set cmdBx = cmdLevel2b
        Trig = True
    End If
    If Not (cmdLevel2c.Enabled) Then
        Set cmdBx = cmdLevel2c
        Trig = True
    End If
    If Not (cmdLevel2d.Enabled) Then
       Set cmdBx = cmdLevel2d
        Trig = True
    End If
    If Not (cmdLevel2e.Enabled) Then
        Set cmdBx = cmdLevel2e
        Trig = True
    End If
    If Not (cmdLevel2f.Enabled) Then
        Set cmdBx = cmdLevel2f
        Trig = True
    End If
    If Not (cmdLevel2g.Enabled) Then
        Set cmdBx = cmdLevel2g
        Trig = True
    End If
    If Not (cmdLevel2h.Enabled) Then
        Set cmdBx = cmdLevel2h
        Trig = True
    End If
    If Not (cmdLevel2i.Enabled) Then
        Set cmdBx = cmdLevel2i
        Trig = True
    End If
    If Not (cmdLevel2j.Enabled) Then
        Set cmdBx = cmdLevel2j
        Trig = True
    End If
    If Not (cmdLevel2k.Enabled) Then
        Set cmdBx = cmdLevel2k
        Trig = True
    End If
    If Not (cmdLevel2l.Enabled) Then
        Set cmdBx = cmdLevel2l
        Trig = True
    End If
ElseIf (ALevel = 3) Then
    If Not (cmdLevel3a.Enabled) Then
        Set cmdBx = cmdLevel3a
        Trig = True
    End If
    If Not (cmdLevel3b.Enabled) Then
        Set cmdBx = cmdLevel3b
        Trig = True
    End If
    If Not (cmdLevel3c.Enabled) Then
        Set cmdBx = cmdLevel3c
        Trig = True
    End If
    If Not (cmdLevel3d.Enabled) Then
        Set cmdBx = cmdLevel3d
        Trig = True
    End If
    If Not (cmdLevel3e.Enabled) Then
        Set cmdBx = cmdLevel3e
        Trig = True
    End If
    If Not (cmdLevel3f.Enabled) Then
        Set cmdBx = cmdLevel3f
        Trig = True
    End If
    If Not (cmdLevel3g.Enabled) Then
        Set cmdBx = cmdLevel3g
        Trig = True
    End If
    If Not (cmdLevel3h.Enabled) Then
        Set cmdBx = cmdLevel3h
        Trig = True
    End If
    If Not (cmdLevel3i.Enabled) Then
        Set cmdBx = cmdLevel3i
        Trig = True
    End If
    If Not (cmdLevel3j.Enabled) Then
        Set cmdBx = cmdLevel3j
        Trig = True
    End If
    If Not (cmdLevel3k.Enabled) Then
        Set cmdBx = cmdLevel3k
        Trig = True
    End If
    If Not (cmdLevel3l.Enabled) Then
        Set cmdBx = cmdLevel3l
        Trig = True
    End If
ElseIf (ALevel = 4) Then
    If Not (cmdLevel4a.Enabled) Then
        Set cmdBx = cmdLevel4a
        Trig = True
    End If
    If Not (cmdLevel4b.Enabled) Then
        Set cmdBx = cmdLevel4b
        Trig = True
    End If
    If Not (cmdLevel4c.Enabled) Then
        Set cmdBx = cmdLevel4c
        Trig = True
    End If
    If Not (cmdLevel4d.Enabled) Then
        Set cmdBx = cmdLevel4d
        Trig = True
    End If
    If Not (cmdlevel4e.Enabled) Then
        Set cmdBx = cmdlevel4e
        Trig = True
    End If
    If Not (cmdLevel4f.Enabled) Then
        Set cmdBx = cmdLevel4f
        Trig = True
    End If
    If Not (cmdLevel4g.Enabled) Then
        Set cmdBx = cmdLevel4g
        Trig = True
    End If
    If Not (cmdLevel4h.Enabled) Then
        Set cmdBx = cmdLevel4h
        Trig = True
    End If
    If Not (cmdLevel4i.Enabled) Then
        Set cmdBx = cmdLevel4i
        Trig = True
    End If
    If Not (cmdLevel4j.Enabled) Then
        Set cmdBx = cmdLevel4j
        Trig = True
    End If
    If Not (cmdLevel4k.Enabled) Then
        Set cmdBx = cmdLevel4k
        Trig = True
    End If
End If
If (Trig) Then
    Trig = False
    Lv1 = Val(Left(cmdBx.ToolTipText, 2))
    Call TriggerLevel(ALevel + 1, cmdBx, TheQuestion, Trig, Lv1)
    NextCurrentLevelQuestion = Trig
End If
End Function

Private Sub PostToFileAndTrigger(TheLevel As Integer)
Dim w As Integer, p As Integer
Dim TestStore As String
Dim ThePatient As String
Dim ButtonName As String
Dim FileNum As Integer
Dim LocalRef As Integer
Dim PostedResults As Boolean
If (Trim(StoreResults) = "") Then
    Exit Sub
End If
p = InStrPS(StoreResults, "_")
If (p = 0) Then
    Exit Sub
Else
    w = InStrPS(p, StoreResults, ".")
    If (w = 0) Then
        Exit Sub
    End If
    ThePatient = Mid(StoreResults, p + 1, w - (p + 1))
End If
LocalRef = 0
PostedResults = False
FileNum = FreeFile
Dim path As String
path = FM.actualpath(StoreResults)
    FM.OpenFile StoreResults, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
If (TheLevel = 1) Then
    RefToField = ""
    Print #FileNum, "QUESTION=" + "/" + Trim(SelectionLevel1(1).Question)
    LocalRef = MaxLevel1
    For p = 1 To LocalRef
        If (SelectionLevel1(p).ElementOn) And Not (SelectionLevel1(p).ElementPosted) Then
            RefToField = Trim(SelectionLevel1(p).ControlName)
            If (Trim(SelectionLevel1(p).DateTrigger) <> "") Then
                ButtonName = Trim(SelectionLevel1(p).ControlName) + "=T" + Trim(str(SelectionLevel1(p).ControlId)) + " <" + Trim(SelectionLevel1(p).ControlName) + ">" + "(DATE=" + Trim(SelectionLevel1(p).DateTrigger) + ")"
            Else
                ButtonName = Trim(SelectionLevel1(p).ControlName) + "=T" + Trim(str(SelectionLevel1(p).ControlId)) + " <" + Trim(SelectionLevel1(p).ControlName) + ">"
            End If
             
            
            ButtonName = ButtonName '& AllergyRtnSvt.Reaction & AllergyRtnSvt.Severity
            If (InStrPS(StoreResults, "P66A") <> 0) Then
                    Dim frmSys1 As New frmSystems1
                    frmSys1.DeleteDuplicateAllergyIfExistsComplaintFromFile globalExam.iPatientId, "/ALLERGIES", ButtonName + FSlash + AllergyRtnSvt.Reaction + FSlash + AllergyRtnSvt.Severity
                AllergyRtnSvt.Show vbModal, frmEditPI
                If AllergyRtnSvt.Reaction = "" Then
                    AllergyRtnSvt.Reaction = "None"
                End If
                If AllergyRtnSvt.Severity = "" Then
                    AllergyRtnSvt.Severity = "None"
                End If
                ButtonName = ButtonName + FSlash + AllergyRtnSvt.Reaction + FSlash + AllergyRtnSvt.Severity
             ElseIf (InStrPS(StoreResults, "P69A") <> 0) Then
                ButtonName = ButtonName + FSlash + "None" + FSlash + "None"
             End If
            'ButtonName = ButtonName & "ContraindicationNWMILD TO MODERATE"
            PostedResults = True
            Print #FileNum, ButtonName
            SelectionLevel1(p).ElementPosted = True
        End If
    Next p
    FM.CloseFile CLng(FileNum)
ElseIf (TheLevel = 2) Then
    Print #FileNum, "QUESTION=" + "?" + Trim(SelectionLevel2(1).Question)
    LocalRef = MaxLevel2
    For p = 1 To LocalRef
        If ((SelectionLevel2(p).ElementOn) And Not (SelectionLevel2(p).ElementPosted)) Then
            If (Trim(SelectionLevel2(p).DateTrigger) <> "") Then
                ButtonName = Trim(SelectionLevel2(p).ControlName) + "=T" + Trim(str(SelectionLevel2(p).ControlId)) + " <" + Trim(RefToField) + ">" + "(DATE=" + Trim(SelectionLevel2(p).DateTrigger) + ")"
            Else
                ButtonName = Trim(SelectionLevel2(p).ControlName) + "=T" + Trim(str(SelectionLevel2(p).ControlId)) + " <" + Trim(RefToField) + ">"
            End If
            PostedResults = True
            Print #FileNum, ButtonName
            SelectionLevel2(p).ElementPosted = True
        End If
    Next p
    FM.CloseFile CLng(FileNum)
    If Not (TriggerOnDone(2)) And Not (cmdLevel3a.Visible) Then
        LevelAt = 1
        Call TurnOffOthers(1)
        If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(2))) Then
            Call TurnOn(1)
        End If
    End If
ElseIf (TheLevel = 3) Then
    Print #FileNum, "QUESTION=" + "]" + Trim(SelectionLevel3(1).Question)
    LocalRef = MaxLevel3
    For p = 1 To LocalRef
        If (SelectionLevel3(p).ElementOn) And Not (SelectionLevel3(p).ElementPosted) Then
            If (Trim(SelectionLevel3(p).DateTrigger) <> "") Then
                ButtonName = Trim(SelectionLevel3(p).ControlName) + "=T" + Trim(str(SelectionLevel3(p).ControlId)) + " <" + Trim(RefToField) + ">" + "(DATE=" + Trim(SelectionLevel3(p).DateTrigger) + ")"
            Else
                ButtonName = Trim(SelectionLevel3(p).ControlName) + "=T" + Trim(str(SelectionLevel3(p).ControlId)) + " <" + Trim(RefToField) + ">"
            End If
            PostedResults = True
            Print #FileNum, ButtonName
            SelectionLevel3(p).ElementPosted = True
        End If
    Next p
    FM.CloseFile CLng(FileNum)
    If Not (TriggerOnDone(3)) And Not (cmdLevel4a.Visible) Then
        LevelAt = 2
        Call TurnOffOthers(2)
        If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(3))) Then
            Call TurnOn(2)
        Else
            LevelAt = 1
            Call TurnOffOthers(1)
        End If
    End If
ElseIf (TheLevel = 4) Then
    Print #FileNum, "QUESTION=" + "[" + Trim(SelectionLevel4(1).Question)
    LocalRef = MaxLevel4
    For p = 1 To LocalRef
        If (SelectionLevel4(p).ElementOn) And Not (SelectionLevel4(p).ElementPosted) Then
            If (Trim(SelectionLevel4(p).DateTrigger) <> "") Then
                ButtonName = Trim(SelectionLevel4(p).ControlName) + "=T" + Trim(str(SelectionLevel4(p).ControlId)) + " <" + Trim(RefToField) + ">" + "(DATE=" + Trim(SelectionLevel4(p).DateTrigger) + ")"
            Else
                ButtonName = Trim(SelectionLevel4(p).ControlName) + "=T" + Trim(str(SelectionLevel4(p).ControlId)) + " <" + Trim(RefToField) + ">"
            End If
            PostedResults = True
            Print #FileNum, ButtonName
            SelectionLevel4(p).ElementPosted = True
        End If
    Next p
    FM.CloseFile CLng(FileNum)
    If Not (TriggerOnDone(4)) Then
        LevelAt = 4
        Call TurnOffOthers(4)
        If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(4))) Then
            Call TurnOn(3)
        Else
            LevelAt = 3
            Call TurnOffOthers(3)
            If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(3))) Then
                Call TurnOn(3)
            Else
                LevelAt = 2
                Call TurnOffOthers(2)
                If Not (NextCurrentLevelQuestion(LevelAt, QuestionId(2))) Then
                    Call TurnOn(2)
                Else
                    LevelAt = 1
                    Call TurnOffOthers(1)
                End If
            End If
        End If
    End If
Else
    FM.CloseFile CLng(FileNum)
    If Not (PostedResults) Then
        FM.Kill (StoreResults)
    End If
End If
End Sub
Private Sub EnableBtnLvlOneAlg(AllBtnName As String)
     Select Case AllBtnName
            Case "cmdLevel1a"
                 cmdLevel1a.Enabled = True
            Case "cmdLevel1b"
                 cmdLevel1b.Enabled = True
            Case "cmdLevel1c"
                 cmdLevel1c.Enabled = True
            Case "cmdLevel1d"
                 cmdLevel1d.Enabled = True
            Case "cmdLevel1e"
                 cmdLevel1e.Enabled = True
            Case "cmdLevel1f"
                 cmdLevel1f.Enabled = True
            Case "cmdLevel1g"
                 cmdLevel1g.Enabled = True
            Case "cmdLevel1h"
                 cmdLevel1h.Enabled = True
            Case "cmdLevel1i"
                 cmdLevel1i.Enabled = True
            Case "cmdLevel1j"
                 cmdLevel1j.Enabled = True
            Case "cmdLevel1k"
                 cmdLevel1k.Enabled = True
     End Select
End Sub
Private Function MergeFiles(OrgFile As String, NewFile As String) As Boolean
Dim Rec As String
MergeFiles = True
    If (FM.IsFileThere(OrgFile)) Then
        FM.OpenFile OrgFile, FileOpenMode.InputFileOpenMode, FileAccess.Read, CLng(101)
    FM.OpenFile NewFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(102)
    While Not (EOF(101))
            Line Input #101, Rec
        If (Left(Rec, 1) <> "/") And (Left(Rec, 6) <> "Recap=") Then
                Print #102, Rec
        End If
    Wend
    FM.CloseFile CLng(101)
    FM.CloseFile CLng(102)

End If
End Function

Private Function ReplaceFiles(OrgFile As String, NewFile As String, ReplaceItem As String) As Boolean
Dim Rec As String
Dim SkipOn As Boolean
ReplaceFiles = True
If (FM.IsFileThere(OrgFile)) Then
    SkipOn = False
    FM.OpenFile OrgFile, FileOpenMode.InputFileOpenMode, FileAccess.Read, CLng(101)
    FM.OpenFile NewFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(102)
    While Not (EOF(101))
        Line Input #101, Rec
        If (SkipOn) And (Mid(Rec, 10, 1) = "/") Then
            SkipOn = False
        End If
        If (Mid(Rec, 10, 1) <> "/") And (Left(Rec, 6) <> "Recap=") Then
            If Not (SkipOn) Then
                If (InStrPS(Rec, ReplaceItem) = 0) Then
                    SkipOn = False
                    Print #102, Rec
                Else
                    SkipOn = True
                End If
            End If
        End If
    Wend
    FM.CloseFile CLng(101)
    FM.CloseFile CLng(102)
End If
End Function

Private Function DoDateEntry(TheDate As String) As Boolean
Dim ADate As String
TheDate = ""
frmNumericPad.NumPad_Result = ""
frmNumericPad.NumPad_Field = "Enter a date, or just press Done"
frmNumericPad.NumPad_Max = 0
frmNumericPad.NumPad_Min = 0
frmNumericPad.NumPad_Default = 0
frmNumericPad.NumPad_ForceDecimal = False
frmNumericPad.NumPad_HelpField = ""
frmNumericPad.NumPad_EyesOn = False
frmNumericPad.NumPad_CommentOn = False
frmNumericPad.NumPad_TimeFrameOn = False
frmNumericPad.NumPad_DisplayFull = True
frmNumericPad.NumPad_Choice1 = "AM"
frmNumericPad.NumPad_Choice2 = "before"
frmNumericPad.NumPad_Choice3 = "approx"
frmNumericPad.NumPad_Choice4 = "since"
frmNumericPad.NumPad_Choice5 = ""
frmNumericPad.NumPad_Choice6 = "PM"
frmNumericPad.NumPad_Choice7 = "Hour"
frmNumericPad.NumPad_Choice8 = "Min"
frmNumericPad.NumPad_Choice9 = "ago"
frmNumericPad.NumPad_Choice10 = ""
frmNumericPad.NumPad_Choice11 = "Days"
frmNumericPad.NumPad_Choice12 = "Weeks"
frmNumericPad.NumPad_Choice13 = "Months"
frmNumericPad.NumPad_Choice14 = "Years"
frmNumericPad.NumPad_Choice15 = ""
frmNumericPad.NumPad_Choice16 = ""
frmNumericPad.NumPad_Choice17 = ""
frmNumericPad.NumPad_Choice18 = ""
frmNumericPad.NumPad_Choice19 = ""
frmNumericPad.NumPad_Choice20 = ""
frmNumericPad.NumPad_Choice21 = ""
frmNumericPad.NumPad_Choice22 = ""
frmNumericPad.NumPad_Choice23 = ""
frmNumericPad.NumPad_Choice24 = ""
frmNumericPad.Show 1
If Not (frmNumericPad.NumPad_Quit) Then
    If (Trim(frmNumericPad.NumPad_Result) <> "") Then
        TheDate = Trim(frmNumericPad.NumPad_Result)
        If (frmNumericPad.NumPad_ChoiceOn1) Then
            TheDate = TheDate + " (AM) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn2) Then
            TheDate = TheDate + " (before) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn3) Then
            TheDate = TheDate + " (approx) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn4) Then
            TheDate = TheDate + " (since) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn6) Then
            TheDate = TheDate + " (PM) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn7) Then
            TheDate = TheDate + " (Hour) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn8) Then
            TheDate = TheDate + " (Min) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn9) Then
            TheDate = TheDate + " (ago) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn11) Then
            TheDate = TheDate + " (Days) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn12) Then
            TheDate = TheDate + " (Weeks) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn13) Then
            TheDate = TheDate + " (Months) "
        End If
        If (frmNumericPad.NumPad_ChoiceOn14) Then
            TheDate = TheDate + " (Years) "
        End If
    End If
End If
End Function
Public Function FrmClose()
Call cmdNo_Click
 Unload Me
End Function

Private Sub fpBtn1_Click()
ASeverity = "MILD TO MODERATE"
End Sub

Private Sub fpBtn2_Click()
ASeverity = "MILD"
End Sub

Private Sub fpBtn3_Click()
AReaction = "Difficulty Breathing"
End Sub


Private Sub fpBtn4_Click()
AReaction = "No Therapeutic Effect"
'fpBtn4.Enabled
End Sub
Private Sub SetRctSvt()
If (FM.IsFileThere(StoreResults)) Then
    Dim Alg69ARows() As String
    Dim UnAlgLen As Integer
    Dim UnAlgLenD As Integer
    Dim Rec As String
    Dim FileNum As Integer
    Dim AngIndex As Integer
    FileNum = FreeFile
    FM.OpenFile StoreResults, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
     While Not (EOF(FileNum))
            Line Input #FileNum, Rec
            UnAlgLen = UnAlgLen + 1
            ReDim Preserve Alg69ARows(1 To UnAlgLen) As String
            Alg69ARows(UnAlgLen) = Rec
     Wend
    FM.CloseFile CLng(FileNum)
    Dim AlgLen As Integer
    If (UnAlgLen > 1) Then
        Dim frmS1 As New frmSystems1
            frmS1.DeleteDuplicateAllergyIfExistsComplaintFromFile globalExam.iPatientId, "/ANY DRUG ALLERGIES", Rec
        AlgLen = UBound(Alg69ARows)
        If AReaction = "" Then
            AReaction = "None"
        End If
        If ASeverity = "" Then
            ASeverity = "None"
        End If
        If (AlgLen > 3 Or AlgLen = 4) Then
            If (InStrPS(Alg69ARows(AlgLen - 1), "QUESTION=/ANY DRUG ALLERGIES") <> 0) Then
                AngIndex = InStrPS(Alg69ARows(AlgLen), ">")
                If AngIndex > 0 Then
                    Alg69ARows(AlgLen) = Mid(Alg69ARows(AlgLen), 1, AngIndex) + FSlash + AReaction + FSlash + ASeverity
                End If
            ElseIf (InStrPS(Alg69ARows(AlgLen - 3), "QUESTION=/ANY DRUG ALLERGIES") <> 0) Then
                AngIndex = InStrPS(Alg69ARows(AlgLen - 2), ">")
                If AngIndex > 0 Then
                    Alg69ARows(AlgLen - 2) = Mid(Alg69ARows(AlgLen - 2), 1, AngIndex) + FSlash + AReaction + FSlash + ASeverity
                End If
            End If
        ElseIf (AlgLen = 2) Then
            If (InStrPS(Alg69ARows(AlgLen - 1), "QUESTION=/ANY DRUG ALLERGIES") <> 0) Then
                AngIndex = InStrPS(Alg69ARows(AlgLen), ">")
                If AngIndex > 0 Then
                    Alg69ARows(AlgLen) = Mid(Alg69ARows(AlgLen), 1, AngIndex) + FSlash + AReaction + FSlash + ASeverity
                End If
            End If
        End If
    End If
    If UnAlgLen > 1 Then
        Dim Index As Integer
        If FM.IsFileThere(StoreResults) Then
            FM.Kill (StoreResults)
        End If
        For Index = 1 To UnAlgLen
            FM.OpenFile StoreResults, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
                 Print #FileNum, Alg69ARows(Index)
            FM.CloseFile (FileNum)
        Next
    End If
 End If
End Sub



Private Sub EnableRctSvrtBt()
    fpBtnRc1.Enabled = True
    FpBtnRc2.Enabled = True
    fpBtnRc3.Enabled = True
    fpBtnRc4.Enabled = True
    fpBtnRc5.Enabled = True
    fpBtnRc6.Enabled = True
    fpBtnRc7.Enabled = True
    fpBtnRc8.Enabled = True
    fpBtnRc9.Enabled = True
    fpBtnRc10.Enabled = True
    fpBtnRc11.Enabled = True
    fpBtnSv1.Enabled = True
    fpBtnSv2.Enabled = True
    fpBtnSv3.Enabled = True
    fpBtnSv4.Enabled = True
    fpBtnSv5.Enabled = True
End Sub
Private Sub fpBtnRc1_Click()
If (AReaction <> "" And AReaction <> "None") Then
    
Else
AReaction = fpBtnRc1.Text
fpBtnRc1.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub FpBtnRc2_Click()
If (AReaction <> "" And AReaction <> "None") Then
Else
AReaction = FpBtnRc2.Text
FpBtnRc2.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnRc3_Click()
If (AReaction <> "" And AReaction <> "None") Then
    
Else
AReaction = fpBtnRc3.Text
fpBtnRc3.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnRc4_Click()
If (AReaction <> "None" And AReaction <> "") Then
    
Else
AReaction = fpBtnRc4.Text
fpBtnRc4.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnRc5_Click()
If (AReaction <> "None" And AReaction <> "") Then
    
Else
AReaction = fpBtnRc5.Text
fpBtnRc5.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnRc6_Click()
If (AReaction <> "None" And AReaction <> "") Then
    
Else
AReaction = fpBtnRc6.Text
fpBtnRc6.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnRc7_Click()
If (AReaction <> "None" And AReaction <> "") Then
    
Else
AReaction = fpBtnRc7.Text
fpBtnRc7.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnRc8_Click()
If (AReaction <> "None" And AReaction <> "") Then
    
Else
AReaction = fpBtnRc8.Text
fpBtnRc8.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnRc9_Click()
If (AReaction <> "None" And AReaction <> "") Then
    
Else
AReaction = fpBtnRc9.Text
fpBtnRc9.Enabled = False
Call SetRctSvt
End If
End Sub
Private Sub fpBtnRc10_Click()
If (AReaction <> "None" And AReaction <> "") Then
    
Else
AReaction = fpBtnRc10.Text
fpBtnRc10.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnRc11_Click()
If (AReaction <> "None" And AReaction <> "") Then
    
Else
AReaction = fpBtnRc11.Text
fpBtnRc11.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnSv1_Click()
If (ASeverity <> "None" And ASeverity <> "") Then
    
Else
ASeverity = fpBtnSv1.Text
fpBtnSv1.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnSv2_Click()
If (ASeverity <> "None" And ASeverity <> "") Then
    
Else
ASeverity = fpBtnSv2.Text
fpBtnSv2.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnSv3_Click()
If (ASeverity <> "None" And ASeverity <> "") Then
    
Else
ASeverity = fpBtnSv3.Text
fpBtnSv3.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnSv4_Click()
If (ASeverity <> "None" And ASeverity <> "") Then
    
Else
ASeverity = fpBtnSv4.Text
fpBtnSv4.Enabled = False
Call SetRctSvt
End If
End Sub

Private Sub fpBtnSv5_Click()
If (ASeverity <> "None" And ASeverity <> "") Then
    
Else
ASeverity = fpBtnSv5.Text
fpBtnSv5.Enabled = False
Call SetRctSvt
End If
End Sub
