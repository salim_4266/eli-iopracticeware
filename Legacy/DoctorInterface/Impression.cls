VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CImpression"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Id As String
Public Name As String
Public Eye As String
Public Quant As String
Public SeverityA As String
Public SeverityB As String
Public Active As Boolean
Public IgnoreEye As Boolean
Public RuleOut As Boolean
Public HistoryOf As Boolean
Public NegativeFinding As Boolean
Public ImprOn As Boolean
Public OrderId As Long

Private Const QntRef As String = "["
Private Const QntRefEnd As String = "]"
Private Const CntRef As String = "-NF"
Private Const CntRefSet As String = "!"
Private Const RORef As String = "-RO"
Private Const RORefSet As String = "#"
Private Const HORef As String = "-HO"
Private Const HORefSet As String = "%"

Private Sub Class_Initialize()
    Id = ""
    Name = ""
    Eye = ""
    Quant = ""
    SeverityA = ""
    SeverityB = ""
    Active = False
    IgnoreEye = False
    RuleOut = False
    HistoryOf = False
    NegativeFinding = False
    ImprOn = False
    OrderId = 0
End Sub

Public Function IsSet() As Boolean
    IsSet = Id <> ""
End Function

Public Function Post(iPatientId As Long, iAppointmentId As Long) As Boolean
Dim sSymptom As String
Dim oClinical As PatientClinical

        sSymptom = ""
        If NegativeFinding Then
            sSymptom = sSymptom & CntRefSet
        End If
        If RuleOut Then
            sSymptom = sSymptom & RORefSet
        End If
        If HistoryOf Then
            sSymptom = sSymptom & HORefSet
        End If
        Set oClinical = New PatientClinical
        oClinical.ClinicalId = 0
        If (oClinical.RetrievePatientClinical) Then
            oClinical.AppointmentId = iAppointmentId
            oClinical.PatientId = iPatientId
            oClinical.EyeContext = Eye
            oClinical.ClinicalType = "U"
            oClinical.Symptom = Trim$(str$(OrderId)) & sSymptom & Quant
            oClinical.Findings = Id
            oClinical.ImageInstructions = SeverityA
            oClinical.ImageDescriptor = SeverityB
            oClinical.ClinicalFollowup = ""
            oClinical.ClinicalHighlights = ""
            If Not (oClinical.ApplyPatientClinical) Then
                LogError "CImpression", "Post", vbError, Err.Description, "Impression " & Trim$(Name) & " Not Posted for ApptId = " & Trim$(str$(iAppointmentId)), iPatientId, iAppointmentId
            End If
        End If
        Set oClinical = Nothing
            
End Function

Public Sub SetInactive()
    Active = False
    OrderId = 0
    SeverityA = ""
    SeverityB = ""
End Sub

Public Sub Copy(oImpression As CImpression)
    Me.Id = oImpression.Id
    Me.Name = oImpression.Name
    Me.Eye = oImpression.Eye
    Me.Quant = oImpression.Quant
    Me.SeverityA = oImpression.SeverityA
    Me.SeverityB = oImpression.SeverityB
    Me.Active = oImpression.Active
    Me.IgnoreEye = oImpression.IgnoreEye
    Me.RuleOut = oImpression.RuleOut
    Me.HistoryOf = oImpression.HistoryOf
    Me.NegativeFinding = oImpression.NegativeFinding
    Me.ImprOn = oImpression.ImprOn
    Me.OrderId = oImpression.OrderId
End Sub
