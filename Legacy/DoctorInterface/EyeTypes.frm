VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmEyeTypes 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9165
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12150
   ForeColor       =   &H00008000&
   LinkTopic       =   "Form1"
   Picture         =   "EyeTypes.frx":0000
   ScaleHeight     =   9165
   ScaleWidth      =   12150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstSelect 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   900
      ItemData        =   "EyeTypes.frx":36C9
      Left            =   4680
      List            =   "EyeTypes.frx":36CB
      TabIndex        =   22
      Top             =   7800
      Width           =   3855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary1 
      Height          =   735
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":36CD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary2 
      Height          =   735
      Left            =   240
      TabIndex        =   1
      Top             =   840
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":38B7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary3 
      Height          =   735
      Left            =   240
      TabIndex        =   2
      Top             =   1560
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":3AA1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary4 
      Height          =   735
      Left            =   240
      TabIndex        =   3
      Top             =   2280
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":3C88
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary5 
      Height          =   735
      Left            =   240
      TabIndex        =   4
      Top             =   3000
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":3E72
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary6 
      Height          =   735
      Left            =   240
      TabIndex        =   5
      Top             =   3720
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":4062
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond1 
      Height          =   1095
      Left            =   3360
      TabIndex        =   6
      Top             =   120
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":424F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond2 
      Height          =   1095
      Left            =   3360
      TabIndex        =   7
      Top             =   1320
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":4435
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond3 
      Height          =   1095
      Left            =   3360
      TabIndex        =   8
      Top             =   3720
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":4616
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond4 
      Height          =   1095
      Left            =   3360
      TabIndex        =   9
      Top             =   4920
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":4806
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond5 
      Height          =   1095
      Left            =   3360
      TabIndex        =   10
      Top             =   6120
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":49F6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond6 
      Height          =   1095
      Left            =   6120
      TabIndex        =   11
      Top             =   120
      Width           =   2640
      _Version        =   131072
      _ExtentX        =   4657
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":4BE3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond7 
      Height          =   1110
      Left            =   6120
      TabIndex        =   12
      Top             =   1320
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":4DD0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond8 
      Height          =   1110
      Left            =   6120
      TabIndex        =   13
      Top             =   2520
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":4FB8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond9 
      Height          =   1110
      Left            =   6120
      TabIndex        =   14
      Top             =   3720
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":51A0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond10 
      Height          =   1095
      Left            =   6120
      TabIndex        =   15
      Top             =   4920
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":5389
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10320
      TabIndex        =   16
      Top             =   7800
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":5568
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary8 
      Height          =   735
      Left            =   240
      TabIndex        =   17
      Top             =   5280
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":5747
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary7 
      Height          =   855
      Left            =   240
      TabIndex        =   18
      Top             =   4440
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":5934
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary9 
      Height          =   735
      Left            =   240
      TabIndex        =   19
      Top             =   6000
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":5B24
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond11 
      Height          =   1095
      Left            =   3360
      TabIndex        =   20
      Top             =   2520
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":5D05
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNoLoc 
      Height          =   990
      Left            =   8640
      TabIndex        =   21
      Top             =   7800
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":5EE5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary10 
      Height          =   735
      Left            =   240
      TabIndex        =   23
      Top             =   6720
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":60D0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrimary11 
      Height          =   735
      Left            =   240
      TabIndex        =   24
      Top             =   7440
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":62BC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond12 
      Height          =   1095
      Left            =   8880
      TabIndex        =   25
      Top             =   120
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":64A6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond13 
      Height          =   1095
      Left            =   8880
      TabIndex        =   26
      Top             =   1320
      Width           =   2640
      _Version        =   131072
      _ExtentX        =   4657
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":6698
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond14 
      Height          =   1095
      Left            =   8880
      TabIndex        =   27
      Top             =   2520
      Width           =   2640
      _Version        =   131072
      _ExtentX        =   4657
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":688A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSecond15 
      Height          =   1095
      Left            =   8880
      TabIndex        =   28
      Top             =   3720
      Width           =   2640
      _Version        =   131072
      _ExtentX        =   4657
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EyeTypes.frx":6A7A
   End
End
Attribute VB_Name = "frmEyeTypes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TheResults As String
Private TotalCount As Integer
Private SetBackColor As Long
Private SetForeColor As Long
Private BaseBackColor As Long
Private BaseForeColor As Long
Private AdminList As String

Private Sub cmdDone_Click()
Dim i As Integer
For i = 0 To lstSelect.ListCount - 1
    TheResults = TheResults + lstSelect.List(i)
    If (i <> lstSelect.ListCount - 1) Then
        TheResults = TheResults + " and "
    End If
Next i
Unload frmEyeTypes
End Sub

Private Sub cmdNoLoc_Click()
TheResults = "N/A"
Unload frmEyeTypes
End Sub

Private Sub cmdPrimary1_Click()
Call SetItem(cmdPrimary1)
End Sub

Private Sub cmdPrimary2_Click()
Call SetItem(cmdPrimary2)
End Sub

Private Sub cmdPrimary3_Click()
Call SetItem(cmdPrimary3)
End Sub

Private Sub cmdPrimary4_Click()
Call SetItem(cmdPrimary4)
End Sub

Private Sub cmdPrimary5_Click()
Call SetItem(cmdPrimary5)
End Sub

Private Sub cmdPrimary6_Click()
Call SetItem(cmdPrimary6)
End Sub

Private Sub cmdPrimary7_Click()
Call SetItem(cmdPrimary7)
End Sub

Private Sub cmdPrimary8_Click()
Call SetItem(cmdPrimary8)
End Sub

Private Sub cmdPrimary9_Click()
Call SetItem(cmdPrimary9)
End Sub

Private Sub cmdPrimary10_Click()
Call SetItem(cmdPrimary10)
End Sub

Private Sub cmdPrimary11_Click()
Call SetItem(cmdPrimary11)
End Sub

Private Sub cmdSecond1_Click()
Call SetItem(cmdSecond1)
End Sub

Private Sub cmdSecond2_Click()
Call SetItem(cmdSecond2)
End Sub

Private Sub cmdSecond3_Click()
Call SetItem(cmdSecond3)
End Sub

Private Sub cmdSecond4_Click()
Call SetItem(cmdSecond4)
End Sub

Private Sub cmdSecond5_Click()
Call SetItem(cmdSecond5)
End Sub

Private Sub cmdSecond6_Click()
Call SetItem(cmdSecond6)
End Sub

Private Sub cmdSecond7_Click()
Call SetItem(cmdSecond7)
End Sub

Private Sub cmdSecond8_Click()
Call SetItem(cmdSecond8)
End Sub

Private Sub cmdSecond9_Click()
Call SetItem(cmdSecond9)
End Sub

Private Sub cmdSecond10_Click()
Call SetItem(cmdSecond10)
End Sub

Private Sub cmdSecond11_Click()
Call SetItem(cmdSecond11)
End Sub

Private Sub cmdSecond12_Click()
Call SetItem(cmdSecond12)
End Sub

Private Sub cmdSecond13_Click()
Call SetItem(cmdSecond13)
End Sub

Private Sub cmdSecond14_Click()
Call SetItem(cmdSecond14)
End Sub

Private Sub cmdSecond15_Click()
Call SetItem(cmdSecond15)
End Sub

Private Sub SetItem(AButton As fpBtn)
If (AButton.BackColor = BaseBackColor) Then
    If (TotalCount < 6) Then
        AButton.BackColor = SetBackColor
        AButton.ForeColor = SetForeColor
        AdminList = Trim(Trim(AdminList) + " " + Trim(AButton.Text))
        If (InStrPS(AButton.Name, "Second") > 0) Then
            lstSelect.AddItem AdminList
            TotalCount = lstSelect.ListCount
            AdminList = ""
            Call ClearAll
            Call TurnPrimaryOn
            Call TurnSecond(False)
        Else
            Call TurnPrimaryOff
            Call TurnSecond(True)
        End If
    Else
        frmEventMsgs.Header = "Only 6 Locations Allowed"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseForeColor
    If (InStrPS(AButton.Name, "Primary") > 0) Then
        Call TurnPrimaryOn
        Call TurnSecond(False)
    End If
    AdminList = ""
End If
End Sub

Private Sub Form_Load()
BaseBackColor = cmdPrimary1.BackColor
BaseForeColor = cmdPrimary1.ForeColor
SetBackColor = 14745312
SetForeColor = 0
cmdDone.Visible = True
cmdNoLoc.Visible = True
TotalCount = 0
TheResults = ""
AdminList = ""
lstSelect.Clear
Call TurnPrimaryOn
Call TurnSecond(False)
End Sub

Private Sub ClearAll()
cmdPrimary1.BackColor = BaseBackColor
cmdPrimary1.ForeColor = BaseForeColor
cmdPrimary2.BackColor = BaseBackColor
cmdPrimary2.ForeColor = BaseForeColor
cmdPrimary3.BackColor = BaseBackColor
cmdPrimary3.ForeColor = BaseForeColor
cmdPrimary4.BackColor = BaseBackColor
cmdPrimary4.ForeColor = BaseForeColor
cmdPrimary5.BackColor = BaseBackColor
cmdPrimary5.ForeColor = BaseForeColor
cmdPrimary6.BackColor = BaseBackColor
cmdPrimary6.ForeColor = BaseForeColor
cmdPrimary7.BackColor = BaseBackColor
cmdPrimary7.ForeColor = BaseForeColor
cmdPrimary8.BackColor = BaseBackColor
cmdPrimary8.ForeColor = BaseForeColor
cmdPrimary9.BackColor = BaseBackColor
cmdPrimary9.ForeColor = BaseForeColor
cmdSecond1.BackColor = BaseBackColor
cmdSecond1.ForeColor = BaseForeColor
cmdSecond2.BackColor = BaseBackColor
cmdSecond2.ForeColor = BaseForeColor
cmdSecond3.BackColor = BaseBackColor
cmdSecond3.ForeColor = BaseForeColor
cmdSecond4.BackColor = BaseBackColor
cmdSecond4.ForeColor = BaseForeColor
cmdSecond5.BackColor = BaseBackColor
cmdSecond5.ForeColor = BaseForeColor
cmdSecond6.BackColor = BaseBackColor
cmdSecond6.ForeColor = BaseForeColor
cmdSecond7.BackColor = BaseBackColor
cmdSecond7.ForeColor = BaseForeColor
cmdSecond8.BackColor = BaseBackColor
cmdSecond8.ForeColor = BaseForeColor
cmdSecond9.BackColor = BaseBackColor
cmdSecond9.ForeColor = BaseForeColor
cmdSecond10.BackColor = BaseBackColor
cmdSecond10.ForeColor = BaseForeColor
cmdSecond11.BackColor = BaseBackColor
cmdSecond11.ForeColor = BaseForeColor
cmdSecond12.BackColor = BaseBackColor
cmdSecond12.ForeColor = BaseForeColor
cmdSecond13.BackColor = BaseBackColor
cmdSecond13.ForeColor = BaseForeColor
cmdSecond14.BackColor = BaseBackColor
cmdSecond14.ForeColor = BaseForeColor
cmdSecond15.BackColor = BaseBackColor
cmdSecond15.ForeColor = BaseForeColor
End Sub

Private Sub TurnPrimaryOn()
cmdPrimary1.Enabled = True
cmdPrimary2.Enabled = True
cmdPrimary3.Enabled = True
cmdPrimary4.Enabled = True
cmdPrimary5.Enabled = True
cmdPrimary6.Enabled = True
cmdPrimary7.Enabled = True
cmdPrimary8.Enabled = True
cmdPrimary9.Enabled = True
End Sub

Private Sub TurnPrimaryOff()
If (cmdPrimary1.BackColor <> SetBackColor) Then
    cmdPrimary1.Enabled = False
End If
If (cmdPrimary2.BackColor <> SetBackColor) Then
    cmdPrimary2.Enabled = False
End If
If (cmdPrimary3.BackColor <> SetBackColor) Then
    cmdPrimary3.Enabled = False
End If
If (cmdPrimary4.BackColor <> SetBackColor) Then
    cmdPrimary4.Enabled = False
End If
If (cmdPrimary5.BackColor <> SetBackColor) Then
    cmdPrimary5.Enabled = False
End If
If (cmdPrimary6.BackColor <> SetBackColor) Then
    cmdPrimary6.Enabled = False
End If
If (cmdPrimary7.BackColor <> SetBackColor) Then
    cmdPrimary7.Enabled = False
End If
If (cmdPrimary8.BackColor <> SetBackColor) Then
    cmdPrimary8.Enabled = False
End If
If (cmdPrimary9.BackColor <> SetBackColor) Then
    cmdPrimary9.Enabled = False
End If
End Sub

Private Sub TurnSecond(ViewOn As Boolean)
cmdSecond1.Enabled = ViewOn
cmdSecond2.Enabled = ViewOn
cmdSecond3.Enabled = ViewOn
cmdSecond4.Enabled = ViewOn
cmdSecond5.Enabled = ViewOn
cmdSecond6.Enabled = ViewOn
cmdSecond7.Enabled = ViewOn
cmdSecond8.Enabled = ViewOn
cmdSecond9.Enabled = ViewOn
cmdSecond10.Enabled = ViewOn
cmdSecond11.Enabled = ViewOn
cmdSecond12.Enabled = ViewOn
cmdSecond13.Enabled = ViewOn
cmdSecond14.Enabled = ViewOn
cmdSecond15.Enabled = ViewOn
End Sub

Private Sub lstSelect_Click()
If (lstSelect.ListIndex >= 0) Then
    frmEventMsgs.Header = "Remove ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Remove"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        lstSelect.RemoveItem lstSelect.ListIndex
        TotalCount = lstSelect.ListCount - 1
    End If
End If
End Sub
