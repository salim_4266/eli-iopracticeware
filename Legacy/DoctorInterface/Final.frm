VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmFinal 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstBilled 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2280
      ItemData        =   "Final.frx":0000
      Left            =   6000
      List            =   "Final.frx":0002
      MultiSelect     =   1  'Simple
      TabIndex        =   8
      Top             =   2640
      Width           =   5775
   End
   Begin VB.ListBox lstTests 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2280
      ItemData        =   "Final.frx":0004
      Left            =   120
      List            =   "Final.frx":0006
      MultiSelect     =   1  'Simple
      TabIndex        =   7
      Top             =   2640
      Width           =   5775
   End
   Begin VB.ListBox lstFinding 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2055
      ItemData        =   "Final.frx":0008
      Left            =   120
      List            =   "Final.frx":000A
      MultiSelect     =   1  'Simple
      TabIndex        =   2
      Top             =   480
      Width           =   5775
   End
   Begin VB.ListBox lstActions 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2955
      ItemData        =   "Final.frx":000C
      Left            =   120
      List            =   "Final.frx":000E
      MultiSelect     =   1  'Simple
      TabIndex        =   1
      Top             =   5040
      Width           =   11655
   End
   Begin VB.ListBox lstImp 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2055
      ItemData        =   "Final.frx":0010
      Left            =   6000
      List            =   "Final.frx":0012
      TabIndex        =   0
      Top             =   480
      Width           =   5775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   9720
      TabIndex        =   3
      Top             =   8040
      Width           =   1995
      _Version        =   131072
      _ExtentX        =   3519
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Final.frx":0014
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPlan 
      Height          =   975
      Left            =   7200
      TabIndex        =   4
      Top             =   8040
      Width           =   1995
      _Version        =   131072
      _ExtentX        =   3519
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Final.frx":025B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   975
      Left            =   240
      TabIndex        =   6
      Top             =   8040
      Width           =   1995
      _Version        =   131072
      _ExtentX        =   3519
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Final.frx":04A0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpr 
      Height          =   975
      Left            =   2640
      TabIndex        =   9
      Top             =   8040
      Width           =   1995
      _Version        =   131072
      _ExtentX        =   3519
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Final.frx":06E5
   End
   Begin VB.Label lblName 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Final Review"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   1470
   End
   Begin VB.Label lblImp 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Final Review"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   5280
      TabIndex        =   5
      Top             =   120
      Width           =   1470
   End
End
Attribute VB_Name = "frmFinal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Result As Integer
Public PatientID As Long
Public AppointmentId As Long
Public ActivityId As Long
Public ResourceName As String
Private ViewOn As Boolean

Private Sub cmdDone_Click()
Call SetHighlights
Result = 0
Unload frmFinal
End Sub

Private Sub SetHighlights()
Dim i As Integer
Dim k As Integer
Dim Id As String
Dim RetNotes As PatientNotes
For i = 0 To lstBilled.ListCount - 1
    If (Len(lstBilled.List(i)) > 100) Then
        If (lstBilled.Selected(i)) Then
            Id = Mid(lstBilled.List(i), 102, Len(lstBilled.List(i)) - 101)
            If (Mid(lstBilled.List(i), 101, 1) = "D") Then
                Call frmSetProcedures.SetDiagnosisHighlight(Id, True)
            ElseIf (Mid(lstBilled.List(i), 101, 1) = "P") Then
                Call frmSetProcedures.SetProcedureHighlight(Id, True)
            ElseIf (Mid(lstBilled.List(i), 101, 1) = "E") Then
                Call frmSetEMProcedure.SetEMProcedureHighlight(True)
            End If
        Else
            Id = Mid(lstBilled.List(i), 102, Len(lstBilled.List(i)) - 101)
            If (Mid(lstBilled.List(i), 101, 1) = "D") Then
                Call frmSetProcedures.SetDiagnosisHighlight(Id, False)
            ElseIf (Mid(lstBilled.List(i), 101, 1) = "P") Then
                Call frmSetProcedures.SetProcedureHighlight(Id, False)
            ElseIf (Mid(lstBilled.List(i), 101, 1) = "E") Then
                Call frmSetEMProcedure.SetEMProcedureHighlight(False)
            End If
        End If
    End If
Next i
Call frmSetProcedures.RetainBilledDiagnosis(PatientID)
Call frmSetProcedures.RetainBilledProcedures(PatientID)
Call frmSetEMProcedure.RetainEMProcedure(PatientID)
For i = 0 To lstFinding.ListCount - 1
    If (Len(lstFinding.List(i)) > 78) Then
        Id = Trim(Mid(lstFinding.List(i), 77, Len(lstFinding.List(i)) - 76))
        If (lstFinding.Selected(i)) Then
            Call frmSystems1.SetFindingHighlight(Left(lstFinding.List(i), 2), Id, True)
        Else
            Call frmSystems1.SetFindingHighlight(Left(lstFinding.List(i), 2), Id, False)
        End If
    ElseIf (Len(lstFinding.List(i)) > 65) Then
        Id = Trim(Mid(lstFinding.List(i), 66, Len(lstFinding.List(i)) - 65))
        If (lstFinding.Selected(i)) Then
            Call frmSystems1.SetFindingHighlight(Left(lstFinding.List(i), 2), Id, True)
        Else
            Call frmSystems1.SetFindingHighlight(Left(lstFinding.List(i), 2), Id, False)
        End If
    End If
Next i
Call frmSystems1.RetainDiagList(PatientID)
For i = 0 To lstActions.ListCount - 1
    If (Len(lstActions.List(i)) > 110) Then
        If (Mid(lstActions.List(i), 111, 1) = "-") Then
            Id = Mid(lstActions.List(i), 112, Len(lstActions.List(i)) - 111)
            Set RetNotes = New PatientNotes
            RetNotes.NotesId = Val(Id)
            If (RetNotes.RetrieveNotes) Then
                RetNotes.NotesHighlight = "V"
                Call RetNotes.ApplyNotes
            End If
            Set RetNotes = Nothing
        ElseIf (Mid(lstActions.List(i), 110, 1) = "-") Then
            Id = Mid(lstActions.List(i), 111, Len(lstActions.List(i)) - 110)
            Set RetNotes = New PatientNotes
            RetNotes.NotesId = Val(Id)
            If (RetNotes.RetrieveNotes) Then
                RetNotes.NotesHighlight = "V"
                Call RetNotes.ApplyNotes
            End If
            Set RetNotes = Nothing
        Else
            Id = Mid(lstActions.List(i), 111, Len(lstActions.List(i)) - 110)
            If (Len(Id) < 3) Then
                If (lstActions.Selected(i)) Then
                    Call frmEvaluation.SetActionHighlight(Val(Id), True)
                Else
                    Call frmEvaluation.SetActionHighlight(Val(Id), False)
                End If
            End If
        End If
    End If
Next i
Call frmEvaluation.PostToFile(PatientID)
For i = 0 To lstTests.ListCount - 1
    If (Len(lstTests.List(i)) > 70) Then
        Id = Mid(lstTests.List(i), 76, Len(lstTests.List(i)) - 75)
        If (lstTests.Selected(i)) Then
            Call frmSystems1.SetTestHighlight(Val(Id), True)
        Else
            Call frmSystems1.SetTestHighlight(Val(Id), False)
        End If
    End If
Next i
For i = 0 To lstTests.ListCount - 1
    If (lstTests.Selected(i)) Then
        If (Len(lstTests.List(i)) > 75) Then
            k = Val(Mid(lstTests.List(i), 76, Len(lstTests.List(i)) - 75))
            If (k > 0) Then
                Call frmSystems1.SetTestHighlight(k, True)
            End If
        End If
    End If
Next i
End Sub

Private Sub cmdImpr_Click()
Call SetHighlights
frmSetDiagnosis.PatientID = PatientID
frmSetDiagnosis.AppointmentId = AppointmentId
frmSetDiagnosis.ActivityId = ActivityId
frmSetDiagnosis.ResourceName = ResourceName
If (frmSetDiagnosis.LoadImpressions(True, True)) Then
    frmSetDiagnosis.Show 1
    Call frmSetDiagnosis.RetainImpressions(PatientID)
    If (frmSetDiagnosis.ResourceName <> ResourceName) Then
        ResourceName = frmSetDiagnosis.ResourceName
    End If
    Call LoadFinal(ViewOn)
End If
End Sub

Private Sub cmdPlan_Click()
Call SetHighlights
frmEvaluation.PatientID = PatientID
frmEvaluation.AppointmentId = AppointmentId
frmEvaluation.ActivityId = ActivityId
frmEvaluation.ResourceName = ResourceName
If (frmEvaluation.LoadActions(True, False)) Then
    frmEvaluation.Show 1
    If (frmEvaluation.ResourceName <> ResourceName) Then
        ResourceName = frmEvaluation.ResourceName
    End If
    Call frmEvaluation.PostToFile(PatientID)
    If (frmEvaluation.NavigationAction = 99) Or (frmEvaluation.NavigationAction = 2) Then
        Result = -2
        Unload frmFinal
    ElseIf (frmEvaluation.NavigationAction = 90) Then
        Result = -1
        Unload frmFinal
    Else
        Call LoadFinal(ViewOn)
    End If
Else
    Result = -2
    Unload frmFinal
End If
End Sub

Private Sub cmdQuit_Click()
Call SetHighlights
Result = -2
Unload frmFinal
End Sub

Public Function LoadFinal(AView As Boolean)
Dim FirstOn As Boolean, AHigh As Boolean
Dim PrescribedOn As Boolean, RetHigh As Boolean
Dim zz As Integer, Apo As Integer
Dim i As Integer, j As Integer
Dim w As Integer, p As Integer
Dim z As Integer, z1 As Integer
Dim tt As Integer, ListType As Integer
Dim ATemp As String
Dim TestOrder As String, AType As String
Dim TestParty As String, TestTime As String
Dim TestText As String, TestQuestion As String
Dim AId As String, AString As String
Dim AEye As String, ASevA As String, ASevB As String
Dim UTemp As String, PTemp As String, QTemp As String
Dim Temp As String, Temp1 As String, KTemp As String
Dim TheText As String, TheDate As String, RxDate As String
Dim RetQ As String
Dim RetId As String, RetString As String, RetA As String
Dim RetEye As String, RetSevA As String, RetSevB As String
Dim RRul As Boolean, RCnf As Boolean, RHst As Boolean
Dim RetFollow As Boolean
Dim RetNotes As PatientNotes
Dim RetClinical As PatientClinical
Dim RetrieveDiag As DiagnosisMasterPrimary
Dim RetrieveProc As DiagnosisMasterProcedures
LoadFinal = False
ViewOn = AView
TheDate = ""
Call FormatTodaysDate(TheDate, False)
lblName.Caption = frmSystems1.txtSummary.Text
i = InStrPS(lblName.Caption, ",")
If (i > 0) Then
    lblName.Caption = Left(lblName.Caption, i - 1)
End If
lstFinding.Clear
lstFinding.AddItem "Findings"
For i = 0 To frmSystems1.lstOD.ListCount - 1
    lstFinding.AddItem "OD - " + frmSystems1.lstOD.List(i)
    If (Mid(frmSystems1.lstOD.List(i), 69, 1) = "T") Then
        lstFinding.Selected(lstFinding.ListCount - 1) = True
    ElseIf (Mid(frmSystems1.lstOD.List(i), 60, 1) = "T") Then
        lstFinding.Selected(lstFinding.ListCount - 1) = True
    End If
Next i
For i = 0 To frmSystems1.lstOS.ListCount - 1
    lstFinding.AddItem "OS - " + frmSystems1.lstOS.List(i)
    If (Mid(frmSystems1.lstOS.List(i), 69, 1) = "T") Then
        lstFinding.Selected(lstFinding.ListCount - 1) = True
    ElseIf (Mid(frmSystems1.lstOS.List(i), 60, 1) = "T") Then
        lstFinding.Selected(lstFinding.ListCount - 1) = True
    End If
Next i

lstTests.Clear
lstTests.AddItem "Tests"
tt = frmSystems1.GetTotalTests
For i = 1 To tt
    Call frmSystems1.GetTestText(i, TestText, TestQuestion, TestOrder, TestParty, TestTime, AHigh, AType, Apo)
    If (Trim(TestOrder) <> "") Then
        If (Mid(TestText, 1) < " ") Then
            Temp = Trim(Mid(TestText, 3, Len(TestText) - 2))
            Call StripCharacters(Temp, Chr(13))
            Call StripCharacters(Temp, Chr(10))
            j = InStrPS(Temp, ":")
            If (j > 0) Then
                Temp1 = Left(Temp, j)
                Call ReplaceCharacters(Temp, Temp1, " ")
                Temp = Temp1 + Temp
            End If
            j = InStrPS(Temp, "OD:")
            If (j > 0) Then
                UTemp = Left(Temp, j - 1)
                If (Len(UTemp) < 76) Then
                    UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                Else
                    UTemp = Left(UTemp, 75) + Trim(Str(i))
                End If
                lstTests.AddItem UTemp
                If (AHigh) Then
                    lstTests.Selected(lstTests.ListCount - 1) = True
                End If
                Temp = Mid(Temp, j, Len(Temp) - (j - 1))
                j = InStrPS(Temp, "OS:")
                If (j > 0) Then
                    UTemp = Left(Temp, j - 1)
                    If (Len(UTemp) < 76) Then
                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                    Else
                        UTemp = Left(UTemp, 75) + Trim(Str(i))
                    End If
                    lstTests.AddItem UTemp
                    If (AHigh) Then
                        lstTests.Selected(lstTests.ListCount - 1) = True
                    End If
                    UTemp = Mid(Temp, j, Len(Temp) - (j - 1))
                    If (Len(UTemp) < 76) Then
                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                    Else
                        UTemp = Left(UTemp, 75) + Trim(Str(i))
                    End If
                    lstTests.AddItem UTemp
                    If (AHigh) Then
                        lstTests.Selected(lstTests.ListCount - 1) = True
                    End If
                Else
                    UTemp = Temp
                    If (Len(UTemp) < 76) Then
                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                    Else
                        UTemp = Left(UTemp, 75) + Trim(Str(i))
                    End If
                    lstTests.AddItem UTemp
                    If (AHigh) Then
                        lstTests.Selected(lstTests.ListCount - 1) = True
                    End If
                End If
            Else
                UTemp = Temp
                If (Len(UTemp) < 76) Then
                    UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                Else
                    UTemp = Left(UTemp, 75) + Trim(Str(i))
                End If
                lstTests.AddItem UTemp
                If (AHigh) Then
                    lstTests.Selected(lstTests.ListCount - 1) = True
                End If
            End If
            lstTests.AddItem String(30, "_")
        Else
            Temp = Trim(TestText)
            j = InStrPS(Temp, ":")
            If (j > 0) Then
                Temp1 = Left(Temp, j)
                Call ReplaceCharacters(Temp, Temp1, " ")
                Temp = Temp1 + Trim(Temp)
                j = InStrPS(Temp, ";")
                If (j > 0) Then
                    Mid(Temp, j, 1) = ":"
                End If
                j = InStrPS(Temp, "OD:")
                If (j < 1) Then
                    Call StripCharacters(Temp, Chr(13))
                    Call StripCharacters(Temp, Chr(10))
                    UTemp = Temp
                    If (Len(UTemp) < 55) Then
                        If (Len(UTemp) < 76) Then
                            UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                        Else
                            UTemp = Left(UTemp, 75) + Trim(Str(i))
                        End If
                    Else
                        UTemp = Left(UTemp, 54) + Space(21) + Trim(Str(i))
                    End If
                    lstTests.AddItem UTemp
                    If (AHigh) Then
                        lstTests.Selected(lstTests.ListCount - 1) = True
                    End If
                    For j = 55 To Len(Temp) Step 54
                        UTemp = Mid(Temp, j, 54)
                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                        lstTests.AddItem UTemp
                        If (AHigh) Then
                            lstTests.Selected(lstTests.ListCount - 1) = True
                        End If
                    Next j
                Else
                    While (j > 0)
                        KTemp = Left(Temp, j - 1)
                        Call StripCharacters(KTemp, Chr(13))
                        Call StripCharacters(KTemp, Chr(10))
                        UTemp = KTemp
                        If (Len(UTemp) < 76) Then
                            UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                        Else
                            UTemp = Left(UTemp, 75) + Trim(Str(i))
                        End If
                        lstTests.AddItem UTemp
                        If (AHigh) Then
                            lstTests.Selected(lstTests.ListCount - 1) = True
                        End If
                        Temp = Mid(Temp, j, Len(Temp) - (j - 1))
                        j = InStrPS(Temp, "OS:")
                        If (j > 0) Then
                            KTemp = Left(Temp, j - 1)
                            If (TestOrder <> "55A") Then
                                Call StripCharacters(KTemp, Chr(13))
                                Call StripCharacters(KTemp, Chr(10))
                                UTemp = KTemp
                                If (Len(UTemp) < 55) Then
                                    If (Len(UTemp) < 76) Then
                                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                                    Else
                                        UTemp = Left(UTemp, 75) + Trim(Str(i))
                                    End If
                                    lstTests.AddItem UTemp
                                    If (AHigh) Then
                                        lstTests.Selected(lstTests.ListCount - 1) = True
                                    End If
                                Else
                                    For zz = 1 To Len(UTemp) Step 54
                                        Temp1 = Mid(UTemp, zz, 54)
                                        If (Len(Temp1) < 54) Then
                                            Temp1 = Temp1 + Space(54 - Len(Temp1)) + Space(21) + Trim(Str(i))
                                        Else
                                            Temp1 = Mid(UTemp, zz, 54) + Space(21) + Trim(Str(i))
                                        End If
                                        lstTests.AddItem Temp1
                                        If (AHigh) Then
                                            lstTests.Selected(lstTests.ListCount - 1) = True
                                        End If
                                    Next zz
                                End If
                                Temp = Mid(Temp, j, Len(Temp) - (j - 1))
                                j = InStrPS(Temp, vbCrLf)
                                If (j > 0) Then
                                    While (j > 0)
                                        KTemp = Left(Temp, j - 1)
                                        Call StripCharacters(KTemp, Chr(13))
                                        Call StripCharacters(KTemp, Chr(10))
                                        UTemp = KTemp
                                        If (Len(UTemp) < 76) Then
                                            UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                                        Else
                                            UTemp = Left(UTemp, 75) + Trim(Str(i))
                                        End If
                                        lstTests.AddItem UTemp
                                        If (AHigh) Then
                                            lstTests.Selected(lstTests.ListCount - 1) = True
                                        End If
                                        Temp = Mid(Temp, j + 2, Len(Temp) - (j + 1))
                                        j = InStrPS(Temp, vbCrLf)
                                    Wend
                                    If (Len(Temp) <> 0) Then
                                        KTemp = Left(Temp, Len(Temp))
                                        Call StripCharacters(KTemp, Chr(13))
                                        Call StripCharacters(KTemp, Chr(10))
                                        UTemp = KTemp
                                        If (Len(UTemp) < 76) Then
                                            UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                                        Else
                                            UTemp = Left(UTemp, 75) + Trim(Str(i))
                                        End If
                                        lstTests.AddItem UTemp
                                        If (AHigh) Then
                                            lstTests.Selected(lstTests.ListCount - 1) = True
                                        End If
                                    End If
                                Else
                                    KTemp = Temp
                                    Call StripCharacters(KTemp, Chr(13))
                                    Call StripCharacters(KTemp, Chr(10))
                                    UTemp = KTemp
                                    If (Len(UTemp) < 76) Then
                                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                                        lstTests.AddItem UTemp
                                        If (AHigh) Then
                                            lstTests.Selected(lstTests.ListCount - 1) = True
                                        End If
                                        Temp = Mid(Temp, j + 2, Len(Temp) - (j + 1))
                                    Else
                                        For zz = 1 To Len(UTemp) Step 54
                                            Temp1 = Mid(UTemp, zz, 54)
                                            If (Len(Temp1) < 54) Then
                                                Temp1 = Temp1 + Space(54 - Len(Temp1)) + Space(21) + Trim(Str(i))
                                            Else
                                                Temp1 = Mid(UTemp, zz, 54) + Space(21) + Trim(Str(i))
                                            End If
                                            lstTests.AddItem Temp1
                                            If (AHigh) Then
                                                lstTests.Selected(lstTests.ListCount - 1) = True
                                            End If
                                        Next zz
                                        If (AHigh) Then
                                            lstTests.Selected(lstTests.ListCount - 1) = True
                                        End If
                                    End If
                                End If
                            Else
                                p = 1
                                w = InStrPS(KTemp, vbCrLf)
                                While (w > 0)
                                    UTemp = Mid(KTemp, p, w - 1)
                                    If (Len(UTemp) < 76) Then
                                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                                    Else
                                        UTemp = Left(UTemp, 75) + Trim(Str(i))
                                    End If
                                    lstTests.AddItem UTemp
                                    If (AHigh) Then
                                        lstTests.Selected(lstTests.ListCount - 1) = True
                                    End If
                                    p = w + 2
                                    KTemp = Mid(KTemp, p, Len(KTemp) - (w + 1))
                                    w = InStrPS(p, KTemp, vbCrLf)
                                Wend
                                If (Len(KTemp) > 2) Then
                                    Call StripCharacters(KTemp, Chr(13))
                                    Call StripCharacters(KTemp, Chr(10))
                                    UTemp = KTemp
                                    If (Len(UTemp) < 76) Then
                                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                                    Else
                                        UTemp = Left(UTemp, 75) + Trim(Str(i))
                                    End If
                                    lstTests.AddItem UTemp
                                    If (AHigh) Then
                                        lstTests.Selected(lstTests.ListCount - 1) = True
                                    End If
                                    p = w + 2
                                    KTemp = Mid(KTemp, w + 2, Len(KTemp) - (w + 1))
                                    w = InStrPS(p, KTemp, vbCrLf)
                                End If
                                Temp = Mid(Temp, j, Len(Temp) - (j - 1))
                                j = Len(Temp) + 1
                                KTemp = Left(Temp, j - 1)
                                p = 1
                                w = InStrPS(KTemp, vbCrLf)
                                While (w > 0)
                                    UTemp = Mid(KTemp, p, w - 1)
                                    If (Len(UTemp) < 76) Then
                                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                                    Else
                                        UTemp = Left(UTemp, 75) + Trim(Str(i))
                                    End If
                                    lstTests.AddItem UTemp
                                    If (AHigh) Then
                                        lstTests.Selected(lstTests.ListCount - 1) = True
                                    End If
                                    p = w + 2
                                    KTemp = Mid(KTemp, p, Len(KTemp) - (w + 1))
                                    w = InStrPS(p, KTemp, vbCrLf)
                                Wend
                                If (Len(KTemp) > 2) Then
                                    Call StripCharacters(KTemp, Chr(13))
                                    Call StripCharacters(KTemp, Chr(10))
                                    UTemp = KTemp
                                    If (Len(UTemp) < 76) Then
                                        UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                                    Else
                                        UTemp = Left(UTemp, 75) + Trim(Str(i))
                                    End If
                                    lstTests.AddItem UTemp
                                    If (AHigh) Then
                                        lstTests.Selected(lstTests.ListCount - 1) = True
                                    End If
                                    p = w + 2
                                    KTemp = Mid(KTemp, w + 2, Len(KTemp) - (w + 1))
                                    w = InStrPS(p, KTemp, vbCrLf)
                                End If
                            End If
                        Else
                            UTemp = Temp
                            If (Len(UTemp) < 76) Then
                                UTemp = UTemp + Space(75 - Len(UTemp)) + Trim(Str(i))
                            Else
                                UTemp = Left(UTemp, 75) + Trim(Str(i))
                            End If
                            lstTests.AddItem UTemp
                            If (AHigh) Then
                                lstTests.Selected(lstTests.ListCount - 1) = True
                            End If
                            Temp = ""
                        End If
                        j = InStrPS(Temp, "OD:")
                    Wend
                End If
                lstTests.AddItem String(30, "_")
            End If
        End If
        Set RetNotes = New PatientNotes
        RetNotes.NotesPatientId = PatientID
        RetNotes.NotesAppointmentId = AppointmentId
        RetNotes.NotesType = "C"
        RetNotes.NotesSystem = "T" + Trim(TestOrder)
        If (RetNotes.FindNotes > 0) Then
            w = 1
            While (RetNotes.SelectNotes(w))
                Temp = ""
                If (Trim(RetNotes.NotesSystem) = "") Then
                    If (Trim(RetNotes.NotesText1) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText1)
                    End If
                    If (Trim(RetNotes.NotesText2) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText2)
                    End If
                    If (Trim(RetNotes.NotesText3) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText3)
                    End If
                    If (Trim(RetNotes.NotesText4) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText4)
                    End If
                End If
                Call StripCharacters(Temp, Chr(13))
                Call StripCharacters(Temp, Chr(10))
                If (Trim(Temp) <> "") Then
                    For tt = 1 To Len(Temp) Step 50
                        QTemp = Mid(Temp, tt, 50)
                        ListType = 1
                        GoSub WrapIt
                    Next tt
                End If
                w = w + 1
            Wend
        End If
        Set RetNotes = Nothing
    End If
Next i

lstBilled.Clear
lstBilled.AddItem "Diagnosis for billing"
Temp = ""
Temp1 = ""
Set RetrieveDiag = New DiagnosisMasterPrimary
j = frmSetProcedures.TotalBilledDiagnosis
For i = 1 To j
    If (frmSetProcedures.GetBilledDiagnosis(i, RetId, RetString, RetEye, RetSevA, RetSevB, RetHigh)) Then
        Temp = Space(100)
        RetrieveDiag.PrimarySystem = ""
        RetrieveDiag.PrimaryDiagnosis = ""
        RetrieveDiag.PrimaryNextLevelDiagnosis = Trim(RetId)
        RetrieveDiag.PrimaryLevel = 0
        RetrieveDiag.PrimaryRank = 0
        RetrieveDiag.PrimaryBilling = False
        If (RetrieveDiag.FindPrimarybyDiagnosis > 0) Then
            If (RetrieveDiag.SelectPrimary(1)) Then
                If (Trim(RetrieveDiag.PrimaryLingo) <> "") Then
                    TheText = RetrieveDiag.PrimaryLingo
                Else
                    TheText = RetrieveDiag.PrimaryName
                End If
                Temp1 = RetId
                z = InStrPS(Temp1, ".")
                If (z > 0) Then
                    z1 = InStrPS(z + 1, Temp1, ".")
                    If (z1 > 0) Then
                        Temp1 = Left(RetId, z1 - 1)
                    End If
                End If
                Mid(Temp, 1, Len(Trim(Temp1)) + 1 + Len(Trim(TheText))) = Trim(Temp1) + " " + Trim(TheText)
                Temp = Temp + "D" + Trim(RetId)
                lstBilled.AddItem Temp
                If (RetHigh) Then
                    lstBilled.Selected(lstBilled.ListCount - 1) = True
                End If
            End If
        End If
    End If
Next i
Set RetrieveDiag = Nothing
        
lstBilled.AddItem "_______________________"
lstBilled.AddItem "Procedures to be billed"
Temp = ""
Set RetrieveProc = New DiagnosisMasterProcedures
j = frmSetProcedures.TotalBillingProcedure
For i = 1 To j
    If (frmSetProcedures.GetBillingProcedure(i, RetId, RetString, RetEye, RetSevA, RetHigh)) Then
        Temp = Space(100)
        RetrieveProc.ProcedureCPT = RetId
        RetrieveProc.ProcedureDiagnosis = ""
        RetrieveProc.ProcedureOrder = 0
        RetrieveProc.ProcedureRank = 0
        RetrieveProc.ProcedureLinkType = ""
        If (RetrieveProc.FindProcedurebyNumeric > 0) Then
            If (RetrieveProc.SelectProcedure(1)) Then
                TheText = Trim(RetrieveProc.ProcedureName)
                If (RetString = "?") Then
                    If (Trim(RetSevA) <> "") Then
                        Call frmSetProcedures.GetBilledDiagnosis(Val(Left(RetSevA, 1)), AId, AString, AEye, ASevA, ASevB, AHigh)
                    Else
                        Call frmSetProcedures.GetBilledDiagnosis(1, AId, AString, AEye, ASevA, ASevB, AHigh)
                    End If
                    Mid(Temp, 1, Len(Trim(RetId) + " " + Trim(AId) + " " + Trim(RetEye) + " " + Trim(RetSevA) + " " + Trim(TheText))) = Trim(RetId) + " " + Trim(AId) + " " + Trim(RetEye) + " " + Trim(RetSevA) + " " + Trim(TheText)
                Else
                    Mid(Temp, 1, Len(Trim(RetId) + " " + Trim(RetString) + " " + Trim(RetEye) + " " + Trim(RetSevA) + " " + Trim(TheText))) = Trim(RetId) + " " + Trim(RetString) + " " + Trim(RetEye) + " " + Trim(RetSevA) + " " + Trim(TheText)
                End If
                Temp = Temp + "P" + Trim(RetId)
                lstBilled.AddItem Temp
                If (RetHigh) Then
                    lstBilled.Selected(lstBilled.ListCount - 1) = True
                End If
            End If
        End If
    End If
Next i
Set RetrieveProc = Nothing

Temp = ""
Set RetrieveProc = New DiagnosisMasterProcedures
If (frmSetEMProcedure.GetEMProcedure(RetId, RetString, RetEye, RetHigh)) Then
    Temp = Space(100)
    RetrieveProc.ProcedureCPT = RetId
    RetrieveProc.ProcedureDiagnosis = ""
    RetrieveProc.ProcedureOrder = 0
    RetrieveProc.ProcedureRank = 0
    If (RetrieveProc.FindProcedurebyEM > 0) Then
        If (RetrieveProc.SelectProcedure(1)) Then
            TheText = Trim(RetrieveProc.ProcedureName)
            Mid(Temp, 1, Len(Trim(RetId) + " " + Trim(RetString) + " " + Trim(RetEye) + " " + Trim(TheText))) = Trim(RetId) + " " + Trim(RetString) + " " + Trim(RetEye) + " " + Trim(TheText)
            Temp = Temp + "E" + Trim(RetId)
            lstBilled.AddItem Temp
            If (RetHigh) Then
                lstBilled.Selected(lstBilled.ListCount - 1) = True
            End If
        End If
    End If
End If
Set RetrieveProc = Nothing

lstImp.Clear
lstImp.AddItem "Impressions"
j = frmSetDiagnosis.TotalImpressions
For i = 1 To j
    If (frmSetDiagnosis.GetImpressions(i, RetId, RetString, RetEye, RetSevA, RetSevB, RetA, RetQ, RCnf, RHst, RRul)) Then
        Temp = ""
        If (RetA = "T") Then
            If (Trim(RetSevA) <> "") Then
                Temp = "OD " + RetSevA
            End If
            If (Trim(RetSevB) <> "") Then
                Temp = Temp + " OS " + RetSevB
            End If
            If (Left(RetId, 1) = "-") Then
                RetId = ""
            End If
            ATemp = ""
            If (RCnf) Then
                ATemp = ATemp + "NF-"
            End If
            If (RHst) Then
                ATemp = ATemp + "HO-"
            End If
            If (RRul) Then
                ATemp = ATemp + "RO-"
            End If
            ATemp = ATemp + Trim(RetQ)
            lstImp.AddItem RetEye + " " + ATemp + Trim(RetString) + " " + Temp
        End If
    End If
Next i

lstActions.Clear
lstActions.AddItem "Plan"
j = frmEvaluation.TotalActionEvaluation
For i = 1 To j
    If (frmEvaluation.GetActionEvaluation(i, RetString, RetHigh, RetFollow)) Then
        Call frmSystems1.BuildEvaluationLine(Trim(RetString), TheText, PrescribedOn)
        If (Trim(TheText) <> "") Then
            Temp = ""
            FirstOn = True
            If (UCase(Left(RetString, 2)) <> "RX") Then
                PrescribedOn = True
            ElseIf (UCase(Left(RetString, 2)) = "RX") Then
                If (PrescribedOn) Then
                    RetHigh = True
                    Call frmEvaluation.SetActionHighlight(i, True)
                End If
            End If
            Call ReplaceCharacters(TheText, "-0/", " ")
            Call ReplaceCharacters(TheText, "-1/", " ")
            Call ReplaceCharacters(TheText, "-2/", " ")
            Call ReplaceCharacters(TheText, "-3/", " ")
            Call ReplaceCharacters(TheText, "-4/", " ")
            Call ReplaceCharacters(TheText, "-5/", " ")
            Call ReplaceCharacters(TheText, "-6/", " ")
            Call ReplaceCharacters(TheText, "P-7/", " ")
            Call ReplaceCharacters(TheText, "-8/", " ")
            Call ReplaceCharacters(TheText, "/", " ")
            Call ReplaceCharacters(TheText, "--", " ")
            Call ReplaceCharacters(TheText, "  ", " ")
            If (UCase(Left(TheText, 2)) = "RX") Then
                TheText = Mid(TheText, 4, Len(TheText) - 3)
                z1 = InStrPS(TheText, "[Last")
                If (z1 > 0) Then
                    z = InStrPS(z1, TheText, "]")
                    If (z > 0) Then
                        TheText = Left(TheText, z1 - 1) + Mid(TheText, z + 1, Len(TheText) - z)
                    End If
                End If
            End If
            TheText = Trim(TheText)
            If (Mid(TheText, Len(TheText) - 1, 2) = " C") Then
                TheText = Trim(TheText) + "hart"
            End If
            If (Trim(TheText) <> "") Then
                z = 1
                While (z < Len(TheText))
                    z1 = 0
                    Temp = Mid(TheText, z, 110)
                    If (z + 110 < Len(TheText)) Then
                        For tt = Len(Temp) To 1 Step -1
                            If (Mid(Temp, tt, 1) = " ") Then
                                z = z + tt
                                z1 = z
                                Temp = Left(Temp, tt)
                                Exit For
                            End If
                        Next tt
                    End If
                    If (z1 = 0) Then
                        z = z + 110
                    End If
                    If (Len(Temp) < 110) Then
                        Temp = Temp + Space(110 - Len(Temp)) + Trim(Str(i))
                    Else
                        Temp = Left(Temp, 110) + Trim(Str(i))
                    End If
                    If (Trim(Temp) <> "") Then
                        lstActions.AddItem Temp
                        If (RetHigh) Then
                            lstActions.Selected(lstActions.ListCount - 1) = True
                        End If
                    End If
                Wend
            End If
        End If
    End If
Next i

' Do All Drugs
Set RetClinical = New PatientClinical
RetClinical.AppointmentId = 0
RetClinical.PatientID = PatientID
RetClinical.ClinicalType = "A"
RetClinical.Symptom = ""
RetClinical.Findings = ""
RetClinical.ImageDescriptor = ""
If (RetClinical.FindRxClinical > 0) Then
    j = 1
    While (RetClinical.SelectPatientClinical(j))
        Temp = ""
        If (UCase(Left(RetClinical.Findings, 2)) = "RX") Then
            If (Trim(RetClinical.ImageDescriptor) = "D" + TheDate) Or (Trim(RetClinical.ImageDescriptor) = "R" + TheDate) Or (Trim(RetClinical.ImageDescriptor) = "") Then
                RetString = Trim(RetClinical.Findings)
                Call frmSystems1.BuildEvaluationLine(Trim(RetString), TheText, PrescribedOn)
                If (Left(RetClinical.ImageDescriptor, 1) = "D") Then
                    Temp = Trim(TheText) + "Discontinued on " + TheDate
                ElseIf (Left(RetClinical.ImageDescriptor, 1) = "R") Then
                    Temp = Trim(TheText) + "Refilled on " + TheDate
                Else
                    Temp = Trim(TheText)
                End If
                Call ReplaceCharacters(Temp, "-0/", " ")
                Call ReplaceCharacters(Temp, "-1/", " ")
                Call ReplaceCharacters(Temp, "-2/", " ")
                Call ReplaceCharacters(Temp, "-3/", " ")
                Call ReplaceCharacters(Temp, "-4/", " ")
                Call ReplaceCharacters(Temp, "-5/", " ")
                Call ReplaceCharacters(Temp, "-6/", " ")
                Call ReplaceCharacters(Temp, "P-7/", " ")
                Call ReplaceCharacters(Temp, "-8/", " ")
                Call ReplaceCharacters(Temp, "/", " ")
                Call ReplaceCharacters(Temp, "--", " ")
                Call ReplaceCharacters(Temp, "  ", " ")
                If (UCase(Left(Temp, 2)) = "RX") Then
                    Temp = Mid(Temp, 4, Len(Temp) - 3)
                End If
                TheText = Trim(Temp)
                If (Trim(TheText) <> "") Then
                    z = 1
                    While (z < Len(TheText))
                        z1 = 0
                        Temp = Mid(TheText, z, 110)
                        If (z + 110 < Len(TheText)) Then
                            For tt = Len(Temp) To 1 Step -1
                                If (Mid(Temp, tt, 1) = " ") Then
                                    z = z + tt
                                    z1 = z
                                    Temp = Left(Temp, tt)
                                    Exit For
                                End If
                            Next tt
                        End If
                        If (z1 = 0) Then
                            z = z + 110
                        End If
                        If (UCase(Left(RetString, 2)) = "RX") Then
                            If (InStrPS(RetString, "P-7") < 1) Then
                                Temp = ""
                            End If
                        End If
                        If (Temp <> "") Then
                            If (PrescribedOn) Then
                                lstActions.AddItem Trim(Temp)
                                If (Left(RetClinical.ImageDescriptor, 1) <> "D") Then
                                    lstActions.Selected(lstActions.ListCount - 1) = True
                                End If
                            End If
                        End If
                    Wend
                End If
            End If
        End If
        j = j + 1
    Wend
End If
Set RetClinical = Nothing

' Notes
i = 1
Set RetNotes = New PatientNotes
RetNotes.NotesPatientId = PatientID
RetNotes.NotesAppointmentId = AppointmentId
RetNotes.NotesType = "C"
RetNotes.NotesSystem = ""
If (RetNotes.FindNotes > 0) Then
    While (RetNotes.SelectNotes(i))
        If (Trim(RetNotes.NotesSystem) = "") Then
            Temp = ""
            If (Trim(RetNotes.NotesText1) <> "") Then
                Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                     + Trim(RetNotes.NotesText2) + " " _
                     + Trim(RetNotes.NotesText3) + " " _
                     + Trim(RetNotes.NotesText4)
                Temp = Trim(Temp)
                Call StripCharacters(Temp, Chr(13))
                Call StripCharacters(Temp, Chr(10))
                TheText = Trim(Temp)
                If (Trim(TheText) <> "") Then
                    z = 1
                    While (z < Len(TheText))
                        z1 = 0
                        Temp = Mid(TheText, z, 110)
                        If (z + 110 < Len(TheText)) Then
                            For tt = Len(Temp) To 1 Step -1
                                If (Mid(Temp, tt, 1) = " ") Then
                                    z = z + tt
                                    z1 = z
                                    Temp = Left(Temp, tt)
                                    Exit For
                                End If
                            Next tt
                        End If
                        If (z1 = 0) Then
                            z = z + 110
                        End If
                        Temp = Temp + Space(110 - Len(Temp))
                        Temp = Temp + "-" + Trim(Str(RetNotes.NotesId))
                        lstActions.AddItem Trim(Temp)
                        If (Trim(RetNotes.NotesHighlight) <> "") Then
                            lstActions.Selected(lstActions.ListCount - 1) = True
                        End If
                    Wend
                End If
            End If
        ElseIf (Trim(RetNotes.NotesSystem) = "IMPR") Then
            Temp = ""
            If (Trim(RetNotes.NotesText1) <> "") Then
                Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                     + Trim(RetNotes.NotesText2) + " " _
                     + Trim(RetNotes.NotesText3) + " " _
                     + Trim(RetNotes.NotesText4)
                Temp = Trim(Temp)
                Call StripCharacters(Temp, Chr(13))
                Call StripCharacters(Temp, Chr(10))
                If (Trim(Temp) <> "") Then
                    For tt = 1 To Len(Temp) Step 50
                        QTemp = Mid(Temp, tt, 50)
                        ListType = 2
                        GoSub WrapIt
                    Next tt
                End If
            End If
        ElseIf (Left(Trim(RetNotes.NotesSystem), 5) = "IMPR-") Then
            Temp = RetNotes.NotesEye + ":"
            If (Trim(RetNotes.NotesText1) <> "") Then
                Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                     + Trim(RetNotes.NotesText2) + " " _
                     + Trim(RetNotes.NotesText3) + " " _
                     + Trim(RetNotes.NotesText4)
                Temp = Trim(Temp)
                Call StripCharacters(Temp, Chr(13))
                Call StripCharacters(Temp, Chr(10))
                If (Trim(Temp) <> "") Then
                    For tt = 1 To Len(Temp) Step 50
                        QTemp = Mid(Temp, tt, 50)
                        ListType = 3
                        GoSub WrapIt
                    Next tt
                End If
            End If
        End If
        i = i + 1
    Wend
End If
Set RetNotes = Nothing

Call AutoSetHighlightsLocally

cmdDone.Text = "Sign chart and check out"
cmdQuit.Visible = Not ViewOn
cmdPlan.Visible = Not ViewOn
cmdImpr.Visible = Not ViewOn
If (ViewOn) Then
    cmdDone.Text = "Done"
End If
LoadFinal = True
Exit Function
WrapIt:
    If (Trim(QTemp) <> "") Then
        If (Mid(QTemp, Len(QTemp), 1) = " ") Or (Len(QTemp) <= 50) Then
            If (ListType = 1) Then
                lstTests.AddItem QTemp
            ElseIf (ListType = 2) Then
                lstImp.AddItem QTemp
            ElseIf (ListType = 3) Then
                lstActions.AddItem QTemp
            End If
        Else
            For p = Len(QTemp) To 1 Step -1
                If (Mid(QTemp, p, 1) = " ") Then
                    PTemp = Left(QTemp, p)
                    If (ListType = 1) Then
                        lstTests.AddItem PTemp
                        lstTests.AddItem Mid(QTemp, p + 1, Len(QTemp) - p)
                    ElseIf (ListType = 2) Then
                        lstImp.AddItem PTemp
                        lstImp.AddItem Mid(QTemp, p + 1, Len(QTemp) - p)
                    ElseIf (ListType = 3) Then
                        lstActions.AddItem PTemp
                        lstActions.AddItem Mid(QTemp, p + 1, Len(QTemp) - p)
                    End If
                    Exit For
                End If
            Next p
        End If
    End If
    Return
End Function

Private Sub lstTests_Click()
Dim i As Integer
Dim Id1 As String, Id2 As String
If (lstTests.Selected(lstTests.ListIndex)) Then
    If (Len(lstTests.List(lstTests.ListIndex)) > 75) Then
        Id1 = Mid(lstTests.List(lstTests.ListIndex), 76, Len(lstTests.List(lstTests.ListIndex)) - 75)
        For i = lstTests.ListIndex To 0 Step -1
            If (Len(lstTests.List(i)) > 70) Then
                Id2 = Mid(lstTests.List(i), 76, Len(lstTests.List(i)) - 75)
                If (Id1 = Id2) Then
                    lstTests.Selected(i) = True
                Else
                    Exit For
                End If
            End If
        Next i
        For i = lstTests.ListIndex + 1 To lstTests.ListCount - 1
            If (Len(lstTests.List(i)) > 70) Then
                Id2 = Mid(lstTests.List(i), 76, Len(lstTests.List(i)) - 75)
                If (Id1 = Id2) Then
                    lstTests.Selected(i) = True
                Else
                    Exit For
                End If
            End If
        Next i
    End If
Else
    If (Len(lstTests.List(lstTests.ListIndex)) > 75) Then
        Id1 = Mid(lstTests.List(lstTests.ListIndex), 76, Len(lstTests.List(lstTests.ListIndex)) - 75)
        For i = lstTests.ListIndex To 0 Step -1
            If (Len(lstTests.List(i)) > 70) Then
                Id2 = Mid(lstTests.List(i), 76, Len(lstTests.List(i)) - 75)
                If (Id1 = Id2) Then
                    lstTests.Selected(i) = False
                Else
                    Exit For
                End If
            End If
        Next i
        For i = lstTests.ListIndex + 1 To lstTests.ListCount - 1
            If (Len(lstTests.List(i)) > 70) Then
                Id2 = Mid(lstTests.List(i), 76, Len(lstTests.List(i)) - 75)
                If (Id1 = Id2) Then
                    lstTests.Selected(i) = False
                Else
                    Exit For
                End If
            End If
        Next i
    End If
End If
End Sub

Private Function AutoSetHighlightsLocally() As Boolean
Dim i As Integer
Dim j As Integer
Dim EndLink As Integer
Dim ATemp As String
For i = 1 To lstBilled.ListCount - 1
    If (lstBilled.List(i) = "Procedures to be billed") Or _
       (lstBilled.List(i) = "_______________________") Then
        EndLink = i - 1
        Exit For
    End If
Next i
For i = 1 To EndLink
    If (Trim(lstBilled.List(i)) <> "") Then
        lstBilled.Selected(i) = True
    End If
Next i
For i = 0 To lstActions.ListCount - 1
    If (UCase(Left(lstActions.List(i), 17)) = "OFFICE PROCEDURES") Or (UCase(Left(lstActions.List(i), 12)) = "ORDER A TEST") Or (UCase(Left(lstActions.List(i), 16)) = "SCHEDULE SURGERY") Then
        lstActions.Selected(i) = True
    End If
Next i
For i = 0 To lstTests.ListCount - 1
    j = InStrPS(lstTests.List(i), ":")
    If (j > 0) And (j > 3) Then
        ATemp = Left(lstTests.List(i), j - 1)
        If (IsHighlight(ATemp)) Then
            lstTests.Selected(i) = True
        End If
    End If
Next i
End Function

Private Function IsHighlight(AQuest As String) As Boolean
Dim RetQues As DynamicClass
IsHighlight = False
If (Trim(AQuest) <> "") Then
    Set RetQues = New DynamicClass
    RetQues.Question = AQuest
    If (RetQues.RetrieveClassbyQuestion) Then
        If (RetQues.Highlight = "T") Then
            IsHighlight = True
        End If
    End If
    Set RetQues = Nothing
End If
End Function
