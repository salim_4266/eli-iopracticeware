VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmSetProcedures 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12495
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "SetProcedures.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12495
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdSurveillance 
      Height          =   685
      Left            =   5160
      TabIndex        =   51
      Top             =   7200
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1208
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":36C9
   End
   Begin VB.ListBox lstSort 
      Height          =   645
      Left            =   0
      Sorted          =   -1  'True
      TabIndex        =   18
      Top             =   5400
      Visible         =   0   'False
      Width           =   735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReviewSystems 
      Height          =   975
      Left            =   9360
      TabIndex        =   10
      Top             =   7920
      Width           =   1245
      _Version        =   131072
      _ExtentX        =   2196
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":3912
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   11
      Top             =   7920
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":3AF8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   975
      Left            =   1320
      TabIndex        =   12
      Top             =   7920
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":3CD7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPlan 
      Height          =   975
      Left            =   7080
      TabIndex        =   13
      Top             =   7920
      Width           =   885
      _Version        =   131072
      _ExtentX        =   1561
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":3EB7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10680
      TabIndex        =   14
      Top             =   7920
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":4096
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreCPTs 
      Height          =   855
      Left            =   8880
      TabIndex        =   5
      Top             =   6360
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":4275
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCheckOut 
      Height          =   975
      Left            =   8040
      TabIndex        =   19
      Top             =   7920
      Width           =   1245
      _Version        =   131072
      _ExtentX        =   2196
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":4458
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOthers 
      Height          =   975
      Left            =   2400
      TabIndex        =   20
      Top             =   7920
      Width           =   1245
      _Version        =   131072
      _ExtentX        =   2196
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":4698
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther5 
      Height          =   1095
      Left            =   5640
      TabIndex        =   22
      Top             =   5160
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":487F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther10 
      Height          =   1095
      Left            =   8880
      TabIndex        =   23
      Top             =   5160
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":4A63
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther9 
      Height          =   1095
      Left            =   8880
      TabIndex        =   24
      Top             =   3960
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":4C48
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther8 
      Height          =   1095
      Left            =   8880
      TabIndex        =   25
      Top             =   2760
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":4E2C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther7 
      Height          =   1095
      Left            =   8880
      TabIndex        =   26
      Top             =   1560
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":5010
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther6 
      Height          =   1095
      Left            =   8880
      TabIndex        =   27
      Top             =   360
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":51F4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure6 
      Height          =   1095
      Left            =   8880
      TabIndex        =   0
      Top             =   360
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":53D8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure7 
      Height          =   1095
      Left            =   8880
      TabIndex        =   6
      Top             =   1560
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":55C0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure8 
      Height          =   1095
      Left            =   8880
      TabIndex        =   7
      Top             =   2760
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":57A8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure9 
      Height          =   1095
      Left            =   8880
      TabIndex        =   8
      Top             =   3960
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":5990
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure10 
      Height          =   1095
      Left            =   8880
      TabIndex        =   9
      Top             =   5160
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":5B78
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure5 
      Height          =   1095
      Left            =   5640
      TabIndex        =   16
      Top             =   5160
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":5D61
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOtherProcs 
      Height          =   855
      Left            =   5640
      TabIndex        =   17
      Top             =   6360
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":5F49
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther4 
      Height          =   1095
      Left            =   5640
      TabIndex        =   30
      Top             =   3960
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":612E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther3 
      Height          =   1095
      Left            =   5640
      TabIndex        =   29
      Top             =   2760
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":6312
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther2 
      Height          =   1095
      Left            =   5640
      TabIndex        =   28
      Top             =   1560
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":64F6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther1 
      Height          =   1095
      Left            =   5640
      TabIndex        =   21
      Top             =   360
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":66DA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure1 
      Height          =   1095
      Left            =   5640
      TabIndex        =   1
      Top             =   360
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":68BE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure2 
      Height          =   1095
      Left            =   5640
      TabIndex        =   2
      Top             =   1560
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":6AA6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure3 
      Height          =   1095
      Left            =   5640
      TabIndex        =   3
      Top             =   2760
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":6C8E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure4 
      Height          =   1095
      Left            =   5640
      TabIndex        =   4
      Top             =   3960
      Visible         =   0   'False
      Width           =   3135
      _Version        =   131072
      _ExtentX        =   5530
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":6E76
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis6 
      Height          =   1095
      Left            =   2880
      TabIndex        =   32
      Top             =   360
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":705E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis1 
      Height          =   1095
      Left            =   120
      TabIndex        =   33
      Top             =   360
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":7246
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis2 
      Height          =   1095
      Left            =   120
      TabIndex        =   34
      Top             =   1560
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":742E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis3 
      Height          =   1095
      Left            =   120
      TabIndex        =   35
      Top             =   2760
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":7616
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis4 
      Height          =   1095
      Left            =   120
      TabIndex        =   36
      Top             =   3960
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":77FE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis5 
      Height          =   1095
      Left            =   120
      TabIndex        =   37
      Top             =   5160
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":79E6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis7 
      Height          =   1095
      Left            =   2880
      TabIndex        =   38
      Top             =   1560
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":7BCE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis8 
      Height          =   1095
      Left            =   2880
      TabIndex        =   39
      Top             =   2760
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":7DB6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis9 
      Height          =   1095
      Left            =   2880
      TabIndex        =   40
      Top             =   3960
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":7F9E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis10 
      Height          =   1095
      Left            =   2880
      TabIndex        =   41
      Top             =   5160
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":8186
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreDiags 
      Height          =   855
      Left            =   120
      TabIndex        =   42
      Top             =   6360
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":836F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOtherDiags 
      Height          =   855
      Left            =   2880
      TabIndex        =   43
      Top             =   6360
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":8553
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHigh 
      Height          =   975
      Left            =   4800
      TabIndex        =   44
      Top             =   7920
      Width           =   915
      _Version        =   131072
      _ExtentX        =   1614
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":8738
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSumm 
      Height          =   975
      Left            =   3720
      TabIndex        =   47
      Top             =   7920
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":891D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDxOrder 
      Height          =   975
      Left            =   5760
      TabIndex        =   49
      Top             =   7920
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":8AFF
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtn1 
      Height          =   975
      Left            =   0
      TabIndex        =   50
      Top             =   0
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetProcedures.frx":8D42
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Diagnosis"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0FFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   31
      Top             =   0
      Width           =   1140
   End
   Begin VB.Label lblMods 
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   6840
      TabIndex        =   48
      Top             =   7320
      Visible         =   0   'False
      Width           =   4935
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPostOp 
      Alignment       =   2  'Center
      BackColor       =   &H00F7F5F5&
      Caption         =   "In Post-Op period"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   10200
      TabIndex        =   46
      Top             =   0
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblIns 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   615
      Left            =   120
      TabIndex        =   45
      Top             =   7320
      Width           =   4935
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblProcs 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Procedures for Billing, Current Visit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   5640
      TabIndex        =   15
      Top             =   0
      Width           =   4035
   End
End
Attribute VB_Name = "frmSetProcedures"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public AppointmentId As Long
Public NavigationAction As Long
Public InsurerStuff As String

Private Const QntRef As String = "["
Private Const QntRefEnd As String = "]"
Private Const CntRef As String = "-NF"
Private Const CntRefSet As String = "!"
Private Const RORef As String = "-RO"
Private Const RORefSet As String = "#"
Private Const HORef As String = "-HO"
Private Const HORefSet As String = "%"
Private Const MaxBillingDiagnosisAllowed As Integer = 12
Private Const MaxSystems As Integer = 18
Private Const MaxBillingDiagnosis As Integer = 100

Private Type ExternalReferenceDiag
    Id As String
    Name As String
    Eye As String
    SeverityA As String
    SeverityB As String
    Active As Boolean
    IgnoreEye As Boolean
    NegativeFinding As Boolean
    RuleOutOn As Boolean
    HistoryOfOn As Boolean
    Highlight As Boolean
End Type
Private SelectedDiagnosis(MaxBillingDiagnosis) As ExternalReferenceDiag
Private BilledDiagnosis(MaxBillingDiagnosisAllowed * 2) As ExternalReferenceDiag

Private OtherICDAccessType As String
Private CurrentOtherDiagIndex As Integer
Private CurrentSystem As String
Private CurrentDiagSelectIndex As Integer
Private CurrentDiagIndex As Integer
Private TotalOtherDiags As Integer
Private TotalAvailableDiagnosis As Integer
Private TotalCount As Integer

Private Const MaxModifierAllowed As Integer = 4
Private Const MaxBillingProcedures As Integer = 100
Private Const MaxBillingProceduresAllowed As Integer = 24
Private Type ExternalReference
    Id As String
    Diag As String
    OrgDiag As String
    Name As String
    Modifier(MaxModifierAllowed) As String
    LinkedDiag As String
    Ref As Long
    Highlight As Boolean
    WarningOn As Boolean
End Type
Private SortedProcedures(MaxBillingProcedures) As ExternalReference
Private AvailableProcedures(MaxBillingProcedures) As ExternalReference
Private OtherAvailableProcedures(MaxBillingProcedures) As ExternalReference
Private SelectedProcedures(MaxBillingProceduresAllowed + 6) As ExternalReference

Private BusLogOn As Boolean
Private AutoSelectionOn As Boolean
Private PLinkType As String
Private LinkedDiag As String
Private DisplayType As String
Private DisplayStart As String
Private CurrentModsIndex As Integer
Private TotalMods As Integer
Private ModifierCnt As Integer
Private ModifierChangeOn As Integer

Private MaxOtherProcedures As Integer
Private MaxProcedures As Integer
Private CurrentProcIndex As Integer
Private CurrentOtherProcIndex As Integer
Private CurrentAvailableIndex As Integer
Private SelectedLinkedProcedure As Integer

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long
Dim SelectedImpr As Collection

Private Sub ClearDiags()
cmdDiagnosis1.Visible = False
cmdDiagnosis1.BackColor = BaseBackColor
cmdDiagnosis1.ForeColor = BaseTextColor
cmdDiagnosis1.Tag = ""
cmdDiagnosis1.ToolTipText = ""
cmdDiagnosis2.Visible = False
cmdDiagnosis2.BackColor = BaseBackColor
cmdDiagnosis2.ForeColor = BaseTextColor
cmdDiagnosis2.Tag = ""
cmdDiagnosis2.ToolTipText = ""
cmdDiagnosis3.Visible = False
cmdDiagnosis3.BackColor = BaseBackColor
cmdDiagnosis3.ForeColor = BaseTextColor
cmdDiagnosis3.Tag = ""
cmdDiagnosis3.ToolTipText = ""
cmdDiagnosis4.Visible = False
cmdDiagnosis4.BackColor = BaseBackColor
cmdDiagnosis4.ForeColor = BaseTextColor
cmdDiagnosis4.Tag = ""
cmdDiagnosis4.ToolTipText = ""
cmdDiagnosis5.Visible = False
cmdDiagnosis5.BackColor = BaseBackColor
cmdDiagnosis5.ForeColor = BaseTextColor
cmdDiagnosis5.Tag = ""
cmdDiagnosis5.ToolTipText = ""
cmdDiagnosis6.Visible = False
cmdDiagnosis6.BackColor = BaseBackColor
cmdDiagnosis6.ForeColor = BaseTextColor
cmdDiagnosis6.Tag = ""
cmdDiagnosis6.ToolTipText = ""
cmdDiagnosis7.Visible = False
cmdDiagnosis7.BackColor = BaseBackColor
cmdDiagnosis7.ForeColor = BaseTextColor
cmdDiagnosis7.Tag = ""
cmdDiagnosis7.ToolTipText = ""
cmdDiagnosis8.Visible = False
cmdDiagnosis8.BackColor = BaseBackColor
cmdDiagnosis8.ForeColor = BaseTextColor
cmdDiagnosis8.Tag = ""
cmdDiagnosis8.ToolTipText = ""
cmdDiagnosis9.Visible = False
cmdDiagnosis9.BackColor = BaseBackColor
cmdDiagnosis9.ForeColor = BaseTextColor
cmdDiagnosis9.Tag = ""
cmdDiagnosis9.ToolTipText = ""
cmdDiagnosis10.Visible = False
cmdDiagnosis10.BackColor = BaseBackColor
cmdDiagnosis10.ForeColor = BaseTextColor
cmdDiagnosis10.Tag = ""
cmdDiagnosis10.ToolTipText = ""
End Sub

Private Sub cmdDiagnosis1_Click()
Call TriggerDiagnosis(cmdDiagnosis1, 1)
End Sub

Private Sub cmdDiagnosis2_Click()
Call TriggerDiagnosis(cmdDiagnosis2, 2)
End Sub

Private Sub cmdDiagnosis3_Click()
Call TriggerDiagnosis(cmdDiagnosis3, 3)
End Sub

Private Sub cmdDiagnosis4_Click()
Call TriggerDiagnosis(cmdDiagnosis4, 4)
End Sub

Private Sub cmdDiagnosis5_Click()
Call TriggerDiagnosis(cmdDiagnosis5, 5)
End Sub

Private Sub cmdDiagnosis6_Click()
Call TriggerDiagnosis(cmdDiagnosis6, 6)
End Sub

Private Sub cmdDiagnosis7_Click()
Call TriggerDiagnosis(cmdDiagnosis7, 7)
End Sub

Private Sub cmdDiagnosis8_Click()
Call TriggerDiagnosis(cmdDiagnosis8, 8)
End Sub

Private Sub cmdDiagnosis9_Click()
Call TriggerDiagnosis(cmdDiagnosis9, 9)
End Sub

Private Sub cmdDiagnosis10_Click()
Call TriggerDiagnosis(cmdDiagnosis10, 10)
End Sub

Private Sub cmdHigh_Click()
cmdHigh.Enabled = False
frmHighlights.HighlightLoadOn = frmSystems1.IsHighlightLoadOn
If (frmHighlights.LoadHighlights(PatientId, True, True, False)) Then
    frmHighlights.Show 1
    Call frmSystems1.SetHighlightLoad(frmHighlights.HighlightLoadOn)
End If
cmdHigh.Enabled = True
End Sub

Private Sub cmdMoreDiags_Click()
CurrentOtherDiagIndex = 1
cmdOtherDiags.Visible = True
CurrentDiagSelectIndex = CurrentDiagSelectIndex + 1
If (CurrentDiagSelectIndex > TotalAvailableDiagnosis) Then
    CurrentDiagSelectIndex = 1
End If
Call LoadDiagnosis(False)
End Sub

Private Sub cmdOtherDiags_Click()
Dim AText As String
Dim AType As String
frmEventMsgs.Header = "Display Ordering"
frmEventMsgs.AcceptText = "Alpha"
frmEventMsgs.RejectText = "Numeric"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    AType = "A"
    Call GetStartingValue(AText, AType)
    If (Trim(AText) = "") Then
        Exit Sub
    End If
ElseIf (frmEventMsgs.Result = 2) Then
    AType = "N"
    Call GetStartingValue(AText, AType)
    If (Trim(AText) = "") Then
        Exit Sub
    End If
Else
    Exit Sub
End If
If (Trim(AType) <> "") And (Trim(AText) <> "") Then
    frmSearch.DiagOn = True
    frmSearch.FilterBillable = True
    frmSearch.FilterSpecial = True
    frmSearch.Selection = ""
    If (AType = "A") Then
        AText = "*" + AText
    End If
    If (frmSearch.LoadAvailableSearch(AText, AType)) Then
        frmSearch.Show 1
        If (Trim(frmSearch.Selection) <> "") Then
            Call SetAnotherDiagnosis(frmSearch.Selection)
            CurrentOtherDiagIndex = 1
            CurrentDiagSelectIndex = 1
            Call LoadDiagnosis(False)
        End If
    End If
End If
End Sub

Private Sub loadSelectedImpr()
    If SelectedImpr Is Nothing Then
        Set SelectedImpr = New Collection
        
        Dim TheFile As String
        Dim FileNum As Integer
        Dim r As Integer
        Dim Rec(1) As String
        Dim k As Integer
        
        TheFile = DoctorInterfaceDirectory + "DiagImpr" + "_" + Trim$(str$(AppointmentId)) + "_" + Trim$(str$(PatientId)) + ".txt"
        If FM.IsFileThere(TheFile) Then
            FileNum = FreeFile
            FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
            Do Until EOF(FileNum)
                Line Input #FileNum, Rec(0)
                k = InStrPS(Rec(0), "-")
                If (k > 1) Then
                    Rec(0) = Left(Rec(0), k + 2)
                    For r = 1 To 8
                        Line Input #FileNum, Rec(1)
                    Next
                    SelectedImpr.Add Rec(0) & ":" & Format(CInt(Rec(1)), "00"), CStr(SelectedImpr.Count)
                End If
            Loop
            FM.CloseFile CLng(FileNum)
        End If
    End If
End Sub

Private Function notSelectedImpr(Diag As String) As Boolean
Dim i As Integer

Dim FDiag(1) As String
FDiag(0) = Mid(Diag, 1, Len(Diag) - 3)
FDiag(1) = Right(Diag, 2)
Dim fImpr(2) As String

notSelectedImpr = False
For i = 1 To SelectedImpr.Count
    fImpr(0) = Mid(SelectedImpr(i), 1, Len(SelectedImpr(i)) - 6)
    fImpr(1) = Left(Right(SelectedImpr(i), 5), 2)
    fImpr(2) = Right(SelectedImpr(i), 2)
    
    If fImpr(0) = FDiag(0) Then
        If fImpr(1) = FDiag(1) Or FDiag(1) = "OU" Then
            If fImpr(2) = "00" Then
                notSelectedImpr = True
            ElseIf fImpr(2) = "01" And Not notSelectedImpr Then
                notSelectedImpr = False
            End If
        End If
    End If
Next
End Function

Private Sub TriggerDiagnosis(AButton As fpBtn, Ref As Integer)

If AButton.BackColor = BaseBackColor Or Not frmSystems1.CheckDrOrderImpressions Then
    loadSelectedImpr
    If notSelectedImpr(AButton.Tag & "-" & Right(AButton.Text, 2)) Then
        frmEventMsgs.Header = "This Impression is not selected." & vbNewLine & _
                              "Please go to the Impressions screen and select it."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
End If

Dim OrgDiag As String
Dim q As Integer, i As Integer
Dim ClearDiag As Integer, RefDiag As Integer
    On Error GoTo lTriggerDiagnosis_Error

RefDiag = 0
OrgDiag = ""
If (AButton.BackColor = BaseBackColor) Then
    If Not IsDiagSelected(Left$(AButton.Tag, 6)) Then
        If IsDiagExists(Left$(AButton.Tag, 6)) Then
           frmEventMsgs.InfoMessage "Diagnosis " & Left$(AButton.Tag, 5) & " already selected"
           frmEventMsgs.Show 1
           Exit Sub
        End If
        If (CurrentDiagIndex <= MaxBillingDiagnosisAllowed) Then
            q = InStrPS(AButton.Text, " ")
            If (q = 0) Then
                q = 1
            End If
            BilledDiagnosis(CurrentDiagIndex).Name = Trim(Mid(AButton.Text, q, (Len(AButton.Text) - 2) - q))
            BilledDiagnosis(CurrentDiagIndex).Eye = Trim(Mid(AButton.Text, Len(AButton.Text) - 1, 2))
            BilledDiagnosis(CurrentDiagIndex).Id = Trim(AButton.Tag)
            CurrentDiagIndex = CurrentDiagIndex + 1
            If (SetDiagnosisProcedureMatch(Trim(AButton.Tag), CurrentDiagIndex - 1)) Then
                Call RetainBilledDiagnosis(PatientId)
                Call RetainBilledProcedures(PatientId)
                Call LoadSuperBill(True, PLinkType)
            End If
'            AButton.BackColor = ButtonSelectionColor
'            AButton.ForeColor = TextSelectionColor
        Else
            frmEventMsgs.Header = "Only" + str(MaxBillingDiagnosisAllowed) + " Diagnosis are allowed"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
        End If
    Else
        frmEventMsgs.Header = "Diagnosis " & Left$(AButton.Tag, 6) & " already selected."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    If (Mid(AButton.Text, Len(AButton.Text) - 1, 1) = "-") Then
        AButton.Text = Left(AButton.Text, Len(AButton.Text) - 3)
    End If
' Clear Diag
    For i = 1 To CurrentDiagIndex - 1
        If (Trim(AButton.Tag) = Trim(BilledDiagnosis(i).Id)) Then
            OrgDiag = Trim(BilledDiagnosis(i).Id)
            RefDiag = i
            BilledDiagnosis(i).Id = ""
            BilledDiagnosis(i).Name = ""
            BilledDiagnosis(i).Eye = ""
            BilledDiagnosis(i).SeverityA = ""
            BilledDiagnosis(i).SeverityB = ""
            BilledDiagnosis(i).Active = False
            BilledDiagnosis(i).Highlight = False
            BilledDiagnosis(i).NegativeFinding = False
            BilledDiagnosis(i).RuleOutOn = False
            BilledDiagnosis(i).HistoryOfOn = False
            ClearDiag = i
            Exit For
        End If
    Next i
' Filter Cleared diag
    For i = ClearDiag To CurrentDiagIndex
        BilledDiagnosis(i).Id = BilledDiagnosis(i + 1).Id
        BilledDiagnosis(i).Name = BilledDiagnosis(i + 1).Name
        BilledDiagnosis(i).Eye = BilledDiagnosis(i + 1).Eye
        BilledDiagnosis(i).SeverityA = BilledDiagnosis(i + 1).SeverityA
        BilledDiagnosis(i).SeverityB = BilledDiagnosis(i + 1).SeverityB
        BilledDiagnosis(i).Active = BilledDiagnosis(i + 1).Active
        BilledDiagnosis(i).Highlight = BilledDiagnosis(i + 1).Highlight
        BilledDiagnosis(i).NegativeFinding = False
        BilledDiagnosis(i).RuleOutOn = False
        BilledDiagnosis(i).HistoryOfOn = False
    Next i
    CurrentDiagIndex = CurrentDiagIndex - 1
    If (CurrentDiagIndex < 1) Then
        CurrentDiagIndex = 1
    End If
    Call ResetDiagnosisLinks(OrgDiag, RefDiag)
    Call LoadProcedure(True, PLinkType)
End If
Call SetActiveDiag

    Exit Sub

lTriggerDiagnosis_Error:

    LogError "frmSetProcedures", "TriggerDiagnosis", Err, Err.Description
End Sub

Private Function LoadDiagnosis(Init As Boolean) As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
LoadDiagnosis = False
Call ClearDiags
If (Init) Then
    OtherICDAccessType = "OPHT"
    DisplayType = ""
    DisplayStart = ""
    CurrentSystem = ""
    CurrentOtherDiagIndex = 1
    CurrentDiagIndex = 1
    CurrentDiagSelectIndex = 1
    Call InitializeBillingDiagnosis
    Call LoadAvailableDiagnosis
    Call LoadDiagnosisFromFile
End If
i = CurrentDiagSelectIndex
ButtonCnt = 1
While (i <= TotalAvailableDiagnosis) And (ButtonCnt < 11)
    DoEvents
    Call SetButtonDiag(ButtonCnt, SelectedDiagnosis(i).Id, Trim(SelectedDiagnosis(i).Name) + " " + Trim(SelectedDiagnosis(i).Eye))
    ButtonCnt = ButtonCnt + 1
    i = i + 1
Wend
CurrentDiagSelectIndex = i - 1
cmdMoreDiags.Visible = False
If (CurrentDiagSelectIndex < TotalAvailableDiagnosis) Then
    cmdMoreDiags.Text = "More Diagnoses"
Else
    cmdMoreDiags.Text = "First Diagnoses"
End If
If (TotalAvailableDiagnosis > 10) Then
    cmdMoreDiags.Visible = True
End If
LoadDiagnosis = True
Call SetActiveDiag
End Function

Private Sub LoadAvailableDiagnosis()
Dim LoadIt As Boolean
Dim TheId As Long
Dim w As Integer
Dim i As Integer, j As Integer
Dim OtherText As String
Dim PTemp As String, TheText As String
Dim RQuan As String
Dim RSys As String, RText As String
Dim LQuan As String
Dim LSys As String, LText As String
Dim RightDiagnosis As String, LeftDiagnosis As String
TotalAvailableDiagnosis = 0
i = 1
j = 1
While (frmSystems1.GetSystemDiagnosis(i, RSys, RightDiagnosis, RText, RQuan, LSys, LeftDiagnosis, LText, LQuan))
    LoadIt = True
    OtherText = ""
    If (Trim(RightDiagnosis) <> "") And (Left(RightDiagnosis, 2) <> "N-") Then
        If (Left(RightDiagnosis, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Mid(RightDiagnosis, 2, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Left(RightDiagnosis, 1) + Mid(RightDiagnosis, 3, Len(RightDiagnosis) - 2)
        End If
        If (Mid(RightDiagnosis, 3, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Left(RightDiagnosis, 2) + Mid(RightDiagnosis, 4, Len(RightDiagnosis) - 3)
        End If
        If (Left(RightDiagnosis, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Mid(RightDiagnosis, 2, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Left(RightDiagnosis, 1) + Mid(RightDiagnosis, 3, Len(RightDiagnosis) - 2)
        End If
        If (Mid(RightDiagnosis, 3, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Left(RightDiagnosis, 2) + Mid(RightDiagnosis, 4, Len(RightDiagnosis) - 3)
        End If
        If (Left(RightDiagnosis, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Mid(RightDiagnosis, 2, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Left(RightDiagnosis, 1) + Mid(RightDiagnosis, 3, Len(RightDiagnosis) - 2)
        End If
        If (Mid(RightDiagnosis, 3, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Left(RightDiagnosis, 2) + Mid(RightDiagnosis, 4, Len(RightDiagnosis) - 3)
        End If
        If (Left(RightDiagnosis, 1) <> "Y") And _
           (Left(RightDiagnosis, 1) <> "M") And _
           (Left(RightDiagnosis, 1) <> "P") And _
           (Left(RightDiagnosis, 1) <> "X") Then
            LoadIt = True
            If (InStrPS(OtherText, "NF-") <> 0) Then
                LoadIt = False
            End If
            If (InStrPS(OtherText, "RO-") <> 0) Then
                LoadIt = False
            End If
            If (LoadIt) Then
                w = InStrPS(RightDiagnosis, "@")
                If (w > 0) Then
                    PTemp = Left(RightDiagnosis, w - 1)
                Else
                    PTemp = RightDiagnosis
                End If
                If Not (IsDiagThere(PTemp, "OD")) Then
                    If (GetPrimaryDiagnosis(PTemp, RSys, TheText, True, TheId)) Then
                        If (IsDiagThere(PTemp, "OS")) Then
                            Call SetDiagBoth(PTemp)
                        Else
                            If Not (IsDiagThere(PTemp, "OU")) Then
                                SelectedDiagnosis(j).Id = PTemp
                                SelectedDiagnosis(j).Name = TheText
                                SelectedDiagnosis(j).Eye = "OD"
                                SelectedDiagnosis(j).SeverityA = ""
                                SelectedDiagnosis(j).SeverityB = ""
                                SelectedDiagnosis(j).Active = False
                                SelectedDiagnosis(j).IgnoreEye = False
                                SelectedDiagnosis(j).NegativeFinding = False
                                SelectedDiagnosis(j).RuleOutOn = False
                                SelectedDiagnosis(j).HistoryOfOn = False
                                SelectedDiagnosis(j).Highlight = False
                                j = j + 1
                            End If
                        End If
                    End If
                Else
                    If (IsDiagThere(PTemp, "OS")) Then
                        Call SetDiagBoth(PTemp)
                    End If
                End If
            End If
        End If
    End If
    If (Trim(LeftDiagnosis) <> "") And (Left(LeftDiagnosis, 2) <> "N-") Then
        OtherText = ""
        If (Left(LeftDiagnosis, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Mid(LeftDiagnosis, 2, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Left(LeftDiagnosis, 1) + Mid(LeftDiagnosis, 3, Len(LeftDiagnosis) - 2)
        End If
        If (Mid(LeftDiagnosis, 3, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Left(LeftDiagnosis, 2) + Mid(LeftDiagnosis, 4, Len(LeftDiagnosis) - 3)
        End If
        If (Left(LeftDiagnosis, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Mid(LeftDiagnosis, 2, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Left(LeftDiagnosis, 1) + Mid(LeftDiagnosis, 3, Len(LeftDiagnosis) - 2)
        End If
        If (Mid(LeftDiagnosis, 3, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Left(LeftDiagnosis, 2) + Mid(LeftDiagnosis, 4, Len(LeftDiagnosis) - 3)
        End If
        If (Left(LeftDiagnosis, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Mid(LeftDiagnosis, 2, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Left(LeftDiagnosis, 1) + Mid(LeftDiagnosis, 3, Len(LeftDiagnosis) - 2)
        End If
        If (Mid(LeftDiagnosis, 3, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Left(LeftDiagnosis, 2) + Mid(LeftDiagnosis, 4, Len(LeftDiagnosis) - 3)
        End If
        If (Left(LeftDiagnosis, 1) <> "Y") And _
           (Left(LeftDiagnosis, 1) <> "M") And _
           (Left(LeftDiagnosis, 1) <> "P") And _
           (Left(LeftDiagnosis, 1) <> "X") Then
            LoadIt = True
            If (InStrPS(OtherText, "NF-") <> 0) Then
                LoadIt = False
            End If
            If (InStrPS(OtherText, "RO-") <> 0) Then
                LoadIt = False
            End If
            If (LoadIt) Then
                w = InStrPS(LeftDiagnosis, "@")
                If (w > 0) Then
                    PTemp = Left(LeftDiagnosis, w - 1)
                Else
                    PTemp = LeftDiagnosis
                End If
                If Not (IsDiagThere(PTemp, "OS")) Then
                    If (IsDiagThere(PTemp, "OD")) Then
                        Call SetDiagBoth(PTemp)
                    Else
                        If Not (IsDiagThere(PTemp, "OU")) Then
                            If (GetPrimaryDiagnosis(PTemp, LSys, TheText, True, TheId)) Then
                                SelectedDiagnosis(j).Id = PTemp
                                SelectedDiagnosis(j).Name = TheText
                                SelectedDiagnosis(j).Eye = "OS"
                                SelectedDiagnosis(j).SeverityA = ""
                                SelectedDiagnosis(j).SeverityB = ""
                                SelectedDiagnosis(j).Active = False
                                SelectedDiagnosis(j).IgnoreEye = False
                                SelectedDiagnosis(j).NegativeFinding = False
                                SelectedDiagnosis(j).RuleOutOn = False
                                SelectedDiagnosis(j).HistoryOfOn = False
                                SelectedDiagnosis(j).Highlight = False
                                j = j + 1
                            End If
                        End If
                    End If
                Else
                    If (IsDiagThere(PTemp, "OD")) Then
                        Call SetDiagBoth(PTemp)
                    End If
                End If
            End If
        End If
    End If
    i = i + 1
Wend
TotalAvailableDiagnosis = j - 1
End Sub

Private Sub SetButtonDiag(Ref As Integer, TheId As String, TheDisplayName As String)
Dim q As Integer
Dim NewId As String
NewId = TheId
q = InStrPS(NewId, ".")
If (q > 0) Then
    q = InStrPS(q + 1, TheId, ".")
    If (q > 0) Then
        NewId = Left(TheId, q - 1)
    End If
End If
NewId = NewId + Chr(13) + Chr(10)
If (Ref = 1) Then
    cmdDiagnosis1.Text = NewId + " " + TheDisplayName
    cmdDiagnosis1.Tag = TheId
    cmdDiagnosis1.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis1.BackColor = ButtonSelectionColor
        cmdDiagnosis1.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 2) Then
    cmdDiagnosis2.Text = NewId + " " + TheDisplayName
    cmdDiagnosis2.Tag = TheId
    cmdDiagnosis2.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis2.BackColor = ButtonSelectionColor
        cmdDiagnosis2.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 3) Then
    cmdDiagnosis3.Text = NewId + " " + TheDisplayName
    cmdDiagnosis3.Tag = TheId
    cmdDiagnosis3.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis3.BackColor = ButtonSelectionColor
        cmdDiagnosis3.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 4) Then
    cmdDiagnosis4.Text = NewId + " " + TheDisplayName
    cmdDiagnosis4.Tag = TheId
    cmdDiagnosis4.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis4.BackColor = ButtonSelectionColor
        cmdDiagnosis4.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 5) Then
    cmdDiagnosis5.Text = NewId + " " + TheDisplayName
    cmdDiagnosis5.Tag = TheId
    cmdDiagnosis5.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis5.BackColor = ButtonSelectionColor
        cmdDiagnosis5.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 6) Then
    cmdDiagnosis6.Text = NewId + " " + TheDisplayName
    cmdDiagnosis6.Tag = TheId
    cmdDiagnosis6.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis6.BackColor = ButtonSelectionColor
        cmdDiagnosis6.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 7) Then
    cmdDiagnosis7.Text = NewId + " " + TheDisplayName
    cmdDiagnosis7.Tag = TheId
    cmdDiagnosis7.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis7.BackColor = ButtonSelectionColor
        cmdDiagnosis7.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 8) Then
    cmdDiagnosis8.Text = NewId + " " + TheDisplayName
    cmdDiagnosis8.Tag = TheId
    cmdDiagnosis8.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis8.BackColor = ButtonSelectionColor
        cmdDiagnosis8.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 9) Then
    cmdDiagnosis9.Text = NewId + " " + TheDisplayName
    cmdDiagnosis9.Tag = TheId
    cmdDiagnosis9.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis9.BackColor = ButtonSelectionColor
        cmdDiagnosis9.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 10) Then
    cmdDiagnosis10.Text = NewId + " " + TheDisplayName
    cmdDiagnosis10.Tag = TheId
    cmdDiagnosis10.Visible = True
    If (IsDiagSet(TheId)) Then
        cmdDiagnosis10.BackColor = ButtonSelectionColor
        cmdDiagnosis10.ForeColor = TextSelectionColor
    End If
End If
End Sub

Private Sub LoadDiagnosisFromFile()
 On Error GoTo lLoadDiagnosis_Error
Dim FileNum As Integer
Dim k As Integer
Dim TheFile As String
Dim RHigh As String, RetHigh As Boolean
Dim Rec As String, RetId As String
Dim RetEye As String, RetString As String
Dim RetSevA As String, RetSevB As String
TheFile = DoctorInterfaceDirectory + "DiagBill" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    While Not (EOF(FileNum))
        Line Input #FileNum, Rec
        k = InStrPS(Rec, "-")
        If (k > 1) Then
            RetId = Left(Rec, k - 1)
            RetEye = Mid(Rec, k + 1, 2)
            If (k + 3 < Len(Rec)) Then
                RetString = Mid(Rec, k + 3, Len(Rec) - (k + 2))
            Else
                RetString = ""
            End If
            Line Input #FileNum, RetSevA
            Line Input #FileNum, RetSevB
            Line Input #FileNum, RHigh
            RetHigh = False
            If (RHigh = "T") Then
                RetHigh = True
            End If
            If (CurrentDiagIndex <= MaxBillingDiagnosisAllowed) Then
                If (IsDiagThere(RetId, RetEye)) Then
                    If Not (IsDiagSet(RetId)) Then
                        BilledDiagnosis(CurrentDiagIndex).Id = RetId
                        BilledDiagnosis(CurrentDiagIndex).Name = RetString
                        BilledDiagnosis(CurrentDiagIndex).Eye = RetEye
                        BilledDiagnosis(CurrentDiagIndex).SeverityA = Trim(RetSevA)
                        BilledDiagnosis(CurrentDiagIndex).SeverityB = Trim(RetSevB)
                        BilledDiagnosis(CurrentDiagIndex).Highlight = RetHigh
                        CurrentDiagIndex = CurrentDiagIndex + 1
                    End If
                Else
                    If (IsDiagThere(RetId, RetEye)) Then
                        If Not (IsDiagSet(RetId)) Then
                        TotalAvailableDiagnosis = TotalAvailableDiagnosis + 1
                        SelectedDiagnosis(TotalAvailableDiagnosis).Id = RetId
                        SelectedDiagnosis(TotalAvailableDiagnosis).Name = RetString
                        SelectedDiagnosis(TotalAvailableDiagnosis).Eye = RetEye
                        SelectedDiagnosis(TotalAvailableDiagnosis).SeverityA = ""
                        SelectedDiagnosis(TotalAvailableDiagnosis).SeverityB = ""
                        SelectedDiagnosis(TotalAvailableDiagnosis).Highlight = False
                        BilledDiagnosis(CurrentDiagIndex).Id = RetId
                        BilledDiagnosis(CurrentDiagIndex).Name = RetString
                        BilledDiagnosis(CurrentDiagIndex).Eye = RetEye
                        BilledDiagnosis(CurrentDiagIndex).SeverityA = Trim(RetSevA)
                        BilledDiagnosis(CurrentDiagIndex).SeverityB = Trim(RetSevB)
                        BilledDiagnosis(CurrentDiagIndex).Highlight = RetHigh
                        CurrentDiagIndex = CurrentDiagIndex + 1
                        End If
                    End If
                End If
            End If
        End If
    Wend
    FM.CloseFile CLng(FileNum)
    Call SetActiveDiag
    'FM.Kill TheFile

lLoadDiagnosis_Error:
    FM.CloseFile CLng(FileNum)
        'LogError "frmSetProcedures", "LoadDiagnosisFromFile", Err, Err.Description
End If
End Sub

Private Sub SetAnotherDiagnosis(TheDiag)
Dim Temp As String
Dim AEye As String
If (Trim(TheDiag) <> "") Then
    Temp = TheDiag
    frmEventMsgs.Header = "Specify Eye"
    frmEventMsgs.AcceptText = "OD"
    frmEventMsgs.RejectText = "OS"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "OU"
    frmEventMsgs.Other1Text = "Not Needed"
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result <> 4) Then
        AEye = ""
        If (frmEventMsgs.Result = 1) Then
            AEye = "OD"
        ElseIf (frmEventMsgs.Result = 2) Then
            AEye = "OS"
        ElseIf (frmEventMsgs.Result = 3) Then
            AEye = "OU"
        End If
        If Not (IsDiagThere(Trim(Mid(Temp, 1, 10)), AEye)) Then
            If (CurrentDiagIndex > MaxBillingDiagnosisAllowed) Then
                frmEventMsgs.Header = "Only " + Trim(str(MaxBillingDiagnosisAllowed)) + " Diagnosis are allowed"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Exit Sub
            End If
            TotalAvailableDiagnosis = TotalAvailableDiagnosis + 1
            SelectedDiagnosis(TotalAvailableDiagnosis).Id = Trim(Mid(Temp, 1, 10))
            SelectedDiagnosis(TotalAvailableDiagnosis).Name = Trim(Mid(Temp, 11, 40))
            SelectedDiagnosis(TotalAvailableDiagnosis).Eye = AEye
            SelectedDiagnosis(TotalAvailableDiagnosis).SeverityA = ""
            SelectedDiagnosis(TotalAvailableDiagnosis).SeverityB = ""
            SelectedDiagnosis(TotalAvailableDiagnosis).Highlight = False
            BilledDiagnosis(CurrentDiagIndex).Id = Trim(Mid(Temp, 1, 10))
            BilledDiagnosis(CurrentDiagIndex).Name = Trim(Mid(Temp, 11, 40))
            If (frmEventMsgs.Result = 1) Then
                BilledDiagnosis(CurrentDiagIndex).Eye = "OD"
            ElseIf (frmEventMsgs.Result = 2) Then
                BilledDiagnosis(CurrentDiagIndex).Eye = "OS"
            Else
                BilledDiagnosis(CurrentDiagIndex).Eye = "OU"
            End If
            BilledDiagnosis(CurrentDiagIndex).SeverityA = ""
            BilledDiagnosis(CurrentDiagIndex).SeverityB = ""
            BilledDiagnosis(CurrentDiagIndex).Highlight = False
            CurrentDiagIndex = CurrentDiagIndex + 1
        End If
    Else
        frmEventMsgs.Header = "Diagnosis Already Listed"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub SetActiveDiag()
Dim ATag As String
Dim AText As String
Dim ButtonOn As Boolean
Dim k As Integer, i As Integer
ATag = cmdDiagnosis1.Tag
AText = cmdDiagnosis1.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis1.BackColor = ButtonSelectionColor
    cmdDiagnosis1.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis1.Text, Len(cmdDiagnosis1.Text) - 1, 1) = "-") Then
        cmdDiagnosis1.Text = Left(cmdDiagnosis1.Text, Len(cmdDiagnosis1.Text) - 3)
    End If
    cmdDiagnosis1.Text = cmdDiagnosis1.Text + " -" + Trim(str(k))
End If
ATag = cmdDiagnosis2.Tag
AText = cmdDiagnosis2.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis2.BackColor = ButtonSelectionColor
    cmdDiagnosis2.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis2.Text, Len(cmdDiagnosis2.Text) - 1, 1) = "-") Then
        cmdDiagnosis2.Text = Left(cmdDiagnosis2.Text, Len(cmdDiagnosis2.Text) - 3)
    End If
    cmdDiagnosis2.Text = cmdDiagnosis2.Text + " -" + Trim(str(k))
End If
ATag = cmdDiagnosis3.Tag
AText = cmdDiagnosis3.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis3.BackColor = ButtonSelectionColor
    cmdDiagnosis3.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis3.Text, Len(cmdDiagnosis3.Text) - 1, 1) = "-") Then
        cmdDiagnosis3.Text = Left(cmdDiagnosis3.Text, Len(cmdDiagnosis3.Text) - 3)
    End If
    cmdDiagnosis3.Text = cmdDiagnosis3.Text + " -" + Trim(str(k))
End If
ATag = cmdDiagnosis4.Tag
AText = cmdDiagnosis4.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis4.BackColor = ButtonSelectionColor
    cmdDiagnosis4.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis4.Text, Len(cmdDiagnosis4.Text) - 1, 1) = "-") Then
        cmdDiagnosis4.Text = Left(cmdDiagnosis4.Text, Len(cmdDiagnosis4.Text) - 3)
    End If
    cmdDiagnosis4.Text = cmdDiagnosis4.Text + " -" + Trim(str(k))
End If
ATag = cmdDiagnosis5.Tag
AText = cmdDiagnosis5.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis5.BackColor = ButtonSelectionColor
    cmdDiagnosis5.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis5.Text, Len(cmdDiagnosis5.Text) - 1, 1) = "-") Then
        cmdDiagnosis5.Text = Left(cmdDiagnosis5.Text, Len(cmdDiagnosis5.Text) - 3)
    End If
    cmdDiagnosis5.Text = cmdDiagnosis5.Text + " -" + Trim(str(k))
End If
ATag = cmdDiagnosis6.Tag
AText = cmdDiagnosis6.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis6.BackColor = ButtonSelectionColor
    cmdDiagnosis6.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis6.Text, Len(cmdDiagnosis6.Text) - 1, 1) = "-") Then
        cmdDiagnosis6.Text = Left(cmdDiagnosis6.Text, Len(cmdDiagnosis6.Text) - 3)
    End If
    cmdDiagnosis6.Text = cmdDiagnosis6.Text + " -" + Trim(str(k))
End If
ATag = cmdDiagnosis7.Tag
AText = cmdDiagnosis7.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis7.BackColor = ButtonSelectionColor
    cmdDiagnosis7.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis7.Text, Len(cmdDiagnosis7.Text) - 1, 1) = "-") Then
        cmdDiagnosis7.Text = Left(cmdDiagnosis7.Text, Len(cmdDiagnosis7.Text) - 3)
    End If
    cmdDiagnosis7.Text = cmdDiagnosis7.Text + " -" + Trim(str(k))
End If
ATag = cmdDiagnosis8.Tag
AText = cmdDiagnosis8.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis8.BackColor = ButtonSelectionColor
    cmdDiagnosis8.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis8.Text, Len(cmdDiagnosis8.Text) - 1, 1) = "-") Then
        cmdDiagnosis8.Text = Left(cmdDiagnosis8.Text, Len(cmdDiagnosis8.Text) - 3)
    End If
    cmdDiagnosis8.Text = cmdDiagnosis8.Text + " -" + Trim(str(k))
End If
ATag = cmdDiagnosis9.Tag
AText = cmdDiagnosis9.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis9.BackColor = ButtonSelectionColor
    cmdDiagnosis9.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis9.Text, Len(cmdDiagnosis9.Text) - 1, 1) = "-") Then
        cmdDiagnosis9.Text = Left(cmdDiagnosis9.Text, Len(cmdDiagnosis9.Text) - 3)
    End If
    cmdDiagnosis9.Text = cmdDiagnosis9.Text + " -" + Trim(str(k))
End If
ATag = cmdDiagnosis10.Tag
AText = cmdDiagnosis10.Text
GoSub SetButton
If (ButtonOn) Then
    cmdDiagnosis10.BackColor = ButtonSelectionColor
    cmdDiagnosis10.ForeColor = TextSelectionColor
    If (Mid(cmdDiagnosis10.Text, Len(cmdDiagnosis10.Text) - 1, 1) = "-") Then
        cmdDiagnosis10.Text = Left(cmdDiagnosis10.Text, Len(cmdDiagnosis10.Text) - 3)
    End If
    cmdDiagnosis10.Text = cmdDiagnosis10.Text + " -" + Trim(str(k))
End If
Exit Sub
SetButton:
    k = 0
    ButtonOn = False
    For i = 1 To TotalBilledDiagnosis
        If (ATag = Trim(BilledDiagnosis(i).Id)) And (InStrPS(AText, Trim(BilledDiagnosis(i).Eye)) > 0) Then
            k = i
            ButtonOn = True
            Exit For
        End If
    Next i
    Return
End Sub

Private Sub SetDiagBoth(TheId As String)
Dim i As Integer
For i = 1 To TotalBilledDiagnosis
    If (Trim(TheId) <> "") Then
        If (Trim(BilledDiagnosis(i).Id) = Trim(TheId)) Then
            BilledDiagnosis(i).Eye = "OU"
            Exit For
        End If
    End If
Next i
For i = 1 To MaxBillingDiagnosis
    If (Trim(TheId) <> "") Then
        If (Trim(SelectedDiagnosis(i).Id) = Trim(TheId)) Then
            SelectedDiagnosis(i).Eye = "OU"
            Exit For
        End If
    End If
Next i
End Sub

Private Function IsDiagThere(TheId As String, AEye As String) As Boolean
Dim i As Integer
IsDiagThere = False
For i = 1 To MaxBillingDiagnosis
    If (Trim(TheId) <> "") Then
        If (Trim(SelectedDiagnosis(i).Id) = Trim(TheId)) Then
            If (SelectedDiagnosis(i).Eye = AEye) Or (Trim(AEye) = "") Or (Trim(SelectedDiagnosis(i).Eye) = "") Then
                IsDiagThere = True
                Exit For
            End If
        End If
    End If
Next i
End Function

Private Function IsDiagSet(TheId As String) As Boolean
Dim i As Integer
IsDiagSet = False
For i = 1 To TotalBilledDiagnosis
    If (Trim(TheId) <> "") Then
        If (Trim(BilledDiagnosis(i).Id) = Trim(TheId)) Then
            IsDiagSet = True
            Exit For
        End If
    End If
Next i
End Function

Private Function GetPrimaryDiagnosis(Id As String, ASys As String, TheName As String, BillOn As Boolean, AId As Long) As Boolean
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
GetPrimaryDiagnosis = False
TheName = ""
If (Id <> "") And (Len(Id) > 1) Then
    Set RetrieveDiagnosis = New DiagnosisMasterPrimary
    RetrieveDiagnosis.PrimarySystem = ASys
    RetrieveDiagnosis.PrimaryDiagnosis = ""
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = Id
    RetrieveDiagnosis.PrimaryLevel = 0
    RetrieveDiagnosis.PrimaryRank = 0
    RetrieveDiagnosis.PrimaryBilling = BillOn
    RetrieveDiagnosis.PrimaryDiscipline = ""
    If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
        If (RetrieveDiagnosis.SelectPrimary(1)) Then
            TheName = Trim(RetrieveDiagnosis.PrimaryShortName)
            If (TheName = "") Then
                TheName = Trim(RetrieveDiagnosis.PrimaryLingo)
                If (Trim(TheName) = "") Then
                    TheName = Trim(RetrieveDiagnosis.PrimaryName)
                End If
            End If
            AId = RetrieveDiagnosis.PrimaryId
            GetPrimaryDiagnosis = True
        End If
    Else
        RetrieveDiagnosis.PrimarySystem = ""
        If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
            If (RetrieveDiagnosis.SelectPrimary(1)) Then
                TheName = Trim(RetrieveDiagnosis.PrimaryShortName)
                If (TheName = "") Then
                    TheName = Trim(RetrieveDiagnosis.PrimaryLingo)
                    If (Trim(TheName) = "") Then
                        TheName = Trim(RetrieveDiagnosis.PrimaryName)
                    End If
                End If
                AId = RetrieveDiagnosis.PrimaryId
                GetPrimaryDiagnosis = True
            End If
        End If
    End If
    Set RetrieveDiagnosis = Nothing
End If
End Function

Public Function InitializeBillingDiagnosis() As Boolean
Dim i As Integer
TotalAvailableDiagnosis = 0
For i = 1 To MaxBillingDiagnosis
    SelectedDiagnosis(i).Id = ""
    SelectedDiagnosis(i).Name = ""
    SelectedDiagnosis(i).Eye = ""
    SelectedDiagnosis(i).SeverityA = ""
    SelectedDiagnosis(i).SeverityB = ""
    SelectedDiagnosis(i).NegativeFinding = False
    SelectedDiagnosis(i).Active = False
    SelectedDiagnosis(i).IgnoreEye = False
    SelectedDiagnosis(i).RuleOutOn = False
    SelectedDiagnosis(i).Highlight = False
    SelectedDiagnosis(i).HistoryOfOn = False
    If (i <= MaxBillingDiagnosisAllowed) Then
        BilledDiagnosis(i).Id = ""
        BilledDiagnosis(i).Name = ""
        BilledDiagnosis(i).Eye = ""
        BilledDiagnosis(i).SeverityA = ""
        BilledDiagnosis(i).SeverityB = ""
        BilledDiagnosis(i).Active = False
        BilledDiagnosis(i).IgnoreEye = False
        BilledDiagnosis(i).NegativeFinding = False
        BilledDiagnosis(i).RuleOutOn = False
        BilledDiagnosis(i).Highlight = False
        BilledDiagnosis(i).HistoryOfOn = False
    End If
Next i
InitializeBillingDiagnosis = True
End Function

Private Function SetBilledDiagnosis(Ref As Integer, TheDiag As String, TheDiagnosis As String, TheEye As String, TheSeverityA As String, TheSeverityB As String, TheHigh As Boolean) As Boolean
SetBilledDiagnosis = False
TheEye = ""
TheDiag = ""
TheDiagnosis = ""
TheSeverityA = ""
TheSeverityB = ""
TheHigh = False
If (Ref > 0) And (Ref <= TotalBilledDiagnosis) Then
    If (Trim(BilledDiagnosis(Ref).Id) <> "") Then
        TheDiag = Trim(BilledDiagnosis(Ref).Id)
        TheDiagnosis = Trim(BilledDiagnosis(Ref).Name)
        TheSeverityA = Trim(BilledDiagnosis(Ref).SeverityA)
        TheSeverityB = Trim(BilledDiagnosis(Ref).SeverityB)
        TheHigh = BilledDiagnosis(Ref).Highlight
        TheEye = Trim(BilledDiagnosis(Ref).Eye)
        If (Len(TheEye) <> 2) Then
            TheEye = "  "
        End If
        SetBilledDiagnosis = True
    End If
End If
End Function

Public Function GetBilledDiagnosis(RefId As Integer, ADiag As String, ADiagnosis As String, AEye As String, ASeverityA As String, ASeverityB As String, AHigh As Boolean) As Boolean
GetBilledDiagnosis = SetBilledDiagnosis(RefId, ADiag, ADiagnosis, AEye, ASeverityA, ASeverityB, AHigh)
End Function

Public Function TotalBilledDiagnosis() As Integer
Dim i As Integer
TotalBilledDiagnosis = 0
For i = 1 To MaxBillingDiagnosisAllowed
    If (Trim(BilledDiagnosis(i).Id) = "") Then
        TotalBilledDiagnosis = i - 1
        Exit For
    Else
        TotalBilledDiagnosis = i
    End If
Next i
End Function

Public Function GetDiagnosisLink(TheDiag As String) As Integer
Dim i As Integer
GetDiagnosisLink = 1
For i = 1 To TotalBilledDiagnosis
    If (Trim(TheDiag) = Trim(Left(BilledDiagnosis(i).Id, Len(TheDiag)))) Then
        GetDiagnosisLink = i
        Exit For
    End If
Next i
End Function

Public Function RetainBilledDiagnosis(PatId As Long) As Boolean
Dim u As Integer
Dim Totals As Integer, FileNum As Integer
Dim RetHigh As Boolean, RHigh As String
Dim TheFile As String, RetEye As String
Dim RetId As String, RetString As String
Dim RetSevA As String, RetSevB As String
RetainBilledDiagnosis = False
If (PatId > 0) Then
    FileNum = FreeFile
'    TheFile = DoctorInterfaceDirectory + "DiagBill" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
'    Totals = TotalBilledDiagnosis
'    If (Totals > 0) Then
'        FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(FileNum)
'        For u = 1 To Totals
'            If (GetBilledDiagnosis(u, RetId, RetString, RetEye, RetSevA, RetSevB, RetHigh)) Then
'                RHigh = "F"
'                If RetHigh Then
'                    RHigh = "T"
'                End If
'                Print #FileNum, RetId + "-" + RetEye + RetString
'                Print #FileNum, RetSevA
'                Print #FileNum, RetSevB
'                Print #FileNum, RHigh
'            End If
'        Next u
'        FM.CloseFile CLng(FileNum)
'    End If
    RetainBilledDiagnosis = True
End If
End Function

Public Function PurgeRetainedList(PatId As Long, ABack As Boolean) As Boolean
Dim AFile As String
Dim OtherFile As String
PurgeRetainedList = True
If (PatId > 0) Then
    Call InitializeBillingDiagnosis
    AFile = DoctorInterfaceDirectory + "DiagBill" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
    OtherFile = DoctorInterfaceDirectory + "Backup\DiagBill" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
    If FM.IsFileThere(AFile) Then
        If (ABack) Then
            FM.FileCopy AFile, OtherFile
        End If
        FM.Kill AFile
    End If
End If
End Function

Private Sub ClearCPTs()
cmdProcedure1.Visible = False
cmdProcedure1.BackColor = BaseBackColor
cmdProcedure1.ForeColor = BaseTextColor
cmdProcedure2.Visible = False
cmdProcedure2.BackColor = BaseBackColor
cmdProcedure2.ForeColor = BaseTextColor
cmdProcedure3.Visible = False
cmdProcedure3.BackColor = BaseBackColor
cmdProcedure3.ForeColor = BaseTextColor
cmdProcedure4.Visible = False
cmdProcedure4.BackColor = BaseBackColor
cmdProcedure4.ForeColor = BaseTextColor
cmdProcedure5.Visible = False
cmdProcedure5.BackColor = BaseBackColor
cmdProcedure5.ForeColor = BaseTextColor
cmdProcedure6.Visible = False
cmdProcedure6.BackColor = BaseBackColor
cmdProcedure6.ForeColor = BaseTextColor
cmdProcedure7.Visible = False
cmdProcedure7.BackColor = BaseBackColor
cmdProcedure7.ForeColor = BaseTextColor
cmdProcedure8.Visible = False
cmdProcedure8.BackColor = BaseBackColor
cmdProcedure8.ForeColor = BaseTextColor
cmdProcedure9.Visible = False
cmdProcedure9.BackColor = BaseBackColor
cmdProcedure9.ForeColor = BaseTextColor
cmdProcedure10.Visible = False
cmdProcedure10.BackColor = BaseBackColor
cmdProcedure10.ForeColor = BaseTextColor
End Sub

Private Sub SetVisibleCPTs(Display As Boolean)
cmdProcedure1.Visible = Display
cmdProcedure2.Visible = Display
cmdProcedure3.Visible = Display
cmdProcedure4.Visible = Display
cmdProcedure5.Visible = Display
cmdProcedure6.Visible = Display
cmdProcedure7.Visible = Display
cmdProcedure8.Visible = Display
cmdProcedure9.Visible = Display
cmdProcedure10.Visible = Display
lblProcs.Visible = Display
End Sub

Private Sub cmdCheckOut_Click()
frmEventMsgs.Header = "Express Check-Out WILL CAUSE WORKFLOW PROBLEMS. Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Check-Out"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    NavigationAction = 90
    Unload frmSetProcedures
End If
End Sub

Private Sub cmdOther1_Click()
Call TriggerOtherProcedure(cmdOther1)
End Sub

Private Sub cmdOther2_Click()
Call TriggerOtherProcedure(cmdOther2)
End Sub

Private Sub cmdOther3_Click()
Call TriggerOtherProcedure(cmdOther3)
End Sub

Private Sub cmdOther4_Click()
Call TriggerOtherProcedure(cmdOther4)
End Sub

Private Sub cmdOther5_Click()
Call TriggerOtherProcedure(cmdOther5)
End Sub

Private Sub cmdOther6_Click()
Call TriggerOtherProcedure(cmdOther6)
End Sub

Private Sub cmdOther7_Click()
Call TriggerOtherProcedure(cmdOther7)
End Sub

Private Sub cmdOther8_Click()
Call TriggerOtherProcedure(cmdOther8)
End Sub

Private Sub cmdOther9_Click()
Call TriggerOtherProcedure(cmdOther9)
End Sub

Private Sub cmdOther10_Click()
Call TriggerOtherProcedure(cmdOther10)
End Sub

Private Sub cmdOtherProcs_Click()
frmEventMsgs.Header = "Display Ordering"
frmEventMsgs.AcceptText = "Alpha"
frmEventMsgs.RejectText = "Numeric"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    DisplayType = "A"
    Call GetStartingValue(DisplayStart, DisplayType)
    If (DisplayStart = "") Then
        Exit Sub
    End If
    DisplayStart = "*" + DisplayStart
ElseIf (frmEventMsgs.Result = 2) Then
    DisplayType = "N"
    Call GetStartingValue(DisplayStart, DisplayType)
    If (DisplayStart = "") Then
        Exit Sub
    End If
Else
    Exit Sub
End If
frmSearch.DiagOn = False
frmSearch.FilterBillable = False
frmSearch.FilterSpecial = False
frmSearch.Selection = ""
If (frmSearch.LoadAvailableSearch(DisplayStart, DisplayType)) Then
    frmSearch.Show 1
    If (Trim(frmSearch.Selection) <> "") Then
        Call SetUpProcedure(frmSearch.Selection)
        Call LoadAvailableProcedures
    End If
Else
    frmEventMsgs.Header = "No Procedures Found"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdOthers_Click()
If (cmdOthers.Text = "Current Procedures") Then
    Call LoadAvailableProcedures
Else
    Call LoadOtherProcedures
End If
End Sub

Private Sub cmdPlan_Click()
If (frmEvaluation.LoadActions(True, True)) Then
    frmEvaluation.Show 1
    Call frmEvaluation.PostToFile
End If
End Sub

Private Sub cmdProcedure1_Click()
Call TriggerProcedure(cmdProcedure1)
End Sub

Private Sub cmdProcedure2_Click()
Call TriggerProcedure(cmdProcedure2)
End Sub

Private Sub cmdProcedure3_Click()
Call TriggerProcedure(cmdProcedure3)
End Sub

Private Sub cmdProcedure4_Click()
Call TriggerProcedure(cmdProcedure4)
End Sub

Private Sub cmdProcedure5_Click()
Call TriggerProcedure(cmdProcedure5)
End Sub

Private Sub cmdProcedure6_Click()
Call TriggerProcedure(cmdProcedure6)
End Sub

Private Sub cmdProcedure7_Click()
Call TriggerProcedure(cmdProcedure7)
End Sub

Private Sub cmdProcedure8_Click()
Call TriggerProcedure(cmdProcedure8)
End Sub

Private Sub cmdProcedure9_Click()
Call TriggerProcedure(cmdProcedure9)
End Sub

Private Sub cmdProcedure10_Click()
Call TriggerProcedure(cmdProcedure10)
End Sub

Private Sub cmdHome_Click()
NavigationAction = 99
Unload frmSetProcedures
End Sub

Private Sub cmdMoreCPTs_Click()
If (InStrPS(cmdMoreCPTs.Text, "Linked Procs") <> 0) Then
    CurrentOtherProcIndex = CurrentOtherProcIndex + 1
    If (CurrentOtherProcIndex > MaxOtherProcedures) Then
        CurrentOtherProcIndex = 1
    End If
    Call LoadOtherAvailableProcedures
Else
    If (InStrPS(cmdMoreCPTs.Text, "Completed") <> 0) Then
        If (cmdMoreCPTs.Text = "Modifier Selection Completed") Then
            Call SetVisibleCPTs(True)
        End If
        cmdMoreCPTs.Text = "More Procedures"
        cmdOtherProcs.Text = "Other CPTs"
        cmdOtherProcs.Visible = True
        cmdNotes.Enabled = True
        cmdDone.Enabled = True
        cmdPlan.Enabled = True
        cmdReviewSystems.Enabled = True
        cmdHome.Enabled = True
    End If
    If (CurrentAvailableIndex > MaxProcedures) Then
        CurrentAvailableIndex = 1
    End If
    Call LoadAvailableProcedures
End If
End Sub

Public Sub cmdDone_Click()
NavigationAction = 1
Unload frmSetProcedures
End Sub

Private Sub cmdReviewSystems_Click()
NavigationAction = 2
CurrentProcIndex = 1
Unload frmSetProcedures
End Sub

Private Sub cmdNotes_Click()
If Not UserLogin.HasPermission(epPatientNotes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
frmNotes.NoteId = 0
frmNotes.NoteOn = False
frmNotes.SystemReference = ""
frmNotes.EyeContext = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Public Function LoadSuperBill(Init As Boolean, TheLink As String) As Boolean
    On Error GoTo lLoadSuperBill_Error

LoadSuperBill = True

    BusLogOn = CheckConfigCollection("BUSLOGON") = "T"
    Call InitializeBillingDiagnosis
    Call InitializeBillingProcedure
    
Call LoadDiagnosis(Init)
Call LoadProcedure(Init, TheLink)
lblPostOp.Visible = False
If (BusLogOn) Then
    If (frmSystems1.IsPostOpOn) Then
        lblPostOp.Visible = True
    End If
End If
cmdDxOrder.Visible = False
If (frmSystems1.GetBringForward) Then
    ' We'll get to finishing this implementation later.
    'cmdDxOrder.Visible = True
End If

    Exit Function

lLoadSuperBill_Error:

    LogError "frmSetProcedures", "LoadSuperBill", Err, Err.Description
End Function

Private Function LoadProcedure(Init As Boolean, TheLink As String) As Boolean
LoadProcedure = False
If (Init) Then
    AutoSelectionOn = True
    PLinkType = TheLink
    ModifierChangeOn = 0
    DisplayType = ""
    DisplayStart = ""
    MaxProcedures = 0
    CurrentProcIndex = 1
    CurrentAvailableIndex = 1
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = &H9B9626
    BaseTextColor = &HFFFFFF
    Call InitializeBillingProcedure
    Call SetTestProcedures(PatientId)
    lblIns.Caption = InsurerStuff
End If
If (MaxProcedures > 0) Then
    LoadProcedure = LoadAvailableProcedures
    lblMods.Visible = SetWarning
Else
    Call ClearCPTs
    Call ClearOtherCPTs
    LoadProcedure = True
    cmdMoreCPTs.Visible = False
End If
End Function

Private Function LoadAvailableProcedures() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
LoadAvailableProcedures = False
SelectedLinkedProcedure = 0
Call ClearCPTs
Call ClearOtherCPTs
ButtonCnt = 1
i = CurrentProcIndex
'i = CurrentAvailableIndex
If (i >= MaxProcedures) Then
    i = 1
End If
While (i <= MaxProcedures) And (ButtonCnt < 11)
    DoEvents
    If (Trim(AvailableProcedures(i).Name) <> "") Then
        If (ButtonCnt = 1) Then
            Call SetButton(ButtonCnt, i, cmdProcedure1)
        ElseIf (ButtonCnt = 2) Then
            Call SetButton(ButtonCnt, i, cmdProcedure2)
        ElseIf (ButtonCnt = 3) Then
            Call SetButton(ButtonCnt, i, cmdProcedure3)
        ElseIf (ButtonCnt = 4) Then
            Call SetButton(ButtonCnt, i, cmdProcedure4)
        ElseIf (ButtonCnt = 5) Then
            Call SetButton(ButtonCnt, i, cmdProcedure5)
        ElseIf (ButtonCnt = 6) Then
            Call SetButton(ButtonCnt, i, cmdProcedure6)
        ElseIf (ButtonCnt = 7) Then
            Call SetButton(ButtonCnt, i, cmdProcedure7)
        ElseIf (ButtonCnt = 8) Then
            Call SetButton(ButtonCnt, i, cmdProcedure8)
        ElseIf (ButtonCnt = 9) Then
            Call SetButton(ButtonCnt, i, cmdProcedure9)
        ElseIf (ButtonCnt = 10) Then
            Call SetButton(ButtonCnt, i, cmdProcedure10)
        End If
        ButtonCnt = ButtonCnt + 1
    End If
    i = i + 1
Wend
CurrentProcIndex = i
cmdOthers.Text = "Linked Procs"
LoadAvailableProcedures = True
If (CurrentAvailableIndex <= MaxProcedures) Then
    If (MaxProcedures > 10) Then
        cmdMoreCPTs.Text = "More Procedures"
        cmdMoreCPTs.Visible = True
    Else
        cmdMoreCPTs.Visible = False
    End If
Else
    If (MaxProcedures > 10) Then
        cmdMoreCPTs.Text = "First Procedures"
        cmdMoreCPTs.Visible = True
    Else
        cmdMoreCPTs.Visible = False
    End If
End If
cmdOtherProcs.Visible = True
End Function

Private Sub TriggerProcedure(AButton As fpBtn)
Dim ClearCPT As Integer
Dim g As Integer
Dim j As Integer, i As Integer
Dim ATemp As Integer, Id As String
Dim DiagId As String, AMod As String
If (AButton.BackColor = BaseBackColor) Then
    If (TotalBillingProcedure + 1 > MaxBillingProceduresAllowed) Then
        AButton.BackColor = BaseBackColor
        AButton.ForeColor = BaseTextColor
        frmEventMsgs.Header = "Only" + str(MaxBillingProceduresAllowed) + " Procedures Allowed"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
' Check if present already
    i = InStrPS(AButton.Text, ":")
    If (i > 0) Then
        Id = Trim(Left(AButton.Text, i - 1))
        ClearCPT = IsProcSet(Id, DiagId)
        If (CurrentAvailableIndex <= MaxBillingProceduresAllowed) Then
            If (ClearCPT = 0) And (CurrentAvailableIndex <= MaxBillingProceduresAllowed) Then
                SelectedProcedures(CurrentAvailableIndex).Id = Id
                j = InStrPS(AButton.Text, Chr(13) + Chr(10))
                If (j > 0) Then
                    SelectedProcedures(CurrentAvailableIndex).Diag = Trim(Mid(AButton.Text, i + 1, (j - 1) - i))
                    SelectedProcedures(CurrentAvailableIndex).Name = Trim(Mid(AButton.Text, j + 2, Len(AButton.Text) - j))
                    SelectedProcedures(CurrentAvailableIndex).Ref = Trim(str(AButton.Tag))
                    SelectedProcedures(CurrentAvailableIndex).Highlight = False
                    SelectedProcedures(CurrentAvailableIndex).LinkedDiag = Trim(str(GetDiagnosisLink(SelectedProcedures(CurrentAvailableIndex).Diag)))
                    If (SelectedProcedures(CurrentAvailableIndex).LinkedDiag = "0") Then
                        SelectedProcedures(CurrentAvailableIndex).LinkedDiag = ""
                    End If
                    If (BusLogOn) Then
                        g = VerifyPostOpTest(Trim(SelectedProcedures(CurrentAvailableIndex).Id))
                        If (g = 1) Then
                            If (SetPostOpTest(SelectedProcedures(CurrentAvailableIndex).Id, AMod)) Then
                                SelectedProcedures(CurrentAvailableIndex).Modifier(1) = AMod
                                SelectedProcedures(CurrentAvailableIndex).Modifier(2) = ""
                                SelectedProcedures(CurrentAvailableIndex).Modifier(3) = ""
                                SelectedProcedures(CurrentAvailableIndex).Modifier(4) = ""
                            End If
                        ElseIf (g = 0) Then
                            Call ClearSelection(CurrentAvailableIndex)
                            Exit Sub
                        End If
                    End If
                    AButton.BackColor = ButtonSelectionColor
                    AButton.ForeColor = TextSelectionColor
                    i = InStrPS(AButton.Text, "*")
                    If (i > 0) Then
                        AButton.Text = Trim(Left(AButton.Text, i - 1))
                        AButton.Text = AButton.Text + "*" + Trim(SelectedProcedures(CurrentAvailableIndex).Modifier(1))
                        AButton.Text = AButton.Text + Trim(SelectedProcedures(CurrentAvailableIndex).Modifier(2))
                        AButton.Text = AButton.Text + Trim(SelectedProcedures(CurrentAvailableIndex).Modifier(3))
                        AButton.Text = AButton.Text + Trim(SelectedProcedures(CurrentAvailableIndex).Modifier(4))
                        AButton.Text = AButton.Text + "/" + Trim(SelectedProcedures(CurrentAvailableIndex).LinkedDiag)
                    Else
                        AButton.Text = AButton.Text + "*" + Trim(SelectedProcedures(CurrentAvailableIndex).Modifier(1))
                        AButton.Text = AButton.Text + Trim(SelectedProcedures(CurrentAvailableIndex).Modifier(2))
                        AButton.Text = AButton.Text + Trim(SelectedProcedures(CurrentAvailableIndex).Modifier(3))
                        AButton.Text = AButton.Text + Trim(SelectedProcedures(CurrentAvailableIndex).Modifier(4))
                        AButton.Text = AButton.Text + "/" + Trim(SelectedProcedures(CurrentAvailableIndex).LinkedDiag)
                    End If
                    CurrentAvailableIndex = TotalBillingProcedure + 1
                End If
            Else
                If (CurrentAvailableIndex > MaxBillingProceduresAllowed) Then
                    frmEventMsgs.Header = "Only" + str(MaxBillingProceduresAllowed) + " Procedures Allowed"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                End If
            End If
        End If
    End If
Else
    frmEventMsgs.Header = AButton.Text
    frmEventMsgs.AcceptText = "Change Modifiers"
    frmEventMsgs.RejectText = "De-Select"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Link Diagnosis"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        AButton.BackColor = BaseBackColor
        AButton.ForeColor = BaseTextColor
        AButton.ToolTipText = ""
        i = InStrPS(AButton.Text, "*")
        If (i > 0) Then
            AButton.Text = Left(AButton.Text, i - 1)
        End If
        ATemp = CurrentAvailableIndex - 1
        If (CurrentAvailableIndex = 1) Then
            ATemp = CurrentAvailableIndex
        End If
        For i = 1 To ATemp
            If (Trim(AButton.Tag) = Trim(str(SelectedProcedures(i).Ref))) Then
                SelectedProcedures(i).Id = ""
                SelectedProcedures(i).Diag = ""
                SelectedProcedures(i).OrgDiag = ""
                SelectedProcedures(i).Name = ""
                SelectedProcedures(i).Ref = 0
                SelectedProcedures(i).LinkedDiag = ""
                SelectedProcedures(i).Highlight = False
                For j = 1 To MaxModifierAllowed
                    SelectedProcedures(i).Modifier(j) = ""
                Next j
                ClearCPT = i
                Exit For
            End If
        Next i
        If (ClearCPT > 0) And (ClearCPT <= MaxBillingProceduresAllowed) Then
            For i = ClearCPT To MaxBillingProceduresAllowed - 1
                SelectedProcedures(i).Id = SelectedProcedures(i + 1).Id
                SelectedProcedures(i).Diag = SelectedProcedures(i + 1).Diag
                SelectedProcedures(i).OrgDiag = SelectedProcedures(i + 1).OrgDiag
                SelectedProcedures(i).Name = SelectedProcedures(i + 1).Name
                SelectedProcedures(i).Ref = SelectedProcedures(i + 1).Ref
                SelectedProcedures(i).Highlight = SelectedProcedures(i + 1).Highlight
                For j = 1 To MaxModifierAllowed
                    SelectedProcedures(i).Modifier(j) = SelectedProcedures(i + 1).Modifier(j)
                Next j
                SelectedProcedures(i).LinkedDiag = SelectedProcedures(i + 1).LinkedDiag
            Next i
            SelectedProcedures(MaxBillingProceduresAllowed).Id = ""
            SelectedProcedures(MaxBillingProceduresAllowed).Diag = ""
            SelectedProcedures(MaxBillingProceduresAllowed).OrgDiag = ""
            SelectedProcedures(MaxBillingProceduresAllowed).Name = ""
            SelectedProcedures(MaxBillingProceduresAllowed).Ref = 0
            SelectedProcedures(MaxBillingProceduresAllowed).Highlight = False
            SelectedProcedures(MaxBillingProceduresAllowed).LinkedDiag = ""
            For j = 1 To MaxModifierAllowed
                SelectedProcedures(MaxBillingProceduresAllowed).Modifier(j) = ""
            Next j
            CurrentAvailableIndex = TotalBillingProcedure + 1
            If (CurrentAvailableIndex < 1) Then
                CurrentAvailableIndex = 1
            End If
        End If
' for now leave it there
'        For i = 1 To MaxProcedures
'            If (Trim(AButton.Tag) = Trim(Str(AvailableProcedures(i).Ref))) Then
'                AvailableProcedures(i).Diag = AvailableProcedures(i).OrgDiag
'                If (AvailableProcedures(i).Diag <> "?") Then
'                    AvailableProcedures(i).Id = ""
'                    AvailableProcedures(i).Diag = ""
'                    AvailableProcedures(i).Name = ""
'                    AvailableProcedures(i).Ref = 0
'                    AvailableProcedures(i).Highlight = False
'                    AvailableProcedures(i).LinkedDiag = ""
'                    For j = 1 To MaxModifierAllowed
'                        AvailableProcedures(i).Modifier(j) = ""
'                    Next j
'                    ClearCPT = i
'                End If
'                Exit For
'            End If
'        Next i
'        If (ClearCPT > 0) And (ClearCPT <= MaxProcedures) Then
'            For i = ClearCPT To MaxProcedures - 1
'                AvailableProcedures(i).Id = AvailableProcedures(i + 1).Id
'                AvailableProcedures(i).Diag = AvailableProcedures(i + 1).Diag
'                AvailableProcedures(i).Name = AvailableProcedures(i + 1).Name
'                AvailableProcedures(i).Ref = AvailableProcedures(i + 1).Ref
'                AvailableProcedures(i).Highlight = AvailableProcedures(i + 1).Highlight
'                For j = 1 To MaxModifierAllowed
'                    AvailableProcedures(i).Modifier(j) = AvailableProcedures(i + 1).Modifier(j)
'                Next j
'                AvailableProcedures(i).LinkedDiag = AvailableProcedures(i + 1).LinkedDiag
'            Next i
'            AvailableProcedures(MaxBillingProcedures).Id = ""
'            AvailableProcedures(MaxBillingProcedures).Diag = ""
'            AvailableProcedures(MaxBillingProcedures).Name = ""
'            AvailableProcedures(MaxBillingProcedures).Ref = 0
'            AvailableProcedures(MaxBillingProcedures).Highlight = False
'            AvailableProcedures(MaxBillingProcedures).LinkedDiag = ""
'            For j = 1 To MaxModifierAllowed
'                AvailableProcedures(MaxBillingProcedures).Modifier(j) = ""
'            Next j
'            MaxProcedures = 1
'            For j = 1 To MaxBillingProcedures
'                If (AvailableProcedures(j).Id = "") Then
'                    Exit For
'                End If
'                MaxProcedures = MaxProcedures + 1
'            Next j
'            If (MaxProcedures < 1) Then
'                MaxProcedures = 1
'            End If
'            Call LoadAvailableProcedures
'        End If
    ElseIf (frmEventMsgs.Result = 1) Then
        frmModifiers.ModifierOn = True
        If (frmModifiers.LoadModifiers(True)) Then
            frmModifiers.Show 1
            If (Trim(frmModifiers.Modifiers) <> "") Then
                i = InStrPS(AButton.Text, "*")
                If (i > 0) Then
                    Id = Left(AButton.Text, i - 1)
                End If
                ATemp = -1
                For i = 1 To CurrentAvailableIndex
                    If (InStrPS(Id, Trim(SelectedProcedures(i).Name)) > 0) Then
                        ATemp = i
                        Exit For
                    End If
                Next i
                If (ATemp > -1) Then
                    For i = 1 To Len(frmModifiers.Modifiers) Step 2
                        SelectedProcedures(ATemp).Modifier(Int(i / 2) + 1) = Mid(frmModifiers.Modifiers, i, 2)
                    Next i
                End If
            End If
            CurrentProcIndex = 1
            Call LoadAvailableProcedures
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        SelectedLinkedProcedure = Val(Trim(AButton.Tag))
        frmModifiers.LinkedDiagOn = True
        If (frmModifiers.LoadLinkedDiags(True)) Then
            frmModifiers.Show 1
            If (Trim(frmModifiers.LinkedDiags) <> "") Then
                For i = 1 To CurrentAvailableIndex - 1
                    If (SelectedProcedures(i).Ref = SelectedLinkedProcedure) Then
                        SelectedProcedures(i).LinkedDiag = frmModifiers.LinkedDiags
                        SelectedProcedures(i).Diag = BilledDiagnosis(Val(Left(frmModifiers.LinkedDiags, 1))).Id
                        Exit For
                    End If
                Next i
                CurrentProcIndex = 1
                Call LoadAvailableProcedures
            End If
        End If
    End If
End If
End Sub

Private Sub ClearSelection(Idx As Integer)
Dim j As Integer, i As Integer
For i = Idx To MaxBillingProceduresAllowed - 1
    SelectedProcedures(i).Id = SelectedProcedures(i + 1).Id
    SelectedProcedures(i).Diag = SelectedProcedures(i + 1).Diag
    SelectedProcedures(i).OrgDiag = SelectedProcedures(i + 1).OrgDiag
    SelectedProcedures(i).Name = SelectedProcedures(i + 1).Name
    SelectedProcedures(i).Ref = SelectedProcedures(i + 1).Ref
    SelectedProcedures(i).Highlight = SelectedProcedures(i + 1).Highlight
    For j = 1 To MaxModifierAllowed
        SelectedProcedures(i).Modifier(j) = SelectedProcedures(i + 1).Modifier(j)
    Next j
    SelectedProcedures(i).LinkedDiag = SelectedProcedures(i + 1).LinkedDiag
Next i
SelectedProcedures(MaxBillingProceduresAllowed).Id = ""
SelectedProcedures(MaxBillingProceduresAllowed).Diag = ""
SelectedProcedures(MaxBillingProceduresAllowed).OrgDiag = ""
SelectedProcedures(MaxBillingProceduresAllowed).Name = ""
SelectedProcedures(MaxBillingProceduresAllowed).Ref = 0
SelectedProcedures(MaxBillingProceduresAllowed).Highlight = False
SelectedProcedures(MaxBillingProceduresAllowed).LinkedDiag = ""
For j = 1 To MaxModifierAllowed
    SelectedProcedures(MaxBillingProceduresAllowed).Modifier(j) = ""
Next j
CurrentAvailableIndex = TotalBillingProcedure + 1
If (CurrentAvailableIndex < 1) Then
    CurrentAvailableIndex = 1
End If
End Sub

Private Sub SetButton(Ref As Integer, ProcId As Integer, ABut As fpBtn)
Dim i As Integer
Dim k As Integer
Dim TheId As String
Dim TheDisplayName As String
Dim TheTag As String
Dim q As Integer
Dim NewId As String
NewId = Trim(AvailableProcedures(ProcId).Diag)
If (SelectedProcedures(ProcId).Diag <> NewId) And (NewId <> "") And (Trim(SelectedProcedures(ProcId).Diag) <> "") Then
    NewId = Trim(SelectedProcedures(ProcId).Diag)
End If
q = InStrPS(NewId, ".")
If (q > 0) Then
    q = InStrPS(q + 1, AvailableProcedures(ProcId).Diag, ".")
    If (q > 0) Then
        NewId = Left(AvailableProcedures(ProcId).Diag, q - 1)
    End If
End If
TheId = AvailableProcedures(ProcId).Id
TheDisplayName = AvailableProcedures(ProcId).Id + "  :  " + NewId + Chr(13) + Chr(10) + AvailableProcedures(ProcId).Name
TheTag = Trim(str(AvailableProcedures(ProcId).Ref))
ABut.Text = TheDisplayName
ABut.Tag = TheTag
ABut.Visible = True
i = IsProcSet(TheId, NewId)
If (i > 0) Then
    ABut.BackColor = ButtonSelectionColor
    ABut.ForeColor = TextSelectionColor
    ABut.ToolTipText = ""
    For k = 1 To 4
        ABut.ToolTipText = ABut.ToolTipText + Trim(SelectedProcedures(i).Modifier(k))
    Next k
    ABut.Text = ABut.Text + "*" + ABut.ToolTipText
    If (Trim(SelectedProcedures(i).LinkedDiag) <> "") Then
        ABut.Text = ABut.Text + "/" + Trim(SelectedProcedures(i).LinkedDiag)
    End If
End If
End Sub

Public Function IsProcSet(TheProcedure As String, DiagId As String) As Integer
Dim i As Integer
IsProcSet = 0
For i = 1 To TotalBillingProcedure
    If (Trim(DiagId) <> "") Then
        If (Trim(SelectedProcedures(i).Id) = TheProcedure) And (Trim(SelectedProcedures(i).Diag) = Left(DiagId, Len(Trim(SelectedProcedures(i).Diag)))) Then
            IsProcSet = i
            Exit For
        End If
'    Else
'        If (Trim(SelectedProcedures(i).Id) = TheProcedure) Then
'            IsProcSet = i
'            Exit For
'        End If
    End If
Next i
End Function

Private Function SetBillingProcedure(Ref As Integer, TheProcedure As String, TheDiag As String, TheMods As String, TheLinks As String, TheHigh As Boolean) As Boolean
Dim i As Integer
SetBillingProcedure = False
TheProcedure = ""
TheMods = ""
TheDiag = ""
TheHigh = False
If (Ref > 0) Then
    If (Trim(SelectedProcedures(Ref).Id) <> "") Then
        TheProcedure = SelectedProcedures(Ref).Id
        TheDiag = SelectedProcedures(Ref).Diag
        For i = 1 To MaxModifierAllowed
            TheMods = TheMods + SelectedProcedures(Ref).Modifier(i)
        Next i
        TheLinks = SelectedProcedures(Ref).LinkedDiag
        TheHigh = SelectedProcedures(Ref).Highlight
        SetBillingProcedure = True
    End If
End If
End Function

Public Function InitializeBillingProcedure() As Boolean
Dim i As Integer
Dim j As Integer
    On Error GoTo lInitializeBillingProcedure_Error

For i = 1 To MaxBillingProcedures
    AvailableProcedures(i).Id = ""
    AvailableProcedures(i).Diag = ""
    AvailableProcedures(i).OrgDiag = ""
    AvailableProcedures(i).Name = ""
    AvailableProcedures(i).Ref = 0
    AvailableProcedures(i).Highlight = False
    AvailableProcedures(i).WarningOn = False
Next i
For i = 1 To MaxBillingProcedures
    OtherAvailableProcedures(i).Id = ""
    OtherAvailableProcedures(i).Diag = ""
    OtherAvailableProcedures(i).OrgDiag = ""
    OtherAvailableProcedures(i).Name = ""
    OtherAvailableProcedures(i).Ref = 0
    OtherAvailableProcedures(i).Highlight = False
    OtherAvailableProcedures(i).WarningOn = False
Next i
For i = 1 To MaxBillingProcedures
    SortedProcedures(i).Id = ""
    SortedProcedures(i).Diag = ""
    SortedProcedures(i).OrgDiag = ""
    SortedProcedures(i).Name = ""
    SortedProcedures(i).Ref = 0
    SortedProcedures(i).Highlight = False
    SortedProcedures(i).WarningOn = False
Next i
For i = 1 To MaxBillingProceduresAllowed
    SelectedProcedures(i).Id = ""
    SelectedProcedures(i).Diag = ""
    SelectedProcedures(i).OrgDiag = ""
    SelectedProcedures(i).Name = ""
    SelectedProcedures(i).Ref = 0
    SelectedProcedures(i).LinkedDiag = ""
    For j = 1 To MaxModifierAllowed
        SelectedProcedures(i).Modifier(j) = ""
    Next j
    SelectedProcedures(i).Highlight = False
    SelectedProcedures(i).WarningOn = False
Next i
InitializeBillingProcedure = True

    Exit Function

lInitializeBillingProcedure_Error:

    LogError "frmSetProcedures", "InitializeBillingProcedure", Err, Err.Description
End Function

Public Function TotalBillingProcedure() As Integer
Dim i As Integer
TotalBillingProcedure = 0
For i = 1 To MaxBillingProceduresAllowed
    If (Trim(SelectedProcedures(i).Id) = "") Then
        TotalBillingProcedure = i - 1
        Exit For
    Else
        TotalBillingProcedure = i
    End If
Next i
End Function

Public Function GetBillingProcedure(RefId As Integer, AProcedure As String, ADiag As String, AMod As String, ALnk As String, AHigh As Boolean) As Boolean
GetBillingProcedure = SetBillingProcedure(RefId, AProcedure, ADiag, AMod, ALnk, AHigh)
End Function

Private Sub SetUpProcedure(Temp As String)
Dim i As Integer
If (Trim(Temp) <> "") Then
    If Not (IsProcSet(Trim(Mid(Temp, 1, 10)), "")) And (CurrentAvailableIndex <= MaxBillingProceduresAllowed) Then
        If (MaxProcedures < 1) Then
            MaxProcedures = MaxProcedures + 1
        End If
        If (AvailableProcedures(MaxProcedures).Id <> "") Then
            MaxProcedures = MaxProcedures + 1
        End If
        AvailableProcedures(MaxProcedures).Id = Trim(Mid(Temp, 1, 10))
        AvailableProcedures(MaxProcedures).Name = Trim(Mid(Temp, 11, 40))
        AvailableProcedures(MaxProcedures).Diag = "?"
        AvailableProcedures(MaxProcedures).OrgDiag = "?"
        AvailableProcedures(MaxProcedures).Ref = Val(Trim(Mid(Temp, 76, Len(Temp) - 75)))
        AvailableProcedures(MaxProcedures).Highlight = False
        Call SortTheList(MaxProcedures)
        SelectedProcedures(CurrentAvailableIndex).Id = Trim(Mid(Temp, 1, 10))
        SelectedProcedures(CurrentAvailableIndex).Name = Trim(Mid(Temp, 11, 40))
        SelectedProcedures(CurrentAvailableIndex).Diag = "?"
        SelectedProcedures(CurrentAvailableIndex).OrgDiag = "?"
        SelectedProcedures(CurrentAvailableIndex).Ref = Val(Trim(Mid(Temp, 76, Len(Temp) - 75)))
        SelectedProcedures(CurrentAvailableIndex).Highlight = False
        For i = 1 To MaxModifierAllowed
            SelectedProcedures(CurrentAvailableIndex).Modifier(i) = ""
        Next i
        SelectedProcedures(CurrentAvailableIndex).LinkedDiag = ""
        SelectedProcedures(CurrentAvailableIndex).WarningOn = False
        CurrentAvailableIndex = TotalBillingProcedure + 1
    End If
End If
End Sub

Private Function FindButtonProc(Ref, fpButton As fpBtn) As Boolean
FindButtonProc = False
If (cmdProcedure1.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure1
ElseIf (cmdProcedure2.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure2
ElseIf (cmdProcedure3.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure3
ElseIf (cmdProcedure4.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure4
ElseIf (cmdProcedure5.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure5
ElseIf (cmdProcedure6.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure6
ElseIf (cmdProcedure7.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure7
ElseIf (cmdProcedure8.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure8
ElseIf (cmdProcedure9.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure9
ElseIf (cmdProcedure10.Tag = Trim(str(SelectedProcedures(Ref).Ref))) Then
    FindButtonProc = True
    Set fpButton = cmdProcedure10
End If
End Function

Private Function GetStartingValue(TheText As String, TheType As String) As Boolean
TheText = ""
GetStartingValue = True
If (TheType = "N") Then
    frmNumericPad.NumPad_Field = "Starting with Procedure Number"
    frmNumericPad.NumPad_Result = ""
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_ChoiceOn1 = False
    frmNumericPad.NumPad_Choice1 = ""
    frmNumericPad.NumPad_ChoiceOn2 = False
    frmNumericPad.NumPad_Choice2 = ""
    frmNumericPad.NumPad_ChoiceOn3 = False
    frmNumericPad.NumPad_Choice3 = ""
    frmNumericPad.NumPad_ChoiceOn4 = False
    frmNumericPad.NumPad_Choice4 = ""
    frmNumericPad.NumPad_ChoiceOn5 = False
    frmNumericPad.NumPad_Choice5 = ""
    frmNumericPad.NumPad_ChoiceOn6 = False
    frmNumericPad.NumPad_Choice6 = ""
    frmNumericPad.NumPad_ChoiceOn7 = False
    frmNumericPad.NumPad_Choice7 = ""
    frmNumericPad.NumPad_ChoiceOn8 = False
    frmNumericPad.NumPad_Choice8 = ""
    frmNumericPad.NumPad_ChoiceOn9 = False
    frmNumericPad.NumPad_Choice9 = ""
    frmNumericPad.NumPad_ChoiceOn10 = False
    frmNumericPad.NumPad_Choice10 = ""
    frmNumericPad.NumPad_ChoiceOn11 = False
    frmNumericPad.NumPad_Choice11 = ""
    frmNumericPad.NumPad_ChoiceOn12 = False
    frmNumericPad.NumPad_Choice12 = ""
    frmNumericPad.NumPad_ChoiceOn13 = False
    frmNumericPad.NumPad_Choice13 = ""
    frmNumericPad.NumPad_ChoiceOn14 = False
    frmNumericPad.NumPad_Choice14 = ""
    frmNumericPad.NumPad_ChoiceOn15 = False
    frmNumericPad.NumPad_Choice15 = ""
    frmNumericPad.NumPad_ChoiceOn16 = False
    frmNumericPad.NumPad_Choice16 = ""
    frmNumericPad.NumPad_ChoiceOn17 = False
    frmNumericPad.NumPad_Choice17 = ""
    frmNumericPad.NumPad_ChoiceOn18 = False
    frmNumericPad.NumPad_Choice18 = ""
    frmNumericPad.NumPad_ChoiceOn19 = False
    frmNumericPad.NumPad_Choice19 = ""
    frmNumericPad.NumPad_ChoiceOn20 = False
    frmNumericPad.NumPad_Choice20 = ""
    frmNumericPad.NumPad_ChoiceOn21 = False
    frmNumericPad.NumPad_Choice21 = ""
    frmNumericPad.NumPad_ChoiceOn22 = False
    frmNumericPad.NumPad_Choice22 = ""
    frmNumericPad.NumPad_ChoiceOn23 = False
    frmNumericPad.NumPad_Choice23 = ""
    frmNumericPad.NumPad_ChoiceOn24 = False
    frmNumericPad.NumPad_Choice24 = ""
    frmNumericPad.Show 1
    If Not (frmNumericPad.NumPad_Quit) Then
        TheText = Trim(frmNumericPad.NumPad_Result)
    Else
        TheText = ""
    End If
ElseIf (TheType = "A") Then
    frmAlphaPad.NumPad_Field = "Starting with Procedure Alpha"
    frmAlphaPad.NumPad_Result = ""
    frmAlphaPad.NumPad_DisplayFull = True
    frmAlphaPad.Show 1
    If Not (frmAlphaPad.NumPad_Quit) Then
        TheText = Trim(frmAlphaPad.NumPad_Result)
    Else
        TheText = ""
    End If
End If
End Function

Public Function RetainBilledProcedures(PatId As Long) As Boolean
Dim u As Integer
Dim Totals As Integer
Dim RetModifier As String
Dim RetId As String
Dim RetString As String
Dim RetLinks As String
Dim RetHigh As Boolean, RHigh As String
Dim FileNum As Integer
Dim TheFile As String
RetainBilledProcedures = False
If (PatId > 0) Then
    FileNum = FreeFile
'    TheFile = DoctorInterfaceDirectory + "ProcBill" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
'    FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(FileNum)
'    Totals = TotalBillingProcedure
'    If (Totals > 0) Then
'        For u = 1 To Totals
'            If (GetBillingProcedure(u, RetString, RetId, RetModifier, RetLinks, RetHigh)) Then
'                RHigh = "F"
'                If (RetHigh) Then
'                    RHigh = "T"
'                End If
'                Print #FileNum, RetId + "-" + RHigh + RetString + ":" + RetModifier + "/" + RetLinks
'            End If
'        Next u
'    End If
  '  FM.CloseFile CLng(FileNum)
    RetainBilledProcedures = True
End If
End Function

Private Function GetProc(TheProc As String, TheName As String, WarningOn As Boolean) As Long
Dim RetProc As DiagnosisMasterProcedures
Set RetProc = New DiagnosisMasterProcedures
GetProc = 0
TheName = ""
WarningOn = False
RetProc.ProcedureCPT = Trim(TheProc)
RetProc.ProcedureDiagnosis = ""
RetProc.ProcedureRank = 0
If (RetProc.FindProcedure > 0) Then
    If (RetProc.SelectProcedure(1)) Then
        TheName = Trim(RetProc.ProcedureName)
        If (RetProc.ProcedureInsClass6 = "T") Then
            WarningOn = True
        End If
        GetProc = RetProc.ProcedureId
    End If
End If
Set RetProc = Nothing
End Function

Private Sub SortTheList(TheSize As Integer)
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim DisplayText As String
    lstsort.Clear
Erase SortedProcedures
If (AvailableProcedures(TheSize).Id <> "") Then
    TheSize = TheSize + 1
End If
For i = 1 To TheSize - 1
    DisplayText = Space(40)
    Mid(DisplayText, 1, 10) = AvailableProcedures(i).Id
    Mid(DisplayText, 11, 10) = AvailableProcedures(i).Diag
    Mid(DisplayText, 21, 5) = Trim(str(i))
    If (Trim(Left(DisplayText, 10)) <> "") Then
            lstsort.AddItem DisplayText
    End If
Next i
    For i = 0 To lstsort.ListCount - 1
        j = Val(Trim(Mid(lstsort.List(i), 21, 5)))
        SortedProcedures(i + 1).Id = AvailableProcedures(j).Id
        SortedProcedures(i + 1).Diag = AvailableProcedures(j).Diag
        SortedProcedures(i + 1).OrgDiag = AvailableProcedures(j).OrgDiag
        SortedProcedures(i + 1).LinkedDiag = AvailableProcedures(j).LinkedDiag
        For k = 1 To 4
            SortedProcedures(i + 1).Modifier(k) = AvailableProcedures(j).Modifier(k)
        Next k
        SortedProcedures(i + 1).Name = AvailableProcedures(j).Name
        SortedProcedures(i + 1).Ref = AvailableProcedures(j).Ref
        SortedProcedures(i + 1).WarningOn = AvailableProcedures(j).WarningOn
    Next i
    TheSize = lstsort.ListCount
If (TheSize > 1) Then
    Erase AvailableProcedures
    For i = 1 To TheSize
        AvailableProcedures(i).Id = SortedProcedures(i).Id
        AvailableProcedures(i).Diag = SortedProcedures(i).Diag
        AvailableProcedures(i).OrgDiag = SortedProcedures(i).OrgDiag
        AvailableProcedures(i).LinkedDiag = SortedProcedures(i).LinkedDiag
        For k = 1 To 4
            AvailableProcedures(i).Modifier(k) = SortedProcedures(i).Modifier(k)
        Next k
        AvailableProcedures(i).Name = SortedProcedures(i).Name
        AvailableProcedures(i).Ref = SortedProcedures(i).Ref
        AvailableProcedures(i).WarningOn = SortedProcedures(i).WarningOn
    Next i
End If
lstsort.Clear
TheSize = TheSize + 1
End Sub

Public Function PurgeRetainedProcedures(PatId As Long, ABack As Boolean) As Boolean
Dim TheFile As String
Dim OtherFile As String
PurgeRetainedProcedures = True
If (PatId > 0) Then
    Call InitializeBillingProcedure
    TheFile = DoctorInterfaceDirectory + "ProcBill" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
    OtherFile = DoctorInterfaceDirectory + "Backup\ProcBill" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
    If (FM.IsFileThere(TheFile)) Then
        If (ABack) Then
            FM.FileCopy TheFile, OtherFile
        End If
        FM.Kill TheFile
    End If
    
    TheFile = DoctorInterfaceDirectory + "ProcBill_Icd10" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
    OtherFile = DoctorInterfaceDirectory + "Backup\ProcBill_Icd10" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
    If (FM.IsFileThere(TheFile)) Then
        If (ABack) Then
            FM.FileCopy TheFile, OtherFile
        End If
        FM.Kill TheFile
    End If
    
End If
End Function

Private Function SetTestProcedures(PatId As Long) As Boolean
Dim u As Integer
Dim TestId As Integer
Dim i As Integer, n As Integer
Dim k As Integer, MaxChoices As Integer
Dim ARef As Long
Dim AProc As String, AName As String
Dim MyFile As String, TheFile As String
Dim TheOrder As String, ADiag As String
Dim AllProc(10) As String
Dim AllOn(10) As Boolean
Dim WarningOn As Boolean
Dim DoIt As Boolean
Dim ClinicalClass As DynamicClass
SetTestProcedures = True
TestId = 0
Erase AllProc
For i = 1 To 10
    AllOn(i) = True
Next i
If (PatId > 0) Then
    TheFile = GetTestFileName("*")
    Set ClinicalClass = New DynamicClass
    MyFile = Dir(TheFile)
    While (MyFile <> "")
        TheOrder = Mid(MyFile, 2, 3)
        ClinicalClass.QuestionParty = "T"
        ClinicalClass.Question = ""
        ClinicalClass.QuestionSet = "First Visit"
        ClinicalClass.QuestionOrder = TheOrder
        If (ClinicalClass.RetrieveClassbyQuestion) Then
            If (Trim(ClinicalClass.ControlName) <> "") Then
                AProc = Trim(ClinicalClass.ControlName)
                If (Mid(AProc, Len(AProc), 1) = ",") Then
                    AProc = Left(AProc, Len(AProc) - 1)
                End If
                k = 0
                n = 0
                i = InStrPS(AProc, ",")
                While (i > 0)
                    k = k + 1
                    AllProc(k) = Trim(Mid(AProc, n + 1, (i - 1) - n))
                    If (Mid(AllProc(k), Len(AllProc(k)), 1) = "-") Then
                        AllOn(k) = False
                        Mid(AllProc(k), Len(AllProc(k)), 1) = " "
                        AllProc(k) = Trim(AllProc(k))
                    End If
                    n = i
                    i = InStrPS(n + 1, AProc, ",")
                Wend
                k = k + 1
                AllProc(k) = Mid(AProc, n + 1, Len(AProc) - n)
                If (Trim(AllProc(k)) <> "") Then
                    If (Mid(AllProc(k), Len(AllProc(k)), 1) = "-") Then
                        AllOn(k) = False
                        Mid(AllProc(k), Len(AllProc(k)), 1) = " "
                        AllProc(k) = Trim(AllProc(k))
                    End If
                    MaxChoices = k
                    For k = 1 To MaxChoices
                        DoIt = True
                        For u = 1 To MaxProcedures
                            If (AllProc(k) = Trim(AvailableProcedures(u).Id)) Then
                                DoIt = False
                                Exit For
                            End If
                        Next u
                        If (DoIt) Then
                            AProc = Trim(AllProc(k))
                            ARef = GetProc(AProc, AName, WarningOn)
                            If (Trim(AName) <> "") Then
                                TestId = TestId + 1
                                MaxProcedures = MaxProcedures + 1
                                AvailableProcedures(MaxProcedures).Id = Trim(AProc)
                                AvailableProcedures(MaxProcedures).Name = Trim(AName)
                                AvailableProcedures(MaxProcedures).Diag = "?"
                                If (ProcedureMatching(AProc, ADiag)) Then
                                    AvailableProcedures(MaxProcedures).Diag = ADiag
                                End If
                                AvailableProcedures(MaxProcedures).OrgDiag = "?"
                                AvailableProcedures(MaxProcedures).Ref = TestId * -1
                                AvailableProcedures(MaxProcedures).Highlight = False
                                AvailableProcedures(MaxProcedures).WarningOn = WarningOn
                                AvailableProcedures(MaxProcedures).LinkedDiag = ""
                                If (ADiag <> "?") Then
                                    AvailableProcedures(MaxProcedures).LinkedDiag = GetDiagnosisLink(ADiag)
                                End If
                                For i = 1 To MaxModifierAllowed
                                    AvailableProcedures(MaxProcedures).Modifier(i) = ""
                                Next i
                                If (Not (frmSystems1.IsPostOpOn)) Or (Not (BusLogOn)) Then
                                    If (CurrentAvailableIndex <= MaxBillingProceduresAllowed) Then
                                        ADiag = ""
                                        SelectedProcedures(CurrentAvailableIndex).Id = Trim(AProc)
                                        SelectedProcedures(CurrentAvailableIndex).Name = Trim(AName)
                                        SelectedProcedures(CurrentAvailableIndex).Diag = "?"
                                        If (ProcedureMatching(AProc, ADiag)) Then
                                            SelectedProcedures(CurrentAvailableIndex).Diag = ADiag
                                        End If
                                        SelectedProcedures(CurrentAvailableIndex).OrgDiag = "?"
                                        If (ProcedureMatching(AProc, ADiag)) Then
                                            SelectedProcedures(CurrentAvailableIndex).OrgDiag = ADiag
                                        End If
                                        SelectedProcedures(CurrentAvailableIndex).Ref = TestId * -1
                                        SelectedProcedures(CurrentAvailableIndex).Highlight = False
                                        SelectedProcedures(CurrentAvailableIndex).WarningOn = WarningOn
                                        SelectedProcedures(CurrentAvailableIndex).LinkedDiag = ""
                                        If (ADiag <> "?") Then
                                            SelectedProcedures(CurrentAvailableIndex).LinkedDiag = GetDiagnosisLink(ADiag)
                                        End If
                                        For i = 1 To MaxModifierAllowed
                                            SelectedProcedures(CurrentAvailableIndex).Modifier(i) = ""
                                        Next i
                                        CurrentAvailableIndex = TotalBillingProcedure + 1
                                    End If
                                End If
                            End If
                        End If
                    Next k
                End If
            End If
        End If
        MyFile = Dir
    Wend
    Set ClinicalClass = Nothing
End If
End Function

Private Function LoadOtherProcedures() As Boolean
Dim q As Integer, p As Integer
Dim j As Integer, i As Integer
Dim Id As String
Dim NewId As String
Dim AEye As String, ASevA As String
Dim ASevB As String, Diag As String, AHigh As Boolean
Dim TotalDiags As Integer
Dim RetrieveProcedure As DiagnosisMasterProcedures
Set RetrieveProcedure = New DiagnosisMasterProcedures
LoadOtherProcedures = False
MaxOtherProcedures = 0
CurrentOtherProcIndex = 1
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = &H9B9626
BaseTextColor = &HFFFFFF
Erase OtherAvailableProcedures
p = 1
TotalDiags = TotalBilledDiagnosis
For i = 1 To TotalDiags
    If (GetBilledDiagnosis(i, Id, Diag, AEye, ASevA, ASevB, AHigh)) Then
        NewId = Id
        q = InStrPS(NewId, ".")
        If (q > 0) Then
            q = InStrPS(q + 1, Id, ".")
            If (q > 0) Then
                NewId = Left(Id, q - 1)
            End If
        End If
        RetrieveProcedure.ProcedureDiagnosis = NewId
        RetrieveProcedure.ProcedureRank = 0
        RetrieveProcedure.ProcedureLinkType = PLinkType
        RetrieveProcedure.ProcedureInsClass1 = frmSystems1.InsurerClassId
        RetrieveProcedure.ProcedureInsClass2 = frmSystems1.InsurerClassId
        RetrieveProcedure.ProcedureInsClass3 = frmSystems1.InsurerClassId
        RetrieveProcedure.ProcedureInsClass4 = frmSystems1.InsurerClassId
        RetrieveProcedure.ProcedureInsClass5 = frmSystems1.InsurerClassId
        RetrieveProcedure.ProcedureInsClass6 = ""
        If (RetrieveProcedure.FindProcedurebyInsClass > 0) Then
'        If (RetrieveProcedure.FindProcedurebyDiagnosis > 0) Then
            j = 1
            While (RetrieveProcedure.SelectProcedure(j)) And (p < MaxBillingProcedures)
                OtherAvailableProcedures(p).Id = Trim(RetrieveProcedure.ProcedureCPT)
                OtherAvailableProcedures(p).Diag = Trim(RetrieveProcedure.ProcedureDiagnosis)
                OtherAvailableProcedures(p).OrgDiag = Trim(RetrieveProcedure.ProcedureDiagnosis)
                OtherAvailableProcedures(p).Name = Trim(RetrieveProcedure.ProcedureName)
                OtherAvailableProcedures(p).Ref = RetrieveProcedure.ProcedureId
                OtherAvailableProcedures(p).Highlight = False
                OtherAvailableProcedures(p).WarningOn = False
                If (RetrieveProcedure.ProcedureInsClass6 = "T") Then
                    OtherAvailableProcedures(p).WarningOn = True
                End If
                p = p + 1
                j = j + 1
            Wend
        End If
    End If
Next i
MaxOtherProcedures = p - 1
If (MaxOtherProcedures > 0) Then
    LoadOtherProcedures = LoadOtherAvailableProcedures
Else
    LoadOtherProcedures = True
    cmdMoreCPTs.Visible = False
End If
cmdOtherProcs.Visible = False
cmdOthers.Text = "Current Procedures"
Set RetrieveProcedure = Nothing
End Function

Private Function LoadOtherAvailableProcedures() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
LoadOtherAvailableProcedures = False
Call ClearCPTs
Call ClearOtherCPTs
i = CurrentOtherProcIndex - 1
If (i < 1) Then
    i = 1
End If
DoEvents
ButtonCnt = 1
While (i <= MaxOtherProcedures) And (ButtonCnt < 11)
    If (ButtonCnt = 1) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther1)
    ElseIf (ButtonCnt = 2) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther2)
    ElseIf (ButtonCnt = 3) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther3)
    ElseIf (ButtonCnt = 4) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther4)
    ElseIf (ButtonCnt = 5) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther5)
    ElseIf (ButtonCnt = 6) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther6)
    ElseIf (ButtonCnt = 7) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther7)
    ElseIf (ButtonCnt = 8) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther8)
    ElseIf (ButtonCnt = 9) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther9)
    ElseIf (ButtonCnt = 10) Then
        Call SetOtherButton(ButtonCnt, i, cmdOther10)
    End If
    ButtonCnt = ButtonCnt + 1
    i = i + 1
Wend
CurrentOtherProcIndex = i
cmdOthers.Text = "Current Procedures"
LoadOtherAvailableProcedures = True
If (CurrentOtherProcIndex <= MaxOtherProcedures) Then
    cmdMoreCPTs.Text = "More Linked Procedures"
    cmdMoreCPTs.Visible = True
Else
    If (MaxOtherProcedures > 10) Then
        cmdMoreCPTs.Text = "Beginning of Linked Procedure List"
        cmdMoreCPTs.Visible = True
    Else
        cmdMoreCPTs.Visible = False
    End If
End If
cmdOtherProcs.Visible = False
End Function

Private Sub ClearOtherCPTs()
cmdOther1.Visible = False
cmdOther1.BackColor = BaseBackColor
cmdOther1.ForeColor = BaseTextColor
cmdOther2.Visible = False
cmdOther2.BackColor = BaseBackColor
cmdOther2.ForeColor = BaseTextColor
cmdOther3.Visible = False
cmdOther3.BackColor = BaseBackColor
cmdOther3.ForeColor = BaseTextColor
cmdOther4.Visible = False
cmdOther4.BackColor = BaseBackColor
cmdOther4.ForeColor = BaseTextColor
cmdOther5.Visible = False
cmdOther5.BackColor = BaseBackColor
cmdOther5.ForeColor = BaseTextColor
cmdOther6.Visible = False
cmdOther6.BackColor = BaseBackColor
cmdOther6.ForeColor = BaseTextColor
cmdOther7.Visible = False
cmdOther7.BackColor = BaseBackColor
cmdOther7.ForeColor = BaseTextColor
cmdOther8.Visible = False
cmdOther8.BackColor = BaseBackColor
cmdOther8.ForeColor = BaseTextColor
cmdOther9.Visible = False
cmdOther9.BackColor = BaseBackColor
cmdOther9.ForeColor = BaseTextColor
cmdOther10.Visible = False
cmdOther10.BackColor = BaseBackColor
cmdOther10.ForeColor = BaseTextColor
End Sub

Private Sub SetOtherButton(Ref As Integer, ProcId As Integer, ABut As fpBtn)
Dim TheId As String
Dim TheDisplayName As String
Dim TheTag As String
Dim q As Integer
Dim NewId As String
NewId = Trim(OtherAvailableProcedures(ProcId).Diag)
q = InStrPS(NewId, ".")
If (q > 0) Then
    q = InStrPS(q + 1, OtherAvailableProcedures(ProcId).Diag, ".")
    If (q > 0) Then
        NewId = Left(OtherAvailableProcedures(ProcId).Diag, q - 1)
    End If
End If
TheId = OtherAvailableProcedures(ProcId).Id
TheDisplayName = OtherAvailableProcedures(ProcId).Id + "  :  " + NewId + Chr(13) + Chr(10) + OtherAvailableProcedures(ProcId).Name
TheTag = Trim(str(OtherAvailableProcedures(ProcId).Ref))
ABut.Text = TheDisplayName
ABut.Tag = TheTag
ABut.Visible = True
ABut.ToolTipText = ""
End Sub

Private Sub TriggerOtherProcedure(AButton As fpBtn)
Dim j As Integer
Dim i As Integer
Dim Id As String
Dim ClearCPT As Integer
i = InStrPS(AButton.Text, ":")
If (i > 0) Then
    ClearCPT = 0
    Id = Trim(Left(AButton.Text, i - 1))
    For j = 1 To MaxProcedures
        If (Id = AvailableProcedures(j).Id) Then
            ClearCPT = j
            Exit For
        End If
    Next j
    If (ClearCPT = 0) Then
        If (MaxProcedures < 1) Then
            MaxProcedures = 1
        End If
        j = InStrPS(i, AButton.Text, vbCrLf)
        If (j > 0) Then
            AvailableProcedures(MaxProcedures).Id = Id
            AvailableProcedures(MaxProcedures).Diag = Trim(Mid(AButton.Text, i + 1, (j - 1) - i))
            AvailableProcedures(MaxProcedures).Name = Trim(Mid(AButton.Text, j + 2, Len(AButton.Text) - j))
            AvailableProcedures(MaxProcedures).Ref = Trim(str(AButton.Tag))
            AvailableProcedures(MaxProcedures).LinkedDiag = Trim(str(GetDiagnosisLink(AvailableProcedures(MaxProcedures).Diag)))
            MaxProcedures = MaxProcedures + 1
            If (Trim(SelectedProcedures(CurrentAvailableIndex).Id) <> "") Then
                CurrentAvailableIndex = TotalBillingProcedure + 1
            End If
            SelectedProcedures(CurrentAvailableIndex).Id = Id
            SelectedProcedures(CurrentAvailableIndex).Diag = Trim(Mid(AButton.Text, i + 1, (j - 1) - i))
            SelectedProcedures(CurrentAvailableIndex).Name = Trim(Mid(AButton.Text, j + 2, Len(AButton.Text) - j))
            SelectedProcedures(CurrentAvailableIndex).Ref = Trim(str(AButton.Tag))
            SelectedProcedures(CurrentAvailableIndex).LinkedDiag = AvailableProcedures(MaxProcedures).LinkedDiag
            SelectedProcedures(CurrentAvailableIndex).WarningOn = AvailableProcedures(MaxProcedures).WarningOn
            For i = 1 To MaxModifierAllowed
                SelectedProcedures(CurrentAvailableIndex).Modifier(i) = ""
            Next i
            CurrentAvailableIndex = TotalBillingProcedure + 1
        End If
    End If
    Call cmdOthers_Click
End If
End Sub

Private Function ProcedureMatching(AProc As String, ADiag As String) As Boolean
Dim q As Integer, i As Integer
Dim r As Integer, TotalDiags As Integer
Dim Id As String, NewId As String
Dim AEye As String, ASevA As String
Dim ASevB As String, Diag As String, AHigh As Boolean
Dim RetrieveProcedure As DiagnosisMasterProcedures
ADiag = "?"
ProcedureMatching = False
If (Trim(AProc) <> "") Then
    TotalDiags = TotalBilledDiagnosis
    For i = 1 To TotalDiags
        If (GetBilledDiagnosis(i, Id, Diag, AEye, ASevA, ASevB, AHigh)) Then
            NewId = Id
            r = InStrPS(NewId, ".")
            If (r > 0) Then
                q = InStrPS(r + 1, Id, ".")
                If (q > 0) Then
                    NewId = Left(Id, q - 1)
                End If
            End If
            Set RetrieveProcedure = New DiagnosisMasterProcedures
            RetrieveProcedure.ProcedureCPT = AProc
            RetrieveProcedure.ProcedureDiagnosis = NewId
            RetrieveProcedure.ProcedureRank = 0
            RetrieveProcedure.ProcedureLinkType = ""
            If (RetrieveProcedure.FindProcedure > 0) Then
                ADiag = NewId
                ProcedureMatching = True
                Set RetrieveProcedure = Nothing
                Exit For
            End If
            Set RetrieveProcedure = Nothing
        End If
    Next i
End If
End Function

Public Function PurgeTestProcedure(PatId As Long, TstId As String) As Boolean
Dim PostIt As Boolean
Dim q As Integer, MaxProc As Integer
Dim i As Integer, n As Integer, k As Integer
Dim FileNum As Integer, FileNum1 As Integer
Dim AProc As String
Dim AllProc(20) As String, Rec As String
Dim RetId As String
Dim TheFile As String, NewFile As String
Dim ClinicalClass As DynamicClass
PurgeTestProcedure = True
Erase AllProc
If (PatId > 0) And (Trim(TstId) <> "") Then
    k = 0
    TheFile = GetTestFileName(TstId)
    Set ClinicalClass = New DynamicClass
    ClinicalClass.QuestionParty = "T"
    ClinicalClass.Question = ""
    ClinicalClass.QuestionSet = "First Visit"
    ClinicalClass.QuestionOrder = TstId
    If (ClinicalClass.RetrieveClassbyQuestion) Then
        If (Trim(ClinicalClass.ControlName) <> "") Then
            AProc = Trim(ClinicalClass.ControlName)
            n = 0
            i = InStrPS(AProc, ",")
            While (i > 0)
                k = k + 1
                AllProc(k) = Trim(Mid(AProc, n + 1, (i - 1) - n))
                If (Mid(AllProc(k), Len(AllProc(k)), 1) = "-") Then
                    Mid(AllProc(k), Len(AllProc(k)), 1) = " "
                    AllProc(k) = Trim(AllProc(k))
                End If
                n = i
                i = InStrPS(n + 1, AProc, ",")
            Wend
            k = k + 1
            AllProc(k) = Mid(AProc, n + 1, Len(AProc) - n)
            If (Mid(AllProc(k), Len(AllProc(k)), 1) = "-") Then
                Mid(AllProc(k), Len(AllProc(k)), 1) = " "
                AllProc(k) = Trim(AllProc(k))
            End If
        End If
    End If
    Set ClinicalClass = Nothing
    MaxProc = k
    If (MaxProc > 0) Then
        TheFile = DoctorInterfaceDirectory + "ProcBill" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        NewFile = DoctorInterfaceDirectory + "ProcBill" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txtA"
        If (FM.IsFileThere(TheFile)) Then
            FileNum = FreeFile
            FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
            FileNum1 = FreeFile
            FM.OpenFile NewFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(FileNum1)
            While Not (EOF(FileNum))
                PostIt = True
                Line Input #FileNum, Rec
                k = InStrPS(Rec, "-")
                If (k > 0) Then
                    RetId = Trim(Mid(Rec, k + 1, 5))
                    For q = 1 To MaxProc
                        If (AllProc(q) = RetId) Then
                            PostIt = False
                            Exit For
                        End If
                    Next q
                    If (PostIt) Then
                        Print #FileNum1, Rec
                    End If
                End If
            Wend
            FM.CloseFile CLng(FileNum)
            FM.CloseFile CLng(FileNum1)
            FM.Kill TheFile
            FM.Name NewFile, TheFile
        End If
    End If
End If
End Function

Public Function ResetDiagnosisLinks(DeletedDiag As String, iLoc As Integer) As Boolean
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim Y As Integer
If (Trim(DeletedDiag) <> "") And (iLoc > 0) Then
    k = TotalBillingProcedure
    For i = 1 To k
        For j = 1 To Len(SelectedProcedures(i).LinkedDiag)
            If (Mid(SelectedProcedures(i).LinkedDiag, j, 1) = Trim(str(iLoc))) Then
                Mid(SelectedProcedures(i).LinkedDiag, j, 1) = " "
            End If
        Next j
        If (SelectedProcedures(i).Diag = Left(DeletedDiag, Len(Trim(SelectedProcedures(i).Diag)))) And (Trim(SelectedProcedures(i).Diag) <> "") Then
            If (Trim(SelectedProcedures(i).LinkedDiag) = "") Then
                For j = i To MaxBillingProceduresAllowed - 1
                    SelectedProcedures(j).Id = SelectedProcedures(j + 1).Id
                    SelectedProcedures(j).Diag = SelectedProcedures(j + 1).Diag
                    SelectedProcedures(j).OrgDiag = SelectedProcedures(j + 1).OrgDiag
                    SelectedProcedures(j).Name = SelectedProcedures(j + 1).Name
                    SelectedProcedures(j).Ref = SelectedProcedures(j + 1).Ref
                    SelectedProcedures(j).Highlight = SelectedProcedures(j + 1).Highlight
                    For Y = 1 To MaxModifierAllowed
                        SelectedProcedures(j).Modifier(Y) = SelectedProcedures(j + 1).Modifier(Y)
                    Next Y
                    SelectedProcedures(j).LinkedDiag = SelectedProcedures(j + 1).LinkedDiag
                    SelectedProcedures(j).Highlight = SelectedProcedures(j + 1).Highlight
                    SelectedProcedures(j).WarningOn = SelectedProcedures(j + 1).WarningOn
                Next j
                SelectedProcedures(MaxBillingProceduresAllowed).Id = ""
                SelectedProcedures(MaxBillingProceduresAllowed).Diag = ""
                SelectedProcedures(MaxBillingProceduresAllowed).OrgDiag = ""
                SelectedProcedures(MaxBillingProceduresAllowed).Name = ""
                SelectedProcedures(MaxBillingProceduresAllowed).Ref = 0
                SelectedProcedures(MaxBillingProceduresAllowed).Highlight = False
                SelectedProcedures(MaxBillingProceduresAllowed).LinkedDiag = ""
                SelectedProcedures(MaxBillingProceduresAllowed).WarningOn = False
                For j = 1 To MaxModifierAllowed
                    SelectedProcedures(MaxBillingProceduresAllowed).Modifier(j) = ""
                Next j
                i = i - 1
            End If
        End If
    Next i
' re-write the Link Diags
    For i = 1 To k
        For j = 1 To Len(SelectedProcedures(i).LinkedDiag)
            If (Mid(SelectedProcedures(i).LinkedDiag, j, 1) > Trim(str(iLoc))) And (Trim(Mid(SelectedProcedures(i).LinkedDiag, j, 1)) <> "") Then
                Mid(SelectedProcedures(i).LinkedDiag, j, 1) = Trim(str(Val(Mid(SelectedProcedures(i).LinkedDiag, j, 1)) - 1))
            End If
        Next j
    Next i
    Call RetainBilledProcedures(PatientId)
End If
End Function

Public Function SetDiagnosisHighlight(Diag As String, IHow As Boolean) As Boolean
Dim i As Integer
SetDiagnosisHighlight = True
For i = 1 To TotalBilledDiagnosis
    If (Diag = Trim(BilledDiagnosis(i).Id)) Then
        BilledDiagnosis(i).Highlight = IHow
    End If
Next i
End Function

Public Function SetProcedureHighlight(Diag As String, IHow As Boolean) As Boolean
Dim i As Integer
SetProcedureHighlight = True
For i = 1 To TotalBillingProcedure
    If (Diag = Trim(SelectedProcedures(i).Id)) Then
        SelectedProcedures(i).Highlight = IHow
    End If
Next i
End Function

Private Function SetDiagnosisProcedureMatch(ADiag As String, MyLink As Integer) As Boolean
Dim q As Integer, i As Integer
Dim r As Integer, TotalProcs As Integer
Dim NewId As String
Dim RetrieveProcedure As DiagnosisMasterProcedures
SetDiagnosisProcedureMatch = False
If (Trim(ADiag) <> "") Then
    NewId = ADiag
    r = InStrPS(NewId, ".")
    If (r > 0) Then
        q = InStrPS(r + 1, NewId, ".")
        If (q > 0) Then
            NewId = Left(NewId, q - 1)
        End If
    End If
    Set RetrieveProcedure = New DiagnosisMasterProcedures
    RetrieveProcedure.ProcedureCPT = ""
    RetrieveProcedure.ProcedureDiagnosis = NewId
    RetrieveProcedure.ProcedureRank = 0
    RetrieveProcedure.ProcedureLinkType = ""
    RetrieveProcedure.ProcedureInsClass1 = frmSystems1.InsurerClassId
    RetrieveProcedure.ProcedureInsClass2 = frmSystems1.InsurerClassId
    RetrieveProcedure.ProcedureInsClass3 = frmSystems1.InsurerClassId
    RetrieveProcedure.ProcedureInsClass4 = frmSystems1.InsurerClassId
    RetrieveProcedure.ProcedureInsClass5 = frmSystems1.InsurerClassId
    RetrieveProcedure.ProcedureInsClass6 = frmSystems1.InsurerClassId
    If (RetrieveProcedure.FindProcedurebyInsClass > 0) Then
        i = 1
        While (RetrieveProcedure.SelectProcedure(i))
            TotalProcs = TotalBillingProcedure
            For q = 1 To TotalProcs
                If (Trim(SelectedProcedures(q).Id) = Trim(RetrieveProcedure.ProcedureCPT)) Then
                    If (Trim(SelectedProcedures(q).Diag) = "?") Then
                        SelectedProcedures(q).Diag = ADiag
                        If (MyLink > 0) And (MyLink < 5) Then
                            SelectedProcedures(q).LinkedDiag = Trim(SelectedProcedures(q).LinkedDiag) + Trim(str(MyLink))
                        End If
                        SetDiagnosisProcedureMatch = True
                    End If
                End If
            Next q
            i = i + 1
        Wend
    End If
    Set RetrieveProcedure = Nothing
End If
End Function

Private Function SetPostOpTest(CPT As String, AMod As String) As Boolean
Dim ModToUse As String
Dim DefaultOn As Boolean
Dim RetMod As PracticeModifierRules
SetPostOpTest = True
AMod = ""
DefaultOn = True
Set RetMod = New PracticeModifierRules
RetMod.ModifierClassId = frmSystems1.BusinessClassId
RetMod.ModifierCPT = CPT
If (RetMod.FindModifierRule > 0) Then
    If (RetMod.SelectModifierRule(1)) Then
        ModToUse = Trim(RetMod.ModifierRule5)
        DefaultOn = False
    End If
End If
Set RetMod = Nothing
If (DefaultOn) Then
    ModToUse = frmSystems1.GetDefaultClassModifier
End If
AMod = ModToUse
End Function

Private Function VerifyPostOpTest(AText As String) As Integer
VerifyPostOpTest = 2    ' Default to dont apply modifiers
If (frmSystems1.IsPostOpOn) Then
    frmEventMsgs.Header = "Is this service, " + AText + ", related to the surgery ?"
    frmEventMsgs.AcceptText = "Unrelated Bill Service"
    frmEventMsgs.RejectText = "Related Do Not Bill"
    frmEventMsgs.CancelText = "Related Bill Service"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 4) Then
        VerifyPostOpTest = 2    ' Do Not Apply the modifiers
    ElseIf (frmEventMsgs.Result = 2) Then
        VerifyPostOpTest = 0    ' Default is deselected
    ElseIf (frmEventMsgs.Result = 1) Then
        VerifyPostOpTest = 1    ' Apply the Modifiers
    End If
End If
End Function

Private Sub cmdSumm_Click()
frmFinalNew.PatientId = PatientId
frmFinalNew.AppointmentId = AppointmentId
frmFinalNew.ActivityId = globalExam.iActivityId
If (frmFinalNew.LoadFinal(True)) Then
    frmFinalNew.Show 1
End If
End Sub

Private Function SetWarning() As Boolean
Dim i As Integer
SetWarning = False
lblMods.Caption = ""
For i = 1 To MaxBillingProceduresAllowed
    If (SelectedProcedures(i).WarningOn) Then
        lblMods.Caption = Trim(lblMods.Caption) + " " + Trim(SelectedProcedures(i).Name) + " requires modifiers to be set." + vbCrLf
    End If
Next i
If (lblMods.Caption <> "") Then
    SetWarning = True
End If
End Function

Private Function IsDiagSelected(sDiag As String) As Boolean
Dim iIndex As Long
    For iIndex = 1 To MaxBillingDiagnosisAllowed
        If Left$(BilledDiagnosis(iIndex).Id, 6) = sDiag Then
            IsDiagSelected = True
        End If
    Next
End Function
Private Function IsDiagExists(sDiag As String) As Boolean
Dim iIndex As Long
    For iIndex = 1 To CurrentDiagSelectIndex
        If Left$(BilledDiagnosis(iIndex).Id, 6) = Left$(sDiag, 6) Then
            IsDiagExists = True
            Exit For
        End If
    Next
End Function
Private Sub cmdSurveillance_Click()
On Error GoTo lcmdSurveillance_Click

Dim objSurveillance As New Surveillance.frmSurveillance
Dim Helper As New Surveillance.DbHelper
Helper.DbObject = IdbConnection
objSurveillance.PatientId = PatientId
objSurveillance.EncounterId = AppointmentId
objSurveillance.DmdbConnectionString = dbConnectionAccess & dbDiagnosisMaster & ";"
objSurveillance.ShowDialog

Exit Sub
lcmdSurveillance_Click:
    LogError "frmSetProcesures", "cmdSurveillance_Click", Err, Err.Description
End Sub
Public Function FrmClose()
Call cmdHome_Click
Unload Me
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Set SelectedImpr = Nothing
End Sub

Public Sub LoadBilledDiagnosesFromFile(ByVal SelectedDiagnosis As String)
On Error GoTo lLoadBilledDiagnosesFromFile_Error
Dim FileNum As Integer
Dim i As Long
Dim k As Long
Dim TheFile As String
Dim sRec As String
Dim sDiag As String
Dim sDiagEye As String
    
CurrentDiagIndex = 1
Call InitializeBillingDiagnosis

Dim strsplit() As String
strsplit() = Split(SelectedDiagnosis, ";")

For i = 0 To UBound(strsplit) - 1
sRec = strsplit(i)
k = InStrPS(sRec, "-")
If (k > 1) Then
    sDiag = Left$(sRec, k - 1)
    sDiagEye = Mid$(sRec, k + 1, 2)
    If (CurrentDiagIndex <= MaxBillingDiagnosisAllowed) Then
        BilledDiagnosis(CurrentDiagIndex).Name = sDiagEye
        BilledDiagnosis(CurrentDiagIndex).Eye = ""
        BilledDiagnosis(CurrentDiagIndex).Id = sDiag
        CurrentDiagIndex = CurrentDiagIndex + 1
    End If
End If
Next

Exit Sub
lLoadBilledDiagnosesFromFile_Error:
End Sub

Private Function GetIcdMode() As Boolean
    ' Billing Diagnosis
On Error GoTo lGetIcdMode_Error
GetIcdMode = False
Dim FileNum As Integer
Dim k As Integer
Dim TheFile As String
Dim Rec As String

TheFile = DoctorInterfaceDirectory + "DiagBill_Icd10" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    While Not (EOF(FileNum))
        Line Input #FileNum, Rec
        k = InStrPS(Rec, "-")
        If (k > 1) Then
            GetIcdMode = True
            Exit Function
        End If
    Wend
    FM.CloseFile CLng(FileNum)
End If

Exit Function

lGetIcdMode_Error:
    GetIcdMode = False
End Function

