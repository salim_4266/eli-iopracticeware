VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmEvaluation 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9045
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12150
   FontTransparent =   0   'False
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "Evaluation.frx":0000
   ScaleHeight     =   9045
   ScaleWidth      =   12150
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdAction11 
      Height          =   1095
      Left            =   6000
      TabIndex        =   21
      Top             =   600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction16 
      Height          =   1095
      Left            =   8760
      TabIndex        =   20
      Top             =   600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":38A7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction12 
      Height          =   1095
      Left            =   6000
      TabIndex        =   22
      Top             =   1800
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":3A85
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction13 
      Height          =   1095
      Left            =   6000
      TabIndex        =   23
      Top             =   3000
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":3C63
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction14 
      Height          =   1095
      Left            =   6000
      TabIndex        =   24
      Top             =   4200
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":3E41
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction15 
      Height          =   1095
      Left            =   6000
      TabIndex        =   25
      Top             =   5400
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":401F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction17 
      Height          =   1095
      Left            =   8760
      TabIndex        =   26
      Top             =   1800
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":41FD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction18 
      Height          =   1095
      Left            =   8760
      TabIndex        =   27
      Top             =   3000
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":43DB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction19 
      Height          =   1095
      Left            =   8760
      TabIndex        =   28
      Top             =   4200
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":45B9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction20 
      Height          =   1095
      Left            =   8760
      TabIndex        =   29
      Top             =   5400
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":4797
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction6 
      Height          =   1095
      Left            =   3120
      TabIndex        =   11
      Top             =   600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":4976
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction1 
      Height          =   1095
      Left            =   360
      TabIndex        =   1
      Top             =   600
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":4B54
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction2 
      Height          =   1095
      Left            =   360
      TabIndex        =   2
      Top             =   1800
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":4D32
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction3 
      Height          =   1095
      Left            =   360
      TabIndex        =   3
      Top             =   3000
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":4F10
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction4 
      Height          =   1095
      Left            =   360
      TabIndex        =   4
      Top             =   4200
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":50EE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction5 
      Height          =   1095
      Left            =   360
      TabIndex        =   5
      Top             =   5400
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":52CC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreActions 
      Height          =   1095
      Left            =   360
      TabIndex        =   6
      Top             =   6600
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":54AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReviewSystems 
      Height          =   975
      Left            =   9360
      TabIndex        =   7
      Top             =   7920
      Width           =   1245
      _Version        =   131072
      _ExtentX        =   2196
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":568D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   8
      Top             =   7920
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":58C7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   975
      Left            =   1200
      TabIndex        =   9
      Top             =   7920
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":5AFA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10680
      TabIndex        =   10
      Top             =   7920
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":5D2E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction7 
      Height          =   1095
      Left            =   3120
      TabIndex        =   12
      Top             =   1800
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":5F61
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction8 
      Height          =   1095
      Left            =   3120
      TabIndex        =   13
      Top             =   3000
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":613F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction9 
      Height          =   1095
      Left            =   3120
      TabIndex        =   14
      Top             =   4200
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":631D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAction10 
      Height          =   1095
      Left            =   3120
      TabIndex        =   15
      Top             =   5400
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":64FB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAssign 
      Height          =   975
      Left            =   2280
      TabIndex        =   16
      Top             =   7920
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":66DA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCheckOut 
      Height          =   975
      Left            =   7680
      TabIndex        =   19
      Top             =   7920
      Visible         =   0   'False
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":690E
   End
   Begin VB.ListBox lstReview 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5100
      ItemData        =   "Evaluation.frx":6B4E
      Left            =   120
      List            =   "Evaluation.frx":6B50
      TabIndex        =   17
      Top             =   600
      Visible         =   0   'False
      Width           =   11775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHigh 
      Height          =   975
      Left            =   6120
      TabIndex        =   33
      Top             =   7920
      Width           =   1485
      _Version        =   131072
      _ExtentX        =   2619
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":6B52
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSumm 
      Height          =   975
      Left            =   4680
      TabIndex        =   34
      Top             =   7920
      Width           =   1365
      _Version        =   131072
      _ExtentX        =   2408
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Evaluation.frx":6D8A
   End
   Begin VB.Label lblBirthdate 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Birthdate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   6000
      TabIndex        =   36
      Top             =   120
      Width           =   990
   End
   Begin VB.Label lblPatientAge 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Age"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   5160
      TabIndex        =   35
      Top             =   120
      Width           =   405
   End
   Begin VB.Label lblAlert 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      Caption         =   "Alert ON"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   8880
      TabIndex        =   32
      Top             =   120
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label lblType 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   8040
      TabIndex        =   31
      Top             =   120
      Width           =   525
   End
   Begin VB.Label lblName 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1680
      TabIndex        =   30
      Top             =   120
      Width           =   600
   End
   Begin VB.Label lblPrint 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Printing In Progress"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   8880
      TabIndex        =   18
      Top             =   360
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "PLAN"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   360
      TabIndex        =   0
      Top             =   120
      Width           =   750
   End
End
Attribute VB_Name = "frmEvaluation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public SetCloseChart As Boolean
Public SetInFlow As Boolean
Public IsIcd10 As Boolean
Private PatientId As Long
Private AppointmentId As Long
Public NavigationAction As Long

Private Const RXReference As String = "Spectacle"
Private Const MaxActions As Integer = 100
Private Type ExternalReference
    Name As String * 64
    Trigger(3) As String * 1
End Type
Private StandardAction(100) As ExternalReference

Private Const CntRefSet As String = "!"
Private Const RORefSet As String = "#"
Private Const HORefSet As String = "%"
Private Const MaxBillingDiagnosisAllowed As Integer = 12
Private Const MaxBillingDiagnosis As Integer = 100

Private Type ExternalReferenceDiag
    Id As String
    Name As String
    Eye As String
    SeverityA As String
    SeverityB As String
    Active As Boolean
    IgnoreEye As Boolean
    NegativeFinding As Boolean
    RuleOutOn As Boolean
    HistoryOfOn As Boolean
    Highlight As Boolean
End Type
Private SelectedDiagnosis(MaxBillingDiagnosis) As ExternalReferenceDiag
Private BilledDiagnosis(MaxBillingDiagnosisAllowed * 2) As ExternalReferenceDiag

Private Type TheDetails
    Name As String * 64
    Trigger(3) As String * 1
    TriggerResult(3) As String
    Highlight As Boolean
    FollowUp As Boolean
    SpecCLRx As String
    Comments(1) As String
End Type
Private Type OfficeVisitArguments
    EMProc As String
    OPProc As String
    EMLevel As String
    OPLevel As String
End Type
Private OfficeVisitLoadArguments(1) As OfficeVisitArguments
Private SelectedActions(MaxActions) As TheDetails
Private MaxOtherActions As Integer
Private CurrentActionIndex As Integer

Private ViewOn As Boolean
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long
Private Const MaxImpressions As Integer = 100
Private SelectedImpressionsOD(MaxImpressions) As New CImpression
Private SelectedImpressionsOS(MaxImpressions) As New CImpression
' These are the total impressions
Public TotalImpressionsOD As Integer
Public TotalImpressionsOS As Integer

Public Function IsThereERx() As Boolean
Dim i As Integer, j As Integer
For i = 1 To MaxActions
    If UCase(Trim(SelectedActions(i).Name)) = "RX" Then
        For j = 1 To 3
            If Not SelectedActions(i).TriggerResult(j) = "" Then
                If InStrPS(1, SelectedActions(i).TriggerResult(j), "9-9/") > 0 Then
                    IsThereERx = True
                    Exit Function
                End If
            End If
        Next
    End If
Next
End Function



Private Sub cmdCheckOut_Click()
frmEventMsgs.Header = "Express Check-Out WILL CAUSE WORKFLOW PROBLEMS. Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Check-Out"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    NavigationAction = 90
    Unload frmEvaluation
End If
End Sub

Private Function IsDiagThere(TheId As String, AEye As String) As Boolean
Dim i As Integer
IsDiagThere = False
For i = 1 To MaxBillingDiagnosis
    If (Trim(TheId) <> "") Then
        If (Trim(SelectedDiagnosis(i).Id) = Trim(TheId)) Then
            If (SelectedDiagnosis(i).Eye = AEye) Or (Trim(AEye) = "") Or (Trim(SelectedDiagnosis(i).Eye) = "") Then
                IsDiagThere = True
                Exit For
            End If
        End If
    End If
Next i
End Function

Private Function GetPrimaryDiagnosis(Id As String, ASys As String, TheName As String, BillOn As Boolean, AId As Long) As Boolean
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
GetPrimaryDiagnosis = False
TheName = ""
If (Id <> "") And (Len(Id) > 1) Then
    Set RetrieveDiagnosis = New DiagnosisMasterPrimary
    RetrieveDiagnosis.PrimarySystem = ASys
    RetrieveDiagnosis.PrimaryDiagnosis = ""
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = Id
    RetrieveDiagnosis.PrimaryLevel = 0
    RetrieveDiagnosis.PrimaryRank = 0
    RetrieveDiagnosis.PrimaryBilling = BillOn
    RetrieveDiagnosis.PrimaryDiscipline = ""
    If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
        If (RetrieveDiagnosis.SelectPrimary(1)) Then
            TheName = Trim(RetrieveDiagnosis.PrimaryShortName)
            If (TheName = "") Then
                TheName = Trim(RetrieveDiagnosis.PrimaryLingo)
                If (Trim(TheName) = "") Then
                    TheName = Trim(RetrieveDiagnosis.PrimaryName)
                End If
            End If
            AId = RetrieveDiagnosis.PrimaryId
            GetPrimaryDiagnosis = True
        End If
    Else
        RetrieveDiagnosis.PrimarySystem = ""
        If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
            If (RetrieveDiagnosis.SelectPrimary(1)) Then
                TheName = Trim(RetrieveDiagnosis.PrimaryShortName)
                If (TheName = "") Then
                    TheName = Trim(RetrieveDiagnosis.PrimaryLingo)
                    If (Trim(TheName) = "") Then
                        TheName = Trim(RetrieveDiagnosis.PrimaryName)
                    End If
                End If
                AId = RetrieveDiagnosis.PrimaryId
                GetPrimaryDiagnosis = True
            End If
        End If
    End If
    Set RetrieveDiagnosis = Nothing
End If
End Function

Public Function TotalBilledDiagnosis() As Integer
Dim i As Integer
TotalBilledDiagnosis = 0
For i = 1 To MaxBillingDiagnosisAllowed
    If (Trim(BilledDiagnosis(i).Id) = "") Then
        TotalBilledDiagnosis = i - 1
        Exit For
    Else
        TotalBilledDiagnosis = i
    End If
Next i
End Function

Private Sub SetDiagBoth(TheId As String)
Dim i As Integer
For i = 1 To TotalBilledDiagnosis
    If (Trim(TheId) <> "") Then
        If (Trim(BilledDiagnosis(i).Id) = Trim(TheId)) Then
            BilledDiagnosis(i).Eye = "OU"
            Exit For
        End If
    End If
Next i
For i = 1 To MaxBillingDiagnosis
    If (Trim(TheId) <> "") Then
        If (Trim(SelectedDiagnosis(i).Id) = Trim(TheId)) Then
            SelectedDiagnosis(i).Eye = "OU"
            Exit For
        End If
    End If
Next i
End Sub

Public Function InitializeBillingDiagnosis() As Boolean
Dim i As Integer
For i = 1 To MaxBillingDiagnosis
    SelectedDiagnosis(i).Id = ""
    SelectedDiagnosis(i).Name = ""
    SelectedDiagnosis(i).Eye = ""
    SelectedDiagnosis(i).SeverityA = ""
    SelectedDiagnosis(i).SeverityB = ""
    SelectedDiagnosis(i).NegativeFinding = False
    SelectedDiagnosis(i).Active = False
    SelectedDiagnosis(i).IgnoreEye = False
    SelectedDiagnosis(i).RuleOutOn = False
    SelectedDiagnosis(i).Highlight = False
    SelectedDiagnosis(i).HistoryOfOn = False
    If (i <= MaxBillingDiagnosisAllowed) Then
        BilledDiagnosis(i).Id = ""
        BilledDiagnosis(i).Name = ""
        BilledDiagnosis(i).Eye = ""
        BilledDiagnosis(i).SeverityA = ""
        BilledDiagnosis(i).SeverityB = ""
        BilledDiagnosis(i).Active = False
        BilledDiagnosis(i).IgnoreEye = False
        BilledDiagnosis(i).NegativeFinding = False
        BilledDiagnosis(i).RuleOutOn = False
        BilledDiagnosis(i).Highlight = False
        BilledDiagnosis(i).HistoryOfOn = False
    End If
Next i
InitializeBillingDiagnosis = True
End Function

Private Function GetAvailableDiagnosis() As String
Dim LoadIt As Boolean
Dim TheId As Long
Dim w As Integer
Dim i As Integer, j As Integer
Dim OtherText As String
Dim PTemp As String, TheText As String
Dim RQuan As String
Dim RSys As String, RText As String
Dim LQuan As String
Dim LSys As String, LText As String
Dim RightDiagnosis As String, LeftDiagnosis As String
Call InitializeBillingDiagnosis
i = 1
j = 1
While (frmSystems1.GetSystemDiagnosis(i, RSys, RightDiagnosis, RText, RQuan, LSys, LeftDiagnosis, LText, LQuan))
    LoadIt = True
    OtherText = ""
    If (Trim(RightDiagnosis) <> "") And (Left(RightDiagnosis, 2) <> "N-") Then
        If (Left(RightDiagnosis, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Mid(RightDiagnosis, 2, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Left(RightDiagnosis, 1) + Mid(RightDiagnosis, 3, Len(RightDiagnosis) - 2)
        End If
        If (Mid(RightDiagnosis, 3, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Left(RightDiagnosis, 2) + Mid(RightDiagnosis, 4, Len(RightDiagnosis) - 3)
        End If
        If (Left(RightDiagnosis, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Mid(RightDiagnosis, 2, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Left(RightDiagnosis, 1) + Mid(RightDiagnosis, 3, Len(RightDiagnosis) - 2)
        End If
        If (Mid(RightDiagnosis, 3, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Left(RightDiagnosis, 2) + Mid(RightDiagnosis, 4, Len(RightDiagnosis) - 3)
        End If
        If (Left(RightDiagnosis, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Mid(RightDiagnosis, 2, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Left(RightDiagnosis, 1) + Mid(RightDiagnosis, 3, Len(RightDiagnosis) - 2)
        End If
        If (Mid(RightDiagnosis, 3, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Left(RightDiagnosis, 2) + Mid(RightDiagnosis, 4, Len(RightDiagnosis) - 3)
        End If
        If (Left(RightDiagnosis, 1) <> "Y") And _
           (Left(RightDiagnosis, 1) <> "M") And _
           (Left(RightDiagnosis, 1) <> "P") And _
           (Left(RightDiagnosis, 1) <> "X") Then
            LoadIt = True
            If (InStrPS(OtherText, "NF-") <> 0) Then
                LoadIt = False
            End If
            If (InStrPS(OtherText, "RO-") <> 0) Then
                LoadIt = False
            End If
            If (LoadIt) Then
                w = InStrPS(RightDiagnosis, "@")
                If (w > 0) Then
                    PTemp = Left(RightDiagnosis, w - 1)
                Else
                    PTemp = RightDiagnosis
                End If
                If Not (IsDiagThere(PTemp, "OD")) Then
                    If (GetPrimaryDiagnosis(PTemp, RSys, TheText, True, TheId)) Then
                        If (IsDiagThere(PTemp, "OS")) Then
                            Call SetDiagBoth(PTemp)
                        Else
                            If Not (IsDiagThere(PTemp, "OU")) Then
                                SelectedDiagnosis(j).Id = PTemp
                                SelectedDiagnosis(j).Name = TheText
                                SelectedDiagnosis(j).Eye = "OD"
                                SelectedDiagnosis(j).SeverityA = ""
                                SelectedDiagnosis(j).SeverityB = ""
                                SelectedDiagnosis(j).Active = False
                                SelectedDiagnosis(j).IgnoreEye = False
                                SelectedDiagnosis(j).NegativeFinding = False
                                SelectedDiagnosis(j).RuleOutOn = False
                                SelectedDiagnosis(j).HistoryOfOn = False
                                SelectedDiagnosis(j).Highlight = False
                                j = j + 1
                            End If
                        End If
                    End If
                Else
                    If (IsDiagThere(PTemp, "OS")) Then
                        Call SetDiagBoth(PTemp)
                    End If
                End If
            End If
        End If
    End If
    If (Trim(LeftDiagnosis) <> "") And (Left(LeftDiagnosis, 2) <> "N-") Then
        OtherText = ""
        If (Left(LeftDiagnosis, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Mid(LeftDiagnosis, 2, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Left(LeftDiagnosis, 1) + Mid(LeftDiagnosis, 3, Len(LeftDiagnosis) - 2)
        End If
        If (Mid(LeftDiagnosis, 3, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Left(LeftDiagnosis, 2) + Mid(LeftDiagnosis, 4, Len(LeftDiagnosis) - 3)
        End If
        If (Left(LeftDiagnosis, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Mid(LeftDiagnosis, 2, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Left(LeftDiagnosis, 1) + Mid(LeftDiagnosis, 3, Len(LeftDiagnosis) - 2)
        End If
        If (Mid(LeftDiagnosis, 3, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Left(LeftDiagnosis, 2) + Mid(LeftDiagnosis, 4, Len(LeftDiagnosis) - 3)
        End If
        If (Left(LeftDiagnosis, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Mid(LeftDiagnosis, 2, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Left(LeftDiagnosis, 1) + Mid(LeftDiagnosis, 3, Len(LeftDiagnosis) - 2)
        End If
        If (Mid(LeftDiagnosis, 3, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Left(LeftDiagnosis, 2) + Mid(LeftDiagnosis, 4, Len(LeftDiagnosis) - 3)
        End If
        If (Left(LeftDiagnosis, 1) <> "Y") And _
           (Left(LeftDiagnosis, 1) <> "M") And _
           (Left(LeftDiagnosis, 1) <> "P") And _
           (Left(LeftDiagnosis, 1) <> "X") Then
            LoadIt = True
            If (InStrPS(OtherText, "NF-") <> 0) Then
                LoadIt = False
            End If
            If (InStrPS(OtherText, "RO-") <> 0) Then
                LoadIt = False
            End If
            If (LoadIt) Then
                w = InStrPS(LeftDiagnosis, "@")
                If (w > 0) Then
                    PTemp = Left(LeftDiagnosis, w - 1)
                Else
                    PTemp = LeftDiagnosis
                End If
                If Not (IsDiagThere(PTemp, "OS")) Then
                    If (IsDiagThere(PTemp, "OD")) Then
                        Call SetDiagBoth(PTemp)
                    Else
                        If Not (IsDiagThere(PTemp, "OU")) Then
                            If (GetPrimaryDiagnosis(PTemp, LSys, TheText, True, TheId)) Then
                                SelectedDiagnosis(j).Id = PTemp
                                SelectedDiagnosis(j).Name = TheText
                                SelectedDiagnosis(j).Eye = "OS"
                                SelectedDiagnosis(j).SeverityA = ""
                                SelectedDiagnosis(j).SeverityB = ""
                                SelectedDiagnosis(j).Active = False
                                SelectedDiagnosis(j).IgnoreEye = False
                                SelectedDiagnosis(j).NegativeFinding = False
                                SelectedDiagnosis(j).RuleOutOn = False
                                SelectedDiagnosis(j).HistoryOfOn = False
                                SelectedDiagnosis(j).Highlight = False
                                j = j + 1
                            End If
                        End If
                    End If
                Else
                    If (IsDiagThere(PTemp, "OD")) Then
                        Call SetDiagBoth(PTemp)
                    End If
                End If
            End If
        End If
    End If
    i = i + 1
Wend
End Function
Public Sub cmdDone_Click()
On Error GoTo lcmdDone_Error

NavigationAction = 1

Call PostToFile
If cmdDone.Text = "Close Chart" Then
    Call InsertLog("FrmEvaluation.frm", "Saved Patient  " & CStr(PatientId) & " Encounter", LoginCatg.EncounterSave, "", "", 0, globalExam.iAppointmentId)
    Unload frmEvaluation
    Exit Sub
End If

If (SetInFlow) Then
    Dim StandardExamComWrapper As New comWrapper
    Dim loadArguments As Object
    Dim OfficeVisitLoadArgument As Object
    Dim DiagnosisLoadArgument As Variant
    Dim arguments() As Variant
    Dim i As Integer
    
    StandardExamComWrapper.RethrowExceptions = False
    Call StandardExamComWrapper.Create(StandardExamLoadArgumentsType, emptyArgs)
    Set loadArguments = StandardExamComWrapper.Instance
    loadArguments.PatientId = PatientId
    loadArguments.EncounterId = AppointmentId
    
    'Building OfficeVisit LoadArguments Logic
    Call StandardExamComWrapper.Create(StandardExamOfficeVisitLoadArgumentsType, emptyArgs)
    Set OfficeVisitLoadArgument = StandardExamComWrapper.Instance
    Call BuildOfficeVisitsArguments
    
    'Assign back office visit info to LoadArguments
    If Not OfficeVisitLoadArgument Is Nothing Then
        OfficeVisitLoadArgument.EmProcCode = OfficeVisitLoadArguments(0).EMProc
        OfficeVisitLoadArgument.EMLevel = OfficeVisitLoadArguments(0).EMLevel
        OfficeVisitLoadArgument.EyeProcCode = OfficeVisitLoadArguments(0).OPProc
        OfficeVisitLoadArgument.EyeLevel = OfficeVisitLoadArguments(0).OPLevel
        loadArguments.OfficeVisitLoadArguments = OfficeVisitLoadArgument
    End If
    
    'Diagnosis
    Call GetAvailableDiagnosis
    Dim wrapper As New comWrapper
    Call wrapper.Create("System.Collections.Generic.List`1[[IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments]]", emptyArgs)
    
    Dim j As Integer
    Dim RQuantExists As Boolean
    Dim LQuantExists As Boolean
    Dim RQuant As String
    Dim LQuant As String
    Call frmSetDiagnosis.GetSelectedImpressions(SelectedImpressionsOS, SelectedImpressionsOD, TotalImpressionsOS, TotalImpressionsOD)
    
    While (i <= MaxBillingDiagnosis)
        If (Trim$(SelectedDiagnosis(i).Id) <> "") Then
                Dim AddOSItemArgs() As Variant
                Dim AddODItemArgs() As Variant
                Dim AddItemArgs() As Variant
                RQuant = ""
                LQuant = ""
                If (SelectedDiagnosis(i).Eye = "") Then SelectedDiagnosis(i).Eye = "OU"
                For j = 0 To TotalImpressionsOS - 1
                    If (SelectedImpressionsOS(j).Id = SelectedDiagnosis(i).Id) Then
                        RQuant = CheckQuantifierExists(SelectedImpressionsOS(j).Quant)
                        Exit For
                    End If
                Next
                For j = 0 To TotalImpressionsOD - 1
                    If (SelectedImpressionsOD(j).Id = SelectedDiagnosis(i).Id) Then
                        LQuant = CheckQuantifierExists(SelectedImpressionsOD(j).Quant)
                        Exit For
                    End If
                Next
                If LQuant <> "" Then
                    AddODItemArgs = PrepareDiagnosesLoadArguments(SelectedDiagnosis(i).Id, SelectedDiagnosis(i).Name, "OD", LQuant)
                    Call wrapper.InvokeMethod("Add", AddODItemArgs)
                End If
                If RQuant <> "" Then
                    AddOSItemArgs = PrepareDiagnosesLoadArguments(SelectedDiagnosis(i).Id, SelectedDiagnosis(i).Name, "OS", RQuant)
                    Call wrapper.InvokeMethod("Add", AddOSItemArgs)
                End If
                If (SelectedDiagnosis(i).Eye = "OU") Then
                    If (RQuant <> "" And LQuant = "") Then
                        AddItemArgs = PrepareDiagnosesLoadArguments(SelectedDiagnosis(i).Id, SelectedDiagnosis(i).Name, "OD", "")
                        Call wrapper.InvokeMethod("Add", AddItemArgs)
                    ElseIf (RQuant = "" And LQuant <> "") Then
                        AddItemArgs = PrepareDiagnosesLoadArguments(SelectedDiagnosis(i).Id, SelectedDiagnosis(i).Name, "OS", "")
                        Call wrapper.InvokeMethod("Add", AddItemArgs)
                    End If
                End If
                If (RQuant = "" And LQuant = "") Then
                    AddItemArgs = PrepareDiagnosesLoadArguments(SelectedDiagnosis(i).Id, SelectedDiagnosis(i).Name, SelectedDiagnosis(i).Eye, "")
                    Call wrapper.InvokeMethod("Add", AddItemArgs)
                End If
        End If
        i = i + 1
    Wend

    loadArguments.DiagnosesLoadArguments = wrapper.Instance
    arguments = Array(loadArguments)

    Call StandardExamComWrapper.Create(StandardExamViewManagerType, emptyArgs)
    Call StandardExamComWrapper.InvokeMethod("ShowStandardExam", arguments)
    
    Dim returnVal As Object
    Set returnVal = StandardExamComWrapper.InvokeMethod("GetCheckOutData", emptyArgs)
    IsIcd10 = returnVal.IsIcd10
       
    If returnVal.FormName = "Impressions" Then
        If Not frmSetDiagnosis.Visible Then
          If (frmSetDiagnosis.LoadImpressions(True, True, False, False, False)) Then
              frmSetDiagnosis.Show 1
             Call frmSetDiagnosis.RetainImpressions(PatientId)
          End If
        End If
   ElseIf returnVal.FormName = "Chart" Then
        NavigationAction = 99
        Unload frmEvaluation
    End If
    If returnVal.FormName = "Summary" Then
        'Write the ICD10 content to current VB6 Array
        FM.Clear
        Call frmSetEMProcedure.ClearEMProcedure
        Call frmSetEMProcedure.LoadFromFile(True)
        Unload frmEvaluation
    End If
    Else
         Unload frmEvaluation
    End If
    Exit Sub
lcmdDone_Error:
    LogError "frmEvaluation", "CmdDone_Click", Err, Err.Description
End Sub
Private Sub BuildOfficeVisitsArguments()
On Error GoTo lBuildOfficeVisitsArguments_Error
Dim ETemp As String, OTemp As String, EMBasis As String
Dim ALevel As Integer, OLevel As Integer
Dim Temp As String, RemoveOn As Boolean
Dim RS As Recordset, InsurerId As Long
Dim ProcedureEM As String

OfficeVisitLoadArguments(0).EMProc = ""
OfficeVisitLoadArguments(0).EMLevel = ""
OfficeVisitLoadArguments(0).OPProc = ""
OfficeVisitLoadArguments(0).OPLevel = ""

'We have Stored Procedure which gets EmBasis Info from dbo.AppointmetType.So,Assign Directly
EMBasis = frmSystems1.EMBasis

If EMBasis = "" Then Exit Sub

'if it's PostOp Proc no need of checking Eye Proc.
If (EMBasis = "99024") Then
    ProcedureEM = "99024"
    OTemp = ""
Else
    Call frmInquiry.GetAnalyticalLevel(ALevel, OLevel)
    If Not (frmSystems1.IsPostOpOn) Then
        ETemp = Trim(str(Val(EMBasis) + ALevel))
    Else
        ETemp = "99024"
    End If
    If (OLevel = 0) Then
        OLevel = 1
    End If
    OTemp = "9200" + Trim(str(OLevel))
    If (Mid(ETemp, 3, 2) = "24") Then
        OTemp = "9204" + Trim(str(OLevel))
    ElseIf (Mid(ETemp, 3, 2) = "21") Then
        OTemp = "9201" + Trim(str(OLevel))
    End If
    
    ProcedureEM = ETemp
    
    'For now implementing Pricing Part from vb6.As we dont have model Table Structure
    'Get InsurerId
    Dim RetPatientFinancialService As PatientFinancialService
    Set RetPatientFinancialService = New PatientFinancialService
    RetPatientFinancialService.InsuredPatientId = PatientId
    Set RS = RetPatientFinancialService.GetPatientPrimaryInsurer
    If (Not RS Is Nothing And RS.RecordCount > 0) Then
        InsurerId = RS("InsurerId")
    End If
    Set RetPatientFinancialService = Nothing
    
    If (frmSetEMProcedure.IsOCodeValid(InsurerId, ETemp, OTemp, Temp, RemoveOn)) Then
        If (Trim(Temp) <> "") Then
            ProcedureEM = Temp
        End If
    End If
            
    'Build OfficeVisit Info to Pass to .Net
    OfficeVisitLoadArguments(0).EMProc = ProcedureEM
    OfficeVisitLoadArguments(0).EMLevel = ALevel
    OfficeVisitLoadArguments(0).OPProc = OTemp
    OfficeVisitLoadArguments(0).OPLevel = OLevel
End If
Exit Sub
lBuildOfficeVisitsArguments_Error:
    
End Sub
Private Sub cmdHigh_Click()
cmdHigh.Enabled = False
frmHighlights.HighlightLoadOn = frmSystems1.IsHighlightLoadOn
If (frmHighlights.LoadHighlights(PatientId, True, True, False)) Then
     frmHighlights.Show 1
    Call frmSystems1.SetHighlightLoad(frmHighlights.HighlightLoadOn)
End If
cmdHigh.Enabled = True
End Sub

Private Sub cmdNotes_Click()
If Not UserLogin.HasPermission(epPatientNotes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
frmNotes.NoteId = 0
frmNotes.NoteOn = False
frmNotes.EyeContext = ""
frmNotes.SystemReference = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    If (IsAlertPresent(PatientId, EAlertType.eatPlan)) Then
        lblAlert.Caption = "Plan Alert ON"
        lblAlert.Visible = True
    Else
        lblAlert.Caption = "Alert ON"
        lblAlert.Visible = IsAlertPresent(PatientId, 0)
    End If
End If
End Sub

Private Sub cmdReviewSystems_Click()
NavigationAction = 2
Unload frmEvaluation
End Sub

Private Sub cmdAssign_Click()
frmSetDiagnosis.PatientId = PatientId
frmSetDiagnosis.AppointmentId = AppointmentId
If (frmSetDiagnosis.LoadImpressions(True, True, False, False, False)) Then
    frmSetDiagnosis.Show 1
    Call frmSetDiagnosis.RetainImpressions(PatientId)
End If
End Sub

Private Sub cmdHome_Click()
NavigationAction = 99
Unload frmEvaluation
End Sub

Private Sub ClearActions()
cmdAction1.Visible = False
cmdAction1.BackColor = BaseBackColor
cmdAction1.ForeColor = BaseTextColor
cmdAction1.Text = ""
cmdAction1.Tag = ""
cmdAction2.Visible = False
cmdAction2.BackColor = BaseBackColor
cmdAction2.ForeColor = BaseTextColor
cmdAction2.Text = ""
cmdAction2.Tag = ""
cmdAction3.Visible = False
cmdAction3.BackColor = BaseBackColor
cmdAction3.ForeColor = BaseTextColor
cmdAction3.Text = ""
cmdAction3.Tag = ""
cmdAction4.Visible = False
cmdAction4.BackColor = BaseBackColor
cmdAction4.ForeColor = BaseTextColor
cmdAction4.Text = ""
cmdAction4.Tag = ""
cmdAction5.Visible = False
cmdAction5.BackColor = BaseBackColor
cmdAction5.ForeColor = BaseTextColor
cmdAction5.Text = ""
cmdAction5.Tag = ""
cmdAction6.Visible = False
cmdAction6.BackColor = BaseBackColor
cmdAction6.ForeColor = BaseTextColor
cmdAction6.Text = ""
cmdAction6.Tag = ""
cmdAction7.Visible = False
cmdAction7.BackColor = BaseBackColor
cmdAction7.ForeColor = BaseTextColor
cmdAction7.Text = ""
cmdAction7.Tag = ""
cmdAction8.Visible = False
cmdAction8.BackColor = BaseBackColor
cmdAction8.ForeColor = BaseTextColor
cmdAction8.Text = ""
cmdAction8.Tag = ""
cmdAction9.Visible = False
cmdAction9.BackColor = BaseBackColor
cmdAction9.ForeColor = BaseTextColor
cmdAction9.Text = ""
cmdAction9.Tag = ""
cmdAction10.Visible = False
cmdAction10.BackColor = BaseBackColor
cmdAction10.ForeColor = BaseTextColor
cmdAction10.Text = ""
cmdAction10.Tag = ""
cmdAction11.Visible = False
cmdAction11.BackColor = BaseBackColor
cmdAction11.ForeColor = BaseTextColor
cmdAction11.Text = ""
cmdAction11.Tag = ""
cmdAction12.Visible = False
cmdAction12.BackColor = BaseBackColor
cmdAction12.ForeColor = BaseTextColor
cmdAction12.Text = ""
cmdAction12.Tag = ""
cmdAction13.Visible = False
cmdAction13.BackColor = BaseBackColor
cmdAction13.ForeColor = BaseTextColor
cmdAction13.Text = ""
cmdAction13.Tag = ""
cmdAction14.Visible = False
cmdAction14.BackColor = BaseBackColor
cmdAction14.ForeColor = BaseTextColor
cmdAction14.Text = ""
cmdAction14.Tag = ""
cmdAction15.Visible = False
cmdAction15.BackColor = BaseBackColor
cmdAction15.ForeColor = BaseTextColor
cmdAction15.Text = ""
cmdAction15.Tag = ""
cmdAction16.Visible = False
cmdAction16.BackColor = BaseBackColor
cmdAction16.ForeColor = BaseTextColor
cmdAction16.Text = ""
cmdAction16.Tag = ""
cmdAction17.Visible = False
cmdAction17.BackColor = BaseBackColor
cmdAction17.ForeColor = BaseTextColor
cmdAction17.Text = ""
cmdAction17.Tag = ""
cmdAction18.Visible = False
cmdAction18.BackColor = BaseBackColor
cmdAction18.ForeColor = BaseTextColor
cmdAction18.Text = ""
cmdAction18.Tag = ""
cmdAction19.Visible = False
cmdAction19.BackColor = BaseBackColor
cmdAction19.ForeColor = BaseTextColor
cmdAction19.Text = ""
cmdAction19.Tag = ""
cmdAction20.Visible = False
cmdAction20.BackColor = BaseBackColor
cmdAction20.ForeColor = BaseTextColor
cmdAction20.Text = ""
cmdAction20.Tag = ""
End Sub

Private Sub cmdAction1_Click()
Call TriggerActions(cmdAction1)
End Sub

Private Sub cmdAction2_Click()
Call TriggerActions(cmdAction2)
End Sub

Private Sub cmdAction3_Click()
Call TriggerActions(cmdAction3)
End Sub

Private Sub cmdAction4_Click()
Call TriggerActions(cmdAction4)
End Sub

Private Sub cmdAction5_Click()
Call TriggerActions(cmdAction5)
End Sub

Private Sub cmdAction6_Click()
Call TriggerActions(cmdAction6)
End Sub

Private Sub cmdAction7_Click()
Call TriggerActions(cmdAction7)
End Sub

Private Sub cmdAction8_Click()
Call TriggerActions(cmdAction8)
End Sub

Private Sub cmdAction9_Click()
Call TriggerActions(cmdAction9)
End Sub

Private Sub cmdAction10_Click()
Call TriggerActions(cmdAction10)
End Sub

Private Sub cmdAction11_Click()
Call TriggerActions(cmdAction11)
End Sub

Private Sub cmdAction12_Click()
Call TriggerActions(cmdAction12)
End Sub

Private Sub cmdAction13_Click()
Call TriggerActions(cmdAction13)
End Sub

Private Sub cmdAction14_Click()
Call TriggerActions(cmdAction14)
End Sub

Private Sub cmdAction15_Click()
Call TriggerActions(cmdAction15)
End Sub

Private Sub cmdAction16_Click()
Call TriggerActions(cmdAction16)
End Sub

Private Sub cmdAction17_Click()
Call TriggerActions(cmdAction17)
End Sub

Private Sub cmdAction18_Click()
Call TriggerActions(cmdAction18)
End Sub

Private Sub cmdAction19_Click()
Call TriggerActions(cmdAction19)
End Sub

Private Sub cmdAction20_Click()
Call TriggerActions(cmdAction20)
End Sub
Private Function GetButtonTextForPlan()
    frmAlphaPad.NumPad_Field = "Starting with Plan Alpha"
    frmAlphaPad.NumPad_Result = ""
    frmAlphaPad.NumPad_DisplayFull = True
    frmAlphaPad.Show 1
    If Not (frmAlphaPad.NumPad_Quit) Then
        GetButtonTextForPlan = Trim(frmAlphaPad.NumPad_Result)
    Else
        GetButtonTextForPlan = ""
    End If
End Function
Private Sub cmdMoreActions_Click()
If (CurrentActionIndex > MaxOtherActions) Then
    CurrentActionIndex = 1
End If
Call LoadActions(False, ViewOn)
End Sub

Public Function LoadActions(Init As Boolean, AView As Boolean) As Boolean
AppointmentId = globalExam.iAppointmentId
PatientId = globalExam.iPatientId


Dim AText As String, BText As String
Dim CText As String, DText As String
Dim i As Integer, j As Integer, t As Integer
Dim ButtonCnt As Integer, TotalDiags As Integer
LoadActions = False
ViewOn = AView
If (Init) Then
    SetDemoLabels
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = &H9B9626
    BaseTextColor = &HFFFFFF
    CurrentActionIndex = 1
    i = 1
    While (frmHome.GetAction(i, AText, BText, CText, DText))
        j = InStrPS(AText, "!")
        If (j <> 0) Then
            StandardAction(i).Name = Left(AText, j - 1)
        Else
            StandardAction(i).Name = Trim(AText)
            t = InStrPS(StandardAction(i).Name, "-")
            If (t > 0) Then
                StandardAction(i).Name = Trim(Mid(StandardAction(i).Name, t + 1, Len(StandardAction(i).Name) - t))
            End If
        End If
        StandardAction(i).Trigger(1) = BText
        StandardAction(i).Trigger(2) = CText
        StandardAction(i).Trigger(3) = DText
        i = i + 1
    Wend
    MaxOtherActions = i - 1
    For i = 1 To MaxActions
        SelectedActions(i).Name = ""
        For j = 1 To 3
            SelectedActions(i).Trigger(j) = ""
            SelectedActions(i).TriggerResult(j) = ""
            SelectedActions(i).FollowUp = False
        Next j
    Next i
    
    'load
    Call LoadFromRefractFile(True)
    If (IsAlertPresent(PatientId, EAlertType.eatPlan)) Then
        lblAlert.Caption = "Plan Alert ON"
        lblAlert.Visible = True
    Else
        lblAlert.Caption = "Alert ON"
        lblAlert.Visible = IsAlertPresent(PatientId, 0)
    End If
End If
Call ClearActions
TotalDiags = 1
If (TotalDiags <= MaxOtherActions) Then
    i = CurrentActionIndex
    ButtonCnt = 1
    While (i <= MaxOtherActions) And (ButtonCnt < 21)
        If (ButtonCnt = 1) Then
            cmdAction1.Text = StandardAction(i).Name
            cmdAction1.Tag = Trim(str(i))
            cmdAction1.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction1.BackColor = ButtonSelectionColor
                cmdAction1.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 2) Then
            cmdAction2.Text = StandardAction(i).Name
            cmdAction2.Tag = Trim(str(i))
            cmdAction2.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction2.BackColor = ButtonSelectionColor
                cmdAction2.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 3) Then
            cmdAction3.Text = StandardAction(i).Name
            cmdAction3.Tag = Trim(str(i))
            cmdAction3.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction3.BackColor = ButtonSelectionColor
                cmdAction3.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 4) Then
            cmdAction4.Text = StandardAction(i).Name
            cmdAction4.Tag = Trim(str(i))
            cmdAction4.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction4.BackColor = ButtonSelectionColor
                cmdAction4.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 5) Then
            cmdAction5.Text = StandardAction(i).Name
            cmdAction5.Tag = Trim(str(i))
            cmdAction5.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction5.BackColor = ButtonSelectionColor
                cmdAction5.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 6) Then
            cmdAction6.Text = StandardAction(i).Name
            cmdAction6.Tag = Trim(str(i))
            cmdAction6.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction6.BackColor = ButtonSelectionColor
                cmdAction6.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 7) Then
            cmdAction7.Text = StandardAction(i).Name
            cmdAction7.Tag = Trim(str(i))
            cmdAction7.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction7.BackColor = ButtonSelectionColor
                cmdAction7.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 8) Then
            cmdAction8.Text = StandardAction(i).Name
            cmdAction8.Tag = Trim(str(i))
            cmdAction8.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction8.BackColor = ButtonSelectionColor
                cmdAction8.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 9) Then
            cmdAction9.Text = StandardAction(i).Name
            cmdAction9.Tag = Trim(str(i))
            cmdAction9.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction9.BackColor = ButtonSelectionColor
                cmdAction9.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 10) Then
            cmdAction10.Text = StandardAction(i).Name
            cmdAction10.Tag = Trim(str(i))
            cmdAction10.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction10.BackColor = ButtonSelectionColor
                cmdAction10.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 11) Then
            cmdAction11.Text = StandardAction(i).Name
            cmdAction11.Tag = Trim(str(i))
            cmdAction11.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction11.BackColor = ButtonSelectionColor
                cmdAction11.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 12) Then
            cmdAction12.Text = StandardAction(i).Name
            cmdAction12.Tag = Trim(str(i))
            cmdAction12.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction12.BackColor = ButtonSelectionColor
                cmdAction12.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 13) Then
            cmdAction13.Text = StandardAction(i).Name
            cmdAction13.Tag = Trim(str(i))
            cmdAction13.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction13.BackColor = ButtonSelectionColor
                cmdAction13.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 14) Then
            cmdAction14.Text = StandardAction(i).Name
            cmdAction14.Tag = Trim(str(i))
            cmdAction14.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction14.BackColor = ButtonSelectionColor
                cmdAction14.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 15) Then
            cmdAction15.Text = StandardAction(i).Name
            cmdAction15.Tag = Trim(str(i))
            cmdAction15.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction15.BackColor = ButtonSelectionColor
                cmdAction15.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 16) Then
            cmdAction16.Text = StandardAction(i).Name
            cmdAction16.Tag = Trim(str(i))
            cmdAction16.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction16.BackColor = ButtonSelectionColor
                cmdAction16.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 17) Then
            cmdAction17.Text = StandardAction(i).Name
            cmdAction17.Tag = Trim(str(i))
            cmdAction17.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction17.BackColor = ButtonSelectionColor
                cmdAction17.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 18) Then
            cmdAction18.Text = StandardAction(i).Name
            cmdAction18.Tag = Trim(str(i))
            cmdAction18.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction18.BackColor = ButtonSelectionColor
                cmdAction18.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 19) Then
            cmdAction19.Text = StandardAction(i).Name
            cmdAction19.Tag = Trim(str(i))
            cmdAction19.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction19.BackColor = ButtonSelectionColor
                cmdAction19.ForeColor = TextSelectionColor
            End If
        ElseIf (ButtonCnt = 20) Then
            cmdAction20.Text = StandardAction(i).Name
            cmdAction20.Tag = Trim(str(i))
            cmdAction20.Visible = True
            If (IsActionSet(StandardAction(i).Name)) Then
                cmdAction20.BackColor = ButtonSelectionColor
                cmdAction20.ForeColor = TextSelectionColor
            End If
        End If
        i = i + 1
        ButtonCnt = ButtonCnt + 1
        CurrentActionIndex = i
    Wend
    LoadActions = True
End If

ColorRxBtns

If (CurrentActionIndex <= MaxOtherActions) Then
    cmdMoreActions.Text = "More Actions"
Else
    cmdMoreActions.Text = "Beginning of Action List"
End If
If (MaxOtherActions < 21) Then
    cmdMoreActions.Visible = False
End If
cmdAssign.Visible = Not ViewOn
cmdCheckOut.Visible = Not ViewOn
cmdCheckOut.Visible = False
cmdReviewSystems.Visible = Not ViewOn
cmdDone.Text = "Done"
If (SetCloseChart) Then
    cmdDone.Text = "Close Chart"
End If


logPatientId PatientId
End Function

Private Function SetDemoLabels() As Boolean
    On Error GoTo lSetDemoLabels_Error

    lblPatientAge.Caption = frmSystems1.txtAge.Text
    globalExam.SetExamIds PatientId, AppointmentId, 0
    globalExam.GetDemoInfo
    lblName.Caption = globalExam.sFullName
    lblName.Visible = True
    lblType.Caption = globalExam.sType
    lblType.Visible = True
    lblBirthdate.Caption = globalExam.dtBirthDate.GetDisplayDate(False)
    lblBirthdate.Visible = True
    DoEvents

    Exit Function

lSetDemoLabels_Error:

    LogError "frmEvaluation", "SetDemoLabels", Err, Err.Description
End Function

Private Sub ColorRxBtns()
Dim cntl As Control
Dim FileType As String
Dim TheFile As String

For Each cntl In Me
    If InStrPS(1, cntl.Name, "cmdAction") > 0 Then
        Select Case UCase(Trim(cntl.Text))
        Case "DISPENSE SPECTACLE RX"
            FileType = "07R"
        Case "DISPENSE CL RX"
            FileType = "07S"
        Case Else
            FileType = ""
        End Select
        
        If Not FileType = "" Then
            TheFile = GetTestFileName(FileType)
            If DoesScratchFileExist(TheFile) Then
                cntl.BackColor = ButtonSelectionColor
                cntl.ForeColor = TextSelectionColor
            Else
                cntl.BackColor = BaseBackColor
                cntl.ForeColor = BaseTextColor
            End If
        End If
    End If
Next

End Sub


Private Sub TriggerActions(AButton As fpBtn)

Select Case UCase(Trim(AButton.Text))
Case "DISPENSE SPECTACLE RX", "DISPENSE CL RX"
    Call frmSystems1.ShowRefract
    'refreash
    Call LoadFromRefractFile(False)
    ColorRxBtns
    Exit Sub
End Select
'===================================

Dim CurMax As Integer
Dim Index As Integer, ClearDiag As Integer
Dim Ref As String, Temp As String
If (AButton.BackColor = BaseBackColor) _
And Not AButton.Text = "Rx                                                              " Then
    Call LoadReview(AButton.Text, AButton.Tag)
    If (lstReview.ListCount > 1) Then
        lstReview.ListIndex = 1
        CurMax = TotalSpecificActions(AButton.Text)
        If (CurMax > 0) And (CurMax < MaxActions) Then
            AButton.BackColor = ButtonSelectionColor
            AButton.ForeColor = TextSelectionColor
            If (UCase(Trim(AButton.Text)) = "RX") Then
                If (AnyRxChanges(PatientId)) Then
                    AButton.BackColor = ButtonSelectionColor
                    AButton.ForeColor = TextSelectionColor
                End If
            End If
        Else
            If (CurMax > MaxActions) Then
                frmEventMsgs.Header = "Plan elements are at a maximium."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            Else
                Call ProcessAction(AButton)
            End If
        End If
    Else
        Call ProcessAction(AButton)
    End If
Else
    Call LoadReview(AButton.Text, AButton.Tag)
    If (lstReview.ListCount > 1) And (InStrPS(UCase(lstReview.List(1)), "DISPENSE") = 0) Then
        lstReview.ListIndex = 1
    Else
        lstReview.AddItem ""
        lstReview.ListIndex = 1
        'Call ProcessAction(AButton)
    End If
End If
End Sub

Public Function IsActionSet(TheAction As String) As Boolean
Dim i As Integer
IsActionSet = False
For i = 1 To TotalActionEvaluation(True)
    'If (InStrPS(1, UCase(Trim(SelectedActions(i).Name)), UCase(Trim(TheAction)), vbTextCompare) <> 0) Then
    If (UCase(Trim(SelectedActions(i).Name)) = UCase(Trim(TheAction))) Then
        IsActionSet = True
        Exit For
    End If
Next i
End Function

Public Function InitializeActionEvaluation() As Boolean
Dim i As Integer
Dim j As Integer
InitializeActionEvaluation = True
For i = 1 To MaxActions
    SelectedActions(i).Name = ""
    For j = 1 To 3
        SelectedActions(i).Trigger(j) = ""
        SelectedActions(i).TriggerResult(j) = ""
    Next j
    SelectedActions(i).Highlight = False
    SelectedActions(i).FollowUp = False
    SelectedActions(i).SpecCLRx = ""
    SelectedActions(i).Comments(0) = ""
    SelectedActions(i).Comments(1) = ""
Next i
End Function

'Public Function TotalActionEvaluation() As Integer
'Dim i As Integer
'TotalActionEvaluation = 0
'For i = 1 To MaxActions
'    If (Trim(SelectedActions(i).Name) = "") Then
'        TotalActionEvaluation = i - 1
'        Exit For
'    Else
'        TotalActionEvaluation = i
'    End If
'Next i
'End Function

Public Function TotalActionEvaluation(AllItems As Boolean) As Integer
Dim i As Integer
Dim Cntr(1) As Integer
Dim Cond(1) As Boolean
For Cntr(1) = 1 To MaxActions
    Cond(0) = (Trim(SelectedActions(Cntr(1)).Name) = "")
    Cond(1) = (Trim(SelectedActions(Cntr(1)).SpecCLRx) = "")
    Select Case True
    Case Cond(0) And Cond(1)
        If Cntr(0) = 0 Then Cntr(0) = Cntr(1)
    Case Else
        If Cntr(0) > 0 And Cntr(0) < Cntr(1) Then
            ' move up: item(cntr(0)) = item(cntr(1))
            SelectedActions(Cntr(0)).FollowUp = SelectedActions(Cntr(1)).FollowUp
            SelectedActions(Cntr(0)).Highlight = SelectedActions(Cntr(1)).Highlight
            SelectedActions(Cntr(0)).Name = SelectedActions(Cntr(1)).Name
            SelectedActions(Cntr(0)).Trigger(1) = SelectedActions(Cntr(1)).Trigger(1)
            SelectedActions(Cntr(0)).Trigger(2) = SelectedActions(Cntr(1)).Trigger(2)
            SelectedActions(Cntr(0)).Trigger(3) = SelectedActions(Cntr(1)).Trigger(3)
            SelectedActions(Cntr(0)).TriggerResult(1) = SelectedActions(Cntr(1)).TriggerResult(1)
            SelectedActions(Cntr(0)).TriggerResult(2) = SelectedActions(Cntr(1)).TriggerResult(2)
            SelectedActions(Cntr(0)).TriggerResult(3) = SelectedActions(Cntr(1)).TriggerResult(3)
            SelectedActions(Cntr(0)).SpecCLRx = SelectedActions(Cntr(1)).SpecCLRx
            SelectedActions(Cntr(0)).Comments(0) = SelectedActions(Cntr(1)).Comments(0)
            SelectedActions(Cntr(0)).Comments(1) = SelectedActions(Cntr(1)).Comments(1)
            
            SelectedActions(Cntr(1)).FollowUp = False
            SelectedActions(Cntr(1)).Highlight = False
            SelectedActions(Cntr(1)).Name = ""
            SelectedActions(Cntr(1)).Trigger(1) = ""
            SelectedActions(Cntr(1)).Trigger(2) = ""
            SelectedActions(Cntr(1)).Trigger(3) = ""
            SelectedActions(Cntr(1)).TriggerResult(1) = ""
            SelectedActions(Cntr(1)).TriggerResult(2) = ""
            SelectedActions(Cntr(1)).TriggerResult(3) = ""
            SelectedActions(Cntr(1)).SpecCLRx = ""
            SelectedActions(Cntr(1)).Comments(0) = ""
            SelectedActions(Cntr(1)).Comments(1) = ""
            Cntr(0) = Cntr(0) + 1
        End If
    End Select
Next

Cntr(0) = 0
Cntr(1) = 0
For i = 1 To MaxActions
    Cond(0) = (Trim(SelectedActions(i).Name) = "")
    Cond(1) = (Trim(SelectedActions(i).SpecCLRx) = "")
    Select Case True
    Case (Not Cond(0)) And Cond(1)
        Cntr(0) = Cntr(0) + 1
    Case Cond(0) And (Not Cond(1))
        Cntr(1) = Cntr(1) + 1
    Case Cond(0) And Cond(1)
        Exit For
    Case Else
        MsgBox "Error in TotalActionEvaluation"
    End Select
Next

If AllItems Then
    TotalActionEvaluation = Cntr(0) + Cntr(1)
Else
    TotalActionEvaluation = Cntr(0)
End If
End Function

Public Function GetActionEvaluation(RefId As Integer, AEvaluation As String, AHigh As Boolean, AFollow As Boolean, _
                                    Optional SpecCLRx As String, _
                                    Optional comm0 As String, _
                                    Optional comm1 As String) As Boolean
GetActionEvaluation = SetActionEvaluation(RefId, AEvaluation, AHigh, AFollow, _
                                    SpecCLRx, comm0, comm1)
End Function

Private Function SetActionEvaluation(Ref As Integer, TheActionEvaluation As String, TheHigh As Boolean, TheFollow As Boolean, _
                                    Optional SpecCLRx As String, _
                                    Optional comm0 As String, _
                                    Optional comm1 As String) As Boolean
Dim q As Integer
SetActionEvaluation = False
TheActionEvaluation = ""
If (Ref < 1) Or (Ref > MaxActions) Then
    Exit Function
End If
If (Trim(SelectedActions(Ref).Name) <> "") And (Left(SelectedActions(Ref).Name, 1) <> Chr(0)) Then
    TheActionEvaluation = Trim(SelectedActions(Ref).Name)
    If (Len(Trim(TheActionEvaluation)) > 1) Then
        For q = 1 To 3
            If (Trim(SelectedActions(Ref).Trigger(q)) <> "") And (Trim(SelectedActions(Ref).TriggerResult(q)) <> "") Then
                TheActionEvaluation = TheActionEvaluation + "-" + Trim(SelectedActions(Ref).Trigger(q)) + "/" + Trim(SelectedActions(Ref).TriggerResult(q))
            End If
        Next q
        TheHigh = SelectedActions(Ref).Highlight
        TheFollow = SelectedActions(Ref).FollowUp
    Else
        TheActionEvaluation = ""
    End If
    SetActionEvaluation = True
End If

SpecCLRx = SelectedActions(Ref).SpecCLRx
comm0 = SelectedActions(Ref).Comments(0)
comm1 = SelectedActions(Ref).Comments(1)
If Not SelectedActions(Ref).SpecCLRx = "" Then SetActionEvaluation = True
End Function

Private Function GetReferralDoctor(ApptId As Long, RefId As Long) As String
Dim RetAppt As SchedulerAppointment
Dim RetPat As Patient
Dim RetVen As PracticeVendors
GetReferralDoctor = ""
RefId = 0
If (ApptId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        Set RetPat = New Patient
        RetPat.PatientId = RetAppt.AppointmentPatientId
        If (RetPat.RetrievePatient) Then
            If (RetPat.ReferringPhysician > 0) Then
                Set RetVen = New PracticeVendors
                RetVen.VendorId = RetPat.ReferringPhysician
                If (RetVen.RetrieveVendor) Then
                    GetReferralDoctor = Trim(RetVen.VendorName)
                    RefId = RetVen.VendorId
                End If
                Set RetVen = Nothing
            End If
        End If
        Set RetPat = Nothing
    End If
    Set RetAppt = Nothing
End If
End Function

Private Function GetPCP(ApptId As Long, RefId As Long) As String
Dim RetAppt As SchedulerAppointment
Dim RetPat As Patient
Dim RetVen As PracticeVendors
GetPCP = ""
RefId = 0
Set RetAppt = New SchedulerAppointment
RetAppt.AppointmentId = ApptId
If (RetAppt.RetrieveSchedulerAppointment) Then
    Set RetPat = New Patient
    RetPat.PatientId = RetAppt.AppointmentPatientId
    If (RetPat.RetrievePatient) Then
        If (RetPat.PrimaryCarePhysician > 0) Then
            Set RetVen = New PracticeVendors
            RetVen.VendorId = RetPat.PrimaryCarePhysician
            If (RetVen.RetrieveVendor) Then
                GetPCP = Trim(RetVen.VendorName)
                RefId = RetVen.VendorId
            End If
            Set RetVen = Nothing
        End If
    End If
    Set RetPat = Nothing
End If
Set RetAppt = Nothing
End Function

Public Function PurgeRetainedEvaluations(PatId As Long) As Boolean
Dim TheFile As String
PurgeRetainedEvaluations = True
If (PatId > 0) Then
    Call InitializeActionEvaluation
    TheFile = DoctorInterfaceDirectory + "EvalActions" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
    If (FM.IsFileThere(TheFile)) Then
        FM.Kill TheFile
    End If
End If
End Function


Public Sub LoadFromRefractFile(ActionOn As Boolean)
On Error GoTo errorhandeler

Dim i As Integer
Dim q As Integer
Dim Temp As String
Dim Rec As String
Dim TheFile As String
Dim FileNum As Integer
Dim AButton As fpBtn
Dim RetCLInv As New CLInventory
Dim MemI As Integer
Dim segment() As String
Dim s As Integer
Dim CN As Integer
Dim c As Integer
Dim ImageDesc As String
Dim comm(24, 1) As String

'===================================================
For i = 1 To MaxActions
    If ActionOn _
    Or (Not SelectedActions(i).SpecCLRx = "") Then
        SelectedActions(i).FollowUp = False
        SelectedActions(i).Highlight = False
        SelectedActions(i).Name = ""
        SelectedActions(i).Trigger(1) = ""
        SelectedActions(i).Trigger(2) = ""
        SelectedActions(i).Trigger(3) = ""
        SelectedActions(i).TriggerResult(1) = ""
        SelectedActions(i).TriggerResult(2) = ""
        SelectedActions(i).TriggerResult(3) = ""
        SelectedActions(i).SpecCLRx = ""
        SelectedActions(i).Comments(0) = ""
        SelectedActions(i).Comments(1) = ""
    End If
Next
i = TotalActionEvaluation(True)
'==================================================
If ActionOn Then
    TheFile = DoctorInterfaceDirectory + "EvalActions" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
    If (FM.IsFileThere(TheFile)) Then
        FileNum = FreeFile
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
        While Not (EOF(FileNum))
            i = i + 1
            Line Input #FileNum, Rec
            If (Trim(Rec) <> "") Then
                SelectedActions(i).Name = Trim(Rec)
                For q = 1 To 3
                    If Not (EOF(FileNum)) Then
                        Line Input #FileNum, Rec
                        SelectedActions(i).Trigger(q) = Val(Trim(Rec))
                    End If
                    If Not (EOF(FileNum)) Then
                        Line Input #FileNum, Rec
                        Temp = Rec
                        Call ReplaceCharacters(Temp, "-&-", vbCrLf)
                        SelectedActions(i).TriggerResult(q) = Trim(Temp)
                    End If
                Next q
                If (Not (EOF(FileNum))) Then
                    Line Input #FileNum, Rec
                    SelectedActions(i).Highlight = False
                    If (Trim(Rec) = "T") Then
                        SelectedActions(i).Highlight = True
                    End If
                End If
                If (Not (EOF(FileNum))) Then
                    Line Input #FileNum, Rec
                    SelectedActions(i).FollowUp = False
                    If (Trim(Rec) = "T") Then
                        SelectedActions(i).FollowUp = True
                    End If
                End If
            End If
        Wend
        FM.CloseFile CLng(FileNum)
        'Kill TheFile
    End If
End If

MemI = i
CN = i
'------------------------------------------------------------------------------
' 3 read T07R SpecRx
ImageDesc = "^"
Temp = ""
TheFile = GetTestFileName("07R")
If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    Do Until EOF(FileNum)
        Line Input #FileNum, Rec
        Rec = Trim(Rec)
        Temp = UCase(Rec)
        
        If Mid(Rec, 1, 6) = "Recap=" Then
            Select Case True
            Case Mid(Temp, 1, 13) = "RECAP=SPEC RX"
                If IsNumeric(Mid(Rec, 14, 1)) Then
                    i = Mid(Rec, 14, 1)
                    If i > 1 Then GoSub getSpecCLRx
                    ReDim segment(7) As String
                    segment(0) = "Dispense Spectacle Rx-9/"
                Else
                    s = 2
                End If
            Case Mid(Temp, 1, 13) = "RECAP=*OS-GL~"
                s = 3
            Case Mid(Temp, 1, 11) = "RECAP=TYPE:"
                segment(1) = Mid(Rec, 12, Len(Rec) - 12)
            Case Else
                Temp = Mid(Rec, 7, Len(Rec) - 7)
                If Len(Temp) > 1 Then
                    If UCase(Left(Temp, 2)) = "XX" Then Temp = Mid(Temp, 2)
                End If
                segment(s) = segment(s) & " " & Temp
            End Select
        Else
            Select Case True
            Case UCase(Mid(Rec, 1, 4)) = "TIME"
                CN = CN + 1
            Case UCase(Mid(Rec, 1, 4)) = "*OD-"
                c = 0
            Case UCase(Mid(Rec, 1, 4)) = "*OS-"
                c = 1
            Case UCase(Mid(Rec, 1, 8)) = "*COMMENT"
                GoSub GetComm
            End Select
        End If
        If (EOF(FileNum)) Then GoSub getSpecCLRx
    Loop
    FM.CloseFile CLng(FileNum)
End If
'------------------------------------------------------------------------------
'read T07S CLRx
ImageDesc = "^"
Temp = ""
TheFile = GetTestFileName("07S")
If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    Do Until EOF(FileNum)
        Line Input #FileNum, Rec
        Rec = Trim(Rec)
        Temp = UCase(Rec)
        
        If Mid(Rec, 1, 6) = "Recap=" Then
            Select Case True
            Case Mid(Temp, 1, 10) = "RECAP=CLRX"
                i = Mid(Rec, 11, 1)
                If i > 1 Then GoSub getSpecCLRx
                ReDim segment(7) As String
                segment(0) = "Dispense CL Rx-9/"
            Case Mid(Temp, 1, 17) = "RECAP=CL RX:^OD:~"
                s = 2
            Case Mid(Temp, 1, 11) = "RECAP=^OS:~"
                s = 3
            Case Mid(Temp, 1, 12) = "RECAP=MODEL:"
                segment(s + 3) = RetCLInv.GetCLInventoryIdGeneric(Mid(Rec, 13, Len(Rec) - 13))
            Case Else
                Temp = Mid(Rec, 7, Len(Rec) - 7)
                If Len(Temp) > 1 Then
                    If UCase(Left(Temp, 2)) = "XX" Then Temp = Mid(Temp, 2)
                End If
                segment(s) = segment(s) & " " & Temp
            End Select
        Else
            Select Case True
            Case UCase(Mid(Rec, 1, 4)) = "TIME"
                CN = CN + 1
            Case UCase(Mid(Rec, 1, 4)) = "*OD-"
                c = 0
            Case UCase(Mid(Rec, 1, 4)) = "*OS-"
                c = 1
            Case UCase(Mid(Rec, 1, 8)) = "*COMMENT"
                GoSub GetComm
            End Select
        End If
        If (EOF(FileNum)) Then GoSub getSpecCLRx
    Loop
    FM.CloseFile CLng(FileNum)
End If
'------------------------------------------------------------------------------
'read T09G  orderTrial
ImageDesc = "~"
Temp = ""
TheFile = GetTestFileName("09G")
If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    Do Until EOF(FileNum)
        Line Input #FileNum, Rec
        Rec = Trim(Rec)
        Temp = UCase(Rec)
        
        If Mid(Rec, 1, 6) = "Recap=" Then
            Select Case True
            Case Mid(Temp, 1, 18) = "RECAP=CLORDERTRIAL"
                i = Mid(Rec, 19, 1)
                If i > 1 Then GoSub getSpecCLRx
                ReDim segment(7) As String
                segment(0) = "Order Trial CL-9/"
            Case Mid(Temp, 1, 26) = "RECAP=CL ORDER TRIAL:^OD:~"
                s = 2
            Case Mid(Temp, 1, 11) = "RECAP=^OS:~"
                s = 3
            Case Mid(Temp, 1, 12) = "RECAP=MODEL:"
                segment(s + 3) = RetCLInv.GetCLInventoryIdGeneric(Mid(Rec, 13, Len(Rec) - 13))
            Case Else
                Temp = Mid(Rec, 7, Len(Rec) - 7)
                If Len(Temp) > 1 Then
                    If UCase(Left(Temp, 2)) = "XX" Then Temp = Mid(Temp, 2)
                End If
                segment(s) = segment(s) & " " & Temp
            End Select
        Else
            Select Case True
            Case UCase(Mid(Rec, 1, 4)) = "TIME"
                CN = CN + 1
            Case UCase(Mid(Rec, 1, 4)) = "*OD-"
                c = 0
            Case UCase(Mid(Rec, 1, 4)) = "*OS-"
                c = 1
            Case UCase(Mid(Rec, 1, 8)) = "*COMMENT"
                GoSub GetComm
            End Select
        End If
        If (EOF(FileNum)) Then GoSub getSpecCLRx
    Loop
    FM.CloseFile CLng(FileNum)
End If
'------------------------------------------------------------------------------
Set RetCLInv = Nothing
Exit Sub
'==============================================================================
getSpecCLRx:
If UBound(segment) > 0 Then
    MemI = MemI + 1
    Temp = segment(0)
    For s = 1 To 7
        Temp = Temp & "-" & s & "/" & segment(s)
    Next
    SelectedActions(MemI).SpecCLRx = Temp
    SelectedActions(MemI).Comments(0) = comm(MemI, 0)
    SelectedActions(MemI).Comments(1) = comm(MemI, 1)
End If
ReDim segment(0) As String
Temp = ""
Return
'=============================================
GetComm:
Temp = Mid(Rec, 1, Len(Rec) - 1)
Do Until Temp = ""
    If Left(Temp, 1) = "<" Then
        Temp = Mid(Temp, 2)
        Exit Do
    End If
    Temp = Mid(Temp, 2)
Loop

comm(CN, c) = Temp
Return

errorhandeler:
Dim FileDesc As String
Select Case UCase(Mid(Dir(TheFile), 1, 4))
Case "T07A"
    FileDesc = "GLASSES"
Case "T08A"
    FileDesc = "CONTACT LENSES"
Case "T09A"
    FileDesc = "RETINOSCOPY"
Case "T10A"
    FileDesc = "AUTOREFRACTION"
Case "T13A"
    FileDesc = "MANIFEST REFRACTION"
Case "T07R"
    FileDesc = "GLASSES RX"
Case "T07S"
    FileDesc = "CL RX"
Case "T07T"
    FileDesc = "CL TRIAL 1"
Case "T07U"
    FileDesc = "CL TRIAL 2"
Case "T09G"
    FileDesc = "CL ORDER TRIAL"
Case "T09D"
    FileDesc = "DILATED REFRACTION"
End Select

MsgBox "There was a problem loading the " & _
        FileDesc & " file." & vbNewLine & _
        "Open the Refract screen to check if an element is missing." & vbNewLine & _
        "Press Done from the refract screen to correct the problem."

LogError "frmEvaluation", "LoadFromRefractFile: " & TheFile, Err, Err.Description
FM.CloseFile CLng(FileNum)
End Sub

Public Sub PostToFile()

Dim PatId As Long
Dim AptId As Long

AptId = globalExam.iAppointmentId
PatId = globalExam.iPatientId

Dim i As Integer
Dim q As Integer
Dim Temp As String
Dim Rec As String
Dim TheFile As String
Dim FileNum As Integer
Dim AButton As fpBtn
If (PatId > 0) Then
    FileNum = FreeFile
    TheFile = DoctorInterfaceDirectory + "EvalActions" + "_" + Trim(str(AptId)) + "_" + Trim(str(PatId)) + ".txt"
    FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(FileNum)
    For i = 1 To TotalActionEvaluation(True)
        If (Trim(SelectedActions(i).Name) <> "") Then
            Print #FileNum, Trim(SelectedActions(i).Name)
            For q = 1 To 3
                Print #FileNum, Trim(SelectedActions(i).Trigger(q))
                Temp = Trim(SelectedActions(i).TriggerResult(q))
                Call ReplaceCharacters(Temp, vbCrLf, "-&-")
                Print #FileNum, Temp
            Next q
            If (SelectedActions(i).Highlight) Then
                Print #FileNum, "T"
            Else
                Print #FileNum, "F"
            End If
            If (SelectedActions(i).FollowUp) Then
                Print #FileNum, "T"
            Else
                Print #FileNum, "F"
            End If
        End If
    Next i
    FM.CloseFile CLng(FileNum)
End If
End Sub
Private Sub lblAlert_Click()
If (frmAlert.LoadAlerts(PatientId, EAlertType.eatPlan, True)) Then
    frmAlert.Show 1
    If (IsAlertPresent(PatientId, EAlertType.eatPlan)) Then
        lblAlert.Caption = "Plan Alert ON"
        lblAlert.Visible = True
    Else
        lblAlert.Caption = "Alert ON"
        lblAlert.Visible = IsAlertPresent(PatientId, 0)
    End If
End If
End Sub

Private Sub lstReview_Click()
Dim LocalChoice As Integer
Dim m As Integer, w As Integer
Dim k As Integer, j As Integer, i As Integer
Dim Loc As Integer, RefI As Integer, ClearDiag As Integer
Dim Temp As String, Temp1 As String, ATemp As String
Dim RxOn As Boolean
Dim Proceed As Boolean
Dim KeepProcessing As Boolean
LocalChoice = lstReview.ListIndex
If (LocalChoice = 0) Then
    lstReview.Visible = False
    lstReview.Clear
ElseIf (LocalChoice > 0) Then
    RxOn = False
    If (InStrPS(lstReview.List(0), "RX") > 0) Then
     If Not UserLogin.HasPermission(epMeds) Then
            frmEventMsgs.Header = "Not Permissioned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
            End If
        RxOn = True
    End If
    Proceed = True
    Do Until Not (Proceed)
        KeepProcessing = False
        If (RxOn) Then
            frmRxManage.EditPreviousOn = False
            frmRxManage.LatestAppointmentOn = frmSystems1.LatestAppointmentOn
            frmRxManage.PrescribeOn = True
            frmRxManage.ActivityId = globalExam.iActivityId
            frmRxManage.AppointmentId = AppointmentId
            frmRxManage.PatientId = PatientId
            Dim pat As New Patient
            frmRxManage.lblPatient = pat.GetPatientInfo(PatientId)
            Set pat = Nothing
            Set frmRxManage.lstExternal = frmSystems1.GetAllergy
            If (frmRxManage.LoadRxs(True)) Then
                frmRxManage.Show 1
                KeepProcessing = False
                Proceed = False
                i = Val(Mid(lstReview.Tag, 2, Len(lstReview.Tag) - 1))
                Temp = Left(lstReview.List(1), 100)
                Call ResetButton("Rx ", (TotalSpecificActions("RX") > 0))
            Else
                KeepProcessing = False
                Proceed = False
            End If
        Else
            frmAppType.ApptPeriod = ""
            frmAppType.ApptTypeId = 0
            frmAppType.ApptTypeName = ""
            Set frmAppType.lstBox = lstReview
            If (frmAppType.LoadAppt("]", True)) Then
                frmAppType.Show 1
                If (frmAppType.ApptTypeId = -3) Then
                    If (lstReview.ListCount - 1 > 1) Then
                        Loc = LocalChoice
                        j = InStrPS(lstReview.List(2), "-")
                        If (j > 0) Then
                            Temp = Left(lstReview.List(2), 100)
                        Else
                            Temp = Left(lstReview.List(1), 100)
                        End If
                    Else
                        j = InStrPS(lstReview.List(1), "-")
                    End If
                    Loc = LocalChoice
                    Temp = Left(lstReview.List(1), 100)
                    If (Left(Temp, 5) = "     ") Then
                        For i = lstReview.ListIndex To 1 Step -1
                            If (Left(lstReview.List(i), 5) = "Rx - ") Then
                                Temp = Trim(Left(lstReview.List(i), 100))
                                Loc = i
                                Exit For
                            End If
                        Next i
                    End If
                    i = Val(Mid(lstReview.Tag, 2, Len(lstReview.Tag) - 1))
                    ClearDiag = GetNextAction
                    If (ClearDiag > 0) Then
                        k = InStrPS(Temp, "-")
                        If (k > 0) Then
                            Temp = Left(Temp, k - 1)
                        End If
                        Call ProcessTrigger(Temp, ClearDiag, i)
                        Call ResetButton(Temp, (IsActionSet(Temp)))
                        Call LoadReview(Temp, Trim(str(i)))
                    End If
                ElseIf (frmAppType.ApptTypeId = -2) Then
                    Proceed = False
                    If (lstReview.ListCount > 2) Then
                        k = InStrPS(lstReview.List(2), "-")
                        RefI = 2
                        If (k < 1) Then
                            k = InStrPS(lstReview.List(1), "-")
                            RefI = 1
                        End If
                    Else
                        k = InStrPS(lstReview.List(1), "-")
                        RefI = 1
                    End If
                    If (k > 0) Then
                        Temp = Trim(Left(lstReview.List(RefI), k - 1))
                    End If
                    Call ClearTrigger(Temp, 0)
                    Call ResetButton(Temp, False)
                ElseIf (frmAppType.ApptTypeId < 1) Then
                    Proceed = False
                    j = InStrPS(lstReview.List(LocalChoice), "-")
                    If (j > 0) Then
                        Temp = Trim(Left(lstReview.List(LocalChoice), j - 1))
                        Call ResetButton(Temp, (TotalSpecificActions(Temp) > 0))
                    End If
                    i = Val(Mid(lstReview.Tag, 2, Len(lstReview.Tag) - 1))
                    lstReview.Visible = False
                    lstReview.Clear
                Else
                    Proceed = False
                    LocalChoice = frmAppType.ApptTypeId
                    KeepProcessing = True
                End If
            Else
                KeepProcessing = False
                Proceed = False
            End If
        End If
        
        If (KeepProcessing) Then
            Proceed = True
            j = InStrPS(lstReview.List(LocalChoice), "-")
            Loc = LocalChoice
            Temp = Left(lstReview.List(LocalChoice), 100)
            If (Left(Temp, 5) = "     ") Then
                For i = lstReview.ListIndex To 1 Step -1
                    If (UCase(Left(lstReview.List(i), 5)) = "RX - ") Then
                        Temp = Trim(Left(lstReview.List(i), 100))
                        Loc = i
                        Exit For
                    End If
                Next i
            End If
            frmEventMsgs.Header = Trim(Temp)
            frmEventMsgs.AcceptText = ""
            If (InStrPS(UCase(Temp), "WRITTEN") <> 0) Then
                frmEventMsgs.AcceptText = "Print"
            End If
            frmEventMsgs.RejectText = "Delete"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                lblPrint.Caption = "Printing Written Instructions"
                DoEvents
                m = 0
                ATemp = Temp
                Call ReplaceCharacters(ATemp, "-0", " ")
                Call ReplaceCharacters(ATemp, "//", "/")
                Call StripCharacters(ATemp, " ")
                ATemp = Trim(ATemp)
                If (Mid(ATemp, Len(ATemp) - 1, 2) = "//") Then
                    Mid(ATemp, Len(ATemp) - 1, 2) = "  "
                ElseIf (Mid(ATemp, Len(ATemp), 1) = "/") Then
                    Mid(ATemp, Len(ATemp), 1) = " "
                End If
                ATemp = Trim(ATemp)
                For w = Len(ATemp) To 1 Step -1
                    If (Mid(ATemp, w, 1) = "/") Then
                        m = w + 1
                        Exit For
                    End If
                Next w
                If (m > 1) Then
                    ATemp = Trim(Mid(ATemp, m, Len(ATemp) - (m - 1)))
                    w = InStrPS(UCase(ATemp), "-F")
                    If (w > 0) Then
                        ATemp = Left(ATemp, w - 1)
                    End If
                    ATemp = TemplateDirectory + ATemp + ".doc"
                    If (FM.IsFileThere(ATemp)) Then
                        Call PrintDocument("Written Instructions", ATemp, "")
                    End If
                End If
            ElseIf (frmEventMsgs.Result = 2) Then
                i = InStrPS(Temp, "/")
                If (i < 1) Then
                    i = Len(Temp)
                End If
                If (Mid(Temp, i - 1, 1) = "-") Then
                    Call ClearTrigger(Left(Temp, i - 2), Val(Mid(lstReview.List(Loc), 240, 3)))
                Else
                    Call ClearTrigger(Left(Temp, i - 3), Val(Mid(lstReview.List(Loc), 240, 3)))
                End If
                i = InStrPS(Temp, "/")
                If (i > 0) Then
                    Temp = Left(Temp, i - 3)
                End If
                i = TotalSpecificActions(Temp)
                If (i < 1) Then
                    Call ResetButton(Temp, False)
                End If
                Call DeleteRxNote(AppointmentId, PatientId, Trim(Temp))
                i = Val(Mid(lstReview.Tag, 2, Len(lstReview.Tag) - 1))
            End If
            Temp = lstReview.List(1)
            Call LoadReview(Temp, Trim(str(i)))
        End If
    Loop
    lstReview.Visible = False
    lstReview.Clear
End If
End Sub

Private Function ProcessTrigger(RefName As String, SelectedIndex As Integer, Index As Integer) As Boolean
Dim IsRVOn As Boolean
Dim SetApptOn As Boolean
Dim ApptTypeId As Long, RefId As Long
Dim ATemp As Boolean, OkayToPost As Boolean
Dim OrgSelectedIndex As Integer
Dim f1 As Integer, f2 As Integer, f3 As Integer
Dim f As Integer, j As Integer
Dim w As Integer, q As Integer
Dim i As Integer, t As Integer
Dim u As Integer, h As Integer
Dim z As Integer, z1 As Integer, zz As Integer
Dim UTemp(5) As String
Dim YTemp As String, OtherTemp As String, NRef As String
Dim ApptPeriod As String, Ref As String, YRef As String
Dim Add1 As String, Add2 As String, Add3 As String
Dim Id As String, AMods As String, AEye As String
Dim Diag As String, ASevA As String, ASevB As String
Dim RNote As String, RInvId1 As Long, RInvId2 As Long
Dim Rx As String, RxDiag As String, RxDose As String
Dim RxFreq As String, RxDuration As String, RxRefill As String
Dim RxOrder As String, RxNote As String, TempText As String
Dim RxId As String

Dim Temp As ExternalReference
Dim SelectedText As TheDetails
Dim LocalSelected As TheDetails
ProcessTrigger = True
OkayToPost = True
If (SelectedIndex < 1) Then
    ProcessTrigger = False
    Exit Function
End If
IsRVOn = CheckConfigCollection("DORV") = "T"
Temp = StandardAction(Index)
SelectedText = SelectedActions(SelectedIndex)
ApptPeriod = ""
OrgSelectedIndex = SelectedIndex
SelectedText.Name = Trim(RefName)
q = InStrPS(RefName, "-")
If (q > 0) Then
    SelectedText.Name = Trim(Left(RefName, q - 1))
End If
Add1 = ""
Add2 = ""
Add3 = ""
SetApptOn = False
For q = 1 To 3
    If (Temp.Trigger(q) = "1") Then
        If (Trim(ApptPeriod) = "") Then
            frmNumericPad.NumPad_Result = ""
            frmNumericPad.NumPad_Field = Trim(Temp.Name)
            frmNumericPad.NumPad_Max = 0
            frmNumericPad.NumPad_Min = 0
            frmNumericPad.NumPad_CommentOn = True
            frmNumericPad.NumPad_EyesOn = False
            If (InStrPS(UCase(RefName), "TEST") <> 0) Then
                frmNumericPad.NumPad_EyesOn = True
            ElseIf (InStrPS(UCase(RefName), "OFFICE PROCEDURES") <> 0) Then
                frmNumericPad.NumPad_EyesOn = True
            End If
            frmNumericPad.NumPad_TimeFrameOn = True
            frmNumericPad.NumPad_TimeFrame = ""
            frmNumericPad.NumPad_DisplayFull = True
            frmNumericPad.NumPad_ChoiceOn1 = False
            frmNumericPad.NumPad_Choice1 = ""
            frmNumericPad.NumPad_ChoiceOn2 = False
            frmNumericPad.NumPad_Choice2 = ""
            frmNumericPad.NumPad_ChoiceOn3 = False
            frmNumericPad.NumPad_Choice3 = ""
            frmNumericPad.NumPad_ChoiceOn4 = False
            frmNumericPad.NumPad_Choice4 = ""
            frmNumericPad.NumPad_ChoiceOn5 = False
            frmNumericPad.NumPad_Choice5 = ""
            frmNumericPad.NumPad_ChoiceOn6 = False
            frmNumericPad.NumPad_Choice6 = ""
            frmNumericPad.NumPad_ChoiceOn7 = False
            frmNumericPad.NumPad_Choice7 = ""
            frmNumericPad.NumPad_ChoiceOn8 = False
            frmNumericPad.NumPad_Choice8 = ""
            frmNumericPad.NumPad_ChoiceOn9 = False
            frmNumericPad.NumPad_Choice9 = ""
            frmNumericPad.NumPad_ChoiceOn10 = False
            frmNumericPad.NumPad_Choice10 = ""
            frmNumericPad.NumPad_ChoiceOn11 = False
            frmNumericPad.NumPad_Choice11 = ""
            frmNumericPad.NumPad_ChoiceOn12 = False
            frmNumericPad.NumPad_Choice12 = ""
            frmNumericPad.NumPad_ChoiceOn13 = False
            frmNumericPad.NumPad_Choice13 = ""
            frmNumericPad.NumPad_ChoiceOn14 = False
            frmNumericPad.NumPad_Choice14 = ""
            frmNumericPad.NumPad_ChoiceOn15 = False
            frmNumericPad.NumPad_Choice15 = ""
            frmNumericPad.NumPad_ChoiceOn16 = False
            frmNumericPad.NumPad_Choice16 = ""
            frmNumericPad.NumPad_ChoiceOn17 = False
            frmNumericPad.NumPad_Choice17 = ""
            frmNumericPad.NumPad_ChoiceOn18 = False
            frmNumericPad.NumPad_Choice18 = ""
            frmNumericPad.NumPad_ChoiceOn19 = False
            frmNumericPad.NumPad_Choice19 = ""
            frmNumericPad.NumPad_ChoiceOn20 = False
            frmNumericPad.NumPad_Choice20 = ""
            frmNumericPad.NumPad_ChoiceOn21 = False
            frmNumericPad.NumPad_Choice21 = ""
            frmNumericPad.NumPad_ChoiceOn22 = False
            frmNumericPad.NumPad_Choice22 = ""
            frmNumericPad.NumPad_ChoiceOn23 = False
            frmNumericPad.NumPad_Choice23 = ""
            frmNumericPad.NumPad_ChoiceOn24 = False
            frmNumericPad.NumPad_Choice24 = ""
            frmNumericPad.Show 1
            If (Trim(frmNumericPad.NumPad_Result) <> "") Then
                SetApptOn = frmNumericPad.SetApptOn
                SelectedText.Trigger(q) = "1"
                SelectedText.TriggerResult(q) = Trim(frmNumericPad.NumPad_Result) + " " + Trim(frmNumericPad.NumPad_TimeFrame)
                Add2 = Trim(frmNumericPad.NumPad_Result) + " " + Trim(frmNumericPad.NumPad_TimeFrame)
                If (Trim(frmNumericPad.NumPad_Comments) <> "") Then
                    SelectedText.TriggerResult(q) = Trim(SelectedText.TriggerResult(q)) + "<" + Trim(frmNumericPad.NumPad_Comments) + ">"
                    Add2 = Add2 + "<" + Trim(frmNumericPad.NumPad_Comments) + ">"
                End If
                If (IsRVOn) Then
                    If (SetApptOn) Or ((InStrPS(UCase(RefName), "TEST") = 0) And (InStrPS(UCase(RefName), "OFFICE") = 0) And (InStrPS(UCase(RefName), "SURGERY") = 0)) Then
                        frmAppType.ApptTypeId = 0
                        frmAppType.ApptTypeName = ""
                        frmAppType.ApptPeriod = ""
                        If (frmAppType.LoadAppt("v", True)) Then
                            frmAppType.Show 1
                            If (Trim(frmAppType.ApptTypeName) <> "") And (Trim(frmAppType.ApptTypeName) <> "CURRENTRV") Then
                                If (InStrPS(UCase(RefName), "SURGERY") = 0) Then
                                    SelectedText.TriggerResult(q) = Trim(SelectedText.TriggerResult(q)) + " <^" + Trim(frmAppType.ApptTypeName) + ">"
                                End If
                                Add2 = Add2 + " <^" + Trim(frmAppType.ApptTypeName) + ">"
                            ElseIf (Trim(frmAppType.ApptTypeName) = "CURRENTRV") Then
                                NRef = ""
                                If (frmSystems1.GetRVReference(1, NRef)) Then
                                    Call ReplaceCharacters(NRef, "<", "(")
                                    Call ReplaceCharacters(NRef, ">", ")")
                                    NRef = Trim(Left(NRef, 50))
                                    If (Mid(NRef, 3, 1) = ":") Then
                                        NRef = Mid(NRef, 4, Len(NRef) - 3)
                                    End If
                                End If
                                SelectedText.TriggerResult(q) = Trim(SelectedText.TriggerResult(q)) + " <^" + Trim(NRef) + ">"
                                Add2 = Add2 + " <^" + Trim(NRef) + ">"
                            End If
                        End If
                    End If
                End If
                If (SetApptOn) Then
                    NRef = "a"
                    YRef = ""
                    YTemp = ""
                    If (InStrPS(UCase(RefName), "TEST") <> 0) Then
                        NRef = "A"
                        YRef = "External Tests"
                        YTemp = Add1
                        h = InStrPS(YTemp, "/")
                        If (h > 0) Then
                            YTemp = Trim(Left(YTemp, h - 3))
                        End If
                    ElseIf (InStrPS(UCase(RefName), "OFFICE PROCEDURES") <> 0) Then
                        NRef = "A"
                        YRef = "OfficeProcedures"
                        YTemp = Add1
                        h = InStrPS(YTemp, "/")
                        If (h > 0) Then
                            YTemp = Trim(Left(YTemp, h - 3))
                        End If
                    ElseIf (InStrPS(UCase(RefName), "SCHEDULE SURGERY") <> 0) Then
                        NRef = "A"
                        YRef = "SURGERYTYPE"
                        YTemp = SelectedText.TriggerResult(1)
                        h = InStrPS(YTemp, "/")
                        If (h > 0) Then
                            YTemp = Trim(Left(YTemp, h - 3))
                        End If
                        h = InStrPS(YTemp, "(")
                        If (h > 0) Then
                            YTemp = Trim(Left(YTemp, h - 1))
                        End If
                    End If
                    If Not (IsTestApptTypeSet(YRef, YTemp, OtherTemp)) Then
                        frmAppType.ApptTypeId = 0
                        frmAppType.ApptTypeName = ""
                        frmAppType.ApptPeriod = ""
                        If (frmAppType.LoadAppt(NRef, True)) Then
                            frmAppType.Show 1
                            If (frmAppType.ApptTypeId > 0) Then
                                ApptTypeId = frmAppType.ApptTypeId
                                If (frmAppType.ApptPeriod = "Y") Then
                                    ApptPeriod = "YEAR"
                                ElseIf (frmAppType.ApptPeriod = "M") Then
                                    ApptPeriod = "MONTH"
                                ElseIf (frmAppType.ApptPeriod = "W") Then
                                    ApptPeriod = "WEEK"
                                Else
                                    ApptPeriod = ""
                                End If
                                Add1 = Trim(frmAppType.ApptTypeName) + " "
                            End If
                        End If
                    Else
                        Add1 = Trim(OtherTemp) + " "
                    End If
                    frmAppType.ApptTypeId = ApptTypeId
                    frmAppType.ApptTypeName = ""
                    If (frmAppType.LoadAppt("S", True)) Then
                        frmAppType.Show 1
                        If (Trim(frmAppType.ApptTypeName) <> "") Then
                            If (InStrPS(UCase(RefName), "SURGERY") = 0) Then
                                SelectedText.TriggerResult(q) = Trim(SelectedText.TriggerResult(q)) + " <" + Trim(frmAppType.ApptPeriod) + ">"
                            End If
                            Add3 = Add3 + Trim(frmAppType.ApptPeriod)
                        End If
                    End If
                End If
            Else
                OkayToPost = False
                Exit For
            End If
        Else
            SelectedText.Trigger(q) = "1"
            SelectedText.TriggerResult(q) = Trim(ApptPeriod)
        End If
    ElseIf (Temp.Trigger(q) = "2") Then
        frmAppType.ApptTypeId = 0
        frmAppType.ApptTypeName = ""
        frmAppType.ApptPeriod = ""
        If (frmAppType.LoadAppt("a", True)) Then
            frmAppType.Show 1
            If (frmAppType.ApptTypeId > 0) Then
                ApptTypeId = frmAppType.ApptTypeId
                If (frmAppType.ApptPeriod = "Y") Then
                    ApptPeriod = "YEAR"
                ElseIf (frmAppType.ApptPeriod = "M") Then
                    ApptPeriod = "MONTH"
                ElseIf (frmAppType.ApptPeriod = "W") Then
                    ApptPeriod = "WEEK"
                Else
                    ApptPeriod = ""
                End If
                SelectedText.Trigger(q) = "2"
                SelectedText.TriggerResult(q) = frmAppType.ApptTypeName
            Else
                OkayToPost = False
                Exit For
            End If
        End If
    ElseIf (Temp.Trigger(q) = "3") Then
        frmAppType.ApptTypeId = 0
        frmAppType.ApptTypeName = ""
        If (frmAppType.LoadAppt("C", True)) Then
            frmAppType.Show 1
            If (Trim(frmAppType.ApptTypeName) <> "") Then
                SelectedText.Trigger(q) = "3"
                SelectedText.TriggerResult(q) = frmAppType.ApptTypeName
                Add1 = Trim(frmAppType.ApptTypeName)
                If (IsTestFollowup(Add1, "SURGERYTYPE")) Then
                    SelectedText.FollowUp = True
                End If
            Else
                OkayToPost = False
                Exit For
            End If
        End If
    ElseIf (Temp.Trigger(q) = "4") Then
        frmAppType.ApptTypeId = 0
        frmAppType.ApptTypeName = ""
        If (frmAppType.LoadAppt("D", True)) Then
            frmAppType.Show 1
            If (Trim(frmAppType.ApptTypeName) <> "") Then
                SelectedText.Trigger(q) = "4"
                SelectedText.TriggerResult(q) = frmAppType.ApptTypeName
            Else
                OkayToPost = False
                Exit For
            End If
        End If
    ElseIf (Temp.Trigger(q) = "5") Then
        frmAppType.ApptTypeId = ApptTypeId
        frmAppType.ApptTypeName = ""
        If Trim$(RefName) = "Schedule Appointment" Then
            frmAppType.LoadAppt "S", True, True
        Else
            frmAppType.LoadAppt "S", True
        End If

        frmAppType.Show 1
        If (Trim(frmAppType.ApptTypeName) <> "") Then
            SelectedText.Trigger(q) = "5"
            SelectedText.TriggerResult(q) = Trim(frmAppType.ApptPeriod)
            Call InsertLog("FrmEvaluation.frm", "Order Appointment: " & CStr(frmAppType.ApptTypeName), LoginCatg.ApptCreate, "", "", 0, globalExam.iAppointmentId)
        Else
            OkayToPost = False
            Exit For
        End If

    ElseIf (Temp.Trigger(q) = "6") Then
        SelectedText.Trigger(q) = 6
        SelectedText.TriggerResult(q) = GetReferralDoctor(AppointmentId, RefId)
        If (Trim(SelectedText.TriggerResult(q)) = "") Then
            frmAppType.ApptTypeId = 0
            frmAppType.ApptTypeName = ""
            If (frmAppType.LoadAppt("D", True)) Then
                frmAppType.Show 1
                If (Trim(frmAppType.ApptTypeName) <> "") Then
                    SelectedText.Trigger(q) = "6"
                    If (RefId > 0) Then
                        SelectedText.TriggerResult(q) = frmAppType.ApptTypeName + "(" + RefId + ")"
                    Else
                        SelectedText.TriggerResult(q) = frmAppType.ApptTypeName
                    End If
                Else
                    OkayToPost = False
                    Exit For
                End If
            Else
                OkayToPost = False
                Exit For
            End If
        End If
    ElseIf (Temp.Trigger(q) = "7") Then
        frmAppType.ApptTypeId = 0
        frmAppType.ApptTypeName = ""
        frmAppType.ApptPeriod = ""
        If (frmAppType.LoadAppt("l", True)) Then
            frmAppType.Show 1
            If (Trim(frmAppType.ApptTypeName) <> "") Then
                If (InStrPS(UCase(Trim(SelectedText.Name)), "SCHEDULE SURGERY") > 0) Then
                    Add1 = Trim(frmAppType.ApptTypeName)
                    frmAppType.ApptTypeId = 0
                    frmAppType.ApptTypeName = ""
                    frmAppType.ApptPeriod = ""
                    If (frmAppType.LoadAppt("S", True, True)) Then
                        frmAppType.Show 1
                        If (Trim(frmAppType.ApptTypeName) <> "") Then
                            SelectedText.Trigger(q) = "7"
                            SelectedText.TriggerResult(q) = Add1 + " (" + Trim(frmAppType.ApptPeriod) + ")"

                            Call InsertLog("FrmEvaluation.frm", "Order Appointment: " & CStr(frmAppType.ApptTypeName), LoginCatg.ApptCreate, "", "", 0, globalExam.iAppointmentId)
                        Else
                            SelectedText.Trigger(q) = "7"
                            SelectedText.TriggerResult(q) = Add1
                        End If
                    Else
                        SelectedText.Trigger(q) = "7"
                        SelectedText.TriggerResult(q) = Add1
                    End If
                Else
                    SelectedText.Trigger(q) = "7"
                    SelectedText.TriggerResult(q) = Trim(frmAppType.ApptTypeName)
                End If
            Else
                OkayToPost = False
                Exit For
            End If
        End If
    ElseIf (Temp.Trigger(q) = "8") Then
        SelectedText.Name = ""
        frmAssignDrugs.Diagnosis = ""
        frmAssignDrugs.PatientId = PatientId
        frmAssignDrugs.AppointmentId = AppointmentId
        frmAssignDrugs.FavoriteOn = True
        frmRxDetails.WriteRx = True
        frmAssignDrugs.frmRxManage_Write_RX = (frmRxManage.cmdAnother.Text = "Write RX")
        If (frmAssignDrugs.LoadAvailableRx(True)) Then
        
            frmAssignDrugs.Show 1
            
            For z = 1 To frmAssignDrugs.TotalRx
                If (frmAssignDrugs.getRx(z, Rx, RxDiag, RxDose, RxFreq, RxRefill, RxDuration, RxNote, RxId)) Then
                    SelectedText.Name = "Rx"
                    SelectedText.Trigger(q) = "8"
                    SelectedText.TriggerResult(q) = Rx + "-2/" + RxDose + "-3/" + RxFreq + "-4/" + RxRefill + "-5/" + RxDuration + "-6/" + "P-7/"
                    SelectedActions(SelectedIndex) = SelectedText
                    SelectedIndex = GetNextAction
                    OrgSelectedIndex = SelectedIndex
                    OkayToPost = False
                End If
            Next z
        End If
    ElseIf (Temp.Trigger(q) = "9") Then
        Ref = "CL"
        If (InStrPS(Temp.Name, "CL") = 0) Then
            Ref = RXReference
        End If
        w = 0
        If CheckConfigCollection("NEWEYEWEAR") = "T" Then
            'If (frmAssignWearNew.LoadWear(True, Ref)) Then
            If (frmAssignWearNew.LoadWear) Then
                frmAssignWearNew.PID = "D"
                frmAssignWearNew.EditPreviousVisit = False
                frmAssignWearNew.ActivityId = globalExam.iActivityId
                frmAssignWearNew.AppointmentId = AppointmentId
                frmAssignWearNew.PatientId = PatientId
                frmAssignWearNew.Show 1
                If (Ref = "CL") Then
                    w = frmAssignWearNew.TotalCLRx
                Else
                    w = frmAssignWearNew.TotalPCRx
                End If
                If (w < 1) Then
                    RefName = Temp.Name
                    Call ClearTrigger(Trim(RefName), 0)
                    Call ResetButton(Trim(RefName), False)
                Else
                    RefName = Temp.Name
                    Call ClearTrigger(Trim(RefName), 0)
                    SelectedIndex = GetNextAction
                End If
                For z1 = 1 To w
                    If (InStrPS(RefName, RXReference) <> 0) Then
                        If (frmAssignWearNew.GetPC(z1, Rx, RxDiag, RxDose, RxFreq, RInvId1, RInvId2, RNote)) Then
                            SelectedText.Name = Trim(RefName)
                            SelectedText.Trigger(q) = "9"
                            SelectedText.TriggerResult(q) = RxDiag + "-1/" + Rx + "-2/" + RxDose + "-3/" + RxFreq + "-4/" + RNote + "-5/" + Trim(str(RInvId1)) + "-6/" + Trim(str(RInvId2)) + "-7/"
                            SelectedActions(SelectedIndex) = SelectedText
                            SelectedIndex = GetNextAction
                            OrgSelectedIndex = SelectedIndex
                            OkayToPost = False
                        End If
                    Else
                        If (frmAssignWearNew.GetCL(z1, Rx, RxDiag, RxDose, RxFreq, RInvId1, RInvId2, RNote)) Then
                            SelectedText.Name = Trim(RefName)
                            SelectedText.Trigger(q) = "9"
                            SelectedText.TriggerResult(q) = Rx + "-1/" + RxDiag + "-2/" + RxDose + "-3/" + RxFreq + "-4/" + RNote + "-5/" + Trim(str(RInvId1)) + "-6/" + Trim(str(RInvId2)) + "-7/"
                            SelectedActions(SelectedIndex) = SelectedText
                            SelectedIndex = GetNextAction
                            OrgSelectedIndex = SelectedIndex
                            OkayToPost = False
                        End If
                    End If
                Next z1
            End If
        Else
            If (frmAssignWear.LoadWear(True, Ref)) Then
                frmAssignWear.PID = "D"
                frmAssignWear.EditPreviousVisit = False
                frmAssignWear.ActivityId = globalExam.iActivityId
                frmAssignWear.AppointmentId = AppointmentId
                frmAssignWear.PatientId = PatientId
                frmAssignWear.Show 1
                If (Ref = "CL") Then
                    w = frmAssignWear.TotalCLRx
                Else
                    w = frmAssignWear.TotalPCRx
                End If
                If (w < 1) Then
                    RefName = Temp.Name
                    Call ClearTrigger(Trim(RefName), 0)
                    Call ResetButton(Trim(RefName), False)
                Else
                    RefName = Temp.Name
                    Call ClearTrigger(Trim(RefName), 0)
                    SelectedIndex = GetNextAction
                End If
                For z1 = 1 To w
                    If (InStrPS(RefName, RXReference) <> 0) Then
                        If (frmAssignWear.GetPC(z1, Rx, RxDiag, RxDose, RxFreq, RInvId1, RInvId2, RNote)) Then
                            SelectedText.Name = Trim(RefName)
                            SelectedText.Trigger(q) = "9"
                            SelectedText.TriggerResult(q) = RxDiag + "-1/" + Rx + "-2/" + RxDose + "-3/" + RxFreq + "-4/" + RNote + "-5/" + Trim(str(RInvId1)) + "-6/" + Trim(str(RInvId2)) + "-7/"
                            SelectedActions(SelectedIndex) = SelectedText
                            SelectedIndex = GetNextAction
                            OrgSelectedIndex = SelectedIndex
                        End If
                    Else
                        If (frmAssignWear.GetCL(z1, Rx, RxDiag, RxDose, RxFreq, RInvId1, RInvId2, RNote)) Then
                            SelectedText.Name = Trim(RefName)
                            SelectedText.Trigger(q) = "9"
                            SelectedText.TriggerResult(q) = Rx + "-1/" + RxDiag + "-2/" + RxDose + "-3/" + RxFreq + "-4/" + RNote + "-5/" + Trim(str(RInvId1)) + "-6/" + Trim(str(RInvId2)) + "-7/"
                            SelectedActions(SelectedIndex) = SelectedText
                            SelectedIndex = GetNextAction
                            OrgSelectedIndex = SelectedIndex
                        End If
                    End If
                Next z1
            End If
            SelectedText.Name = ""
            OkayToPost = False
        End If
    ElseIf (Temp.Trigger(q) = "A") Then
        frmAppType.ApptTypeId = "0"
        frmAppType.ApptTypeName = ""
        If (frmAppType.LoadAppt("W", True)) Then
            frmAppType.Show 1
            If (frmAppType.ApptTypeId >= 0) Then
                f1 = 1
                f2 = InStrPS(f1, frmAppType.ApptTypeName, "/")
                
                While (f2 > 0)

                    Call InsertLog("FrmEvalution.frm", "Written Instruction " & CStr(frmAppType.ApptTypeName) & "", LoginCatg.OrderSet, "", "", 0, globalExam.iAppointmentId)

                    SelectedText.Trigger(q) = "A"
                    SelectedText.TriggerResult(q) = Trim(Mid(frmAppType.ApptTypeName, f1, f2 - (f1 - 1)))
                    SelectedActions(SelectedIndex) = SelectedText
                    f1 = f2 + 1
                    f2 = InStrPS(f1, frmAppType.ApptTypeName, "/")
                    If (f2 > 0) Then
                        SelectedIndex = GetNextAction
                        SelectedText = SelectedActions(SelectedIndex)
                        OrgSelectedIndex = SelectedIndex
                        SelectedText.Name = Trim(RefName)
                        f3 = InStrPS(RefName, "-")
                        If (f3 > 0) Then
                            SelectedText.Name = Trim(Left(RefName, f3 - 1))
                        End If
                    End If
                Wend
                If (f1 < Len(frmAppType.ApptTypeName)) Then
                    SelectedText.Trigger(q) = "A"
                    SelectedText.TriggerResult(q) = Trim(Mid(frmAppType.ApptTypeName, f1, Len(frmAppType.ApptTypeName) - (f1 - 1)))
                Else
                    OkayToPost = False
                End If
            Else
                OkayToPost = False
            End If
            If Not (OkayToPost) Then
                Exit For
            End If
        End If
    ElseIf (Temp.Trigger(q) = "T") Then
        frmAppType.ApptTypeId = ApptTypeId
        frmAppType.ApptTypeName = ""
        frmAppType.lblPatientName.Visible = True
        frmAppType.lblPatientName.Caption = lblName.Caption
        frmAppType.lblPatientAge.Visible = True
        frmAppType.lblPatientAge.Caption = lblPatientAge.Caption
        If (frmAppType.LoadAppt("T", True)) Then
            frmAppType.Show 1
            If (Trim(frmAppType.ApptTypeName) <> "") Then
                SelectedText.Trigger(q) = "T"
                SelectedText.TriggerResult(q) = Trim(frmAppType.ApptTypeName)
                Add1 = Trim(frmAppType.ApptTypeName)
                If (IsTestFollowup(Add1, "EXTERNAL TESTS")) Then
                    SelectedText.FollowUp = True
                End If
            Else
                OkayToPost = False
                Exit For
            End If
        End If
        frmAppType.lblPatientName.Visible = False
        frmAppType.lblPatientAge.Visible = False
    ElseIf (Temp.Trigger(q) = "B") Then
        frmAppType.ApptTypeId = ApptTypeId
        If (Trim(SelectedText.TriggerResult(q - 1)) <> "") Then
            frmAppType.ApptTypeName = Trim(SelectedText.TriggerResult(q - 1))
            If (frmAppType.LoadAppt("B", True)) Then
                frmAppType.Show 1
                If (Trim(frmAppType.ApptTypeName) <> "") Then
                    SelectedText.Trigger(q) = "B"
                    SelectedText.TriggerResult(q) = Trim(frmAppType.ApptTypeName)
                Else
                    OkayToPost = False
                    Exit For
                End If
            End If
        Else
            frmAppType.ApptTypeName = ""
            SelectedText.Trigger(q) = "B"
            SelectedText.TriggerResult(q) = Trim(frmAppType.ApptTypeName)
        End If
    ElseIf (Temp.Trigger(q) = "O") Then
        frmAppType.ApptTypeId = ApptTypeId
        frmAppType.ApptTypeName = ""
        If (frmAppType.LoadAppt("O", True)) Then
            frmAppType.Show 1
            If (Trim(frmAppType.ApptTypeName) <> "") Then
                SelectedText.Trigger(q) = "O"
                SelectedText.TriggerResult(q) = Trim(frmAppType.ApptTypeName)
                Add1 = Trim(frmAppType.ApptTypeName)
            Else
                OkayToPost = False
                Exit For
            End If
            End If
        ElseIf (Temp.Trigger(q) = "W") Then 'code is XXW
            Dim TextPlan As String
            TextPlan = GetButtonTextForPlan
            If (Trim(TextPlan) <> "") Then
                SelectedText.Trigger(q) = "W"
                SelectedText.TriggerResult(q) = Trim(TextPlan)
            Else
                OkayToPost = False
                Exit For
            End If
    ElseIf (Temp.Trigger(q) = "C") Or (Temp.Trigger(q) = "R") Then
        frmLetters.LetterPCP = ""
        frmLetters.LetterPCP = GetPCP(AppointmentId, RefId)
        frmLetters.LetterPCPId = RefId
        frmLetters.LetterRef = ""
        frmLetters.LetterRef = GetReferralDoctor(AppointmentId, RefId)
        frmLetters.LetterRefId = RefId
        If (frmLetters.LoadLetters(Temp.Trigger(q), True)) Then
            frmLetters.Show 1
            OkayToPost = False
            TempText = ""
            If (Trim(frmLetters.LetterName) <> "") Then
                OkayToPost = True
                SelectedText.Trigger(q) = Temp.Trigger(q)
                If (Trim(frmLetters.LetterDestinationCCpcp) <> "") Then
                    TempText = TempText + Trim(frmLetters.LetterDestinationCCpcp) + ","
                End If
                If (Trim(frmLetters.LetterDestinationCCref) <> "") Then
                    TempText = TempText + Trim(frmLetters.LetterDestinationCCref) + ","
                End If
                If (Trim(frmLetters.LetterDestinationCCoth) <> "") Then
                    TempText = TempText + Trim(frmLetters.LetterDestinationCCoth) + ","
                End If
                If (Trim(frmLetters.LetterDestinationCCpat) <> "") Then
                    TempText = TempText + "(P)"
                End If
                TempText = Trim(TempText)
                If (Trim(TempText) <> "") Then
                    If (Mid(TempText, Len(TempText), 1) = ",") Then
                        TempText = Left(TempText, Len(TempText) - 1)
                    End If
                End If
                SelectedText.TriggerResult(q) = Trim(frmLetters.LetterName) + "/" + Trim(frmLetters.LetterDestination) + "/" + TempText

                If Temp.Trigger(q) = "C" Then
                    Call InsertLog("FrmEvalution.frm", "Ordered Letter to " & CStr(frmLetters.LetterDestination) & " ", LoginCatg.OrderSet, "", "", 0, globalExam.iAppointmentId)
                End If
                If Temp.Trigger(q) = "R" Then
                    Call InsertLog("FrmEvalution.frm", "Referred Patient to Physician " & CStr(frmLetters.LetterDestination) & " ", LoginCatg.OrderSet, "", "", 0, globalExam.iAppointmentId)
                End If

            End If
            If Not (OkayToPost) Then
                Exit For
            End If
        End If
    ElseIf (Temp.Trigger(q) = "&") Then
        frmAppType.ApptTypeId = ApptTypeId
        frmAppType.ApptTypeName = ""
        If (frmAppType.LoadAppt("&", True)) Then
            frmAppType.Show 1
            If (Trim(frmAppType.ApptTypeName) <> "") Then
                SelectedText.Trigger(q) = "&"
                SelectedText.TriggerResult(q) = Trim(frmAppType.ApptTypeName)
            Else
                OkayToPost = False
                Exit For
            End If
        End If
    Else
        SelectedText.Trigger(q) = ""
        SelectedText.TriggerResult(q) = ""
    End If
Next q
If (OkayToPost) Then
    If (Trim(SelectedText.Name) <> "") And (SelectedIndex = OrgSelectedIndex) Then

    If (UCase(Trim(SelectedText.Name)) = "ORDER A TEST") Then
        Call InsertLog("FrmEvalution.frm", " Ordered Test " & CStr(frmAppType.ApptTypeName) & " ", LoginCatg.OrderSet, "", "", 0, globalExam.iAppointmentId)
    End If

        If (UCase(Trim(SelectedText.Name)) = "SCHEDULE SURGERY") Then
            Erase UTemp
            LocalSelected = SelectedText
            YTemp = SelectedText.TriggerResult(1)
            f1 = 1
            f2 = InStrPS(f1, YTemp, "/")
            If (f2 > 0) Then
                UTemp(1) = Mid(YTemp, f1, f2 - (f1 - 1))
                f1 = f2 + 1
                f2 = InStrPS(f1, YTemp, "/")
                If (f2 > 0) Then
                    UTemp(2) = Mid(YTemp, f1, f2 - (f1 - 1))
                    f1 = f2 + 1
                    f2 = InStrPS(f1, YTemp, "/")
                    If (f2 > 0) Then
                        UTemp(3) = Mid(YTemp, f1, f2 - (f1 - 1))
                        f1 = f2 + 1
                        f2 = InStrPS(f1, YTemp, "/")
                        If (f2 > 0) Then
                            UTemp(4) = Mid(YTemp, f1, f2 - (f1 - 1))
                            f1 = f2 + 1
                            f2 = InStrPS(f1, YTemp, "/")
                            If (f2 > 0) Then
                                UTemp(5) = Mid(YTemp, f1, f2 - (f1 - 1))
                            End If
                        End If
                    End If
                End If
            End If
            For f1 = 1 To 5
                If (Trim(UTemp(f1)) <> "") Then
                    SelectedText.Name = LocalSelected.Name
                    SelectedText.Highlight = LocalSelected.Highlight
                    SelectedText.FollowUp = LocalSelected.FollowUp
                    SelectedText.Trigger(1) = LocalSelected.Trigger(1)
                    SelectedText.TriggerResult(1) = UTemp(f1)
                    SelectedText.Trigger(2) = LocalSelected.Trigger(2)
                    SelectedText.TriggerResult(2) = LocalSelected.TriggerResult(2)
                    SelectedText.Trigger(3) = LocalSelected.Trigger(3)
                    SelectedText.TriggerResult(3) = LocalSelected.TriggerResult(3)
                    SelectedActions(SelectedIndex) = SelectedText
                    SelectedIndex = GetNextAction
                End If
            Next f1
        ElseIf (UCase(Trim(SelectedText.Name)) = "OFFICE PROCEDURES") Then
            Erase UTemp
            LocalSelected = SelectedText
            YTemp = SelectedText.TriggerResult(1)
            f1 = 1
            f2 = InStrPS(f1, YTemp, "/")
            If (f2 > 0) Then
                UTemp(1) = Mid(YTemp, f1, f2 - (f1 - 1))
                f1 = f2 + 1
                f2 = InStrPS(f1, YTemp, "/")
                If (f2 > 0) Then
                    UTemp(2) = Mid(YTemp, f1, f2 - (f1 - 1))
                    f1 = f2 + 1
                    f2 = InStrPS(f1, YTemp, "/")
                    If (f2 > 0) Then
                        UTemp(3) = Mid(YTemp, f1, f2 - (f1 - 1))
                        f1 = f2 + 1
                        f2 = InStrPS(f1, YTemp, "/")
                        If (f2 > 0) Then
                            UTemp(4) = Mid(YTemp, f1, f2 - (f1 - 1))
                            f1 = f2 + 1
                            f2 = InStrPS(f1, YTemp, "/")
                            If (f2 > 0) Then
                                UTemp(5) = Mid(YTemp, f1, f2 - (f1 - 1))
                            End If
                        End If
                    End If
                End If
            End If
            For f1 = 1 To 5
                If (Trim(UTemp(f1)) <> "") Then
                    SelectedText.Name = LocalSelected.Name
                    SelectedText.Highlight = LocalSelected.Highlight
                    SelectedText.FollowUp = LocalSelected.FollowUp
                    SelectedText.Trigger(1) = LocalSelected.Trigger(1)
                    SelectedText.TriggerResult(1) = UTemp(f1)
                    SelectedText.Trigger(2) = LocalSelected.Trigger(2)
                    SelectedText.TriggerResult(2) = LocalSelected.TriggerResult(2)
                    SelectedText.Trigger(3) = LocalSelected.Trigger(3)
                    SelectedText.TriggerResult(3) = LocalSelected.TriggerResult(3)
                    SelectedActions(SelectedIndex) = SelectedText
                    SelectedIndex = GetNextAction
                End If
            Next f1

            Call InsertLog("FrmEvalution.frm", " Ordered Office Procedure " & CStr(YTemp) & " ", LoginCatg.OrderSet, "", "", 0, globalExam.iAppointmentId)

        Else
            SelectedActions(SelectedIndex) = SelectedText
        End If
        If (SetApptOn) Then
            If (Trim(Add1) <> "") And (Trim(Add2) <> "") And (Trim(Add3) <> "") Then
                If (UCase(Trim(SelectedText.Name)) <> "OFFICE PROCEDURES") Then
                    SelectedIndex = GetNextAction
                End If
                SelectedText = SelectedActions(SelectedIndex)
                SelectedText.Name = "Schedule Appointment"
                SelectedText.Trigger(1) = "2"
                SelectedText.TriggerResult(1) = Add1
                If (InStrPS(Add1, "-f/") > 0) Then
                    SelectedText.TriggerResult(1) = Left(Add1, Len(Add1) - 3)
                End If
                SelectedText.Trigger(2) = "1"
                SelectedText.TriggerResult(2) = Add2
                SelectedText.Trigger(3) = "5"
                SelectedText.TriggerResult(3) = Add3
                SelectedActions(SelectedIndex) = SelectedText
            End If
        End If
    End If
End If
End Function

Private Function ClearTrigger(TheName As String, JustOne As Integer) As Boolean
Dim q As Integer
Dim i As Integer
Dim z1 As Integer
Dim StartItem As Integer
Dim EndItem As Integer
Dim MaxItem As Integer
StartItem = 1
EndItem = TotalActionEvaluation(True)
MaxItem = EndItem
If (JustOne > 0) Then
    StartItem = JustOne
    EndItem = JustOne
End If
For i = StartItem To EndItem
    If (UCase(Trim(TheName)) = UCase(Trim(SelectedActions(i).Name))) Then
        For z1 = i To MaxItem - 1
            SelectedActions(z1).Name = SelectedActions(z1 + 1).Name
            For q = 1 To 3
                SelectedActions(z1).Trigger(q) = SelectedActions(z1 + 1).Trigger(q)
                SelectedActions(z1).TriggerResult(q) = SelectedActions(z1 + 1).TriggerResult(q)
            Next q
        Next z1
        SelectedActions(MaxItem).Name = ""
        For q = 1 To 3
            SelectedActions(MaxItem).Trigger(q) = 0
            SelectedActions(MaxItem).TriggerResult(q) = ""
        Next q
        SelectedActions(MaxItem).Highlight = False
        SelectedActions(MaxItem).FollowUp = False
        If (StartItem = EndItem) Then
            Exit For
        End If
        EndItem = EndItem - 1
        MaxItem = MaxItem - 1
        If (MaxItem < 1) Then
            MaxItem = 1
            Exit For
        End If
        i = i - 1
    End If
Next i
End Function

Private Function ResetButton(TheName As String, Selected As Boolean) As Boolean
Dim BColor As Long
Dim TColor As Long
If Selected Then
    BColor = ButtonSelectionColor
    TColor = TextSelectionColor
Else
    BColor = BaseBackColor
    TColor = BaseTextColor
End If

If (UCase(Trim(TheName)) = UCase(Trim(cmdAction1.Text))) Then
    cmdAction1.BackColor = BColor
    cmdAction1.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction2.Text))) Then
    cmdAction2.BackColor = BColor
    cmdAction2.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction3.Text))) Then
    cmdAction3.BackColor = BColor
    cmdAction3.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction4.Text))) Then
    cmdAction4.BackColor = BColor
    cmdAction4.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction5.Text))) Then
    cmdAction5.BackColor = BColor
    cmdAction5.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction6.Text))) Then
    cmdAction6.BackColor = BColor
    cmdAction6.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction7.Text))) Then
    cmdAction7.BackColor = BColor
    cmdAction7.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction8.Text))) Then
    cmdAction8.BackColor = BColor
    cmdAction8.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction9.Text))) Then
    cmdAction9.BackColor = BColor
    cmdAction9.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction10.Text))) Then
    cmdAction10.BackColor = BColor
    cmdAction10.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction11.Text))) Then
    cmdAction11.BackColor = BColor
    cmdAction11.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction12.Text))) Then
    cmdAction12.BackColor = BColor
    cmdAction12.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction13.Text))) Then
    cmdAction13.BackColor = BColor
    cmdAction13.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction14.Text))) Then
    cmdAction14.BackColor = BColor
    cmdAction14.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction15.Text))) Then
    cmdAction15.BackColor = BColor
    cmdAction15.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction16.Text))) Then
    cmdAction16.BackColor = BColor
    cmdAction16.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction17.Text))) Then
    cmdAction17.BackColor = BColor
    cmdAction17.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction18.Text))) Then
    cmdAction18.BackColor = BColor
    cmdAction18.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction19.Text))) Then
    cmdAction19.BackColor = BColor
    cmdAction19.ForeColor = TColor
ElseIf (UCase(Trim(TheName)) = UCase(Trim(cmdAction20.Text))) Then
    cmdAction20.BackColor = BColor
    cmdAction20.ForeColor = TColor
End If
End Function

Private Function GetNextAction() As Integer
GetNextAction = TotalActionEvaluation(True)
GetNextAction = GetNextAction + 1
If (GetNextAction > MaxActions) Then
    GetNextAction = 0
Else
'Ensures that it is clean upon selection
    SelectedActions(GetNextAction).FollowUp = False
    SelectedActions(GetNextAction).Highlight = False
    SelectedActions(GetNextAction).Name = ""
    SelectedActions(GetNextAction).Trigger(1) = ""
    SelectedActions(GetNextAction).Trigger(2) = ""
    SelectedActions(GetNextAction).Trigger(3) = ""
    SelectedActions(GetNextAction).TriggerResult(1) = ""
    SelectedActions(GetNextAction).TriggerResult(2) = ""
    SelectedActions(GetNextAction).TriggerResult(3) = ""
    SelectedActions(GetNextAction).SpecCLRx = ""
    SelectedActions(GetNextAction).Comments(0) = ""
    SelectedActions(GetNextAction).Comments(1) = ""
End If
End Function

Private Function LoadReview(TheName As String, TheTag As String) As Boolean
Dim q As Integer, j As Integer, k As Integer
Dim Rx As String, Fq As String, Dt As String
Dim Ref As String, Temp As String
Dim AHigh As Boolean, RetFollow As Boolean
lstReview.Visible = False
lstReview.Clear
q = 1
lstReview.Tag = "A" + Trim(TheTag)
If (InStrPS(UCase(Left(TheName, 2)), "RX") > 0) Then
    lstReview.AddItem "Close Review List (RX)"
Else
    lstReview.AddItem "Close Review List"
End If
Dim SpecCLRx As String
While (GetActionEvaluation(q, Ref, AHigh, RetFollow, SpecCLRx)) And (q < MaxActions)
    Dt = Space(240)
    j = InStrPS(Ref, "-")
    If (j > 0) Then
        Temp = Trim(Left(Ref, j - 1))
    Else
        Temp = Ref
    End If
    If (Trim(Left(TheName, Len(Temp))) = Temp) Then
        If (InStrPS(UCase(Left(TheName, 2)), "RX") > 0) Then
            j = InStrPS(Ref, "-1/")
            k = InStrPS(Ref, "-2/")
            If (j > 0) Then
                Dt = Space(240)
                Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
                Mid(Dt, 1, Len(Rx) + 5) = "Rx - " + Rx
                lstReview.AddItem Dt + Trim(str(q))
                j = InStrPS(Ref, "-3/")
                If (j > 0) Then
                    Dt = Space(240)
                    Rx = Mid(Ref, k + 3, (j - 1) - (k + 2))
                    Mid(Dt, 5, Len(Rx)) = Rx
                    lstReview.AddItem Dt + Trim(str(q))
                    k = InStrPS(Ref, "-4/")
                    If (k > 0) Then
                        Dt = Space(240)
                        Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
                        Mid(Dt, 5, Len(Rx)) = Rx
                        lstReview.AddItem Dt + Trim(str(q))
                        j = InStrPS(Ref, "-5/")
                        If (j > 0) Then
                            Dt = Space(240)
                            Rx = Mid(Ref, k + 3, (j - 1) - (k + 2))
                            Mid(Dt, 5, Len(Rx)) = Rx
                            lstReview.AddItem Dt + Trim(str(q))
                            k = InStrPS(Ref, "-6/")
                            If (k > 0) Then
                                Dt = Space(240)
                                Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
                                Mid(Dt, 5, Len(Rx)) = Rx
                                lstReview.AddItem Dt + Trim(str(q))
                            End If
                        End If
                    End If
                End If
            Else
                Mid(Dt, 1, Len(Trim(Ref))) = Trim(Ref)
                lstReview.AddItem Dt + Trim(str(q))
            End If
        Else
            If SpecCLRx = "" Then
                Call ReplaceCharacters(Ref, "-/", "-0/")
                Mid(Dt, 1, Len(Trim(Ref))) = Trim(Ref)
                lstReview.AddItem Dt + Trim(str(q))
            End If
        End If
    End If
    q = q + 1
Wend
If (InStrPS(UCase(Left(TheName, 2)), "RX") > 0) Then
        If Not UserLogin.HasPermission(epMeds) Then
                Exit Function
        End If
    Call LoadPreviousRx(PatientId, lstReview, False)
End If
End Function

Private Function TotalSpecificActions(AType As String) As Integer
Dim i As Integer
TotalSpecificActions = 0
For i = 1 To MaxActions
    If (UCase(Trim(SelectedActions(i).Name)) = UCase(Trim(AType))) Then
        TotalSpecificActions = TotalSpecificActions + 1
    End If
Next i
End Function

Private Function LoadPreviousRx(PatId As Long, AList As ListBox, Init As Boolean) As Boolean
Dim i As Integer
Dim k As Integer
Dim j As Integer
Dim Temp As String
Dim TheDate As String
Dim TheStat As String
Dim DisplayText As String
Dim RetCln As PatientClinical
Dim RetAppt As SchedulerAppointment
LoadPreviousRx = False
If (Init) Then
    AList.Clear
End If
If (PatId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.PatientId = PatId
    RetCln.AppointmentId = 0
    RetCln.ClinicalType = "A"
    RetCln.EyeContext = ""
    RetCln.Findings = ""
    RetCln.Symptom = ""
    RetCln.ImageDescriptor = ""
    RetCln.ImageInstructions = ""
    If (RetCln.FindPatientClinical > 0) Then
        i = 1
        While (RetCln.SelectPatientClinical(i))
            If (Left(RetCln.Findings, 2) = "RX") Then
                k = InStrPS(RetCln.Findings, "-8/")
                If (k <= 0) Then
                    k = InStrPS(RetCln.Findings, "-2/")
                End If
                If (k > 0) Then
                    TheDate = Trim(Mid(RetCln.Findings, 3, (k - 1) - 2))
                    If (Trim(TheDate) = "") Then
                        Set RetAppt = New SchedulerAppointment
                        RetAppt.AppointmentId = RetCln.AppointmentId
                        If (RetAppt.RetrieveSchedulerAppointment) Then
                            TheDate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
                        End If
                        Set RetAppt = Nothing
                    End If
                    TheStat = "Active"
                    If (Left(RetCln.ImageDescriptor, 1) = "R") Then
                        TheStat = "Refilled On " + Trim(Mid(RetCln.ImageDescriptor, 2, 10))
                    ElseIf (Left(RetCln.ImageDescriptor, 1) = "D") Then
                        TheStat = "DisContd On " + Trim(Mid(RetCln.ImageDescriptor, 2, 10))
                    ElseIf (Left(RetCln.ImageDescriptor, 1) = "C") Then
                        TheStat = "Changed On  " + Trim(Mid(RetCln.ImageDescriptor, 2, 10))
                    End If
                    k = InStrPS(RetCln.Findings, "-1/")
                    j = InStrPS(RetCln.Findings, "-2/")
                    If (k >= j) Then
                        k = InStrPS(RetCln.Findings, "-8/")
                    End If
                    If (j > 0) Then
                        Temp = Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                        Temp = "Rx - (P) " + Temp + " On " + TheDate + " Status is " + TheStat
                        DisplayText = Space(240)
                        Mid(DisplayText, 1, Len(Temp)) = Temp
                        Mid(DisplayText, 225, 10) = Trim(str(RetCln.ClinicalId))
                        AList.AddItem DisplayText
                        k = InStrPS(RetCln.Findings, "-2/")
                        j = InStrPS(RetCln.Findings, "-3/")
                        Temp = Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                        DisplayText = Space(240)
                        Mid(DisplayText, 1, Len(Temp) + 5) = Space(5) + Temp
                        Mid(DisplayText, 225, 10) = Trim(str(RetCln.ClinicalId))
                        AList.AddItem DisplayText
                        k = InStrPS(RetCln.Findings, "-3/")
                        j = InStrPS(RetCln.Findings, "-4/")
                        Temp = Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                        DisplayText = Space(240)
                        Mid(DisplayText, 1, Len(Temp) + 5) = Space(5) + Temp
                        Mid(DisplayText, 225, 10) = Trim(str(RetCln.ClinicalId))
                        AList.AddItem DisplayText
                        k = InStrPS(RetCln.Findings, "-4/")
                        j = InStrPS(RetCln.Findings, "-5/")
                        Temp = Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                        DisplayText = Space(240)
                        Mid(DisplayText, 1, Len(Temp) + 5) = Space(5) + Temp
                        Mid(DisplayText, 225, 10) = Trim(str(RetCln.ClinicalId))
                        AList.AddItem DisplayText
                        k = InStrPS(RetCln.Findings, "-5/")
                        j = InStrPS(RetCln.Findings, "-6/")
                        If (j < k) Then
                            j = Len(RetCln.Findings) + 1
                        End If
                        Temp = Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                        DisplayText = Space(240)
                        Mid(DisplayText, 1, Len(Temp) + 5) = Space(5) + Temp
                        Mid(DisplayText, 225, 10) = Trim(str(RetCln.ClinicalId))
                        AList.AddItem DisplayText
                    End If
                End If
            End If
            i = i + 1
        Wend
    End If
    Set RetCln = Nothing
End If
If (AList.ListCount > 2) Then
    LoadPreviousRx = True
End If
End Function

Private Function SetRxStatus(ClnId As Long, IType As String, TheDate As String) As Boolean
Dim RetCln As PatientClinical
SetRxStatus = False
If (ClnId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        RetCln.ImageDescriptor = IType + TheDate
        Call RetCln.ApplyPatientClinical
        SetRxStatus = True
    End If
    Set RetCln = Nothing
End If
End Function

Private Function AnyRxChanges(PatId As Long) As Boolean
Dim oDate As New CIODateTime
Dim oClinical As New PatientClinical
    If PatId > 0 Then
        oClinical.PatientId = PatId
        oClinical.AppointmentId = 0
        oClinical.Symptom = ""
        oClinical.Findings = ""
        oClinical.ClinicalType = "A"
        oClinical.EyeContext = ""
        oClinical.ImageInstructions = ""
        oClinical.ImageDescriptor = "R" & oDate.GetDisplayDate(False)
        If oClinical.FindRxClinical > 0 Then
            AnyRxChanges = True
        End If
        If Not AnyRxChanges Then
            oClinical.PatientId = PatId
            oClinical.AppointmentId = 0
            oClinical.Symptom = ""
            oClinical.Findings = ""
            oClinical.ClinicalType = "A"
            oClinical.EyeContext = ""
            oClinical.ImageInstructions = ""
            oClinical.ImageDescriptor = "D" & oDate.GetDisplayDate(False)
            If oClinical.FindRxClinical > 0 Then
                AnyRxChanges = True
            End If
        End If
    End If
    Set oClinical = Nothing
    Set oDate = Nothing
End Function

Private Function DeleteRxNote(ApptId As Long, PatId As Long, TheRx As String) As Boolean
Dim i As Integer
Dim RetNote As PatientNotes
DeleteRxNote = False
If (ApptId > 0) And (PatId > 0) And (Trim(TheRx) <> "") And (Len(Trim(TheRx)) > 6) Then
    DeleteRxNote = True
    Set RetNote = New PatientNotes
    RetNote.NotesAppointmentId = ApptId
    RetNote.NotesPatientId = PatId
    RetNote.NotesType = "R"
    RetNote.NotesSystem = Trim(Mid(TheRx, 6, Len(TheRx) - 5))
    If (RetNote.FindNotesbyPatient > 0) Then
        i = 1
        While (RetNote.SelectNotes(i))
            Call RetNote.DeleteNotes
            i = i + 1
        Wend
    End If
    Set RetNote = Nothing
End If
End Function

Public Function AddAnAction(TheAction As String, Trig As String, TheName As String) As Boolean
Dim ClearDiag As Integer
If (Trim(TheAction) <> "") Then
    ClearDiag = GetNextAction
    If (ClearDiag > 0) Then
        SelectedActions(ClearDiag).Name = TheName
        SelectedActions(ClearDiag).Trigger(1) = Trig
        SelectedActions(ClearDiag).TriggerResult(1) = TheAction
    End If
End If
End Function

Public Function DeleteAnAction(RefId As Long) As Boolean
Dim q As Integer
Dim z As Long
Dim EndItem As Integer
Dim ClearDiag As Integer
If (RefId > 0) Then
    EndItem = TotalActionEvaluation(True)
    If (RefId > EndItem) Then
        RefId = RefId - 1
    End If
    For z = RefId To EndItem
        If (z <> EndItem) Then
            SelectedActions(z).Name = SelectedActions(z + 1).Name
            SelectedActions(z).SpecCLRx = SelectedActions(z + 1).SpecCLRx
            For q = 1 To 3
                SelectedActions(z).Trigger(q) = SelectedActions(z + 1).Trigger(q)
                SelectedActions(z).TriggerResult(q) = SelectedActions(z + 1).TriggerResult(q)
            Next q
        Else
            SelectedActions(z).Name = ""
            SelectedActions(z).SpecCLRx = ""
            For q = 1 To 3
                SelectedActions(z).Trigger(q) = ""
                SelectedActions(z).TriggerResult(q) = ""
            Next q
        End If
    Next z
End If
End Function

Public Function UpdateActionStatus(RefId As Integer, NewStatus As String) As Boolean
Dim delim As Integer
If (RefId > 0) Then
    delim = InStrPS(1, SelectedActions(RefId).TriggerResult(1), "-9/")
    Mid(SelectedActions(RefId).TriggerResult(1), delim - 1, 1) = NewStatus
End If
End Function


Public Function SetActionHighlight(Ref As Integer, IHow As Boolean) As Boolean
SetActionHighlight = True
If (Ref > 0) And (Ref <= MaxActions) Then
    If (Trim(SelectedActions(Ref).Name) <> "") Then
        SelectedActions(Ref).Highlight = IHow
    End If
End If
End Function

Private Function VerifyButtons() As Boolean
VerifyButtons = True
If (cmdAction1.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction1.Text)) Then
        cmdAction1.BackColor = ButtonSelectionColor
        cmdAction1.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction2.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction2.Text)) Then
        cmdAction2.BackColor = ButtonSelectionColor
        cmdAction2.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction3.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction3.Text)) Then
        cmdAction3.BackColor = ButtonSelectionColor
        cmdAction3.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction4.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction4.Text)) Then
        cmdAction4.BackColor = ButtonSelectionColor
        cmdAction4.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction5.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction5.Text)) Then
        cmdAction5.BackColor = ButtonSelectionColor
        cmdAction5.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction6.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction6.Text)) Then
        cmdAction6.BackColor = ButtonSelectionColor
        cmdAction6.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction7.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction7.Text)) Then
        cmdAction7.BackColor = ButtonSelectionColor
        cmdAction7.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction8.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction8.Text)) Then
        cmdAction8.BackColor = ButtonSelectionColor
        cmdAction8.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction9.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction9.Text)) Then
        cmdAction9.BackColor = ButtonSelectionColor
        cmdAction9.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction10.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction10.Text)) Then
        cmdAction10.BackColor = ButtonSelectionColor
        cmdAction10.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction11.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction11.Text)) Then
        cmdAction11.BackColor = ButtonSelectionColor
        cmdAction11.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction12.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction12.Text)) Then
        cmdAction12.BackColor = ButtonSelectionColor
        cmdAction12.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction13.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction13.Text)) Then
        cmdAction13.BackColor = ButtonSelectionColor
        cmdAction13.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction14.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction14.Text)) Then
        cmdAction14.BackColor = ButtonSelectionColor
        cmdAction14.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction15.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction15.Text)) Then
        cmdAction15.BackColor = ButtonSelectionColor
        cmdAction15.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction16.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction16.Text)) Then
        cmdAction16.BackColor = ButtonSelectionColor
        cmdAction16.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction17.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction17.Text)) Then
        cmdAction17.BackColor = ButtonSelectionColor
        cmdAction17.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction18.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction18.Text)) Then
        cmdAction18.BackColor = ButtonSelectionColor
        cmdAction18.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction19.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction19.Text)) Then
        cmdAction19.BackColor = ButtonSelectionColor
        cmdAction19.ForeColor = TextSelectionColor
    End If
End If
If (cmdAction20.BackColor = BaseBackColor) Then
    If (IsActionSet(cmdAction20.Text)) Then
        cmdAction20.BackColor = ButtonSelectionColor
        cmdAction20.ForeColor = TextSelectionColor
    End If
End If
End Function

Private Function IsTestFollowup(AName As String, MyType As String) As Boolean
Dim i As Integer
Dim MyTemp As String
Dim RetCode As PracticeCodes
IsTestFollowup = False
If (Trim(AName) <> "") And (Trim(MyType) <> "") Then
    If (InStrPS(AName, "/") > 0) Then
        MyTemp = UCase(Left(AName, Len(AName) - 3))
        Call ReplaceCharacters(MyTemp, "(OS)", "~")
        Call StripCharacters(MyTemp, "~")
        Call ReplaceCharacters(MyTemp, "(OS)", "~")
        Call StripCharacters(MyTemp, "~")
        Call ReplaceCharacters(MyTemp, "(OU)", "~")
        Call StripCharacters(MyTemp, "~")
        Call ReplaceCharacters(MyTemp, "(NA)", "~")
        Call StripCharacters(MyTemp, "~")
        MyTemp = Trim(MyTemp)
    Else
        MyTemp = UCase(AName)
    End If
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = MyType
    If (RetCode.FindCode > 0) Then
        i = 1
        Do Until Not (RetCode.SelectCode(i))
            If (InStrPS(UCase(RetCode.ReferenceCode), MyTemp) <> 0) Then
                If (Trim(RetCode.FollowUp) = "F") Then
                    IsTestFollowup = True
                End If
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetCode = Nothing
End If
End Function

Private Function IsTestApptTypeSet(ARef As String, AName As String, AType As String) As Boolean
Dim i As Integer
Dim RetCode As PracticeCodes
IsTestApptTypeSet = False
AType = ""
If (Trim(ARef) <> "") And (Trim(AName) <> "") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = UCase(Trim(ARef))
    If (RetCode.FindCode > 0) Then
        i = 1
        Do Until Not (RetCode.SelectCode(i))
            If (InStrPS(RetCode.ReferenceCode, AName) <> 0) Then
                If (Trim(RetCode.ReferenceAlternateCode) <> "") Then
                    IsTestApptTypeSet = True
                    AType = Trim(RetCode.ReferenceAlternateCode)
                    If (Len(AType) < 2) Then
                        IsTestApptTypeSet = False
                        AType = ""
                    End If
                End If
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetCode = Nothing
End If
End Function

Private Sub cmdSumm_Click()
frmFinalNew.PatientId = PatientId
frmFinalNew.AppointmentId = AppointmentId
frmFinalNew.ActivityId = globalExam.iActivityId
If (frmFinalNew.LoadFinal(True)) Then
    frmFinalNew.Show 1
End If
End Sub

Private Sub ProcessAction(ABtn As fpBtn)
Dim Index As Integer
Dim ClearDiag As Integer
Index = Val(Trim(ABtn.Tag))
ClearDiag = GetNextAction
If (ClearDiag > 0) Then
    Call ProcessTrigger(ABtn.Text, ClearDiag, Index)
    If Not (IsActionSet(ABtn.Text)) Then
        Call ClearTrigger(ABtn.Text, 0)
        Call ResetButton(ABtn.Text, False)
    Else
        ABtn.BackColor = ButtonSelectionColor
        ABtn.ForeColor = TextSelectionColor
        Call VerifyButtons
    End If
Else
    frmEventMsgs.Header = "Plan elements are at a maximium."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub
Public Function FrmClose()
If cmdDone.Text = "Close Chart" Then
Call cmdDone_Click
End If
 'Call cmdHome_Click
 Unload Me
 End Function
Private Function PrepareDiagnosesLoadArguments(ByVal DiagCode As String, ByVal DiagDescription As String, _
                                               ByVal EyeContext As String, ByVal EyeLidLocation As String) As Variant()
On Error GoTo lPrepareDiagnosesLoadArguments_Error

Dim i As Integer
Dim j As Integer
Dim DiagnosisLoadArgument As Variant
Dim AddItemArgs() As Variant
Dim Quant As String

Quant = EyeLidLocation

Dim StandardExamLoadArgsComWrapper As New comWrapper
StandardExamLoadArgsComWrapper.RethrowExceptions = False
Call StandardExamLoadArgsComWrapper.Create(StandardExamDiagnosesLoadArgumentsType, emptyArgs)
Set DiagnosisLoadArgument = StandardExamLoadArgsComWrapper.Instance
DiagnosisLoadArgument.DiagnosisCode = DiagCode
DiagnosisLoadArgument.DiagnosisDescription = DiagDescription
DiagnosisLoadArgument.EyeContext = EyeContext
DiagnosisLoadArgument.EyeLidLocation = Quant

PrepareDiagnosesLoadArguments = Array(DiagnosisLoadArgument)
Exit Function

lPrepareDiagnosesLoadArguments_Error:
    PrepareDiagnosesLoadArguments = Array(Empty)
End Function

Private Function CheckQuantifierExists(ByVal SelectedQuantifiers As String) As String
On Error GoTo lCheckQuantifierExists_Error
Dim StrSql As String
Dim RS As Recordset
Dim i, j As Integer
Dim arrQuantifiers() As String
CheckQuantifierExists = ""

If (Trim$(SelectedQuantifiers) <> "") Then
    arrQuantifiers = Split(SelectedQuantifiers, ",")
    For j = 0 To UBound(arrQuantifiers)
        If (Trim$(arrQuantifiers(j)) <> "") Then
            i = InStrPS(arrQuantifiers(j), "[")
            If (i > 0) Then arrQuantifiers(j) = Mid(arrQuantifiers(j), i + 1, Len(arrQuantifiers(j)))
            i = InStrPS(arrQuantifiers(j), "]")
            If (i > 0) Then arrQuantifiers(j) = Mid(arrQuantifiers(j), 1, i - 1)
            
            arrQuantifiers(j) = Trim$(arrQuantifiers(j))
            
            StrSql = " Select * FROM PracticeCodeTable WHERE ReferenceType = 'QUANTIFIERSLIDS' AND AlternateCode = '" & arrQuantifiers(j) & "' ORDER BY ReferenceType ASC, Rank ASC, Code ASC "
            Set RS = GetRS(StrSql)
            If Not RS Is Nothing Then
                If RS.RecordCount > 0 Then
                    If (Trim$(CheckQuantifierExists) <> "") Then
                        CheckQuantifierExists = CheckQuantifierExists + arrQuantifiers(j) + ","
                    Else
                        CheckQuantifierExists = arrQuantifiers(j) + ","
                    End If
                End If
            End If
        End If
    Next
    i = 0
    i = InStrRev(CheckQuantifierExists, ",")
    If (i > 0) Then CheckQuantifierExists = Mid$(CheckQuantifierExists, 1, i - 1)
End If
CheckQuantifierExists = Trim$(CheckQuantifierExists)
Exit Function
lCheckQuantifierExists_Error:
    CheckQuantifierExists = ""
End Function
