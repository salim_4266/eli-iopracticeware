VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmSetDiagnosis 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   FillColor       =   &H0077742D&
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "SetDiagnosis.frx":0000
   ScaleHeight     =   9000
   ScaleMode       =   0  'User
   ScaleWidth      =   11979
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtImpr 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   735
      Left            =   4080
      MultiLine       =   -1  'True
      TabIndex        =   7
      Top             =   6960
      Width           =   3855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReviewSystems 
      Height          =   975
      Left            =   9000
      TabIndex        =   1
      Top             =   7920
      Width           =   1365
      _Version        =   131072
      _ExtentX        =   2408
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   2
      Top             =   7920
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":3903
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   975
      Left            =   1560
      TabIndex        =   3
      Top             =   7920
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":3B36
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10440
      TabIndex        =   4
      Top             =   7920
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":3D6A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Visible         =   0   'False
      Width           =   1853
      _Version        =   131072
      _ExtentX        =   3268
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":3F9D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreOD 
      Height          =   1095
      Left            =   2160
      TabIndex        =   5
      Top             =   6720
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":4184
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCheckOut 
      Height          =   975
      Left            =   7440
      TabIndex        =   6
      Top             =   7920
      Width           =   1485
      _Version        =   131072
      _ExtentX        =   2619
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":4372
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSumm 
      Height          =   975
      Left            =   2880
      TabIndex        =   8
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":45B2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPlan 
      Height          =   975
      Left            =   6000
      TabIndex        =   9
      Top             =   7920
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":47E8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHigh 
      Height          =   975
      Left            =   4440
      TabIndex        =   12
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":4A1B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreOS 
      Height          =   1095
      Left            =   8160
      TabIndex        =   15
      Top             =   6720
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":4C53
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOrderOD 
      Height          =   975
      Left            =   120
      TabIndex        =   16
      Top             =   6720
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":4E41
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOrderOS 
      Height          =   975
      Left            =   10320
      TabIndex        =   17
      Top             =   6720
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":5084
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   1
      Left            =   120
      TabIndex        =   18
      Top             =   1680
      Visible         =   0   'False
      Width           =   1853
      _Version        =   131072
      _ExtentX        =   3268
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":52C7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   2
      Left            =   120
      TabIndex        =   19
      Top             =   2880
      Visible         =   0   'False
      Width           =   1853
      _Version        =   131072
      _ExtentX        =   3268
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":54AE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   3
      Left            =   120
      TabIndex        =   20
      Top             =   4080
      Visible         =   0   'False
      Width           =   1853
      _Version        =   131072
      _ExtentX        =   3268
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":5695
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   4
      Left            =   120
      TabIndex        =   21
      Top             =   5280
      Visible         =   0   'False
      Width           =   1853
      _Version        =   131072
      _ExtentX        =   3268
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":587C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   5
      Left            =   2040
      TabIndex        =   22
      Top             =   480
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":5A63
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   6
      Left            =   2040
      TabIndex        =   23
      Top             =   1680
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":5C4A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   7
      Left            =   2040
      TabIndex        =   24
      Top             =   2880
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":5E31
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   8
      Left            =   2040
      TabIndex        =   25
      Top             =   4080
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":6018
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   9
      Left            =   2040
      TabIndex        =   26
      Top             =   5280
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":61FF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   10
      Left            =   3960
      TabIndex        =   27
      Top             =   480
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":63E6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   11
      Left            =   3960
      TabIndex        =   28
      Top             =   1680
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":65CD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   12
      Left            =   3960
      TabIndex        =   29
      Top             =   2880
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":67B4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   13
      Left            =   3960
      TabIndex        =   30
      Top             =   4080
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":699B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOD 
      Height          =   1095
      Index           =   14
      Left            =   3960
      TabIndex        =   31
      Top             =   5280
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":6B82
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   0
      Left            =   6240
      TabIndex        =   32
      Top             =   480
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":6D69
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   1
      Left            =   6240
      TabIndex        =   33
      Top             =   1680
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":6F50
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   2
      Left            =   6240
      TabIndex        =   34
      Top             =   2880
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":7137
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   3
      Left            =   6240
      TabIndex        =   35
      Top             =   4080
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":731E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   4
      Left            =   6240
      TabIndex        =   36
      Top             =   5280
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":7505
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   5
      Left            =   8160
      TabIndex        =   37
      Top             =   480
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":76EC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   6
      Left            =   8160
      TabIndex        =   38
      Top             =   1680
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":78D3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   7
      Left            =   8160
      TabIndex        =   39
      Top             =   2880
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":7ABA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   8
      Left            =   8160
      TabIndex        =   40
      Top             =   4080
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":7CA1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   9
      Left            =   8160
      TabIndex        =   41
      Top             =   5280
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":7E88
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   10
      Left            =   10080
      TabIndex        =   42
      Top             =   480
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":806F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   11
      Left            =   10080
      TabIndex        =   43
      Top             =   1680
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":8256
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   12
      Left            =   10080
      TabIndex        =   44
      Top             =   2880
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":843D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   13
      Left            =   10080
      TabIndex        =   45
      Top             =   4080
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":8624
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpressionOS 
      Height          =   1095
      Index           =   14
      Left            =   10080
      TabIndex        =   46
      Top             =   5280
      Visible         =   0   'False
      Width           =   1860
      _Version        =   131072
      _ExtentX        =   3281
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDiagnosis.frx":880B
   End
   Begin VB.Label AutoOrderingOff 
      Alignment       =   2  'Center
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Auto-Ordering OFF"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   5040
      TabIndex        =   47
      Top             =   6480
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label lblOS 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "OS Impressions"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   6240
      TabIndex        =   14
      Top             =   120
      Width           =   2070
   End
   Begin VB.Label lblOD 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "OD Impressions"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   2085
   End
   Begin VB.Label lblPatName 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Pat Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   2640
      TabIndex        =   11
      Top             =   120
      Width           =   1005
   End
   Begin VB.Label lblPatType 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Pat Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   8760
      TabIndex        =   10
      Top             =   120
      Width           =   930
   End
End
Attribute VB_Name = "frmSetDiagnosis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' REFACTOR: I have CountTotalImpressions calls everywhere in this form.  As long as we keep smart track of the totals we shouldn't have to count all the time.
Public PatientId As Long
Public AppointmentId As Long
Public NavigationAction As Long
Public EditPreviousOn As Boolean

Private Const CntRef As String = "NF-"
Private Const CntRefSet As String = "!"
Private Const RORef As String = "RO-"
Private Const RORefSet As String = "#"
Private Const HORef As String = "HO-"
Private Const HORefSet As String = "%"
Private Const MaxImpressions As Integer = 100

Private SelectedImpressionsOU As CImpression
Private SelectedImpressionsOD(MaxImpressions) As New CImpression
Private SelectedImpressionsOS(MaxImpressions) As New CImpression
Private SelectedImpressionsODTemp(MaxImpressions) As New CImpression
Private SelectedImpressionsOSTemp(MaxImpressions) As New CImpression
' These are the impressions with active = true
Private ActiveImpressionsOD As Long
Private ActiveImpressionsOS As Long
' These are the total impressions
Public TotalImpressionsOD As Long
Public TotalImpressionsOS As Long

Private OrderingODOn As Boolean
Private OrderingOSOn As Boolean
Private ViewOn As Boolean
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long
Private fIsModified As Boolean
Private iODPageCount As Long
Private iOSPageCount As Long

Private Const MAX_OD_BUTTONS As Long = 15
Private Const MAX_OS_BUTTONS As Long = 15

Private Sub cmdCheckOut_Click()
    On Error GoTo lcmdCheckOut_Click_Error

frmEventMsgs.SetButtons "Express Check-Out WILL CAUSE WORKFLOW PROBLEMS. Are you sure ?", "", "Check-Out", "Cancel"
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    NavigationAction = 90
    Unload frmSetDiagnosis
End If

    Exit Sub

lcmdCheckOut_Click_Error:

    LogError "frmSetDiagnosis", "cmdCheckOut_Click", Err, Err.Description
End Sub

Private Sub cmdHigh_Click()
    On Error GoTo lcmdHigh_Click_Error

    cmdHigh.Enabled = False
    frmHighlights.HighlightLoadOn = frmSystems1.IsHighlightLoadOn
    If frmHighlights.LoadHighlights(PatientId, True, True, False) Then
        frmHighlights.Show 1
        frmSystems1.SetHighlightLoad frmHighlights.HighlightLoadOn
    End If
    cmdHigh.Enabled = True

    Exit Sub

lcmdHigh_Click_Error:

    LogError "frmSetDiagnosis", "cmdHigh_Click", Err, Err.Description, , PatientId
End Sub

Private Sub cmdImpressionOD_Click(Index As Integer)
    On Error GoTo lcmdImpressionOD_Click_Error

    TriggerImpression Index, "OD"

    Exit Sub

lcmdImpressionOD_Click_Error:

    LogError "frmSetDiagnosis", "cmdImpressionOD_Click", Err, Err.Description
End Sub

Private Sub cmdImpressionOS_Click(Index As Integer)
    On Error GoTo lcmdImpressionOS_Click_Error

    TriggerImpression Index, "OS"

    Exit Sub

lcmdImpressionOS_Click_Error:

    LogError "frmSetDiagnosis", "cmdImpressionOS_Click", Err, Err.Description
End Sub

Private Sub cmdHome_Click()
    On Error GoTo lcmdHome_Click_Error

    NavigationAction = EFormResults.efrHome
    Unload frmSetDiagnosis

    Exit Sub

lcmdHome_Click_Error:

    LogError "frmSetDiagnosis", "cmdHome_Click", Err, Err.Description
End Sub

Private Sub cmdDone_Click()
    On Error GoTo lcmdDone_Click_Error

    If OrderingODOn Then
        Call cmdOrderOD_Click
    End If
    If OrderingOSOn Then
        Call cmdOrderOS_Click
    End If
    NavigationAction = EFormResults.efrDone
    If EditPreviousOn Then
        If Not fIsModified Then
            NavigationAction = EFormResults.efrHome
        End If
    End If
    Unload frmSetDiagnosis

    Exit Sub

lcmdDone_Click_Error:

    LogError "frmSetDiagnosis", "cmdDone_Click", Err, Err.Description
End Sub

Private Sub cmdMoreOD_Click()
    On Error GoTo lcmdMoreOD_Click_Error

    If (iODPageCount + 1) * MAX_OD_BUTTONS >= TotalImpressionsOD Then
        iODPageCount = 0
    Else
        iODPageCount = iODPageCount + 1
    End If
    ClearImpressionsButtons EEyeRef.eerOD
    DisplayImpressionsButtons EEyeRef.eerOD, iODPageCount * MAX_OD_BUTTONS

    Exit Sub

lcmdMoreOD_Click_Error:

    LogError "frmSetDiagnosis", "cmdMoreOD_Click", Err, Err.Description
End Sub

Private Sub cmdMoreOS_Click()
    On Error GoTo lcmdMoreOS_Click_Error

    If (iOSPageCount + 1) * MAX_OS_BUTTONS >= TotalImpressionsOS Then
        iOSPageCount = 0
    Else
        iOSPageCount = iOSPageCount + 1
    End If
    ClearImpressionsButtons EEyeRef.eerOS
    DisplayImpressionsButtons EEyeRef.eerOS, iOSPageCount * MAX_OS_BUTTONS

    Exit Sub

lcmdMoreOS_Click_Error:

    LogError "frmSetDiagnosis", "cmdMoreOS_Click", Err, Err.Description
End Sub

Private Sub cmdOrderOD_Click()
    On Error GoTo lcmdOrderOD_Click_Error

    frmSystems1.SetDrOrderImpressions True
    OrderingODOn = Not OrderingODOn
    If OrderingODOn Then
        ClearImpressionsOD
        cmdOrderOD.Text = "Order OD Impressions Off"
    Else
        cmdOrderOD.Text = "Order OD Impressions"
    End If
    RetainImpressions PatientId
    LoadImpressions True, ViewOn, False, True, True

    Exit Sub

lcmdOrderOD_Click_Error:

    LogError "frmSetDiagnosis", "cmdOrderOD_Click", Err, Err.Description
End Sub

Private Sub cmdOrderOS_Click()
    On Error GoTo lcmdOrderOS_Click_Error

    frmSystems1.SetDrOrderImpressions True
    OrderingOSOn = Not OrderingOSOn
    If OrderingOSOn Then
        ClearImpressionsOS
        cmdOrderOS.Text = "Order OS Impressions Off"
    Else
        cmdOrderOS.Text = "Order OS Impressions"
    End If
    RetainImpressions PatientId
    LoadImpressions True, ViewOn, False, True, True

    Exit Sub

lcmdOrderOS_Click_Error:

    LogError "frmSetDiagnosis", "cmdOrderOS_Click", Err, Err.Description
End Sub

Private Sub cmdPlan_Click()
    On Error GoTo lcmdPlan_Click_Error
    If frmEvaluation.LoadActions(True, True) Then
        frmEvaluation.Show 1
        frmEvaluation.PostToFile
    End If

    Exit Sub

lcmdPlan_Click_Error:

    LogError "frmSetDiagnosis", "cmdPlan_Click", Err, Err.Description
End Sub

Private Sub cmdReviewSystems_Click()
    On Error GoTo lcmdReviewSystems_Click_Error

    If OrderingODOn Then
        Call cmdOrderOD_Click
    End If
    If OrderingOSOn Then
        Call cmdOrderOS_Click
    End If
    'REFACTOR: Remove hardcoded 91 and replace with an EFormResults
    NavigationAction = 91
    Unload frmSetDiagnosis

    Exit Sub

lcmdReviewSystems_Click_Error:

    LogError "frmSetDiagnosis", "cmdReviewSystems_Click", Err, Err.Description
End Sub

Private Sub cmdNotes_Click()
If Not UserLogin.HasPermission(epPatientNotes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
    On Error GoTo lcmdNotes_Click_Error

    frmNotes.NoteId = 0
    frmNotes.EyeContext = ""
    frmNotes.NoteOn = False
    frmNotes.SystemReference = "IMPR"
    frmNotes.PatientId = PatientId
    frmNotes.AppointmentId = AppointmentId
    frmNotes.MaintainOn = True
    frmNotes.SetTo = "C"
    If frmNotes.LoadNotes Then
        frmNotes.Show 1
        SetNotes PatientId, AppointmentId
    End If

    Exit Sub

lcmdNotes_Click_Error:

    LogError "frmSetDiagnosis", "cmdNotes_Click", Err, Err.Description, , PatientId, AppointmentId
End Sub

Public Function LoadImpressions(Init As Boolean, AView As Boolean, ReloadOn As Boolean, OverrideODOn As Boolean, OverrideOSOn As Boolean) As Boolean
Dim sPatName As String
Dim sPatType As String
Dim sFileName As String
    On Error GoTo lLoadImpressions_Error

    ViewOn = AView
    If Init Then
        ClearImpressionsButtons EEyeRef.eerOU
        frmSystems1.GetPatientInfo sPatName, sPatType
        lblPatName.Caption = sPatName
        lblPatType.Caption = sPatType
        InitializeImpressions
        If EditPreviousOn Then
            sFileName = DoctorInterfaceDirectory & "DiagImpr" & "_" & Trim$(str$(AppointmentId)) & "_" & Trim$(str$(PatientId)) & ".txt"
            If DoesScratchFileExist(sFileName) Then
                LoadImpressionsFromFile
            Else
                LoadImpressionsFromDB AppointmentId, PatientId
            End If
        Else
            If Not ReloadOn Then
                Call LoadImpressionsFromFile
            End If
        End If
        LoadAvailableImpressions OverrideODOn, OverrideOSOn
        iODPageCount = 0
        iOSPageCount = 0
    End If
    
    ReOrderImpressions eerOU

    DisplayImpressionsButtons eerOD, iODPageCount * MAX_OD_BUTTONS
    DisplayImpressionsButtons eerOS, iOSPageCount * MAX_OS_BUTTONS
    
    LoadImpressions = True
    CountTotalImpressions
    cmdMoreOD.Visible = TotalImpressionsOD > MAX_OD_BUTTONS
    cmdMoreOS.Visible = TotalImpressionsOS > MAX_OS_BUTTONS
    cmdSumm.Visible = Not ViewOn
    cmdPlan.Visible = Not ViewOn
    cmdCheckOut.Visible = Not ViewOn
    cmdReviewSystems.Visible = Not ViewOn
    If EditPreviousOn Then
        cmdSumm.Visible = False
        cmdPlan.Visible = False
        cmdCheckOut.Visible = False
        cmdReviewSystems.Visible = False
    End If
    AutoOrderingOff.Visible = frmSystems1.CheckDrOrderImpressions

    Exit Function

lLoadImpressions_Error:

    LogError "frmSetDiagnosis", "LoadImpressions", Err, Err.Description, , PatientId, AppointmentId
End Function

Private Sub TriggerImpression(iButtonIndex As Integer, sEye As String)
Dim MyOrder As Integer
Dim Temp1 As String
Dim Temp2 As String
Dim sHeader As String
Dim iSelectedImprIndex As Integer
Dim fIsModifiedBackup As Boolean
Dim fIsSelected As Boolean
    On Error GoTo lTriggerImpression_Error

    fIsModifiedBackup = fIsModified
    fIsModified = True
    If sEye = "OD" Then
        iSelectedImprIndex = GetSelectedIndex(iButtonIndex, EEyeRef.eerOD)
    ElseIf sEye = "OS" Then
        iSelectedImprIndex = GetSelectedIndex(iButtonIndex, EEyeRef.eerOS)
    End If
    If OrderingODOn And sEye = "OD" Then
        With SelectedImpressionsOD(iSelectedImprIndex)
            If .Active Then
                AdvanceOrder .OrderId, EEyeRef.eerOD
                .SetInactive
            Else
                .OrderId = GetNextOrder("OD")
                .Active = True
            End If
        End With
        DisplayImpressionsButtons EEyeRef.eerOD, iODPageCount * MAX_OD_BUTTONS
    ElseIf OrderingOSOn And sEye = "OS" Then
        With SelectedImpressionsOS(iSelectedImprIndex)
            If .Active Then
                AdvanceOrder .OrderId, EEyeRef.eerOS
                .SetInactive
            Else
                .OrderId = GetNextOrder("OS")
                .Active = True
            End If
        End With
        DisplayImpressionsButtons EEyeRef.eerOS, iOSPageCount * MAX_OS_BUTTONS
    Else
        If sEye = "OD" Then
            fIsSelected = SelectedImpressionsOD(iSelectedImprIndex).Active
        ElseIf sEye = "OS" Then
            fIsSelected = SelectedImpressionsOS(iSelectedImprIndex).Active
        End If
        If Not fIsSelected Then
            If sEye = "OD" Then
                SelectedImpressionsOD(iSelectedImprIndex).OrderId = GetNextOrder("OD")
                SelectedImpressionsOD(iSelectedImprIndex).Active = True
                If Not ReadBringForward(PatientId, AppointmentId) Then
                    If Not frmSystems1.CheckDrOrderImpressions Then
                        If Not EditPreviousOn Then
                            OrderSelectedBySystem "OD"
                        End If
                    End If
                End If
            ElseIf sEye = "OS" Then
                SelectedImpressionsOS(iSelectedImprIndex).OrderId = GetNextOrder("OS")
                SelectedImpressionsOS(iSelectedImprIndex).Active = True
                If Not ReadBringForward(PatientId, AppointmentId) Then
                    If Not frmSystems1.CheckDrOrderImpressions Then
                        If Not EditPreviousOn Then
                            OrderSelectedBySystem "OS"
                        End If
                    End If
                End If
            End If
            ReOrderImpressions eerOU
            DisplayImpressionsButtons eerOU
        Else
            If sEye = "OD" Then
                sHeader = cmdImpressionOD(iButtonIndex).Text
            ElseIf sEye = "OS" Then
                sHeader = cmdImpressionOS(iButtonIndex).Text
            End If
            frmEventMsgs.SetButtons sHeader, "Change Severity", "De-Select", "Cancel", "Imp Link Plan Notes", "", "Imp Discussions", "", "Set Order"
            frmEventMsgs.Show 1
            If frmEventMsgs.Result = 1 Then
                If sEye = "OD" Then
                    frmNumericPad.NumPad_Field = "Severity for " + SelectedImpressionsOD(iSelectedImprIndex).Name
                Else
                    frmNumericPad.NumPad_Field = "Severity for " + SelectedImpressionsOS(iSelectedImprIndex).Name
                End If
                frmNumericPad.NumPad_Result = ""
                frmNumericPad.NumPad_Max = 0
                frmNumericPad.NumPad_Min = 0
                frmNumericPad.NumPad_EyesOn = False
                frmNumericPad.NumPad_CommentOn = False
                frmNumericPad.NumPad_TimeFrameOn = False
                frmNumericPad.NumPad_TimeFrame = ""
                frmNumericPad.NumPad_DisplayFull = True
                frmNumericPad.NumPad_ChoiceOn1 = False
                frmNumericPad.NumPad_Choice1 = "Severe"
                frmNumericPad.NumPad_ChoiceOn2 = False
                frmNumericPad.NumPad_Choice2 = "Mild"
                frmNumericPad.NumPad_ChoiceOn3 = False
                frmNumericPad.NumPad_Choice3 = "Stable"
                frmNumericPad.NumPad_ChoiceOn4 = False
                frmNumericPad.NumPad_Choice4 = ""
                frmNumericPad.NumPad_ChoiceOn5 = False
                frmNumericPad.NumPad_Choice5 = ""
                frmNumericPad.NumPad_ChoiceOn6 = False
                frmNumericPad.NumPad_Choice6 = "Better"
                frmNumericPad.NumPad_ChoiceOn7 = False
                frmNumericPad.NumPad_Choice7 = "Worse"
                frmNumericPad.NumPad_ChoiceOn8 = False
                frmNumericPad.NumPad_Choice8 = "New"
                frmNumericPad.NumPad_ChoiceOn9 = False
                frmNumericPad.NumPad_Choice9 = ""
                frmNumericPad.NumPad_ChoiceOn10 = False
                frmNumericPad.NumPad_Choice10 = ""
                frmNumericPad.NumPad_ChoiceOn11 = False
                frmNumericPad.NumPad_Choice11 = ""
                frmNumericPad.NumPad_ChoiceOn12 = False
                frmNumericPad.NumPad_Choice12 = ""
                frmNumericPad.NumPad_ChoiceOn13 = False
                frmNumericPad.NumPad_Choice13 = ""
                frmNumericPad.NumPad_ChoiceOn14 = False
                frmNumericPad.NumPad_Choice14 = ""
                frmNumericPad.NumPad_ChoiceOn15 = False
                frmNumericPad.NumPad_Choice15 = ""
                frmNumericPad.NumPad_ChoiceOn16 = False
                frmNumericPad.NumPad_Choice16 = ""
                frmNumericPad.NumPad_ChoiceOn17 = False
                frmNumericPad.NumPad_Choice17 = ""
                frmNumericPad.NumPad_ChoiceOn18 = False
                frmNumericPad.NumPad_Choice18 = ""
                frmNumericPad.NumPad_ChoiceOn19 = False
                frmNumericPad.NumPad_Choice19 = ""
                frmNumericPad.NumPad_ChoiceOn20 = False
                frmNumericPad.NumPad_Choice20 = ""
                frmNumericPad.NumPad_ChoiceOn21 = False
                frmNumericPad.NumPad_Choice21 = ""
                frmNumericPad.NumPad_ChoiceOn22 = False
                frmNumericPad.NumPad_Choice22 = ""
                frmNumericPad.NumPad_ChoiceOn23 = False
                frmNumericPad.NumPad_Choice23 = ""
                frmNumericPad.NumPad_ChoiceOn24 = False
                frmNumericPad.NumPad_Choice24 = ""
                frmNumericPad.Show 1
                If Not frmNumericPad.NumPad_Quit Then
                    ' REFACTOR: We already know the eye - it's sEye - based on the button they pushed.
                    Temp1 = ""
                    Temp2 = frmNumericPad.NumPad_Result
                    If frmNumericPad.NumPad_ChoiceOn1 Then
                        Temp1 = Trim$(frmNumericPad.NumPad_Choice1)
                    ElseIf frmNumericPad.NumPad_ChoiceOn2 Then
                        Temp1 = Trim$(frmNumericPad.NumPad_Choice2)
                    ElseIf frmNumericPad.NumPad_ChoiceOn3 Then
                        Temp1 = Trim$(frmNumericPad.NumPad_Choice3)
                    ElseIf frmNumericPad.NumPad_ChoiceOn6 Then
                        Temp1 = Trim$(frmNumericPad.NumPad_Choice6)
                    ElseIf frmNumericPad.NumPad_ChoiceOn7 Then
                        Temp1 = Trim$(frmNumericPad.NumPad_Choice7)
                    ElseIf frmNumericPad.NumPad_ChoiceOn8 Then
                        Temp1 = Trim$(frmNumericPad.NumPad_Choice8)
                    End If
                    If sEye = "OD" Then
                        SelectedImpressionsOD(iSelectedImprIndex).SeverityA = Temp2 + Temp1
                    Else
                        SelectedImpressionsOS(iSelectedImprIndex).SeverityB = Temp2 + Temp1
                    End If
                End If
            ElseIf frmEventMsgs.Result = 2 Then
                If sEye = "OD" Then
                    MyOrder = SelectedImpressionsOD(iSelectedImprIndex).OrderId
                    SelectedImpressionsOD(iSelectedImprIndex).SetInactive
                    frmNotes.PurgeNotes PatientId, AppointmentId, "C", "IMPR-" & Trim$(SelectedImpressionsOD(iSelectedImprIndex).Id)
                    AdvanceOrder MyOrder, EEyeRef.eerOD
                Else
                    MyOrder = SelectedImpressionsOS(iSelectedImprIndex).OrderId
                    SelectedImpressionsOS(iSelectedImprIndex).SetInactive
                    frmNotes.PurgeNotes PatientId, AppointmentId, "C", "IMPR-" & Trim$(SelectedImpressionsOS(iSelectedImprIndex).Id)
                    AdvanceOrder MyOrder, EEyeRef.eerOS
                End If
                SetDrOrderImpressionsOn
                RetainImpressions PatientId
                LoadImpressions True, ViewOn, False, True, True
            ElseIf frmEventMsgs.Result = 3 Then 'Or (frmEventMsgs.Result = 5) Then
             If Not UserLogin.HasPermission(epPatientNotes) Then
                frmEventMsgs.Header = "Not Permissioned"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Exit Sub
            End If
                If sEye = "OD" Then
                    frmNotes.EyeContext = Trim$(SelectedImpressionsOD(iSelectedImprIndex).Eye)
                    If GetImpressionLocation(SelectedImpressionsOD(iSelectedImprIndex).Id, "OS") > -1 Then
                        frmEventMsgs.SetButtons "Applies to Both Eyes", "", "Yes", "No"
                        frmEventMsgs.Show 1
                        If frmEventMsgs.Result = 2 Then
                            frmNotes.EyeContext = "OU"
                        End If
                    End If
                    frmNotes.NoteId = 0
                    frmNotes.NoteOn = False
                    frmNotes.SystemReference = "IMPR-" + Trim$(SelectedImpressionsOD(iSelectedImprIndex).Id)
                    frmNotes.PatientId = PatientId
                    frmNotes.AppointmentId = AppointmentId
                    frmNotes.MaintainOn = True
                    frmNotes.SetTo = "C"
                    If frmNotes.LoadNotes Then
                        frmNotes.Show 1
                        SetNotes PatientId, AppointmentId
                    End If
                Else
                    frmNotes.EyeContext = Trim$(SelectedImpressionsOS(iSelectedImprIndex).Eye)
                    If GetImpressionLocation(SelectedImpressionsOS(iSelectedImprIndex).Id, "OD") > -1 Then
                        frmEventMsgs.SetButtons "Applies to Both Eyes", "", "Yes", "No"
                        frmEventMsgs.Show 1
                        If frmEventMsgs.Result = 2 Then
                            frmNotes.EyeContext = "OU"
                        End If
                    End If
                    frmNotes.NoteId = 0
                    frmNotes.NoteOn = False
                    frmNotes.SystemReference = "IMPR-" + Trim$(SelectedImpressionsOS(iSelectedImprIndex).Id)
                    frmNotes.PatientId = PatientId
                    frmNotes.AppointmentId = AppointmentId
                    frmNotes.MaintainOn = True
                    frmNotes.SetTo = "C"
                    If frmNotes.LoadNotes Then
                        frmNotes.Show 1
                        SetNotes PatientId, AppointmentId
                    End If
                End If
            ElseIf frmEventMsgs.Result = 6 Then ' Or (frmEventMsgs.Result = 7) Then
             If Not UserLogin.HasPermission(epPatientNotes) Then
                frmEventMsgs.Header = "Not Permissioned"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Exit Sub
            End If
                frmNotes.NoteId = 0
                If sEye = "OD" Then
                    frmNotes.EyeContext = Trim$(SelectedImpressionsOD(iSelectedImprIndex).Eye)
                    frmNotes.SystemReference = "IMDN-" + Trim$(SelectedImpressionsOD(iSelectedImprIndex).Id)
                Else
                    frmNotes.EyeContext = Trim$(SelectedImpressionsOS(iSelectedImprIndex).Eye)
                    frmNotes.SystemReference = "IMDN-" + Trim$(SelectedImpressionsOS(iSelectedImprIndex).Id)
                End If
                frmNotes.NoteOn = False
                frmNotes.PatientId = PatientId
                frmNotes.AppointmentId = AppointmentId
                frmNotes.MaintainOn = True
                frmNotes.SetTo = "C"
                If frmNotes.LoadNotes Then
                    frmNotes.Show 1
                    SetNotes PatientId, AppointmentId
                End If
            ElseIf frmEventMsgs.Result = 8 Then
                If sEye = "OD" Then
                    frmImpr.ImprName = cmdImpressionOD(iSelectedImprIndex).Text
                ElseIf sEye = "OS" Then
                    frmImpr.ImprName = cmdImpressionOS(iSelectedImprIndex).Text
                End If
                frmImpr.OrderNumber = 0
                frmImpr.Show 1
                If frmImpr.OrderNumber > 0 Then
                    SetImprOrder iSelectedImprIndex, frmImpr.OrderNumber, sEye
                    RetainImpressions PatientId
                    LoadImpressions True, True, False, False, False
                End If
            ElseIf frmEventMsgs.Result = 4 Then
                fIsModified = fIsModifiedBackup
            End If
        End If
    End If

    Exit Sub

lTriggerImpression_Error:

    LogError "frmSetDiagnosis", "TriggerImpression", Err, Err.Description, , PatientId, AppointmentId
End Sub

Private Sub SetDrOrderImpressionsOn()
frmSystems1.SetDrOrderImpressions True
AutoOrderingOff.Visible = True
End Sub


Private Function GetSelectedIndex(iButtonIndex As Integer, eerEye As EEyeRef) As Integer
    On Error GoTo lGetSelectedIndex_Error

    Select Case eerEye
        Case EEyeRef.eerOD
            GetSelectedIndex = iButtonIndex + iODPageCount * MAX_OD_BUTTONS
        Case EEyeRef.eerOS
            GetSelectedIndex = iButtonIndex + iOSPageCount * MAX_OS_BUTTONS
    End Select

    Exit Function

lGetSelectedIndex_Error:

    LogError "frmSetDiagnosis", "GetSelectedIndex", Err, Err.Description
End Function

Private Function SetImprOrder(Idx As Integer, OrderNumber As Integer, AEye As String) As Boolean
Dim i As Integer
    On Error GoTo lSetImprOrder_Error

    SetImprOrder = True
    CountTotalImpressions
    If (Idx > -1) And (OrderNumber > 0) Then
        If (AEye = "OD") Then
            SelectedImpressionsOD(Idx).Active = True
            SelectedImpressionsOD(Idx).OrderId = OrderNumber
            For i = 0 To TotalImpressionsOD - 1
                If (SelectedImpressionsOD(i).Active) And (SelectedImpressionsOD(i).OrderId >= OrderNumber) And (i <> Idx) Then
                    SelectedImpressionsOD(i).OrderId = SelectedImpressionsOD(i).OrderId + 1
                End If
            Next
        Else
            SelectedImpressionsOS(Idx).Active = True
            SelectedImpressionsOS(Idx).OrderId = OrderNumber
            For i = 0 To TotalImpressionsOS - 1
                If (SelectedImpressionsOS(i).Active) And (SelectedImpressionsOS(i).OrderId >= OrderNumber) And (i <> Idx) Then
                    SelectedImpressionsOS(i).OrderId = SelectedImpressionsOS(i).OrderId + 1
                End If
            Next
        End If
    End If

    Exit Function

lSetImprOrder_Error:

    LogError "frmSetDiagnosis", "SetImprOrder", Err, Err.Description
End Function

Private Function GetPrimaryDiagnosis(Id As String, ASys As String, TheName As String, BillOn As Boolean, ImprOn As Boolean, AId As Long) As Boolean
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
    On Error GoTo lGetPrimaryDiagnosis_Error

    TheName = ""
    ImprOn = True
    If (Id <> "") And (Len(Id) > 1) Then
        Set RetrieveDiagnosis = New DiagnosisMasterPrimary
        RetrieveDiagnosis.PrimarySystem = ASys
        RetrieveDiagnosis.PrimaryDiagnosis = ""
        RetrieveDiagnosis.PrimaryNextLevelDiagnosis = Id
        RetrieveDiagnosis.PrimaryLevel = 0
        RetrieveDiagnosis.PrimaryRank = 0
        RetrieveDiagnosis.PrimaryBilling = BillOn
        RetrieveDiagnosis.PrimaryDiscipline = ""
        If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
            If (RetrieveDiagnosis.SelectPrimary(1)) Then
                TheName = Trim$(RetrieveDiagnosis.PrimaryShortName)
                If (TheName = "") Then
                    TheName = Trim$(RetrieveDiagnosis.PrimaryLingo)
                    If (Trim$(TheName) = "") Then
                        TheName = Trim$(RetrieveDiagnosis.PrimaryName)
                    End If
                End If
                AId = RetrieveDiagnosis.PrimaryId
                ImprOn = RetrieveDiagnosis.PrimaryImpression
                GetPrimaryDiagnosis = True
            End If
        Else
            RetrieveDiagnosis.PrimarySystem = ""
            If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
                If (RetrieveDiagnosis.SelectPrimary(1)) Then
                    TheName = Trim$(RetrieveDiagnosis.PrimaryShortName)
                    If (TheName = "") Then
                        TheName = Trim$(RetrieveDiagnosis.PrimaryLingo)
                        If (Trim$(TheName) = "") Then
                            TheName = Trim$(RetrieveDiagnosis.PrimaryName)
                        End If
                    End If
                    AId = RetrieveDiagnosis.PrimaryId
                    ImprOn = RetrieveDiagnosis.PrimaryImpression
                    GetPrimaryDiagnosis = True
                End If
            End If
        End If
        Set RetrieveDiagnosis = Nothing
    End If

    Exit Function

lGetPrimaryDiagnosis_Error:

    LogError "frmSetDiagnosis", "GetPrimaryDiagnosis", Err, Err.Description
End Function

Private Sub SetButton(iButtonIndex As Long, AName As String, AEye As String, AId As String)
Dim i As Integer
Dim MyIdx As Integer
    On Error GoTo lSetButton_Error

    If (AId = "N") Then
        AId = "N-" + AName
    End If
    i = IsImprSet(AId, AEye)
    If (i > -1) Then
        If (AEye = "OD") Then
            cmdImpressionOD(iButtonIndex).BackColor = BaseBackColor
            cmdImpressionOD(iButtonIndex).ForeColor = BaseTextColor
            cmdImpressionOD(iButtonIndex).Text = AName
            cmdImpressionOD(iButtonIndex).Tag = AId
            cmdImpressionOD(iButtonIndex).Visible = True
            cmdImpressionOD(iButtonIndex).ToolTipText = "OD:" + Trim$(SelectedImpressionsOD(i).SeverityA) + " OS:" + Trim$(SelectedImpressionsOD(i).SeverityB)
            If (SelectedImpressionsOD(i).Active) Then
                cmdImpressionOD(iButtonIndex).BackColor = ButtonSelectionColor
                cmdImpressionOD(iButtonIndex).ForeColor = TextSelectionColor
                If (SelectedImpressionsOD(i).OrderId < 1) Then
                    MyIdx = GetNextOrder("OD")
                    SelectedImpressionsOD(i).OrderId = MyIdx
                    cmdImpressionOD(iButtonIndex).Text = cmdImpressionOD(iButtonIndex).Text + " - " + Trim$(str$(SelectedImpressionsOD(i).OrderId))
                End If
            End If
        Else
            cmdImpressionOS(iButtonIndex).BackColor = BaseBackColor
            cmdImpressionOS(iButtonIndex).ForeColor = BaseTextColor
            cmdImpressionOS(iButtonIndex).Text = AName
            cmdImpressionOS(iButtonIndex).Tag = AId
            cmdImpressionOS(iButtonIndex).Visible = True
            cmdImpressionOS(iButtonIndex).ToolTipText = "OD:" + Trim$(SelectedImpressionsOS(i).SeverityA) + " OS:" + Trim$(SelectedImpressionsOS(i).SeverityB)
            If (SelectedImpressionsOS(i).Active) Then
                cmdImpressionOS(iButtonIndex).BackColor = ButtonSelectionColor
                cmdImpressionOS(iButtonIndex).ForeColor = TextSelectionColor
                If (SelectedImpressionsOS(i).OrderId < 1) Then
                    SelectedImpressionsOS(i).OrderId = GetNextOrder("OS")
                    cmdImpressionOS(iButtonIndex).Text = cmdImpressionOS(iButtonIndex).Text + " - " + Trim$(str$(SelectedImpressionsOS(i).OrderId))
                End If
            End If
        End If
    End If

    Exit Sub

lSetButton_Error:

    LogError "frmSetDiagnosis", "SetButton", Err, Err.Description
End Sub

Private Function IsImprSet(TheId As String, Eye As String) As Integer
Dim i As Integer
    On Error GoTo lIsImprSet_Error

    IsImprSet = -1
    If Trim$(TheId) <> "" Then
        If Eye = "OD" Then
            For i = 0 To TotalImpressionsOD - 1
                If (Trim$(SelectedImpressionsOD(i).Id) = Trim$(TheId)) And ((Trim$(Eye) = SelectedImpressionsOD(i).Eye) Or (Trim$(Eye) = "")) Then
                    IsImprSet = i
                    Exit For
                End If
            Next
        Else
            For i = 0 To TotalImpressionsOS - 1
                If (Trim$(SelectedImpressionsOS(i).Id) = Trim$(TheId)) And ((Trim$(Eye) = SelectedImpressionsOS(i).Eye) Or (Trim$(Eye) = "")) Then
                    IsImprSet = i
                    Exit For
                End If
            Next
        End If
    End If

    Exit Function

lIsImprSet_Error:

    LogError "frmSetDiagnosis", "IsImprSet", Err, Err.Description
End Function

Private Function GetImpressionLocation(sTheId As String, sEye As String) As Long
Dim i As Integer
    On Error GoTo lGetImpressionLocation_Error
    
    GetImpressionLocation = -1
    CountTotalImpressions
    If Trim$(sTheId) <> "" Then
        If sEye = "OD" Then
            For i = 0 To TotalImpressionsOD - 1
                If Trim$(SelectedImpressionsOD(i).Id) = Trim$(sTheId) Then
                    If Trim$(sEye) = SelectedImpressionsOD(i).Eye Then
                        GetImpressionLocation = i
                        Exit For
                    End If
                End If
            Next
        Else
            For i = 0 To TotalImpressionsOS - 1
                If Trim$(SelectedImpressionsOS(i).Id) = Trim$(sTheId) Then
                    If Trim$(sEye) = SelectedImpressionsOS(i).Eye Then
                        GetImpressionLocation = i
                        Exit For
                    End If
                End If
            Next
        End If
    End If

    Exit Function

lGetImpressionLocation_Error:

    LogError "frmSetDiagnosis", "GetImpressionLocation", Err, Err.Description
End Function

Public Function InitializeImpressions() As Boolean
Dim i As Integer
    On Error GoTo lInitializeImpressions_Error

    For i = 0 To MaxImpressions
        Set SelectedImpressionsOD(i) = New CImpression
        Set SelectedImpressionsOS(i) = New CImpression
        Set SelectedImpressionsODTemp(i) = New CImpression
        Set SelectedImpressionsOSTemp(i) = New CImpression
    Next
    Set SelectedImpressionsOU = New CImpression
    TotalImpressionsOD = 0
    TotalImpressionsOS = 0
    InitializeImpressions = True

    Exit Function

lInitializeImpressions_Error:

    LogError "frmSetDiagnosis", "InitializeImpressions", Err, Err.Description
End Function

Public Function TotalSelectedImpressionsOD() As Long
Dim i As Integer
    On Error GoTo lTotalSelectedImpressionsOD_Error

    For i = 0 To MaxImpressions
        If Not SelectedImpressionsOD(i).Active Then
            TotalSelectedImpressionsOD = TotalSelectedImpressionsOD + 1
        End If
    Next

    Exit Function

lTotalSelectedImpressionsOD_Error:

    LogError "frmSetDiagnosis", "TotalSelectedImpressionsOD", Err, Err.Description
End Function

Public Function TotalSelectedImpressionsOS() As Long
Dim i As Integer
    On Error GoTo lTotalSelectedImpressionsOS_Error

    For i = 0 To MaxImpressions
        If Not SelectedImpressionsOS(i).Active Then
            TotalSelectedImpressionsOS = TotalSelectedImpressionsOS + 1
        End If
    Next

    Exit Function

lTotalSelectedImpressionsOS_Error:

    LogError "frmSetDiagnosis", "TotalSelectedImpressionsOS", Err, Err.Description
End Function

Private Sub ClearImpressionsOD()
Dim i As Integer
    On Error GoTo lClearImpressionsOD_Error

For i = 0 To TotalImpressionsOD - 1
    SelectedImpressionsOD(i).Active = False
    SelectedImpressionsOD(i).OrderId = 0
Next

    Exit Sub

lClearImpressionsOD_Error:

    LogError "frmSetDiagnosis", "ClearImpressionsOD", Err, Err.Description
End Sub

Private Sub ClearImpressionsOS()
Dim i As Integer
    On Error GoTo lClearImpressionsOS_Error

For i = 0 To TotalImpressionsOS - 1
    SelectedImpressionsOS(i).Active = False
    SelectedImpressionsOS(i).OrderId = 0
Next

    Exit Sub

lClearImpressionsOS_Error:

    LogError "frmSetDiagnosis", "ClearImpressionsOS", Err, Err.Description
End Sub

Private Function SetImpressionsOD(Ref As Integer, TheDiagId As String, TheDiagnosis As String, TheEye As String, TheSeverityA As String, TheSeverityB As String, TheRetA As String, TheQuan As String, TheCnf As Boolean, TheHst As Boolean, TheRul As Boolean, TheOrd As Integer) As Boolean
    On Error GoTo lSetImpressionsOD_Error

SetImpressionsOD = False
TheCnf = False
TheRul = False
TheHst = False
TheDiagId = ""
TheDiagnosis = ""
TheSeverityA = ""
TheSeverityB = ""
TheQuan = ""
TheOrd = 0
If (Ref > -1) Then
    If (Trim$(SelectedImpressionsOD(Ref).Name) <> "") Then
        TheEye = SelectedImpressionsOD(Ref).Eye
        If (Len(TheEye) <> 2) Then
            TheEye = "  "
        End If
        If (SelectedImpressionsOD(Ref).NegativeFinding) Then
            TheCnf = True
        End If
        If (SelectedImpressionsOD(Ref).RuleOut) Then
            TheRul = True
        End If
        If (SelectedImpressionsOD(Ref).HistoryOf) Then
            TheHst = True
        End If
        TheDiagId = Trim$(SelectedImpressionsOD(Ref).Id)
        TheDiagnosis = Trim$(SelectedImpressionsOD(Ref).Name)
        TheSeverityA = Trim$(SelectedImpressionsOD(Ref).SeverityA)
        TheSeverityB = Trim$(SelectedImpressionsOD(Ref).SeverityB)
        TheQuan = Trim$(SelectedImpressionsOD(Ref).Quant)
        TheRetA = "F"
        If (SelectedImpressionsOD(Ref).Active) Then
            TheRetA = "T"
        End If
        TheOrd = SelectedImpressionsOD(Ref).OrderId
        SetImpressionsOD = True
    End If
End If

    Exit Function

lSetImpressionsOD_Error:

    LogError "frmSetDiagnosis", "SetImpressionsOD", Err, Err.Description
End Function

Private Function SetImpressionsOS(Ref As Integer, TheDiagId As String, TheDiagnosis As String, TheEye As String, TheSeverityA As String, TheSeverityB As String, TheRetA As String, TheQuan As String, TheCnf As Boolean, TheHst As Boolean, TheRul As Boolean, TheOrd As Integer) As Boolean
    On Error GoTo lSetImpressionsOS_Error

SetImpressionsOS = False
TheCnf = False
TheRul = False
TheHst = False
TheDiagId = ""
TheDiagnosis = ""
TheSeverityA = ""
TheSeverityB = ""
TheQuan = ""
TheOrd = 0
If (Ref > -1) Then
    If (Trim$(SelectedImpressionsOS(Ref).Name) <> "") Then
        TheEye = SelectedImpressionsOS(Ref).Eye
        If (Len(TheEye) <> 2) Then
            TheEye = "  "
        End If
        If (SelectedImpressionsOS(Ref).NegativeFinding) Then
            TheCnf = True
        End If
        If (SelectedImpressionsOS(Ref).RuleOut) Then
            TheRul = True
        End If
        If (SelectedImpressionsOS(Ref).HistoryOf) Then
            TheHst = True
        End If
        TheDiagId = Trim$(SelectedImpressionsOS(Ref).Id)
        TheDiagnosis = Trim$(SelectedImpressionsOS(Ref).Name)
        TheSeverityA = Trim$(SelectedImpressionsOS(Ref).SeverityA)
        TheSeverityB = Trim$(SelectedImpressionsOS(Ref).SeverityB)
        TheQuan = Trim$(SelectedImpressionsOS(Ref).Quant)
        TheRetA = "F"
        If (SelectedImpressionsOS(Ref).Active) Then
            TheRetA = "T"
        End If
        TheOrd = SelectedImpressionsOS(Ref).OrderId
        SetImpressionsOS = True
    End If
End If

    Exit Function

lSetImpressionsOS_Error:

    LogError "frmSetDiagnosis", "SetImpressionsOS", Err, Err.Description
End Function

Public Function GetImpressionsOD(RefId As Integer, ADiag As String, ADiagnosis As String, AEye As String, ASeverityA As String, ASeverityB As String, ARetA As String, AQuan As String, ACnf As Boolean, AHst As Boolean, ARul As Boolean, AOrd As Integer) As Boolean
    On Error GoTo lGetImpressionsOD_Error

    GetImpressionsOD = SetImpressionsOD(RefId, ADiag, ADiagnosis, AEye, ASeverityA, ASeverityB, ARetA, AQuan, ACnf, AHst, ARul, AOrd)
    
    Exit Function

lGetImpressionsOD_Error:

    LogError "frmSetDiagnosis", "GetImpressionsOD", Err, Err.Description
End Function

Public Function GetImpressionsOS(RefId As Integer, ADiag As String, ADiagnosis As String, AEye As String, ASeverityA As String, ASeverityB As String, ARetA As String, AQuan As String, ACnf As Boolean, AHst As Boolean, ARul As Boolean, AOrd As Integer) As Boolean
    On Error GoTo lGetImpressionsOS_Error

GetImpressionsOS = SetImpressionsOS(RefId, ADiag, ADiagnosis, AEye, ASeverityA, ASeverityB, ARetA, AQuan, ACnf, AHst, ARul, AOrd)

    Exit Function

lGetImpressionsOS_Error:

    LogError "frmSetDiagnosis", "GetImpressionsOS", Err, Err.Description
End Function

Private Sub LoadAvailableImpressions(OverrideODOn As Boolean, OverrideOSOn As Boolean)
Dim PreSet As Boolean
Dim ImprOn As Boolean
Dim iIter As Long
Dim w As Integer, i As Integer
Dim q As Integer, z As Integer
Dim iPos As Integer
Dim TheId As Long
Dim Rec As String, TheText As String
Dim RQuan As String
Dim RSys As String, RText As String
Dim RightDiagnosis As String
Dim LQuan As String
Dim LSys As String, LText As String
Dim LeftDiagnosis As String
Dim iFreeFile As Long
Dim GeneralImpressions As String
Dim oImpression As New CImpression
    On Error GoTo lLoadAvailableImpressions_Error

    'unknown parameter. hardcoded override.
    'PreSet = Not (CheckConfigCollection("SETIMPRESSIONS") = "F")
    PreSet = False
    
    CountTotalImpressions
    i = 1
    While frmSystems1.GetSystemDiagnosis(i, RSys, RightDiagnosis, RText, RQuan, LSys, LeftDiagnosis, LText, LQuan)
        If Trim$(RightDiagnosis) <> "" Then
            Set oImpression = New CImpression
            For iIter = 1 To 3
                If Mid$(RightDiagnosis, iIter, 1) = CntRefSet Then
                    oImpression.NegativeFinding = True
                    RightDiagnosis = Left$(RightDiagnosis, iIter - 1) & Mid$(RightDiagnosis, iIter + 1, Len(RightDiagnosis) - iIter)
                End If
            Next iIter
            For iIter = 1 To 3
                If Mid$(RightDiagnosis, iIter, 1) = HORefSet Then
                    oImpression.HistoryOf = True
                    RightDiagnosis = Left$(RightDiagnosis, iIter - 1) & Mid$(RightDiagnosis, iIter + 1, Len(RightDiagnosis) - iIter)
                End If
            Next iIter
            For iIter = 1 To 3
                If Mid$(RightDiagnosis, iIter, 1) = RORefSet Then
                    oImpression.RuleOut = True
                    RightDiagnosis = Left$(RightDiagnosis, iIter - 1) & Mid$(RightDiagnosis, iIter + 1, Len(RightDiagnosis) - iIter)
                End If
            Next iIter
            w = InStrPS(RightDiagnosis, "@")
            If w > 0 Then
                RightDiagnosis = Left$(RightDiagnosis, w - 1)
            End If
            iPos = IsImprSet(RightDiagnosis, "OD")
            If iPos = -1 Then
                If GetPrimaryDiagnosis(RightDiagnosis, RSys, TheText, False, ImprOn, TheId) Then
                    If OverrideODOn Then
                        ImprOn = False
                    End If
                    SelectedImpressionsOD(TotalImpressionsOD).Id = RightDiagnosis
                    SelectedImpressionsOD(TotalImpressionsOD).Name = TheText
                    ' REFACTOR: Shouldn't have a special case for C/D.  Need to put this into the DM.
                    If (InStrPS(RText, "C/D:") > 0) Then
                        If (UCase$(Trim$(SelectedImpressionsOD(TotalImpressionsOD).Name)) = "CUP TO DISC") Then
                            SelectedImpressionsOD(TotalImpressionsOD).Name = RText
                        Else
                            SelectedImpressionsOD(TotalImpressionsOD).Name = TheText + " " + RText
                        End If
                    End If
                    SelectedImpressionsOD(TotalImpressionsOD).Eye = "OD"
                    SelectedImpressionsOD(TotalImpressionsOD).Quant = RQuan
                    SelectedImpressionsOD(TotalImpressionsOD).ImprOn = ImprOn
                    SelectedImpressionsOD(TotalImpressionsOD).SeverityA = ""
                    SelectedImpressionsOD(TotalImpressionsOD).SeverityB = ""
                    SelectedImpressionsOD(TotalImpressionsOD).Active = PreSet
                    If ImprOn Then
                        SelectedImpressionsOD(TotalImpressionsOD).OrderId = GetNextOrder("OD")
                        SelectedImpressionsOD(TotalImpressionsOD).Active = True
                    End If
                    SelectedImpressionsOD(TotalImpressionsOD).IgnoreEye = False
                    SelectedImpressionsOD(TotalImpressionsOD).NegativeFinding = oImpression.NegativeFinding
                    SelectedImpressionsOD(TotalImpressionsOD).RuleOut = oImpression.RuleOut
                    SelectedImpressionsOD(TotalImpressionsOD).HistoryOf = oImpression.HistoryOf
                    TotalImpressionsOD = TotalImpressionsOD + 1
                End If
            Else
                If HasFindingChanged(iPos, EEyeRef.eerOD, RQuan, oImpression) Then
                    UpdateImpression iPos, EEyeRef.eerOD, RQuan, oImpression
                End If
            End If
        End If
        
        If (Trim$(LeftDiagnosis) <> "") Then
            Set oImpression = New CImpression
            For iIter = 1 To 3
                If Mid$(LeftDiagnosis, iIter, 1) = CntRefSet Then
                    oImpression.NegativeFinding = True
                    LeftDiagnosis = Left$(LeftDiagnosis, iIter - 1) & Mid$(LeftDiagnosis, iIter + 1, Len(LeftDiagnosis) - iIter)
                End If
            Next iIter
            For iIter = 1 To 3
                If Mid$(LeftDiagnosis, iIter, 1) = HORefSet Then
                    oImpression.HistoryOf = True
                    LeftDiagnosis = Left$(LeftDiagnosis, iIter - 1) & Mid$(LeftDiagnosis, iIter + 1, Len(LeftDiagnosis) - iIter)
                End If
            Next iIter
            For iIter = 1 To 3
                If Mid$(LeftDiagnosis, iIter, 1) = RORefSet Then
                    oImpression.RuleOut = True
                    LeftDiagnosis = Left$(LeftDiagnosis, iIter - 1) & Mid$(LeftDiagnosis, iIter + 1, Len(LeftDiagnosis) - iIter)
                End If
            Next iIter
            w = InStrPS(LeftDiagnosis, "@")
            If w > 0 Then
                LeftDiagnosis = Left$(LeftDiagnosis, w - 1)
            End If
            iPos = IsImprSet(LeftDiagnosis, "OS")
            If iPos = -1 Then
                If GetPrimaryDiagnosis(LeftDiagnosis, LSys, TheText, False, ImprOn, TheId) Then
                    If OverrideOSOn Then
                        ImprOn = False
                    End If
                    SelectedImpressionsOS(TotalImpressionsOS).Id = LeftDiagnosis
                    SelectedImpressionsOS(TotalImpressionsOS).Name = TheText
                    ' REFACTOR: Shouldn't have a special case for C/D.  Need to put this into the DM.
                    If (InStrPS(LText, "C/D:") > 0) Then
                        If (UCase$(Trim$(SelectedImpressionsOS(TotalImpressionsOS).Name)) = "CUP TO DISC") Then
                            SelectedImpressionsOS(TotalImpressionsOS).Name = LText
                        Else
                            SelectedImpressionsOS(TotalImpressionsOS).Name = TheText + " " + LText
                        End If
                    End If
                    SelectedImpressionsOS(TotalImpressionsOS).Eye = "OS"
                    SelectedImpressionsOS(TotalImpressionsOS).Quant = LQuan
                    SelectedImpressionsOS(TotalImpressionsOS).ImprOn = ImprOn
                    SelectedImpressionsOS(TotalImpressionsOS).SeverityA = ""
                    SelectedImpressionsOS(TotalImpressionsOS).SeverityB = ""
                    SelectedImpressionsOS(TotalImpressionsOS).Active = PreSet
                    If (ImprOn) Then
                        SelectedImpressionsOS(TotalImpressionsOS).OrderId = GetNextOrder("OS")
                        SelectedImpressionsOS(TotalImpressionsOS).Active = True
                    End If
                    SelectedImpressionsOS(TotalImpressionsOS).IgnoreEye = False
                    SelectedImpressionsOS(TotalImpressionsOS).NegativeFinding = oImpression.NegativeFinding
                    SelectedImpressionsOS(TotalImpressionsOS).RuleOut = oImpression.RuleOut
                    SelectedImpressionsOS(TotalImpressionsOS).HistoryOf = oImpression.HistoryOf
                    TotalImpressionsOS = TotalImpressionsOS + 1
                End If
            Else
                If HasFindingChanged(iPos, EEyeRef.eerOS, LQuan, oImpression) Then
                    UpdateImpression iPos, EEyeRef.eerOS, LQuan, oImpression
                End If
            End If
        End If
        i = i + 1
    Wend
    
GeneralImpressions = GetTestFileName("69A")
If FM.IsFileThere(GeneralImpressions) Then
    iFreeFile = FreeFile
    FM.OpenFile GeneralImpressions, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(iFreeFile)
    Do Until (EOF(iFreeFile))
        Line Input #iFreeFile, Rec
        If (Mid$(Rec, 10, 1) <> "/") And (Left$(Rec, 6) <> "Recap=") Then
            q = InStrPS(Rec, "=")
            If (q > 0) Then
                z = InStrPS(q, Rec, " ")
                If (z > 0) Then
                    TheId = Val(Trim$(Mid$(Rec, q + 2, (z - 1) - (q + 1))))
                Else
                    TheId = Val(Trim$(Mid$(Rec, q + 2, 5)))
                End If
                If (RetrieveGeneralImpression(TheId, TheText)) Then
                    If (InStrPS(TheText, ":") = 0) Then
                        SelectedImpressionsOU.Id = Trim$(str$((-1) * TheId))
                        SelectedImpressionsOU.Name = TheText
                        SelectedImpressionsOU.Eye = "OU"
                        SelectedImpressionsOU.Quant = ""
                        SelectedImpressionsOU.ImprOn = False
                        SelectedImpressionsOU.SeverityA = ""
                        SelectedImpressionsOU.SeverityB = ""
                        SelectedImpressionsOU.Active = PreSet
                        SelectedImpressionsOU.IgnoreEye = True
                        SelectedImpressionsOU.NegativeFinding = False
                        SelectedImpressionsOU.RuleOut = False
                        SelectedImpressionsOU.HistoryOf = False
                        SelectedImpressionsOU.OrderId = 99
                    End If
                End If
            End If
        End If
    Loop
    FM.CloseFile CLng(iFreeFile)
End If
Call SetNotes(PatientId, AppointmentId)
    Set oImpression = Nothing

    Exit Sub

lLoadAvailableImpressions_Error:

    LogError "frmSetDiagnosis", "LoadAvailableImpressions", Err, Err.Description
End Sub

Private Sub cmdSumm_Click()
    On Error GoTo lcmdSumm_Click_Error

frmFinalNew.PatientId = PatientId
frmFinalNew.AppointmentId = AppointmentId
frmFinalNew.ActivityId = globalExam.iActivityId
If (frmFinalNew.LoadFinal(True)) Then
    frmFinalNew.Show 1
End If

    Exit Sub

lcmdSumm_Click_Error:

    LogError "frmSetDiagnosis", "cmdSumm_Click", Err, Err.Description, , PatientId, AppointmentId
End Sub

Private Sub Form_Initialize()
    On Error GoTo lForm_Initialize_Error

    InitializeImpressions

    Exit Sub

lForm_Initialize_Error:

    LogError "frmSetDiagnosis", "Form_Initialize", Err, Err.Description
End Sub

Private Sub Form_Load()
    On Error GoTo lForm_Load_Error

    OrderingODOn = False
    OrderingOSOn = False
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = &H9B9626
    BaseTextColor = &HFFFFFF
    fIsModified = False

    Exit Sub

lForm_Load_Error:

    LogError "frmSetDiagnosis", "Form_Load", Err, Err.Description
End Sub

Private Sub LoadImpressionsFromFile(Optional fDeleteFile As Boolean = True)
Dim k As Integer
Dim MyIdx As Integer
Dim Rec As String
Dim RetId As String
Dim RetEye As String
Dim RetQuan As String
Dim RetSevA As String
Dim RetSevB As String
Dim RetA As String
Dim RetB As String
Dim RetC As String
Dim RetD As String
Dim RetO As String
Dim RetAct As Boolean
Dim RetNeg As Boolean
Dim RetRul As Boolean
Dim RetHst As Boolean
Dim RetString As String
Dim TheFile As String
Dim FileNum As Integer
    On Error GoTo lLoadImpressionsFromFile_Error

TheFile = DoctorInterfaceDirectory + "DiagImpr" + "_" + Trim$(str$(AppointmentId)) + "_" + Trim$(str$(PatientId)) + ".txt"
If FM.IsFileThere(TheFile) Then
    CountTotalImpressions
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    While Not (EOF(FileNum))
        Line Input #FileNum, Rec
        k = InStrPS(Rec, "-")
        If (k > 1) Then
            RetId = Left$(Rec, k - 1)
            RetEye = Mid$(Rec, k + 1, 2)
            RetString = Mid$(Rec, k + 3, Len(Rec) - (k + 2))
            Line Input #FileNum, RetQuan
            Line Input #FileNum, RetSevA
            Line Input #FileNum, RetSevB
            Line Input #FileNum, RetA
            Line Input #FileNum, RetB
            Line Input #FileNum, RetC
            Line Input #FileNum, RetD
            If Not (EOF(FileNum)) Then
                Line Input #FileNum, RetO
            End If
            RetAct = True
            If (RetA = "F") Then
                RetAct = False
            End If
            If (RetId = "N") Then
                RetId = "N-" + RetString
            End If
            RetNeg = False
            If (RetB = "T") Then
                RetNeg = True
            End If
            RetRul = False
            If (RetC = "T") Then
                RetRul = True
            End If
            RetHst = False
            If (RetD = "T") Then
                RetHst = True
            End If
            If IsImprSet(RetId, RetEye) = -1 Then
                If RetEye = "OD" Then
                    If TotalImpressionsOD < MaxImpressions Then
                        SelectedImpressionsOD(TotalImpressionsOD).Id = RetId
                        If Left$(RetId, 1) = "N" Then
                            SelectedImpressionsOD(TotalImpressionsOD).Id = Left$(RetId, 1)
                        End If
                        SelectedImpressionsOD(TotalImpressionsOD).Name = RetString
                        SelectedImpressionsOD(TotalImpressionsOD).Eye = RetEye
                        SelectedImpressionsOD(TotalImpressionsOD).Quant = RetQuan
                        SelectedImpressionsOD(TotalImpressionsOD).ImprOn = False
                        SelectedImpressionsOD(TotalImpressionsOD).SeverityA = Trim$(RetSevA)
                        SelectedImpressionsOD(TotalImpressionsOD).SeverityB = Trim$(RetSevB)
                        SelectedImpressionsOD(TotalImpressionsOD).OrderId = Val(RetO)
                        If RetD = "T" And Val(RetO) < 1 Then
                            MyIdx = GetNextOrder("OD")
                            SelectedImpressionsOD(TotalImpressionsOD).OrderId = MyIdx
                        End If
                        SelectedImpressionsOD(TotalImpressionsOD).Active = RetAct
                        SelectedImpressionsOD(TotalImpressionsOD).IgnoreEye = False
                        SelectedImpressionsOD(TotalImpressionsOD).NegativeFinding = RetNeg
                        SelectedImpressionsOD(TotalImpressionsOD).RuleOut = RetRul
                        SelectedImpressionsOD(TotalImpressionsOD).HistoryOf = RetHst
                        TotalImpressionsOD = TotalImpressionsOD + 1
                    End If
                ElseIf (RetEye = "OS") Then
                    If TotalImpressionsOS < MaxImpressions Then
                        SelectedImpressionsOS(TotalImpressionsOS).Id = RetId
                        If Left$(RetId, 1) = "N" Then
                            SelectedImpressionsOS(TotalImpressionsOS).Id = Left$(RetId, 1)
                        End If
                        SelectedImpressionsOS(TotalImpressionsOS).Name = RetString
                        SelectedImpressionsOS(TotalImpressionsOS).Eye = RetEye
                        SelectedImpressionsOS(TotalImpressionsOS).Quant = RetQuan
                        SelectedImpressionsOS(TotalImpressionsOS).ImprOn = False
                        SelectedImpressionsOS(TotalImpressionsOS).SeverityA = Trim$(RetSevA)
                        SelectedImpressionsOS(TotalImpressionsOS).SeverityB = Trim$(RetSevB)
                        SelectedImpressionsOS(TotalImpressionsOS).OrderId = Val(RetO)
                        If RetD = "T" And Val(RetO) < 1 Then
                            MyIdx = GetNextOrder("OS")
                            SelectedImpressionsOS(TotalImpressionsOS).OrderId = MyIdx
                        End If
                        SelectedImpressionsOS(TotalImpressionsOS).Active = RetAct
                        SelectedImpressionsOS(TotalImpressionsOS).IgnoreEye = False
                        SelectedImpressionsOS(TotalImpressionsOS).NegativeFinding = RetNeg
                        SelectedImpressionsOS(TotalImpressionsOS).RuleOut = RetRul
                        SelectedImpressionsOS(TotalImpressionsOS).HistoryOf = RetHst
                        TotalImpressionsOS = TotalImpressionsOS + 1
                    End If
                End If
            End If
        End If
    Wend
    FM.CloseFile CLng(FileNum)
    If fDeleteFile Then
        FM.Kill TheFile
    End If
End If

    Exit Sub

lLoadImpressionsFromFile_Error:

    LogError "frmSetDiagnosis", "LoadImpressionsFromFile", Err, Err.Description, , PatientId, AppointmentId
End Sub

Private Function LoadImpressionsFromDB(iAppointmentId As Long, iPatientId As Long) As Boolean
Dim Clinical As PatientClinical
Dim i As Long
Dim AId As Long
Dim UTemp As String
Dim LDiag As String
Dim TheSystem As String
Dim TheText As String
Dim ZTemp As String
Dim QTemp As String
Dim ImprOn As Boolean
Dim p As Long
Dim q As Long

    On Error GoTo lLoadImpressionsFromDB_Error

    If iAppointmentId > 0 And iPatientId > 0 Then
        CountTotalImpressions
        Set Clinical = New PatientClinical
        Clinical.PatientId = iPatientId
        Clinical.AppointmentId = iAppointmentId
        Clinical.ClinicalType = "U"
        If Clinical.FindDoctorClinical > 0 Then
            i = 1
            While Clinical.SelectPatientClinical(i)
                UTemp = Trim$(Clinical.Symptom)
                LDiag = Trim$(Clinical.Findings)
                TheSystem = ""
                TheText = ""
                ZTemp = ""
                If (InStrPS(UTemp, "!") <> 0) Then
                    ZTemp = ZTemp + CntRef
                End If
                If (InStrPS(UTemp, "%") <> 0) Then
                    ZTemp = ZTemp + HORef
                End If
                If (InStrPS(UTemp, "#") <> 0) Then
                    ZTemp = ZTemp + RORef
                End If
                QTemp = ""
                If (InStrPS(UTemp, "[") <> 0) Then
                    p = InStrPS(UTemp, "[")
                    q = InStrPS(UTemp, "]")
                    If (p > 0) And (q > 0) Then
                        QTemp = Mid$(UTemp, p, q - (p - 1))
                    End If
                End If
                If Clinical.EyeContext = "OD" Then
                    If GetPrimaryDiagnosis(LDiag, TheSystem, TheText, False, ImprOn, AId) Then
                        If AId > 0 And Clinical.EyeContext = "OD" Then
                            SelectedImpressionsOD(TotalImpressionsOD).Id = LDiag
                            SelectedImpressionsOD(TotalImpressionsOD).Name = TheText
                            SelectedImpressionsOD(TotalImpressionsOD).ImprOn = ImprOn
                            SelectedImpressionsOD(TotalImpressionsOD).Eye = "OD"
                            SelectedImpressionsOD(TotalImpressionsOD).Quant = QTemp
                            SelectedImpressionsOD(TotalImpressionsOD).Active = True
                            SelectedImpressionsOD(TotalImpressionsOD).IgnoreEye = False
                            SelectedImpressionsOD(TotalImpressionsOD).NegativeFinding = InStrPS(ZTemp, CntRef)
                            SelectedImpressionsOD(TotalImpressionsOD).RuleOut = InStrPS(ZTemp, RORef)
                            SelectedImpressionsOD(TotalImpressionsOD).HistoryOf = InStrPS(ZTemp, HORef)
                            If Val(Left$(UTemp, 1)) <> TotalImpressionsOD + 1 Then
                                SelectedImpressionsOD(TotalImpressionsOD).OrderId = TotalImpressionsOD + 1
                                ' Update db here to fix old records?
                            Else
                                SelectedImpressionsOD(TotalImpressionsOD).OrderId = Val(Left$(UTemp, 1))
                            End If
                            TotalImpressionsOD = TotalImpressionsOD + 1
                        End If
                    End If
                ElseIf Clinical.EyeContext = "OS" Then
                    If GetPrimaryDiagnosis(LDiag, TheSystem, TheText, False, ImprOn, AId) Then
                        If AId > 0 And Clinical.EyeContext = "OS" Then
                            SelectedImpressionsOS(TotalImpressionsOS).Id = LDiag
                            SelectedImpressionsOS(TotalImpressionsOS).Name = TheText
                            SelectedImpressionsOS(TotalImpressionsOS).ImprOn = ImprOn
                            SelectedImpressionsOS(TotalImpressionsOS).Eye = "OS"
                            SelectedImpressionsOS(TotalImpressionsOS).Quant = QTemp
                            SelectedImpressionsOS(TotalImpressionsOS).Active = True
                            SelectedImpressionsOS(TotalImpressionsOS).IgnoreEye = False
                            SelectedImpressionsOS(TotalImpressionsOS).NegativeFinding = InStrPS(ZTemp, CntRef)
                            SelectedImpressionsOS(TotalImpressionsOS).RuleOut = InStrPS(ZTemp, RORef)
                            SelectedImpressionsOS(TotalImpressionsOS).HistoryOf = InStrPS(ZTemp, HORef)
                            If Val(Left$(UTemp, 1)) <> TotalImpressionsOS + 1 Then
                                SelectedImpressionsOS(TotalImpressionsOS).OrderId = TotalImpressionsOS + 1
                                'Update old records here?
                            Else
                                SelectedImpressionsOS(TotalImpressionsOS).OrderId = Val(Left$(UTemp, 1))
                            End If
                            TotalImpressionsOS = TotalImpressionsOS + 1
                        End If
                    End If
                End If
                i = i + 1
            Wend
            Set Clinical = Nothing
            LoadImpressionsFromDB = True
        End If
    End If

    Exit Function

lLoadImpressionsFromDB_Error:

    LogError "frmSetDiagnosis", "LoadImpressionsFromDB", Err, Err.Description, , iPatientId, iAppointmentId
End Function

Public Function RetainImpressions(PatId As Long) As Boolean
Dim u As Integer
Dim FileNum As Integer
Dim ROrd As Integer
Dim RetString As String, RetQuan As String
Dim RetId As String
Dim RetEye As String, RetSevA As String
Dim RetSevB As String, RetAct As String
Dim RetNeg As String, RetRul As String
Dim RetHst As String, TheFile As String
Dim RRul As Boolean, RHst As Boolean, RCnf As Boolean
    On Error GoTo lRetainImpressions_Error

If (PatId > 0) Then
' Always reorder the impressions so that the active ones are first
    ReOrderImpressions eerOU
    FileNum = FreeFile
    TheFile = DoctorInterfaceDirectory + "DiagImpr" + "_" + Trim$(str$(AppointmentId)) + "_" + Trim$(str$(PatId)) + ".txt"
    FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(FileNum)
    If TotalImpressionsOD > 0 Then
        For u = 0 To TotalImpressionsOD - 1
            If (GetImpressionsOD(u, RetId, RetString, RetEye, RetSevA, RetSevB, RetAct, RetQuan, RCnf, RHst, RRul, ROrd)) Then
                If (Left$(RetId, 1) = "N") Then
                    RetId = "N"
                End If
                RetNeg = "F"
                If (RCnf) Then
                    RetNeg = "T"
                End If
                RetRul = "F"
                If (RRul) Then
                    RetRul = "T"
                End If
                RetHst = "F"
                If (RHst) Then
                    RetHst = "T"
                End If
                Print #FileNum, RetId + "-" + RetEye + RetString
                Print #FileNum, RetQuan
                Print #FileNum, RetSevA
                Print #FileNum, RetSevB
                Print #FileNum, RetAct
                Print #FileNum, RetNeg
                Print #FileNum, RetRul
                Print #FileNum, RetHst
                Print #FileNum, Trim$(str$(ROrd))
            End If
        Next
    End If
    If TotalImpressionsOS > 0 Then
        For u = 0 To TotalImpressionsOS - 1
            If (GetImpressionsOS(u, RetId, RetString, RetEye, RetSevA, RetSevB, RetAct, RetQuan, RCnf, RHst, RRul, ROrd)) Then
                If (Left$(RetId, 1) = "N") Then
                    RetId = "N"
                End If
                RetNeg = "F"
                If (RCnf) Then
                    RetNeg = "T"
                End If
                RetRul = "F"
                If (RRul) Then
                    RetRul = "T"
                End If
                RetHst = "F"
                If (RHst) Then
                    RetHst = "T"
                End If
                Print #FileNum, RetId + "-" + RetEye + RetString
                Print #FileNum, RetQuan
                Print #FileNum, RetSevA
                Print #FileNum, RetSevB
                Print #FileNum, RetAct
                Print #FileNum, RetNeg
                Print #FileNum, RetRul
                Print #FileNum, RetHst
                Print #FileNum, Trim$(str$(ROrd))
            End If
        Next
    End If
    FM.CloseFile CLng(FileNum)
    RetainImpressions = True
End If

    Exit Function

lRetainImpressions_Error:

    LogError "frmSetDiagnosis", "RetainImpressions", Err, Err.Description, , PatientId, AppointmentId
End Function

Private Function RetrieveGeneralImpression(RecId As Long, AText As String) As Boolean
Dim RetDF As DynamicControls
    On Error GoTo lRetrieveGeneralImpression_Error

AText = ""
RetrieveGeneralImpression = False
If (RecId > 0) Then
    Set RetDF = New DynamicControls
    RetDF.ControlId = RecId
    If (RetDF.RetrieveControl) Then
        If (Trim$(RetDF.DoctorLingo) <> "") Then
            AText = Trim$(RetDF.DoctorLingo)
            Call StripCharacters(AText, "~")
            If (Trim$(AText) <> "") Then
                RetrieveGeneralImpression = True
            End If
        End If
    End If
    Set RetDF = Nothing
End If

    Exit Function

lRetrieveGeneralImpression_Error:

    LogError "frmSetDiagnosis", "RetrieveGeneralImpression", Err, Err.Description
End Function

Public Function PurgeRetainedList(PatId As Long, ABack As Boolean) As Boolean
Dim AFile As String
Dim OtherFile As String
    On Error GoTo lPurgeRetainedList_Error

PurgeRetainedList = True
If (PatId > 0) Then
    Call InitializeImpressions
    AFile = DoctorInterfaceDirectory + "DiagImpr" + "_" + Trim$(str$(AppointmentId)) + "_" + Trim$(str$(PatId)) + ".txt"
    OtherFile = DoctorInterfaceDirectory + "Backup\DiagImpr" + "_" + Trim$(str$(AppointmentId)) + "_" + Trim$(str$(PatId)) + ".txt"
    If FM.IsFileThere(AFile) Then
        If (ABack) Then
            FM.FileCopy AFile, OtherFile
        End If
        FM.Kill AFile
    End If
End If

    Exit Function

lPurgeRetainedList_Error:

    LogError "frmSetDiagnosis", "PurgeRetainedList", Err, Err.Description, , PatId, AppointmentId
End Function

Private Function SetNotes(PatId As Long, ApptId As Long) As Boolean
Dim i As Integer
Dim TheText As String
Dim RetNotes As PatientNotes
    On Error GoTo lSetNotes_Error

txtImpr.Text = ""
If (PatId > 0) And (ApptId > 0) Then
    Set RetNotes = New PatientNotes
    RetNotes.NotesPatientId = PatId
    RetNotes.NotesAppointmentId = ApptId
    RetNotes.NotesSystem = "IMPR"
    RetNotes.NotesType = "C"
    If (RetNotes.FindNotes > 0) Then
        i = 1
        While (RetNotes.SelectNotes(i))
            TheText = ""
            If (i > 1) Then
                TheText = " "
            End If
            If (Trim$(RetNotes.NotesText1) <> "") Then
                TheText = TheText + Trim$(RetNotes.NotesText1)
            End If
            If (Trim$(RetNotes.NotesText2) <> "") Then
                TheText = TheText + " " + Trim$(RetNotes.NotesText2)
            End If
            If (Trim$(RetNotes.NotesText3) <> "") Then
                TheText = TheText + " " + Trim$(RetNotes.NotesText3)
            End If
            If (Trim$(RetNotes.NotesText4) <> "") Then
                TheText = TheText + " " + Trim$(RetNotes.NotesText4)
            End If
            txtImpr.Text = txtImpr.Text + TheText
            i = i + 1
        Wend
    End If
    Set RetNotes = Nothing
End If

    Exit Function

lSetNotes_Error:

    LogError "frmSetDiagnosis", "SetNotes", Err, Err.Description, , PatId, ApptId
End Function

Private Function GetNextOrder(AEye As String) As Integer
'Counts active impressions and returns their number plus 1
Dim i As Integer
Dim MyMax As Integer
    On Error GoTo lGetNextOrder_Error

    If AEye = "OD" Then
        For i = 0 To TotalImpressionsOD - 1
            If SelectedImpressionsOD(i).Active Then
                MyMax = MyMax + 1
            End If
        Next
        MyMax = MyMax + 1
    ElseIf AEye = "OS" Then
        For i = 0 To TotalImpressionsOS - 1
            If SelectedImpressionsOS(i).Active Then
                MyMax = MyMax + 1
            End If
        Next
        MyMax = MyMax + 1
    End If
    GetNextOrder = MyMax

    Exit Function

lGetNextOrder_Error:

    LogError "frmSetDiagnosis", "GetNextOrder", Err, Err.Description
End Function

Public Sub ReOrderImpressions(EyeRef As EEyeRef)
Dim i As Integer
Dim LocalIdx As Integer
    On Error GoTo lReOrderImpressions_Error

CountTotalImpressions
CountActiveImpressions
If EyeRef = eerOD Or EyeRef = eerOU Then
    Erase SelectedImpressionsODTemp
    LocalIdx = 1
    If ActiveImpressionsOD > 0 Then
        For LocalIdx = 1 To ActiveImpressionsOD
            For i = 0 To MaxImpressions - 1
                If SelectedImpressionsOD(i).Active Then
                    If SelectedImpressionsOD(i).OrderId = LocalIdx Then
                        SelectedImpressionsODTemp(LocalIdx - 1).Copy SelectedImpressionsOD(i)
                        Exit For
                    End If
                End If
            Next
        Next
    End If
    For i = 0 To TotalImpressionsOD - 1
        If Not SelectedImpressionsOD(i).Active Then
            SelectedImpressionsODTemp(LocalIdx - 1).Copy SelectedImpressionsOD(i)
            SelectedImpressionsODTemp(LocalIdx - 1).OrderId = 0
            LocalIdx = LocalIdx + 1
        End If
    Next
    For i = 0 To TotalImpressionsOD - 1
        SelectedImpressionsOD(i).Copy SelectedImpressionsODTemp(i)
    Next
End If
If EyeRef = eerOS Or EyeRef = eerOU Then
    Erase SelectedImpressionsOSTemp
    LocalIdx = 1
    If ActiveImpressionsOS > 0 Then
        For LocalIdx = 1 To ActiveImpressionsOS
            For i = 0 To MaxImpressions - 1
                If SelectedImpressionsOS(i).Active Then
                    If SelectedImpressionsOS(i).OrderId = LocalIdx Then
                        SelectedImpressionsOSTemp(LocalIdx - 1).Copy SelectedImpressionsOS(i)
                        Exit For
                    End If
                End If
            Next
        Next
    End If
    For i = 0 To TotalImpressionsOS - 1
        If Not SelectedImpressionsOS(i).Active Then
            SelectedImpressionsOSTemp(LocalIdx - 1).Copy SelectedImpressionsOS(i)
            SelectedImpressionsOSTemp(LocalIdx - 1).OrderId = 0
            LocalIdx = LocalIdx + 1
        End If
    Next
    For i = 0 To TotalImpressionsOS - 1
        SelectedImpressionsOS(i).Copy SelectedImpressionsOSTemp(i)
    Next
End If

    Exit Sub

lReOrderImpressions_Error:

    LogError "frmSetDiagnosis", "ReOrderImpressions", Err, Err.Description
End Sub

Public Sub SetImpressionsOrder(Idx As Integer, ADiag As String, AEye As String)
Dim i As Integer
Dim myI As Long
    On Error GoTo lSetImpressionsOrder_Error

CountTotalImpressions
If (ADiag <> "") And (Idx > 0) Then
    If (AEye = "OD") Then
        For i = 0 To TotalImpressionsOD - 1
            If (SelectedImpressionsOD(i).Active) Then
                If (ADiag = Trim$(SelectedImpressionsOD(i).Id)) Then
                    SelectedImpressionsOD(i).OrderId = Idx
                    myI = i
                    Exit For
                End If
            End If
        Next
        If (myI > -1) Then
            For i = 0 To TotalImpressionsOD - 1
                If (Idx >= SelectedImpressionsOD(i).OrderId) And (i <> myI) Then
                    SelectedImpressionsOD(i).OrderId = SelectedImpressionsOD(i).OrderId + 1
                End If
            Next
        End If
    ElseIf (AEye = "OS") Then
        For i = 0 To TotalImpressionsOS - 1
            If (SelectedImpressionsOD(i).Active) Then
                If (ADiag = Trim$(SelectedImpressionsOS(i).Id)) Then
                    SelectedImpressionsOS(i).OrderId = Idx
                    myI = i
                    Exit For
                End If
            End If
        Next
        If (myI > -1) Then
            For i = 0 To TotalImpressionsOS - 1
                If (Idx >= SelectedImpressionsOS(i).OrderId) And (i <> myI) Then
                    SelectedImpressionsOS(i).OrderId = SelectedImpressionsOS(i).OrderId + 1
                End If
            Next
        End If
    End If
End If

    Exit Sub

lSetImpressionsOrder_Error:

    LogError "frmSetDiagnosis", "SetImpressionsOrder", Err, Err.Description
End Sub

Public Function LoadSelectedImpressions() As Boolean
    On Error GoTo lLoadSelectedImpressions_Error

    Call InitializeImpressions
    LoadImpressionsFromFile False
    LoadSelectedImpressions = True

    Exit Function

lLoadSelectedImpressions_Error:

    LogError "frmSetDiagnosis", "LoadSelectedImpressions", Err, Err.Description
End Function

Public Function OrderImpressionsByDiagnoses() As Boolean
Dim asDiags() As String
Dim i As Long
Dim iOrderOD As Long
Dim iOrderOS As Long

    On Error GoTo lOrderImpressionsByDiagnoses_Error

    i = 1
    iOrderOD = 1
    iOrderOS = 1
    If LoadSelectedImpressions Then
        CountTotalImpressions
        Erase SelectedImpressionsODTemp
        Erase SelectedImpressionsOSTemp
        asDiags() = LoadBillDiagnosesOrderFromFile()
        Do While asDiags(i) <> ""
            Select Case Mid$(asDiags(i), 1, 2)
                Case "OU"
                    If SetNextImprOD(Mid$(asDiags(i), 4, 6), iOrderOD) Then
                        'iOrderOD = iOrderOD + 1
                    End If
                    If SetNextImprOS(Mid$(asDiags(i), 4, 6), iOrderOS) Then
                        'iOrderOS = iOrderOS + 1
                    End If
                Case "OD"
                    If SetNextImprOD(Mid$(asDiags(i), 4, 6), iOrderOD) Then
                        'iOrderOD = iOrderOD + 1
                    End If
                Case "OS"
                    If SetNextImprOS(Mid$(asDiags(i), 4, 6), iOrderOS) Then
                        'iOrderOS = iOrderOS + 1
                    End If
            End Select
            i = i + 1
        Loop
        FillTempOD iOrderOD
        FillTempOS iOrderOS
        For i = 0 To TotalImpressionsOD - 1
            SelectedImpressionsOD(i).Copy SelectedImpressionsODTemp(i)
        Next
        For i = 0 To TotalImpressionsOS - 1
            SelectedImpressionsOS(i).Copy SelectedImpressionsOSTemp(i)
        Next
        RetainImpressions PatientId
        OrderImpressionsByDiagnoses = True
    End If

    Exit Function

lOrderImpressionsByDiagnoses_Error:

    LogError "frmSetDiagnosis", "OrderImpressionsByDiagnoses", Err, Err.Description
End Function

Private Function LoadBillDiagnosesOrderFromFile() As String()
Dim FileNum As Integer
Dim i As Long
Dim k As Long
Dim TheFile As String
Dim sRec As String
Dim sDiag As String
Dim sDiagEye As String
Dim asDiags(MaxImpressions) As String

    On Error GoTo lLoadBillDiagnosesOrderFromFile_Error

    If frmEvaluation.IsIcd10 Then
        TheFile = DoctorInterfaceDirectory + "DiagBill_Icd10" + "_" + Trim$(str$(globalExam.iAppointmentId)) + "_" + Trim$(str$(globalExam.iPatientId)) + ".txt"
    Else
        TheFile = DoctorInterfaceDirectory + "DiagBill" + "_" + Trim$(str$(globalExam.iAppointmentId)) + "_" + Trim$(str$(globalExam.iPatientId)) + ".txt"
    End If
    'TheFile = DoctorInterfaceDirectory + "DiagBill" + "_" + Trim$(str$(AppointmentId)) + "_" + Trim$(str$(PatientId)) + ".txt"
    
    If (FM.IsFileThere(TheFile)) Then
        FileNum = FreeFile
        'FM.Clear
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
        i = 0
        While Not (EOF(FileNum))
            Line Input #FileNum, sRec
            k = InStrPS(sRec, "-")
            If (k > 1) Then
                i = i + 1
                sDiag = Left$(sRec, k - 1)
                sDiagEye = Mid$(sRec, k + 1, 2)
                If (i <= MaxImpressions) Then
                    asDiags(i) = sDiagEye & "-" & sDiag
                End If
            End If
        Wend
        FM.CloseFile CLng(FileNum), False
        
    End If
    LoadBillDiagnosesOrderFromFile = asDiags

    Exit Function

lLoadBillDiagnosesOrderFromFile_Error:

    LogError "frmSetDiagnosis", "LoadBillDiagnosesOrderFromFile", Err, Err.Description, , PatientId, AppointmentId
End Function

Private Function SetNextImprOD(sDiag As String, ByRef iOrder As Long) As Boolean
'This is used to reorder the Impressions in SelectedImpressions into SelectedImpressionsTemp
Dim i As Long

    On Error GoTo lSetNextImprOD_Error

    For i = 0 To TotalImpressionsOD - 1
        If Mid$(SelectedImpressionsOD(i).Id, 1, 6) = sDiag Then
            SelectedImpressionsODTemp(iOrder - 1).Copy SelectedImpressionsOD(i)
            SelectedImpressionsODTemp(iOrder - 1).OrderId = iOrder
            iOrder = iOrder + 1
            SelectedImpressionsOD(i).Id = ""
            SetNextImprOD = True
        End If
    Next

    Exit Function

lSetNextImprOD_Error:

    LogError "frmSetDiagnosis", "SetNextImprOD", Err, Err.Description
End Function

Private Function SetNextImprOS(sDiag As String, ByRef iOrder As Long) As Boolean
'This is used to reorder the Impressions in SelectedImpressions into SelectedImpressionsTemp
Dim i As Long
    On Error GoTo lSetNextImprOS_Error

    For i = 0 To TotalImpressionsOS - 1
        If Mid$(SelectedImpressionsOS(i).Id, 1, 6) = sDiag Then
            SelectedImpressionsOSTemp(iOrder - 1).Copy SelectedImpressionsOS(i)
            SelectedImpressionsOSTemp(iOrder - 1).OrderId = iOrder
            iOrder = iOrder + 1
            SelectedImpressionsOS(i).Id = ""
            SetNextImprOS = True
        End If
    Next

    Exit Function

lSetNextImprOS_Error:

    LogError "frmSetDiagnosis", "SetNextImprOS", Err, Err.Description
End Function

Private Sub FillTempOD(ByRef iNextOrder As Long)
Dim i As Long
    On Error GoTo lFillTempOD_Error

    For i = 0 To TotalImpressionsOD - 1
        If SelectedImpressionsOD(i).Id <> "" Then
            If SelectedImpressionsOD(i).Active Then
                SelectedImpressionsODTemp(iNextOrder - 1).Copy SelectedImpressionsOD(i)
                SelectedImpressionsODTemp(iNextOrder - 1).OrderId = iNextOrder
                SelectedImpressionsOD(i).Id = ""
                iNextOrder = iNextOrder + 1
            End If
        End If
    Next

    Exit Sub

lFillTempOD_Error:

    LogError "frmSetDiagnosis", "FillTempOD", Err, Err.Description
End Sub

Private Sub FillTempOS(ByRef iNextOrder As Long)
Dim i As Long
    On Error GoTo lFillTempOS_Error

    For i = 0 To TotalImpressionsOS - 1
        If SelectedImpressionsOS(i).Id <> "" Then
            If SelectedImpressionsOS(i).Active Then
                SelectedImpressionsOSTemp(iNextOrder - 1).Copy SelectedImpressionsOS(i)
                SelectedImpressionsOSTemp(iNextOrder - 1).OrderId = iNextOrder
                SelectedImpressionsOS(i).Id = ""
                iNextOrder = iNextOrder + 1
            End If
        End If
    Next

    Exit Sub

lFillTempOS_Error:

    LogError "frmSetDiagnosis", "FillTempOS", Err, Err.Description
End Sub

Private Function OrderSelectedBySystem(sEye As String) As Boolean
Dim iMaxIndex As Long
Dim iTempIndex As Long
Dim iDiagnosisCount As Long
Dim iDiagnosisIndex As Long
Dim iCurrentOrder As Long
Dim sDiagnoses As String
Dim Primary As DiagnosisMasterPrimary
    On Error GoTo lOrderSelectedBySystem_Error

    Set Primary = New DiagnosisMasterPrimary
    If (sEye = "OD") Then
        Erase SelectedImpressionsODTemp
        iMaxIndex = TotalImpressionsOD
        If (iMaxIndex > 1) Then
            For iTempIndex = 0 To iMaxIndex - 1
                If SelectedImpressionsOD(iTempIndex).Active Then
                    sDiagnoses = sDiagnoses & "," & SelectedImpressionsOD(iTempIndex).Id
                End If
            Next
            sDiagnoses = Mid$(sDiagnoses, 2)
            Primary.PrimaryNextLevelDiagnosis = sDiagnoses
            iDiagnosisCount = Primary.FindPrimaryOrderBySystem
            For iDiagnosisIndex = 1 To iDiagnosisCount
                If Primary.SelectPrimary(iDiagnosisIndex) Then
                    For iTempIndex = 0 To iMaxIndex - 1
                        If SelectedImpressionsOD(iTempIndex).Id = Primary.PrimaryNextLevelDiagnosis Then
                            iCurrentOrder = iCurrentOrder + 1
                            Set SelectedImpressionsODTemp(iCurrentOrder - 1) = New CImpression
                            SelectedImpressionsODTemp(iCurrentOrder - 1).Copy SelectedImpressionsOD(iTempIndex)
                            SelectedImpressionsODTemp(iCurrentOrder - 1).OrderId = iCurrentOrder
                            SelectedImpressionsOD(iTempIndex).Id = ""
                            Exit For
                        End If
                    Next
                End If
            Next
            For iTempIndex = 0 To iMaxIndex - 1
                If SelectedImpressionsOD(iTempIndex).Id <> "" Then
                    iCurrentOrder = iCurrentOrder + 1
                    SelectedImpressionsODTemp(iCurrentOrder - 1).Copy SelectedImpressionsOD(iTempIndex)
                End If
            Next
            For iTempIndex = 0 To iMaxIndex - 1
                SelectedImpressionsOD(iTempIndex).Copy SelectedImpressionsODTemp(iTempIndex)
            Next
        End If
    ElseIf (sEye = "OS") Then
        Erase SelectedImpressionsOSTemp
        iMaxIndex = TotalImpressionsOS
        If (iMaxIndex > 1) Then
            For iTempIndex = 0 To iMaxIndex - 1
                If SelectedImpressionsOS(iTempIndex).Active Then
                    sDiagnoses = sDiagnoses & "," & SelectedImpressionsOS(iTempIndex).Id
                End If
            Next
            sDiagnoses = Mid$(sDiagnoses, 2)
            Primary.PrimaryNextLevelDiagnosis = sDiagnoses
            iDiagnosisCount = Primary.FindPrimaryOrderBySystem
            For iDiagnosisIndex = 1 To iDiagnosisCount
                If Primary.SelectPrimary(iDiagnosisIndex) Then
                    For iTempIndex = 0 To iMaxIndex - 1
                        If SelectedImpressionsOS(iTempIndex).Id = Primary.PrimaryNextLevelDiagnosis Then
                            iCurrentOrder = iCurrentOrder + 1
                            Set SelectedImpressionsOSTemp(iCurrentOrder - 1) = New CImpression
                            SelectedImpressionsOSTemp(iCurrentOrder - 1).Copy SelectedImpressionsOS(iTempIndex)
                            SelectedImpressionsOSTemp(iCurrentOrder - 1).OrderId = iCurrentOrder
                            SelectedImpressionsOS(iTempIndex).Id = ""
                            Exit For
                        End If
                    Next
                End If
            Next
            For iTempIndex = 0 To iMaxIndex - 1
                If SelectedImpressionsOS(iTempIndex).Id <> "" Then
                    iCurrentOrder = iCurrentOrder + 1
                    SelectedImpressionsOSTemp(iCurrentOrder - 1).Copy SelectedImpressionsOS(iTempIndex)
                End If
            Next
            For iTempIndex = 0 To iMaxIndex - 1
                SelectedImpressionsOS(iTempIndex).Copy SelectedImpressionsOSTemp(iTempIndex)
            Next
        End If
    End If
    Set Primary = Nothing
    RetainImpressions PatientId
    LoadImpressions True, ViewOn, False, True, True

    Exit Function

lOrderSelectedBySystem_Error:

    LogError "frmSetDiagnosis", "OrderSelectedBySystem", Err, Err.Description
End Function

Private Function ReadBringForward(iPatId As Long, iAppointmentId As Long) As Boolean
Dim FileNum As Long
Dim Temp As String
Dim TheFile As String
    On Error GoTo lReadBringForward_Error

    If iPatId > 0 Then
        If iAppointmentId > 0 Then
            FileNum = FreeFile
            TheFile = DoctorInterfaceDirectory & "BringForward" & "_" & Trim$(str$(iAppointmentId)) & "_" & Trim$(str$(iPatId)) & ".txt"
            If FM.IsFileThere(TheFile) Then
                FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
                If Not (EOF(FileNum)) Then
                    Line Input #FileNum, Temp
                    If Temp = "BRINGFORWARD=T" Then
                        ReadBringForward = True
                    End If
                End If
                FM.CloseFile CLng(FileNum)
            End If
        End If
    End If

    Exit Function

lReadBringForward_Error:

    LogError "frmSetDiagnosis", "ReadBringForward", Err, Err.Description, , iPatId, iAppointmentId
End Function

Public Function PostImpressions(iPatId As Long, iAppointmentId As Long) As Boolean
Dim Totals As Long
Dim i As Integer

    On Error GoTo lPostImpressions_Error

    CountTotalImpressions

    Totals = TotalImpressionsOD
    For i = 0 To Totals - 1
        If SelectedImpressionsOD(i).Active Then
            SelectedImpressionsOD(i).Post iPatId, iAppointmentId
        End If
    Next
    
    Totals = TotalImpressionsOS
    For i = 0 To Totals - 1
        If SelectedImpressionsOS(i).Active Then
            SelectedImpressionsOS(i).Post iPatId, iAppointmentId
        End If
    Next

    Exit Function

lPostImpressions_Error:

    LogError "frmSetDiagnosis", "PostImpressions", Err, Err.Description, , iPatId, iAppointmentId

End Function

Public Function PurgeImpressions(iPatientId As Long, iAppointmentId As Long) As Boolean
Dim i As Integer
Dim Clinical As PatientClinical
    On Error GoTo lPurgeImpressions_Error

    If iPatientId > 0 And iAppointmentId > 0 Then
        Set Clinical = New PatientClinical
        Clinical.PatientId = iPatientId
        Clinical.AppointmentId = iAppointmentId
        Clinical.ClinicalType = "U"
        If Clinical.FindDoctorClinical > 0 Then
            i = 1
            While Clinical.SelectPatientClinical(i)
                Clinical.DeletePatientClinical
                i = i + 1
            Wend
        End If
        Set Clinical = Nothing
    End If

    Exit Function

lPurgeImpressions_Error:

    LogError "frmSetDiagnosis", "PurgeImpressions", Err, Err.Description, , iPatientId, iAppointmentId
End Function

Private Function ClearImpressionsButtons(Optional EyeRef As EEyeRef = eerOU) As Boolean
Dim i As Long
    On Error GoTo lClearImpressionsButtons_Error

    If EyeRef = eerOD Or EyeRef = eerOU Then
        For i = 0 To MAX_OD_BUTTONS - 1
            cmdImpressionOD(i).Text = ""
            cmdImpressionOD(i).Tag = ""
            cmdImpressionOD(i).Visible = False
            cmdImpressionOD(i).BackColor = BaseBackColor
            cmdImpressionOD(i).ForeColor = BaseTextColor
        Next
    End If
    If EyeRef = eerOS Or EyeRef = eerOU Then
        For i = 0 To MAX_OS_BUTTONS - 1
            cmdImpressionOS(i).Text = ""
            cmdImpressionOS(i).Tag = ""
            cmdImpressionOS(i).Visible = False
            cmdImpressionOS(i).BackColor = BaseBackColor
            cmdImpressionOS(i).ForeColor = BaseTextColor
        Next
    End If
    
    ClearImpressionsButtons = True
    Exit Function

lClearImpressionsButtons_Error:

    LogError "frmSetDiagnosis", "ClearImpressionsButtons", Err, Err.Description
End Function

Private Function DisplayImpressionsButtons(Optional EyeRef As EEyeRef = eerOU, Optional iStartAtIndex As Long = 0) As Boolean
Dim i As Long
Dim ButtonCntOD As Long
Dim ButtonCntOS As Long
Dim ATemp As String
Dim MyOrder As String

   On Error GoTo lDisplayImpressionsButtons_Error

    CountTotalImpressions
    If EyeRef = eerOD Or EyeRef = eerOU Then
        i = iStartAtIndex
        While i < TotalImpressionsOD And ButtonCntOD < MAX_OD_BUTTONS
            DoEvents
            ATemp = ""
            If (SelectedImpressionsOD(i).NegativeFinding) Then
                ATemp = ATemp + CntRef
            End If
            If (SelectedImpressionsOD(i).HistoryOf) Then
                ATemp = ATemp + HORef
            End If
            If (SelectedImpressionsOD(i).RuleOut) Then
                ATemp = ATemp + RORef
            End If
            MyOrder = ""
            If (SelectedImpressionsOD(i).OrderId > 0) Then
                MyOrder = " - " + Trim$(str$(SelectedImpressionsOD(i).OrderId))
            End If
            SetButton ButtonCntOD, ATemp + Trim$(SelectedImpressionsOD(i).Quant) + Trim$(SelectedImpressionsOD(i).Name) + MyOrder, "OD", SelectedImpressionsOD(i).Id
            ButtonCntOD = ButtonCntOD + 1
            i = i + 1
        Wend
    End If
    If EyeRef = eerOS Or EyeRef = eerOU Then
        i = iStartAtIndex
        While i < TotalImpressionsOS And ButtonCntOS < MAX_OS_BUTTONS
            DoEvents
            ATemp = ""
            If (SelectedImpressionsOS(i).NegativeFinding) Then
                ATemp = ATemp + CntRef
            End If
            If (SelectedImpressionsOS(i).HistoryOf) Then
                ATemp = ATemp + HORef
            End If
            If (SelectedImpressionsOS(i).RuleOut) Then
                ATemp = ATemp + RORef
            End If
            MyOrder = ""
            If (SelectedImpressionsOS(i).OrderId > 0) Then
                MyOrder = " - " + Trim$(str$(SelectedImpressionsOS(i).OrderId))
            End If
            SetButton ButtonCntOS, ATemp + Trim$(SelectedImpressionsOS(i).Quant) + Trim$(SelectedImpressionsOS(i).Name) + MyOrder, "OS", SelectedImpressionsOS(i).Id
            ButtonCntOS = ButtonCntOS + 1
            i = i + 1
        Wend
    End If

    DisplayImpressionsButtons = True
    Exit Function

lDisplayImpressionsButtons_Error:

    LogError "frmSetDiagnosis", "DisplayImpressionsButtons", Err, Err.Description

End Function

Public Sub CountTotalImpressions()
Dim i As Integer
    
   On Error GoTo lCountTotalImpressions_Error

    TotalImpressionsOD = 0
    For i = 0 To MaxImpressions
        If SelectedImpressionsOD(i).IsSet Then
            TotalImpressionsOD = TotalImpressionsOD + 1
        End If
    Next
    TotalImpressionsOS = 0
    For i = 0 To MaxImpressions
        If SelectedImpressionsOS(i).IsSet Then
            TotalImpressionsOS = TotalImpressionsOS + 1
        End If
    Next

   Exit Sub

lCountTotalImpressions_Error:

    LogError "frmSetDiagnosis", "CountTotalImpressions", Err, Err.Description
End Sub

Public Sub CountActiveImpressions()
Dim i As Integer
    
   On Error GoTo lCountActiveImpressions_Error

    ActiveImpressionsOD = 0
    For i = 0 To MaxImpressions
        If SelectedImpressionsOD(i).Active Then
            ActiveImpressionsOD = ActiveImpressionsOD + 1
        End If
    Next
    ActiveImpressionsOS = 0
    For i = 0 To MaxImpressions
        If SelectedImpressionsOS(i).Active Then
            ActiveImpressionsOS = ActiveImpressionsOS + 1
        End If
    Next

   Exit Sub

lCountActiveImpressions_Error:

    LogError "frmSetDiagnosis", "CountActiveImpressions", Err, Err.Description
End Sub

Public Sub LoadImpressionsFromSource(iAppointmentId As Long, iPatientId As Long)

   On Error GoTo lLoadImpressionsFromSource_Error

    Call InitializeImpressions
    LoadImpressionsFromDB iAppointmentId, iPatientId

   Exit Sub

lLoadImpressionsFromSource_Error:

    LogError "frmSetDiagnosis", "LoadImpressionsFromSource", Err, Err.Description, , iPatientId, iAppointmentId

End Sub

Private Function HasFindingChanged(iIndex As Integer, eerER As EEyeRef, sNewQuantifier As String, oImpression As CImpression) As Boolean
    
    On Error GoTo lHasFindingChanged_Error
    Select Case eerER
        Case EEyeRef.eerOD
            If SelectedImpressionsOD(iIndex).Quant <> sNewQuantifier Then
                HasFindingChanged = True
            End If
            If SelectedImpressionsOD(iIndex).NegativeFinding <> oImpression.NegativeFinding Then
                HasFindingChanged = True
            End If
            If SelectedImpressionsOD(iIndex).RuleOut <> oImpression.RuleOut Then
                HasFindingChanged = True
            End If
            If SelectedImpressionsOD(iIndex).HistoryOf <> oImpression.HistoryOf Then
                HasFindingChanged = True
            End If
        Case EEyeRef.eerOS
            If SelectedImpressionsOS(iIndex).Quant <> sNewQuantifier Then
                HasFindingChanged = True
            End If
            If SelectedImpressionsOS(iIndex).NegativeFinding <> oImpression.NegativeFinding Then
                HasFindingChanged = True
            End If
            If SelectedImpressionsOS(iIndex).RuleOut <> oImpression.RuleOut Then
                HasFindingChanged = True
            End If
            If SelectedImpressionsOS(iIndex).HistoryOf <> oImpression.HistoryOf Then
                HasFindingChanged = True
            End If
    End Select

    Exit Function

lHasFindingChanged_Error:

    LogError "frmSetDiagnosis", "HasFindingChanged", Err, Err.Description
End Function

Private Function UpdateImpression(iIndex As Integer, eerER As EEyeRef, sNewQuantifier As String, oImpression As CImpression) As Boolean
    
    On Error GoTo lUpdateImpression_Error
    Select Case eerER
        Case EEyeRef.eerOD
            SelectedImpressionsOD(iIndex).Quant = sNewQuantifier
            SelectedImpressionsOD(iIndex).NegativeFinding = oImpression.NegativeFinding
            SelectedImpressionsOD(iIndex).RuleOut = oImpression.RuleOut
            SelectedImpressionsOD(iIndex).HistoryOf = oImpression.HistoryOf
        Case EEyeRef.eerOS
            SelectedImpressionsOS(iIndex).Quant = sNewQuantifier
            SelectedImpressionsOS(iIndex).NegativeFinding = oImpression.NegativeFinding
            SelectedImpressionsOS(iIndex).RuleOut = oImpression.RuleOut
            SelectedImpressionsOS(iIndex).HistoryOf = oImpression.HistoryOf
    End Select
    
    Exit Function

lUpdateImpression_Error:

    LogError "frmSetDiagnosis", "UpdateImpression", Err, Err.Description
End Function

Public Function DeleteImpression(sDiagCode As String, eerER As EEyeRef) As Boolean
Dim sEyeRef As String
Dim iPos As Integer
Dim iOrder As Integer
    On Error GoTo lDeleteImpression_Error
    
    If eerER = EEyeRef.eerOD Then sEyeRef = "OD"
    If eerER = EEyeRef.eerOS Then sEyeRef = "OS"
    If eerER = EEyeRef.eerOU Then sEyeRef = "OD"
    iPos = IsImprSet(sDiagCode, sEyeRef)
    If iPos > -1 Then
        iOrder = SelectedImpressionsOD(iPos).OrderId
        If RemoveImpression(iPos, eerER) Then
            AdvanceOrder iOrder, eerER
            ReOrderImpressions eerER
        End If
    End If
    RetainImpressions PatientId
    
    DeleteImpression = True
    Exit Function

lDeleteImpression_Error:

    LogError "frmSetDiagnosis", "DeleteImpression", Err, Err.Description, , PatientId
End Function

Private Function RemoveImpression(iPos As Integer, eerER As EEyeRef) As Boolean
    On Error GoTo lRemoveImpression_Error

    If eerER = EEyeRef.eerOD Then
        Set SelectedImpressionsOD(iPos) = New CImpression
    ElseIf eerER = EEyeRef.eerOS Then
        Set SelectedImpressionsOS(iPos) = New CImpression
    End If
    RemoveImpression = True

    Exit Function

lRemoveImpression_Error:

    LogError "frmSetDiagnosis", "RemoveImpression", Err, Err.Description
End Function

Private Function AdvanceOrder(iOrderFrom As Integer, eerER As EEyeRef) As Boolean
Dim iIndex As Integer
    On Error GoTo lAdvanceOrder_Error

    If iOrderFrom > 0 Then
        If eerER = EEyeRef.eerOD Then
            For iIndex = 0 To TotalImpressionsOD - 1
                If SelectedImpressionsOD(iIndex).Active Then
                    If SelectedImpressionsOD(iIndex).OrderId > iOrderFrom Then
                        SelectedImpressionsOD(iIndex).OrderId = SelectedImpressionsOD(iIndex).OrderId - 1
                    End If
                End If
            Next
        ElseIf eerER = EEyeRef.eerOS Then
            For iIndex = 0 To TotalImpressionsOS - 1
                If SelectedImpressionsOS(iIndex).Active Then
                    If SelectedImpressionsOS(iIndex).OrderId > iOrderFrom Then
                        SelectedImpressionsOS(iIndex).OrderId = SelectedImpressionsOS(iIndex).OrderId - 1
                    End If
                End If
            Next
        End If
    End If

    AdvanceOrder = True
    Exit Function

lAdvanceOrder_Error:

    LogError "frmSetDiagnosis", "AdvanceOrder", Err, Err.Description
    
End Function
Public Function FrmClose()
Call cmdHome_Click
  Unload Me
End Function
Public Function GetSelectedImpressions(ByRef SelectedImpressionsOSArg() As CImpression, ByRef SelectedImpressionsODArg() As CImpression, ByRef TotalImpressionsOSRef As Integer, ByRef TotalImpressionsODRef As Integer)
Dim i As Integer
For i = 0 To MaxImpressions
    Set SelectedImpressionsOSArg(i) = New CImpression
    Set SelectedImpressionsODArg(i) = New CImpression
Next
For i = 0 To TotalImpressionsOD - 1
        SelectedImpressionsODArg(i).Copy SelectedImpressionsOD(i)
Next
For i = 0 To TotalImpressionsOS - 1
        SelectedImpressionsOSArg(i).Copy SelectedImpressionsOS(i)
Next
TotalImpressionsODRef = TotalImpressionsOD
TotalImpressionsOSRef = TotalImpressionsOS
End Function
