VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmPatientLocator 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9210
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12210
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "PatientLocator.frx":0000
   ScaleHeight     =   9210
   ScaleWidth      =   12210
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstDoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "PatientLocator.frx":36C9
      Left            =   7560
      List            =   "PatientLocator.frx":36CB
      TabIndex        =   7
      Top             =   480
      Width           =   1935
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "PatientLocator.frx":36CD
      Left            =   9600
      List            =   "PatientLocator.frx":36CF
      TabIndex        =   5
      Top             =   480
      Width           =   1935
   End
   Begin VB.ListBox lstPayment 
      Height          =   450
      Left            =   2160
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   960
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstAppts 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5700
      ItemData        =   "PatientLocator.frx":36D1
      Left            =   240
      List            =   "PatientLocator.frx":36D3
      TabIndex        =   1
      Top             =   1440
      Width           =   11295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9720
      TabIndex        =   2
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PatientLocator.frx":36D5
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   960
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   0
      BackColor       =   16777215
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      ForeColorSelected=   0
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin VB.Label lblDate 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Appointment Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   10
      Top             =   720
      Width           =   1560
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   7560
      TabIndex        =   8
      Top             =   240
      Width           =   570
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9600
      TabIndex        =   6
      Top             =   240
      Width           =   735
   End
   Begin VB.Label lblPrint 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   5280
      TabIndex        =   4
      Top             =   1080
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Patient Locator"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   4620
      TabIndex        =   3
      Top             =   240
      Width           =   2535
   End
End
Attribute VB_Name = "frmPatientLocator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public AppointmentDate As String
Public LocationId As Long
Public DoctorId As Long
Public PatientId As Long

Private ApplPatientId As Long
Private ApplLastName As String
Private ApplFirstName As String
Private ApplMiddleName As String
Private ApplNameRef As String
Private ApplBirthDate As String
Private ApplHPhone As String
Private ApplBPhone As String
Private ApplApptId As Long
Private ApplApptDate As String
Private ApplApptTime As Integer
Private ApplApptComments As String
Private ApplReferralId As Long
Private ApplApptType As String
Private ApplResourceId As Long
Private ApplResource As String
Private ApplPatType As String
Private ApplStatus As String
Private ApplActStatus As String
Private ApplLocId As Long
Private ApplSrvCode As String

Private ApplListTotal As Long
Private ApplListTbl As ADODB.Recordset

Private Sub cmdDone_Click()
    PatientId = 0
    AppointmentDate = ""
    Unload frmPatientLocator
End Sub

Private Sub Form_Load()
    PatientId = 0
End Sub

Private Sub lstAppts_Click()
Dim PatId As Long
Dim ApptId As Long
    If lstAppts.ListIndex >= 0 Then
        If UCase(Mid(lstAppts.List(lstAppts.ListIndex), 34, 4)) = "EXAM" Then
            frmEventMsgs.SetButtons "Action?", "Enter Previous Visit", "Patient Demo", "Quit", "Cancel Appt"
        Else
            frmEventMsgs.SetButtons "Action?", "Enter Previous Visit", "Patient Demo", "Quit"
        End If
        frmEventMsgs.Show 1
        Select Case frmEventMsgs.Result
            Case 1
                If Not UserLogin.HasPermission(epEditPreviousVisit) Then
                    frmEventMsgs.InfoMessage "Not Permissioned"
                    frmEventMsgs.Show 1
                Else
                    AppointmentDate = SSDateCombo1.DateValue
                    Call FormatTodaysDate(AppointmentDate, False)
                    PatId = ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
                    PatientId = PatId
                    Unload frmPatientLocator
                End If
            Case 2
                PatId = ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
                Dim PatientDemographics As New PatientDemographics
                PatientDemographics.PatientId = PatId
                If PatientDemographics.DisplayPatientInfoScreen Then
                    AppointmentLoadList
                End If
            Case 3
                If Not UserLogin.HasPermission(epCancelAppointmentFromExam) Then
                    frmEventMsgs.InfoMessage "Not Permissioned"
                    frmEventMsgs.Show 1
                Else
                PatId = ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
                ApptId = ApplGetApptId(lstAppts.List(lstAppts.ListIndex), False)
                If PatId > 0 And ApptId > 0 Then
                    CancelAppointment PatId, ApptId
                    AppointmentLoadList
                End If
                End If
        End Select
    End If
End Sub

Private Sub lstLoc_Click()
    If lstLoc.ListIndex >= 0 Then
        If lstLoc.ListIndex = 0 Then
            LocationId = -1
        Else
            LocationId = ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
        End If
        If frmPatientLocator.Visible Then
            AppointmentLoadList
        End If
    End If
End Sub

Private Sub lstDoc_Click()
    If lstDoc.ListIndex >= 0 Then
        DoctorId = ApplGetListResourceId(lstDoc.List(lstDoc.ListIndex))
        AppointmentLoadList
    End If
End Sub

Private Sub SSDateCombo1_Change()
    If Trim$(SSDateCombo1.Date) <> "" Then
        SSDateCombo1_KeyPress 13
    End If
End Sub

Private Sub SSDateCombo1_Click()
    If Trim(SSDateCombo1.Date) <> "" Then
        SSDateCombo1_KeyPress 13
    End If
End Sub

Private Sub SSDateCombo1_CloseUp()
    If Trim$(SSDateCombo1.Date) <> "" Then
        AppointmentDate = SSDateCombo1.DateValue
        Call FormatTodaysDate(AppointmentDate, False)
        If AppointmentDate = "" Then
            SSDateCombo1.Text = ""
        Else
            SSDateCombo1.Text = AppointmentDate
        End If
        AppointmentLoadList
    End If
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
Dim ATemp As String
    If KeyAscii = 13 Then
        If Trim$(SSDateCombo1.Date) <> "" And SSDateCombo1.DateValue <> SSDateCombo1.Date Then
            AppointmentDate = SSDateCombo1.DateValue
            Call FormatTodaysDate(AppointmentDate, False)
            If AppointmentDate = "" Then
                SSDateCombo1.Text = ""
            Else
                SSDateCombo1.Text = AppointmentDate
                AppointmentLoadList
            End If
        Else
            If AppointmentDate = "" Then
                SSDateCombo1.Text = ""
            ElseIf SSDateCombo1.DateValue = "" Then
                SSDateCombo1.Text = AppointmentDate
            Else
                AppointmentDate = SSDateCombo1.DateValue
                Call FormatTodaysDate(AppointmentDate, False)
                AppointmentDate = Mid$(AppointmentDate, 1, 2) + Mid$(AppointmentDate, 4, 2) + Mid$(AppointmentDate, 9, 2)
            End If
            ATemp = AppointmentDate
            If Len(AppointmentDate) > 10 Then
                ATemp = Mid$(AppointmentDate, 11, Len(AppointmentDate) - 10)
            ElseIf AppointmentDate = "" Then
                Call FormatTodaysDate(AppointmentDate, False)
                ATemp = Mid$(AppointmentDate, 1, 2) + Mid$(AppointmentDate, 4, 2) + Mid$(AppointmentDate, 9, 2)
            End If
            ATemp = Left$(ATemp, 4) + "20" + Mid$(ATemp, 5, 2)
            AppointmentDate = Mid$(ATemp, 1, 2) + "/" + Mid$(ATemp, 3, 2) + "/" + Mid$(ATemp, 5, 4)
            Call FormatTodaysDate(AppointmentDate, False)
            If AppointmentDate = "" And SSDateCombo1.DateValue <> SSDateCombo1.Date Then
                AppointmentDate = SSDateCombo1.Date
                Call FormatTodaysDate(AppointmentDate, False)
                SSDateCombo1.Text = AppointmentDate
            Else
                SSDateCombo1.Text = AppointmentDate
            End If
            AppointmentLoadList
        End If
    ElseIf KeyAscii = 27 Then
        PrintContent lstAppts, Label5.Caption & " For " & Trim(SSDateCombo1.DateValue)
    Else
        If KeyAscii > 47 And KeyAscii < 58 Then
            AppointmentDate = AppointmentDate & Chr(KeyAscii)
        ElseIf KeyAscii = vbKeyDelete And Len(AppointmentDate) > 0 Then
            AppointmentDate = Left$(AppointmentDate, Len(AppointmentDate) - 1)
        ElseIf KeyAscii = vbKeyDelete And Len(AppointmentDate) < 1 Then
            AppointmentDate = ""
        End If
    End If
End Sub

Public Function AppointmentLoadList() As Boolean
Dim i As Integer
Dim LocId As Long
Dim DocId As Long
Dim oLocalDate As New CIODateTime
    On Error GoTo lAppointmentLoadList_Error

    lstAppts.Clear
    If lstLoc.ListCount < 1 Then
        ApplLoadLocation lstLoc
        LocationId = dbPracticeId
    End If
    If lstDoc.ListCount < 1 Then
        ApplLoadStaff lstDoc
        DoctorId = UserLogin.iId
        DocId = -1
        For i = 0 To lstDoc.ListCount - 1
            If (DoctorId = Val(Trim(Mid(lstDoc.List(i), 76, 5)))) Then
                DocId = i
                Exit For
            End If
        Next i
        If (DocId < 0) Then
            DoctorId = -1
        End If
    End If
    If Trim$(AppointmentDate) <> "" Then
        oLocalDate.SetDateUnknown AppointmentDate
    Else
        oLocalDate.MyDateTime = Now
    End If
    ApplLocId = LocationId
    ApplResourceId = DoctorId
    ApplRetrievePatientLocatorList oLocalDate.GetIODate, lstAppts
    AppointmentLoadList = True
    If LocationId > -1 And Not frmPatientLocator.Visible Then
        For i = 0 To lstLoc.ListCount - 1
            If LocationId = Val(Trim$(Mid$(lstLoc.List(i), 56, 5))) Then
                LocId = i
                Exit For
            End If
        Next i
        If LocId > 0 Then
            lstLoc.ListIndex = LocId
        End If
    End If
    If DoctorId > -1 And Not frmPatientLocator.Visible Then
        For i = 0 To lstDoc.ListCount - 1
            If DoctorId = Val(Trim$(Mid$(lstDoc.List(i), 76, 5))) Then
                DocId = i
                Exit For
            End If
        Next i
        If DocId > 0 Then
            lstDoc.ListIndex = DocId
        End If
    End If
    Set oLocalDate = Nothing

    Exit Function

lAppointmentLoadList_Error:

    LogError "frmPatientLocator", "AppointmentLoadList", Err, Err.Description

End Function

Private Function ApplGetListResourceId(TheItem As String) As Long
    If (Len(TheItem) > 75) Then
        ApplGetListResourceId = Val(Trim(Mid(TheItem, 76, Len(TheItem) - 75)))
    ElseIf (Len(TheItem) > 55) Then
        ApplGetListResourceId = Val(Trim(Mid(TheItem, 56, Len(TheItem) - 55)))
    End If
End Function

Private Function ApplGetPatientId(TheItem As String) As Long
Dim k As Integer
    If (Trim(TheItem) > 75) Then
        k = InStrPS(75, TheItem, "/")
        If (k > 0) Then
            ApplGetPatientId = Val(Trim(Mid(TheItem, k + 1, Len(TheItem) - k)))
        End If
    End If
End Function

Private Function ApplGetApptId(TheItem As String, MyTest As Boolean) As Long
Dim k As Integer
Dim RetApt As SchedulerAppointment
    If (Trim(TheItem) > 75) Then
        k = InStrPS(75, TheItem, "/")
        If (k > 0) Then
            ApplGetApptId = Val(Trim(Mid(TheItem, 76, (k - 1) - 75)))
            If (ApplGetApptId > 0) Then
                Set RetApt = New SchedulerAppointment
                RetApt.AppointmentId = ApplGetApptId
                If (RetApt.RetrieveSchedulerAppointment) Then
                    If (MyTest) Then
                        If (InStrPS("PR", RetApt.AppointmentStatus) = 0) Then
                            ApplGetApptId = 0
                        End If
                    Else
                        If (RetApt.AppointmentStatus <> "A") Then
                            ApplGetApptId = 0
                        End If
                    End If
                End If
                Set RetApt = Nothing
            End If
        End If
    End If
End Function

Private Function ApplLoadLocation(ALstBox As ListBox) As Boolean
Dim i As Integer
Dim DisplayText As String
Dim RetPrc As PracticeName
Dim RetDr As SchedulerResource
    ApplLoadLocation = True
    ALstBox.Clear
    DisplayText = Space(56)
    Mid(DisplayText, 1, 13) = "All Locations"
    DisplayText = DisplayText + Trim(str(-1))
    ALstBox.AddItem DisplayText
    Set RetPrc = New PracticeName
    RetPrc.PracticeType = "P"
    RetPrc.PracticeName = Chr(1)
    If (RetPrc.FindPractice > 0) Then
        i = 1
        While (RetPrc.SelectPractice(i))
            DisplayText = Space(56)
            Mid(DisplayText, 1, 6) = "Office"
            If (Trim(RetPrc.PracticeLocationReference) <> "") Then
                Mid(DisplayText, 7, Len(Trim(RetPrc.PracticeLocationReference)) + 1) = "-" + Trim(RetPrc.PracticeLocationReference)
                DisplayText = DisplayText + Trim(str(1000 + RetPrc.PracticeId))
            Else
                DisplayText = DisplayText + Trim(str(0))
            End If
            ALstBox.AddItem DisplayText
            i = i + 1
        Wend
    End If
    Set RetPrc = Nothing
    Set RetDr = New SchedulerResource
    RetDr.ResourceName = Chr(1)
    If (RetDr.FindResourcebyRoom) Then
        i = 1
        While (RetDr.SelectResource(i))
            If (RetDr.ResourceServiceCode = "02") Then
                DisplayText = Space(75)
                Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
                DisplayText = DisplayText + Trim(str(RetDr.ResourceId))
                ALstBox.AddItem DisplayText
            End If
            i = i + 1
        Wend
    End If
    Set RetDr = Nothing
End Function

Private Function ApplLoadStaff(ALstBox As ListBox) As Boolean
Dim i As Integer
Dim DisplayText As String
Dim RetDr As SchedulerResource
    ApplLoadStaff = True
    ALstBox.Clear
    DisplayText = Space(75)
    Mid(DisplayText, 1, 13) = "All Resources"
    DisplayText = DisplayText + Trim(str(0))
    ALstBox.AddItem DisplayText
    Set RetDr = New SchedulerResource
    RetDr.ResourceName = Chr(1)
    If (RetDr.FindResourcebyDoctor) Then
        i = 1
        While (RetDr.SelectResource(i))
            DisplayText = Space(75)
            Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
            DisplayText = DisplayText + Trim(str(RetDr.ResourceId))
            ALstBox.AddItem DisplayText
            i = i + 1
        Wend
    End If
    Set RetDr = Nothing
End Function

Private Function ApplRetrievePatientLocatorList(ApptDate As String, ALstBox As ListBox) As Long
Dim i As Integer
Dim w As Integer
Dim ActId As Long
Dim Age As Integer
Dim Temp As String
Dim NewStatus As String
Dim CurStatus As String
Dim DisplayItem As String
    ApplRetrievePatientLocatorList = -1
    If (Trim(ApptDate) <> "") Then
        ApplRetrievePatientLocatorList = GetActivityList(ApptDate)
        ALstBox.Clear
        For i = 1 To ApplRetrievePatientLocatorList
            Call SelectList(i)
            DisplayItem = Space(75)
            If (ApplApptTime <> 0) Then
                Call ConvertMinutesToTime(ApplApptTime, Temp)
                Mid(DisplayItem, 1, 8) = Temp
            End If
            If (Len(Trim(ApplFirstName)) < 8) Then
                Mid(DisplayItem, 10, Len(Trim(ApplFirstName))) = Trim(ApplFirstName)
            Else
                Mid(DisplayItem, 10, 7) = Left(Trim(ApplFirstName), 7)
            End If
            If (Trim(ApplMiddleName) <> "") Then
                Mid(DisplayItem, 18, 1) = ApplMiddleName
            End If
            If (Len(Trim(ApplLastName)) < 12) Then
                Mid(DisplayItem, 20, Len(Trim(ApplLastName))) = Trim(ApplLastName)
            Else
                Mid(DisplayItem, 20, 12) = Left(Trim(ApplLastName), 12)
            End If
            CurStatus = ""
            If (InStrPS("PRDA", ApplStatus) <> 0) Or (ApplStatus = "") Then
                CurStatus = "Not In"
                If (ApplActStatus = "W") Then
                    CurStatus = "Quest "
                ElseIf (ApplActStatus = "M") Then
                    CurStatus = "Wait  "
                ElseIf (ApplActStatus = "E") Then
                    CurStatus = "Exam  "
                ElseIf (ApplActStatus = "H") Then
                    CurStatus = "ChkOut"
                ElseIf (ApplActStatus = "D") Then
                    CurStatus = "Dischg"
                End If
                If (lstLoc.ListIndex > 0) Then
                    NewStatus = ""
                    Call GetActivityStatus(ApplApptId, ApplApptDate, NewStatus, ActId)
                    If (Trim(NewStatus) <> Trim(CurStatus)) Then
                        CurStatus = NewStatus
                    End If
                End If
            ElseIf (ApplStatus = "F") Then
                CurStatus = "Left  "
            ElseIf (ApplStatus = "N") Then
                CurStatus = "Can N "
            ElseIf (ApplStatus = "S") Then
                CurStatus = "Can SD"
            ElseIf (ApplStatus = "O") Then
                CurStatus = "Can O "
            ElseIf (ApplStatus = "Y") Then
                CurStatus = "Can P "
            ElseIf (ApplStatus = "X") Then
                CurStatus = "Cancel"
            ElseIf (ApplStatus = "Q") Then
                CurStatus = "Can SP"
            Else
                CurStatus = "Unk   "
            End If
            Mid(DisplayItem, 33, 1) = " "
            Mid(DisplayItem, 34, 6) = Left(CurStatus, 6)
            Age = GetAge(ApplBirthDate)
            Mid(DisplayItem, 41, Len(Trim(str(Age)))) = Trim(str(Age))
            Mid(DisplayItem, 44, 8) = Trim(Left(ApplApptType, 8))
            If (lstLoc.ListIndex > 0) Then
                Temp = ""
                For w = 1 To lstLoc.ListCount - 1
                    If (Val(Trim(Mid(lstLoc.List(w), 57, Len(lstLoc.List(w)) - 56))) = ApplLocId) Then
                        Temp = Trim(Left(lstLoc.List(w), 55))
                        Exit For
                    End If
                Next w
            Else
                Temp = GetLocation(ApplLocId)
            End If
            If (Temp = "") Then
                Temp = "Office"
            End If
            Mid(DisplayItem, 53, 8) = Trim(Temp)
            Mid(DisplayItem, 62, 5) = ApplPatType
            Mid(DisplayItem, 68, 7) = Trim(Left(ApplResource, 7))
            DisplayItem = DisplayItem + Trim(str(ApplApptId)) + "/" + Trim(str(ApplPatientId))
            If (ApplApptTime > 0) Then
                ALstBox.AddItem DisplayItem
            End If
        Next i
    End If
End Function

Private Function GetActivityList(ApplDate As String) As Long
On Error GoTo DbErrorHandler
Dim PTemp(4) As String
    ApplListTotal = 0
    GetActivityList = -1
    If (Trim(ApplDate) <> "") Then
        Set ApplListTbl = Nothing
        Set ApplListTbl = CreateAdoRecordset
        PTemp(1) = Trim(ApplDate)
        If (ApplResourceId > 0) Then
            PTemp(2) = Trim(str(ApplResourceId))
        End If
        If (ApplLocId > -1) Then
            PTemp(3) = Trim(str(ApplLocId))
        End If
        MyPracticeRepositoryCmd.CommandType = adCmdStoredProc
        MyPracticeRepositoryCmd.CommandText = "PatientLocator"
        Set MyParameters = Nothing
        Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@AptDate")
        MyParameters.Type = adVarChar
        MyParameters.Direction = adParamInput
        MyParameters.Size = Len(Trim(PTemp(1)))
        MyParameters.Value = Trim(PTemp(1))
        MyPracticeRepositoryCmd.Parameters()(0) = MyParameters
        
        Set MyParameters = Nothing
        Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@LocId")
        MyParameters.Type = adInteger
        MyParameters.Direction = adParamInput
        MyParameters.Size = 4
        If (Trim(PTemp(3)) = "") Then
            PTemp(3) = "-1"
        End If
        MyParameters.Value = Int(Val(Trim(PTemp(3))))
        MyPracticeRepositoryCmd.Parameters()(1) = MyParameters
        
        Set MyParameters = Nothing
        Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@ResId")
        MyParameters.Type = adInteger
        MyParameters.Direction = adParamInput
        MyParameters.Size = 4
        If (Trim(PTemp(2)) = "") Then
            PTemp(2) = "-1"
        End If
        MyParameters.Value = Int(Val(Trim(PTemp(2))))
        MyPracticeRepositoryCmd.Parameters()(2) = MyParameters
        Call ApplListTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, adCmdStoredProc)
        If Not (ApplListTbl.EOF) Then
            ApplListTbl.MoveLast
            ApplListTotal = ApplListTbl.RecordCount
            GetActivityList = ApplListTotal
        End If
    End If
    Exit Function
DbErrorHandler:
End Function

Private Function SelectList(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
    SelectList = False
    If (ApplListTbl.EOF) Or (ListItem < 1) Or (ListItem > ApplListTotal) Then
        Exit Function
    End If
    ApplListTbl.MoveFirst
    ApplListTbl.Move ListItem - 1
    Call LoadApplList
    SelectList = True
    Exit Function
DbErrorHandler:
End Function

Private Sub LoadApplList()
    If (ApplListTbl("AppointmentId") <> "") Then
        ApplApptId = ApplListTbl("AppointmentId")
    Else
        ApplApptId = 0
    End If
    If (ApplListTbl("AppDate") <> "") Then
        ApplApptDate = ApplListTbl("AppDate")
    Else
        ApplApptDate = ""
    End If
    If (ApplListTbl("AppTime") <> "") Then
        ApplApptTime = ApplListTbl("AppTime")
    Else
        ApplApptTime = 0
    End If
    If (ApplListTbl("AppointmentType") <> "") Then
        ApplApptType = ApplListTbl("AppointmentType")
    Else
        ApplApptType = ""
    End If
    If (ApplListTbl("ResourceId2") <> "") Then
        ApplLocId = ApplListTbl("ResourceId2")
    Else
        ApplLocId = 0
    End If
    If (ApplListTbl("ResourceName") <> "") Then
        ApplResource = ApplListTbl("ResourceName")
    Else
        ApplResource = ""
    End If
    If (ApplListTbl("ScheduleStatus") <> "") Then
        ApplStatus = ApplListTbl("ScheduleStatus")
    Else
        ApplStatus = ""
    End If
    If (ApplListTbl("PatientId") <> "") Then
        ApplPatientId = ApplListTbl("PatientId")
    Else
        ApplPatientId = 0
    End If
    If (ApplListTbl("LastName") <> "") Then
        ApplLastName = ApplListTbl("LastName")
    Else
        ApplLastName = ""
    End If
    If (ApplListTbl("FirstName") <> "") Then
        ApplFirstName = ApplListTbl("FirstName")
    Else
        ApplFirstName = ""
    End If
    If (ApplListTbl("MiddleInitial") <> "") Then
        ApplMiddleName = ApplListTbl("MiddleInitial")
    Else
        ApplMiddleName = ""
    End If
    If (ApplListTbl("NameReference") <> "") Then
        ApplNameRef = ApplListTbl("NameReference")
    Else
        ApplNameRef = ""
    End If
    If (ApplListTbl("BirthDate") <> "") Then
        ApplBirthDate = ApplListTbl("BirthDate")
    Else
        ApplBirthDate = ""
    End If
    If (ApplListTbl("ServiceCode") <> "") Then
        ApplSrvCode = ApplListTbl("ServiceCode")
    Else
        ApplSrvCode = ""
    End If
    ApplHPhone = ""
    ApplBPhone = ""
    ApplApptComments = ""
    ApplReferralId = 0
    If (ApplListTbl("PatType") <> "") Then
        ApplPatType = ApplListTbl("PatType")
    Else
        ApplPatType = ""
    End If
    If (ApplListTbl("HomePhone") <> "") Then
        ApplHPhone = ApplListTbl("HomePhone")
    Else
        ApplHPhone = ""
    End If
    If (ApplListTbl("BusinessPhone") <> "") Then
        ApplBPhone = ApplListTbl("BusinessPhone")
    Else
        ApplBPhone = ""
    End If
    If (ApplListTbl("Status") <> "") Then
        ApplActStatus = ApplListTbl("Status")
    Else
        ApplActStatus = ""
    End If
End Sub

Public Function GetActivityStatus(ApptId As Long, TheDate As String, TheStatus As String, ActId As Long) As Boolean
Dim i As Integer
Dim RetAct As PracticeActivity
    ActId = 0
    TheStatus = "Not In"
    If (ApptId > 0) Then
        Set RetAct = New PracticeActivity
        RetAct.ActivityLocationId = -1
        RetAct.ActivityAppointmentId = ApptId
        RetAct.ActivityDate = ""
        RetAct.ActivityStatus = ""
        If (RetAct.FindActivity > 0) Then
            i = 1
            Do Until Not (RetAct.SelectActivity(i))
                GetActivityStatus = True
                ActId = RetAct.ActivityId
                If (RetAct.ActivityStatus = "W") Then
                    TheStatus = "Quest "
                ElseIf (RetAct.ActivityStatus = "M") Then
                    TheStatus = "Wait  "
                ElseIf (RetAct.ActivityStatus = "E") Then
                    TheStatus = "Exam  "
                ElseIf (RetAct.ActivityStatus = "H") Then
                    TheStatus = "ChkOut"
                ElseIf (RetAct.ActivityStatus = "D") Then
                    TheStatus = "Dischg"
                ElseIf (RetAct.ActivityStatus = "F") Then
                    TheStatus = "Left  "
                ElseIf (RetAct.ActivityStatus = "N") Then
                    TheStatus = "Can N "
                ElseIf (RetAct.ActivityStatus = "S") Then
                    TheStatus = "Can SD"
                ElseIf (RetAct.ActivityStatus = "O") Then
                    TheStatus = "Can O "
                ElseIf (RetAct.ActivityStatus = "Y") Then
                    TheStatus = "Can P "
                ElseIf (RetAct.ActivityStatus = "X") Then
                    TheStatus = "Cancel"
                ElseIf (RetAct.ActivityStatus = "Q") Then
                    TheStatus = "Can SP"
                Else
                    TheStatus = "Unk   "
                End If
                If (TheDate <> RetAct.ActivityDate) Then
                    TheStatus = "Not In"
                Else
                    Exit Do
                End If
                i = i + 1
            Loop
        End If
        Set RetAct = Nothing
    End If
End Function

Private Function GetLocation(Id As Long) As String
Dim i As Integer
    If (Id >= 0) Then
        For i = 0 To lstLoc.ListCount - 1
            If (Id = Val(Trim(Mid(lstLoc.List(i), 56, 5)))) Then
                GetLocation = lstLoc.List(i)
                Exit For
            End If
        Next i
    End If
End Function

Public Function CancelAppointment(PatId As Long, ApptId As Long) As Boolean
Dim i As Integer
Dim ActId As Long, BTime As Long
Dim TimeIn As String, FileFamily As String
Dim RetAppt As SchedulerAppointment
Dim RetrieveActivity As PracticeActivity
Dim SqlStr As String
Dim RS As Recordset
    If (ApptId > 0) And (PatId > 0) Then
        Set RetrieveActivity = New PracticeActivity
        RetrieveActivity.ActivityAppointmentId = ApptId
        RetrieveActivity.ActivityLocationId = -1
        RetrieveActivity.ActivityStatus = ""
        If (RetrieveActivity.FindActivity > 0) Then
            i = 1
            Do Until Not (RetrieveActivity.SelectActivity(i))
                If (RetrieveActivity.ActivityStatus = "E") Then
                    ActId = RetrieveActivity.ActivityId
                    Exit Do
                End If
                i = i + 1
            Loop
        End If
        Set RetrieveActivity = Nothing
    End If
    If (ActId > 0) And (PatId > 0) And (ApptId > 0) Then
        Set RetrieveActivity = New PracticeActivity
        RetrieveActivity.ActivityId = ActId
        If (RetrieveActivity.RetrieveActivity) Then
            CancelAppointment = True
            Call FormatTimeNow(TimeIn)
            BTime = ConvertTimeToMinutes(TimeIn)
            RetrieveActivity.ActivityCurrResId = UserLogin.iId
            RetrieveActivity.ActivityStationId = 0
            RetrieveActivity.ActivityStatus = "S"
            RetrieveActivity.ActivityStatusTime = TimeIn
            RetrieveActivity.ActivityStatusTRef = BTime
            Call RetrieveActivity.ApplyActivity
            Call PullFromServer("D", PatId, RetrieveActivity.ActivityAppointmentId)
            FileFamily = "*_" + Trim(str(ApptId)) + "_" + Trim(str(PatId)) + ".txt"
            Call CleanUp(DoctorInterfaceDirectory, FileFamily, False)
        End If
        Set RetrieveActivity = Nothing
    End If
    If (ApptId > 0) And (CancelAppointment) Then
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentId = ApptId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            RetAppt.AppointmentStatus = "S"
            Call RetAppt.ApplySchedulerAppointment
        End If
        Set RetAppt = Nothing
        
        'Clear model.ScheduleBlockAppointmentCategories
        SqlStr = "UPDATE model.ScheduleBlockAppointmentCategories SET AppointmentId=null WHERE AppointmentId=" & ApptId
        Set RS = GetRS(SqlStr)
    End If
End Function
Public Function FrmClose()
Call cmdDone_Click
 Unload Me
End Function

