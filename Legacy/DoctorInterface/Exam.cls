VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "privateCExam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public iPrimaryInsurerId As Long
Public dtDate As CIODateTime
Public fActiveExam As Boolean
Public fImpressionsOrderSetByDr As Boolean
Public fBringForward As Boolean
Public fDxOrderImpressions As Boolean
Public dtBirthDate As CIODateTime

Private m_iPatientId As Long
Private m_iAppointmentId As Long
Private m_iActivityId As Long
Private m_sFirstName As String
Private m_sLastName As String
Private m_sMiddleInitial As String
Private m_sType As String

Public Property Get sFirstName() As String
    sFirstName = m_sFirstName
End Property

Public Property Get sLastName() As String
    sLastName = m_sLastName
End Property

Public Property Get sFullName() As String
    sFullName = Trim$(m_sFirstName & " " & m_sMiddleInitial) & " " & m_sLastName
End Property

Public Property Get sType() As String
    sType = m_sType
End Property

Public Property Get iPatientId() As Long
    iPatientId = m_iPatientId
End Property

Public Property Get iAppointmentId() As Long
    iAppointmentId = m_iAppointmentId
End Property

Public Property Get iActivityId() As Long
    iActivityId = m_iActivityId
End Property

Public Property Get sAppPatIds() As String
    sAppPatIds = Trim$(Str$(m_iAppointmentId)) & "_" & Trim$(Str$(m_iPatientId))
End Property

Public Function GetDemoInfo() As Boolean
Dim oPatient As New Patient
    Set dtBirthDate = New CIODateTime
    With oPatient
        .PatientId = m_iPatientId
        If .RetrievePatient Then
            m_sFirstName = .FirstName
            m_sLastName = .LastName
            m_sMiddleInitial = .MiddleInitial
            m_sType = .PatType
            dtBirthDate.SetDateFromIO .BirthDate
        End If
    End With
    Set oPatient = Nothing
End Function

Public Function SetExamIds(iPatientId As Long, iAppointmentId As Long, iActivityId As Long) As Boolean
    If iPatientId > 0 Then
        m_iPatientId = iPatientId
    End If
    If iAppointmentId > 0 Then
        m_iAppointmentId = iAppointmentId
    End If
    If iActivityId > 0 Then
        m_iActivityId = iActivityId
        SetIdsFromActivity
    End If
End Function

Private Function SetIdsFromActivity() As Boolean
Dim oActivity As New PracticeActivity
    oActivity.ActivityId = m_iActivityId
    If oActivity.RetrieveActivity Then
        If m_iAppointmentId = 0 Then
            m_iAppointmentId = oActivity.ActivityAppointmentId
        End If
        If m_iPatientId = 0 Then
            m_iPatientId = oActivity.ActivityPatientId
        End If
    End If
End Function

Public Function ResetPatient() As Boolean
    m_iPatientId = 0
    m_iAppointmentId = 0
    m_iActivityId = 0
    m_sFirstName = ""
    m_sLastName = ""
    m_sMiddleInitial = ""
    m_sType = ""
End Function


