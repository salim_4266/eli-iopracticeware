VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmTestEntry 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ClipControls    =   0   'False
   ForeColor       =   &H00FFC0FF&
   LinkTopic       =   "Form1"
   Picture         =   "TestEntry.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdLabResults 
      Height          =   1095
      Left            =   9600
      TabIndex        =   10
      Top             =   6480
      Visible         =   0   'False
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestEntry.frx":36C9
   End
   Begin VB.TextBox txtBMI 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   495
      Left            =   5040
      TabIndex        =   9
      Top             =   6720
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox txtResults 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1095
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   7800
      Width           =   4935
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   1095
      Left            =   10800
      TabIndex        =   0
      Top             =   7800
      Visible         =   0   'False
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestEntry.frx":38B3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   1095
      Left            =   8520
      TabIndex        =   1
      Top             =   7800
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestEntry.frx":3AEA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   1095
      Left            =   5040
      TabIndex        =   4
      Top             =   7800
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestEntry.frx":3D22
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRetain 
      Height          =   1095
      Left            =   7440
      TabIndex        =   5
      Top             =   7800
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestEntry.frx":3F59
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRemove 
      Height          =   1095
      Left            =   6120
      TabIndex        =   6
      Top             =   7800
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestEntry.frx":4195
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHigh 
      Height          =   1095
      Left            =   9600
      TabIndex        =   7
      Top             =   7800
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestEntry.frx":43CE
   End
   Begin VB.Label lblAnnual 
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   7320
      Visible         =   0   'False
      Width           =   4935
   End
   Begin VB.Label lblEye 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFC0FF&
      Caption         =   "OD"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   240
      TabIndex        =   3
      Top             =   240
      Visible         =   0   'False
      Width           =   465
   End
End
Attribute VB_Name = "frmTestEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public ActivityId As Long
Public AppointmentId As Long

Public ChangeOn As Boolean
Public ReloadForm As Boolean
Public StoreResults As String
Public FormOn As Boolean
Public FormColor As Long
Public Question As String
Public ManifestResults As String
Public ModalSetting As Integer

Private Const MaxBtnAllowed = 8
Private Const MaxPadsAllowed = 8
Private Const MaxPadChoices = 24
Private Const MaxButtonsAllowed = 37
Private Const MaxAudioAllowed = 1
Private Const SelectionColor As Long = &HC0C000

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long

Private JustANote As Boolean
Private CurrentOrder As String
Private TestTime As String
Private ExitFast As Boolean
Private PrevTestFile As String
Private ScratchTestFile As String
Private ShadowFileName As String
Private CurrentPostingSet As Integer
Private ScratchRecordsRead As Integer

Private EyeWearOn As Boolean
Private CurrentFormId As Long
Private CurrentControlName As String
Private PostedQuestion As Boolean
Private RtriggerOn As Boolean
Private SelectionMade As Boolean
Private AlternateOn As Boolean
Private CurrentEye As String
Private EquipmentType As String

Private TheButtons(MaxButtonsAllowed) As Boolean
Private TheButtonName(MaxButtonsAllowed) As String
Private TheButtonText(MaxButtonsAllowed) As String
Private TheButtonNest(MaxButtonsAllowed) As String
Private TheButtonDisplay(MaxButtonsAllowed) As String * 64
Private TheButtonTrigger(MaxButtonsAllowed) As String
Private TheButtonTriggerText(MaxButtonsAllowed) As String
Private TheButtonVisible(MaxButtonsAllowed) As Boolean
Private TheButtonTextColor(MaxButtonsAllowed) As Long
Private TheButtonColor(MaxButtonsAllowed) As Long
Private TheButtonId(MaxButtonsAllowed) As Long
Private TheChoice(MaxButtonsAllowed) As String
Private TheChoiceName(MaxButtonsAllowed) As String

Private TheAudio(MaxAudioAllowed) As Boolean
Private TheAudioName(MaxAudioAllowed) As String
Private TheAudioText(MaxAudioAllowed) As String
Private TheAudioDisplay(MaxAudioAllowed) As String * 64
Private TheAudioVisible(MaxAudioAllowed) As Boolean
Private TheAudioTextColor(MaxAudioAllowed) As Long
Private TheAudioColor(MaxAudioAllowed) As Long
Private TheAudioId(MaxAudioAllowed) As Long

Private ThePads(MaxPadsAllowed) As Boolean
Private ThePadName(MaxPadsAllowed) As String
Private ThePadValue(MaxPadsAllowed) As String
Private ThePadChoice(MaxPadsAllowed, MaxPadChoices) As String
Private ThePadTrigger(MaxButtonsAllowed) As String
Private ThePadTriggerText(MaxButtonsAllowed) As String
Private ThePadTextColor(MaxPadsAllowed) As Long
Private ThePadColor(MaxPadsAllowed) As Long
Private ThePadId(MaxPadsAllowed) As Long
Private ThePadVisible(MaxPadsAllowed) As Boolean
Private ThePadDisplay(MaxPadsAllowed) As String * 64
Private ThePadDecimal(MaxPadsAllowed) As String * 1
Private ThePadHelpText(MaxPadsAllowed) As String
Private ThePadIncrement(MaxPadsAllowed) As String
Private ThePadExoticOn(MaxPadsAllowed) As Boolean

Private TheBtns(MaxBtnAllowed) As Boolean
Private TheBtnName(MaxBtnAllowed) As String
Private TheBtnText(MaxBtnAllowed) As String
Private TheBtnNest(MaxBtnAllowed) As String
Private TheBtnDisplay(MaxBtnAllowed) As String * 64
Private TheBtnTrigger(MaxBtnAllowed) As String
Private TheBtnTriggerText(MaxBtnAllowed) As String
Private TheBtnVisible(MaxBtnAllowed) As Boolean
Private TheBtnTextColor(MaxBtnAllowed) As Long
Private TheBtnColor(MaxBtnAllowed) As Long
Private TheBtnId(MaxBtnAllowed) As Long

Private MaxControls As Integer
Private MaxAudio As Integer
Private MaxButtons As Integer
Private MaxLabel As Integer
Private MaxPads As Integer
Private MaxBtns As Integer
Private MaxEPads As Integer

Private ValueInInche As Double
Private ValueInLbs As Double
Private ValueInBMI As Double
Dim iValH(4) As Double
Dim iValW(4) As Double

Private TheForm As DynamicForms
Private TheControl As DynamicControls
Private TheSecondaryForm As DynamicSecondaryForms
Private TheSecondaryControl As DynamicSecondaryControls

' you can have any number of Labels
' code is not required for Label processing
Private WithEvents lblLabel As Label
Attribute lblLabel.VB_VarHelpID = -1
' you can have one (1) Audio Button per form
Private WithEvents cmdAudio1 As fpBtn
Attribute cmdAudio1.VB_VarHelpID = -1
' you can have eight (8) Retained Buttons per form
Private WithEvents cmdBtn1 As fpBtn
Attribute cmdBtn1.VB_VarHelpID = -1
Private WithEvents cmdBtn2 As fpBtn
Attribute cmdBtn2.VB_VarHelpID = -1
Private WithEvents cmdBtn3 As fpBtn
Attribute cmdBtn3.VB_VarHelpID = -1
Private WithEvents cmdBtn4 As fpBtn
Attribute cmdBtn4.VB_VarHelpID = -1
Private WithEvents cmdBtn5 As fpBtn
Attribute cmdBtn5.VB_VarHelpID = -1
Private WithEvents cmdBtn6 As fpBtn
Attribute cmdBtn6.VB_VarHelpID = -1
Private WithEvents cmdBtn7 As fpBtn
Attribute cmdBtn7.VB_VarHelpID = -1
Private WithEvents cmdBtn8 As fpBtn
Attribute cmdBtn8.VB_VarHelpID = -1
' you can have eight (8) ExoticPads per form
Private WithEvents cmdEPad1 As fpBtn
Attribute cmdEPad1.VB_VarHelpID = -1
Private WithEvents cmdEPad2 As fpBtn
Attribute cmdEPad2.VB_VarHelpID = -1
Private WithEvents cmdEPad3 As fpBtn
Attribute cmdEPad3.VB_VarHelpID = -1
Private WithEvents cmdEPad4 As fpBtn
Attribute cmdEPad4.VB_VarHelpID = -1
Private WithEvents cmdEPad5 As fpBtn
Attribute cmdEPad5.VB_VarHelpID = -1
Private WithEvents cmdEPad6 As fpBtn
Attribute cmdEPad6.VB_VarHelpID = -1
Private WithEvents cmdEPad7 As fpBtn
Attribute cmdEPad7.VB_VarHelpID = -1
Private WithEvents cmdEPad8 As fpBtn
Attribute cmdEPad8.VB_VarHelpID = -1
' you can have eight (8) NumericPads per form
Private WithEvents cmdPad1 As fpBtn
Attribute cmdPad1.VB_VarHelpID = -1
Private WithEvents cmdPad2 As fpBtn
Attribute cmdPad2.VB_VarHelpID = -1
Private WithEvents cmdPad3 As fpBtn
Attribute cmdPad3.VB_VarHelpID = -1
Private WithEvents cmdPad4 As fpBtn
Attribute cmdPad4.VB_VarHelpID = -1
Private WithEvents cmdPad5 As fpBtn
Attribute cmdPad5.VB_VarHelpID = -1
Private WithEvents cmdPad6 As fpBtn
Attribute cmdPad6.VB_VarHelpID = -1
Private WithEvents cmdPad7 As fpBtn
Attribute cmdPad7.VB_VarHelpID = -1
Private WithEvents cmdPad8 As fpBtn
Attribute cmdPad8.VB_VarHelpID = -1
' you can have 30 CommandButtons per form
Private WithEvents cmdButton1 As fpBtn
Attribute cmdButton1.VB_VarHelpID = -1
Private WithEvents cmdButton2 As fpBtn
Attribute cmdButton2.VB_VarHelpID = -1
Private WithEvents cmdButton3 As fpBtn
Attribute cmdButton3.VB_VarHelpID = -1
Private WithEvents cmdButton4 As fpBtn
Attribute cmdButton4.VB_VarHelpID = -1
Private WithEvents cmdButton5 As fpBtn
Attribute cmdButton5.VB_VarHelpID = -1
Private WithEvents cmdButton6 As fpBtn
Attribute cmdButton6.VB_VarHelpID = -1
Private WithEvents cmdButton7 As fpBtn
Attribute cmdButton7.VB_VarHelpID = -1
Private WithEvents cmdButton8 As fpBtn
Attribute cmdButton8.VB_VarHelpID = -1
Private WithEvents cmdButton9 As fpBtn
Attribute cmdButton9.VB_VarHelpID = -1
Private WithEvents cmdButton10 As fpBtn
Attribute cmdButton10.VB_VarHelpID = -1
Private WithEvents cmdButton11 As fpBtn
Attribute cmdButton11.VB_VarHelpID = -1
Private WithEvents cmdButton12 As fpBtn
Attribute cmdButton12.VB_VarHelpID = -1
Private WithEvents cmdButton13 As fpBtn
Attribute cmdButton13.VB_VarHelpID = -1
Private WithEvents cmdButton14 As fpBtn
Attribute cmdButton14.VB_VarHelpID = -1
Private WithEvents cmdButton15 As fpBtn
Attribute cmdButton15.VB_VarHelpID = -1
Private WithEvents cmdButton16 As fpBtn
Attribute cmdButton16.VB_VarHelpID = -1
Private WithEvents cmdButton17 As fpBtn
Attribute cmdButton17.VB_VarHelpID = -1
Private WithEvents cmdButton18 As fpBtn
Attribute cmdButton18.VB_VarHelpID = -1
Private WithEvents cmdButton19 As fpBtn
Attribute cmdButton19.VB_VarHelpID = -1
Private WithEvents cmdButton20 As fpBtn
Attribute cmdButton20.VB_VarHelpID = -1
Private WithEvents cmdButton21 As fpBtn
Attribute cmdButton21.VB_VarHelpID = -1
Private WithEvents cmdButton22 As fpBtn
Attribute cmdButton22.VB_VarHelpID = -1
Private WithEvents cmdButton23 As fpBtn
Attribute cmdButton23.VB_VarHelpID = -1
Private WithEvents cmdButton24 As fpBtn
Attribute cmdButton24.VB_VarHelpID = -1
Private WithEvents cmdButton25 As fpBtn
Attribute cmdButton25.VB_VarHelpID = -1
Private WithEvents cmdButton26 As fpBtn
Attribute cmdButton26.VB_VarHelpID = -1
Private WithEvents cmdButton27 As fpBtn
Attribute cmdButton27.VB_VarHelpID = -1
Private WithEvents cmdButton28 As fpBtn
Attribute cmdButton28.VB_VarHelpID = -1
Private WithEvents cmdButton29 As fpBtn
Attribute cmdButton29.VB_VarHelpID = -1
Private WithEvents cmdButton30 As fpBtn
Attribute cmdButton30.VB_VarHelpID = -1
Private WithEvents cmdButton31 As fpBtn
Attribute cmdButton31.VB_VarHelpID = -1
Private WithEvents cmdButton32 As fpBtn
Attribute cmdButton32.VB_VarHelpID = -1
Private WithEvents cmdButton33 As fpBtn
Attribute cmdButton33.VB_VarHelpID = -1
Private WithEvents cmdButton34 As fpBtn
Attribute cmdButton34.VB_VarHelpID = -1
Private WithEvents cmdButton35 As fpBtn
Attribute cmdButton35.VB_VarHelpID = -1
Private WithEvents cmdButton36 As fpBtn
Attribute cmdButton36.VB_VarHelpID = -1
Private WithEvents cmdButton37 As fpBtn
Attribute cmdButton37.VB_VarHelpID = -1

Private Sub SetRTestButtons(DisplayType As Boolean)
Dim z As Integer
For z = 1 To MaxBtns
    If (z = 1) Then
        cmdBtn1.Visible = DisplayType
        cmdBtn1.Enabled = DisplayType
    ElseIf (z = 2) Then
        cmdBtn2.Visible = DisplayType
        cmdBtn2.Enabled = DisplayType
    ElseIf (z = 3) Then
        cmdBtn3.Visible = DisplayType
        cmdBtn3.Enabled = DisplayType
    ElseIf (z = 4) Then
        cmdBtn4.Visible = DisplayType
        cmdBtn4.Enabled = DisplayType
    ElseIf (z = 5) Then
        cmdBtn5.Visible = DisplayType
        cmdBtn5.Enabled = DisplayType
    ElseIf (z = 6) Then
        cmdBtn6.Visible = DisplayType
        cmdBtn6.Enabled = DisplayType
    ElseIf (z = 7) Then
        cmdBtn7.Visible = DisplayType
        cmdBtn7.Enabled = DisplayType
    ElseIf (z = 8) Then
        cmdBtn8.Visible = DisplayType
        cmdBtn8.Enabled = DisplayType
    End If
Next z
End Sub

Private Sub SetTriggerButtons(DisplayType As Boolean, TheTrigger As String)
Dim Counter As Integer
For Counter = 3 To MaxButtons
    If (TheButtonTriggerText(Counter) = TheTrigger) Or (Trim(TheTrigger) = "") Then
        If (Counter = 3) Then
            cmdButton3.Visible = DisplayType
        ElseIf (Counter = 4) Then
            cmdButton4.Visible = DisplayType
        ElseIf (Counter = 5) Then
            cmdButton5.Visible = DisplayType
        ElseIf (Counter = 6) Then
            cmdButton6.Visible = DisplayType
        ElseIf (Counter = 7) Then
            cmdButton7.Visible = DisplayType
        ElseIf (Counter = 8) Then
            cmdButton8.Visible = DisplayType
        ElseIf (Counter = 9) Then
            cmdButton9.Visible = DisplayType
        ElseIf (Counter = 10) Then
            cmdButton10.Visible = DisplayType
        ElseIf (Counter = 11) Then
            cmdButton11.Visible = DisplayType
        ElseIf (Counter = 12) Then
            cmdButton12.Visible = DisplayType
        ElseIf (Counter = 13) Then
            cmdButton13.Visible = DisplayType
        ElseIf (Counter = 14) Then
            cmdButton14.Visible = DisplayType
        ElseIf (Counter = 15) Then
            cmdButton15.Visible = DisplayType
        ElseIf (Counter = 16) Then
            cmdButton16.Visible = DisplayType
        ElseIf (Counter = 17) Then
            cmdButton17.Visible = DisplayType
        ElseIf (Counter = 18) Then
            cmdButton18.Visible = DisplayType
        ElseIf (Counter = 19) Then
            cmdButton19.Visible = DisplayType
        ElseIf (Counter = 20) Then
            cmdButton20.Visible = DisplayType
        ElseIf (Counter = 21) Then
            cmdButton21.Visible = DisplayType
        ElseIf (Counter = 22) Then
            cmdButton22.Visible = DisplayType
        ElseIf (Counter = 23) Then
            cmdButton23.Visible = DisplayType
        ElseIf (Counter = 24) Then
            cmdButton24.Visible = DisplayType
        ElseIf (Counter = 25) Then
            cmdButton25.Visible = DisplayType
        ElseIf (Counter = 26) Then
            cmdButton26.Visible = DisplayType
        ElseIf (Counter = 27) Then
            cmdButton27.Visible = DisplayType
        ElseIf (Counter = 28) Then
            cmdButton28.Visible = DisplayType
        ElseIf (Counter = 29) Then
            cmdButton29.Visible = DisplayType
        ElseIf (Counter = 30) Then
            cmdButton30.Visible = DisplayType
        ElseIf (Counter = 31) Then
            cmdButton31.Visible = DisplayType
        ElseIf (Counter = 32) Then
            cmdButton32.Visible = DisplayType
        ElseIf (Counter = 33) Then
            cmdButton33.Visible = DisplayType
        ElseIf (Counter = 34) Then
            cmdButton34.Visible = DisplayType
        ElseIf (Counter = 35) Then
            cmdButton35.Visible = DisplayType
        ElseIf (Counter = 36) Then
            cmdButton36.Visible = DisplayType
        End If
    End If
    Call InitializeButton(Counter)
Next Counter
If (MaxButtons > 2) Then
    If Not DisplayType Then
        Call InitializeButton(1)
    End If
    cmdButton1.Visible = Not DisplayType
    If Not DisplayType Then
        Call InitializeButton(2)
    End If
    cmdButton2.Visible = Not DisplayType
    If Not (cmdButton1.Visible) Then
        lblEye.Caption = CurrentEye
        lblEye.BackColor = frmTestEntry.BackColor
        lblEye.ForeColor = &HFFFFFF
        lblEye.Visible = True
    Else
        lblEye.Visible = False
    End If
End If
End Sub

Private Function GetButtonbyPad(AButton As String) As Integer
Dim u As Integer
GetButtonbyPad = 0
For u = 1 To MaxButtons
    If (Trim(AButton) = Trim(TheButtonName(u))) Then
        GetButtonbyPad = u
        Exit Function
    End If
Next u
End Function

Private Sub cmdHigh_Click()
cmdHigh.Enabled = False
frmHighlights.HighlightLoadOn = frmSystems1.IsHighlightLoadOn
If (frmHighlights.LoadHighlights(PatientId, True, True, False)) Then
    frmHighlights.Show 1
    Call frmSystems1.SetHighlightLoad(frmHighlights.HighlightLoadOn)
End If
cmdHigh.Enabled = True
frmTestEntry.ZOrder 0
End Sub

Private Sub cmdLabResults_Click()
Dim objLabForm As New LabReport.frmLabReport
Dim Helper As New LabReport.DbHelper
Helper.DbObject = IdbConnection
objLabForm.PatientId = PatientId
objLabForm.Show
End Sub

Private Sub cmdRemove_Click()
Dim i As Integer
Dim PurgeIt As Boolean
frmEventMsgs.Header = "Remove Test Results"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Ok"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    PurgeIt = True
    If (frmSystems1.EditPreviousOn) Then
        frmEventMsgs.Header = "This will permanently remove this Test Result from the patient's record. Are you sure ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Yes"
        frmEventMsgs.CancelText = "No"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result <> 2) Then
            PurgeIt = False
        End If
    End If
    If (PurgeIt) Then
        If (FM.IsFileThere(StoreResults)) Then
            FM.Kill StoreResults
        End If
        If (FM.IsFileThere(ScratchTestFile)) Then
            FM.Kill ScratchTestFile
        End If
        If (FM.IsFileThere(PrevTestFile)) Then
            FM.Kill PrevTestFile
        End If
        If (frmSystems1.EditPreviousOn) Then
            Call frmSystems1.DeleteOriginalTest(Question, AppointmentId, PatientId)
        End If
    End If
End If
Unload frmTestEntry
FormOn = False
End Sub

Private Sub cmdNotes_Click()
frmNotes.NoteId = 0
frmNotes.SystemReference = "T" + CurrentOrder
frmNotes.EyeContext = CurrentEye
If (CurrentEye <> "OD") And (CurrentEye <> "OS") Then
    frmNotes.EyeContext = "OU"
End If
frmNotes.NoteOn = False
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    If (frmNotes.NoteOn) Then
        SelectionMade = True
        JustANote = True
    End If
End If
frmTestEntry.ZOrder 0
End Sub

Private Sub cmdRetain_Click()
Dim u As Integer
Dim RscId As Long
Dim Rec As String
Dim TestTime As String
Dim AName As String
Dim FileNum As Integer
If (FM.IsFileThere(PrevTestFile)) Then
    FileNum = FreeFile
    FM.OpenFile PrevTestFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
    FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum + 1)
    While Not (EOF(FileNum))
        Line Input #FileNum, Rec
        If (Left(Rec, 6) <> "Recap=") Then
            If (InStrPS(Rec, "TIME") <> 0) Then
                Call FormatTimeNow(TestTime)
                RscId = UserLogin.iId
                AName = UserLogin.sNameShort
                If (InStrPS(TestTime, Trim(AName)) = 0) Then
                    Print #FileNum + 1, "TIME=" + TestTime + " (" + AName + ")"
                Else
                    Print #FileNum + 1, "TIME=" + TestTime
                End If
            Else
                Print #FileNum + 1, Rec
            End If
        End If
    Wend
    FM.CloseFile CLng(FileNum)
    FM.CloseFile CLng(FileNum + 1)
    FM.Kill PrevTestFile
    If (FM.IsFileThere(ScratchTestFile)) Then
        FM.Kill ScratchTestFile
    End If
    Call InsertLog("frmTestEntry.frm", Question & " Brought Forward form Pervious Visit for Patient " & globalExam.iPatientId, LoginCatg.BringFindingsForwardCopy, "", "", 0, globalExam.iAppointmentId)
End If
Set TheForm = Nothing
Set TheControl = Nothing
Unload frmTestEntry
FormOn = False
End Sub

Private Sub cmdQuit_Click()
Set TheForm = Nothing
Set TheControl = Nothing
If (FM.IsFileThere(ScratchTestFile)) Then
    FM.Kill ScratchTestFile
End If
If (FM.IsFileThere(PrevTestFile)) Then
    FM.Kill PrevTestFile
End If
If (IsTestFileThere(StoreResults)) Then
    FM.Kill StoreResults
End If
StoreResults = ""
Unload frmTestEntry
FormOn = False
End Sub

Private Sub cmdApply_Click()
Dim z As Integer
Dim p As Integer
If (Trim(ScratchTestFile) = "") Then
    Set TheForm = Nothing
    Set TheControl = Nothing
    If (FM.IsFileThere(PrevTestFile)) Then
        FM.Kill PrevTestFile
    End If
    Unload frmTestEntry
    FormOn = False
    Exit Sub
End If
If Not (SelectionMade) Then
    Set TheForm = Nothing
    Set TheControl = Nothing
    If (FM.IsFileThere(PrevTestFile)) Then
        FM.Kill PrevTestFile
    End If
    Unload frmTestEntry
    FormOn = False
    Exit Sub
End If
If (AlternateOn) Then
    If (CurrentPostingSet < 1) Then
        Call AssumeYouStartOver
    End If
    Call PostTestResults
    AlternateOn = False
    Exit Sub
End If
If (RtriggerOn) And _
   ((CurrentPostingSet = 2) Or (CurrentPostingSet = 4) Or (CurrentPostingSet = 6) Or _
    (CurrentPostingSet = 8) Or (CurrentPostingSet = 10) Or (CurrentPostingSet = 12) Or _
    (CurrentPostingSet = 14) Or (CurrentPostingSet = 16)) Then
    RtriggerOn = False
    Call PostTestResults
    Call SetRTestButtons(True)
    Call SetTriggerButtons(False, TheButtonTrigger(2))
    Exit Sub
Else
    If (CurrentPostingSet < 2) Then
        Call PostTestResults
    Else
        For p = 1 To MaxButtons
            If (TheButtons(p)) Then
                Call PostTestResults
                Exit For
            End If
        Next p
    End If
End If
Set TheForm = Nothing
Set TheControl = Nothing
If (FM.IsFileThere(ScratchTestFile)) Then
    If (Trim(StoreResults) <> "") Then
        FM.FileCopy ScratchTestFile, StoreResults
    End If
    FM.Kill ScratchTestFile
End If
If (FM.IsFileThere(PrevTestFile)) Then
    FM.Kill PrevTestFile
End If
Unload frmTestEntry
FormOn = False
End Sub

Private Sub PostTestResults()
Dim i As Integer
Dim RscId As Long
Dim AName As String
Dim PadOn As Integer
Dim j As Integer, p As Integer, q As Integer
Dim k As Integer, z As Integer, z1 As Integer
Dim OtherFile As String
Dim TestName As String, TheRef As String
Dim DispBtn As String, DisplayButton As String, PadName As String
Dim FileNum As Integer, ButtonName As String
Dim ImageName As String, SliderName As String
On Error GoTo UI_ErrorHandler
If (Trim(ScratchTestFile) = "") Then
    Exit Sub
End If
FileNum = FreeFile
FM.OpenFile ScratchTestFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
If (CurrentPostingSet = 0) And Not (PostedQuestion) Then
    Print #FileNum, "QUESTION=" + "/" + Question
    Call FormatTimeNow(TestTime)
    RscId = UserLogin.iId
    AName = UserLogin.sNameShort
    Print #FileNum, "TIME=" + TestTime + " (" + AName + ")"
    PostedQuestion = True
End If
TestName = ""
For p = 1 To 2
    If (TheButtons(p)) Or (JustANote) Then
        If (TheButtons(p)) Or ((JustANote) And (p = 1)) Then
            ButtonName = TheButtonName(p) + "=T" + Trim(str(TheButtonId(p))) + " <" + TheButtonNest(p) + ">"
            If (MaxBtns < 1) And (p = 1) Then
                Print #FileNum, ButtonName
            End If
            If (Trim(TheButtonDisplay(p)) <> "") Then
                Call ConvertButtonDisplay(Trim(TheButtonDisplay(p)), DisplayButton)
                z = InStrPS(DisplayButton, "OD")
                If (z > 3) Then
                    TestName = Trim(Left(DisplayButton, z - 3))
                    For q = 1 To MaxBtns
                        If (TheBtns(q)) Then
                            Print #FileNum, TheBtnName(q) + "=T" + Trim(str(TheBtnId(q))) + " <" + TheBtnNest(q) + ">"
                            Print #FileNum, ButtonName
                        End If
                    Next q
                Else
                    If (p > 1) Or (MaxBtns > 0) Then
                        Print #FileNum, ButtonName
                    End If
                End If
            End If
        End If
    End If
Next p
For p = 3 To MaxButtons
    If (TheButtons(p)) Then
    'Changes made for BMI
        If Trim(str(TheButtonId(p))) = "14905" Then
            If Trim(txtBMI.Text) <> "" Then
                TheButtonNest(p) = txtBMI.Text
            End If
        End If
        ButtonName = TheButtonName(p) + "=T" + Trim(str(TheButtonId(p))) + " <" + TheButtonNest(p) + ">"
        Print #FileNum, ButtonName
    End If
Next p
For p = 1 To MaxAudio
    If (TheAudio(p)) Then
        ButtonName = TheAudioName(p) + "=T" + Trim(str(TheAudioId(p))) + " <Audio>"
        Print #FileNum, ButtonName
    End If
Next p

PadOn = 0
For p = 1 To MaxPads
    If (ThePads(p)) Then
        PadOn = p
        PadName = ThePadName(p) + "=T" + Trim(str(ThePadId(p))) + " <" + Trim(ThePadValue(p)) + ">"
        Print #FileNum, PadName
        For k = 1 To MaxPadChoices
            If (Trim(ThePadChoice(p, k)) <> "") Then
                j = GetButtonbyPad(ThePadChoice(p, k))
                If (j <> 0) Then
                    ButtonName = TheButtonName(j) + "=T" + Trim(str(TheButtonId(j))) + " <" + TheButtonNest(j) + ">"
                    Print #FileNum, ButtonName
                End If
            End If
        Next k
    End If
Next p
FM.CloseFile CLng(FileNum)
If (CurrentPostingSet > 1) Then
    If (MaxBtns < 1) Then
        Set TheForm = Nothing
        Set TheControl = Nothing
        If (FM.IsFileThere(ScratchTestFile)) Then
            FM.FileCopy ScratchTestFile, StoreResults
            FM.Kill ScratchTestFile
        End If
        If (FM.IsFileThere(PrevTestFile)) Then
            FM.Kill PrevTestFile
        End If
        If (frmHome.BackUpOn) Then
            i = FM.InFileNameStartStr(StoreResults, "T")
            If (i > 0) Then
                OtherFile = Left(StoreResults, i - 1) + "Backup\" + Mid(StoreResults, i, Len(StoreResults) - i + 1)
                FM.FileCopy StoreResults, OtherFile
            End If
        End If
        Unload frmTestEntry
        FormOn = False
    End If
Else
    If (TheButtons(1)) And ((MaxPads < 2) Or (PadOn >= MaxPads)) Then
        If (cmdButton2.Text = "OS") Then
            CurrentPostingSet = CurrentPostingSet + 1
            AlternateOn = False
            TheButtons(1) = False
            Call FormReLoad
            Call cmdButton2_Click
            If (CurrentPostingSet > 2) Or (CurrentPostingSet < 1) Then
                If (MaxBtns < 1) Then
                    Set TheForm = Nothing
                    Set TheControl = Nothing
                    If (FM.IsFileThere(ScratchTestFile)) Then
                        FM.FileCopy ScratchTestFile, StoreResults
                        FM.Kill ScratchTestFile
                    End If
                    If (FM.IsFileThere(PrevTestFile)) Then
                        FM.Kill PrevTestFile
                    End If
                    If (frmHome.BackUpOn) Then
                        i = FM.InFileNameStartStr(StoreResults, "T")
                        If (i > 0) Then
                            OtherFile = Left(StoreResults, i - 1) + "Backup\" + Mid(StoreResults, i, Len(StoreResults) - i + 1)
                            FM.FileCopy StoreResults, OtherFile
                        End If
                    End If
                    Unload frmTestEntry
                    FormOn = False
                End If
            End If
        End If
    Else
        Call FormReLoad
    End If
End If
Exit Sub
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Sub

Private Function ProcessPadTrigger(TheTrigger As String) As Boolean
Dim k As Integer
Dim q As Integer
Dim p As Integer
ProcessPadTrigger = True
For k = 1 To MaxPads
    p = 0
    For q = 1 To MaxButtons
        TheChoice(q) = ""
        TheChoiceName(q) = ""
    Next q
    For q = 3 To MaxButtons
        If (Trim(TheButtonTriggerText(q) <> "") <> 0) Then
            If (TheButtonTriggerText(q) = ThePadTriggerText(k)) Then
                If (TheButtonVisible(q)) And (ThePadVisible(k)) Then
                    p = p + 1
                    TheChoice(p) = Trim(TheButtonText(q))
                    TheChoiceName(p) = TheButtonName(q)
                End If
            End If
        End If
    Next q
    If (k = 1) Then
        Call cmdPad1_Click
    ElseIf (k = 2) Then
        Call cmdPad2_Click
    ElseIf (k = 3) Then
        Call cmdPad3_Click
    ElseIf (k = 4) Then
        Call cmdPad4_Click
    ElseIf (k = 5) Then
        Call cmdPad5_Click
    ElseIf (k = 6) Then
        Call cmdPad6_Click
    ElseIf (k = 7) Then
        Call cmdPad7_Click
    ElseIf (k = 8) Then
        Call cmdPad8_Click
    End If
    If (ExitFast) Then
        Call cmdQuit_Click
        Exit For
    End If
Next k
End Function

Private Sub ButtonAction(AButton As fpBtn, Ref As Integer)
If (TheButtonColor(Ref) = AButton.BackColor) Then
    SelectionMade = True
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    If (InStrPS(TheButtonName(Ref), "OD") <> 0) Then
        Call AssumeYouStartOver
    End If
Else
    AButton.BackColor = TheButtonColor(Ref)
    AButton.ForeColor = TheButtonTextColor(Ref)
End If
TheButtons(Ref) = Not (TheButtons(Ref))

' Set the Process for Trigger Conditions
If Not (AlternateOn) Then
    If (TheButtons(Ref)) Then
        If (Trim(TheButtonTrigger(Ref)) <> "") Then
            If (InStrPS(TheButtonTrigger(Ref), "Pad") <> 0) Then
                Call ProcessPadTrigger(TheButtonTrigger(Ref))
                If Not (ExitFast) Then
                    Call PostTestResults
                    If (Ref = 2) Then
                        RtriggerOn = False
                        Call SetRTestButtons(True)
                        Call SetTriggerButtons(False, TheButtonTrigger(2))
                        Call FormReLoad
                    End If
                End If
            ElseIf (InStrPS(TheButtonTrigger(Ref), "Btn") <> 0) Then
                AlternateOn = True
                If (Ref = 2) Then
                    AlternateOn = False
                End If
                Call SetTriggerButtons(True, TheButtonTrigger(Ref))
            End If
        End If
    End If
End If
' Check to see if single selection is on
If ((TheButtons(Ref)) And Not (ExitFast)) Then
    If Not (cmdApply.Visible) Then
        Call cmdApply_Click
        Exit Sub
    End If
End If
If (TheButtonText(1) = "Start") Or (TheButtonText(2) = "Start") Then
    Call cmdApply_Click
End If
'For BMI Calculation
If (TheButtonId(Ref) = "14905") Then
    If Not ValueInBMI = 0 Then
    txtBMI.Text = Math.Round(ValueInBMI, 1)
    txtBMI.Visible = True
    End If
End If
'For Growth Charts
If (TheButtonId(Ref) = "14906") Then
    If Not (TheButtonColor(Ref) = AButton.BackColor) Then
    Dim frm As New GrowthCharts.FrmCharts
    Dim Helper As New GrowthCharts.DbHelper
    Helper.DbObject = IdbConnection
    frm.PatientId = PatientId
    frm.Show
    End If
End If
End Sub

Private Sub cmdButton1_Click()
CurrentEye = "OD"
Call ButtonAction(cmdButton1, 1)
End Sub

Private Sub cmdButton2_Click()
CurrentEye = "OS"
Call ButtonAction(cmdButton2, 2)
End Sub

Private Sub cmdButton3_Click()
Call ButtonAction(cmdButton3, 3)
End Sub

Private Sub cmdButton4_Click()
Call ButtonAction(cmdButton4, 4)
End Sub

Private Sub cmdButton5_Click()
Call ButtonAction(cmdButton5, 5)
End Sub

Private Sub cmdButton6_Click()
Call ButtonAction(cmdButton6, 6)
End Sub

Private Sub cmdButton7_Click()
Call ButtonAction(cmdButton7, 7)
End Sub

Private Sub cmdButton8_Click()
Call ButtonAction(cmdButton8, 8)
End Sub

Private Sub cmdButton9_Click()
Call ButtonAction(cmdButton9, 9)
End Sub

Private Sub cmdButton10_Click()
Call ButtonAction(cmdButton10, 10)
End Sub

Private Sub cmdButton11_Click()
Call ButtonAction(cmdButton11, 11)
End Sub

Private Sub cmdButton12_Click()
Call ButtonAction(cmdButton12, 12)
End Sub

Private Sub cmdButton13_Click()
Call ButtonAction(cmdButton13, 13)
End Sub

Private Sub cmdButton14_Click()
Call ButtonAction(cmdButton14, 14)
End Sub

Private Sub cmdButton15_Click()
Call ButtonAction(cmdButton15, 15)
End Sub

Private Sub cmdButton16_Click()
Call ButtonAction(cmdButton16, 16)
End Sub

Private Sub cmdButton17_Click()
Call ButtonAction(cmdButton17, 17)
End Sub

Private Sub cmdButton18_Click()
Call ButtonAction(cmdButton18, 18)
End Sub

Private Sub cmdButton19_Click()
Call ButtonAction(cmdButton19, 19)
End Sub

Private Sub cmdButton20_Click()
Call ButtonAction(cmdButton20, 20)
End Sub

Private Sub cmdButton21_Click()
Call ButtonAction(cmdButton21, 21)
End Sub

Private Sub cmdButton22_Click()
Call ButtonAction(cmdButton22, 22)
End Sub

Private Sub cmdButton23_Click()
Call ButtonAction(cmdButton23, 23)
End Sub

Private Sub cmdButton24_Click()
Call ButtonAction(cmdButton24, 24)
End Sub

Private Sub cmdButton25_Click()
Call ButtonAction(cmdButton25, 25)
End Sub

Private Sub cmdButton26_Click()
Call ButtonAction(cmdButton26, 26)
End Sub

Private Sub cmdButton27_Click()
Call ButtonAction(cmdButton27, 27)
End Sub

Private Sub cmdButton28_Click()
Call ButtonAction(cmdButton28, 28)
End Sub

Private Sub cmdButton29_Click()
Call ButtonAction(cmdButton29, 29)
End Sub

Private Sub cmdButton30_Click()
Call ButtonAction(cmdButton30, 30)
End Sub

Private Sub cmdButton31_Click()
Call ButtonAction(cmdButton31, 31)
End Sub

Private Sub cmdButton32_Click()
Call ButtonAction(cmdButton32, 32)
End Sub

Private Sub cmdButton33_Click()
Call ButtonAction(cmdButton33, 33)
End Sub

Private Sub cmdButton34_Click()
Call ButtonAction(cmdButton34, 34)
End Sub

Private Sub cmdButton35_Click()
Call ButtonAction(cmdButton35, 35)
End Sub

Private Sub cmdButton36_Click()
Call ButtonAction(cmdButton36, 36)
End Sub

Private Sub cmdButton37_Click()
Call ButtonAction(cmdButton36, 37)
End Sub

Private Sub SetButtonCharacteristics(RefControl As DynamicControls, AButton As fpBtn)
Dim q As Integer
Dim VisualText As String
With AButton
    If (Trim(RefControl.ControlTemplateName) <> "") Then
        .TemplateName = BtnDir + Trim(RefControl.ControlTemplateName) + ".btn"
        .Action = fpActionLoadTemplate
        .CellIndex = 2
        .SegmentApplyTo = fpSegmentApplyToButton
        .BackStyle = fpBackStyleTransparentGrayArea
         RefControl.BackColor = .BackColor
    End If
    '.BackColor = RefControl.BackColor
    .BackColor = &HC19B49
    If (Trim(RefControl.ControlText) <> "") Then
        q = InStrPS(RefControl.ControlText, "^")
        If (q <> 0) Then
            VisualText = Left(RefControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(RefControl.ControlText, q + 1, Len(RefControl.ControlText) - q)
        Else
            VisualText = RefControl.ControlText
        End If
        .Text = VisualText
    Else
        .Text = RefControl.ControlName
    End If
    If (Trim(RefControl.IEChoiceName) <> "") Then
        .Tag = RefControl.IEChoiceName
    Else
        .Tag = RefControl.ControlName
    End If
    .ForeColor = RefControl.ForeColor
    .Left = RefControl.ControlLeft
    .Top = RefControl.ControlTop
    .Height = RefControl.ControlHeight
    .Width = RefControl.ControlWidth
    .TabIndex = RefControl.TabIndex
    .Visible = RefControl.ControlVisible
    .Enabled = True
    .Font.Size = RefControl.ResponseLength
    If (Trim(RefControl.ControlFont) = "") Then
        .Font.Name = "Lucida Sans"
    Else
        .Font.Name = Trim(RefControl.ControlFont)
    End If
    .FontBold = False
    .ToolTipText = ""
End With
End Sub

Private Sub InitializeButton(Counter As Integer)
If (Counter = 1) Then
    cmdButton1.BackColor = TheButtonColor(Counter)
    cmdButton1.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 2) Then
    cmdButton2.BackColor = TheButtonColor(Counter)
    cmdButton2.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 3) Then
    cmdButton3.BackColor = TheButtonColor(Counter)
    cmdButton3.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 4) Then
    cmdButton4.BackColor = TheButtonColor(Counter)
    cmdButton4.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 5) Then
    cmdButton5.BackColor = TheButtonColor(Counter)
    cmdButton5.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 6) Then
    cmdButton6.BackColor = TheButtonColor(Counter)
    cmdButton6.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 7) Then
    cmdButton7.BackColor = TheButtonColor(Counter)
    cmdButton7.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 8) Then
    cmdButton8.BackColor = TheButtonColor(Counter)
    cmdButton8.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 9) Then
    cmdButton9.BackColor = TheButtonColor(Counter)
    cmdButton9.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 10) Then
    cmdButton10.BackColor = TheButtonColor(Counter)
    cmdButton10.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 11) Then
    cmdButton11.BackColor = TheButtonColor(Counter)
    cmdButton11.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 12) Then
    cmdButton12.BackColor = TheButtonColor(Counter)
    cmdButton12.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 13) Then
    cmdButton13.BackColor = TheButtonColor(Counter)
    cmdButton13.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 14) Then
    cmdButton14.BackColor = TheButtonColor(Counter)
    cmdButton14.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 15) Then
    cmdButton15.BackColor = TheButtonColor(Counter)
    cmdButton15.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 16) Then
    cmdButton16.BackColor = TheButtonColor(Counter)
    cmdButton16.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 17) Then
    cmdButton17.BackColor = TheButtonColor(Counter)
    cmdButton17.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 18) Then
    cmdButton18.BackColor = TheButtonColor(Counter)
    cmdButton18.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 19) Then
    cmdButton19.BackColor = TheButtonColor(Counter)
    cmdButton19.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 20) Then
    cmdButton20.BackColor = TheButtonColor(Counter)
    cmdButton20.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 21) Then
    cmdButton21.BackColor = TheButtonColor(Counter)
    cmdButton21.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 22) Then
    cmdButton22.BackColor = TheButtonColor(Counter)
    cmdButton22.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 23) Then
    cmdButton23.BackColor = TheButtonColor(Counter)
    cmdButton23.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 24) Then
    cmdButton24.BackColor = TheButtonColor(Counter)
    cmdButton24.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 25) Then
    cmdButton25.BackColor = TheButtonColor(Counter)
    cmdButton25.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 26) Then
    cmdButton26.BackColor = TheButtonColor(Counter)
    cmdButton26.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 27) Then
    cmdButton27.BackColor = TheButtonColor(Counter)
    cmdButton27.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 28) Then
    cmdButton28.BackColor = TheButtonColor(Counter)
    cmdButton28.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 29) Then
    cmdButton29.BackColor = TheButtonColor(Counter)
    cmdButton29.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 30) Then
    cmdButton30.BackColor = TheButtonColor(Counter)
    cmdButton30.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 31) Then
    cmdButton31.BackColor = TheButtonColor(Counter)
    cmdButton31.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 32) Then
    cmdButton32.BackColor = TheButtonColor(Counter)
    cmdButton32.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 33) Then
    cmdButton33.BackColor = TheButtonColor(Counter)
    cmdButton33.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 34) Then
    cmdButton34.BackColor = TheButtonColor(Counter)
    cmdButton34.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 35) Then
    cmdButton35.BackColor = TheButtonColor(Counter)
    cmdButton35.ForeColor = TheButtonTextColor(Counter)
ElseIf (Counter = 36) Then
    cmdButton36.BackColor = TheButtonColor(Counter)
    cmdButton36.ForeColor = TheButtonTextColor(Counter)
End If
TheButtons(Counter) = False
End Sub

Private Sub PlantButton(RefControl As DynamicControls, Counter As Integer)
Dim LibType As String
LibType = "fpBtn.fpBtn.2"
Counter = Counter + 1
If (Counter = 1) Then
    Set cmdButton1 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton1)
    TheButtonText(Counter) = cmdButton1.Text
ElseIf (Counter = 2) Then
    Set cmdButton2 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton2)
    TheButtonText(Counter) = cmdButton2.Text
ElseIf (Counter = 3) Then
    Set cmdButton3 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton3)
    TheButtonText(Counter) = cmdButton3.Text
ElseIf (Counter = 4) Then
    Set cmdButton4 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton4)
    TheButtonText(Counter) = cmdButton4.Text
ElseIf (Counter = 5) Then
    Set cmdButton5 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton5)
    TheButtonText(Counter) = cmdButton5.Text
ElseIf (Counter = 6) Then
    Set cmdButton6 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton6)
    TheButtonText(Counter) = cmdButton6.Text
ElseIf (Counter = 7) Then
    Set cmdButton7 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton7)
    TheButtonText(Counter) = cmdButton7.Text
ElseIf (Counter = 8) Then
    Set cmdButton8 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton8)
    TheButtonText(Counter) = cmdButton8.Text
ElseIf (Counter = 9) Then
    Set cmdButton9 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton9)
    TheButtonText(Counter) = cmdButton9.Text
ElseIf (Counter = 10) Then
    Set cmdButton10 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton10)
    TheButtonText(Counter) = cmdButton10.Text
ElseIf (Counter = 11) Then
    Set cmdButton11 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton11)
    TheButtonText(Counter) = cmdButton11.Text
ElseIf (Counter = 12) Then
    Set cmdButton12 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton12)
    TheButtonText(Counter) = cmdButton12.Text
ElseIf (Counter = 13) Then
    Set cmdButton13 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton13)
    TheButtonText(Counter) = cmdButton13.Text
ElseIf (Counter = 14) Then
    Set cmdButton14 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton14)
    TheButtonText(Counter) = cmdButton14.Text
ElseIf (Counter = 15) Then
    Set cmdButton15 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton15)
    TheButtonText(Counter) = cmdButton15.Text
ElseIf (Counter = 16) Then
    Set cmdButton16 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton16)
    TheButtonText(Counter) = cmdButton16.Text
ElseIf (Counter = 17) Then
    Set cmdButton17 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton17)
    TheButtonText(Counter) = cmdButton17.Text
ElseIf (Counter = 18) Then
    Set cmdButton18 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton18)
    TheButtonText(Counter) = cmdButton18.Text
ElseIf (Counter = 19) Then
    Set cmdButton19 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton19)
    TheButtonText(Counter) = cmdButton19.Text
ElseIf (Counter = 20) Then
    Set cmdButton20 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton20)
    TheButtonText(Counter) = cmdButton20.Text
ElseIf (Counter = 21) Then
    Set cmdButton21 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton21)
    TheButtonText(Counter) = cmdButton21.Text
ElseIf (Counter = 22) Then
    Set cmdButton22 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton22)
    TheButtonText(Counter) = cmdButton22.Text
ElseIf (Counter = 23) Then
    Set cmdButton23 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton23)
    TheButtonText(Counter) = cmdButton23.Text
ElseIf (Counter = 24) Then
    Set cmdButton24 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton24)
    TheButtonText(Counter) = cmdButton24.Text
ElseIf (Counter = 25) Then
    Set cmdButton25 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton25)
    TheButtonText(Counter) = cmdButton25.Text
ElseIf (Counter = 26) Then
    Set cmdButton26 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton26)
    TheButtonText(Counter) = cmdButton26.Text
ElseIf (Counter = 27) Then
    Set cmdButton27 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton27)
    TheButtonText(Counter) = cmdButton27.Text
ElseIf (Counter = 28) Then
    Set cmdButton28 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton28)
    TheButtonText(Counter) = cmdButton28.Text
ElseIf (Counter = 29) Then
    Set cmdButton29 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton29)
    TheButtonText(Counter) = cmdButton29.Text
ElseIf (Counter = 30) Then
    Set cmdButton30 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton30)
    TheButtonText(Counter) = cmdButton30.Text
ElseIf (Counter = 31) Then
    Set cmdButton31 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton31)
    TheButtonText(Counter) = cmdButton31.Text
ElseIf (Counter = 32) Then
    Set cmdButton32 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton32)
    TheButtonText(Counter) = cmdButton32.Text
ElseIf (Counter = 33) Then
    Set cmdButton33 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton30)
    TheButtonText(Counter) = cmdButton33.Text
ElseIf (Counter = 34) Then
    Set cmdButton34 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton30)
    TheButtonText(Counter) = cmdButton34.Text
ElseIf (Counter = 35) Then
    Set cmdButton35 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton35)
    TheButtonText(Counter) = cmdButton35.Text
ElseIf (Counter = 36) Then
    Set cmdButton36 = frmTestEntry.Controls.Add(LibType, "cmdButton" + Trim(str(Counter)))
    Call SetButtonCharacteristics(RefControl, cmdButton36)
    TheButtonText(Counter) = cmdButton36.Text
Else
    Exit Sub
End If
If (Trim(RefControl.IEChoiceName) <> "") Then
    TheButtonName(Counter) = Trim(RefControl.IEChoiceName)
Else
    TheButtonName(Counter) = "*" + Trim(RefControl.ControlName)
End If
TheButtonNest(Counter) = Trim(RefControl.ControlName)
TheButtonTrigger(Counter) = Trim(RefControl.ControlAlternateTrigger)
TheButtonTriggerText(Counter) = Trim(RefControl.ControlAlternateText)
TheButtonVisible(Counter) = RefControl.ControlVisibleAfterTrigger
TheButtonColor(Counter) = RefControl.BackColor
TheButtonTextColor(Counter) = RefControl.ForeColor
TheButtonDisplay(Counter) = Trim(RefControl.DoctorLingo)
TheButtons(Counter) = False
End Sub

Private Sub AudioAction(AButton As fpBtn, Ref As Integer)
If (TheAudioColor(Ref) = AButton.BackColor) Then
    SelectionMade = True
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
Else
    AButton.BackColor = TheAudioColor(Ref)
    AButton.ForeColor = TheAudioTextColor(Ref)
End If
TheAudio(Ref) = Not (TheAudio(Ref))
If Not (AlternateOn) Then
    If (TheAudio(Ref)) Then
        Call ProcessAudio
        If Not (ExitFast) Then
            Call PostTestResults
        End If
    End If
End If
' Check to see if single selection is on
If ((TheAudio(Ref)) And Not (ExitFast)) Then
    If Not (cmdApply.Visible) Then
        Call cmdApply_Click
        Exit Sub
    End If
End If
End Sub

Private Sub cmdAudio1_Click()
Call AudioAction(cmdAudio1, 1)
End Sub

Private Sub SetAudioCharacteristics(RefControl As DynamicControls, AButton As fpBtn)
Dim q As Integer
Dim VisualText As String
With AButton
    If (Trim(RefControl.ControlTemplateName) <> "") Then
        .TemplateName = BtnDir + Trim(RefControl.ControlTemplateName) + ".btn"
        .Action = fpActionLoadTemplate
        .CellIndex = 2
        .SegmentApplyTo = fpSegmentApplyToButton
        .BackStyle = fpBackStyleTransparentGrayArea
         RefControl.BackColor = .BackColor
    End If
    .BackColor = RefControl.BackColor
    If (Trim(RefControl.ControlText) <> "") Then
        q = InStrPS(RefControl.ControlText, "^")
        If (q <> 0) Then
            VisualText = Left(RefControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(RefControl.ControlText, q + 1, Len(RefControl.ControlText) - q)
        Else
            VisualText = RefControl.ControlText
        End If
        .Text = VisualText
    Else
        .Text = RefControl.ControlName
    End If
    If (Trim(RefControl.IEChoiceName) <> "") Then
        .Tag = RefControl.IEChoiceName
    Else
        .Tag = RefControl.ControlName
    End If
    .ForeColor = RefControl.ForeColor
    .Left = RefControl.ControlLeft
    .Top = RefControl.ControlTop
    .Height = RefControl.ControlHeight
    .Width = RefControl.ControlWidth
    .TabIndex = RefControl.TabIndex
    .Visible = RefControl.ControlVisible
    .Enabled = True
    .Font.Size = RefControl.ResponseLength
    If (Trim(RefControl.ControlFont) = "") Then
        .Font.Name = "Lucida Sans"
    Else
        .Font.Name = Trim(RefControl.ControlFont)
    End If
    .FontBold = False
    .ToolTipText = ""
End With
End Sub

Private Sub InitializeAudio(Counter As Integer)
If (Counter = 1) Then
    cmdAudio1.BackColor = TheAudioColor(Counter)
    cmdAudio1.ForeColor = TheAudioTextColor(Counter)
End If
TheAudio(Counter) = False
End Sub

Private Sub PlantAudio(RefControl As DynamicControls, Counter As Integer)
Dim LibType As String
LibType = "fpBtn.fpBtn.2"
Counter = Counter + 1
If (Counter = 1) Then
    Set cmdAudio1 = frmTestEntry.Controls.Add(LibType, "cmdAudio" + Trim(str(Counter)))
    Call SetAudioCharacteristics(RefControl, cmdAudio1)
    TheAudioText(Counter) = cmdAudio1.Text
Else
    Exit Sub
End If
If (Trim(RefControl.IEChoiceName) <> "") Then
    TheAudioName(Counter) = Trim(RefControl.IEChoiceName)
Else
    TheAudioName(Counter) = "*" + Trim(RefControl.ControlName)
End If
TheAudioVisible(Counter) = RefControl.ControlVisibleAfterTrigger
TheAudioColor(Counter) = RefControl.BackColor
TheAudioTextColor(Counter) = RefControl.ForeColor
TheAudioDisplay(Counter) = Trim(RefControl.DoctorLingo)
TheAudio(Counter) = False
End Sub

Private Sub BtnAction(AButton As fpBtn, Ref As Integer)
Dim z As Integer
If (TheBtnColor(Ref) = AButton.BackColor) Then
    SelectionMade = True
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
Else
    AButton.BackColor = TheBtnColor(Ref)
    AButton.ForeColor = TheBtnTextColor(Ref)
End If
TheBtns(Ref) = Not (TheBtns(Ref))
If (TheBtns(Ref)) Then
    Call SetRTestButtons(False)
    AButton.Visible = True
    RtriggerOn = True
    Call cmdButton1_Click
End If
End Sub

Private Sub SetBtnCharacteristics(RefControl As DynamicControls, AButton As fpBtn)
Dim q As Integer
Dim VisualText As String
With AButton
    If (Trim(RefControl.ControlTemplateName) <> "") Then
        .TemplateName = BtnDir + Trim(RefControl.ControlTemplateName) + ".btn"
        .Action = fpActionLoadTemplate
        .CellIndex = 2
        .SegmentApplyTo = fpSegmentApplyToButton
        .BackStyle = fpBackStyleTransparentGrayArea
        RefControl.BackColor = .BackColor
    End If
    .BackColor = RefControl.BackColor
    If (Trim(RefControl.ControlText) <> "") Then
        q = InStrPS(RefControl.ControlText, "^")
        If (q <> 0) Then
            VisualText = Left(RefControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(RefControl.ControlText, q + 1, Len(RefControl.ControlText) - q)
        Else
            VisualText = RefControl.ControlText
        End If
        .Text = VisualText
    Else
        .Text = RefControl.ControlName
    End If
    If (Trim(RefControl.IEChoiceName) <> "") Then
        .Tag = RefControl.IEChoiceName
    Else
        .Tag = RefControl.ControlName
    End If
    .ForeColor = RefControl.ForeColor
    .Left = RefControl.ControlLeft
    .Top = RefControl.ControlTop
    .Height = RefControl.ControlHeight
    .Width = RefControl.ControlWidth
    .TabIndex = RefControl.TabIndex
    .Visible = RefControl.ControlVisible
    .Enabled = True
    .Font.Size = RefControl.ResponseLength
    If (Trim(RefControl.ControlFont) = "") Then
        .Font.Name = "Lucida Sans"
    Else
        .Font.Name = Trim(RefControl.ControlFont)
    End If
    .FontBold = False
    .ToolTipText = ""
End With
End Sub

Private Sub InitializeBtn(Counter As Integer)
If (Counter = 1) Then
    cmdBtn1.BackColor = TheBtnColor(Counter)
    cmdBtn1.ForeColor = TheBtnTextColor(Counter)
ElseIf (Counter = 2) Then
    cmdBtn2.BackColor = TheBtnColor(Counter)
    cmdBtn2.ForeColor = TheBtnTextColor(Counter)
ElseIf (Counter = 3) Then
    cmdBtn3.BackColor = TheBtnColor(Counter)
    cmdBtn3.ForeColor = TheBtnTextColor(Counter)
ElseIf (Counter = 4) Then
    cmdBtn4.BackColor = TheBtnColor(Counter)
    cmdBtn4.ForeColor = TheBtnTextColor(Counter)
ElseIf (Counter = 5) Then
    cmdBtn5.BackColor = TheBtnColor(Counter)
    cmdBtn5.ForeColor = TheBtnTextColor(Counter)
ElseIf (Counter = 6) Then
    cmdBtn6.BackColor = TheBtnColor(Counter)
    cmdBtn6.ForeColor = TheBtnTextColor(Counter)
ElseIf (Counter = 7) Then
    cmdBtn7.BackColor = TheBtnColor(Counter)
    cmdBtn7.ForeColor = TheBtnTextColor(Counter)
ElseIf (Counter = 8) Then
    cmdBtn8.BackColor = TheBtnColor(Counter)
    cmdBtn8.ForeColor = TheBtnTextColor(Counter)
End If
TheBtns(Counter) = False
End Sub

Private Sub PlantBtn(RefControl As DynamicControls, Counter As Integer)
Dim LibType As String
LibType = "fpBtn.fpBtn.2"
Counter = Counter + 1
If (Counter = 1) Then
    Set cmdBtn1 = frmTestEntry.Controls.Add(LibType, "cmdBtn" + Trim(str(Counter)))
    Call SetBtnCharacteristics(RefControl, cmdBtn1)
    TheBtnText(Counter) = cmdBtn1.Text
ElseIf (Counter = 2) Then
    Set cmdBtn2 = frmTestEntry.Controls.Add(LibType, "cmdBtn" + Trim(str(Counter)))
    Call SetBtnCharacteristics(RefControl, cmdBtn2)
    TheBtnText(Counter) = cmdBtn2.Text
ElseIf (Counter = 3) Then
    Set cmdBtn3 = frmTestEntry.Controls.Add(LibType, "cmdBtn" + Trim(str(Counter)))
    Call SetBtnCharacteristics(RefControl, cmdBtn3)
    TheBtnText(Counter) = cmdBtn3.Text
ElseIf (Counter = 4) Then
    Set cmdBtn4 = frmTestEntry.Controls.Add(LibType, "cmdBtn" + Trim(str(Counter)))
    Call SetBtnCharacteristics(RefControl, cmdBtn4)
    TheBtnText(Counter) = cmdBtn4.Text
ElseIf (Counter = 5) Then
    Set cmdBtn5 = frmTestEntry.Controls.Add(LibType, "cmdBtn" + Trim(str(Counter)))
    Call SetBtnCharacteristics(RefControl, cmdBtn5)
    TheBtnText(Counter) = cmdBtn5.Text
ElseIf (Counter = 6) Then
    Set cmdBtn6 = frmTestEntry.Controls.Add(LibType, "cmdBtn" + Trim(str(Counter)))
    Call SetBtnCharacteristics(RefControl, cmdBtn6)
    TheBtnText(Counter) = cmdBtn6.Text
ElseIf (Counter = 7) Then
    Set cmdBtn7 = frmTestEntry.Controls.Add(LibType, "cmdBtn" + Trim(str(Counter)))
    Call SetBtnCharacteristics(RefControl, cmdBtn7)
    TheBtnText(Counter) = cmdBtn7.Text
ElseIf (Counter = 8) Then
    Set cmdBtn8 = frmTestEntry.Controls.Add(LibType, "cmdBtn" + Trim(str(Counter)))
    Call SetBtnCharacteristics(RefControl, cmdBtn8)
    TheBtnText(Counter) = cmdBtn8.Text
Else
    Exit Sub
End If
If (Trim(RefControl.IEChoiceName) <> "") Then
    TheBtnName(Counter) = Trim(RefControl.IEChoiceName)
Else
    TheBtnName(Counter) = "*" + Trim(RefControl.ControlName)
End If
TheBtnNest(Counter) = Trim(RefControl.ControlName)
TheBtnTrigger(Counter) = Trim(RefControl.ControlAlternateTrigger)
TheBtnTriggerText(Counter) = Trim(RefControl.ControlAlternateText)
TheBtnVisible(Counter) = RefControl.ControlVisibleAfterTrigger
TheBtnColor(Counter) = RefControl.BackColor
TheBtnTextColor(Counter) = RefControl.ForeColor
TheBtnDisplay(Counter) = Trim(RefControl.DoctorLingo)
TheBtns(Counter) = False
End Sub

Private Sub cmdBtn1_Click()
Call BtnAction(cmdBtn1, 1)
End Sub

Private Sub cmdBtn2_Click()
Call BtnAction(cmdBtn2, 2)
End Sub

Private Sub cmdBtn3_Click()
Call BtnAction(cmdBtn3, 3)
End Sub

Private Sub cmdBtn4_Click()
Call BtnAction(cmdBtn4, 4)
End Sub

Private Sub cmdBtn5_Click()
Call BtnAction(cmdBtn5, 5)
End Sub

Private Sub cmdBtn6_Click()
Call BtnAction(cmdBtn6, 6)
End Sub

Private Sub cmdBtn7_Click()
Call BtnAction(cmdBtn7, 7)
End Sub

Private Sub cmdBtn8_Click()
Call BtnAction(cmdBtn8, 8)
End Sub

Private Sub PadAction(APad As fpBtn, Ref As Integer)
Dim q, g As Integer
Dim iMax As Double
Dim iMin As Double
Dim iDefault As Double
If (ThePadExoticOn(Ref)) Then
    Call EPadAction(APad, Ref)
    Exit Sub
End If
If (ThePadColor(Ref) = APad.BackColor) Then
    SelectionMade = True
    APad.BackColor = ButtonSelectionColor
    APad.ForeColor = TextSelectionColor
Else
    APad.BackColor = ThePadColor(Ref)
    APad.ForeColor = ThePadTextColor(Ref)
End If
ThePads(Ref) = Not (ThePads(Ref))
g = InStrPS(APad.Tag, "/")
If (g = 0) Then
    iMax = 0
    iMin = 0
    iDefault = Val(Trim(APad.Tag))
Else
    iMax = Val(Left(APad.Tag, g - 1))
    q = InStrPS(g + 1, APad.Tag, "/")
    If (q = 0) Then
        iMin = Val(Mid(APad.Tag, g + 1, Len(APad.Tag) - g))
        iDefault = iMin
    Else
        iMin = Val(Mid(APad.Tag, g + 1, (q - 1) - g))
        If (Mid(APad.Tag, q + 1, Len(APad.Tag) - q) = "?") Then
            iDefault = -9999
        Else
            iDefault = Val(Mid(APad.Tag, q + 1, Len(APad.Tag) - q))
        End If
    End If
End If
frmNumericPad.NumPad_Result = ""
If (Left(ThePadName(Ref), 1) = "*") Then
    frmNumericPad.NumPad_Field = CurrentEye + " " + Mid(ThePadName(Ref), 2, Len(ThePadName(Ref)) - 1)
Else
    frmNumericPad.NumPad_Field = CurrentEye + " " + ThePadName(Ref)
End If
frmNumericPad.NumPad_Max = iMax
frmNumericPad.NumPad_Min = iMin
frmNumericPad.NumPad_Default = iDefault
frmNumericPad.NumPad_ForceDecimal = False
frmNumericPad.NumPad_HelpField = ThePadHelpText(Ref)
frmNumericPad.NumPad_Increment = ThePadIncrement(Ref)
frmNumericPad.NumPad_CommentOn = False
If (ThePadDecimal(Ref) <> "N") Then
    frmNumericPad.NumPad_ForceDecimal = True
End If
frmNumericPad.NumPad_EyesOn = False
frmNumericPad.NumPad_TimeFrameOn = False
frmNumericPad.NumPad_DisplayFull = True
frmNumericPad.NumPad_Choice1 = TheChoice(1)
frmNumericPad.NumPad_Choice2 = TheChoice(2)
frmNumericPad.NumPad_Choice3 = TheChoice(3)
frmNumericPad.NumPad_Choice4 = TheChoice(4)
frmNumericPad.NumPad_Choice5 = TheChoice(5)
frmNumericPad.NumPad_Choice6 = TheChoice(6)
frmNumericPad.NumPad_Choice7 = TheChoice(7)
frmNumericPad.NumPad_Choice8 = TheChoice(8)
frmNumericPad.NumPad_Choice9 = TheChoice(9)
frmNumericPad.NumPad_Choice10 = TheChoice(10)
frmNumericPad.NumPad_Choice11 = TheChoice(11)
frmNumericPad.NumPad_Choice12 = TheChoice(12)
frmNumericPad.NumPad_Choice13 = TheChoice(13)
frmNumericPad.NumPad_Choice14 = TheChoice(14)
frmNumericPad.NumPad_Choice15 = TheChoice(15)
frmNumericPad.NumPad_Choice16 = TheChoice(16)
frmNumericPad.NumPad_Choice17 = TheChoice(17)
frmNumericPad.NumPad_Choice18 = TheChoice(18)
frmNumericPad.NumPad_Choice19 = TheChoice(19)
frmNumericPad.NumPad_Choice20 = TheChoice(20)
frmNumericPad.NumPad_Choice21 = TheChoice(21)
frmNumericPad.NumPad_Choice22 = TheChoice(22)
frmNumericPad.NumPad_Choice23 = TheChoice(23)
frmNumericPad.NumPad_Choice24 = TheChoice(24)
frmNumericPad.cmdLTR.Visible = False
frmNumericPad.Show 1
txtBMI.Text = ""
If Not (frmNumericPad.NumPad_Quit) Then
    If (Trim(frmNumericPad.NumPad_Result) <> "") Then
        If (frmNumericPad.NumPad_Result <> "-") Then
            If Not (frmNumericPad.NumPad_NoValue) Then
                ThePadValue(Ref) = frmNumericPad.NumPad_Result
                'Changes Made for BMI
                If UCase(Question) = "HEIGHT AND WEIGHT" Then
                    Dim str() As String
                    str = Split(ThePadName(Ref), "-")
                     If UCase(str(0)) = UCase("*Height") Then
                         ConvertHeightToInche CDbl(ThePadValue(Ref)), str(1)
                         ValueInInche = iValH(0) + iValH(1) + iValH(2) + iValH(3)
                     ElseIf UCase(str(0)) = UCase("*Weight") Then
                       ConvertWeightToLbs CDbl(ThePadValue(Ref)), str(1)
                        ValueInLbs = iValW(0) + iValW(1) + iValW(2) + iValW(3)
                     End If
                    If ValueInLbs <> 0 And ValueInInche <> 0 Then
                        ValueInBMI = (ValueInLbs * 703) / (ValueInInche * ValueInInche)
                    Else
                        txtBMI.Text = ""
                    End If
                End If
            Else
                If (EyeWearOn) Then
                    ThePadValue(Ref) = "NE"
                Else
                    ThePadValue(Ref) = ""
                End If
            End If
        Else
            ThePadValue(Ref) = ""
        End If
    Else
        If (EyeWearOn) Then
            ThePadValue(Ref) = "NE"
        Else
            ThePadValue(Ref) = ""
        End If
    End If
    If (frmNumericPad.NumPad_ChoiceOn1) Then
        ThePadChoice(Ref, 1) = TheChoiceName(1)
    End If
    If (frmNumericPad.NumPad_ChoiceOn2) Then
        ThePadChoice(Ref, 2) = TheChoiceName(2)
    End If
    If (frmNumericPad.NumPad_ChoiceOn3) Then
        ThePadChoice(Ref, 3) = TheChoiceName(3)
    End If
    If (frmNumericPad.NumPad_ChoiceOn4) Then
        ThePadChoice(Ref, 4) = TheChoiceName(4)
    End If
    If (frmNumericPad.NumPad_ChoiceOn5) Then
        ThePadChoice(Ref, 5) = TheChoiceName(5)
    End If
    If (frmNumericPad.NumPad_ChoiceOn6) Then
        ThePadChoice(Ref, 6) = TheChoiceName(6)
    End If
    If (frmNumericPad.NumPad_ChoiceOn7) Then
        ThePadChoice(Ref, 7) = TheChoiceName(7)
    End If
    If (frmNumericPad.NumPad_ChoiceOn8) Then
        ThePadChoice(Ref, 8) = TheChoiceName(8)
    End If
    If (frmNumericPad.NumPad_ChoiceOn9) Then
        ThePadChoice(Ref, 9) = TheChoiceName(9)
    End If
    If (frmNumericPad.NumPad_ChoiceOn10) Then
        ThePadChoice(Ref, 10) = TheChoiceName(10)
    End If
    If (frmNumericPad.NumPad_ChoiceOn11) Then
        ThePadChoice(Ref, 11) = TheChoiceName(11)
    End If
    If (frmNumericPad.NumPad_ChoiceOn12) Then
        ThePadChoice(Ref, 12) = TheChoiceName(12)
    End If
    If (frmNumericPad.NumPad_ChoiceOn13) Then
        ThePadChoice(Ref, 13) = TheChoiceName(13)
    End If
    If (frmNumericPad.NumPad_ChoiceOn14) Then
        ThePadChoice(Ref, 14) = TheChoiceName(14)
    End If
    If (frmNumericPad.NumPad_ChoiceOn15) Then
        ThePadChoice(Ref, 15) = TheChoiceName(15)
    End If
    If (frmNumericPad.NumPad_ChoiceOn16) Then
        ThePadChoice(Ref, 16) = TheChoiceName(16)
    End If
    If (frmNumericPad.NumPad_ChoiceOn17) Then
        ThePadChoice(Ref, 17) = TheChoiceName(17)
    End If
    If (frmNumericPad.NumPad_ChoiceOn18) Then
        ThePadChoice(Ref, 18) = TheChoiceName(18)
    End If
    If (frmNumericPad.NumPad_ChoiceOn19) Then
        ThePadChoice(Ref, 19) = TheChoiceName(19)
    End If
    If (frmNumericPad.NumPad_ChoiceOn20) Then
        ThePadChoice(Ref, 20) = TheChoiceName(20)
    End If
    If (frmNumericPad.NumPad_ChoiceOn21) Then
        ThePadChoice(Ref, 21) = TheChoiceName(21)
    End If
    If (frmNumericPad.NumPad_ChoiceOn22) Then
        ThePadChoice(Ref, 22) = TheChoiceName(22)
    End If
    If (frmNumericPad.NumPad_ChoiceOn23) Then
        ThePadChoice(Ref, 23) = TheChoiceName(23)
    End If
    If (frmNumericPad.NumPad_ChoiceOn24) Then
        ThePadChoice(Ref, 24) = TheChoiceName(24)
    End If
    frmTestEntry.Refresh
Else
    ExitFast = True
End If
End Sub

Private Sub cmdPad1_Click()
Call PadAction(cmdPad1, 1)
End Sub

Private Sub cmdPad2_Click()
Call PadAction(cmdPad2, 2)
End Sub

Private Sub cmdPad3_Click()
Call PadAction(cmdPad3, 3)
End Sub

Private Sub cmdPad4_Click()
Call PadAction(cmdPad4, 4)
End Sub

Private Sub cmdPad5_Click()
Call PadAction(cmdPad5, 5)
End Sub

Private Sub cmdPad6_Click()
Call PadAction(cmdPad6, 6)
End Sub

Private Sub cmdPad7_Click()
Call PadAction(cmdPad7, 7)
End Sub

Private Sub cmdPad8_Click()
Call PadAction(cmdPad8, 8)
End Sub

Private Sub InitializePad(Counter As Integer)
Dim i As Integer
If (Counter = 1) Then
    cmdPad1.BackColor = ThePadColor(Counter)
    cmdPad1.ForeColor = ThePadTextColor(Counter)
ElseIf (Counter = 2) Then
    cmdPad2.BackColor = ThePadColor(Counter)
    cmdPad2.ForeColor = ThePadTextColor(Counter)
ElseIf (Counter = 3) Then
    cmdPad3.BackColor = ThePadColor(Counter)
    cmdPad3.ForeColor = ThePadTextColor(Counter)
ElseIf (Counter = 4) Then
    cmdPad4.BackColor = ThePadColor(Counter)
    cmdPad4.ForeColor = ThePadTextColor(Counter)
ElseIf (Counter = 5) Then
    cmdPad5.BackColor = ThePadColor(Counter)
    cmdPad5.ForeColor = ThePadTextColor(Counter)
ElseIf (Counter = 6) Then
    cmdPad6.BackColor = ThePadColor(Counter)
    cmdPad6.ForeColor = ThePadTextColor(Counter)
ElseIf (Counter = 7) Then
    cmdPad7.BackColor = ThePadColor(Counter)
    cmdPad7.ForeColor = ThePadTextColor(Counter)
ElseIf (Counter = 8) Then
    cmdPad8.BackColor = ThePadColor(Counter)
    cmdPad8.ForeColor = ThePadTextColor(Counter)
End If
For i = 1 To MaxPadChoices
    ThePadChoice(Counter, i) = ""
Next i
ThePads(Counter) = False
ThePadValue(Counter) = ""
End Sub

Private Sub SetPadCharacteristics(RefControl As DynamicControls, AButton As fpBtn)
Dim VisualText As String
Dim iDefault, iMax, iMin As Double
Dim g, q As Integer
q = InStrPS(RefControl.ControlTemplateName, "/")
If (q <> 0) Then
    iMin = Val(Trim(Left(RefControl.ControlTemplateName, q - 1)))
    g = InStrPS(q + 1, RefControl.ControlTemplateName, "/")
    If (g = 0) Then
        iMax = Val(Trim(Mid(RefControl.ControlTemplateName, q + 1, Len(RefControl.ControlTemplateName) - q)))
        iDefault = iMin
    Else
        iMax = Val(Trim(Mid(RefControl.ControlTemplateName, q + 1, (g - 1) - q)))
        iDefault = Val(Trim(Mid(RefControl.ControlTemplateName, g + 1, Len(RefControl.ControlTemplateName) - g)))
    End If
Else
    iMin = 0
    iMax = Val(Trim(Mid(RefControl.ControlTemplateName, q + 1, Len(RefControl.ControlTemplateName) - q)))
    iDefault = iMin
End If
If (iMax < iMin) Then
    q = iMin
    iMin = iMax
    iMax = q
End If
If (iDefault < iMin) Then
    iDefault = iMin
End If
With AButton
    .TemplateName = BtnDir + "rec1.btn"
    .Action = fpActionLoadTemplate
    .CellIndex = 2
    .SegmentApplyTo = fpSegmentApplyToButton
    .BackStyle = fpBackStyleTransparentGrayArea
    RefControl.BackColor = .BackColor
    If (Trim(RefControl.ControlText) <> "") Then
        q = InStrPS(RefControl.ControlText, "^")
        If (q <> 0) Then
            VisualText = Left(RefControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(RefControl.ControlText, q + 1, Len(RefControl.ControlText) - q)
        Else
            VisualText = RefControl.ControlText
        End If
        .Text = VisualText
    Else
        .Text = RefControl.ControlName
    End If
    If (Trim(RefControl.IEChoiceName) <> "") Then
        .Tag = RefControl.IEChoiceName
    Else
        .Tag = RefControl.ControlName
    End If
    .Tag = Trim(str(iMax)) + "/" + Trim(str(iMin)) + "/" + Trim(iDefault)
    .ForeColor = RefControl.ForeColor
    .Left = RefControl.ControlLeft
    .Top = RefControl.ControlTop
    .Height = RefControl.ControlHeight
    .Width = RefControl.ControlWidth
    .TabIndex = RefControl.TabIndex
    .Visible = RefControl.ControlVisible
    .Enabled = True
    .Font.Size = RefControl.ResponseLength
    If (Trim(RefControl.ControlFont) = "") Then
        .Font.Name = "Lucida Sans"
    Else
        .Font.Name = Trim(RefControl.ControlFont)
        .Font.Name = "Lucida Sans"
    End If
    .FontBold = False
    .ToolTipText = ""
End With
End Sub

Private Sub PlantNumericPad(RefControl As DynamicControls, Counter As Integer)
Dim LibType As String
LibType = "fpBtn.fpBtn.2"
Counter = Counter + 1
If (Counter = 1) Then
    Set cmdPad1 = frmTestEntry.Controls.Add(LibType, "cmdPad" + Trim(str(Counter)))
    Call SetPadCharacteristics(RefControl, cmdPad1)
ElseIf (Counter = 2) Then
    Set cmdPad2 = frmTestEntry.Controls.Add(LibType, "cmdPad" + Trim(str(Counter)))
    Call SetPadCharacteristics(RefControl, cmdPad2)
ElseIf (Counter = 3) Then
    Set cmdPad3 = frmTestEntry.Controls.Add(LibType, "cmdPad" + Trim(str(Counter)))
    Call SetPadCharacteristics(RefControl, cmdPad3)
ElseIf (Counter = 4) Then
    Set cmdPad4 = frmTestEntry.Controls.Add(LibType, "cmdPad" + Trim(str(Counter)))
    Call SetPadCharacteristics(RefControl, cmdPad4)
ElseIf (Counter = 5) Then
    Set cmdPad5 = frmTestEntry.Controls.Add(LibType, "cmdPad" + Trim(str(Counter)))
    Call SetPadCharacteristics(RefControl, cmdPad5)
ElseIf (Counter = 6) Then
    Set cmdPad6 = frmTestEntry.Controls.Add(LibType, "cmdPad" + Trim(str(Counter)))
    Call SetPadCharacteristics(RefControl, cmdPad6)
ElseIf (Counter = 7) Then
    Set cmdPad7 = frmTestEntry.Controls.Add(LibType, "cmdPad" + Trim(str(Counter)))
    Call SetPadCharacteristics(RefControl, cmdPad7)
ElseIf (Counter = 8) Then
    Set cmdPad8 = frmTestEntry.Controls.Add(LibType, "cmdPad" + Trim(str(Counter)))
    Call SetPadCharacteristics(RefControl, cmdPad8)
Else
    Exit Sub
End If
If (Trim(RefControl.IEChoiceName) <> "") Then
    ThePadName(Counter) = Trim(RefControl.IEChoiceName)
Else
    ThePadName(Counter) = "*" + Trim(RefControl.ControlName)
End If
ThePadColor(Counter) = RefControl.BackColor
ThePadTextColor(Counter) = RefControl.ForeColor
ThePadDisplay(Counter) = Trim(RefControl.DoctorLingo)
ThePadVisible(Counter) = RefControl.ControlVisibleAfterTrigger
ThePadTrigger(Counter) = RefControl.ControlAlternateTrigger
ThePadTriggerText(Counter) = RefControl.ControlAlternateText
ThePadExoticOn(Counter) = False
If (ThePadTriggerText(Counter) = "EPad") Then
    ThePadExoticOn(Counter) = True
End If
ThePadDecimal(Counter) = RefControl.DecimalDefault
ThePadHelpText(Counter) = Trim(RefControl.ConfirmLingo)
ThePadIncrement(Counter) = Trim(RefControl.AIncrement)
If (Trim(ThePadDecimal(Counter)) = "") Then
    ThePadDecimal(Counter) = "N"
End If
End Sub

Private Sub EPadAction(APad As fpBtn, Ref As Integer)
Dim i As Integer
Dim q As Integer
Dim g As Integer
Dim ATemp As String
If (ThePadColor(Ref) = APad.BackColor) Then
    APad.BackColor = ButtonSelectionColor
    APad.ForeColor = TextSelectionColor
Else
    APad.BackColor = ThePadColor(Ref)
    APad.ForeColor = ThePadTextColor(Ref)
End If
ThePads(Ref) = Not (ThePads(Ref))
If (ThePads(Ref)) Then
    frmTestTrack.EyeContext = ""
    frmTestTrack.PatientId = PatientId
    frmTestTrack.AppointmentId = AppointmentId
    If (frmTestTrack.LoadTestTrack(CurrentFormId)) Then
        frmTestTrack.Show 1
        For i = 1 To frmTestTrack.TotalFindings
            If (frmTestTrack.GetFindingValue(i, ATemp)) Then
                ThePadValue(Ref) = ThePadValue(Ref) + ATemp + "&"
            End If
        Next i
        For i = 1 To frmTestTrack.TotalImpressions
            If (frmTestTrack.GetImpressionValue(i, ATemp)) Then
                ThePadChoice(Ref, i) = ATemp
            End If
        Next i
        frmTestEntry.Refresh
    Else
        ExitFast = True
    End If
Else
    ThePadValue(Ref) = ""
    For i = 1 To MaxPadChoices
        ThePadChoice(Ref, i) = ""
    Next i
End If
End Sub

Private Sub PlantLabel(RefControl As DynamicControls, Counter As Integer)
Dim VisualText As String
Dim q As Integer
Counter = Counter + 1
Set lblLabel = frmTestEntry.Controls.Add("VB.Label", "Label" + Trim(str(Counter)))
With lblLabel
    .Top = RefControl.ControlTop
    If (.Top < 1) Then
        .Top = 1
    End If
    .Left = RefControl.ControlLeft
    If (.Left < 1) Then
        .Left = 1
    End If
    If (RefControl.ControlWidth > 0) Then
        .AutoSize = False
        .Width = RefControl.ControlWidth
        If (.Left + .Width > frmTestEntry.Width) Then
            .Width = (frmTestEntry.Width - (.Left + 2))
        End If
        .Alignment = 2
    Else
        .AutoSize = True
    End If
    .Alignment = 2
    .WordWrap = True
    .BackColor = RefControl.BackColor
    '.ForeColor = RefControl.ForeColor
    .ForeColor = &H0&
    .BackStyle = vbTransparent
    
    If (Trim(RefControl.ControlText) = "") Then
        .Caption = RefControl.ControlName
    Else
        q = InStrPS(RefControl.ControlText, "^")
        If (q = 0) Then
            VisualText = RefControl.ControlText
        Else
            VisualText = RefControl.ControlText
            While (q <> 0)
                .AutoSize = True
                VisualText = Left(VisualText, q - 1) + Chr(13) + Chr(10) + Mid(VisualText, q + 1, Len(VisualText) - q)
                q = InStrPS(VisualText, "^")
            Wend
        End If
        .Caption = VisualText
    End If
    .TabIndex = RefControl.TabIndex
    .Visible = True
    .Enabled = True
    If (RefControl.ResponseLength > 100) Then
        .Height = RefControl.ResponseLength
    Else
        .Height = 24 * RefControl.ControlHeight
    End If
    .Font.Size = RefControl.ControlHeight
    If (Trim(RefControl.ControlFont) = "") Then
        .Font.Name = "Lucida Sans"
    Else
        .Font.Name = Trim(RefControl.ControlFont)
        .Font.Name = "Lucida Sans"
    End If
    .Font.Bold = True
End With
End Sub

Private Function DynamicEntry(Question As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Integer
Dim FileNum As Integer
DynamicEntry = True
lblAnnual.Visible = False
JustANote = False
EyeWearOn = False
CurrentOrder = ""
Set TheForm = New DynamicForms
Set TheControl = New DynamicControls
TheForm.Question = UCase(Trim(Question))
If (TheForm.RetrieveForm) And (TheForm.FormId > 0) Then
    i = InStrPS(StoreResults, "_")
    If (i > 0) Then
        CurrentOrder = Mid(StoreResults, i - 3, 3)
        If (Mid(StoreResults, i - 4, 1) = "G") Then
            EyeWearOn = True
        End If
    End If
    CurrentFormId = TheForm.FormId
    FormColor = TheForm.BackColor
    frmTestEntry.BorderStyle = 0
    frmTestEntry.BackColor = TheForm.BackColor
    frmTestEntry.ForeColor = TheForm.ForeColor
    frmTestEntry.Caption = TheForm.FormName
    frmTestEntry.WindowState = TheForm.WindowState
    frmTestEntry.BorderStyle = 0
    frmTestEntry.Enabled = True
    If (TheForm.ApplyButton) Then
        frmTestEntry.cmdApply.GrayAreaColor = frmTestEntry.BackColor
        frmTestEntry.cmdApply.Enabled = True
        frmTestEntry.cmdApply.Visible = True
    Else
        frmTestEntry.cmdApply.Enabled = False
        frmTestEntry.cmdApply.Visible = False
    End If

    frmTestEntry.cmdNotes.GrayAreaColor = frmTestEntry.BackColor
    frmTestEntry.cmdNotes.Enabled = True
    frmTestEntry.cmdNotes.Visible = True
    
    frmTestEntry.cmdQuit.GrayAreaColor = frmTestEntry.BackColor
    frmTestEntry.cmdQuit.Enabled = True
    frmTestEntry.cmdQuit.Visible = True
    
    frmTestEntry.cmdRetain.GrayAreaColor = frmTestEntry.BackColor
    frmTestEntry.cmdRetain.Enabled = True
    frmTestEntry.cmdRetain.Visible = True
' Build the form's controls,
' They are different based on the Question that drives the Form
    TheControl.FormId = TheForm.FormId
    MaxControls = TheControl.FindControl
    If (MaxControls > 0) Then
        MaxPads = 0
        MaxBtns = 0
        MaxLabel = 0
        MaxButtons = 0
        MaxAudio = 0
        For i = 1 To MaxControls
            If (TheControl.SelectControl(i)) Then
                If Not (TheControl.IgnoreIt) Then
                    If (TheControl.ControlType = "L") Then
                        Call PlantLabel(TheControl, MaxLabel)
                    ElseIf (TheControl.ControlType = "B") Then
                        Call PlantButton(TheControl, MaxButtons)
                        TheButtonId(MaxButtons) = TheControl.ControlId
                    ElseIf (TheControl.ControlType = "A") Then
                        Call PlantAudio(TheControl, MaxAudio)
                        TheAudioId(MaxAudio) = TheControl.ControlId
                    ElseIf (TheControl.ControlType = "S") Then
                        Call PlantNumericPad(TheControl, MaxPads)
                        ThePadId(MaxPads) = TheControl.ControlId
                    ElseIf (TheControl.ControlType = "N") Then
                        Call PlantNumericPad(TheControl, MaxPads)
                        ThePadId(MaxPads) = TheControl.ControlId
                    ElseIf (TheControl.ControlType = "R") Then
                        Call PlantBtn(TheControl, MaxBtns)
                        TheBtnId(MaxBtns) = TheControl.ControlId
                    End If
                End If
            End If
        Next i
    End If
' Check for Static Label, used to set external data also
    If (Trim(TheForm.StaticLabel) <> "") Then
        TheControl.FormId = 99999
        TheControl.ControlName = Trim(TheForm.StaticLabel)
        If (TheControl.FindControl > 0) Then
            If (TheControl.SelectControl(1)) Then
                If (TheControl.ControlType = "L") Then
                    Call PlantLabel(TheControl, MaxLabel)
                End If
            End If
        End If
    End If
Else
    DynamicEntry = False
    Exit Function
End If
For i = 1 To MaxButtons
    Call InitializeButton(i)
Next i
For i = 1 To MaxPads
    Call InitializePad(i)
Next i
For i = 1 To MaxBtns
    Call InitializeBtn(i)
Next i
If (MaxButtons > 2) Then
    If (cmdButton1.Text = "OD") Then
        cmdButton2.Enabled = False
    End If
    If (MaxBtns > 0) Then
        cmdButton1.Enabled = False
    End If
End If
If Not (cmdButton1.Visible) And Not (cmdButton2.Visible) And (MaxBtns < 1) Then
    FileNum = FreeFile
    FM.OpenFile ScratchTestFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
    FM.CloseFile CLng(FileNum)
End If
Exit Function
UI_ErrorHandler:
    DynamicEntry = False
    Set TheForm = Nothing
    Set TheControl = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Sub Form_Load()
Dim z As Integer
frmTestEntry.KeyPreview = True
ButtonSelectionColor = 14745312
TextSelectionColor = 0
PostedQuestion = False
RtriggerOn = False
AlternateOn = False
txtResults.Text = ""
txtBMI.Text = ""
'txtBMI.Visible = False
CurrentPostingSet = 0
SelectionMade = False
ExitFast = False
ValueInInche = 0
ValueInLbs = 0
ValueInBMI = 0
iValH(0) = 0
iValH(1) = 0
iValH(2) = 0
iValH(3) = 0
iValW(0) = 0
iValW(1) = 0
iValW(2) = 0
iValW(3) = 0
If Question = "LAB AND MEDICAL RESULTS" Then
    cmdLabResults.Visible = True
Else
    cmdLabResults.Visible = False
End If
frmTestEntry.MousePointer = 2
frmTestEntry.ZOrder 0
lblEye.Visible = False
For z = 1 To MaxButtonsAllowed
    TheButtons(z) = False
    TheButtonName(z) = ""
    TheButtonText(z) = ""
    TheButtonNest(z) = ""
    TheButtonDisplay(z) = ""
    TheButtonTrigger(z) = ""
    TheButtonTriggerText(z) = ""
    TheButtonVisible(z) = False
    TheButtonTextColor(z) = 0
    TheButtonColor(z) = 0
    TheButtonId(z) = 0
    TheChoice(z) = ""
    TheChoiceName(z) = ""
Next z
Erase ThePadChoice
For z = 1 To MaxPadsAllowed
    ThePads(z) = False
    ThePadName(z) = ""
    ThePadValue(z) = ""
    ThePadVisible(z) = False
    ThePadTrigger(z) = ""
    ThePadTriggerText(z) = ""
    ThePadTextColor(z) = 0
    ThePadColor(z) = 0
    ThePadId(z) = 0
    ThePadDisplay(z) = ""
    ThePadDecimal(z) = ""
    ThePadHelpText(z) = ""
Next z
For z = 1 To MaxBtnAllowed
    TheBtns(z) = False
    TheBtnName(z) = ""
    TheBtnText(z) = ""
    TheBtnNest(z) = ""
    TheBtnDisplay(z) = ""
    TheBtnTrigger(z) = ""
    TheBtnTriggerText(z) = ""
    TheBtnVisible(z) = False
    TheBtnTextColor(z) = 0
    TheBtnColor(z) = 0
    TheBtnId(z) = 0
Next z
z = InStrPS(StoreResults, ".")
If (z <> 0) Then
    ScratchTestFile = Left(StoreResults, z - 1) + ".tmp"
    PrevTestFile = Left(StoreResults, z - 1) + ".prv"
Else
    ScratchTestFile = StoreResults + ".tmp"
    PrevTestFile = StoreResults + ".prv"
End If
Call DynamicEntry(Question)
cmdRetain.Visible = GetPreviousTest(PatientId, Question, PrevTestFile)
'For Rule Engine Invocation
    Dim Rule As New DI_RuleAlert
    Call Rule.RuleAlert(PatientId, AppointmentId, 4, "EXAMELEMENTS", "EXAMELEMENTS", Question)
End Sub

Private Sub FormReLoad()
Dim i As Integer
CurrentPostingSet = CurrentPostingSet + 1
For i = 1 To MaxButtons
    Call InitializeButton(i)
Next i
For i = 1 To MaxPads
    Call InitializePad(i)
Next i
For i = 1 To MaxBtns
    Call InitializeBtn(i)
Next i
End Sub

Private Sub AssumeYouStartOver()
Dim FileNum As Integer
CurrentPostingSet = 0
If (MaxBtns > 0) And (PostedQuestion) Then
    If (TheBtns(1)) Or (TheBtns(2)) Or (TheBtns(3)) Or _
       (TheBtns(4)) Or (TheBtns(5)) Or (TheBtns(6)) Then
        Exit Sub
    End If
End If
If (Trim(ScratchTestFile) <> "") Then
    FileNum = FreeFile
    FM.OpenFile ScratchTestFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
    FM.CloseFile CLng(FileNum)
End If
End Sub

Private Function GetPreviousTest(PatId As Long, TestQuestion As String, TheFile As String) As Boolean
Dim FileNum As Integer
Dim w As Integer
Dim i As Integer, j As Integer
Dim ApptId As Long
Dim CurWaitValue As Integer, CurLifeValue As Integer, CurAnnualValue As Integer
Dim WaitValue As Integer, LifeValue As Integer, AnnualValue As Integer
Dim WaitOn As Boolean, LifeOn As Boolean, AnnualOn As Boolean
Dim BusLogicOn As Boolean
Dim NowDate As String, NewDate As String
Dim Rec As String, Temp As String
Dim BaseDate As String
Dim Srv As String, ACPT(10) As String
Dim ACptWait(10) As Integer
Dim ACptAnnual(10) As Integer, ACptLife(10)
Dim TimeRec As String, TheDate As String
Dim RetCls As DynamicClass
Dim RetCln As PatientClinical
Dim RetExm As DI_ExamClinical
Dim RetAppt As SchedulerAppointment
BaseDate = ""
NewDate = frmSystems1.ActiveActivityDate
NewDate = Mid(NewDate, 5, 2) + "/" + Mid(NewDate, 7, 2) + "/" + Left(NewDate, 4)
GetPreviousTest = False
txtResults.Visible = False
If (PatId > 0) And (Trim(TestQuestion) <> "") And (Trim(TheFile) <> "") Then
    BusLogicOn = CheckConfigCollection("BUSLOGON") = "T"
    TimeRec = ""
    Set RetCln = New PatientClinical
    RetCln.PatientId = PatId
    RetCln.ClinicalType = "F"
    RetCln.Symptom = "/" + Trim(TestQuestion)
    If (RetCln.FindPatientClinicalTests > 0) Then
        FileNum = FreeFile
        FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
        Print #FileNum, "QUESTION=" + "/" + TestQuestion
        i = 1
        ApptId = 0
        TheDate = ""
        Do Until Not (RetCln.SelectPatientClinical(i))
            If (ApptId < 1) Then
                ApptId = RetCln.AppointmentId
                Set RetAppt = New SchedulerAppointment
                RetAppt.AppointmentId = ApptId
                If (RetAppt.RetrieveSchedulerAppointment) Then
                    BaseDate = RetAppt.AppointmentDate
                    TheDate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
                End If
                Set RetAppt = Nothing
            End If
            If (ApptId = RetCln.AppointmentId) Then
                If (TimeRec = "") Then
                    Print #FileNum, Trim(RetCln.Findings)
                Else
                    If (TimeRec = RetCln.Findings) Then
                        Exit Do
                    End If
                    Print #FileNum, Trim(RetCln.Findings)
                End If
                If (TimeRec = "") Then
                    If (InStrPS(RetCln.Findings, "TIME") <> 0) Then
                        TimeRec = RetCln.Findings
                    End If
                End If
            Else
                Exit Do
            End If
            i = i + 1
        Loop
        FM.CloseFile CLng(FileNum)
        GetPreviousTest = True
    End If
    Set RetCln = Nothing
    If (FM.IsFileThere(TheFile)) Then
        Temp = ""
        txtResults.Text = ""
        Call frmTestResults.PushList(TheFile)
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
        While Not (EOF(FileNum))
            Line Input #FileNum, Rec
            If (Left(Rec, 6) = "Recap=") Then
                Temp = Temp + Trim(Mid(Rec, 7, Len(Rec) - 6))
            End If
        Wend
        Temp = Replace(Temp, "%", " ")
        Temp = Replace(Temp, "^", " ")
        Temp = Replace(Temp, "!", " ")
        Temp = Replace(Temp, "~", " ")
        txtResults.Text = Trim(Temp)
        If (TheDate <> "") Then
            txtResults.Text = TheDate + Chr(13) + Chr(10) + Trim(Temp)
            txtResults.Visible = True
        End If
        FM.CloseFile CLng(FileNum)

' check business logic for test factors (life-annual max, wait period)
        If (BusLogicOn) And (txtResults.Visible) Then
            w = 0
            LifeOn = False
            AnnualOn = False
            WaitOn = False
            WaitValue = 0
            LifeValue = 0
            AnnualValue = 0
            CurWaitValue = 0
            CurLifeValue = 0
            CurAnnualValue = 0
            Set RetCls = New DynamicClass
            RetCls.Question = TestQuestion
            If (RetCls.FindClassFormsbyName > 0) Then
                If (RetCls.SelectClassForm(1)) Then
                    j = 1
                    i = InStrPS(RetCls.ControlName, ",")
                    While (i > 0)
                        w = w + 1
                        ACPT(w) = Trim(Mid(RetCls.ControlName, j, (i - 1) - (j - 1)))
                        If (frmSystems1.GetTestFactors(ACPT(w), WaitValue, AnnualValue, LifeValue)) Then
                            ACptWait(w) = WaitValue
                            ACptAnnual(w) = AnnualValue
                            ACptLife(w) = LifeValue
                        End If
                        j = i + 1
                        i = InStrPS(j, RetCls.ControlName, ",")
                    Wend
                    w = w + 1
                    ACPT(w) = Trim(Mid(RetCls.ControlName, j, (i - 1) - (j - 1)))
                    If (frmSystems1.GetTestFactors(ACPT(w), WaitValue, AnnualValue, LifeValue)) Then
                        ACptWait(w) = WaitValue
                        ACptAnnual(w) = AnnualValue
                        ACptLife(w) = LifeValue
                    End If
                End If
            End If
            Set RetCls = Nothing
' do wait period test
            For i = 1 To w
                If (Trim(ACPT(i)) = "") Then
                    Exit For
                End If
                Srv = ACPT(i)
                WaitValue = ACptWait(i)
                AnnualValue = ACptAnnual(i)
                LifeValue = ACptLife(i)
                If (WaitValue > 0) Then
                    Call AdjustDate(NewDate, -1 * WaitValue, NowDate)
                    NowDate = Mid(NowDate, 7, 4) + Mid(NowDate, 1, 2) + Mid(NowDate, 4, 2)
                    If (NowDate > TheDate) Then
                        WaitOn = True
                    End If
                    If (WaitOn) Then
                        txtResults.BackColor = &HC000&
                    End If
                End If
' do annual max test
                If (AnnualValue > 0) Then
                    Call AdjustDate(NewDate, -365, NowDate)
                    NowDate = Mid(NowDate, 7, 4) + Mid(NowDate, 1, 2) + Mid(NowDate, 4, 2)
                    Set RetExm = New DI_ExamClinical
                    CurAnnualValue = RetExm.RetrieveProcedureCount(PatientId, Srv, NowDate)
                    If (CurAnnualValue >= AnnualValue) Then
                        AnnualOn = True
                    End If
                    Set RetExm = Nothing
                    If (AnnualOn) Then
                        txtResults.BackColor = &HC0C0&
                    End If
                End If
' do life max test
                If (LifeValue > 0) Then
                    Set RetExm = New DI_ExamClinical
                    If (RetExm.RetrieveProcedureCount(PatientId, Srv, "") >= LifeValue) Then
                        LifeOn = True
                    End If
                    Set RetExm = Nothing
                    If (LifeOn) Then
                        txtResults.BackColor = &HC000C0
                    End If
                End If
                If (LifeValue > 0) Or (AnnualValue > 0) Then
                    lblAnnual.Caption = "Annual Performed/Allowed:" + str(CurAnnualValue) + "/" + str(AnnualValue) + " Life Allowed:" + str(LifeValue)
                    lblAnnual.Visible = True
                End If
            Next i
        End If
    End If
End If
End Function

Private Function ProcessAudio() As Boolean
Dim i As Integer
Dim Temp As String
ProcessAudio = False
Temp = ""
If (Trim(StoreResults) <> "") Then
    i = FM.InFileNameStartStr(StoreResults, "T")
    If (i > 0) Then
        Temp = Mid(StoreResults, i, Len(StoreResults) - i + 1)
    End If
    i = InStrPS(Temp, ".")
    If (i > 0) Then
        Temp = Left(Temp, i - 1)
    End If
    If (Temp <> "") Then
        frmAudio.WavFileName = DocumentDirectory + "Audio-" + Trim(Temp) + ".wav"
        frmAudio.Show 1
    End If
End If
End Function

Private Sub ConvertHeightToInche(Value As Double, Text As String)
     'ConvertHeightToInche = 0
    If UCase(Text) = UCase("feet") Then
         'ConvertHeightToInche = Value * 12
         iValH(0) = Value * 12
    ElseIf UCase(Text) = UCase("meters") Then
        'ConvertHeightToInche = Value * 39.3700787
        iValH(1) = Value * 39.3700787
    ElseIf UCase(Text) = UCase("cm") Then
        'ConvertHeightToInche = Value * 0.393700787
        iValH(2) = Value * 0.393700787
    Else
        'ConvertHeightToInche = Value
        iValH(3) = Value
    End If
End Sub

Private Sub ConvertWeightToLbs(Value As Double, Text As String)
     'ConvertWeightToLbs = 0
    If UCase(Text) = UCase("grams") Then
        'ConvertWeightToLbs = Value * 0.00220462262
        iValW(0) = Value * 0.00220462262
    ElseIf UCase(Text) = UCase("ounces") Then
        'ConvertWeightToLbs = Value * 0.0625
        iValW(1) = Value * 0.062
    ElseIf UCase(Text) = UCase("kgs") Then
        'ConvertWeightToLbs = Value * 2.20462262
        iValW(2) = Value * 2.20462262
    Else
        'ConvertWeightToLbs = Value
        iValW(3) = Value
    End If
End Sub
Private Sub RemoveHieghtValue(Value As Double, Text As String)
    If UCase(Text) = UCase("feet") Then
        ValueInInche = ValueInInche - iValH(0)
    ElseIf UCase(Text) = UCase("meters") Then
            ValueInInche = ValueInInche - iValH(1)
    ElseIf UCase(Text) = UCase("cm") Then
            ValueInInche = ValueInInche - iValH(2)
    Else
          ValueInInche = ValueInInche - iValH(3)
    End If
End Sub
Private Sub RemoveWeightValue(Value As Double, Text As String)
    If UCase(Text) = UCase("grams") Then
        ValueInLbs = ValueInLbs - iValW(0)
    ElseIf UCase(Text) = UCase("ounces") Then
            ValueInLbs = ValueInLbs - iValW(1)
    ElseIf UCase(Text) = UCase("kgs") Then
            ValueInLbs = ValueInLbs - iValW(2)
    Else
          ValueInLbs = ValueInLbs - iValW(3)
    End If
End Sub
Public Function FrmClose()
 Call cmdQuit_Click
 Unload Me
End Function

