VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmFollowUp 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9210
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12330
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "FollowUp.frx":0000
   ScaleHeight     =   9210
   ScaleWidth      =   12330
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstSta 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      ItemData        =   "FollowUp.frx":36C9
      Left            =   3240
      List            =   "FollowUp.frx":36CB
      TabIndex        =   14
      Top             =   2640
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.ListBox lstTests 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   4200
      Sorted          =   -1  'True
      TabIndex        =   8
      Top             =   360
      Width           =   2295
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   9960
      TabIndex        =   4
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   8280
      TabIndex        =   0
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstDocs 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   6060
      Left            =   240
      MultiSelect     =   1  'Simple
      TabIndex        =   1
      Top             =   1320
      Width           =   11295
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "FollowUp.frx":36CD
      Left            =   6600
      List            =   "FollowUp.frx":36CF
      TabIndex        =   6
      Top             =   360
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   9960
      TabIndex        =   10
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FollowUp.frx":36D1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   870
      Left            =   1560
      TabIndex        =   11
      Top             =   7440
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FollowUp.frx":38B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   855
      Left            =   240
      TabIndex        =   12
      Top             =   7440
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FollowUp.frx":3A8F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDisplay 
      Height          =   1110
      Left            =   3600
      TabIndex        =   15
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FollowUp.frx":3C6E
   End
   Begin VB.Label lblLoad 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loading Patient Information"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   240
      TabIndex        =   13
      Top             =   720
      Visible         =   0   'False
      Width           =   3555
   End
   Begin VB.Label lblTests 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Tests"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   4200
      TabIndex        =   9
      Top             =   120
      Width           =   480
   End
   Begin VB.Label lblStatus 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   6600
      TabIndex        =   7
      Top             =   120
      Width           =   585
   End
   Begin VB.Label lblLoc 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9960
      TabIndex        =   5
      Top             =   120
      Width           =   825
   End
   Begin VB.Label lblDoctor 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   8280
      TabIndex        =   3
      Top             =   120
      Width           =   615
   End
   Begin VB.Label lblTitle 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Clinical FollowUp"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   2160
   End
End
Attribute VB_Name = "frmFollowUp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public QuitOn As Boolean
Private PatientId As Long
Private ResourceId As Long
Private ResourceLocId As Long
Private TheTest As String
Private TheStatus As String
Private PageCnt As Long
Private TotalPageCnt As Long
Private CurrentIndex As Long
Private MidCurrentIndex As Long
Private PrevCurrentIndex As Long
Private TotalFound As Long

Private TriggeredClnId As Long
Private ApplStartAt As Long
Private ApplReturnCount As Long
Private ApplLocId As Long
Private ApplApptId As Long
Private ApplPatientId As Long
Private ApplClinicalId As Long
Private ApplDate As String
Private ApplHPhone As String
Private ApplSymptom As String
Private ApplFinding As String
Private ApplDetail1 As String
Private ApplDetail2 As String
Private ApplClnType As String
Private ApplResource As String
Private ApplFollowUp As String
Private ApplLastName As String
Private ApplApptDate As String
Private ApplFirstName As String
Private ApplStartDate As String
Private ApplClnStatus As String
Private ApplMiddleName As String
Private ApplConfirmStatus As String
Private ApplTransactionRef As String

Private ApplListTotal As Long
Private ApplListTbl As ADODB.Recordset
Private Const MaxCount As Long = 25
Public GetPatid As Long
Dim pat As New Patient
Public patientRuns As Long
Public PatId As Long

Private Sub cmdDisplay_Click()
lblLoad.Caption = "Load Matching Results"
lblLoad.Visible = True
DoEvents
CurrentIndex = 1
Call LoadClinicalList(False)
lblLoad.Caption = "Load Patient Information"
lblLoad.Visible = False
DoEvents
End Sub

Private Sub cmdDone_Click()
Unload frmFollowUp
QuitOn = False
End Sub

Private Sub cmdNext_Click()
    On Error GoTo lcmdNext_Click_Error

PrevCurrentIndex = MidCurrentIndex
MidCurrentIndex = CurrentIndex
If (PrevCurrentIndex >= TotalFound) Then
    PrevCurrentIndex = 1
    MidCurrentIndex = 1
    CurrentIndex = 1
End If
Call LoadClinicalList(False)

    Exit Sub

lcmdNext_Click_Error:

    LogError "frmFollowUp", "cmdNext_Click", Err, Err.Description
End Sub

Private Sub cmdPrev_Click()
Dim OrgPrev As Long
    On Error GoTo lcmdPrev_Click_Error

CurrentIndex = PrevCurrentIndex
MidCurrentIndex = CurrentIndex / 2
PrevCurrentIndex = OrgPrev
PrevCurrentIndex = PrevCurrentIndex - MaxCount
If (PrevCurrentIndex < 1) Then
    PrevCurrentIndex = 1
End If
Call LoadClinicalList(False)

    Exit Sub

lcmdPrev_Click_Error:

    LogError "frmFollowUp", "cmdPrev_Click", Err, Err.Description
End Sub

Public Function LoadClinicalList(InitOn As Boolean) As Boolean
Dim oDate As New CIODateTime
    On Error GoTo lLoadClinicalList_Error

LoadClinicalList = True
PageCnt = 0
oDate.MyDateTime = Now
cmdNext.Visible = False
cmdPrev.Visible = False
If (InitOn) Then
    TheStatus = ""
    TheTest = ""
    Call ApplLoadStaff(lstDr)
    ResourceId = UserLogin.iId
    Call ApplLoadLocation(lstLoc)
    ResourceLocId = dbPracticeId
    Call ApplGetCodesbyList("CLINICALFOLLOWUPSTATUS", False, True, lstStatus)
    Call ApplGetCodesbyTest(lstTests)
    TriggeredClnId = 0
    CurrentIndex = 1
    PrevCurrentIndex = 1
    MidCurrentIndex = 1
    Exit Function
End If
lstDocs.Clear
ApplLocId = 0
ApplApptId = 0
ApplPatientId = 0
ApplClinicalId = 0
ApplSymptom = ""
ApplFinding = ""
ApplApptDate = ""
ApplConfirmStatus = ""
ApplDate = oDate.GetDisplayDate(False)
ApplConfirmStatus = TheStatus
ApplTransactionRef = TheTest
ApplStartAt = CurrentIndex
MidCurrentIndex = CurrentIndex
ApplReturnCount = 25
TotalFound = ApplRetrieveReviewClinicalList
TotalPageCnt = Int(TotalFound / MaxCount)
CurrentIndex = ApplReturnCount
If (TotalFound > 25) Then
    cmdPrev.Visible = True
    cmdNext.Visible = True
Else
    cmdPrev.Visible = False
    cmdNext.Visible = False
End If
lstDocs.ListIndex = -1
Set oDate = Nothing

    Exit Function

lLoadClinicalList_Error:

    LogError "frmFollowUp", "LoadClinicalList", Err, Err.Description
End Function

Private Sub lstDocs_Click()
Dim u As Long, MyCheck As Boolean
Dim AptId As Long, ActId As Long
Dim ClnId As Long

    On Error GoTo llstDocs_Click_Error

If (lstDocs.ListIndex >= 0) Then
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Interpret"
    frmEventMsgs.RejectText = "Go To Edit Previous"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Set Status"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
     If Not UserLogin.HasPermission(epEditPreviousVisit) Then
            frmEventMsgs.Header = "Not Permissioned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
            End If
            
            patientRuns = pat.GetPatientAccess(ApplPatientId)
            If patientRuns = -1 Then
            frmEventMsgs.Header = "Access Blocked"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
            End If
            
        u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "*")
        If (u > 0) Then
            ClnId = Val(Mid(lstDocs.List(lstDocs.ListIndex), u + 1, Len(lstDocs.List(lstDocs.ListIndex)) - u))
            If (ClnId > 0) Then
                AptId = Val(Mid(lstDocs.List(lstDocs.ListIndex), 96, (u - 1) - 95))
                If (AptId > 0) Then
                    ActId = ApplGetActivityId(AptId, MyCheck)
                    ' Give error message if this fails. Currently failing on existing scratch file.
                    If (ActId > 0) Then
                        If (ApplFollowUpTrigger(ClnId, ActId)) Then
                            TriggeredClnId = ClnId
                            CurrentIndex = MidCurrentIndex
                            Call LoadClinicalList(False)
                            If patientRuns = 1 Then
Call pat.GetPatientUpdate(PatId, pat.runscmplted)
End If
                        End If
                    End If
                End If
            End If
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
     If Not UserLogin.HasPermission(epEditPreviousVisit) Then
            frmEventMsgs.Header = "Not Permissioned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
            End If
             patientRuns = pat.GetPatientAccess(ApplPatientId) 'GetPatid)
                If patientRuns = -1 Then
                 frmEventMsgs.Header = " Access Blocked"
                 frmEventMsgs.AcceptText = ""
                 frmEventMsgs.RejectText = "Ok"
                 frmEventMsgs.CancelText = ""
                 frmEventMsgs.Other0Text = ""
                 frmEventMsgs.Other1Text = ""
                 frmEventMsgs.Other2Text = ""
                 frmEventMsgs.Other3Text = ""
                 frmEventMsgs.Other4Text = ""
                 frmEventMsgs.Show 1
                 'Call TurnOn
                 Exit Sub
                End If
                     
        u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "/")
        If (u > 0) Then
            AptId = Val(Mid(lstDocs.List(lstDocs.ListIndex), 96, (u - 1) - 95))
            If (AptId > 0) Then
                ActId = ApplGetActivityId(AptId, MyCheck)
                If (ActId > 0) Then
                    Call SelectSystems(ActId)
                    CurrentIndex = MidCurrentIndex
                     Call pat.GetPatientUpdate(ApplPatientId, pat.runscmplted)
                    Call LoadClinicalList(False)
                Else
                    frmEventMsgs.Header = "Patient has an active appointment, close it before editing"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                End If
            End If
        End If
        lblLoad.Visible = False
    ElseIf (frmEventMsgs.Result = 3) Then
        u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "*")
        If (u > 0) Then
            ClnId = Val(Mid(lstDocs.List(lstDocs.ListIndex), u + 1, Len(lstDocs.List(lstDocs.ListIndex)) - u))
            If (ClnId > 0) Then
                TriggeredClnId = ClnId
                lstSta.Clear
                lstSta.AddItem "Close FollowUp Status"
                For u = 1 To lstStatus.ListCount - 1
                    lstSta.AddItem lstStatus.List(u)
                Next u
                lstSta.Visible = True
                lstDr.Enabled = False
                lstLoc.Enabled = False
                lstDocs.Enabled = False
                lstTests.Enabled = False
                cmdPrev.Enabled = False
                cmdNext.Enabled = False
            End If
        End If
    End If
End If

frmFollowUp.ZOrder 0

    Exit Sub

llstDocs_Click_Error:

    LogError "frmFollowUp", "lstDocs_Click", Err, Err.Description
End Sub

Private Sub lstDocs_DblClick()
If (lstDocs.ListIndex >= 0) Then
    Call lstDocs_Click
End If
End Sub

Private Sub lstLoc_Click()
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        ResourceLocId = -1
    Else
        ResourceLocId = ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
    End If
End If
End Sub

Private Sub lstDr_Click()
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        ResourceId = -1
    Else
        ResourceId = ApplGetListResourceId(lstDr.List(lstDr.ListIndex))
    End If
End If
End Sub

Private Sub lstStatus_Click()
If (lstStatus.ListIndex >= 0) Then
    If (lstStatus.ListIndex = 0) Then
        TheStatus = ""
    Else
        TheStatus = Left(lstStatus.List(lstStatus.ListIndex), 1)
    End If
End If
End Sub

Private Sub lstTests_Click()
If (lstTests.ListIndex >= 0) Then
    If (TheTest = Trim(lstTests.List(lstTests.ListIndex))) Or (lstTests.ListIndex = 0) Then
        TheTest = ""
    Else
        TheTest = Trim(lstTests.List(lstTests.ListIndex))
    End If
End If
End Sub

Private Sub lstSta_Click()
Dim Temp As String
    On Error GoTo llstSta_Click_Error

If (lstSta.ListIndex > 0) Then
    If (TriggeredClnId > 0) Then
        Temp = Left(lstSta.List(lstSta.ListIndex), 1)
        Call ApplSetFollowUpStatus(TriggeredClnId, Temp)
        TriggeredClnId = 0
        If (Temp <> TheStatus) Then
            CurrentIndex = 1
            Call LoadClinicalList(False)
        End If
    End If
    lstSta.Visible = False
    lstDr.Enabled = True
    lstLoc.Enabled = True
    lstDocs.Enabled = True
    lstTests.Enabled = True
    cmdPrev.Enabled = True
    cmdNext.Enabled = True
ElseIf (lstSta.ListIndex = 0) Then
    TriggeredClnId = 0
    lstSta.Visible = False
    lstDr.Enabled = True
    lstLoc.Enabled = True
    lstDocs.Enabled = True
    lstTests.Enabled = True
    cmdPrev.Enabled = True
    cmdNext.Enabled = True
End If

    Exit Sub

llstSta_Click_Error:

    LogError "frmFollowUp", "lstSta_Click", Err, Err.Description
End Sub

Public Function ApplSetFollowUpStatus(ClnId As Long, AStat As String) As Boolean
Dim i As Long
Dim RetCln As PatientClinical
Dim RetCln1 As PatientClinical
    On Error GoTo lApplSetFollowUpStatus_Error

ApplSetFollowUpStatus = False
If (ClnId > 0) And (Trim(AStat) <> "") Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        If (RetCln.ClinicalType = "F") Then
            Set RetCln1 = New PatientClinical
            RetCln1.ClinicalType = "F"
            RetCln1.AppointmentId = RetCln.AppointmentId
            RetCln1.PatientId = RetCln.PatientId
            RetCln1.Symptom = RetCln.Symptom
            RetCln1.Findings = ""
            RetCln1.ImageDescriptor = ""
            RetCln1.ImageInstructions = ""
            RetCln1.EyeContext = ""
            If (RetCln1.FindPatientClinical > 0) Then
                i = 1
                While (RetCln1.SelectPatientClinical(i))
                    RetCln1.ClinicalFollowup = AStat
                    Call RetCln1.ApplyPatientClinical
                    i = i + 1
                Wend
            End If
            Set RetCln1 = Nothing
        Else
            RetCln.ClinicalFollowup = AStat
            Call RetCln.ApplyPatientClinical
        End If
    End If
    Set RetCln = Nothing
End If

    Exit Function

lApplSetFollowUpStatus_Error:

    LogError "frmFollowUp", "ApplSetFollowUpStatus", Err, Err.Description
End Function

Public Function ApplGetListResourceId(TheItem As String) As Long
    On Error GoTo lApplGetListResourceId_Error

ApplGetListResourceId = 0
If (Len(Trim(TheItem)) > 75) Then
    ApplGetListResourceId = Val(Trim(Mid(TheItem, 76, Len(TheItem) - 75)))
ElseIf (Len(Trim(TheItem)) > 56) Then
    ApplGetListResourceId = Val(Trim(Mid(TheItem, 56, Len(TheItem) - 55)))
End If

    Exit Function

lApplGetListResourceId_Error:

    LogError "frmFollowUp", "ApplGetListResourceId", Err, Err.Description
End Function

Private Function ApplLoadLocation(ALstBox As ListBox) As Boolean
Dim i As Integer
Dim DisplayText As String
Dim RetPrc As PracticeName
Dim RetDr As SchedulerResource
    On Error GoTo lApplLoadLocation_Error

ApplLoadLocation = True
ALstBox.Clear
DisplayText = Space(56)
Mid(DisplayText, 1, 13) = "All Locations"
DisplayText = DisplayText + Trim(str(-1))
ALstBox.AddItem DisplayText
Set RetPrc = New PracticeName
RetPrc.PracticeType = "P"
RetPrc.PracticeName = Chr(1)
If (RetPrc.FindPractice > 0) Then
    i = 1
    While (RetPrc.SelectPractice(i))
        DisplayText = Space(56)
        Mid(DisplayText, 1, 6) = "Office"
        If (Trim(RetPrc.PracticeLocationReference) <> "") Then
            Mid(DisplayText, 7, Len(Trim(RetPrc.PracticeLocationReference)) + 1) = "-" + Trim(RetPrc.PracticeLocationReference)
            DisplayText = DisplayText + Trim(str(1000 + RetPrc.PracticeId))
        Else
            DisplayText = DisplayText + Trim(str(0))
        End If
        ALstBox.AddItem DisplayText
        i = i + 1
    Wend
End If
Set RetPrc = Nothing
Set RetDr = New SchedulerResource
RetDr.ResourceName = Chr(1)
If (RetDr.FindResourcebyRoom) Then
    i = 1
    While (RetDr.SelectResource(i))
        If (RetDr.ResourceServiceCode = "02") Then
            DisplayText = Space(75)
            Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
            DisplayText = DisplayText + Trim(str(RetDr.ResourceId))
            ALstBox.AddItem DisplayText
        End If
        i = i + 1
    Wend
End If
Set RetDr = Nothing

    Exit Function

lApplLoadLocation_Error:

    LogError "frmFollowUp", "ApplLoadLocation", Err, Err.Description
End Function

Private Function ApplLoadStaff(ALstBox As ListBox) As Boolean
Dim i As Integer
Dim DisplayText As String
Dim RetDr As SchedulerResource
    On Error GoTo lApplLoadStaff_Error

ApplLoadStaff = True
ALstBox.Clear
DisplayText = Space(75)
Mid(DisplayText, 1, 13) = "All Resources"
DisplayText = DisplayText + Trim(str(0))
ALstBox.AddItem DisplayText
Set RetDr = New SchedulerResource
RetDr.ResourceName = Chr(1)
If (RetDr.FindResourcebyDoctor) Then
    i = 1
    While (RetDr.SelectResource(i))
        DisplayText = Space(75)
        Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
        DisplayText = DisplayText + Trim(str(RetDr.ResourceId))
        ALstBox.AddItem DisplayText
        i = i + 1
    Wend
End If
Set RetDr = Nothing

    Exit Function

lApplLoadStaff_Error:

    LogError "frmFollowUp", "ApplLoadStaff", Err, Err.Description
End Function

Public Function ApplGetCodesbyList(CodeType As String, SetIt As Boolean, ViewAll As Boolean, ListBox As ListBox) As Boolean
Dim k As Integer
Dim DisplayText As String
Dim RetrieveCode As PracticeCodes
    On Error GoTo lApplGetCodesbyList_Error

Set RetrieveCode = New PracticeCodes
ListBox.Clear
If (ViewAll) Then
    ListBox.AddItem "All Status"
End If
RetrieveCode.ReferenceType = Trim(CodeType)
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        DisplayText = Space(40)
        If (SetIt) Then
            Mid(DisplayText, 1, Len(RetrieveCode.ReferenceCode) - 3) = Left(RetrieveCode.ReferenceCode, Len(RetrieveCode.ReferenceCode) - 3)
            Mid(DisplayText, 40, 1) = Mid(RetrieveCode.ReferenceCode, Len(RetrieveCode.ReferenceCode), 1)
        Else
            DisplayText = Trim(RetrieveCode.ReferenceCode)
        End If
        ListBox.AddItem DisplayText
        k = k + 1
    Loop
End If
Set RetrieveCode = Nothing

    Exit Function

lApplGetCodesbyList_Error:

    LogError "frmFollowUp", "ApplGetCodesbyList", Err, Err.Description
End Function

Public Function ApplGetCodesbyTest(ListBox As ListBox) As Boolean
Dim k As Integer
Dim DisplayText As String
Dim RetrieveCode As PracticeCodes
Dim RetCls As DynamicClass
    On Error GoTo lApplGetCodesbyTest_Error

Set RetrieveCode = New PracticeCodes
ListBox.Clear
ListBox.AddItem " All Tests"
RetrieveCode.ReferenceType = "External Tests"
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        DisplayText = Space(40)
        If (Trim(RetrieveCode.FollowUp) = "F") Then
            If (InStrPS(RetrieveCode.ReferenceCode, "-") = 0) Then
                DisplayText = Trim(RetrieveCode.ReferenceCode)
            Else
                DisplayText = Trim(Mid(RetrieveCode.ReferenceCode, 6, Len(RetrieveCode.ReferenceCode) - 5))
            End If
            ListBox.AddItem DisplayText
        End If
        k = k + 1
    Loop
End If
Set RetrieveCode = Nothing
Set RetCls = New DynamicClass
RetCls.QuestionParty = "T"
RetCls.Question = ""
If (RetCls.FindClassForms > 0) Then
    k = 1
    While (RetCls.SelectClassForm(k))
        If (Trim(RetCls.FollowUp) <> "") Then
            DisplayText = Trim(RetCls.Question)
            ListBox.AddItem DisplayText
        End If
        k = k + 1
    Wend
End If
Set RetCls = Nothing

    Exit Function

lApplGetCodesbyTest_Error:

    LogError "frmFollowUp", "ApplGetCodesbyTest", Err, Err.Description
End Function

Public Function ApplRetrieveReviewClinicalList() As Long
Dim z As Long
Dim PostIt As Boolean
Dim PatId As Long
Dim AptId As Long
Dim i As Long
Dim Pst As Long
Dim Temp As String, CurTest As String
Dim OtherDisp As String
Dim ITemp As String
Dim MyTest As String
Dim PatName As String, DisplayItem As String
    On Error GoTo lApplRetrieveReviewClinicalList_Error

ApplRetrieveReviewClinicalList = -1
MyTest = ApplTransactionRef
ApplStartDate = Mid(ApplDate, 7, 4) + Mid(ApplDate, 1, 2) + Mid(ApplDate, 4, 2)
ApplRetrieveReviewClinicalList = GetReviewClinicalList
If (ApplStartAt < 1) Or (ApplStartAt > ApplRetrieveReviewClinicalList) Then
    ApplStartAt = 1
End If
lstDocs.Clear
Pst = 0
AptId = 0
PatId = 0
CurTest = ""
OtherDisp = ""
i = ApplStartAt
While (ApplReturnCount > Pst)
    If (SelectList(i, "XX")) Then
        PostIt = True
        If (ApplClnType = "F") Then
            If (Trim(MyTest) <> "") Then
                PostIt = False
                If (Left(ApplSymptom, Len(MyTest) + 1) = "/" + Trim(MyTest)) Then
                    If (ApplPatientId <> PatId) Or (ApplApptId <> AptId) Then
                        PostIt = True
                    End If
                End If
            End If
            If (Trim(ApplSymptom) = CurTest) Then
                If (ApplPatientId = PatId) And (ApplApptId = AptId) Then
                    PostIt = False
                End If
            Else
                PatId = ApplPatientId
                AptId = ApplApptId
                CurTest = Trim(ApplSymptom)
                If CurTest = "/" + Trim(MyTest) And PostIt = False Then
                    PostIt = True
                End If
            End If
        Else
            If (Trim(MyTest) <> "") Then
                PostIt = False
                If (Left(ApplSymptom, Len(MyTest) + 1) = "/" + Trim(MyTest)) Then
                    If (ApplPatientId <> PatId) Or (ApplApptId <> AptId) Then
                        PostIt = True
                    End If
                End If
            End If
        End If
        If (Trim(ApplFollowUp) = "") Or ((Trim(TheStatus) = "") And (Trim(ApplFollowUp) = "C")) Then
            PostIt = False
        End If
        If (PostIt) Then
            DisplayItem = Space(95)
            Mid(DisplayItem, 1, 1) = ApplFollowUp
            Mid(DisplayItem, 3, 10) = Mid(ApplApptDate, 5, 2) + "/" + Mid(ApplApptDate, 7, 2) + "/" + Left(ApplApptDate, 4)
            PatName = Trim(ApplLastName) + " " + Trim(ApplMiddleName) + " " + Trim(ApplFirstName)
            Mid(DisplayItem, 14, Len(PatName)) = PatName
            If (ApplClnType = "F") Then
                Mid(DisplayItem, 40, 19) = Trim(Mid(ApplSymptom, 2, Len(ApplSymptom) - 1))
            Else
                z = InStrPS(ApplFinding, "/")
                If (z > 0) Then
                    ITemp = Mid(ApplFinding, z + 1, Len(ApplFinding) - z)
                Else
                    ITemp = ApplFinding
                End If
                Mid(DisplayItem, 40, 19) = Trim(ITemp)
            End If
            Mid(DisplayItem, 65, 12) = Left(ApplResource, 12)
            Call DisplayPhone(ApplHPhone, Temp)
            If (Len(Trim(ApplDetail2)) = 1) Then
                Mid(DisplayItem, 79, 1) = Left(ApplDetail2, 1)
            End If
            Mid(DisplayItem, 80, 14) = Temp
            DisplayItem = DisplayItem + Trim(str(ApplApptId)) + "/" + Trim(str(ApplPatientId)) + "*" + Trim(str(ApplClinicalId))
            If (Left(OtherDisp, 75) <> Left(DisplayItem, 75)) Then
                lstDocs.AddItem DisplayItem
                OtherDisp = DisplayItem
                Pst = Pst + 1
            End If
        End If
    Else
        Pst = ApplReturnCount + 1
    End If
    i = i + 1
Wend
ApplReturnCount = i

    Exit Function

lApplRetrieveReviewClinicalList_Error:

    LogError "frmFollowUp", "ApplRetrieveReviewClinicalList", Err, Err.Description
End Function

Private Function GetReviewClinicalList() As Long
    On Error GoTo lGetReviewClinicalList_Error

On Error GoTo DbErrorHandler
Dim PTemp(6) As String
ApplListTotal = 0
GetReviewClinicalList = -1
Set ApplListTbl = Nothing
Set ApplListTbl = CreateAdoRecordset
If (Trim(ApplStartDate) <> "") Then
    PTemp(1) = Trim(str(ApplStartDate))
End If
If (ResourceLocId >= 0) Then
    PTemp(2) = Trim(str(ResourceLocId))
End If
If (ResourceId > 0) Then
    PTemp(3) = Trim(str(ResourceId))
End If
If (Trim(ApplConfirmStatus) <> "") Then
    PTemp(4) = Trim(ApplConfirmStatus)
Else
    PTemp(4) = "^"
End If
MyPracticeRepositoryCmd.CommandType = adCmdStoredProc
MyPracticeRepositoryCmd.CommandText = "RetrieveClnTransactions"
Set MyParameters = Nothing
Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@SDate")
MyParameters.Type = adVarChar
MyParameters.Direction = adParamInput
MyParameters.Size = Len(Trim(PTemp(1)))
MyParameters.Value = Trim(PTemp(1))
MyPracticeRepositoryCmd.Parameters()(0) = MyParameters

Set MyParameters = Nothing
Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@LocId")
MyParameters.Type = adInteger
MyParameters.Direction = adParamInput
MyParameters.Size = 4
If (Trim(PTemp(2)) = "") Then
    PTemp(2) = "-1"
End If
MyParameters.Value = Int(Val(Trim(PTemp(2))))
MyPracticeRepositoryCmd.Parameters()(1) = MyParameters

Set MyParameters = Nothing
Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@ResId")
MyParameters.Type = adInteger
MyParameters.Direction = adParamInput
MyParameters.Size = 4
If (Trim(PTemp(3)) = "") Then
    PTemp(3) = "-1"
End If
MyParameters.Value = Int(Val(Trim(PTemp(3))))
MyPracticeRepositoryCmd.Parameters()(2) = MyParameters

Set MyParameters = Nothing
Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@StaId")
MyParameters.Type = adVarChar
MyParameters.Direction = adParamInput
MyParameters.Size = 1
If (Trim(PTemp(4)) = "") Then
    PTemp(4) = "^"
End If
MyParameters.Value = Trim(PTemp(4))
MyPracticeRepositoryCmd.Parameters()(3) = MyParameters
Call ApplListTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, adCmdStoredProc)
If Not (ApplListTbl.EOF) Then
    ApplListTbl.MoveLast
    ApplListTotal = ApplListTbl.RecordCount
    GetReviewClinicalList = ApplListTotal
End If
Exit Function
DbErrorHandler:
    Call PinpointError("GetReviewClinicalList", Err.Source + " " + Err.Description)

    Exit Function

lGetReviewClinicalList_Error:

    LogError "frmFollowUp", "GetReviewClinicalList", Err, Err.Description
End Function

Private Sub LoadApplList(IType As String)
    On Error GoTo lLoadApplList_Error

If (IType = "XX") Then
    If (ApplListTbl("AppointmentId") <> "") Then
        ApplApptId = ApplListTbl("AppointmentId")
    Else
        ApplApptId = 0
    End If
    If (ApplListTbl("PatientId") <> "") Then
        ApplPatientId = ApplListTbl("PatientId")
    Else
        ApplPatientId = 0
    End If
    If (ApplListTbl("ClinicalId") <> "") Then
        ApplClinicalId = ApplListTbl("ClinicalId")
    Else
        ApplClinicalId = 0
    End If
    If (ApplListTbl("LastName") <> "") Then
        ApplLastName = ApplListTbl("LastName")
    Else
        ApplLastName = ""
    End If
    If (ApplListTbl("FirstName") <> "") Then
        ApplFirstName = ApplListTbl("FirstName")
    Else
        ApplFirstName = ""
    End If
    If (ApplListTbl("MiddleInitial") <> "") Then
        ApplMiddleName = ApplListTbl("MiddleInitial")
    Else
        ApplMiddleName = ""
    End If
    If (ApplListTbl("HomePhone") <> "") Then
        ApplHPhone = ApplListTbl("HomePhone")
    Else
        ApplHPhone = ""
    End If
    If (ApplListTbl("Symptom") <> "") Then
        ApplSymptom = ApplListTbl("Symptom")
    Else
        ApplSymptom = ""
    End If
    If (ApplListTbl("FindingDetail") <> "") Then
        ApplFinding = ApplListTbl("FindingDetail")
    Else
        ApplFinding = ""
    End If
    If (ApplListTbl("ImageDescriptor") <> "") Then
        ApplDetail1 = ApplListTbl("ImageDescriptor")
    Else
        ApplDetail1 = ""
    End If
    If (ApplListTbl("ImageInstructions") <> "") Then
        ApplDetail2 = ApplListTbl("ImageInstructions")
    Else
        ApplDetail2 = ""
    End If
    If (ApplListTbl("ClinicalType") <> "") Then
        ApplClnType = ApplListTbl("ClinicalType")
    Else
        ApplClnType = ""
    End If
    If (ApplListTbl("FollowUp") <> "") Then
        ApplFollowUp = ApplListTbl("FollowUp")
    Else
        ApplFollowUp = ""
    End If
    If (ApplListTbl("Status") <> "") Then
        ApplClnStatus = ApplListTbl("Status")
    Else
        ApplClnStatus = ""
    End If
    If (ApplListTbl("AppDate") <> "") Then
        ApplApptDate = ApplListTbl("AppDate")
    Else
        ApplApptDate = ""
    End If
    If (ApplListTbl("ResourceId2") <> "") Then
        ApplLocId = ApplListTbl("ResourceId2")
    Else
        ApplLocId = 0
    End If
    If (ApplListTbl("ResourceName") <> "") Then
        ApplResource = ApplListTbl("ResourceName")
    Else
        ApplResource = ""
    End If
End If

    Exit Sub

lLoadApplList_Error:

    LogError "frmFollowUp", "LoadApplList", Err, Err.Description
End Sub

Private Function SelectList(ListItem As Long, TheType As String) As Boolean
On Error GoTo DbErrorHandler
SelectList = False
If (ApplListTbl.EOF) Or (ListItem < 1) Or (ListItem > ApplListTotal) Then
    Exit Function
End If
ApplListTbl.MoveFirst
ApplListTbl.Move ListItem - 1
Call LoadApplList(TheType)
SelectList = True
Exit Function
DbErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Sub SelectSystems(ActId As Long)
    On Error GoTo lSelectSystems_Error

If (ActId > 0) Then
    frmSystems1.TheStationId = 999
    If frmSystems1.PrintAll Then globalExam.ResetPatient
    globalExam.SetExamIds 0, 0, ActId
    frmSystems1.EditPreviousOn = True
    lblLoad = "Loading Clinical Records"
    lblLoad.Visible = True
    frmFollowUp.Enabled = False
    frmSystems1.PrintAll = False
    DoEvents
    If (frmSystems1.LoadSummaryInfo(True)) Then
        frmSystems1.cmdPrint = False
        frmSystems1.FormOn = True
        frmSystems1.Show 1
        frmFollowUp.ZOrder 0
    End If
    frmFollowUp.Enabled = True
    lblLoad.Visible = False
End If

    Exit Sub

lSelectSystems_Error:

    LogError "frmFollowUp", "SelectSystems", Err, Err.Description
End Sub

Private Function ApplGetActivityId(ApptId As Long, CheckedOut As Boolean) As Long
Dim PatId As Long
Dim DFile As String
Dim RetAct As PracticeActivity
    On Error GoTo lApplGetActivityId_Error

ApplGetActivityId = 0
CheckedOut = True
If (ApptId > 0) Then
    Set RetAct = New PracticeActivity
    RetAct.ActivityAppointmentId = ApptId
    RetAct.ActivityLocationId = -1
    RetAct.ActivityStatus = ""
    If (RetAct.FindActivity > 0) Then
        If (RetAct.SelectActivity(1)) Then
            PatId = RetAct.ActivityPatientId
            If (RetAct.ActivityStatus = "H") Or (RetAct.ActivityStatus = "D") Then
                DFile = Dir(DoctorInterfaceDirectory + "*" + "_" + Trim(str(ApptId)) + "_" + Trim(str(PatId)) + ".txt")
                If (DFile <> "") Then
                    ApplGetActivityId = 0
                Else
                    ApplGetActivityId = RetAct.ActivityId
                End If
                If (RetAct.ActivityStatus = "H") Then
                    CheckedOut = False
                End If
            End If
        End If
    End If
    Set RetAct = Nothing
End If

    Exit Function

lApplGetActivityId_Error:

    LogError "frmFollowUp", "ApplGetActivityId", Err, Err.Description
End Function

Private Function ApplFollowUpTrigger(ClnId As Long, ActId As Long) As Boolean
Dim u As Long
Dim ApptId As Long
Dim PatId As Long
Dim RetCln As PatientClinical
    On Error GoTo lApplFollowUpTrigger_Error

ApplFollowUpTrigger = True
If (ClnId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        PatId = RetCln.PatientId
        ApptId = RetCln.AppointmentId
        globalExam.SetExamIds PatId, ApptId, ActId
        If (RetCln.ClinicalType = "F") Then
            If (DoTest(Trim(RetCln.Symptom), RetCln.PatientId, ActId, RetCln.AppointmentId)) Then
                Call ApplSetFollowUpStatus(ClnId, "I")
' taken then right to EPV Plan
                If (ApptId > 0) And (PatId > 0) Then
                    frmImpression.EditPreviousOn = True
                    frmImpression.ActivityId = ActId
                    frmImpression.PatientId = PatId
                    frmImpression.AppointmentId = ApptId
                    If (frmImpression.LoadImpression(True)) Then
                        frmImpression.Show 1
                    End If
                End If
'                Call SelectSystems(ActId)
                lstSta.Clear
                lstSta.AddItem "Close FollowUp Status"
                For u = 1 To lstStatus.ListCount - 1
                    lstSta.AddItem lstStatus.List(u)
                Next u
                lstSta.Visible = True
                lstDr.Enabled = False
                lstLoc.Enabled = False
                lstDocs.Enabled = False
                lstTests.Enabled = False
                cmdPrev.Enabled = False
                cmdNext.Enabled = False
            Else
                ApplFollowUpTrigger = False
            End If
        ElseIf (RetCln.ClinicalType = "A") Then
            frmScan.Whom = ""
            frmScan.PatientId = RetCln.PatientId
            frmScan.AppointmentId = RetCln.AppointmentId
            If (frmScan.LoadScan) Then
                frmScan.Show 1
                lstSta.Clear
                lstSta.AddItem "Close FollowUp Status"
                For u = 1 To lstStatus.ListCount - 1
                    lstSta.AddItem lstStatus.List(u)
                Next u
                lstSta.Visible = True
                lstDr.Enabled = False
                lstLoc.Enabled = False
                lstDocs.Enabled = False
                lstTests.Enabled = False
                cmdPrev.Enabled = False
                cmdNext.Enabled = False
            End If
        End If
    End If
    Set RetCln = Nothing
End If
    globalExam.ResetPatient

    Exit Function

lApplFollowUpTrigger_Error:

    LogError "frmFollowUp", "ApplFollowUpTrigger", Err, Err.Description
End Function

Private Function DoTest(QuesId As String, PatId As Long, ActId As Long, AptId As Long) As Boolean
Dim Style As Boolean
Dim TheFile As String
Dim TestTime As String
Dim TestId As String
Dim AQues As String
Dim RetCls As DynamicClass
Dim RetCls1 As DynamicClass
    On Error GoTo lDoTest_Error

AQues = ""
DoTest = False
If (Trim(QuesId) <> "") And (PatId > 0) And (ActId > 0) And (AptId > 0) Then
    frmHighlights.DisplayBasis = True
    frmHighlights.HighlightLoadOn = True
    If (frmHighlights.LoadHighlights(PatId, True, True, True)) Then
        Call InsertLog("FrmFollow.frm", "Opened Patient Chart " & CStr(PatId) & "", LoginCatg.EncounterOpen, "", "", 0, globalExam.iAppointmentId)
        frmHighlights.Show 1
        If (frmHighlights.QuitOn) Then
            Exit Function
        End If
    End If
    Set RetCls = New DynamicClass
    RetCls.QuestionParty = "T"
    RetCls.Question = Mid(QuesId, 2, Len(QuesId) - 1)
    If (RetCls.RetrieveClassbyQuestion) Then
        If (Trim(RetCls.FollowUp) <> "") Then
            Set RetCls1 = New DynamicClass
            RetCls1.QuestionParty = "T"
            RetCls1.Question = Chr(1)
            RetCls1.QuestionOrder = RetCls.FollowUp
            If (RetCls1.FindQuestionForms > 0) Then
                If (RetCls1.SelectClassForm(1)) Then
                    AQues = Trim(RetCls1.Question)
                    TestId = Trim(RetCls1.QuestionOrder)
                    Style = False
                    If (RetCls1.QuestionIndicator = "T") Then
                        Style = True
                    End If
                End If
            End If
            Set RetCls1 = Nothing
        End If
    End If
    Set RetCls = Nothing
    If (Trim(AQues) <> "") And (Trim(TestId) <> "") Then
        TheFile = GetTestFileName(TestId)
        If Not (FM.IsFileThere(TheFile)) Then
            If (Style) Then
                FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(1)
                Print #1, "QUESTION=\" + UCase(Trim(AQues))
                Call FormatTimeNow(TestTime)
                Print #1, "TIME=" + TestTime
                FM.CloseFile CLng(1)
            End If
        End If
        frmHome.FollowUpOn = True
        If (Style) Then
            frmEditTests.BackDoorOn = True
            frmEditTests.ManifestResults = ""
            frmEditTests.ManifestResultsOn = False
            frmEditTests.StoreResults = TheFile
            frmEditTests.Question = UCase(AQues)
            frmEditTests.PatientId = PatId
            frmEditTests.AppointmentId = AptId
            frmEditTests.FormOn = True
            frmFollowUp.Enabled = False
            frmEditTests.Show 1
            frmEditTests.BackDoorOn = False
            frmFollowUp.Enabled = True
            frmFollowUp.ZOrder 0
            If (Not (frmEditTests.QuitOn)) And (FM.IsFileThere(TheFile)) Then
                DoTest = PostTestResults(TestId, PatId, AptId)
            End If
        Else
            frmTestEntry.ActivityId = ActId
            frmTestEntry.PatientId = PatId
            frmTestEntry.AppointmentId = AptId
            frmTestEntry.ChangeOn = False
            frmTestEntry.StoreResults = TheFile
            frmTestEntry.Question = UCase(Trim(AQues))
            frmTestEntry.FormOn = True
            frmFollowUp.Enabled = False
            frmTestEntry.Show 1
            frmFollowUp.Enabled = True
            frmFollowUp.ZOrder 0
            If (FM.IsFileThere(TheFile)) Then
                DoTest = PostTestResults(TestId, PatId, AptId)
            End If
        End If
        frmHome.FollowUpOn = False
        frmFollowUp.Enabled = True
        frmFollowUp.ZOrder 0
    End If
End If

    Exit Function

lDoTest_Error:

    LogError "frmFollowUp", "DoTest", Err, Err.Description
End Function

Private Function PostTestResults(TestId As String, PatId As Long, AptId As Long) As Boolean
Dim q As Integer
Dim Rec As String
Dim MyFile As String
Dim Symptom As String
Dim Finding As String
Dim ImageText As String
Dim AQuestion As String
Dim iFileNum As Long
Dim RetCln As PatientClinical
    On Error GoTo lPostTestResults_Error

    If (Trim(TestId) <> "") And (PatId > 0) And (AptId > 0) Then
        MyFile = GetTestFileName(TestId)
        If (MyFile <> "") Then
            Set RetCln = New PatientClinical
            AQuestion = ""
            iFileNum = FreeFile
            FM.OpenFile MyFile, FileOpenMode.InputFileOpenMode, FileAccess.Read, CLng(iFileNum)
            While (Not (EOF(iFileNum)))
                Line Input #iFileNum, Rec
                If (Left(Rec, 6) <> "Recap=") Then
                    If (Left(Rec, 9) = "QUESTION=") Then
                        AQuestion = Mid(Rec, 10, Len(Rec) - 9)
                    ElseIf (Trim(AQuestion) <> "") Then
                        ImageText = ""
                        If (Left(Rec, 5) = "TIME=") Then
                            q = Len(Rec) + 1
                        Else
                            q = InStrPS(Rec, "<")
                            If (q <> 0) Then
                                q = Len(Rec) + 1
                            Else
                                ImageText = Mid(Rec, q, Len(Rec) - (q - 1))
                            End If
                        End If
                        Symptom = AQuestion
                        Finding = Left(Rec, q - 1)
                        Set RetCln = New PatientClinical
                        RetCln.ClinicalId = 0
                        If (RetCln.RetrievePatientClinical) Then
                            RetCln.AppointmentId = AptId
                            RetCln.PatientId = PatId
                            RetCln.ClinicalType = "F"
                            RetCln.EyeContext = ""
                            RetCln.Symptom = Symptom
                            RetCln.Findings = Finding
                            RetCln.ImageInstructions = ImageText
                            RetCln.ImageDescriptor = ""
                            RetCln.ClinicalHighlights = "T"
                            RetCln.ClinicalFollowup = ""
                            If Not (RetCln.ApplyPatientClinical) Then
                                Call PinpointError("DI-FollowUpS", "Test " + Trim(Symptom) + " Not Posted")
                            End If
                        End If
                        Set RetCln = Nothing
                    End If
                End If
            Wend
            FM.CloseFile CLng(iFileNum)
            KillFile MyFile
            PostTestResults = True
        End If
    End If

    Exit Function

lPostTestResults_Error:

    LogError "frmFollowUp", "PostTestResults", Err, Err.Description, "File = " & MyFile
End Function

