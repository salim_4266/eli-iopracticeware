VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmImpression 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "frmImpression"
   ClientHeight    =   9195
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12375
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "Impression.frx":0000
   ScaleHeight     =   9195
   ScaleWidth      =   12375
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstZoom 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5280
      ItemData        =   "Impression.frx":36C9
      Left            =   240
      List            =   "Impression.frx":36CB
      TabIndex        =   17
      Top             =   2040
      Visible         =   0   'False
      Width           =   11655
   End
   Begin VB.ListBox lstItems 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5310
      ItemData        =   "Impression.frx":36CD
      Left            =   1920
      List            =   "Impression.frx":36CF
      Sorted          =   -1  'True
      TabIndex        =   8
      Top             =   1920
      Visible         =   0   'False
      Width           =   8535
   End
   Begin VB.ListBox lstFindingsNow 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1470
      ItemData        =   "Impression.frx":36D1
      Left            =   120
      List            =   "Impression.frx":36D3
      TabIndex        =   15
      Top             =   6240
      Width           =   4455
   End
   Begin VB.ListBox lstBox 
      Height          =   840
      Left            =   7920
      Sorted          =   -1  'True
      TabIndex        =   10
      Top             =   120
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.ListBox lstImp 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3630
      ItemData        =   "Impression.frx":36D5
      Left            =   120
      List            =   "Impression.frx":36D7
      TabIndex        =   5
      Top             =   720
      Width           =   4455
   End
   Begin VB.ListBox lstActions 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   7005
      ItemData        =   "Impression.frx":36D9
      Left            =   4680
      List            =   "Impression.frx":36DB
      TabIndex        =   3
      Top             =   720
      Width           =   7215
   End
   Begin VB.ListBox lstFinding 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1470
      ItemData        =   "Impression.frx":36DD
      Left            =   120
      List            =   "Impression.frx":36DF
      TabIndex        =   1
      Top             =   4560
      Width           =   4455
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10320
      TabIndex        =   2
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Impression.frx":36E1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   975
      Left            =   2040
      TabIndex        =   9
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Impression.frx":3914
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdINotes 
      Height          =   975
      Left            =   6120
      TabIndex        =   12
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Impression.frx":3B4B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPNotes 
      Height          =   975
      Left            =   8160
      TabIndex        =   13
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Impression.frx":3D8A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   14
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Impression.frx":3FC3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHxRx 
      Height          =   975
      Left            =   4080
      TabIndex        =   18
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Impression.frx":41F6
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Current Findings"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   16
      Top             =   6000
      Width           =   1575
   End
   Begin VB.Label lblPrint 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Printing RX"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9360
      TabIndex        =   11
      Top             =   120
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Impressions"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   1125
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Plan"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   4680
      TabIndex        =   6
      Top             =   480
      Width           =   435
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Findings"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   4
      Top             =   4320
      Width           =   810
   End
   Begin VB.Label lblImp 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Impression and Plan"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2340
   End
End
Attribute VB_Name = "frmImpression"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public EditPreviousOn As Boolean
Public PatientId As Long
Public AppointmentId As Long
Public ActivityId As Long
Private Trigger As String
Private CurrentIndex As Integer

Private Const InActiveRef As String = "$"
Private Const QntRef As String = "["
Private Const QntRefEnd As String = "]"
Private Const CntRef As String = "-NF"
Private Const CntRefSet As String = "!"
Private Const RORef As String = "-RO"
Private Const RORefSet As String = "#"
Private Const HORef As String = "-HO"
Private Const HORefSet As String = "%"

Public Function LoadImpression(Init As Boolean) As Boolean
Dim ListType As Integer
Dim i As Long
Dim e As Integer, t As Integer
Dim m As Integer, q As Integer, qz As Integer
Dim p As Integer, z As Integer, h As Integer
Dim w As Integer, j As Integer, g As Integer
Dim ContOn As Boolean
Dim DispenseCLOn As Boolean
Dim DispensePCOn As Boolean
Dim ApptId As Long, NoteId As Long
Dim ATemp As String
Dim BTemp As String
Dim DTemp As String
Dim TheEye As String
Dim sOrderSeries As String
Dim PTemp As String, Temp As String
Dim TheText As String, LDiag As String
Dim QTemp As String
Dim TheSystem As String
Dim DisplayText As String
Dim OtherText As String, Quant As String
Dim RetDr As SchedulerResource
Dim RetAppt As SchedulerAppointment
Dim RetNotes As PatientNotes
Dim ClinicalRecords As DI_ExamClinical
Dim RetPC As PCInventory
Dim RetCL As CLInventory
Dim oLocalDate As New CIODateTime
Trigger = ""
Dim comm() As String
Dim sCDrugRx As CDrugRx

If (AppointmentId > 0) And (PatientId > 0) Then
    If (Init) Then
        CurrentIndex = 1
    End If
    lstFinding.Clear
    lstFindingsNow.Clear
    lstImp.Clear
    lstActions.Clear
    cmdPNotes.Visible = False
    cmdINotes.Visible = False
    If (EditPreviousOn) Then
        lstImp.AddItem "Add Impressions"
        lstActions.AddItem "Add Actions"
        cmdPNotes.Visible = True
        cmdINotes.Visible = True
    End If
    Set RetDr = New SchedulerResource
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = AppointmentId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ApptId = RetAppt.AppointmentId
        oLocalDate.SetDateFromIO RetAppt.AppointmentDate
        lblImp.Caption = "Impression and Plan: " & oLocalDate.GetDisplayDate(False)
        RetDr.ResourceId = RetAppt.AppointmentResourceId1
        If (RetDr.RetrieveSchedulerResource) Then
            lblImp.Caption = lblImp.Caption + " " + RetDr.ResourceDescription
        End If

' Findings
        Set ClinicalRecords = New DI_ExamClinical
        If (ClinicalRecords.LoadPreviousClinicalImprPlan(ApptId)) Then
            If (ClinicalRecords.LocateStartingRecord("Q")) Then
                i = 1
                Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
                    If (ClinicalRecords.ClinicalType = "Q") Then
                        DisplayText = Space(75)
                        LDiag = Trim(ClinicalRecords.ClinicalFindings)
                        TheSystem = ""
                        TheText = ""
                        If (GetDiagnosis(TheSystem, LDiag, TheText, 0)) Then
                            OtherText = ""
                            If (InStrPS(ClinicalRecords.ClinicalSymptom, CntRefSet) <> 0) Then
                                OtherText = "NF-"
                            End If
                            If (InStrPS(ClinicalRecords.ClinicalSymptom, HORefSet) <> 0) Then
                                OtherText = OtherText + "HO-"
                            End If
                            If (InStrPS(ClinicalRecords.ClinicalSymptom, RORefSet) <> 0) Then
                                OtherText = OtherText + "RO-"
                            End If
                            Quant = ""
                            w = InStrPS(ClinicalRecords.ClinicalSymptom, "[")
                            If (w > 0) Then
                                g = InStrPS(w, ClinicalRecords.ClinicalSymptom, "]")
                                If (g > 0) Then
                                    Quant = Mid(ClinicalRecords.ClinicalSymptom, w, g - (w - 1))
                                End If
                            End If
                            OtherText = OtherText + Trim(Quant)
                            Mid(DisplayText, 1, Len(Trim(OtherText)) + Len(Trim(TheText)) + 4) = Trim(ClinicalRecords.ClinicalEyeContext) + ": " + Trim(OtherText) + Trim(TheText)
                            Mid(DisplayText, 67, Len(LDiag)) = LDiag
                            DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                            lstFinding.AddItem DisplayText
                        End If
                    Else
                        Exit Do
                    End If
                    i = i + 1
                Loop
            End If
        End If
        If Not (EditPreviousOn) Then
            Call AddedDiagnosis
            Label2.Visible = True
            lstFindingsNow.Visible = True
        Else
            Label2.Visible = False
            lstFindingsNow.Visible = False
        End If

' Impressions
        If (ClinicalRecords.LocateStartingRecord("U")) Then
            i = 1
            Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
                If (ClinicalRecords.ClinicalType = "U") Then
                    Temp = ""
                    DisplayText = Space(75)
                    LDiag = Trim(ClinicalRecords.ClinicalFindings)
                    TheSystem = ""
                    TheText = ""
                    OtherText = ""
                    If (InStrPS(ClinicalRecords.ClinicalSymptom, CntRefSet) <> 0) Then
                        OtherText = "NF-"
                    End If
                    If (InStrPS(ClinicalRecords.ClinicalSymptom, HORefSet) <> 0) Then
                        OtherText = OtherText + "HO-"
                    End If
                    If (InStrPS(ClinicalRecords.ClinicalSymptom, RORefSet) <> 0) Then
                        OtherText = OtherText + "RO-"
                    End If
                    Quant = ""
                    w = InStrPS(ClinicalRecords.ClinicalSymptom, "[")
                    If (w > 0) Then
                        g = InStrPS(w, ClinicalRecords.ClinicalSymptom, "]")
                        If (g > 0) Then
                            Quant = Mid(ClinicalRecords.ClinicalSymptom, w, g - (w - 1))
                        End If
                    End If
                    OtherText = OtherText + Trim(Quant)
                    If (GetDiagnosis(TheSystem, LDiag, TheText, 0)) Then
                        Temp = OtherText + TheText
                        If (Trim(ClinicalRecords.ClinicalEyeContext) = "OD") Then
                            Temp = "OD " + Temp
                        ElseIf (Trim(ClinicalRecords.ClinicalEyeContext) = "OS") Then
                            Temp = "OS " + Temp
                        ElseIf (Trim(ClinicalRecords.ClinicalEyeContext) = "OU") Then
                            Temp = "OU " + Temp
                        End If
                        If (Trim(ClinicalRecords.ClinicalInstructions) <> "") Then
                            Temp = Temp + " OS:" + Trim(ClinicalRecords.ClinicalInstructions)
                        End If
                        If (Trim(ClinicalRecords.ClinicalDescriptor) <> "") Then
                            Temp = Temp + " OD:" + Trim(ClinicalRecords.ClinicalDescriptor)
                        End If
                        Mid(DisplayText, 1, Len(Temp)) = Temp
                        DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                        lstImp.AddItem DisplayText
                    End If
                Else
                    Exit Do
                End If
                i = i + 1
            Loop
        End If

' Actions
        If (ClinicalRecords.LocateStartingRecord("A")) Then
            i = 1
            Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
                If (ClinicalRecords.ClinicalType <> "A") Then
                    Exit Do
                End If
                If (ClinicalRecords.ClinicalStatus = "A") Then
                    DispenseCLOn = (InStrPS(UCase(ClinicalRecords.ClinicalFindings), "DISPENSE CL") + _
                                   InStrPS(UCase(ClinicalRecords.ClinicalFindings), "ORDER TRIAL CL") <> 0)
                    DispensePCOn = InStrPS(UCase(ClinicalRecords.ClinicalFindings), "DISPENSE SPEC") <> 0
                    ContOn = True
                    If (InStrPS(UCase(ClinicalRecords.ClinicalFindings), "RX") <> 0) And (Not (DispensePCOn) And Not (DispenseCLOn)) Then
                        If (InStrPS(ClinicalRecords.ClinicalFindings, "P-7") < 1) Then
                            ContOn = False
                        End If
                    End If
                    ATemp = Trim(ClinicalRecords.ClinicalFindings)
                    If (IsNumeric(Left(ATemp, 2))) Then
                        ATemp = Trim(Mid(ATemp, 5, Len(ATemp) - 4))
                    End If
                    If (ATemp <> "") And (ContOn) And (Left(ATemp, 1) <> "-") Then
                        g = 0
                        DisplayText = Space(200)
                        p = InStrPS(ATemp, "/")
                        If (p < 4) Then
                            If (Trim(ATemp) <> "") Then
                                p = 0
                            Else
                                p = -1
                            End If
                            Temp = Trim(ATemp)
                        Else
                            Temp = Left(ATemp, p - 3)
                        End If
                        If (p = 0) Then
                            DisplayText = Space(200)
                            Temp = ATemp
                            If (Val(Trim(Temp)) > 0) And (Len(Trim(Temp)) < 7) Then
                                If (DispenseCLOn) Then
                                    GoSub GetOrders
                                    Temp = Space(5) + sOrderSeries
                                ElseIf (DispensePCOn) Then
                                    GoSub GetOrders
                                    Temp = Space(5) + sOrderSeries
                                End If
                            End If
                            Mid(DisplayText, 1, Len(Temp)) = Temp
                            DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                            lstActions.AddItem DisplayText
                        ElseIf (p > 0) Then
                            Temp = Left(ATemp, p - 3)
                            If (Val(Trim(Temp)) <> 0) And (Len(Trim(Temp)) < 7) Then
                                If (DispenseCLOn) Then
                                    GoSub GetOrders
                                    Temp = Space(5) + sOrderSeries
                                ElseIf (DispensePCOn) Then
                                    GoSub GetOrders
                                    Temp = Space(5) + sOrderSeries
                                End If
                            End If
                            Mid(DisplayText, 1, Len(Temp)) = Temp
                            DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                            lstActions.AddItem DisplayText
                            q = p
'=====================================================================================================
                            If Temp = "RX" Then
                                Set sCDrugRx = New CDrugRx
                                Call sCDrugRx.LoadRxFromDB(ATemp)
                                BTemp = sCDrugRx.GetDrugRxFormat("ImprPlan")
                                Set sCDrugRx = Nothing
                                Do
                                    e = InStrPS(1, BTemp, vbCrLf)
                                    If e = 0 Then Exit Do
                                    
                                    DisplayText = Space(200)
                                    Temp = Space(5) & Mid(BTemp, 1, e - 1)
                                    BTemp = Mid(BTemp, e + 2)
                                    Mid(DisplayText, 1, Len(Temp)) = Temp
                                    DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                                    lstActions.AddItem DisplayText
                                Loop
                                
                                e = InStrPS(1, ATemp, "-6/")
                                BTemp = Mid(ATemp, e + 3)
                                For t = 7 To 9
                                    Temp = "-" & t & "/"
                                    e = InStrPS(1, BTemp, Temp)
                                    If e > 0 Then
                                        DisplayText = Space(200)
                                        Temp = Space(5) & Mid(BTemp, 1, e - 1)
                                        BTemp = Mid(BTemp, e + 3)
                                        Mid(DisplayText, 1, Len(Temp)) = Temp
                                        DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                                        lstActions.AddItem DisplayText
                                    End If
                                Next
                            Else
                            '-----------------------------------------------------------------------------------
                                While (p <> 0)
                                    g = g + 1
                                    p = InStrPS(q + 1, ATemp, "/")
                                    If (p = 0) And (q < Len(ATemp)) Then
                                        p = Len(ATemp) + 3
                                    End If
                                    If (p > 0) Then
                                        'If ((p - 3) > q) Then
                                            DisplayText = Space(200)
                                            If (Mid(ATemp, p - 2, 1) = "-") Then
                                                Temp = Space(5) + Mid(ATemp, q + 1, (p - 3) - q)
                                            Else
                                                Temp = Space(5) + Mid(ATemp, q + 1, (p - 1) - q)
                                            End If
                                            If (Len(Temp) <= 60) Then
                                                If (IsNumeric(Trim(Temp))) And (Len(Trim(Temp)) < 7) Then
                                                    If (Mid(ATemp, p - 2, 2) = "-6") Or (Mid(ATemp, p - 2, 2) = "-7") Then
                                                        If (DispenseCLOn) Then
                                                            GoSub GetOrders
                                                            Temp = Space(5) + sOrderSeries
                                                        ElseIf (DispensePCOn) Then
                                                            GoSub GetOrders
                                                            Temp = Space(5) + sOrderSeries
                                                        End If
                                                    End If
                                                End If
                                                Mid(DisplayText, 1, Len(Temp)) = Temp
                                                DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                                                lstActions.AddItem DisplayText
                                            Else
                                                For h = 1 To Len(Temp) Step 60
                                                    DisplayText = Space(200)
                                                    Mid(DisplayText, 1, 60) = Mid(Temp, h, 60)
                                                    Mid(DisplayText, 199, 1) = "*"
                                                    DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                                                    lstActions.AddItem DisplayText
                                                Next h
                                            End If
                                        'Else
                                        '    If (g < 6) Then
                                        '        DisplayText = Space(200)
                                        '        DisplayText = DisplayText + Trim(Str(ClinicalRecords.ClinicalId))
                                        '        lstActions.AddItem DisplayText
                                        '    End If
                                        'End If
                                        q = p
                                    Else
                                        If (q < Len(Trim(Temp))) Then
                                            DisplayText = Space(200)
                                            Temp = Space(5) + Mid(ATemp, q + 1, Len(ATemp) - q)
                                            If (Len(Temp) <= 64) Then
                                                If (Val(Trim(Temp)) > 0) And (Len(Trim(Temp)) < 7) Then
                                                    If (DispenseCLOn) Then
                                                        GoSub GetOrders
                                                        Temp = Space(5) + sOrderSeries
                                                    ElseIf (DispensePCOn) Then
                                                        GoSub GetOrders
                                                        Temp = Space(5) + sOrderSeries
                                                    End If
                                                End If
                                                Mid(DisplayText, 1, Len(Temp)) = Temp
                                                DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                                                lstActions.AddItem DisplayText
                                            Else
                                                For h = 1 To Len(Temp) Step 60
                                                    DisplayText = Space(200)
                                                    Mid(DisplayText, 1, 60) = Mid(Temp, h, 60)
                                                    Mid(DisplayText, 199, 1) = "*"
                                                    DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                                                    lstActions.AddItem DisplayText
                                                Next h
                                            End If
                                        End If
                                    End If
                                Wend
                            '-----------------------------------------------------------------------------------
                            End If
'=====================================================================================================
                            If (Trim(ClinicalRecords.ClinicalDescriptor) <> "") Then
                                ReDim comm(2) As String
                                comm(0) = Trim(ClinicalRecords.ClinicalDescriptor)
                                Call frmSystems1.GetCommentsFromDB(comm(0), comm(1), comm(2))
                                Temp = Space(5) & Trim(comm(1) & " " & comm(2)) & Space(200)
                                DisplayText = Space(200)
                                Mid(DisplayText, 1, Len(Temp)) = Temp
                                For z = 1 To 180 Step 60
                                    If (Trim(Mid(DisplayText, z, 60)) <> "") And (z = 1) Then
                                        lstActions.AddItem Mid(DisplayText, z, 60) + Space(140) + Trim(str(ClinicalRecords.ClinicalId))
                                    Else
                                        lstActions.AddItem Space(5) + Mid(DisplayText, z, 60) + Space(135) + Trim(str(ClinicalRecords.ClinicalId))
                                    End If
                                Next z
                            End If
                        End If
                    End If
                End If
                i = i + 1
            Loop
        End If
        Set ClinicalRecords = Nothing

' Notes
        m = 1
        Set RetNotes = New PatientNotes
        RetNotes.NotesPatientId = PatientId
        RetNotes.NotesAppointmentId = ApptId
        RetNotes.NotesType = "C"
        RetNotes.NotesSystem = ""
        If (RetNotes.FindNotes > 0) Then
            While (RetNotes.SelectNotes(m))
                NoteId = RetNotes.NotesId
                Temp = ""
                If (Trim(RetNotes.NotesSystem) = "") Then
                    If (Trim(RetNotes.NotesText1) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                             + Trim(RetNotes.NotesText2) + " " _
                             + Trim(RetNotes.NotesText3) + " " _
                             + Trim(RetNotes.NotesText4)
                    End If
                    Call StripCharacters(Temp, Chr(13))
                    Call StripCharacters(Temp, Chr(10))
                    If (Trim(Temp) <> "") Then
                        ListType = 3
                        For j = 1 To Len(Temp) Step 47
                            QTemp = Mid(Temp, j, 46)
                            GoSub WrapIt
                            If (qz > 0) Then
                                j = j - (Len(QTemp) - (qz - 1))
                            End If
                        Next j
                    End If
                ElseIf (Len(Trim(RetNotes.NotesSystem)) > 1) And (Trim(RetNotes.NotesSystem) <> "10") And (Trim(RetNotes.NotesSystem) <> "IMPR") And (Left(Trim(RetNotes.NotesSystem), 5) <> "IMPR-") And (Left(RetNotes.NotesSystem, 1) <> "T") Then
                    TheEye = Trim(RetNotes.NotesEye)
                    If (Trim(RetNotes.NotesText1) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                             + Trim(RetNotes.NotesText2) + " " _
                             + Trim(RetNotes.NotesText3) + " " _
                             + Trim(RetNotes.NotesText4)
                    End If
                    Call StripCharacters(Temp, Chr(13))
                    Call StripCharacters(Temp, Chr(10))
                    If (Trim(Temp) <> "") Then
                        Temp = TheEye + ":" + Temp
                        ListType = 2
                        For j = 1 To Len(Temp) Step 47
                            QTemp = Mid(Temp, j, 46)
                            If (Mid(Temp, j + 46, 1) = " ") Then
                                QTemp = QTemp + " "
                            End If
                            GoSub WrapIt
                            If (qz > 0) Then
                                j = j - (Len(QTemp) - (qz - 1))
                            End If
                        Next j
                    End If
                ElseIf ((Len(Trim(RetNotes.NotesSystem)) > 1) And (Trim(RetNotes.NotesSystem) <> "10")) And ((Trim(RetNotes.NotesSystem) = "IMPR") Or (Left(Trim(RetNotes.NotesSystem), 5) = "IMPR-")) Then
                    If (Trim(RetNotes.NotesText1) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                             + Trim(RetNotes.NotesText2) + " " _
                             + Trim(RetNotes.NotesText3) + " " _
                             + Trim(RetNotes.NotesText4)
                    End If
                    Call StripCharacters(Temp, Chr(13))
                    Call StripCharacters(Temp, Chr(10))
                    If (Trim(Temp) <> "") Then
                        ListType = 1
                        For j = 1 To Len(Temp) Step 47
                            QTemp = Mid(Temp, j, 46)
                            GoSub WrapIt
                            If (qz > 0) Then
                                j = j - (Len(QTemp) - (qz - 1))
                            End If
                        Next j
                    End If
                ElseIf ((Len(Trim(RetNotes.NotesSystem)) > 1) And (Trim(RetNotes.NotesSystem) <> "10")) And (Trim(RetNotes.NotesSystem) <> "IMPR") And (Left(Trim(RetNotes.NotesSystem), 5) = "IMPR-") Then
                    If (Trim(RetNotes.NotesText1) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                             + Trim(RetNotes.NotesText2) + " " _
                             + Trim(RetNotes.NotesText3) + " " _
                             + Trim(RetNotes.NotesText4)
                    End If
                    Call StripCharacters(Temp, Chr(13))
                    Call StripCharacters(Temp, Chr(10))
                    If (Trim(Temp) <> "") Then
                        ListType = 3
                        For j = 1 To Len(Temp) Step 47
                            QTemp = Mid(Temp, j, 46)
                            GoSub WrapIt
                            If (qz > 0) Then
                                j = j - (Len(QTemp) - (qz - 1))
                            End If
                        Next j
                    End If
                End If
                m = m + 1
            Wend
        End If
        Set RetNotes = Nothing
        If (lstFinding.ListCount > 0) Or (lstActions.ListCount > 0) Then
            LoadImpression = True
        End If
    End If
    Set RetDr = Nothing
    Set RetAppt = Nothing
    cmdRx.Visible = EditPreviousOn
End If
Set oLocalDate = Nothing
Exit Function
GetOrders:
    sOrderSeries = ""
    If (Val(Trim(Temp)) > 0) Then
        If (DispensePCOn) Then
            Set RetPC = New PCInventory
            RetPC.InventoryId = Val(Trim(Temp))
            If (RetPC.RetrievePCInventory) Then
                sOrderSeries = Trim(RetPC.Type_)
            End If
            Set RetPC = Nothing
        ElseIf (DispenseCLOn) Then
            Set RetCL = New CLInventory
            RetCL.InventoryId = Val(Trim(Temp))
            If (RetCL.RetrieveCLInventory) Then
                sOrderSeries = Trim(RetCL.Series)
            End If
            Set RetCL = Nothing
        End If
    End If
    Return
WrapIt:
    If (Trim(QTemp) <> "") Then
        If (Mid(QTemp, Len(QTemp), 1) = " ") And (Len(QTemp) <= 47) Then
            DTemp = Space(46)
            Mid(DTemp, 1, Len(QTemp)) = QTemp
            DTemp = DTemp + Trim(str(NoteId))
            If (ListType = 1) Then
                lstImp.AddItem DTemp
            ElseIf (ListType = 2) Then
                lstFinding.AddItem DTemp
            ElseIf (ListType = 3) Then
                lstActions.AddItem DTemp
            End If
            qz = 0
        Else
            For qz = Len(QTemp) To 1 Step -1
                DTemp = Space(46)
                If (Mid(QTemp, qz, 1) = " ") Then
                    PTemp = Left(QTemp, qz)
                    If (ListType = 1) Then
                        Mid(DTemp, 1, Len(PTemp)) = PTemp
                        DTemp = DTemp + Trim(str(NoteId))
                        lstImp.AddItem DTemp
                    ElseIf (ListType = 2) Then
                        Mid(DTemp, 1, Len(PTemp)) = PTemp
                        DTemp = DTemp + Trim(str(NoteId))
                        lstFinding.AddItem DTemp
                    ElseIf (ListType = 3) Then
                        Mid(DTemp, 1, Len(PTemp)) = PTemp
                        DTemp = DTemp + Trim(str(NoteId))
                        lstActions.AddItem DTemp
                    End If
                    Exit For
                End If
            Next qz
        End If
    End If
    Return
End Function

Private Sub cmdDone_Click()
Unload frmImpression
End Sub

Private Sub cmdHome_Click()
Unload frmImpression
End Sub

Private Function GetDiagnosis(TheSystem As String, TheDiagnosis As String, TheName As String, TheLevel As Integer) As Boolean
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
GetDiagnosis = False
TheName = ""
If (Trim(TheDiagnosis) <> "") Then
    Set RetrieveDiagnosis = New DiagnosisMasterPrimary
    RetrieveDiagnosis.PrimarySystem = TheSystem
    RetrieveDiagnosis.PrimaryDiagnosis = ""
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = TheDiagnosis
    RetrieveDiagnosis.PrimaryLevel = TheLevel
    RetrieveDiagnosis.PrimaryRank = 0
    If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
        If (RetrieveDiagnosis.SelectPrimary(1)) Then
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                TheName = RetrieveDiagnosis.PrimaryLingo
            Else
                TheName = RetrieveDiagnosis.PrimaryName
            End If
            If (TheSystem = "") Then
                TheSystem = RetrieveDiagnosis.PrimarySystem
            End If
            GetDiagnosis = True
        End If
    End If
    Set RetrieveDiagnosis = Nothing
End If
End Function

Private Sub cmdHxRx_Click()
frmRxHistory.CurrentAction = "--"
frmRxHistory.EditPrevOn = EditPreviousOn
frmRxHistory.ActivityId = ActivityId
If (frmRxHistory.LoadHistoryRx(PatientId, False)) Then
    frmRxHistory.Show 1
    frmRxHistory.CurrentAction = ""
End If
End Sub

Private Sub cmdINotes_Click()
frmNotes.NoteId = 0
frmNotes.EyeContext = ""
frmNotes.NoteOn = False
frmNotes.SystemReference = "IMPR"
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    If (frmNotes.NoteOn) Then
        Call LoadImpression(True)
    End If
End If
End Sub

Private Sub cmdPNotes_Click()
frmNotes.NoteId = 0
frmNotes.EyeContext = ""
frmNotes.NoteOn = False
frmNotes.SystemReference = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    If (frmNotes.NoteOn) Then
        Call LoadImpression(True)
    End If
End If
End Sub

Private Sub cmdRx_Click()
Dim i As Integer
Dim u As Integer
Dim AType As String
Dim ManyOn As Integer
Dim PrintIt As Boolean
Dim Items(10) As Integer
Erase Items
PrintIt = False
u = 0
ManyOn = 0
frmEventMsgs.Header = "Drugs or Spectacle Rxs ?"
frmEventMsgs.AcceptText = "Drug"
frmEventMsgs.RejectText = "Spectacle"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    For i = 0 To lstActions.ListCount - 1
        If (UCase(Left(lstActions.List(i), 2)) = "RX") Then
            u = u + 1
            Items(u) = i
            ManyOn = ManyOn + 1
        End If
    Next i
    For i = 1 To u
        PrintIt = True
        lblPrint.Visible = True
        DoEvents
        Call DoDrugPrint(Items(i), AppointmentId, True, True)
'        If (i = 1) And (u = 1) Then
'            Call DoDrugPrint(Items(i), AppointmentId, True, True)
'        ElseIf (i = 1) And (u > 1) And (i <> u) Then
'            Call DoDrugPrint(Items(i), AppointmentId, True, False)
'        ElseIf (i <> 1) And (i = u) Then
'            Call DoDrugPrint(Items(i), AppointmentId, False, True)
'        Else
'            Call DoDrugPrint(Items(i), AppointmentId, False, False)
'        End If
    Next i
    lblPrint.Visible = False
ElseIf (frmEventMsgs.Result = 2) Then
    For i = 0 To lstActions.ListCount - 1
        If (UCase(Left(lstActions.List(i), 8)) = "DISPENSE") Then
            u = u + 1
            Items(u) = i
            ManyOn = ManyOn + 1
        End If
    Next i
    For i = 1 To u
        AType = "PC"
        If (InStrPS(UCase(lstActions.List(Items(i))), "SPECTACLE") = 0) Then
            AType = "CL"
        End If
        PrintIt = True
        lblPrint.Visible = True
        DoEvents
        Call DoEyeWearPrint(Items(i), PatientId, AppointmentId, AType, True, True)
'        If (i = 1) And (u = 1) Then
'            Call DoEyeWearPrint(Items(i), PatientId, AppointmentId, AType, True, True)
'        ElseIf (i = 1) And (u > 1) And (i <> u) Then
'            Call DoEyeWearPrint(Items(i), PatientId, AppointmentId, AType, True, False)
'        Else
'            Call DoEyeWearPrint(Items(i), PatientId, AppointmentId, AType, False, True)
'        End If
    Next i
    lblPrint.Visible = False
Else
    Exit Sub
End If
If Not (PrintIt) Then
    frmEventMsgs.Header = "No Rxs to print"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub


Private Sub lstActions_Click()
If (EditPreviousOn) Then
    If (lstActions.ListIndex = 0) Then
        Call LoadCodes("ACTION")
        lstItems.Visible = True
        Trigger = "A"
    ElseIf (lstActions.ListIndex > 0) Then
        If (Len(lstActions.List(lstActions.ListIndex)) > 75) Then
            Dim Y As Integer
            For Y = lstActions.ListIndex To 0 Step -1
                If (Left(lstActions.List(Y), 5) <> "     ") Then
                    Exit For
                End If
            Next
            Select Case True
            Case InStrPS(UCase(lstActions.List(Y)), "DISPENSE") <> 0, _
                 InStrPS(UCase(lstActions.List(Y)), "ORDER TRIAL CL") <> 0
                Call frmSystems1.ShowRefract
                LoadImpression (True)
            Case Else
                Call MaintainActions(lstActions, Y)
            End Select
        End If
    End If
End If
End Sub

Private Sub lstFinding_Click()
If (EditPreviousOn) Then
    If (lstFinding.ListIndex >= 0) Then
        Call MaintainFind(lstFinding, lstFinding.ListIndex)
    End If
End If
End Sub

Private Sub lstImp_Click()
    If (EditPreviousOn) Then
        frmSetDiagnosis.EditPreviousOn = True
        frmSetDiagnosis.PatientId = PatientId
        frmSetDiagnosis.AppointmentId = AppointmentId
        If frmSetDiagnosis.LoadImpressions(True, False, False, True, True) Then
            frmSetDiagnosis.Show 1
            If frmSetDiagnosis.NavigationAction = EFormResults.efrDone Then
                frmSetDiagnosis.RetainImpressions PatientId
                frmSetDiagnosis.PurgeImpressions PatientId, AppointmentId
                frmSetDiagnosis.PostImpressions PatientId, AppointmentId
                LoadImpression True
            ElseIf frmSetDiagnosis.NavigationAction = EFormResults.efrHome Then
                frmSetDiagnosis.PurgeRetainedList PatientId, False
            End If
        End If
    End If
End Sub

Private Sub lstItems_Click()
Dim TheId As Long, VndId As Long
Dim j As Integer
Dim a As Integer, u As Integer
Dim Idx As Integer
Dim MyTemp As String
Dim TDate As String
Dim AEye As String, Vndr As String
Dim Temp1 As String, Temp2 As String
Dim AResult As String
Dim OResult As String, DisplayText As String
Dim FollowOn As Boolean
FollowOn = False
If (lstItems.ListIndex > 0) Then
    If (Trigger = "I") Then
        AEye = ""
        frmEventMsgs.Header = "Select Eye ?"
        frmEventMsgs.AcceptText = "OD"
        frmEventMsgs.RejectText = "OS"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "OU"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            AEye = "OD"
        ElseIf (frmEventMsgs.Result = 2) Then
            AEye = "OS"
        ElseIf (frmEventMsgs.Result = 3) Then
            AEye = "OU"
        End If
        If (AEye <> "") Then
            Call GetSeverity(DisplayText, AEye)
            Idx = lstImp.ListCount
            TheId = 0
            If (lstImp.ListIndex > 0) Then
                Idx = lstImp.ListIndex + 1
                TheId = Val(Trim(Mid(lstImp.List(lstImp.ListIndex), 76, Len(lstImp.List(lstImp.ListIndex)) - 75)))
            End If
            If (ChangeImpression(TheId, lstItems.List(lstItems.ListIndex), DisplayText, Idx, AEye, False)) Then
                Call LoadImpression(True)
            End If
        End If
    ElseIf (Trigger = "A") Then
        Temp1 = Trim(Left(lstItems.List(lstItems.ListIndex), 60))
        Temp2 = Mid(lstItems.List(lstItems.ListIndex), 61, 3)
        If (InStrPS(UCase(Temp1), "- RX") <> 0) Then
            frmRxManage.CurrentAction = "-"
            frmRxManage.EditPreviousOn = EditPreviousOn
            frmRxManage.LatestAppointmentOn = frmSystems1.LatestAppointmentOn
            frmRxManage.PrescribeOn = True
            frmRxManage.ActivityId = ActivityId
            frmRxManage.AppointmentId = AppointmentId
            frmRxManage.PatientId = PatientId
            Set frmRxManage.lstExternal = frmSystems1.GetAllergy
            If (frmRxManage.LoadRxs(True)) Then
                frmRxManage.Show 1
                Call LoadImpression(True)
            End If
        Else
            DisplayText = ""
            Call ProcessTrigger(Temp1, Temp2, DisplayText, OResult, AResult, lstActions.ListIndex, FollowOn)
            If (Len(DisplayText) < 6) Then
                DisplayText = ""
            End If
            If (Trim(DisplayText) = "") Or (Len(Trim(DisplayText)) < 6) Then
                If (Temp2 = "000") Or (Trim(Temp2) = "") Then
                    Temp1 = Mid(Temp1, 6, Len(Temp1) - 5)
                    If (InStrPS(UCase(Temp1), "- RX") > 0) Then
                        DisplayText = Temp1 + "-8/"
                    Else
                        DisplayText = Temp1 + "-1/"
                    End If
                End If
            End If
            If (Trim(Temp1) <> "") And (Len(DisplayText) > 5) Then
                TheId = 0
                Idx = lstActions.ListCount
                If (lstActions.ListIndex > 0) Then
                    Idx = lstActions.ListIndex + 1
                    TheId = Val(Trim(Mid(DisplayText, 76, Len(lstActions.List(lstActions.ListIndex)) - 75)))
                End If
                If (ChangeImpression(TheId, lstItems.List(lstItems.ListIndex), DisplayText, Idx, "", FollowOn)) Then
                    If (Trim(OResult) <> "") Then
                        Call SetNewActionImpression(OResult)
                    End If
                    If (Trim(AResult) <> "") Then
                        Call SetNewActionImpression(AResult)
                    End If
                    If (EditPreviousOn) Then
                        If (InStrPS(UCase(DisplayText), "SEND A CONSULT") <> 0) Then
                            u = InStrPS(DisplayText, "/")
                            If (u > 0) Then
                                j = InStrPS(u + 1, DisplayText, "/")
                                If (j > 0) Then
                                    Temp1 = "/Ltr/" + Trim(Mid(DisplayText, u + 1, (j - 1) - u))
                                    Temp2 = Trim(Mid(DisplayText, j + 1, Len(DisplayText) - j))
                                    j = InStrPS(Temp2, "(")
                                    If (j > 0) Then
                                        a = InStrPS(j, Temp2, ")")
                                        If (a > 0) Then
                                            Vndr = Mid(Temp2, j + 1, (a - 1) - j)
                                            VndId = Val(Trim(Vndr))
                                        End If
                                    End If
                                    If (j > 0) Then
                                        Temp2 = Left(Temp2, j - 1)
                                    End If
' look for ccs
                                    j = InStrPS(DisplayText, "/(")
                                    If (j > 0) Then
                                        MyTemp = Mid(DisplayText, j + 1, (Len(DisplayText) - 3) - j)
                                        Temp1 = Temp1 + MyTemp
                                    End If
                                Else
                                    Temp1 = "/Ltr/" + Trim(Mid(DisplayText, u + 1, Len(DisplayText) - u))
                                    Temp2 = ""
                                End If
                                TDate = ""
                                Call FormatTodaysDate(TDate, False)
                                Call ApplSetNewTransaction(AppointmentId, "S", TDate, Temp2, Temp1, VndId)
                            End If
                        End If
                    End If
                    Call LoadImpression(True)
                End If
            End If
        End If
    End If
    lstItems.Visible = False
    Trigger = ""
ElseIf (lstItems.ListIndex = 0) Then
    lstItems.Visible = False
End If
End Sub

Private Function MaintainFind(AList As ListBox, ListIndex As Integer) As Boolean
Dim TheId As Long
Dim Temp As String
Dim IType As String
If (ListIndex >= 0) Then
    If (Len(AList.List(ListIndex)) > 75) Then
        IType = ""
        If (AList.Name = "lstFinding") Then
            IType = "Q"
        ElseIf (AList.Name = "lstActions") Then
            IType = "A"
        End If
        frmEventMsgs.Header = Left(AList.List(ListIndex), 20)
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Highlight"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 2) Then
            Temp = AList.List(ListIndex)
            If (Len(Temp) > 75) Then
                TheId = Val(Mid(Temp, 76, Len(Temp) - 75))
                Call SetHighlight(TheId, IType)
            End If
        End If
    End If
End If
End Function

Private Function MaintainActions(AList As ListBox, ListIndex As Integer) As Boolean
Dim Y As Integer
Dim i As Integer, j As Integer
Dim p As Integer, u As Integer
Dim TheId As Long
Dim Temp As String, AType As String
Dim ATemp As String, IType As String
If (ListIndex > 0) Then
    If (Len(AList.List(ListIndex)) > 75) Then
        For Y = ListIndex To 0 Step -1
            If (Left(AList.List(Y), 5) <> "     ") Then
                ListIndex = Y
                Exit For
            End If
        Next Y
        IType = ""
        frmEventMsgs.Header = Left(AList.List(ListIndex), 20)
        frmEventMsgs.AcceptText = "Print"
        frmEventMsgs.RejectText = "Delete"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Magnify"
        frmEventMsgs.Other1Text = ""
        If (EditPreviousOn) Then
            frmEventMsgs.Other1Text = "Highlight"
            If (AList.Name = "lstFinding") Then
                IType = "F"
            ElseIf (AList.Name = "lstActions") Then
                IType = "A"
            End If
        End If
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            If (InStrPS(UCase(lstActions.List(ListIndex)), "DISPENSE") <> 0) Then
                AType = ""
                If (InStrPS(UCase(lstActions.List(ListIndex)), "SPECTACLE") = 0) Then
                    AType = "CL"
                End If
                Call DoEyeWearPrint(ListIndex, PatientId, AppointmentId, AType, True, True)
            ElseIf (InStrPS(UCase(lstActions.List(ListIndex)), "RX") <> 0) Then
                Call DoDrugPrint(ListIndex, AppointmentId, True, True)
            ElseIf (InStrPS(UCase(lstActions.List(ListIndex)), "INSTRUCTIONS") <> 0) Then
                u = InStrPS(UCase(lstActions.List(ListIndex + 1)), "-INSTRUCTIONS")
                If (u > 0) Then
                    Temp = Trim(Left(lstActions.List(ListIndex + 1), 75))
                    Temp = TemplateDirectory + Temp + ".doc"
                    If (FM.IsFileThere(Temp)) Then
                        Call PrintDocument("Written Instructions", Temp, GetPrinter(2))
                    End If
                End If
            End If
        ElseIf (frmEventMsgs.Result = 2) Then
            Temp = AList.List(ListIndex)
            If (Len(Temp) > 75) Then
                If (AList.Name = "lstActions") Then
                    TheId = Val(Mid(Temp, 201, Len(Temp) - 200))
                Else
                    TheId = Val(Mid(Temp, 76, Len(Temp) - 75))
                End If
                If (DeleteImpression(TheId)) Then
                    Call LoadImpression(True)
                End If
            End If
        ElseIf (frmEventMsgs.Result = 3) Then
            i = ListIndex
            Do Until (Left(AList.List(i), 5) <> "     ")
                i = i - 1
                If (i < 1) Then
                    i = 1
                    Exit Do
                End If
            Loop
            lstZoom.Clear
            lstZoom.AddItem AList.List(i)
            For j = i + 1 To AList.ListCount - 1
                If (Left(AList.List(j), 5) = "     ") Then
                    If (InStrPS(AList.List(j), "Trl:") > 0) Then
                        p = InStrPS(AList.List(j), "OS:")
                        If (p > 0) Then
                            u = InStrPS(AList.List(j), "Trl:")
                            If (u > 0) Then
                                ATemp = Left(AList.List(j), u + 3)
                                lstZoom.AddItem ATemp
                                ATemp = Space(9) + Mid(AList.List(j), u + 4, (p - 1) - (u + 3))
                            Else
                                ATemp = Left(AList.List(j), p - 1)
                            End If
                            Call ReplaceCharacters(ATemp, "  ", " ~")
                            Call ReplaceCharacters(ATemp, vbCrLf, " ")
                            Call StripCharacters(ATemp, "~")
                            If (Len(ATemp) > 110) Then
                                lstZoom.AddItem Left(ATemp, 110)
                                lstZoom.AddItem Space(5) + Mid(ATemp, 111, Len(ATemp) - 110)
                            Else
                                lstZoom.AddItem ATemp
                            End If
                            ATemp = Mid(AList.List(j), p, Len(AList.List(j)) - (p - 1))
                            Call ReplaceCharacters(ATemp, "  ", " ~")
                            Call StripCharacters(ATemp, "~")
                            If (Len(ATemp) > 110) Then
                                lstZoom.AddItem Left(ATemp, 110)
                                lstZoom.AddItem Space(5) + Mid(ATemp, 111, Len(ATemp) - 110)
                            Else
                                lstZoom.AddItem Space(5) + ATemp
                            End If
                        Else
                            lstZoom.AddItem AList.List(j)
                        End If
                    ElseIf (InStrPS(AList.List(j), "Order Trl:") > 0) Then
                        p = InStrPS(AList.List(j), "OS:")
                        If (p > 0) Then
                            lstZoom.AddItem Left(AList.List(j), p - 1)
                            lstZoom.AddItem Space(5) + Mid(AList.List(j), p, Len(AList.List(j)) - (p - 1))
                        Else
                            lstZoom.AddItem AList.List(j)
                        End If
                    Else
                        lstZoom.AddItem AList.List(j)
                    End If
                Else
                    Exit For
                End If
            Next j
            If (lstZoom.ListCount > 0) Then
                lstZoom.Visible = True
            End If
        ElseIf (frmEventMsgs.Result = 5) Then
            Temp = AList.List(ListIndex)
            If (Len(Temp) > 75) Then
                TheId = Val(Mid(Temp, 76, Len(Temp) - 75))
                Call SetHighlight(TheId, IType)
            End If
        End If
    End If
End If
End Function

Private Function GetSeverity(RetText As String, TheEye As String) As Boolean
Dim Temp1 As String
Dim SeverityA As String
Dim SeverityB As String
RetText = ""
frmNumericPad.NumPad_Field = TheEye + " - Select Severity"
frmNumericPad.NumPad_Result = ""
frmNumericPad.NumPad_Max = 0
frmNumericPad.NumPad_Min = 0
frmNumericPad.NumPad_EyesOn = False
frmNumericPad.NumPad_CommentOn = False
frmNumericPad.NumPad_TimeFrameOn = False
frmNumericPad.NumPad_TimeFrame = ""
frmNumericPad.NumPad_DisplayFull = True
frmNumericPad.NumPad_ChoiceOn1 = False
frmNumericPad.NumPad_Choice1 = "Severe"
frmNumericPad.NumPad_ChoiceOn2 = False
frmNumericPad.NumPad_Choice2 = "Mild"
frmNumericPad.NumPad_ChoiceOn3 = False
frmNumericPad.NumPad_Choice3 = "Stable"
frmNumericPad.NumPad_ChoiceOn4 = False
frmNumericPad.NumPad_Choice4 = ""
frmNumericPad.NumPad_ChoiceOn5 = False
frmNumericPad.NumPad_Choice5 = ""
frmNumericPad.NumPad_ChoiceOn6 = False
frmNumericPad.NumPad_Choice6 = "Better"
frmNumericPad.NumPad_ChoiceOn7 = False
frmNumericPad.NumPad_Choice7 = "Worse"
frmNumericPad.NumPad_ChoiceOn8 = False
frmNumericPad.NumPad_Choice8 = "New"
frmNumericPad.NumPad_ChoiceOn9 = False
frmNumericPad.NumPad_Choice9 = ""
frmNumericPad.NumPad_ChoiceOn10 = False
frmNumericPad.NumPad_Choice10 = ""
frmNumericPad.NumPad_ChoiceOn11 = False
frmNumericPad.NumPad_Choice11 = ""
frmNumericPad.NumPad_ChoiceOn12 = False
frmNumericPad.NumPad_Choice12 = ""
frmNumericPad.NumPad_ChoiceOn13 = False
frmNumericPad.NumPad_Choice13 = ""
frmNumericPad.NumPad_ChoiceOn14 = False
frmNumericPad.NumPad_Choice14 = ""
frmNumericPad.NumPad_ChoiceOn15 = False
frmNumericPad.NumPad_Choice15 = ""
frmNumericPad.NumPad_ChoiceOn16 = False
frmNumericPad.NumPad_Choice16 = ""
frmNumericPad.NumPad_ChoiceOn17 = False
frmNumericPad.NumPad_Choice17 = ""
frmNumericPad.NumPad_ChoiceOn18 = False
frmNumericPad.NumPad_Choice18 = ""
frmNumericPad.NumPad_ChoiceOn19 = False
frmNumericPad.NumPad_Choice19 = ""
frmNumericPad.NumPad_ChoiceOn20 = False
frmNumericPad.NumPad_Choice20 = ""
frmNumericPad.NumPad_ChoiceOn21 = False
frmNumericPad.NumPad_Choice21 = ""
frmNumericPad.NumPad_ChoiceOn22 = False
frmNumericPad.NumPad_Choice22 = ""
frmNumericPad.NumPad_ChoiceOn23 = False
frmNumericPad.NumPad_Choice23 = ""
frmNumericPad.NumPad_ChoiceOn24 = False
frmNumericPad.NumPad_Choice24 = ""
frmNumericPad.Show 1
If Not (frmNumericPad.NumPad_Quit) Then
    Temp1 = Trim(frmNumericPad.NumPad_Result)
    If (frmNumericPad.NumPad_ChoiceOn1) Then
        Temp1 = Temp1 + " " + Trim(frmNumericPad.NumPad_Choice1)
    ElseIf (frmNumericPad.NumPad_ChoiceOn2) Then
        Temp1 = Temp1 + " " + Trim(frmNumericPad.NumPad_Choice2)
    ElseIf (frmNumericPad.NumPad_ChoiceOn3) Then
        Temp1 = Temp1 + " " + Trim(frmNumericPad.NumPad_Choice3)
    ElseIf (frmNumericPad.NumPad_ChoiceOn6) Then
        Temp1 = Temp1 + " " + Trim(frmNumericPad.NumPad_Choice6)
    ElseIf (frmNumericPad.NumPad_ChoiceOn7) Then
        Temp1 = Temp1 + " " + Trim(frmNumericPad.NumPad_Choice7)
    ElseIf (frmNumericPad.NumPad_ChoiceOn8) Then
        Temp1 = Temp1 + " " + Trim(frmNumericPad.NumPad_Choice8)
    End If
    If (TheEye = "OU") Then
        SeverityA = Trim(Temp1)
        SeverityB = Trim(Temp1)
    ElseIf (TheEye = "OD") Then
        SeverityA = Trim(Temp1)
    ElseIf (TheEye = "OS") Then
        SeverityB = Trim(Temp1)
    End If
    RetText = "OD:" + SeverityA + " OS:" + SeverityB
End If
End Function

Private Function LoadCodes(TheType As String) As Boolean
Dim i As Integer
Dim Temp As String
Dim DisplayText As String
Dim RetCode As PracticeCodes
LoadCodes = False
lstItems.Clear
lstItems.AddItem "0  - Close Actions List"
If (Trim(TheType) <> "") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = TheType
    If (RetCode.FindCode > 0) Then
        i = 1
        While (RetCode.SelectCode(i))
            DisplayText = Space(75)
            Temp = Trim(RetCode.ReferenceCode)
            If (IsNumeric(Left(Temp, 2))) Then
                Temp = Trim(Mid(Temp, 5, Len(Temp) - 4))
            End If
            Mid(DisplayText, 1, Trim(Len(RetCode.ReferenceCode))) = Temp
            Mid(DisplayText, 61, 3) = Left(RetCode.ReferenceAlternateCode, 3)
            lstItems.AddItem DisplayText
            i = i + 1
        Wend
        LoadCodes = True
    End If
    Set RetCode = Nothing
End If
End Function

Private Function DeleteImpression(AId As Long) As Boolean
Dim ApptId As Long
Dim ConsultLow As Boolean
Dim RetAppt As SchedulerAppointment
Dim RetCln As PatientClinical
DeleteImpression = False
ConsultLow = False
ApptId = 0
If (AId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = AId
    If (RetCln.RetrievePatientClinical) Then
        If (InStrPS(RetCln.Findings, "SEND A CONSULT") > 0) Then
            ConsultLow = True
            ApptId = RetCln.AppointmentId
        End If
        RetCln.ClinicalStatus = "D"
        Call RetCln.ApplyPatientClinical
        DeleteImpression = True
    End If
    Set RetCln = Nothing
    If (ConsultLow) And (ApptId > 0) Then
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentId = ApptId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            RetAppt.AppointmentResourceId8 = 0
            Call RetAppt.ApplySchedulerAppointment
        End If
        Set RetAppt = Nothing
    End If
End If
End Function

Private Function SetHighlight(AId As Long, IType As String) As Boolean
Dim RetCln As PatientClinical
SetHighlight = False
If (AId > 0) And (Trim(IType) <> "") Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = AId
    If (RetCln.RetrievePatientClinical) Then
        RetCln.ClinicalHighlights = IType
        Call RetCln.ApplyPatientClinical
        SetHighlight = True
    End If
    Set RetCln = Nothing
End If
End Function

Private Function SetNewActionImpression(AResult As String) As Boolean
Dim ConsultHigh As Boolean
Dim RetAppt As SchedulerAppointment
Dim RetCln As PatientClinical
SetNewActionImpression = False
ConsultHigh = False
If (Trim(AResult) <> "") Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = 0
    If (RetCln.RetrievePatientClinical) Then
        RetCln.AppointmentId = AppointmentId
        RetCln.PatientId = PatientId
        RetCln.Symptom = Trim(str(99))
        RetCln.EyeContext = "OU"
        RetCln.ClinicalType = "A"
        RetCln.ImageDescriptor = ""
        RetCln.ImageInstructions = ""
        RetCln.ClinicalStatus = "A"
        RetCln.ClinicalFollowup = ""
        RetCln.Findings = Trim(AResult)
        If (InStrPS(UCase(AResult), "SEND A CONSULT") > 0) Then
            ConsultHigh = True
        End If
        RetCln.ClinicalHighlights = ""
        If (InStrPS(UCase(AResult), "OFFICE PROCEDURE") > 0) Then
            RetCln.ClinicalHighlights = "A"
        ElseIf (InStrPS(UCase(AResult), "ORDER A TEST") > 0) Then
            RetCln.ClinicalHighlights = "A"
        ElseIf (InStrPS(UCase(AResult), "SCHEDULE SURGERY") > 0) Then
            RetCln.ClinicalHighlights = "A"
        End If
        Call RetCln.ApplyPatientClinical
        SetNewActionImpression = False
        If (ConsultHigh) Then
            Set RetAppt = New SchedulerAppointment
            RetAppt.AppointmentId = AppointmentId
            If (RetAppt.RetrieveSchedulerAppointment) Then
                RetAppt.AppointmentResourceId8 = 999
                Call RetAppt.ApplySchedulerAppointment
            End If
            Set RetAppt = Nothing
        End If
    End If
    Set RetCln = Nothing
End If
End Function

Private Function ChangeImpression(AId As Long, ARef As String, ADetails As String, Idx As Integer, TheEye As String, AFollowUp As Boolean) As Boolean
Dim i As Integer
Dim j As Integer
Dim p As Integer, q As Integer
Dim ConsultHigh As Boolean
Dim Temp1 As String
Dim Temp2 As String
Dim RetCln As PatientClinical
Dim RetCln1 As PatientClinical
Dim RetAppt As SchedulerAppointment
ChangeImpression = False
If (Trim(TheEye) = "") Then
    TheEye = "OU"
End If
If (Trim(ARef) <> "") Then
    Temp1 = ""
    Temp2 = ""
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = AId
    If (RetCln.RetrievePatientClinical) Then
        RetCln.AppointmentId = AppointmentId
        RetCln.PatientId = PatientId
        RetCln.Symptom = Trim(str(Idx))
        RetCln.EyeContext = TheEye
        If (Trigger = "I") Then
            RetCln.ClinicalType = "U"
            RetCln.Findings = Trim(Left(ARef, 9))
            i = InStrPS(ADetails, "OS:")
            If (i > 0) Then
                Temp1 = Trim(Left(ADetails, i - 1))
                Temp2 = Trim(Mid(ADetails, i, Len(ADetails) - (i - 1)))
                Call ReplaceCharacters(Temp1, "OD:", " ")
                Call ReplaceCharacters(Temp2, "OS:", " ")
                Temp1 = Trim(Temp1)
                Temp2 = Trim(Temp2)
            End If
            RetCln.ImageDescriptor = Temp1
            RetCln.ImageInstructions = Temp2
            RetCln.ClinicalFollowup = ""
            RetCln.ClinicalStatus = "A"
            Call RetCln.ApplyPatientClinical
        Else
            ConsultHigh = False
            If (InStrPS(ADetails, "/#") <> 0) Then
                j = 1
                i = InStrPS(ADetails, "/#")
                While (i > 0)
                    Temp1 = Mid(ADetails, j, i - (j - 1))
                    If (Left(Temp1, 1) = "-") Then
                        If (InStrPS(UCase(ADetails), "PC RX") > 0) Then
                            Temp1 = "Dispense PC Rx-9/" + Temp1
                        ElseIf (InStrPS(UCase(ADetails), "CL RX") > 0) Then
                            Temp1 = "Dispense CL Rx-9/" + Temp1
                        End If
                    End If
                    GoSub WriteIt
                    j = i + 2
                    i = InStrPS(j, ADetails, "/#")
                Wend
            Else
                RetCln.ClinicalType = "A"
                RetCln.ImageDescriptor = ""
                RetCln.ImageInstructions = ""
                RetCln.ClinicalStatus = "A"
                RetCln.ClinicalSurgery = ""
                RetCln.ClinicalFollowup = ""
                If (AFollowUp) Then
                    RetCln.ClinicalFollowup = "U"
                End If
                RetCln.ClinicalHighlights = ""
                If (InStrPS(UCase(ADetails), "OFFICE PROCEDURE") > 0) Then
                    RetCln.ClinicalHighlights = "A"
                ElseIf (InStrPS(UCase(ADetails), "ORDER A TEST") > 0) Then
                    RetCln.ClinicalHighlights = "A"
                ElseIf (InStrPS(UCase(ADetails), "SCHEDULE SURGERY") > 0) Then
                    RetCln.ClinicalHighlights = "A"
                    RetCln.ClinicalSurgery = "U"
                ElseIf (InStrPS(UCase(ADetails), "SEND A CONSULT") > 0) Then
                    ConsultHigh = True
                End If
                RetCln.Findings = Trim(ADetails)
                Call RetCln.ApplyPatientClinical
                If (ConsultHigh) Then
                    Set RetAppt = New SchedulerAppointment
                    RetAppt.AppointmentId = AppointmentId
                    If (RetAppt.RetrieveSchedulerAppointment) Then
                        RetAppt.AppointmentResourceId8 = 999
                        Call RetAppt.ApplySchedulerAppointment
                    End If
                    Set RetAppt = Nothing
                End If
            End If
        End If
        ChangeImpression = True
    End If
    Set RetCln = Nothing
End If
Exit Function
WriteIt:
    If (Trim(Temp1) <> "") Then
        Set RetCln1 = New PatientClinical
        RetCln1.ClinicalId = 0
        If (RetCln1.RetrievePatientClinical) Then
            RetCln1.AppointmentId = AppointmentId
            RetCln1.PatientId = PatientId
            RetCln1.Symptom = Trim(str(Idx))
            RetCln1.EyeContext = TheEye
            RetCln1.ClinicalType = "A"
            RetCln1.ImageInstructions = ""
            RetCln1.ImageDescriptor = ""
            If (UCase(Left(Temp1, 11)) = "DISPENSE CL") Then
                p = InStrPS(Temp1, "-4/")
                If (p > 0) Then
                    q = InStrPS(Temp1, "-5/")
                    If (q > 0) Then
                        RetCln1.ImageDescriptor = Mid(Temp1, p + 3, (q - 1) - (p + 2))
                        Call ReplaceCharacters(Temp1, Trim(RetCln1.ImageDescriptor), "}")
                        Call StripCharacters(Temp1, "}")
                    End If
                End If
            End If
            RetCln1.Findings = Trim(Temp1)
            If UCase(Mid(Temp1, 1, 5)) = "RX-8/" Then
                RetCln1.ImageDescriptor = "!" & Format(Now, "mm/dd/yyyy")
                RetCln1.ClinicalDraw = frmSystems1.GetDrugId(Temp1)
            End If
            RetCln1.ClinicalStatus = "A"
            RetCln1.ClinicalHighlights = ""
            If (InStrPS(UCase(ADetails), "OFFICE PROCEDURE") > 0) Then
                RetCln.ClinicalHighlights = "A"
            ElseIf (InStrPS(UCase(ADetails), "ORDER A TEST") > 0) Then
                RetCln.ClinicalHighlights = "A"
            ElseIf (InStrPS(UCase(ADetails), "SCHEDULE SURGERY") > 0) Then
                RetCln.ClinicalHighlights = "A"
            End If
            RetCln1.ClinicalFollowup = ""
            If (AFollowUp) Then
                RetCln1.ClinicalFollowup = "U"
            End If
            Call RetCln1.ApplyPatientClinical
        End If
        Set RetCln1 = Nothing
    End If
    Return
End Function

Private Function ProcessTrigger(RefName As String, TheTriggers As String, TheResult As String, OtherResult As String, AnotherResult As String, Idx As Integer, FollowOn As Boolean) As Boolean
Dim w1 As Integer
Dim f As Integer, j As Integer
Dim w As Integer, q As Integer
Dim i As Integer, t As Integer
Dim u As Integer, h As Integer
Dim z As Integer
Dim f1 As Integer, f2 As Integer
Dim RefDrId As Long, PCPId As Long
Dim ApptTypeId As Long
Dim RInvId1 As Long, RInvId2 As Long
Dim ATemp As Boolean, IsRVOn As Boolean, SetApptOn As Boolean
Dim OtherTemp As String
Dim PTemp As String
Dim Ref As String
Dim ApptPeriod As String
Dim Add1 As String, Add2 As String, Add3 As String
Dim NRef As String, YRef As String, YTemp As String
Dim Rx As String, RxDiag As String, RxDose As String
Dim RxFreq As String, RxDuration As String, RxRefill As String
Dim RxNote As String, RNote As String
Dim RxId As String
Dim TempText As String, Temp As String, TempA As String
ProcessTrigger = False
FollowOn = False
TheResult = ""
OtherResult = ""
AnotherResult = ""
Add1 = ""
Add2 = ""
Add3 = ""
IsRVOn = CheckConfigCollection("DORV") = "T"
SetApptOn = False
If (Trim(TheTriggers) <> "") And (Trim(RefName) <> "") Then
    ApptPeriod = ""
    If (Mid(RefName, 3, 1) = "-") Then
        TempA = Trim(Mid(RefName, 5, Len(RefName) - 4))
    Else
        TempA = Trim(RefName)
    End If
    TheResult = Trim(TempA) + "-" + Mid(TheTriggers, 1, 1) + "/"
    If (Mid(TheTriggers, 1, 1) = "0") Then
        TheResult = Trim(TempA) + "-1/"
    End If
    For q = 1 To 3
        Temp = ""
        If (Mid(TheTriggers, q, 1) = "1") Then
            If (Trim(ApptPeriod) = "") Then
                frmNumericPad.NumPad_Result = ""
                frmNumericPad.NumPad_Field = Trim(TempA)
                frmNumericPad.NumPad_Max = 0
                frmNumericPad.NumPad_Min = 0
                frmNumericPad.NumPad_CommentOn = True
                frmNumericPad.NumPad_EyesOn = False
                If (InStrPS(UCase(RefName), "TEST") <> 0) Then
                    frmNumericPad.NumPad_EyesOn = True
                ElseIf (InStrPS(UCase(RefName), "OFFICE PROCEDURES") <> 0) Then
                    frmNumericPad.NumPad_EyesOn = True
                End If
                frmNumericPad.NumPad_TimeFrameOn = True
                frmNumericPad.NumPad_TimeFrame = ""
                frmNumericPad.NumPad_DisplayFull = True
                frmNumericPad.NumPad_ChoiceOn1 = False
                frmNumericPad.NumPad_Choice1 = ""
                frmNumericPad.NumPad_ChoiceOn2 = False
                frmNumericPad.NumPad_Choice2 = ""
                frmNumericPad.NumPad_ChoiceOn3 = False
                frmNumericPad.NumPad_Choice3 = ""
                frmNumericPad.NumPad_ChoiceOn4 = False
                frmNumericPad.NumPad_Choice4 = ""
                frmNumericPad.NumPad_ChoiceOn5 = False
                frmNumericPad.NumPad_Choice5 = ""
                frmNumericPad.NumPad_ChoiceOn6 = False
                frmNumericPad.NumPad_Choice6 = ""
                frmNumericPad.NumPad_ChoiceOn7 = False
                frmNumericPad.NumPad_Choice7 = ""
                frmNumericPad.NumPad_ChoiceOn8 = False
                frmNumericPad.NumPad_Choice8 = ""
                frmNumericPad.NumPad_ChoiceOn9 = False
                frmNumericPad.NumPad_Choice9 = ""
                frmNumericPad.NumPad_ChoiceOn10 = False
                frmNumericPad.NumPad_Choice10 = ""
                frmNumericPad.NumPad_ChoiceOn11 = False
                frmNumericPad.NumPad_Choice11 = ""
                frmNumericPad.NumPad_ChoiceOn12 = False
                frmNumericPad.NumPad_Choice12 = ""
                frmNumericPad.NumPad_ChoiceOn13 = False
                frmNumericPad.NumPad_Choice13 = ""
                frmNumericPad.NumPad_ChoiceOn14 = False
                frmNumericPad.NumPad_Choice14 = ""
                frmNumericPad.NumPad_ChoiceOn15 = False
                frmNumericPad.NumPad_Choice15 = ""
                frmNumericPad.NumPad_ChoiceOn16 = False
                frmNumericPad.NumPad_Choice16 = ""
                frmNumericPad.NumPad_ChoiceOn17 = False
                frmNumericPad.NumPad_Choice17 = ""
                frmNumericPad.NumPad_ChoiceOn18 = False
                frmNumericPad.NumPad_Choice18 = ""
                frmNumericPad.NumPad_ChoiceOn19 = False
                frmNumericPad.NumPad_Choice19 = ""
                frmNumericPad.NumPad_ChoiceOn20 = False
                frmNumericPad.NumPad_Choice20 = ""
                frmNumericPad.NumPad_ChoiceOn21 = False
                frmNumericPad.NumPad_Choice21 = ""
                frmNumericPad.NumPad_ChoiceOn22 = False
                frmNumericPad.NumPad_Choice22 = ""
                frmNumericPad.NumPad_ChoiceOn23 = False
                frmNumericPad.NumPad_Choice23 = ""
                frmNumericPad.NumPad_ChoiceOn24 = False
                frmNumericPad.NumPad_Choice24 = ""
                frmNumericPad.Show 1
                If (Trim(frmNumericPad.NumPad_Result) <> "") Then
                    SetApptOn = frmNumericPad.SetApptOn
                    Temp = Trim(frmNumericPad.NumPad_Result) + " " + Trim(frmNumericPad.NumPad_TimeFrame)
                    Add2 = Trim(frmNumericPad.NumPad_Result) + " " + Trim(frmNumericPad.NumPad_TimeFrame)
                    If (Trim(frmNumericPad.NumPad_Comments) <> "") Then
                        Temp = Trim(Temp) + "<" + Trim(frmNumericPad.NumPad_Comments) + ">"
                        Add2 = Add2 + "<" + Trim(frmNumericPad.NumPad_Comments) + ">"
                    End If
                    If (IsRVOn) Then
                        If (SetApptOn) Or ((InStrPS(UCase(RefName), "TEST") = 0) And (InStrPS(UCase(RefName), "OFFICE") = 0) And (InStrPS(UCase(RefName), "SCHEDULE SURGERY") = 0)) Then
                            frmAppType.ApptTypeId = 0
                            frmAppType.ApptTypeName = ""
                            frmAppType.ApptPeriod = ""
                            If (frmAppType.LoadAppt("v", True)) Then
                                frmAppType.Show 1
                                If (Trim(frmAppType.ApptTypeName) <> "") Then
                                    Temp = Trim(Temp) + " <^" + Trim(frmAppType.ApptTypeName) + ">"
                                    Add2 = Add2 + " <^" + Trim(frmAppType.ApptTypeName) + ">"
                                End If
                            End If
                        End If
                    End If
                    If (SetApptOn) Then
                        NRef = "a"
                        YRef = ""
                        YTemp = ""
                        If (InStrPS(UCase(RefName), "TEST") <> 0) Then
                            NRef = "A"
                            YRef = "External Tests"
                            YTemp = Add1
                            h = InStrPS(YTemp, "/")
                            If (h > 0) Then
                                YTemp = Trim(Left(YTemp, h - 3))
                            End If
                        ElseIf (InStrPS(UCase(RefName), "OFFICE PROCEDURES") <> 0) Then
                            NRef = "A"
                            YRef = "OfficeProcedures"
                            YTemp = Add1
                            h = InStrPS(YTemp, "/")
                            If (h > 0) Then
                                YTemp = Trim(Left(YTemp, h - 3))
                            End If
                        ElseIf (InStrPS(UCase(RefName), "SCHEDULE SURGERY") <> 0) Then
                            NRef = "A"
                            YRef = "SURGERYTYPE"
                            YTemp = Temp
                            h = InStrPS(YTemp, "/")
                            If (h > 0) Then
                                YTemp = Trim(Left(YTemp, h - 3))
                            End If
                            h = InStrPS(YTemp, "(")
                            If (h > 0) Then
                                YTemp = Trim(Left(YTemp, h - 1))
                            End If
                            h = InStrPS(YTemp, "<^")
                            If (h > 0) Then
                                YTemp = Trim(Mid(YTemp, h + 2, Len(YTemp) - (h + 1)))
                            End If
                        End If
                        If Not (IsTestApptTypeSet(YRef, YTemp, OtherTemp)) Then
                            frmAppType.ApptTypeId = 0
                            frmAppType.ApptTypeName = ""
                            frmAppType.ApptPeriod = ""
                            If (frmAppType.LoadAppt(NRef, True)) Then
                                frmAppType.Show 1
                                If (frmAppType.ApptTypeId > 0) Then
                                    ApptTypeId = frmAppType.ApptTypeId
                                    If (frmAppType.ApptPeriod = "Y") Then
                                        ApptPeriod = "YEAR"
                                    ElseIf (frmAppType.ApptPeriod = "M") Then
                                        ApptPeriod = "MONTH"
                                    ElseIf (frmAppType.ApptPeriod = "W") Then
                                        ApptPeriod = "WEEK"
                                    Else
                                        ApptPeriod = ""
                                    End If
                                    Add1 = Trim(frmAppType.ApptTypeName) + " "
                                End If
                            End If
                        Else
                            Add1 = Trim(OtherTemp) + " "
                        End If
                        frmAppType.ApptTypeId = ApptTypeId
                        frmAppType.ApptTypeName = ""
                        If (frmAppType.LoadAppt("S", True)) Then
                            frmAppType.Show 1
                            If (Trim(frmAppType.ApptTypeName) <> "") Then
                                Temp = Trim(Temp) + " <" + Trim(frmAppType.ApptPeriod) + ">"
                                Add3 = Add3 + Trim(frmAppType.ApptPeriod)
                            End If
                        End If
                    End If
                Else
                    TheResult = ""
                    Exit For
                End If
            Else
                Temp = Trim(ApptPeriod)
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "2") Then
            frmAppType.ApptTypeId = 0
            frmAppType.ApptTypeName = ""
            frmAppType.ApptPeriod = ""
            If (frmAppType.LoadAppt("a", True)) Then
                frmAppType.Show 1
                If (frmAppType.ApptTypeId > 0) Then
                    ApptTypeId = frmAppType.ApptTypeId
                    If (frmAppType.ApptPeriod = "Y") Then
                        ApptPeriod = "YEAR"
                    ElseIf (frmAppType.ApptPeriod = "M") Then
                        ApptPeriod = "MONTH"
                    ElseIf (frmAppType.ApptPeriod = "W") Then
                        ApptPeriod = "WEEK"
                    Else
                        ApptPeriod = ""
                    End If
                    Temp = frmAppType.ApptTypeName
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "3") Then
            frmAppType.ApptTypeId = 0
            frmAppType.ApptTypeName = ""
            If (frmAppType.LoadAppt("C", True)) Then
                frmAppType.Show 1
                If (Trim(frmAppType.ApptTypeName) <> "") Then
                    Temp = frmAppType.ApptTypeName
                    Add1 = Temp
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "4") Then
            frmAppType.ApptTypeId = 0
            frmAppType.ApptTypeName = ""
            If (frmAppType.LoadAppt("D", True)) Then
                frmAppType.Show 1
                If (Trim(frmAppType.ApptTypeName) <> "") Then
                    Temp = frmAppType.ApptTypeName
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "5") Then
            frmAppType.ApptTypeId = ApptTypeId
            frmAppType.ApptTypeName = ""
            If (frmAppType.LoadAppt("S", True)) Then
                frmAppType.Show 1
                If (Trim(frmAppType.ApptTypeName) <> "") Then
                    Temp = Trim(frmAppType.ApptPeriod)
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "6") Then
            Temp = GetReferralDoctor(AppointmentId, RefDrId)
            If (Trim(Temp) = "") Then
                frmAppType.ApptTypeId = 0
                frmAppType.ApptTypeName = ""
                If (frmAppType.LoadAppt("D", True)) Then
                    frmAppType.Show 1
                    If (Trim(frmAppType.ApptTypeName) <> "") Then
                        Temp = frmAppType.ApptTypeName
                    Else
                        TheResult = ""
                        Exit For
                    End If
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "7") Then
            frmAppType.ApptTypeId = 0
            frmAppType.ApptTypeName = ""
            frmAppType.ApptPeriod = ""
            If (frmAppType.LoadAppt("l", True)) Then
                frmAppType.Show 1
                If (Trim(frmAppType.ApptTypeName) <> "") Then
                    If (InStrPS(UCase(Trim(RefName)), "SCHEDULE SURGERY") > 0) Then
                        Add1 = Trim(frmAppType.ApptTypeName)
                        frmAppType.ApptTypeId = 0
                        frmAppType.ApptTypeName = ""
                        frmAppType.ApptPeriod = ""
                        If (frmAppType.LoadAppt("S", True)) Then
                            frmAppType.Show 1
                            If (Trim(frmAppType.ApptTypeName) <> "") Then
                                Temp = Add1 + " (" + Trim(frmAppType.ApptPeriod) + ")"
                            Else
                                Temp = Add1
                            End If
                        Else
                            Temp = Add1
                        End If
                    Else
                        Temp = Trim(frmAppType.ApptTypeName)
                    End If
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "8") Then
            frmAssignDrugs.Diagnosis = ""
            frmAssignDrugs.PatientId = PatientId
            frmAssignDrugs.AppointmentId = AppointmentId
            frmAssignDrugs.FavoriteOn = True
            frmAssignDrugs.frmRxManage_Write_RX = (frmRxManage.cmdAnother.Text = "Write RX")
            frmAssignDrugs.EditPreviousOn = EditPreviousOn
            If (frmAssignDrugs.LoadAvailableRx(True)) Then
            'DrFirst Open SSO
                Dim objPracticeCode As PracticeCodes
                Dim sqlConString As String
                Dim PracticeToken As String
                sqlConString = ConnectionString
                Set objPracticeCode = New PracticeCodes
                PracticeToken = objPracticeCode.GetPracticeToken("PracticeAuthToken")
                If (CStr(PatientId) <> "") Then
                    Dim frmErx As New ClinicalIntegration.frmErxHelper
                    frmErx.PatientId = CStr(PatientId)
                    frmErx.PracticeToken = PracticeToken
                    'frmErx.sqlConString = ""
                    frmErx.UserId = CStr(UserLogin.iId)
                    frmErx.AppointmentId = AppointmentId
                    frmErx.DbObject = IdbConnection
                    frmErx.ShowDialog
                    frmErx.Close
                    frmErx.Dispose
                End If
'                frmAssignDrugs.Show 1
'                For z = 1 To frmAssignDrugs.TotalRx
'                    If (frmAssignDrugs.getRx(z, Rx, RxDiag, RxDose, RxFreq, RxRefill, RxDuration, RxNote, RxId)) Then
'                        Temp = Temp + TempA + "-8/" + Rx + "-2/" + RxDose + "-3/" + RxFreq + "-4/" + RxRefill + "-5/" + RxDuration + "-6/" + "P-7/" + "#"
'                    End If
'                    If frmAssignDrugs.getRx_6(z, Rx, "record2") Then
'                        Temp = "Rx-8/" & Rx & "P-7/#"
'                    End If
'                Next z
'                If (Trim(Temp) <> "") Then
'                    Call ReplaceCharacters(Temp, "-1/", "~")
'                    Call StripCharacters(Temp, "~")
'                    TheResult = Temp
'                    Exit For
'                End If
                Dim Query As String
                Dim RS As Recordset
                Query = "select top 1 findingdetail from patientclinical where patientid=" + CStr(PatientId) + " and appointmentid= " + CStr(AppointmentId) + " order 			by ClinicalId desc"
                Set RS = GetRS(Query)
                If RS.RecordCount > 0 And (CStr(RS("findingdetail"))) <> "" Then
                    Dim indx, Cnt As Integer
                    Dim s As Object
                        For indx = 1 To lstActions.ListCount
                            If (InStrPS(1, (CStr(RS("findingdetail"))), lstActions.List(indx)) <> 0) Then
                                Cnt = Cnt + 1
                            End If
                        Next
                        If (Cnt = 0) Then
                            TheResult = (CStr(RS("findingdetail")))
                        End If
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "9") Then
            Call frmSystems1.ShowRefract
        ElseIf (Mid(TheTriggers, q, 1) = "A") Then
            frmAppType.ApptTypeId = "0"
            frmAppType.ApptTypeName = ""
            If (frmAppType.LoadAppt("W", True)) Then
                frmAppType.Show 1
                If (frmAppType.ApptTypeId >= 0) Then
                    f1 = 1
                    f2 = InStrPS(f1, frmAppType.ApptTypeName, "/")
                    While (f2 > 0)
                        Temp = Temp + TempA + "-1/" + Trim(Mid(frmAppType.ApptTypeName, f1, f2 - (f1 - 1))) + "#"
                        f1 = f2 + 1
                        f2 = InStrPS(f1, frmAppType.ApptTypeName, "/")
                    Wend
                    If (f1 < Len(frmAppType.ApptTypeName)) Then
                        Temp = Temp + TempA + "-1/" + Trim(Mid(frmAppType.ApptTypeName, f1, f2 - (f1 - 1))) + "#"
                        TheResult = Temp
                    Else
                        If (Trim(Temp) <> "") Then
                            TheResult = Temp
                        End If
                        Exit For
                    End If
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "T") Then
            frmAppType.ApptTypeId = ApptTypeId
            frmAppType.ApptTypeName = ""
            If (frmAppType.LoadAppt("T", True)) Then
                frmAppType.Show 1
                If (Trim(frmAppType.ApptTypeName) <> "") Then
                    Temp = Trim(frmAppType.ApptTypeName)
                    If (IsTestFollowup(Temp)) Then
                        FollowOn = True
                    End If
                    Add1 = Temp
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "C") Or (Mid(TheTriggers, q, 1) = "R") Then
            frmLetters.LetterPCP = ""
            frmLetters.LetterRef = ""
            If (Mid(TheTriggers, q, 1) = "C") Then
                frmLetters.LetterPCP = GetPCP(AppointmentId, PCPId)
                frmLetters.LetterPCPId = PCPId
                frmLetters.LetterRef = GetReferralDoctor(AppointmentId, RefDrId)
                frmLetters.LetterRefId = RefDrId
            End If
            If (frmLetters.LoadLetters(Mid(TheTriggers, q, 1), True)) Then
                frmLetters.Show 1
                If (Trim(frmLetters.LetterName) <> "") Then
                    TempText = ""
                    If (Trim(frmLetters.LetterDestinationCCpcp) <> "") Then
                        TempText = TempText + Trim(frmLetters.LetterDestinationCCpcp) + ","
                    End If
                    If (Trim(frmLetters.LetterDestinationCCref) <> "") Then
                        TempText = TempText + Trim(frmLetters.LetterDestinationCCref) + ","
                    End If
                    If (Trim(frmLetters.LetterDestinationCCoth) <> "") Then
                        TempText = TempText + Trim(frmLetters.LetterDestinationCCoth) + ","
                    End If
                    If (Trim(frmLetters.LetterDestinationCCpat) <> "") Then
                        TempText = TempText + "(P)"
                    End If
                    TempText = Trim(TempText)
                    If (Trim(TempText) <> "") Then
                        If (Mid(TempText, Len(TempText), 1) = ",") Then
                            TempText = Left(TempText, Len(TempText) - 1)
                        End If
                    End If
                    Temp = Trim(frmLetters.LetterName) + "/" + Trim(frmLetters.LetterDestination) + "/" + TempText
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "B") Then
            frmAppType.ApptTypeId = ApptTypeId
            If (Trim(TheResult) <> "") Then
                j = InStrPS(TheResult, "/")
                If (j > 0) Then
                    Temp = Mid(TheResult, j + 1, Len(TheResult) - j)
                    Call ReplaceCharacters(Temp, "-1/", " ")
                    Call ReplaceCharacters(Temp, "-T/", " ")
                End If
                frmAppType.ApptTypeName = Trim(Temp)
                If (frmAppType.LoadAppt("B", True)) Then
                    frmAppType.Show 1
                    If (Trim(frmAppType.ApptTypeName) <> "") Then
                        Temp = Trim(frmAppType.ApptTypeName)
                    Else
                        TheResult = ""
                        Exit For
                    End If
                Else
                    Temp = ""
                End If
            Else
                TheResult = ""
                Exit For
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "O") Then
            frmAppType.ApptTypeId = ApptTypeId
            frmAppType.ApptTypeName = ""
            If (frmAppType.LoadAppt("O", True)) Then
                frmAppType.Show 1
                If (Trim(frmAppType.ApptTypeName) <> "") Then
                    Temp = Trim(frmAppType.ApptTypeName)
                    Add1 = Temp
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        ElseIf (Mid(TheTriggers, q, 1) = "&") Then
            frmAppType.ApptTypeId = ApptTypeId
            frmAppType.ApptTypeName = ""
            If (frmAppType.LoadAppt("&", True)) Then
                frmAppType.Show 1
                If (Trim(frmAppType.ApptTypeName) <> "") Then
                    Temp = Trim(frmAppType.ApptPeriod)
                Else
                    TheResult = ""
                    Exit For
                End If
            End If
        Else
            Temp = ""
        End If
        If (Trim(Temp) <> "") Then
            TheResult = TheResult + Temp + "-" + Mid(TheTriggers, q, 1) + "/"
        End If
    Next q
    If (Mid(TheResult, 3, 1) = "-") Then
        PTemp = Trim(Left(TheResult, 2))
        If (Val(PTemp) > 0) Then
            TheResult = Trim(Mid(TheResult, 4, Len(TheResult) - 3))
        End If
    End If
    If (SetApptOn) Then
        OtherResult = "Schedule Appointment-2/"
        OtherResult = OtherResult + Add1 + "-2/" + Add2 + "-1/"
        OtherResult = OtherResult + Add3 + "-5/"
    End If
End If
End Function

Private Function GetReferralDoctor(ApptId As Long, RefId As Long) As String
Dim RetAppt As SchedulerAppointment
Dim RetPat As Patient
Dim RetRef As PatientReferral
Dim RetVen As PracticeVendors
' Reverse the order look for patient first then paper referring dr
GetReferralDoctor = ""
RefId = 0
If (ApptId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        Set RetPat = New Patient
        RetPat.PatientId = RetAppt.AppointmentPatientId
        If (RetPat.RetrievePatient) Then
            If (RetPat.ReferringPhysician > 0) Then
                Set RetVen = New PracticeVendors
                RetVen.VendorId = RetPat.ReferringPhysician
                If (RetVen.RetrieveVendor) Then
                    GetReferralDoctor = Trim(RetVen.VendorName)
                    RefId = RetVen.VendorId
                End If
                Set RetVen = Nothing
            End If
        End If
        Set RetPat = Nothing
        If (RefId < 1) Then
            If (RetAppt.AppointmentReferralId > 0) Then
                Set RetRef = New PatientReferral
                RetRef.ReferralId = RetAppt.AppointmentId
                If (RetRef.RetrievePatientReferral) Then
                    If (RetRef.ReferralFromId > 0) Then
                        Set RetVen = New PracticeVendors
                        RetVen.VendorId = RetRef.ReferralFromId
                        If (RetVen.RetrieveVendor) Then
                            GetReferralDoctor = Trim(RetVen.VendorName)
                            RefId = RetVen.VendorId
                        End If
                        Set RetVen = Nothing
                    End If
                End If
                Set RetRef = Nothing
            End If
        End If
    End If
    Set RetAppt = Nothing
End If
End Function

Private Function GetPCP(ApptId As Long, RefId As Long) As String
Dim RetAppt As SchedulerAppointment
Dim RetPat As Patient
Dim RetVen As PracticeVendors
GetPCP = ""
RefId = 0
Set RetAppt = New SchedulerAppointment
RetAppt.AppointmentId = ApptId
If (RetAppt.RetrieveSchedulerAppointment) Then
    Set RetPat = New Patient
    RetPat.PatientId = RetAppt.AppointmentPatientId
    If (RetPat.RetrievePatient) Then
        If (RetPat.PrimaryCarePhysician > 0) Then
            Set RetVen = New PracticeVendors
            RetVen.VendorId = RetPat.PrimaryCarePhysician
            If (RetVen.RetrieveVendor) Then
                GetPCP = Trim(RetVen.VendorName)
                RefId = RetVen.VendorId
            End If
            Set RetVen = Nothing
        End If
    End If
    Set RetPat = Nothing
End If
Set RetAppt = Nothing
End Function

Private Sub DoDrugPrint(Index As Integer, ApptId As Long, FirstOn As Boolean, LastOn As Boolean)
Dim w As Integer, p As Integer
Dim k As Integer
Dim RxDura As String
Dim Rx As String, SigFile As String
Dim RxDose As String, RxFreq As String
Dim RxDate As String, RxRefill As String
Dim RxDaw As String
Dim RxNote As String, RxNote2 As String
Dim TheFile As String
Dim TempFile As String
Dim ThePrinter As String
Rx = ""
RxDura = ""
RxDose = ""
RxFreq = ""
RxRefill = ""
RxNote = ""
If (Index >= 0) Then
    RxDate = ""
    Call FormatTodaysDate(RxDate, False)
    Rx = Trim(Left(lstActions.List(Index + 1), 50))
    If (Trim(Left(lstActions.List(Index + 2), 4)) = "") Then
        RxDose = Trim(Left(lstActions.List(Index + 2), 50))
    End If
    If (Trim(Left(lstActions.List(Index + 3), 4)) = "") Then
        RxFreq = Trim(Left(lstActions.List(Index + 3), 50))
    End If
    If (Trim(Left(lstActions.List(Index + 4), 4)) = "") And (Trim(Left(lstActions.List(Index + 3), 4)) = "") Then
        RxRefill = Trim(Left(lstActions.List(Index + 4), 50))
        w = InStrPS(RxRefill, "[Last")
        If (w > 0) Then
            RxRefill = Left(RxRefill, w - 1)
        End If
    End If
    If (Trim(Left(lstActions.List(Index + 5), 4)) = "") Then
        RxDura = Trim(Left(lstActions.List(Index + 5), 50))
    End If
    k = InStrPS(UCase(RxRefill), "(DAW")
    If (k <> 0) Then
        RxDaw = "daw"
        If (k = 1) Then
            RxRefill = ""
        Else
            If (Len(RxRefill) - (k + 4) > 0) Then
                RxRefill = Left(RxRefill, k - 1) + Mid(RxRefill, k + 5, Len(RxRefill) - (k + 4))
            Else
                RxRefill = Left(RxRefill, k - 1)
            End If
        End If
        
    End If
    Call frmRxDetails.GetRxNotes(AppointmentId, PatientId, Trim(Rx), RxNote, RxNote2, False)
    If RxNote2 <> "" Then
        RxNote2 = ", " & "Note to Pharmacist: " & RxNote2
    End If
    RxNote = Trim(RxNote & RxNote2)
    TheFile = DoctorInterfaceDirectory + "DrugRx" + "_" + Trim(str(ApptId)) + "_" + Trim(str(PatientId)) + ".txt"
    If (FirstOn) Then
        If (FM.IsFileThere(TheFile)) Then
            Call KillDocumentProcessing(TheFile, True, True)
        End If
    End If
    TempFile = TemplateDirectory + "DrugRx.doc"
    'If (CreateDrugs(PatientId, AppointmentId, ActivityId, TheFile, Trim(Rx), Trim(RxDose), Trim(RxFreq), Trim(RxRefill), Trim(RxDaw), Trim(RxNote), Trim(RxDura), FirstOn, LastOn)) Then
    If (CreateDrugs(PatientId, AppointmentId, ActivityId, TheFile, Trim(Rx), Trim(RxDose), Trim(RxFreq), Trim(RxRefill), "", Trim(RxNote), Trim(RxDura), FirstOn, LastOn)) Then
        If (LastOn) Then
            lblPrint.Visible = True
            Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
' use a special signature file setup for Rxs
            If (Trim(SigFile) <> "") Then
                p = InStrPS(SigFile, ".")
                If (p > 0) Then
                    SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
                End If
            End If
            ThePrinter = GetPrinter(PRINTER_Rx)
            Call PrintTemplate(TheFile, TempFile, True, False, 1, ThePrinter, SigFile, 1, True)
            lblPrint.Visible = False
        End If
    End If
End If
End Sub

Private Function CreateDrugs(PatId As Long, ApptId As Long, ActId As Long, ResultFile As String, DrugName As String, Dosage As String, Freq As String, Refill As String, Daw As String, ANote As String, Dura As String, FirstOn As Boolean, LastOn As Boolean) As Boolean
Dim FileNum As Integer
Dim h As Integer
Dim i As Integer, q As Integer
Dim RscId As Long
Dim Pills As String
Dim Resultfeild As String
Dim Resultvalue As String
Dim resultstring As String
Dim Temp As String, ADate As String
Dim NewFile As String, TempZip As String
Dim RetAppt As SchedulerAppointment
Dim RetDr As SchedulerResource
Dim RetAct As PracticeActivity
Dim RetPat As Patient
Dim RetPrc As PracticeName
Dim dte As String
CreateDrugs = False
lstBox.Clear
If (PatId > 0) Then
    Set RetPat = New Patient
    Set RetPrc = New PracticeName
    Set RetDr = New SchedulerResource
    Set RetAppt = New SchedulerAppointment
    Set RetAct = New PracticeActivity
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        If (ActId > 0) Then
            RscId = 0
            RetAct.ActivityId = ActId
            If (RetAct.RetrieveActivity) Then
                RscId = RetAct.ActivityCurrResId
                If (RscId > 0) Then
                    RetDr.ResourceId = RscId
                    If (RetDr.RetrieveSchedulerResource) Then
                        If (Trim(RetDr.ResourceSignatureFile) = "") Then
                            RscId = 0
                        End If
                    End If
                End If
            End If
            RetAppt.AppointmentId = ApptId
            If (RetAppt.RetrieveSchedulerAppointment) Then
                If (RscId < 1) Then
                    If (RscId = 0) Then
                        If (RetAppt.AppointmentResourceId1 > 0) Then
                            RetDr.ResourceId = RetAppt.AppointmentResourceId1
                            If (RetDr.RetrieveSchedulerResource) Then
                    
                            End If
                        ElseIf (RetPat.SchedulePrimaryDoctor > 0) Then
                            RetDr.ResourceId = RetPat.SchedulePrimaryDoctor
                            If (RetDr.RetrieveSchedulerResource) Then
                    
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
' Load the Merge File
    lstBox.Clear
    If (RetDr.ResourceId > 0) Then
        lstBox.AddItem "01:" + Trim(RetDr.ResourceDescription)
        lstBox.AddItem "03:" + Trim(RetDr.ResourceDea)
        lstBox.AddItem "04:" + Trim(RetDr.ResourceLic)
        If (RetDr.PracticeId > 0) Then
            RetPrc.PracticeId = RetDr.PracticeId
            If (RetPrc.RetrievePracticeName) Then
                TempZip = ""
                Call DisplayPhone(RetPrc.PracticePhone, TempZip)
                lstBox.AddItem "02:" + TempZip
                lstBox.AddItem "05:" + Trim(RetPrc.PracticeAddress)
                TempZip = ""
                Call DisplayPhone(RetPrc.PracticeZip, TempZip)
                Temp = Trim(RetPrc.PracticeCity) + ", " + RetPrc.PracticeState + " " + TempZip
                lstBox.AddItem "06:" + Temp
            Else
                lstBox.AddItem "02:    "
                lstBox.AddItem "05:    "
                lstBox.AddItem "06:    "
            End If
        Else
            lstBox.AddItem "02:    "
            lstBox.AddItem "05:    "
            lstBox.AddItem "06:    "
        End If
    Else
        lstBox.AddItem "01:    "
        lstBox.AddItem "02:    "
        lstBox.AddItem "03:    "
        lstBox.AddItem "04:    "
        lstBox.AddItem "05:    "
        lstBox.AddItem "06:    "
    End If
    Temp = Trim(Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial) + " " + Trim(RetPat.LastName) + " " + Trim(RetPat.NameRef))
    Call StripCharacters(Temp, Chr(34))
    lstBox.AddItem "07:" + Temp
    i = GetAge(RetPat.BirthDate)
    lstBox.AddItem "08:" + Trim(str(i))
    lstBox.AddItem "09:" + Trim(RetPat.Address)
    TempZip = ""
    Call DisplayZip(RetPat.Zip, TempZip)
    Temp = Trim(RetPat.City) + ", " + RetPat.State + " " + TempZip
    lstBox.AddItem "10:" + Temp
    If (Trim(DrugName) <> "") Then
        lstBox.AddItem "11:" + Trim(DrugName)
    Else
        lstBox.AddItem "11:    "
    End If
    
    If (Trim(Dosage) <> "") Then
        lstBox.AddItem "12:" + Trim(Dosage)
    Else
        lstBox.AddItem "12:    "
    End If
    
    If (Trim(Freq) <> "") Then
        lstBox.AddItem "20:" + Trim(Freq)
    Else
        lstBox.AddItem "20:    "
    End If
    
    If (Trim(Refill) <> "") Then
        lstBox.AddItem "13:" + Trim(Refill)
    Else
        lstBox.AddItem "13:    "
    End If
    
    If (Trim(Dura) <> "") Then
        If InStrPS(1, Dura, "DAW") > 0 Then
            lstBox.AddItem "14:" + "DAW"
        Else
            lstBox.AddItem "14:    "
        End If
    Else
        lstBox.AddItem "14:    "
    End If
    
    If (Trim(ANote) <> "") Then
        Call StripCharacters(ANote, Chr(13))
        Call StripCharacters(ANote, Chr(10))
        lstBox.AddItem "16:" + Trim(ANote)
    Else
        lstBox.AddItem "16:    "
    End If
    
    Call FormatTodaysDate(ADate, False)
    lstBox.AddItem "15:" + ADate
    If (Trim(RetDr.ResourceSignatureFile) <> "") Then
        If (FM.IsFileThere(Trim(RetDr.ResourceSignatureFile))) Then
            lstBox.AddItem "17:" + Trim(RetDr.ResourceSignatureFile)
        Else
            lstBox.AddItem "17:    "
        End If
    Else
        lstBox.AddItem "17:    "
    End If
    If (Trim(RetPat.BirthDate) <> "") Then
        lstBox.AddItem "18:" + Mid(RetPat.BirthDate, 5, 2) + "/" + Mid(RetPat.BirthDate, 7, 2) + "/" + Left(RetPat.BirthDate, 4)
    Else
        lstBox.AddItem "18:    "
    End If
    If (Trim(RetPat.Gender) <> "") Then
        lstBox.AddItem "19:" + RetPat.Gender
    Else
        lstBox.AddItem "19:"
    End If
    lstBox.AddItem "04a:    "
    lstBox.AddItem "04b:    "
    lstBox.AddItem "05a:    "
    lstBox.AddItem "06a:    "
    lstBox.AddItem "07a:    "
    lstBox.AddItem "08a:    "
    lstBox.AddItem "09a:    "
    lstBox.AddItem "09b:    "
    lstBox.AddItem "11a:    "
    lstBox.AddItem "11b:    "
    lstBox.AddItem "18a:    "
    lstBox.AddItem "18b:    "
    lstBox.AddItem "18c:    "
    lstBox.AddItem "18d:    "
    lstBox.AddItem "18e:    "
    lstBox.AddItem "18f:    "
    
    If (Trim(RetDr.ResourceNPI) <> "") Then
        lstBox.AddItem "21:" + Trim(RetDr.ResourceNPI)
    Else
        lstBox.AddItem "21:    "
    End If
    
    If (Trim(RetAppt.AppointmentDate) <> "") Then
        dte = Trim(RetAppt.AppointmentDate)
        lstBox.AddItem "22:" & Mid(dte, 5, 2) & "/" & Mid(dte, 7, 2) & "/" & Mid(dte, 1, 4)
    Else
        lstBox.AddItem "22:    "
    End If
    
    lstBox.AddItem "23:    "
    lstBox.AddItem "24:    "
    lstBox.AddItem "25:    "
    lstBox.AddItem "26:    "
    lstBox.AddItem "27:    "
    lstBox.AddItem "28:    "
    lstBox.AddItem "29:    "
    lstBox.AddItem "30:    "
    lstBox.AddItem "31:    "
    lstBox.AddItem "32:    "
    lstBox.AddItem "33:    "
    lstBox.AddItem "34:    "
    lstBox.AddItem "35:    "
    lstBox.AddItem "36:    "
    lstBox.AddItem "37:    "
    lstBox.AddItem "38:    "
    lstBox.AddItem "39:    "
    lstBox.AddItem "40:    "
    lstBox.AddItem "41:    "
    lstBox.AddItem "42:    "
    lstBox.AddItem "43:    "
    lstBox.AddItem "44:    "
    lstBox.AddItem "45:    "
    lstBox.AddItem "46:    "
    lstBox.AddItem "47:    "
    lstBox.AddItem "48:    "
    lstBox.AddItem "49:    "
    lstBox.AddItem "50:    "
    lstBox.AddItem "51:   "
    lstBox.AddItem "52:   "
    lstBox.AddItem "53:   "
    lstBox.AddItem "54:   "
    lstBox.AddItem "55:   "
    lstBox.AddItem "56:   "
    lstBox.AddItem "57:   "
    lstBox.AddItem "58:   "
    lstBox.AddItem "59:   "
    lstBox.AddItem "60:   "
    lstBox.AddItem "61:   "
    lstBox.AddItem "62:   "
    lstBox.AddItem "63:   "
    lstBox.AddItem "64:   "
    lstBox.AddItem "65:   "
    lstBox.AddItem "66:   "
    lstBox.AddItem "67:   "
    lstBox.AddItem "68:   "
    lstBox.AddItem "69:   "
'    If (FirstOn) Then
'        If (FM.IsFileThere(ResultFile)) Then
'            Kill ResultFile
'        End If
'        FileCopy TemplateDirectory + "Letters.txt", ResultFile
'        FileNum = FreeFile
'        Open ResultFile For Binary As #FileNum
'        Close #FileNum
'    Else
'        FileNum = FreeFile
'    End If
'    Open ResultFile For Append As #FileNum
    For i = 0 To lstBox.ListCount - 1
        q = InStrPS(lstBox.List(i), ":")
        If (q > 0) Then
        If Resultvalue = "" Then
            
             Resultvalue = Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q) & vbTab
             
             Else
                   
                Resultvalue = Resultvalue & Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q) & vbTab
            
             End If
'            Print #FileNum, Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q); vbTab;
        End If
    Next i
'    Print #FileNum, ""
'    Close #FileNum
    
      Call createresultfield(Resultfeild)
         resultstring = Resultfeild & vbCrLf & Resultvalue
    
    Set RetAppt = Nothing
    Set RetAct = Nothing
    Set RetPat = Nothing
    Set RetPrc = Nothing
    Set RetDr = Nothing
    If (LastOn) Then
        NewFile = Left(ResultFile, Len(ResultFile) - 4) + ".xls"
        ResultFile = resultstring & "@" & NewFile
'        If (FM.IsFileThere(NewFile)) Then
'            Kill NewFile
'        End If
'        Name ResultFile As NewFile
'        ResultFile = NewFile
    End If
    CreateDrugs = True
End If
End Function
Private Sub createresultfield(Resultfeild As String)

        Resultfeild = ":1:" & vbTab & ":2:" & vbTab & ":3:" & vbTab & ":4:" & vbTab & ":4a:" & vbTab & ":4b:" & vbTab & ":5:" & vbTab & ":5a:" & vbTab & ":6:" & vbTab & ":6a:" & vbTab & ":7:" & vbTab & ":7a:" & vbTab & ":8:" & vbTab & ":8a:" & vbTab & ":9:" & vbTab & ":9a:" & vbTab & ":9b:" & vbTab & ":10:" & vbTab & ":11:" & vbTab & ":11a:" & vbTab & ":11b:" & vbTab & ":12:" & vbTab & ":13:" & vbTab & ":14:" & vbTab & ":15:" & vbTab & ":16:" & vbTab & ":17:" & vbTab & ":18:" & vbTab & ":18a:" & vbTab & ":18b:" & vbTab & ":18c:" & vbTab & ":18d:" & vbTab & ":18e:" & vbTab & ":18f:" & vbTab & ":19:" & vbTab & ":20:" & vbTab & ":21:" & vbTab & ":22:" & vbTab & ":23:" & vbTab & ":24:" & vbTab & ":25:" & vbTab & ":26:" & vbTab & ":27:" & vbTab & ":28:" & vbTab & ":29:"
        Resultfeild = Resultfeild + vbTab & ":30:" & vbTab & ":31:" & vbTab & ":32:" & vbTab & ":33:" & vbTab & ":34:" & vbTab & ":35:" & vbTab & ":36:" & vbTab & ":37:" & vbTab & ":38:" & vbTab & ":39:" & vbTab & ":40:" & vbTab & ":41:" & vbTab & ":42:" & vbTab & ":43:" & vbTab & ":44:" & vbTab & ":45:" & vbTab & ":46:" & vbTab & ":47:" & vbTab & ":48:" & vbTab & ":49:" & vbTab & ":50:" & vbTab & ":51:" & vbTab & ":52:" & vbTab & ":53:" & vbTab & ":54:" & vbTab & ":55:" & vbTab & ":56:" & vbTab & ":57:" & vbTab & ":58:" & vbTab & ":59:" & vbTab & ":60:" & vbTab & ":61:" & vbTab & ":62:" & vbTab & ":63:" & vbTab & ":64:" & vbTab & ":65:" & vbTab & ":66:" & vbTab & ":67:" & vbTab & ":68:" & vbTab & ":69:" & vbTab & ":70:" & vbTab & ":70a:"
        Resultfeild = Resultfeild + vbTab & ":71:" & vbTab & ":71a:" & vbTab & ":72:" & vbTab & ":72a:" & vbTab & ":73:" & vbTab & ":73a:" & vbTab & ":74:" & vbTab & ":74a:" & vbTab & ":75:" & vbTab & ":75a:" & vbTab & ":76:" & vbTab & ":76a:" & vbTab & ":77:" & vbTab & ":77a:" & vbTab & ":78:" & vbTab & ":78a:" & vbTab & ":79:" & vbTab & ":79a:" & vbTab & ":80:" & vbTab & ":80a:" & vbTab & ":81:" & vbTab & ":81a:" & vbTab & ":82:" & vbTab & ":82a:" & vbTab & ":83:" & vbTab & ":83a:" & vbTab & ":84:" & vbTab & ":84a:" & vbTab & ":85:" & vbTab & ":85a:" & vbTab & ":86:" & vbTab & ":86a:" & vbTab & ":87:" & vbTab & ":87a:" & vbTab & ":88:" & vbTab & ":88a:" & vbTab & ":89:" & vbTab & ":89a:" & vbTab & ":90:" & vbTab & ":90a:" & vbTab & ":91:" & vbTab & ":91a:" & vbTab & ":92:" & vbTab & ":92a:"
        Resultfeild = Resultfeild + vbTab & ":93:" & vbTab & ":93a" & vbTab & ":94:" & vbTab & ":94a:" & vbTab & ":95:" & vbTab & ":95a:" & vbTab & ":96:" & vbTab & ":96a:" & vbTab & ":97:" & vbTab & ":97a:" & vbTab & ":98:" & vbTab & ":98a:" & vbTab & ":99:" & vbTab & ":99a:" & vbTab & ":100:" & vbTab & ":100a:" & vbTab & ":101:" & vbTab & ":101a" & vbTab & ":102:" & vbTab & ":102a:" & vbTab & ":103:" & vbTab & ":103a" & vbTab & ":104:" & vbTab & ":104a" & vbTab & ":105:" & vbTab & ":105a" & vbTab & ":106:" & vbTab & ":106a:" & vbTab & ":107:" & vbTab & ":107a" & vbTab & ":108:" & vbTab & ":108a:" & vbTab & ":109:" & vbTab & ":109a:" & vbTab & ":110:" & vbTab & ":110a:" & vbTab & ":111:" & vbTab & ":111a:" & vbTab & ":112:" & vbTab & ":112a:" & vbTab & ":113:" & vbTab & ":113a:" & vbTab & ":114:" & vbTab & ":114a:" & vbTab & ":115:" & vbTab & ":115a:" & vbTab & ":116:" & vbTab & ":116a:" & vbTab & ":117:" & vbTab & ":117a:" & vbTab & ":118:" & vbTab & ":118a:" & vbTab & ":119:" & vbTab & ":119a:"
        Resultfeild = Resultfeild + vbTab & ":120:" & vbTab & ":120a:" & vbTab & ":121:" & vbTab & ":121a:" & vbTab & ":122:" & vbTab & ":122a:" & vbTab & ":123:" & vbTab & ":123a:" & vbTab & ":124:" & vbTab & ":124a:" & vbTab & ":125:" & vbTab & ":125a:" & vbTab & ":126:" & vbTab & ":126a:" & vbTab & ":127:" & vbTab & ":127a:" & vbTab & ":128:" & vbTab & ":128a:" & vbTab & ":129:" & vbTab & ":129a:" & vbTab & ":130:" & vbTab & ":130a:" & vbTab & ":131:" & vbTab & ":131a:" & vbTab & ":132:" & vbTab & ":132a:" & vbTab & ":133:" & vbTab & ":133a:" & vbTab & ":134:" & vbTab & ":134a:" & vbTab & ":135:" & vbTab & ":136:" & vbTab & ":137:" & vbTab & ":138:" & vbTab & ":139:" & vbTab & ":140:" & vbTab & ":141:" & vbTab & ":142:" & vbTab & ":143:" & vbTab & ":144:" & vbTab & ":145:" & vbTab & ":146:"
        Resultfeild = Resultfeild + vbTab & ":147:" & vbTab & ":148:" & vbTab & ":149:" & vbTab & ":150:" & vbTab & ":151:" & vbTab & ":152:" & vbTab & ":153:" & vbTab & ":154:" & vbTab & ":155:" & vbTab & ":156:" & vbTab & ":157:" & vbTab & ":158:" & vbTab & ":159:" & vbTab & ":160:" & vbTab & ":161:" & vbTab & ":162:" & vbTab & ":163:" & vbTab & ":164:" & vbTab & ":165:" & vbTab & ":166:" & vbTab & ":167:" & vbTab & ":168:" & vbTab & ":169:" & vbTab & ":170:" & vbTab & ":171:" & vbTab & ":172:" & vbTab & ":173:"

End Sub

Private Function DoEyeWearPrint(Index As Integer, PatId As Long, ApptId As Long, TheType As String, FirstOn As Boolean, LastOn As Boolean) As Boolean
Dim p As Integer
Dim MyIndex As Integer
Dim ProceedOn As Boolean
Dim ThePrinter As String
Dim Lc As String, Lt As String
Dim ODRx As String, OSRx As String
Dim ANote As String, TempFile As String
Dim SigFile As String, TheFile As String
Dim CLOn As Boolean
ODRx = ""
OSRx = ""
ANote = ""
MyIndex = Index
If (PatId > 0) And (ApptId > 0) Then
    ThePrinter = GetPrinter(PRINTER_Wear)
    If (Index >= 0) Then
        Lc = ""
        Lt = ""
        CLOn = False
        If (TheType = "CL") Then
            CLOn = True
            MyIndex = MyIndex + 1
            Lt = Trim(Left(lstActions.List(MyIndex), 200))
            MyIndex = MyIndex + 1
            Lc = Trim(Left(lstActions.List(MyIndex), 200))
            MyIndex = MyIndex + 1
            ODRx = Trim(Left(lstActions.List(MyIndex), 190))
            If (Mid(lstActions.List(MyIndex), 199, 1) = "*") Then
                MyIndex = MyIndex + 1
                ODRx = ODRx + Trim(Left(lstActions.List(MyIndex), 190))
            End If
            MyIndex = MyIndex + 1
            OSRx = Trim(Left(lstActions.List(MyIndex), 190))
            If (Mid(lstActions.List(MyIndex), 199, 1) = "*") Then
                MyIndex = MyIndex + 1
                OSRx = OSRx + Trim(Left(lstActions.List(MyIndex), 190))
            End If
            MyIndex = MyIndex + 1
            If (Left(lstActions.List(MyIndex), 5) = "     ") Then
                ANote = Trim(Left(lstActions.List(MyIndex), 200))
            End If
            MyIndex = MyIndex + 1
            If (Left(lstActions.List(MyIndex), 5) = "     ") Then
                If (Trim(Left(lstActions.List(MyIndex), 200)) <> "0") Then
                    ANote = Trim(ANote) + " " + Trim(Left(lstActions.List(MyIndex), 200))
                End If
            End If
            MyIndex = MyIndex + 1
            If (Left(lstActions.List(MyIndex), 5) = "     ") Then
                If (Trim(Left(lstActions.List(MyIndex), 200)) <> "0") Then
                    ANote = Trim(ANote) + " " + Trim(Left(lstActions.List(MyIndex), 200))
                    Call ReplaceCharacters(ANote, vbCrLf, "-&-")
                    If (Mid(ANote, 1, 1) = "~") Then
                        ANote = "Order Trial:" + Mid(ANote, 2, Len(ANote) - 1)
                    ElseIf (Mid(ANote, 1, 1) = "*") Then
                        ANote = "Trial:" + Mid(ANote, 2, Len(ANote) - 1)
                    ElseIf (Mid(ANote, 1, 1) = "^") Then
                        ANote = Mid(ANote, 2, Len(ANote) - 1)
                    End If
                End If
            End If
            While (Trim(ANote) <> "") And (Left(lstActions.List(MyIndex + 1), 5) = "     ")
                MyIndex = MyIndex + 1
                ANote = Trim(ANote) + " " + Trim(Left(lstActions.List(MyIndex), 200))
                Call ReplaceCharacters(ANote, vbCrLf, "-&-")
            Wend
        Else
            MyIndex = MyIndex + 1
            Lt = Trim(Left(lstActions.List(MyIndex), 30))
            MyIndex = MyIndex + 1
            Lc = Trim(Left(lstActions.List(MyIndex), 30))
            MyIndex = MyIndex + 1
            ODRx = Trim(Left(lstActions.List(MyIndex), 190))
            If (Mid(lstActions.List(MyIndex), 199, 1) = "*") Then
                MyIndex = MyIndex + 1
                ODRx = ODRx + Trim(Left(lstActions.List(MyIndex), 190))
            End If
            MyIndex = MyIndex + 1
            OSRx = Trim(Left(lstActions.List(MyIndex), 190))
            If (Mid(lstActions.List(MyIndex), 199, 1) = "*") Then
                MyIndex = MyIndex + 1
                OSRx = OSRx + Trim(Left(lstActions.List(MyIndex), 190))
            End If
            MyIndex = MyIndex + 1
            If (Left(lstActions.List(MyIndex), 5) = "     ") Then
                ANote = Left(lstActions.List(MyIndex), 200)
            End If
        End If
        ProceedOn = True
        If (Trim(ANote) <> "") Then
            If (Left(ANote, 3) = "Trl") Then
                ProceedOn = False
            ElseIf (Left(ANote, 9) = "Order Trl") Then
                ProceedOn = False
            End If
        End If
        TheFile = DoctorInterfaceDirectory + "EyeWearRx" + "_" + Trim(str(ApptId)) + "_" + Trim(str(PatId)) + ".txt"
        If (FirstOn) Then
            If (FM.IsFileThere(TheFile)) Then
                Call KillDocumentProcessing(TheFile, True, True)
            End If
        End If
        TempFile = TemplateDirectory + "EyeWearRx.doc"
        If (CLOn) Then
            TempFile = TemplateDirectory + "CLRx.doc"
        End If
        If (ProceedOn) Then
            If (CreateEyeWear(PatientId, AppointmentId, ActivityId, TheFile, ODRx, OSRx, Lt, Lc, ANote, CLOn, True, True)) Then
                If (LastOn) Then
                    lblPrint.Visible = True
                    Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
' use a special signature file setup for Rxs
                    If (Trim(SigFile) <> "") Then
                        p = InStrPS(SigFile, ".")
                        If (p > 0) Then
                            SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
                        End If
                    End If
                    Call PrintTemplate(TheFile, TempFile, True, True, 1, ThePrinter, SigFile, 1, True)
                    lblPrint.Visible = False
                End If
            End If
        Else
            If (LastOn) Then
                lblPrint.Visible = True
                Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
' use a special signature file setup for Rxs
                If (Trim(SigFile) <> "") Then
                    p = InStrPS(SigFile, ".")
                    If (p > 0) Then
                        SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
                    End If
                End If
                If (FM.IsFileThere(TheFile)) Then
                    Call PrintTemplate(TheFile, TempFile, True, True, 1, ThePrinter, SigFile, 1, True)
                End If
                lblPrint.Visible = False
            End If
        End If
    End If
End If
End Function

Private Function CreateEyeWear(PatId As Long, ApptId As Long, ActId As Long, ResultFile As String, ODRx As String, OSRx As String, LensType As String, LensCompany As String, ANote As String, CLOn As Boolean, FirstOn As Boolean, LastOn As Boolean) As Boolean
Dim FileNum As Integer, Idx As Integer
Dim i As Integer, q As Integer
Dim u As Integer, w As Integer
Dim RscId1 As Long
Dim KeyWords(5)
Dim ZTemp As String
Dim TName As String, ATemp As String
Dim Temp As String, ADate As String
Dim NewFile As String, TempZip As String
Dim RetAct As PracticeActivity
Dim RetAppt As SchedulerAppointment
Dim RetDr As SchedulerResource
Dim RetPrc As PracticeName
Dim RetPat As Patient
Dim Resultfeild As String
Dim Resultvalue As String
Dim resultstring As String
ADate = ""
CreateEyeWear = False
lstBox.Clear
KeyWords(1) = "ADD-READING:"
KeyWords(2) = "ADD-INTERMEDIATE:"
KeyWords(3) = "PRISM OF ANGLE 1:"
KeyWords(4) = "PRISM OF ANGLE 2:"
KeyWords(5) = "Vertex dist:"
If (CLOn) Then
    KeyWords(1) = "ADD-READING:"
    KeyWords(2) = "BASE CURVE:"
    KeyWords(3) = "DIAMETER:"
    KeyWords(4) = "PERIPH CURVE:"
    KeyWords(5) = ""
End If
If (PatId > 0) And (ApptId > 0) Then
    If (InStrPS(ODRx, "ANGLE1") <> 0) Then
        Call ReplaceCharacters(ODRx, "ANGLE1", "ANGLE 1")
    ElseIf (InStrPS(ODRx, "ANGLE2") <> 0) Then
        Call ReplaceCharacters(ODRx, "ANGLE2", "ANGLE 2")
    End If
    If (InStrPS(OSRx, "ANGLE1") <> 0) Then
        Call ReplaceCharacters(OSRx, "ANGLE1", "ANGLE 1")
    ElseIf (InStrPS(OSRx, "ANGLE2") <> 0) Then
        Call ReplaceCharacters(OSRx, "ANGLE2", "ANGLE 2")
    End If
    Set RetPat = New Patient
    Set RetPrc = New PracticeName
    Set RetDr = New SchedulerResource
    Set RetAct = New PracticeActivity
    Set RetAppt = New SchedulerAppointment
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
    
    End If
    RscId1 = 0
    If (ActId > 0) Then
        RetAct.ActivityId = ActId
        If (RetAct.RetrieveActivity) Then
            If (RetAct.ActivityCurrResId > 0) Then
                RetDr.ResourceId = RetAct.ActivityCurrResId
                If (RetDr.RetrieveSchedulerResource) Then
                    If (Trim(RetDr.ResourceSignatureFile) <> "") Then
                        RscId1 = RetDr.ResourceId
                    End If
                End If
            End If
        End If
    End If
    If (RscId1 < 1) Then
        RetAppt.AppointmentId = ApptId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            If (RetAppt.AppointmentResourceId1 > 0) Then
                RetDr.ResourceId = RetAppt.AppointmentResourceId1
                If (RetDr.RetrieveSchedulerResource) Then
                
                End If
            End If
        End If
    End If
' Load the Merge File
    lstBox.Clear
    If (RetDr.ResourceId > 0) Then
        lstBox.AddItem "01:" + Trim(RetDr.ResourceDescription)
        lstBox.AddItem "02:" + Trim(RetDr.ResourceLic)
        If (RetDr.PracticeId > 0) Then
            RetPrc.PracticeId = RetDr.PracticeId
            If (RetPrc.RetrievePracticeName) Then
                lstBox.AddItem "03:" + Trim(RetPrc.PracticeAddress)
                TempZip = ""
                Call DisplayPhone(RetPrc.PracticeZip, TempZip)
                Temp = Trim(RetPrc.PracticeCity) + ", " + RetPrc.PracticeState + " " + TempZip
                lstBox.AddItem "04:" + Temp
                TempZip = ""
                Call DisplayPhone(RetPrc.PracticePhone, TempZip)
                lstBox.AddItem "05:" + TempZip
            Else
                lstBox.AddItem "03:    "
                lstBox.AddItem "04:    "
                lstBox.AddItem "05:    "
            End If
        Else
            lstBox.AddItem "03:    "
            lstBox.AddItem "04:    "
            lstBox.AddItem "05     "
        End If
    Else
        lstBox.AddItem "01:    "
        lstBox.AddItem "02:    "
        lstBox.AddItem "03:    "
        lstBox.AddItem "04:    "
        lstBox.AddItem "05:    "
    End If
    Call FormatTodaysDate(ADate, False)
    lstBox.AddItem "06:" + ADate
    Temp = Trim(Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial) + " " + Trim(RetPat.LastName) + " " + Trim(RetPat.NameRef))
    Call StripCharacters(Temp, Chr(34))
    lstBox.AddItem "07:" + Temp
    lstBox.AddItem "04a:" + Trim(RetPat.Address)
    Call DisplayZip(RetPat.Zip, Temp)
    lstBox.AddItem "04b:" + Trim(RetPat.City) + ", " + Trim(RetPat.State) + " " + Trim(Temp)
' Parse and Place the Od/Os Rx fields
    If (Left(ODRx, 2) = "OD") Then
        ODRx = Trim(Mid(ODRx, 4, Len(ODRx) - 3))
    End If
    q = InStrPS(ODRx, " ")
    If (q > 0) Then
        Temp = Trim(Left(ODRx, q))
        If (Temp = "NE") Then
            Temp = "    "
        End If
        Call ConvertOldZeros(Temp)
        w = InStrPS(q + 1, ODRx, " ")
        If (w > 0) Then
            TName = Mid(ODRx, q + 1, (w - 1) - q)
            If (UCase(TName) = "BALANCE") Then
                Temp = Temp + " " + Trim(TName) + " LENS"
                q = w + 5
            End If
        End If
        lstBox.AddItem "07a:" + Temp
    ElseIf (Trim(ODRx) <> "") Then
        Temp = Trim(ODRx)
        Call ConvertOldZeros(Temp)
        lstBox.AddItem "07a:" + Temp
    Else
        lstBox.AddItem "07a:    "
    End If
    i = InStrPS(q + 1, ODRx, " ")
    If (i > 0) Then
        Temp = Trim(Mid(ODRx, q + 1, i - q))
        If (Temp = "NE") Then
            Temp = "    "
        End If
        If (UCase(Left(Temp, 3)) <> "ADD") And (UCase(Left(Temp, 3)) <> "PRI") And (UCase(Left(Temp, 3)) <> "BAS") And (UCase(Left(Temp, 3)) <> "DIA") And (UCase(Left(Temp, 1)) <> "X") Then
            Call ConvertOldZeros(Temp)
            lstBox.AddItem "08:" + Temp
        Else
            lstBox.AddItem "08:    "
        End If
    Else
        TName = "  "
        w = InStrPS(q + 1, UCase(ODRx), "X")
        If (w > q) Then
            TName = Mid(ODRx, q + 1, (w - 1) - q)
        End If
        lstBox.AddItem "08:" + TName
    End If
    q = InStrPS(ODRx, "X")
    If (q < 1) Then
        q = InStrPS(ODRx, "x")
    End If
    If (q > 0) Then
        i = InStrPS(q, ODRx, " ")
        If (i > 0) Then
            Temp = Trim(Mid(ODRx, q, i - (q - 1)))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            Call ConvertOldZeros(Temp)
            If (UCase(Temp) = "X") Then
                lstBox.AddItem "08a:    "
            Else
                lstBox.AddItem "08a:" + Temp
            End If
        Else
            Temp = Trim(Mid(ODRx, q, Len(ODRx) - (q - 1)))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            Call ConvertOldZeros(Temp)
            If (UCase(Temp) = "X") Then
                lstBox.AddItem "08a:    "
            Else
                lstBox.AddItem "08a:" + Temp
            End If
        End If
    Else
        lstBox.AddItem "08a:   "
    End If
    
' OD Data
    Idx = 1
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "16:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "16:" + ATemp
    End If
    
    Idx = 2
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "09:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "09:" + ATemp
    End If
    
    Idx = 3
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "09a:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "09a:" + ATemp
    End If
    
    Idx = 4
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "17:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "17:" + ATemp
    End If
    lstBox.AddItem "09b:    "
    lstBox.AddItem "10:    "

    Idx = 5
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "23:   "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "23:" + ATemp
    End If

' OS Data
    If (Left(OSRx, 2) = "OS") Then
        OSRx = Trim(Mid(OSRx, 4, Len(OSRx) - 3))
    End If
    q = InStrPS(OSRx, " ")
    If (q > 0) Then
        Temp = Trim(Left(OSRx, q))
        If (Temp = "NE") Then
            Temp = "    "
        End If
        Call ConvertOldZeros(Temp)
        w = InStrPS(q + 1, OSRx, " ")
        If (w > 0) Then
            TName = Mid(OSRx, q + 1, (w - 1) - q)
            If (UCase(TName) = "BALANCE") Then
                Temp = Temp + " " + Trim(TName) + " LENS"
                q = w + 5
            End If
        End If
        lstBox.AddItem "11:" + Temp
    ElseIf (Trim(OSRx) <> "") Then
        Temp = Trim(OSRx)
        Call ConvertOldZeros(Temp)
        lstBox.AddItem "11:" + Temp
    Else
        lstBox.AddItem "11:    "
    End If
    i = InStrPS(q + 1, OSRx, " ")
    If (i > 0) Then
        Temp = Trim(Mid(OSRx, q + 1, i - q))
        If (Temp = "NE") Then
            Temp = "    "
        End If
        If (UCase(Left(Temp, 3)) <> "ADD") And (UCase(Left(Temp, 3)) <> "PRI") And (UCase(Left(Temp, 3)) <> "BAS") And (UCase(Left(Temp, 3)) <> "DIA") And (UCase(Left(Temp, 1)) <> "X") Then
            Call ConvertOldZeros(Temp)
            lstBox.AddItem "11a:" + Temp
        Else
            lstBox.AddItem "11a:    "
        End If
    Else
        TName = "  "
        w = InStrPS(q + 1, UCase(OSRx), "X")
        If (w > q) Then
            TName = Mid(OSRx, q + 1, (w - 1) - q)
        End If
        lstBox.AddItem "11a:" + TName
    End If
    q = InStrPS(OSRx, "X")
    If (q < 1) Then
        q = InStrPS(OSRx, "x")
    End If
    If (q > 0) Then
        i = InStrPS(q, OSRx, " ")
        If (i > 0) Then
            Temp = Trim(Mid(OSRx, q, i - (q - 1)))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            If (UCase(Temp) = "X") Then
                lstBox.AddItem "11b:    "
            Else
                lstBox.AddItem "11b:" + Temp
            End If
        Else
            Temp = Trim(Mid(OSRx, q, Len(OSRx) - (q - 1)))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            Call ConvertOldZeros(Temp)
            If (UCase(Temp) = "X") Then
                lstBox.AddItem "11b:    "
            Else
                lstBox.AddItem "11b:" + Temp
            End If
        End If
    Else
        lstBox.AddItem "11b:   "
    End If
    
    Idx = 1
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "18:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "18:" + ATemp
    End If
    
    Idx = 2
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "12:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "12:" + ATemp
    End If
    
    Idx = 3
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "13:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "13:" + ATemp
    End If
    
    Idx = 4
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "19:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "19:" + ATemp
    End If
    
    Idx = 5
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "24:   "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "24:" + ATemp
    End If
    
    lstBox.AddItem "14:    "
    lstBox.AddItem "15:    "
    lstBox.AddItem "18a:    "
    lstBox.AddItem "18b:    "
    lstBox.AddItem "18c:    "
    lstBox.AddItem "18d:    "
    lstBox.AddItem "18e:    "
    If (Trim(LensType) <> "") And (UCase(Trim(LensType)) <> "NONE") Then
        lstBox.AddItem "20:" + Trim(LensType)
    Else
        lstBox.AddItem "20:    "
    End If
        
    If (Trim(LensCompany) <> "") And (UCase(Trim(LensCompany)) <> "NONE") Then
        lstBox.AddItem "21:" + Trim(LensCompany)
    Else
        lstBox.AddItem "21:    "
    End If
    If (Trim(ANote) <> "") Then
        lstBox.AddItem "22:" + Trim(ANote)
    Else
        lstBox.AddItem "22:   "
    End If
    If (Trim(RetPat.BirthDate) <> "") Then
        lstBox.AddItem "25:" + Mid(RetPat.BirthDate, 5, 2) + "/" + Mid(RetPat.BirthDate, 7, 2) + "/" + Left(RetPat.BirthDate, 4)
    Else
        lstBox.AddItem "25:    "
    End If
' Add the appointment date
    ADate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
    lstBox.AddItem "26:" + ADate
    
    lstBox.AddItem "27:" & PatId
    
    lstBox.AddItem "28:   "
    lstBox.AddItem "29:   "
    lstBox.AddItem "30:   "
    lstBox.AddItem "31:   "
    lstBox.AddItem "32:   "
    lstBox.AddItem "33:   "
    lstBox.AddItem "34:   "
    lstBox.AddItem "35:   "
    lstBox.AddItem "36:   "
    lstBox.AddItem "37:   "
    lstBox.AddItem "38:   "
    lstBox.AddItem "39:   "
    lstBox.AddItem "40:   "
    lstBox.AddItem "41:   "
    lstBox.AddItem "42:   "
    lstBox.AddItem "43:   "
    lstBox.AddItem "44:   "
    lstBox.AddItem "45:   "
    lstBox.AddItem "46:   "
    lstBox.AddItem "47:   "
    lstBox.AddItem "48:   "
    lstBox.AddItem "49:   "
    lstBox.AddItem "50:   "
    lstBox.AddItem "51:   "
    lstBox.AddItem "52:   "
    lstBox.AddItem "53:   "
    lstBox.AddItem "54:   "
    lstBox.AddItem "55:   "
    lstBox.AddItem "56:   "
    lstBox.AddItem "57:   "
    lstBox.AddItem "58:   "
    lstBox.AddItem "59:   "
    lstBox.AddItem "60:   "
    lstBox.AddItem "61:   "
    lstBox.AddItem "62:   "
    lstBox.AddItem "63:   "
    lstBox.AddItem "64:   "
    lstBox.AddItem "65:   "
    lstBox.AddItem "66:   "
    lstBox.AddItem "67:   "
    lstBox.AddItem "68:   "
    lstBox.AddItem "69:   "
    lstBox.AddItem "05a:    "
    lstBox.AddItem "06a:    "
    lstBox.AddItem "18f:    "
    'If (FirstOn) Then
'        If (FM.IsFileThere(ResultFile)) Then
'            Kill ResultFile
'        End If
'        FileCopy TemplateDirectory + "Letters.txt", ResultFile
'        FileNum = FreeFile
'        Open ResultFile For Binary As #FileNum
'        Close #FileNum
'    Else
'        FileNum = FreeFile
'    End If
'    Open ResultFile For Append As #FileNum
    For i = 0 To lstBox.ListCount - 1
        q = InStrPS(lstBox.List(i), ":")
        If (q > 0) Then
         If Resultvalue = "" Then
            
             Resultvalue = Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q) & vbTab
             
             Else
                   
                Resultvalue = Resultvalue & Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q) & vbTab
            
'            Print #FileNum, Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q); vbTab;
        End If
        End If
    Next i
'    Print #FileNum, ""
'    Close #FileNum
      Call createresultfield(Resultfeild)
         resultstring = Resultfeild & vbCrLf & Resultvalue
    Set RetAct = Nothing
    Set RetAppt = Nothing
    Set RetPat = Nothing
    Set RetPrc = Nothing
    Set RetDr = Nothing
    If (LastOn) Then
        NewFile = Left(ResultFile, Len(ResultFile) - 4) + ".xls"
'        If (FM.IsFileThere(NewFile)) Then
'            Kill NewFile
'        End If
'        Name ResultFile As NewFile
'        ResultFile = NewFile
    ResultFile = resultstring & "@" & NewFile
    End If
    CreateEyeWear = True
End If
Exit Function
GetField:
    ATemp = ""
    If (Idx > 0) Then
        If (KeyWords(Idx) <> "") Then
            i = InStrPS(UCase(Temp), UCase(KeyWords(Idx)))
            If (i > 0) Then
                GoSub PutEnd
                If (u > 0) Then
                    ATemp = Mid(Temp, i, (u - 1) - (i - 1))
                Else
                    ATemp = Mid(Temp, i, Len(Temp) - (i - 1))
                End If
            End If
        End If
        If (ATemp = "") Then
            ZTemp = KeyWords(Idx)
            Call StripCharacters(ZTemp, " ")
            If (ZTemp <> "") Then
                i = InStrPS(UCase(Temp), UCase(ZTemp))
                If (i > 0) Then
                    GoSub PutEnd
                    If (u > 0) Then
                        ATemp = Mid(Temp, i, (u - 1) - (i - 1))
                    Else
                        ATemp = Mid(Temp, i, Len(Temp) - (i - 1))
                    End If
                End If
            End If
        End If
    End If
    If (Trim(ATemp) <> "") Then
        i = InStrPS(ATemp, ":")
        If (i > 0) Then
            ATemp = Trim(Mid(ATemp, i + 1, Len(ATemp) - i))
        End If
        Call ConvertOldZeros(ATemp)
    End If
    Return
PutEnd:
    u = 0
    For w = Idx + 1 To 5
        If (Trim(KeyWords(w)) <> "") Then
            ZTemp = KeyWords(w)
            u = InStrPS(UCase(Temp), UCase(ZTemp))
            If (u > 0) Then
                Exit For
            End If
        End If
    Next w
    If (u = 0) Then
        For w = Idx + 1 To 5
            ZTemp = KeyWords(w)
            Call StripCharacters(ZTemp, " ")
            If (Trim(ZTemp) <> "") Then
                u = InStrPS(UCase(Temp), UCase(ZTemp))
                If (u > 0) Then
                    Exit For
                End If
            End If
        Next w
    End If
    Return
End Function

Private Function AddedDiagnosis() As Boolean
Dim Found As Boolean
Dim p As Integer
Dim i As Integer
Dim OtherText As String
Dim TheText As String, TheSystem As String
Dim RQuan As String
Dim RSys As String, RText As String
Dim LQuan As String
Dim LSys As String, LText As String
Dim RightDiagnosis As String, LeftDiagnosis As String
Dim DisplayText As String
p = 0
i = 1
While (frmSystems1.GetSystemDiagnosis(i, RSys, RightDiagnosis, RText, RQuan, LSys, LeftDiagnosis, LText, LQuan))
    If (Trim(RightDiagnosis) <> "") And (Left(RightDiagnosis, 2) <> "N-") Then
        OtherText = ""
        If (Left(RightDiagnosis, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Mid(RightDiagnosis, 2, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Left(RightDiagnosis, 1) + Mid(RightDiagnosis, 3, Len(RightDiagnosis) - 2)
        End If
        If (Mid(RightDiagnosis, 3, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Left(RightDiagnosis, 2) + Mid(RightDiagnosis, 4, Len(RightDiagnosis) - 3)
        End If
        If (Left(RightDiagnosis, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Mid(RightDiagnosis, 2, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Left(RightDiagnosis, 1) + Mid(RightDiagnosis, 3, Len(RightDiagnosis) - 2)
        End If
        If (Mid(RightDiagnosis, 3, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Left(RightDiagnosis, 2) + Mid(RightDiagnosis, 4, Len(RightDiagnosis) - 3)
        End If
        If (Left(RightDiagnosis, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Mid(RightDiagnosis, 2, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Left(RightDiagnosis, 1) + Mid(RightDiagnosis, 3, Len(RightDiagnosis) - 2)
        End If
        If (Mid(RightDiagnosis, 3, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Left(RightDiagnosis, 2) + Mid(RightDiagnosis, 4, Len(RightDiagnosis) - 3)
        End If
        OtherText = OtherText + Trim(RQuan)
        Found = False

        If Not (Found) Then
            DisplayText = Space(75)
            TheSystem = ""
            TheText = ""
            If (GetDiagnosis(TheSystem, RightDiagnosis, TheText, 0)) Then
                Mid(DisplayText, 1, Len(TheText) + 4) = "OD: " + OtherText + TheText
                DisplayText = DisplayText + Trim(str(0))
                lstFindingsNow.AddItem DisplayText
            End If
        End If
    End If
    If (Trim(LeftDiagnosis) <> "") And (Left(LeftDiagnosis, 2) <> "N-") Then
        OtherText = ""
        If (Left(LeftDiagnosis, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Mid(LeftDiagnosis, 2, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Left(LeftDiagnosis, 1) + Mid(LeftDiagnosis, 3, Len(LeftDiagnosis) - 2)
        End If
        If (Mid(LeftDiagnosis, 3, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Left(LeftDiagnosis, 2) + Mid(LeftDiagnosis, 4, Len(LeftDiagnosis) - 3)
        End If
        If (Left(LeftDiagnosis, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Mid(LeftDiagnosis, 2, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Left(LeftDiagnosis, 1) + Mid(LeftDiagnosis, 3, Len(LeftDiagnosis) - 2)
        End If
        If (Mid(LeftDiagnosis, 3, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Left(LeftDiagnosis, 2) + Mid(LeftDiagnosis, 4, Len(LeftDiagnosis) - 3)
        End If
        If (Left(LeftDiagnosis, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Mid(LeftDiagnosis, 2, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Left(LeftDiagnosis, 1) + Mid(LeftDiagnosis, 3, Len(LeftDiagnosis) - 2)
        End If
        If (Mid(LeftDiagnosis, 3, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Left(LeftDiagnosis, 2) + Mid(LeftDiagnosis, 4, Len(LeftDiagnosis) - 3)
        End If
        OtherText = OtherText + Trim(LQuan)
        Found = False

        If Not (Found) Then
            DisplayText = Space(75)
            TheSystem = ""
            TheText = ""
            If (GetDiagnosis(TheSystem, LeftDiagnosis, TheText, 0)) Then
                Mid(DisplayText, 1, Len(TheText) + 4) = "OS: " + TheText
                DisplayText = DisplayText + Trim(str(0))
                lstFindingsNow.AddItem DisplayText
            End If
        End If
    End If
    i = i + 1
Wend
End Function

Private Function IsTestFollowup(AName As String) As Boolean
Dim i As Integer
Dim RetCode As PracticeCodes
IsTestFollowup = False
If (Trim(AName) <> "") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "EXTERNAL TESTS"
    RetCode.ReferenceCode = ""
    If (RetCode.FindCode > 0) Then
        i = 1
        Do Until Not (RetCode.SelectCode(i))
            If (InStrPS(RetCode.ReferenceCode, AName) <> 0) Then
                If (Trim(RetCode.FollowUp) = "F") Then
                    IsTestFollowup = True
                End If
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetCode = Nothing
End If
End Function

Private Function ApplSetNewTransaction(TransId As Long, TransType As String, TheDate As String, Ref As String, Remark As String, VndId As Long) As Boolean
Dim RetrieveTrans As PracticeTransactionJournal
Dim oLocalDate As New CIODateTime
oLocalDate.SetDateUnknown TheDate
Set RetrieveTrans = New PracticeTransactionJournal
RetrieveTrans.TransactionJournalId = 0
If (RetrieveTrans.RetrieveTransactionJournal) Then
    RetrieveTrans.TransactionJournalType = TransType
    RetrieveTrans.TransactionJournalTypeId = TransId
    RetrieveTrans.TransactionJournalStatus = "P"
    RetrieveTrans.TransactionJournalAction = "B"
    RetrieveTrans.TransactionJournalDate = oLocalDate.GetIODate
    RetrieveTrans.TransactionJournalTime = 0
    RetrieveTrans.TransactionJournalReference = Ref
    RetrieveTrans.TransactionJournalRemark = Remark
    RetrieveTrans.TransactionJournalRcvrId = VndId
    Call RetrieveTrans.ApplyTransactionJournal
End If
Set RetrieveTrans = Nothing
Set oLocalDate = Nothing
ApplSetNewTransaction = True
End Function

Private Function IsTestApptTypeSet(ARef As String, AName As String, AType As String) As Boolean
Dim i As Integer
Dim RetCode As PracticeCodes
IsTestApptTypeSet = False
AType = ""
If (Trim(ARef) <> "") And (Trim(AName) <> "") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = UCase(Trim(ARef))
    If (RetCode.FindCode > 0) Then
        i = 1
        Do Until Not (RetCode.SelectCode(i))
            If (InStrPS(RetCode.ReferenceCode, AName) <> 0) Then
                If (Trim(RetCode.ReferenceAlternateCode) <> "") Then
                    IsTestApptTypeSet = True
                    AType = Trim(RetCode.ReferenceAlternateCode)
                    If (Len(AType) < 2) Then
                        IsTestApptTypeSet = False
                        AType = ""
                    End If
                End If
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetCode = Nothing
End If
End Function

Private Sub lstZoom_Click()
lstZoom.Visible = False
lstZoom.Clear
End Sub
Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
End Function
