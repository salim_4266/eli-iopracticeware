VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{4BD5A3A1-7FFE-11D4-A13A-004005FA6275}#1.0#0"; "ImagXpr6.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmHighlights 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9840
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12705
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "Highlights.frx":0000
   ScaleHeight     =   9840
   ScaleWidth      =   12705
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdGlaucoma 
      Height          =   855
      Left            =   10920
      TabIndex        =   43
      ToolTipText     =   "Glaucoma Monitor"
      Top             =   7080
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":36C9
   End
   Begin VB.ListBox lstLinkNotes 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   4350
      ItemData        =   "Highlights.frx":38A7
      Left            =   3960
      List            =   "Highlights.frx":38A9
      TabIndex        =   21
      Top             =   3360
      Visible         =   0   'False
      Width           =   7815
   End
   Begin VB.ListBox lstHist 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5550
      ItemData        =   "Highlights.frx":38AB
      Left            =   2520
      List            =   "Highlights.frx":38AD
      TabIndex        =   10
      Top             =   1800
      Visible         =   0   'False
      Width           =   8055
   End
   Begin VB.ListBox lstImpr 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5550
      ItemData        =   "Highlights.frx":38AF
      Left            =   2520
      List            =   "Highlights.frx":38B1
      TabIndex        =   22
      Top             =   1440
      Visible         =   0   'False
      Width           =   7815
   End
   Begin VB.ListBox lstPrevDrs 
      Appearance      =   0  'Flat
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5550
      ItemData        =   "Highlights.frx":38B3
      Left            =   2520
      List            =   "Highlights.frx":38B5
      TabIndex        =   18
      Top             =   1440
      Visible         =   0   'False
      Width           =   7815
   End
   Begin MSComctlLib.ListView lstHistCTests1 
      Height          =   5055
      Left            =   360
      TabIndex        =   41
      Top             =   1680
      Visible         =   0   'False
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   8916
      View            =   3
      LabelEdit       =   1
      SortOrder       =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   15723490
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   13
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnIds"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   1746
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "OD VCC Dist"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "OS VCC Dist"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "OD VCC Pin"
         Object.Width           =   1852
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "OS VCC Pin"
         Object.Width           =   1852
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "OD VSC Dist"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "OS VSC Dist"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "OD VSC Pin"
         Object.Width           =   1852
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "OS VSC Pin"
         Object.Width           =   1852
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "OD IOP"
         Object.Width           =   1556
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "OS IOP"
         Object.Width           =   1556
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "ClnIds"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstAllergies 
      Height          =   3735
      Left            =   2880
      TabIndex        =   36
      Top             =   1560
      Visible         =   0   'False
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   6588
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12648447
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Allergies"
         Object.Width           =   7320
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Doctor"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Ref"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstCTests 
      Height          =   855
      Left            =   6360
      TabIndex        =   20
      Top             =   120
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   1508
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "VCC Dist"
         Object.Width           =   1711
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "VCC Pin"
         Object.Width           =   1711
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "VSC Dist"
         Object.Width           =   1711
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "VSC Pin"
         Object.Width           =   1711
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "IOP"
         Object.Width           =   1658
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   855
      Left            =   10680
      TabIndex        =   0
      Top             =   8040
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":38B7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   855
      Left            =   8520
      TabIndex        =   1
      Top             =   8040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":3A92
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   855
      Left            =   120
      TabIndex        =   2
      Top             =   8040
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":3C6E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDocs 
      Height          =   855
      Left            =   2520
      TabIndex        =   3
      Top             =   8040
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":3E49
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAlert 
      Height          =   855
      Left            =   1440
      TabIndex        =   8
      Top             =   8040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":4029
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLink 
      Height          =   855
      Left            =   6240
      TabIndex        =   17
      Top             =   8040
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":420F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpr 
      Height          =   855
      Left            =   3840
      TabIndex        =   19
      Top             =   8040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":43F3
   End
   Begin MSComctlLib.ListView lstProcsLV 
      Height          =   1455
      Left            =   6360
      TabIndex        =   24
      Top             =   2400
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   2566
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Completed Procedures"
         Object.Width           =   4673
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Doctor"
         Object.Width           =   1711
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Ref"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstDxLV 
      Height          =   1335
      Left            =   120
      TabIndex        =   27
      Top             =   5520
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   2355
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Impressions, Last Visit"
         Object.Width           =   7320
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "BI"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstOcuMeds 
      Height          =   1215
      Left            =   120
      TabIndex        =   25
      Top             =   4320
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   2143
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Status"
         Object.Width           =   1323
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Ocular Medication"
         Object.Width           =   4145
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Freq"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Width           =   0
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReset 
      Height          =   855
      Left            =   7440
      TabIndex        =   28
      Top             =   8040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":45D6
   End
   Begin MSComctlLib.ListView lstOTestsLV 
      Height          =   1455
      Left            =   6360
      TabIndex        =   23
      Top             =   960
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   2566
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Completed Tests"
         Object.Width           =   4673
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Doctor"
         Object.Width           =   1711
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Ref"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstOTestsOrgLV 
      Height          =   1455
      Left            =   6360
      TabIndex        =   29
      Top             =   960
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   2566
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FlatScrollBar   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Completed Tests"
         Object.Width           =   5203
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Doctor"
         Object.Width           =   1711
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Ref"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstSurg 
      Height          =   855
      Left            =   6360
      TabIndex        =   30
      Top             =   3840
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   1508
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Date"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "External Procedures"
         Object.Width           =   4674
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Doctor"
         Object.Width           =   1711
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstExamLV 
      Height          =   1095
      Left            =   120
      TabIndex        =   32
      Top             =   6840
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   1931
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Other Highlights"
         Object.Width           =   7320
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Doctor"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Ref"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin IMAGXPR6LibCtl.ImagXpress FxImage2 
      Height          =   1695
      Left            =   120
      TabIndex        =   33
      Top             =   1200
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   2990
      ErrStr          =   "MDYC0090GEP-0B3060SXEP"
      ErrCode         =   223363867
      ErrInfo         =   -630001961
      Persistence     =   -1  'True
      _cx             =   1
      _cy             =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   16777215
      AutoSize        =   4
      BorderType      =   2
      ScrollBarLargeChangeH=   10
      ScrollBarSmallChangeH=   1
      OLEDropMode     =   0
      ScrollBarLargeChangeV=   10
      ScrollBarSmallChangeV=   1
      DisplayProgressive=   -1  'True
      SaveTIFByteOrder=   0
      LoadRotated     =   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
   End
   Begin IMAGXPR6LibCtl.ImagXpress FxImage1 
      Height          =   3255
      Left            =   6360
      TabIndex        =   26
      Top             =   4680
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   5741
      ErrStr          =   "MDYC0090GEP-0B3060SXEP"
      ErrCode         =   223363867
      ErrInfo         =   -630001961
      Persistence     =   -1  'True
      _cx             =   1
      _cy             =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   16777215
      AutoSize        =   4
      BorderType      =   2
      ScrollBarLargeChangeH=   10
      ScrollBarSmallChangeH=   1
      OLEDropMode     =   0
      ScrollBarLargeChangeV=   10
      ScrollBarSmallChangeV=   1
      DisplayProgressive=   -1  'True
      SaveTIFByteOrder=   0
      LoadRotated     =   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      Begin VB.Label lblDrawDate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         Height          =   255
         Left            =   4200
         TabIndex        =   35
         Top             =   120
         Visible         =   0   'False
         Width           =   1215
      End
   End
   Begin VB.ListBox lstSort1 
      Height          =   450
      ItemData        =   "Highlights.frx":47BA
      Left            =   240
      List            =   "Highlights.frx":47BC
      Sorted          =   -1  'True
      TabIndex        =   13
      Top             =   1920
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.ListBox lstSort 
      Height          =   450
      ItemData        =   "Highlights.frx":47BE
      Left            =   240
      List            =   "Highlights.frx":47C0
      Sorted          =   -1  'True
      TabIndex        =   12
      Top             =   2400
      Visible         =   0   'False
      Width           =   1935
   End
   Begin MSComctlLib.ListView lstOrdersLV 
      Height          =   1455
      Left            =   120
      TabIndex        =   31
      Top             =   2880
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   2566
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Eye"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Pending Plan"
         Object.Width           =   7320
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Doctor"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Ref"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAlg 
      Height          =   855
      Left            =   4920
      TabIndex        =   37
      Top             =   8040
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":47C2
   End
   Begin MSComctlLib.ListView lstHPILV 
      Height          =   1695
      Left            =   2160
      TabIndex        =   34
      Top             =   1200
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   2990
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   7
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Eye"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "HPI (Click in HPI box for past HPIs)"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Doctor"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Ref"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRefract 
      Height          =   855
      Left            =   9600
      TabIndex        =   42
      Top             =   8040
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Highlights.frx":49A2
   End
   Begin VB.Label lblApptType 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   3480
      TabIndex        =   6
      Top             =   120
      Width           =   1845
   End
   Begin VB.Label lblSex 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   3240
      TabIndex        =   40
      Top             =   120
      Width           =   285
   End
   Begin VB.Label lblAge 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   2880
      TabIndex        =   39
      Top             =   120
      Width           =   405
   End
   Begin VB.Label lblAlert 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      Caption         =   "Alert ON"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   3600
      TabIndex        =   11
      Top             =   960
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblLang 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   5280
      TabIndex        =   38
      Top             =   120
      Width           =   1005
   End
   Begin VB.Label lblRefDr 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3480
      TabIndex        =   9
      Top             =   480
      Width           =   2805
   End
   Begin VB.Label txtCC 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   840
      Width           =   3405
   End
   Begin VB.Label lblPat 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   2805
   End
   Begin VB.Label lblPostOp 
      Alignment       =   2  'Center
      BackColor       =   &H00FF0000&
      Caption         =   "In Post-Op period"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4560
      TabIndex        =   16
      Top             =   960
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Other Highlights"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFF00&
      Height          =   210
      Left            =   120
      TabIndex        =   15
      Top             =   6840
      Visible         =   0   'False
      Width           =   1140
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Completed Procedures"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFF00&
      Height          =   210
      Left            =   6360
      TabIndex        =   14
      Top             =   2400
      Width           =   1635
   End
   Begin VB.Label lblLDate 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   3405
   End
End
Attribute VB_Name = "frmHighlights"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public HighlightLoadOn As Boolean
Public QuitOn As Boolean
Public DisplayBasis As Boolean

Private FirstTimeIn As Boolean
Private BackDoorOn As Boolean
Private CurrentOn As Boolean
Private PatientId As Long
Private PrevApptId As Long
Private AppointmentId As Long
Private ActivityId As Long
Private ActiveDate As String
Private TriggerOn As Boolean
Private RefDr As Long

Private Const QntRef As String = "["
Private Const QntRefEnd As String = "]"
Private Const CntRef As String = "-NF"
Private Const CntRefSet As String = "!"
Private Const RORef As String = "-RO"
Private Const RORefSet As String = "#"
Private Const HORef As String = "-HO"
Private Const HORefSet As String = "%"
Private Const MaxHistoryHPI As Integer = 200 'Was 30
Private Const MaxHistoryTests As Integer = 200 'Was 30
Private Const MaxTestResults As Integer = 500 'Was 100
Private Const MaxHistoryTestsDisplay As Integer = 50 'was 10
Private Type TestDetails
    TestDate As String
    TestTime As String
    TestParty As String
    TestText As String
    TestDoc As String
    TestQuestion As String
    TestOrderNum As String
    TestMove As Boolean
    TestNoteOn As Boolean
    TestNote As String
    TestApptId As Long
    TestClnId As Long
    TestHighlightOn As Boolean
    TestStatus As String * 1
    TestLocalStatus As String * 1
End Type
Private TestResultsIndex As Integer
Private TestResultsStorage(MaxTestResults) As TestDetails
Private QTestResultsIndex As Integer
Private QTestResultsStorage(MaxTestResults) As TestDetails
Private ReferenceTest As TestDetails
Private IOPResultsStorage As TestDetails
Private VCCResultsStorage As TestDetails
Private VSCResultsStorage As TestDetails
Private HistResultsIndex1 As Integer
Private HistResultsIndex2 As Integer
Private HistResultsIndex3 As Integer
Private HistResultsStorage(MaxHistoryTests) As TestDetails
Private HistHPIComplaints(MaxHistoryHPI) As String
Private RetCln As DI_ExamClinical
Private Sub cmdAlert_Click()
If Not UserLogin.HasPermission(epPatientNotes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
frmNotes.NoteId = 0
frmNotes.EyeContext = ""
frmNotes.NoteOn = False
frmNotes.SystemReference = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.MaintainOn = True
frmNotes.SetTo = "P"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Private Sub cmdAlg_Click()
If Not UserLogin.HasPermission(epHistoryAndAllergies) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
If (lstAllergies.Visible) Then
    lstAllergies.Visible = False
ElseIf (lstAllergies.ListItems.Count < 1) Then
    frmEventMsgs.Header = "No Highlighted Allergies"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
Else
    lstAllergies.Visible = True
End If
End Sub

Private Sub cmdDocs_Click()
frmScan.Whom = ""
frmScan.PatientId = PatientId
frmScan.AppointmentId = AppointmentId
If (frmScan.LoadScan) Then
    frmScan.Show 1
End If
End Sub

Private Sub cmdDone_Click()
Dim k As Integer
For k = 1 To MaxTestResults
    If (TestResultsStorage(k).TestLocalStatus = "R") Then
        Call ChangeTestHighlight(k, 0)
    End If
Next k
HighlightLoadOn = False
If (BackDoorOn) Then
    Unload frmHighlights
Else
     frmHighlights.Hide
End If
End Sub

Private Sub cmdGlaucoma_Click()
    Dim GlaucomaComWrapper As New comWrapper
    Call GlaucomaComWrapper.Create(GlaucomaMonitorManagerType, emptyArgs)
    GlaucomaComWrapper.InvokeMethod "ShowGlaucomaMonitor", emptyArgs
End Sub

Private Sub cmdHome_Click()
Dim k As Integer
For k = 1 To MaxTestResults
    If (TestResultsStorage(k).TestLocalStatus = "R") Then
        Call ChangeTestHighlight(k, 0)
    End If
Next k
QuitOn = True
HighlightLoadOn = False
If (BackDoorOn) Then
    Unload frmHighlights
Else
    frmHighlights.Hide
End If
End Sub

Private Sub cmdLink_Click()
If Not UserLogin.HasPermission(epPatientNotes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
Dim DrLists As DILists
If (lstLinkNotes.ListCount < 1) Then
    Set DrLists = New DILists
    DrLists.ApplPatientId = PatientId
    DrLists.ApplDate = ActiveDate
    DrLists.ApplStartAt = 1
    DrLists.ApplReturnCount = 0
    Set DrLists.ApplList = lstLinkNotes
    Call DrLists.ApplLinkNotesList
End If
If (lstLinkNotes.ListCount > 0) Then
    lstLinkNotes.Visible = True
    If (lstImpr.Visible) Then
        lstImpr.Visible = False
    End If
    If (lstHist.Visible) Then
        lstHist.Visible = False
    End If
Else
    frmEventMsgs.Header = "No Linked Notes"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdRefract_Click()
If Not UserLogin.HasPermission(epExamElements) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
frmAssignWearNew.cmdHiL.Enabled = False
Call frmSystems1.ShowRefract
frmAssignWearNew.cmdHiL.Enabled = True
End Sub

Private Sub cmdReset_Click()
Dim i As Integer
lstOTestsLV.ListItems.Clear
For i = 1 To lstOTestsOrgLV.ListItems.Count
    lstOTestsLV.ListItems.Add = lstOTestsOrgLV.SelectedItem
    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(1) = lstOTestsOrgLV.ListItems.Item(i).SubItems(1)
    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(2) = lstOTestsOrgLV.ListItems.Item(i).SubItems(2)
    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(3) = lstOTestsOrgLV.ListItems.Item(i).SubItems(3)
    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(4) = lstOTestsOrgLV.ListItems.Item(i).SubItems(4)
    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(5) = lstOTestsOrgLV.ListItems.Item(i).SubItems(5)
Next i
End Sub

Private Sub cmdNotes_Click()
If Not UserLogin.HasPermission(epPatientNotes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
frmNotes.NoteId = 0
frmNotes.EyeContext = ""
frmNotes.NoteOn = False
frmNotes.SystemReference = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    If (IsAlertPresent(PatientId, EAlertType.eatHighlight)) Then
        lblAlert.Caption = "High Alert ON"
        lblAlert.Visible = True
    Else
        lblAlert.Caption = "Alert ON"
        lblAlert.Visible = IsAlertPresent(PatientId, 0)
    End If
End If
End Sub

Private Sub InitTests(IType As Integer)
Dim i As Integer
If (IType = 1) Then
    IOPResultsStorage.TestApptId = 0
    IOPResultsStorage.TestClnId = 0
    IOPResultsStorage.TestDate = ""
    IOPResultsStorage.TestTime = ""
    IOPResultsStorage.TestDate = ""
    IOPResultsStorage.TestText = ""
    IOPResultsStorage.TestDoc = ""
    IOPResultsStorage.TestQuestion = ""
    IOPResultsStorage.TestOrderNum = ""
    IOPResultsStorage.TestParty = ""
    IOPResultsStorage.TestMove = False
    IOPResultsStorage.TestNoteOn = False
    IOPResultsStorage.TestNote = ""
    IOPResultsStorage.TestHighlightOn = False
    IOPResultsStorage.TestStatus = ""
    IOPResultsStorage.TestLocalStatus = ""
    
    VCCResultsStorage.TestApptId = 0
    VCCResultsStorage.TestClnId = 0
    VCCResultsStorage.TestDate = ""
    VCCResultsStorage.TestTime = ""
    VCCResultsStorage.TestDate = ""
    VCCResultsStorage.TestText = ""
    VCCResultsStorage.TestDoc = ""
    VCCResultsStorage.TestQuestion = ""
    VCCResultsStorage.TestOrderNum = ""
    VCCResultsStorage.TestParty = ""
    VCCResultsStorage.TestMove = False
    VCCResultsStorage.TestNoteOn = False
    VCCResultsStorage.TestNote = ""
    VCCResultsStorage.TestHighlightOn = False
    VCCResultsStorage.TestStatus = ""
    VCCResultsStorage.TestLocalStatus = ""

    VSCResultsStorage.TestApptId = 0
    VSCResultsStorage.TestClnId = 0
    VSCResultsStorage.TestDate = ""
    VSCResultsStorage.TestTime = ""
    VSCResultsStorage.TestDate = ""
    VSCResultsStorage.TestText = ""
    VSCResultsStorage.TestDoc = ""
    VSCResultsStorage.TestQuestion = ""
    VSCResultsStorage.TestOrderNum = ""
    VSCResultsStorage.TestParty = ""
    VSCResultsStorage.TestMove = False
    VSCResultsStorage.TestNoteOn = False
    VSCResultsStorage.TestNote = ""
    VSCResultsStorage.TestHighlightOn = False
    VSCResultsStorage.TestStatus = ""
    VSCResultsStorage.TestLocalStatus = ""
ElseIf (IType = 2) Then
    For i = 1 To MaxTestResults
        TestResultsStorage(i).TestApptId = 0
        TestResultsStorage(i).TestClnId = 0
        TestResultsStorage(i).TestDate = ""
        TestResultsStorage(i).TestTime = ""
        TestResultsStorage(i).TestDate = ""
        TestResultsStorage(i).TestText = ""
        TestResultsStorage(i).TestDoc = ""
        TestResultsStorage(i).TestQuestion = ""
        TestResultsStorage(i).TestOrderNum = ""
        TestResultsStorage(i).TestParty = ""
        TestResultsStorage(i).TestMove = False
        TestResultsStorage(i).TestNoteOn = False
        TestResultsStorage(i).TestNote = ""
        TestResultsStorage(i).TestHighlightOn = False
        TestResultsStorage(i).TestStatus = ""
        TestResultsStorage(i).TestLocalStatus = ""
    Next i
ElseIf (IType = 3) Then
    For i = 1 To MaxTestResults
        QTestResultsStorage(i).TestApptId = 0
        QTestResultsStorage(i).TestClnId = 0
        QTestResultsStorage(i).TestDate = ""
        QTestResultsStorage(i).TestTime = ""
        QTestResultsStorage(i).TestDate = ""
        QTestResultsStorage(i).TestText = ""
        QTestResultsStorage(i).TestDoc = ""
        QTestResultsStorage(i).TestQuestion = ""
        QTestResultsStorage(i).TestOrderNum = ""
        QTestResultsStorage(i).TestParty = ""
        QTestResultsStorage(i).TestMove = False
        QTestResultsStorage(i).TestNoteOn = False
        QTestResultsStorage(i).TestNote = ""
        QTestResultsStorage(i).TestHighlightOn = False
        QTestResultsStorage(i).TestStatus = ""
        QTestResultsStorage(i).TestLocalStatus = ""
    Next i
End If
End Sub

Private Sub LoadPhoto(PatId As Long)
Dim Ac As Boolean
Dim ATemp As String
If (PatId > 0) Then
    Ac = False
    ATemp = MyScanDirectory + "Photo_" + Trim(str(PatId)) + ".jpg"
    If (Dir(ATemp) = "") Then
        ATemp = MyScanDirectory + "Photo_" + Trim(str(PatId)) + ".bmp"
        If (Dir(ATemp) <> "") Then
            Ac = True
        End If
    Else
        Ac = True
    End If
    If Not (Ac) Then
        ATemp = MyScanDirectory + "Photo_Default.jpg"
        If (Dir(ATemp) = "") Then
            ATemp = MyScanDirectory + "Photo_Default.bmp"
            If (Dir(ATemp) <> "") Then
                Ac = True
            End If
        Else
            Ac = True
        End If
    End If
    If (Ac) Then
        If (InStrPS(ATemp, ".bmp") <> 0) Then
            FXImage2.picture = FM.LoadPicture(ATemp)
        Else
            FXImage2.FileName = FM.GetPathForDirectIO(ATemp)
        End If
    End If
Else
    ATemp = MyScanDirectory + "Photo_Default.jpg"
    If (Dir(ATemp) = "") Then
        ATemp = MyScanDirectory + "Photo_Default.bmp"
        If (Dir(ATemp) <> "") Then
            Ac = True
        End If
    Else
        Ac = True
    End If
End If
FXImage2.Visible = True
End Sub

Public Function LoadHighlights(PatId As Long, CurrOn As Boolean, TriggerOn As Boolean, BDOn As Boolean) As Boolean
Dim HstOn As Boolean
Dim Duped As Boolean
Dim LocalEditPreviousOn As Boolean
Dim ApptId As Long
Dim DrawApptId As Long
Dim TempApptId As Long
Dim DisplayClnId As Long
Dim IClnId As Long, QryId As Long
Dim AId As Long
Dim DrId As Long
Dim ApptTypeId As Long, ID As Long, ApptIdA As Long
Dim PrescribeOn As Boolean
Dim Ac As Boolean
Dim DupOn As Boolean, VampOn As Boolean
Dim SplitOn As Boolean, QHigh As Boolean
Dim InactiveDiag As Boolean
Dim PrevComp As Boolean, ITriggerOn As Boolean
Dim Af As Integer, Uf As Integer
Dim ComplaintRef As Long, z1 As Integer
Dim Family As Integer, SlotId As Integer
Dim MaxItems As Integer, MaxComplaints As Integer
Dim p As Integer, w As Integer, z As Integer
Dim g As Integer, h As Integer
Dim i As Integer, j As Integer
Dim MyBase As String, TestDate As String
Dim MyAge As Integer, MySex As String
Dim Quant As String, OtherText As String
Dim DocName As String, MyTest As String
Dim PrimaryDiagnosis As String
Dim ATy As String, RTemp As String
Dim ZTemp As String, Ar As String
Dim SortDate As String, DTemp As String
Dim NastyNote As String, TempZ As String
Dim TDisplay As String, CurDate As String
Dim VSCOrder As String, Opr As String
Dim VCCOrder As String, IOPOrder As String
Dim QType As String, QPO As Integer
Dim QText As String, QTime As String, QParty As String
Dim QOrder As String, AText As String, ADate As String
Dim DisplayText As String, LDiag As String
Dim TheSystem As String, TheText As String
Dim ATemp As String, ATemp1 As String, ATemp2 As String
Dim Temp As String, Temp1 As String, Temp2 As String
Dim AFreq As String, AEye As String, AStat As String
Dim RetPat As Patient
Dim RetNote As PatientNotes
Dim RetAct As PracticeActivity
Dim RetAppt As SchedulerAppointment
Dim RetApptType As SchedulerAppointmentType
Dim DrLists As DILists
Dim dtDate As CIODateTime
On Error GoTo UI_ErrorHandler
DisplayClnId = 0
RefDr = 0
ActivityId = 0
LocalEditPreviousOn = False
If (BDOn) Then
    HighlightLoadOn = True
Else
    LocalEditPreviousOn = frmSystems1.EditPreviousOn
End If
LoadHighlights = False
Call LoadPhoto(PatId)
FM.EnlistTempLocalFile (DoctorInterfaceDirectory + Trim(str(PatId)) + "TestFile.txt") ' File is temporary and cleaned up after each tests are loaded

VCCOrder = "03A"
VSCOrder = "02A"
IOPOrder = "26A"
lstCTests.ListItems.Clear
lstCTests.ListItems.Add = "OD"
lstCTests.ListItems.Add = "OS"
If Not (HighlightLoadOn) Then
    LoadHighlights = True
    If Not (LocalEditPreviousOn) Then
        MaxItems = frmSystems1.lstHPI.ListCount - 1
        ATemp = "RV: " + Trim(Mid(frmSystems1.lstHPI.List(0), 4, 50))
        Call ReplaceCharacters(ATemp, "(T)", " ")
        txtCC.Caption = " " + ATemp
        i = frmSystems1.FindTestResults(VCCOrder)
        If (i > 0) Then
            Call frmSystems1.GetTestText(i, AText, QText, QOrder, QParty, QTime, QHigh, QType, QPO)
            SlotId = 1
            GoSub SetupStandardTests
        End If
        i = frmSystems1.FindTestResults(VSCOrder)
        If (i > 0) Then
            Call frmSystems1.GetTestText(i, AText, QText, QOrder, QParty, QTime, QHigh, QType, QPO)
            SlotId = 3
            GoSub SetupStandardTests
        End If
        i = frmSystems1.FindTestResults(IOPOrder)
        If (i > 0) Then
            Call frmSystems1.GetTestText(i, AText, QText, QOrder, QParty, QTime, QHigh, QType, QPO)
            SlotId = 5
            GoSub SetupStandardTests
        End If
    Else
        Set dtDate = New CIODateTime
        dtDate.MyDateTime = Now
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentPatientId = PatId
        RetAppt.AppointmentDate = dtDate.GetIODate
        RetAppt.AppointmentStatus = "D"
        If (RetAppt.FindAppointmentPreviousbyDate > 0) Then
            If (RetAppt.SelectAppointment(1)) Then
                ApptId = RetAppt.AppointmentId
            End If
        End If
        Set RetAppt = Nothing
        Set dtDate = Nothing
        Call GetRVContent(PatientId, ApptId, AText)
        If (Trim(AText) <> "") Then
            txtCC.Caption = " RV:" + AText
        End If
        Call GetOtherTestResult(PatientId, ApptId, "IOP")
        i = InStrPS(IOPResultsStorage.TestText, "IOP")
        If (i > 0) Then
            AText = Trim(Mid(IOPResultsStorage.TestText, i, Len(IOPResultsStorage.TestText) - (i - 1)))
        Else
            AText = Trim(IOPResultsStorage.TestText)
        End If
        SlotId = 5
        GoSub SetupStandardTests
        Call GetOtherTestResult(PatientId, ApptId, "VISION CORRECTED")
        AText = Trim(VCCResultsStorage.TestText)
        If (Len(AText) > 3) Then
            If (Left(AText, 2) = vbCrLf) Then
                AText = Mid(AText, 3, Len(AText) - 2)
            End If
        End If
        SlotId = 1
        GoSub SetupStandardTests
        Call GetOtherTestResult(PatientId, ApptId, "VISION UNCORRECTED")
        AText = Trim(VSCResultsStorage.TestText)
        If (Len(AText) > 3) Then
            If (Left(AText, 2) = vbCrLf) Then
                AText = Mid(AText, 3, Len(AText) - 2)
            End If
        End If
        SlotId = 3
        GoSub SetupStandardTests
    End If

    cmdAlert.BackColor = &H9B9626
    If (IsAlertPresent(PatId, EAlertType.eatHighlight)) Then
        lblAlert.Caption = "High Alert ON"
        lblAlert.Visible = True
    Else
        lblAlert.Caption = "Alert ON"
        lblAlert.Visible = IsAlertPresent(PatId, 0)
    End If
    FirstTimeIn = True
    Exit Function
End If
ITriggerOn = False
CurDate = ""
ADate = ""
txtCC.Caption = ""
lstsort.Clear
lstSort1.Clear
lstAllergies.ListItems.Clear
lstExamLV.ListItems.Clear
lstOcuMeds.ListItems.Clear
lstProcsLV.ListItems.Clear
lstOTestsLV.ListItems.Clear
lstOrdersLV.ListItems.Clear
lstDxLV.ListItems.Clear
lstHPILV.ListItems.Clear
Erase HistHPIComplaints
lstImpr.Clear
lstLinkNotes.Clear
BackDoorOn = BDOn
If (PatId > 0) Then
    Call InitTests(1)
    PatientId = PatId
    CurrentOn = CurrOn
    LoadHighlights = True
    If Not (BackDoorOn) Then
        frmHome.lblLoad.Caption = "Highlight Previous Appointments"
        DoEvents
    End If
    ADate = ""
    Call FormatTodaysDate(ADate, False)
    ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
    DTemp = ""
    RTemp = ""
    PrevApptId = 0

    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        lblLang.Caption = " " + Trim(RetPat.Language)
        MyAge = GetAge(RetPat.BirthDate)
        MySex = RetPat.Gender
    End If
    Set RetPat = Nothing
    Set DrLists = New DILists
    DrLists.ApplPatientId = PatientId
    DrLists.ApplDate = ADate
    DrLists.ApplStartAt = 1
    DrLists.ApplReturnCount = 0
    Set DrLists.ApplList = lstPrevDrs
    Call DrLists.ApplApptDrList
    If (lstPrevDrs.ListCount > 0) Then
        ApptId = lstPrevDrs.ItemData(0)
        If (lstPrevDrs.ListCount > 1) Then
            PrevApptId = ApptId
            If Not (LocalEditPreviousOn) And Not (BDOn) Then
                PrevApptId = lstPrevDrs.ItemData(1)
            End If
        End If
        DTemp = Mid(lstPrevDrs.List(0), 12, 10)
        If (Len(lstPrevDrs.List(0)) > 36) Then
            RTemp = Mid(lstPrevDrs.List(0), 36, Len(lstPrevDrs.List(0)) - 35)
            i = InStrPS(lstPrevDrs.List(0), "-")
            If (i > 0) Then
                RefDr = Val(Trim(Mid(lstPrevDrs.List(0), i + 1, Len(lstPrevDrs.List(0)) - i)))
            End If
        End If
        lblLDate.Caption = " Last Visit: " + Trim(Left(lstPrevDrs.List(0), 10))
    Else
        lblLDate.Caption = " First Visit"
        Set RetPat = New Patient
        RetPat.PatientId = PatId
        If (RetPat.RetrievePatient) Then
            DrLists.ApplPatName = Trim(Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial)) + " " + Trim(RetPat.LastName)
        End If
        Set RetPat = Nothing
    End If
    lblLDate.Caption = " " + Trim(Trim(lblLDate.Caption) + " " + DTemp)
    lblPat.Caption = " " + Trim(DrLists.ApplPatName)
    lblAge.Caption = Trim(str(MyAge))
    lblSex.Caption = MySex
    Set DrLists = Nothing

    If (ApptId < 1) Then
        Set RetAct = New PracticeActivity
        RetAct.ActivityDate = ADate
        RetAct.ActivityPatientId = PatId
        If (RetAct.FindActivity > 0) Then
            If (RetAct.SelectActivity(1)) Then
                ApptId = RetAct.ActivityAppointmentId
            End If
        End If
        Set RetAct = Nothing
    End If

    If (ApptId > 0) Then
        AppointmentId = ApptId
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentId = ApptId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            DrId = RetAppt.AppointmentResourceId1
            ADate = RetAppt.AppointmentDate
            CurDate = ADate
            ApptTypeId = RetAppt.AppointmentTypeId
        End If
        Set RetAppt = Nothing
    End If

    ActiveDate = ADate
    If Not (LocalEditPreviousOn) And Not (BDOn) Then
        ATemp1 = Trim(frmSystems1.txtSum2.Text)
        If (Len(Trim(ATemp1)) > 1) Then
            w = InStrPS(ATemp1, ",")
            If (w > 1) Then
                ATemp = Trim(Left(ATemp1, w - 1))
                RTemp = Trim(Mid(ATemp1, w + 1, Len(ATemp1) - w))
                If (Trim(frmSystems1.txtSum2.Tag) <> "") Then
                    RefDr = Val(Trim(frmSystems1.txtSum2.Tag))
                End If
            Else
                ATemp = Trim(ATemp1)
            End If
        Else
            ATemp = ""
            If (ApptTypeId > 0) Then
                Set RetApptType = New SchedulerAppointmentType
                RetApptType.AppointmentTypeId = ApptTypeId
                If (RetApptType.RetrieveSchedulerAppointmentType) Then
                    ATemp = Trim(RetApptType.AppointmentType)
                End If
                Set RetApptType = Nothing
            End If
        End If
    Else
        ATemp = ""
        If (ApptTypeId > 0) Then
            Set RetApptType = New SchedulerAppointmentType
            RetApptType.AppointmentTypeId = ApptTypeId
            If (RetApptType.RetrieveSchedulerAppointmentType) Then
                ATemp = Trim(RetApptType.AppointmentType)
            End If
            Set RetApptType = Nothing
        End If
    End If

' appointment type and referring doctor
    lblApptType.Caption = " " + ATemp
    If (Trim(RTemp) <> "") Then
        If (InStrPS(RTemp, "-" + Trim(str(RefDr))) = 0) Then
            RTemp = RTemp + "-" + Trim(str(RefDr))
        End If
        lblRefDr.Caption = " " + RTemp
    End If

    If Not (BackDoorOn) And Not (LocalEditPreviousOn) Then
        MaxItems = frmSystems1.lstHPI.ListCount - 1
        ATemp = "RV: " + Trim(Mid(frmSystems1.lstHPI.List(0), 4, 50))
        Call ReplaceCharacters(ATemp, "(T)", " ")
        txtCC.Caption = " " + ATemp
        i = frmSystems1.FindTestResults(VCCOrder)
        If (i > 0) Then
            Call frmSystems1.GetTestText(i, AText, QText, QOrder, QParty, QTime, QHigh, QType, QPO)
            SlotId = 1
            GoSub SetupStandardTests
        End If
        i = frmSystems1.FindTestResults(VSCOrder)
        If (i > 0) Then
            Call frmSystems1.GetTestText(i, AText, QText, QOrder, QParty, QTime, QHigh, QType, QPO)
            SlotId = 3
            GoSub SetupStandardTests
        End If
        i = frmSystems1.FindTestResults(IOPOrder)
        If (i > 0) Then
            Call frmSystems1.GetTestText(i, AText, QText, QOrder, QParty, QTime, QHigh, QType, QPO)
            SlotId = 5
            GoSub SetupStandardTests
        End If
    Else
        Call GetRVContent(PatientId, ApptId, AText)
        txtCC.Caption = " RV:" + AText
        Call GetOtherTestResult(PatientId, ApptId, "IOP")
        i = InStrPS(IOPResultsStorage.TestText, "IOP")
        If (i > 0) Then
            AText = Trim(Mid(IOPResultsStorage.TestText, i, Len(IOPResultsStorage.TestText) - (i - 1)))
        Else
            AText = Trim(IOPResultsStorage.TestText)
        End If
        SlotId = 5
        GoSub SetupStandardTests
        Call GetOtherTestResult(PatientId, ApptId, "VISION CORRECTED")
        AText = Trim(VCCResultsStorage.TestText)
        If (Len(AText) > 3) Then
            If (Left(AText, 2) = vbCrLf) Then
                AText = Mid(AText, 3, Len(AText) - 2)
            End If
        End If
        SlotId = 1
        GoSub SetupStandardTests
        Call GetOtherTestResult(PatientId, ApptId, "VISION UNCORRECTED")
        AText = Trim(VSCResultsStorage.TestText)
        If (Len(AText) > 3) Then
            If (Left(AText, 2) = vbCrLf) Then
                AText = Mid(AText, 3, Len(AText) - 2)
            End If
        End If
        SlotId = 3
        GoSub SetupStandardTests
    End If

' Previous Billed Diagnosis
    If Not (BackDoorOn) Then
        frmHome.lblLoad.Caption = "Highlight Previous Diags"
        DoEvents
    End If
    Uf = 0

    MyBase = ""
    TempApptId = ApptId
' Get the primary Diagnosis (use this to align the first Impression)
    PrimaryDiagnosis = ""


' Impressions, Last visit (use PrimaryDiagnosis to locate and set the first impression)
    Set RetCln = New DI_ExamClinical
    If (RetCln.LoadHighlightsImpressions(TempApptId)) Then
        If (RetCln.LocateStartingRecord("")) Then
            QryId = 1
            Do Until Not (RetCln.RetrieveHighlightClinicalItem(QryId))
                If (RetCln.ClinicalStatus = "A") Then
                    ITriggerOn = True
                    MyTest = "X"
                    GoSub SetupDiags
                End If
                QryId = QryId + 1
            Loop
        End If
    End If
    Set RetCln = Nothing
    ITriggerOn = False

' Highlights
    If Not (BackDoorOn) Then
        frmHome.lblLoad.Caption = "Highlight All Others"
        DoEvents
    End If
    Set RetCln = New DI_ExamClinical
    If (RetCln.LoadHighlightsClinicalData(PatientId, ActiveDate)) Then
        If (RetCln.LocateStartingRecord("")) Then
            QryId = 1
            Do Until Not (RetCln.RetrieveHighlightClinicalItem(QryId))
                If (RetCln.ClinicalStatus = "A") Then
                    If (RetCln.ClinicalType = "A") And (Left(RetCln.ClinicalFindings, 2) = "RX") And (Trim(RetCln.ClinicalHighlights) = "") Then
                        ADate = Trim(Mid(RetCln.ClinicalDescriptor, 2, 10))
                        If (ADate <> "") Then
                            ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
                        End If
                        If (ActiveDate = ADate) Or (ActiveDate = RetCln.ClinicalApptDate) Then
                            RetCln.ClinicalHighlights = "A"
                        End If
                        If (RetCln.ClinicalHighlights <> "A") Then
                            ADate = Mid(ActiveDate, 5, 2) + "/" + Mid(ActiveDate, 7, 2) + "/" + Left(ActiveDate, 4)
                            If (InStrPS(RetCln.ClinicalDescriptor, ADate) <> 0) Then
                                RetCln.ClinicalHighlights = "A"
                            End If
                        End If
                    End If
                    If (RetCln.ClinicalType = "A") And (RetCln.ClinicalHighlights = "A") Then
                        If (Left(RetCln.ClinicalFindings, 2) = "RX") Then
                            GoSub SetupRX
                        Else
                            GoSub SetupOrders
                        End If
                    ElseIf (RetCln.ClinicalType = "Z") And (RetCln.ClinicalHighlights = "P") Then
                        TempZ = "Z"
                        GoSub SetupProcs
                    ElseIf (RetCln.ClinicalType = "Q") And (RetCln.ClinicalHighlights = "Q") Then
                        GoSub SetupFindings
                    ElseIf (RetCln.ClinicalType = "P") And (RetCln.ClinicalHighlights = "P") Then
                        TempZ = "P"
                        GoSub SetupProcs
                    ElseIf ((RetCln.ClinicalType = "C") Or (RetCln.ClinicalType = "H")) And ((RetCln.ClinicalHighlights = "C") Or (RetCln.ClinicalHighlights = "H")) Then
                        GoSub SetupComplaints
                    End If
                End If
                QryId = QryId + 1
            Loop
        End If
    End If

' Tests that are Highlighted
    Temp = ""
    Temp1 = ""
    ATemp = ""
    ATemp1 = ""
    DisplayText = ""
    If Not (BackDoorOn) Then
        frmHome.lblLoad.Caption = "Highlight Tests"
        DoEvents
    End If

    Call InitTests(2)
    TestResultsIndex = 0
    HstOn = False
    GoSub SetupTests

' Tests that are Linked to closed Actions
    If Not (BackDoorOn) Then
        frmHome.lblLoad.Caption = "Highlight Tests Linked to Closed Actions"
        DoEvents
    End If

    Call InitTests(3)
    QTestResultsIndex = 0
    HstOn = True
    GoSub SetupTests
    Set RetCln = Nothing
    Call LoadTestNotes

    If Not (BackDoorOn) Then
        frmHome.lblLoad.Caption = "Highlight Current Complaints"
        DoEvents
    End If
' Current Complaints
    PrevComp = False
    Family = 0
    ComplaintRef = i
    ATemp1 = ""
    DisplayText = ""
    MaxComplaints = frmSystems1.TotalListedComplaints
    For i = 1 To MaxComplaints Step 2
        If (frmSystems1.GetComplaintEntry(i, ATy, Temp, Temp2, Ac, Af, Ar, AId, True)) Then
            If (frmSystems1.GetComplaintEntry(i + 1, ATy, Temp1, Temp2, Ac, Af, Ar, AId, True)) Then
                SortDate = Temp2
                If (Len(SortDate) < 10) Then
                    SortDate = frmSystems1.ActiveActivityDate
                Else
                    SortDate = Mid(SortDate, 7, 4) + Mid(SortDate, 1, 2) + Mid(SortDate, 4, 2)
                End If
                If (Left(Temp, 1) = "/") And (i > 1) Then
                    If (Len(Trim(ATemp1)) > 65) Then
                        For w = 1 To Len(ATemp1) Step 65
                            DisplayText = Mid(ATemp1, w, 64)
                            DisplayText = DisplayText + Space(75 - Len(DisplayText))
                            If (PrevComp) Then
                                If (Family = 1) Then
                                    If (Trim(DisplayText) <> "") And Not (BDOn) Then
                                        lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "H" + DisplayText + Space(25) + "c" + Trim(str(ComplaintRef))
                                    End If
                                ElseIf (Family = 9) Then
                                    If (Trim(DisplayText) <> "") Then
                                        lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "g" + DisplayText + Space(25) + "g" + Trim(str(ComplaintRef))
                                    End If
                                Else
                                    If (Trim(DisplayText) <> "") Then
                                        lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "E" + DisplayText + Space(25) + "c" + Trim(str(ComplaintRef))
                                    End If
                                End If
                            End If
                        Next w
                    Else
                        ATemp1 = ATemp1 + Space(75 - Len(ATemp1))
                        If (PrevComp) Then
                            If (Family = 1) Then
                                If (Trim(ATemp1) <> "") And Not (BDOn) Then
                                    lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "H" + ATemp1 + "c" + Trim(str(ComplaintRef))
                                End If
                            ElseIf (Family = 9) Then
                                If (Trim(ATemp1) <> "") Then
                                    lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "g" + ATemp1 + "g" + Trim(str(ComplaintRef))
                                End If
                            Else
                                If (Trim(ATemp1) <> "") Then
                                    lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "E" + ATemp1 + "c" + Trim(str(ComplaintRef))
                                End If
                            End If
                        End If
                    End If
                    Family = Af
                    PrevComp = Ac
                    ATemp1 = ""
                    DisplayText = ""
                    ComplaintRef = i
                Else
                    Family = Af
                    PrevComp = Ac
                End If
                If (Trim(Temp1) <> "") Then
                    If (GetComplaintsLingo(Temp, Temp1, ATemp, Uf)) Then
                        If (Trim(ATemp) = "") Then
                            If (Family = 10) Then
                                ATemp1 = Trim(ATemp1) + " " + Trim(Temp1) + " " + Trim(Temp2) + " "
                            Else
                                ATemp1 = Trim(ATemp1) + " " + Trim(ATemp) + " " + Trim(Temp2) + " "
                            End If
                        Else
                            ATemp1 = Trim(ATemp1) + " " + Trim(ATemp) + " " + Trim(Temp2) + " "
                        End If
                    End If
                End If
            End If
        Else
            If (Trim(ATemp1) <> "") And (PrevComp) Then
                If (Len(ATemp1) > 65) Then
                    For w = 1 To Len(ATemp1) Step 65
                        DisplayText = Mid(ATemp1, 1, 64)
                        DisplayText = DisplayText + Space(75 - Len(DisplayText))
                        If (Family = 1) Then
                            If Not (BDOn) And Not (LocalEditPreviousOn) Then
                                lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "H" + DisplayText + "c" + Trim(str(ComplaintRef))
                            End If
                        ElseIf (Family = 9) Then
                            lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "g" + DisplayText + "g" + Trim(str(ComplaintRef))
                        Else
                            lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "E" + DisplayText + "c" + Trim(str(ComplaintRef))
                        End If
                    Next w
                Else
                    ATemp1 = ATemp1 + Space(75 - Len(ATemp1))
                    If (Family = 1) Then
                        If Not (BDOn) And Not (LocalEditPreviousOn) Then
                            lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "H" + ATemp1 + "c" + Trim(str(ComplaintRef))
                        End If
                    ElseIf (Family = 9) Then
                        lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "g" + ATemp1 + "g" + Trim(str(ComplaintRef))
                    Else
                        lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "E" + ATemp1 + "c" + Trim(str(ComplaintRef))
                    End If
                End If
                ATemp1 = ""
                DisplayText = ""
            End If
        End If
    Next i
' to catch the last one/ may cause a dup check it
    If (Trim(ATemp1) <> "") And (PrevComp) Then
        If (Len(ATemp1) > 65) Then
            For w = 1 To Len(ATemp1) Step 65
                DisplayText = Mid(ATemp1, 1, 64)
                DisplayText = DisplayText + Space(75 - Len(DisplayText))
                If (Family = 1) Then
                    If Not (BDOn) And Not (LocalEditPreviousOn) Then
                        lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "H" + DisplayText + "c" + Trim(str(ComplaintRef))
                    End If
                ElseIf (Family = 9) Then
                    lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "g" + DisplayText + "g" + Trim(str(ComplaintRef))
                Else
                    lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "E" + DisplayText + "c" + Trim(str(ComplaintRef))
                End If
            Next w
        Else
            ATemp1 = ATemp1 + Space(75 - Len(ATemp1))
            If (Family = 1) Then
                If Not (BDOn) And Not (LocalEditPreviousOn) Then
                    lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "H" + ATemp1 + "c" + Trim(str(ComplaintRef))
                End If
            ElseIf (Family = 9) Then
                lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "g" + ATemp1 + "g" + Trim(str(ComplaintRef))
            Else
                lstSort1.AddItem SortDate + " " + ConvertInteger(ComplaintRef, False) + "E" + ATemp1 + "c" + Trim(str(ComplaintRef))
            End If
        End If
        ATemp1 = ""
        DisplayText = ""
    End If

' set up the boxes
    DrawApptId = ApptId
    For i = lstSort1.ListCount - 1 To 0 Step -1
        If (Trim(lstSort1.List(i)) <> "") Then
            ATemp = lstSort1.List(i)
            ATemp1 = Mid(ATemp, 21, Len(ATemp) - 20)
            If (Mid(ATemp, 20, 1) = "E") Then
                If (Trim(Mid(ATemp, 10, 20)) <> "") Then
                    If (i > 0) Then
                        If (Mid(lstSort1.List(i - 1), 1, 20) = Mid(ATemp, 1, 20)) Then
                            lstExamLV.ListItems.Add = Mid(ATemp1, 77, Len(ATemp1) - 76)
                            If (UCase(Mid(ATemp1, 76, 1)) = "C") Then
                                lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(1) = Mid(ActiveDate, 5, 2) + "-" + Mid(ActiveDate, 7, 2) + "-" + Mid(ActiveDate, 3, 2)
                                If (Mid(ATemp1, 76, 1) = "C") Then
                                    lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(3) = Trim(Mid(lstSort1.List(i - 1), 21, 65)) + Mid(ATemp1, 10, 40)
                                Else
                                    lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(3) = Trim(Mid(lstSort1.List(i - 1), 21, 65)) + Mid(ATemp1, 1, 40)
                                End If
                            Else
                                lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                                lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(3) = Trim(Mid(lstSort1.List(i - 1), 21, 65)) + Mid(ATemp1, 10, 40)
                            End If
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(2) = "--"
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(4) = ""
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(5) = Mid(ATemp, 76, 1)
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(6) = Mid(ATemp1, 77, Len(ATemp1) - 76)
                            i = i - 1
                        Else
                            lstExamLV.ListItems.Add = Mid(ATemp1, 77, Len(ATemp1) - 76)
                            If (UCase(Mid(ATemp1, 76, 1)) = "C") Then
                                lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(1) = Mid(ATemp, 5, 2) + "-" + Mid(ATemp, 7, 2) + "-" + Mid(ATemp, 3, 2)
                                lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(3) = Mid(ATemp1, 1, 40)
                            Else
                                lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                                lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(3) = Mid(ATemp1, 10, 40)
                            End If
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(2) = "--"
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(4) = ""
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(5) = Mid(ATemp, 76, 1)
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(6) = Mid(ATemp1, 77, Len(ATemp1) - 76)
                        End If
                    ElseIf (i = 0) Then
                        lstExamLV.ListItems.Add = Mid(ATemp1, 77, Len(ATemp1) - 76)
                        If (UCase(Mid(ATemp1, 76, 1)) = "C") Then
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(1) = Mid(ActiveDate, 5, 2) + "-" + Mid(ActiveDate, 7, 2) + "-" + Mid(ActiveDate, 3, 2)
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(3) = Mid(ATemp1, 1, 40)
                        Else
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                            lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(3) = Mid(ATemp1, 10, 40)
                        End If
                        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(2) = "--"
                        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(4) = ""
                        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(5) = Mid(ATemp, 76, 1)
                        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(6) = Mid(ATemp1, 77, Len(ATemp1) - 76)
                    End If
                End If
            ElseIf (Mid(ATemp, 20, 1) = "g") Then
                If (Trim(Mid(ATemp, 10, 20)) <> "") Then
                    DupOn = False
                    For j = 1 To lstAllergies.ListItems.Count
                        If (Trim(Mid(ATemp1, 9, 41)) = Trim(lstAllergies.ListItems.Item(j).SubItems(3))) And _
                           (Left(ATemp1, 8) = lstAllergies.ListItems.Item(j).SubItems(1)) Then
                            DupOn = True
                            Exit For
                        End If
                    Next j
                    If Not (DupOn) Then
                        If (i > 0) Then
                            If (Mid(lstSort1.List(i - 1), 1, 20) = Mid(ATemp, 1, 20)) Then
                                lstAllergies.ListItems.Add = Mid(ATemp1, 77, Len(ATemp1) - 76)
                                If (Mid(ATemp1, 3, 1) = "-") Then
                                    lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                                    lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(3) = Mid(ATemp1, 10, 40)
                                Else
                                    lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(1) = Mid(ActiveDate, 5, 2) + "-" + Mid(ActiveDate, 7, 2) + "-" + Mid(ActiveDate, 3, 2)
                                    lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(3) = Mid(ATemp1, 1, 40)
                                End If
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(2) = "--"
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(4) = ""
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(5) = Mid(ATemp, 76, 1)
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(6) = Mid(ATemp1, 77, Len(ATemp1) - 76)
                                i = i - 1
                            Else
                                lstAllergies.ListItems.Add = Mid(ATemp1, 77, Len(ATemp1) - 76)
                                If (Mid(ATemp1, 3, 1) = "-") Then
                                    lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                                    lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(3) = Mid(ATemp1, 10, 40)
                                Else
                                    lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(1) = Mid(ActiveDate, 5, 2) + "-" + Mid(ActiveDate, 7, 2) + "-" + Mid(ActiveDate, 3, 2)
                                    lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(3) = Mid(ATemp1, 1, 40)
                                End If
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(2) = "--"
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(4) = ""
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(5) = Mid(ATemp, 76, 1)
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(6) = Mid(ATemp1, 77, Len(ATemp1) - 76)
                            End If
                        ElseIf (i = 0) Then
                            lstAllergies.ListItems.Add = Mid(ATemp1, 77, Len(ATemp1) - 76)
                            If (Mid(ATemp1, 3, 1) = "-") Then
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(3) = Mid(ATemp1, 10, 40)
                            Else
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(1) = Mid(ActiveDate, 5, 2) + "-" + Mid(ActiveDate, 7, 2) + "-" + Mid(ActiveDate, 3, 2)
                                lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(3) = Mid(ATemp1, 1, 40)
                            End If
                            lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(2) = "--"
                            lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(4) = ""
                            lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(5) = Mid(ATemp, 76, 1)
                            lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(6) = Mid(ATemp1, 77, Len(ATemp1) - 76)
                        End If
                    End If
                End If
            ElseIf (Mid(ATemp, 20, 1) = "H") Then
                If (Trim(Mid(ATemp, 10, 20)) <> "") Then
                    DupOn = False
                    For j = 1 To lstHPILV.ListItems.Count
                        If (Trim(Mid(ATemp1, 9, 41)) = Trim(lstHPILV.ListItems.Item(j).SubItems(3))) Or (InStrPS(Trim(lstHPILV.ListItems.Item(j).SubItems(3)), Trim(Mid(ATemp1, 11, 41))) <> 0) Then
                            DupOn = True
                            Exit For
                        End If
                        If (InStrPS(Trim(lstHPILV.ListItems.Item(j).SubItems(3)), Trim(Mid(ATemp1, 11, 41))) <> 0) Then
                            DupOn = True
                            Exit For
                        End If
                    Next j
                    If Not (DupOn) Then
                        If (i > 0) Then
                            If (Mid(lstSort1.List(i - 1), 1, 20) = Mid(ATemp, 1, 20)) Then
                                lstHPILV.ListItems.Add = Mid(ATemp1, 77, Len(ATemp1) - 76)
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(1) = ""
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(2) = "--"
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(3) = Trim(Mid(lstSort1.List(i - 1), 21, 65)) + Trim(Mid(ATemp1, 1, 65))
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(4) = ""
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(5) = Mid(ATemp1, 76, 1)
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(6) = Mid(ATemp1, 77, Len(ATemp1) - 76)
                                i = i - 1
                            Else
                                lstHPILV.ListItems.Add = Mid(ATemp1, 77, Len(ATemp1) - 76)
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(1) = ""
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(2) = "--"
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(3) = Trim(Mid(ATemp1, 1, 65))
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(4) = ""
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(5) = Mid(ATemp1, 76, 1)
                                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(6) = Mid(ATemp1, 77, Len(ATemp1) - 76)
                            End If
                        ElseIf (i = 0) Then
                            lstHPILV.ListItems.Add = Mid(ATemp1, 77, Len(ATemp1) - 76)
                            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(1) = ""
                            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(2) = "--"
                            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(3) = Trim(Mid(ATemp1, 1, 65))
                            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(4) = ""
                            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(5) = Mid(ATemp1, 76, 1)
                            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(6) = Mid(ATemp1, 77, Len(ATemp1) - 76)
                        End If
                    End If
                End If
            ElseIf (Mid(ATemp, 20, 1) = "O") Or (Mid(ATemp, 20, 1) = "P") Or _
                   (Mid(ATemp, 20, 1) = "a") Or (Mid(ATemp, 20, 1) = "Y") Then
                lstOrdersLV.ListItems.Add = Mid(ATemp1, 202, Len(ATemp1) - 201)
                lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(2) = Mid(ATemp1, 10, 2)
                lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(3) = Trim(Mid(ATemp1, 13, 70))
                lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(4) = ""
                lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(5) = Mid(ATemp1, 201, 1)
                lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(6) = Mid(ATemp1, 202, Len(ATemp1) - 201)
            ElseIf (Mid(ATemp, 20, 1) = "X") Then
                DupOn = False
                For j = 1 To lstDxLV.ListItems.Count
                    If (Mid(ATemp1, 13, 41) = lstDxLV.ListItems.Item(j).SubItems(3)) Then
                        DupOn = True
                        Exit For
                    End If
                Next j
                If Not (DupOn) Then
                    If (lstDxLV.ListItems.Count = 0) Then
                        Call GetDrawingBase(Trim(Mid(ATemp1, 65, 10)), TempApptId, MyBase, IClnId, DrawApptId)
                    Else
                        MyBase = ""
                    End If
                    lstDxLV.ListItems.Add = Trim(Mid(ATemp1, 76, Len(ATemp1) - 75))
                    lstDxLV.ListItems.Item(lstDxLV.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                    lstDxLV.ListItems.Item(lstDxLV.ListItems.Count).SubItems(2) = Trim(Mid(ATemp1, 10, 2))
                    lstDxLV.ListItems.Item(lstDxLV.ListItems.Count).SubItems(3) = Trim(Mid(ATemp1, 13, 41))
                    lstDxLV.ListItems.Item(lstDxLV.ListItems.Count).SubItems(4) = MyBase
                    lstDxLV.ListItems.Item(lstDxLV.ListItems.Count).SubItems(5) = Trim(str(IClnId))
                End If
            ElseIf (Mid(ATemp, 20, 1) = "R") Or (Mid(ATemp, 20, 1) = "T") Then
                DupOn = False
                For j = 1 To lstOTestsLV.ListItems.Count
                    If (Mid(ATemp1, 13, 61) = lstOTestsLV.ListItems.Item(j).SubItems(3)) Then
                        DupOn = True
                        Exit For
                    End If
                Next j
                If Not (DupOn) Then
                    GoSub GetTestDoc
                    lstOTestsLV.ListItems.Add = Trim(Mid(ATemp1, 76, Len(ATemp1) - 75))
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(2) = Trim(Mid(ATemp1, 10, 2))
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(3) = Trim(Mid(ATemp1, 13, 61))
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(4) = Temp1
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(5) = Trim(Mid(ATemp1, 76, 1))
                End If
            ElseIf (Mid(ATemp, 20, 1) = "p") Or (Mid(ATemp, 20, 1) = "q") Then
                GoSub GetTestDoc
                lstProcsLV.ListItems.Add = Trim(Mid(ATemp1, 77, Len(ATemp1) - 76))
                lstProcsLV.ListItems.Item(lstProcsLV.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
                lstProcsLV.ListItems.Item(lstProcsLV.ListItems.Count).SubItems(2) = Trim(Mid(ATemp1, 10, 2))
                lstProcsLV.ListItems.Item(lstProcsLV.ListItems.Count).SubItems(3) = Trim(Mid(ATemp1, 13, 61))
                lstProcsLV.ListItems.Item(lstProcsLV.ListItems.Count).SubItems(4) = Temp1
                lstProcsLV.ListItems.Item(lstProcsLV.ListItems.Count).SubItems(5) = Trim(Mid(ATemp1, 76, 1))
                lstProcsLV.ListItems.Item(lstProcsLV.ListItems.Count).SubItems(6) = Trim(Mid(ATemp1, 77, Len(ATemp1) - 76))
            ElseIf (UCase(Mid(ATemp, 20, 1)) = "W") Then
'                lstOcuMeds.ListItems.Add = Trim(Mid(ATemp1, 202, Len(ATemp1) - 201))
'                lstOcuMeds.ListItems.Item(lstOcuMeds.ListItems.Count).SubItems(1) = Left(ATemp1, 8)
'                lstOcuMeds.ListItems.Item(lstOcuMeds.ListItems.Count).SubItems(2) = Trim(Mid(ATemp1, 10, 2))
'                RTemp = Trim(Mid(ATemp1, 13, 61))
'                Call ReplaceCharacters(RTemp, "(" + Trim(Mid(ATemp1, 10, 2)) + ")", "~")
'                Call StripCharacters(RTemp, "~")
'                lstOcuMeds.ListItems.Item(lstOcuMeds.ListItems.Count).SubItems(3) = Trim(RTemp)
'                lstOcuMeds.ListItems.Item(lstOcuMeds.ListItems.Count).SubItems(4) = Mid(ATemp, 20, 1)
            End If
        End If
    Next i

' Plan Note Highlights in Orders / Otherwise Junk box
    Set RetNote = New PatientNotes
    RetNote.NotesPatientId = PatId
    RetNote.NotesAppointmentId = 0
    RetNote.NotesType = "C"
    RetNote.NotesHighlight = "V"
    RetNote.NotesSystem = ""
    If (RetNote.FindNotesbyHighlight > 0) Then
        i = 1
        While (RetNote.SelectNotes(i))
            GoSub SetupNotes
            i = i + 1
        Wend
    End If
    Set RetNote = Nothing

' External Procedures
    lstSort1.Clear
    Set RetNote = New PatientNotes
    RetNote.NotesPatientId = PatId
    RetNote.NotesAppointmentId = 0
    RetNote.NotesType = "C"
    RetNote.NotesHighlight = "]"
    RetNote.NotesSystem = ""
    If (RetNote.FindNotesbyHighlight > 0) Then
        i = 1
        While (RetNote.SelectNotes(i))
            GoSub SetupExternal
            i = i + 1
        Wend
    End If
    Set RetNote = Nothing
    lstSurg.ListItems.Clear
    For i = lstSort1.ListCount - 1 To 0 Step -1
        ATemp1 = Mid(lstSort1.List(i), 5, 2) + "-" + Mid(lstSort1.List(i), 7, 2) + "-" + Mid(lstSort1.List(i), 3, 2)
        lstSurg.ListItems.Add = ATemp1
        lstSurg.ListItems.Item(lstSurg.ListItems.Count).SubItems(1) = Mid(lstSort1.List(i), 11, 2)
        lstSurg.ListItems.Item(lstSurg.ListItems.Count).SubItems(2) = Trim(Mid(lstSort1.List(i), 13, 40))
        lstSurg.ListItems.Item(lstSurg.ListItems.Count).SubItems(3) = Trim(Mid(lstSort1.List(i), 53, 20))
        lstSurg.ListItems.Item(lstSurg.ListItems.Count).SubItems(4) = Trim(Mid(lstSort1.List(i), 76, Len(lstSort1.List(i)) - 75))
    Next i

' Historic Tests
    If Not (BackDoorOn) Then
        frmHome.lblLoad.Caption = "Highlight Test History"
        DoEvents
    End If
    For i = 1 To MaxHistoryTests
        HistResultsStorage(i).TestApptId = 0
        HistResultsStorage(i).TestClnId = 0
        HistResultsStorage(i).TestDate = ""
        HistResultsStorage(i).TestMove = False
        HistResultsStorage(i).TestNoteOn = False
        HistResultsStorage(i).TestNote = ""
        HistResultsStorage(i).TestOrderNum = ""
        HistResultsStorage(i).TestParty = ""
        HistResultsStorage(i).TestQuestion = ""
        HistResultsStorage(i).TestText = ""
        HistResultsStorage(i).TestDoc = ""
        HistResultsStorage(i).TestTime = ""
    Next i
    HistResultsIndex1 = 0
    Call SetHistoricTests(PatientId, 0, CurDate)
        HistResultsIndex2 = 50
    Call SetHistoricTests(PatientId, 1, CurDate)
        HistResultsIndex3 = 100
    Call SetHistoricTests(PatientId, 2, CurDate)

' Rationalize completed Orders
    If Not (BackDoorOn) Then
        frmHome.lblLoad.Caption = "Reconcile Completed Orders"
        DoEvents
    End If
    If (TriggerOn) Then
        Call CloseOutOrders
    End If

' Set Background Tests for filtering
    lstOTestsOrgLV.ListItems.Clear
    For i = 1 To lstOTestsLV.ListItems.Count
        lstOTestsOrgLV.ListItems.Add = lstOTestsLV.ListItems.Item(i).Text
        lstOTestsOrgLV.ListItems.Item(lstOTestsOrgLV.ListItems.Count).SubItems(1) = lstOTestsLV.ListItems.Item(i).SubItems(1)
        lstOTestsOrgLV.ListItems.Item(lstOTestsOrgLV.ListItems.Count).SubItems(2) = lstOTestsLV.ListItems.Item(i).SubItems(2)
        lstOTestsOrgLV.ListItems.Item(lstOTestsOrgLV.ListItems.Count).SubItems(3) = lstOTestsLV.ListItems.Item(i).SubItems(3)
        lstOTestsOrgLV.ListItems.Item(lstOTestsOrgLV.ListItems.Count).SubItems(4) = lstOTestsLV.ListItems.Item(i).SubItems(4)
        lstOTestsOrgLV.ListItems.Item(lstOTestsOrgLV.ListItems.Count).SubItems(5) = lstOTestsLV.ListItems.Item(i).SubItems(5)
    Next i

' Get the drawing
    Call SetDrawing(DrawApptId)
    If (lstAllergies.ListItems.Count > 0) Then
        cmdAlg.ThreeDHighlightColor = &HFF
        cmdAlg.ThreeDShadowColor = &HFF
    Else
        cmdAlg.ThreeDHighlightColor = cmdHome.ThreeDHighlightColor
        cmdAlg.ThreeDShadowColor = cmdHome.ThreeDShadowColor
    End If
    If (BackDoorOn) Then
        cmdAlert.ThreeDHighlightColor = &HD6CF56
        cmdAlert.ThreeDShadowColor = &H76721D
        Set RetNote = New PatientNotes
        RetNote.NotesPatientId = PatientId
        RetNote.NotesType = "P"
        If (RetNote.FindNotesbyPatient > 0) Then
            cmdAlert.ThreeDHighlightColor = &HFF
            cmdAlert.ThreeDShadowColor = &HFF
        End If
        Set RetNote = Nothing
    Else
        If (frmSystems1.cmdNotes.ThreeDHighlightColor = 255) Then
            cmdAlert.ThreeDHighlightColor = frmSystems1.cmdNotes.ThreeDHighlightColor
            cmdAlert.ThreeDShadowColor = frmSystems1.cmdNotes.ThreeDShadowColor
        End If
    End If
    cmdAlert.BackColor = &H9B9626
    If (IsAlertPresent(PatId, EAlertType.eatHighlight)) Then
        lblAlert.Caption = "High Alert ON"
        lblAlert.Visible = True
    Else
        lblAlert.Caption = "Alert ON"
        lblAlert.Visible = IsAlertPresent(PatId, 0)
    End If
    If (BackDoorOn) Then
        lblPostOp.Visible = False
    Else
        lblPostOp.Visible = frmSystems1.IsPostOpOn
    End If
    Call ClearSelectLists
    LoadHighlights = True
End If
Exit Function
SetupTests:
    Temp = ""
    Temp1 = ""
    ATemp1 = ""
    DisplayText = ""
    If (HstOn) Then
        Call SetTestFiles(PatientId, False, True)
    Else
        Call SetTestFiles(PatientId, True, False)
    End If
    For i = 1 To MaxTestResults
        If (HstOn) Then
            ReferenceTest = QTestResultsStorage(i)
        Else
            ReferenceTest = TestResultsStorage(i)
        End If
        SortDate = ""
        If (Trim(ReferenceTest.TestText) <> "") Then
            DisplayText = Space(75)
            TempZ = IsProcOrTest(Trim(ReferenceTest.TestQuestion))
            Mid(DisplayText, 1, 8) = Left(ReferenceTest.TestDate, 2) + "-" + Mid(ReferenceTest.TestDate, 4, 2) + "-" + Mid(ReferenceTest.TestDate, 9, 2)
            SortDate = Mid(ReferenceTest.TestDate, 7, 4) + Mid(ReferenceTest.TestDate, 1, 2) + Mid(ReferenceTest.TestDate, 4, 2)
            Mid(DisplayText, 13, Len(Trim(ReferenceTest.TestQuestion))) = Trim(ReferenceTest.TestQuestion)
            ATemp1 = ""
            w = InStrPS(ReferenceTest.TestText, "/")
            If (w > 0) Then
                z = InStrPS(w + 1, ReferenceTest.TestText, "/")
                If (z > 0) Then
                    ATemp = Mid(ReferenceTest.TestText, w + 1, (z - 1) - w)
                    w = InStrPS(ATemp, "AM")
                    If (w = 0) Then
                        w = InStrPS(ATemp, "PM")
                        If (w > 0) Then
                            ATemp1 = Mid(ATemp, w + 3, (z - 1) - (w + 2))
                        End If
                    Else
                        If (w > 0) Then
                            ATemp1 = Mid(ATemp, w + 3, (z - 1) - (w + 2))
                        End If
                    End If
                End If
            End If
            If (Trim(ATemp1) <> "") Then
                Call StripCharacters(ATemp1, "(")
                Call StripCharacters(ATemp1, ")")
                Mid(DisplayText, 35, Len(Trim(ATemp1))) = Trim(ATemp1)
                ATemp1 = ""
            End If
            w = InStrPS(ReferenceTest.TestText, "OS:")
            If w = 0 Then
                w = Len(Trim$(ReferenceTest.TestText)) + 1
            End If
            If (w > 0) Then
                ATemp = Trim(Left(ReferenceTest.TestText, w - 1))
                ATemp1 = Trim(Mid(ReferenceTest.TestText, w, Len(Trim(ReferenceTest.TestText)) - (w - 1)))
                w = InStrPS(ATemp, ":")
                If (w > 0) Then
                    ATemp = Mid(ATemp, w + 1, Len(ATemp) - w)
                End If
                Call ReplaceCharacters(ATemp, "OD:", " ")
                Call ReplaceCharacters(ATemp, vbCrLf, " ")
                Call StripCharacters(ATemp, " ")
                Call ReplaceCharacters(ATemp1, "OS:", " ")
                Call ReplaceCharacters(ATemp1, vbCrLf, " ")
                Call StripCharacters(ATemp1, " ")
                If (Trim(ATemp) <> "") And (Trim(ATemp1) <> "") Then
                    Mid(DisplayText, 10, 2) = "OU"
                ElseIf (Trim(ATemp) <> "") Then
                    Mid(DisplayText, 10, 2) = "OD"
                ElseIf (Trim(ATemp1) <> "") Then
                    Mid(DisplayText, 10, 2) = "OS"
                Else
                    Mid(DisplayText, 10, 2) = "--"
                End If
            End If
            If (TempZ = "T") Then
                If (HstOn) Then
                    DisplayText = DisplayText + "T" + Trim(str(i))
                    lstSort1.AddItem SortDate + " " + ConvertInteger(ReferenceTest.TestClnId, False) + "R" + DisplayText
                    lstSort1.AddItem SortDate + " " + ConvertInteger(ReferenceTest.TestClnId, False) + "r" + DisplayText
                Else
                    DisplayText = DisplayText + "R" + Trim(str(i))
                    lstSort1.AddItem SortDate + " " + ConvertInteger(ReferenceTest.TestClnId, False) + "T" + DisplayText
                    lstSort1.AddItem SortDate + " " + ConvertInteger(ReferenceTest.TestClnId, False) + "t" + DisplayText
                End If
            ElseIf (TempZ = "P") Then
                If (HstOn) Then
                    DisplayText = DisplayText + "q" + Trim(str(i))
                    lstSort1.AddItem SortDate + " " + ConvertInteger(ReferenceTest.TestClnId, False) + "q" + DisplayText
                Else
                    DisplayText = DisplayText + "p" + Trim(str(i))
                    lstSort1.AddItem SortDate + " " + ConvertInteger(ReferenceTest.TestClnId, False) + "p" + DisplayText
                End If
            Else
                DisplayText = DisplayText + "E" + Trim(str(i))
                lstSort1.AddItem SortDate + " " + ConvertInteger(ReferenceTest.TestClnId, False) + "E" + DisplayText
            End If
        End If
    Next i
    Return
GetTestDoc:
    Temp1 = ""
    If (Trim(ATemp1) <> "") And (Trim(ATemp) <> "") Then
        ID = Val(Trim(Mid(ATemp1, 77, Len(ATemp1) - 76)))
        If (ID > 0) Then
            If (Mid(ATemp, 20, 1) = "R") Or (Mid(ATemp, 20, 1) = "r") Then
                Temp1 = Trim(QTestResultsStorage(ID).TestDoc)
            ElseIf (Mid(ATemp, 20, 1) = "T") Or (Mid(ATemp, 20, 1) = "t") Then
                Temp1 = Trim(TestResultsStorage(ID).TestDoc)
            ElseIf (Mid(ATemp, 20, 1) = "q") Then
                Temp1 = Trim(QTestResultsStorage(ID).TestDoc)
            ElseIf (Mid(ATemp, 20, 1) = "p") Then
                Temp1 = Trim(TestResultsStorage(ID).TestDoc)
            End If
        End If
    End If
    Return
SetupComplaints:
    SortDate = ""
    Temp = ""
    Temp1 = ""
    DisplayText = Space(75)
    If (RetCln.ClinicalStatus <> "X") Then
        Temp = Trim(RetCln.ClinicalSymptom)
        Temp1 = Trim(RetCln.ClinicalFindings)
        If (Left(Temp, 1) = "/") Or ((Left(Temp, 1) = "?") And (Af <> 1) And (Af <> 9)) Then
            If (GetComplaintsLingo(Temp, Temp1, ATemp, Af)) Then
                DocName = Trim(RetCln.ClinicalResourceName)
                ADate = RetCln.ClinicalApptDate
                SortDate = ADate
                ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
                'Mid(DisplayText, 1, 8) = Left(ADate, 2) + "-" + Mid(ADate, 4, 2) + "-" + Mid(ADate, 9, 2)
                If (Len(Trim(ATemp)) < 65) Then
                    Mid(DisplayText, 10, Len(ATemp)) = ATemp
                Else
                    Mid(DisplayText, 10, 65) = Left(ATemp, 65)
                End If
                DisplayText = DisplayText + "C" + Trim(str(RetCln.ClinicalId))
                If (Left(Temp, 1) = "/") Then
                    DisplayClnId = RetCln.ClinicalId
                End If
                If (SortDate < CurDate) Then
                    If (Af = 1) Then
                        Uf = Uf + 1
                        If (Uf <= MaxHistoryHPI) And (SortDate < CurDate) Then
                            HistHPIComplaints(Uf) = DisplayText
                        End If
                    ElseIf (Af = 9) Then
                        Mid(DisplayText, 76, 1) = "g"
                        lstSort1.AddItem SortDate + " " + ConvertInteger(DisplayClnId, False) + "g" + DisplayText
                    Else
                        lstSort1.AddItem SortDate + " " + ConvertInteger(DisplayClnId, False) + "E" + DisplayText
                    End If
                Else
                    If ((Af = 1) And Not (LocalEditPreviousOn)) Then
                        If (BDOn) Then
                            lstSort1.AddItem SortDate + " " + ConvertInteger(DisplayClnId, False) + "H" + DisplayText
                        Else
                            Uf = Uf + 1
                            If (Uf <= MaxHistoryHPI) Then
                                HistHPIComplaints(Uf) = DisplayText
                            End If
                        End If
                    ElseIf (Af = 1) And (LocalEditPreviousOn) Then
                        lstSort1.AddItem SortDate + " " + ConvertInteger(DisplayClnId, False) + "H" + DisplayText
                    ElseIf (Af = 9) Then
                        Mid(DisplayText, 76, 1) = "g"
                        lstSort1.AddItem SortDate + " " + ConvertInteger(DisplayClnId, False) + "g" + DisplayText
                    Else
                        lstSort1.AddItem SortDate + " " + ConvertInteger(DisplayClnId, False) + "E" + DisplayText
                    End If
                End If
            End If
        End If
    End If
    Return

SetupExternal:
    If (RetNote.NotesAppointmentId > 0) Then
        ATemp1 = Space(75)
        SortDate = ""
        z = InStrPS(RetNote.NotesText1, "[")
        z1 = InStrPS(RetNote.NotesText1, "]")
        If (z > 0) And (z1 > 0) Then
            ADate = Trim(Mid(RetNote.NotesText1, z + 1, (z1 - 1) - z))
            Call FormatTodaysDate(ADate, False)
        Else
            ADate = RetNote.NotesDate
            ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Mid(ADate, 1, 4)
        End If
        SortDate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        Mid(ATemp1, 1, 10) = SortDate
        ATemp = RetNote.NotesEye
        If (Trim(ATemp) = "") Then
            ATemp = "--"
        End If
        Mid(ATemp1, 11, 2) = ATemp
        NastyNote = ""
        z = InStrPS(RetNote.NotesText1, "(")
        If (z > 0) Then
            NastyNote = Trim(Left(RetNote.NotesText1, z - 1))
        Else
            NastyNote = Trim(RetNote.NotesText1)
        End If
        Mid(ATemp1, 13, 40) = NastyNote
        ATemp = ""
        z = InStrPS(RetNote.NotesText1, "(")
        z1 = InStrPS(RetNote.NotesText1, ")")
        If (z > 0) And (z1 > 0) And (z1 > z) Then
            ATemp = Trim(Mid(RetNote.NotesText1, z + 1, (z1 - 1) - z))
        End If
        Mid(ATemp1, 53, 20) = ATemp
        ATemp1 = ATemp1 + Trim(str(RetNote.NotesId))
        If (Trim(NastyNote) <> "") Then
            lstSort1.AddItem ATemp1
        End If
    End If
    Return
SetupNotes:
    If (RetNote.NotesAppointmentId > 0) Then
        SortDate = ""
        Call GetAppointmentDate(RetNote.NotesAppointmentId, ADate, DocName)
        SortDate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        ATemp = RetNote.NotesEye
        If (Trim(ATemp) = "") Then
            ATemp = "--"
        End If
        NastyNote = Trim(RetNote.NotesText1) + " " + Trim(RetNote.NotesText2) + " " + Trim(RetNote.NotesText3) + " " + Trim(RetNote.NotesText4)
        If (Trim(NastyNote) <> "") Then
            ATemp1 = Trim(NastyNote)
            Call StripCharacters(ATemp1, Chr(13))
            Call StripCharacters(ATemp1, Chr(10))
            z = 1
            While z < Len(ATemp1)
                ATemp2 = Mid(ATemp1, z, 38)
                For z1 = 38 To 1 Step -1
                    If (Len(ATemp2) < 38) Or (Mid(ATemp2, z1, 1) = " ") Or _
                       (Mid(ATemp2, z1, 1) = ".") Or (Mid(ATemp2, z1, 1) = ",") Then
                        Exit For
                    End If
                Next z1
                If (z1 < 1) Then
                    z1 = 38
                End If
                If (z + z1 >= Len(Trim(ATemp1))) Then
                    z1 = Len(ATemp2)
                End If
                GoSub WriteIt
                z = z + z1
            Wend
        End If
    End If
    Return
WriteIt:
    DisplayText = Space(200)
    If (z = 1) Then
        Mid(DisplayText, 1, 8) = Mid(ADate, 1, 2) + "-" + Mid(ADate, 4, 2) + "-" + Mid(ADate, 9, 2)
        Mid(DisplayText, 10, 2) = ATemp
    End If
    Mid(DisplayText, 13, z1) = Left(ATemp2, z1)
    DisplayText = DisplayText + "V" + Trim(str(RetNote.NotesId))
    If (Trim(RetNote.NotesSystem) = "") Then
        lstOrdersLV.ListItems.Add = Trim(str(RetNote.NotesId))
        lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(1) = Left(DisplayText, 8)
        lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(2) = Mid(DisplayText, 10, 2)
        lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(3) = Mid(DisplayText, 13, z1)
        lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(4) = ""
        lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(5) = "V"
        lstOrdersLV.ListItems.Item(lstOrdersLV.ListItems.Count).SubItems(6) = Trim(str(RetNote.NotesId))
    ElseIf (Trim(RetNote.NotesSystem) = "1") Then
        If (Not (LocalEditPreviousOn) And (ActiveDate < SortDate)) Or _
           ((LocalEditPreviousOn) And (RetNote.NotesAppointmentId = TempApptId)) Or (TempApptId < 1) Then
            lstHPILV.ListItems.Add = Trim(str(RetNote.NotesId))
            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(1) = Left(DisplayText, 8)
            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(2) = Mid(DisplayText, 10, 2)
            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(3) = Mid(DisplayText, 13, z1)
            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(4) = ""
            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(5) = "V"
            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(6) = Trim(str(RetNote.NotesId))
        Else
            If (BDOn) And (ActiveDate = SortDate) Then
                lstHPILV.ListItems.Add = Trim(str(RetNote.NotesId))
                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(1) = Left(DisplayText, 8)
                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(2) = Mid(DisplayText, 10, 2)
                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(3) = Mid(DisplayText, 13, z1)
                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(4) = ""
                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(5) = "V"
                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(6) = Trim(str(RetNote.NotesId))
            Else
                Uf = Uf + 1
                If (Uf <= MaxHistoryHPI) Then
                    HistHPIComplaints(Uf) = DisplayText
                End If
            End If
        End If
    ElseIf (Trim(RetNote.NotesSystem) = "9") Then
        lstAllergies.ListItems.Add = Trim(str(RetNote.NotesId))
        lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(1) = Left(DisplayText, 8)
        lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(2) = Mid(DisplayText, 10, 2)
        lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(3) = Mid(DisplayText, 13, z1)
        lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(4) = ""
        lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(5) = "V"
        lstAllergies.ListItems.Item(lstAllergies.ListItems.Count).SubItems(6) = Trim(str(RetNote.NotesId))
    Else
        lstExamLV.ListItems.Add = Trim(str(RetNote.NotesId))
        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(1) = Left(DisplayText, 8)
        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(2) = Mid(DisplayText, 10, 2)
        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(3) = Mid(DisplayText, 13, z1)
        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(4) = ""
        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(5) = "V"
        lstExamLV.ListItems.Item(lstExamLV.ListItems.Count).SubItems(6) = Trim(str(RetNote.NotesId))
    End If
    Return
SetupFindings:
    If (RetCln.ClinicalStatus <> "X") And (ApptId >= RetCln.ClinicalApptId) Then
        SortDate = ""
        ADate = Mid(RetCln.ClinicalApptDate, 5, 2) + "/" + Mid(RetCln.ClinicalApptDate, 7, 2) + "/" + Left(RetCln.ClinicalApptDate, 4)
        DocName = Trim(RetCln.ClinicalResourceName)
        SortDate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        DisplayText = Space(75)
        LDiag = Trim(RetCln.ClinicalFindings)
        TheSystem = ""
        TheText = ""
        Temp = RetCln.ClinicalEyeContext
        If (Trim(Temp) = "") Then
            Temp = "OU"
        End If
        If (GetDiagnosis(TheSystem, LDiag, TheText, 0)) Then
            Mid(DisplayText, 1, 8) = Mid(ADate, 9, 2) + "-" + Mid(ADate, 1, 2) + "-" + Mid(ADate, 4, 2)
            Mid(DisplayText, 10, Len(TheText) + 4) = Temp + ": " + TheText
            DisplayText = DisplayText + Trim(str(RetCln.ClinicalId))
            Mid(DisplayText, 75, 1) = "F"
            lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + "E" + DisplayText
        End If
    End If
    Return
SetupProcs:
    If (RetCln.ClinicalStatus <> "X") And (ApptId >= RetCln.ClinicalApptId) Then
        SortDate = ""
        ADate = RetCln.ClinicalApptDate
        ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
        DocName = Trim(RetCln.ClinicalResourceName)
        SortDate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        DisplayText = Space(75)
        LDiag = Trim(RetCln.ClinicalFindings)
        TheSystem = ""
        TheText = ""
        If (GetProc(LDiag, TheText) > 0) Then
            Mid(DisplayText, 1, 8) = Mid(ADate, 1, 2) + "-" + Mid(ADate, 4, 2) + "-" + Mid(ADate, 9, 2)
            Mid(DisplayText, 10, Len(TheText) + 4) = TheText
            DisplayText = DisplayText + Trim(str(RetCln.ClinicalId))
            Mid(DisplayText, 75, 1) = TempZ
            lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + "E" + DisplayText
        End If
    End If
    Return
SetupDiags:
    TDisplay = ""
    SortDate = ""
    ADate = RetCln.ClinicalApptDate
    SortDate = ADate
    ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
    DocName = Trim(RetCln.ClinicalResourceName)
    If (SortDate <= CurDate) Then
        DisplayText = Space(75)
        LDiag = Trim(RetCln.ClinicalFindings)
        h = 0
        For p = 0 To lstSort1.ListCount - 1
            If (Mid(lstSort1.List(p), 20, 1) = MyTest) Then
                If (Trim(Mid(lstSort1.List(p), 85, 10)) = LDiag) Then
                    Temp = RetCln.ClinicalEyeContext
                    If (Trim(Temp) = "") Then
                        Temp = "--"
                    End If
                    If (Temp = Mid(lstSort1.List(p), 30, 2)) Then
                        lstSort1.RemoveItem p
                        Exit For
                    End If
                End If
            End If
        Next p
        TheSystem = ""
        TheText = ""
        Temp = RetCln.ClinicalEyeContext
        If (Trim(Temp) = "") Then
            Temp = "--"
        End If
        If (GetDiagnosis(TheSystem, LDiag, TheText, 0)) Then
            OtherText = ""
            If (InStrPS(RetCln.ClinicalSymptom, CntRefSet) <> 0) Then
                OtherText = "NF-"
            End If
            If (InStrPS(RetCln.ClinicalSymptom, HORefSet) <> 0) Then
                OtherText = OtherText + "HO-"
            End If
            If (InStrPS(RetCln.ClinicalSymptom, RORefSet) <> 0) Then
                OtherText = OtherText + "RO-"
            End If
            Quant = ""
            w = InStrPS(RetCln.ClinicalSymptom, "[")
            If (w > 0) Then
                g = InStrPS(w, RetCln.ClinicalSymptom, "]")
                If (g > 0) Then
                    Quant = Mid(RetCln.ClinicalSymptom, w, g - (w - 1))
                End If
            End If
            OtherText = OtherText + Trim(Quant)
            Mid(DisplayText, 1, 8) = Mid(ADate, 1, 2) + "-" + Mid(ADate, 4, 2) + "-" + Mid(ADate, 9, 2)
            Mid(DisplayText, 10, Len(TheText) + Len(OtherText) + 4) = Temp + " " + OtherText + TheText
            Mid(DisplayText, 65, Len(Trim(LDiag))) = Trim(LDiag)
            DisplayText = DisplayText + Trim(str(RetCln.ClinicalId))
            If (ITriggerOn) Then
                If (LDiag = PrimaryDiagnosis) Then
                    TDisplay = SortDate + Trim(str(("9"))) + ConvertInteger(RetCln.ClinicalId, False) + MyTest + DisplayText
                Else
                    TDisplay = SortDate + Trim(str((10 - Val(Left(RetCln.ClinicalSymptom, 1))))) + ConvertInteger(RetCln.ClinicalId, False) + MyTest + DisplayText
                End If
            Else
                TDisplay = SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + MyTest + DisplayText
            End If
            InactiveDiag = False
            Temp1 = LDiag
            ApptIdA = RetCln.ClinicalApptId
            GoSub CheckILPN
        End If
    End If
    Return
CheckILPN:
    If (Trim(Temp1) <> "") And (Trim(TDisplay) <> "") Then
        VampOn = False
        Set RetNote = New PatientNotes
        RetNote.NotesPatientId = PatId
        RetNote.NotesAppointmentId = ApptIdA
        RetNote.NotesType = "C"
        RetNote.NotesHighlight = ""
        RetNote.NotesSystem = "IMPR-" + Temp1
        If (RetNote.FindNotes > 0) Then
            j = 1
            Do Until Not (RetNote.SelectNotes(j))
                If (Trim(RetNote.NotesILPNRef) <> "") Then
                    ZTemp = Trim(RetNote.NotesILPNRef)
                    If (Len(ZTemp) > 40) Then
                        ZTemp = Left(ZTemp, 40)
                    End If
                    If (Trim(RetNote.NotesEye) = "") Then
                        Mid(TDisplay, 30, 2) = "--"
                    Else
                        Mid(TDisplay, 30, 2) = RetNote.NotesEye
                    End If

                    If (InStrPS(UCase(RetNote.NotesILPNRef), "DICTATED") = 0) And (InStrPS(UCase(RetNote.NotesILPNRef), "AUDIO") = 0) Then
                        Mid(TDisplay, 33, Len(ZTemp)) = ZTemp
                        Mid(TDisplay, 33 + Len(ZTemp), 85 - (33 + Len(ZTemp))) = Space(85 - (33 + Len(ZTemp)))
                    Else
                        ZTemp = Trim(RetNote.NotesILPNRef)
                        If (Len(ZTemp) > 20) Then
                            ZTemp = Left(ZTemp, 20)
                        End If
                        Mid(TDisplay, 53, Len(ZTemp)) = ZTemp
                        Mid(TDisplay, 53 + Len(ZTemp), 85 - (53 + Len(ZTemp))) = Space(85 - (53 + Len(ZTemp)))
                    End If
                    lstSort1.AddItem TDisplay
                    VampOn = True
                    Exit Do
                End If
                j = j + 1
            Loop
            If Not (VampOn) Then
                lstSort1.AddItem TDisplay
            End If
        Else
            lstSort1.AddItem TDisplay
        End If
        Set RetNote = Nothing
    End If
    Return
SetupRX:
    PrescribeOn = True
    If (RetCln.ClinicalStatus <> "X") And (Trim(RetCln.ClinicalFindings) <> "") And (RetCln.ClinicalStatus <> "D") Then
        If (InStrPS(RetCln.ClinicalFindings, "P-7") = 0) Then
            PrescribeOn = False
            h = InStrPS(RetCln.ClinicalFindings, "-2/")
            If (h > 0) Then
                TDisplay = Trim(Mid(RetCln.ClinicalFindings, 6, (h - 1) - 5))
                If (IsDrugOkay(TDisplay) = "O") Then
                    PrescribeOn = True
                End If
            End If
        End If
        If (PrescribeOn) Then
            If (Len(ADate) < 10) Then
                ADate = Mid(ActiveDate, 5, 2) + "/" + Mid(ActiveDate, 7, 2) + "/" + Left(ActiveDate, 4)
            End If
            If (InStrPS("!K^CDR", Left(RetCln.ClinicalDescriptor, 1)) <> 0) Then
                TestDate = ADate
                If (Len(ADate) < 10) Then
                    TestDate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
                End If
                Call GetRxData(RetCln.ClinicalFindings, RetCln.ClinicalDescriptor, TDisplay, AEye, TestDate, AFreq, AStat)
                If (Trim(TDisplay) <> "") Then
                    Duped = False
                    For w = lstOcuMeds.ListItems.Count To 1 Step -1
                        If (TDisplay = lstOcuMeds.ListItems.Item(w).SubItems(4)) Then
                            Duped = True
                        End If
                    Next w
                    If Not (Duped) Then
                        lstOcuMeds.ListItems.Add = Trim(str(RetCln.ClinicalId))
                        lstOcuMeds.ListItems.Item(lstOcuMeds.ListItems.Count).SubItems(1) = Left(TestDate, 6) + Mid(TestDate, 9, 2)
                        lstOcuMeds.ListItems.Item(lstOcuMeds.ListItems.Count).SubItems(2) = AEye
                        lstOcuMeds.ListItems.Item(lstOcuMeds.ListItems.Count).SubItems(3) = AStat
                        lstOcuMeds.ListItems.Item(lstOcuMeds.ListItems.Count).SubItems(4) = TDisplay
                        lstOcuMeds.ListItems.Item(lstOcuMeds.ListItems.Count).SubItems(5) = AFreq
                    End If
                End If
            ElseIf (Left(RetCln.ClinicalDescriptor, 1) = "C") Then
' find the same active drug
                For w = lstOcuMeds.ListItems.Count To 1 Step -1
                    If (TDisplay = lstOcuMeds.ListItems.Item(w).SubItems(4)) Then
                        lstOcuMeds.ListItems.Item(w).SubItems(3) = "Rvisd"
                    End If
                Next w
            End If
        End If
    End If
    Return
SetupOrders:
    PrescribeOn = True
    SplitOn = True
    If (RetCln.ClinicalStatus <> "X") And (Trim(RetCln.ClinicalFindings) <> "") And (RetCln.ClinicalStatus <> "D") Then
        ATemp = "/"
        If (InStrPS(UCase(RetCln.ClinicalFindings), "OFFICE PROCEDURE") > 0) Then
            SplitOn = False
            ATemp = "-F/"
        ElseIf (InStrPS(UCase(RetCln.ClinicalFindings), "ORDER A TEST") > 0) Or (InStrPS(UCase(RetCln.ClinicalFindings), "SCHEDULE SURGERY") > 0) Then
            SplitOn = False
            ATemp = "-1/"
        End If
        SortDate = ""
        DocName = Trim(RetCln.ClinicalResourceName)
        SortDate = RetCln.ClinicalApptDate
        ADate = Mid(RetCln.ClinicalApptDate, 5, 2) + "/" + Mid(RetCln.ClinicalApptDate, 7, 2) + "/" + Left(RetCln.ClinicalApptDate, 4)
        DisplayText = Space(200)
        p = InStrPS(RetCln.ClinicalFindings, "/")
        If (p < 4) Then
            p = -1
            Temp = Trim(RetCln.ClinicalFindings)
            If (Trim(Temp) <> "") Then
                p = 0
            End If
        Else
            g = p + 1
            p = InStrPS(g, RetCln.ClinicalFindings, ATemp)
        End If
        If (p < 1) Then
            Opr = "E"
            If (InStrPS(UCase(RetCln.ClinicalFindings), "ORDER A TEST") > 0) Then
                Opr = "O"
            ElseIf (InStrPS(UCase(RetCln.ClinicalFindings), "OFFICE PROCEDURE") > 0) Then
                Opr = "P"
            ElseIf (InStrPS(UCase(RetCln.ClinicalFindings), "SCHEDULE SURGERY") > 0) Then
                Opr = "Y"
            End If
            DisplayText = Space(200)
            Temp = RetCln.ClinicalFindings
            If (InStrPS(UCase(Temp), "ORDER A TEST") = 0) And _
               (InStrPS(UCase(Temp), "SURGERY") = 0) And _
               (InStrPS(UCase(Temp), "OFFICE PROCEDURE") = 0) Then
                Mid(DisplayText, 1, 8) = Left(ADate, 2) + "-" + Mid(ADate, 4, 2) + "-" + Mid(ADate, 9, 2)
                Mid(DisplayText, 10, Len(Temp)) = "--"
                Mid(DisplayText, 13, Len(Temp)) = Temp
                DisplayText = DisplayText + Trim(str(RetCln.ClinicalId))
                lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + Opr + DisplayText
            End If
            Temp = ""
            g = 0
            h = 0
        ElseIf (p > 0) Then
            Mid(DisplayText, 1, 8) = Left(ADate, 2) + "-" + Mid(ADate, 4, 2) + "-" + Mid(ADate, 9, 2)
            If (InStrPS(RetCln.ClinicalFindings, "/OU") <> 0) Then
                Mid(DisplayText, 10, 2) = "OU"
            ElseIf (InStrPS(RetCln.ClinicalFindings, "/OD") <> 0) Then
                Mid(DisplayText, 10, 2) = "OD"
            ElseIf (InStrPS(RetCln.ClinicalFindings, "/OS") <> 0) Then
                Mid(DisplayText, 10, 2) = "OS"
            Else
                If (InStrPS(RetCln.ClinicalFindings, "(OU)") <> 0) Then
                    Mid(DisplayText, 10, 2) = "OU"
                ElseIf (InStrPS(RetCln.ClinicalFindings, "(OD)") <> 0) Then
                    Mid(DisplayText, 10, 2) = "OD"
                ElseIf (InStrPS(RetCln.ClinicalFindings, "(OS)") <> 0) Then
                    Mid(DisplayText, 10, 2) = "OS"
                Else
                    Mid(DisplayText, 10, 2) = "--"
                End If
            End If
            If (InStrPS(UCase(RetCln.ClinicalFindings), "ORDER A TEST") > 0) Or _
               (InStrPS(UCase(RetCln.ClinicalFindings), "OFFICE PROCEDURE") > 0) Or _
               (InStrPS(UCase(RetCln.ClinicalFindings), "SCHEDULE SURGERY") > 0) Then
                If (InStrPS(UCase(RetCln.ClinicalFindings), "ORDER A TEST") > 0) Then
                    Opr = "O"
                ElseIf (InStrPS(UCase(RetCln.ClinicalFindings), "OFFICE PROCEDURE") > 0) Then
                    Opr = "P"
                ElseIf (InStrPS(UCase(RetCln.ClinicalFindings), "SCHEDULE SURGERY") > 0) Then
                    Opr = "Y"
                Else
                    Opr = " "
                End If
                z = InStrPS(RetCln.ClinicalFindings, "/")
                ATemp = Trim(Mid(RetCln.ClinicalFindings, z + 1, Len(RetCln.ClinicalFindings) - z))
                Temp = ""
                w = 1
                While (w > 0)
                    Call GetReferenceFromAction(ATemp, w, "/", ATemp1)
                    If (ATemp1 <> "") And (Len(Trim(ATemp1)) > 2) And (InStrPS(ATemp1, "<") = 0) Then
                        If (InStrPS(ATemp1, "-0") = 0) And (InStrPS(ATemp1, "-1") = 0) And (InStrPS(ATemp1, "-O") = 0) And (InStrPS(ATemp1, "-T") = 0) And (InStrPS(ATemp1, "-B") = 0) Then
                            z = InStrPS(ATemp1, ":")
                            If (z > 0) Then
                                ATemp1 = Mid(ATemp1, z + 1, Len(ATemp1) - z)
                            Else
                                Temp = ""
                            End If
                            z = InStrPS(ATemp1, "-")
                            If (z > 0) Then
                                ATemp1 = Left(ATemp1, z - 1)
                                z = InStrPS(ATemp1, "<")
                                If (z > 0) Then
                                    ATemp1 = Left(ATemp1, z - 1)
                                End If
                            End If
                            z = InStrPS(RetCln.ClinicalFindings, ATemp1 + ATemp1 + ":")
                            If (z > 0) Then
                                h = InStrPS(z, RetCln.ClinicalFindings, "/")
                                If (h > 0) Then
                                    ATemp1 = ATemp1 + ":" + Mid(RetCln.ClinicalFindings, z + (2 * Len(ATemp1)) + 1, (h - 3) - (z + (2 * Len(ATemp1))))
                                End If
                                Mid(DisplayText, 13, Len(ATemp1)) = ATemp1
                                DisplayText = DisplayText + Opr + Trim(str(RetCln.ClinicalId))
                                If (PrescribeOn) Then
                                    lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + Opr + DisplayText
                                    If (TempApptId = RetCln.ClinicalApptId) And (Opr = "w") And (InStrPS(RetCln.ClinicalFindings, "P-7") <> 0) Then
                                        lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + "a" + DisplayText
                                    End If
                                End If
                                DisplayText = Left(DisplayText, 15) + Space(185)
                                Temp = ""
                                w = h
                            Else
                                ATemp1 = Temp + ATemp1
                                Mid(DisplayText, 13, Len(ATemp1)) = ATemp1
                                DisplayText = DisplayText + Opr + Trim(str(RetCln.ClinicalId))
                                If (PrescribeOn) Then
                                    lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + Opr + DisplayText
                                    If (TempApptId = RetCln.ClinicalApptId) And (Opr = "w") And (InStrPS(RetCln.ClinicalFindings, "P-7") <> 0) Then
                                        lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + "a" + DisplayText
                                    End If
                                End If
                                DisplayText = Left(DisplayText, 12) + Space(188)
                                Temp = ""
                            End If
                        ElseIf (InStrPS(ATemp1, "-0") > 0) Then
                            Temp = ""
                            z = InStrPS(ATemp1, "-0")
                            If (z > 0) Then
                                Temp = Left(ATemp1, z - 1) + ":"
                            End If
                        ElseIf (InStrPS(ATemp1, "-1") > 0) Then
                            If (InStrPS(ATemp, "<^") = 0) Then
                                If (Temp = "") Then
                                    z = InStrPS(ATemp1, "-1")
                                    If (z > 0) Then
                                        If (Trim(Temp) = "") Then
                                            Temp = Left(ATemp1, z - 1)
                                        End If
                                    End If
                                End If
                            ElseIf (Trim(Temp) = "") Then
                                z = InStrPS(ATemp1, "-")
                                If (z > 0) Then
                                    If (Trim(Temp) = "") Then
                                        Temp = Left(ATemp1, z - 1)
                                    End If
                                End If
                            End If
                        ElseIf (InStrPS(ATemp1, "-T") > 0) Then
                            Temp = ""
                            z = InStrPS(ATemp1, "-T")
                            If (z > 0) Then
                                Temp = Left(ATemp1, z - 1) + ":"
                            End If
                        ElseIf (InStrPS(ATemp1, "-B") > 0) Then
                            Temp = ""
                            z = InStrPS(ATemp1, "-B")
                            If (z > 0) Then
                                Temp = Left(ATemp1, z - 1) + ":"
                            End If
                        End If
                    Else
                        w = 0
                        If (Trim(Temp) <> "") Then
                            ATemp1 = Temp
                            Call StripCharacters(ATemp1, ":")
                            Mid(DisplayText, 13, Len(ATemp1)) = ATemp1
                            DisplayText = DisplayText + Opr + Trim(str(RetCln.ClinicalId))
                            If (PrescribeOn) Then
                                lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + Opr + DisplayText
                                If (TempApptId = RetCln.ClinicalApptId) And (Opr = "w") And (InStrPS(RetCln.ClinicalFindings, "P-7") <> 0) Then
                                    lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + "a" + DisplayText
                                End If
                            End If
                            DisplayText = Left(DisplayText, 15) + Space(185)
                            DisplayText = ""
                            w = 0
                        End If
                    End If
                Wend
                Temp = ""
            ElseIf (InStrPS(UCase(RetCln.ClinicalFindings), "SCHEDULE SURGERY") > 0) Then
                Temp = Mid(RetCln.ClinicalFindings, g, (p - 3) - (g - 1))
                z = InStrPS(Temp, "(")
                If (z > 0) Then
                    Temp = Trim(Left(Temp, z - 1))
                End If
            Else
                h = InStrPS(RetCln.ClinicalFindings, "-2/")
                If (h < g) And (h > 0) Then
                    Temp = Left(RetCln.ClinicalFindings, h - 1) + " "
                ElseIf (h > g) And (h > 0) Then
                    Temp = Trim(Left(RetCln.ClinicalFindings, g)) + " "
                    If (Len(Temp) > 4) Then
                        z = InStrPS(Temp, "-")
                        If (z > 0) Then
                            Temp = Left(Temp, z - 1)
                            Temp = Temp + " "
                        End If
                    End If
                ElseIf (h < 1) Then
                    If (g > 0) Then
                        Temp = Trim(Left(RetCln.ClinicalFindings, g)) + " "
                        If (Len(Temp) > 4) Then
                            z = InStrPS(Temp, "-")
                            If (z > 0) Then
                                Temp = Left(Temp, z - 1)
                                Temp = Temp + " "
                            End If
                        End If
                    End If
                End If
                If ((p - 3) > (g - 1)) Then
                    Temp = Temp + Mid(RetCln.ClinicalFindings, g, (p - 3) - (g - 1))
                End If
                g = p + 1
                p = InStrPS(g, RetCln.ClinicalFindings, "/")
                If (p > 0) And ((p - 3) - (g - 1) > 0) Then
                    Temp = Temp + " " + Mid(RetCln.ClinicalFindings, g, (p - 3) - (g - 1))
                End If
                Opr = "a"
            End If
            If (Opr = "") Then
                If (InStrPS("PD", Left(RetCln.ClinicalDescriptor, 1)) <> 0) Then
                    Temp = ""
                End If
            End If
            If (Trim(Temp) <> "") Or (p < 1) Then
                Mid(DisplayText, 13, Len(Temp)) = Temp
                DisplayText = DisplayText + " " + Trim(str(RetCln.ClinicalId))
                If (Opr = "") Then
                    Opr = "a"
                End If
                If (PrescribeOn) Then
                    lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + Opr + DisplayText
                    If (TempApptId = RetCln.ClinicalApptId) And (Opr = "w") And (InStrPS(RetCln.ClinicalFindings, "P-7") <> 0) Then
                        lstSort1.AddItem SortDate + " " + ConvertInteger(RetCln.ClinicalId, False) + "a" + DisplayText
                    End If
                End If
                DisplayText = Left(DisplayText, 200)
                Temp = ""
                DisplayText = ""
                w = 0
            End If
        End If
    End If
    Return
SetupStandardTests:
    If (Trim(AText) <> "") And (SlotId > 0) Then
        Call SetupStandardTests(AText, SlotId)
    End If
    Return
UI_ErrorHandler:
    Call PinpointError("LoadHighlights", Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume LeaveFast
LeaveFast:
End Function

Private Function SetupStandardTests(AText As String, SlotId As Integer) As Boolean
Dim j As Integer, p As Integer
Dim q As Integer, w1 As Integer
Dim ATextOD As String
Dim ATextOS As String
If (Trim(AText) <> "") And (SlotId > 0) Then
    ATextOD = ""
    ATextOS = ""
    j = InStrPS(AText, "OD:")
    p = InStrPS(AText, "OS:")
    q = InStrPS(AText, "Pinhole")
    If (q < 1) Then
        q = InStrPS(AText, "Pin")
    End If
    If (q > j) Or (q = 0) Then
        If (j > 0) And (p > 0) And (p > j) Then
            ATextOD = Trim(Mid(AText, j + 3, (p - 1) - (j + 2)))
            If (q > 0) And (q > p) Then
                ATextOS = Trim(Mid(AText, p + 3, (q - 1) - (p + 2)))
            Else
                ATextOS = Trim(Mid(AText, p + 3, Len(AText) - (p + 2)))
            End If
        ElseIf (p > 0) Then
            ATextOD = ""
            If (q > 0) And (q > p) Then
                ATextOS = Trim(Mid(AText, p + 3, (q - 1) - (p + 2)))
            Else
                ATextOS = Trim(Mid(AText, p + 3, Len(AText) - (p + 2)))
            End If
        ElseIf (j > 0) Then
            If (q > 0) And (q > j) Then
                ATextOD = Trim(Mid(AText, j + 3, (q - 1) - (j + 2)))
            Else
                ATextOD = Trim(Mid(AText, j + 3, Len(AText) - (j + 2)))
            End If
            ATextOS = ""
        End If
        Call StripCharacters(ATextOD, Chr(13))
        Call StripCharacters(ATextOD, Chr(10))
        Call StripCharacters(ATextOS, Chr(13))
        Call StripCharacters(ATextOS, Chr(10))
        If (SlotId = 5) Then
            w1 = InStrPS(ATextOD, " ")
            If (w1 > 0) Then
                ATextOD = Left(ATextOD, w1 - 1)
            End If
            w1 = InStrPS(ATextOS, " ")
            If (w1 > 0) Then
                ATextOS = Left(ATextOS, w1 - 1)
            End If
        End If
        lstCTests.ListItems.Item(1).SubItems(SlotId) = Trim(ATextOD)
        lstCTests.ListItems.Item(2).SubItems(SlotId) = Trim(ATextOS)
    End If
' This Gets Pinhole values or generally any test result
    If (q > 0) Then
        AText = Mid(AText, q, Len(AText) - (q - 1))
        ATextOD = ""
        ATextOS = ""
        j = InStrPS(AText, "OD:")
        p = InStrPS(AText, "OS:")
        If (j > 0) And (p > 0) And (p > j) Then
            ATextOD = Mid(AText, j + 3, (p - 1) - (j + 2))
            ATextOS = Mid(AText, p + 3, Len(AText) - (p + 2))
        ElseIf (p > 0) And (q < p) Then
            ATextOD = ""
            ATextOS = Mid(AText, p + 3, Len(AText) - (p + 2))
        ElseIf (j > 0) Then
            ATextOD = Mid(AText, j + 3, Len(AText) - (j + 2))
        End If
        Call StripCharacters(ATextOD, Chr(13))
        Call StripCharacters(ATextOD, Chr(10))
        Call StripCharacters(ATextOS, Chr(13))
        Call StripCharacters(ATextOS, Chr(10))
        lstCTests.ListItems.Item(1).SubItems(SlotId + 1) = Trim(ATextOD)
        lstCTests.ListItems.Item(2).SubItems(SlotId + 1) = Trim(ATextOS)
    End If
End If
End Function

Private Function GetDiagnosis(TheSystem As String, TheDiagnosis As String, TheName As String, TheLevel As Integer) As Boolean
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
GetDiagnosis = False
TheName = ""
If (Trim(TheDiagnosis) <> "") Then
    Set RetrieveDiagnosis = New DiagnosisMasterPrimary
    RetrieveDiagnosis.PrimarySystem = TheSystem
    RetrieveDiagnosis.PrimaryDiagnosis = ""
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = TheDiagnosis
    RetrieveDiagnosis.PrimaryLevel = TheLevel
    RetrieveDiagnosis.PrimaryRank = 0
    If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
        If (RetrieveDiagnosis.SelectPrimary(1)) Then
            If (Trim(RetrieveDiagnosis.PrimaryShortName) <> "") Then
                TheName = RetrieveDiagnosis.PrimaryShortName
            ElseIf (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                TheName = RetrieveDiagnosis.PrimaryLingo
            Else
                TheName = RetrieveDiagnosis.PrimaryName
            End If
            If (TheSystem = "") Then
                TheSystem = RetrieveDiagnosis.PrimarySystem
            End If
            GetDiagnosis = True
        End If
    End If
    Set RetrieveDiagnosis = Nothing
End If
End Function

Private Function GetProc(TheProc As String, TheName As String) As Long
Dim RetProc As DiagnosisMasterProcedures
Set RetProc = New DiagnosisMasterProcedures
GetProc = 0
TheName = ""
RetProc.ProcedureCPT = Trim(TheProc)
RetProc.ProcedureDiagnosis = ""
RetProc.ProcedureRank = 0
If (RetProc.FindProcedure > 0) Then
    If (RetProc.SelectProcedure(1)) Then
        TheName = Trim(RetProc.ProcedureName)
        GetProc = RetProc.ProcedureId
    End If
End If
Set RetProc = Nothing
End Function

Private Function SetTestFiles(PatId As Long, TestType As Boolean, HistOn As Boolean) As Boolean
On Error GoTo UI_ErrorHandler
Dim QryId As Long
Dim TestOn As Boolean
Dim ApptId As Long, CurApptId As Long
Dim ClnId As Long, CurClnId As Long
Dim i As Integer
Dim TestFileNum As Integer
Dim Rec As String, Temp As String
Dim DocName As String, TId As String
Dim ADate As String, TheLingo As String
Dim TheText As String, LocalText As String
Dim CurrentTestName As String
Dim SchedulerTestFile As String
SetTestFiles = False
TestFileNum = FreeFile
SchedulerTestFile = DoctorInterfaceDirectory + Trim(str(PatId)) + "TestFile.txt"
If (TestType) Then
    TId = "T"
Else
    TId = "R"
End If
If (RetCln.LocateStartingRecord("F")) Then
    QryId = 1
    CurApptId = 0
    ClnId = 0
    CurClnId = 0
    TestOn = False
    CurrentTestName = ""
    Do Until Not (RetCln.RetrieveHighlightClinicalItem(QryId))
        If (TId = RetCln.ClinicalHighlights) And (RetCln.ClinicalType = "F") Then
            If (Mid(RetCln.ClinicalSymptom, 1, 1) = "/") And _
               (CurrentTestName <> RetCln.ClinicalSymptom) And (TestOn) Then
                FM.CloseFile CLng(TestFileNum)
                If (TestType) Then
                    Call PushList(SchedulerTestFile, 0, ApptId, ADate, CurClnId, DocName, HistOn)
                Else
                    Call PushList(SchedulerTestFile, 99, ApptId, ADate, CurClnId, DocName, HistOn)
                End If
                ApptId = RetCln.ClinicalApptId
                ClnId = RetCln.ClinicalId
                CurClnId = RetCln.ClinicalId
                CurApptId = RetCln.ClinicalApptId
                ADate = RetCln.ClinicalApptDate
                ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
                DocName = Trim(RetCln.ClinicalResourceName)
                FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
                Print #TestFileNum, "QUESTION=" + Trim(RetCln.ClinicalSymptom)
                Print #TestFileNum, "DATE=" + Trim(ADate)
                Print #TestFileNum, Trim(RetCln.ClinicalFindings) + " " + Trim(RetCln.ClinicalInstructions)
                CurrentTestName = RetCln.ClinicalSymptom
                TestOn = True
            ElseIf (CurApptId <> RetCln.ClinicalApptId) And (CurApptId <> 0) Then
                FM.CloseFile CLng(TestFileNum)
                If (TestType) Then
                    Call PushList(SchedulerTestFile, 0, ApptId, ADate, CurClnId, DocName, HistOn)
                Else
                    Call PushList(SchedulerTestFile, 99, ApptId, ADate, CurClnId, DocName, HistOn)
                End If
                ClnId = RetCln.ClinicalId
                CurClnId = RetCln.ClinicalId
                ApptId = RetCln.ClinicalApptId
                CurApptId = RetCln.ClinicalApptId
                ADate = RetCln.ClinicalApptDate
                ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
                DocName = Trim(RetCln.ClinicalResourceName)
                FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
                Print #TestFileNum, "QUESTION=" + Trim(RetCln.ClinicalSymptom)
                Print #TestFileNum, "DATE=" + Trim(ADate)
                Print #TestFileNum, Trim(RetCln.ClinicalFindings) + " " + Trim(RetCln.ClinicalInstructions)
                CurrentTestName = RetCln.ClinicalSymptom
                TestOn = True
            ElseIf (Not TestOn) Then
                ApptId = RetCln.ClinicalApptId
                CurApptId = RetCln.ClinicalApptId
                ClnId = RetCln.ClinicalId
                CurClnId = RetCln.ClinicalId
                ADate = RetCln.ClinicalApptDate
                ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
                DocName = Trim(RetCln.ClinicalResourceName)
                FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
                Print #TestFileNum, "QUESTION=" + Trim(RetCln.ClinicalSymptom)
                Print #TestFileNum, "DATE=" + Trim(ADate)
                Print #TestFileNum, Trim(RetCln.ClinicalFindings) + " " + Trim(RetCln.ClinicalInstructions)
                CurrentTestName = RetCln.ClinicalSymptom
                TestOn = True
            Else
                If (TestOn) Then
                    Print #TestFileNum, Trim(RetCln.ClinicalFindings) + " " + Trim(RetCln.ClinicalInstructions)
                End If
            End If
        ElseIf (RetCln.ClinicalType <> "F") Then
            Exit Do
        End If
        QryId = QryId + 1
    Loop
End If
If (TestOn) Then
    FM.CloseFile CLng(TestFileNum)
    If (TestType) Then
        Call PushList(SchedulerTestFile, 0, ApptId, ADate, CurClnId, DocName, HistOn)
    Else
        Call PushList(SchedulerTestFile, 99, ApptId, ADate, CurClnId, DocName, HistOn)
    End If
End If
If (FM.IsFileThere(SchedulerTestFile)) Then
    FM.Kill SchedulerTestFile
End If
SetTestFiles = True
Exit Function
UI_ErrorHandler:
    SetTestFiles = True
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetOtherTestResult(PatId As Long, ApptId As Long, TestId As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim CurApptId As Long
Dim ClnId As Long
Dim CurClnId As Long
Dim i As Integer
Dim TestFileNum As Integer
Dim DocName As String
Dim Rec As String, Temp As String
Dim ADate As String, TheLingo As String
Dim TheText As String, LocalText As String
Dim TestOn As Boolean
Dim CurrentTestName As String
Dim SchedulerTestFile As String
Dim RetrievalClinical As PatientClinical
GetOtherTestResult = False
TestFileNum = FreeFile
SchedulerTestFile = DoctorInterfaceDirectory + Trim(str(PatId)) + "TestFile.txt"
Set RetrievalClinical = New PatientClinical
RetrievalClinical.PatientId = PatientId
RetrievalClinical.AppointmentId = ApptId
RetrievalClinical.ClinicalType = "F"
RetrievalClinical.Symptom = "/" + TestId
RetrievalClinical.Findings = ""
RetrievalClinical.ImageDescriptor = ""
RetrievalClinical.ImageInstructions = ""
RetrievalClinical.ClinicalHighlights = ""
If (RetrievalClinical.FindPatientClinical > 0) Then
    i = 1
    CurApptId = 0
    ClnId = 0
    CurClnId = 0
    TestOn = False
    CurrentTestName = ""
    While (RetrievalClinical.SelectPatientClinical(i))
        If (Mid(RetrievalClinical.Symptom, 1, 1) = "/") And _
           (CurrentTestName <> RetrievalClinical.Symptom) And (TestOn) Then
            FM.CloseFile CLng(TestFileNum)
            If (TestId = "IOP") Then
                Call PushList(SchedulerTestFile, 90, ApptId, ADate, CurClnId, DocName, True)
            ElseIf (TestId = "VCC") Then
                Call PushList(SchedulerTestFile, 91, ApptId, ADate, CurClnId, DocName, True)
            ElseIf (TestId = "VSC") Then
                Call PushList(SchedulerTestFile, 92, ApptId, ADate, CurClnId, DocName, True)
            End If
            ApptId = RetrievalClinical.AppointmentId
            ClnId = RetrievalClinical.ClinicalId
            CurClnId = RetrievalClinical.ClinicalId
            CurApptId = RetrievalClinical.AppointmentId
            Call GetAppointmentDate(ApptId, ADate, DocName)
            FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
            Print #TestFileNum, "QUESTION=" + Trim(RetrievalClinical.Symptom)
            Print #TestFileNum, "DATE=" + Trim(ADate)
            Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
            CurrentTestName = RetrievalClinical.Symptom
            TestOn = True
        ElseIf (CurApptId <> RetrievalClinical.AppointmentId) And (CurApptId <> 0) Then
            FM.CloseFile CLng(TestFileNum)
            If (TestId = "IOP") Then
                Call PushList(SchedulerTestFile, 90, ApptId, ADate, CurClnId, DocName, True)
            ElseIf (TestId = "VCC") Then
                Call PushList(SchedulerTestFile, 91, ApptId, ADate, CurClnId, DocName, True)
            ElseIf (TestId = "VSC") Then
                Call PushList(SchedulerTestFile, 92, ApptId, ADate, CurClnId, DocName, True)
            End If
            ClnId = RetrievalClinical.ClinicalId
            CurClnId = RetrievalClinical.ClinicalId
            ApptId = RetrievalClinical.AppointmentId
            CurApptId = RetrievalClinical.AppointmentId
            Call GetAppointmentDate(ApptId, ADate, DocName)
            FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
            Print #TestFileNum, "QUESTION=" + Trim(RetrievalClinical.Symptom)
            Print #TestFileNum, "DATE=" + Trim(ADate)
            Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
            CurrentTestName = RetrievalClinical.Symptom
            TestOn = True
        ElseIf (Not TestOn) Then
            ApptId = RetrievalClinical.AppointmentId
            CurApptId = RetrievalClinical.AppointmentId
            ClnId = RetrievalClinical.ClinicalId
            CurClnId = RetrievalClinical.ClinicalId
            Call GetAppointmentDate(ApptId, ADate, DocName)
            FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
            Print #TestFileNum, "QUESTION=" + Trim(RetrievalClinical.Symptom)
            Print #TestFileNum, "DATE=" + Trim(ADate)
            Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
            CurrentTestName = RetrievalClinical.Symptom
            TestOn = True
        Else
            If (TestOn) Then
                Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
            End If
        End If
        i = i + 1
    Wend
End If
Set RetrievalClinical = Nothing
If (TestOn) Then
    FM.CloseFile CLng(TestFileNum)
    If (TestId = "IOP") Then
        Call PushList(SchedulerTestFile, 90, ApptId, ADate, CurClnId, DocName, True)
    ElseIf (TestId = "VISION CORRECTED") Then
        Call PushList(SchedulerTestFile, 91, ApptId, ADate, CurClnId, DocName, True)
    ElseIf (TestId = "VISION UNCORRECTED") Then
        Call PushList(SchedulerTestFile, 92, ApptId, ADate, CurClnId, DocName, True)
    End If
End If
If (FM.IsFileThere(SchedulerTestFile)) Then
    FM.Kill SchedulerTestFile
End If
GetOtherTestResult = True
Exit Function
UI_ErrorHandler:
    GetOtherTestResult = True
    Set RetrievalClinical = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetRVContent(PatId As Long, ApptId As Long, AText As String) As Boolean
Dim i As Integer
Dim ATemp As String
Dim RetNote As PatientNotes
Dim RetrievalClinical As PatientClinical
GetRVContent = False
AText = ""
Set RetrievalClinical = New PatientClinical
RetrievalClinical.PatientId = PatId
RetrievalClinical.AppointmentId = ApptId
RetrievalClinical.ClinicalType = "H"
RetrievalClinical.Symptom = "/CHIEFCOMPLAINTS"
RetrievalClinical.Findings = ""
RetrievalClinical.ImageDescriptor = ""
RetrievalClinical.ImageInstructions = ""
RetrievalClinical.ClinicalHighlights = ""
If (RetrievalClinical.FindPatientClinical > 0) Then
    i = 1
    While (RetrievalClinical.SelectPatientClinical(i))
        ATemp = Trim(RetrievalClinical.Findings)
        ATemp = Mid(ATemp, 4, Len(ATemp) - 3)
        If (Trim(RetrievalClinical.EyeContext) <> "") Then
            ATemp = ATemp + " <" + RetrievalClinical.EyeContext + ">"
        End If
        If (Trim(ATemp) <> "") Then
            If (Trim(AText) <> "") Then
                AText = AText + vbCrLf + ATemp
            Else
                AText = ATemp
            End If
        End If
        i = i + 1
    Wend
End If
Set RetrievalClinical = Nothing
Set RetNote = New PatientNotes
RetNote.NotesAppointmentId = ApptId
RetNote.NotesPatientId = PatId
RetNote.NotesType = "C"
RetNote.NotesSystem = "10"
If (RetNote.FindNotes > 0) Then
    i = 1
    While (RetNote.SelectNotes(i))
        ATemp = Trim(RetNote.NotesText1) + Trim(RetNote.NotesText2) _
              + Trim(RetNote.NotesText3) + Trim(RetNote.NotesText4)
        AText = AText + vbCrLf + ATemp
        i = i + 1
    Wend
End If
Set RetNote = Nothing
If (Trim(AText) = "") And Not (BackDoorOn) Then
    AText = Trim(Mid(frmSystems1.lstHPI.List(0), 4, 50))
End If
GetRVContent = True
End Function

Private Sub PushList(FileName As String, IType As Integer, ApptId As Long, ADate As String, AClnId As Long, ADocName As String, HistOn As Boolean)
    

Dim MyYes As Boolean
Dim DoRecapCheck As Boolean
Dim z1 As Integer
Dim Level1 As Integer
Dim FileNum As Integer
Dim MaxEntries As Integer
Dim WhereTestNameIs As Integer
Dim w As Integer, s As Integer
Dim q As Integer, r As Integer
Dim k As Integer
Dim Y As Integer, z As Integer
Dim ADoc As String, NoteText As String
Dim Rec As String, TheText As String
Dim ATime As String, ATestId As String
Dim TheLingo As String, PrevLine As String
Dim PadValue As String, TheValue As String
Dim TestDate As String, TestTime As String
Dim Question As String, AQuestion As String
Dim TheRecord As String, LocalText As String
Dim Recap(40) As String
Dim TestEntries(100) As String
Dim TestWideName As String
Dim LocalTestName As String
Dim MultiTestIndicator As String
Dim RetNote As PatientNotes
Dim ControlLevel1 As DynamicControls
    
    On Error GoTo lPushList_Error
    
Level1 = 0
FileNum = 0
NoteText = ""
WhereTestNameIs = 0
MaxEntries = 0
Erase Recap
Erase TestEntries
LocalTestName = ""
TestTime = ""
DoRecapCheck = True
FileNum = FreeFile
FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
Do Until (EOF(FileNum))
    Line Input #FileNum, TheRecord
    MaxEntries = MaxEntries + 1
    TestEntries(MaxEntries) = TheRecord
    If (InStrPS(TheRecord, "QUESTION=") <> 0) Then
        Question = Trim(Mid(TheRecord, 10, Len(TheRecord) - 9))
        If (UCase(Trim(Question)) = "/NATIONAL ORIGIN") Then
            DoRecapCheck = False
        End If
    ElseIf (InStrPS(TheRecord, "DATE=") <> 0) Then
        TestDate = Trim(Mid(TheRecord, 6, Len(TheRecord) - 5))
    ElseIf (InStrPS(TheRecord, "TIME=") <> 0) Then
        TestTime = Trim(Mid(TheRecord, 6, Len(TheRecord) - 5))
    Else
        r = InStrPS(TheRecord, "=")
        If (r = 0) Then
            r = Len(TheRecord) + 1
        End If
        r = r - 1
        w = r + 3
        Y = InStrPS(w, TheRecord, " ")
        If (Y = 0) Then
            Y = Len(TheRecord) + 1
        End If
        Set ControlLevel1 = New DynamicControls
        ControlLevel1.ControlId = Val(Trim(Mid(TheRecord, w, Y - w)))
        ControlLevel1.ControlName = ""
        ControlLevel1.IEChoiceName = ""
        If (ControlLevel1.RetrieveControl) Then
            MultiTestIndicator = ""
            PadValue = ""
            TheValue = ""
            If (ControlLevel1.ControlType = "N") Or (ControlLevel1.ControlType = "S") Then
                Y = InStrPS(w, TheRecord, "<")
                If (Y <> 0) Then
                    z = InStrPS(w, TheRecord, ">")
                    If (z <> 0) Then
                        PadValue = "@" + Mid(TheRecord, Y + 1, (z - 1) - Y)
                    End If
                End If
            ElseIf (ControlLevel1.ControlType = "R") Then
                MultiTestIndicator = ""
            End If
            Level1 = Level1 + 1
            If (Trim(ControlLevel1.LetterLanguage) <> "") And ((Not HistOn) Or (InStrPS(Trim(ControlLevel1.LetterLanguage), "Pin") <> 0)) Then
                Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.LetterLanguage) + " " + PadValue
                Call GetPadValue(Recap(Level1), TheValue)
                Call SetPadValue(Recap(Level1), TheValue)
                s = InStrPS(Recap(Level1), "/Time/")
                If (s <> 0) Then
                    Recap(Level1) = Left(Recap(Level1), s) + TestTime + Mid(Recap(Level1), s + 5, Len(Recap(Level1)) - (s + 4))
                End If
                s = InStrPS(Recap(Level1), ":^OD")
                z1 = InStrPS(Recap(Level1), ":/")
                If (s <> 0) Then
                    WhereTestNameIs = Level1
                    LocalTestName = Trim(Left(Recap(Level1), s - 1))
                ElseIf (z1 <> 0) Then
                    WhereTestNameIs = Level1
                    LocalTestName = Trim(Left(Recap(Level1), z1 - 1))
                End If
            ElseIf (Trim(ControlLevel1.DoctorLingo) <> "") Then
                Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.DoctorLingo) + " " + PadValue
                Call GetPadValue(Recap(Level1), TheValue)
                Call SetPadValue(Recap(Level1), TheValue)
                s = InStrPS(Recap(Level1), "/Time/")
                If (s <> 0) Then
                    Recap(Level1) = Left(Recap(Level1), s) + TestTime + Mid(Recap(Level1), s + 5, Len(Recap(Level1)) - (s + 4))
                End If
                s = InStrPS(Recap(Level1), ":^OD")
                z1 = InStrPS(Recap(Level1), ":/")
                If (s <> 0) Then
                    WhereTestNameIs = Level1
                    LocalTestName = Trim(Left(Recap(Level1), s - 1))
                ElseIf (z1 <> 0) Then
                    WhereTestNameIs = Level1
                    LocalTestName = Trim(Left(Recap(Level1), z1 - 1))
                End If
            Else
                Recap(Level1) = "-"
            End If
            If (DoRecapCheck) Then
                s = InStrPS(Recap(Level1), LocalTestName)
                If (s <> 0) Then
                    s = InStrPS(Recap(Level1), "^OS")
                    If (s <> 0) Then
                        Recap(Level1) = Mid(Recap(Level1), s, Len(Recap(Level1)) - (s - 1))
                    Else
                        If (WhereTestNameIs > 0) And (WhereTestNameIs <> Level1) Then
                            s = InStrPS(Recap(WhereTestNameIs), ":")
                            If (s <> 0) Then
                                TestWideName = Trim(Recap(Level1))
                                k = InStrPS(Recap(Level1), ":")
                                If (k <> 0) Then
                                    TestWideName = Trim(Mid(Recap(Level1), s + 1, Len(Recap(Level1)) - s))
                                End If
                                Recap(WhereTestNameIs) = Left(Recap(WhereTestNameIs), s) + TestWideName + Mid(Recap(WhereTestNameIs), s + 1, Len(Recap(WhereTestNameIs)) - s)
                                Recap(Level1) = "-"
                            End If
                        End If
                    End If
                End If
            End If
        End If
        Set ControlLevel1 = Nothing
    End If
Loop
FM.CloseFile CLng(FileNum)
' Build Recap Values
For s = 1 To Level1
    If (Trim(Recap(s)) <> "") And (Trim(Recap(s)) <> "-") Then
        MaxEntries = MaxEntries + 1
        TestEntries(MaxEntries) = "Recap=" + Recap(s)
    End If
Next s
GoSub ApplyTest
Exit Sub

ApplyTest:
    ADoc = ""
    ATime = ""
    ADate = ""
    TheText = ""
    For Y = 1 To MaxEntries
        PrevLine = ""
        Rec = TestEntries(Y)
        LocalText = ""
        If (Left(Rec, 10) = "QUESTION=/") Then
            AQuestion = Trim(Mid(Rec, 11, Len(Rec) - 10))
        End If
        If (Left(Rec, 5) = "DATE=") Then
            ADate = Trim(Mid(Rec, 6, Len(Rec) - 5))
        End If
        If (Left(Rec, 5) = "TIME=") Then
            ATime = Trim(Mid(Rec, 6, Len(Rec) - 5))
            s = InStrPS(ATime, "(")
            If (s > 0) Then
                ADoc = Mid(ATime, s, Len(ATime) - (s - 1))
                ATime = Trim(Left(ATime, s - 1))
            End If
        End If
        If (Left(Rec, 6) = "Recap=") Then
            LocalText = LocalText + Trim(Mid(Rec, 7, Len(Rec) - 6))
            q = InStrPS(LocalText, "/Time/")
            If (q <> 0) And (Trim(ATime) <> "") Then
                LocalText = Left(LocalText, q) + ATime + Mid(LocalText, q + 5, Len(LocalText) - (q + 4))
            End If
            Call ConvertButtonDisplay(LocalText, TheLingo)
            If (Len(TheText) > 0) And (Left(TheLingo, 1) = "%") Then
                q = InStrPS(TheLingo, ":")
                If (q <> 0) Then
                    If (Len(Trim(PrevLine)) > 3) Then
                        TheLingo = Left(TheLingo, q) + Mid(PrevLine, 2, Len(PrevLine) - 1) + Mid(TheLingo, q + 1, Len(TheLingo) - q)
                        TheText = Left(TheText, Len(TheText) - (Len(PrevLine) - 1))
                    End If
                End If
                TheText = TheText + Chr(13) + Chr(10) + Mid(TheLingo, 2, Len(TheLingo) - 1)
            Else
                TheText = TheText + TheLingo
            End If
        End If
        PrevLine = TheLingo
    Next Y
    If TheText Like "*Vision*" Then
        Dim FormattedText As String
        FormattedText = FormatText(TheText)
        If Not FormattedText = "" Then
            TheText = FormattedText
        End If
    End If
    If (Trim(TheText) <> "") Or (MaxEntries > 2) Then
        If (Trim(TheText) = "") Then
            TheText = "-"
        End If
        Call GetTestOrder(AQuestion, ATestId)
        If (IType = 0) Then
            TestResultsIndex = TestResultsIndex + 1
            TestResultsStorage(TestResultsIndex).TestOrderNum = ATestId
            TestResultsStorage(TestResultsIndex).TestQuestion = AQuestion
            TestResultsStorage(TestResultsIndex).TestText = TheText
            TestResultsStorage(TestResultsIndex).TestDoc = ADocName
            TestResultsStorage(TestResultsIndex).TestDate = ADate
            TestResultsStorage(TestResultsIndex).TestApptId = ApptId
            TestResultsStorage(TestResultsIndex).TestClnId = AClnId
            TestResultsStorage(TestResultsIndex).TestStatus = "T"
            TestResultsStorage(TestResultsIndex).TestNote = NoteText
        ElseIf (IType = 1) Then
            If (HistResultsIndex1 + 1 < 51) Then
                GoSub GetNoteText
                HistResultsIndex1 = HistResultsIndex1 + 1
                HistResultsStorage(HistResultsIndex1).TestOrderNum = ATestId
                HistResultsStorage(HistResultsIndex1).TestQuestion = AQuestion
                HistResultsStorage(HistResultsIndex1).TestText = TheText
                HistResultsStorage(HistResultsIndex1).TestDoc = ADocName
                HistResultsStorage(HistResultsIndex1).TestDate = ADate
                HistResultsStorage(HistResultsIndex1).TestApptId = ApptId
                HistResultsStorage(HistResultsIndex1).TestClnId = AClnId
                HistResultsStorage(HistResultsIndex1).TestStatus = ""
                HistResultsStorage(HistResultsIndex1).TestNote = NoteText
            End If
        ElseIf (IType = 2) Then
            If (HistResultsIndex2 + 1 < 101) Then
                GoSub GetNoteText
                HistResultsIndex2 = HistResultsIndex2 + 1
                HistResultsStorage(HistResultsIndex2).TestOrderNum = ATestId
                HistResultsStorage(HistResultsIndex2).TestQuestion = AQuestion
                HistResultsStorage(HistResultsIndex2).TestText = TheText
                HistResultsStorage(HistResultsIndex2).TestDoc = ADocName
                HistResultsStorage(HistResultsIndex2).TestDate = ADate
                HistResultsStorage(HistResultsIndex2).TestApptId = ApptId
                HistResultsStorage(HistResultsIndex2).TestClnId = AClnId
                HistResultsStorage(HistResultsIndex2).TestStatus = ""
                HistResultsStorage(HistResultsIndex2).TestNote = NoteText
            End If
        ElseIf (IType = 3) Then
            If (HistResultsIndex3 + 1 < 151) Then
                GoSub GetNoteText
                HistResultsIndex3 = HistResultsIndex3 + 1
                HistResultsStorage(HistResultsIndex3).TestOrderNum = ATestId
                HistResultsStorage(HistResultsIndex3).TestQuestion = AQuestion
                HistResultsStorage(HistResultsIndex3).TestText = TheText
                HistResultsStorage(HistResultsIndex3).TestDoc = ADocName
                HistResultsStorage(HistResultsIndex3).TestDate = ADate
                HistResultsStorage(HistResultsIndex3).TestApptId = ApptId
                HistResultsStorage(HistResultsIndex3).TestClnId = AClnId
                HistResultsStorage(HistResultsIndex3).TestStatus = ""
                HistResultsStorage(HistResultsIndex3).TestNote = NoteText
            End If
        ElseIf (IType = 99) Then
            MyYes = True
            For s = 1 To QTestResultsIndex
                If (QTestResultsStorage(s).TestText = TheText) And (QTestResultsStorage(s).TestDate = ADate) Then
                    MyYes = False
                End If
            Next s
            For s = 1 To TestResultsIndex
                If (TestResultsStorage(s).TestText = TheText) And (TestResultsStorage(s).TestDate = ADate) Then
                    MyYes = False
                End If
            Next s
            If (MyYes) Then
                GoSub GetNoteText
                QTestResultsIndex = QTestResultsIndex + 1
                QTestResultsStorage(QTestResultsIndex).TestOrderNum = ATestId
                QTestResultsStorage(QTestResultsIndex).TestQuestion = AQuestion
                QTestResultsStorage(QTestResultsIndex).TestText = TheText
                QTestResultsStorage(QTestResultsIndex).TestDoc = ADocName
                QTestResultsStorage(QTestResultsIndex).TestDate = ADate
                QTestResultsStorage(QTestResultsIndex).TestApptId = ApptId
                QTestResultsStorage(QTestResultsIndex).TestClnId = AClnId
                QTestResultsStorage(QTestResultsIndex).TestStatus = "R"
                QTestResultsStorage(QTestResultsIndex).TestNote = NoteText
            End If
        ElseIf (IType = 90) Then
            GoSub GetNoteText
            IOPResultsStorage.TestOrderNum = ATestId
            IOPResultsStorage.TestQuestion = AQuestion
            IOPResultsStorage.TestText = TheText
            IOPResultsStorage.TestDoc = ADocName
            IOPResultsStorage.TestDate = ADate
            IOPResultsStorage.TestApptId = ApptId
            IOPResultsStorage.TestClnId = AClnId
            IOPResultsStorage.TestStatus = "R"
            IOPResultsStorage.TestNote = NoteText
        ElseIf (IType = 91) Then
            GoSub GetNoteText
            VCCResultsStorage.TestOrderNum = ATestId
            VCCResultsStorage.TestQuestion = AQuestion
            VCCResultsStorage.TestText = TheText
            VCCResultsStorage.TestDoc = ADocName
            VCCResultsStorage.TestDate = ADate
            VCCResultsStorage.TestApptId = ApptId
            VCCResultsStorage.TestClnId = AClnId
            VCCResultsStorage.TestStatus = "R"
            VCCResultsStorage.TestNote = NoteText
        ElseIf (IType = 92) Then
            GoSub GetNoteText
            VSCResultsStorage.TestOrderNum = ATestId
            VSCResultsStorage.TestQuestion = AQuestion
            VSCResultsStorage.TestText = TheText
            VSCResultsStorage.TestDoc = ADocName
            VSCResultsStorage.TestDate = ADate
            VSCResultsStorage.TestApptId = ApptId
            VSCResultsStorage.TestClnId = AClnId
            VSCResultsStorage.TestStatus = "R"
            VSCResultsStorage.TestNote = NoteText
        End If
    End If
    Return
    
GetNoteText:
    NoteText = ""
    If (ApptId > 0) And (Trim(ATestId) <> "") Then
        Set RetNote = New PatientNotes
        RetNote.NotesPatientId = 0
        RetNote.NotesAppointmentId = ApptId
        RetNote.NotesType = "C"
        RetNote.NotesSystem = "T" + Trim(ATestId)
        If (RetNote.FindNotes > 0) Then
            If (RetNote.SelectNotes(1)) Then
                NoteText = Trim(RetNote.NotesText1) + " " _
                         + Trim(RetNote.NotesText2) + " " _
                         + Trim(RetNote.NotesText3) + " " _
                         + Trim(RetNote.NotesText4)
            End If
        End If
        Set RetNote = Nothing
    End If
    Return

lPushList_Error:

    If (FileNum <> 0) Then
        FM.CloseFile CLng(FileNum)
    End If
    Set ControlLevel1 = Nothing

    LogError "frmHighlights", "PushList", Err, Err.Description, "Appt# " & str(ApptId) & " - " & Question

End Sub

Private Function LoadTestNotes() As Boolean
Dim i As Integer
Dim ApptId As Long
Dim ATestId As String
Dim NoteText As String
Dim RetNote As PatientNotes
For i = 1 To 60
    If (Trim(TestResultsStorage(i).TestText) <> "") Then
        ApptId = TestResultsStorage(i).TestApptId
        ATestId = Trim(TestResultsStorage(i).TestOrderNum)
        GoSub GetNoteText
        TestResultsStorage(i).TestNote = NoteText
    End If
Next i
For i = 1 To 60
    If (Trim(QTestResultsStorage(i).TestText) <> "") Then
        ApptId = QTestResultsStorage(i).TestApptId
        ATestId = Trim(QTestResultsStorage(i).TestOrderNum)
        GoSub GetNoteText
        QTestResultsStorage(i).TestNote = NoteText
    End If
Next i
Exit Function
GetNoteText:
    NoteText = ""
    If (ApptId > 0) And (Trim(ATestId) <> "") Then
        Set RetNote = New PatientNotes
        RetNote.NotesPatientId = 0
        RetNote.NotesAppointmentId = ApptId
        RetNote.NotesType = "C"
        RetNote.NotesSystem = "T" + Trim(ATestId)
        If (RetNote.FindNotes > 0) Then
            If (RetNote.SelectNotes(1)) Then
                NoteText = Trim(RetNote.NotesText1) + " " _
                         + Trim(RetNote.NotesText2) + " " _
                         + Trim(RetNote.NotesText3) + " " _
                         + Trim(RetNote.NotesText4)
            End If
        End If
        Set RetNote = Nothing
    End If
    Return
End Function

Private Sub AddItemToList(AListbox As ListBox, AText As String, ADate As String, Idx As Integer, ANote As String)
Dim k As Integer
Dim zz As Integer
Dim Temp As String
Dim ATemp As String
Dim UTemp As String
Dim CurrentPos As Integer
Dim DisplayText As String
If (Left(Trim(AText), 2) = vbCrLf) Then
    AText = Mid(Trim(AText), 3, Len(Trim(AText)) - 2)
End If
CurrentPos = 1
k = InStrPS(CurrentPos, AText, Chr(13) + Chr(10))
While (k > 0)
    If (k <> CurrentPos) Then
        Temp = Mid(AText, CurrentPos, k - CurrentPos)
        Call ReplaceCharacters(Temp, Chr(13), " ")
        Call ReplaceCharacters(Temp, Chr(10), " ")
        If (AListbox.Name = "lstHist") Then
            If (InStrPS(Temp, "Vision Corrected") > 0) Then
                Temp = Temp + " " + ADate
            ElseIf (InStrPS(Temp, "IOP") > 0) Then
                Temp = Temp + " " + ADate
            ElseIf (InStrPS(Temp, "Vision Uncorrected") > 0) Then
                Temp = Temp + " " + ADate
            End If
        Else
            If (CurrentPos = 1) Then
                Temp = Temp + " " + ADate
            End If
        End If
        If (Len(Trim(Temp)) > 0) Then
            DisplayText = Space(75)
            If (AListbox.Name = "lstHist") Then
                If (Len(Temp) > 60) Then
                    ATemp = Left(Temp, 60)
                    Do Until (Len(Temp) < 1)
                        If (Mid(ATemp, Len(ATemp), 1) = " ") Then
                            Mid(DisplayText, 1, Len(ATemp)) = ATemp
                            Mid(DisplayText, 75, 1) = "x"
                            DisplayText = DisplayText + Trim(str(Idx))
                            AListbox.AddItem DisplayText
                            Temp = Mid(Temp, Len(ATemp) + 1, Len(Temp) - Len(ATemp))
                            If (Len(Temp) > 60) Then
                                ATemp = Left(Temp, 60)
                            Else
                                DisplayText = Space(75)
                                Mid(DisplayText, 1, Len(Temp)) = Temp
                                Mid(DisplayText, 75, 1) = "x"
                                DisplayText = DisplayText + Trim(str(Idx))
                                AListbox.AddItem DisplayText
                                Exit Do
                            End If
                        Else
                            ATemp = Left(ATemp, Len(ATemp) - 1)
                        End If
                    Loop
                    DisplayText = ""
                Else
                    Mid(DisplayText, 1, Len(Temp)) = Temp
                    Mid(DisplayText, 75, 1) = "x"
                End If
            Else
                Mid(DisplayText, 1, Len(Temp)) = Temp
                Mid(DisplayText, 75, 1) = "X"
                DisplayText = DisplayText + Trim(str(Idx))
            End If
            If (Trim(DisplayText) <> "") Then
                AListbox.AddItem DisplayText
            End If
        End If
    End If
    CurrentPos = k + 2
    k = InStrPS(CurrentPos, AText, Chr(13) + Chr(10))
Wend
If (CurrentPos < Len(AText)) Then
    Temp = Mid(AText, CurrentPos, Len(AText) - (CurrentPos - 1))
    Call ReplaceCharacters(Temp, Chr(13), " ")
    Call ReplaceCharacters(Temp, Chr(10), " ")
    If (Len(Trim(Temp)) > 2) Then
        For zz = 1 To Len(Temp) Step 66
            UTemp = Mid(Temp, zz, 66)
            DisplayText = Space(75)
            Mid(DisplayText, 1, Len(UTemp)) = UTemp
            If (AListbox.Name = "lstHist") Then
                Mid(DisplayText, 75, 1) = "x"
                DisplayText = DisplayText + Trim(str(Idx))
            Else
                Mid(DisplayText, 75, 1) = "X"
                DisplayText = DisplayText + Trim(str(Idx))
            End If
            AListbox.AddItem DisplayText
        Next zz
    End If
End If
If (Trim(ANote) <> "") Then
    DisplayText = Space(75)
    Mid(DisplayText, 1, Len(ANote)) = ANote
    If (AListbox.Name = "lstHist") Then
        Mid(DisplayText, 75, 1) = "x"
        DisplayText = DisplayText + Trim(str(Idx))
    Else
        Mid(DisplayText, 75, 1) = "X"
        DisplayText = DisplayText + Trim(str(Idx))
    End If
    AListbox.AddItem DisplayText
End If
End Sub

Private Sub Form_Activate()
QuitOn = False
Dim IOP As String
IOP = ""
If Not lstCTests.ListItems.Item(1).SubItems(5) = "" Then
        If IsNumeric(lstCTests.ListItems.Item(1).SubItems(5)) Then
            IOP = lstCTests.ListItems.Item(1).SubItems(5)
            IOP = IOP & ","
        End If
End If
If Not lstCTests.ListItems.Item(2).SubItems(5) = "" Then
        If IsNumeric(lstCTests.ListItems.Item(2).SubItems(5)) Then
                IOP = IOP & lstCTests.ListItems.Item(2).SubItems(5)
        End If
End If

Dim Rule As New DI_RuleAlert
Dim objUserCds As New UserCDS
Dim GetCds As Boolean
Dim ShowAlert As Boolean
GetCds = objUserCds.GetCDSByRole(UserLogin.iId)
If GetCds = True Then
    objUserCds.SelectCDS (1)
    ShowAlert = objUserCds.Alerts
End If
If (lblAlert.Visible) Then
    If (FirstTimeIn) Then
        FirstTimeIn = False
        Call lblAlert_Click
        'For Rule Engine Invocation
        Rule.IOPValues = IOP
        If ShowAlert = True Then
            Call Rule.RuleAlert(PatientId, AppointmentId, 1)
        End If
        DoEvents
    End If
Else
    If FirstTimeIn Then
        FirstTimeIn = False
        Rule.IOPValues = IOP
        If ShowAlert = True Then
            Call Rule.RuleAlert(PatientId, AppointmentId, 1)
        End If
    End If
End If
End Sub

Private Sub form_load()
FirstTimeIn = True
End Sub

Private Sub cmdImpr_Click()
If Not UserLogin.HasPermission(epPastVisits) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
Dim DrLists As DILists
If (lstImpr.ListCount < 1) Then
    Set DrLists = New DILists
    DrLists.ApplPatientId = PatientId
    DrLists.ApplDate = ActiveDate
    DrLists.ApplStartAt = 1
    DrLists.ApplReturnCount = 0
    Set DrLists.ApplList = lstImpr
    Call DrLists.ApplImpressionsList
End If
If (lstImpr.ListCount > 0) Then
    lstImpr.Visible = True
    If (lstLinkNotes.Visible) Then
        lstLinkNotes.Visible = False
    End If
    If (lstHist.Visible) Then
        lstHist.Visible = False
    End If
Else
    frmEventMsgs.Header = "No Previous Impressions"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub FxImage1_Click()
Dim ClnId As Long
ClnId = Val(Trim(FxImage1.Tag))
If (ClnId > 0) Then
    If (frmViewDraw.LoadDraw(ClnId, "")) Then
        frmViewDraw.Show 1
    End If
End If
End Sub

Private Sub lblAlert_Click()
If (frmAlert.LoadAlerts(PatientId, EAlertType.eatHighlight, True)) Then
    frmAlert.Show 1
    If (IsAlertPresent(PatientId, EAlertType.eatHighlight)) Then
        lblAlert.Caption = "High Alert ON"
        lblAlert.Visible = True
    Else
        lblAlert.Caption = "Alert ON"
        lblAlert.Visible = IsAlertPresent(PatientId, 0)
    End If
End If
End Sub

Private Sub lblLDate_Click()
If (lstPrevDrs.ListCount > 0) Then
    lstPrevDrs.Visible = Not (lstPrevDrs.Visible)
End If
End Sub

Private Sub lblPostOp_Click()
Dim i As Integer
Dim PDesc As String
Dim DisplayText As String, UDate As String
Dim PPeriod As Long, ClnId As Long, ApptId As Long
Dim ApptDate As String, PMods As String, PDetails As String
i = 1
lstHist.Clear
lstHist.AddItem "Close Post Op"
Do Until Not (frmSystems1.GetPostOpConditions(i, ClnId, ApptId, ApptDate, PPeriod, PDetails, PMods, PDesc))
    UDate = Mid(ApptDate, 5, 2) + "/" + Mid(ApptDate, 7, 2) + "/" + Left(ApptDate, 4)
    DisplayText = UDate + " " + Trim(PDesc)
    lstHist.AddItem DisplayText
    i = i + 1
Loop
If (lstHist.ListCount > 1) Then
    lstHist.Visible = True
End If
End Sub

Private Sub lblRefDr_Click()
If (RefDr > 0) Then
    If (frmViewVendors.VendorLoadDisplay(RefDr)) Then
        frmViewVendors.Show 1
    End If
End If
End Sub

Private Sub lstAllergies_Click()
lstAllergies.Visible = Not (lstAllergies.Visible)
End Sub

Private Sub lstCTests_Click()
Call LoadHistoricTestResults
End Sub

Private Sub lstHist_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
Dim i As Integer
Dim Trig As Boolean
If (lstHist.ListIndex > 0) Then
    If (InStrPS(lstHist.List(0), "HPI") <> 0) Then
        Trig = False
        i = UnSetOn(2)
        If (i = 2) Then
            If (Mid(lstHist.List(lstHist.ListIndex), 76, 1) = "c") Or (Mid(lstHist.List(lstHist.ListIndex), 76, 1) = "C") Then
                Call UnsetComplaintHighlight(lstHist, lstHist.ListIndex)
                HistHPIComplaints(lstHist.ListIndex) = ""
                For i = lstHist.ListIndex + 1 To MaxHistoryHPI
                    HistHPIComplaints(lstHist.ListIndex) = HistHPIComplaints(i)
                Next i
                lstHist.RemoveItem lstHist.ListIndex
            End If
            If (lstHist.ListCount < 2) Then
                lstHist.Visible = False
                lstHist.Clear
            End If
        End If
    Else
        lstHist.Visible = False
        lstHist.Clear
    End If
ElseIf (lstHist.ListIndex = 0) Then
    lstHist.Visible = False
    lstHist.Clear
Else
    lstHist.Visible = False
    lstHist.Clear
End If
End Sub

Private Function SetHistoricTests(PatId As Long, AType As Integer, IDate As String) As Boolean
Dim ApptId As Long
Dim CurApptId As Long
Dim ClnId As Long
Dim TestOn As Boolean
Dim TestFileNum As Integer
Dim i As Integer, IType As Integer
Dim DocName As String
Dim ADate As String
Dim LDate As String
Dim CurrentTestName As String
Dim SchedulerTestFile As String
Dim PDate As String
Dim RetrievalClinical As PatientClinical
Dim RetAppt As SchedulerAppointment
Dim FirstTime As Boolean
SetHistoricTests = False
TestFileNum = FreeFile
SchedulerTestFile = DoctorInterfaceDirectory + Trim(str(PatId)) + "TestFile.txt"
Set RetrievalClinical = New PatientClinical
RetrievalClinical.PatientId = PatId
RetrievalClinical.AppointmentId = 0
RetrievalClinical.ClinicalType = "F"
IType = 1
RetrievalClinical.Symptom = "/VISION CORRECTED"
If (AType = 1) Then
    IType = 2
    RetrievalClinical.Symptom = "/IOP"
End If
If (AType = 2) Then
    IType = 3
    RetrievalClinical.Symptom = "/VISION UNCORRECTED"
End If
RetrievalClinical.Findings = ""
RetrievalClinical.ImageDescriptor = ""
RetrievalClinical.ImageInstructions = ""
If (RetrievalClinical.FindPatientClinicalTests > 0) Then
    i = 1
    CurApptId = 0
    TestOn = False
    CurrentTestName = ""
    FirstTime = True
    Do Until Not (RetrievalClinical.SelectPatientClinical(i))
        If (CurApptId <> RetrievalClinical.AppointmentId) Then
            LDate = ADate
            GoSub GetApptDate
            CurApptId = RetrievalClinical.AppointmentId
        End If
' There is a condition where multiple appointments in the same day needs to be covered
' If the test in is in the DB then the appointment was performed (sort of ... maybe not discharged yet)
' So the date part of the 'or' should catch them.
        If (AppointmentId > RetrievalClinical.AppointmentId) Or (IDate >= PDate) Then
            If (Mid(RetrievalClinical.Symptom, 1, 1) = "/") And _
               ((CurrentTestName <> RetrievalClinical.Symptom) Or (RetrievalClinical.AppointmentId <> ApptId)) And (TestOn) Then
                FM.CloseFile CLng(TestFileNum)
                Call PushList(SchedulerTestFile, IType, ApptId, LDate, ClnId, DocName, True)
                If (IType = 1) Then
                    If (HistResultsIndex1 >= MaxHistoryTestsDisplay) Then
                        Exit Do
                    End If
                ElseIf (IType = 2) Then
                        If ((HistResultsIndex2 - 50) >= MaxHistoryTestsDisplay) Then
                        Exit Do
                    End If
                ElseIf (IType = 3) Then
                        If ((HistResultsIndex3 - 100) >= MaxHistoryTestsDisplay) Then
                        Exit Do
                    End If
                End If
                ApptId = RetrievalClinical.AppointmentId
                ClnId = RetrievalClinical.ClinicalId
                FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
                Print #TestFileNum, "QUESTION=" + Trim(RetrievalClinical.Symptom)
                Print #TestFileNum, "DATE=" + ADate
                Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
                CurrentTestName = RetrievalClinical.Symptom
                TestOn = True
            ElseIf (Not TestOn) Then
                ApptId = RetrievalClinical.AppointmentId
                ClnId = RetrievalClinical.ClinicalId
                FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
                Print #TestFileNum, "QUESTION=" + Trim(RetrievalClinical.Symptom)
                Print #TestFileNum, "DATE=" + ADate
                Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
                CurrentTestName = RetrievalClinical.Symptom
                TestOn = True
            Else
                If (TestOn) Then
                    Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
                End If
            End If
        End If
        i = i + 1
    Loop
End If
Set RetrievalClinical = Nothing
If (TestOn) Then
    FM.CloseFile CLng(TestFileNum)
    Call PushList(SchedulerTestFile, IType, ApptId, ADate, ClnId, DocName, True)
End If
If (FM.IsFileThere(SchedulerTestFile)) Then
    FM.Kill SchedulerTestFile
End If
SetHistoricTests = True

Exit Function
GetApptDate:
    PDate = ""
    ADate = ""
    If (RetrievalClinical.AppointmentId > 0) Then
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentId = RetrievalClinical.AppointmentId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            PDate = RetAppt.AppointmentDate
            ADate = Mid(PDate, 5, 2) + "/" + Mid(PDate, 7, 2) + "/" + Left(PDate, 4)
        End If
        Set RetAppt = Nothing
    End If
    Return
End Function

Private Function LoadHistoricHPI() As Boolean
Dim i As Integer
lstHist.Clear
lstHist.AddItem "    Close Historic HPI"
For i = 1 To 30
    If (Trim(HistHPIComplaints(i)) <> "") Then
        lstHist.AddItem HistHPIComplaints(i)
    End If
Next i
End Function
Private Function LoadHistoricTestResults()
Dim r As Integer, s As Integer
Dim StartWith As Integer, z1 As Integer
Dim z As Integer, w1 As Integer
Dim j As Integer, q As Integer, p As Integer
Dim i As Integer, SlotIdx As Integer
Dim SlotId As Integer
Dim st As Integer, ed As Integer
Dim VCCId As Long, VSCId As Long, IOPId As Long
Dim LclTemp As String
Dim MyDate As String
Dim LclDate1 As String
Dim LclDate2 As String
Dim LclDate3 As String
Dim ATemp As String, ADate As String, ApptIdTemp As Long
Dim AText As String
Dim Temp As String
Dim CurDate As String
Dim ATextOD As String, ATextOS As String
Dim Flag As Boolean
Dim PrevTemp As String
Dim Tempvcc As Integer
Dim Tempvsc As Integer
LoadHistoricTestResults = True
lstHistCTests1.ListItems.Clear
StartWith = 1
CurDate = ""
MyDate = ""
For st = 1 To MaxHistoryTests
    CurDate = Mid(HistResultsStorage(st).TestDate, 7, 4) + Mid(HistResultsStorage(st).TestDate, 1, 2) + Mid(HistResultsStorage(st).TestDate, 4, 2)
    If (MyDate = "") Then
        MyDate = CurDate
    End If
    If (MyDate < CurDate) And (Trim(CurDate) <> "") Then
        MyDate = CurDate
            If (st < 51) Then
            StartWith = 1
            ElseIf (st < 101) Then
            StartWith = 2
            ElseIf (st < 151) Then
            StartWith = 3
        End If
    End If
Next st
If (StartWith = 1) Then
    VCCId = -1
    Temp = "VCC"
    st = 1
        ed = 50
    SlotId = 2
    PrevTemp = Temp
    GoSub NewLoadIt
    
    VSCId = -1
    Temp = "VSC"
        st = 101
        ed = 150
    SlotId = 6
    GoSub NewLoadIt
    
    IOPId = -1
    Temp = "IOP"
        st = 51
        ed = 100
    SlotId = 10
    GoSub NewLoadIt
ElseIf (StartWith = 2) Then
    VSCId = -1
    Temp = "VSC"
        st = 101
        ed = 150
    SlotId = 6
    PrevTemp = Temp
    GoSub NewLoadIt
    
    IOPId = -1
    Temp = "IOP"
        st = 51
        ed = 100
    SlotId = 10
    GoSub NewLoadIt
    
    VCCId = -1
    Temp = "VCC"
    st = 1
        ed = 50
    SlotId = 2
    GoSub NewLoadIt
Else
    IOPId = -1
    Temp = "IOP"
        st = 51
        ed = 100
    SlotId = 10
    PrevTemp = Temp
    GoSub NewLoadIt
    
    VCCId = -1
    Temp = "VCC"
    st = 1
        ed = 50
    SlotId = 2
    GoSub NewLoadIt
    
    VSCId = -1
    Temp = "VSC"
        st = 101
        ed = 150
    SlotId = 6
    GoSub NewLoadIt
End If
If (lstHistCTests1.ListItems.Count > 0) Then
    lstHistCTests1.Visible = True
End If
Exit Function
NewLoadIt:
    For i = st To ed
        If (Trim(HistResultsStorage(i).TestText) <> "") Then
            AText = Trim(HistResultsStorage(i).TestText)
            ADate = HistResultsStorage(i).TestDate
            ApptIdTemp = HistResultsStorage(i).TestApptId
            GoSub FindSlot
            If (SlotIdx = 0) Then
                ATemp = Trim(str(HistResultsStorage(i).TestApptId))
                lstHistCTests1.ListItems.Add = ATemp
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(1) = ADate
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(12) = ApptIdTemp
                SlotIdx = lstHistCTests1.ListItems.Count
            Else
                If (SlotIdx > lstHistCTests1.ListItems.Count) Then
                    lstHistCTests1.ListItems.Add = ATemp
                    lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(1) = ADate
                    lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(12) = ApptIdTemp
                Else
                    ATemp = Trim(lstHistCTests1.ListItems.Item(SlotIdx).SubItems(12)) + "," + Trim(str(HistResultsStorage(i).TestClnId))
                    lstHistCTests1.ListItems.Item(SlotIdx).SubItems(12) = ApptIdTemp
                End If
            End If
            GoSub SetupMyStandardTests
        End If
    Next i
    PrevTemp = Temp
    Return
SetupMyStandardTests:
    If (Trim(AText) <> "") And (SlotId > 0) And (SlotIdx > 0) Then
        ATextOD = ""
        ATextOS = ""
        Tempvcc = 0
        Tempvsc = 0
        j = InStrPS(AText, "OD:")
        p = InStrPS(AText, "OS:")
        q = InStrPS(AText, "Pinhole")
        Tempvcc = InStrPS(AText, "Vision Corrected")
        Tempvsc = InStrPS(AText, "Vision Uncorrected")
        If Tempvcc > 0 And q > 0 Then
            SlotId = 2
        End If
        If Tempvsc > 0 And q > 0 Then
            SlotId = 6
        End If
        If (j > 0) And (p > 0) And (p > j) Then
            ATextOD = Trim(Mid(AText, j + 3, (p - 1) - (j + 2)))
            'If p > q And q > 0 Then   'org
            If p > q And q > 1 Then
                ATextOD = Trim$(Mid$(AText, j + 3, (q - 1) - (j + 2)))
                ATextOS = ""
            ElseIf (q > 1) Then
                ATextOS = Trim(Mid(AText, p + 3, (q - 1) - (p + 2)))
            Else
                ATextOS = Trim(Mid(AText, p + 3, Len(AText) - (p + 2)))
            End If
        ElseIf (p > 0) Then
            ATextOD = ""
            If (q > 1) Then
                ATextOS = Trim(Mid(AText, p + 3, (q - 1) - (p + 2)))
            Else
                ATextOS = Trim(Mid(AText, p + 3, Len(AText) - (p + 2)))
            End If
        ElseIf (j > 0) Then
            If (q > 1) Then
                If ((q - 1) - (j + 2) > 0) Then
                    ATextOD = Trim(Mid(AText, j + 3, (q - 1) - (j + 2)))
                Else
                    ATextOD = ""
                End If
            Else
                ATextOD = Trim(Mid(AText, j + 3, Len(AText) - (j + 2)))
            End If
            ATextOS = ""
        End If
        
        r = InStrPS(ATextOD, vbCrLf)
        s = InStrPS(ATextOS, vbCrLf)
        If r > 0 Then
            ATextOD = Mid(ATextOD, 1, r + 1)
        End If
        If s > 0 Then
            ATextOS = Mid(ATextOS, 1, s + 1)
        End If
        Call StripCharacters(ATextOD, Chr(13))
        Call StripCharacters(ATextOD, Chr(10))
        Call StripCharacters(ATextOS, Chr(13))
        Call StripCharacters(ATextOS, Chr(10))
        If (SlotId = 10) Then
            w1 = InStrPS(ATextOD, " ")
            If (w1 > 0) Then
                ATextOD = Left(ATextOD, w1 - 1)
            End If
            w1 = InStrPS(ATextOS, " ")
            If (w1 > 0) Then
                ATextOS = Left(ATextOS, w1 - 1)
            End If
        End If
        If (q = 1) And (SlotId = 6) Then
            q = 0
            SlotId = 8
            lstHistCTests1.ListItems.Item(SlotIdx).SubItems(SlotId) = Trim(ATextOD)
            lstHistCTests1.ListItems.Item(SlotIdx).SubItems(SlotId + 1) = Trim(ATextOS)
        ElseIf (q = 1) And (SlotId = 2) Then
            q = 0
            SlotId = 4
            lstHistCTests1.ListItems.Item(SlotIdx).SubItems(SlotId) = Trim(ATextOD)
            lstHistCTests1.ListItems.Item(SlotIdx).SubItems(SlotId + 1) = Trim(ATextOS)
        Else
            lstHistCTests1.ListItems.Item(SlotIdx).SubItems(SlotId) = Trim(ATextOD)
            lstHistCTests1.ListItems.Item(SlotIdx).SubItems(SlotId + 1) = Trim(ATextOS)
        End If
        
        If (q > 0) Then
            AText = Mid(AText, q, Len(AText) - (q - 1))
            ATextOD = ""
            ATextOS = ""
            j = InStrPS(AText, "OD:")
            p = InStrPS(AText, "OS:")
            If (j > 0) And (p > 0) And (p > j) Then
                ATextOD = Mid(AText, j + 3, (p - 1) - (j + 2))
                ATextOS = Mid(AText, p + 3, Len(AText) - (p + 2))
            ElseIf (p > 0) Then
                ATextOD = ""
                ATextOS = Mid(AText, p + 3, Len(AText) - (p + 2))
            ElseIf (j > 0) Then
                ATextOD = Mid(AText, j + 3, Len(AText) - (j + 2))
            End If
            Call StripCharacters(ATextOD, Chr(13))
            Call StripCharacters(ATextOD, Chr(10))
            Call StripCharacters(ATextOS, Chr(13))
            Call StripCharacters(ATextOS, Chr(10))
            lstHistCTests1.ListItems.Item(SlotIdx).SubItems(SlotId + 2) = Trim(ATextOD)
            lstHistCTests1.ListItems.Item(SlotIdx).SubItems(SlotId + 3) = Trim(ATextOS)
        End If
    End If
    Return
FindSlot:
    SlotIdx = 0
    For z = 1 To lstHistCTests1.ListItems.Count
        Dim FoundItem As Integer
        FoundItem = InStrPS(1, lstHistCTests1.ListItems.Item(z).SubItems(12), CStr(ApptIdTemp))
        If (ADate = lstHistCTests1.ListItems.Item(z).SubItems(1) And FoundItem = 1) Then
            SlotIdx = z
            Exit For
        End If
    Next z
     If (SlotIdx = 0) Then
' Array Shift cant use listview sort/key/order function because the association fails betweeen the load array and the 3 classes of tests (what a nightmare)
        LclDate1 = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        For z = 1 To lstHistCTests1.ListItems.Count
            LclDate3 = lstHistCTests1.ListItems.Item(z).SubItems(1)
            LclDate2 = Mid(LclDate3, 7, 4) + Mid(LclDate3, 1, 2) + Mid(LclDate3, 4, 2)
            If (LclDate1 > LclDate2) Then
                LclTemp = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).Text
                lstHistCTests1.ListItems.Add = LclTemp
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(1) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(1)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(2) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(2)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(3) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(3)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(4) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(4)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(5) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(5)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(6) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(6)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(7) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(7)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(8) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(8)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(9) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(9)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(10) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(10)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(11) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(11)
                lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count).SubItems(12) = lstHistCTests1.ListItems.Item(lstHistCTests1.ListItems.Count - 1).SubItems(12)
                For z1 = lstHistCTests1.ListItems.Count - 1 To z Step -1
                    If (z1 > 1) Then
                        lstHistCTests1.ListItems.Item(z1).Text = lstHistCTests1.ListItems.Item(z1 - 1).Text
                        lstHistCTests1.ListItems.Item(z1).SubItems(1) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(1)
                        lstHistCTests1.ListItems.Item(z1).SubItems(2) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(2)
                        lstHistCTests1.ListItems.Item(z1).SubItems(3) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(3)
                        lstHistCTests1.ListItems.Item(z1).SubItems(4) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(4)
                        lstHistCTests1.ListItems.Item(z1).SubItems(5) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(5)
                        lstHistCTests1.ListItems.Item(z1).SubItems(6) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(6)
                        lstHistCTests1.ListItems.Item(z1).SubItems(7) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(7)
                        lstHistCTests1.ListItems.Item(z1).SubItems(8) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(8)
                        lstHistCTests1.ListItems.Item(z1).SubItems(9) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(9)
                        lstHistCTests1.ListItems.Item(z1).SubItems(10) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(10)
                        lstHistCTests1.ListItems.Item(z1).SubItems(11) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(11)
                        lstHistCTests1.ListItems.Item(z1).SubItems(12) = lstHistCTests1.ListItems.Item(z1 - 1).SubItems(12)
                    End If
                Next z1
                lstHistCTests1.ListItems.Item(z).Text = Trim(str(HistResultsStorage(i).TestClnId))
                lstHistCTests1.ListItems.Item(z).SubItems(1) = ADate
                lstHistCTests1.ListItems.Item(z).SubItems(2) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(3) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(4) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(5) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(6) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(7) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(8) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(9) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(10) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(11) = ""
                lstHistCTests1.ListItems.Item(z).SubItems(12) = ""
                SlotIdx = z
                Exit For
            End If
        Next z
    End If
    Return
End Function
Private Sub lstHistCTests1_Click()
lstHistCTests1.Visible = False
End Sub

Private Sub lstHPILV_Click()
Dim i As Long
Dim lvi As ListItem
If (lstHPILV.ListItems.Count > 0) Then
    i = Val(lstHPILV.SelectedItem)
    Set lvi = lstHPILV.FindItem(lstHPILV.SelectedItem)
    frmEventMsgs.Header = "Unset Highlight ?"
    frmEventMsgs.AcceptText = "Magnify"
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = "Past HPI"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        Call LoadMagnifyNew(lstHPILV)
        If (lstHist.ListCount > 0) Then
            lstHist.Visible = True
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        If (AreYouSure) Then
            If (UCase(lvi.SubItems(5)) = "C") Then
                Call UnsetComplaintHighlightNew(i, lvi.SubItems(5))
            ElseIf (UCase(lvi.SubItems(5)) = "V") Then
                Call UnsetHighlightNew(i, "V")
            End If
            If (lvi.SubItems(5) <> "c") And (lvi.SubItems(5) <> " ") Then
                Call VerifyHighlightNew(lstHPILV)
            ElseIf (lvi.SubItems(5) = " ") Then
                lstHPILV.ListItems.Remove lstHPILV.SelectedItem.Index
            Else
                lstHPILV.ListItems.Remove lstHPILV.SelectedItem.Index
            End If
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        Call LoadHistoricHPI
        If (lstHist.ListCount > 0) Then
            lstHist.Visible = True
        End If
    End If
End If
End Sub

Private Sub lstHPILV_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
If (lstHPILV.ListItems.Count < 1) Then
    frmEventMsgs.Header = "View Past HPI ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        Call LoadHistoricHPI
        If (lstHist.ListCount > 0) Then
            lstHist.Visible = True
        End If
    End If
End If
End Sub

Private Sub lstImpr_Click()
lstImpr.Visible = False
End Sub

Private Sub lstImpr_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
lstImpr.Visible = False
End Sub

Private Sub lstLinkNotes_Click()
lstLinkNotes.Visible = False
End Sub

Private Sub lstLinkNotes_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
lstLinkNotes.Visible = False
End Sub

Private Sub lstOcuMeds_Click()
If Not UserLogin.HasPermission(epHistoryAndAllergies) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
Dim RetAct As PracticeActivity
If (AppointmentId > 0) And (ActivityId < 1) Then
    Set RetAct = New PracticeActivity
    RetAct.ActivityAppointmentId = AppointmentId
    If (RetAct.FindActivity > 0) Then
        If (RetAct.SelectActivity(1)) Then
            ActivityId = RetAct.ActivityId
        End If
    End If
    Set RetAct = Nothing
    frmRxHistory.ActivityId = ActivityId
    frmRxHistory.EditPrevOn = frmSystems1.EditPreviousOn
    If (frmRxHistory.LoadHistoryRx(PatientId, frmSystems1.EditPreviousOn)) Then
        frmRxHistory.Show 1
    End If
End If
End Sub

Private Sub lstOTestsLV_Click()
Dim i As Integer
Dim j As Integer
Dim ClnId As Long
Dim Temp As String
Dim ATemp As String
Dim lvi As ListItem
If (lstOTestsLV.ListItems.Count > 0) Then
    i = Val(Mid(lstOTestsLV.SelectedItem, 2, Len(lstOTestsLV.SelectedItem) - 1))
    Set lvi = lstOTestsLV.ListItems(lstOTestsLV.SelectedItem.Index)
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Details"
    frmEventMsgs.RejectText = "Unset Highlight"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "View Document"
    frmEventMsgs.Other1Text = "Filter"
    If (lstOTestsLV.ListItems.Count <> lstOTestsOrgLV.ListItems.Count) Then
        frmEventMsgs.Other1Text = "UnFilter"
    End If
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        lstHist.Clear
        lstHist.AddItem "Close Details"
        If (lvi.SubItems(5) = "R") Then
            Call AddItemToList(lstHist, TestResultsStorage(i).TestText, TestResultsStorage(i).TestDate, i, TestResultsStorage(i).TestNote)
        Else
            Call AddItemToList(lstHist, QTestResultsStorage(i).TestText, QTestResultsStorage(i).TestDate, i, QTestResultsStorage(i).TestNote)
        End If
        lstHist.Visible = True
    ElseIf (frmEventMsgs.Result = 2) Then
        If (AreYouSure) Then
            ClnId = i
            If (ClnId > 0) Then
                Call UnsetHighlightNew(ClnId, lvi.SubItems(5))
                lstOTestsLV.ListItems.Remove lvi.Index
            End If
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        Temp = Trim(lvi.SubItems(3))
        If (Trim(Temp) <> "") Then
            If (lvi.SubItems(5) = "R") Then
                ATemp = TestResultsStorage(i).TestOrderNum
            Else
                ATemp = QTestResultsStorage(i).TestOrderNum
            End If
            Call StripCharacters(Temp, " ")
            If Not (TriggerViewDocuments(Temp, PrevApptId, lvi.SubItems(1), ATemp)) Then
                Call cmdDocs_Click
            End If
        Else
            Call cmdDocs_Click
        End If
    ElseIf (frmEventMsgs.Result = 5) Then
        If (frmEventMsgs.Other1Text = "Filter") Then
            Temp = Trim(lvi.SubItems(3))
            lstOTestsLV.ListItems.Clear
            For j = 1 To lstOTestsOrgLV.ListItems.Count
                If (Trim(lstOTestsOrgLV.ListItems.Item(j).SubItems(3)) = Temp) Then
                    lstOTestsLV.ListItems.Add = lstOTestsOrgLV.ListItems.Item(j).Text
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(1) = lstOTestsOrgLV.ListItems.Item(j).SubItems(1)
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(2) = lstOTestsOrgLV.ListItems.Item(j).SubItems(2)
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(3) = lstOTestsOrgLV.ListItems.Item(j).SubItems(3)
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(4) = lstOTestsOrgLV.ListItems.Item(j).SubItems(4)
                    lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(5) = lstOTestsOrgLV.ListItems.Item(j).SubItems(5)
                End If
            Next j
        Else
            lstOTestsLV.ListItems.Clear
            For j = 1 To lstOTestsOrgLV.ListItems.Count
                lstOTestsLV.ListItems.Add = lstOTestsOrgLV.ListItems.Item(j).Text
                lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(1) = lstOTestsOrgLV.ListItems.Item(j).SubItems(1)
                lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(2) = lstOTestsOrgLV.ListItems.Item(j).SubItems(2)
                lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(3) = lstOTestsOrgLV.ListItems.Item(j).SubItems(3)
                lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(4) = lstOTestsOrgLV.ListItems.Item(j).SubItems(4)
                lstOTestsLV.ListItems.Item(lstOTestsLV.ListItems.Count).SubItems(5) = lstOTestsOrgLV.ListItems.Item(j).SubItems(5)
            Next j
        End If
    End If
End If
End Sub

Private Sub lstPrevDrs_Click()
lstPrevDrs.Visible = False
End Sub

Private Sub lstPrevDrs_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
lstPrevDrs.Visible = False
End Sub

Private Sub lstProcsLV_Click()
Dim i As Integer
Dim ATemp As String
Dim ClnId As Long
Dim Temp As String
Dim lvi As ListItem
If (lstProcsLV.ListItems.Count > 0) Then
    i = lstProcsLV.SelectedItem.Index
    Set lvi = lstProcsLV.ListItems(i)
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Details"
    frmEventMsgs.RejectText = "Unset Highlight"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "View Document"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        Temp = lvi.SubItems(3)
        lstHist.Clear
        lstHist.AddItem "Close Details"
        If ((lvi.SubItems(5) = "R") Or (lvi.SubItems(5) = "q")) Then
            Call AddItemToList(lstHist, QTestResultsStorage(lvi.Text).TestText, QTestResultsStorage(lvi.Text).TestDate, lvi.Text, QTestResultsStorage(lvi.Text).TestNote)
        Else
            Call AddItemToList(lstHist, TestResultsStorage(lvi.Text).TestText, TestResultsStorage(lvi.Text).TestDate, lvi.Text, TestResultsStorage(lvi.Text).TestNote)
        End If
        lstHist.Visible = True
    ElseIf (frmEventMsgs.Result = 2) Then
        If (AreYouSure) Then
            If (i > 0) Then
                ClnId = i
                Call UnsetHighlightNew(ClnId, lvi.SubItems(5))
                lstProcsLV.ListItems.Remove lvi.Index
            End If
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        Temp = Trim(lvi.SubItems(3))
        If (Trim(Temp) <> "") Then
            If (lvi.SubItems(5) = "R") Then
                ATemp = TestResultsStorage(i).TestOrderNum
            Else
                ATemp = QTestResultsStorage(i).TestOrderNum
            End If
            Call StripCharacters(Temp, " ")
            If Not (TriggerViewDocuments(Temp, PrevApptId, lvi.SubItems(1), ATemp)) Then
                Call cmdDocs_Click
            End If
        Else
            Call cmdDocs_Click
        End If
    End If
End If
End Sub

Private Sub lstOrdersLV_Click()
Dim i As Long
Dim lvi As ListItem
Dim Id1 As String
If (lstOrdersLV.ListItems.Count > 0) Then
    Id1 = lstOrdersLV.SelectedItem
    i = Val(lstOrdersLV.SelectedItem)
    Set lvi = lstOrdersLV.FindItem(lstOrdersLV.SelectedItem)
    If (Left(lvi.SubItems(3), 2) <> "RX") Then
        frmEventMsgs.Header = "Unset Highlight ?"
        frmEventMsgs.AcceptText = "Magnify"
        frmEventMsgs.RejectText = "Yes"
        frmEventMsgs.CancelText = "No"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            Call LoadMagnifyNew(lstOrdersLV)
            If (lstHist.ListCount > 0) Then
                lstHist.Visible = True
            End If
        ElseIf (frmEventMsgs.Result = 2) Then
            If (AreYouSure) Then
                Call UnsetHighlightNew(i, lvi.SubItems(5))
                lstOrdersLV.ListItems.Remove lvi.Index
            End If
        End If
    End If
End If
End Sub

Private Sub lstExamLV_Click()
Dim i As Long
Dim lvi As ListItem
Dim Id1 As String
If (lstExamLV.ListItems.Count > 0) Then
    Id1 = lstExamLV.SelectedItem
    i = Val(lstExamLV.SelectedItem)
    Set lvi = lstExamLV.FindItem(lstExamLV.SelectedItem)
    frmEventMsgs.Header = "Unset Highlight ?"
    frmEventMsgs.AcceptText = "Magnify"
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        Call LoadMagnifyNew(lstExamLV)
        If (lstHist.ListCount > 0) Then
            lstHist.Visible = True
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        If (AreYouSure) Then
            If (lvi.SubItems(4) = "X") Then
                Call UnsetTestHighlightNew(i)
            ElseIf (UCase(lvi.SubItems(4)) = "C") Then
                Call UnsetComplaintHighlightNew(i, lvi.SubItems(4))
            Else
                If (Trim(lvi.SubItems(4)) = "") Then
                    Call UnsetHighlightNew(i, lvi.SubItems(5))
                Else
                    Call UnsetHighlightNew(i, lvi.SubItems(4))
                End If
                lstExamLV.ListItems.Remove lvi.Index
            End If
            Call VerifyHighlightNew(lstExamLV)
        End If
    End If
End If
End Sub

Private Function UnsetComplaintHighlight(AList As ListBox, ListIndex As Integer) As Boolean
Dim Ac As Boolean
Dim MyId As Long
Dim j As Long, AId As Long
Dim Af As Integer
Dim i As Integer, EndPoint As Integer
Dim Ar As String
Dim TempA As String, TempB As String
Dim Temp As String, ADate As String
Dim AQues As String, AAns As String
Dim RetCln As PatientClinical
Dim RetCln1 As PatientClinical
Temp = AList.List(ListIndex)
If (Len(Temp) > 75) Then
    j = Val(Mid(Temp, 77, Len(Temp) - 76))
    If (j > 0) Then
        If (Mid(Temp, 76, 1) = "C") Then
            GoSub DoDBHigh
        ElseIf (Mid(Temp, 76, 1) = "c") Then
            i = Val(Mid(Temp, 77, Len(Temp) - 76))
            If (frmSystems1.GetComplaintEntry(i, "B", AQues, ADate, Ac, Af, Ar, AId, True)) Then
                MyId = AId
                AQues = Mid(AQues, 2, Len(AQues) - 1)
                Call frmSystems1.GetComplaintEntry(i + 1, "B", AAns, ADate, Ac, Af, Ar, AId, True)
                EndPoint = i + 2
                Do Until Not (frmSystems1.GetComplaintEntry(EndPoint, "B", TempA, TempB, Ac, Af, Ar, AId, True))
                    If (Left(TempA, 1) = "/") Then
                        Exit Do
                    End If
                    EndPoint = EndPoint + 1
                Loop
                Call frmSystems1.SetComplaintHighlight("B", EndPoint - 1)
                Call frmSystems1.SetComplaintHighlightInFile(PatientId, AQues, AAns, False)
                If (MyId > 0) Then
                    j = MyId
                    GoSub DoDBHigh
                End If
            End If
        End If
    End If
End If
Exit Function
DoDBHigh:
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = j
    If (RetCln.RetrievePatientClinical) Then
        Set RetCln1 = New PatientClinical
        RetCln1.AppointmentId = RetCln.AppointmentId
        RetCln1.PatientId = RetCln.PatientId
        RetCln1.ClinicalType = RetCln.ClinicalType
        RetCln1.ClinicalHighlights = ""
        If (RetCln1.FindPatientClinicalbyHighlights > 0) Then
            i = 1
            Do Until Not (RetCln1.SelectPatientClinical(i))
                If (RetCln1.ClinicalType = RetCln.ClinicalType) Then
                    If (RetCln1.ClinicalId > RetCln.ClinicalId) Then
                        If (Left(RetCln1.Symptom, 1) <> "/") Then
                            RetCln1.ClinicalHighlights = ""
                            Call RetCln1.ApplyPatientClinical
                        ElseIf (Left(RetCln1.Symptom, 1) <> "/") Then
                            Exit Do
                        End If
                    End If
                Else
                    Exit Do
                End If
                i = i + 1
            Loop
        End If
        Set RetCln1 = Nothing
        RetCln.ClinicalHighlights = ""
        Call RetCln.ApplyPatientClinical
    End If
    Set RetCln = Nothing
    Return
End Function

Private Function UnsetComplaintHighlightNew(AId As Long, AType As String) As Boolean
Dim Ac As Boolean
Dim MyId As Long, j As Long
Dim Af As Integer
Dim i As Integer, EndPoint As Integer
Dim Ar As String
Dim TempA As String, TempB As String
Dim ADate As String
Dim AQues As String, AAns As String
Dim RetCln As PatientClinical
Dim RetCln1 As PatientClinical
If (AId > 0) Then
    j = AId
    If (AType = "C") Then
        GoSub DoDBHigh
    ElseIf (AType = "c") Then
        i = Int(AId)
        If (frmSystems1.GetComplaintEntry(i, "B", AQues, ADate, Ac, Af, Ar, AId, True)) Then
            MyId = AId
            AQues = Mid(AQues, 2, Len(AQues) - 1)
            Call frmSystems1.GetComplaintEntry(i + 1, "B", AAns, ADate, Ac, Af, Ar, AId, True)
            EndPoint = i + 2
            Do Until Not (frmSystems1.GetComplaintEntry(EndPoint, "B", TempA, TempB, Ac, Af, Ar, AId, True))
                If (Left(TempA, 1) = "/") Then
                    Exit Do
                End If
                EndPoint = EndPoint + 1
            Loop
            Call frmSystems1.SetComplaintHighlight("B", EndPoint - 1)
            Call frmSystems1.SetComplaintHighlightInFile(PatientId, AQues, AAns, False)
            If (MyId > 0) Then
                j = MyId
                GoSub DoDBHigh
            End If
        End If
    End If
End If
Exit Function
DoDBHigh:
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = j
    If (RetCln.RetrievePatientClinical) Then
        Set RetCln1 = New PatientClinical
        RetCln1.AppointmentId = RetCln.AppointmentId
        RetCln1.PatientId = RetCln.PatientId
        RetCln1.ClinicalType = RetCln.ClinicalType
        RetCln1.ClinicalHighlights = ""
        If (RetCln1.FindPatientClinicalbyHighlights > 0) Then
            i = 1
            Do Until Not (RetCln1.SelectPatientClinical(i))
                If (RetCln1.ClinicalType = RetCln.ClinicalType) Then
                    If (RetCln1.ClinicalId > RetCln.ClinicalId) Then
                        If (Left(RetCln1.Symptom, 1) <> "/") Then
                            RetCln1.ClinicalHighlights = ""
                            Call RetCln1.ApplyPatientClinical
                        ElseIf (Left(RetCln1.Symptom, 1) <> "/") Then
                            Exit Do
                        End If
                    End If
                Else
                    Exit Do
                End If
                i = i + 1
            Loop
        End If
        Set RetCln1 = Nothing
        RetCln.ClinicalHighlights = ""
        Call RetCln.ApplyPatientClinical
    End If
    Set RetCln = Nothing
    Return
End Function

Private Function ChangeTestHighlight(Idx As Integer, AClnId As Long) As Boolean
Dim i As Integer
Dim RetClnA As PatientClinical
Dim RetClnB As PatientClinical
ChangeTestHighlight = False
If (Idx > 0) Then
    Set RetClnA = New PatientClinical
    RetClnA.ClinicalType = "F"
    RetClnA.PatientId = PatientId
    RetClnA.AppointmentId = TestResultsStorage(Idx).TestApptId
    RetClnA.Symptom = "/" + TestResultsStorage(Idx).TestQuestion
    RetClnA.Findings = ""
    RetClnA.EyeContext = ""
    RetClnA.ImageDescriptor = ""
    RetClnA.ImageInstructions = ""
    RetClnA.ClinicalHighlights = "T"
    If (RetClnA.FindPatientClinical > 0) Then
        i = 1
        While (RetClnA.SelectPatientClinical(i))
            RetClnA.ClinicalHighlights = "R"
            Call RetClnA.ApplyPatientClinical
            ChangeTestHighlight = True
            i = i + 1
        Wend
    End If
    Set RetClnA = Nothing
ElseIf (AClnId > 0) Then
    Set RetClnB = New PatientClinical
    RetClnB.ClinicalId = AClnId
    If (RetClnB.RetrievePatientClinical) Then
        Set RetClnA = New PatientClinical
        RetClnA.ClinicalType = "F"
        RetClnA.PatientId = RetClnB.PatientId
        RetClnA.AppointmentId = RetClnB.AppointmentId
        RetClnA.Symptom = RetClnB.Symptom
        RetClnA.Findings = ""
        RetClnA.EyeContext = ""
        RetClnA.ImageDescriptor = ""
        RetClnA.ImageInstructions = ""
        RetClnA.ClinicalHighlights = ""
        If (RetClnA.FindPatientClinical > 0) Then
            i = 1
            While (RetClnA.SelectPatientClinical(i))
                RetClnA.ClinicalHighlights = "R"
                Call RetClnA.ApplyPatientClinical
                ChangeTestHighlight = True
                i = i + 1
            Wend
        End If
        Set RetClnA = Nothing
    End If
End If
End Function

Private Function UnsetTestHighlightNew(AId As Long) As Boolean
Dim i As Integer
Dim RetClnA As PatientClinical
UnsetTestHighlightNew = False
If (AId > 0) Then
    Set RetClnA = New PatientClinical
    RetClnA.ClinicalType = "F"
    RetClnA.PatientId = PatientId
    RetClnA.AppointmentId = TestResultsStorage(AId).TestApptId
    RetClnA.Symptom = "/" + TestResultsStorage(AId).TestQuestion
    RetClnA.Findings = ""
    RetClnA.EyeContext = ""
    RetClnA.ImageDescriptor = ""
    RetClnA.ImageInstructions = ""
    If (RetClnA.FindPatientClinical > 0) Then
        i = 1
        While (RetClnA.SelectPatientClinical(i))
            RetClnA.ClinicalHighlights = ""
            Call RetClnA.ApplyPatientClinical
            UnsetTestHighlightNew = True
            i = i + 1
        Wend
    End If
    Set RetClnA = Nothing
End If
End Function

Private Function UnsetHighlightNew(ID As Long, IType As String) As Boolean
Dim q As Integer
Dim AId As Long
Dim RetCln As PatientClinical
Dim RetCln1 As PatientClinical
Dim RetNote As PatientNotes
UnsetHighlightNew = False
AId = 0
If (ID > 0) Then
    If (InStrPS("VXYPaO] ", IType) > 0) Then
        AId = ID
    ElseIf (IType = "T") Or (IType = "p") Then
        AId = TestResultsStorage(ID).TestClnId
    ElseIf (IType = "R") Or (IType = "q") Then
        AId = QTestResultsStorage(ID).TestClnId
    End If
    If (AId > 0) Then
        If (IType = "X") Or (IType = "P") Or (IType = " ") Or (IType = "O") Then
            Set RetCln = New PatientClinical
            RetCln.ClinicalId = AId
            If (RetCln.RetrievePatientClinical) Then
                RetCln.ClinicalHighlights = ""
                Call RetCln.ApplyPatientClinical
                UnsetHighlightNew = True
            End If
            Set RetCln = Nothing
        ElseIf (IType = "T") Or (IType = "q") Or (IType = "p") Or (IType = "R") Then
            Set RetCln1 = New PatientClinical
            RetCln1.ClinicalId = AId
            If (RetCln1.RetrievePatientClinical) Then
                Set RetCln = New PatientClinical
                RetCln.AppointmentId = RetCln1.AppointmentId
                RetCln.PatientId = RetCln1.PatientId
                RetCln.Symptom = RetCln1.Symptom
                RetCln.Findings = ""
                RetCln.ClinicalType = RetCln1.ClinicalType
                If (RetCln.FindPatientClinical > 0) Then
                    q = 1
                    While (RetCln.SelectPatientClinical(q))
                        RetCln.ClinicalHighlights = ""
                        Call RetCln.ApplyPatientClinical
                        UnsetHighlightNew = True
                        q = q + 1
                    Wend
                End If
                Set RetCln = Nothing
            End If
            Set RetCln1 = Nothing
        ElseIf (IType = "V") Or (IType = "]") Then
            Set RetNote = New PatientNotes
            RetNote.NotesId = AId
            If (RetNote.RetrieveNotes) Then
                RetNote.NotesHighlight = ""
                Call RetNote.ApplyNotes
                UnsetHighlightNew = False
            End If
            Set RetNote = Nothing
        End If
    End If
End If
End Function

Private Function UnSetOn(IType As Integer) As Integer
UnSetOn = 0
frmEventMsgs.Header = "Unset Highlight ?"
frmEventMsgs.AcceptText = "Magnify"
If (IType = 9) Then
    frmEventMsgs.AcceptText = ""
End If
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
If (IType = 3) Then
    frmEventMsgs.Other0Text = "Past HPI"
End If
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    UnSetOn = 2
ElseIf (frmEventMsgs.Result = 1) Then
    UnSetOn = 3
End If
End Function

Private Function IsProcOrTest(AQuest As String) As String
Dim RetQues As DynamicClass
IsProcOrTest = ""
If (Trim(AQuest) <> "") Then
    Set RetQues = New DynamicClass
    RetQues.Question = AQuest
    If (RetQues.RetrieveClassbyQuestion) Then
        IsProcOrTest = RetQues.QuestionIndicator
    End If
    Set RetQues = Nothing
End If
End Function

Private Function GetAppointmentDate(ApptId As Long, ApptDate As String, ADoc As String) As Boolean
Dim RetAppt As SchedulerAppointment
Dim RetRes As SchedulerResource
GetAppointmentDate = False
ApptDate = ""
ADoc = ""
If (ApptId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ApptDate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
        Set RetRes = New SchedulerResource
        RetRes.ResourceId = RetAppt.AppointmentResourceId1
        If (RetRes.RetrieveSchedulerResource) Then
            ADoc = Trim(RetRes.ResourceName)
        End If
        Set RetRes = Nothing
        GetAppointmentDate = True
    End If
    Set RetAppt = Nothing
End If
End Function

Private Function CloseOutOrders() As Boolean
Dim OrdId As Long, ClnId As Long
Dim lvi As ListItem
Dim TryIt As Boolean
Dim GotIt As Boolean, OkayLoad As Boolean
Dim VerifyOD As Boolean, VerifyOS As Boolean
Dim z As Integer, w As Integer
Dim i As Integer, k As Integer, u As Integer
Dim AEye As String
Dim TTemp As String, IRef As String
Dim LDate As String, oDate As String
Dim ADate As String, AOrder As String
Dim ACode As String, ARef As String
Dim RetCln As PatientClinical
CloseOutOrders = True
OkayLoad = False
For i = 1 To lstOrdersLV.ListItems.Count
    Set lvi = lstOrdersLV.ListItems.Item(i)
    OrdId = Val(lvi.SubItems(6))
    If (OrdId > 0) Then
        ADate = Trim(lvi.SubItems(1))
        AEye = Trim(lvi.SubItems(2))
        ACode = Trim(lvi.SubItems(3))
        If (lvi.SubItems(5) = "O") Then
            ARef = "External Tests"
            w = InStrPS(ACode, ":")
            If (w > 0) Then
                If (w <> Len(Trim(ACode))) Then
                    ARef = "ExternalTestLinks"
                End If
            End If
        ElseIf (lvi.SubItems(5) = "P") Then
            ARef = "OfficeProcedures"
        ElseIf (lvi.SubItems(5) = "Y") Then
            ARef = "SurgeryType"
            w = InStrPS(ACode, "(")
            If (w > 0) Then
                ACode = Trim(Left(ACode, w - 1))
            End If
        End If
        If (Trim(ARef) <> "") Then
            If (ARef <> "ExternalTestLinks") Then
                z = InStrPS(ACode, ":")
                If (z > 0) Then
                    ARef = Left(ACode, z - 1)
                    ACode = Mid(ACode, z + 1, Len(ACode) - z)
                End If
            End If
            Call GetCodeOrder(ARef, ACode, AOrder)
            If (AOrder <> "") Then
                oDate = "20" + Mid(ADate, 7, 2) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
                For k = 1 To MaxTestResults
                    TryIt = False
                    If (Trim(TestResultsStorage(k).TestOrderNum) = AOrder) And (TestResultsStorage(k).TestLocalStatus <> "R") Then
                        LDate = Mid(TestResultsStorage(k).TestDate, 7, 4) + Mid(TestResultsStorage(k).TestDate, 1, 2) + Mid(TestResultsStorage(k).TestDate, 4, 2)
                        If (LDate >= oDate) Then
                            GotIt = False
                            If (lvi.SubItems(5) = "P") Then
                                If (AEye <> "--") Then
                                    If (AEye = "OD") Or (AEye = "OU") Then
                                        GoSub VerifyODContent
                                    End If
                                    If (AEye = "OS") Or (AEye = "OU") Then
                                        GoSub VerifyOSContent
                                    End If
                                    If (AEye = "OD") And (VerifyOD) Then
                                        GotIt = True
                                    End If
                                    If (AEye = "OS") And (VerifyOS) Then
                                        GotIt = True
                                    End If
                                    If (AEye = "OU") And ((VerifyOS) Or (VerifyOD)) Then
                                        GotIt = True
                                    End If
                                Else
                                    GotIt = True
                                End If
                            Else
                                GotIt = True
                            End If
                            If (GotIt) Then
                                Set RetCln = New PatientClinical
                                RetCln.ClinicalId = OrdId
                                If (RetCln.RetrievePatientClinical) Then
                                    RetCln.ClinicalHighlights = ""
                                    Call RetCln.ApplyPatientClinical
                                    TryIt = True
                                End If
                                Set RetCln = Nothing
                                TestResultsStorage(k).TestLocalStatus = "R"
                                OkayLoad = True
                                Exit For
                            End If
                        End If
                    End If
                Next k
' this is done to catch any non-highlighted tests
                If Not (TryIt) Then
                    ClnId = IsTestPerformed(AOrder, oDate, AEye, AppointmentId)
                    If (ClnId > 0) Then
                        Set RetCln = New PatientClinical
                        RetCln.ClinicalId = OrdId
                        If (RetCln.RetrievePatientClinical) Then
                            RetCln.ClinicalHighlights = ""
                            Call RetCln.ApplyPatientClinical
                        End If
                        Set RetCln = Nothing
                        Call ChangeTestHighlight(0, ClnId)
                        OkayLoad = True
                    End If
                End If
            End If
        End If
    End If
Next i
If (OkayLoad) Then
    Call VerifyHighlightNew(lstOrdersLV)
    Call VerifyHighlightNew(lstProcsLV)
    Call VerifyHighlightNew(lstOTestsLV)
End If
Exit Function
VerifyODContent:
    IRef = ""
    VerifyOD = False
    TTemp = Trim(TestResultsStorage(k).TestText)
    Call StripCharacters(TTemp, Chr(13))
    Call StripCharacters(TTemp, Chr(10))
    z = InStrPS(TTemp, "OS:")
    If (z > 0) Then
        u = InStrPS(TTemp, "OD:")
        If (u > 0) Then
            IRef = Trim(Mid(TTemp, u + 3, (z - 1) - (u + 2)))
        End If
    Else
        u = Len(TTemp) + 1
        IRef = Trim(Mid(TTemp, u + 3, (z - 1) - (u + 2)))
    End If
    If (Trim(IRef) <> "") Then
        VerifyOD = True
    End If
    Return
VerifyOSContent:
    IRef = ""
    VerifyOS = False
    TTemp = Trim(TestResultsStorage(k).TestText)
    Call StripCharacters(TTemp, Chr(13))
    Call StripCharacters(TTemp, Chr(10))
    z = InStrPS(TTemp, "OS:")
    If (z > 0) Then
        IRef = Trim(Mid(TTemp, z + 3, Len(TTemp) - (z + 2)))
    End If
    If (Trim(IRef) <> "") Then
        VerifyOS = True
    End If
    Return
End Function

Private Function GetCodeOrder(ARef As String, ACode As String, AOrder As String) As Boolean
Dim RetCode As PracticeCodes
GetCodeOrder = False
AOrder = ""
If (Trim(ARef) <> "") And (Trim(ACode) <> "") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = ARef
    RetCode.ReferenceCode = ACode
    If (RetCode.FindCode > 0) Then
        If (RetCode.SelectCode(1)) Then
            AOrder = Trim(RetCode.TestOrder)
            GetCodeOrder = True
        End If
    End If
    Set RetCode = Nothing
End If
End Function

Private Function GetTestOrder(TheQuestion As String, TheOrder As String) As Boolean
Dim RetrieveQuestion As DynamicClass
GetTestOrder = False
TheOrder = ""
If (Trim(TheQuestion) <> "") Then
    Set RetrieveQuestion = New DynamicClass
    RetrieveQuestion.QuestionSet = "First Visit"
    RetrieveQuestion.QuestionParty = "T"
    RetrieveQuestion.Question = Trim(TheQuestion)
    If (RetrieveQuestion.RetrieveClassbyQuestion) Then
        TheOrder = Trim(RetrieveQuestion.QuestionOrder)
        GetTestOrder = True
    End If
    Set RetrieveQuestion = Nothing
End If
End Function

Private Function GetOrderQuestion(TheOrder As String, TheQuestion As String) As Boolean
Dim RetrieveQuestion As DynamicClass
GetOrderQuestion = False
TheQuestion = ""
If (Trim(TheOrder) <> "") Then
    Set RetrieveQuestion = New DynamicClass
    RetrieveQuestion.QuestionSet = "First Visit"
    RetrieveQuestion.QuestionParty = "T"
    RetrieveQuestion.QuestionOrder = Trim(TheOrder)
    If (RetrieveQuestion.FindClassForms > 0) Then
        If (RetrieveQuestion.SelectClassForm(1)) Then
            TheQuestion = Trim(RetrieveQuestion.Question)
            GetOrderQuestion = True
        End If
    End If
    Set RetrieveQuestion = Nothing
End If
End Function

Private Function GetReferenceFromAction(TheAct As String, StartAt As Integer, LocStr As String, RetStr As String) As Boolean
Dim i As Integer
Dim Temp As String
GetReferenceFromAction = False
RetStr = ""
If (Trim(TheAct) <> "") And (StartAt > 0) And (StartAt < Len(TheAct)) And (Trim(LocStr) <> "") Then
    i = InStrPS(StartAt, TheAct, LocStr)
    If (i > 0) Then
        Temp = Mid(TheAct, StartAt, i - (StartAt - 1))
        Temp = Left(Temp, Len(Temp) - Len(LocStr))
        RetStr = Temp
        StartAt = i + Len(LocStr)
        GetReferenceFromAction = True
    End If
End If
End Function

Private Function GetComplaintsLingo(AComp As String, BComp As String, AText As String, AFamily As Integer) As Boolean
On Error GoTo UI_ErrorHandler
Dim TheSys As String
Dim TheDiag As String
Dim i As Integer, q As Integer
Dim s As Integer, t As Integer
Dim Y As Integer, z As Boolean
Dim s1 As Long, ApptId As Long
Dim TheQuestion As String
Dim TheComplaint As String
Dim DoctorLingo As String
Dim ControlId As Long
Dim QuestionType As String * 1
Dim RetrieveControl1 As DynamicControls
Dim RetrieveControl2 As DynamicSecondaryControls
Dim RetrieveControl3 As DynamicThirdControls
Dim RetrieveControl4 As DynamicFourthControls
Dim DmPrimary As DiagnosisMasterPrimary
Dim CAllerg As CAllergies

GetComplaintsLingo = False
AText = ""
AFamily = 0
If (Trim(AComp) <> "") And (Trim(BComp) <> "") Then
    TheComplaint = AComp
    If (Left(AComp, 1) = "/") Then
        TheQuestion = TheComplaint
        QuestionType = Left(TheComplaint, 1)
        AFamily = frmSystems1.SetFamilybyQuestion(Mid(TheQuestion, 2, Len(TheQuestion) - 1))
    End If
    If (InStrPS("?[]", Left(AComp, 1)) <> 0) Then
        QuestionType = Left(AComp, 1)
    End If
    q = InStrPS(BComp, "NOTSURE")
    If (q <> 0) Then
        DoctorLingo = "Not Sure - " + Mid(TheQuestion, 2, Len(TheQuestion) - 1)
    ElseIf (Left(BComp, 8) = "NEWDIAG:") Then
        q = InStrPS(BComp, "=")
        If (q > 0) Then
            t = InStrPS(q, BComp, "<")
            If t > 0 Then
                Set DmPrimary = New DiagnosisMasterPrimary
                DmPrimary.PrimaryId = Val(Trim$(Mid(BComp, q + 2, t - q - 1)))
                If DmPrimary.RetrievePrimary Then
                    DoctorLingo = DmPrimary.PrimaryLingo
                End If
                Set DmPrimary = Nothing
            End If
        Else
            DoctorLingo = Trim(Mid(BComp, 9, Len(BComp) - 8))
        End If
    Else
        Set RetrieveControl1 = New DynamicControls
        Set RetrieveControl2 = New DynamicSecondaryControls
        Set RetrieveControl3 = New DynamicThirdControls
        Set RetrieveControl4 = New DynamicFourthControls
        
        q = InStrPS(BComp, "=T")
        If (q <> 0) Then
            ControlId = Val(Trim(Mid(BComp, q + 2, Len(BComp) - (q + 1))))
            If (QuestionType = "/") Then
                RetrieveControl1.FormId = 0
                RetrieveControl1.ControlId = ControlId
                RetrieveControl1.ControlName = ""
                RetrieveControl1.IEChoiceName = ""
                If (RetrieveControl1.RetrieveControl) Then
                    If (Trim(RetrieveControl1.DoctorLingo) <> "") Then
                        DoctorLingo = RetrieveControl1.DoctorLingo
                    ElseIf (Trim(RetrieveControl1.ConfirmLingo) <> "") Then
                        DoctorLingo = RetrieveControl1.ConfirmLingo
                    Else
                        DoctorLingo = ""
                    End If
                End If
            ElseIf (QuestionType = "?") Then
                RetrieveControl2.SecondaryFormId = 0
                RetrieveControl2.SecondaryControlId = ControlId
                RetrieveControl2.ControlName = ""
                RetrieveControl2.IEChoiceName = ""
                If (RetrieveControl2.RetrieveControl) Then
                    If (Trim(RetrieveControl2.DoctorLingo) <> "") Then
                        DoctorLingo = RetrieveControl2.DoctorLingo
                    ElseIf (Trim(RetrieveControl2.ConfirmLingo) <> "") Then
                        DoctorLingo = RetrieveControl2.ConfirmLingo
                    Else
                        DoctorLingo = ""
                    End If
                End If
            ElseIf (QuestionType = "]") Then
                RetrieveControl3.ThirdFormId = 0
                RetrieveControl3.ThirdControlId = ControlId
                RetrieveControl3.ControlName = ""
                RetrieveControl3.IEChoiceName = ""
                If (RetrieveControl3.RetrieveControl) Then
                    If (Trim(RetrieveControl3.DoctorLingo) <> "") Then
                        DoctorLingo = RetrieveControl3.DoctorLingo
                    ElseIf (Trim(RetrieveControl3.ConfirmLingo) <> "") Then
                        DoctorLingo = RetrieveControl3.ConfirmLingo
                    Else
                        DoctorLingo = ""
                    End If
                End If
            ElseIf (QuestionType = "[") Then
                RetrieveControl4.FourthFormId = 0
                RetrieveControl4.FourthControlId = ControlId
                RetrieveControl4.ControlName = ""
                RetrieveControl4.IEChoiceName = ""
                If (RetrieveControl4.RetrieveControl) Then
                    If (Trim(RetrieveControl4.DoctorLingo) <> "") Then
                        DoctorLingo = RetrieveControl4.DoctorLingo
                    ElseIf (Trim(RetrieveControl4.ConfirmLingo) <> "") Then
                        DoctorLingo = RetrieveControl4.ConfirmLingo
                    Else
                        DoctorLingo = ""
                    End If
                End If
            Else
                DoctorLingo = ""
            End If
        Else
            If UCase(AComp) = "/ALLERGIES ALL" Then
                DoctorLingo = Mid(BComp, 1, Len(BComp) - 1)
                Do
                    q = InStrPS(1, DoctorLingo, "<")
                    If q > 0 Then
                        DoctorLingo = Mid(DoctorLingo, q + 1)
                        q = InStrPS(1, DoctorLingo, "<")
                        If q > 0 Then
                            Set CAllerg = New CAllergies
                            If CAllerg.GetAllergyId(DoctorLingo) > 0 Then Exit Do
                        Else
                            Exit Do
                        End If
                    Else
                        DoctorLingo = ""
                    End If
                    If DoctorLingo = "" Then Exit Do
                Loop
            Else
                DoctorLingo = ""
            End If
        End If
        Set RetrieveControl1 = Nothing
        Set RetrieveControl2 = Nothing
        Set RetrieveControl3 = Nothing
        Set RetrieveControl4 = Nothing
    End If
    AText = Trim(DoctorLingo)
    
' its probably because its a Chief complaint (maybe)
    If (Trim(AText) = "") Then
        AText = Trim(BComp)
    End If
    
    If IsNumeric(Mid(AText, 1, 1)) Then
        AText = "_" & AText
    End If
    i = i + 1
End If
GetComplaintsLingo = True
Exit Function
UI_ErrorHandler:
    GetComplaintsLingo = False
    Set RetrieveControl1 = Nothing
    Set RetrieveControl2 = Nothing
    Set RetrieveControl3 = Nothing
    Set RetrieveControl4 = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Function IsTestPerformed(OOrder As String, oDate As String, OEye As String, OApptId As Long) As Long
Dim i As Integer
Dim ATemp As String
Dim BTemp As String
Dim CTemp As String
Dim DocName As String
Dim CheckEye As Boolean
Dim FirstClnId As Long
Dim RetCln As PatientClinical
Dim RetNote As PatientNotes
BTemp = ""
IsTestPerformed = 0
FirstClnId = 0
If (Trim(OOrder) <> "") And (Trim(oDate) <> "") And (OApptId > 0) Then
    If (GetOrderQuestion(OOrder, BTemp)) Then
        ATemp = OOrder
        CheckEye = False
        Set RetCln = New PatientClinical
        RetCln.AppointmentId = 0
        RetCln.PatientId = PatientId
        RetCln.ClinicalType = "F"
        RetCln.Symptom = "/" + Trim(BTemp)
        RetCln.Findings = ""
        If (RetCln.FindPatientClinicalTests > 0) Then
            i = 1
            Do Until Not (RetCln.SelectPatientClinical(i))
                If (FirstClnId < 1) Then
                    FirstClnId = RetCln.ClinicalId
                    CheckEye = True
                End If
                If (RetCln.ClinicalHighlights <> "R") And (RetCln.ClinicalHighlights <> "T") And (CheckEye) Then
                    If (ATemp <> "") And (ATemp = OOrder) Then
                        Call GetAppointmentDate(RetCln.AppointmentId, CTemp, DocName)
                        CTemp = Mid(CTemp, 7, 4) + Mid(CTemp, 1, 2) + Mid(CTemp, 4, 2)
                        If (CTemp >= oDate) Then
                            CheckEye = True
                            If (InStrPS(RetCln.Findings, OEye) <> 0) Then
                                IsTestPerformed = RetCln.ClinicalId
                                Exit Do
                            ElseIf (OEye = "OU") And (InStrPS(RetCln.Findings, "OS") <> 0) Or (InStrPS(RetCln.Findings, "OD") <> 0) Then
                                IsTestPerformed = RetCln.ClinicalId
                                Exit Do
                            ElseIf (OEye = "--") Then
                                IsTestPerformed = RetCln.ClinicalId
                                Exit Do
                            End If
                        End If
                    End If
                Else
                    CheckEye = False
                    CTemp = ""
                End If
                i = i + 1
            Loop
        End If
        Set RetCln = Nothing
        If (IsTestPerformed = 0) And (FirstClnId > 0) Then
            Set RetNote = New PatientNotes
            RetNote.NotesAppointmentId = OApptId
            RetNote.NotesPatientId = PatientId
            RetNote.NotesEye = ""
            RetNote.NotesSystem = "T" + OOrder
            If (RetNote.FindNotes > 0) Then
                IsTestPerformed = FirstClnId
            End If
            Set RetNote = Nothing
        End If
    End If
End If
End Function

Private Function VerifyHighlightNew(RefBox As ListView) As Boolean
Dim i As Integer, j As Integer
Dim iId As Long, AId As Long
Dim AType As String
Dim RetNote As PatientNotes
Dim RetCln As PatientClinical
VerifyHighlightNew = True
If (RefBox.ListItems.Count > 0) Then
    j = RefBox.ListItems.Count
    For i = 1 To j
        iId = 0
        AId = 0
        AType = ""
        If (i > RefBox.ListItems.Count) Then
            Exit For
        End If
        If (RefBox.Name = "lstHPILV") Then
            iId = Val(RefBox.ListItems.Item(i).SubItems(6))
            AType = RefBox.ListItems.Item(i).SubItems(5)
        ElseIf (RefBox.Name = "lstOrdersLV") Then
            iId = Val(RefBox.ListItems.Item(i).SubItems(6))
            AType = RefBox.ListItems.Item(i).SubItems(5)
        Else
            iId = Val(RefBox.ListItems.Item(i).SubItems(5))
            AType = RefBox.ListItems.Item(i).SubItems(6)
        End If
        If (iId > 0) Then
            If (AType = "T") Or (AType = "p") Then
                AId = TestResultsStorage(iId).TestClnId
            ElseIf (AType = "R") Or (AType = "q") Then
                AId = QTestResultsStorage(iId).TestClnId
            ElseIf (AType = "V") Then
                AId = iId
            End If
        End If
        If (AId > 0) And (iId > 0) And (AType <> "V") And (UCase(AType) <> "C") Then
            If (AType = "R") Then
                If (QTestResultsStorage(iId).TestLocalStatus = "R") Then
                    RefBox.ListItems.Remove RefBox.ListItems.Item(i).Index
                    i = i - 1
                    j = j - 1
                    If (j < 0) Then
                        Exit For
                    End If
                    If (i >= RefBox.ListItems.Count) Then
                        Exit For
                    End If
                End If
            End If
        ElseIf (UCase(AType) = "V") Then
            Set RetNote = New PatientNotes
            RetNote.NotesId = AId
            If (RetNote.RetrieveNotes) Then
                If (Trim(RetNote.NotesHighlight) = "") Then
                    RefBox.ListItems.Remove RefBox.ListItems.Item(i).Index
                    i = i - 1
                    j = j - 1
                    If (j < 0) Then
                        Set RetNote = Nothing
                        Exit For
                    End If
                    If (i >= RefBox.ListItems.Count) Then
                        Exit For
                    End If
                End If
            End If
            Set RetNote = Nothing
        ElseIf (AType = "P") Or (AType = "O") Then
            Set RetCln = New PatientClinical
            RetCln.ClinicalId = iId
            If (RetCln.RetrievePatientClinical) Then
                If (RetCln.ClinicalHighlights = "") Then
                    RefBox.ListItems.Remove RefBox.ListItems.Item(i).Index
                    i = i - 1
                    j = j - 1
                    If (j < 0) Then
                        Exit For
                    End If
                    If (i >= RefBox.ListItems.Count) Then
                        Exit For
                    End If
                End If
            End If
        ElseIf (AType = "c") Then
            RefBox.ListItems.Remove RefBox.ListItems.Item(i).Index
            Exit For
            i = i - 1
            j = j - 1
            If (j < 0) Then
                Exit For
            End If
            If (i >= RefBox.ListItems.Count) Then
                Exit For
            End If
        End If
    Next i
End If
End Function

Private Function LoadMagnifyNew(AList As ListView) As Boolean
Dim i As Integer
Dim w As Integer
Dim Temp As String
Dim lvi As ListItem
lstHist.Clear
lstHist.AddItem "Close Magnify"
For i = 1 To AList.ListItems.Count
    Temp = Space(75)
    Set lvi = AList.ListItems(i)
    If (Len(lvi.SubItems(3)) > 50) Then
        Mid(Temp, 1, 8) = lvi.SubItems(1)
        Mid(Temp, 10, 2) = lvi.SubItems(2)
        For w = 1 To Len(lvi.SubItems(3)) Step 50
            Mid(Temp, 13, 50) = Space(50)
            Mid(Temp, 13, 50) = Mid(lvi.SubItems(3), w, 50)
            lstHist.AddItem Temp
        Next w
    Else
        Mid(Temp, 1, 8) = lvi.SubItems(1)
        Mid(Temp, 10, 2) = lvi.SubItems(2)
        Mid(Temp, 13, Len(Trim(lvi.SubItems(3)))) = lvi.SubItems(3)
        lstHist.AddItem Temp
    End If
Next i
End Function

Private Function LoadMagnifyRV(EPVOn As Boolean) As Boolean
Dim i As Integer
Dim Rec As String
lstHist.Clear
lstHist.AddItem "Close Magnify"
If Not (EPVOn) Then
    i = 1
    While (frmSystems1.GetRVReference(i, Rec))
        lstHist.AddItem Rec
        i = i + 1
    Wend
Else
End If
End Function

Private Function GetDrawingBase(ATemp As String, ApptId As Long, IBase As String, ICln As Long, DrawId As Long) As Boolean
Dim u As Integer
Dim i As Integer, p As Integer
Dim MyFile As String
Dim TempFile As String
Dim MyDiag As String
Dim MySys As String
Dim JTemp As String
Dim RetCln As PatientClinical
Dim RetDiag As DiagnosisMasterBrush
Dim RetAssoc As DiagnosisMasterAssociated
GetDrawingBase = False
MySys = ""
MyDiag = ""
IBase = ""
ICln = 0
If (Trim(ATemp) <> "") And (ApptId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.PatientId = PatientId
    RetCln.AppointmentId = ApptId
    RetCln.ClinicalType = "Q"
    RetCln.Findings = ATemp
    If (RetCln.FindPatientClinical > 0) Then
        If (RetCln.SelectPatientClinical(1)) Then
            If (Trim(RetCln.ClinicalDraw) = "") Then
                MySys = RetCln.ImageDescriptor
                Set RetCln = Nothing
                GoSub GetDiagId
                If (MyDiag <> "") Then
                    GoSub ImageRec
                Else
                    GoSub GetAssDiagId
                End If
            Else
                ICln = RetCln.ClinicalId
                IBase = Trim(RetCln.ClinicalDraw)
                MySys = Trim(RetCln.ImageDescriptor)
                GetDrawingBase = True
            End If
        End If
    End If
    Set RetCln = Nothing
    If Not (GetDrawingBase) Then
        JTemp = ATemp
        Call ReplaceCharacters(JTemp, ".", "-")
        Set RetCln = New PatientClinical
        RetCln.PatientId = PatientId
        RetCln.AppointmentId = ApptId
        RetCln.ClinicalType = "I"
        RetCln.Findings = JTemp
        If (RetCln.FindPatientClinical > 0) Then
            If (RetCln.SelectPatientClinical(1)) Then
                ICln = RetCln.ClinicalId
                IBase = Trim(RetCln.ClinicalDraw)
                If (IBase = "") Then
                    IBase = "I-08"
                End If
                GetDrawingBase = True
            End If
        End If
        Set RetCln = Nothing
    End If
End If
If Not (GetDrawingBase) Then
    For u = 0 To lstPrevDrs.ListCount - 1
        TempFile = MyImageDir + "MyDraw-A" + Trim(lstPrevDrs.ItemData(u)) + "-*I-08.*"
        MyFile = Dir(TempFile)
        If (MyFile <> "") Then
            i = InStrPS(MyFile, "-")
            If (i > 0) Then
                p = InStrPS(i + 1, MyFile, "-")
                If (p > 0) Then
                    DrawId = Val(Mid(MyFile, i + 2, (p - 1) - (i + 1)))
                    IBase = "I-08"
                    Set RetCln = New PatientClinical
                    RetCln.PatientId = PatientId
                    RetCln.AppointmentId = DrawId
                    RetCln.ClinicalType = "I"
                    RetCln.Findings = ""
                    RetCln.ImageDescriptor = ""
                    RetCln.ImageInstructions = ""
                    RetCln.ClinicalDraw = "I-08"
                    If (RetCln.FindPatientClinical > 0) Then
                        If (RetCln.SelectPatientClinical(1)) Then
                            ICln = RetCln.ClinicalId
                            GetDrawingBase = True
                        End If
                    End If
                    Set RetCln = Nothing
                    Exit For
                End If
            End If
        End If
    Next u
End If
Exit Function
GetAssDiagId:
    MyDiag = ""
    If (MySys <> "") Then
        Set RetAssoc = New DiagnosisMasterAssociated
        RetAssoc.AssociatedDiagnosis = ATemp
        If (RetAssoc.FindAssociatedbyPrimary > 0) Then
            If (RetAssoc.SelectAssociated(1)) Then
                MyDiag = Trim(RetAssoc.PrimaryDiagnosis)
            End If
        End If
        Set RetAssoc = Nothing
        If (MyDiag <> "") Then
            JTemp = MyDiag
            Call ReplaceCharacters(JTemp, ".", "-")
            Set RetCln = New PatientClinical
            RetCln.PatientId = PatientId
            RetCln.AppointmentId = ApptId
            RetCln.ClinicalType = "I"
            RetCln.Findings = JTemp
            If (RetCln.FindPatientClinical > 0) Then
                If (RetCln.SelectPatientClinical(1)) Then
                    ICln = RetCln.ClinicalId
                    IBase = Trim(RetCln.ClinicalDraw)
                    GetDrawingBase = True
                End If
            End If
            Set RetCln = Nothing
        End If
    End If
    Return
GetDiagId:
    MyDiag = ""
    If (MySys <> "") Then
        Set RetDiag = New DiagnosisMasterBrush
        RetDiag.BrushDiagnosis = ATemp
        If (RetDiag.FindBrushbyDiagnosis > 0) Then
            If (RetDiag.SelectBrush(1)) Then
                MyDiag = Trim(RetDiag.BrushName)
            End If
        End If
        Set RetDiag = Nothing
    End If
    Return
ImageRec:
    Set RetCln = New PatientClinical
    RetCln.PatientId = PatientId
    RetCln.AppointmentId = ApptId
    RetCln.ClinicalType = "I"
    RetCln.Findings = ""
    RetCln.Symptom = ""
    RetCln.ImageDescriptor = MyDiag
    RetCln.ImageInstructions = ""
    If (RetCln.FindPatientClinical > 0) Then
        If (RetCln.SelectPatientClinical(1)) Then
            ICln = RetCln.ClinicalId
            IBase = Trim(RetCln.ClinicalDraw)
            GetDrawingBase = True
        End If
    End If
    Set RetCln = Nothing
    Return
End Function

Private Function SetDrawing(ApptId As Long) As Boolean
Dim i As Integer
Dim Temp As String
Dim ADate As String
Dim MyFile As String
Dim TheFile As String
Dim RetCln As PatientClinical
Dim RetAppt As SchedulerAppointment
On Error GoTo UI_ErrorHandler
SetDrawing = False
FxImage1.Tag = ""
Set FxImage1.picture = Nothing
lblDrawDate.Visible = False
lblDrawDate.Caption = ""
If (lstDxLV.ListItems.Count > 0) And (ApptId > 0) Then
    If (Val(lstDxLV.ListItems.Item(1).SubItems(5)) > 0) Then
        Set RetCln = New PatientClinical
        RetCln.ClinicalId = Val(lstDxLV.ListItems.Item(1).SubItems(5))
        If (RetCln.RetrievePatientClinical) Then
            Set RetAppt = New SchedulerAppointment
            RetAppt.AppointmentId = RetCln.AppointmentId
            If (RetAppt.RetrieveSchedulerAppointment) Then
                ADate = RetAppt.AppointmentDate
                ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Mid(ADate, 1, 4)
            End If
            Set RetAppt = Nothing
        End If
        Set RetCln = Nothing
    Else
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentId = ApptId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            ADate = RetAppt.AppointmentDate
            ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Mid(ADate, 1, 4)
        End If
        Set RetAppt = Nothing
    End If
'    For i = 1 To lstDxLV.ListItems.Count
    For i = 1 To 1
        If (lstDxLV.ListItems.Item(1).SubItems(4) <> "") Then
            Temp = lstDxLV.ListItems.Item(1).SubItems(4)
'            ADate = lstDxLV.ListItems.Item(1).SubItems(1)
            If (Temp <> "") Then
                If (Left(Temp, 2) = "R-") Then
                    MyFile = MyImageDir + Mid(Temp, 3, Len(Temp) - 2)
                Else
                    MyFile = MyImageDir + "MyDraw-A" + Trim(str(ApptId)) + "*" + Temp + ".jpg"
                End If
                TheFile = Dir(MyFile)
                If (Trim(TheFile) <> "") Then
                    MyFile = MyImageDir + TheFile
                Else
                    MyFile = MyImageDir + "MyDraw-A" + Trim(str(ApptId)) + "*" + Temp + ".bmp"
                    TheFile = Dir(MyFile)
                    If (Trim(TheFile) <> "") Then
                        MyFile = MyImageDir + TheFile
                    Else
                        MyFile = ""
                    End If
                End If
                If (Trim(MyFile) <> "") Then
                    If (InStrPS(MyFile, ".bmp") = 0) Then
                        FxImage1.FileName = FM.GetPathForDirectIO(MyFile)
                    Else
                        FxImage1.picture = FM.LoadPicture(MyFile)
                    End If
                    FxImage1.Tag = lstDxLV.ListItems.Item(1).SubItems(5)
                    lblDrawDate.Caption = ADate
                    lblDrawDate.Visible = True
                    Exit For
                End If
            End If
        End If
    Next i
End If
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Function TriggerViewDocuments(IRef As String, ApptId As Long, ADate As String, TestId As String) As Boolean
Dim IDate As String
TriggerViewDocuments = False
If (Trim(IRef) <> "") Then
    IDate = ADate
    If (Trim(ADate) <> "") Then
        IDate = Mid(ADate, 1, 2) + "/" + Mid(ADate, 4, 2) + "/" + "20" + Mid(ADate, 7, 2)
    End If
    frmScan.Whom = "D"
    frmScan.PatientId = PatientId
    frmScan.AppointmentId = ApptId
    frmScan.SystemReference = ""
    frmScan.ReplacementFile = ""
    If (frmScan.LoadScan) Then
        If (frmScan.LoadSelectedFile(IRef, IDate, TestId)) Then
            frmScan.Show 1
            TriggerViewDocuments = True
        End If
    End If
End If
End Function

Private Function AreYouSure() As Boolean
AreYouSure = False
frmEventMsgs.Header = "Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    AreYouSure = True
End If
End Function

Private Sub lstSurg_Click()
Dim lvi As ListItem
If (lstSurg.ListItems.Count > 0) Then
    Set lvi = lstSurg.FindItem(lstSurg.SelectedItem)
    frmEventMsgs.Header = "Unset Highlight ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        If (AreYouSure) Then
            Call UnsetHighlightNew(Val(lvi.SubItems(4)), "]")
            lstSurg.ListItems.Remove lvi.Index
        End If
    End If
End If
End Sub

Private Function IsDrugOkay(TheName As String) As String
Dim RetDrug As DiagnosisMasterDrugs
IsDrugOkay = ""
If (Trim(TheName) <> "") Then
    If (Mid(TheName, Len(TheName), 1) = "*") Then
        TheName = Left(TheName, Len(TheName) - 1)
    End If
    Set RetDrug = New DiagnosisMasterDrugs
    RetDrug.DrugName = Trim(TheName)
    If (RetDrug.FindDrugbyName > 0) Then
        If (RetDrug.SelectDrug(1)) Then
            IsDrugOkay = Trim(RetDrug.DrugSpecialtyType)
        End If
    End If
    Set RetDrug = Nothing
End If
End Function

Private Function ClearSelectLists() As Boolean
Dim i As Integer
For i = 1 To lstHPILV.ListItems.Count
    lstHPILV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstDxLV.ListItems.Count
    lstDxLV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstExamLV.ListItems.Count
    lstExamLV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstOcuMeds.ListItems.Count
    lstOcuMeds.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstOrdersLV.ListItems.Count
    lstOrdersLV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstProcsLV.ListItems.Count
    lstProcsLV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstSurg.ListItems.Count
    lstSurg.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstOTestsLV.ListItems.Count
    lstOTestsLV.ListItems.Item(i).Selected = False
Next i
End Function

Private Sub txtCC_Click()
If (Len(Trim(txtCC.Caption)) > 3) Then
    Call LoadMagnifyRV(BackDoorOn)
    If (lstHist.ListCount > 0) Then
        lstHist.Visible = True
    End If
End If
End Sub

Private Sub GetRxData(Temp1 As String, Temp2 As String, RxName As String, RxEye As String, RxDate As String, RxFreq As String, RxStat As String)
Dim MyLoc As Integer
Dim j As Integer
Dim k As Integer
Dim Rx As String
Dim Ref As String
Dim LclDate As String
If (Temp1 <> "") Then
    LclDate = Mid(RxDate, 7, 4) + Mid(RxDate, 1, 2) + Mid(RxDate, 4, 2)
    RxEye = "--"
    RxName = ""
    Ref = Trim(Temp1)
    j = InStrPS(Ref, "-1/")
    If (j < 1) Then
        j = InStrPS(Ref, "-8/")
    Else
        Call ReplaceCharacters(Ref, "-8/", " ")
    End If
    k = InStrPS(Ref, "-2/")
    If (k > 0) And (k < j) Then
        j = 1
    End If
    If (j > 0) And (k > j) Then
'=======================================================================
        
'        Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
'        Call ReplaceCharacters(Rx, "-1/", "~")
'        Call StripCharacters(Rx, "~")
'        Rx = Trim(Rx)
'        RxName = Rx
'        j = InStrPS(Ref, "-3/")
'        If (j > 0) Then
'            If (InStrPS(Ref, "(OU)") <> 0) Then
'                RxEye = "OU"
'            ElseIf (InStrPS(Ref, "(OS)") <> 0) Then
'                RxEye = "OS"
'            ElseIf (InStrPS(Ref, "(OD)") <> 0) Then
'                RxEye = "OD"
'            End If
'            k = InStrPS(Ref, "-4/")
'            If (k > 0) Then
'                RxFreq = Mid(Ref, j + 3, (k - 1) - (j + 2))
'                j = InStrPS(Ref, "-5/")
'                If (j > 0) Then
'                    Rx = Mid(Ref, k + 3, (j - 1) - (k + 2))
'                    k = InStrPS(Ref, "-6/")
'                    If (k > 0) Then
'                        Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
'                    End If
'                End If
'            End If
'        End If
        Dim fCDrugRx As New CDrugRx
        fCDrugRx.LoadRxFromDB Temp1
        RxName = fCDrugRx.Name
        RxEye = DisplayEyeRef(fCDrugRx.EyeRef)
        If RxEye = "" Then RxEye = "--"
        RxFreq = fCDrugRx.Frequency
'=======================================================================
        
        MyLoc = 2
        If (RxDate <> "") Or (Len(RxDate) < 10) Then
            MyLoc = InStrPS(Temp2, RxDate)
            If (MyLoc = 0) Then
                MyLoc = 2
            End If
        End If
        RxStat = "Active  "
        If (Trim(Temp2) <> "") Then
            If (Mid(Temp2, MyLoc - 1, 1) = "D") Then
                RxStat = "Disc   "
            ElseIf (Mid(Temp2, MyLoc - 1, 1) = "R") Then
                RxStat = "Refill "
            ElseIf (Mid(Temp2, MyLoc - 1, 1) = "C") Then
                RxStat = "Rvisd  "
            ElseIf (Mid(Temp2, MyLoc - 1, 1) = "P") Then
                RxStat = "Pdisc  "
            ElseIf (Mid(Temp2, MyLoc - 1, 1) = "K") Then
                RxStat = "Cont   "
            ElseIf (Mid(Temp2, MyLoc - 1, 1) = "X") Then
                RxStat = "Deleted "
            Else
                If (frmSystems1.ActiveActivityDate = LclDate) Then
                    If ((MyLoc + 11) >= Len(Trim(Temp2))) Then
                        RxStat = "Start   "
                    End If
                End If
            End If
            RxDate = Mid(Temp2, MyLoc, 10)
        Else
            RxStat = "Start   "
        End If
    End If
End If
End Sub
Public Function FrmClose()
 'Call cmdHome_Click
'    Dim frm As New frmInferenceAlerts
'     frm.Close
    Unload Me
End Function
Public Function FormatText(ByVal TheText As String)
FormatText = ""
Dim i As Integer
Dim q As Integer
Dim k As Integer
Dim str As String
k = InStrPS(TheText, "Pin")

If k > 0 Then
    i = InStrPS(k + 3, TheText, "OS:")
    If i > 0 Then
        q = InStrPS(i + 3, TheText, vbCrLf)
        If q > 0 Then
            FormatText = Mid(TheText, 1, q)
        End If
    End If
Else
    i = InStrPS(TheText, "OS:")
    If i > 0 Then
        q = InStrPS(i + 3, TheText, vbCrLf)
        If q > 0 Then
            FormatText = Mid(TheText, 1, q)
        End If
    End If
End If
End Function

