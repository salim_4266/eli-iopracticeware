VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{4BD5A3A1-7FFE-11D4-A13A-004005FA6275}#1.0#0"; "ImagXpr6.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmFinalNew 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9420
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12300
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "FinalNew.frx":0000
   ScaleHeight     =   9420
   ScaleWidth      =   12300
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtZoom 
      BackColor       =   &H00EFEBE2&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1815
      Left            =   2160
      MultiLine       =   -1  'True
      TabIndex        =   19
      Top             =   3360
      Visible         =   0   'False
      Width           =   5895
   End
   Begin MSComctlLib.ListView lstTestsLV 
      Height          =   1575
      Left            =   5880
      TabIndex        =   20
      Top             =   120
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   2778
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Todays Tests and Procedures"
         Object.Width           =   9349
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Hi"
         Object.Width           =   564
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstActionsLV 
      Height          =   1335
      Left            =   120
      TabIndex        =   8
      Top             =   2400
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   2355
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Plan"
         Object.Width           =   8996
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Hi"
         Object.Width           =   582
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "IRef"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstAllergy1LV 
      Height          =   855
      Left            =   5880
      TabIndex        =   16
      Top             =   2640
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   1508
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Allergies"
         Object.Width           =   4410
      EndProperty
   End
   Begin MSComctlLib.ListView lstImpLVOD 
      Height          =   1215
      Left            =   120
      TabIndex        =   7
      Top             =   3720
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   2143
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "OD Impressions"
         Object.Width           =   4410
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10160
      TabIndex        =   0
      Top             =   8030
      Width           =   1675
      _Version        =   131072
      _ExtentX        =   2955
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FinalNew.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPlan 
      Height          =   975
      Left            =   3600
      TabIndex        =   1
      Top             =   8030
      Width           =   1675
      _Version        =   131072
      _ExtentX        =   2955
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FinalNew.frx":3910
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   975
      Left            =   120
      TabIndex        =   2
      Top             =   8030
      Width           =   1675
      _Version        =   131072
      _ExtentX        =   2955
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FinalNew.frx":3B55
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpr 
      Height          =   975
      Left            =   1860
      TabIndex        =   3
      Top             =   8030
      Width           =   1675
      _Version        =   131072
      _ExtentX        =   2955
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FinalNew.frx":3D9A
   End
   Begin MSComctlLib.ListView lstHPILV 
      Height          =   975
      Left            =   120
      TabIndex        =   6
      Top             =   1440
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   1720
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "HPI"
         Object.Width           =   9349
      EndProperty
   End
   Begin MSComctlLib.ListView lstILPNLV 
      Height          =   1335
      Left            =   120
      TabIndex        =   9
      Top             =   4920
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   2355
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "ILPN & ILDN"
         Object.Width           =   9349
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstBilledLV 
      Height          =   1695
      Left            =   120
      TabIndex        =   10
      Top             =   6240
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   2990
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Billed"
         Object.Width           =   8819
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Hi"
         Object.Width           =   582
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Ref"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Code"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "IRef"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstFindingOD 
      Height          =   1335
      Left            =   5880
      TabIndex        =   11
      Top             =   3480
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   2355
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "OD Finding"
         Object.Width           =   4233
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "BI"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Hi"
         Object.Width           =   582
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Diag"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstFindingOS 
      Height          =   1335
      Left            =   8880
      TabIndex        =   12
      Top             =   3480
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   2355
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "OS Finding"
         Object.Width           =   4145
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "BI"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Hi"
         Object.Width           =   582
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Diag"
         Object.Width           =   0
      EndProperty
   End
   Begin IMAGXPR6LibCtl.ImagXpress FxImage1 
      Height          =   3135
      Left            =   5880
      TabIndex        =   13
      Top             =   4800
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   5530
      ErrStr          =   "MDYC0090GEP-0B3060SXEP"
      ErrCode         =   1234897882
      ErrInfo         =   1374762512
      Persistence     =   -1  'True
      _cx             =   1
      _cy             =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   16777215
      AutoSize        =   4
      BorderType      =   1
      ScrollBarLargeChangeH=   10
      ScrollBarSmallChangeH=   1
      OLEDropMode     =   0
      ScrollBarLargeChangeV=   10
      ScrollBarSmallChangeV=   1
      DisplayProgressive=   -1  'True
      SaveTIFByteOrder=   0
      LoadRotated     =   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      Begin VB.Label lblDrawDate 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         Height          =   255
         Left            =   4440
         TabIndex        =   14
         Top             =   240
         Visible         =   0   'False
         Width           =   1335
      End
   End
   Begin MSComctlLib.ListView lstOcuMeds1 
      Height          =   975
      Left            =   5880
      TabIndex        =   15
      Top             =   1680
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   1720
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Ocular Medications"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Hi"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstRvLV 
      Height          =   615
      Left            =   120
      TabIndex        =   17
      Top             =   840
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   1085
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "RV"
         Object.Width           =   9349
      EndProperty
   End
   Begin MSComctlLib.ListView lstLtrs 
      Height          =   375
      Left            =   120
      TabIndex        =   18
      Top             =   480
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   661
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Letters"
         Object.Width           =   4233
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "P"
         Object.Width           =   582
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Rcpt"
         Object.Width           =   4427
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.ListView lstImpLVOS 
      Height          =   1215
      Left            =   3000
      TabIndex        =   22
      Top             =   3720
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   2143
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "OS Impressions"
         Object.Width           =   4410
      EndProperty
   End
   Begin MSComctlLib.ListView lstAllergy2LV 
      Height          =   855
      Left            =   8880
      TabIndex        =   23
      Top             =   2640
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   1508
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   4410
      EndProperty
   End
   Begin MSComctlLib.ListView lstOcuMeds2 
      Height          =   975
      Left            =   8880
      TabIndex        =   24
      Top             =   1680
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   1720
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ClnId"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Hi"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "IClnId"
         Object.Width           =   0
      EndProperty
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdODraw 
      Height          =   975
      Left            =   7040
      TabIndex        =   25
      Top             =   8030
      Width           =   955
      _Version        =   131072
      _ExtentX        =   1685
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FinalNew.frx":3FDF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdERx 
      Height          =   975
      Left            =   8040
      TabIndex        =   26
      Top             =   8040
      Visible         =   0   'False
      Width           =   2040
      _Version        =   131072
      _ExtentX        =   3598
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FinalNew.frx":4218
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCDA 
      Height          =   975
      Left            =   5355
      TabIndex        =   27
      Top             =   8030
      Width           =   1590
      _Version        =   131072
      _ExtentX        =   2805
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "FinalNew.frx":445D
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   21
      Top             =   120
      Width           =   885
   End
   Begin VB.Label lblRefDr 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3540
      TabIndex        =   5
      Top             =   120
      Width           =   2355
   End
   Begin VB.Label lblName 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      TabIndex        =   4
      Top             =   120
      Width           =   2445
   End
End
Attribute VB_Name = "frmFinalNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Result As Integer
Public PatientId As Long
Public AppointmentId As Long
Public ActivityId As Long
Private ExitOn As Boolean
Private ViewOn As Boolean
Private MaxDraw As Integer
Private CurDraw As Integer
Private DrawFile(20) As String

Private Const CdaSelectionBackColor As Long = vbWhite
Private Const CdaSelectionForeColor As Long = vbBlack
Private Const CdaBaseBackColor As Long = &H8CBF15
Private Const CdaBaseForeColor As Long = &HFFFFFF

Private Sub cmdCDA_Click()
    If (cmdCDA.BackColor = CdaSelectionBackColor) Then
        cmdCDA.BackColor = CdaBaseBackColor
        cmdCDA.ForeColor = CdaBaseForeColor
    Else
        cmdCDA.BackColor = CdaSelectionBackColor
        cmdCDA.ForeColor = CdaSelectionForeColor
    End If
End Sub

Private Sub cmdERx_Click()
If UserLogin.HasPermission(EPermissions.epeRx) Then
        Result = 1

Else
    frmEventMsgs.InfoMessage "Not permissioned for eRx"
    frmEventMsgs.Show 1
        Result = 0
End If

If Not (ExitOn) Then
    If CheckConfigCollection("MUON") = "T" Then
        If Not CurrentMeds Then Exit Sub
    End If

    Call SetHighlights
    ExitOn = True
End If
Unload frmFinalNew
' Insert code to call frmDisposition_Done_Click code here

End Sub

Public Sub cmdDone_Click()

If Not (ExitOn) Then
    If CheckConfigCollection("MUON") = "T" Then
        If Not CurrentMeds Then Exit Sub
    End If

    Call SetHighlights
    ExitOn = True
End If
    Result = 0
Unload frmFinalNew
' Insert code to call frmDisposition_Done_Click code here
End Sub

Private Function CurrentMeds() As Boolean
Dim FileName As String
Dim FileNum As Long
Dim Rec As String
FileName = GetTestFileName("EvalActions")
FileNum = FreeFile
If FM.IsFileThere(FileName) Then
    FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.Read, CLng(FileNum)
    Do Until (EOF(FileNum))
        Line Input #FileNum, Rec
        If UCase(Rec) Like "RX*" Then
            CurrentMeds = True
            Exit Do
        End If
    Loop
    FM.CloseFile CLng(FileNum)
End If
'If Medication doesn't exists in the EvalActions Scratch File.Find the medications in the DB.
If Not CurrentMeds Then
    Dim RetCln As New PatientClinical
    If RetCln.getMeds(globalExam.iPatientId) > 0 Then CurrentMeds = True
End If
'If in both DB and Scratch file medication doesn't exists then display the Prompt.
If Not CurrentMeds Then
    frmEventMsgs.Header = "Medications not documented"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Patient taking NO MEDS"
    frmEventMsgs.CancelText = "Patient taking meds"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        CurrentMeds = True
    End If
End If
End Function


Private Sub cmdImpr_Click()
Call SetHighlights
frmSetDiagnosis.PatientId = PatientId
frmSetDiagnosis.AppointmentId = AppointmentId
If frmSetDiagnosis.LoadImpressions(True, True, False, False, False) Then
    frmSetDiagnosis.Show 1
    frmSetDiagnosis.RetainImpressions PatientId
    LoadFinal ViewOn
End If
End Sub

Private Sub cmdODraw_Click()
Call NextDrawing(AppointmentId)
End Sub

Private Sub cmdPlan_Click()
Call SetHighlights
If (frmEvaluation.LoadActions(True, False)) Then
    frmEvaluation.Show 1
    Call frmEvaluation.PostToFile
    If (frmEvaluation.NavigationAction = 99) Or (frmEvaluation.NavigationAction = 2) Then
            Result = -2
        Unload frmFinalNew
    ElseIf (frmEvaluation.NavigationAction = 90) Then
            Result = -1
        Unload frmFinalNew
    Else
            cmdERx.Visible = frmEvaluation.IsThereERx
            cmdERx.Visible = True
            Call LoadFinal(ViewOn)
    End If
Else
        Result = -2
    Unload frmFinalNew
End If
End Sub

Private Sub cmdQuit_Click()
Call SetHighlights
    Result = -2
Unload frmFinalNew
End Sub

Private Sub SetHighlights()
Dim i As Integer
Dim k As Integer
Dim Id As String
Dim RetNotes As PatientNotes
For i = 1 To lstBilledLV.ListItems.Count
    Id = lstBilledLV.ListItems.Item(i).Text
    If (lstBilledLV.ListItems.Item(i).SubItems(2) = "T") Then
        Id = lstBilledLV.ListItems.Item(i).Text
        If (lstBilledLV.ListItems.Item(i).SubItems(2) = "D") Then
            Call frmSetProcedures.SetDiagnosisHighlight(Id, True)
        ElseIf (lstBilledLV.ListItems.Item(i).SubItems(2) = "P") Then
            Call frmSetProcedures.SetProcedureHighlight(Id, True)
        ElseIf (lstBilledLV.ListItems.Item(i).SubItems(2) = "E") Then
            Call frmSetEMProcedure.SetEMProcedureHighlight(True)
        End If
    Else
        If (lstBilledLV.ListItems.Item(i).SubItems(2) = "D") Then
            Call frmSetProcedures.SetDiagnosisHighlight(Id, False)
        ElseIf (lstBilledLV.ListItems.Item(i).SubItems(2) = "P") Then
            Call frmSetProcedures.SetProcedureHighlight(Id, False)
        ElseIf (lstBilledLV.ListItems.Item(i).SubItems(2) = "E") Then
            Call frmSetEMProcedure.SetEMProcedureHighlight(False)
        End If
    End If
Next i
'Call frmSetProcedures.RetainBilledDiagnosis(PatientId)
'Call frmSetProcedures.RetainBilledProcedures(PatientId)
Call frmSetEMProcedure.RetainEMProcedure(PatientId)
For i = 1 To lstFindingOD.ListItems.Count
    Id = Trim(lstFindingOD.ListItems.Item(i).Text)
    If (lstFindingOD.ListItems.Item(i).SubItems(2) = "T") Then
        Call frmSystems1.SetFindingHighlight("OD", Id, True)
    Else
        Call frmSystems1.SetFindingHighlight("OD", Id, False)
    End If
Next i
For i = 1 To lstFindingOS.ListItems.Count
    Id = Trim(lstFindingOS.ListItems.Item(i).Text)
    If (lstFindingOS.ListItems.Item(i).SubItems(2) = "T") Then
        Call frmSystems1.SetFindingHighlight("OS", Id, True)
    Else
        Call frmSystems1.SetFindingHighlight("OS", Id, False)
    End If
Next i
Call frmSystems1.RetainDiagList(PatientId)
For i = 1 To lstActionsLV.ListItems.Count
    Id = lstActionsLV.ListItems.Item(i).SubItems(3)
    If (Val(Id) > 0) Then
        If (lstActionsLV.ListItems.Item(i).SubItems(4) = "N") Then
            Set RetNotes = New PatientNotes
            RetNotes.NotesId = Val(Id)
            If (RetNotes.RetrieveNotes) Then
                If (lstActionsLV.ListItems.Item(i).SubItems(2) = "T") Then
                    RetNotes.NotesHighlight = "V"
                Else
                    RetNotes.NotesHighlight = ""
                End If
                Call RetNotes.ApplyNotes
            End If
            Set RetNotes = Nothing
        Else
            If (lstActionsLV.ListItems.Item(i).SubItems(2) = "T") Then
                If (lstActionsLV.ListItems.Item(i).SubItems(4) <> "A") Then
                    Call frmEvaluation.SetActionHighlight(Val(Id), True)
                End If
            Else
                If (lstActionsLV.ListItems.Item(i).SubItems(4) <> "A") Then
                    Call frmEvaluation.SetActionHighlight(Val(Id), False)
                End If
            End If
        End If
    End If
Next i
Call frmEvaluation.PostToFile
For i = 1 To lstTestsLV.ListItems.Count
    Id = lstTestsLV.ListItems.Item(i).SubItems(3)
    If (lstTestsLV.ListItems.Item(i).SubItems(2) = "T") Then
        Call frmSystems1.SetTestHighlight(Val(Id), True)
    Else
        Call frmSystems1.SetTestHighlight(Val(Id), False)
    End If
Next i

'If checked true then auto transmit cda file.
If (cmdCDA.BackColor = CdaSelectionBackColor) Then Call AutoTransmitCdaFile
End Sub

Public Function LoadFinal(AView As Boolean)
Dim WrapClean As Boolean
Dim FirstOn As Boolean, AHigh As Boolean
Dim PrescribedOn As Boolean, RetHigh As Boolean
Dim ROrd As Integer
Dim RRul As Boolean, RCnf As Boolean
Dim RetFollow As Boolean, RHst As Boolean
Dim MyCln As Long, RefDr As Long
Dim zz As Integer, Apo As Integer
Dim i As Integer, j As Integer
Dim w As Integer, p As Integer
Dim z As Integer, z1 As Integer
Dim tt As Integer, ListType As Integer
Dim ATemp As String, RTemp As String
Dim TestOrder As String, AType As String
Dim TestParty As String, TestTime As String
Dim TestText As String, TestQuestion As String
Dim AId As String, AString As String
Dim AEye As String, ASevA As String, ASevB As String
Dim UTemp As String, PTemp As String, QTemp As String
Dim Temp As String, Temp1 As String, KTemp As String
Dim TheText As String, RxDate As String
Dim MyArray(10) As String
Dim RetQ As String
Dim RetId As String, RetString As String, RetA As String
Dim RetEye As String, RetSevA As String, RetSevB As String
Dim RetNotes As PatientNotes
Dim RetClinical As PatientClinical
Dim RetrieveDiag As DiagnosisMasterPrimary
Dim RetrieveProc As DiagnosisMasterProcedures
Dim oDate As New CIODateTime
On Error GoTo UI_ErrorHandler
LoadFinal = False

cmdCDA.BackColor = CdaBaseBackColor
cmdCDA.ForeColor = CdaBaseForeColor

ViewOn = AView
oDate.MyDateTime = Now
lblName.Caption = frmSystems1.txtSummary.Text
i = InStrPS(lblName.Caption, ",")
If (i > 0) Then
    lblName.Caption = Left(lblName.Caption, i - 1)
End If
lblName.Caption = " " + Trim(lblName.Caption)
ATemp = Trim(frmSystems1.txtSum2.Text)
If (Len(Trim(ATemp)) > 1) Then
    w = InStrPS(ATemp, ",")
    If (w > 1) Then
        RTemp = Trim(Mid(ATemp, w + 1, Len(ATemp) - w))
        lblRefDr.Caption = " " + RTemp
    End If
End If

If (CanGenerateCdaFiles = True) Then
    cmdCDA.BackColor = CdaSelectionBackColor
    cmdCDA.ForeColor = CdaSelectionForeColor
End If

' Findings
lstFindingOD.ListItems.Clear
For i = 0 To frmSystems1.lstOD.ListCount - 1
    If (Mid(frmSystems1.lstOD.List(i), 73, 1) <> "N") And (Mid(frmSystems1.lstOD.List(i), 73, 1) <> "X") Then
        lstFindingOD.ListItems.Add Trim(str(lstFindingOD.ListItems.Count + 1))
        lstFindingOD.ListItems.Item(lstFindingOD.ListItems.Count).SubItems(1) = Trim(Left(frmSystems1.lstOD.List(i), 64))
        If (Mid(frmSystems1.lstOD.List(i), 69, 1) = "T") Then
            lstFindingOD.ListItems.Item(lstFindingOD.ListItems.Count).SubItems(3) = "T"
        ElseIf (Mid(frmSystems1.lstOD.List(i), 60, 1) = "T") Then
            lstFindingOD.ListItems.Item(lstFindingOD.ListItems.Count).SubItems(3) = "T"
        Else
            lstFindingOD.ListItems.Item(lstFindingOD.ListItems.Count).SubItems(3) = " "
        End If
        z1 = InStrPS(72, frmSystems1.lstOD.List(i), "@")
        If (z1 > 0) Then
            lstFindingOD.ListItems.Item(lstFindingOD.ListItems.Count).SubItems(4) = Mid(frmSystems1.lstOD.List(i), 72, (z1 - 1) - 71)
        Else
            z1 = Len(frmSystems1.lstOD.List(i))
            If (z1 > 70) Then
                lstFindingOD.ListItems.Item(lstFindingOD.ListItems.Count).SubItems(4) = Mid(frmSystems1.lstOD.List(i), 72, z1 - 71)
            End If
        End If
    End If
Next i
lstFindingOS.ListItems.Clear
For i = 0 To frmSystems1.lstOS.ListCount - 1
    If (Mid(frmSystems1.lstOS.List(i), 73, 1) <> "N") And (Mid(frmSystems1.lstOS.List(i), 73, 1) <> "X") Then
        lstFindingOS.ListItems.Add Trim(str(lstFindingOS.ListItems.Count + 1))
        lstFindingOS.ListItems.Item(lstFindingOS.ListItems.Count).SubItems(1) = Trim(Left(frmSystems1.lstOS.List(i), 64))
        If (Mid(frmSystems1.lstOS.List(i), 69, 1) = "T") Then
            lstFindingOS.ListItems.Item(lstFindingOS.ListItems.Count).SubItems(3) = "T"
        ElseIf (Mid(frmSystems1.lstOS.List(i), 60, 1) = "T") Then
            lstFindingOS.ListItems.Item(lstFindingOS.ListItems.Count).SubItems(3) = "T"
        Else
            lstFindingOS.ListItems.Item(lstFindingOS.ListItems.Count).SubItems(3) = " "
        End If
        z1 = InStrPS(72, frmSystems1.lstOS.List(i), "@")
        If (z1 > 0) Then
            lstFindingOS.ListItems.Item(lstFindingOS.ListItems.Count).SubItems(4) = Mid(frmSystems1.lstOS.List(i), 72, (z1 - 1) - 71)
        Else
            z1 = Len(frmSystems1.lstOS.List(i))
            If (z1 > 70) Then
                lstFindingOS.ListItems.Item(lstFindingOS.ListItems.Count).SubItems(4) = Mid(frmSystems1.lstOS.List(i), 72, z1 - 71)
            End If
        End If
    End If
Next i
' Allergies
lstAllergy1LV.ListItems.Clear
lstAllergy2LV.ListItems.Clear
For i = 1 To frmSystems1.lstALG.ListCount - 1 Step 2
    lstAllergy1LV.ListItems.Add Trim(str(lstAllergy1LV.ListItems.Count + 1))
    ATemp = Trim(Left(frmSystems1.lstALG.List(i), 60))
    If (Len(frmSystems1.lstALG.List(i)) > 70) And (Len(frmSystems1.lstALG.List(i + 1)) > 70) Then
        For z1 = i To frmSystems1.lstALG.ListCount - 1
            If (Mid(frmSystems1.lstCC.List(z1), 72, 3) = Mid(frmSystems1.lstALG.List(z1 + 1), 72, 3)) Then
                ATemp = Trim(ATemp) + " " + Trim(Left(frmSystems1.lstALG.List(z1 + 1), 60))
            Else
                Exit For
            End If
        Next z1
        i = z1
    ElseIf (Len(frmSystems1.lstALG.List(i)) < 70) And (Len(frmSystems1.lstALG.List(i + 1)) < 70) Then
        For z1 = i To frmSystems1.lstALG.ListCount - 1
            If (Mid(frmSystems1.lstALG.List(z1), 61, 10) = Mid(frmSystems1.lstALG.List(z1 + 1), 61, 10)) Then
                ATemp = Trim(ATemp) + " " + Trim(Left(frmSystems1.lstALG.List(z1 + 1), 60))
            Else
                Exit For
            End If
        Next z1
        i = z1
    End If
    If (Left(ATemp, 3) = "(T)") Or (Left(ATemp, 3) = "(H)") Then
        ATemp = Trim(Mid(ATemp, 4, Len(ATemp) - 3))
    End If
    If (Left(ATemp, 2) = "- ") Then
        ATemp = Trim(Mid(ATemp, 3, Len(ATemp) - 2))
    End If
    lstAllergy1LV.ListItems.Item(lstAllergy1LV.ListItems.Count).SubItems(1) = ATemp
Next i
For i = 2 To frmSystems1.lstALG.ListCount - 1 Step 2
    lstAllergy2LV.ListItems.Add Trim(str(lstAllergy2LV.ListItems.Count + 1))
    ATemp = Trim(Left(frmSystems1.lstALG.List(i), 60))
    If (Len(frmSystems1.lstALG.List(i)) > 70) And (Len(frmSystems1.lstALG.List(i + 1)) > 70) Then
        For z1 = i To frmSystems1.lstALG.ListCount - 1
            If (Mid(frmSystems1.lstCC.List(z1), 72, 3) = Mid(frmSystems1.lstALG.List(z1 + 1), 72, 3)) Then
                ATemp = Trim(ATemp) + " " + Trim(Left(frmSystems1.lstALG.List(z1 + 1), 60))
            Else
                Exit For
            End If
        Next z1
        i = z1
    ElseIf (Len(frmSystems1.lstALG.List(i)) < 70) And (Len(frmSystems1.lstALG.List(i + 1)) < 70) Then
        For z1 = i To frmSystems1.lstALG.ListCount - 1
            If (Mid(frmSystems1.lstALG.List(z1), 61, 10) = Mid(frmSystems1.lstALG.List(z1 + 1), 61, 10)) Then
                ATemp = Trim(ATemp) + " " + Trim(Left(frmSystems1.lstALG.List(z1 + 1), 60))
            Else
                Exit For
            End If
        Next z1
        i = z1
    End If
    If (Left(ATemp, 3) = "(T)") Or (Left(ATemp, 3) = "(H)") Then
        ATemp = Trim(Mid(ATemp, 4, Len(ATemp) - 3))
    End If
    If (Left(ATemp, 2) = "- ") Then
        ATemp = Trim(Mid(ATemp, 3, Len(ATemp) - 2))
    End If
    lstAllergy2LV.ListItems.Item(lstAllergy2LV.ListItems.Count).SubItems(1) = ATemp
Next i
' Current HPI
Call frmSystems1.LoadPIList(frmSystems1.lstZoom, 1, 0)
lstHPILV.ListItems.Clear
For i = 1 To frmSystems1.lstZoom.ListCount - 1
    If (Len(frmSystems1.lstZoom.List(i)) > 70) Then
        If (frmSystems1.IsComplaintHighlight(Val(Trim(Mid(frmSystems1.lstZoom.List(i), 77, 3))))) Then
            lstHPILV.ListItems.Add Trim(str(lstHPILV.ListItems.Count + 1))
            ATemp = Trim(Left(frmSystems1.lstZoom.List(i), 60))
            If (Len(frmSystems1.lstZoom.List(i)) > 70) And (Len(frmSystems1.lstZoom.List(i + 1)) > 70) Then
                For z1 = i To frmSystems1.lstZoom.ListCount - 1
                    If (Mid(frmSystems1.lstZoom.List(z1), 77, 3) = Mid(frmSystems1.lstZoom.List(z1 + 1), 77, 3)) Then
                        ATemp = Trim(ATemp) + " " + Trim(Left(frmSystems1.lstZoom.List(z1 + 1), 60))
                    Else
                        Exit For
                    End If
                Next z1
                i = z1
            ElseIf (Len(frmSystems1.lstZoom.List(i)) < 70) And (Len(frmSystems1.lstZoom.List(i + 1)) < 70) Then
                For z1 = i To frmSystems1.lstZoom.ListCount - 1
                    If (Mid(frmSystems1.lstZoom.List(z1), 61, 10) = Mid(frmSystems1.lstZoom.List(z1 + 1), 61, 10)) Then
                        ATemp = Trim(ATemp) + " " + Trim(Left(frmSystems1.lstZoom.List(z1 + 1), 60))
                    Else
                        Exit For
                    End If
                Next z1
                i = z1
            End If
            If (Left(ATemp, 3) = "(T)") Or (Left(ATemp, 3) = "(H)") Then
                ATemp = Trim(Mid(ATemp, 4, Len(ATemp) - 3))
            End If
            If (Left(ATemp, 2) = "- ") Then
                ATemp = Trim(Mid(ATemp, 3, Len(ATemp) - 2))
            End If
            lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(1) = ATemp
        End If
    Else
        ATemp = Trim(Left(frmSystems1.lstZoom.List(i), 60))
        For z1 = i To frmSystems1.lstZoom.ListCount - 1
            If (Mid(frmSystems1.lstZoom.List(z1), 61, 10) = Mid(frmSystems1.lstZoom.List(z1 + 1), 61, 10)) Then
                ATemp = Trim(ATemp) + " " + Trim(Left(frmSystems1.lstZoom.List(z1 + 1), 60))
            Else
                Exit For
            End If
        Next z1
        i = z1
        If (Left(ATemp, 3) = "(T)") Or (Left(ATemp, 3) = "(H)") Then
            ATemp = Trim(Mid(ATemp, 4, Len(ATemp) - 3))
        End If
        If (Left(ATemp, 2) = "- ") Then
            ATemp = Trim(Mid(ATemp, 3, Len(ATemp) - 2))
        End If
        If (Trim(ATemp) <> "") Then
            For tt = 1 To Len(ATemp) Step 60
                QTemp = Mid(ATemp, tt, 60)
                WrapClean = False
                If (Mid(ATemp, tt + 60, 1) = " ") Then
                    WrapClean = True
                End If
                ListType = 4
                GoSub WrapIt
            Next tt
        End If
    End If
Next i
' Current RV
lstRvLV.ListItems.Clear
For i = 1 To 10
    If (frmSystems1.GetRVReference(i, ATemp)) Then
        lstRvLV.ListItems.Add Trim(str(lstRvLV.ListItems.Count + 1))
        ATemp = Trim(Left(ATemp, 60))
        If (Left(ATemp, 3) = "(T)") Or (Left(ATemp, 3) = "(H)") Then
            ATemp = Trim(Mid(ATemp, 4, Len(ATemp) - 3))
        End If
        If (Left(ATemp, 2) = "- ") Then
            ATemp = Trim(Mid(ATemp, 3, Len(ATemp) - 2))
        End If
        lstRvLV.ListItems.Item(lstRvLV.ListItems.Count).SubItems(1) = "RV:" & ATemp
    Else
        Exit For
    End If
Next i
' Billed CPT/EM
lstBilledLV.ListItems.Clear
ListType = 0
Temp = ""
Temp1 = ""
Temp = ""
Set RetrieveProc = New DiagnosisMasterProcedures
Dim FileNum As Integer
Dim TheFile As String
Dim sRec As String
'Dim i As Integer'
Dim n As Integer
'Dim j As Integer
Dim k As Integer
Dim ADiag As String
Dim AProc As String
Dim AMod As String
Dim ALink As String


If frmEvaluation.IsIcd10 Then
    TheFile = DoctorInterfaceDirectory + "ProcBill_Icd10" + "_" + Trim$(str$(globalExam.iAppointmentId)) + "_" + Trim$(str$(globalExam.iPatientId)) + ".txt"
Else
    TheFile = DoctorInterfaceDirectory + "ProcBill" + "_" + Trim$(str$(globalExam.iAppointmentId)) + "_" + Trim$(str$(globalExam.iPatientId)) + ".txt"
End If
    If (FM.IsFileThere(TheFile)) Then
        FileNum = FreeFile
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
        i = 1
        While Not (EOF(FileNum))
            Line Input #FileNum, sRec
                     n = InStrPS(sRec, "-")
                     If n > 0 Then
                       ADiag = Mid(sRec, 1, n - 1)
                       j = InStrPS(sRec, ":")
                       If j > 0 Then
                         AProc = Mid(sRec, n + 2, j - 1 - n - 1)
                         k = InStrPS(sRec, "/")
                         If k > 0 Then
                            AMod = Mid(sRec, j + 1, k - 1 - j)
                            ALink = Mid(sRec, k + 1, Len(sRec) - k)
                         End If
                            
                        End If
                     End If
    
        Temp = Space(100)
        RetrieveProc.ProcedureCPT = AProc
        RetrieveProc.ProcedureDiagnosis = ""
        RetrieveProc.ProcedureOrder = 0
        RetrieveProc.ProcedureRank = 0
        RetrieveProc.ProcedureLinkType = ""
        If (RetrieveProc.FindProcedurebyNumeric > 0) Then
            If (RetrieveProc.SelectProcedure(1)) Then
                If (Trim(RetrieveProc.ProcedureLtrTrans) <> "") Then
                    TheText = Trim(RetrieveProc.ProcedureLtrTrans)
                Else
                    TheText = Trim(RetrieveProc.ProcedureName)
                End If
                    Mid(Temp, 1, Len(Trim(AProc) + " " + Trim(ADiag) + " " + Trim("") + Trim(AMod) + " " + Trim("") + " " + Trim(TheText))) = Trim(AProc) + " " + Trim(ADiag) + " " + Trim("") + Trim(AMod) + " " + Trim("") + " " + Trim(TheText)
                ListType = ListType + 1
                lstBilledLV.ListItems.Add Trim(str(ListType))
                lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(1) = Trim(Left(Temp, 64))
                lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(2) = " "
'                If (RetHigh) Then
'                    lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(2) = "T"
'                End If
                lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(3) = "P"
                lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(4) = Trim(AProc)
                lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(5) = Trim(str(i))
            End If
        End If
    
   i = i + 1
  Wend
  FM.CloseFile CLng(FileNum), False
  
 End If
Set RetrieveProc = Nothing

Temp = ""
Set RetrieveProc = New DiagnosisMasterProcedures
If (frmSetEMProcedure.GetEMProcedure(RetId, RetString, RetEye, RetHigh)) Then
    Temp = Space(100)
    RetrieveProc.ProcedureCPT = RetId
    RetrieveProc.ProcedureDiagnosis = ""
    RetrieveProc.ProcedureOrder = 0
    RetrieveProc.ProcedureRank = 0
    If (RetrieveProc.FindProcedurebyEM > 0) Then
        If (RetrieveProc.SelectProcedure(1)) Then
            If (Trim(RetrieveProc.ProcedureLtrTrans) <> "") Then
                TheText = Trim(RetrieveProc.ProcedureLtrTrans)
            Else
                TheText = Trim(RetrieveProc.ProcedureName)
            End If
            Mid(Temp, 1, Len(Trim(RetId) + " " + Trim(RetString) + " " + Trim(RetEye) + " " + Trim(TheText))) = Trim(RetId) + " " + Trim(RetString) + " " + Trim(RetEye) + " " + Trim(TheText)
            ListType = ListType + 1
            lstBilledLV.ListItems.Add Trim(str(ListType))
            lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(1) = Trim(Left(Temp, 64))
            lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(2) = " "
            If (RetHigh) Then
                lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(2) = "T"
            End If
            lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(3) = "E"
            lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(4) = Trim(RetId)
            lstBilledLV.ListItems.Item(lstBilledLV.ListItems.Count).SubItems(5) = Trim(str(i))
        End If
    End If
End If
Set RetrieveProc = Nothing

' Impressions
ListType = 0
lstImpLVOD.ListItems.Clear
lstImpLVOS.ListItems.Clear
frmSetDiagnosis.CountTotalImpressions
For i = 0 To frmSetDiagnosis.TotalImpressionsOD - 1
    If frmSetDiagnosis.GetImpressionsOD(i, RetId, RetString, RetEye, RetSevA, RetSevB, RetA, RetQ, RCnf, RHst, RRul, ROrd) Then
        Temp = ""
        If (RetA = "T") Then
            If (Trim(RetSevA) <> "") Then
                Temp = "OD " + RetSevA
            End If
            If (Trim(RetSevB) <> "") Then
                Temp = Temp + " OS " + RetSevB
            End If
            If (Left(RetId, 1) = "-") Then
                RetId = ""
            End If
            ATemp = ""
            If (RCnf) Then
                ATemp = ATemp + "NF-"
            End If
            If (RHst) Then
                ATemp = ATemp + "HO-"
            End If
            If (RRul) Then
                ATemp = ATemp + "RO-"
            End If
            ATemp = ATemp + Trim(RetQ)
            If (RetEye = "OD") Then
                lstImpLVOD.ListItems.Add Trim(str(lstImpLVOD.ListItems.Count + 1))
                lstImpLVOD.ListItems.Item(lstImpLVOD.ListItems.Count).SubItems(1) = ATemp + Trim(RetString) + " " + Temp
            ElseIf (RetEye = "OS") Then
                lstImpLVOS.ListItems.Add Trim(str(lstImpLVOS.ListItems.Count + 1))
                lstImpLVOS.ListItems.Item(lstImpLVOS.ListItems.Count).SubItems(1) = ATemp + Trim(RetString) + " " + Temp
            End If
        End If
    End If
Next i

For i = 0 To frmSetDiagnosis.TotalImpressionsOS - 1
    If frmSetDiagnosis.GetImpressionsOS(i, RetId, RetString, RetEye, RetSevA, RetSevB, RetA, RetQ, RCnf, RHst, RRul, ROrd) Then
        Temp = ""
        If (RetA = "T") Then
            If (Trim(RetSevA) <> "") Then
                Temp = "OD " + RetSevA
            End If
            If (Trim(RetSevB) <> "") Then
                Temp = Temp + " OS " + RetSevB
            End If
            If (Left(RetId, 1) = "-") Then
                RetId = ""
            End If
            ATemp = ""
            If (RCnf) Then
                ATemp = ATemp + "NF-"
            End If
            If (RHst) Then
                ATemp = ATemp + "HO-"
            End If
            If (RRul) Then
                ATemp = ATemp + "RO-"
            End If
            ATemp = ATemp + Trim(RetQ)
            If (RetEye = "OD") Then
                lstImpLVOD.ListItems.Add Trim(str(lstImpLVOD.ListItems.Count + 1))
                lstImpLVOD.ListItems.Item(lstImpLVOD.ListItems.Count).SubItems(1) = ATemp + Trim(RetString) + " " + Temp
            ElseIf (RetEye = "OS") Then
                lstImpLVOS.ListItems.Add Trim(str(lstImpLVOS.ListItems.Count + 1))
                lstImpLVOS.ListItems.Item(lstImpLVOS.ListItems.Count).SubItems(1) = ATemp + Trim(RetString) + " " + Temp
            End If
        End If
    End If
Next i

lstTestsLV.ListItems.Clear
tt = frmSystems1.GetTotalTests
For i = 1 To tt
    Call frmSystems1.GetTestText(i, TestText, TestQuestion, TestOrder, TestParty, TestTime, AHigh, AType, Apo)
    If (Trim(TestOrder) <> "") And (AHigh) Then
        lstTestsLV.ListItems.Add Trim(str(lstTestsLV.ListItems.Count + 1))
        lstTestsLV.ListItems.Item(lstTestsLV.ListItems.Count).SubItems(1) = Trim(TestQuestion)
        lstTestsLV.ListItems.Item(lstTestsLV.ListItems.Count).SubItems(2) = " "
        If (AHigh) Then
            lstTestsLV.ListItems.Item(lstTestsLV.ListItems.Count).SubItems(2) = "T"
        End If
        lstTestsLV.ListItems.Item(lstTestsLV.ListItems.Count).SubItems(3) = Trim(str(i))
    ElseIf (Trim(TestOrder) <> "") And (TestOrder = "27A") Then
        lstTestsLV.ListItems.Add Trim(str(lstTestsLV.ListItems.Count + 1))
        lstTestsLV.ListItems.Item(lstTestsLV.ListItems.Count).SubItems(1) = Trim(TestQuestion)
        lstTestsLV.ListItems.Item(lstTestsLV.ListItems.Count).SubItems(2) = " "
        If (AHigh) Then
            lstTestsLV.ListItems.Item(lstTestsLV.ListItems.Count).SubItems(2) = "T"
        End If
        lstTestsLV.ListItems.Item(lstTestsLV.ListItems.Count).SubItems(3) = Trim(str(i))
    End If
Next i

'Actions
lstActionsLV.ListItems.Clear
lstOcuMeds1.ListItems.Clear
lstOcuMeds2.ListItems.Clear
lstLtrs.ListItems.Clear
j = frmEvaluation.TotalActionEvaluation(True)
Dim SpecCLRx As String
For i = 1 To j
    If (frmEvaluation.GetActionEvaluation(i, RetString, RetHigh, RetFollow, SpecCLRx)) Then
        If Not SpecCLRx = "" Then
            RetString = SpecCLRx
        End If
        Call frmSystems1.BuildEvaluationLine(Trim(RetString), TheText, PrescribedOn)
        If (Trim(TheText) <> "") Then
            Temp = ""
            FirstOn = True
            If (UCase(Left(RetString, 2)) <> "RX") Then
                PrescribedOn = True
            ElseIf (UCase(Left(RetString, 2)) = "RX") Then
                If (PrescribedOn) Then
                    RetHigh = True
                    Call frmEvaluation.SetActionHighlight(i, True)
                Else
                    z1 = InStrPS(6, RetString, "/")
                    If (z1 > 6) Then
                        If (IsDrugOkay(Mid(RetString, 6, (z1 - 3) - 5)) <> "O") Then
                            PrescribedOn = False
                        Else
                            PrescribedOn = True
                        End If
                    End If
                End If
            End If
            Call ReplaceCharacters(TheText, "-0/", " ")
            Call ReplaceCharacters(TheText, "-1/", " ")
            Call ReplaceCharacters(TheText, "-2/", " ")
            Call ReplaceCharacters(TheText, "-3/", " ")
            Call ReplaceCharacters(TheText, "-4/", " ")
            Call ReplaceCharacters(TheText, "-5/", " ")
            Call ReplaceCharacters(TheText, "-6/", " ")
            Call ReplaceCharacters(TheText, "P-7/", " ")
            Call ReplaceCharacters(TheText, "-8/", " ")
            Call ReplaceCharacters(TheText, "/", " ")
            Call ReplaceCharacters(TheText, "--", " ")
            Call ReplaceCharacters(TheText, "  ", " ")
            If (UCase(Left(TheText, 2)) = "RX") Then
                TheText = Mid(TheText, 4, Len(TheText) - 3)
                z1 = InStrPS(TheText, "[Last")
                If (z1 > 0) Then
                    z = InStrPS(z1, TheText, "]")
                    If (z > 0) Then
                        TheText = Left(TheText, z1 - 1) + Mid(TheText, z + 1, Len(TheText) - z)
                    End If
                End If
            End If
            TheText = Trim(TheText)
            If (Mid(TheText, Len(TheText) - 1, 2) = " C") Then
                TheText = Trim(TheText) + "hart"
            End If
            If (Trim(TheText) <> "") Then
                z = 1
                While (z < Len(TheText))
                    z1 = 0
                    Temp = Mid(TheText, z, 110)
                    If (z + 110 < Len(TheText)) Then
                        For tt = Len(Temp) To 1 Step -1
                            If (Mid(Temp, tt, 1) = " ") Then
                                z = z + tt
                                z1 = z
                                Temp = Left(Temp, tt)
                                Exit For
                            End If
                        Next tt
                    End If
                    If (z1 = 0) Then
                        z = z + 110
                    End If
                    If (Len(Temp) < 110) Then
                        Temp = Temp + Space(110 - Len(Temp)) + Trim(str(i))
                    Else
                        Temp = Left(Temp, 110) + Trim(str(i))
                    End If
                    If (Trim(Temp) <> "") Then
                        If (UCase(Left(RetString, 2)) = "RX") Then
                            If (PrescribedOn) Then
                                w = InStrPS(Temp, "-")
                                If (w = 0) Then
                                    w = 111
                                End If
                                If (lstOcuMeds1.ListItems.Count = lstOcuMeds2.ListItems.Count) Then
                                    lstOcuMeds1.ListItems.Add Trim(str(lstOcuMeds1.ListItems.Count + 1))
                                    lstOcuMeds1.ListItems.Item(lstOcuMeds1.ListItems.Count).SubItems(1) = Trim(Left(Temp, w - 1))
                                    lstOcuMeds1.ListItems.Item(lstOcuMeds1.ListItems.Count).SubItems(2) = " "
                                    If (RetHigh) Then
                                        lstOcuMeds1.ListItems.Item(lstOcuMeds1.ListItems.Count).SubItems(2) = "T"
                                    End If
                                    lstOcuMeds1.ListItems.Item(lstOcuMeds1.ListItems.Count).SubItems(3) = Trim(str(-1 * i))
                                Else
                                    lstOcuMeds2.ListItems.Add Trim(str(lstOcuMeds2.ListItems.Count + 1))
                                    lstOcuMeds2.ListItems.Item(lstOcuMeds2.ListItems.Count).SubItems(1) = Trim(Left(Temp, w - 1))
                                    lstOcuMeds2.ListItems.Item(lstOcuMeds2.ListItems.Count).SubItems(2) = " "
                                    If (RetHigh) Then
                                        lstOcuMeds2.ListItems.Item(lstOcuMeds2.ListItems.Count).SubItems(2) = "T"
                                    End If
                                    lstOcuMeds2.ListItems.Item(lstOcuMeds2.ListItems.Count).SubItems(3) = Trim(str(-1 * i))
                                End If
                            End If
                        ElseIf (InStrPS(UCase(RetString), "CONSULT") > 0) Or (InStrPS(UCase(RetString), "LETTER") > 0) Then
                            Temp = Left(Temp, 110)
                            lstLtrs.ListItems.Add Trim(str(lstLtrs.ListItems.Count + 1))
                            lstLtrs.ListItems.Item(lstLtrs.ListItems.Count).SubItems(4) = Trim(str(i))
                            If (InStrPS(Temp, ",(P)") > 0) Then
                                lstLtrs.ListItems.Item(lstLtrs.ListItems.Count).SubItems(2) = "T"
                                Call ReplaceCharacters(Temp, ",(P)", "~")
                                Call StripCharacters(Temp, "~")
                            ElseIf (InStrPS(Temp, "-(P)") > 0) Then
                                lstLtrs.ListItems.Item(lstLtrs.ListItems.Count).SubItems(2) = "T"
                                Call ReplaceCharacters(Temp, "-(P)", "~")
                                Call StripCharacters(Temp, "~")
                            End If
                            zz = InStrPS(Temp, "-(")
                            If (zz > 0) Then
                                ATemp = Trim(Mid(Temp, zz + 1, Len(Temp) - zz))
                                zz = InStrPS(ATemp, ")")
                                If (zz > 0) Then
                                    ATemp = Left(ATemp, zz)
                                    Call StripCharacters(ATemp, "(")
                                    Call StripCharacters(ATemp, ")")
                                    Call GetCCDr(Val(ATemp), RTemp)
                                    lstLtrs.ListItems.Item(lstLtrs.ListItems.Count).SubItems(3) = Trim(RTemp)
                                End If
                            Else
                                zz = InStrPS(Temp, "(")
                                If (zz > 0) Then
                                    ATemp = Trim(Mid(Temp, zz, Len(Temp) - (zz - 1)))
                                    zz = InStrPS(ATemp, ")")
                                    If (zz > 0) Then
                                        ATemp = Left(ATemp, zz)
                                        Call StripCharacters(ATemp, "(")
                                        Call StripCharacters(ATemp, ")")
                                        Call GetCCDr(Val(ATemp), RTemp)
                                        lstLtrs.ListItems.Item(lstLtrs.ListItems.Count).SubItems(3) = Trim(RTemp)
                                    End If
                                End If
                            End If
                            zz = InStrPS(Temp, "-")
                            If (zz > 0) Then
                                Temp = Mid(Temp, zz + 1, Len(Temp) - zz)
                            End If
                            zz = InStrPS(Temp, "-")
                            If (zz > 0) Then
                                Temp = Left(Temp, zz - 1)
                            End If
                            lstLtrs.ListItems.Item(lstLtrs.ListItems.Count).SubItems(1) = Trim(Temp)
                        Else
                            lstActionsLV.ListItems.Add Trim(str(lstActionsLV.ListItems.Count + 1))
                            lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(1) = Trim(Left(Temp, 110))
                            lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(2) = " "
                            If (RetHigh) Then
                                lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(2) = "T"
                            End If
                            lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(3) = Trim(str(i))
                            lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(4) = " "
                        End If
                    End If
                Wend
            End If
        End If
    End If
Next i

' Do All Highlighted Actions
ListType = 0
Set RetClinical = New PatientClinical
RetClinical.AppointmentId = 0
RetClinical.PatientId = PatientId
RetClinical.ClinicalType = "A"
RetClinical.Symptom = ""
RetClinical.Findings = ""
RetClinical.ImageDescriptor = ""
If (RetClinical.FindPatientClinicalbyHighlights > 0) Then
    j = 1
    While (RetClinical.SelectPatientClinical(j))
        Temp = ""
        If (UCase(Left(RetClinical.Findings, 2)) <> "RX") And _
           (InStrPS(UCase(RetClinical.Findings), "CONSULT") = 0) And _
           (InStrPS(UCase(RetClinical.Findings), "LETTER") = 0) And _
           (Trim(RetClinical.Findings) <> "") Then
            TheText = Trim(RetClinical.Findings)
            Temp = ""
            FirstOn = True
            PrescribedOn = True
            Call ReplaceCharacters(TheText, "-0/", " ")
            Call ReplaceCharacters(TheText, "-1/", " ")
            Call ReplaceCharacters(TheText, "-2/", " ")
            Call ReplaceCharacters(TheText, "-3/", " ")
            Call ReplaceCharacters(TheText, "-4/", " ")
            Call ReplaceCharacters(TheText, "-5/", " ")
            Call ReplaceCharacters(TheText, "-6/", " ")
            Call ReplaceCharacters(TheText, "P-7/", " ")
            Call ReplaceCharacters(TheText, "-8/", " ")
            Call ReplaceCharacters(TheText, "/", " ")
            Call ReplaceCharacters(TheText, "--", " ")
            Call ReplaceCharacters(TheText, "  ", " ")
            TheText = Trim(TheText)
            If (Mid(TheText, Len(TheText) - 1, 2) = " C") Then
                TheText = Trim(TheText) + "hart"
            End If
            If (Trim(TheText) <> "") Then
                z = 1
                While (z < Len(TheText))
                    z1 = 0
                    Temp = Mid(TheText, z, 110)
                    If (z + 110 < Len(TheText)) Then
                        For tt = Len(Temp) To 1 Step -1
                            If (Mid(Temp, tt, 1) = " ") Then
                                z = z + tt
                                z1 = z
                                Temp = Left(Temp, tt)
                                Exit For
                            End If
                        Next tt
                    End If
                    If (z1 = 0) Then
                        z = z + 110
                    End If
                    If (Len(Temp) < 110) Then
                        Temp = Temp + Space(110 - Len(Temp)) + Trim(str(i))
                    Else
                        Temp = Left(Temp, 110) + Trim(str(i))
                    End If
                    If (Trim(Temp) <> "") Then
                        lstActionsLV.ListItems.Add Trim(str(lstActionsLV.ListItems.Count + 1))
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(1) = Trim(Left(Temp, 110))
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(2) = "T"
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(3) = Trim(str(RetClinical.ClinicalId))
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(4) = "A"
                    End If
                Wend
            End If
        End If
        j = j + 1
    Wend
End If
Set RetClinical = Nothing

' Do All Drugs
ListType = 0
Set RetClinical = New PatientClinical
RetClinical.AppointmentId = 0
RetClinical.PatientId = PatientId
RetClinical.ClinicalType = "A"
RetClinical.Symptom = ""
RetClinical.Findings = ""
RetClinical.ImageDescriptor = ""
If (RetClinical.FindRxClinical > 0) Then
    j = 1
    While (RetClinical.SelectPatientClinical(j))
        Temp = ""
        If (UCase(Left(RetClinical.Findings, 2)) = "RX") Then
            MyCln = RetClinical.ClinicalId
            If (Trim(RetClinical.ImageDescriptor) = "D" & oDate.GetDisplayDate(False)) Or (Trim(RetClinical.ImageDescriptor) = "R" & oDate.GetDisplayDate(False)) Or (Trim(RetClinical.ImageDescriptor) = "") And (Len(Trim(RetClinical.Findings)) > 4) Then
                RetString = Trim(RetClinical.Findings)
                Call frmSystems1.BuildEvaluationLine(Trim(RetString), TheText, PrescribedOn)
                If (Left(RetClinical.ImageDescriptor, 1) = "D") Then
                    Temp = Trim(TheText) + "Discontinued on " & oDate.GetDisplayDate(False)
                ElseIf (Left(RetClinical.ImageDescriptor, 1) = "R") Then
                    Temp = Trim(TheText) + "Refilled on " & oDate.GetDisplayDate(False)
                Else
                    Temp = Trim(TheText)
                End If
                If (InStrPS(RetString, "P-7") < 1) Then
                    PrescribedOn = False
                End If
                z1 = InStrPS(6, RetString, "/")
                If (z1 > 6) Then
                    If (IsDrugOkay(Mid(RetString, 6, (z1 - 3) - 5)) <> "O") Then
                        PrescribedOn = False
                    Else
                        PrescribedOn = True
                    End If
                End If
                Call ReplaceCharacters(Temp, "-0/", " ")
                Call ReplaceCharacters(Temp, "-1/", " ")
                Call ReplaceCharacters(Temp, "-2/", " ")
                Call ReplaceCharacters(Temp, "-3/", " ")
                Call ReplaceCharacters(Temp, "-4/", " ")
                Call ReplaceCharacters(Temp, "-5/", " ")
                Call ReplaceCharacters(Temp, "-6/", " ")
                Call ReplaceCharacters(Temp, "P-7/", " ")
                Call ReplaceCharacters(Temp, "-8/", " ")
                Call ReplaceCharacters(Temp, "/", " ")
                Call ReplaceCharacters(Temp, "--", " ")
                Call ReplaceCharacters(Temp, "  ", " ")
                If (UCase(Left(Temp, 2)) = "RX") Then
                    Temp = Mid(Temp, 4, Len(Temp) - 3)
                End If
                TheText = Trim(Temp)
                If (Trim(TheText) <> "") Then
                    z = 1
                    While (z < Len(TheText))
                        z1 = 0
                        Temp = Mid(TheText, z, 110)
                        If (z + 110 < Len(TheText)) Then
                            For tt = Len(Temp) To 1 Step -1
                                If (Mid(Temp, tt, 1) = " ") Then
                                    z = z + tt
                                    z1 = z
                                    Temp = Left(Temp, tt)
                                    Exit For
                                End If
                            Next tt
                        End If
                        If (z1 = 0) Then
                            z = z + 110
                        End If
                        If (UCase(Left(RetString, 2)) <> "RX") Then
                            Temp = ""
                        End If
                        If (Temp <> "") Then
                            If (PrescribedOn) Then
                                w = InStrPS(Temp, "-")
                                If (w = 0) Then
                                    w = 111
                                End If
                                If (lstOcuMeds1.ListItems.Count = lstOcuMeds2.ListItems.Count) Then
                                    lstOcuMeds1.ListItems.Add Trim(str(lstOcuMeds1.ListItems.Count + 1))
                                    lstOcuMeds1.ListItems.Item(lstOcuMeds1.ListItems.Count).SubItems(1) = Left(Temp, w - 1)
                                    lstOcuMeds1.ListItems.Item(lstOcuMeds1.ListItems.Count).SubItems(2) = " "
                                    If (Left(RetClinical.ImageDescriptor, 1) <> "D") Then
                                        lstOcuMeds1.ListItems.Item(lstOcuMeds1.ListItems.Count).SubItems(2) = "T"
                                    End If
                                    lstOcuMeds1.ListItems.Item(lstOcuMeds1.ListItems.Count).SubItems(3) = Trim(str(MyCln))
                                Else
                                    lstOcuMeds2.ListItems.Add Trim(str(lstOcuMeds2.ListItems.Count + 1))
                                    lstOcuMeds2.ListItems.Item(lstOcuMeds2.ListItems.Count).SubItems(1) = Left(Temp, w - 1)
                                    lstOcuMeds2.ListItems.Item(lstOcuMeds2.ListItems.Count).SubItems(2) = " "
                                    If (Left(RetClinical.ImageDescriptor, 1) <> "D") Then
                                        lstOcuMeds2.ListItems.Item(lstOcuMeds2.ListItems.Count).SubItems(2) = "T"
                                    End If
                                    lstOcuMeds2.ListItems.Item(lstOcuMeds2.ListItems.Count).SubItems(3) = Trim(str(MyCln))
                                End If
                            End If
                        End If
                    Wend
                End If
            End If
        End If
        j = j + 1
    Wend
End If
Set RetClinical = Nothing

' Notes
i = 1
Set RetNotes = New PatientNotes
RetNotes.NotesPatientId = PatientId
RetNotes.NotesAppointmentId = AppointmentId
RetNotes.NotesType = "C"
RetNotes.NotesSystem = ""
If (RetNotes.FindNotes > 0) Then
    While (RetNotes.SelectNotes(i))
        MyCln = RetNotes.NotesId
        RetHigh = False
        If (Trim(RetNotes.NotesHighlight) <> "") Then
            RetHigh = True
        End If
        If (Trim(RetNotes.NotesSystem) = "") Then
            Temp = ""
            If (Trim(RetNotes.NotesText1) <> "") Then
                Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                     + Trim(RetNotes.NotesText2) + " " _
                     + Trim(RetNotes.NotesText3) + " " _
                     + Trim(RetNotes.NotesText4)
                Temp = Trim(Temp)
                Call StripCharacters(Temp, Chr(13))
                Call StripCharacters(Temp, Chr(10))
                TheText = Trim(Temp)
                If (Trim(TheText) <> "") Then
                    z = 1
                    While (z < Len(TheText))
                        z1 = 0
                        Temp = Mid(TheText, z, 110)
                        If (z + 110 < Len(TheText)) Then
                            For tt = Len(Temp) To 1 Step -1
                                If (Mid(Temp, tt, 1) = " ") Then
                                    z = z + tt
                                    z1 = z
                                    Temp = Left(Temp, tt)
                                    Exit For
                                End If
                            Next tt
                        End If
                        If (z1 = 0) Then
                            z = z + 110
                        End If
                        Temp = Trim(Temp)
                        Call StripCharacters(Temp, Chr(13))
                        Call StripCharacters(Temp, Chr(10))
                        If (Trim(Temp) <> "") Then
                            For tt = 1 To Len(Temp) Step 60
                                QTemp = Mid(Temp, tt, 60)
                                ListType = 3
                                GoSub WrapIt
                            Next tt
                        End If
                    Wend
                End If
            End If
        ElseIf (Trim(RetNotes.NotesSystem) = "IMPR") Then
            Temp = ""
            If (Trim(RetNotes.NotesText1) <> "") Then
                Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                     + Trim(RetNotes.NotesText2) + " " _
                     + Trim(RetNotes.NotesText3) + " " _
                     + Trim(RetNotes.NotesText4)
                Temp = Trim(Temp)
                Call StripCharacters(Temp, Chr(13))
                Call StripCharacters(Temp, Chr(10))
                If (Trim(Temp) <> "") Then
                    For tt = 1 To Len(Temp) Step 60
                        QTemp = Mid(Temp, tt, 60)
                        ListType = 2
                        GoSub WrapIt
                    Next tt
                End If
            End If
        ElseIf (Left(Trim(RetNotes.NotesSystem), 5) = "IMPR-") Or (Left(Trim(RetNotes.NotesSystem), 5) = "IMDN-") Then
            If (Trim(RetNotes.NotesILPNRef) = "") Then
                tt = InStrPS(RetNotes.NotesSystem, "-")
                If (tt > 0) Then
                    Temp = Trim(Mid(RetNotes.NotesSystem, tt + 1, Len(RetNotes.NotesSystem) - tt))
                    Call GetDiagnosis(Temp, QTemp)
                    If (QTemp <> "") Then
                        Temp = RetNotes.NotesEye + ":" + Trim(QTemp) + "-"
                    Else
                        Temp = RetNotes.NotesEye + ":" + Trim(RetNotes.NotesILPNRef) + " "
                    End If
                Else
                    Temp = RetNotes.NotesEye + ":" + Trim(RetNotes.NotesILPNRef) + " "
                End If
            Else
                Temp = RetNotes.NotesEye + ":" + Trim(RetNotes.NotesILPNRef) + " "
            End If
            If (Trim(RetNotes.NotesText1) <> "") Then
                Temp = Temp + Trim(RetNotes.NotesText1) + " " _
                     + Trim(RetNotes.NotesText2) + " " _
                     + Trim(RetNotes.NotesText3) + " " _
                     + Trim(RetNotes.NotesText4)
                Temp = Trim(Temp)
                Call StripCharacters(Temp, Chr(13))
                Call StripCharacters(Temp, Chr(10))
                If (Trim(Temp) <> "") Then
                    For tt = 1 To Len(Temp) Step 60
                        QTemp = Mid(Temp, tt, 60)
                        WrapClean = False
                        If (Mid(Temp, tt + 60, 1) = " ") Then
                            WrapClean = True
                        End If
                        ListType = 2
                        GoSub WrapIt
                    Next tt
                End If
            End If
        End If
        i = i + 1
    Wend
End If
Set RetNotes = Nothing
Call SetDrawing(AppointmentId)

Call AutoSetHighlightsLocally

cmdQuit.Visible = Not ViewOn
cmdPlan.Visible = Not ViewOn
cmdImpr.Visible = Not ViewOn
cmdDone.Text = "Sign chart and check out"
If (ViewOn) Then
    cmdDone.Text = "Done"
End If
LoadFinal = True
Exit Function
WrapIt:
    If (Trim(QTemp) <> "") Then
        If (Mid(QTemp, Len(QTemp), 1) = " ") Or (Len(QTemp) < 60) Or (WrapClean) Then
            If (ListType = 1) Then
            
            ElseIf (ListType = 2) Then
                lstILPNLV.ListItems.Add Trim(str(lstILPNLV.ListItems.Count + 1))
                lstILPNLV.ListItems.Item(lstILPNLV.ListItems.Count).SubItems(1) = Trim(QTemp)
                lstILPNLV.ListItems.Item(lstILPNLV.ListItems.Count).SubItems(2) = Trim(str(MyCln))
            ElseIf (ListType = 3) Then
                lstActionsLV.ListItems.Add Trim(str(lstActionsLV.ListItems.Count + 1))
                lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(1) = QTemp
                lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(2) = " "
                If (RetHigh) Then
                    lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(2) = "T"
                End If
                lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(3) = Trim(str(MyCln))
                lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(4) = "N"
            ElseIf (ListType = 4) Then
                lstHPILV.ListItems.Add Trim(str(lstHPILV.ListItems.Count + 1))
                lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(1) = QTemp
            End If
        Else
            For p = Len(QTemp) To 1 Step -1
                If (Mid(QTemp, p, 1) = " ") Then
                    PTemp = Left(QTemp, p)
                    If (ListType = 1) Then
                    
                    ElseIf (ListType = 2) Then
                        lstILPNLV.ListItems.Add Trim(str(lstILPNLV.ListItems.Count + 1))
                        lstILPNLV.ListItems.Item(lstILPNLV.ListItems.Count).SubItems(1) = Trim(PTemp)
                        lstILPNLV.ListItems.Item(lstILPNLV.ListItems.Count).SubItems(2) = Trim(str(MyCln))
                        tt = tt - (Len(QTemp) - p)
                    ElseIf (ListType = 3) Then
                        lstActionsLV.ListItems.Add Trim(str(lstActionsLV.ListItems.Count + 1))
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(1) = Trim(PTemp)
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(2) = " "
                        If (RetHigh) Then
                            lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(2) = "T"
                        End If
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(3) = Trim(str(MyCln))
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(4) = "N"
                        lstActionsLV.ListItems.Add Trim(str(lstActionsLV.ListItems.Count + 1))
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(1) = Mid(QTemp, p + 1, Len(QTemp) - p)
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(2) = " "
                        If (RetHigh) Then
                            lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(2) = "T"
                        End If
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(3) = Trim(str(MyCln))
                        lstActionsLV.ListItems.Item(lstActionsLV.ListItems.Count).SubItems(4) = "N"
                    ElseIf (ListType = 4) Then
                        lstHPILV.ListItems.Add Trim(str(lstHPILV.ListItems.Count + 1))
                        lstHPILV.ListItems.Item(lstHPILV.ListItems.Count).SubItems(1) = Trim(PTemp)
                        tt = tt - (Len(QTemp) - p)
                    End If
                    Exit For
                End If
            Next p
        End If
    End If
    Return
UI_ErrorHandler:
    LoadFinal = False
    MsgBox "Error Occurred !!! " + Error(Err)
    Resume LeaveFast
LeaveFast:
End Function

Private Function SetDrawing(ApptId As Long) As Boolean
Dim ImageOrder As Boolean
Dim i As Integer, j As Integer
Dim Rec As String
Dim Temp As String, ADate As String
Dim MyFile As String, TheFile As String
Dim ScratchFile As String, MyBase As String
Dim ScratchFile1 As String, ScratchFile2 As String
Dim iFreeFile As Long
Dim RetAppt As SchedulerAppointment
ImageOrder = CheckConfigCollection("IMAGEORDER") = "T"
FxImage1.Tag = ""
Set FxImage1.picture = Nothing
lblDrawDate.Visible = False
lblDrawDate.Caption = ""
If (ApptId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ADate = RetAppt.AppointmentDate
        ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Mid(ADate, 1, 4)
    End If
    Set RetAppt = Nothing
' Find all drawings for this appointment
    Erase DrawFile
    MyFile = MyImageDir + "MyDraw-A" + Trim(str(ApptId)) + "-*.jpg"
    TheFile = Dir(MyFile)
    While (TheFile <> "") And (j < 20)
        j = j + 1
        DrawFile(j) = TheFile
        TheFile = Dir
    Wend
' Also do Other Images
    MyFile = MyImageDir + "NewBase-D*-A" + Trim(str(ApptId)) + "-*.jpg"
    TheFile = Dir(MyFile)
    While (TheFile <> "") And (j < 20)
        j = j + 1
        DrawFile(j) = TheFile
        TheFile = Dir
    Wend
    MaxDraw = j
    If (ImageOrder) And (MaxDraw > 1) Then
' make 08 #1
        For i = 1 To MaxDraw
            If (InStrPS(DrawFile(i), "08") > 0) Then
                TheFile = DrawFile(i)
                For j = i To MaxDraw - 1
                    DrawFile(j) = DrawFile(j + 1)
                Next j
                DrawFile(MaxDraw) = ""
                For j = MaxDraw To 2 Step -1
                    DrawFile(j) = DrawFile(j - 1)
                Next j
                DrawFile(1) = TheFile
                TheFile = ""
                Exit For
            End If
        Next i
    End If
    CurDraw = 1
    TheFile = DrawFile(CurDraw)
    If (Trim(TheFile) <> "") Then
        MyBase = ""
        i = InStrPS(TheFile, ".")
        If (i > 0) Then
            MyBase = Mid(TheFile, i - 2, 2)
        End If
        If (Trim(TheFile) <> "") Then
            MyFile = MyImageDir + TheFile
            FxImage1.FileName = FM.GetPathForDirectIO(MyFile)
            GoSub GetScratch
            FxImage1.Tag = ScratchFile
            lblDrawDate.Caption = ADate
            lblDrawDate.Visible = False
        End If
    End If
    cmdODraw.Visible = False
    If (MaxDraw > 1) Then
        cmdODraw.Visible = True
    End If
End If
Exit Function
GetScratch:
    ScratchFile = ""
    If (Trim(MyBase) <> "") Then
        ScratchFile2 = DoctorInterfaceDirectory + "I*_" + Trim(str(ApptId)) + "_*.txt"
        ScratchFile1 = Dir(ScratchFile2)
        Do Until (Trim(ScratchFile1) = "")
            ScratchFile1 = DoctorInterfaceDirectory + ScratchFile1
            iFreeFile = FreeFile
            FM.OpenFile ScratchFile1, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(iFreeFile)
            Line Input #iFreeFile, Rec
            If (Left(Rec, 7) = "MYDRAW=") Then
                If (InStrPS(Rec, MyBase) > 0) Then
                    ScratchFile = ScratchFile1
                    FM.CloseFile CLng(iFreeFile)
                    Exit Do
                End If
            End If
            FM.CloseFile CLng(iFreeFile)
            ScratchFile1 = Dir
        Loop
    End If
    Return
End Function

Private Function NextDrawing(ApptId As Long) As Boolean
Dim i As Integer, j As Integer
Dim Rec As String
Dim Temp As String, ADate As String
Dim MyFile As String, TheFile As String
Dim ScratchFile As String, MyBase As String
Dim ScratchFile1 As String, ScratchFile2 As String
Dim iFreeFile As Long
Dim RetAppt As SchedulerAppointment
NextDrawing = False
CurDraw = CurDraw + 1
If (CurDraw > MaxDraw) Then
    CurDraw = 1
End If
FxImage1.Tag = ""
Set FxImage1.picture = Nothing
lblDrawDate.Visible = False
lblDrawDate.Caption = ""
TheFile = DrawFile(CurDraw)
If (Trim(TheFile) <> "") Then
    MyBase = ""
    i = InStrPS(TheFile, ".")
    If (i > 0) Then
        MyBase = Mid(TheFile, i - 2, 2)
    End If
    If (Left(TheFile, 7) = "NewBase") Then
        MyFile = MyImageDir + TheFile
        FxImage1.FileName = FM.GetPathForDirectIO(MyFile)
        GoSub GetScratch
        FxImage1.Tag = ScratchFile
        lblDrawDate.Caption = ADate
        lblDrawDate.Visible = False
    ElseIf (Trim(TheFile) <> "") Then
        MyFile = MyImageDir + TheFile
        FxImage1.FileName = FM.GetPathForDirectIO(MyFile)
        GoSub GetScratch
        FxImage1.Tag = ScratchFile
        lblDrawDate.Caption = ADate
        lblDrawDate.Visible = False
    End If
End If
Exit Function
GetScratch:
    ScratchFile = ""
    If (Trim(MyBase) <> "") Then
        ScratchFile2 = DoctorInterfaceDirectory + "I*_" + Trim(str(ApptId)) + "_*.txt"
        ScratchFile1 = Dir(ScratchFile2)
        Do Until (Trim(ScratchFile1) = "")
            ScratchFile1 = DoctorInterfaceDirectory + ScratchFile1
            iFreeFile = FreeFile
            FM.OpenFile ScratchFile1, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(iFreeFile)
            Line Input #iFreeFile, Rec
            If (Left(Rec, 7) = "MYDRAW=") Then
                If (InStrPS(Rec, MyBase) > 0) Then
                    ScratchFile = ScratchFile1
                    FM.CloseFile CLng(iFreeFile)
                    Exit Do
                End If
            End If
            FM.CloseFile CLng(iFreeFile)
            ScratchFile1 = Dir
        Loop
    End If
    Return
End Function

Private Sub form_load()
ExitOn = False
End Sub

Private Sub FxImage1_Click()
If (Trim(FxImage1.Tag) <> "") Then
    If (frmViewDraw.LoadDraw(0, Trim(FxImage1.Tag))) Then
        frmViewDraw.Show 1
    End If
End If
End Sub

Private Sub lstActionsLV_Click()
Dim i As Integer
Dim Id As String
Dim MyCnt As Integer
Dim RetNotes As PatientNotes
If (lstActionsLV.ListItems.Count > 0) Then
    MyCnt = 0
    For i = 1 To lstActionsLV.ListItems.Count
        If (lstActionsLV.ListItems.Item(i).Selected) Then
            MyCnt = i
            Exit For
        End If
    Next i
    If (MyCnt > 0) And (Trim(lstActionsLV.ListItems.Item(i).SubItems(1)) <> "") Then
        frmEventMsgs.Header = "Highlight ?"
        frmEventMsgs.AcceptText = "Set"
        frmEventMsgs.RejectText = "UnSet"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Magnify"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                Id = lstActionsLV.ListItems.Item(MyCnt).SubItems(3)
                If (Val(Id) > 0) Then
                    If (lstActionsLV.ListItems.Item(MyCnt).SubItems(4) = "N") Then
                        Set RetNotes = New PatientNotes
                        RetNotes.NotesId = Val(Id)
                        If (RetNotes.RetrieveNotes) Then
                            RetNotes.NotesHighlight = "V"
                            Call RetNotes.ApplyNotes
                        End If
                        Set RetNotes = Nothing
                        lstActionsLV.ListItems.Item(MyCnt).SubItems(2) = "T"
                    ElseIf (lstActionsLV.ListItems.Item(MyCnt).SubItems(4) = "A") Then
                        ' cannot change
                    Else
                        Call frmEvaluation.SetActionHighlight(Val(Id), True)
                        lstActionsLV.ListItems.Item(MyCnt).SubItems(2) = "T"
                        Call frmEvaluation.PostToFile
                    End If
                End If
            ElseIf (frmEventMsgs.Result = 2) Then
                Id = lstActionsLV.ListItems.Item(MyCnt).SubItems(3)
                If (Val(Id) > 0) Then
                    If (lstActionsLV.ListItems.Item(MyCnt).SubItems(4) = "N") Then
                        Set RetNotes = New PatientNotes
                        RetNotes.NotesId = Val(Id)
                        If (RetNotes.RetrieveNotes) Then
                            RetNotes.NotesHighlight = ""
                            Call RetNotes.ApplyNotes
                        End If
                        Set RetNotes = Nothing
                        lstActionsLV.ListItems.Item(i).SubItems(2) = " "
                    ElseIf (lstActionsLV.ListItems.Item(MyCnt).SubItems(4) = "A") Then
                        ' cannot change
                    Else
                        Call frmEvaluation.SetActionHighlight(Val(Id), False)
                        lstActionsLV.ListItems.Item(i).SubItems(2) = " "
                        Call frmEvaluation.PostToFile
                    End If
                End If
            ElseIf (frmEventMsgs.Result = 3) Then
                txtZoom.Text = ""
                txtZoom.Text = lstActionsLV.ListItems.Item(i).SubItems(1)
                txtZoom.Visible = True
            End If
    End If
End If
End Sub

Private Sub lstBilledLV_Click()
Dim i As Integer
Dim Id As String
Dim MyCnt As Integer
If (lstBilledLV.ListItems.Count > 0) Then
    MyCnt = 0
    For i = 1 To lstBilledLV.ListItems.Count
        If (lstBilledLV.ListItems.Item(i).Selected) Then
            MyCnt = i
            Exit For
        End If
    Next i
    If (MyCnt > 0) Then
        frmEventMsgs.Header = "Highlight ?"
        frmEventMsgs.AcceptText = "Set"
        frmEventMsgs.RejectText = "UnSet"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Magnify"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                lstBilledLV.ListItems.Item(MyCnt).SubItems(2) = "T"
                Id = lstBilledLV.ListItems.Item(MyCnt).SubItems(4)
                If (lstBilledLV.ListItems.Item(MyCnt).SubItems(2) = "D") Then
                    Call frmSetProcedures.SetDiagnosisHighlight(Id, True)
                    Call frmSetProcedures.RetainBilledDiagnosis(PatientId)
                ElseIf (lstBilledLV.ListItems.Item(MyCnt).SubItems(2) = "P") Then
                    Call frmSetProcedures.SetProcedureHighlight(Id, True)
                    Call frmSetProcedures.RetainBilledProcedures(PatientId)
                ElseIf (lstBilledLV.ListItems.Item(MyCnt).SubItems(2) = "E") Then
                    Call frmSetEMProcedure.SetEMProcedureHighlight(True)
                    Call frmSetEMProcedure.RetainEMProcedure(PatientId)
                End If
            ElseIf (frmEventMsgs.Result = 2) Then
                lstBilledLV.ListItems.Item(MyCnt).SubItems(2) = " "
                Id = lstBilledLV.ListItems.Item(MyCnt).SubItems(4)
                If (lstBilledLV.ListItems.Item(MyCnt).SubItems(2) = "D") Then
                    Call frmSetProcedures.SetDiagnosisHighlight(Id, False)
                    Call frmSetProcedures.RetainBilledDiagnosis(PatientId)
                ElseIf (lstBilledLV.ListItems.Item(MyCnt).SubItems(2) = "P") Then
                    Call frmSetProcedures.SetProcedureHighlight(Id, False)
                    Call frmSetProcedures.RetainBilledProcedures(PatientId)
                ElseIf (lstBilledLV.ListItems.Item(MyCnt).SubItems(2) = "E") Then
                    Call frmSetEMProcedure.SetEMProcedureHighlight(False)
                    Call frmSetEMProcedure.RetainEMProcedure(PatientId)
                End If
            ElseIf (frmEventMsgs.Result = 3) Then
                txtZoom.Text = lstBilledLV.ListItems.Item(MyCnt).SubItems(1)
                txtZoom.Visible = True
            End If
    End If
End If
End Sub

Private Sub lstFindingOD_Click()
Dim i As Integer
Dim Id As String
Dim MyCnt As Integer
If (lstFindingOD.ListItems.Count > 0) Then
    MyCnt = 0
    For i = 1 To lstFindingOD.ListItems.Count
        If (lstFindingOD.ListItems.Item(i).Selected) Then
            MyCnt = i
            Exit For
        End If
    Next i
    If (MyCnt > 0) Then
        frmEventMsgs.Header = "Highlight ?"
        frmEventMsgs.AcceptText = "Set"
        frmEventMsgs.RejectText = "UnSet"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Magnify"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                lstFindingOD.ListItems.Item(MyCnt).SubItems(3) = "T"
                Id = Trim(lstFindingOD.ListItems.Item(MyCnt).SubItems(4))
                Call frmSystems1.SetFindingHighlight("OD", Id, True)
                Call frmSystems1.RetainDiagList(PatientId)
            ElseIf (frmEventMsgs.Result = 2) Then
                lstFindingOD.ListItems.Item(MyCnt).SubItems(3) = " "
                Id = Trim(lstFindingOD.ListItems.Item(MyCnt).SubItems(4))
                Call frmSystems1.SetFindingHighlight("OD", Id, False)
                Call frmSystems1.RetainDiagList(PatientId)
            ElseIf (frmEventMsgs.Result = 3) Then
                txtZoom.Text = ""
                txtZoom.Text = lstFindingOD.ListItems.Item(i).SubItems(1)
                txtZoom.Visible = True
            End If
    End If
End If
End Sub

Private Sub lstFindingOS_Click()
Dim i As Integer
Dim Id As String
Dim MyCnt As Integer
If (lstFindingOS.ListItems.Count > 0) Then
    MyCnt = 0
    For i = 1 To lstFindingOS.ListItems.Count
        If (lstFindingOS.ListItems.Item(i).Selected) Then
            MyCnt = i
            Exit For
        End If
    Next i
    If (MyCnt > 0) Then
        frmEventMsgs.Header = "Highlight ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Magnify"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                lstFindingOS.ListItems.Item(MyCnt).SubItems(3) = "T"
                Id = Trim(lstFindingOS.ListItems.Item(MyCnt).SubItems(4))
                Call frmSystems1.SetFindingHighlight("OS", Id, True)
                Call frmSystems1.RetainDiagList(PatientId)
            ElseIf (frmEventMsgs.Result = 2) Then
                lstFindingOS.ListItems.Item(MyCnt).SubItems(3) = " "
                Id = Trim(lstFindingOS.ListItems.Item(MyCnt).SubItems(4))
                Call frmSystems1.SetFindingHighlight("OS", Id, False)
                Call frmSystems1.RetainDiagList(PatientId)
            ElseIf (frmEventMsgs.Result = 2) Then
                txtZoom.Text = ""
                txtZoom.Text = lstFindingOS.ListItems.Item(i).SubItems(1)
                txtZoom.Visible = True
            End If
    End If
End If
End Sub

Private Sub lstTestsLV_Click()
Dim i As Integer
Dim Apo As Integer
Dim MyCnt As Integer
Dim AHigh As Boolean
Dim Id As String
Dim TestText As String, TestQuestion As String
Dim TestOrder As String, TestParty As String
Dim TestTime As String, AType As String
If (lstTestsLV.ListItems.Count > 0) Then
    MyCnt = 0
    For i = 1 To lstTestsLV.ListItems.Count
        If (lstTestsLV.ListItems.Item(i).Selected) Then
            MyCnt = i
            Exit For
        End If
    Next i
    If (MyCnt > 0) And (Trim(lstTestsLV.ListItems.Item(i).SubItems(1)) <> "") Then
        frmEventMsgs.Header = "Highlight ?"
        frmEventMsgs.AcceptText = "Set"
        frmEventMsgs.RejectText = "UnSet"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Details"
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                lstTestsLV.ListItems.Item(MyCnt).SubItems(2) = "T"
                Id = lstTestsLV.ListItems.Item(MyCnt).SubItems(3)
                If (Val(Id) > 0) Then
                    Call frmSystems1.SetTestHighlight(Val(Id), True)
                End If
            ElseIf (frmEventMsgs.Result = 2) Then
                lstTestsLV.ListItems.Item(MyCnt).SubItems(2) = " "
                Id = lstTestsLV.ListItems.Item(MyCnt).SubItems(3)
                If (Val(Id) > 0) Then
                    Call frmSystems1.SetTestHighlight(Val(Id), False)
                End If
            ElseIf (frmEventMsgs.Result = 3) Then
                Id = lstTestsLV.ListItems.Item(MyCnt).SubItems(3)
                If (Val(Id) > 0) Then
                    Call frmSystems1.GetTestText(Val(Id), TestText, TestQuestion, TestOrder, TestParty, TestTime, AHigh, AType, Apo)
                    txtZoom.Text = TestText
                    txtZoom.Visible = True
                End If
            End If
    End If
End If
End Sub

Private Function AutoSetHighlightsLocally() As Boolean
Dim i As Integer
Dim j As Integer
Dim EndLink As Integer
Dim ATemp As String
For i = 1 To lstBilledLV.ListItems.Count
    If (lstBilledLV.ListItems.Item(i).SubItems(3) = "P") Then
        lstBilledLV.ListItems.Item(i).SubItems(2) = "T"
    ElseIf (lstBilledLV.ListItems.Item(i).SubItems(3) = "E") Then
        lstBilledLV.ListItems.Item(i).SubItems(2) = "T"
    End If
Next i
For i = 1 To lstBilledLV.ListItems.Count
    lstBilledLV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstActionsLV.ListItems.Count
    If (UCase(Left(lstActionsLV.ListItems.Item(i).SubItems(1), 17)) = "OFFICE PROCEDURES") Or (UCase(Left(lstActionsLV.ListItems.Item(i).SubItems(1), 12)) = "ORDER A TEST") Or (UCase(Left(lstActionsLV.ListItems.Item(i).SubItems(1), 16)) = "SCHEDULE SURGERY") Then
        lstActionsLV.ListItems.Item(i).SubItems(2) = "T"
    End If
Next i
For i = 1 To lstActionsLV.ListItems.Count
    lstActionsLV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstTestsLV.ListItems.Count
    ATemp = Trim(lstTestsLV.ListItems.Item(i).SubItems(1))
    If (IsHighlight(ATemp)) Then
        lstTestsLV.ListItems.Item(i).SubItems(2) = "T"
    End If
Next i
For i = 1 To lstTestsLV.ListItems.Count
    lstTestsLV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstRvLV.ListItems.Count
    lstRvLV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstHPILV.ListItems.Count
    lstHPILV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstImpLVOD.ListItems.Count
    lstImpLVOD.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstImpLVOS.ListItems.Count
    lstImpLVOS.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstAllergy1LV.ListItems.Count
    lstAllergy1LV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstAllergy2LV.ListItems.Count
    lstAllergy2LV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstFindingOD.ListItems.Count
    lstFindingOD.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstFindingOS.ListItems.Count
    lstFindingOS.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstOcuMeds1.ListItems.Count
    lstOcuMeds1.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstOcuMeds2.ListItems.Count
    lstOcuMeds2.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstILPNLV.ListItems.Count
    lstILPNLV.ListItems.Item(i).Selected = False
Next i
For i = 1 To lstLtrs.ListItems.Count
    lstLtrs.ListItems.Item(i).Selected = False
Next i
End Function

Private Function IsHighlight(AQuest As String) As Boolean
Dim RetQues As DynamicClass
IsHighlight = False
If (Trim(AQuest) <> "") Then
    Set RetQues = New DynamicClass
    RetQues.Question = AQuest
    If (RetQues.RetrieveClassbyQuestion) Then
        If (RetQues.Highlight = "T") Then
            IsHighlight = True
        End If
    End If
    Set RetQues = Nothing
End If
End Function

Private Sub txtZoom_Click()
txtZoom.Visible = False
End Sub

Private Function GetCCDr(VndId As Long, AName As String) As Boolean
Dim RetVnd As PracticeVendors
AName = ""
If (VndId > 0) Then
    Set RetVnd = New PracticeVendors
    RetVnd.VendorId = VndId
    If (RetVnd.RetrieveVendor) Then
        AName = Trim(RetVnd.VendorName)
    End If
    Set RetVnd = Nothing
End If
End Function

Private Function IsDrugOkay(TheName As String) As String
Dim RetDrug As DiagnosisMasterDrugs
IsDrugOkay = ""
If (Trim(TheName) <> "") Then
    If (Mid(TheName, Len(TheName), 1) = "*") Then
        TheName = Left(TheName, Len(TheName) - 1)
    End If
    Set RetDrug = New DiagnosisMasterDrugs
    RetDrug.DrugName = Trim(TheName)
    If (RetDrug.FindDrugbyName > 0) Then
        If (RetDrug.SelectDrug(1)) Then
            IsDrugOkay = Trim(RetDrug.DrugSpecialtyType)
        End If
    End If
    Set RetDrug = Nothing
End If
End Function

Private Function GetDiagnosis(TheDiagnosis As String, TheName As String) As Boolean
Dim TotalFound As Integer
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
GetDiagnosis = False
TheName = ""
If (Trim(TheDiagnosis) <> "") Then
    Set RetrieveDiagnosis = New DiagnosisMasterPrimary
    RetrieveDiagnosis.PrimarySystem = ""
    RetrieveDiagnosis.PrimaryDiagnosis = ""
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = TheDiagnosis
    RetrieveDiagnosis.PrimaryLevel = 0
    RetrieveDiagnosis.PrimaryRank = 0
    If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
        If (RetrieveDiagnosis.SelectPrimary(1)) Then
            If (Trim(RetrieveDiagnosis.PrimaryShortName) <> "") Then
                TheName = RetrieveDiagnosis.PrimaryShortName
            ElseIf (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                TheName = RetrieveDiagnosis.PrimaryLingo
            Else
                TheName = RetrieveDiagnosis.PrimaryName
            End If
            GetDiagnosis = True
        End If
    End If
    Set RetrieveDiagnosis = Nothing
End If
End Function
Public Function FrmClose()
If cmdDone.Text = "Done" Then
Call cmdDone_Click
End If
 Call cmdQuit_Click
 Unload Me
 End Function
Private Function CanGenerateCdaFiles() As Boolean
On Error GoTo lCanGenerateCdaFiles_Error
CanGenerateCdaFiles = False
Dim SQL As String
Dim RS As Recordset

SQL = " Select TOP 1 Value from model.ApplicationSettings where Name='GenerateClinicalDataFilesOnCheckout'"
Set RS = GetRS(SQL)
If RS.RecordCount > 0 Then
   If RS("Value") <> "" Then CanGenerateCdaFiles = CBool(RS("Value"))
End If
Exit Function

lCanGenerateCdaFiles_Error:
    CanGenerateCdaFiles = False
End Function
Private Function AutoTransmitCdaFile()
On Error GoTo lTransmitCdaFile_Error
AutoTransmitCdaFile = False

Dim manager
Dim TransmitCdaFilecomwrapper As New comWrapper
Call TransmitCdaFilecomwrapper.Create("IO.Practiceware.Presentation.Views.ClinicalDataFiles.ClinicalDataFilesViewManager", emptyArgs)
Set manager = TransmitCdaFilecomwrapper.Instance
manager.TransmitClinicalDataFiles AppointmentId
AutoTransmitCdaFile = True

Exit Function
lTransmitCdaFile_Error:
    LogError "frmDisposition", "AutoTransmitCdaFile", Err, Err.Description
End Function

