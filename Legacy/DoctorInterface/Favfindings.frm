VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmFavfindings 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "&H00C0FFFF&"
   ClientHeight    =   9270
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12240
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "Favfindings.frx":0000
   ScaleHeight     =   9270
   ScaleWidth      =   12240
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   240
      TabIndex        =   0
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10320
      TabIndex        =   1
      Top             =   7680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":38A8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt1 
      Height          =   1095
      Left            =   255
      TabIndex        =   2
      Top             =   360
      Width           =   1560
      _Version        =   131072
      _ExtentX        =   2752
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":3A87
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt2 
      Height          =   1095
      Left            =   240
      TabIndex        =   3
      Top             =   1560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":3C68
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt3 
      Height          =   1095
      Left            =   240
      TabIndex        =   4
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":3E49
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt4 
      Height          =   1095
      Left            =   240
      TabIndex        =   5
      Top             =   3960
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":402A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt5 
      Height          =   1095
      Left            =   240
      TabIndex        =   6
      Top             =   5160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":420B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt6 
      Height          =   1095
      Left            =   240
      TabIndex        =   7
      Top             =   6360
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":43EC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt7 
      Height          =   1095
      Left            =   1920
      TabIndex        =   8
      Top             =   360
      Width           =   1560
      _Version        =   131072
      _ExtentX        =   2752
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":45CD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt8 
      Height          =   1095
      Left            =   1920
      TabIndex        =   9
      Top             =   1560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":47AE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt9 
      Height          =   1095
      Left            =   1920
      TabIndex        =   10
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":498F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt10 
      Height          =   1095
      Left            =   1920
      TabIndex        =   11
      Top             =   3960
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":4B70
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt11 
      Height          =   1095
      Left            =   1920
      TabIndex        =   12
      Top             =   5160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":4D51
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt12 
      Height          =   1095
      Left            =   1920
      TabIndex        =   13
      Top             =   6360
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":4F32
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt13 
      Height          =   1095
      Left            =   3600
      TabIndex        =   14
      Top             =   360
      Width           =   1560
      _Version        =   131072
      _ExtentX        =   2752
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":5113
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt14 
      Height          =   1110
      Left            =   3600
      TabIndex        =   15
      Top             =   1560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":52F4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt15 
      Height          =   1110
      Left            =   3600
      TabIndex        =   16
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":54D5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt16 
      Height          =   1110
      Left            =   3600
      TabIndex        =   17
      Top             =   3960
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":56B6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt17 
      Height          =   1095
      Left            =   3600
      TabIndex        =   18
      Top             =   5160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":5897
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt18 
      Height          =   1110
      Left            =   3600
      TabIndex        =   19
      Top             =   6360
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":5A78
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt19 
      Height          =   1095
      Left            =   5280
      TabIndex        =   20
      Top             =   360
      Width           =   1560
      _Version        =   131072
      _ExtentX        =   2752
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":5C59
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt20 
      Height          =   1095
      Left            =   5280
      TabIndex        =   21
      Top             =   1560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":5E3A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt21 
      Height          =   1095
      Left            =   5280
      TabIndex        =   22
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":601B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt22 
      Height          =   1095
      Left            =   5280
      TabIndex        =   23
      Top             =   3960
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":61FC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt23 
      Height          =   1095
      Left            =   5280
      TabIndex        =   24
      Top             =   5160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":63DD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt24 
      Height          =   1095
      Left            =   5280
      TabIndex        =   25
      Top             =   6360
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":65BE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt25 
      Height          =   1095
      Left            =   6960
      TabIndex        =   26
      Top             =   360
      Width           =   1560
      _Version        =   131072
      _ExtentX        =   2752
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":679F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt26 
      Height          =   1095
      Left            =   6960
      TabIndex        =   27
      Top             =   1560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":6980
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt27 
      Height          =   1095
      Left            =   6960
      TabIndex        =   28
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":6B61
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt28 
      Height          =   1095
      Left            =   6960
      TabIndex        =   29
      Top             =   3960
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":6D42
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt29 
      Height          =   1095
      Left            =   6960
      TabIndex        =   30
      Top             =   5160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":6F23
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt30 
      Height          =   1095
      Left            =   6960
      TabIndex        =   31
      Top             =   6360
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":7104
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt31 
      Height          =   1095
      Left            =   8640
      TabIndex        =   32
      Top             =   360
      Width           =   1560
      _Version        =   131072
      _ExtentX        =   2752
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":72E5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt32 
      Height          =   1110
      Left            =   8640
      TabIndex        =   33
      Top             =   1560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":74C6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt33 
      Height          =   1110
      Left            =   8640
      TabIndex        =   34
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":76A7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt34 
      Height          =   1110
      Left            =   8640
      TabIndex        =   35
      Top             =   3960
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":7888
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt35 
      Height          =   1095
      Left            =   8640
      TabIndex        =   36
      Top             =   5160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":7A69
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt36 
      Height          =   1110
      Left            =   8640
      TabIndex        =   37
      Top             =   6360
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":7C4A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt37 
      Height          =   1095
      Left            =   10320
      TabIndex        =   38
      Top             =   360
      Width           =   1560
      _Version        =   131072
      _ExtentX        =   2752
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":7E2B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt38 
      Height          =   1110
      Left            =   10320
      TabIndex        =   39
      Top             =   1560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":800C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt39 
      Height          =   1110
      Left            =   10320
      TabIndex        =   40
      Top             =   2760
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":81ED
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt40 
      Height          =   1110
      Left            =   10320
      TabIndex        =   41
      Top             =   3960
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":83CE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt41 
      Height          =   1095
      Left            =   10320
      TabIndex        =   42
      Top             =   5160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":85AF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt42 
      Height          =   1110
      Left            =   10320
      TabIndex        =   43
      Top             =   6360
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":8790
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   990
      Left            =   5280
      TabIndex        =   44
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favfindings.frx":8971
   End
   Begin VB.Label lblField 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Select An Appointment Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   240
      TabIndex        =   45
      Top             =   0
      Width           =   3135
   End
End
Attribute VB_Name = "frmFavfindings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public AppointmentId As Long
Public DeleteOn As Boolean
Public ApptTypeName As String
Public ApptTypeId As Long
Public ApptPeriod As String
Public lstBox As ListBox
Public NoteOn As Boolean

Private Const MaxSelections As Integer = 42
Private SetBackColor As Long
Private SetForeColor As Long
Private BaseBackColor As Long
Private BaseForeColor As Long
Private MaxItems As Integer
Private CurrentAdmin As Integer
Private AdminList(50) As String
Private DisplayType As String
Private CurrentIndex As Integer
Private TheComplaintCategory As String
Private TotalAppts As Long

Private Sub cmdDone_Click()
Dim i As Integer
ApptTypeId = -1
ApptTypeName = Trim(str(CurrentAdmin))
Unload frmFavfindings
End Sub

Private Sub cmdQuit_Click()
ApptTypeId = -1
ApptTypeName = ""
Unload frmFavfindings
End Sub

Private Sub cmdMore_Click()
CurrentIndex = CurrentIndex + 1
If (CurrentIndex > TotalAppts) Then
    CurrentIndex = 1
End If
Call LoadAppt(DisplayType, False)
End Sub

Private Sub cmdAppt1_Click()
Call SetItem(cmdAppt1)
End Sub

Private Sub cmdAppt2_Click()
Call SetItem(cmdAppt2)
End Sub

Private Sub cmdAppt3_Click()
Call SetItem(cmdAppt3)
End Sub

Private Sub cmdAppt4_Click()
Call SetItem(cmdAppt4)
End Sub

Private Sub cmdAppt5_Click()
Call SetItem(cmdAppt5)
End Sub

Private Sub cmdAppt6_Click()
Call SetItem(cmdAppt6)
End Sub

Private Sub cmdAppt7_Click()
Call SetItem(cmdAppt7)
End Sub

Private Sub cmdAppt8_Click()
Call SetItem(cmdAppt8)
End Sub

Private Sub cmdAppt9_Click()
Call SetItem(cmdAppt9)
End Sub

Private Sub cmdAppt10_Click()
Call SetItem(cmdAppt10)
End Sub

Private Sub cmdAppt11_Click()
Call SetItem(cmdAppt11)
End Sub

Private Sub cmdAppt12_Click()
Call SetItem(cmdAppt12)
End Sub

Private Sub cmdAppt13_Click()
Call SetItem(cmdAppt13)
End Sub

Private Sub cmdAppt14_Click()
Call SetItem(cmdAppt14)
End Sub

Private Sub cmdAppt15_Click()
Call SetItem(cmdAppt15)
End Sub

Private Sub cmdAppt16_Click()
Call SetItem(cmdAppt16)
End Sub

Private Sub cmdAppt17_Click()
Call SetItem(cmdAppt17)
End Sub

Private Sub cmdAppt18_Click()
Call SetItem(cmdAppt18)
End Sub

Private Sub cmdAppt19_Click()
Call SetItem(cmdAppt19)
End Sub

Private Sub cmdAppt20_Click()
Call SetItem(cmdAppt20)
End Sub

Private Sub cmdAppt21_Click()
Call SetItem(cmdAppt21)
End Sub

Private Sub cmdAppt22_Click()
Call SetItem(cmdAppt22)
End Sub

Private Sub cmdAppt23_Click()
Call SetItem(cmdAppt23)
End Sub

Private Sub cmdAppt24_Click()
Call SetItem(cmdAppt24)
End Sub

Private Sub cmdAppt25_Click()
Call SetItem(cmdAppt25)
End Sub

Private Sub cmdAppt26_Click()
Call SetItem(cmdAppt26)
End Sub

Private Sub cmdAppt27_Click()
Call SetItem(cmdAppt27)
End Sub

Private Sub cmdAppt28_Click()
Call SetItem(cmdAppt28)
End Sub

Private Sub cmdAppt29_Click()
Call SetItem(cmdAppt29)
End Sub

Private Sub cmdAppt30_Click()
Call SetItem(cmdAppt30)
End Sub

Private Sub cmdAppt31_Click()
Call SetItem(cmdAppt31)
End Sub

Private Sub cmdAppt32_Click()
Call SetItem(cmdAppt32)
End Sub

Private Sub cmdAppt33_Click()
Call SetItem(cmdAppt33)
End Sub

Private Sub cmdAppt34_Click()
Call SetItem(cmdAppt34)
End Sub

Private Sub cmdAppt35_Click()
Call SetItem(cmdAppt35)
End Sub

Private Sub cmdAppt36_Click()
Call SetItem(cmdAppt36)
End Sub

Private Sub cmdAppt37_Click()
Call SetItem(cmdAppt37)
End Sub

Private Sub cmdAppt38_Click()
Call SetItem(cmdAppt38)
End Sub

Private Sub cmdAppt39_Click()
Call SetItem(cmdAppt39)
End Sub

Private Sub cmdAppt40_Click()
Call SetItem(cmdAppt40)
End Sub

Private Sub cmdAppt41_Click()
Call SetItem(cmdAppt41)
End Sub

Private Sub cmdAppt42_Click()
Call SetItem(cmdAppt42)
End Sub

Private Sub ClearAppt()
cmdAppt1.Text = ""
cmdAppt1.BackColor = BaseBackColor
cmdAppt1.ForeColor = BaseForeColor
cmdAppt1.AlignTextH = fpAlignTextHCenter
cmdAppt1.AlignTextV = fpAlignTextVCenter
cmdAppt1.Visible = False
cmdAppt2.Text = ""
cmdAppt2.BackColor = BaseBackColor
cmdAppt2.ForeColor = BaseForeColor
cmdAppt2.AlignTextH = fpAlignTextHCenter
cmdAppt2.AlignTextV = fpAlignTextVCenter
cmdAppt2.Visible = False
cmdAppt3.Text = ""
cmdAppt3.BackColor = BaseBackColor
cmdAppt3.ForeColor = BaseForeColor
cmdAppt3.AlignTextH = fpAlignTextHCenter
cmdAppt3.AlignTextV = fpAlignTextVCenter
cmdAppt3.Visible = False
cmdAppt4.Text = ""
cmdAppt4.BackColor = BaseBackColor
cmdAppt4.ForeColor = BaseForeColor
cmdAppt4.AlignTextH = fpAlignTextHCenter
cmdAppt4.AlignTextV = fpAlignTextVCenter
cmdAppt4.Visible = False
cmdAppt5.Text = ""
cmdAppt5.BackColor = BaseBackColor
cmdAppt5.ForeColor = BaseForeColor
cmdAppt5.AlignTextH = fpAlignTextHCenter
cmdAppt5.AlignTextV = fpAlignTextVCenter
cmdAppt5.Visible = False
cmdAppt6.Text = ""
cmdAppt6.BackColor = BaseBackColor
cmdAppt6.ForeColor = BaseForeColor
cmdAppt6.AlignTextH = fpAlignTextHCenter
cmdAppt6.AlignTextV = fpAlignTextVCenter
cmdAppt6.Visible = False
cmdAppt7.Text = ""
cmdAppt7.BackColor = BaseBackColor
cmdAppt7.ForeColor = BaseForeColor
cmdAppt7.AlignTextH = fpAlignTextHCenter
cmdAppt7.AlignTextV = fpAlignTextVCenter
cmdAppt7.Visible = False
cmdAppt8.Text = ""
cmdAppt8.BackColor = BaseBackColor
cmdAppt8.ForeColor = BaseForeColor
cmdAppt8.AlignTextH = fpAlignTextHCenter
cmdAppt8.AlignTextV = fpAlignTextVCenter
cmdAppt8.Visible = False
cmdAppt9.Text = ""
cmdAppt9.BackColor = BaseBackColor
cmdAppt9.ForeColor = BaseForeColor
cmdAppt9.AlignTextH = fpAlignTextHCenter
cmdAppt9.AlignTextV = fpAlignTextVCenter
cmdAppt9.Visible = False
cmdAppt10.Text = ""
cmdAppt10.BackColor = BaseBackColor
cmdAppt10.ForeColor = BaseForeColor
cmdAppt10.AlignTextH = fpAlignTextHCenter
cmdAppt10.AlignTextV = fpAlignTextVCenter
cmdAppt10.Visible = False
cmdAppt11.Text = ""
cmdAppt11.BackColor = BaseBackColor
cmdAppt11.ForeColor = BaseForeColor
cmdAppt11.AlignTextH = fpAlignTextHCenter
cmdAppt11.AlignTextV = fpAlignTextVCenter
cmdAppt11.Visible = False
cmdAppt12.Text = ""
cmdAppt12.BackColor = BaseBackColor
cmdAppt12.ForeColor = BaseForeColor
cmdAppt12.AlignTextH = fpAlignTextHCenter
cmdAppt12.AlignTextV = fpAlignTextVCenter
cmdAppt12.Visible = False
cmdAppt13.Text = ""
cmdAppt13.BackColor = BaseBackColor
cmdAppt13.ForeColor = BaseForeColor
cmdAppt13.AlignTextH = fpAlignTextHCenter
cmdAppt13.AlignTextV = fpAlignTextVCenter
cmdAppt13.Visible = False
cmdAppt14.Text = ""
cmdAppt14.BackColor = BaseBackColor
cmdAppt14.ForeColor = BaseForeColor
cmdAppt14.AlignTextH = fpAlignTextHCenter
cmdAppt14.AlignTextV = fpAlignTextVCenter
cmdAppt14.Visible = False
cmdAppt15.Text = ""
cmdAppt15.BackColor = BaseBackColor
cmdAppt15.ForeColor = BaseForeColor
cmdAppt15.AlignTextH = fpAlignTextHCenter
cmdAppt15.AlignTextV = fpAlignTextVCenter
cmdAppt15.Visible = False
cmdAppt16.Text = ""
cmdAppt16.BackColor = BaseBackColor
cmdAppt16.ForeColor = BaseForeColor
cmdAppt16.AlignTextH = fpAlignTextHCenter
cmdAppt16.AlignTextV = fpAlignTextVCenter
cmdAppt16.Visible = False
cmdAppt17.Text = ""
cmdAppt17.BackColor = BaseBackColor
cmdAppt17.ForeColor = BaseForeColor
cmdAppt17.AlignTextH = fpAlignTextHCenter
cmdAppt17.AlignTextV = fpAlignTextVCenter
cmdAppt17.Visible = False
cmdAppt18.Text = ""
cmdAppt18.BackColor = BaseBackColor
cmdAppt18.ForeColor = BaseForeColor
cmdAppt18.AlignTextH = fpAlignTextHCenter
cmdAppt18.AlignTextV = fpAlignTextVCenter
cmdAppt18.Visible = False
cmdAppt19.Text = ""
cmdAppt19.BackColor = BaseBackColor
cmdAppt19.ForeColor = BaseForeColor
cmdAppt19.AlignTextH = fpAlignTextHCenter
cmdAppt19.AlignTextV = fpAlignTextVCenter
cmdAppt19.Visible = False
cmdAppt20.Text = ""
cmdAppt20.BackColor = BaseBackColor
cmdAppt20.ForeColor = BaseForeColor
cmdAppt20.AlignTextH = fpAlignTextHCenter
cmdAppt20.AlignTextV = fpAlignTextVCenter
cmdAppt20.Visible = False
cmdAppt21.Text = ""
cmdAppt21.BackColor = BaseBackColor
cmdAppt21.ForeColor = BaseForeColor
cmdAppt21.AlignTextH = fpAlignTextHCenter
cmdAppt21.AlignTextV = fpAlignTextVCenter
cmdAppt21.Visible = False
cmdAppt22.Text = ""
cmdAppt22.BackColor = BaseBackColor
cmdAppt22.ForeColor = BaseForeColor
cmdAppt22.AlignTextH = fpAlignTextHCenter
cmdAppt22.AlignTextV = fpAlignTextVCenter
cmdAppt22.Visible = False
cmdAppt23.Text = ""
cmdAppt23.BackColor = BaseBackColor
cmdAppt23.ForeColor = BaseForeColor
cmdAppt23.AlignTextH = fpAlignTextHCenter
cmdAppt23.AlignTextV = fpAlignTextVCenter
cmdAppt23.Visible = False
cmdAppt24.Text = ""
cmdAppt24.BackColor = BaseBackColor
cmdAppt24.ForeColor = BaseForeColor
cmdAppt24.AlignTextH = fpAlignTextHCenter
cmdAppt24.AlignTextV = fpAlignTextVCenter
cmdAppt24.Visible = False
cmdAppt25.Text = ""
cmdAppt25.BackColor = BaseBackColor
cmdAppt25.ForeColor = BaseForeColor
cmdAppt25.AlignTextH = fpAlignTextHCenter
cmdAppt25.AlignTextV = fpAlignTextVCenter
cmdAppt25.Visible = False
cmdAppt26.Text = ""
cmdAppt26.BackColor = BaseBackColor
cmdAppt26.ForeColor = BaseForeColor
cmdAppt26.AlignTextH = fpAlignTextHCenter
cmdAppt26.AlignTextV = fpAlignTextVCenter
cmdAppt26.Visible = False
cmdAppt27.Text = ""
cmdAppt27.BackColor = BaseBackColor
cmdAppt27.ForeColor = BaseForeColor
cmdAppt27.AlignTextH = fpAlignTextHCenter
cmdAppt27.AlignTextV = fpAlignTextVCenter
cmdAppt27.Visible = False
cmdAppt28.Text = ""
cmdAppt28.BackColor = BaseBackColor
cmdAppt28.ForeColor = BaseForeColor
cmdAppt28.AlignTextH = fpAlignTextHCenter
cmdAppt28.AlignTextV = fpAlignTextVCenter
cmdAppt28.Visible = False
cmdAppt29.Text = ""
cmdAppt29.BackColor = BaseBackColor
cmdAppt29.ForeColor = BaseForeColor
cmdAppt29.AlignTextH = fpAlignTextHCenter
cmdAppt29.AlignTextV = fpAlignTextVCenter
cmdAppt29.Visible = False
cmdAppt30.Text = ""
cmdAppt30.BackColor = BaseBackColor
cmdAppt30.ForeColor = BaseForeColor
cmdAppt30.AlignTextH = fpAlignTextHCenter
cmdAppt30.AlignTextV = fpAlignTextVCenter
cmdAppt30.Visible = False
cmdAppt31.Text = ""
cmdAppt31.BackColor = BaseBackColor
cmdAppt31.ForeColor = BaseForeColor
cmdAppt31.AlignTextH = fpAlignTextHCenter
cmdAppt31.AlignTextV = fpAlignTextVCenter
cmdAppt31.Visible = False
cmdAppt32.Text = ""
cmdAppt32.BackColor = BaseBackColor
cmdAppt32.ForeColor = BaseForeColor
cmdAppt32.AlignTextH = fpAlignTextHCenter
cmdAppt32.AlignTextV = fpAlignTextVCenter
cmdAppt32.Visible = False
cmdAppt33.Text = ""
cmdAppt33.BackColor = BaseBackColor
cmdAppt33.ForeColor = BaseForeColor
cmdAppt33.AlignTextH = fpAlignTextHCenter
cmdAppt33.AlignTextV = fpAlignTextVCenter
cmdAppt33.Visible = False
cmdAppt34.Text = ""
cmdAppt34.BackColor = BaseBackColor
cmdAppt34.ForeColor = BaseForeColor
cmdAppt34.AlignTextH = fpAlignTextHCenter
cmdAppt34.AlignTextV = fpAlignTextVCenter
cmdAppt34.Visible = False
cmdAppt35.Text = ""
cmdAppt35.BackColor = BaseBackColor
cmdAppt35.ForeColor = BaseForeColor
cmdAppt35.AlignTextH = fpAlignTextHCenter
cmdAppt35.AlignTextV = fpAlignTextVCenter
cmdAppt35.Visible = False
cmdAppt36.Text = ""
cmdAppt36.BackColor = BaseBackColor
cmdAppt36.ForeColor = BaseForeColor
cmdAppt36.AlignTextH = fpAlignTextHCenter
cmdAppt36.AlignTextV = fpAlignTextVCenter
cmdAppt36.Visible = False
cmdAppt37.Text = ""
cmdAppt37.BackColor = BaseBackColor
cmdAppt37.ForeColor = BaseForeColor
cmdAppt37.AlignTextH = fpAlignTextHCenter
cmdAppt37.AlignTextV = fpAlignTextVCenter
cmdAppt37.Visible = False
cmdAppt38.Text = ""
cmdAppt38.BackColor = BaseBackColor
cmdAppt38.ForeColor = BaseForeColor
cmdAppt38.AlignTextH = fpAlignTextHCenter
cmdAppt38.AlignTextV = fpAlignTextVCenter
cmdAppt38.Visible = False
cmdAppt39.Text = ""
cmdAppt39.BackColor = BaseBackColor
cmdAppt39.ForeColor = BaseForeColor
cmdAppt39.AlignTextH = fpAlignTextHCenter
cmdAppt39.AlignTextV = fpAlignTextVCenter
cmdAppt39.Visible = False
cmdAppt40.Text = ""
cmdAppt40.BackColor = BaseBackColor
cmdAppt40.ForeColor = BaseForeColor
cmdAppt40.AlignTextH = fpAlignTextHCenter
cmdAppt40.AlignTextV = fpAlignTextVCenter
cmdAppt40.Visible = False
cmdAppt41.Text = ""
cmdAppt41.BackColor = BaseBackColor
cmdAppt41.ForeColor = BaseForeColor
cmdAppt41.AlignTextH = fpAlignTextHCenter
cmdAppt41.AlignTextV = fpAlignTextVCenter
cmdAppt41.Visible = False
cmdAppt42.Text = ""
cmdAppt42.BackColor = BaseBackColor
cmdAppt42.ForeColor = BaseForeColor
cmdAppt42.AlignTextH = fpAlignTextHCenter
cmdAppt42.AlignTextV = fpAlignTextVCenter
cmdAppt42.Visible = False
cmdMore.Text = "More"
cmdMore.Visible = False
End Sub

Private Sub SetItemOn(AButton As fpBtn)
AButton.BackColor = SetBackColor
AButton.ForeColor = SetForeColor
End Sub

Private Sub SetItem(AButton As fpBtn)
Dim i As Integer
Dim j As Integer
Dim p As Integer
Dim ITemp As String
If (AButton.BackColor = BaseBackColor) Then
    If (MaxItems > 0) Then
        If (CurrentAdmin + 1 > MaxItems) Then
            frmEventMsgs.Header = "Too Many Selections"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
        End If
    End If
    AButton.BackColor = SetBackColor
    AButton.ForeColor = SetForeColor
    CurrentAdmin = CurrentAdmin + 1
    AdminList(CurrentAdmin) = AButton.Text
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseForeColor
    For i = 1 To CurrentAdmin
        j = InStrPS(AdminList(i), "(")
        If (j > 0) Then
            ITemp = Left(AdminList(i), j - 1)
        Else
            ITemp = AdminList(i)
        End If
        If (UCase(Trim(AButton.Text)) = UCase(Trim(ITemp))) Then
            For p = i To CurrentAdmin - 1
                AdminList(p) = AdminList(p + 1)
            Next p
            AdminList(CurrentAdmin) = ""
        End If
    Next i
    CurrentAdmin = CurrentAdmin - 1
    If (CurrentAdmin < 1) Then
        CurrentAdmin = 1
    End If
End If
End Sub

Public Function LoadAppt(TheType As String, Init As Boolean) As Boolean
Dim k As Integer, i As Integer, z As Integer
Dim ButtonCnt As Integer
Dim DisplayText As String
Dim RetCode As PracticeCodes
LoadAppt = False
MaxItems = 0
ApptPeriod = ""
DisplayType = TheType
Call ClearAppt
cmdMore.Visible = False
cmdQuit.Visible = True
cmdDone.Visible = True
If (TheType = "!") Or (TheType = "'") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "CLINICALTEMPLATES"
    RetCode.ReferenceCode = ""
    If (TheType = "!") Then
        RetCode.ReferenceAlternateCode = "S"
        lblField.Caption = "Post Status Templates"
    Else
        RetCode.ReferenceAlternateCode = "F"
        lblField.Caption = "Favorite Findings Templates"
    End If
    TotalAppts = RetCode.FindAlternateCode
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetCode.SelectCode(i)) And (ButtonCnt < 43) And (i <= TotalAppts)
            DisplayText = Trim(RetCode.ReferenceCode)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = "0"
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = "0"
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = "0"
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = "0"
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = "0"
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = "0"
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = "0"
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = "0"
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = "0"
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = "0"
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = "0"
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = "0"
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = "0"
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = "0"
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = "0"
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = "0"
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = "0"
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = "0"
                cmdAppt18.Visible = True
            ElseIf (ButtonCnt = 19) Then
                cmdAppt19.Text = DisplayText
                cmdAppt19.Tag = "0"
                cmdAppt19.Visible = True
            ElseIf (ButtonCnt = 20) Then
                cmdAppt20.Text = DisplayText
                cmdAppt20.Tag = "0"
                cmdAppt20.Visible = True
            ElseIf (ButtonCnt = 21) Then
                cmdAppt21.Text = DisplayText
                cmdAppt21.Tag = "0"
                cmdAppt21.Visible = True
            ElseIf (ButtonCnt = 22) Then
                cmdAppt22.Text = DisplayText
                cmdAppt22.Tag = "0"
                cmdAppt22.Visible = True
            ElseIf (ButtonCnt = 23) Then
                cmdAppt23.Text = DisplayText
                cmdAppt23.Tag = "0"
                cmdAppt23.Visible = True
            ElseIf (ButtonCnt = 24) Then
                cmdAppt24.Text = DisplayText
                cmdAppt24.Tag = "0"
                cmdAppt24.Visible = True
            ElseIf (ButtonCnt = 25) Then
                cmdAppt25.Text = DisplayText
                cmdAppt25.Tag = "0"
                cmdAppt25.Visible = True
            ElseIf (ButtonCnt = 26) Then
                cmdAppt26.Text = DisplayText
                cmdAppt26.Tag = "0"
                cmdAppt26.Visible = True
            ElseIf (ButtonCnt = 27) Then
                cmdAppt27.Text = DisplayText
                cmdAppt27.Tag = "0"
                cmdAppt27.Visible = True
            ElseIf (ButtonCnt = 28) Then
                cmdAppt28.Text = DisplayText
                cmdAppt28.Tag = "0"
                cmdAppt28.Visible = True
            ElseIf (ButtonCnt = 29) Then
                cmdAppt29.Text = DisplayText
                cmdAppt29.Tag = "0"
                cmdAppt29.Visible = True
            ElseIf (ButtonCnt = 30) Then
                cmdAppt30.Text = DisplayText
                cmdAppt30.Tag = "0"
                cmdAppt30.Visible = True
            ElseIf (ButtonCnt = 31) Then
                cmdAppt31.Text = DisplayText
                cmdAppt31.Tag = "0"
                cmdAppt31.Visible = True
            ElseIf (ButtonCnt = 32) Then
                cmdAppt32.Text = DisplayText
                cmdAppt32.Tag = "0"
                cmdAppt32.Visible = True
            ElseIf (ButtonCnt = 33) Then
                cmdAppt33.Text = DisplayText
                cmdAppt33.Tag = "0"
                cmdAppt33.Visible = True
            ElseIf (ButtonCnt = 34) Then
                cmdAppt34.Text = DisplayText
                cmdAppt34.Tag = "0"
                cmdAppt34.Visible = True
            ElseIf (ButtonCnt = 35) Then
                cmdAppt35.Text = DisplayText
                cmdAppt35.Tag = "0"
                cmdAppt35.Visible = True
            ElseIf (ButtonCnt = 36) Then
                cmdAppt36.Text = DisplayText
                cmdAppt36.Tag = "0"
                cmdAppt36.Visible = True
            ElseIf (ButtonCnt = 37) Then
                cmdAppt37.Text = DisplayText
                cmdAppt37.Tag = "0"
                cmdAppt37.Visible = True
            ElseIf (ButtonCnt = 38) Then
                cmdAppt38.Text = DisplayText
                cmdAppt38.Tag = "0"
                cmdAppt38.Visible = True
            ElseIf (ButtonCnt = 39) Then
                cmdAppt39.Text = DisplayText
                cmdAppt39.Tag = "0"
                cmdAppt39.Visible = True
            ElseIf (ButtonCnt = 40) Then
                cmdAppt40.Text = DisplayText
                cmdAppt40.Tag = "0"
                cmdAppt40.Visible = True
            ElseIf (ButtonCnt = 41) Then
                cmdAppt41.Text = DisplayText
                cmdAppt41.Tag = "0"
                cmdAppt41.Visible = True
            ElseIf (ButtonCnt = 42) Then
                cmdAppt42.Text = DisplayText
                cmdAppt42.Tag = "0"
                cmdAppt42.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetCode = Nothing
    End If
End If
If (CurrentIndex <= TotalAppts) And (TotalAppts > 42) Then
    cmdMore.Text = "More"
    cmdMore.Visible = True
Else
    cmdMore.Text = "Beginning of List"
    cmdMore.Visible = True
End If
If (TotalAppts < 42) Then
    cmdMore.Visible = False
End If
End Function

Private Sub Form_Load()
Erase AdminList
CurrentAdmin = 0
CurrentIndex = 1
BaseBackColor = cmdAppt1.BackColor
BaseForeColor = cmdAppt1.ForeColor
SetBackColor = 14745312
SetForeColor = 0
End Sub

Public Function GetAdminItem(Ref As Integer, Item As String) As Boolean
GetAdminItem = False
Item = ""
If (Ref > 0) And (Ref < MaxSelections) Then
    Item = Trim(AdminList(Ref))
    If (Item <> "") Then
        GetAdminItem = True
    End If
End If
End Function
Public Function FrmClose()
 Call cmdQuit_Click
 Unload Me
End Function
