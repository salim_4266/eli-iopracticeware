VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmViewVendors 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12105
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "ViewVendors.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12105
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkXRef 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Exclude Referring Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   8400
      TabIndex        =   45
      Top             =   4320
      Width           =   3015
   End
   Begin VB.TextBox txtTaxo 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8400
      MaxLength       =   16
      TabIndex        =   16
      Top             =   3840
      Width           =   3375
   End
   Begin VB.TextBox TaxId 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8400
      MaxLength       =   32
      TabIndex        =   44
      Top             =   3360
      Width           =   3375
   End
   Begin VB.TextBox txtFirm 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   64
      TabIndex        =   15
      Top             =   3840
      Width           =   4695
   End
   Begin VB.TextBox txtSal 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   25
      TabIndex        =   13
      Top             =   2880
      Width           =   3015
   End
   Begin VB.TextBox txtTitle 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   8
      TabIndex        =   2
      Top             =   960
      Width           =   1935
   End
   Begin VB.TextBox txtMI 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   9960
      MaxLength       =   1
      TabIndex        =   5
      Top             =   1440
      Width           =   495
   End
   Begin VB.TextBox txtFName 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   7080
      MaxLength       =   16
      TabIndex        =   4
      Top             =   1440
      Width           =   2055
   End
   Begin VB.TextBox txtLName 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   20
      TabIndex        =   3
      Top             =   1440
      Width           =   3135
   End
   Begin VB.TextBox txtUpin 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   32
      TabIndex        =   17
      Top             =   4320
      Width           =   4695
   End
   Begin VB.ListBox lstAff 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1260
      ItemData        =   "ViewVendors.frx":36C9
      Left            =   2280
      List            =   "ViewVendors.frx":36CB
      Sorted          =   -1  'True
      TabIndex        =   20
      Top             =   5760
      Width           =   4575
   End
   Begin VB.TextBox txtOtherOffice2 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   80
      TabIndex        =   19
      Top             =   5280
      Width           =   8055
   End
   Begin VB.TextBox txtOtherOffice1 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   80
      TabIndex        =   18
      Top             =   4800
      Width           =   8055
   End
   Begin VB.TextBox Fax 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   5640
      MaxLength       =   16
      TabIndex        =   12
      Top             =   2880
      Width           =   1695
   End
   Begin VB.TextBox VName 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   64
      TabIndex        =   1
      Top             =   960
      Width           =   4935
   End
   Begin VB.TextBox State 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   6720
      MaxLength       =   2
      TabIndex        =   9
      Top             =   2400
      Width           =   615
   End
   Begin VB.TextBox Zip 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   10
      TabIndex        =   10
      Top             =   2400
      Width           =   1695
   End
   Begin VB.TextBox Address 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   50
      TabIndex        =   6
      Top             =   1920
      Width           =   4935
   End
   Begin VB.TextBox City 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   30
      TabIndex        =   8
      Top             =   2400
      Width           =   3135
   End
   Begin VB.TextBox Phone 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   16
      TabIndex        =   11
      Top             =   2880
      Width           =   2415
   End
   Begin VB.TextBox Suite 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   8760
      MaxLength       =   10
      TabIndex        =   7
      Top             =   1920
      Width           =   1695
   End
   Begin VB.TextBox Email 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   64
      TabIndex        =   14
      Top             =   3360
      Width           =   3495
   End
   Begin VB.TextBox Specialty 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   2280
      MaxLength       =   64
      TabIndex        =   0
      Top             =   480
      Width           =   6975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   360
      TabIndex        =   21
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ViewVendors.frx":36CD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   990
      Left            =   10080
      TabIndex        =   46
      Top             =   7440
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ViewVendors.frx":38AC
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Taxonomy"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Index           =   1
      Left            =   7080
      TabIndex        =   43
      Top             =   3840
      Width           =   1260
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Firm Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   480
      TabIndex        =   42
      Top             =   3840
      Width           =   1575
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Salutation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   7440
      TabIndex        =   41
      Top             =   2880
      Width           =   1215
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   7800
      TabIndex        =   40
      Top             =   960
      Width           =   855
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "MI"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   9360
      TabIndex        =   39
      Top             =   1440
      Width           =   495
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "First Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   5640
      TabIndex        =   38
      Top             =   1440
      Width           =   1335
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Last Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   720
      TabIndex        =   37
      Top             =   1440
      Width           =   1335
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "UPIN"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   915
      TabIndex        =   36
      Top             =   4320
      Width           =   1140
   End
   Begin VB.Label lblAffiliations 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Affiliations"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   390
      Left            =   600
      TabIndex        =   35
      Top             =   5760
      Width           =   1455
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Other Office"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   240
      TabIndex        =   34
      Top             =   5280
      Width           =   1815
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Other Office"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   240
      TabIndex        =   33
      Top             =   4800
      Width           =   1815
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Fax"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   4800
      TabIndex        =   32
      Top             =   2880
      Width           =   735
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Display Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   120
      TabIndex        =   31
      Top             =   960
      Width           =   1935
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Tax Id"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Index           =   0
      Left            =   7200
      TabIndex        =   30
      Top             =   3360
      Width           =   1140
   End
   Begin VB.Label Label19 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "State"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   5640
      TabIndex        =   29
      Top             =   2400
      Width           =   1020
   End
   Begin VB.Label Label23 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "City"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   960
      TabIndex        =   28
      Top             =   2400
      Width           =   1095
   End
   Begin VB.Label Label24 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Zip"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   8100
      TabIndex        =   27
      Top             =   2400
      Width           =   555
   End
   Begin VB.Label Label26 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   960
      TabIndex        =   26
      Top             =   1920
      Width           =   1095
   End
   Begin VB.Label Label27 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   960
      TabIndex        =   25
      Top             =   2880
      Width           =   1095
   End
   Begin VB.Label Label25 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Suite"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   7680
      TabIndex        =   24
      Top             =   1920
      Width           =   975
   End
   Begin VB.Label Label28 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "E-mail"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   960
      TabIndex        =   23
      Top             =   3360
      Width           =   1125
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Specialty"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   405
      Left            =   720
      TabIndex        =   22
      Top             =   480
      Width           =   1290
   End
End
Attribute VB_Name = "frmViewVendors"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdHome_Click()
Unload frmViewVendors
End Sub

Private Sub cmdApply_Click()
Unload frmViewVendors
End Sub

Public Function VendorLoadDisplay(VendorId As Long) As Boolean
Dim ATaxo As String
Dim AName As String, ALName As String, AFName As String
Dim AMI As String, ATitle As String, AVType As String
Dim AAddr As String, ASuite As String, ACity As String
Dim AState As String, AZip As String, APhone As String
Dim AFax As String, ATaxId As String, AUpin As String
Dim AEmail As String, ASpec As String, AOther1 As String
Dim AOther2 As String, ASal As String, ADisplay As String
Dim XRef As Boolean
VendorLoadDisplay = True
Call ApplLoadVendor(VendorId, AName, ALName, AFName, AMI, _
                    ATitle, AVType, AAddr, ASuite, ACity, _
                    AState, AZip, APhone, AFax, ATaxId, AUpin, _
                    AEmail, ASpec, AOther1, AOther2, ASal, _
                    ADisplay, ATaxo, XRef)
VName.Text = AName
txtLName.Text = ALName
txtFName.Text = AFName
txtMI.Text = AMI
txtTitle.Text = ATitle
Address.Text = AAddr
Suite.Text = ASuite
City.Text = ACity
State.Text = AState
Zip.Text = AZip
Phone.Text = APhone
Fax.Text = AFax
TaxId.Text = ATaxId
txtUpin.Text = AUpin
txtFirm.Text = ADisplay
Email.Text = AEmail
Specialty.Text = ASpec
txtOtherOffice1.Text = AOther1
txtOtherOffice2.Text = AOther2
txtSal.Text = ASal
txtTaxo.Text = ATaxo
chkXRef.Value = 0
If (XRef) Then
    chkXRef.Value = 1
End If
End Function

Public Function ApplLoadVendor(VendorId As Long, TheName As String, TheLName As String, TheFName As String, TheMI As String, TheTitle As String, TheVType As String, TheAddress As String, TheSuite As String, TheCity As String, TheState As String, TheZip As String, ThePhone As String, TheFax As String, TheTaxId As String, TheUpin As String, TheEmail As String, TheSpec As String, TheOther1 As String, TheOther2 As String, Sal As String, VDisplay As String, VTaxo As String, VRef As Boolean) As Boolean
Dim RetrieveVendor As PracticeVendors
ApplLoadVendor = True
If (VendorId > 0) Then
    Set RetrieveVendor = New PracticeVendors
    RetrieveVendor.VendorId = VendorId
    Call RetrieveVendor.RetrieveVendor
    TheName = RetrieveVendor.VendorName
    TheLName = RetrieveVendor.VendorLastName
    TheFName = RetrieveVendor.VendorFirstName
    TheMI = RetrieveVendor.VendorMI
    TheTitle = RetrieveVendor.VendorTitle
    TheVType = RetrieveVendor.VendorType
    TheAddress = RetrieveVendor.VendorAddress
    TheSuite = RetrieveVendor.VendorSuite
    TheCity = RetrieveVendor.VendorCity
    TheState = RetrieveVendor.VendorState
    TheZip = RetrieveVendor.VendorZip
    ThePhone = RetrieveVendor.VendorPhone
    TheFax = RetrieveVendor.VendorFax
    TheTaxId = RetrieveVendor.VendorTaxId
    TheUpin = RetrieveVendor.VendorUpin
    TheEmail = RetrieveVendor.VendorEmail
    TheSpec = RetrieveVendor.VendorSpecialty
    TheOther1 = RetrieveVendor.VendorOtherOffice1
    TheOther2 = RetrieveVendor.VendorOtherOffice2
    Sal = RetrieveVendor.VendorSalutation
    VDisplay = RetrieveVendor.VendorFirmName
    VTaxo = RetrieveVendor.VendorTaxonomy
    VRef = RetrieveVendor.VendorExcludeRefDr
    Call ApplLoadAffiliations(VendorId, "V")
    Set RetrieveVendor = Nothing
End If
End Function

Public Function ApplLoadAffiliations(ResId As Long, IType As String) As Boolean
Dim i As Integer
Dim DisplayText As String
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
Dim RetAff As PracticeAffiliations
ApplLoadAffiliations = True
If (ResId > 0) Then
    Set RetAff = New PracticeAffiliations
    lstAff.Clear
    RetAff.AffiliationResourceId = ResId
    RetAff.AffiliationResourceType = IType
    RetAff.AffiliationPin = ""
    RetAff.AffiliationLocation = 0
    RetAff.AffiliationPlanId = 0
    If (RetAff.FindAffiliation > 0) Then
        i = 1
        While (RetAff.SelectAffiliation(i))
            If (RetAff.AffiliationPlanId > 0) Then
                Set RetPatientFinancialService = New PatientFinancialService
                RetPatientFinancialService.InsurerId = RetAff.AffiliationPlanId
                Set RS = RetPatientFinancialService.RetrieveInsurer
                If (Not RS Is Nothing And RS.RecordCount > 0) Then
                    DisplayText = Space(75)
                    Mid(DisplayText, 1, 25) = Trim(RS("Name"))
                    Mid(DisplayText, 39, 25) = Trim(RetAff.AffiliationPin)
                    DisplayText = DisplayText + Trim(str(RetAff.AffiliationId))
                    lstAff.AddItem DisplayText
                End If
                Set RetPatientFinancialService = Nothing
            End If
            i = i + 1
        Wend
    End If
    Set RetAff = Nothing
End If
End Function
