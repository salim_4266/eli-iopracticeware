VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmSendMessage 
   Appearance      =   0  'Flat
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "SendMessage.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg6 
      Height          =   855
      Left            =   4080
      TabIndex        =   26
      Top             =   5760
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":36C9
   End
   Begin VB.ListBox lstTests 
      Height          =   450
      Left            =   8640
      TabIndex        =   24
      Top             =   240
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstMsgs 
      Height          =   450
      Left            =   7680
      TabIndex        =   23
      Top             =   240
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstStaff 
      Height          =   450
      Left            =   6720
      TabIndex        =   22
      Top             =   240
      Visible         =   0   'False
      Width           =   735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest1 
      Height          =   855
      Left            =   8040
      TabIndex        =   14
      Top             =   960
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":38A4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg1 
      Height          =   855
      Left            =   4080
      TabIndex        =   0
      Top             =   960
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":3A7F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   9840
      TabIndex        =   1
      Top             =   7800
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":3C5A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff1 
      Height          =   855
      Left            =   240
      TabIndex        =   2
      Top             =   960
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":3E8D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff2 
      Height          =   855
      Left            =   240
      TabIndex        =   3
      Top             =   1920
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":4068
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff3 
      Height          =   855
      Left            =   240
      TabIndex        =   4
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":4243
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff4 
      Height          =   855
      Left            =   240
      TabIndex        =   5
      Top             =   3840
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":441E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff6 
      Height          =   855
      Left            =   240
      TabIndex        =   6
      Top             =   5760
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":45F9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStaff5 
      Height          =   855
      Left            =   240
      TabIndex        =   7
      Top             =   4800
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":47D4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreMsgs 
      Height          =   855
      Left            =   4080
      TabIndex        =   8
      Top             =   6720
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":49AF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreStaff 
      Height          =   855
      Left            =   240
      TabIndex        =   9
      Top             =   6720
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":4B97
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg2 
      Height          =   855
      Left            =   4080
      TabIndex        =   10
      Top             =   1920
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":4D7C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg3 
      Height          =   855
      Left            =   4080
      TabIndex        =   11
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":4F57
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg4 
      Height          =   855
      Left            =   4080
      TabIndex        =   12
      Top             =   3840
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":5132
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest2 
      Height          =   855
      Left            =   8040
      TabIndex        =   15
      Top             =   1920
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":530D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest3 
      Height          =   855
      Left            =   8040
      TabIndex        =   16
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":54E8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest4 
      Height          =   855
      Left            =   8040
      TabIndex        =   17
      Top             =   3840
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":56C3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest6 
      Height          =   855
      Left            =   8040
      TabIndex        =   18
      Top             =   5760
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":589E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTest5 
      Height          =   855
      Left            =   8040
      TabIndex        =   19
      Top             =   4800
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":5A79
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreTests 
      Height          =   855
      Left            =   8040
      TabIndex        =   20
      Top             =   6720
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":5C54
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   975
      Left            =   240
      TabIndex        =   21
      Top             =   7800
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":5E39
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMsg5 
      Height          =   855
      Left            =   4080
      TabIndex        =   25
      Top             =   4800
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SendMessage.frx":606C
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Message to a co-worker"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   240
      TabIndex        =   13
      Top             =   240
      Width           =   3405
   End
End
Attribute VB_Name = "frmSendMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long

Private ReceiverId As Long
Private SenderId As Long
Private TestId As String
Private MessageId As String

Private TotalStaff As Integer
Private CurrentStaffIndex As Integer
Private TotalTests As Integer
Private CurrentTestsIndex As Integer
Private TotalMessages As Integer
Private CurrentMsgsIndex As Integer

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub ResetButtons(ClearType As String)
If (ClearType = "S") Then
    cmdStaff1.BackColor = BaseBackColor
    cmdStaff1.ForeColor = BaseTextColor
    cmdStaff2.BackColor = BaseBackColor
    cmdStaff2.ForeColor = BaseTextColor
    cmdStaff3.BackColor = BaseBackColor
    cmdStaff3.ForeColor = BaseTextColor
    cmdStaff4.BackColor = BaseBackColor
    cmdStaff4.ForeColor = BaseTextColor
    cmdStaff5.BackColor = BaseBackColor
    cmdStaff5.ForeColor = BaseTextColor
    cmdStaff6.BackColor = BaseBackColor
    cmdStaff6.ForeColor = BaseTextColor
ElseIf (ClearType = "M") Then
    cmdMsg1.BackColor = BaseBackColor
    cmdMsg1.ForeColor = BaseTextColor
    cmdMsg2.BackColor = BaseBackColor
    cmdMsg2.ForeColor = BaseTextColor
    cmdMsg3.BackColor = BaseBackColor
    cmdMsg3.ForeColor = BaseTextColor
    cmdMsg4.BackColor = BaseBackColor
    cmdMsg4.ForeColor = BaseTextColor
    cmdMsg5.BackColor = BaseBackColor
    cmdMsg5.ForeColor = BaseTextColor
    cmdMsg6.BackColor = BaseBackColor
    cmdMsg6.ForeColor = BaseTextColor
ElseIf (ClearType = "T") Then
    cmdTest1.BackColor = BaseBackColor
    cmdTest1.ForeColor = BaseTextColor
    cmdTest2.BackColor = BaseBackColor
    cmdTest2.ForeColor = BaseTextColor
    cmdTest3.BackColor = BaseBackColor
    cmdTest3.ForeColor = BaseTextColor
    cmdTest4.BackColor = BaseBackColor
    cmdTest4.ForeColor = BaseTextColor
    cmdTest5.BackColor = BaseBackColor
    cmdTest5.ForeColor = BaseTextColor
    cmdTest6.BackColor = BaseBackColor
    cmdTest6.ForeColor = BaseTextColor
End If
End Sub

Private Sub ClearButtons(ClearType As String)
If (ClearType = "S") Then
    cmdStaff1.Text = ""
    cmdStaff1.Visible = False
    cmdStaff2.Text = ""
    cmdStaff2.Visible = False
    cmdStaff3.Text = ""
    cmdStaff3.Visible = False
    cmdStaff4.Text = ""
    cmdStaff4.Visible = False
    cmdStaff5.Text = ""
    cmdStaff5.Visible = False
    cmdStaff6.Text = ""
    cmdStaff6.Visible = False
    cmdMoreStaff.Text = "More Staff"
    cmdMoreStaff.Visible = False
ElseIf (ClearType = "M") Then
    cmdMsg1.Text = ""
    cmdMsg1.Visible = False
    cmdMsg2.Text = ""
    cmdMsg2.Visible = False
    cmdMsg3.Text = ""
    cmdMsg3.Visible = False
    cmdMsg4.Text = ""
    cmdMsg4.Visible = False
    cmdMsg5.Text = ""
    cmdMsg5.Visible = False
    cmdMsg6.Text = ""
    cmdMsg6.Visible = False
    cmdMoreMsgs.Text = "More Messages"
    cmdMoreMsgs.Visible = False
ElseIf (ClearType = "T") Then
    cmdTest1.Text = ""
    cmdTest1.Visible = False
    cmdTest2.Text = ""
    cmdTest2.Visible = False
    cmdTest3.Text = ""
    cmdTest3.Visible = False
    cmdTest4.Text = ""
    cmdTest4.Visible = False
    cmdTest5.Text = ""
    cmdTest5.Visible = False
    cmdTest6.Text = ""
    cmdTest6.Visible = False
    cmdMoreTests.Text = "More Tests"
    cmdMoreTests.Visible = False
End If
End Sub

Private Sub cmdQuit_Click()
Unload frmSendMessage
End Sub

Private Sub cmdDone_Click()
If (Trim(MessageId) = "") Then
    frmEventMsgs.Header = "Select a Message"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
ElseIf (ReceiverId < 1) Then
    frmEventMsgs.Header = "Select a Receiver"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
If Not (ApplyMessage(MessageId, SenderId, ReceiverId, PatientId, 0, TestId)) Then
    frmEventMsgs.Header = "Message not sent"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Unload frmSendMessage
End Sub

Private Sub cmdStaff1_Click()
Call SelectStaff(cmdStaff1, 1)
End Sub

Private Sub cmdStaff2_Click()
Call SelectStaff(cmdStaff2, 2)
End Sub

Private Sub cmdStaff3_Click()
Call SelectStaff(cmdStaff3, 3)
End Sub

Private Sub cmdStaff4_Click()
Call SelectStaff(cmdStaff4, 4)
End Sub

Private Sub cmdStaff5_Click()
Call SelectStaff(cmdStaff5, 5)
End Sub

Private Sub cmdStaff6_Click()
Call SelectStaff(cmdStaff6, 6)
End Sub

Private Sub cmdMoreStaff_Click()
If (CurrentStaffIndex > TotalStaff) Then
    CurrentStaffIndex = 1
End If
Call LoadStaff
End Sub

Private Sub SelectStaff(AButton As fpBtn, Ref As Integer)
If (AButton.BackColor = BaseBackColor) Then
    Call ResetButtons("S")
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    ReceiverId = Val(Trim(AButton.Tag))
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    ReceiverId = 0
End If
End Sub

Private Sub cmdTest1_Click()
Call SelectTest(cmdTest1, 1)
End Sub

Private Sub cmdTest2_Click()
Call SelectTest(cmdTest2, 2)
End Sub

Private Sub cmdTest3_Click()
Call SelectTest(cmdTest3, 3)
End Sub

Private Sub cmdTest4_Click()
Call SelectTest(cmdTest4, 4)
End Sub

Private Sub cmdTest5_Click()
Call SelectTest(cmdTest5, 5)
End Sub

Private Sub cmdTest6_Click()
Call SelectTest(cmdTest6, 6)
End Sub

Private Sub cmdMoreTests_Click()
If (CurrentTestsIndex > TotalTests) Then
    CurrentTestsIndex = 1
End If
Call LoadTests
End Sub

Private Sub SelectTest(AButton As fpBtn, Ref As Integer)
If (AButton.BackColor = BaseBackColor) Then
    Call ResetButtons("T")
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    TestId = Trim(AButton.Text)
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    TestId = ""
End If
End Sub

Private Sub cmdMsg1_Click()
Call SelectMessage(cmdMsg1, 1)
End Sub

Private Sub cmdMsg2_Click()
Call SelectMessage(cmdMsg2, 2)
End Sub

Private Sub cmdMsg3_Click()
Call SelectMessage(cmdMsg3, 3)
End Sub

Private Sub cmdMsg4_Click()
Call SelectMessage(cmdMsg4, 4)
End Sub

Private Sub cmdMsg5_Click()
Call SelectMessage(cmdMsg5, 5)
End Sub

Private Sub cmdMsg6_Click()
Call SelectMessage(cmdMsg6, 6)
End Sub

Private Sub cmdMoreMsgs_Click()
If (CurrentMsgsIndex > TotalMessages) Then
    CurrentMsgsIndex = 1
End If
Call LoadMessages
End Sub

Private Sub SelectMessage(AButton As fpBtn, Ref As Integer)
If (AButton.BackColor = BaseBackColor) Then
    Call ResetButtons("M")
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    MessageId = Trim(AButton.Text)
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    MessageId = ""
End If
End Sub

Public Function LoadNotes() As Boolean
Dim i As Integer
LoadNotes = False
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdStaff1.BackColor
BaseTextColor = cmdStaff1.ForeColor
TestId = ""
MessageId = ""
ReceiverId = 0
CurrentTestsIndex = 1
CurrentMsgsIndex = 1
CurrentStaffIndex = 1
SenderId = UserLogin.iId
Call LoadStaff
Call LoadMessages
Call LoadTests
LoadNotes = True
End Function

Private Function LoadStaff() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
LoadStaff = False
Call ClearButtons("S")
ButtonCnt = 0
Call ApplLoadResource(lstStaff, "S", False, False, False)
TotalStaff = lstStaff.ListCount
If (TotalStaff > 0) Then
    i = CurrentStaffIndex
    ButtonCnt = 1
    While (i <= TotalStaff) And (ButtonCnt < 7)
        If (Val(Mid(lstStaff.List(i - 1), 56, 5)) <> SenderId) Then
            If (ButtonCnt = 1) Then
                cmdStaff1.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff1.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdStaff2.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff2.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdStaff3.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff3.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdStaff4.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff4.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdStaff5.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff5.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdStaff6.Text = Trim(Left(lstStaff.List(i - 1), 30))
                cmdStaff6.Tag = Mid(lstStaff.List(i - 1), 56, 5)
                cmdStaff6.Visible = True
            End If
            ButtonCnt = ButtonCnt + 1
        End If
        i = i + 1
        CurrentStaffIndex = i
    Wend
End If
If (ButtonCnt > 0) Then
    LoadStaff = True
End If
If (TotalStaff < 7) Then
    cmdMoreStaff.Visible = False
Else
    cmdMoreStaff.Text = "More Staff"
    cmdMoreStaff.Visible = True
End If
End Function

Private Function LoadMessages() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
LoadMessages = False
Call ClearButtons("M")
Call ApplLoadCodes(lstMsgs, "Message", False)
TotalMessages = lstMsgs.ListCount
If (TotalMessages > 0) Then
    i = CurrentMsgsIndex
    ButtonCnt = 1
    While (i <= TotalMessages) And (ButtonCnt < 7)
        If (ButtonCnt = 1) Then
            cmdMsg1.Text = Trim(lstMsgs.List(i - 1))
            cmdMsg1.Tag = "1"
            cmdMsg1.Visible = True
        ElseIf (ButtonCnt = 2) Then
            cmdMsg2.Text = Trim(lstMsgs.List(i - 1))
            cmdMsg2.Tag = "2"
            cmdMsg2.Visible = True
        ElseIf (ButtonCnt = 3) Then
            cmdMsg3.Text = Trim(lstMsgs.List(i - 1))
            cmdMsg3.Tag = "3"
            cmdMsg3.Visible = True
        ElseIf (ButtonCnt = 4) Then
            cmdMsg4.Text = Trim(lstMsgs.List(i - 1))
            cmdMsg4.Tag = "4"
            cmdMsg4.Visible = True
        ElseIf (ButtonCnt = 5) Then
            cmdMsg5.Text = Trim(lstMsgs.List(i - 1))
            cmdMsg5.Tag = "5"
            cmdMsg5.Visible = True
        ElseIf (ButtonCnt = 6) Then
            cmdMsg6.Text = Trim(lstMsgs.List(i - 1))
            cmdMsg6.Tag = "6"
            cmdMsg6.Visible = True
        End If
        i = i + 1
        ButtonCnt = ButtonCnt + 1
        CurrentMsgsIndex = i
    Wend
End If
If (ButtonCnt > 0) Then
    LoadMessages = True
End If
If (TotalMessages < 7) Then
    cmdMoreMsgs.Visible = False
Else
    cmdMoreMsgs.Text = "More"
    cmdMoreMsgs.Visible = True
End If
End Function

Private Function LoadTests() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim RetrieveClass As DynamicClass
LoadTests = False
Call ClearButtons("T")
Set RetrieveClass = New DynamicClass
RetrieveClass.QuestionSet = "First Visit"
RetrieveClass.QuestionParty = "T"
TotalTests = RetrieveClass.FindClassFormsbyName
If (TotalTests > 0) Then
    i = CurrentTestsIndex
    ButtonCnt = 1
    While (RetrieveClass.SelectClassForm(i)) And (ButtonCnt < 7)
        If (ButtonCnt = 1) Then
            cmdTest1.Text = RetrieveClass.Question
            cmdTest1.Tag = RetrieveClass.Question
            cmdTest1.Visible = True
        ElseIf (ButtonCnt = 2) Then
            cmdTest2.Text = RetrieveClass.Question
            cmdTest2.Tag = RetrieveClass.Question
            cmdTest2.Visible = True
        ElseIf (ButtonCnt = 3) Then
            cmdTest3.Text = RetrieveClass.Question
            cmdTest3.Tag = RetrieveClass.Question
            cmdTest3.Visible = True
        ElseIf (ButtonCnt = 4) Then
            cmdTest4.Text = RetrieveClass.Question
            cmdTest4.Tag = RetrieveClass.Question
            cmdTest4.Visible = True
        ElseIf (ButtonCnt = 5) Then
            cmdTest5.Text = RetrieveClass.Question
            cmdTest5.Tag = RetrieveClass.Question
            cmdTest5.Visible = True
        ElseIf (ButtonCnt = 6) Then
            cmdTest6.Text = RetrieveClass.Question
            cmdTest6.Tag = RetrieveClass.Question
            cmdTest6.Visible = True
        End If
        i = i + 1
        ButtonCnt = ButtonCnt + 1
        CurrentTestsIndex = i
    Wend
End If
If (ButtonCnt > 0) Then
    LoadTests = True
End If
If (TotalTests < 7) Then
    cmdMoreTests.Visible = False
Else
    cmdMoreTests.Text = "More Tests"
    cmdMoreTests.Visible = True
End If
Set RetrieveClass = Nothing
End Function

Private Function ApplyMessage(TheMessage As String, TheSenderId As Long, TheReceiverId As Long, ThePatientId As Long, TheRoomId As Long, TestText As String) As Boolean
Dim k As Integer
Dim NewTime As String, NewMsg As String
Dim NewDate As String, TheDate As String
Dim PatientName As String, RoomName As String
Dim SenderName As String, ReceiverName As String
ApplyMessage = False
Call FormatTodaysDate(NewDate, False)
NewDate = Mid(NewDate, 7, 4) + Mid(NewDate, 1, 2) + Mid(NewDate, 4, 2)
Call FormatTimeNow(NewTime)
SenderName = "Scheduler"
Call ApplGetResourceName(0, TheSenderId, SenderName)
ReceiverName = ""
Call ApplGetResourceName(0, TheReceiverId, ReceiverName)
PatientName = ""
Call ApplGetPatientName(PatientId, PatientName)
' Format the Message
NewMsg = ReceiverName + ": " + TheMessage
k = InStrPS(NewMsg, "<Sender>")
If (k <> 0) Then
    NewMsg = Left(NewMsg, k - 1) + SenderName + Mid(NewMsg, k + 8, Len(NewMsg) - (k + 7))
End If
k = InStrPS(NewMsg, "<Patient>")
If (k <> 0) Then
    NewMsg = Left(NewMsg, k - 1) + PatientName + Mid(NewMsg, k + 9, Len(NewMsg) - (k + 8))
End If
k = InStrPS(NewMsg, "<Test>")
If (k <> 0) Then
    NewMsg = Left(NewMsg, k - 1) + TestText + Mid(NewMsg, k + 6, Len(NewMsg) - (k + 5))
End If
ApplyMessage = ApplPostMessage(TheSenderId, TheReceiverId, NewDate, NewTime, NewMsg)
End Function

Private Function ApplPostMessage(SenderId As Long, ReceiverId As Long, TheDate As String, TheTime As String, MsgText As String) As Boolean
Dim RetrieveMessage As PracticeMessages
ApplPostMessage = False
Set RetrieveMessage = New PracticeMessages
RetrieveMessage.MessagesId = 0
Call RetrieveMessage.RetrieveMessages
RetrieveMessage.MessagesReceiverId = ReceiverId
RetrieveMessage.MessagesSenderId = SenderId
RetrieveMessage.MessagesStatus = "R"
RetrieveMessage.MessagesDate = TheDate
RetrieveMessage.MessagesText = MsgText
RetrieveMessage.MessagesTime = ConvertTimeToMinutes(TheTime)
ApplPostMessage = RetrieveMessage.ApplyMessages
Set RetrieveMessage = Nothing
End Function

Private Function ApplGetPatientName(RefId As Long, TheName As String) As Boolean
Dim RetrievePat As Patient
TheName = ""
ApplGetPatientName = False
Set RetrievePat = New Patient
RetrievePat.PatientId = RefId
If (RetrievePat.RetrievePatient) Then
    TheName = Trim(RetrievePat.FirstName) + " " _
            + Trim(RetrievePat.MiddleInitial) + " " _
            + Trim(RetrievePat.LastName) + " " _
            + Trim(RetrievePat.NameRef)
    ApplGetPatientName = True
End If
Set RetrievePat = Nothing
End Function

Private Sub ApplGetResourceName(RefId As Long, BRefId As Long, TheName As String)
Dim RetAppt As SchedulerAppointment
Dim RetRes As SchedulerResource
TheName = ""
Set RetAppt = New SchedulerAppointment
Set RetRes = New SchedulerResource
If (BRefId > 0) Then
    RetRes.ResourceId = BRefId
    If (RetRes.RetrieveSchedulerResource) Then
        TheName = Trim(RetRes.ResourceDescription)
    End If
End If
If (RefId > 0) And (Trim(TheName) = "") Then
    RetAppt.AppointmentId = RefId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        RetRes.ResourceId = RetAppt.AppointmentResourceId1
        If (RetRes.RetrieveSchedulerResource) Then
            TheName = Trim(RetRes.ResourceDescription)
        End If
    End If
End If
Set RetAppt = Nothing
Set RetRes = Nothing
End Sub

Private Function ApplLoadResource(lstBox As ListBox, LoadType As String, AllOn As Boolean, RefOn As Boolean, CompressOn As Boolean) As Boolean
Dim k As Integer
Dim g As Integer
Dim u As Integer
Dim DisplayItem As String
Dim RetrieveResource As SchedulerResource
ApplLoadResource = False
Set RetrieveResource = New SchedulerResource
RetrieveResource.ResourceName = Chr(1)
RetrieveResource.ResourceType = UCase(Trim(LoadType))
If (RetrieveResource.FindResource > 0) Then
    lstBox.Clear
    If (RefOn) Then
        If (AllOn) Then
            lstBox.AddItem "New Staff"
        Else
            lstBox.AddItem "All Staff"
        End If
    End If
    k = 1
    Do Until Not (RetrieveResource.SelectResource(k))
        DisplayItem = Space(56)
        If (Trim(RetrieveResource.ResourceDescription) <> "") Then
            Mid(DisplayItem, 1, Len(RetrieveResource.ResourceDescription)) = RetrieveResource.ResourceDescription
        Else
            Mid(DisplayItem, 1, Len(RetrieveResource.ResourceName)) = RetrieveResource.ResourceName
        End If
        If (LoadType = "R") Then
            DisplayItem = Left(DisplayItem, 56) + Trim(str(RetrieveResource.ResourceId))
            If (Not CompressOn) Then
                lstBox.AddItem DisplayItem
            Else
                If (RetrieveResource.ResourceServiceCode = "01") Then
                    DisplayItem = Space(56)
                    Mid(DisplayItem, 1, 7) = "Office-"
                    DisplayItem = Left(DisplayItem, 56) + Trim(str(RetrieveResource.ResourceId))
                    g = 0
                    For u = 0 To lstBox.ListCount - 1
                        If (Left(lstBox.List(u), 7) = "Office-") Then
                            g = 1
                            Exit For
                        End If
                    Next u
                    If (g = 0) Then
                        lstBox.AddItem DisplayItem
                    End If
                Else
                    lstBox.AddItem DisplayItem
                End If
            End If
        Else
            lstBox.AddItem Left(DisplayItem, 56) + Trim(str(RetrieveResource.ResourceId))
        End If
        k = k + 1
    Loop
    ApplLoadResource = True
End If
Set RetrieveResource = Nothing
End Function

Private Function ApplLoadCodes(lstBox As ListBox, CodeType As String, AllOn As Boolean) As Boolean
Dim k As Integer
Dim DisplayText As String
Dim RetrieveCode As PracticeCodes
ApplLoadCodes = False
Set RetrieveCode = New PracticeCodes
lstBox.Clear
If (AllOn) Then
    lstBox.AddItem "Create a new code"
End If
RetrieveCode.ReferenceType = Trim(CodeType)
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        DisplayText = Space(128)
        Mid(DisplayText, 1, Len(Trim(RetrieveCode.ReferenceCode))) = Trim(RetrieveCode.ReferenceCode)
        Mid(DisplayText, 65, Len(Trim(RetrieveCode.ReferenceAlternateCode))) = Trim(RetrieveCode.ReferenceAlternateCode)
        lstBox.AddItem DisplayText
        k = k + 1
    Loop
    ApplLoadCodes = True
End If
Set RetrieveCode = Nothing
End Function
