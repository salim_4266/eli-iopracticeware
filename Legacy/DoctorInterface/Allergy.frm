VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmAllergy 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   10350
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12705
   ForeColor       =   &H00B60000&
   LinkTopic       =   "Form1"
   Picture         =   "Allergy.frx":0000
   ScaleHeight     =   10350
   ScaleWidth      =   12705
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSFlexGridLib.MSFlexGrid fgc 
      Height          =   6255
      Left            =   3240
      TabIndex        =   34
      Top             =   1080
      Visible         =   0   'False
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   11033
      _Version        =   393216
      Rows            =   0
      FixedRows       =   0
      FixedCols       =   0
      BackColor       =   16250357
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   735
      Left            =   10320
      TabIndex        =   0
      Top             =   8160
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNo 
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   8160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":38FC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Top             =   360
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":3B2F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":3D5A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   2
      Left            =   120
      TabIndex        =   5
      Top             =   2040
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":3F85
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   3
      Left            =   120
      TabIndex        =   6
      Top             =   2880
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":41B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   4
      Left            =   120
      TabIndex        =   7
      Top             =   3720
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":43DB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   5
      Left            =   120
      TabIndex        =   8
      Top             =   4560
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":4606
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   6
      Left            =   120
      TabIndex        =   9
      Top             =   5400
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":4831
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   7
      Left            =   120
      TabIndex        =   10
      Top             =   6240
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":4A5C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   8
      Left            =   120
      TabIndex        =   11
      Top             =   7080
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":4C87
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPage 
      Height          =   735
      Index           =   0
      Left            =   2520
      TabIndex        =   12
      Top             =   8160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":4EB2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPage 
      Height          =   735
      Index           =   1
      Left            =   4200
      TabIndex        =   13
      Top             =   8160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":50E5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   9
      Left            =   4080
      TabIndex        =   14
      Top             =   360
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":5318
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   10
      Left            =   4080
      TabIndex        =   15
      Top             =   1200
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":5543
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   11
      Left            =   4080
      TabIndex        =   16
      Top             =   2040
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":576E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   12
      Left            =   4080
      TabIndex        =   17
      Top             =   2880
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":5999
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   13
      Left            =   4080
      TabIndex        =   18
      Top             =   3720
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":5BC4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   14
      Left            =   4080
      TabIndex        =   19
      Top             =   4560
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":5DEF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   15
      Left            =   4080
      TabIndex        =   20
      Top             =   5400
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":601A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   16
      Left            =   4080
      TabIndex        =   21
      Top             =   6240
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":6245
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   17
      Left            =   4080
      TabIndex        =   22
      Top             =   7080
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":6470
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   18
      Left            =   8040
      TabIndex        =   23
      Top             =   360
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":669B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   19
      Left            =   8040
      TabIndex        =   24
      Top             =   1200
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":68C6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   20
      Left            =   8040
      TabIndex        =   25
      Top             =   2040
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":6AF1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   21
      Left            =   8040
      TabIndex        =   26
      Top             =   2880
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":6D1C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   22
      Left            =   8040
      TabIndex        =   27
      Top             =   3720
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":6F47
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   23
      Left            =   8040
      TabIndex        =   28
      Top             =   4560
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":7172
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   24
      Left            =   8040
      TabIndex        =   29
      Top             =   5400
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":739D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   25
      Left            =   8040
      TabIndex        =   30
      Top             =   6240
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":75C8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   735
      Index           =   26
      Left            =   8040
      TabIndex        =   31
      Top             =   7080
      Visible         =   0   'False
      Width           =   3795
      _Version        =   131072
      _ExtentX        =   6694
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":77F3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch 
      Height          =   735
      Left            =   6240
      TabIndex        =   32
      Top             =   8160
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   13684944
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Allergy.frx":7A1E
   End
   Begin VB.Label lblPat 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Allergies"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   120
      TabIndex        =   33
      Top             =   120
      Width           =   3780
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblLevel1 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Allergies"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   4080
      TabIndex        =   2
      Top             =   120
      Width           =   900
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmAllergy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public TheFile As String
Public Question As String
Private cmdClr(3) As Long
Private Const FSlash As String = "/"
Private CAllerg As CAllergies

Private Sub cmd_Click(i As Integer)

frmSystems1.AllergyChanges = True

Dim n As Integer
Dim r As Integer

    If cmd(i).BackColor = cmdClr(0) Then
        AllergyRtnSvt.Show vbModal, frmAllergy
    'If (InStrPS(StoreResults, "P66A") <> 0) Then

        If AllergyRtnSvt.Reaction = "" Then
            AllergyRtnSvt.Reaction = "None"
        End If
        If AllergyRtnSvt.Severity = "" Then
            AllergyRtnSvt.Severity = "None"
        End If
        fgc.AddItem cmd(i).Text & vbTab & cmd(i).Tag + FSlash + AllergyRtnSvt.Reaction + FSlash + AllergyRtnSvt.Severity
                'ButtonName = ButtonName + "\" + AllergyRtnSvt.Reaction + "\" + AllergyRtnSvt.Severity
        'End If
        'fgc.AddItem cmd(i).Text & vbTab & cmd(i).Tag
        n = 2
    Else
        For r = 0 To fgc.Rows - 1
        
'        If InStrPS(fgc.TextMatrix(r, 0), "=") Then
'
'            If Mid(fgc.TextMatrix(r, 0), 1, InStrPS(fgc.TextMatrix(r, 0), FSlash) - 1) = cmd(i).Tag Then
'                If fgc.Rows = 1 Then
'                    fgc.Rows = 0
'                Else
'                    fgc.RemoveItem r
'                End If
'                Exit For
'            End If
'        Else
            If fgc.TextMatrix(r, 1) = cmd(i).Tag Then
                If fgc.Rows = 1 Then
                    fgc.Rows = 0
                Else
                    fgc.RemoveItem r
                End If
                Exit For
            End If
'        End If
'
    Next
    n = 0
End If
cmd(i).BackColor = cmdClr(n)
cmd(i).ForeColor = cmdClr(n + 1)
End Sub

Private Sub cmdApply_Click()
Dim Rec(1) As New Collection
Dim Ln As String
Dim e As Variant
Dim i As Integer
Dim r(1) As Integer
Dim FileNum As Integer
FileNum = FreeFile

For r(1) = 0 To fgc.Rows - 1
    Ln = "QUESTION=/" & Question
    Rec(0).Add Ln
    Dim delimE As Integer
    delimE = InStr(1, fgc.TextMatrix(r(1), 0), "=")
    If delimE > 0 Then
        Ln = fgc.TextMatrix(r(1), 0)
'        & "=" & _
'         fgc.TextMatrix(r(1), 1) & " <" & _
'         fgc.TextMatrix(r(1), 0) & ">"
    Else
        Ln = fgc.TextMatrix(r(1), 0) & "=" & _
         fgc.TextMatrix(r(1), 1) & " <" & _
         fgc.TextMatrix(r(1), 0) & ">"
    End If
    
    Rec(0).Add Ln
    Ln = "Recap=" & fgc.TextMatrix(r(1), 0)
    Rec(1).Add Ln
Next

If Rec(0).Count = 0 Then
    If FM.IsFileThere(TheFile) Then
        FM.Kill TheFile
    End If
Else
    FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
    For i = 0 To 1
        For Each e In Rec(i)
        Dim GH As String
        GH = FM.actualpath(TheFile)
            Print #FileNum, e
        Next
    Next
    FM.CloseFile CLng(FileNum)
End If

Unload Me
End Sub

Private Sub cmdNo_Click()
Unload Me
End Sub

Public Function LoadAllergy() As Boolean
'On Error GoTo UI_ErrorHandler
Dim drug(1) As String
Dim delim As Integer
Dim r As Integer

'load file
Dim FileNum As Integer
Dim Rec As String
FileNum = FreeFile
If FM.IsFileThere(TheFile) Then
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
    Do Until EOF(FileNum)
        Line Input #FileNum, Rec
        If UCase(Left(Rec, 9)) = "QUESTION=" _
        Or UCase(Left(Rec, 6)) = "RECAP=" Then
        Else
            delim = InStr(1, Rec, "=")
            'If UCase(Mid(Rec, delim + 1, 1)) = p Then
                'drug(0) = Left(Rec, delim - 1)
                drug(0) = Rec
                'here code modefied to update the allergies with reaactions ans severity
                Dim LSlash As Integer
                LSlash = InStrPS(Rec, FSlash)
                If (LSlash > 0) Then
                    drug(1) = myTrim(Mid(Rec, delim + 1, LSlash - delim - 1))
                Else
                    drug(1) = myTrim(Mid(Rec, delim + 1))
                End If
                Dim fp As String
                fp = FM.actualpath(TheFile)
                fgc.AddItem drug(0) & vbTab & drug(1)
            'End If
        End If
    Loop
    FM.CloseFile CLng(FileNum)
End If

'load form
Dim tst As String
tst = SearchAllergy
If Not tst = "" Then
    Set CAllerg = New CAllergies
    Call CAllerg.GetAllergyRS(tst)
    Call LoadElements
    LoadAllergy = True
End If
Exit Function
UI_ErrorHandler:
    LoadAllergy = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function SearchAllergy() As String
frmAlphaPad.NumPad_Field = "Name of Drug"
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.Show 1
If (Trim(frmAlphaPad.NumPad_Result) <> "") Then
    SearchAllergy = Trim(frmAlphaPad.NumPad_Result)
End If
End Function

Private Sub LoadElements()

Dim i As Integer

For i = 0 To 26
    If CAllerg.AllergyTblEof Then
        cmd(i).Text = ""
        cmd(i).Tag = ""
        cmd(i).Visible = False
    Else
        cmd(i).Text = CAllerg.fldDescription
        cmd(i).Tag = CAllerg.fldCompositeAllergyID
        cmd(i).Visible = True
        CAllerg.TblMove "Next"
    End If
    ColorCmd cmd(i)
Next
Dim pg As Boolean
pg = (CAllerg.AllergyTblRecordCount > 27)
cmdPage(0).Visible = pg
cmdPage(1).Visible = pg
End Sub

Private Sub ColorCmd(cmd As fpBtn)
Dim r As Integer
Dim n As Integer
Dim delimD As Integer
For r = 0 To fgc.Rows - 1
    delimD = InStr(1, fgc.TextMatrix(r, 0), "=")
    If delimD > 0 Then
        If Left(fgc.TextMatrix(r, 0), delimD - 1) = cmd.Text _
        And fgc.TextMatrix(r, 1) = cmd.Tag Then
            n = 2
            Exit For
        End If
    Else
        If fgc.TextMatrix(r, 0) = cmd.Text _
        And fgc.TextMatrix(r, 1) = cmd.Tag Then
            n = 2
            Exit For
        End If
    End If
Next

cmd.BackColor = cmdClr(n)
cmd.ForeColor = cmdClr(n + 1)
End Sub

Private Sub cmdPage_Click(Index As Integer)
Select Case Index
Case 0
    Do Until CAllerg.AllergyTblBof
        CAllerg.TblMove "Previous"
        If CAllerg.fldCompositeAllergyID = cmd(0).Tag _
        And CAllerg.fldDescription = cmd(0).Text Then
            Exit Do
        End If
    Loop
    Dim i As Integer
    For i = 0 To 26
        CAllerg.TblMove "Previous"
        If CAllerg.AllergyTblBof Then
            CAllerg.TblMove "First"
            Exit For
        End If
    Next
Case 1
    If CAllerg.AllergyTblEof Then
        CAllerg.TblMove "First"
    End If
End Select

LoadElements
End Sub
Private Sub cmdSearch_Click()
Call CAllerg.GetAllergyRS(SearchAllergy)
Call LoadElements
End Sub
Private Sub form_load()
fgc.ColWidth(1) = 1200
fgc.ColWidth(0) = fgc.Width - fgc.ColWidth(1) - 120
cmdClr(0) = &H9B9626
cmdClr(1) = &HFFFFFF
cmdClr(2) = 14745312
cmdClr(3) = 0
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Set CAllerg = Nothing
End Sub
Public Function FrmClose()
  Unload Me
End Function
