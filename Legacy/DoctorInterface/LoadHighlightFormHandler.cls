VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LoadHighlightFormHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IO_Practiceware_Interop.IEventHandler


Private Sub IEventHandler_OnEvent(ByVal sender As Variant, ByVal e As Variant)

frmHighlights.HighlightLoadOn = frmSystems1.IsHighlightLoadOn
If (frmHighlights.LoadHighlights(globalExam.iPatientId, True, True, False)) Then
    frmHighlights.Enabled = True
    frmHighlights.Show 1
    Call frmSystems1.SetHighlightLoad(frmHighlights.HighlightLoadOn)
End If
End Sub



