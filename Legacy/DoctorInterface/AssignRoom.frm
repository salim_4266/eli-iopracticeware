VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmAssignRoom 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9720
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12720
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "AssignRoom.frx":0000
   ScaleHeight     =   9720
   ScaleWidth      =   12720
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   1
      Left            =   240
      TabIndex        =   7
      Top             =   2400
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   0
      Left            =   240
      TabIndex        =   6
      Top             =   1080
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":38B6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10080
      TabIndex        =   0
      Top             =   7800
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":3AA3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   975
      Left            =   6240
      TabIndex        =   1
      Top             =   7800
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":3C82
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   2
      Left            =   240
      TabIndex        =   8
      Top             =   3720
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":3E61
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   3
      Left            =   240
      TabIndex        =   9
      Top             =   5040
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":404E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   4
      Left            =   240
      TabIndex        =   10
      Top             =   6360
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":423B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   5
      Left            =   3120
      TabIndex        =   11
      Top             =   1080
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":4428
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   6
      Left            =   3120
      TabIndex        =   12
      Top             =   2400
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":4615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   7
      Left            =   3120
      TabIndex        =   13
      Top             =   3720
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":4802
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   8
      Left            =   3120
      TabIndex        =   14
      Top             =   5040
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":49EF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   9
      Left            =   3120
      TabIndex        =   15
      Top             =   6360
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":4BDC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   10
      Left            =   6000
      TabIndex        =   16
      Top             =   1080
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":4DC9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   11
      Left            =   6000
      TabIndex        =   17
      Top             =   2400
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":4FB6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   12
      Left            =   6000
      TabIndex        =   18
      Top             =   3720
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":51A3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   13
      Left            =   6000
      TabIndex        =   19
      Top             =   5040
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":5390
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   14
      Left            =   6000
      TabIndex        =   20
      Top             =   6360
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":557D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   15
      Left            =   8880
      TabIndex        =   21
      Top             =   1080
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":576A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   16
      Left            =   8880
      TabIndex        =   22
      Top             =   2400
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":5957
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   17
      Left            =   8880
      TabIndex        =   23
      Top             =   3720
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":5B44
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   18
      Left            =   8880
      TabIndex        =   24
      Top             =   5040
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":5D31
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRoom 
      Height          =   1215
      Index           =   19
      Left            =   8880
      TabIndex        =   25
      Top             =   6360
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   2143
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignRoom.frx":5F1E
   End
   Begin VB.Label lblPatient 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Patient"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   1605
      TabIndex        =   5
      Top             =   120
      Width           =   765
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Rooms"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   4200
      TabIndex        =   4
      Top             =   480
      Width           =   2775
   End
   Begin VB.Label lblDate 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Date:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   8505
      TabIndex        =   3
      Top             =   120
      Width           =   585
   End
   Begin VB.Label lblTime 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Time:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   8520
      TabIndex        =   2
      Top             =   480
      Width           =   615
   End
End
Attribute VB_Name = "frmAssignRoom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TheActivityId As Long
Public ThePatientName As String
Public TheResourceId As Long

Private Const AcceptIt As Integer = 1
Private Const Other1It As Integer = 2
Private Const Other2It As Integer = 3
Private Const CancelIt As Integer = 4

Private ActivityColor As Long
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long
Private TotalResources As Integer
Private CurrentIndex As Integer
Private PatientId As Long
Private AppointmentId As Long

Private Const MAX_ROOM_BUTTONS As Long = 20

Private Sub ClearButtons()
Dim i As Integer
    For i = 0 To MAX_ROOM_BUTTONS - 1
        cmdRoom(i).Text = ""
        cmdRoom(i).Tag = ""
        cmdRoom(i).Visible = False
    Next i
End Sub

Public Function LoadAssignableRooms(Init As Boolean) As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Integer, z As Integer
Dim CurBackColor As Long
Dim CurTextColor As Long
Dim ButtonCnt As Integer
Dim PatientText As String
Dim iActAppId As Long
Dim LocalActivity As PracticeActivity
Dim RetrieveActivity As PracticeActivity
Dim RetrieveResource As SchedulerResource
Dim oLocalDate As New CIODateTime
LoadAssignableRooms = False
If (Init) Then
    TheResourceId = 0
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = cmdRoom(0).BackColor
    BaseTextColor = cmdRoom(0).ForeColor
    CurrentIndex = 1
End If
Call ClearButtons
oLocalDate.MyDateTime = Now
lblDate.Caption = oLocalDate.GetDisplayDate(True)
lblTime.Caption = oLocalDate.GetDisplayTime
lblPatient.Caption = ThePatientName
Set RetrieveActivity = New PracticeActivity
RetrieveActivity.ActivityId = TheActivityId
If (RetrieveActivity.RetrieveActivity) Then
    ActivityColor = GetDoctorColor(RetrieveActivity.ActivityAppointmentId)
    ButtonCnt = 1
    Set RetrieveResource = New SchedulerResource
    RetrieveResource.ResourceName = Chr(1)
    RetrieveResource.PracticeId = dbPracticeId
    If (dbPracticeId > 1000) Then
        RetrieveResource.PracticeId = dbPracticeId - 1000
    ElseIf (dbPracticeId = 0) Then
        RetrieveResource.PracticeId = -1
    End If
    TotalResources = RetrieveResource.FindResourcebyRoom
    If (TotalResources > 0) Then
        ButtonCnt = 1
        i = CurrentIndex
        Set LocalActivity = New PracticeActivity
        Do Until (Not (RetrieveResource.SelectResource(i)) Or (ButtonCnt > 20))
            z = 0
            If (Trim(dbSoftwareLocation) <> "" And Trim(dbSoftwareLocation) <> "Office") Then
                If (RetrieveResource.PracticeId = (dbPracticeId - 1000)) Then
                    z = 1
                Else
                    TotalResources = TotalResources - 1
                End If
            ElseIf (RetrieveResource.ResourceServiceCode = "01") Then
                z = 1
            Else
                TotalResources = TotalResources - 1
            End If
            If (z > 0) Then
                iActAppId = GetRoomOccupants(RetrieveResource.ResourceId, oLocalDate.GetIODate, PatientText)
                If (Trim(PatientText) <> "") Then
                    CurBackColor = GetDoctorColor(iActAppId)
                    CurTextColor = TextSelectionColor
                Else
                    CurBackColor = BaseBackColor
                    CurTextColor = BaseTextColor
                End If
                With cmdRoom(ButtonCnt - 1)
                    .BackColor = CurBackColor
                    If CurTextColor > 0 Then
                        .ForeColor = CurTextColor
                    End If
                    .Text = RetrieveResource.ResourceDescription & PatientText
                    .Tag = Trim$(str$(RetrieveResource.ResourceId))
                    .Visible = True
                End With
                ButtonCnt = ButtonCnt + 1
            End If
            i = i + 1
        Loop
        CurrentIndex = i - 1
        If (ButtonCnt > 0) Then
            LoadAssignableRooms = True
        End If
        If (TotalResources > MAX_ROOM_BUTTONS) Then
            cmdMore.Visible = True
            cmdMore.Text = "Next"
        Else
            cmdMore.Visible = False
        End If
    End If
End If
Set RetrieveResource = Nothing
Set LocalActivity = Nothing
Set RetrieveActivity = Nothing
Set oLocalDate = Nothing
Exit Function
UI_ErrorHandler:
    Set RetrieveResource = Nothing
    Set LocalActivity = Nothing
    Set RetrieveActivity = Nothing
    Call PinpointError("LoadAssignableRooms", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume LeaveFast
LeaveFast:
End Function

Private Function GetRoomOccupants(ResourceId As Long, LocalDate As String, TheNames As String) As Long
Dim i As Integer
Dim TempText As String
Dim RetrievePat As Patient
Dim RetrieveAct As PracticeActivity
TheNames = ""
If (ResourceId > 0) And (Trim(LocalDate) <> "") Then
    Set RetrievePat = New Patient
    Set RetrieveAct = New PracticeActivity
    RetrieveAct.ActivityDate = 0
    RetrieveAct.ActivityStatus = "E"
    RetrieveAct.ActivityResourceId = ResourceId
    RetrieveAct.ActivityLocationId = dbPracticeId
    RetrieveAct.ActivityAppointmentId = 0
    RetrieveAct.ActivityPatientId = 0
    If (RetrieveAct.FindActivity > 0) Then
        i = 1
        Do Until Not (RetrieveAct.SelectActivity(i))
            RetrievePat.PatientId = RetrieveAct.ActivityPatientId
            If (RetrievePat.RetrievePatient) Then
                TempText = Trim(RetrievePat.FirstName) + " " _
                         + Trim(RetrievePat.MiddleInitial) + " " _
                         + Trim(RetrievePat.LastName) + " " _
                         + Trim(RetrievePat.NameRef)
                TheNames = TheNames + Chr(13) + Chr(10) + TempText
                GetRoomOccupants = RetrieveAct.ActivityAppointmentId
            End If
            i = i + 1
        Loop
    End If
    Set RetrievePat = Nothing
    Set RetrieveAct = Nothing
End If
End Function

Private Function GetDoctorColor(ApptId As Long) As Long
Dim RetrieveAppt As SchedulerAppointment
Dim i As Long
GetDoctorColor = ButtonSelectionColor
Set RetrieveAppt = New SchedulerAppointment
RetrieveAppt.AppointmentId = ApptId
If (RetrieveAppt.RetrieveSchedulerAppointment) Then
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId1)
    If (i >= 0) Then
        GetDoctorColor = i
        Set RetrieveAppt = Nothing
        Exit Function
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId2)
    If (i >= 0) Then
        GetDoctorColor = i
        Set RetrieveAppt = Nothing
        Exit Function
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId3)
    If (i >= 0) Then
        GetDoctorColor = i
        Set RetrieveAppt = Nothing
        Exit Function
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId4)
    If (i >= 0) Then
        GetDoctorColor = i
        Set RetrieveAppt = Nothing
        Exit Function
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId5)
    If (i >= 0) Then
        GetDoctorColor = i
        Set RetrieveAppt = Nothing
        Exit Function
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId6)
    If (i >= 0) Then
        GetDoctorColor = i
        Set RetrieveAppt = Nothing
        Exit Function
    End If
    i = LocateDoctorColor(RetrieveAppt.AppointmentResourceId7)
    If (i >= 0) Then
        GetDoctorColor = i
        Set RetrieveAppt = Nothing
        Exit Function
    End If
End If
Set RetrieveAppt = Nothing
End Function

Private Function LocateDoctorColor(ResourceId As Long) As Long
Dim RetrieveRes As SchedulerResource
LocateDoctorColor = -1
If (ResourceId > 0) Then
    Set RetrieveRes = New SchedulerResource
    RetrieveRes.ResourceId = ResourceId
    If (RetrieveRes.RetrieveSchedulerResource) Then
        If (RetrieveRes.ResourceType <> "R") Then
            LocateDoctorColor = RetrieveRes.ResourceColor
        End If
    End If
    Set RetrieveRes = Nothing
End If
End Function

Private Sub cmdDone_Click()
Dim ATime As String
Dim BTime As Long
Dim RetrieveActivity As PracticeActivity
If (TheResourceId > 0) Then
    Set RetrieveActivity = New PracticeActivity
    RetrieveActivity.ActivityId = TheActivityId
    If (RetrieveActivity.RetrieveActivity) Then
        RetrieveActivity.ActivityResourceId = TheResourceId
        ATime = ""
        Call FormatTimeNow(ATime)
        BTime = ConvertTimeToMinutes(ATime)
        RetrieveActivity.ActivityStatusTime = ATime
        RetrieveActivity.ActivityStatusTRef = BTime
        RetrieveActivity.ActivityStatus = "E"
        If Not (RetrieveActivity.ApplyActivity) Then
            frmEventMsgs.InfoMessage "Activity Assignment failed"
            frmEventMsgs.Show 1
        End If
    End If
    Set RetrieveActivity = Nothing
End If
Unload frmAssignRoom
End Sub

Private Sub cmdMore_Click()
If (CurrentIndex > TotalResources) Then
    CurrentIndex = 1
End If
Call LoadAssignableRooms(False)
End Sub

Private Sub SelectRoom(iIndex As Integer)
    If cmdRoom(iIndex).BackColor = BaseBackColor Then
        cmdRoom(iIndex).BackColor = ButtonSelectionColor
        cmdRoom(iIndex).ForeColor = TextSelectionColor
        TheResourceId = Val(Trim$(cmdRoom(iIndex).Tag))
        cmdDone_Click
    ElseIf cmdRoom(iIndex).BackColor = ButtonSelectionColor Then
        cmdRoom(iIndex).BackColor = BaseBackColor
        cmdRoom(iIndex).ForeColor = BaseTextColor
        TheResourceId = 0
    ElseIf Left$(cmdRoom(iIndex).Text, 1) = "*" Then
        cmdRoom(iIndex).Text = Mid$(cmdRoom(iIndex).Text, 3, Len(cmdRoom(iIndex).Text) - 2)
        TheResourceId = 0
    Else
        frmEventMsgs.SetButtons "Room is in use - Assign anyway ?", "", "Yes", "No"
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = Other1It) Then
            cmdRoom(iIndex).Text = "* " & cmdRoom(iIndex).Text
            TheResourceId = Val(Trim$(cmdRoom(iIndex).Tag))
            cmdDone_Click
        Else
            TheResourceId = 0
        End If
    End If
End Sub

Private Sub cmdRoom_Click(Index As Integer)
    SelectRoom Index
End Sub
