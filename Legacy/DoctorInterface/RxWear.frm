VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmRxWear 
   BackColor       =   &H00505050&
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   4110
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "RxWear.frx":0000
   ScaleHeight     =   4110
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdItem13 
      Height          =   1095
      Left            =   6000
      TabIndex        =   0
      Top             =   1320
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem2 
      Height          =   1095
      Left            =   1680
      TabIndex        =   1
      Top             =   120
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":38FE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem1 
      Height          =   1095
      Left            =   240
      TabIndex        =   2
      Top             =   120
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":3B32
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem3 
      Height          =   1095
      Left            =   3120
      TabIndex        =   3
      Top             =   120
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":3D66
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem4 
      Height          =   1095
      Left            =   4560
      TabIndex        =   4
      Top             =   120
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":3F9A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem5 
      Height          =   1095
      Left            =   6000
      TabIndex        =   5
      Top             =   120
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":41CE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem12 
      Height          =   1095
      Left            =   4560
      TabIndex        =   6
      Top             =   1320
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":4402
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem11 
      Height          =   1095
      Left            =   3120
      TabIndex        =   7
      Top             =   1320
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":4637
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem10 
      Height          =   1095
      Left            =   1680
      TabIndex        =   8
      Top             =   1320
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":486C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem9 
      Height          =   1095
      Left            =   240
      TabIndex        =   9
      Top             =   1320
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":4AA1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem14 
      Height          =   1095
      Left            =   7440
      TabIndex        =   10
      Top             =   1320
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":4CD5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem6 
      Height          =   1095
      Left            =   7440
      TabIndex        =   11
      Top             =   120
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":4F0A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem7 
      Height          =   1095
      Left            =   8880
      TabIndex        =   12
      Top             =   120
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":513E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem15 
      Height          =   1095
      Left            =   8880
      TabIndex        =   13
      Top             =   1320
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":5372
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem8 
      Height          =   1095
      Left            =   10320
      TabIndex        =   14
      Top             =   120
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":55A7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdItem16 
      Height          =   1095
      Left            =   10320
      TabIndex        =   15
      Top             =   1320
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":57DB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdApply 
      Height          =   1095
      Left            =   10320
      TabIndex        =   16
      Top             =   2760
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":5A10
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   1095
      Left            =   240
      TabIndex        =   17
      Top             =   2880
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":5C43
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRemove 
      Height          =   1095
      Left            =   3120
      TabIndex        =   18
      Top             =   2760
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":5E82
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   1095
      Left            =   4680
      TabIndex        =   19
      Top             =   2760
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":60BF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCopy 
      Height          =   1095
      Left            =   6240
      TabIndex        =   20
      Top             =   2760
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxWear.frx":62FF
   End
End
Attribute VB_Name = "frmRxWear"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public StoreResults As String
Public FormOn As Boolean
Public Question As String
Public QuitOn As Boolean
Public PatientId As Long
Public AppointmentId As Long
Public ManifestResults As String
Public ManifestResultsOn As Boolean
Public CLFitOn As Boolean

Private Const MaxTestsWideAllowed = 7
Private Const MaxTestsAllowed = 6
Private Const MaxBtnAllowed = 6
Private Const MaxPadsAllowed = 8
Private Const MaxPadChoices = 24
Private Const MaxButtonsAllowed = 38

Private Type ButtonDetail
    Buttons As Boolean
    ButtonOn As Boolean
    ButtonName As String
    ButtonType As String
    ButtonText As String
    ButtonRange As String
    ButtonDeviceRef As String
    ButtonVisible As Boolean
    ButtonTrigger As String
    ButtonTriggerText As String
    ButtonIncrement As String
    ButtonMutual As Boolean
    ButtonPadOrder As Boolean
    ButtonUseOtherText As Boolean
    ButtonId As Long
    Result As String
End Type

Private UseManifest As Boolean
Private TheTestId As Integer
Private RemoveOD(MaxTestsAllowed) As Boolean
Private RemoveOS(MaxTestsAllowed) As Boolean
Private RemoveOU(MaxTestsAllowed) As Boolean
Private TimeRef(MaxTestsAllowed) As ButtonDetail
Private MaxWriteButtonsForTests(MaxTestsAllowed) As Integer
Private MaxTests As Integer
Private MaxTestAudio As Integer
Private MaxTestWideBtns As Integer
Private MaxPadsForTests As Integer
Private MaxButtonsForTests As Integer
Private MaxButtonsForPads As Integer
Private MaxButtonsForButtons As Integer
Private DisplayTests(MaxTestsAllowed) As ButtonDetail
Private DisplayPads(MaxTestsAllowed, 2, MaxPadsAllowed) As ButtonDetail
Private DisplayBtns(MaxTestsAllowed, 2) As ButtonDetail
Private TestAudio(MaxTestsAllowed, 2) As ButtonDetail
Private TestWideButtons(MaxTestsAllowed, MaxButtonsAllowed) As ButtonDetail
Private PlantTriggerButtons(MaxTestsAllowed, 2, MaxButtonsAllowed) As ButtonDetail
Private PlantTriggerPads(MaxTestsAllowed, 2, MaxPadsAllowed, MaxPadChoices) As ButtonDetail

Private OrgButtonColor As Long
Private OrgTextColor As Long
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long

Private CurrentEye As String
Private CurrentOrder As String
Private ExitFast As Boolean
Private ScratchTestFile As String
Private PrevTestFile As String
Private CurrentFormId As Long

Private Sub cmdQuit_Click()
On Error GoTo UI_EH
If (FM.IsFileThere(ScratchTestFile)) Then
    FM.Kill ScratchTestFile
End If
If (FM.IsFileThere(PrevTestFile)) Then
    FM.Kill PrevTestFile
End If
If Not (VerifyScratchFile(StoreResults)) Then
    If Not (IsTestFileThere(StoreResults)) Then
        FM.Kill StoreResults
    End If
End If
ACont:
QuitOn = True
ManifestResultsOn = False
Unload frmEditTests
FormOn = False
Exit Sub
UI_EH:
    Resume ACont
End Sub

Private Sub cmdApply_Click()
Dim PlantFirst As Boolean
Dim PostIt As Boolean, TestOn As Boolean
Dim RscId As Long
Dim u As Integer
Dim FileNum As Integer
Dim i As Integer, j As Integer
Dim p As Integer, k As Integer
Dim StartRef As Integer, EndRef As Integer
Dim PadName As String, AName As String
Dim TestTime As String, TestName As String
Dim ButtonName As String, OtherFile As String
If (Trim(ScratchTestFile) <> "") Then
    FileNum = FreeFile
    FM.OpenFile ScratchTestFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
    Print #FileNum, "QUESTION=" + "/" + Question
    For i = 1 To MaxTests
        TestOn = False
        If (Trim(DisplayTests(i).ButtonName) <> "") And ((DisplayTests(i).Buttons) Or (DisplayTests(i).ButtonOn) Or (MaxTests = 1)) Then
            TestOn = True
            If (Trim(TimeRef(i).Result) <> "") Then
                TestTime = Trim(TimeRef(i).Result)
            Else
                Call FormatTimeNow(TestTime)
            End If
            RscId = UserLogin.iId
            AName = UserLogin.sNameShort
            If (InStrPS(TestTime, Trim(AName)) = 0) Then
                Print #FileNum, "TIME=" + TestTime + " (" + AName + ")"
            Else
                Print #FileNum, "TIME=" + TestTime
            End If
            TestName = DisplayTests(i).ButtonName + "=T" + Trim(str(DisplayTests(i).ButtonId)) + " <" + Trim(DisplayTests(i).ButtonName) + ">"
            Print #FileNum, TestName
        End If
        If (TestOn) Then
            For j = 1 To 2
                PostIt = False
                If (j = 1) And Not (RemoveOD(i)) Then
                    PostIt = True
                End If
                If (j = 2) And Not (RemoveOS(i)) Then
                    PostIt = True
                End If
                If (PostIt) Then
                    If (DisplayBtns(i, j).ButtonOn) Then
                        ButtonName = DisplayBtns(i, j).ButtonName + "=T" + Trim(str(DisplayBtns(i, j).ButtonId)) + " <" + DisplayBtns(i, j).ButtonName + ">"
                        Print #FileNum, ButtonName
                        For k = 1 To MaxButtonsAllowed
                            If (PlantTriggerButtons(i, j, k).ButtonOn) Then
                                Print #FileNum, PlantTriggerButtons(i, j, k).ButtonName + "=T" + Trim(str(PlantTriggerButtons(i, j, k).ButtonId)) + " <" + Trim(PlantTriggerButtons(i, j, k).ButtonName) + ">"
                            End If
                        Next k
                    ElseIf (DisplayBtns(i, j).Buttons) Then
                        ButtonName = DisplayBtns(i, j).ButtonName + "=T" + Trim(str(DisplayBtns(i, j).ButtonId)) + " <" + DisplayBtns(i, j).ButtonName + ">"
                        Print #FileNum, ButtonName
                        For k = 1 To MaxButtonsAllowed
                            If (PlantTriggerButtons(i, j, k).Buttons) Then
                                Print #FileNum, PlantTriggerButtons(i, j, k).ButtonName + "=T" + Trim(str(PlantTriggerButtons(i, j, k).ButtonId)) + " <" + Trim(PlantTriggerButtons(i, j, k).ButtonName) + ">"
                            End If
                        Next k
                    ElseIf (Trim(DisplayBtns(i, j).ButtonName) <> "") Then
                        If (DisplayBtns(i, j).Buttons) Then
                            ButtonName = DisplayBtns(i, j).ButtonName + "=T" + Trim(str(DisplayBtns(i, j).ButtonId)) + " <" + DisplayBtns(i, j).ButtonName + ">"
                            Print #FileNum, ButtonName
                            For k = 1 To MaxButtonsAllowed
                                If (PlantTriggerButtons(i, j, k).Buttons) Then
                                    Print #FileNum, PlantTriggerButtons(i, j, k).ButtonName + "=T" + Trim(str(PlantTriggerButtons(i, j, k).ButtonId)) + " <" + Trim(PlantTriggerButtons(i, j, k).ButtonName) + ">"
                                End If
                            Next k
                        ElseIf (Mid(DisplayBtns(i, j).ButtonName, 2, 1) = "O") Then
                            ButtonName = DisplayBtns(i, j).ButtonName + "=T" + Trim(str(DisplayBtns(i, j).ButtonId)) + " <" + DisplayBtns(i, j).ButtonName + ">"
                            Print #FileNum, ButtonName
                            For k = 1 To MaxButtonsAllowed
                                If (PlantTriggerButtons(i, j, k).Buttons) Then
                                    Print #FileNum, PlantTriggerButtons(i, j, k).ButtonName + "=T" + Trim(str(PlantTriggerButtons(i, j, k).ButtonId)) + " <" + Trim(PlantTriggerButtons(i, j, k).ButtonName) + ">"
                                End If
                            Next k
                        End If
                    Else
                        For k = 1 To MaxButtonsAllowed
                            If (PlantTriggerButtons(i, j, k).ButtonOn) Then
                                Print #FileNum, PlantTriggerButtons(i, j, k).ButtonName + "=T" + Trim(str(PlantTriggerButtons(i, j, k).ButtonId)) + " <" + Trim(PlantTriggerButtons(i, j, k).ButtonName) + ">"
                            End If
                        Next k
                        For k = 1 To MaxButtonsAllowed
                            If (PlantTriggerButtons(i, j, k).Buttons) Then
                                Print #FileNum, PlantTriggerButtons(i, j, k).ButtonName + "=T" + Trim(str(PlantTriggerButtons(i, j, k).ButtonId)) + " <" + Trim(PlantTriggerButtons(i, j, k).ButtonName) + ">"
                            End If
                        Next k
                    End If

                    If (FM.InFileNameStartStr(StoreResults, "G") < 1) Then
                        For p = 1 To MaxTestWideBtns
                            If (TestWideButtons(i, p).ButtonMutual) Then
                                If (TestWideButtons(i, p).ButtonOn) And (TestWideButtons(i, p).Buttons) Then
                                    ButtonName = TestWideButtons(i, p).ButtonName + "=T" + Trim(str(TestWideButtons(i, p).ButtonId)) + " <" + Trim(TestWideButtons(i, p).Result) + ">"
                                    Print #FileNum, ButtonName
                                End If
                            Else        ' Mutually Inclusive
                                If (TestWideButtons(i, p).Buttons) Then
                                    ButtonName = TestWideButtons(i, p).ButtonName + "=T" + Trim(str(TestWideButtons(i, p).ButtonId)) + " <" + Trim(TestWideButtons(i, p).Result) + ">"
                                    Print #FileNum, ButtonName
                                ElseIf (TestWideButtons(i, p).ButtonOn) Then
                                    ButtonName = TestWideButtons(i, p).ButtonName + "=T" + Trim(str(TestWideButtons(i, p).ButtonId)) + " <" + Trim(TestWideButtons(i, p).Result) + ">"
                                    Print #FileNum, ButtonName
                                End If
                            End If
                        Next p
                        For p = 1 To MaxTestAudio
                            If (TestAudio(i, p).ButtonOn) Or (TestAudio(i, p).Buttons) Then
                                ButtonName = TestAudio(i, p).ButtonName + "=T" + Trim(str(TestAudio(i, p).ButtonId)) + " <Audio>"
                                Print #FileNum, ButtonName
                            End If
                        Next p
                    End If
                    
                    For p = 1 To MaxPadsForTests
                        PlantFirst = True
                        If (DisplayPads(i, j, p).ButtonOn) Then
                            If (Trim(PlantTriggerPads(i, j, p, 1).Result) <> "") Then
                                If (Trim(DisplayPads(i, j, p).Result) <> "") Then
                                    If (PlantTriggerPads(i, j, p, 1).ButtonOn) Then
                                        PadName = DisplayPads(i, j, p).ButtonName + "=T" + Trim(str(DisplayPads(i, j, p).ButtonId)) + " <" + Trim(PlantTriggerPads(i, j, p, 1).Result) + "; " + Trim(DisplayPads(i, j, p).Result) + ">"
                                    Else
                                        PadName = DisplayPads(i, j, p).ButtonName + "=T" + Trim(str(DisplayPads(i, j, p).ButtonId)) + " <" + Trim(PlantTriggerPads(i, j, p, 1).Result) + ">"
                                    End If
                                Else
                                    PadName = DisplayPads(i, j, p).ButtonName + "=T" + Trim(str(DisplayPads(i, j, p).ButtonId)) + " <" + Trim(PlantTriggerPads(i, j, p, 1).Result) + ">"
                                End If
                            Else
                                PadName = DisplayPads(i, j, p).ButtonName + "=T" + Trim(str(DisplayPads(i, j, p).ButtonId)) + " <" + Trim(PlantTriggerPads(i, j, p, 1).Result) + "; " + Trim(PlantTriggerPads(i, j, p, 1).Result) + ">"
                                If (Trim(PlantTriggerPads(i, j, p, 1).Result) = "") Then
                                    If (Trim(DisplayPads(i, j, p).Result) <> "") Then
                                        PadName = DisplayPads(i, j, p).ButtonName + "=T" + Trim(str(DisplayPads(i, j, p).ButtonId)) + " <" + Trim(DisplayPads(i, j, p).Result) + ">"
                                    End If
                                End If
                            End If
                            Print #FileNum, PadName
                            For k = 1 To MaxPadChoices
                                If (PlantTriggerPads(i, j, p, k).ButtonOn) Or (PlantTriggerPads(i, j, p, k).Buttons) Then
                                    ButtonName = PlantTriggerPads(i, j, p, k).ButtonName + "=T" + Trim(str(PlantTriggerPads(i, j, p, k).ButtonId)) + " <" + Trim(PlantTriggerPads(i, j, p, k).ButtonName) + ">"
                                    Print #FileNum, ButtonName
                                End If
                            Next k
                        ElseIf (DisplayPads(i, j, p).Buttons) And Not (DisplayPads(i, j, p).ButtonOn) Then
                            PadName = DisplayPads(i, j, p).ButtonName + "=T" + Trim(str(DisplayPads(i, j, p).ButtonId)) + " <" + Trim(DisplayPads(i, j, p).Result) + ">"
                            Print #FileNum, PadName
                            For k = 1 To MaxPadChoices
                                If (PlantTriggerPads(i, j, p, k).ButtonOn) Or (PlantTriggerPads(i, j, p, k).Buttons) Then
                                    ButtonName = PlantTriggerPads(i, j, p, k).ButtonName + "=T" + Trim(str(PlantTriggerPads(i, j, p, k).ButtonId)) + " <" + Trim(PlantTriggerPads(i, j, p, k).ButtonName) + ">"
                                    Print #FileNum, ButtonName
                                End If
                            Next k
                        Else
                            For k = 1 To MaxPadChoices
                                If (PlantTriggerPads(i, j, p, k).ButtonOn) Or (PlantTriggerPads(i, j, p, k).Buttons) Then
                                    ButtonName = PlantTriggerPads(i, j, p, k).ButtonName + "=T" + Trim(str(PlantTriggerPads(i, j, p, k).ButtonId)) + " <" + Trim(PlantTriggerPads(i, j, p, k).ButtonName) + ">"
                                    Print #FileNum, ButtonName
                                End If
                            Next k
                        End If
                    Next p
                End If
            Next j
        End If
    Next i
    FM.CloseFile CLng(FileNum)
    FM.FileCopy ScratchTestFile, StoreResults
    FM.Kill ScratchTestFile
    If (FM.IsFileThere(PrevTestFile)) Then
        FM.Kill PrevTestFile
    End If
    If (frmHome.BackUpOn) Then
        i = FM.InFileNameStartStr(StoreResults, "T")
        If (i > 0) Then
            OtherFile = Left(StoreResults, i - 1) + "Backup\" + Mid(StoreResults, i, Len(StoreResults) - i + 1)
            FM.FileCopy StoreResults, OtherFile
        End If
    End If
    Unload frmRxWear
    FormOn = False
Else
    If (FM.IsFileThere(PrevTestFile)) Then
        FM.Kill PrevTestFile
    End If
    Unload frmRxWear
    FormOn = False
End If
End Sub

Private Sub ClearButtons(DisplayType As Boolean)
cmdItem1.Visible = DisplayType
cmdItem2.Visible = DisplayType
cmdItem3.Visible = DisplayType
cmdItem4.Visible = DisplayType
cmdItem5.Visible = DisplayType
cmdItem6.Visible = DisplayType
cmdItem7.Visible = DisplayType
cmdItem8.Visible = DisplayType
cmdItem9.Visible = DisplayType
cmdItem10.Visible = DisplayType
cmdItem11.Visible = DisplayType
cmdItem12.Visible = DisplayType
cmdItem13.Visible = DisplayType
cmdItem14.Visible = DisplayType
cmdItem15.Visible = DisplayType
cmdItem16.Visible = DisplayType
End Sub

Private Sub ResetButtons()
If (cmdItem1.Tag <> "") Then
    cmdItem1.Visible = True
End If
If (cmdItem2.Tag <> "") Then
    cmdItem2.Visible = True
End If
If (cmdItem3.Tag <> "") Then
    cmdItem3.Visible = True
End If
If (cmdItem4.Tag <> "") Then
    cmdItem4.Visible = True
End If
If (cmdItem5.Tag <> "") Then
    cmdItem5.Visible = True
End If
If (cmdItem6.Tag <> "") Then
    cmdItem6.Visible = True
End If
If (cmdItem7.Tag <> "") Then
    cmdItem7.Visible = True
End If
If (cmdItem8.Tag <> "") Then
    cmdItem8.Visible = True
End If
If (cmdItem9.Tag <> "") Then
    cmdItem9.Visible = True
End If
If (cmdItem10.Tag <> "") Then
    cmdItem10.Visible = True
End If
If (cmdItem11.Tag <> "") Then
    cmdItem11.Visible = True
End If
If (cmdItem12.Tag <> "") Then
    cmdItem12.Visible = True
End If
If (cmdItem13.Tag <> "") Then
    cmdItem13.Visible = True
End If
If (cmdItem14.Tag <> "") Then
    cmdItem14.Visible = True
End If
If (cmdItem15.Tag <> "") Then
    cmdItem15.Visible = True
End If
If (cmdItem16.Tag <> "") Then
    cmdItem16.Visible = True
End If
End Sub

Private Sub ButtonAction(AButton As fpBtn, Ref As Integer)
Dim i As Integer
Dim q As Integer
Dim u As Integer
Dim z As Integer
Dim TestId As Integer
Dim Eye As Integer
Dim Cnt As Integer
If (AButton.BackColor <> ButtonSelectionColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
Else
    AButton.BackColor = OrgButtonColor
    AButton.ForeColor = OrgTextColor
End If
q = InStrPS(AButton.CellTag, "/")
If (q <> 0) Then
    TestId = Val(Left(AButton.CellTag, q - 1))
    If (AButton.ToolTipText = "W") Then
        If (MaxTests > 1) Then
            If (cmdItem1.BackColor = ButtonSelectionColor) Then
                TestId = 1
            ElseIf (cmdItem2.BackColor = ButtonSelectionColor) Then
                TestId = 2
            ElseIf (cmdItem3.BackColor = ButtonSelectionColor) Then
                TestId = 3
            ElseIf (cmdItem4.BackColor = ButtonSelectionColor) Then
                TestId = 4
            End If
        End If
    End If
    u = InStrPS(q + 1, AButton.CellTag, "/")
    If (u <> 0) Then
        Eye = Val(Mid(AButton.CellTag, q + 1, (u - 1) - q))
        Cnt = Val(Mid(AButton.CellTag, u + 1, Len(AButton.CellTag) - u))
    Else
        Exit Sub
    End If
    TheTestId = TestId
Else
    Exit Sub
End If
CurrentEye = Trim(str(Eye))
If (AButton.ToolTipText = "N") Then
    DisplayPads(TestId, Eye, Cnt).ButtonOn = Not (DisplayPads(TestId, Eye, Cnt).ButtonOn)
    DisplayBtns(TestId, Eye).ButtonOn = Not (DisplayBtns(TestId, Eye).ButtonOn)
    If (DisplayPads(TestId, Eye, Cnt).ButtonOn) Then
        If (Trim(DisplayPads(TestId, Eye, Cnt).ButtonTriggerText) <> "") Then
            If (InStrPS(DisplayPads(TestId, Eye, Cnt).ButtonTriggerText, "Pad") <> 0) Then
                Call PadAction(AButton, Ref)
                If (ExitFast) Then
                    Call cmdQuit_Click
                End If
            ElseIf (InStrPS(UCase(DisplayPads(TestId, Eye, Cnt).ButtonTriggerText), "EPAD") <> 0) Then
                Call PadAction(AButton, Ref)
                If (ExitFast) Then
                    Call cmdQuit_Click
                End If
            End If
        Else
            Call PadAction(AButton, Ref)
            If (ExitFast) Then
                Call cmdQuit_Click
            End If
        End If
    End If
ElseIf (AButton.ToolTipText = "R") Then
    DisplayTests(Cnt).ButtonOn = Not (DisplayTests(Cnt).ButtonOn)
    If (DisplayTests(Cnt).ButtonOn) Then
        If (MaxPadsForTests > 0) Then
            For i = 1 To MaxPadsForTests
                Call PlantButton(6, i, 1, TestId, "N", "OD-")
            Next i
            For i = 1 To MaxPadsForTests
                Call PlantButton(14, i, 2, TestId, "N", "OS-")
            Next i
        End If
        If (MaxWriteButtonsForTests(TestId) > 0) Then
            Call PlantButton(22, 1, 1, TestId, "B", "")
            Call PlantButton(22, 2, 2, TestId, "B", "")
        End If
        If (MaxTestWideBtns > 0) Then
            For i = 1 To MaxTestWideBtns
                Call PlantButton(30, i, 0, TestId, "W", " -")
            Next i
        End If
    End If
ElseIf (AButton.ToolTipText = "B") Then
    DisplayBtns(TestId, Cnt).ButtonOn = Not (DisplayBtns(TestId, Cnt).ButtonOn)
    If (DisplayBtns(TestId, Cnt).ButtonOn) Then
        If (Trim(DisplayBtns(TestId, Cnt).ButtonTriggerText) <> "") Then
            If (InStrPS(DisplayBtns(TestId, Cnt).ButtonTriggerText, "Pad") <> 0) Then
                Call PadAction(AButton, Ref)
                If (ExitFast) Then
                    Call cmdQuit_Click
                End If
            End If
        End If
    Else
        DisplayBtns(TestId, Cnt).Result = ""
        DisplayBtns(TestId, Cnt).Buttons = False
        DisplayBtns(TestId, Cnt).ButtonOn = False
        For z = 1 To MaxButtonsAllowed
            PlantTriggerButtons(TestId, Cnt, z).ButtonOn = False
            PlantTriggerButtons(TestId, Cnt, z).Buttons = False
        Next z
    End If
ElseIf (AButton.ToolTipText = "W") Then
    TestWideButtons(TestId, Cnt).ButtonOn = Not (TestWideButtons(TestId, Cnt).ButtonOn)
    If (TestWideButtons(TestId, Cnt).ButtonMutual) Then
        If (TestWideButtons(TestId, Cnt).ButtonOn) Then
            For z = 1 To MaxTestWideBtns
                If (z <> Cnt) Then
                    TestWideButtons(TestId, z).ButtonOn = False
                    TestWideButtons(TestId, z).Buttons = False
                Else
                    TestWideButtons(TestId, z).ButtonOn = True
                    TestWideButtons(TestId, z).Buttons = True
                End If
            Next z
        End If
    End If
End If
UseManifest = False
End Sub

Private Sub PadAction(APad As fpBtn, Ref As Integer)
Dim ATemp As String
Dim i As Integer, q As Integer
Dim u As Integer, g As Integer
Dim TestId As Integer, Temp As Integer
Dim Eye As Integer, Cnt As Integer
Dim iMax As Double, iMin As Double, iDefault As Double
UseManifest = False
g = InStrPS(APad.Tag, "/")
If (g = 0) Then
    iMax = 0
    iMin = 0
    iDefault = 0
Else
    iMax = Val(Left(APad.Tag, g - 1))
    q = InStrPS(g + 1, APad.Tag, "/")
    If (q = 0) Then
        iMin = Val(Mid(APad.Tag, g + 1, Len(APad.Tag) - g))
        iDefault = iMin
    Else
        iMin = Val(Mid(APad.Tag, g + 1, (q - 1) - g))
        If (Mid(APad.Tag, q + 1, Len(APad.Tag) - q) = "?") Then
            iDefault = -9999
        Else
            iDefault = Val(Mid(APad.Tag, q + 1, Len(APad.Tag) - q))
        End If
    End If
End If
q = InStrPS(APad.CellTag, "/")
If (q <> 0) Then
    TestId = Val(Left(APad.CellTag, q - 1))
    u = InStrPS(q + 1, APad.CellTag, "/")
    If (u <> 0) Then
        Eye = Val(Mid(APad.CellTag, q + 1, (u - 1) - q))
        Cnt = Val(Mid(APad.CellTag, u + 1, Len(APad.CellTag) - u))
    Else
        Exit Sub
    End If
    TheTestId = TestId
Else
    Exit Sub
End If
CurrentEye = Trim(str(Eye))
ATemp = Trim(APad.Text)
Call ReplaceCharacters(ATemp, vbCrLf, " ")
If (Trim(DisplayPads(TestId, Eye, Cnt).ButtonTriggerText) <> "Epad") Then
    frmNumericPad.NumPad_Result = ""
    frmNumericPad.NumPad_Field = Trim(ATemp)
    frmNumericPad.NumPad_Max = iMax
    frmNumericPad.NumPad_Min = iMin
    frmNumericPad.NumPad_Default = iDefault
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_Increment = DisplayPads(TestId, Eye, Cnt).ButtonIncrement
    If (Left(PlantTriggerPads(TestId, Eye, Cnt, 1).ButtonText, 2) = "OD") Then
        frmNumericPad.NumPad_Choice1 = ""
    Else
        frmNumericPad.NumPad_Choice1 = PlantTriggerPads(TestId, Eye, Cnt, 1).ButtonText
    End If
    If (Left(PlantTriggerPads(TestId, Eye, Cnt, 2).ButtonText, 2) = "OS") Then
        frmNumericPad.NumPad_Choice2 = ""
    Else
        frmNumericPad.NumPad_Choice2 = PlantTriggerPads(TestId, Eye, Cnt, 2).ButtonText
    End If
    frmNumericPad.NumPad_Choice3 = PlantTriggerPads(TestId, Eye, Cnt, 3).ButtonText
    frmNumericPad.NumPad_Choice4 = PlantTriggerPads(TestId, Eye, Cnt, 4).ButtonText
    frmNumericPad.NumPad_Choice5 = PlantTriggerPads(TestId, Eye, Cnt, 5).ButtonText
    frmNumericPad.NumPad_Choice6 = PlantTriggerPads(TestId, Eye, Cnt, 6).ButtonText
    frmNumericPad.NumPad_Choice7 = PlantTriggerPads(TestId, Eye, Cnt, 7).ButtonText
    frmNumericPad.NumPad_Choice8 = PlantTriggerPads(TestId, Eye, Cnt, 8).ButtonText
    frmNumericPad.NumPad_Choice9 = PlantTriggerPads(TestId, Eye, Cnt, 9).ButtonText
    frmNumericPad.NumPad_Choice10 = PlantTriggerPads(TestId, Eye, Cnt, 10).ButtonText
    frmNumericPad.NumPad_Choice11 = PlantTriggerPads(TestId, Eye, Cnt, 11).ButtonText
    frmNumericPad.NumPad_Choice12 = PlantTriggerPads(TestId, Eye, Cnt, 12).ButtonText
    frmNumericPad.NumPad_Choice13 = PlantTriggerPads(TestId, Eye, Cnt, 13).ButtonText
    frmNumericPad.NumPad_Choice14 = PlantTriggerPads(TestId, Eye, Cnt, 14).ButtonText
    frmNumericPad.NumPad_Choice15 = PlantTriggerPads(TestId, Eye, Cnt, 15).ButtonText
    frmNumericPad.NumPad_Choice16 = PlantTriggerPads(TestId, Eye, Cnt, 16).ButtonText
    frmNumericPad.NumPad_Choice17 = PlantTriggerPads(TestId, Eye, Cnt, 17).ButtonText
    frmNumericPad.NumPad_Choice18 = PlantTriggerPads(TestId, Eye, Cnt, 18).ButtonText
    frmNumericPad.NumPad_Choice19 = PlantTriggerPads(TestId, Eye, Cnt, 19).ButtonText
    frmNumericPad.NumPad_Choice20 = PlantTriggerPads(TestId, Eye, Cnt, 20).ButtonText
    frmNumericPad.NumPad_Choice21 = PlantTriggerPads(TestId, Eye, Cnt, 21).ButtonText
    frmNumericPad.NumPad_Choice22 = PlantTriggerPads(TestId, Eye, Cnt, 22).ButtonText
    frmNumericPad.NumPad_Choice23 = PlantTriggerPads(TestId, Eye, Cnt, 23).ButtonText
    frmNumericPad.NumPad_Choice24 = PlantTriggerPads(TestId, Eye, Cnt, 24).ButtonText
    frmNumericPad.Show 1
    If Not (frmNumericPad.NumPad_Quit) Then
        If (Trim(frmNumericPad.NumPad_Result) <> "") Then
            If (frmNumericPad.NumPad_Result <> "-") Then
                If Not (frmNumericPad.NumPad_NoValue) Then
                    PlantTriggerPads(TestId, Eye, Cnt, 1).Result = frmNumericPad.NumPad_Result
                Else
                    PlantTriggerPads(TestId, Eye, Cnt, 1).Result = "-"
                End If
            Else
                PlantTriggerPads(TestId, Eye, Cnt, 1).Result = ""
                DisplayPads(TestId, Eye, Cnt).Result = ""
                DisplayPads(TestId, Eye, Cnt).ButtonOn = False
                DisplayPads(TestId, Eye, Cnt).Buttons = False
            End If
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 1).Result = ""
            DisplayPads(TestId, Eye, Cnt).Result = ""
            DisplayPads(TestId, Eye, Cnt).ButtonOn = False
            DisplayPads(TestId, Eye, Cnt).Buttons = False
        End If
        For i = 1 To 24
            PlantTriggerPads(TestId, Eye, Cnt, i).Buttons = False
            PlantTriggerPads(TestId, Eye, Cnt, i).ButtonOn = False
        Next i
        If (frmNumericPad.NumPad_ChoiceOn1) Then
            PlantTriggerPads(TestId, Eye, Cnt, 1).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 1).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn2) Then
            PlantTriggerPads(TestId, Eye, Cnt, 2).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 2).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn3) Then
            PlantTriggerPads(TestId, Eye, Cnt, 3).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 3).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn4) Then
            PlantTriggerPads(TestId, Eye, Cnt, 4).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 4).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn5) Then
            PlantTriggerPads(TestId, Eye, Cnt, 5).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 5).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn6) Then
            PlantTriggerPads(TestId, Eye, Cnt, 6).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 6).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn7) Then
            PlantTriggerPads(TestId, Eye, Cnt, 7).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 7).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn8) Then
            PlantTriggerPads(TestId, Eye, Cnt, 8).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 8).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn9) Then
            PlantTriggerPads(TestId, Eye, Cnt, 9).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 9).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn10) Then
            PlantTriggerPads(TestId, Eye, Cnt, 10).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 10).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn11) Then
            PlantTriggerPads(TestId, Eye, Cnt, 11).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 11).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn12) Then
            PlantTriggerPads(TestId, Eye, Cnt, 12).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 12).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn13) Then
            PlantTriggerPads(TestId, Eye, Cnt, 13).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 13).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn14) Then
            PlantTriggerPads(TestId, Eye, Cnt, 14).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 14).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn15) Then
            PlantTriggerPads(TestId, Eye, Cnt, 15).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 15).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn16) Then
            PlantTriggerPads(TestId, Eye, Cnt, 16).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 16).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn17) Then
            PlantTriggerPads(TestId, Eye, Cnt, 17).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 17).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn18) Then
            PlantTriggerPads(TestId, Eye, Cnt, 18).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 18).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn19) Then
            PlantTriggerPads(TestId, Eye, Cnt, 19).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 19).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn20) Then
            PlantTriggerPads(TestId, Eye, Cnt, 20).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 20).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn21) Then
            PlantTriggerPads(TestId, Eye, Cnt, 21).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 21).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn22) Then
            PlantTriggerPads(TestId, Eye, Cnt, 22).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 22).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn23) Then
            PlantTriggerPads(TestId, Eye, Cnt, 23).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 23).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn24) Then
            PlantTriggerPads(TestId, Eye, Cnt, 24).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 24).ButtonOn = False
        End If
    Else
        ExitFast = True
    End If
Else
    If (Eye = 1) Then
        frmTestTrack.EyeContext = "OD"
    Else
        frmTestTrack.EyeContext = "OS"
    End If
    frmTestTrack.AppointmentId = AppointmentId
    frmTestTrack.PatientId = PatientId
    If (frmTestTrack.LoadTestTrack(CurrentFormId)) Then
        frmTestTrack.Show 1
        For i = 1 To frmTestTrack.TotalFindings
            If (i = 1) Then
                If (Trim(DisplayPads(TestId, Eye, Cnt).Result) <> "") Then
                    DisplayPads(TestId, Eye, Cnt).Result = ""
                End If
            End If
            If (frmTestTrack.GetFindingValue(i, ATemp)) Then
                DisplayPads(TestId, Eye, Cnt).ButtonOn = True
                DisplayPads(TestId, Eye, Cnt).Result = DisplayPads(TestId, Eye, Cnt).Result + ATemp
            End If
        Next i
        For i = 1 To frmTestTrack.TotalImpressions
            If (frmTestTrack.GetImpressionValue(i, ATemp)) Then
                DisplayPads(TestId, Eye, Cnt).ButtonOn = True
                DisplayPads(TestId, Eye, Cnt).Result = DisplayPads(TestId, Eye, Cnt).Result + ATemp
            End If
        Next i
    Else
        ExitFast = True
    End If
End If
End Sub

Private Sub BtnAction(AButton As fpBtn, Ref As Integer)
Dim q As Integer
Dim u As Integer
Dim TestId As Integer
Dim Eye As Integer
Dim Cnt As Integer
If (AButton.BackColor <> ButtonSelectionColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
Else
    AButton.BackColor = OrgButtonColor
    AButton.ForeColor = OrgTextColor
End If
q = InStrPS(AButton.CellTag, "/")
If (q <> 0) Then
    TestId = Val(Left(AButton.CellTag, q - 1))
    u = InStrPS(q + 1, AButton.CellTag, "/")
    If (u <> 0) Then
        Eye = Val(Mid(AButton.CellTag, q + 1, (u - 1) - q))
        Cnt = Val(Mid(AButton.CellTag, u + 1, Len(AButton.CellTag) - u))
    Else
        Exit Sub
    End If
    TheTestId = TestId
Else
    Exit Sub
End If
CurrentEye = Trim(str(Eye))
UseManifest = False
PlantTriggerButtons(TestId, Eye, Cnt).ButtonOn = Not (PlantTriggerButtons(TestId, Eye, Cnt).ButtonOn)
End Sub

Private Sub cmdItem1_Click()
Call ButtonAction(cmdItem1, 1)
End Sub

Private Sub cmdItem2_Click()
Call ButtonAction(cmdItem2, 2)
End Sub

Private Sub cmdItem3_Click()
Call ButtonAction(cmdItem3, 3)
End Sub

Private Sub cmdItem4_Click()
Call ButtonAction(cmdItem4, 4)
End Sub

Private Sub cmdItem5_Click()
Call ButtonAction(cmdItem5, 5)
End Sub

Private Sub cmdItem6_Click()
Call ButtonAction(cmdItem6, 6)
End Sub

Private Sub cmdItem7_Click()
Call ButtonAction(cmdItem7, 7)
End Sub

Private Sub cmdItem8_Click()
Call ButtonAction(cmdItem8, 8)
End Sub

Private Sub cmdItem9_Click()
Call ButtonAction(cmdItem9, 9)
End Sub

Private Sub cmdItem10_Click()
Call ButtonAction(cmdItem10, 10)
End Sub

Private Sub cmdItem11_Click()
Call ButtonAction(cmdItem11, 11)
End Sub

Private Sub cmdItem12_Click()
Call ButtonAction(cmdItem12, 12)
End Sub

Private Sub cmdItem13_Click()
Call ButtonAction(cmdItem13, 13)
End Sub

Private Sub cmdItem14_Click()
Call ButtonAction(cmdItem14, 14)
End Sub

Private Sub cmdItem15_Click()
Call ButtonAction(cmdItem15, 15)
End Sub

Private Sub cmdItem16_Click()
Call ButtonAction(cmdItem16, 16)
End Sub

Private Sub PlantButton(Ref As Integer, Counter As Integer, Eye As Integer, TestId As Integer, IType As String, TheText As String)
Dim Temp As Integer
Dim AButton As fpBtn
Temp = Counter + Ref
If (Temp = 1) Then
    Set AButton = cmdItem1
ElseIf (Temp = 2) Then
    Set AButton = cmdItem2
ElseIf (Temp = 3) Then
    Set AButton = cmdItem3
ElseIf (Temp = 4) Then
    Set AButton = cmdItem4
ElseIf (Temp = 5) Then
    Set AButton = cmdItem5
ElseIf (Temp = 6) Then
    Set AButton = cmdItem6
ElseIf (Temp = 7) Then
    Set AButton = cmdItem7
ElseIf (Temp = 8) Then
    Set AButton = cmdItem8
ElseIf (Temp = 9) Then
    Set AButton = cmdItem9
ElseIf (Temp = 10) Then
    Set AButton = cmdItem10
ElseIf (Temp = 11) Then
    Set AButton = cmdItem11
ElseIf (Temp = 12) Then
    Set AButton = cmdItem12
ElseIf (Temp = 13) Then
    Set AButton = cmdItem13
ElseIf (Temp = 14) Then
    Set AButton = cmdItem14
ElseIf (Temp = 15) Then
    Set AButton = cmdItem15
ElseIf (Temp = 16) Then
    Set AButton = cmdItem16
Else
    Exit Sub
End If
With AButton
    .BackColor = OrgButtonColor
    .ForeColor = OrgTextColor
    .Visible = True
    .CellTag = Trim(str(TestId)) + "/" + Trim(str(Eye)) + "/" + Trim(str(Counter))
    .Font.Size = 10
    If (IType = "R") Then
        .Text = Trim(TheText) + DisplayTests(Counter).ButtonText
        .Tag = DisplayTests(Counter).ButtonName
        .ToolTipText = "R"
    ElseIf (IType = "N") Then
        .Text = Trim(TheText) + DisplayPads(TestId, Eye, Counter).ButtonText
        .Tag = DisplayPads(TestId, Eye, Counter).ButtonRange
        .ToolTipText = "N"
    ElseIf (IType = "W") Then
        .Text = Trim(TheText) + TestWideButtons(TestId, Counter).ButtonText
        .Tag = TestWideButtons(TestId, Counter).ButtonName
        .ToolTipText = "W"
    Else
        .Text = Trim(TheText) + DisplayBtns(TestId, Counter).ButtonText
        .Tag = DisplayBtns(TestId, Counter).ButtonName
        .ToolTipText = "B"
    End If
End With
End Sub

Private Function DynamicEntry(Question As String) As Boolean
Dim i As Integer
Dim AltEyeOD As String
Dim AltEyeOS As String
Dim TheForm As DynamicForms
DynamicEntry = False
MaxTests = 0
MaxPadsForTests = 0
MaxButtonsForTests = 0
MaxButtonsForPads = 0
MaxButtonsForButtons = 0
MaxTestWideBtns = 0
AltEyeOD = "OD-"
AltEyeOS = "OS-"
Set TheForm = New DynamicForms
TheForm.Question = UCase(Trim(Question))
If (TheForm.RetrieveForm) Then
    If (TheForm.FormId > 0) Then
        CurrentFormId = TheForm.FormId
        CurrentOrder = ""
        i = InStrPS(StoreResults, "_")
        If (i > 0) Then
            CurrentOrder = Mid(StoreResults, i - 3, 3)
        End If
        Call GetTestWideButtons(CurrentFormId, MaxTestWideBtns)
        Call GetTestOccurrences(CurrentFormId, MaxTests)
        If (MaxTests > 1) Then
            TheTestId = 0
            For i = 1 To MaxTests
                Call PlantButton(0, i, 0, i, "R", "")
            Next i
            If (MaxTestWideBtns > 0) Then
                For i = 1 To MaxTestWideBtns
                    Call PlantButton(30, i, 0, 1, "W", "  -")
                Next i
            End If
            Call LoadFileContent
            DynamicEntry = True
        ElseIf (MaxTests > 0) Then
            If (DisplayPads(1, 1, 1).ButtonUseOtherText) Then
               AltEyeOD = Trim(DisplayBtns(1, 1).ButtonText) + "-"
               AltEyeOS = Trim(DisplayBtns(1, 2).ButtonText) + "-"
            End If
            TheTestId = 1
            If (MaxPadsForTests > 0) Then
                For i = 1 To MaxPadsForTests
                    If (DisplayBtns(1, 1).ButtonVisible) Then
                        Call PlantButton(6, i, 1, 1, "N", AltEyeOD)
                    Else
                        Call PlantButton(6, i, 1, 1, "N", "")
                    End If
                Next i
                For i = 1 To MaxPadsForTests
                    If (DisplayBtns(1, 2).ButtonVisible) Then
                        Call PlantButton(14, i, 2, 1, "N", AltEyeOS)
                    End If
                Next i
            End If
            If (MaxWriteButtonsForTests(1) > 0) Then
                Call PlantButton(22, 1, 1, 1, "B", "")
                Call PlantButton(22, 2, 2, 1, "B", "")
            End If
            If (MaxTestWideBtns > 0) Then
                For i = 1 To MaxTestWideBtns
                    Call PlantButton(30, i, 0, 1, "W", " -")
                Next i
            End If
            Call LoadFileContent
            DynamicEntry = True
        End If
    End If
End If
Set TheForm = Nothing
End Function

Private Sub cmdRemove_Click()
Dim i As Integer
Dim PurgeIt As Boolean
frmEventMsgs.Header = "Remove Test Results"
frmEventMsgs.AcceptText = "OD"
frmEventMsgs.RejectText = "OS"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = "All"
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    If (TheTestId = 0) Then
        For i = 1 To MaxTests
            If (Trim(DisplayTests(i).ButtonName) <> "") Then
                RemoveOD(i) = True
            End If
        Next i
    Else
        RemoveOD(TheTestId) = True
    End If
    Call cmdApply_Click
ElseIf (frmEventMsgs.Result = 2) Then
    If (TheTestId = 0) Then
        For i = 1 To MaxTests
            If (Trim(DisplayTests(i).ButtonName) <> "") Then
                RemoveOS(i) = True
            End If
        Next i
    Else
        RemoveOS(TheTestId) = True
    End If
    Call cmdApply_Click
ElseIf (frmEventMsgs.Result = 3) Then
    PurgeIt = True
    If (frmSystems1.EditPreviousOn) Then
        frmEventMsgs.Header = "This will permanently remove this Test Result from the patient's record. Are you sure ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Yes"
        frmEventMsgs.CancelText = "No"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result <> 2) Then
            PurgeIt = False
        End If
    End If
    If (PurgeIt) Then
        If (FM.IsFileThere(StoreResults)) Then
            FM.Kill StoreResults
        End If
        If (FM.IsFileThere(ScratchTestFile)) Then
            FM.Kill ScratchTestFile
        End If
        If (FM.IsFileThere(PrevTestFile)) Then
            FM.Kill PrevTestFile
        End If
        If (frmSystems1.EditPreviousOn) Then
            Call frmSystems1.DeleteOriginalTest(Question, AppointmentId, PatientId)
        End If
        Unload frmEditTests
        FormOn = False
    End If
End If
End Sub

Private Sub Form_Load()
Dim z As Integer
QuitOn = False
UseManifest = True
If (Trim(ManifestResults) = "") Then
    UseManifest = False
End If
CurrentEye = ""
frmEditTests.KeyPreview = True
ButtonSelectionColor = 14745312
TextSelectionColor = 0
OrgButtonColor = cmdItem1.BackColor
OrgTextColor = cmdItem1.ForeColor
ExitFast = False
frmEditTests.MousePointer = 2
frmEditTests.ZOrder 0
z = InStrPS(StoreResults, ".")
If (z <> 0) Then
    ScratchTestFile = Left(StoreResults, z - 1) + ".tmp"
    PrevTestFile = Left(StoreResults, z - 1) + ".prv"
Else
    ScratchTestFile = StoreResults + ".tmp"
    PrevTestFile = StoreResults + ".prv"
End If
If (DisplayOriginalResults(StoreResults)) Then
    Call ClearButtons(False)
    Call DynamicEntry(Question)
End If
If (FM.InFileNameStartStr(StoreResults, "G") > 0) Then
    cmdRemove.Visible = False
End If
For z = 1 To MaxTestsAllowed
    RemoveOD(z) = False
    RemoveOS(z) = False
Next z
End Sub

Private Function DisplayOriginalResults(FileName As String) As Boolean
Dim TheRecord As String
Dim FileNum As Integer
Dim TheText As String
Dim DispText As String
DisplayOriginalResults = False
If (FM.IsFileThere(StoreResults)) Then
    FileNum = FreeFile
    FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    Do Until (EOF(FileNum))
        Line Input #FileNum, TheRecord
        If (InStrPS(TheRecord, "Recap=") <> 0) Then
            TheText = TheText + Mid(TheRecord, 7, Len(TheRecord) - 6)
        End If
    Loop
    FM.CloseFile CLng(FileNum)
    Call ConvertButtonDisplay(TheText, DispText)
    DisplayOriginalResults = True
End If
End Function

Private Function GetTimeRef() As Boolean
Dim i As Integer
GetTimeRef = True
For i = 1 To MaxTestsAllowed
    TimeRef(i).Buttons = False
    TimeRef(i).ButtonOn = False
    TimeRef(i).ButtonName = ""
    TimeRef(i).ButtonText = ""
    TimeRef(i).ButtonRange = ""
    TimeRef(i).ButtonTrigger = ""
    TimeRef(i).ButtonTriggerText = ""
    TimeRef(i).ButtonId = 0
    TimeRef(i).Result = ""
Next i
End Function

Private Function GetTestLabel(FormId As Long) As String
Dim AControl As DynamicControls
Set AControl = New DynamicControls
GetTestLabel = ""
If (FormId > 0) Then
    AControl.FormId = FormId
    AControl.ControlType = "L"
    AControl.ControlAlternateText = ""
    AControl.ControlVisibleAfterTrigger = False
    If (AControl.FindTriggerControl > 0) Then
        If (AControl.SelectControl(1)) Then
            GetTestLabel = Trim(AControl.ControlText)
        End If
    End If
End If
Set AControl = Nothing
End Function

Private Function GetTestWideButtons(FormId As Long, Counter As Integer) As Boolean
Dim i As Integer
Dim q As Integer
Dim VisualText As String
Dim AControl As DynamicControls
For i = 1 To MaxTestsAllowed
    TimeRef(i).Buttons = False
    TimeRef(i).ButtonOn = False
    TimeRef(i).ButtonName = ""
    TimeRef(i).ButtonText = ""
    TimeRef(i).ButtonRange = ""
    TimeRef(i).ButtonTrigger = ""
    TimeRef(i).ButtonTriggerText = ""
    TimeRef(i).ButtonId = 0
    TimeRef(i).ButtonMutual = False
    TimeRef(i).ButtonPadOrder = False
    TimeRef(i).ButtonUseOtherText = False
    TimeRef(i).ButtonIncrement = ""
    TimeRef(i).ButtonDeviceRef = ""
    TimeRef(i).Result = ""
Next i
For q = 1 To MaxTestsAllowed
    For i = 1 To MaxButtonsAllowed
        TestWideButtons(q, i).Buttons = False
        TestWideButtons(q, i).ButtonOn = False
        TestWideButtons(q, i).ButtonName = ""
        TestWideButtons(q, i).ButtonText = ""
        TestWideButtons(q, i).ButtonRange = ""
        TestWideButtons(q, i).ButtonTrigger = ""
        TestWideButtons(q, i).ButtonTriggerText = ""
        TestWideButtons(q, i).ButtonId = 0
        TestWideButtons(q, i).ButtonMutual = False
        TestWideButtons(q, i).ButtonPadOrder = False
        TestWideButtons(q, i).ButtonUseOtherText = False
        TestWideButtons(q, i).ButtonIncrement = ""
        TestWideButtons(q, i).ButtonDeviceRef = ""
        TestWideButtons(q, i).Result = ""
    Next i
Next q
Set AControl = New DynamicControls
If (FormId > 0) Then
    AControl.FormId = FormId
    AControl.ControlType = "B"
    AControl.ControlAlternateText = ""
    AControl.ControlVisibleAfterTrigger = False
    If (AControl.FindTriggerControl > 0) Then
        i = 1
        While (AControl.SelectControl(i))
            If Not (AControl.IgnoreIt) Then
                If (Left(AControl.ControlName, 3) <> "OS-") And (Left(AControl.ControlName, 3) <> "OD-") And (Trim(AControl.ControlAlternateText) = "") Then
                    Counter = Counter + 1
                    If (Trim(AControl.ControlText) <> "") Then
                        q = InStrPS(AControl.ControlText, "^")
                        If (q <> 0) Then
                            VisualText = Left(AControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(AControl.ControlText, q + 1, Len(AControl.ControlText) - q)
                        Else
                            VisualText = AControl.ControlText
                        End If
                    Else
                        VisualText = AControl.ControlName
                    End If
                    For q = 1 To MaxTestsAllowed
                        TestWideButtons(q, Counter).ButtonId = AControl.ControlId
                        TestWideButtons(q, Counter).ButtonText = VisualText
                        If (Trim(AControl.IEChoiceName) <> "") And (Trim(AControl.IEChoiceName) <> "M") Then
                            TestWideButtons(q, Counter).ButtonName = Trim(AControl.IEChoiceName)
                        Else
                            TestWideButtons(q, Counter).ButtonName = "*" + Trim(AControl.ControlName)
                        End If
                        TestWideButtons(q, Counter).ButtonType = "W"
                        TestWideButtons(q, Counter).ButtonTrigger = Trim(AControl.ControlAlternateTrigger)
                        TestWideButtons(q, Counter).ButtonTriggerText = Trim(AControl.ControlAlternateText)
                        TestWideButtons(q, Counter).Buttons = False
                        TestWideButtons(q, Counter).ButtonOn = False
                        TestWideButtons(q, Counter).ButtonMutual = False
                        TestWideButtons(q, Counter).ButtonIncrement = Trim(AControl.AIncrement)
                        TestWideButtons(q, Counter).ButtonDeviceRef = Trim(AControl.ICDAlias4)
                        If (Trim(AControl.IEChoiceName) = "M") Then
                            TestWideButtons(q, Counter).ButtonMutual = True
                        End If
                        If (Trim(AControl.DecimalDefault) = "T") Then
                            TestWideButtons(q, Counter).ButtonPadOrder = True
                        End If
                        If (Trim(AControl.ICDAlias4) = "T") Then
                            TestWideButtons(q, Counter).ButtonUseOtherText = True
                        End If
                    Next q
                End If
            End If
            i = i + 1
        Wend
    End If
End If
Set AControl = Nothing
End Function

Private Function GetTestOccurrences(FormId As Long, Counter As Integer) As Boolean
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim l As Integer
Dim q As Integer
Dim VisualText As String
Dim TheControl As DynamicControls
For i = 1 To MaxTestsAllowed
    DisplayTests(i).Buttons = False
    DisplayTests(i).ButtonOn = False
    DisplayTests(i).ButtonName = ""
    DisplayTests(i).ButtonText = ""
    DisplayTests(i).ButtonRange = ""
    DisplayTests(i).ButtonTrigger = ""
    DisplayTests(i).ButtonTriggerText = ""
    DisplayTests(i).ButtonId = 0
    DisplayTests(i).ButtonMutual = False
    DisplayTests(i).ButtonPadOrder = False
    DisplayTests(i).ButtonUseOtherText = False
    DisplayTests(i).ButtonIncrement = ""
    DisplayTests(i).ButtonDeviceRef = ""
    DisplayTests(i).Result = ""
    MaxWriteButtonsForTests(i) = 0
    For j = 1 To 2
        For k = 1 To MaxButtonsAllowed
            PlantTriggerButtons(i, j, k).Buttons = False
            PlantTriggerButtons(i, j, k).ButtonOn = False
            PlantTriggerButtons(i, j, k).ButtonName = ""
            PlantTriggerButtons(i, j, k).ButtonText = ""
            PlantTriggerButtons(i, j, k).ButtonRange = ""
            PlantTriggerButtons(i, j, k).ButtonTrigger = ""
            PlantTriggerButtons(i, j, k).ButtonTriggerText = ""
            PlantTriggerButtons(i, j, k).ButtonId = 0
            PlantTriggerButtons(i, j, k).ButtonMutual = False
            PlantTriggerButtons(i, j, k).ButtonPadOrder = False
            PlantTriggerButtons(i, j, k).ButtonUseOtherText = False
            PlantTriggerButtons(i, j, k).ButtonIncrement = ""
            PlantTriggerButtons(i, j, k).ButtonDeviceRef = ""
            PlantTriggerButtons(i, j, k).Result = ""
        Next k
    Next j
Next i
For i = 1 To MaxTestsAllowed
    For j = 1 To 2
        For k = 1 To MaxPadsAllowed
            For l = 1 To MaxPadChoices
                PlantTriggerPads(i, j, k, l).Buttons = False
                PlantTriggerPads(i, j, k, l).ButtonOn = False
                PlantTriggerPads(i, j, k, l).ButtonName = ""
                PlantTriggerPads(i, j, k, l).ButtonText = ""
                PlantTriggerPads(i, j, k, l).ButtonRange = ""
                PlantTriggerPads(i, j, k, l).ButtonTrigger = ""
                PlantTriggerPads(i, j, k, l).ButtonTriggerText = ""
                PlantTriggerPads(i, j, k, l).ButtonId = 0
                PlantTriggerPads(i, j, k, l).ButtonMutual = False
                PlantTriggerPads(i, j, k, l).ButtonPadOrder = False
                PlantTriggerPads(i, j, k, l).ButtonUseOtherText = False
                PlantTriggerPads(i, j, k, l).ButtonIncrement = ""
                PlantTriggerPads(i, j, k, l).ButtonDeviceRef = ""
                PlantTriggerPads(i, j, k, l).Result = ""
            Next l
        Next k
    Next j
Next i
Set TheControl = New DynamicControls
If (FormId > 0) Then
    TheControl.FormId = FormId
    TheControl.ControlType = "R"
    TheControl.ControlAlternateText = ""
    TheControl.ControlVisibleAfterTrigger = False
    If (TheControl.FindTriggerControl > 0) Then
        i = 1
        While (TheControl.SelectControl(i))
            If Not (TheControl.IgnoreIt) Then
                Counter = Counter + 1
                If (Trim(TheControl.ControlText) <> "") Then
                    q = InStrPS(TheControl.ControlText, "^")
                    If (q <> 0) Then
                        VisualText = Left(TheControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(TheControl.ControlText, q + 1, Len(TheControl.ControlText) - q)
                    Else
                        VisualText = TheControl.ControlText
                    End If
                Else
                    VisualText = TheControl.ControlName
                End If
                DisplayTests(Counter).ButtonId = TheControl.ControlId
                DisplayTests(Counter).ButtonText = VisualText
                If (Trim(TheControl.IEChoiceName) <> "") Then
                    DisplayTests(Counter).ButtonName = Trim(TheControl.IEChoiceName)
                Else
                    DisplayTests(Counter).ButtonName = "*" + Trim(TheControl.ControlName)
                End If
                DisplayTests(Counter).ButtonType = Trim(TheControl.ControlType)
                DisplayTests(Counter).ButtonTrigger = Trim(TheControl.ControlAlternateTrigger)
                DisplayTests(Counter).ButtonTriggerText = "Btn1"
                DisplayTests(Counter).ButtonOn = False
                DisplayTests(Counter).Buttons = False
                DisplayTests(Counter).ButtonMutual = False
                DisplayTests(Counter).ButtonPadOrder = False
                DisplayTests(Counter).ButtonUseOtherText = False
                DisplayTests(Counter).ButtonIncrement = Trim(TheControl.AIncrement)
                DisplayTests(Counter).ButtonDeviceRef = Trim(TheControl.ICDAlias4)
                MaxPadsForTests = 0
                Call GetPadsForTest(FormId, Counter, MaxPadsForTests)
                Call GetButtonsForTests(FormId, Counter, MaxButtonsForTests)
            End If
            i = i + 1
        Wend
    Else
        Counter = 1
        DisplayTests(Counter).ButtonOn = True
        DisplayTests(Counter).Buttons = True
        DisplayTests(Counter).ButtonName = "*"
        Call GetPadsForTest(FormId, Counter, MaxPadsForTests)
        Call GetButtonsForTests(FormId, Counter, MaxButtonsForTests)
    End If
End If
Set TheControl = Nothing
End Function

Private Function GetPadsForTest(FormId As Long, TestId As Integer, Counter As Integer) As Boolean
Dim u As Integer
Dim i As Integer
Dim q As Integer
Dim g As Integer
Dim TheControl As DynamicControls
Dim VisualText As String
Dim iDefault As Double
Dim iMax As Double
Dim iMin As Double
For q = 1 To 2
    For i = 1 To MaxPadsAllowed
        DisplayPads(TestId, q, i).Buttons = False
        DisplayPads(TestId, q, i).ButtonOn = False
        DisplayPads(TestId, q, i).ButtonName = ""
        DisplayPads(TestId, q, i).ButtonText = ""
        DisplayPads(TestId, q, i).ButtonRange = ""
        DisplayPads(TestId, q, i).ButtonTrigger = ""
        DisplayPads(TestId, q, i).ButtonTriggerText = ""
        DisplayPads(TestId, q, i).ButtonId = 0
        DisplayPads(TestId, q, i).ButtonMutual = False
        DisplayPads(TestId, q, i).ButtonPadOrder = False
        DisplayPads(TestId, q, i).ButtonUseOtherText = False
        DisplayPads(TestId, q, i).ButtonIncrement = ""
        DisplayPads(TestId, q, i).ButtonDeviceRef = ""
        DisplayPads(TestId, q, i).Result = ""
    Next i
Next q
Set TheControl = New DynamicControls
For u = 1 To 2
    If (FormId > 0) Then
        TheControl.FormId = FormId
        TheControl.ControlType = "N"
        TheControl.ControlAlternateText = ""
        If (TheControl.FindTriggerControl > 0) Then
            i = 1
            Counter = 0
            While (TheControl.SelectControl(i))
                If Not (TheControl.IgnoreIt) Then
                    Counter = Counter + 1
                    If (Trim(TheControl.ControlText) <> "") Then
                        q = InStrPS(TheControl.ControlText, "^")
                        If (q <> 0) Then
                            VisualText = Left(TheControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(TheControl.ControlText, q + 1, Len(TheControl.ControlText) - q)
                        Else
                            VisualText = TheControl.ControlText
                        End If
                    Else
                        VisualText = TheControl.ControlName
                    End If
                    q = InStrPS(TheControl.ControlTemplateName, "/")
                    If (q <> 0) Then
                        iMin = Val(Trim(Left(TheControl.ControlTemplateName, q - 1)))
                        g = InStrPS(q + 1, TheControl.ControlTemplateName, "/")
                        If (g = 0) Then
                            iMax = Val(Trim(Mid(TheControl.ControlTemplateName, q + 1, Len(TheControl.ControlTemplateName) - q)))
                            iDefault = iMin
                        Else
                            iMax = Val(Trim(Mid(TheControl.ControlTemplateName, q + 1, (g - 1) - q)))
                            If (Mid(TheControl.ControlTemplateName, g + 1, Len(TheControl.ControlTemplateName) - g) = "?") Then
                                iDefault = -9999
                            Else
                                iDefault = Val(Trim(Mid(TheControl.ControlTemplateName, g + 1, Len(TheControl.ControlTemplateName) - g)))
                            End If
                        End If
                    Else
                        iMin = 0
                        iMax = Val(Trim(Mid(TheControl.ControlTemplateName, q + 1, Len(TheControl.ControlTemplateName) - q)))
                        iDefault = iMin
                    End If
                    If (iMax < iMin) Then
                        q = iMin
                        iMin = iMax
                        iMax = q
                    End If
                    If (iDefault < iMin) And (iDefault <> -9999) Then
                        iDefault = iMin
                    End If
                    DisplayPads(TestId, u, Counter).ButtonRange = Trim(str(iMax)) + "/" + Trim(str(iMin)) + "/" + Trim(iDefault)
                    DisplayPads(TestId, u, Counter).ButtonId = TheControl.ControlId
                    DisplayPads(TestId, u, Counter).ButtonText = VisualText
                    If (Trim(TheControl.IEChoiceName) <> "") Then
                        DisplayPads(TestId, u, Counter).ButtonName = Trim(TheControl.IEChoiceName)
                    Else
                        DisplayPads(TestId, u, Counter).ButtonName = "*" + Trim(TheControl.ControlName)
                    End If
                    If (Trim(TheControl.DecimalDefault) = "T") Then
                        DisplayPads(TestId, u, Counter).ButtonPadOrder = True
                    End If
                    If (Trim(TheControl.ICDAlias4) = "T") Then
                        DisplayPads(TestId, u, Counter).ButtonUseOtherText = True
                    End If
                    DisplayPads(TestId, u, Counter).ButtonType = Trim(TheControl.ControlType)
                    DisplayPads(TestId, u, Counter).ButtonTrigger = Trim(TheControl.ControlAlternateTrigger)
                    DisplayPads(TestId, u, Counter).ButtonTriggerText = Trim(TheControl.ControlAlternateText)
                    DisplayPads(TestId, u, Counter).ButtonOn = False
                    DisplayPads(TestId, u, Counter).Buttons = False
                    DisplayPads(TestId, u, Counter).ButtonMutual = False
                    DisplayPads(TestId, u, Counter).ButtonIncrement = Trim(TheControl.AIncrement)
                    DisplayPads(TestId, u, Counter).ButtonDeviceRef = Trim(TheControl.ICDAlias4)
                    MaxButtonsForPads = 0
                    If (TheControl.ControlVisibleAfterTrigger) Then
                        Call GetButtonsforPad(FormId, TestId, Counter, DisplayPads(TestId, u, Counter).ButtonTriggerText, TheControl.ControlVisibleAfterTrigger, MaxButtonsForPads)
                    End If
                End If
                i = i + 1
            Wend
        End If
    End If
Next u
Set TheControl = Nothing
For i = 1 To Counter
    DisplayPads(TestId, 2, i) = DisplayPads(TestId, 1, i)
Next i
End Function

Private Function GetButtonsforPad(FormId As Long, TestId As Integer, Cntr As Integer, Trigger As String, Ilook As Boolean, Counter As Integer) As Boolean
Dim i As Integer
Dim k As Integer
Dim q As Integer
Dim AControl As DynamicControls
Dim VisualText As String
k = 0
Set AControl = New DynamicControls
If (FormId > 0) Then
    AControl.FormId = FormId
    AControl.ControlAlternateTrigger = ""
    AControl.ControlAlternateText = Trigger
    AControl.ControlVisibleAfterTrigger = Ilook
    AControl.ControlType = "B"
    If (AControl.FindTriggerControl > 0) Then
        i = 1
        While (AControl.SelectControl(i))
            If Not (AControl.IgnoreIt) Then
                k = k + 1
                If (Trim(AControl.ControlText) <> "") Then
                    q = InStrPS(AControl.ControlText, "^")
                    If (q <> 0) Then
                        VisualText = Left(AControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(AControl.ControlText, q + 1, Len(AControl.ControlText) - q)
                    Else
                        VisualText = AControl.ControlText
                    End If
                Else
                    VisualText = AControl.ControlName
                End If
                For q = 1 To 2
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonId = AControl.ControlId
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonText = VisualText
                    If (Trim(AControl.IEChoiceName) <> "") Then
                        PlantTriggerPads(TestId, q, Cntr, k).ButtonName = Trim(AControl.IEChoiceName)
                    Else
                        PlantTriggerPads(TestId, q, Cntr, k).ButtonName = "*" + Trim(AControl.ControlName)
                    End If
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonType = Trim(AControl.ControlType)
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonTrigger = Trim(AControl.ControlAlternateTrigger)
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonTriggerText = Trim(AControl.ControlAlternateText)
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonOn = False
                    PlantTriggerPads(TestId, q, Cntr, k).Buttons = False
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonMutual = False
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonPadOrder = False
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonUseOtherText = False
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonIncrement = Trim(AControl.AIncrement)
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonDeviceRef = Trim(AControl.ICDAlias4)
                Next q
            End If
            i = i + 1
        Wend
    End If
End If
Set AControl = Nothing
End Function

Private Function GetButtonsForTests(FormId As Long, TestId As Integer, Counter As Integer) As Boolean
Dim i As Integer
Dim q As Integer
Dim VisualText As String
Dim AControl As DynamicControls
Counter = 0
GetButtonsForTests = True
For i = 1 To 2
    DisplayBtns(TestId, i).Buttons = False
    DisplayBtns(TestId, i).ButtonOn = False
    DisplayBtns(TestId, i).ButtonName = ""
    DisplayBtns(TestId, i).ButtonText = ""
    DisplayBtns(TestId, i).ButtonRange = ""
    DisplayBtns(TestId, i).ButtonVisible = False
    DisplayBtns(TestId, i).ButtonTrigger = ""
    DisplayBtns(TestId, i).ButtonTriggerText = ""
    DisplayBtns(TestId, i).ButtonId = 0
    DisplayBtns(TestId, i).ButtonMutual = False
    DisplayBtns(TestId, i).ButtonPadOrder = False
    DisplayBtns(TestId, i).ButtonUseOtherText = False
    DisplayBtns(TestId, i).ButtonIncrement = ""
    DisplayBtns(TestId, i).ButtonDeviceRef = ""
    DisplayBtns(TestId, i).Result = ""
Next i
Set AControl = New DynamicControls
If (FormId > 0) Then
    AControl.FormId = FormId
    AControl.ControlType = "B"
    AControl.ControlAlternateText = ""
    AControl.ControlVisibleAfterTrigger = False
    If (AControl.FindTriggerControl > 0) Then
        i = 1
        While (AControl.SelectControl(i))
            If Not (AControl.IgnoreIt) Then
                If (Left(AControl.ControlName, 3) = "OS-") Or (Left(AControl.ControlName, 3) = "OD-") Then
                    Counter = Counter + 1
                    If (Trim(AControl.ControlText) <> "") Then
                        q = InStrPS(AControl.ControlText, "^")
                        If (q <> 0) Then
                            VisualText = Left(AControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(AControl.ControlText, q + 1, Len(AControl.ControlText) - q)
                        Else
                            VisualText = AControl.ControlText
                        End If
                    Else
                        VisualText = AControl.ControlName
                    End If
                    DisplayBtns(TestId, Counter).ButtonId = AControl.ControlId
                    DisplayBtns(TestId, Counter).ButtonText = VisualText
                    If (Trim(AControl.IEChoiceName) <> "") Then
                        DisplayBtns(TestId, Counter).ButtonName = Trim(AControl.IEChoiceName)
                    Else
                        DisplayBtns(TestId, Counter).ButtonName = "*" + Trim(AControl.ControlName)
                    End If
                    DisplayBtns(TestId, Counter).ButtonType = Trim(AControl.ControlType)
                    DisplayBtns(TestId, Counter).ButtonTrigger = Trim(AControl.ControlAlternateTrigger)
                    DisplayBtns(TestId, Counter).ButtonTriggerText = "Btn1"
                    DisplayBtns(TestId, Counter).Buttons = False
                    DisplayBtns(TestId, Counter).ButtonVisible = AControl.ControlVisible
                    DisplayBtns(TestId, Counter).ButtonMutual = False
                    DisplayBtns(TestId, Counter).ButtonPadOrder = False
                    DisplayBtns(TestId, Counter).ButtonUseOtherText = False
                    DisplayBtns(TestId, Counter).ButtonIncrement = Trim(AControl.AIncrement)
                    DisplayBtns(TestId, Counter).ButtonDeviceRef = Trim(AControl.ICDAlias4)
                    MaxButtonsForButtons = 0
                    Call GetButtonsforButtons(FormId, TestId, Counter, DisplayBtns(TestId, Counter).ButtonTriggerText, AControl.ControlVisibleAfterTrigger, MaxButtonsForButtons)
                    MaxWriteButtonsForTests(TestId) = MaxButtonsForButtons
                End If
            End If
            i = i + 1
        Wend
    End If
End If
Set AControl = Nothing
End Function

Private Function GetButtonsforButtons(FormId As Long, TestId As Integer, Cntr As Integer, Trigger As String, Ilook As Boolean, Counter As Integer) As Boolean
Dim i As Integer
Dim q As Integer
Dim VisualText As String
Dim AControl As DynamicControls
Set AControl = New DynamicControls
If (FormId > 0) Then
    AControl.FormId = FormId
    AControl.ControlAlternateText = Trigger
    AControl.ControlVisibleAfterTrigger = Ilook
    AControl.ControlType = "B"
    If (AControl.FindTriggerControl > 0) Then
        i = 1
        While (AControl.SelectControl(i))
            If Not (AControl.IgnoreIt) Then
                If (Left(AControl.ControlName, 3) <> "OS-") And (Left(AControl.ControlName, 3) <> "OD-") Then
                    Counter = Counter + 1
                    If (Trim(AControl.ControlText) <> "") Then
                        q = InStrPS(AControl.ControlText, "^")
                        If (q <> 0) Then
                            VisualText = Left(AControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(AControl.ControlText, q + 1, Len(AControl.ControlText) - q)
                        Else
                            VisualText = AControl.ControlText
                        End If
                    Else
                        VisualText = AControl.ControlName
                    End If
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonId = AControl.ControlId
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonText = VisualText
                    If (Trim(AControl.IEChoiceName) <> "") Then
                        PlantTriggerButtons(TestId, Cntr, Counter).ButtonName = Trim(AControl.IEChoiceName)
                    Else
                        PlantTriggerButtons(TestId, Cntr, Counter).ButtonName = "*" + Trim(AControl.ControlName)
                    End If
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonType = Trim(AControl.ControlType)
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonTrigger = Trim(AControl.ControlAlternateTrigger)
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonTriggerText = Trim(AControl.ControlAlternateText)
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonOn = False
                    PlantTriggerButtons(TestId, Cntr, Counter).Buttons = False
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonMutual = False
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonPadOrder = False
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonUseOtherText = False
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonIncrement = Trim(AControl.AIncrement)
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonDeviceRef = Trim(AControl.ICDAlias4)
                End If
            End If
            i = i + 1
        Wend
    End If
End If
Set AControl = Nothing
End Function

Private Sub LoadFileContent()
Dim u As Integer
Dim j As Integer
Dim k As Integer
Dim AEye As String
Dim ATest As Integer
Dim AValue As String
Dim TheRec As String
Dim RecNum As Integer
Dim FileNum As Integer
Dim MaxTimeRef As Integer
Dim FeedArray(50) As String
Dim EyeArray(12) As String
MaxTimeRef = 0
AEye = ""
ATest = 0
Call GetTimeRef
FileNum = FreeFile
FM.OpenFile StoreResults, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
While Not (EOF(FileNum))
    Line Input #FileNum, TheRec
    If (Left(TheRec, 5) = "TIME=") Then
        MaxTimeRef = MaxTimeRef + 1
        TimeRef(MaxTimeRef).ButtonName = "TIME"
        TimeRef(MaxTimeRef).ButtonOn = True
        TimeRef(MaxTimeRef).Result = Trim(Mid(TheRec, 6, Len(TheRec) - 5))
        TimeRef(MaxTimeRef).Buttons = True
    End If
    If (Left(TheRec, 9) <> "QUESTION=") And (Left(TheRec, 5) <> "TIME=") Then
        If (Left(TheRec, 3) = "*OD") Then
            AEye = "OD"
        ElseIf (Left(TheRec, 3) = "*OS") Then
            AEye = "OS"
        ElseIf (Trim(AEye) = "") Then
            AEye = "OU"
        End If
        j = InStrPS(TheRec, "=T")
        If (j <> 0) Then
            k = InStrPS(j, TheRec, " ")
            If (k <> 0) Then
                AValue = ""
                RecNum = Val(Mid(TheRec, j + 2, (k - 1) - (j + 1)))
                k = InStrPS(j, TheRec, "<")
                If (k <> 0) Then
                    j = InStrPS(k, TheRec, ">")
                    If (j <> 0) Then
                        AValue = Trim(Mid(TheRec, k + 1, (j - 1) - k))
                    End If
                End If
                Call PostReference(AEye, RecNum, AValue, ATest)
            End If
        End If
    End If
Wend
FM.CloseFile CLng(FileNum)
End Sub

Private Sub PostReference(TheEye As String, TheRecNum As Integer, TheValue As String, TheTest As Integer)
Dim i As Integer
Dim k As Integer
Dim l As Integer
Dim z As Integer
Dim q As Integer
Dim StartEye As Integer
Dim EndEye As Integer
Dim Eye As Integer
Dim Found As Boolean
Eye = 1
If (TheEye = "OD") Then
    Eye = 1
ElseIf (TheEye = "OS") Then
    Eye = 2
Else
    Eye = 3
End If
Found = False
If (Eye = 3) Then
    StartEye = 1
    EndEye = 2
Else
    StartEye = Eye
    EndEye = Eye
End If
If (TheTest < 1) Then
    TheTest = 1
End If
For i = 1 To MaxTests
    If (DisplayTests(i).ButtonId = TheRecNum) Then
        DisplayTests(i).Buttons = True
        DisplayTests(i).Result = TheValue
        TheTest = i
        Found = True
        Exit For
    End If
Next i
If Not Found Then
    For i = TheTest To TheTest
        For k = 1 To MaxPadsForTests
            For q = StartEye To EndEye
                If (TheRecNum = DisplayPads(i, q, k).ButtonId) Then
                    If Not (DisplayPads(i, q, k).Buttons) Then
                        DisplayPads(i, q, k).Buttons = True
                        DisplayPads(i, q, k).Result = TheValue
                        Found = True
                        Exit For
                    End If
                End If
                For l = 1 To MaxPadChoices
                    If (DisplayPads(i, q, k).Buttons) Then
                        If (TheRecNum = PlantTriggerPads(i, q, k, l).ButtonId) Then
                            If Not (PlantTriggerPads(i, q, k, l).Buttons) Then
                                PlantTriggerPads(i, q, k, l).Buttons = True
                                PlantTriggerPads(i, q, k, l).Result = TheValue
                                Found = True
                                Exit For
                            End If
                        End If
                    End If
                Next l
                If (Found) Then
                    Exit For
                End If
            Next q
        Next k
        If (Found) Then
            Exit For
        End If
    Next i
End If
If Not Found Then
    For i = TheTest To TheTest
        For q = StartEye To EndEye
            If (TheRecNum = DisplayBtns(i, q).ButtonId) Then
                If Not (DisplayBtns(i, q).Buttons) Then
                    DisplayBtns(i, q).Buttons = True
                    DisplayBtns(i, q).Result = TheValue
                    Found = True
                    Exit For
                End If
            End If
            For k = 1 To MaxButtonsAllowed
                If (TheRecNum = PlantTriggerButtons(i, q, k).ButtonId) Then
                    If Not (PlantTriggerButtons(i, q, k).Buttons) Then
                        PlantTriggerButtons(i, q, k).Buttons = True
                        PlantTriggerButtons(i, q, k).Result = TheValue
                        Found = True
                        Exit For
                    End If
                End If
            Next k
            If (Found) Then
                Exit For
            End If
        Next q
    Next i
End If
If Not Found Then
    For q = TheTest To TheTest
        For i = 1 To MaxTestWideBtns
            If (TheRecNum = TestWideButtons(q, i).ButtonId) Then
                TestWideButtons(q, i).Buttons = True
                TestWideButtons(q, i).ButtonOn = False
                If (TestWideButtons(q, i).ButtonMutual) Then
                    TestWideButtons(q, i).ButtonOn = True
                End If
                Found = True
                Exit For
            End If
        Next i
    Next q
End If
End Sub

Private Function GetPreviousTest(PatId As Long, TestQuestion As String, TheFile As String) As Boolean
Dim FileNum As Integer
Dim w As Integer
Dim i As Integer, j As Integer
Dim ApptId As Long
Dim CurWaitValue As Integer, CurLifeValue As Integer, CurAnnualValue As Integer
Dim NowDate As String, NewDate As String
Dim Rec As String, Temp As String
Dim BaseDate As String
Dim TimeRec As String, TheDate As String
Dim RetCls As DynamicClass
Dim RetCln As PatientClinical
Dim RetExm As DI_ExamClinical
Dim RetAppt As SchedulerAppointment
BaseDate = ""
NewDate = frmSystems1.ActiveActivityDate
NewDate = Mid(NewDate, 5, 2) + "/" + Mid(NewDate, 7, 2) + "/" + Left(NewDate, 4)
GetPreviousTest = False
If (PatId > 0) And (Trim(TestQuestion) <> "") And (Trim(TheFile) <> "") Then
    TimeRec = ""
    Set RetCln = New PatientClinical
    RetCln.PatientId = PatId
    RetCln.ClinicalType = "F"
    RetCln.Symptom = "/" + Trim(TestQuestion)
    If (RetCln.FindPatientClinicalTests > 0) Then
        FileNum = FreeFile
        FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
        Print #FileNum, "QUESTION=" + "/" + TestQuestion
        i = 1
        ApptId = 0
        TheDate = ""
        Do Until Not (RetCln.SelectPatientClinical(i))
            If (ApptId < 1) Then
                ApptId = RetCln.AppointmentId
                Set RetAppt = New SchedulerAppointment
                RetAppt.AppointmentId = ApptId
                If (RetAppt.RetrieveSchedulerAppointment) Then
                    BaseDate = RetAppt.AppointmentDate
                    TheDate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
                End If
                Set RetAppt = Nothing
            End If
            If (ApptId = RetCln.AppointmentId) Then
                If (TimeRec = "") Then
                    Print #FileNum, Trim(RetCln.Findings)
                Else
                    If (TimeRec = RetCln.Findings) Then
                        Exit Do
                    End If
                    Print #FileNum, Trim(RetCln.Findings)
                End If
                If (TimeRec = "") Then
                    If (InStrPS(RetCln.Findings, "TIME") <> 0) Then
                        TimeRec = RetCln.Findings
                    End If
                End If
            Else
                Exit Do
            End If
            i = i + 1
        Loop
        FM.CloseFile CLng(FileNum)
        GetPreviousTest = True
    End If
    Set RetCln = Nothing
    If (FM.IsFileThere(TheFile)) Then
        Temp = ""
        Call frmTestResults.PushList(TheFile)
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
        While Not (EOF(FileNum))
            Line Input #FileNum, Rec
            If (Left(Rec, 6) = "Recap=") Then
                Temp = Temp + Trim(Mid(Rec, 7, Len(Rec) - 6))
            End If
        Wend
        Temp = Replace(Temp, "%", " ")
        Temp = Replace(Temp, "^", " ")
        Temp = Replace(Temp, "!", " ")
        Temp = Replace(Temp, "~", " ")
        FM.CloseFile CLng(FileNum)
    End If
End If
End Function

Private Sub lblWarning_Click()
UseManifest = True
Call cmdApply_Click
End Sub

Private Function VerifyScratchFile(TheName As String) As Boolean
Dim q As Integer
Dim Rec As String
VerifyScratchFile = True
q = 0
If (FM.IsFileThere(TheName)) Then
    FM.OpenFile TheName, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    While Not (EOF(101))
        Line Input #101, Rec
        q = q + 1
    Wend
    FM.CloseFile CLng(101)
End If
If (q < 3) Then
    VerifyScratchFile = False
End If
End Function

Private Function SetDeviceData(ARef As String, ATest As Integer, AEye As Integer, AText As String) As Boolean
Dim i As Integer
Dim j As Integer
Dim z As Integer
Dim PostIt As Boolean
SetDeviceData = True
If (Trim(ARef) <> "") And (AEye > 0) And (ATest > 0) And (Trim(AText) <> "") Then
    PostIt = False
    DisplayTests(ATest).ButtonOn = True
    For j = 1 To MaxButtonsAllowed
        If (TestWideButtons(ATest, j).ButtonDeviceRef = ARef) Then
            TestWideButtons(ATest, j).ButtonOn = True
            If (TestWideButtons(ATest, j).ButtonMutual) Then
                If (TestWideButtons(ARef, j).ButtonOn) Then
                    For z = 1 To MaxTestWideBtns
                        If (z <> j) Then
                            TestWideButtons(ATest, z).ButtonOn = False
                            TestWideButtons(ATest, z).Buttons = False
                        Else
                            TestWideButtons(ATest, z).ButtonOn = True
                            TestWideButtons(ATest, z).Buttons = True
                        End If
                        PostIt = True
                        Exit For
                    Next z
                End If
            End If
        End If
    Next j
    
    If Not (PostIt) Then
        For j = 1 To MaxPadsAllowed
            For z = 1 To MaxPadChoices
                If (PlantTriggerPads(ATest, AEye, j, z).ButtonDeviceRef = ARef) Then
                    PlantTriggerPads(ATest, AEye, j, z).Result = AText
                    PlantTriggerPads(ATest, AEye, j, z).ButtonOn = True
                    DisplayPads(ATest, AEye, j).ButtonOn = True
                    DisplayPads(ATest, AEye, j).Buttons = True
                    PostIt = True
                    Exit For
                End If
            Next z
        Next j
    End If
    
    If Not (PostIt) Then
        For j = 1 To MaxBtnAllowed
            If (PlantTriggerButtons(ATest, AEye, j).ButtonDeviceRef = ARef) Then
                DisplayBtns(ATest, j).ButtonOn = True
                PlantTriggerButtons(ATest, AEye, j).ButtonOn = True
                PostIt = True
                Exit For
            End If
        Next j
    End If
End If
End Function
Public Function FrmClose()
Unload Me
End Function

