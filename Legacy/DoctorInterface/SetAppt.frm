VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Begin VB.Form frmSetAppt 
   BackColor       =   &H0077742D&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Set Appt Date"
   ClientHeight    =   2520
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   5250
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2520
   ScaleWidth      =   5250
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   855
      Left            =   3480
      TabIndex        =   0
      Top             =   1200
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetAppt.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExam 
      Height          =   855
      Left            =   3480
      TabIndex        =   1
      Top             =   240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetAppt.frx":01DF
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   360
      Width           =   2055
      _Version        =   65537
      _ExtentX        =   3625
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
End
Attribute VB_Name = "frmSetAppt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ApptId As Long
Public PatientId As Long
Public ApptDate As String
Public ActionType As String
Public DidSomething As Boolean

Private ACnt As Integer
Private AppointmentId As Long
Private ActivityId As Long

Private Sub cmdDone_Click()
Unload frmSetAppt
End Sub

Private Function SetupForm() As Boolean
DidSomething = False
If (ActionType = "D") Then
    cmdExam.Text = "Diags"
Else
    cmdExam.Text = "Exam Elements"
End If
Call FormatTodaysDate(ApptDate, False)
SSDateCombo1.DateValue = ApptDate
AppointmentId = 0
ACnt = 0
End Function

Private Sub cmdExam_Click()
Dim ActId As Long
Dim ApptId As Long
Dim ApptIdOnly As Long
Dim AEye As String, AType As String
Dim ADate As String, ADiag As String
Dim AClass As String
Dim RetAct As PracticeActivity
Dim RetApt As SchedulerAppointment
ApptId = 0
ActId = 0
ADate = SSDateCombo1.DateValue
Call FormatTodaysDate(ADate, False)
ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
Set RetAct = New PracticeActivity
RetAct.ActivityPatientId = PatientId
RetAct.ActivityLocationId = -1
RetAct.ActivityAppointmentId = 0
RetAct.ActivityDate = ADate
RetAct.ActivityStatus = "D"
If (RetAct.FindActivity > 0) Then
    frmEventMsgs.Header = "Appointment Exists, Please Use Edit Previous Visit"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "OK"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
ApptId = CreateHighlightAppointment(PatientId, ADate, ActId)
If (ApptId > 0) And (ActId > 0) Then
    ApptIdOnly = ApptId
    If (ActionType = "D") Then
        Call GetStartingValue(AType, AClass)
        If (AType <> "") And (AClass <> "") Then
            frmSearch.DiagOn = True
            frmSearch.FilterBillable = True
            frmSearch.FilterSpecial = False
            frmSearch.Selection = ""
            If (frmSearch.LoadAvailableSearch(AType, AClass)) Then
                frmSearch.Show 1
                If (frmSearch.Selection <> "") Then
                    AEye = "OU"
                    frmEventMsgs.Header = "Eye ?"
                    frmEventMsgs.AcceptText = "OD"
                    frmEventMsgs.RejectText = "OS"
                    frmEventMsgs.CancelText = "OU"
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    If (frmEventMsgs.Result = 1) Then
                        AEye = "OD"
                    ElseIf (frmEventMsgs.Result = 2) Then
                        AEye = "OS"
                    End If
                    ACnt = ACnt + 1
                    ADiag = Trim(Left(frmSearch.Selection, 7))
                    Call PostDiags(ApptIdOnly, PatientId, ADiag, AEye, ACnt)
                    DidSomething = True
                    AppointmentId = ApptIdOnly
                End If
            End If
        End If
    Else
        frmTestResults.ActivityId = ActId
        frmTestResults.AppointmentId = ApptId
        frmTestResults.PatientId = PatientId
        frmTestResults.QuestionSet = "First Visit"
        If (frmTestResults.LoadSummaryInfo(1)) Then
            frmTestResults.FormOn = False
            frmTestResults.Show 1
            If Not (frmTestResults.ExitFast) Then
                Call PostTests(ApptId, PatientId)
                DidSomething = True
            End If
        End If
    End If
End If
End Sub

Private Sub Form_Activate()
Call SetupForm
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Call cmdDone_Click
End Sub

Private Sub SSDateCombo1_Change()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_Click()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_CloseUp()
If (Trim(SSDateCombo1.Date) <> "") Then
    ApptDate = SSDateCombo1.DateValue
    Call FormatTodaysDate(ApptDate, False)
    If (ApptDate = "") Then
        SSDateCombo1.Text = ""
    Else
        SSDateCombo1.Text = ApptDate
    End If
End If
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo1.Date) <> "") And (SSDateCombo1.DateValue <> SSDateCombo1.Date) Then
        ApptDate = SSDateCombo1.DateValue
        Call FormatTodaysDate(ApptDate, False)
        If (ApptDate = "") Then
            SSDateCombo1.Text = ""
        Else
            SSDateCombo1.Text = ApptDate
        End If
    Else
        If (ApptDate = "") Then
            SSDateCombo1.Text = ""
        ElseIf (SSDateCombo1.DateValue = "") Then
            SSDateCombo1.Text = ApptDate
        Else
            ApptDate = SSDateCombo1.DateValue
            Call FormatTodaysDate(ApptDate, False)
            ApptDate = Mid(ApptDate, 1, 2) + Mid(ApptDate, 4, 2) + Mid(ApptDate, 9, 2)
        End If
        ATemp = ApptDate
        If (Len(ApptDate) > 10) Then
            ATemp = Mid(ApptDate, 11, Len(ApptDate) - 10)
        ElseIf (ApptDate = "") Then
            Call FormatTodaysDate(ApptDate, False)
            ATemp = Mid(ApptDate, 1, 2) + Mid(ApptDate, 4, 2) + Mid(ApptDate, 9, 2)
        End If
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        ApptDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        Call FormatTodaysDate(ApptDate, False)
        If (ApptDate = "") And (SSDateCombo1.DateValue <> SSDateCombo1.Date) Then
            ApptDate = SSDateCombo1.Date
            Call FormatTodaysDate(ApptDate, False)
            SSDateCombo1.Text = ApptDate
        Else
            SSDateCombo1.Text = ApptDate
        End If
    End If
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        ApptDate = ApptDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(ApptDate) > 0) Then
        ApptDate = Left(ApptDate, Len(ApptDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(ApptDate) < 1) Then
        ApptDate = ""
    End If
End If
End Sub

Private Function CreateHighlightAppointment(PatId As Long, ADate As String, ActId As Long) As Long
Dim DrId As Long
Dim RetApt As SchedulerAppointment
Dim RetAct As PracticeActivity
CreateHighlightAppointment = 0
ActId = 0
DrId = 0
If (PatId > 0) And (Trim(ADate) <> "") Then
    Set RetApt = New SchedulerAppointment
    RetApt.AppointmentId = ApptId
    If (RetApt.RetrieveSchedulerAppointment) Then
        DrId = RetApt.AppointmentResourceId1
    End If
    Set RetApt = Nothing
    
    Set RetApt = New SchedulerAppointment
    RetApt.AppointmentId = 0
    If (RetApt.RetrieveSchedulerAppointment) Then
        RetApt.AppointmentPatientId = PatId
        RetApt.AppointmentDate = ADate
        RetApt.AppointmentComments = "Add Via Highlights"
        RetApt.AppointmentReferralId = 0
        RetApt.AppointmentPreCertId = 0
        RetApt.AppointmentDuration = 0
        RetApt.AppointmentTime = 0
        RetApt.AppointmentResourceId1 = DrId
        RetApt.AppointmentResourceId2 = 0
        RetApt.AppointmentResourceId3 = 0
        RetApt.AppointmentResourceId4 = 0
        RetApt.AppointmentResourceId5 = 0
        RetApt.AppointmentResourceId6 = 0
        RetApt.AppointmentResourceId7 = 0
        RetApt.AppointmentResourceId8 = 0
        RetApt.AppointmentTypeId = 0
        RetApt.AppointmentTypeTech = 0
        RetApt.AppointmentInsType = "M"
        RetApt.AppointmentStatus = "D"
        Call RetApt.ApplySchedulerAppointment
        CreateHighlightAppointment = RetApt.AppointmentId
    End If
    Set RetApt = Nothing
End If

If (CreateHighlightAppointment > 0) Then
    Set RetAct = New PracticeActivity
    RetAct.ActivityId = 0
    If (RetAct.RetrieveActivity) Then
        RetAct.ActivityCurrResId = 0
        RetAct.ActivityAppointmentId = CreateHighlightAppointment
        RetAct.ActivityPatientId = PatId
        RetAct.ActivityDate = ADate
        RetAct.ActivityLocationId = 0
        RetAct.ActivityPayAmount = 0
        RetAct.ActivityPayMethod = ""
        RetAct.ActivityPayRef = ""
        RetAct.ActivityQuestionSet = "FirstVisit"
        RetAct.ActivityResourceId = 0
        RetAct.ActivityStationId = 0
        RetAct.ActivityStatus = "D"
        RetAct.ActivityStatusTime = 0
        RetAct.ActivityTechConfirm = False
        Call RetAct.ApplyActivity
        ActId = RetAct.ActivityId
    End If
    Set RetAct = Nothing
End If
End Function

Private Function PostDiags(AppId As Long, PatId As Long, ADiag As String, AEye As String, PrimeCount As Integer) As Boolean
Dim ClinicalRec As PatientClinical
If (PatId > 0) And (AppId > 0) And (Trim(ADiag) <> "") And (Trim(AEye) <> "") Then
    Set ClinicalRec = New PatientClinical
    ClinicalRec.ClinicalId = 0
    If (ClinicalRec.RetrievePatientClinical) Then
        ClinicalRec.AppointmentId = AppId
        ClinicalRec.PatientId = PatId
        ClinicalRec.EyeContext = AEye
        ClinicalRec.ClinicalType = "B"
        ClinicalRec.Symptom = Trim(Str(PrimeCount))
        ClinicalRec.Findings = Trim(ADiag)
        ClinicalRec.ImageInstructions = ""
        ClinicalRec.ImageDescriptor = ""
        ClinicalRec.ClinicalHighlights = "K"
        Call ClinicalRec.ApplyPatientClinical
    End If
    Set ClinicalRec = Nothing
End If
End Function

Private Function PostTests(AppId As Long, PatId As Long) As Boolean
Dim FileNum As Long
Dim q As Integer
Dim Symptom As String, Finding As String
Dim AQuestion As String, AImage As String
Dim FileFamily As String, ImageText As String
Dim Rec As String, TheFile As String, MyFile As String
Dim ClinicalRec As PatientClinical
PostTests = False
FileNum = FreeFile
TheFile = DoctorInterfaceDirectory + "T*" + "_" + Trim(Str(AppId)) + "_" + Trim(Str(PatId)) + ".txt"
MyFile = Dir(TheFile)
While (MyFile <> "")
    AQuestion = ""
    FM.OpenFile DoctorInterfaceDirectory + MyFile, FileOpenMode.InputFileOpenMode, FileAccess.Read, CLng(FileNum)
    While (Not (EOF(FileNum)))
        Line Input #FileNum, Rec
        If (Left(Rec, 6) <> "Recap=") Then
            If (Left(Rec, 9) = "QUESTION=") Then
                AQuestion = Mid(Rec, 10, Len(Rec) - 9)
            ElseIf (Trim(AQuestion) <> "") Then
                ImageText = ""
                If (Left(Rec, 5) = "TIME=") Then
                    q = Len(Rec) + 1
                Else
                    q = InStrPS(Rec, "<")
                    If (q <> 0) Then
                        q = Len(Rec) + 1
                    Else
                        ImageText = Mid(Rec, q, Len(Rec) - (q - 1))
                    End If
                End If
                Symptom = AQuestion
                Finding = Left(Rec, q - 1)
                Set ClinicalRec = New PatientClinical
                ClinicalRec.ClinicalId = 0
                If (ClinicalRec.RetrievePatientClinical) Then
                    ClinicalRec.AppointmentId = AppId
                    ClinicalRec.PatientId = PatId
                    ClinicalRec.ClinicalType = "F"
                    ClinicalRec.EyeContext = ""
                    ClinicalRec.Symptom = Symptom
                    ClinicalRec.Findings = Finding
                    ClinicalRec.ImageInstructions = ImageText
                    ClinicalRec.ImageDescriptor = ""
                    ClinicalRec.ClinicalHighlights = "T"
                    Call ClinicalRec.ApplyPatientClinical
                End If
                Set ClinicalRec = Nothing
            End If
        End If
    Wend
    FM.CloseFile CLng(FileNum)
    MyFile = Dir
Wend
FileFamily = "T*" + "_" + Trim(Str(AppId)) + "_" + Trim(Str(PatientId)) + ".txt"
Call CleanUp(DoctorInterfaceDirectory, FileFamily, False)
PostTests = True
End Function

Private Function GetStartingValue(TheText As String, TheClass As String) As Boolean
frmEventMsgs.Header = "Search Type"
frmEventMsgs.AcceptText = "Alpha"
frmEventMsgs.RejectText = "Numeric"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    TheClass = "A"
    GoSub GetIt
ElseIf (frmEventMsgs.Result = 2) Then
    TheClass = "N"
    GoSub GetIt
End If
Exit Function
GetIt:
    TheText = ""
    If (TheClass = "N") Then
        frmNumericPad.NumPad_Field = "Starting with Diagnosis Number"
        frmNumericPad.NumPad_Result = ""
        frmNumericPad.NumPad_Max = 0
        frmNumericPad.NumPad_Min = 0
        frmNumericPad.NumPad_EyesOn = False
        frmNumericPad.NumPad_CommentOn = False
        frmNumericPad.NumPad_TimeFrameOn = False
        frmNumericPad.NumPad_TimeFrame = ""
        frmNumericPad.NumPad_DisplayFull = True
        frmNumericPad.NumPad_ChoiceOn1 = False
        frmNumericPad.NumPad_Choice1 = ""
        frmNumericPad.NumPad_ChoiceOn2 = False
        frmNumericPad.NumPad_Choice2 = ""
        frmNumericPad.NumPad_ChoiceOn3 = False
        frmNumericPad.NumPad_Choice3 = ""
        frmNumericPad.NumPad_ChoiceOn4 = False
        frmNumericPad.NumPad_Choice4 = ""
        frmNumericPad.NumPad_ChoiceOn5 = False
        frmNumericPad.NumPad_Choice5 = ""
        frmNumericPad.NumPad_ChoiceOn6 = False
        frmNumericPad.NumPad_Choice6 = ""
        frmNumericPad.NumPad_ChoiceOn7 = False
        frmNumericPad.NumPad_Choice7 = ""
        frmNumericPad.NumPad_ChoiceOn8 = False
        frmNumericPad.NumPad_Choice8 = ""
        frmNumericPad.NumPad_ChoiceOn9 = False
        frmNumericPad.NumPad_Choice9 = ""
        frmNumericPad.NumPad_ChoiceOn10 = False
        frmNumericPad.NumPad_Choice10 = ""
        frmNumericPad.NumPad_ChoiceOn11 = False
        frmNumericPad.NumPad_Choice11 = ""
        frmNumericPad.NumPad_ChoiceOn12 = False
        frmNumericPad.NumPad_Choice12 = ""
        frmNumericPad.NumPad_ChoiceOn13 = False
        frmNumericPad.NumPad_Choice13 = ""
        frmNumericPad.NumPad_ChoiceOn14 = False
        frmNumericPad.NumPad_Choice14 = ""
        frmNumericPad.NumPad_ChoiceOn15 = False
        frmNumericPad.NumPad_Choice15 = ""
        frmNumericPad.NumPad_ChoiceOn16 = False
        frmNumericPad.NumPad_Choice16 = ""
        frmNumericPad.NumPad_ChoiceOn17 = False
        frmNumericPad.NumPad_Choice17 = ""
        frmNumericPad.NumPad_ChoiceOn18 = False
        frmNumericPad.NumPad_Choice18 = ""
        frmNumericPad.NumPad_ChoiceOn19 = False
        frmNumericPad.NumPad_Choice19 = ""
        frmNumericPad.NumPad_ChoiceOn20 = False
        frmNumericPad.NumPad_Choice20 = ""
        frmNumericPad.NumPad_ChoiceOn21 = False
        frmNumericPad.NumPad_Choice21 = ""
        frmNumericPad.NumPad_ChoiceOn22 = False
        frmNumericPad.NumPad_Choice22 = ""
        frmNumericPad.NumPad_ChoiceOn23 = False
        frmNumericPad.NumPad_Choice23 = ""
        frmNumericPad.NumPad_ChoiceOn24 = False
        frmNumericPad.NumPad_Choice24 = ""
        frmNumericPad.Show 1
        If Not (frmAlphaPad.NumPad_Quit) Then
            TheText = Trim(frmNumericPad.NumPad_Result)
        End If
    ElseIf (TheClass = "A") Then
        frmAlphaPad.NumPad_Field = "Starting with Diagnosis Alpha"
        frmAlphaPad.NumPad_Result = ""
        frmAlphaPad.NumPad_DisplayFull = True
        frmAlphaPad.Show 1
        If Not (frmAlphaPad.NumPad_Quit) Then
            TheText = Trim(frmAlphaPad.NumPad_Result)
        End If
    End If
    Return
End Function
