VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmEventMsgs 
   BackColor       =   &H00505050&
   BorderStyle     =   5  'Sizable ToolWindow
   ClientHeight    =   3870
   ClientLeft      =   2055
   ClientTop       =   435
   ClientWidth     =   8745
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "EventMsgs.frx":0000
   ScaleHeight     =   3870
   ScaleWidth      =   8745
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   990
      Index           =   0
      Left            =   1920
      TabIndex        =   1
      Top             =   1880
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   1110
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   1240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":38A0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   1110
      Index           =   2
      Left            =   1920
      TabIndex        =   3
      Top             =   1240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":3A77
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   1110
      Index           =   3
      Left            =   3600
      TabIndex        =   4
      Top             =   1240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":3C4E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   1110
      Index           =   4
      Left            =   5280
      TabIndex        =   5
      Top             =   1240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":3E25
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   1110
      Index           =   5
      Left            =   240
      TabIndex        =   6
      Top             =   2530
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":3FFC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   1110
      Index           =   6
      Left            =   1920
      TabIndex        =   7
      Top             =   2530
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":41D3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   1110
      Index           =   7
      Left            =   3600
      TabIndex        =   8
      Top             =   2530
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":43AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   1110
      Index           =   8
      Left            =   5280
      TabIndex        =   9
      Top             =   2530
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":4581
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdResult 
      Height          =   1110
      Index           =   9
      Left            =   6960
      TabIndex        =   10
      Top             =   1240
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":4758
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdInfoFindings 
      Height          =   1095
      Left            =   7080
      TabIndex        =   11
      Top             =   2520
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "EventMsgs.frx":492F
   End
   Begin VB.Label lblMessage 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Message"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   6615
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmEventMsgs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Refactor: take out all calls to set messages individually.  Remove string variables.
Private msHeader As String
Private msTopOneText As String
Private msTopTwoText As String
Private msTopThreeText As String
Private msTopFourText As String
Private msTopFiveText As String
Private msBottomOneText As String
Private msBottomTwoText As String
Private msBottomThreeText As String
Private msBottomFourText As String
Public IsInfo As Boolean
Public FindindsInfoText As String
' Refactor: Make the result equal the Index of the button clicked, rather than some crazy number.
Private miResult As Integer
Private mfLoaded As Boolean ' Used to distinguish when new load functions are called

Public Property Let Header(ByVal sText As String)
    msHeader = sText
End Property

Public Property Get Header() As String
    Header = msHeader
End Property

Public Property Let RejectText(ByVal sText As String)
    msTopTwoText = sText
End Property

Public Property Let AcceptText(ByVal sText As String)
    msTopOneText = sText
End Property

Public Property Get AcceptText() As String
    AcceptText = msTopOneText
End Property

Public Property Let CancelText(ByVal sText As String)
    msTopThreeText = sText
End Property

Public Property Let Other0Text(ByVal sText As String)
    msTopFourText = sText
End Property

Public Property Get Other0Text() As String
    Other0Text = msTopFourText
End Property

Public Property Let Other1Text(ByVal sText As String)
    msBottomOneText = sText
End Property

Public Property Get Other1Text() As String
    Other1Text = msBottomOneText
End Property

Public Property Let Other2Text(ByVal sText As String)
    msBottomTwoText = sText
End Property

Public Property Let Other3Text(ByVal sText As String)
    msBottomThreeText = sText
End Property

Public Property Get Other3Text() As String
    Other3Text = msBottomThreeText
End Property

Public Property Let Other4Text(ByVal sText As String)
    msBottomFourText = sText
End Property

Public Property Get Other5Text() As String
    Other5Text = msTopFiveText
End Property

Public Property Let Other5Text(ByVal sText As String)
    msTopFiveText = sText
End Property

Public Property Get Result() As Integer
    Result = miResult
End Property

Public Property Let Result(ByVal sVal As Integer)
    miResult = sVal
End Property


Private Sub cmdInfoFindings_Click()
    On Error GoTo ICmdInfo
If FindindsInfoText = "" Then
    MsgBox "No Information Available!"
Else
 Dim tempResourceId As String
 Dim IsRecordExist As Boolean
 Dim tempExternalId As String
 Dim tempSnomedId As String
 Dim tempOrderString As String
 Dim OrderString As String
 Dim RS As Recordset
 Dim dPosition As Long
 Dim dPosSecond As Long
 FindindsInfoText = LTrim(FindindsInfoText)
 FindindsInfoText = RTrim(FindindsInfoText)
        dPosition = InStr(FindindsInfoText, ".")
        If dPosition > 0 Then
            dPosSecond = InStr(dPosition + 1, FindindsInfoText, ".")
            If dPosSecond > dPosition Then
                FindindsInfoText = Left$(FindindsInfoText, dPosSecond - 1)
            End If
        End If
        ' lngPos = 5
        IsRecordExist = False
 tempOrderString = "Select Top(1) ConceptId, Id as ExternalId, ResourceId from Model.PatientProblemList Where  COMMENTS ='" + FindindsInfoText + "' And PatientId = " + CStr(globalExam.iPatientId)
 Set RS = GetRS(tempOrderString)
 If RS.RecordCount > 0 Then
    tempResourceId = CStr(RS("ResourceId"))
    tempExternalId = CStr(RS("ExternalId"))
    tempSnomedId = CStr(RS("ConceptId"))
    IsRecordExist = True
 End If
 If IsRecordExist Then
    Dim ClinicalManager As New ClinicalIntegration.frmInfo
    ClinicalManager.PatientId = globalExam.iPatientId
    ClinicalManager.IsMedList = False
    ClinicalManager.IsProblemList = True
    ClinicalManager.DbObject = IdbConnection
    ClinicalManager.ResourceId = CStr(tempResourceId)
    ClinicalManager.AppointmentId = globalExam.iAppointmentId
    ClinicalManager.ExternalId = CStr(tempExternalId)
    ClinicalManager.IcdCode = FindindsInfoText
    ClinicalManager.CodeType = "ICD9"
    ClinicalManager.ShowDialog
 Else
    MsgBox "No Information Available!"
 End If
End If
Exit Sub
ICmdInfo:
    MsgBox "No Information Available!"
End Sub

Private Sub cmdResult_Click(Index As Integer)
    ' Refactor: make all the instantiations of this use Index instead of Result
    miResult = Index
    Select Case Index
        Case 0
            miResult = 4
        Case 3
            miResult = 4
        Case 4
            miResult = 3
    End Select
    mfLoaded = False
    Unload frmEventMsgs
End Sub



Private Sub form_load()
    cmdInfoFindings.Visible = False
    miResult = 0
    If Not mfLoaded Then
        SetButtonsOld
    End If
    mfLoaded = False
End Sub

Private Function InitializeButtons() As Boolean
Dim iIndex As Long
    For iIndex = 0 To cmdResult.Count - 1
        cmdResult(iIndex).Visible = False
    Next
End Function

Public Function InfoMessage(Message As String) As Boolean
    InitializeButtons
    lblMessage.Caption = Message
    cmdResult(0).Text = "OK"
    cmdResult(0).Visible = True
    mfLoaded = True
End Function

Public Function SetButtons(Header As String, _
                            Optional OptionOne As String = "", _
                            Optional OptionTwo As String = "", _
                            Optional OptionThree As String = "", _
                            Optional OptionFour As String = "", _
                            Optional OptionFive As String = "", _
                            Optional OptionSix As String = "", _
                            Optional OptionSeven As String = "", _
                            Optional OptionEight As String = "", Optional OptionNine As String = "") As Boolean
Dim iIndex As Long

    On Error GoTo lSetButtons_Error

    lblMessage.Caption = Header
    cmdResult(0).Text = ""
    cmdResult(1).Text = OptionOne
    cmdResult(2).Text = OptionTwo
    cmdResult(3).Text = OptionThree
    cmdResult(4).Text = OptionFour
    cmdResult(5).Text = OptionFive
    cmdResult(6).Text = OptionSix
    cmdResult(7).Text = OptionSeven
    cmdResult(8).Text = OptionEight
    cmdResult(9).Text = OptionNine
    For iIndex = 0 To cmdResult.Count - 1
        cmdResult(iIndex).Visible = cmdResult(iIndex).Text <> ""
    Next
    mfLoaded = True

    Exit Function

lSetButtons_Error:

    LogError "frmEventMsgs", "SetButtons", Err, Err.Description

End Function
                            
Private Function SetButtonsOld() As Boolean
    If Trim$(msTopOneText) = "" And Trim$(msTopTwoText) = "" And Trim$(msTopThreeText) = "" And Trim$(msTopFourText) = "" And _
        Trim$(msBottomOneText) = "" And Trim$(msBottomTwoText) = "" And Trim$(msBottomThreeText) = "" And Trim$(msBottomFourText) = "" And Trim$(msTopFiveText) = "" Then
        cmdResult(0).Visible = False
        cmdResult(1).Visible = False
        cmdResult(2).Visible = False
        cmdResult(3).Visible = False
        cmdResult(4).Visible = False
        cmdResult(5).Visible = False
        cmdResult(6).Visible = False
        cmdResult(7).Visible = False
        cmdResult(8).Visible = False
        cmdResult(9).Visible = False
    Else
        lblMessage.Caption = Trim$(msHeader)
        If Trim$(msTopOneText) = "" And Trim$(msTopTwoText) <> "" And Trim$(msTopThreeText) = "" And Trim$(msTopFourText) = "" And _
            Trim$(msBottomOneText) = "" And Trim$(msBottomTwoText) = "" And Trim$(msBottomThreeText) = "" And Trim$(msBottomFourText) = "" And Trim$(msTopFiveText) = "" Then
            cmdResult(0).Text = msTopTwoText
            cmdResult(0).Visible = True
            cmdResult(1).Visible = False
            cmdResult(2).Visible = False
            cmdResult(3).Visible = False
            cmdResult(4).Visible = False
            cmdResult(5).Visible = False
            cmdResult(6).Visible = False
            cmdResult(7).Visible = False
            cmdResult(8).Visible = False
            cmdResult(9).Visible = False
        ElseIf Trim$(msTopOneText) = "" And Trim$(msTopTwoText) = "" And Trim$(msTopThreeText) <> "" And Trim$(msTopFourText) = "" And _
            Trim$(msBottomOneText) = "" And Trim$(msBottomTwoText) = "" And Trim$(msBottomThreeText) = "" And Trim$(msBottomFourText) = "" And Trim$(msTopFiveText) = "" Then
            cmdResult(0).Text = msTopThreeText
            cmdResult(0).Visible = True
            cmdResult(1).Visible = False
            cmdResult(2).Visible = False
            cmdResult(3).Visible = False
            cmdResult(4).Visible = False
            cmdResult(5).Visible = False
            cmdResult(6).Visible = False
            cmdResult(7).Visible = False
            cmdResult(8).Visible = False
            cmdResult(9).Visible = False
        Else
            cmdResult(0).Visible = False
            cmdResult(1).Visible = False
            cmdResult(2).Visible = False
            cmdResult(3).Visible = False
            cmdResult(4).Visible = False
            cmdResult(5).Visible = False
            cmdResult(6).Visible = False
            cmdResult(7).Visible = False
            cmdResult(8).Visible = False
            cmdResult(9).Visible = False
            If Trim$(msTopOneText) <> "" Then
                cmdResult(1).Text = msTopOneText
                cmdResult(1).Visible = True
            End If
            If Trim$(msTopTwoText) <> "" Then
                cmdResult(2).Text = msTopTwoText
                cmdResult(2).Visible = True
            End If
            If Trim$(msTopThreeText) <> "" Then
                cmdResult(3).Text = msTopThreeText
                cmdResult(3).Visible = True
            Else
                If cmdResult(1).Text <> "Cancel" Then
                    cmdResult(3).Text = "Cancel"
                    cmdResult(3).Visible = True
                End If
            End If
            If Trim$(msTopFourText) <> "" Then
                cmdResult(4).Text = msTopFourText
                cmdResult(4).Visible = True
            End If
            If Trim$(msBottomOneText) <> "" Then
                cmdResult(5).Text = msBottomOneText
                cmdResult(5).Visible = True
            End If
            If Trim$(msBottomTwoText) <> "" Then
                cmdResult(6).Text = msBottomTwoText
                cmdResult(6).Visible = True
            End If
            If Trim$(msBottomThreeText) <> "" Then
                cmdResult(7).Text = msBottomThreeText
                cmdResult(7).Visible = True
            End If
            If Trim$(msBottomFourText) <> "" Then
                cmdResult(8).Text = msBottomFourText
                cmdResult(8).Visible = True
            End If
            If (IsInfo = True) Then
                If Trim$(msTopFiveText) <> "" Then
                    cmdResult(9).Text = msTopFiveText
                    cmdResult(9).Visible = True
                End If
            Else
            cmdResult(9).Visible = False
            End If
        End If
    End If
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If miResult = 0 Then
    miResult = 4
End If
mfLoaded = False
Unload frmEventMsgs
End Sub
Public Function FrmClose()
 Unload Me
End Function
