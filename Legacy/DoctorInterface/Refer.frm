VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmRefer 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   8460
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6645
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   8460
   ScaleWidth      =   6645
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt1 
      Height          =   990
      Left            =   1560
      TabIndex        =   0
      Top             =   600
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Refer.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt2 
      Height          =   990
      Left            =   1560
      TabIndex        =   1
      Top             =   1680
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Refer.frx":01E1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt3 
      Height          =   990
      Left            =   1560
      TabIndex        =   2
      Top             =   2760
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Refer.frx":03C2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt4 
      Height          =   990
      Left            =   1560
      TabIndex        =   3
      Top             =   3840
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Refer.frx":05A3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt5 
      Height          =   990
      Left            =   1560
      TabIndex        =   4
      Top             =   4920
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Refer.frx":0784
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt6 
      Height          =   990
      Left            =   1560
      TabIndex        =   5
      Top             =   6000
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Refer.frx":0965
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   990
      Left            =   1560
      TabIndex        =   6
      Top             =   7080
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Refer.frx":0B46
   End
   Begin VB.Label lblField 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Select a Doctor"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   240
      TabIndex        =   7
      Top             =   120
      Width           =   1740
   End
End
Attribute VB_Name = "frmRefer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public VendorType As String
Public ReferralName As String
Public ApptType As Long

Private CurrentIndex As Integer
Private TotalRefs As Long

Private Sub ClearRefer()
cmdAppt1.Text = ""
cmdAppt1.Visible = False
cmdAppt2.Text = ""
cmdAppt2.Visible = False
cmdAppt3.Text = ""
cmdAppt3.Visible = False
cmdAppt4.Text = ""
cmdAppt4.Visible = False
cmdAppt5.Text = ""
cmdAppt5.Visible = False
cmdAppt6.Text = ""
cmdAppt6.Visible = False
cmdMore.Text = "More"
cmdMore.Visible = False
End Sub

Private Sub cmdAppt1_Click()
ReferralName = cmdAppt1.Text
Unload frmRefer
End Sub

Private Sub cmdAppt2_Click()
ReferralName = cmdAppt2.Text
Unload frmRefer
End Sub

Private Sub cmdAppt3_Click()
ReferralName = cmdAppt3.Text
Unload frmRefer
End Sub

Private Sub cmdAppt4_Click()
ReferralName = cmdAppt4.Text
Unload frmRefer
End Sub

Private Sub cmdAppt5_Click()
ReferralName = cmdAppt5.Text
Unload frmRefer
End Sub

Private Sub cmdAppt6_Click()
ReferralName = cmdAppt6.Text
Unload frmRefer
End Sub

Private Sub cmdMore_Click()
If (CurrentIndex > TotalRefs) Then
    CurrentIndex = 1
End If
Call LoadReferral
End Sub

Public Function LoadReferral() As Boolean
Dim i As Integer
Dim GoodDr As Boolean
Dim DontCare As Boolean
Dim ButtonCnt As Integer
Dim RetrieveReferral As PracticeVendors
Dim RetrieveResource As SchedulerResource
Dim RetApptType As SchedulerAppointmentType
LoadReferral = False
Call ClearRefer
If (VendorType = "D") Or (VendorType = "H") Then
    Set RetrieveReferral = New PracticeVendors
    RetrieveReferral.VendorName = Chr(1)
    RetrieveReferral.VendorType = VendorType
    TotalRefs = RetrieveReferral.FindVendor
    If (TotalRefs > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetrieveReferral.SelectVendor(i)) And (ButtonCnt < 7)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = RetrieveReferral.VendorName
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = RetrieveReferral.VendorName
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = RetrieveReferral.VendorName
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = RetrieveReferral.VendorName
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = RetrieveReferral.VendorName
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = RetrieveReferral.VendorName
                cmdAppt6.Visible = True
            End If
            LoadReferral = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i
    End If
ElseIf (VendorType = "S") Then
    DontCare = False
    Set RetApptType = New SchedulerAppointmentType
    If (ApptType > 0) Then
        RetApptType.AppointmentTypeId = ApptType
        If (RetApptType.RetrieveSchedulerAppointmentType) Then
            If (RetApptType.AppointmentTypeResourceId1 = 0) And _
               (RetApptType.AppointmentTypeResourceId2 = 0) And _
               (RetApptType.AppointmentTypeResourceId3 = 0) And _
               (RetApptType.AppointmentTypeResourceId4 = 0) And _
               (RetApptType.AppointmentTypeResourceId5 = 0) And _
               (RetApptType.AppointmentTypeResourceId6 = 0) And _
               (RetApptType.AppointmentTypeResourceId7 = 0) Then
                DontCare = True
            End If
        End If
    End If
    Set RetrieveResource = New SchedulerResource
    RetrieveResource.ResourceName = Chr(1)
    RetrieveResource.ResourceType = VendorType
    TotalRefs = RetrieveResource.FindResource
    If (TotalRefs > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetrieveResource.SelectResource(i)) And (ButtonCnt < 7)
            GoodDr = False
            If (DontCare) Then
                GoodDr = True
            Else
                If (RetApptType.AppointmentTypeResourceId1 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId2 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId3 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId4 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId5 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId6 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId7 = RetrieveResource.ResourceId) Or _
                   (ApptType < 1) Then
                    GoodDr = True
                End If
            End If
            If (GoodDr) Then
                If (ButtonCnt = 1) Then
                    cmdAppt1.Text = RetrieveResource.ResourceDescription
                    cmdAppt1.Visible = True
                ElseIf (ButtonCnt = 2) Then
                    cmdAppt2.Text = RetrieveResource.ResourceDescription
                    cmdAppt2.Visible = True
                ElseIf (ButtonCnt = 3) Then
                    cmdAppt3.Text = RetrieveResource.ResourceDescription
                    cmdAppt3.Visible = True
                ElseIf (ButtonCnt = 4) Then
                    cmdAppt4.Text = RetrieveResource.ResourceDescription
                    cmdAppt4.Visible = True
                ElseIf (ButtonCnt = 5) Then
                    cmdAppt5.Text = RetrieveResource.ResourceDescription
                    cmdAppt5.Visible = True
                ElseIf (ButtonCnt = 6) Then
                    cmdAppt6.Text = RetrieveResource.ResourceDescription
                    cmdAppt6.Visible = True
                End If
                LoadReferral = True
            End If
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i
        Set RetApptType = Nothing
    End If
Else
    Exit Function
End If
If (CurrentIndex <= TotalRefs) Then
    cmdMore.Text = "More"
    cmdMore.Visible = True
Else
    cmdMore.Text = "Beginning of List"
    cmdMore.Visible = True
End If
If (TotalRefs < 7) Then
    cmdMore.Visible = False
End If
Set RetrieveReferral = Nothing
Set RetrieveResource = Nothing
End Function

Private Sub Form_Load()
CurrentIndex = 1
End Sub

