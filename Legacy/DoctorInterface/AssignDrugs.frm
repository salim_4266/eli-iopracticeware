VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmAssignDrugs 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9345
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12660
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "AssignDrugs.frx":0000
   ScaleHeight     =   9345
   ScaleWidth      =   12660
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin MSFlexGridLib.MSFlexGrid FGC 
      Height          =   1695
      Left            =   1920
      TabIndex        =   41
      Top             =   2040
      Visible         =   0   'False
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   2990
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      BackColor       =   16250357
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   14
      Left            =   8040
      TabIndex        =   38
      Top             =   5280
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   13
      Left            =   8040
      TabIndex        =   37
      Top             =   4080
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":38A6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   12
      Left            =   8040
      TabIndex        =   36
      Top             =   2880
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":3A83
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   11
      Left            =   8040
      TabIndex        =   35
      Top             =   1680
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":3C60
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   10
      Left            =   8040
      TabIndex        =   34
      Top             =   480
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":3E3D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   9
      Left            =   4080
      TabIndex        =   33
      Top             =   5280
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":401A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   8
      Left            =   4080
      TabIndex        =   32
      Top             =   4080
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":41F7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   7
      Left            =   4080
      TabIndex        =   31
      Top             =   2880
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":43D4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   6
      Left            =   4080
      TabIndex        =   30
      Top             =   1680
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":45B1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   5
      Left            =   4080
      TabIndex        =   29
      Top             =   480
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":478E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   4
      Left            =   120
      TabIndex        =   27
      Top             =   5280
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":496B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   3
      Left            =   120
      TabIndex        =   26
      Top             =   4080
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":4B48
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   2
      Left            =   120
      TabIndex        =   25
      Top             =   2880
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":4D25
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   1
      Left            =   120
      TabIndex        =   24
      Top             =   1680
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":4F02
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Index           =   0
      Left            =   120
      TabIndex        =   23
      Top             =   480
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":50DF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10200
      TabIndex        =   12
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":52BC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect 
      Height          =   975
      Left            =   6360
      TabIndex        =   17
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":54EF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFavRx 
      Height          =   975
      Left            =   4080
      TabIndex        =   19
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":572C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFavFm 
      Height          =   975
      Left            =   2280
      TabIndex        =   20
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":5969
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdListFamily 
      Height          =   975
      Left            =   120
      TabIndex        =   21
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":5BAB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx10 
      Height          =   1095
      Left            =   4080
      TabIndex        =   11
      Top             =   5280
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":5DE9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx9 
      Height          =   1095
      Left            =   4080
      TabIndex        =   10
      Top             =   4080
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":5FC8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx7 
      Height          =   1095
      Left            =   4080
      TabIndex        =   15
      Top             =   1680
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":61A6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx6 
      Height          =   1095
      Left            =   4080
      TabIndex        =   0
      Top             =   480
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":6384
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx11 
      Height          =   1095
      Left            =   8040
      TabIndex        =   5
      Top             =   480
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":6562
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx12 
      Height          =   1095
      Left            =   8040
      TabIndex        =   6
      Top             =   1680
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":6741
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx13 
      Height          =   1095
      Left            =   8040
      TabIndex        =   7
      Top             =   2880
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":6920
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx14 
      Height          =   1095
      Left            =   8040
      TabIndex        =   8
      Top             =   4080
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":6AFF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx15 
      Height          =   1095
      Left            =   8040
      TabIndex        =   9
      Top             =   5280
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":6CDE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx2 
      Height          =   1095
      Left            =   120
      TabIndex        =   2
      Top             =   1680
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":6EBD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx3 
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   2880
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":709B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx4 
      Height          =   1095
      Left            =   120
      TabIndex        =   4
      Top             =   4080
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":7279
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx5 
      Height          =   1095
      Left            =   120
      TabIndex        =   14
      Top             =   5280
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":7457
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   1095
      Left            =   120
      TabIndex        =   18
      Top             =   6480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":7635
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreRx 
      Height          =   1095
      Left            =   8040
      TabIndex        =   13
      Top             =   6480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":7818
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx1 
      Height          =   1095
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":7A00
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx8 
      Height          =   1095
      Left            =   4080
      TabIndex        =   28
      Top             =   2880
      Visible         =   0   'False
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":7BDE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelectAll 
      Height          =   975
      Left            =   8160
      TabIndex        =   39
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignDrugs.frx":7DBC
   End
   Begin VB.Label lblPatient 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Name, age, dob "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   4320
      TabIndex        =   40
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label lblType 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Assign An Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   9000
      TabIndex        =   22
      Top             =   120
      Width           =   1545
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Assign An Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   360
      TabIndex        =   16
      Top             =   120
      Width           =   1545
   End
End
Attribute VB_Name = "frmAssignDrugs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Diagnosis As String
Public PatientId As Long
Public AppointmentId As Long
Public LastOn As Boolean
Public FavoriteOn As Boolean
Public NameOnly As Boolean
Public SelectedDrug As String
Public EditPreviousOn As Boolean


Private Const MaxSetRx As Integer = 100
Private Type ExternalReference
    Id As String
    Name As String
    Dosage As String
    Duration As String
    Frequency As String
    Refills As String
    Notes As String
    Ref As Long
    DrugRxRecordFormat As String
End Type
Private PageCnt As Integer
Private BaseBackColor As Long
Private BaseTextColor As Long
Private TextSelectionColor As Long
Private ButtonSelectionColor As Long
Private FavoriteFamilyOn As Boolean
Private FavoriteMedsOn As Boolean
Private DrugName As String
Private DrugFamily As String
Private MaxRx As Integer
Private MaxAlias As Integer
Private MaxFavoriteMeds As Integer
Private MaxFavoriteFamily As Integer
Private CurrentRxIndex As Integer
Private SelectedRx(MaxSetRx) As ExternalReference

Private Const MAX_BUTTONS As Long = 15
Public RSAll As Recordset
Dim StartNum As Long

Public frmRxManage_Write_RX As Boolean

Private Sub cmdDone_Click()
If NameOnly Then
    'No action
Else
    Call PostRxComplete
End If
Unload frmAssignDrugs
End Sub

Private Sub cmdSelect_Click()
frmAlphaPad.NumPad_Field = "Name of Drug"
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.Show 1
If (Trim(frmAlphaPad.NumPad_Result) <> "") Then
    PageCnt = 1
    Call LoadRx("", Trim(frmAlphaPad.NumPad_Result))
End If
End Sub

Private Sub cmdFavRx_Click()
FavoriteMedsOn = True
FavoriteFamilyOn = False
PageCnt = 1
Call LoadFavoriteMeds
End Sub

Private Sub cmdFavFm_Click()
FavoriteMedsOn = False
FavoriteFamilyOn = True
PageCnt = 1
Call LoadFmFavoriteFamily
End Sub

Private Sub cmdListFamily_Click()
FavoriteMedsOn = False
FavoriteFamilyOn = False
PageCnt = 1
Call LoadFm
End Sub

Private Sub cmdRx_Click(Index As Integer)
LoadRx cmdRx(Index).Text, ""
End Sub

Private Sub cmdRx1_Click()
Call TriggerRx(cmdRx1)
End Sub

Private Sub cmdRx2_Click()
Call TriggerRx(cmdRx2)
End Sub

Private Sub cmdRx3_Click()
Call TriggerRx(cmdRx3)
End Sub

Private Sub cmdRx4_Click()
Call TriggerRx(cmdRx4)
End Sub

Private Sub cmdRx5_Click()
Call TriggerRx(cmdRx5)
End Sub

Private Sub cmdRx6_Click()
Call TriggerRx(cmdRx6)
End Sub

Private Sub cmdRx7_Click()
Call TriggerRx(cmdRx7)
End Sub

Private Sub cmdRx8_Click()
Call TriggerRx(cmdRx8)
End Sub

Private Sub cmdRx9_Click()
Call TriggerRx(cmdRx9)
End Sub

Private Sub cmdRx10_Click()
Call TriggerRx(cmdRx10)
End Sub

Private Sub cmdRx11_Click()
Call TriggerRx(cmdRx11)
End Sub

Private Sub cmdRx12_Click()
Call TriggerRx(cmdRx12)
End Sub

Private Sub cmdRx13_Click()
Call TriggerRx(cmdRx13)
End Sub

Private Sub cmdRx14_Click()
Call TriggerRx(cmdRx14)
End Sub

Private Sub cmdRx15_Click()
Call TriggerRx(cmdRx15)
End Sub

Private Sub cmdMoreRx_Click()
If lblType.Caption = "All Drugs" Then
    StartNum = StartNum + 15
    LoadRxAll
    Exit Sub
End If
PageCnt = PageCnt + 1
If (cmdMoreRx.Visible) And (cmdMoreRx.Text = "MORE Categories") Then
    If (((PageCnt - 1) * 15) + 1 > MaxAlias) Then
        PageCnt = 1
    End If
    Call LoadFm
ElseIf (cmdMoreRx.Visible) And (cmdMoreRx.Text = "MORE Favorite Categories") Then
    If (((PageCnt - 1) * 15) + 1 > MaxFavoriteFamily) Then
        PageCnt = 1
    End If
    Call LoadFmFavoriteFamily
ElseIf (cmdMoreRx.Visible) And (cmdMoreRx.Text = "MORE Favorite Meds") Then
    If (((PageCnt - 1) * 15) + 1 > MaxFavoriteMeds) Then
        PageCnt = 1
    End If
    Call LoadFavoriteMeds
Else
    If (((PageCnt - 1) * 15) + 1 > MaxRx) Then
        PageCnt = 1
    End If
    Call LoadRx(DrugFamily, DrugName)
End If
End Sub

Private Sub cmdPrev_Click()
If lblType.Caption = "All Drugs" Then
    StartNum = StartNum - 15
    LoadRxAll
    Exit Sub
End If



PageCnt = PageCnt - 1
If (PageCnt < 1) Then
    PageCnt = 1
End If
If (cmdPrev.Visible) And (cmdPrev.Text = "Previous Categories") Then
    Call LoadFm
ElseIf (cmdPrev.Visible) And (cmdPrev.Text = "Previous Favorite Categories") Then
    Call LoadFmFavoriteFamily
ElseIf (cmdPrev.Visible) And (cmdPrev.Text = "Previous Favorite Meds") Then
    Call LoadFavoriteMeds
Else
    Call LoadRx(DrugFamily, DrugName)
End If
End Sub


Private Function PracticeMed(AButton As fpBtn) As Boolean
Dim fPracticeMed As Boolean
Dim clsDrug As New CDrug
clsDrug.DisplayName = AButton.Text
fPracticeMed = clsDrug.SelectDrug(1)
If fPracticeMed Then
    AButton.Tag = clsDrug.DrugId
Else
    frmEventMsgs.Header = Trim(AButton.Text) + " is not listed in Practice Meds"
    frmEventMsgs.AcceptText = "Add (Ocular)"
    frmEventMsgs.RejectText = "Add (Non Ocular)"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If Not frmEventMsgs.Result = 4 Then
        clsDrug.Drugcode = "T"
        clsDrug.Focus = (frmEventMsgs.Result = 1)
        clsDrug.Family1 = ""
        clsDrug.Family2 = ""
        clsDrug.PutDrug
        clsDrug.SelectDrug 1
        AButton.Tag = clsDrug.DrugId
        fPracticeMed = True
    End If
End If
Set clsDrug = Nothing
PracticeMed = fPracticeMed
End Function

Private Sub TriggerRx(AButton As fpBtn)
If (Trim(AButton.ToolTipText) = "") Then
    If Not PracticeMed(AButton) Then
        If NameOnly Then
            Call cmdDone_Click
        End If
        Exit Sub
    End If
End If

If NameOnly Then
    SelectedDrug = AButton.Text
    Call cmdDone_Click
    Exit Sub
Else
End If

Dim i As Integer
Dim ClearRx As Integer
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
' Check if present already
    ClearRx = 0
    For i = 1 To CurrentRxIndex
        If (Trim(AButton.Tag) = Trim(str(SelectedRx(i).Ref))) Then
            ClearRx = i
            Exit For
        End If
    Next i
    If (ClearRx = 0) And (CurrentRxIndex < MaxSetRx) Then
        SelectedRx(CurrentRxIndex).Id = ""
        SelectedRx(CurrentRxIndex).Name = Trim(AButton.Text)
        SelectedRx(CurrentRxIndex).Ref = Trim(str(AButton.Tag))
        If (Trim(AButton.ToolTipText) <> "") Then
            Call SetRxDetails(CurrentRxIndex, Trim(AButton.ToolTipText))
        Else
            Call SetRxDetails(CurrentRxIndex, "")
        End If
        CurrentRxIndex = CurrentRxIndex + 1
        If frmRxManage_Write_RX Then
            Call cmdDone_Click
        End If
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    For i = 1 To CurrentRxIndex
        If (Trim(AButton.Tag) = Trim(str(SelectedRx(i).Ref))) Then
            SelectedRx(i).Id = ""
            SelectedRx(i).Name = ""
            SelectedRx(i).Dosage = ""
            SelectedRx(i).Frequency = ""
            SelectedRx(i).Refills = ""
            SelectedRx(i).Notes = ""
            SelectedRx(i).Ref = 0
            ClearRx = i
            Exit For
        End If
    Next i
    For i = ClearRx To CurrentRxIndex
        SelectedRx(i).Id = SelectedRx(i + 1).Id
        SelectedRx(i).Name = SelectedRx(i + 1).Name
        SelectedRx(i).Dosage = SelectedRx(i + 1).Dosage
        SelectedRx(i).Frequency = SelectedRx(i + 1).Frequency
        SelectedRx(i).Refills = SelectedRx(i + 1).Refills
        SelectedRx(i).Ref = SelectedRx(i + 1).Ref
        SelectedRx(i).Notes = SelectedRx(i + 1).Notes
    Next i
    CurrentRxIndex = CurrentRxIndex - 1
    If (CurrentRxIndex < 1) Then
        CurrentRxIndex = 1
    End If
End If
End Sub

Private Sub PostRxComplete()
cmdRx1.Visible = False
cmdRx2.Visible = False
cmdRx3.Visible = False
cmdRx4.Visible = False
cmdRx5.Visible = False
cmdRx6.Visible = False
cmdRx7.Visible = False
cmdRx8.Visible = False
cmdRx9.Visible = False
cmdRx10.Visible = False
cmdRx11.Visible = False
cmdRx12.Visible = False
cmdRx13.Visible = False
cmdRx14.Visible = False
cmdRx15.Visible = False
cmdMoreRx.Visible = False
If (CurrentRxIndex < 2) Then
    CurrentRxIndex = 1
End If
PageCnt = 1
Call cmdFavFm_Click
End Sub

Private Function LoadRx(Family As String, DrugRef As String) As Boolean
Dim i As Integer
Dim Ref As Integer
Dim ButtonCnt As Integer
Dim TheDisplayName As String
Dim TheTag As String
Dim RetrieveDrug As DiagnosisMasterDrugs
LoadRx = True
DrugFamily = Family
DrugName = DrugRef
Set RetrieveDrug = New DiagnosisMasterDrugs
RetrieveDrug.DrugCompany = ""
RetrieveDrug.DrugGenericName = ""
RetrieveDrug.DrugRank = 0
If (Trim(DrugName) <> "") Then
    Set RSAll = RetrieveDrug.GetDrugByPartName(DrugName)
Else
    Set RSAll = RetrieveDrug.GetDrugByFamily(Family)
End If
StartNum = 0
LoadRxAll
End Function

Private Function LoadFm() As Boolean
Dim i As Integer
Dim Ref As Integer
Dim ButtonCnt As Integer
Dim Temp1 As String
Dim PCode As DiagnosisMasterDrugs
Dim rsFm As Recordset
LoadFm = True
ClearButtons
Call ClearRxs
cmdMoreRx.Visible = False
Set PCode = New DiagnosisMasterDrugs
ButtonCnt = 1
i = ((PageCnt - 1) * 15) + 1
Set rsFm = PCode.FindDrugFamilies
MaxAlias = rsFm.RecordCount
If Not i > MaxAlias Then rsFm.Move i - 1
Do While Not rsFm.EOF And (ButtonCnt < 16) And (i <= MaxAlias)
    Temp1 = rsFm("code")
    Ref = ButtonCnt
    cmdRx(Ref - 1).Text = Temp1
    cmdRx(Ref - 1).Visible = True
    ButtonCnt = ButtonCnt + 1
    rsFm.MoveNext
    i = i + 1
Loop
Set PCode = Nothing
If (MaxAlias > 15) Then
    cmdMoreRx.Visible = True
    cmdMoreRx.Text = "MORE Categories"
    cmdPrev.Visible = True
    cmdPrev.Text = "Previous Categories"
Else
    cmdPrev.Visible = False
    cmdMoreRx.Visible = False
End If
lblType.Caption = "Categories"


cmdPrev.Visible = (PageCnt > 1)
End Function

Private Function LoadFmFavoriteFamily() As Boolean
Dim i As Integer
Dim Ref As Integer
Dim ButtonCnt As Integer
Dim FCode As PracticeFavorites
Dim PCode As DiagnosisMasterDrugs
LoadFmFavoriteFamily = True
Set FCode = New PracticeFavorites
Set PCode = New DiagnosisMasterDrugs
If (FavoriteFamilyOn) Then
    FCode.FavoritesSystem = "*"
    FCode.FavoritesRank = 0
    FCode.FavoritesDiagnosis = ""
    MaxFavoriteFamily = FCode.FindFavorites
    If (MaxFavoriteFamily > 0) Then
        ClearButtons
        Call ClearRxs
        cmdMoreRx.Visible = False
        i = ((PageCnt - 1) * 15) + 1
        ButtonCnt = 1
        While (FCode.SelectFavorites(i)) And (ButtonCnt < 16)
            PCode.DrugId = Val(FCode.FavoritesDiagnosis)
            If (PCode.RetrieveDrug) Then
                Ref = ButtonCnt
                cmdRx(Ref - 1).Text = Trim$(PCode.DrugDiagnosisAlias1)
                cmdRx(Ref - 1).Visible = True
            End If
            ButtonCnt = ButtonCnt + 1
            i = i + 1
        Wend
    End If
End If
Set PCode = Nothing
Set FCode = Nothing
If (MaxFavoriteFamily > 15) Then
    cmdMoreRx.Visible = True
    cmdMoreRx.Text = "MORE Favorite Categories"
    cmdPrev.Visible = True
    cmdPrev.Text = "Previous Favorite Categories"
Else
    cmdPrev.Visible = False
    cmdMoreRx.Visible = False
End If
lblType.Caption = "Favorite Categories"
End Function

Private Function LoadFavoriteMeds() As Boolean
Dim i As Integer
Dim Ref As Integer
Dim ButtonCnt As Integer
Dim TheDisplayName As String
Dim TheTag As String
Dim FCode As PracticeFavorites

Dim Reload As Boolean
Dim btn As fpBtn
Dim r As Integer

'Dim PCode As DiagnosisMasterDrugs
Dim clsDrug As New CDrug

LoadFavoriteMeds = True
Set FCode = New PracticeFavorites
'Set PCode = New DiagnosisMasterDrugs
If (FavoriteMedsOn) Then
    FCode.FavoritesSystem = "-"
    FCode.FavoritesRank = 0
    FCode.FavoritesDiagnosis = ""
    MaxFavoriteMeds = FCode.FindFavorites
    If (MaxFavoriteMeds > 0) Then
        ClearButtons
        Call ClearRxs
        cmdMoreRx.Visible = False

        
        Do
            i = ((PageCnt - 1) * 15) + 1
            fgc.Rows = 1
            ButtonCnt = 1
            While (FCode.SelectFavorites(i)) And (ButtonCnt < 16)
                'PCode.DrugId = Val(FCode.FavoritesDiagnosis)
                'If (PCode.RetrieveDrug) Then
                clsDrug.DrugId = FCode.FavoritesDiagnosis
                If clsDrug.SelectDrug(0) Then
                    TheDisplayName = Trim(clsDrug.DisplayName)
                    TheTag = Trim(str(clsDrug.DrugId))
                    
                    fgc.AddItem _
                                CInt(FCode.FavoritesRank) & vbTab & _
                                TheDisplayName & vbTab & _
                                TheTag & vbTab & _
                                TheDisplayName & "-2/" & Trim(FCode.FavoritesAlternate)
                    
                    ButtonCnt = ButtonCnt + 1
                End If
                i = i + 1
            Wend
            If ButtonCnt = 1 Then
                If Reload Then
                    Exit Do
                Else
                    Reload = True
                    PageCnt = 1
                End If
            Else
                Exit Do
            End If
        Loop
    End If
End If



fgc.col = 0
fgc.ColSel = 1
fgc.Sort = flexSortStringAscending

For r = 1 To fgc.Rows - 1
    Select Case r
    Case 1
        Set btn = cmdRx1
    Case 2
        Set btn = cmdRx2
    Case 3
        Set btn = cmdRx3
    Case 4
        Set btn = cmdRx4
    Case 5
        Set btn = cmdRx5
    Case 6
        Set btn = cmdRx6
    Case 7
        Set btn = cmdRx7
    Case 8
        Set btn = cmdRx8
    Case 9
        Set btn = cmdRx9
    Case 10
        Set btn = cmdRx10
    Case 11
        Set btn = cmdRx11
    Case 12
        Set btn = cmdRx12
    Case 13
        Set btn = cmdRx13
    Case 14
        Set btn = cmdRx14
    Case 15
        Set btn = cmdRx15
    End Select
    btn.Text = fgc.TextMatrix(r, 1)
    btn.Tag = fgc.TextMatrix(r, 2)
    btn.ToolTipText = fgc.TextMatrix(r, 3)
    btn.Visible = True
    If (IsRxSet(btn.Text)) Then
        cmdRx15.BackColor = ButtonSelectionColor
        cmdRx15.ForeColor = TextSelectionColor
    End If
Next

Set btn = Nothing


Set FCode = Nothing
If (MaxFavoriteMeds > 15) Then
    cmdMoreRx.Text = "MORE Favorite Meds"
    cmdMoreRx.Visible = True
    cmdPrev.Text = "Previous Favorite Meds"
    cmdPrev.Visible = True
Else
    cmdMoreRx.Visible = False
    cmdPrev.Visible = False
End If
lblType.Caption = "Favorite Meds"
End Function

Public Function IsRxSet(TheRx As String) As Boolean
Dim i As Integer
IsRxSet = False
For i = 1 To TotalRx
    If (Trim(SelectedRx(i).Name) = TheRx) Then
        IsRxSet = True
        Exit For
    End If
Next i
End Function

Private Function SetRx(Ref As Integer, TheRx As String, TheDiag As String, TheDosage As String, _
 TheFreq As String, TheRefills As String, TheDur As String, TheNote As String, TheId As String) As Boolean
SetRx = False
TheRx = ""
TheDiag = ""
TheDosage = ""
TheFreq = ""
TheRefills = ""
TheDur = ""
TheNote = ""
If (Ref < 1) Then
    Exit Function
End If
If (Trim(SelectedRx(Ref).Name) <> "") Then
    TheRx = SelectedRx(Ref).Name
    TheDiag = ""
    TheDosage = SelectedRx(Ref).Dosage
    TheFreq = SelectedRx(Ref).Frequency
    TheDur = SelectedRx(Ref).Duration
    TheRefills = SelectedRx(Ref).Refills
    TheNote = SelectedRx(Ref).Notes
    TheId = SelectedRx(Ref).Ref
    SetRx = True
End If
End Function

Public Function getRx_6(Ref As Integer, TheRx As String, EFormat As String) As Boolean
Dim RxRec As String
TheRx = "ERROR"
If Ref > 0 Then
    If (Trim(SelectedRx(Ref).Name) <> "") Then
        RxRec = SelectedRx(Ref).DrugRxRecordFormat
        If EFormat = "record2" Then
            TheRx = RxRec
        Else
            Dim tmp As New CDrugRx
            Call tmp.LoadRxFromDB("Rx-8/" & RxRec)
            TheRx = tmp.GetDrugRxFormat(EFormat)
            Set tmp = Nothing
        End If
    End If
End If
getRx_6 = Not (TheRx = "ERROR")
End Function

Public Function InitializeRx() As Boolean
Dim i As Integer
InitializeRx = True
For i = 1 To MaxSetRx
    SelectedRx(i).Id = ""
    SelectedRx(i).Name = ""
    SelectedRx(i).Dosage = ""
    SelectedRx(i).Duration = ""
    SelectedRx(i).Frequency = ""
    SelectedRx(i).Refills = ""
    SelectedRx(i).Notes = ""
    SelectedRx(i).Ref = 0
Next i
End Function

Public Function TotalRx() As Integer
Dim i As Integer
TotalRx = 0
For i = 1 To MaxSetRx
    If (Trim(SelectedRx(i).Ref) = 0) Then
        TotalRx = i - 1
        Exit For
    End If
Next i
End Function

Public Function getRx(RefId As Integer, ARx As String, ADiag As String, ADosage As String, _
AFrequency As String, ARefills As String, ADur As String, ANote As String, AId As String) As Boolean
MsgBox "getRx to be removed"

getRx = SetRx(RefId, ARx, ADiag, ADosage, AFrequency, ARefills, ADur, ANote, AId)
End Function

Private Sub SetRxDetails(Item As Integer, RxDetails As String)

Dim fLoadRx As Boolean
    If Item > 0 Then
        If (Trim(RxDetails) <> "") Then
            fLoadRx = True
        End If
        Dim pat As New Patient
        frmRxDetails.lblPatient = pat.GetPatientInfo(PatientId)
        Set pat = Nothing
        frmRxDetails.AppointmentId = AppointmentId
        
        'REFACTOR: This is supposed to be a private object.  Change this to use a local CDrugRx and copy it when finished.
        Set frmRxDetails.moDrugRx = New CDrugRx
        frmRxDetails.moDrugRx.Id = Val(Trim(SelectedRx(Item).Ref))
        If Not RxDetails = "" Then
            Call frmRxDetails.moDrugRx.LoadRxFromDB(RxDetails)
        End If
        If (frmRxDetails.LoadRxDetails(False, LastOn, fLoadRx)) Then
            Set frmRxDetails.DIList = Nothing
            If Not frmRxManage_Write_RX Then
                frmRxDetails.lblDosage.Caption = ""
            End If
            frmRxDetails.EditPreviousOn = EditPreviousOn
            frmRxDetails.Show 1
            SelectedRx(Item).DrugRxRecordFormat = frmRxDetails.moDrugRx.GetDrugRxFormat("record2")
            SelectedRx(Item).Notes = Trim(frmRxDetails.moDrugRx.DoctorNotes)
        End If
    End If
'Set oDrugRx = Nothing
End Sub

Private Sub ClearButtons()
Dim i As Long
    For i = 0 To MAX_BUTTONS - 1
        cmdRx(i).Visible = False
        cmdRx(i).BackColor = BaseBackColor
        cmdRx(i).ForeColor = BaseTextColor
    Next i
End Sub

Private Sub ClearRxs()
cmdRx1.Visible = False
cmdRx1.BackColor = BaseBackColor
cmdRx1.ForeColor = BaseTextColor
cmdRx2.Visible = False
cmdRx2.BackColor = BaseBackColor
cmdRx2.ForeColor = BaseTextColor
cmdRx3.Visible = False
cmdRx3.BackColor = BaseBackColor
cmdRx3.ForeColor = BaseTextColor
cmdRx4.Visible = False
cmdRx4.BackColor = BaseBackColor
cmdRx4.ForeColor = BaseTextColor
cmdRx5.Visible = False
cmdRx5.BackColor = BaseBackColor
cmdRx5.ForeColor = BaseTextColor
cmdRx6.Visible = False
cmdRx6.BackColor = BaseBackColor
cmdRx6.ForeColor = BaseTextColor
cmdRx7.Visible = False
cmdRx7.BackColor = BaseBackColor
cmdRx7.ForeColor = BaseTextColor
cmdRx8.Visible = False
cmdRx8.BackColor = BaseBackColor
cmdRx8.ForeColor = BaseTextColor
cmdRx9.Visible = False
cmdRx9.BackColor = BaseBackColor
cmdRx9.ForeColor = BaseTextColor
cmdRx10.Visible = False
cmdRx10.BackColor = BaseBackColor
cmdRx10.ForeColor = BaseTextColor
cmdRx11.Visible = False
cmdRx11.BackColor = BaseBackColor
cmdRx11.ForeColor = BaseTextColor
cmdRx12.Visible = False
cmdRx12.BackColor = BaseBackColor
cmdRx12.ForeColor = BaseTextColor
cmdRx13.Visible = False
cmdRx13.BackColor = BaseBackColor
cmdRx13.ForeColor = BaseTextColor
cmdRx14.Visible = False
cmdRx14.BackColor = BaseBackColor
cmdRx14.ForeColor = BaseTextColor
cmdRx15.Visible = False
cmdRx15.BackColor = BaseBackColor
cmdRx15.ForeColor = BaseTextColor
End Sub


Private Sub cmdSelectAll_Click()
frmAlphaPad.NumPad_Field = "Name of Drug"
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.Show 1
If (Trim(frmAlphaPad.NumPad_Result) <> "") Then
    Dim clsDrug As New CDrug
    clsDrug.DisplayName = Trim(frmAlphaPad.NumPad_Result)
    Set RSAll = clsDrug.GetDrugListAll
    Set clsDrug = Nothing
    StartNum = 0
    Call LoadRxAll
End If
End Sub

Public Function LoadRxAll() As Boolean
Dim i As Integer

If RSAll.RecordCount > 0 Then
    RSAll.MoveFirst
    RSAll.Move StartNum
End If

Dim cmdRxN(15) As fpBtn
Dim cntl As Control
Dim tst As String
For Each cntl In Me
    tst = cntl.Name
    Select Case Len(tst)
    Case 6, 7
        If Mid(tst, 1, 5) = "cmdRx" Then
            tst = Mid(tst, 6)
            If IsNumeric(tst) Then
                Set cmdRxN(tst) = cntl
            End If
        End If
    End Select
Next

ClearButtons
For i = 1 To 15
    cmdRxN(i).Visible = Not RSAll.EOF
    cmdRxN(i).Tag = ""
    cmdRxN(i).ToolTipText = ""
    cmdRxN(i).Text = ""
    If RSAll.EOF Then
    Else
        cmdRxN(i).Text = RSAll("DisplayName")
        cmdRxN(i).FontUnderline = Not (RSAll("Drugcode") = "T")
        RSAll.MoveNext
        cmdRxN(i).FontName = "Arial"
        cmdRxN(i).FontSize = 11
        cmdRxN(i).BackColor = BaseBackColor
    End If
Next
Dim cmdV(1) As Boolean
If (RSAll.RecordCount > 14) Then
    cmdV(0) = Not RSAll.EOF
    If StartNum > 0 Then cmdV(1) = True
End If
cmdMoreRx.Text = "MORE....."
cmdPrev.Text = "Previous"
cmdMoreRx.Visible = cmdV(0)
cmdPrev.Visible = cmdV(1)
lblType.Caption = "All Drugs"
End Function

Public Function LoadAvailableRx(Init As Boolean) As Boolean
On Error GoTo UI_ErrorHandler
Dim RetDiag As DiagnosisMasterPrimary
Set RetDiag = New DiagnosisMasterPrimary
LoadAvailableRx = False
'ButtonSelectionColor = 14745312
'TextSelectionColor = 0
ButtonSelectionColor = cmdRx1.BackColor
TextSelectionColor = cmdRx1.ForeColor
'BaseBackColor = cmdRx1.BackColor
BaseBackColor = &HC19B49
BaseTextColor = cmdRx1.ForeColor
If (Init) Then
    PageCnt = 1
    MaxRx = 0
    MaxFavoriteFamily = 0
    MaxFavoriteMeds = 0
    CurrentRxIndex = 1
    DrugName = ""
    DrugFamily = ""
    Call InitializeRx
    If frmRxManage_Write_RX Then
        FavoriteFamilyOn = False
        FavoriteMedsOn = True
    Else
        FavoriteFamilyOn = True
        FavoriteMedsOn = False
    End If
    cmdFavRx.Visible = FavoriteOn
End If
RetDiag.PrimarySystem = ""
RetDiag.PrimaryRank = 0
RetDiag.PrimaryLevel = 0
RetDiag.PrimaryDiagnosis = ""
RetDiag.PrimaryNextLevelDiagnosis = Diagnosis
If (RetDiag.FindPrimary > 0) Then
    If (RetDiag.SelectPrimary(1)) Then
        If (Trim(RetDiag.PrimaryLingo) <> "") Then
            Label1.Caption = "Assign An Rx for " + Trim(RetDiag.PrimaryLingo)
        Else
            Label1.Caption = "Assign An Rx for " + Trim(RetDiag.PrimaryName)
        End If
    End If
End If
If Not (FavoriteFamilyOn) And Not (FavoriteMedsOn) Then
    LoadAvailableRx = LoadFm
ElseIf (FavoriteFamilyOn) Then
    LoadAvailableRx = LoadFmFavoriteFamily
ElseIf (FavoriteMedsOn) Then
    LoadAvailableRx = LoadFavoriteMeds
End If
Set RetDiag = Nothing
Exit Function
UI_ErrorHandler:
    LoadAvailableRx = False
    Call PinpointError("AssignDrugs", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume LeaveFast
LeaveFast:
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    NameOnly = False
    EditPreviousOn = False
End Sub

'Private Sub cmdSelectAll_Click()
'frmAlphaPad.NumPad_Field = "Name of Drug"
'frmAlphaPad.NumPad_DisplayFull = True
'frmAlphaPad.Show 1
'If (Trim(frmAlphaPad.NumPad_Result) <> "") Then
'    Dim clsDrug As New CDrug
'    clsDrug.DisplayName = Trim(frmAlphaPad.NumPad_Result)
'    Set RSAll = clsDrug.GetDrugListAll
'    Set clsDrug = Nothing
'    StartNum = 0
'    Call LoadRxAll
'End If
'End Sub

'Public Function LoadRxAll() As Boolean
'Dim i As Integer
'
'If RSAll.RecordCount > 0 Then
'    RSAll.MoveFirst
'    RSAll.Move StartNum
'End If
'
'Dim cmdRxN(15) As fpBtn
'Dim cntl As Control
'Dim tst As String
'For Each cntl In Me
'    tst = cntl.Name
'    Select Case Len(tst)
'    Case 6, 7
'        If Mid(tst, 1, 5) = "cmdRx" Then
'            tst = Mid(tst, 6)
'            If IsNumeric(tst) Then
'                Set cmdRxN(tst) = cntl
'            End If
'        End If
'    End Select
'Next
'
'ClearButtons
'For i = 1 To 15
'    cmdRxN(i).Visible = Not RSAll.EOF
'    cmdRxN(i).Tag = ""
'    cmdRxN(i).ToolTipText = ""
'    cmdRxN(i).Text = ""
'    If RSAll.EOF Then
'    Else
'        cmdRxN(i).Text = RSAll("DisplayName")
'        cmdRxN(i).FontUnderline = Not (RSAll("Drugcode") = "T")
'        RSAll.MoveNext
'        cmdRxN(i).FontName = "Arial"
'        cmdRxN(i).FontSize = 11
'    End If
'Next
'Dim cmdV(1) As Boolean
'If (RSAll.RecordCount > 14) Then
'    cmdV(0) = Not RSAll.EOF
'    If StartNum > 0 Then cmdV(1) = True
'End If
'cmdMoreRx.Text = "MORE....."
'cmdPrev.Text = "Previous"
'cmdMoreRx.Visible = cmdV(0)
'cmdPrev.Visible = cmdV(1)
'lblType.Caption = "All Drugs"
'End Function
'
'Public Function LoadAvailableRx(Init As Boolean) As Boolean
'On Error GoTo UI_ErrorHandler
'Dim RetDiag As DiagnosisMasterPrimary
'Set RetDiag = New DiagnosisMasterPrimary
'LoadAvailableRx = False
''ButtonSelectionColor = 14745312
''TextSelectionColor = 0
'ButtonSelectionColor = cmdRx1.BackColor
'TextSelectionColor = cmdRx1.ForeColor
'BaseBackColor = cmdRx1.BackColor
'BaseTextColor = cmdRx1.ForeColor
'If (Init) Then
'    PageCnt = 1
'    MaxRx = 0
'    MaxFavoriteFamily = 0
'    MaxFavoriteMeds = 0
'    CurrentRxIndex = 1
'    DrugName = ""
'    DrugFamily = ""
'    Call InitializeRx
'    If frmRxManage_Write_RX Then
'        FavoriteFamilyOn = False
'        FavoriteMedsOn = True
'    Else
'        FavoriteFamilyOn = True
'        FavoriteMedsOn = False
'    End If
'    cmdFavRx.Visible = FavoriteOn
'End If
'RetDiag.PrimarySystem = ""
'RetDiag.PrimaryRank = 0
'RetDiag.PrimaryLevel = 0
'RetDiag.PrimaryDiagnosis = ""
'RetDiag.PrimaryNextLevelDiagnosis = Diagnosis
'If (RetDiag.FindPrimary > 0) Then
'    If (RetDiag.SelectPrimary(1)) Then
'        If (Trim(RetDiag.PrimaryLingo) <> "") Then
'            Label1.Caption = "Assign An Rx for " + Trim(RetDiag.PrimaryLingo)
'        Else
'            Label1.Caption = "Assign An Rx for " + Trim(RetDiag.PrimaryName)
'        End If
'    End If
'End If
'If Not (FavoriteFamilyOn) And Not (FavoriteMedsOn) Then
'    LoadAvailableRx = LoadFm
'ElseIf (FavoriteFamilyOn) Then
'    LoadAvailableRx = LoadFmFavoriteFamily
'ElseIf (FavoriteMedsOn) Then
'    LoadAvailableRx = LoadFavoriteMeds
'End If
'Set RetDiag = Nothing
'Exit Function
'UI_ErrorHandler:
'    LoadAvailableRx = False
'    Call PinpointError("AssignDrugs", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
'    Resume LeaveFast
'LeaveFast:
'End Function
'
'Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'    NameOnly = False
'    EditPreviousOn = False
'End Sub
'Public Function FrmClose()
' Call cmdDone_Click
' Unload Me
'End Function
