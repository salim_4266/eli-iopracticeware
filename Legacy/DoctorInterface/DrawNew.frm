VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{4BD5A3A1-7FFE-11D4-A13A-004005FA6275}#1.0#0"; "ImagXpr6.dll"
Object = "{918E6E43-F23A-11D0-901E-0020AF7543C2}#5.0#0"; "fximg50g.ocx"
Begin VB.Form frmDrawNew 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9450
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12570
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "DrawNew.frx":0000
   ScaleHeight     =   9450
   ScaleWidth      =   12570
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   1
      Left            =   2935
      TabIndex        =   163
      Top             =   6840
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":36C9
   End
   Begin IMAGXPR6LibCtl.ImagXpress FXImage2 
      Height          =   855
      Left            =   600
      TabIndex        =   106
      Top             =   5880
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1508
      ErrStr          =   "MDYC0090GEP-0B3060SXEP"
      ErrCode         =   21593856
      ErrInfo         =   -534101095
      Persistence     =   -1  'True
      _cx             =   1
      _cy             =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   -2147483633
      ScrollBarLargeChangeH=   10
      ScrollBarSmallChangeH=   1
      OLEDropMode     =   0
      ScrollBarLargeChangeV=   10
      ScrollBarSmallChangeV=   1
      DisplayProgressive=   -1  'True
      SaveTIFByteOrder=   0
      LoadRotated     =   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
   End
   Begin FXIMG50GLib.FXImage imgImage1 
      Height          =   375
      Left            =   2640
      TabIndex        =   0
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage2 
      Height          =   375
      Left            =   3240
      TabIndex        =   1
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage3 
      Height          =   375
      Left            =   3840
      TabIndex        =   2
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage4 
      Height          =   375
      Left            =   4440
      TabIndex        =   3
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage5 
      Height          =   375
      Left            =   5040
      TabIndex        =   4
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage6 
      Height          =   375
      Left            =   5640
      TabIndex        =   5
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage7 
      Height          =   375
      Left            =   6240
      TabIndex        =   6
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage8 
      Height          =   375
      Left            =   6840
      TabIndex        =   7
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage9 
      Height          =   375
      Left            =   7440
      TabIndex        =   8
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage FXImage1 
      Height          =   5265
      Left            =   2400
      TabIndex        =   9
      Top             =   480
      Visible         =   0   'False
      Width           =   6915
      _Version        =   327680
      _ExtentX        =   12197
      _ExtentY        =   9287
      _StockProps     =   65
      BackColor       =   11098385
      Persistence     =   -1  'True
      _StdProps       =   11915965
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
      AutoSize        =   4
      Notify          =   -1  'True
      BackColor       =   11098385
      Begin VB.Label lblRSen15 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label15"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5040
         TabIndex        =   158
         Top             =   2880
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblRSen14 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label14"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5040
         TabIndex        =   157
         Top             =   2600
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblRSen13 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label13"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5040
         TabIndex        =   156
         Top             =   2190
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblRSen12 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label12"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5040
         TabIndex        =   155
         Top             =   1850
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblRSen11 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label11"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5760
         TabIndex        =   154
         Top             =   2190
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblRSen10 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label10"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5760
         TabIndex        =   153
         Top             =   1850
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblRSen9 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label9"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4200
         TabIndex        =   152
         Top             =   2190
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRSen8 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label8"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4200
         TabIndex        =   151
         Top             =   1850
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRSen7 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5040
         TabIndex        =   150
         Top             =   1500
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRSen6 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5040
         TabIndex        =   149
         Top             =   1180
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRSen5 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4920
         TabIndex        =   148
         Top             =   720
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRSen4 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   6120
         TabIndex        =   147
         Top             =   840
         Width           =   570
      End
      Begin VB.Label lblRSen3 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   6120
         TabIndex        =   146
         Top             =   580
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRSen2 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   3960
         TabIndex        =   145
         Top             =   840
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRSen1 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   3960
         TabIndex        =   144
         Top             =   580
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblBM16 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label16"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2880
         TabIndex        =   143
         Top             =   2520
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblBM15 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label15"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2880
         TabIndex        =   142
         Top             =   2160
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblBM14 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label14"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1680
         TabIndex        =   141
         Top             =   2520
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblBM13 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label13"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1680
         TabIndex        =   140
         Top             =   2160
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblBM12 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label12"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   600
         TabIndex        =   139
         Top             =   2520
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblBM11 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label11"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   600
         TabIndex        =   138
         Top             =   2160
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblBM10 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label10"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2880
         TabIndex        =   137
         Top             =   1680
         Visible         =   0   'False
         Width           =   675
      End
      Begin VB.Label lblBM9 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label9"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2880
         TabIndex        =   136
         Top             =   1320
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblBM8 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label8"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1680
         TabIndex        =   135
         Top             =   1680
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblBM7 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1680
         TabIndex        =   134
         Top             =   1320
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblBM6 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   600
         TabIndex        =   133
         Top             =   1680
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblBM5 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   600
         TabIndex        =   132
         Top             =   1320
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblBM4 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1680
         TabIndex        =   131
         Top             =   960
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblBM3 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1680
         TabIndex        =   130
         Top             =   600
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblBM2 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   600
         TabIndex        =   129
         Top             =   960
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblBM1 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   600
         TabIndex        =   128
         Top             =   600
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblSterioD 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4800
         TabIndex        =   127
         Top             =   4390
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblSterioN 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4800
         TabIndex        =   126
         Top             =   4630
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblWD4D 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4800
         TabIndex        =   125
         Top             =   3530
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblWD4N 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4800
         TabIndex        =   124
         Top             =   3770
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblLVers6 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   3120
         TabIndex        =   123
         Top             =   4800
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblLVers5 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   3120
         TabIndex        =   122
         Top             =   4320
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblLVers4 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   3120
         TabIndex        =   121
         Top             =   3480
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblLVers3 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2040
         TabIndex        =   120
         Top             =   4800
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblLVers2 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2040
         TabIndex        =   119
         Top             =   4320
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblLVers1 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2040
         TabIndex        =   118
         Top             =   3480
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRVers6 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1320
         TabIndex        =   117
         Top             =   4800
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRVers5 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1440
         TabIndex        =   116
         Top             =   4080
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRVers4 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   1320
         TabIndex        =   115
         Top             =   3480
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRVers3 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   360
         TabIndex        =   114
         Top             =   4800
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRVers2 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   360
         TabIndex        =   113
         Top             =   4080
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblRVers1 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   360
         TabIndex        =   112
         Top             =   3480
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Line LineA18 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   2640
         Y2              =   2640
      End
      Begin VB.Line LineA17 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   2520
         Y2              =   2520
      End
      Begin VB.Line LineA16 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   2400
         Y2              =   2400
      End
      Begin VB.Line LineA15 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Line LineA14 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   2160
         Y2              =   2160
      End
      Begin VB.Line LineA13 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   2040
         Y2              =   2040
      End
      Begin VB.Line LineA12 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   1920
         Y2              =   1920
      End
      Begin VB.Line LineA11 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   1800
         Y2              =   1800
      End
      Begin VB.Line LineA10 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   1680
         Y2              =   1680
      End
      Begin VB.Line Line18 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Line Line17 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   1200
         Y2              =   1200
      End
      Begin VB.Line Line16 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Line Line15 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Line Line14 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   840
         Y2              =   840
      End
      Begin VB.Line Line13 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Line Line12 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line11 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   480
         Y2              =   480
      End
      Begin VB.Line Line10 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         X1              =   3600
         X2              =   5040
         Y1              =   360
         Y2              =   360
      End
      Begin VB.Line Line1 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   360
         Y2              =   360
      End
      Begin VB.Line Line2 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   480
         Y2              =   480
      End
      Begin VB.Line Line3 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line4 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Line Line5 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   840
         Y2              =   840
      End
      Begin VB.Line Line7 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Line Line8 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   1200
         Y2              =   1200
      End
      Begin VB.Line Line6 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Line Line9 
         BorderColor     =   &H000000FF&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   1320
         Y2              =   1320
      End
      Begin VB.Label lblRTest2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   2040
         TabIndex        =   19
         Top             =   3600
         Visible         =   0   'False
         Width           =   900
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblRTest1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   1440
         TabIndex        =   18
         Top             =   4200
         Visible         =   0   'False
         Width           =   900
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblRTest3 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   2640
         TabIndex        =   17
         Top             =   4200
         Visible         =   0   'False
         Width           =   900
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblRTest4 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   2040
         TabIndex        =   16
         Top             =   4920
         Visible         =   0   'False
         Width           =   900
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblLTest2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   4080
         TabIndex        =   15
         Top             =   3600
         Visible         =   0   'False
         Width           =   900
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblLTest1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   4635
         TabIndex        =   14
         Top             =   4200
         Visible         =   0   'False
         Width           =   915
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblLTest4 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   4080
         TabIndex        =   13
         Top             =   4920
         Visible         =   0   'False
         Width           =   900
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblLTest3 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   3480
         TabIndex        =   12
         Top             =   4200
         Visible         =   0   'False
         Width           =   900
         WordWrap        =   -1  'True
      End
      Begin VB.Line LineA1 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   1680
         Y2              =   1680
      End
      Begin VB.Line LineA2 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   1800
         Y2              =   1800
      End
      Begin VB.Line LineA3 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   1920
         Y2              =   1920
      End
      Begin VB.Line LineA4 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   2040
         Y2              =   2040
      End
      Begin VB.Line LineA5 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   2160
         Y2              =   2160
      End
      Begin VB.Line LineA6 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Line LineA7 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   2400
         Y2              =   2400
      End
      Begin VB.Line LineA8 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   2520
         Y2              =   2520
      End
      Begin VB.Line LineA9 
         BorderColor     =   &H0000C0C0&
         BorderWidth     =   4
         Visible         =   0   'False
         X1              =   360
         X2              =   3360
         Y1              =   2640
         Y2              =   2640
      End
      Begin VB.Label lblLTestA1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         Height          =   195
         Left            =   5850
         TabIndex        =   11
         Top             =   4920
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label lblRTestA1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         Height          =   195
         Left            =   690
         TabIndex        =   10
         Top             =   4920
         Visible         =   0   'False
         Width           =   480
      End
   End
   Begin FXIMG50GLib.FXImage imgImageA1 
      Height          =   375
      Left            =   2760
      TabIndex        =   20
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA2 
      Height          =   375
      Left            =   3360
      TabIndex        =   21
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA3 
      Height          =   375
      Left            =   3960
      TabIndex        =   22
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA4 
      Height          =   375
      Left            =   4560
      TabIndex        =   23
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA5 
      Height          =   375
      Left            =   5160
      TabIndex        =   24
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA6 
      Height          =   375
      Left            =   5760
      TabIndex        =   25
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA7 
      Height          =   375
      Left            =   6360
      TabIndex        =   26
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA8 
      Height          =   375
      Left            =   6960
      TabIndex        =   27
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA9 
      Height          =   375
      Left            =   7560
      TabIndex        =   28
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage10 
      Height          =   375
      Left            =   2640
      TabIndex        =   71
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage11 
      Height          =   375
      Left            =   3240
      TabIndex        =   72
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage12 
      Height          =   375
      Left            =   3840
      TabIndex        =   73
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage13 
      Height          =   375
      Left            =   4440
      TabIndex        =   74
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage14 
      Height          =   375
      Left            =   5040
      TabIndex        =   75
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage15 
      Height          =   375
      Left            =   5640
      TabIndex        =   76
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage16 
      Height          =   375
      Left            =   6240
      TabIndex        =   77
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage17 
      Height          =   375
      Left            =   6840
      TabIndex        =   78
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImage18 
      Height          =   375
      Left            =   7440
      TabIndex        =   79
      Top             =   240
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA10 
      Height          =   375
      Left            =   2760
      TabIndex        =   80
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA11 
      Height          =   375
      Left            =   3360
      TabIndex        =   81
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA12 
      Height          =   375
      Left            =   3960
      TabIndex        =   82
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA13 
      Height          =   375
      Left            =   4560
      TabIndex        =   83
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA14 
      Height          =   375
      Left            =   5160
      TabIndex        =   84
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA15 
      Height          =   375
      Left            =   5760
      TabIndex        =   85
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA16 
      Height          =   375
      Left            =   6360
      TabIndex        =   86
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA17 
      Height          =   375
      Left            =   6960
      TabIndex        =   87
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin FXIMG50GLib.FXImage imgImageA18 
      Height          =   375
      Left            =   7560
      TabIndex        =   88
      Top             =   5520
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   873
      _ExtentY        =   661
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   984239
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   0
      Left            =   1285
      TabIndex        =   90
      Top             =   6840
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15329769
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":48E2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1080
      Left            =   120
      TabIndex        =   91
      Top             =   7920
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":5AF7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1080
      Left            =   10715
      TabIndex        =   92
      Top             =   7920
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":5CD2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreBrushs 
      Height          =   1080
      Left            =   9555
      TabIndex        =   93
      Top             =   7920
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":5EAD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   1080
      Left            =   120
      TabIndex        =   94
      Top             =   6840
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":6091
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther 
      Height          =   960
      Left            =   8520
      TabIndex        =   95
      Top             =   5880
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":626D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImageDraw 
      Height          =   1080
      Left            =   10715
      TabIndex        =   96
      Top             =   6840
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":6451
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPalette 
      Height          =   1080
      Left            =   9555
      TabIndex        =   97
      Top             =   6840
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   8951812
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":6636
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrevOther 
      Height          =   960
      Left            =   10920
      TabIndex        =   98
      Top             =   480
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":6814
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNextOther 
      Height          =   960
      Left            =   10920
      TabIndex        =   99
      Top             =   4680
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":69F4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOImage 
      Height          =   960
      Left            =   120
      TabIndex        =   100
      Top             =   5880
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":6BD4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   960
      Left            =   9555
      TabIndex        =   101
      Top             =   5880
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":6DB5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRotate 
      Height          =   960
      Left            =   6450
      TabIndex        =   102
      Top             =   5880
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":6FA0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdScaleUp 
      Height          =   960
      Left            =   6450
      TabIndex        =   103
      Top             =   5880
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":717D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdScaleDown 
      Height          =   960
      Left            =   6480
      TabIndex        =   104
      Top             =   5880
      Width           =   915
      _Version        =   131072
      _ExtentX        =   1614
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":735C
   End
   Begin FXIMG50GLib.FXImage imgStandard 
      Height          =   500
      Left            =   840
      TabIndex        =   105
      Top             =   5160
      Visible         =   0   'False
      Width           =   500
      _Version        =   327680
      _ExtentX        =   882
      _ExtentY        =   882
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   639375
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
      AutoSize        =   2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdJump 
      Height          =   960
      Left            =   7480
      TabIndex        =   107
      Top             =   5880
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":7541
   End
   Begin FXIMG50GLib.FXImage ImgOrgBase 
      Height          =   495
      Left            =   240
      TabIndex        =   108
      Top             =   5160
      Visible         =   0   'False
      Width           =   495
      _Version        =   327680
      _ExtentX        =   882
      _ExtentY        =   882
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   639375
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   2130706761
      ErrInfo         =   -637278289
      AutoSize        =   2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFreeErase 
      Height          =   960
      Left            =   1200
      TabIndex        =   110
      Top             =   5880
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":7727
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdKill 
      Height          =   960
      Left            =   10715
      TabIndex        =   111
      Top             =   5880
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   1
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":7904
   End
   Begin VB.ListBox lstOthers 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4650
      Left            =   120
      TabIndex        =   29
      Top             =   600
      Visible         =   0   'False
      Width           =   10575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuant 
      Height          =   960
      Left            =   5340
      TabIndex        =   159
      Top             =   5880
      Width           =   1110
      _Version        =   131072
      _ExtentX        =   1958
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":7AE5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRO 
      Height          =   960
      Left            =   2240
      TabIndex        =   160
      Top             =   5880
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":7CC6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCNF 
      Height          =   960
      Left            =   4310
      TabIndex        =   161
      Top             =   5880
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":7EA9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHist 
      Height          =   960
      Left            =   3270
      TabIndex        =   162
      Top             =   5880
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1693
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":8083
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   2
      Left            =   4590
      TabIndex        =   164
      Top             =   6840
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":8265
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   3
      Left            =   6240
      TabIndex        =   165
      Top             =   6840
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":947E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   4
      Left            =   7895
      TabIndex        =   166
      Top             =   6840
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":A697
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   5
      Left            =   1285
      TabIndex        =   167
      Top             =   7920
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15329769
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":B8B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   6
      Left            =   2935
      TabIndex        =   168
      Top             =   7920
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15329769
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":CAC5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   7
      Left            =   4590
      TabIndex        =   169
      Top             =   7920
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":DCDA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   8
      Left            =   6240
      TabIndex        =   170
      Top             =   7920
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":EEF3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrush 
      Height          =   1080
      Index           =   9
      Left            =   7895
      TabIndex        =   171
      Top             =   7920
      Visible         =   0   'False
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   15724527
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrawNew.frx":1010C
   End
   Begin VB.Label lblErase 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Turn Erase Off"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   840
      TabIndex        =   109
      Top             =   360
      Visible         =   0   'False
      Width           =   1350
   End
   Begin VB.Label lblLocView 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Patient Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   8160
      TabIndex        =   89
      Top             =   240
      Visible         =   0   'False
      Width           =   1290
   End
   Begin VB.Label lblImage11 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   70
      Top             =   1080
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage12 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   69
      Top             =   1560
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage13 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   68
      Top             =   2040
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage14 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   67
      Top             =   2520
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage15 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   66
      Top             =   3000
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage16 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   65
      Top             =   3480
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage17 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   64
      Top             =   3960
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage18 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   63
      Top             =   4440
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage10 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   62
      Top             =   600
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA10 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   61
      Top             =   840
      Visible         =   0   'False
      Width           =   2325
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA11 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   60
      Top             =   1320
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA12 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   59
      Top             =   1800
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA13 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   58
      Top             =   2280
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA14 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   57
      Top             =   2760
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA15 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   56
      Top             =   3240
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA16 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   55
      Top             =   3720
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA17 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   54
      Top             =   4200
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA18 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   53
      Top             =   4680
      Visible         =   0   'False
      Width           =   2250
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblDiagnosis 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Diagnosis"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   4080
      TabIndex        =   52
      Top             =   120
      Width           =   915
   End
   Begin VB.Label lblEye 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "OD"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   240
      TabIndex        =   51
      Top             =   120
      Width           =   285
   End
   Begin VB.Label lblImage2 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   50
      Top             =   1080
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage3 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   49
      Top             =   1560
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage4 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   48
      Top             =   2040
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage5 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   47
      Top             =   2520
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage6 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   46
      Top             =   3000
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage7 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   45
      Top             =   3480
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage8 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   44
      Top             =   3960
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPrev 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Previous Draw On"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9480
      TabIndex        =   43
      Top             =   5640
      Visible         =   0   'False
      Width           =   1710
   End
   Begin VB.Label lblImage9 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   42
      Top             =   4440
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImage1 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   41
      Top             =   600
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPat 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Patient Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   840
      TabIndex        =   40
      Top             =   120
      Width           =   1290
   End
   Begin VB.Label lblLoc 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Patient Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   8160
      TabIndex        =   39
      Top             =   0
      Visible         =   0   'False
      Width           =   1290
   End
   Begin VB.Label lblImageA1 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   38
      Top             =   840
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA2 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   37
      Top             =   1320
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA3 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   36
      Top             =   1800
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA4 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   35
      Top             =   2280
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA5 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   34
      Top             =   2760
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA6 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   33
      Top             =   3240
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA7 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   32
      Top             =   3720
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA8 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   9360
      TabIndex        =   31
      Top             =   4200
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblImageA9 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9360
      TabIndex        =   30
      Top             =   4680
      Visible         =   0   'False
      Width           =   2520
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmDrawNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public InitOn As Boolean
Public JumpOn As Boolean
Public HomeOn As Boolean
Public NoteOn As Boolean
Public FormOn As Boolean
Public FirstTimeForImage As Boolean
Public PatientId As Long
Public AppointmentId As Long
Public StoreResults As String
Public SystemReference As String
Public Diagnosis As String
Public DiagnosisROOn As Boolean
Public DiagnosisHOOn As Boolean
Public DiagnosisNFOn As Boolean
Public DiagnosisQuant As String
Public DiagnosisId As Long
Public EyeContext As String
Public DrawFileName As String
Public ReplaceBase As String
Public KillOn As Boolean

'Supports launching menus for findings clicked in frmSystems1
Public sClickedDiag As String
Private iClickedDiagPos As Integer

Private Const NotBillTag As String = "(NB)"
Private Const UnspecTag As String = "(NS)"

Private Const LocRef As String = "{"
Private Const LocRefEnd As String = "}"
Private Const SelectionColor As Long = &HC0C000
Private Const StandardImageWidth As Integer = 500
Private Const StandardImageHeight As Integer = 500
Private Const MaxMagnificationFactor As Integer = 4
Private Const MaxBrushVisible As Long = 9
Private Const MaxImagesAllowed As Long = 18
Private Const MaxBrushsAllowed As Long = 100
Private Const TotalBrushButtons As Long = 10

Private Type BrushDetails
    BrushArrayIndex As Integer
    BrushType As String * 1
    DiagnosisId As Long
    BrushName As String
    AssociatedId As Long
    BrushLingo As String
    RotateOn As Boolean
    SizingOn As Boolean
    ScalingOn As Boolean
    LocateXOn As Single
    LocateYOn As Single
    FrameWidth As Integer
    FrameHeight As Integer
    LocalSide As String * 1
    BrushImageType As String * 1
    DefaultColor As String * 1
    DefaultWidth As String * 1
    Quantifier As String * 32
    TestTrigger As String * 4
    TestTriggerText As String
    LocationSupport As String * 1
End Type

Private Type ImageDetails
    ButtonNumber As Integer
    BrushId As Integer
    Burn As Boolean
    IsImageSet As Boolean
    DroppedImage As Boolean
    ImageName As Integer
    Name As String * 32
    StatusPost As Boolean
    HistoryOf As Boolean
    RuleOut As Boolean
    CNF As Boolean
    Location As String * 4
    LocalSide As String * 1
    Eye As String * 2
    Size As Integer
    Top As Single
    Left As Single
    OriginalTop As Single
    OriginalLeft As Single
    Rotate As Integer
    LabelId As Integer
    LabelName As String * 32
    LabelTop As Integer
    LabelLeft As Integer
    LineId As Integer
    LineName As String * 32
    BrushReference As BrushDetails
End Type

Private LocNames(2, 37) As String
Private OriginalBaseFileName As String
Private ScalingDirection As Integer
Private ReloadReplaceScanOn As Boolean
Private OtherDiags As String
Private CurrentOtherIndex As Integer
Private TotalOtherDiags As Integer
Private LocPropOn As Boolean
Private OtherImagesOn As Boolean
Private PlacementInProgress As Boolean

' For MyDraw functionality
Private EraseWaitForClick As Boolean
Private EraseOn As Boolean
Private DrawingOn As Boolean
Private DropLineOn As Boolean
Private DropOnImage As Boolean
Private PrevX As Single
Private PrevY As Single
Private SelectedX As Single
Private SelectedY As Single

Private OPic As IPictureDisp
Private ControlClear As Boolean
Private ReloadImages As Boolean
Private MagnifyDontShift As Boolean
Private ScaleUpOn As Boolean
Private ScaleDownOn As Boolean

Private StartLinesOn As Boolean
Private AllowIt As Boolean
Private OriginalColor As Long
Private XTwips As Integer
Private YTwips As Integer
Private LocalBrushIndex As Integer
Private CurrentBrushIndex As Integer
Private MaxBrushsAvailable As Integer

Private ImageRef As String
Private DiagnosisName As String
Private TheImage(MaxImagesAllowed) As ImageDetails
Private TheButtons(MaxBrushsAllowed) As BrushDetails
Private GlobalTop As Single
Private GlobalLeft As Single
Private GlobalWidth As Single
Private GlobalHeight As Single

Private PencilWidth As Long
Private PencilColor As Long
Private DrawExt As String
Private BaseExt As String
Private BaseImage As String
Private BaseImageRef As String

Private PrevImageId As Integer
Private CurrentImageId As Integer
Private CurrentEraseImageId As Integer
Private CurrentBrushSelection As Integer
Private CurrentEndXLocation As Integer
Private CurrentEndYLocation As Integer
Private TheBrush As DiagnosisMasterBrush

Private Sub cmdCNF_Click()
Dim Temp As String
    On Error GoTo lcmdCNF_Click_Error

If (CurrentImageId > 0) Then
    TheImage(CurrentImageId).CNF = Not (TheImage(CurrentImageId).CNF)
    Temp = TheImage(CurrentImageId).BrushReference.BrushLingo
    Call SetLabelCaption(CurrentImageId)
End If

    Exit Sub

lcmdCNF_Click_Error:

    LogError "frmDrawNew", "cmdCNF_Click", Err, Err.Description
End Sub

Private Sub cmdFreeErase_Click()
    On Error GoTo lcmdFreeErase_Click_Error

EraseOn = Not (EraseOn)
lblErase.Visible = EraseOn
If (EraseOn) Then
    CurrentEraseImageId = CurrentImageId
    CurrentImageId = 0
    Call LoadBackRoundBaseImage
    EraseOn = True
Else
    CurrentImageId = CurrentEraseImageId
    CurrentEraseImageId = 0
    FxImage1.MousePointer = MP_Cross
    FxImage1.CaptureNow
    Call SaveNewDrawFileName(DrawFileName, False)
End If

    Exit Sub

lcmdFreeErase_Click_Error:

    LogError "frmDrawNew", "cmdFreeErase_Click", Err, Err.Description
End Sub

Private Sub cmdHist_Click()
Dim Idx As Integer
Dim Temp As String
    On Error GoTo lcmdHist_Click_Error

If (CurrentImageId > 0) Then
    Idx = CurrentImageId
    TheImage(Idx).HistoryOf = Not (TheImage(Idx).HistoryOf)
    Temp = TheImage(Idx).BrushReference.BrushLingo
    Call SetLabelCaption(Idx)
End If

    Exit Sub

lcmdHist_Click_Error:

    LogError "frmDrawNew", "cmdHist_Click", Err, Err.Description
End Sub

Private Sub cmdJump_Click()
    On Error GoTo lcmdJump_Click_Error

If (Trim(StoreResults) <> "") Then
    If (CurrentImageId > 0) Then
        Call BurnTheImage
    End If
    Call BuildStoreFile
    Call UpdatePrimaryImage
    Set TheBrush = Nothing
    If Not (ReloadReplaceScanOn) Then
        Unload frmDrawNew
        FormOn = False
        JumpOn = True
    End If
End If

    Exit Sub

lcmdJump_Click_Error:

    LogError "frmDrawNew", "cmdJump_Click", Err, Err.Description
End Sub

Private Sub cmdKill_Click()
    On Error GoTo lcmdKill_Click_Error

Call cmdClear_Click
Call InitializeOtherAssociatedImages(BaseImage)
Call SetNewDrawFile(DrawFileName, True)
lblImageA1.Visible = False
lblImageA1.Caption = "Label1"
lblImageA1.Tag = ""
lblImageA2.Visible = False
lblImageA2.Caption = "Label1"
lblImageA2.Tag = ""
lblImageA3.Visible = False
lblImageA3.Caption = "Label1"
lblImageA3.Tag = ""
lblImageA4.Visible = False
lblImageA4.Caption = "Label1"
lblImageA4.Tag = ""
lblImageA5.Visible = False
lblImageA5.Caption = "Label1"
lblImageA5.Tag = ""
lblImageA6.Visible = False
lblImageA6.Caption = "Label1"
lblImageA6.Tag = ""
lblImageA7.Visible = False
lblImageA7.Caption = "Label1"
lblImageA7.Tag = ""
lblImageA8.Visible = False
lblImageA8.Caption = "Label1"
lblImageA8.Tag = ""
lblImageA9.Visible = False
lblImageA9.Caption = "Label1"
lblImageA9.Tag = ""
lblImageA10.Visible = False
lblImageA10.Caption = "Label1"
lblImageA10.Tag = ""
lblImageA11.Visible = False
lblImageA11.Caption = "Label1"
lblImageA11.Tag = ""
lblImageA12.Visible = False
lblImageA12.Caption = "Label1"
lblImageA12.Tag = ""
lblImageA13.Visible = False
lblImageA13.Caption = "Label1"
lblImageA13.Tag = ""
lblImageA14.Visible = False
lblImageA14.Caption = "Label1"
lblImageA14.Tag = ""
lblImageA15.Visible = False
lblImageA15.Caption = "Label1"
lblImageA15.Tag = ""
lblImageA16.Visible = False
lblImageA16.Caption = "Label1"
lblImageA16.Tag = ""
lblImageA17.Visible = False
lblImageA17.Caption = "Label1"
lblImageA17.Tag = ""
lblImageA18.Visible = False
lblImageA18.Caption = "Label1"
lblImageA18.Tag = ""
KillOn = True

    Exit Sub

lcmdKill_Click_Error:

    LogError "frmDrawNew", "cmdKill_Click", Err, Err.Description
End Sub

Private Sub cmdOImage_Click()
Dim i As Integer
Dim iIndex As Long
Dim IFound As Boolean
Dim Temp As String
Dim SReplaceBase As String
    On Error GoTo lcmdOImage_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
frmEventMsgs.Header = "Other Draws"
frmEventMsgs.AcceptText = "Blank  Sheet"
frmEventMsgs.RejectText = "Replace Draw"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = "Current Draw"
frmEventMsgs.Other1Text = "Add     Draw"
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    If Not (InitOn) Then
        frmEventMsgs.Header = "You may create only one image for these findings"
        frmEventMsgs.AcceptText = "Retain Drawing"
        frmEventMsgs.RejectText = "Draw It Myself"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmDrawNew.ZOrder 0
        frmEventMsgs.Show 1
        frmDrawNew.ZOrder 0
        If (frmEventMsgs.Result = 2) Then
            If (FM.IsFileThere(DrawFileName)) Then
                FM.Kill DrawFileName
            End If
            DrawFileName = ""
        End If
    End If
    If (Trim(DrawFileName) = "") Or (Trim(DrawFileName) = BaseImage + BaseImageRef) Then
        Call SetNewDrawFileName(DrawFileName, False, IFound)
        Call ClearBrushes(0)
        Call SetNewDrawFile(DrawFileName, True)
    Else
        Call SetNewDrawFile(DrawFileName, False)
    End If
ElseIf (frmEventMsgs.Result = 2) Then
    frmScan.Whom = "D"
    frmScan.PatientId = PatientId
    frmScan.AppointmentId = AppointmentId
    frmScan.ReplacementFile = ""
    frmScan.SystemReference = SystemReference
    If (frmScan.LoadScan) Then
        frmScan.Show 1
        If (Trim(frmScan.ReplacementFile) <> "") Then
            frmEventMsgs.Header = "Apply the new base image ?"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = "No"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
' Auto-Kill Current
                Call ClearBrushes(0)
                Temp = frmScan.ReplacementFile
                i = InStrPS(Temp, ".")
                If (i > 0) Then
                    Temp = Mid(Temp, i, Len(Temp) - (i - 1))
                    ReplaceBase = LocalMyImageDir + "NewBase-D" + Trim(str(DiagnosisId)) + "-A" + Trim(str(AppointmentId)) + Temp
                    If (FM.IsFileThere(ReplaceBase)) Then
                        FM.Kill ReplaceBase
                    End If
                    FM.FileCopy Trim(frmScan.ReplacementFile), ReplaceBase
' Resize NewBase
                    Call ResizeScanImage(ReplaceBase)
                    SReplaceBase = MyImageDir + "NewBase-D" + Trim(str(DiagnosisId)) + "-A" + Trim(str(AppointmentId)) + Temp
                    Call PushDrawImageToServer(ReplaceBase, SReplaceBase)
                    Call PullDrawImageFromServer(SReplaceBase, ReplaceBase)
                    DrawFileName = ReplaceBase
                    ReloadReplaceScanOn = True
                    Set FxImage1.picture = Nothing
                    Call LoadDraw(False)
                    ReloadReplaceScanOn = True
                    Call ReloadForm(StoreResults)
                    Call BuildStoreFile
                    ReloadReplaceScanOn = False
                    LocPropOn = False
                End If
            End If
        End If
    End If
    frmScan.Whom = ""
    frmScan.ReplacementFile = ""
ElseIf (frmEventMsgs.Result = 3) Then
    For iIndex = 0 To TotalBrushButtons - 1
        cmdBrush(iIndex).Enabled = True
    Next iIndex
    cmdImageDraw.Enabled = True
    cmdScaleUp.Enabled = True
    cmdScaleDown.Enabled = True
    cmdRotate.Enabled = True
    cmdClear.Enabled = True
    cmdDone.Enabled = True
    cmdMoreBrushs.Enabled = True
    cmdOther.Enabled = True
    cmdImageDraw.Enabled = True
    cmdNotes.Enabled = True
    lblPrev.Visible = False
    Call ClearBrushes(0)
    Call ReloadBaseImage(OriginalBaseFileName)
    Call ReloadForm(StoreResults)
ElseIf (frmEventMsgs.Result = 5) Then

End If
frmDrawNew.ZOrder 0

    Exit Sub

lcmdOImage_Click_Error:

    LogError "frmDrawNew", "cmdOImage_Click", Err, Err.Description
End Sub

Private Sub cmdPalette_Click()
    On Error GoTo lcmdPalette_Click_Error

PlacementInProgress = False
frmPalette.ColorSelected = -1
frmPalette.LineSelected = -1
If (frmPalette.LoadPalette(0, "")) Then
    frmDrawNew.ZOrder 0
    frmPalette.Show 1
    If (frmPalette.ColorSelected <> -1) Then
        PencilColor = frmPalette.ColorSelected
        If (CurrentImageId > 0) Then
            TheImage(CurrentImageId).BrushReference.DefaultColor = PencilColor
        End If
    End If
    If (frmPalette.LineSelected <> -1) Then
        PencilWidth = frmPalette.LineSelected
        If (CurrentImageId > 0) Then
            If (PencilWidth = 6) Then
                TheImage(CurrentImageId).BrushReference.DefaultWidth = "2"
            ElseIf (PencilWidth = 10) Then
                TheImage(CurrentImageId).BrushReference.DefaultWidth = "3"
            Else
                TheImage(CurrentImageId).BrushReference.DefaultWidth = "1"
            End If
            FxImage1.DrawWidth = PencilWidth
        End If
    End If
End If
frmDrawNew.ZOrder 0

    Exit Sub

lcmdPalette_Click_Error:

    LogError "frmDrawNew", "cmdPalette_Click", Err, Err.Description
End Sub

Private Sub cmdNextOther_Click()
    On Error GoTo lcmdNextOther_Click_Error

If (CurrentOtherIndex > TotalOtherDiags) Then
    CurrentOtherIndex = 1
End If
Call LoadOtherDiags(OtherDiags)

    Exit Sub

lcmdNextOther_Click_Error:

    LogError "frmDrawNew", "cmdNextOther_Click", Err, Err.Description
End Sub

Private Sub cmdPrevOther_Click()
    On Error GoTo lcmdPrevOther_Click_Error

CurrentOtherIndex = CurrentOtherIndex - 26
If (CurrentOtherIndex < 1) Then
    CurrentOtherIndex = 1
End If
Call LoadOtherDiags(OtherDiags)

    Exit Sub

lcmdPrevOther_Click_Error:

    LogError "frmDrawNew", "cmdPrevOther_Click", Err, Err.Description
End Sub

Private Sub cmdOther_Click()
Dim DisplayStart As String
    On Error GoTo lcmdOther_Click_Error

If (cmdOther.Text = "Close Finding") Then
    Call CloseOtherSelection(lstOthers.ListIndex)
Else
    If (TotalImagesSelected < MaxImagesAllowed) Then
        frmDrawNew.ZOrder 0
        Call GetStartingValue(DisplayStart)
        If (Trim(DisplayStart) <> "") Then
            If Not (LoadOtherDiags(DisplayStart)) Then
                frmEventMsgs.Header = "No <" + DisplayStart + "> Diagnosis"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmDrawNew.ZOrder 0
                frmEventMsgs.Show 1
            Else
                cmdDone.Enabled = False
                cmdNotes.Enabled = False
                cmdClear.Enabled = False
                cmdImageDraw.Enabled = False
                cmdOther.Text = "Close Finding"
            End If
        End If
    End If
End If
frmDrawNew.ZOrder 0

    Exit Sub

lcmdOther_Click_Error:

    LogError "frmDrawNew", "cmdOther_Click", Err, Err.Description
End Sub

Private Sub cmdClear_Click()
Dim Idx As Integer
    On Error GoTo lcmdClear_Click_Error

For Idx = 1 To MaxImagesAllowed
    Call DeleteTheBrush(Idx)
    If 0 < TheImage(Idx).ButtonNumber <= TotalBrushButtons Then
        If TheImage(Idx).ButtonNumber - 1 >= 0 Then cmdBrush(TheImage(Idx).ButtonNumber - 1).BackColor = OriginalColor
    End If
    Call InitializeImages(Idx, False)
    Call BuildStoreFile
    Call PrevImageName(DrawFileName)
    Call SetNewDrawFile(DrawFileName, False)
    PrevImageId = CurrentImageId
    CurrentImageId = 0
    cmdRotate.Visible = False
    cmdImageDraw.Visible = False
    cmdScaleUp.Visible = False
    cmdScaleUp.ToolTipText = ""
    cmdScaleDown.Visible = False
    cmdScaleDown.ToolTipText = ""
Next Idx

    Exit Sub

lcmdClear_Click_Error:

    LogError "frmDrawNew", "cmdClear_Click", Err, Err.Description, "Index: " & str$(Idx)
End Sub

Private Sub cmdHome_Click()
Dim i As Integer
Dim j As Integer
Dim ATime As Integer
Dim MyTime As String
Dim AFile As String
Dim MyFile As String
Dim NewFile As String
    On Error GoTo lcmdHome_Click_Error

If (FirstTimeForImage) Then
    frmEventMsgs.Header = "Drawing and Finding will not be saved"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "Save"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        If (FM.IsFileThere(StoreResults)) Then
            FM.Kill StoreResults
        End If
        GoSub CleanUp
        HomeOn = True
        Unload frmDrawNew
        FormOn = False
    Else
        Call cmdDone_Click
    End If
Else
    GoSub CleanUp
    HomeOn = True
    Unload frmDrawNew
    FormOn = False
End If
Exit Sub
CleanUp:
    MyTime = ""
    Call FormatTimeNow(MyTime)
    ATime = ConvertTimeToMinutes(MyTime)
    MyTime = Trim(str(ATime))
    i = InStrPS(DrawFileName, "--")
    If (i > 0) Then
        j = InStrPS(i, DrawFileName, ".")
        If (j > 0) Then
            AFile = Left(DrawFileName, i + 1) + "*" + DrawExt
            MyFile = Dir(AFile)
            While (MyFile <> "")
                If (FM.IsFileThere(LocalMyImageDir + MyFile)) Then
                    NewFile = MyFile
                    Call ReplaceCharacters(NewFile, "--", "__")
                    NewFile = NewFile + "-00" + MyTime
                    If (FM.IsFileThere(LocalMyImageDir + NewFile)) Then
                        FM.Kill LocalMyImageDir + NewFile
                    End If
                    FM.Name LocalMyImageDir + MyFile, LocalMyImageDir + NewFile
                End If
                MyFile = Dir
            Wend
        End If
    End If
    Return

    Exit Sub

lcmdHome_Click_Error:

    LogError "frmDrawNew", "cmdHome_Click", Err, Err.Description
End Sub

Private Sub cmdDone_Click()
    On Error GoTo lcmdDone_Click_Error

cmdDone.Enabled = False
If (Trim(StoreResults) <> "") Then
    If (CurrentImageId > 0) Then
        Call BurnTheImage
    End If
    Call BuildStoreFile
    Call UpdatePrimaryImage
    Set TheBrush = Nothing
    If Not (ReloadReplaceScanOn) Then
        cmdDone.Enabled = True
        Unload frmDrawNew
        FormOn = False
        JumpOn = False
    ElseIf (ReloadReplaceScanOn) Then
        ReloadReplaceScanOn = False
    End If
End If
cmdDone.Enabled = True

    Exit Sub

lcmdDone_Click_Error:

    LogError "frmDrawNew", "cmdDone_Click", Err, Err.Description
End Sub

Private Sub cmdImageDraw_Click()
Dim i As Integer
    On Error GoTo lcmdImageDraw_Click_Error

If (cmdImageDraw.Text <> "Draw It Myself") Then
    If (CurrentImageId > 0) Then
        DropOnImage = True
        cmdImageDraw.Text = "Draw It Myself"
        cmdRotate.Visible = False
        If (TheImage(CurrentImageId).BrushReference.SizingOn) Then
            cmdImageDraw.Visible = True
            cmdScaleUp.Visible = False
            cmdScaleUp.ToolTipText = ""
            cmdScaleDown.Visible = False
            cmdScaleDown.ToolTipText = ""
        End If
        If (TheImage(CurrentImageId).BrushReference.ScalingOn) Then
            cmdImageDraw.Visible = True
            cmdScaleUp.Visible = False
            cmdScaleUp.ToolTipText = ""
            cmdScaleDown.Visible = False
            cmdScaleDown.ToolTipText = ""
        End If
    End If
Else
    If (CurrentImageId > 0) Then
        DropOnImage = False
        cmdImageDraw.Text = "Drop On Image"
        cmdImageDraw.Visible = False
        cmdRotate.Visible = False
        cmdScaleUp.Visible = False
        cmdScaleUp.ToolTipText = ""
        cmdScaleDown.Visible = False
        cmdScaleDown.ToolTipText = ""
        PencilColor = GetDefaultPencilColor(CurrentImageId)
        If (Trim(TheImage(CurrentImageId).BrushReference.DefaultWidth) = "2") Then
            PencilWidth = 6
        ElseIf (Trim(TheImage(CurrentImageId).BrushReference.DefaultWidth) = "3") Then
            PencilWidth = 10
        Else
            PencilWidth = 2
        End If
        If (PencilWidth <> frmDrawNew.DrawWidth) Then
            frmDrawNew.DrawWidth = PencilWidth
            FxImage1.DrawWidth = PencilWidth
            DoEvents
            frmDrawNew.DrawWidth = PencilWidth
            FxImage1.DrawWidth = PencilWidth
            DoEvents
'spin
            For i = 1 To 1000
                DoEvents
            Next i
        End If
    End If
End If
frmDrawNew.ZOrder 0

    Exit Sub

lcmdImageDraw_Click_Error:

    LogError "frmDrawNew", "cmdImageDraw_Click", Err, Err.Description
End Sub

Private Sub SetMyDraw()
Dim IFound As Boolean
    On Error GoTo lSetMyDraw_Error

If Not (InitOn) Then
    frmEventMsgs.Header = "You may create only one image for these findings"
    frmEventMsgs.AcceptText = "Retain Drawing"
    frmEventMsgs.RejectText = "Draw It Myself"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmDrawNew.ZOrder 0
    frmEventMsgs.Show 1
    frmDrawNew.ZOrder 0
    If (frmEventMsgs.Result = 2) Then
        If (FM.IsFileThere(DrawFileName)) Then
            FM.Kill DrawFileName
        End If
        DrawFileName = ""
    End If
End If
If (Trim(DrawFileName) = "") Or (Trim(DrawFileName) = BaseImage + BaseImageRef) Or (Trim(ReplaceBase) <> "") Then
    Call SetNewDrawFileName(DrawFileName, False, IFound)
    Call ClearBrushes(0)
    Call SetNewDrawFile(DrawFileName, True)
Else
    Call SetNewDrawFile(DrawFileName, False)
End If

    Exit Sub

lSetMyDraw_Error:

    LogError "frmDrawNew", "SetMyDraw", Err, Err.Description
End Sub

Private Sub cmdNotes_Click()
    On Error GoTo lcmdNotes_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
frmDrawNew.ZOrder 0
frmNotes.NoteId = 0
frmNotes.SystemReference = SystemReference
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    NoteOn = frmNotes.NoteOn
End If
frmDrawNew.ZOrder 0

    Exit Sub

lcmdNotes_Click_Error:

    LogError "frmDrawNew", "cmdNotes_Click", Err, Err.Description
End Sub

Private Sub ActivateBrush(Ref As Integer, SelectNew As Boolean)
Dim Temp As String
Dim BItem As Integer
Dim TestTriggerOn As Boolean
Dim BrushName As String
Dim BrushLingo As String
    On Error GoTo lActivateBrush_Error

PlacementInProgress = False
DropLineOn = False
EraseOn = False
lblErase.Visible = False
cmdImageDraw.Visible = False
cmdScaleUp.Visible = False
cmdScaleDown.Visible = False
cmdRotate.Visible = False
If Not (ReloadImages) And (CurrentImageId < 1) Then
    CurrentImageId = IsBrushAlreadyUsed(Ref)
End If
If ((CurrentImageId < 1) Or (SelectNew)) And (Not ReloadImages) Then
    If (CurrentImageId > 0) Then
        Call BurnTheImage
    End If
    CurrentImageId = SelectImageId
    If (CurrentImageId < 1) Then
        frmEventMsgs.Header = "Only" + str(MaxImagesAllowed) + " Brushes Allowed"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
End If
If (CurrentImageId < 1) Then
    Exit Sub
End If
If (TheImage(CurrentImageId).BrushReference.LocationSupport = "F") Then
    LocPropOn = False
ElseIf (Trim(TheImage(CurrentImageId).BrushReference.LocationSupport) = "") Then
    LocPropOn = True
ElseIf (InStrPS(TheImage(CurrentImageId).BrushReference.LocationSupport, BaseImage) = 0) Then
    LocPropOn = False
Else
    Call IsLocationSupportOn
End If
If -1 < Ref < TotalBrushButtons Then
    With cmdBrush(Ref)
        If Trim$(.ToolTipText) <> "" Then
            .BackColor = SelectionColor
            BItem = Val(Mid$(.ToolTipText, 2, Len(.ToolTipText) - 1))
            BrushName = Trim$(.Tag)
            BrushLingo = Trim$(.Text)
            PlacementInProgress = True
        End If
    End With
Else
    PrevImageId = CurrentImageId
    CurrentImageId = 0
    CurrentBrushSelection = 0
End If
If (BItem > -1) Then
    CurrentBrushSelection = Ref
    LocalBrushIndex = BItem
    CurrentEndXLocation = 0
    CurrentEndYLocation = 0
    If (Not ReloadImages) Then
        Call SetImageArray(CurrentImageId, BrushName)
    End If
    TheImage(CurrentImageId).ButtonNumber = Ref
    If (Trim(TheImage(CurrentImageId).Eye) = "") Then
        'TODO: What about EyeContext = "OU"?
        If (EyeContext = "OS") Then
            TheImage(CurrentImageId).Eye = "OS"
        Else
            TheImage(CurrentImageId).Eye = "OD"
        End If
    End If
    TestTriggerOn = False
    If (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) <> "") Then
        TestTriggerOn = True
    End If
    cmdImageDraw.Visible = False
    If (TheImage(CurrentImageId).BrushReference.FrameHeight > 0) Then
        cmdImageDraw.Visible = True
    End If
    If (DropOnImage) And Not (TestTriggerOn) Then
        cmdRotate.Visible = TheButtons(BItem).RotateOn
        If (TheButtons(BItem).SizingOn) Then
            cmdScaleUp.Visible = False
            cmdScaleUp.ToolTipText = ""
            cmdScaleDown.Visible = False
            cmdScaleDown.ToolTipText = ""
        End If
        If (TheButtons(BItem).ScalingOn) Then
            cmdScaleUp.Visible = False
            cmdScaleUp.ToolTipText = ""
            cmdScaleDown.Visible = False
            cmdScaleDown.ToolTipText = ""
        End If
        If (Trim(TheImage(CurrentImageId).BrushReference.BrushLingo) = "") And Not (TestTriggerOn) Then
            TheImage(CurrentImageId).BrushReference.BrushLingo = BrushLingo
        End If
        Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
        DropLineOn = True
    Else
        If (TestTriggerOn) And Not (ReloadImages) Then
            If (TriggerTest(TheImage(CurrentImageId).BrushReference.TestTrigger, Temp)) Then
                TheImage(CurrentImageId).BrushReference.TestTriggerText = Temp
                If (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "72A") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "27A") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "32Z") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "32X") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "32Y") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "32X") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "61A") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "62A") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "63A") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "62D") Then
                    Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Temp)
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
                Else
                    Call DropLabel(TheImage(CurrentImageId).BrushReference.TestTriggerText, CurrentImageId)
                End If
                DropLineOn = False
            Else
                TheImage(CurrentImageId).Name = ""
                PrevImageId = CurrentImageId
                CurrentImageId = 0
            End If
            Exit Sub
        End If
        If (Trim(TheImage(CurrentImageId).BrushReference.BrushLingo) = "") And (Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText) = "") Then
            TheImage(CurrentImageId).BrushReference.BrushLingo = BrushLingo
            Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
            If (Trim(TheImage(CurrentImageId).BrushReference.Quantifier) <> "") Then
                Call SetLabelCaption(CurrentImageId)
            End If
        ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText) <> "") Then
            If (Trim(TheImage(CurrentImageId).BrushReference.BrushLingo) = "") Then
                TheImage(CurrentImageId).BrushReference.BrushLingo = BrushLingo
            End If
            If (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "72A") Then
                Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText))
                Call DropLabel(BrushLingo, CurrentImageId)
            ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "27A") Then
                Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText))
                Call DropLabel(BrushLingo, CurrentImageId)
            ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "32Z") Then
                Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText))
                Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
            ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "32Y") Then
                Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText))
                Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
            ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "32X") Then
                Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText))
                Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
            ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "61A") Then
                Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText))
                Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
            ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "62A") Then
                Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText))
                Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
            ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "63A") Then
                Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText))
                Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
            ElseIf (Trim(TheImage(CurrentImageId).BrushReference.TestTrigger) = "62D") Then
                Call AppendTextToImage(Trim(TheImage(CurrentImageId).BrushReference.TestTrigger), Trim(TheImage(CurrentImageId).BrushReference.TestTriggerText))
                Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
            Else
                Call DropLabel(TheImage(CurrentImageId).BrushReference.TestTriggerText, CurrentImageId)
            End If
            If (Trim(TheImage(CurrentImageId).BrushReference.Quantifier) <> "") Then
                Call SetLabelCaption(CurrentImageId)
            End If
        Else
            If (Trim(TheImage(CurrentImageId).BrushReference.BrushLingo) = "") Then
                TheImage(CurrentImageId).BrushReference.BrushLingo = BrushLingo
            End If
            Call DropLabel(TheImage(CurrentImageId).BrushReference.BrushLingo, CurrentImageId)
            If (Trim(TheImage(CurrentImageId).BrushReference.Quantifier) <> "") Then
                Call SetLabelCaption(CurrentImageId)
            End If
        End If
        DropLineOn = True
        If (Trim(TheImage(CurrentImageId).BrushReference.DefaultWidth) <> "") Then
            If (Trim(TheImage(CurrentImageId).BrushReference.DefaultWidth) = "2") Then
                PencilWidth = 6
            ElseIf (Trim(TheImage(CurrentImageId).BrushReference.DefaultWidth) = "3") Then
                PencilWidth = 10
            Else
                PencilWidth = 2
            End If
        Else
            PencilWidth = 2
        End If
        FxImage1.DrawWidth = PencilWidth
        PencilColor = GetDefaultPencilColor(CurrentImageId)
    End If
End If

    Exit Sub

lActivateBrush_Error:

    LogError "frmDrawNew", "ActivateBrush", Err, Err.Description
End Sub

Private Sub cmdBrush_Click(Index As Integer)

    On Error GoTo lcmdBrush_Click_Error

    If Trim$(cmdBrush(Index).Tag) <> "9900" Then
        If Trim$(cmdBrush(Index).Tag) <> "" Then
            If FM.IsFileThere(ImageDir + Trim(cmdBrush(Index).Tag) + ".bmp") Then
                DropOnImage = True
                cmdImageDraw.Text = "Draw It Myself"
            Else
                DropOnImage = False
                cmdImageDraw.Text = "Drop On Image"
            End If
        End If
    Else
        DropOnImage = False
        cmdImageDraw.Text = "Drop On Image"
    End If
    ActivateBrush Index, True
    If CurrentImageId < 1 Then
        If cmdBrush(Index).BackColor = SelectionColor Then
            cmdBrush(Index).BackColor = OriginalColor
        End If
    End If

    Exit Sub

lcmdBrush_Click_Error:

    LogError "frmDrawNew", "cmdBrush_Click", Err, Err.Description, str$(Index)
End Sub

Private Sub cmdQuant_Click()
Dim Idx As Integer
    On Error GoTo lcmdQuant_Click_Error

If (CurrentImageId > 0) Then
    Idx = CurrentImageId
    Call BurnTheImage
    Call SetQuantifier(Idx)
End If

    Exit Sub

lcmdQuant_Click_Error:

    LogError "frmDrawNew", "cmdQuant_Click", Err, Err.Description
End Sub

Private Sub cmdRO_Click()
Dim Idx As Integer
Dim Temp As String
    On Error GoTo lcmdRO_Click_Error

If (CurrentImageId > 0) Then
    Idx = CurrentImageId
    TheImage(Idx).RuleOut = Not (TheImage(Idx).RuleOut)
    Temp = TheImage(Idx).BrushReference.BrushLingo
    Call SetLabelCaption(Idx)
End If

    Exit Sub

lcmdRO_Click_Error:

    LogError "frmDrawNew", "cmdRO_Click", Err, Err.Description
End Sub

Private Sub cmdScaleUp_Click()
Dim a As Single, b As Single
Dim TheSize As Integer
    On Error GoTo lcmdScaleUp_Click_Error

ScalingDirection = 1
TheSize = Val(cmdScaleUp.ToolTipText)
If (TheSize < 1) Then
    TheSize = 1
End If
TheSize = TheSize + 1
If (TheSize <= MaxMagnificationFactor) Then
    cmdScaleUp.ToolTipText = Trim(str(TheSize))
    cmdScaleDown.ToolTipText = Trim(str(TheSize))
    TheImage(CurrentImageId).Size = TheSize
    If (TheImage(CurrentImageId).Top > 0) Then
        ScaleUpOn = True
        ScaleDownOn = False
        ReloadImages = True
        If (TheImage(CurrentImageId).OriginalLeft > 0) And (TheImage(CurrentImageId).OriginalTop > 0) Then
            a = TheImage(CurrentImageId).OriginalLeft
            b = TheImage(CurrentImageId).OriginalTop
        Else
            a = TheImage(CurrentImageId).Left
            b = TheImage(CurrentImageId).Top
        End If
        Call DropImage(a, b, True)
        Call DropImage(a, b, True)
        ScaleUpOn = False
        ReloadImages = False
    End If
End If

    Exit Sub

lcmdScaleUp_Click_Error:

    LogError "frmDrawNew", "cmdScaleUp_Click", Err, Err.Description
End Sub

Private Sub cmdScaleDown_Click()
Dim a As Single, b As Single
Dim TheSize As Integer
    On Error GoTo lcmdScaleDown_Click_Error

ScalingDirection = -1
TheSize = Val(cmdScaleUp.ToolTipText)
TheSize = TheSize - 1
If (TheSize >= 1) Then
    cmdScaleUp.ToolTipText = Trim(str(TheSize))
    cmdScaleDown.ToolTipText = Trim(str(TheSize))
    If (TheSize = 1) Then
        cmdScaleUp.ToolTipText = ""
        cmdScaleDown.ToolTipText = ""
    End If
    TheImage(CurrentImageId).Size = TheSize
    If (TheImage(CurrentImageId).Top > 0) Then
        ScaleUpOn = False
        ScaleDownOn = True
        ReloadImages = True
        a = TheImage(CurrentImageId).Left
        b = TheImage(CurrentImageId).Top
        If (TheImage(CurrentImageId).OriginalLeft > 0) And (TheImage(CurrentImageId).OriginalTop > 0) Then
            TheImage(CurrentImageId).OriginalLeft = TheImage(CurrentImageId).Left
            TheImage(CurrentImageId).OriginalTop = TheImage(CurrentImageId).Top
        End If
        Call DropImage(a, b, True)
        ScaleDownOn = False
        ReloadImages = False
    End If
End If

    Exit Sub

lcmdScaleDown_Click_Error:

    LogError "frmDrawNew", "cmdScaleDown_Click", Err, Err.Description
End Sub

Private Sub cmdRotate_Click()
Dim a As Single, b As Single
    On Error GoTo lcmdRotate_Click_Error

If (TheImage(CurrentImageId).Top > 0) Then
    If (TheImage(CurrentImageId).Rotate = ROTATE_None) Then
        TheImage(CurrentImageId).Rotate = ROTATE_90
    ElseIf (TheImage(CurrentImageId).Rotate = ROTATE_90) Then
        TheImage(CurrentImageId).Rotate = ROTATE_180
    ElseIf (TheImage(CurrentImageId).Rotate = ROTATE_180) Then
        TheImage(CurrentImageId).Rotate = ROTATE_270
    ElseIf (TheImage(CurrentImageId).Rotate = ROTATE_270) Then
        TheImage(CurrentImageId).Rotate = ROTATE_None
    Else
        TheImage(CurrentImageId).Rotate = ROTATE_None
    End If
    a = TheImage(CurrentImageId).Left
    b = TheImage(CurrentImageId).Top
    If (TheImage(CurrentImageId).OriginalLeft > 0) And (TheImage(CurrentImageId).OriginalTop > 0) Then
        TheImage(CurrentImageId).OriginalLeft = TheImage(CurrentImageId).Left
        TheImage(CurrentImageId).OriginalTop = TheImage(CurrentImageId).Top
    End If
    ReloadImages = True
    Call DropImage(a, b, True)
    ReloadImages = False
End If

    Exit Sub

lcmdRotate_Click_Error:

    LogError "frmDrawNew", "cmdRotate_Click", Err, Err.Description
End Sub

Private Sub SetImageCharacteristics(BrushName As String, AImage As FXImage, PlantFile As Boolean, ImageType As String)
Dim TheFile As String
    On Error GoTo lSetImageCharacteristics_Error

With AImage
    If (PlantFile) And (GlobalHeight > 0) And (GlobalWidth > 0) Then
        If (ImageType = "B") Then
            TheFile = ImageDir + BrushName + ".bmp"
        ElseIf (ImageType = "J") Then
            TheFile = ImageDir + BrushName + ".jpg"
        ElseIf (ImageType = "G") Then
            TheFile = ImageDir + BrushName + ".gif"
        ElseIf (ImageType = "P") Then
            TheFile = ImageDir + BrushName + ".png"
        ElseIf (ImageType = "T") Then
            TheFile = ImageDir + BrushName + ".tif"
        Else
            TheFile = ImageDir + BrushName + ".bmp"
        End If
        .Left = GlobalLeft
        .Top = GlobalTop
        .Height = GlobalHeight
        .Width = GlobalWidth
        .AutoSize = ISIZE_ResizeImageToControl
        .ShowRotated = TheImage(CurrentImageId).Rotate
        .Tag = ""
        .BackColor = &HFFFFFF
        .Transparent = True
        .TransparentColor = &HFFFFFF
        .TransparentMode = TM_OverlayPlusCapturedImage
        .FileName = TheFile
        .Capture = ICAP_None
        .Update = True
        .Enabled = True
        .Visible = PlantFile
        .ZOrder 0
        .Refresh
    End If
End With

    Exit Sub

lSetImageCharacteristics_Error:

    LogError "frmDrawNew", "SetImageCharacteristics", Err, Err.Description
End Sub

Private Sub PlantImage(BrushName As String, Counter As Integer, ImageType As String)
On Error GoTo UI_ErrorHandler
Dim TheFile As String
Dim FileOk As Boolean
Select Case ImageType
    Case "B"
        TheFile = ImageDir + BrushName + ".bmp"
    Case "J"
        TheFile = ImageDir + BrushName + ".jpg"
    Case "G"
        TheFile = ImageDir + BrushName + ".gif"
    Case "P"
        TheFile = ImageDir + BrushName + ".png"
    Case "T"
        TheFile = ImageDir + BrushName + ".tif"
    Case Else
        TheFile = ImageDir + BrushName + ".bmp"
End Select
frmDrawNew.Enabled = False
FileOk = FM.IsFileThere(TheFile)
If (FileOk) Then
    FileOk = Not (TheImage(Counter).Burn)
End If
If (Counter = 1) Then
    TheImage(Counter).ImageName = 1
    Call SetImageCharacteristics(BrushName, imgImage1, FileOk, ImageType)
ElseIf (Counter = 2) Then
    TheImage(Counter).ImageName = 2
    Call SetImageCharacteristics(BrushName, imgImage2, FileOk, ImageType)
ElseIf (Counter = 3) Then
    TheImage(Counter).ImageName = 3
    Call SetImageCharacteristics(BrushName, imgImage3, FileOk, ImageType)
ElseIf (Counter = 4) Then
    TheImage(Counter).ImageName = 4
    Call SetImageCharacteristics(BrushName, imgImage4, FileOk, ImageType)
ElseIf (Counter = 5) Then
    TheImage(Counter).ImageName = 5
    Call SetImageCharacteristics(BrushName, imgImage5, FileOk, ImageType)
ElseIf (Counter = 6) Then
    TheImage(Counter).ImageName = 6
    Call SetImageCharacteristics(BrushName, imgImage6, FileOk, ImageType)
ElseIf (Counter = 7) Then
    TheImage(Counter).ImageName = 7
    Call SetImageCharacteristics(BrushName, imgImage7, FileOk, ImageType)
ElseIf (Counter = 8) Then
    TheImage(Counter).ImageName = 8
    Call SetImageCharacteristics(BrushName, imgImage8, FileOk, ImageType)
ElseIf (Counter = 9) Then
    TheImage(Counter).ImageName = 9
    Call SetImageCharacteristics(BrushName, imgImage9, FileOk, ImageType)
ElseIf (Counter = 10) Then
    TheImage(Counter).ImageName = 10
    Call SetImageCharacteristics(BrushName, imgImage10, FileOk, ImageType)
ElseIf (Counter = 11) Then
    TheImage(Counter).ImageName = 11
    Call SetImageCharacteristics(BrushName, imgImage11, FileOk, ImageType)
ElseIf (Counter = 12) Then
    TheImage(Counter).ImageName = 12
    Call SetImageCharacteristics(BrushName, imgImage12, FileOk, ImageType)
ElseIf (Counter = 13) Then
    TheImage(Counter).ImageName = 13
    Call SetImageCharacteristics(BrushName, imgImage13, FileOk, ImageType)
ElseIf (Counter = 14) Then
    TheImage(Counter).ImageName = 14
    Call SetImageCharacteristics(BrushName, imgImage14, FileOk, ImageType)
ElseIf (Counter = 15) Then
    TheImage(Counter).ImageName = 15
    Call SetImageCharacteristics(BrushName, imgImage15, FileOk, ImageType)
ElseIf (Counter = 16) Then
    TheImage(Counter).ImageName = 16
    Call SetImageCharacteristics(BrushName, imgImage16, FileOk, ImageType)
ElseIf (Counter = 17) Then
    TheImage(Counter).ImageName = 17
    Call SetImageCharacteristics(BrushName, imgImage17, FileOk, ImageType)
ElseIf (Counter = 18) Then
    TheImage(Counter).ImageName = 18
    Call SetImageCharacteristics(BrushName, imgImage18, FileOk, ImageType)
Else
    frmDrawNew.Enabled = True
    Exit Sub
End If
TheImage(Counter).IsImageSet = FileOk
TheImage(Counter).DroppedImage = DropOnImage
TheImage(Counter).BrushId = CurrentBrushSelection
TheImage(Counter).Name = BrushName
If Not (ReloadImages) Then
    TheImage(Counter).RuleOut = False
End If
TheImage(Counter).Top = GlobalTop
TheImage(Counter).Left = GlobalLeft
If (TheImage(Counter).OriginalLeft = 0) And (TheImage(Counter).OriginalTop = 0) Then
    TheImage(Counter).OriginalLeft = GlobalLeft
    TheImage(Counter).OriginalTop = GlobalTop
End If
If (TheImage(Counter).Left < ((FxImage1.Left + FxImage1.Width) / 1.6)) Then
    If (TheButtons(LocalBrushIndex).LocateYOn < 1) Then
        TheImage(Counter).Eye = "OD"
        If (TheImage(Counter).LabelId < 10) Then
            Call ResetLabel(Counter)
        End If
    Else
        If (TheButtons(LocalBrushIndex).LocalSide = "L") Then
            TheImage(Counter).Eye = "OS"
            Call ResetLabel(Counter)
        Else
            TheImage(Counter).Eye = "OD"
            If (TheImage(Counter).LabelId < 10) Then
                Call ResetLabel(Counter)
            End If
        End If
    End If
Else
    If (TheButtons(LocalBrushIndex).LocateYOn < 1) Then
        TheImage(Counter).Eye = "OS"
        Call ResetLabel(Counter)
    Else
        If (TheButtons(LocalBrushIndex).LocalSide = "L") Then
            TheImage(Counter).Eye = "OS"
            Call ResetLabel(Counter)
        Else
            TheImage(Counter).Eye = "OD"
            If (TheImage(Counter).LabelId < 10) Then
                Call ResetLabel(Counter)
            End If
        End If
    End If
End If
TheImage(Counter).LocalSide = TheButtons(LocalBrushIndex).LocalSide
'TheImage(Counter).CNF = False
TheImage(Counter).Size = Val(cmdScaleUp.ToolTipText)
TheImage(Counter).BrushReference.BrushArrayIndex = LocalBrushIndex
TheImage(Counter).BrushReference.DiagnosisId = TheButtons(LocalBrushIndex).DiagnosisId
TheImage(Counter).BrushReference.BrushType = TheButtons(LocalBrushIndex).BrushType
TheImage(Counter).BrushReference.BrushName = TheButtons(LocalBrushIndex).BrushName
TheImage(Counter).BrushReference.AssociatedId = TheButtons(LocalBrushIndex).AssociatedId
TheImage(Counter).BrushReference.BrushLingo = TheButtons(LocalBrushIndex).BrushLingo
TheImage(Counter).BrushReference.BrushImageType = TheButtons(LocalBrushIndex).BrushImageType
TheImage(Counter).BrushReference.DefaultColor = TheButtons(LocalBrushIndex).DefaultColor
TheImage(Counter).BrushReference.DefaultWidth = TheButtons(LocalBrushIndex).DefaultWidth
TheImage(Counter).BrushReference.RotateOn = TheButtons(LocalBrushIndex).RotateOn
TheImage(Counter).BrushReference.SizingOn = TheButtons(LocalBrushIndex).SizingOn
TheImage(Counter).BrushReference.ScalingOn = TheButtons(LocalBrushIndex).ScalingOn
TheImage(Counter).BrushReference.LocateXOn = TheButtons(LocalBrushIndex).LocateXOn
TheImage(Counter).BrushReference.LocateYOn = TheButtons(LocalBrushIndex).LocateYOn
TheImage(Counter).BrushReference.FrameWidth = TheButtons(LocalBrushIndex).FrameWidth
TheImage(Counter).BrushReference.FrameHeight = TheButtons(LocalBrushIndex).FrameHeight
frmDrawNew.Enabled = True
Exit Sub
UI_ErrorHandler:
    Call PinpointError("PlantImage", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume Next
End Sub

Public Function LoadDraw(InitIt As Boolean) As Boolean
On Error GoTo UI_ErrorHandler
Dim CurrentLocation As Integer
Dim u As Integer, Ref As Integer
Dim OtherText As String
Dim ExtFileName As String
Dim Temp As String, AFile As String
Dim LocalFile As String, ServerFile As String
Dim ABrush As DiagnosisMasterBrush
HomeOn = False
JumpOn = False
AFile = ""
OtherDiags = ""
OriginalBaseFileName = ""
ControlClear = True
MagnifyDontShift = False
ReloadReplaceScanOn = False
CurrentOtherIndex = 1
ScalingDirection = 1
cmdPrevOther.Visible = False
cmdNextOther.Visible = False
StartLinesOn = CheckConfigCollection("DRAWLINEON") = "T"
BaseExt = ".jpg"
NoteOn = False
LocPropOn = False
PencilWidth = 2
PencilColor = frmPalette.cmdBlue.BackColor
frmDrawNew.MousePointer = 2
lstOthers.Visible = False
DrawingOn = False
DropLineOn = False
DropOnImage = False
ScaleUpOn = False
ScaleDownOn = False

ClearBMLabels

ClearRSenLabels

ClearRVersLabels

lblWD4N.Visible = False
lblWD4N.Caption = "Label1"
lblWD4D.Visible = False
lblWD4D.Caption = "Label2"
lblSterioN.Visible = False
lblSterioN.Caption = "Label3"
lblSterioD.Visible = False
lblSterioD.Caption = "Label4"

lblRTest1.Visible = False
lblRTest1.Caption = "Label1"
lblRTest2.Visible = False
lblRTest2.Caption = "Label2"
lblRTest3.Visible = False
lblRTest3.Caption = "Label3"
lblRTest4.Visible = False
lblRTest4.Caption = "Label4"
lblLTest1.Visible = False
lblLTest1.Caption = "Label1"
lblLTest2.Visible = False
lblLTest2.Caption = "Label2"
lblLTest3.Visible = False
lblLTest3.Caption = "Label3"
lblLTest4.Visible = False
lblLTest4.Caption = "Label4"
lblRTestA1.Caption = "Label1"
lblRTestA1.Visible = False
lblLTestA1.Caption = "Label1"
lblLTestA1.Visible = False
XTwips = 1
YTwips = 1
If (frmDrawNew.ScaleMode = 1) Then
    XTwips = Screen.TwipsPerPixelX
    YTwips = Screen.TwipsPerPixelY
End If
If (ReplaceBase = "") Then
    Set OPic = cmdBrush(0).picture
End If
OriginalColor = cmdBrush(0).BackColor
If (InitOn) Then
    InitOn = False
    If Not (VerifyDraw(DiagnosisId, ImageRef)) Then
        Exit Function
    End If
End If
BaseImage = Left(ImageRef, 2)
BaseImageRef = ""
If (EyeContext = "OS") Then
    BaseImageRef = "L"
ElseIf (EyeContext = "OD") Then
    BaseImageRef = "R"
End If
AFile = ImageDir + BaseImage + BaseImageRef
Temp = Dir(AFile + ".*")
If (Trim(Temp) = "") Then
    BaseImageRef = ""
    AFile = ImageDir + BaseImage + BaseImageRef
End If
If Not (FM.IsFileThere(AFile + BaseExt)) Then
    If (BaseExt = ".jpg") Then
        If (FM.IsFileThere(AFile + ".bmp")) Then
            BaseExt = ".bmp"
        End If
    ElseIf (BaseExt = ".bmp") Then
        If (FM.IsFileThere(AFile + ".jpg")) Then
            BaseExt = ".jpg"
        End If
    ElseIf (BaseExt = ".png") Then
        If (FM.IsFileThere(AFile + ".jpg")) Then
            BaseExt = ".jpg"
        End If
    End If
End If
If Not (FM.IsFileThere(AFile + BaseExt)) Then
    BaseImageRef = ""
    AFile = ImageDir + BaseImage + BaseImageRef
End If
If (FM.IsFileThere(AFile + BaseExt)) Then
    Set ABrush = New DiagnosisMasterBrush
    ABrush.BrushLeftRight = ""
    ABrush.BrushDiagnosis = ""
    ABrush.BrushName = String(6, "0") + BaseImage
    If (ABrush.FindBrushbyName) Then
        If (ABrush.SelectBrush(1)) Then
            If (ABrush.BrushFrameWidth > 0) Then
                ABrush.BrushFrameWidth = 462
                FxImage1.Width = ABrush.BrushFrameWidth * XTwips
            End If
            If (ABrush.BrushFrameHeight > 0) Then
                ABrush.BrushFrameHeight = 351
                FxImage1.Height = ABrush.BrushFrameHeight * YTwips
            End If
            FxImage1.AutoSize = ISIZE_ResizeImageToControl
        End If
    End If
    Set ABrush = Nothing
    If (InitIt) Then
        DrawExt = ""
        ExtFileName = MyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-B" + BaseImage + BaseImageRef + ".jpg"
        If (FM.IsFileThere(ExtFileName)) Then
            DrawExt = ".jpg"
        Else
            ExtFileName = MyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-B" + BaseImage + BaseImageRef + ".bmp"
            If (FM.IsFileThere(ExtFileName)) Then
                DrawExt = ".bmp"
            Else
                ExtFileName = MyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-B" + BaseImage + BaseImageRef + ".png"
                If (FM.IsFileThere(ExtFileName)) Then
                    DrawExt = ".png"
                End If
            End If
        End If
        If (DrawExt = "") Then
            ExtFileName = MyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-BI-" + BaseImage + BaseImageRef + ".jpg"
            If (FM.IsFileThere(ExtFileName)) Then
                DrawExt = ".jpg"
            Else
                ExtFileName = MyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-BI-" + BaseImage + BaseImageRef + ".bmp"
                If (FM.IsFileThere(ExtFileName)) Then
                    DrawExt = ".bmp"
                Else
                    ExtFileName = MyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-BI-" + BaseImage + BaseImageRef + ".png"
                    If (FM.IsFileThere(ExtFileName)) Then
                        DrawExt = ".png"
                    Else
                        DrawExt = ".jpg"
                    End If
                End If
            End If
        End If
    End If
    Call VerifyReplacementBase(StoreResults)
    If (Trim(DrawFileName) <> "") And (InStrPS(DrawFileName, ":") = 0) Then
        DrawFileName = LocalMyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-B" + DrawFileName + DrawExt
        If (InStrPS(DrawFileName, "I-") <> 0) Then
            Call SetNewDrawFile(DrawFileName, False)
        Else
            InitOn = True
            Call SetMyDraw
            InitOn = False
        End If
    Else
        If (InitIt) Then
            LocalFile = LocalMyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-B" + BaseImage + BaseImageRef + DrawExt
            ServerFile = MyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-B" + BaseImage + BaseImageRef + DrawExt
            If Not (FM.IsFileThere(ServerFile)) Then
                LocalFile = LocalMyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-BI-" + BaseImage + BaseImageRef + DrawExt
                ServerFile = MyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-BI-" + BaseImage + BaseImageRef + DrawExt
                If (FM.IsFileThere(ServerFile)) Then
                    If (ReplaceBase = "") Then
                        Call PullDrawImageFromServer(ServerFile, LocalFile)
                    End If
                End If
            Else
                If (ReplaceBase = "") Then
                    Call PullDrawImageFromServer(ServerFile, LocalFile)
                End If
            End If
        End If
        If (Trim(ReplaceBase) = "") Then
            DrawFileName = LocalMyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-B" + BaseImage + BaseImageRef + DrawExt
        Else
            DrawFileName = ReplaceBase
        End If
        If (FM.IsFileThere(DrawFileName)) Then
            InitOn = True
            Call SetMyDraw
            InitOn = False
        Else
            DrawFileName = LocalMyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-BI-" + BaseImage + BaseImageRef + DrawExt
            If (FM.IsFileThere(DrawFileName)) Then
                Call SetNewDrawFile(DrawFileName, False)
            Else
                Call SetNewDrawFile(DrawFileName, True)
            End If
        End If
    End If
    If (Trim(ReplaceBase) <> "") Then
        FxImage1.Width = 462 * XTwips
        FxImage1.Height = 351 * YTwips
        FxImage1.AutoSize = ISIZE_ResizeImageToControl
        FxImage1.Tag = ReplaceBase
        FxImage1.Transparent = False
        FxImage1.TransparentColor = &HFFFFFF
        FxImage1.Visible = False
        FxImage1.Enabled = True
        FxImage1.picture = FM.LoadPicture(ReplaceBase)
        OriginalBaseFileName = ReplaceBase
    ElseIf Not (FM.IsFileThere(DrawFileName)) Then
        FxImage1.Width = 462 * XTwips
        FxImage1.Height = 351 * YTwips
        FxImage1.AutoSize = ISIZE_ResizeImageToControl
        FxImage1.Tag = BaseImage + BaseImageRef
        FxImage1.Transparent = False
        FxImage1.TransparentColor = &HFFFFFF
        FxImage1.picture = FM.LoadPicture(ImageDir + BaseImage + BaseImageRef + BaseExt)
        FxImage1.Visible = False
        FxImage1.Enabled = True
        OriginalBaseFileName = ImageDir + BaseImage + BaseImageRef + BaseExt
    End If
    LocPropOn = IsLocationSupportOn
    OtherText = ""
    If (DiagnosisROOn) Then
        OtherText = OtherText + "RO-"
    End If
    If (DiagnosisHOOn) Then
        OtherText = OtherText + "HO-"
    End If
    If (DiagnosisNFOn) Then
        OtherText = OtherText + "NF-"
    End If
    If (Trim(DiagnosisQuant) <> "") Then
        OtherText = OtherText + "[" + Trim(DiagnosisQuant) + "]"
    End If
    If (Trim(OtherText) <> "") Then
        OtherText = OtherText + " "
    End If
    u = InStrPS(DiagnosisName, "-")
    If (u > 0) Then
        lblDiagnosis.Caption = OtherText + Trim(Mid(DiagnosisName, u + 1, Len(DiagnosisName) - u))
    Else
        lblDiagnosis.Caption = OtherText + DiagnosisName
    End If
    lblEye.Caption = EyeContext
    Call InitializeImages(0, True)
    CurrentBrushSelection = 0
    CurrentBrushIndex = 0
    Call LoadBrushArray
    Call LoadAlternateBrushs(StoreResults)
    Call ClearButtons
    Call SetButtons
    If (Trim(ReplaceBase) = "") Then
        Call LoadUnifiedDrawDisplay(BaseImage, StoreResults, False)
    End If
    cmdImageDraw.Visible = False
    cmdScaleUp.Visible = False
    cmdScaleDown.Visible = False
    cmdRotate.Visible = False
    cmdOther.Visible = True
    FxImage1.Visible = True
    Call SaveNewDrawFileName(DrawFileName, False)
End If
If (LocPropOn) Then
    Call LoadLocName(Left(BaseImage, 2))
End If
LoadDraw = True
Call SetPatientName(PatientId, AppointmentId)
ReloadForm StoreResults
Exit Function
UI_ErrorHandler:
    Call PinpointError("LoadDraw", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume LeaveFast
LeaveFast:
End Function

Private Sub ClearButtons()
Dim iIndex As Integer
    For iIndex = 0 To TotalBrushButtons - 1
        With cmdBrush(iIndex)
            .Visible = False
            .Tag = ""
            .Text = ""
            .ToolTipText = ""
            .BackColor = OriginalColor
            .Font.Underline = False
        End With
    Next iIndex
End Sub

Private Sub SetBrush(BrushIndex As Integer, ButtonIndex As Integer)
    On Error GoTo lSetBrush_Error

    With cmdBrush(ButtonIndex)
        .Text = TheButtons(BrushIndex).BrushLingo
        .Tag = " " + TheButtons(BrushIndex).BrushName
        .ToolTipText = TheButtons(BrushIndex).BrushType + Trim(str(BrushIndex))
        .Visible = True
        .Enabled = True
        .BackColor = OriginalColor
        If (TheButtons(BrushIndex).BrushType = "A") Or (TheButtons(BrushIndex).BrushType = "O") Then
            .Font.Underline = True
        Else
            .Font.Underline = False
        End If
        If (FM.IsFileThere(ImageDir + TheButtons(BrushIndex).BrushName + ".bmp")) Then
            Set .picture = OPic
        ElseIf (FM.IsFileThere(ImageDir + TheButtons(BrushIndex).BrushName + ".jpg")) Then
            Set .picture = OPic
        ElseIf (FM.IsFileThere(ImageDir + TheButtons(BrushIndex).BrushName + ".gif")) Then
            Set .picture = OPic
        ElseIf (FM.IsFileThere(ImageDir + TheButtons(BrushIndex).BrushName + ".tif")) Then
            Set .picture = OPic
        ElseIf (FM.IsFileThere(ImageDir + TheButtons(BrushIndex).BrushName + ".png")) Then
            Set .picture = OPic
        Else
            Set .picture = Nothing
        End If
    End With

    Exit Sub

lSetBrush_Error:

    LogError "frmDrawNew", "SetBrush", Err, Err.Description
End Sub

Private Function SetButtons() As Boolean
Dim iBrushIndex As Integer
Dim iButtonIndex As Integer
    On Error GoTo lSetButtons_Error

iButtonIndex = 0
iBrushIndex = CurrentBrushIndex
While (iBrushIndex < MaxBrushsAvailable) And (iButtonIndex < TotalBrushButtons)
    SetBrush iBrushIndex, iButtonIndex
    If IsBrushAlreadyUsed(iButtonIndex) > 0 Then
        cmdBrush(iButtonIndex).BackColor = SelectionColor
    End If
    iButtonIndex = iButtonIndex + 1
    iBrushIndex = iBrushIndex + 1
Wend
CurrentBrushIndex = iBrushIndex
If (MaxBrushsAvailable > MaxBrushVisible + 1) Then
    cmdMoreBrushs.Visible = True
    If (CurrentBrushIndex >= MaxBrushsAvailable) Then
        cmdMoreBrushs.Text = "Start Findings"
    Else
        cmdMoreBrushs.Text = "More Findings"
    End If
Else
    cmdMoreBrushs.Visible = False
End If
If (iButtonIndex >= 0) Then
    SetButtons = True
End If

    Exit Function

lSetButtons_Error:

    LogError "frmDrawNew", "SetButtons", Err, Err.Description
End Function

Private Function LoadNewBrush() As Boolean
Dim Ref As Integer
    On Error GoTo lLoadNewBrush_Error

    Ref = 1
    If (MaxBrushsAvailable < MaxBrushVisible) And (MaxBrushsAvailable > 0) Then
        Ref = MaxBrushsAvailable
        SetBrush Ref, Ref
        If IsBrushAlreadyUsed(Ref) > 0 Then
            cmdBrush(Ref).BackColor = SelectionColor
        End If
    End If
    LoadNewBrush = True

    Exit Function

lLoadNewBrush_Error:

    LogError "frmDrawNew", "LoadNewBrush", Err, Err.Description
End Function

Private Sub cmdMoreBrushs_Click()
    On Error GoTo lcmdMoreBrushs_Click_Error

    ClearButtons
    If (CurrentBrushIndex >= MaxBrushsAvailable) Then
        CurrentBrushIndex = 0
    End If
    SetButtons

    Exit Sub

lcmdMoreBrushs_Click_Error:

    LogError "frmDrawNew", "cmdMoreBrushs_Click", Err, Err.Description
End Sub

Private Sub LoadBrushArray()
Dim i As Integer
Dim Ref As Integer
Dim CheckBrush As String
Dim CheckAssociated As String
Dim CurrentBrushLocation As Integer
Dim RetrieveAssociated As DiagnosisMasterAssociated
Dim Brushes As String
    On Error GoTo lLoadBrushArray_Error

Ref = 0
Erase TheButtons
MaxBrushsAvailable = 0
Brushes = Mid(ImageRef, 3, Len(ImageRef) - 2)
CurrentBrushLocation = 1
While (CurrentBrushLocation < Len(Brushes)) And (Ref < MaxBrushsAllowed)
    Set TheBrush = New DiagnosisMasterBrush
    TheBrush.BrushLeftRight = ""
    If (EyeContext = "OS") Then
        TheBrush.BrushLeftRight = "L"
    ElseIf (EyeContext = "OD") Then
        TheBrush.BrushLeftRight = "R"
    End If
    TheBrush.BrushDiagnosis = ""
    TheBrush.BrushName = Mid(Brushes, CurrentBrushLocation, 4)
    If (TheBrush.FindBrushbyName > 0) Then
        If (TheBrush.SelectBrush(1)) Then
            CheckBrush = Trim(TheBrush.BrushName)
            CheckAssociated = "0"
            If (FindBrushInArray(CheckBrush, "D", CheckAssociated) < 0) Then
                TheButtons(Ref).BrushArrayIndex = Ref
                TheButtons(Ref).DiagnosisId = DiagnosisId
                TheButtons(Ref).BrushType = "D"
                TheButtons(Ref).AssociatedId = 0
                TheButtons(Ref).LocalSide = TheBrush.BrushLeftRight
                TheButtons(Ref).BrushName = TheBrush.BrushName
                TheButtons(Ref).BrushLingo = TheBrush.BrushLingo
                TheButtons(Ref).BrushImageType = TheBrush.BrushType
                TheButtons(Ref).DefaultColor = TheBrush.BrushDefaultColor
                TheButtons(Ref).DefaultWidth = TheBrush.BrushDefaultWidth
                TheButtons(Ref).RotateOn = TheBrush.BrushRotateOn
                TheButtons(Ref).SizingOn = TheBrush.BrushSizingOn
                TheButtons(Ref).ScalingOn = TheBrush.BrushScalingOn
                TheButtons(Ref).LocateXOn = TheBrush.BrushLocateXOn
                If (TheBrush.BrushLocateXOn > -1) Then
                    TheButtons(Ref).LocateXOn = (TheBrush.BrushLocateXOn * XTwips)
                End If
                TheButtons(Ref).LocateYOn = TheBrush.BrushLocateYOn
                If (TheBrush.BrushLocateYOn > -1) Then
                    TheButtons(Ref).LocateYOn = (TheBrush.BrushLocateYOn * YTwips)
                End If
                TheButtons(Ref).FrameWidth = TheBrush.BrushFrameWidth
                TheButtons(Ref).FrameHeight = TheBrush.BrushFrameHeight
                TheButtons(Ref).LocalSide = TheBrush.BrushLeftRight
                TheButtons(Ref).TestTrigger = TheBrush.BrushTestTrigger
                TheButtons(Ref).TestTriggerText = ""
                TheButtons(Ref).Quantifier = ""
                TheButtons(Ref).LocationSupport = TheBrush.BrushLocationSupport
                Ref = Ref + 1
            End If
        End If
    End If
    CurrentBrushLocation = CurrentBrushLocation + 5
    Set TheBrush = Nothing
Wend

Set RetrieveAssociated = New DiagnosisMasterAssociated
RetrieveAssociated.AssociatedDiagnosis = ""
RetrieveAssociated.PrimaryDiagnosis = Diagnosis
If (RetrieveAssociated.FindAssociatedbyPrimary > 0) Then
    i = 1
    While (RetrieveAssociated.SelectAssociated(i)) And (Ref < MaxBrushsAllowed)
        Set TheBrush = New DiagnosisMasterBrush
        CheckBrush = "9900"
        CheckAssociated = Trim(str(RetrieveAssociated.AssociatedId))
        If (Trim(RetrieveAssociated.AssociatedBrushId) <> "") Then
            TheBrush.BrushLeftRight = ""
            TheBrush.BrushDiagnosis = ""
            TheBrush.BrushName = Trim(RetrieveAssociated.AssociatedBrushId)
            If (TheBrush.FindBrushbyName > 0) Then
                If (TheBrush.SelectBrush(1)) Then
                    CheckBrush = Trim(TheBrush.BrushName)
                    CheckAssociated = Trim(str(RetrieveAssociated.AssociatedId))
                End If
            End If
        End If
        If (FindBrushInArray(CheckBrush, "A", CheckAssociated) < 0) Then
            TheButtons(Ref).BrushArrayIndex = Ref
            TheButtons(Ref).DiagnosisId = DiagnosisId
            TheButtons(Ref).BrushType = "A"
            TheButtons(Ref).AssociatedId = RetrieveAssociated.AssociatedId
            If (Trim(RetrieveAssociated.AssociatedBrushId) = "") Then
                TheButtons(Ref).BrushName = "9900"
            Else
                TheButtons(Ref).BrushName = Trim(RetrieveAssociated.AssociatedBrushId)
            End If
            TheButtons(Ref).BrushLingo = RetrieveAssociated.AssociatedLingo
            TheButtons(Ref).DefaultColor = Mid(RetrieveAssociated.AssociatedDefaultColor, 1, 1)
            TheButtons(Ref).DefaultWidth = Mid(RetrieveAssociated.AssociatedDefaultColor, 2, 1)
            If (CheckBrush <> "9900") Then
                TheButtons(Ref).BrushImageType = TheBrush.BrushType
                TheButtons(Ref).RotateOn = TheBrush.BrushRotateOn
                TheButtons(Ref).SizingOn = TheBrush.BrushSizingOn
                TheButtons(Ref).ScalingOn = TheBrush.BrushScalingOn
                TheButtons(Ref).LocateXOn = TheBrush.BrushLocateXOn
                If (TheBrush.BrushLocateXOn > -1) Then
                    TheButtons(Ref).LocateXOn = (TheBrush.BrushLocateXOn * XTwips)
                End If
                TheButtons(Ref).LocateYOn = TheBrush.BrushLocateYOn
                If (TheBrush.BrushLocateYOn > -1) Then
                    TheButtons(Ref).LocateYOn = (TheBrush.BrushLocateYOn * YTwips)
                End If
                TheButtons(Ref).LocalSide = TheBrush.BrushLeftRight
                TheButtons(Ref).FrameWidth = TheBrush.BrushFrameWidth
                TheButtons(Ref).FrameHeight = TheBrush.BrushFrameHeight
                TheButtons(Ref).TestTrigger = TheBrush.BrushTestTrigger
                TheButtons(Ref).TestTriggerText = ""
                TheButtons(Ref).Quantifier = ""
                TheButtons(Ref).LocationSupport = TheBrush.BrushLocationSupport
            Else
                TheButtons(Ref).BrushImageType = ""
                TheButtons(Ref).RotateOn = False
                TheButtons(Ref).SizingOn = False
                TheButtons(Ref).ScalingOn = False
                TheButtons(Ref).LocateXOn = -1
                TheButtons(Ref).LocateYOn = -1
                TheButtons(Ref).LocalSide = ""
                TheButtons(Ref).FrameWidth = 0
                TheButtons(Ref).FrameHeight = 0
                TheButtons(Ref).TestTrigger = ""
                TheButtons(Ref).TestTriggerText = ""
                TheButtons(Ref).Quantifier = ""
                TheButtons(Ref).LocationSupport = RetrieveAssociated.AssociatedLocationSupport
            End If
            Ref = Ref + 1
        End If
        i = i + 1
        Set TheBrush = Nothing
    Wend
End If
MaxBrushsAvailable = Ref
Set RetrieveAssociated = Nothing

    Exit Sub

lLoadBrushArray_Error:

    LogError "frmDrawNew", "LoadBrushArray", Err, Err.Description
End Sub

Private Function LoadBrushAsOther(Id As String) As Boolean
Dim Ref As Integer
Dim CheckBrush As String
Dim CheckAssociated As String
    On Error GoTo lLoadBrushAsOther_Error

LoadBrushAsOther = False
If (Trim(Id) <> "") Then
    Ref = MaxBrushsAvailable
    Set TheBrush = New DiagnosisMasterBrush
    TheBrush.BrushName = Id
    If (TheBrush.FindBrushbyName > 0) Then
        If (TheBrush.SelectBrush(1)) Then
            CheckBrush = Trim(TheBrush.BrushName)
            CheckAssociated = "0"
            If (FindBrushInArray(CheckBrush, "D", CheckAssociated) < 1) Then
                TheButtons(Ref).BrushArrayIndex = Ref
                TheButtons(Ref).DiagnosisId = DiagnosisId
                TheButtons(Ref).BrushType = "D"
                TheButtons(Ref).AssociatedId = 0
                TheButtons(Ref).LocalSide = TheBrush.BrushLeftRight
                TheButtons(Ref).BrushName = TheBrush.BrushName
                TheButtons(Ref).BrushLingo = TheBrush.BrushLingo
                TheButtons(Ref).BrushImageType = TheBrush.BrushType
                TheButtons(Ref).DefaultColor = TheBrush.BrushDefaultColor
                TheButtons(Ref).DefaultWidth = TheBrush.BrushDefaultWidth
                TheButtons(Ref).RotateOn = TheBrush.BrushRotateOn
                TheButtons(Ref).SizingOn = TheBrush.BrushSizingOn
                TheButtons(Ref).ScalingOn = TheBrush.BrushScalingOn
                TheButtons(Ref).LocateXOn = TheBrush.BrushLocateXOn
                If (TheBrush.BrushLocateXOn > -1) Then
                    TheButtons(Ref).LocateXOn = (TheBrush.BrushLocateXOn * XTwips)
                End If
                TheButtons(Ref).LocateYOn = TheBrush.BrushLocateYOn
                If (TheBrush.BrushLocateYOn > -1) Then
                    TheButtons(Ref).LocateYOn = (TheBrush.BrushLocateYOn * YTwips)
                End If
                TheButtons(Ref).FrameWidth = TheBrush.BrushFrameWidth
                TheButtons(Ref).FrameHeight = TheBrush.BrushFrameHeight
                TheButtons(Ref).LocalSide = TheBrush.BrushLeftRight
                TheButtons(Ref).TestTrigger = TheBrush.BrushTestTrigger
                TheButtons(Ref).TestTriggerText = ""
                TheButtons(Ref).Quantifier = ""
                TheButtons(Ref).LocationSupport = TheBrush.BrushLocationSupport
                Ref = Ref + 1
                LoadBrushAsOther = True
            End If
        End If
    End If
    Set TheBrush = Nothing
    MaxBrushsAvailable = Ref
End If

    Exit Function

lLoadBrushAsOther_Error:

    LogError "frmDrawNew", "LoadBrushAsOther", Err, Err.Description
End Function

Private Sub ReloadForm(TheFile As String)
Dim FileNum As Long
Dim i As Integer, k As Integer
Dim Cnt As Integer
Dim LeftOver As Integer
Dim TheContent As String, Rec As String
Dim AFile As String
Dim BAss As String, BType As String
Dim BrushName As String, TheTag As String
    On Error GoTo lReloadForm_Error

Cnt = 0
ReloadImages = True
If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
    While Not (EOF(FileNum)) And (Cnt < MaxImagesAllowed)
        Line Input #FileNum, Rec
        k = InStrPS(Rec, "=")
        If (k <> 0) Then
            TheTag = Trim(Left(Rec, k - 1))
            TheContent = Trim(Mid(Rec, k + 1, Len(Rec) - k))
            If (TheTag = "DIAGNOSIS") Then
                DiagnosisId = Val(TheContent)
            ElseIf (TheTag = "NAME") Then
                BrushName = TheContent
                Cnt = Cnt + 1
                TheImage(Cnt).Name = BrushName
                TheImage(Cnt).BrushReference.DiagnosisId = DiagnosisId
            ElseIf (TheTag = "TEST") Then
                TheImage(Cnt).BrushReference.TestTriggerText = Trim(TheContent)
            ElseIf (TheTag = "LOC") Then
                TheImage(Cnt).Location = Trim(TheContent)
            ElseIf (TheTag = "ORDER") Then
                TheImage(Cnt).BrushReference.TestTrigger = Trim(TheContent)
            ElseIf (TheTag = "BRUSHTYPE") Then
                BType = TheContent
                TheImage(Cnt).BrushReference.BrushType = BType
            ElseIf (TheTag = "EYE") Then
                TheImage(Cnt).Eye = TheContent
            ElseIf (TheTag = "ASSOCIATE") Then
                BAss = TheContent
                TheImage(Cnt).BrushReference.AssociatedId = Val(BAss)
            ElseIf (TheTag = "QUANTIFIER") Then
                TheImage(Cnt).BrushReference.Quantifier = Trim(TheContent)
            ElseIf (TheTag = "HST") Then
                TheImage(Cnt).HistoryOf = False
                If (TheContent = "T") Then
                    TheImage(Cnt).HistoryOf = True
                End If
            ElseIf (TheTag = "RULEOUT") Then
                TheImage(Cnt).RuleOut = False
                If (TheContent = "T") Then
                    TheImage(Cnt).RuleOut = True
                End If
            ElseIf (TheTag = "CNF") Then
                TheImage(Cnt).CNF = False
                If (TheContent = "T") Then
                    TheImage(Cnt).CNF = True
                End If
            ElseIf (TheTag = "DROPPEDIMAGE") Then
                TheImage(Cnt).DroppedImage = True
                If (TheContent = "F") Then
                    TheImage(Cnt).DroppedImage = False
                End If
            ElseIf (TheTag = "BURN") Then
                TheImage(Cnt).Burn = False
                If (TheContent = "T") Then
                    TheImage(Cnt).Burn = True
                End If
            ElseIf (TheTag = "STATUSPOST") Then
                TheImage(Cnt).StatusPost = False
                If (TheContent = "T") Then
                    TheImage(Cnt).StatusPost = True
                End If
            ElseIf (TheTag = "SIZE") Then
                cmdScaleUp.ToolTipText = TheContent
                TheImage(Cnt).Size = Val(TheContent)
            ElseIf (TheTag = "TOP") Then
                GlobalTop = Val(TheContent)
                TheImage(Cnt).Top = GlobalTop
                TheImage(Cnt).OriginalTop = 0
            ElseIf (TheTag = "LEFT") Then
                GlobalLeft = Val(TheContent)
                TheImage(Cnt).Left = GlobalLeft
                TheImage(Cnt).OriginalLeft = 0
            ElseIf (TheTag = "ROTATE") Then
                TheImage(Cnt).Rotate = Val(TheContent)
                If (TheImage(Cnt).Rotate > 3) Then
                    TheImage(Cnt).Rotate = 0
                End If
            ElseIf (TheTag = "OTHERS") Then
            
            ElseIf (TheTag = "ENDTOP") Then
            
            ElseIf (TheTag = "ENDLEFT") Then
                i = FindBrushInArray(BrushName, BType, BAss)
                If (i = -1) Then
                    Call SetUpLocalAssociated(BAss, False)
                    i = FindBrushInArray(BrushName, BType, BAss)
                    If (i = -1) Then
                        If (BType = "D") Then
                            If (LoadBrushAsOther(BrushName)) Then
                                i = FindBrushInArray(BrushName, BType, BAss)
                            End If
                        End If
                    End If
                End If
                If (i <> -1) Then
                    TheImage(Cnt).BrushReference.BrushArrayIndex = i
                    TheImage(Cnt).BrushReference.FrameHeight = TheButtons(i).FrameHeight
                    TheImage(Cnt).BrushReference.FrameWidth = TheButtons(i).FrameWidth
                    CurrentImageId = Cnt
                    TheImage(Cnt).LabelId = GetNextLabel(Cnt)
                    If TheImage(Cnt).LabelId = 0 Then
                        If IsClickedonDiag(TheImage(Cnt).BrushReference.AssociatedId) = sClickedDiag Then
                            iClickedDiagPos = Cnt
                        End If
                    End If
                    TheImage(Cnt).LineId = TheImage(Cnt).LabelId
                    If (TheImage(Cnt).Top = 0) And (TheImage(Cnt).Left = 0) Then
                        CurrentBrushIndex = 0
                        Call cmdMoreBrushs_Click
                        
                        LeftOver = i - (Int(i / TotalBrushButtons) * TotalBrushButtons)
                        If (i < 10) Then
                            LeftOver = i
                        Else
                            CurrentBrushIndex = (Int(i / TotalBrushButtons) * TotalBrushButtons)
                            Call cmdMoreBrushs_Click
                        End If
                        CurrentBrushSelection = LeftOver
                        TheImage(Cnt).ButtonNumber = LeftOver
                        If (BType = "B") Then
                            AFile = ImageDir + BrushName + ".bmp"
                        ElseIf (BType = "J") Then
                            AFile = ImageDir + BrushName + ".jpg"
                        ElseIf (BType = "G") Then
                            AFile = ImageDir + BrushName + ".gif"
                        ElseIf (BType = "P") Then
                            AFile = ImageDir + BrushName + ".png"
                        ElseIf (BType = "T") Then
                            AFile = ImageDir + BrushName + ".tif"
                        Else
                            AFile = ImageDir + BrushName + ".bmp"
                        End If
                        If (FM.IsFileThere(AFile)) And (InStrPS(AFile, "9900") = 0) Then
                            Call TriggerInitialBrush(TheButtons(i).BrushArrayIndex)
                            DropOnImage = TheImage(Cnt).DroppedImage
                            If (TheImage(Cnt).Left > 0) Then
                                Call PlantImage(BrushName, Cnt, BType)
                                Call FxImage1_MouseUp(TheImage(Cnt).BrushReference.BrushArrayIndex, 0, TheImage(i).Left, TheImage(Cnt).Top)
                                Call FxImage1_MouseUp(TheImage(Cnt).BrushReference.BrushArrayIndex, 0, TheImage(i).Left, TheImage(Cnt).Top)
                            Else
                                Call TriggerInitialBrush(CurrentBrushSelection)
                            End If
                        ElseIf (BrushName = "9999") Or (BrushName = "9998") Or (BrushName = "9900") Then
                            Call TriggerInitialBrush(CurrentBrushSelection)
                            DropOnImage = TheImage(Cnt).DroppedImage
                        Else
                            Call TriggerInitialBrush(CurrentBrushSelection)
                            DropOnImage = TheImage(Cnt).DroppedImage
                        End If
                        DropOnImage = False
                    Else
                        LeftOver = i - (Int(i / TotalBrushButtons) * TotalBrushButtons)
                        If (i < TotalBrushButtons) Then
                            LeftOver = i
                        End If
                        CurrentBrushIndex = (Int(i / TotalBrushButtons) * TotalBrushButtons)
                        Call cmdMoreBrushs_Click
                        CurrentBrushSelection = LeftOver
                        TheImage(Cnt).BrushReference.BrushArrayIndex = i
                        If (TheImage(Cnt).Size > 1) Then
                            MagnifyDontShift = True
                        End If
                        Call TriggerInitialBrush(CurrentBrushSelection)
                        DropOnImage = TheImage(Cnt).DroppedImage
                        cmdScaleUp.ToolTipText = Trim(str(TheImage(Cnt).Size))
                        If (TheImage(Cnt).Left > -1) Then
                            Call FxImage1_MouseUp(TheImage(Cnt).BrushReference.BrushArrayIndex, 0, TheImage(Cnt).Left, TheImage(Cnt).Top)
                            Call FxImage1_MouseUp(TheImage(Cnt).BrushReference.BrushArrayIndex, 0, TheImage(Cnt).Left, TheImage(Cnt).Top)
                            If (TheImage(Cnt).Left > -1) Then
                                Call FxImage1_MouseUp(TheImage(Cnt).BrushReference.BrushArrayIndex, 0, TheImage(Cnt).Left, TheImage(Cnt).Top)
                                Call FxImage1_MouseUp(TheImage(Cnt).BrushReference.BrushArrayIndex, 0, TheImage(Cnt).Left, TheImage(Cnt).Top)
                            End If
                        End If
                        DropOnImage = False
                        MagnifyDontShift = False
                        If (Not (TheImage(Cnt).Burn)) And (TheImage(Cnt).DroppedImage) Then
                            Call BurnTheImage
                            TheImage(Cnt).Burn = True
                        End If
                    End If
                End If
                'TODO: Insert save to pop up button click here
            End If
        End If
    Wend
    FM.CloseFile CLng(FileNum)
    
    CurrentBrushIndex = 0
    Call cmdMoreBrushs_Click
    
    cmdRotate.Visible = False
    cmdImageDraw.Visible = False
    cmdScaleUp.Visible = False
    cmdScaleDown.Visible = False
End If
PrevImageId = CurrentImageId
CurrentImageId = 0
ReloadImages = False

    Exit Sub

lReloadForm_Error:

    LogError "frmDrawNew", "ReloadForm", Err, Err.Description
End Sub

Private Sub DropLine(Counter As Integer)
Dim k As Integer
Dim XOffset As Single, YOffset As Single
Dim AHeight As Single, IHeight As Single
Dim AWidth As Single, IWidth As Single
Dim lblLine As Line
    On Error GoTo lDropLine_Error

If (Counter > 0) Then
    AHeight = 240
    AWidth = 2250
    IHeight = 240
    IWidth = 240
    k = TheImage(Counter).LabelId
    If (k = 1) Then
        Set lblLine = Line1
    ElseIf (k = 2) Then
        Set lblLine = Line2
    ElseIf (k = 3) Then
        Set lblLine = Line3
    ElseIf (k = 4) Then
        Set lblLine = Line4
    ElseIf (k = 5) Then
        Set lblLine = Line5
    ElseIf (k = 6) Then
        Set lblLine = Line6
    ElseIf (k = 7) Then
        Set lblLine = Line7
    ElseIf (k = 8) Then
        Set lblLine = Line8
    ElseIf (k = 9) Then
        Set lblLine = Line9
    ElseIf (k = 10) Then
        Set lblLine = Line10
    ElseIf (k = 11) Then
        Set lblLine = Line11
    ElseIf (k = 12) Then
        Set lblLine = Line12
    ElseIf (k = 13) Then
        Set lblLine = Line13
    ElseIf (k = 14) Then
        Set lblLine = Line14
    ElseIf (k = 15) Then
        Set lblLine = Line15
    ElseIf (k = 16) Then
        Set lblLine = Line16
    ElseIf (k = 17) Then
        Set lblLine = Line17
    ElseIf (k = 18) Then
        Set lblLine = Line18
    Else
        Exit Sub
    End If
    XOffset = 0
    YOffset = 0
'    If (TheImage(Counter).Size > 1) And (ScalingDirection = 1) Then
'        XOffset = -1 * TheImage(Counter).Size * 100
'        YOffset = -1 * TheImage(Counter).Size * 100
'    ElseIf (TheImage(Counter).Size >= 1) And (ScalingDirection = -1) Then
'        XOffset = TheImage(Counter).Size * 100
'        YOffset = TheImage(Counter).Size * 100
'        If (TheImage(Counter).Size = 1) Then
'            XOffset = 400
'            YOffset = 400
'        End If
'    End If
    TheImage(Counter).LineId = k
    TheImage(Counter).LineName = "Label" + Trim(str(k))
    If (TheImage(Counter).Left > 0) And (TheImage(Counter).Top > 0) Then
        With lblLine
            .BorderColor = &HC000C0
            .BorderWidth = 2
            If (k < 10) Then
                .x2 = FxImage1.Width
                .y2 = (AHeight * ((k * 2) - 1)) + 2
                If (TheImage(Counter).Size < 2) Then
                    If (ReloadImages) Then
                        XOffset = XOffset + 150
                        YOffset = YOffset + 150
                    Else
                        XOffset = XOffset - 50
                        YOffset = YOffset - 50
                    End If
                End If
            Else
                .x2 = 2
                .y2 = (AHeight * (((k - 9) * 2) - 1)) + 2
                If (TheImage(Counter).Size < 2) Then
                    XOffset = XOffset - 150
                    YOffset = YOffset - 100
                End If
            End If
            If (TheImage(Counter).DroppedImage) Then
                If Not (ReloadImages) Then
                    .x1 = (TheImage(Counter).Left - FxImage1.Left + (IWidth / 2)) + XOffset
                    .y1 = (TheImage(Counter).Top - FxImage1.Top + (IHeight / 2)) + YOffset
                    If (.y1 < 1) Then
                        .y1 = 10
                    End If
                Else
                    XOffset = XOffset * 2
                    If (TheImage(Counter).BrushReference.FrameHeight > 0) Then
                        If (TheImage(Counter).BrushReference.FrameHeight <> 99) Then
                            .x1 = (TheImage(Counter).Left - (FxImage1.Left + (IWidth / 2))) + XOffset
                            .y1 = (TheImage(Counter).Top - (FxImage1.Top + (IHeight / 2))) + YOffset
                        Else
                            .x1 = (TheImage(Counter).Left - (FxImage1.Left + (IWidth / 2))) - XOffset
                            .y1 = (TheImage(Counter).Top - (FxImage1.Top + (IHeight / 2))) - YOffset
                        End If
                        If (.y1 < 1) Then
                            .y1 = 10
                        End If
                    Else
                        .x1 = (TheImage(Counter).Left - FxImage1.Left + 100) + XOffset
                        .y1 = (TheImage(Counter).Top - FxImage1.Top + 100) + YOffset
                        If (.y1 < 1) Then
                            .y1 = 10
                        End If
                    End If
                End If
            Else
                If (InStrPS(DrawFileName, "I-") = 0) Then
                    .x1 = TheImage(Counter).Left - 125
                    .y1 = TheImage(Counter).Top - (FxImage1.Top - 100)
                    If (.y1 < 1) Then
                        .y1 = 10
                    End If
                Else
                    .x1 = TheImage(Counter).Left
                    .y1 = TheImage(Counter).Top
                End If
            End If
            .Visible = StartLinesOn
            .ZOrder 0
            .Refresh
        End With
    End If
End If

    Exit Sub

lDropLine_Error:

    LogError "frmDrawNew", "DropLine", Err, Err.Description
End Sub

Private Sub DropLabel(TheText As String, Counter As Integer)
Dim k As Integer
Dim ALabel As Label
    On Error GoTo lDropLabel_Error

If (Counter > -1) And (Trim(TheText) <> "") Then
    k = TheImage(Counter).LabelId
    If (k < 1) Then
        k = GetNextLabel(Counter)
    End If
    If (k = 1) Then
        Set ALabel = lblImage1
    ElseIf (k = 2) Then
        Set ALabel = lblImage2
    ElseIf (k = 3) Then
        Set ALabel = lblImage3
    ElseIf (k = 4) Then
        Set ALabel = lblImage4
    ElseIf (k = 5) Then
        Set ALabel = lblImage5
    ElseIf (k = 6) Then
        Set ALabel = lblImage6
    ElseIf (k = 7) Then
        Set ALabel = lblImage7
    ElseIf (k = 8) Then
        Set ALabel = lblImage8
    ElseIf (k = 9) Then
        Set ALabel = lblImage9
    ElseIf (k = 10) Then
        Set ALabel = lblImage10
    ElseIf (k = 11) Then
        Set ALabel = lblImage11
    ElseIf (k = 12) Then
        Set ALabel = lblImage12
    ElseIf (k = 13) Then
        Set ALabel = lblImage13
    ElseIf (k = 14) Then
        Set ALabel = lblImage14
    ElseIf (k = 15) Then
        Set ALabel = lblImage15
    ElseIf (k = 16) Then
        Set ALabel = lblImage16
    ElseIf (k = 17) Then
        Set ALabel = lblImage17
    ElseIf (k = 18) Then
        Set ALabel = lblImage18
    Else
        Exit Sub
    End If
    TheImage(Counter).LabelId = k
    TheImage(Counter).LabelName = "lblImage" + Trim(str(k))
    With ALabel
        .AutoSize = True
'        .Alignment = 0
        .WordWrap = True
        .Caption = Trim(TheText)
        If (Len(Trim(TheText)) > 23) Then
            .Caption = Trim(Left(TheText, 23))
        End If
        If (TheImage(Counter).RuleOut) Then
            .Caption = "RO - " + Trim(.Caption)
        End If
        If (TheImage(Counter).CNF) Then
            .Caption = "NF - " + Trim(.Caption)
        End If
        If (TheImage(Counter).HistoryOf) Then
            .Caption = "HO - " + Trim(.Caption)
        End If
        If (TheImage(Counter).StatusPost) Then
            .Caption = "SP - " + Trim(.Caption)
        End If
        If (TheImage(Counter).Size > 0) Then
            .Caption = .Caption + "+" + Trim(str(TheImage(Counter).Size))
        End If
        If (Trim(TheImage(Counter).BrushReference.Quantifier) <> "") Then
            .Caption = Trim(TheImage(Counter).BrushReference.Quantifier) + " " + Trim(.Caption)
        End If
'        If (Trim(TheImage(Counter).Location) <> "") Then
'            .Caption = .Caption + " " + Trim(TheImage(Counter).Location)
'        End If
'        If Not (DropOnImage) And (k < 10) Then
'            .Left = FxImage1.Left + FxImage1.Width + 50
'        End If
         Dim CheckOtherDiagnosis As Boolean
        CheckOtherDiagnosis = CheckOtherDiagnosisExists(k)
        If CheckOtherDiagnosis Then
           If (Len(.Caption) >= 20) Then
               .Caption = Trim(Left(TheText, 20)) + "++"
           End If
        Else
           If (Len(.Caption) >= 55) Then
               .Caption = Trim(Left(TheText, 23)) + "++"
           End If
        End If
        .Enabled = True
        .Visible = True
        .ZOrder 0
        .Refresh
        TheImage(Counter).LabelTop = .Top
        TheImage(Counter).LabelLeft = .Left
    End With
    Call DropLine(Counter)
End If

    Exit Sub

lDropLabel_Error:

    LogError "frmDrawNew", "DropLabel", Err, Err.Description
End Sub

Private Sub DropImage(X As Single, Y As Single, LabelFlag As Boolean)
Dim mFactor As Double
Dim iShift As Single
Dim MXOffset As Single
Dim MYOffset As Single
Dim LabelText As String
Dim BrushName As String
Dim BrushType As String
    On Error GoTo lDropImage_Error
    
    If Trim$(cmdBrush(CurrentBrushSelection).Tag) <> "" Then
        BrushName = Mid$(cmdBrush(CurrentBrushSelection).Tag, 2, Len(cmdBrush(CurrentBrushSelection).Tag) - 1)
        BrushType = Left$(cmdBrush(CurrentBrushSelection).ToolTipText, 1)
        LabelText = Trim$(cmdBrush(CurrentBrushSelection).Text)
    End If

If (X > 0) And (Y > 0) Then
    SelectedX = X
    SelectedY = Y
    If (LocPropOn) And Not (ReloadImages) Then
        Call SetLocation(CurrentImageId, True)
    End If
    iShift = 0
    MXOffset = 0
    MYOffset = 0
    mFactor = 1
    If (cmdScaleUp.Visible) Then
        If (Val(cmdScaleUp.ToolTipText) >= 1) Then
            mFactor = Val(Trim(cmdScaleUp.ToolTipText))
            If (ScalingDirection = -1) Then
                MXOffset = ScalingDirection * ((TheImage(CurrentImageId).BrushReference.FrameWidth / 2))
                MYOffset = ScalingDirection * ((TheImage(CurrentImageId).BrushReference.FrameHeight / 2))
                If (Val(cmdScaleUp.ToolTipText) = 1) Then
                    MXOffset = 0
                    MYOffset = 0
                End If
            End If
        End If
    End If
    GlobalHeight = StandardImageHeight
    GlobalWidth = StandardImageWidth
    If (TheImage(CurrentImageId).BrushReference.FrameWidth > 0) And _
       (TheImage(CurrentImageId).BrushReference.FrameHeight > 0) Then
        If ((TheImage(CurrentImageId).BrushReference.LocateXOn > -1) And (TheImage(CurrentImageId).BrushReference.LocateYOn > -1)) Or _
           Not (TheImage(CurrentImageId).BrushReference.ScalingOn) Then
            GlobalHeight = (TheImage(CurrentImageId).BrushReference.FrameHeight)
            GlobalWidth = (TheImage(CurrentImageId).BrushReference.FrameWidth)
        Else
            GlobalHeight = (TheImage(CurrentImageId).BrushReference.FrameHeight * mFactor)
            GlobalWidth = (TheImage(CurrentImageId).BrushReference.FrameWidth * mFactor)
            iShift = 50 * (mFactor - 1)
            If (MagnifyDontShift) Then
                iShift = 0
            End If
        End If
    End If
' X and Y are considered the left and top; need to adjust to be viewed as the center
    If (GlobalHeight > 300) And (GlobalWidth > 300) Then
        SelectedX = X + (FxImage1.Left - 195)
        SelectedY = Y + (FxImage1.Top - 195)
    Else
        If (GlobalHeight > 250) And (GlobalWidth > 250) Then
            SelectedX = X + (FxImage1.Left - 145)
            SelectedY = Y + (FxImage1.Top - 145)
        ElseIf (GlobalHeight > 99) And (GlobalWidth > 99) Then
            SelectedX = X + (FxImage1.Left - 95)
            SelectedY = Y + (FxImage1.Top - 95)
        Else
            SelectedX = X + (FxImage1.Left - 45)
            SelectedY = Y + (FxImage1.Top - 45)
        End If
    End If
    CurrentEndXLocation = SelectedX
    CurrentEndYLocation = SelectedY
    If (ReloadImages) Then
        CurrentEndXLocation = X - iShift
        CurrentEndYLocation = Y - iShift
    End If
    If ((TheImage(CurrentImageId).BrushReference.LocateXOn > -1) And (TheImage(CurrentImageId).BrushReference.LocateYOn > -1)) Then
        CurrentEndXLocation = TheImage(CurrentImageId).BrushReference.LocateXOn + FxImage1.Left + 500
        CurrentEndYLocation = TheImage(CurrentImageId).BrushReference.LocateYOn + FxImage1.Top - 400
        GlobalHeight = (TheImage(CurrentImageId).BrushReference.FrameHeight) * YTwips
        GlobalWidth = (TheImage(CurrentImageId).BrushReference.FrameWidth) * XTwips
    End If
    GlobalTop = CurrentEndYLocation
    GlobalLeft = CurrentEndXLocation
    If (MYOffset <> 0) Or (ScalingDirection = 1) Then
        If (ScalingDirection = 1) Or ((ScalingDirection = -1) And (mFactor = 1)) Then
            iShift = 0
        End If
        GlobalTop = (CurrentEndYLocation + (iShift / 1.45)) - MYOffset
        GlobalLeft = (CurrentEndXLocation + (iShift / 1.45)) - MXOffset
    End If
End If
Call PlantImage(BrushName, CurrentImageId, BrushType)
TheImage(CurrentImageId).RuleOut = TheImage(CurrentImageId).RuleOut
TheImage(CurrentImageId).CNF = TheImage(CurrentImageId).CNF
TheImage(CurrentImageId).StatusPost = TheImage(CurrentImageId).StatusPost
If (LabelFlag) Then
    Call DropLabel(LabelText, CurrentImageId)
End If

    Exit Sub

lDropImage_Error:

    LogError "frmDrawNew", "DropImage", Err, Err.Description
End Sub

Private Sub Form_Activate()
    On Error GoTo lForm_Activate_Error

KillOn = False
EraseOn = False
EraseWaitForClick = True
OtherImagesOn = False
ReloadReplaceScanOn = False
If Not (lblRTest1.Visible) Then
    lblRTest1.Caption = "Label1"
End If
If Not (lblRTest2.Visible) Then
    lblRTest2.Caption = "Label2"
End If
If Not (lblRTest3.Visible) Then
    lblRTest3.Caption = "Label3"
End If
If Not (lblRTest4.Visible) Then
    lblRTest4.Caption = "Label4"
End If
If Not (lblLTest1.Visible) Then
    lblLTest1.Caption = "Label1"
End If
If Not (lblLTest2.Visible) Then
    lblLTest2.Caption = "Label2"
End If
If Not (lblLTest3.Visible) Then
    lblLTest3.Caption = "Label3"
End If
If Not (lblLTest4.Visible) Then
    lblLTest4.Caption = "Label4"
End If

    DoEvents
    If AllowIt Then
        'Call ReloadForm(StoreResults)
        AllowIt = False
    End If
    DoEvents

    
    If iClickedDiagPos > 0 Then
        UnshownDiagMenu
    End If

    Exit Sub

lForm_Activate_Error:

    LogError "frmDrawNew", "Form_Activate", Err, Err.Description
End Sub

Private Sub Form_Click()
    On Error GoTo lForm_Click_Error

    If CurrentImageId > 0 Then
        CurrentImageId = 0
        CurrentEraseImageId = 0
        DrawingOn = False
        DropOnImage = False
        DropLineOn = False
        cmdRotate.Visible = False
        cmdImageDraw.Visible = False
        cmdScaleUp.Visible = False
        cmdScaleDown.Visible = False
    End If

    Exit Sub

lForm_Click_Error:

    LogError "frmDrawNew", "Form_Click", Err, Err.Description
End Sub

Private Sub Form_Load()
AllowIt = True
End Sub

Private Sub FxImage1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo lFxImage1_MouseMove_Error

If (lblErase.Visible) Then
    EraseOn = True
End If
If Not (DropOnImage) And ((DrawingOn) Or (EraseOn)) Then
    Call MouseMove(FxImage1, X, Y)
ElseIf (LocPropOn) Then
    Call SetLocationBasic(X, Y)
    If (lblErase.Visible) Then
        Call MouseMove(FxImage1, X, Y)
    End If
Else
    Call MouseUp(FxImage1)
End If

    Exit Sub

lFxImage1_MouseMove_Error:

    LogError "frmDrawNew", "FxImage1_MouseMove", Err, Err.Description
End Sub

Private Sub FxImage1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo lFxImage1_MouseUp_Error

If (DropOnImage) Then
    If (EraseOn) Or (lblErase.Visible) Then
        EraseOn = True
        EraseWaitForClick = True
    End If
    If (CurrentImageId > 0) Then
        TheImage(CurrentImageId).OriginalLeft = 0
        TheImage(CurrentImageId).OriginalTop = 0
        If (TheImage(CurrentImageId).BrushReference.LocateXOn > 0) Then
            X = TheImage(CurrentImageId).BrushReference.LocateXOn
        End If
        If (TheImage(CurrentImageId).BrushReference.LocateYOn > 0) Then
            Y = TheImage(CurrentImageId).BrushReference.LocateYOn
        End If
        Call DropImage(X, Y, True)
    Else
        Call MouseUp(FxImage1)
    End If
Else
    If (EraseOn) Or (lblErase.Visible) Then
        EraseWaitForClick = True
    End If
    Call MouseUp(FxImage1)
End If
DrawingOn = False

    Exit Sub

lFxImage1_MouseUp_Error:

    LogError "frmDrawNew", "FxImage1_MouseUp", Err, Err.Description
End Sub

Private Sub FxImage1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo lFxImage1_MouseDown_Error

If (DropOnImage) Then
    CurrentEndXLocation = -1
    CurrentEndYLocation = -1
    If (EraseOn) Or (lblErase.Visible) Then
        EraseOn = True
        EraseWaitForClick = False
    End If
Else
    DrawingOn = True
    If (EraseOn) Or (lblErase.Visible) Then
        EraseOn = True
        EraseWaitForClick = False
    End If
    Call MouseDown(FxImage1, X, Y)
End If

    Exit Sub

lFxImage1_MouseDown_Error:

    LogError "frmDrawNew", "FxImage1_MouseDown", Err, Err.Description
End Sub

Private Sub lblErase_Click()
    On Error GoTo llblErase_Click_Error

If (CurrentImageId = 0) Then
    Call cmdFreeErase_Click
Else
    lblErase.Visible = False
    EraseOn = False
    EraseWaitForClick = True
    FxImage1.MousePointer = MP_Cross
    FxImage1.CaptureNow
    Call SaveNewDrawFileName(DrawFileName, False)
End If

    Exit Sub

llblErase_Click_Error:

    LogError "frmDrawNew", "lblErase_Click", Err, Err.Description
End Sub

Private Sub lblImage1_Click()
Dim q As Integer
    On Error GoTo llblImage1_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(1)
Call BrushOptions(q)

    Exit Sub

llblImage1_Click_Error:

    LogError "frmDrawNew", "lblImage1_Click", Err, Err.Description
End Sub

Private Sub imgImage1_Click()
    On Error GoTo limgImage1_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 1
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage1_Click_Error:

    LogError "frmDrawNew", "imgImage1_Click", Err, Err.Description
End Sub

Private Sub lblImage2_Click()
Dim q As Integer
    On Error GoTo llblImage2_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(2)
Call BrushOptions(q)

    Exit Sub

llblImage2_Click_Error:

    LogError "frmDrawNew", "lblImage2_Click", Err, Err.Description
End Sub

Private Sub imgImage2_Click()
    On Error GoTo limgImage2_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 2
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage2_Click_Error:

    LogError "frmDrawNew", "imgImage2_Click", Err, Err.Description
End Sub

Private Sub lblImage3_Click()
Dim q As Integer
    On Error GoTo llblImage3_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(3)
Call BrushOptions(q)

    Exit Sub

llblImage3_Click_Error:

    LogError "frmDrawNew", "lblImage3_Click", Err, Err.Description
End Sub

Private Sub imgImage3_Click()
    On Error GoTo limgImage3_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 3
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage3_Click_Error:

    LogError "frmDrawNew", "imgImage3_Click", Err, Err.Description
End Sub

Private Sub lblImage4_Click()
Dim q As Integer
    On Error GoTo llblImage4_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(4)
Call BrushOptions(q)

    Exit Sub

llblImage4_Click_Error:

    LogError "frmDrawNew", "lblImage4_Click", Err, Err.Description
End Sub

Private Sub imgImage4_Click()
    On Error GoTo limgImage4_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 4
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage4_Click_Error:

    LogError "frmDrawNew", "imgImage4_Click", Err, Err.Description
End Sub

Private Sub lblImage5_Click()
Dim q As Integer
    On Error GoTo llblImage5_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(5)
Call BrushOptions(q)

    Exit Sub

llblImage5_Click_Error:

    LogError "frmDrawNew", "lblImage5_Click", Err, Err.Description
End Sub

Private Sub imgImage5_Click()
    On Error GoTo limgImage5_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 5
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage5_Click_Error:

    LogError "frmDrawNew", "imgImage5_Click", Err, Err.Description
End Sub

Private Sub lblImage6_Click()
Dim q As Integer
    On Error GoTo llblImage6_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(6)
Call BrushOptions(q)

    Exit Sub

llblImage6_Click_Error:

    LogError "frmDrawNew", "lblImage6_Click", Err, Err.Description
End Sub

Private Sub imgImage6_Click()
    On Error GoTo limgImage6_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 6
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage6_Click_Error:

    LogError "frmDrawNew", "imgImage6_Click", Err, Err.Description
End Sub

Private Sub lblImage7_Click()
Dim q As Integer
    On Error GoTo llblImage7_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(7)
Call BrushOptions(q)

    Exit Sub

llblImage7_Click_Error:

    LogError "frmDrawNew", "lblImage7_Click", Err, Err.Description
End Sub

Private Sub imgImage7_Click()
    On Error GoTo limgImage7_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 7
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage7_Click_Error:

    LogError "frmDrawNew", "imgImage7_Click", Err, Err.Description
End Sub

Private Sub lblImage8_Click()
Dim q As Integer
    On Error GoTo llblImage8_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(8)
Call BrushOptions(q)

    Exit Sub

llblImage8_Click_Error:

    LogError "frmDrawNew", "lblImage8_Click", Err, Err.Description
End Sub

Private Sub imgImage8_Click()
    On Error GoTo limgImage8_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 8
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage8_Click_Error:

    LogError "frmDrawNew", "imgImage8_Click", Err, Err.Description
End Sub

Private Sub lblImage9_Click()
Dim q As Integer
    On Error GoTo llblImage9_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(9)
Call BrushOptions(q)

    Exit Sub

llblImage9_Click_Error:

    LogError "frmDrawNew", "lblImage9_Click", Err, Err.Description
End Sub

Private Sub imgImage9_Click()
    On Error GoTo limgImage9_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 9
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage9_Click_Error:

    LogError "frmDrawNew", "imgImage9_Click", Err, Err.Description
End Sub

Private Sub lblImage10_Click()
Dim q As Integer
    On Error GoTo llblImage10_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(10)
Call BrushOptions(q)

    Exit Sub

llblImage10_Click_Error:

    LogError "frmDrawNew", "lblImage10_Click", Err, Err.Description
End Sub

Private Sub imgImage10_Click()
    On Error GoTo limgImage10_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 10
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage10_Click_Error:

    LogError "frmDrawNew", "imgImage10_Click", Err, Err.Description
End Sub

Private Sub lblImage11_Click()
Dim q As Integer
    On Error GoTo llblImage11_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(11)
Call BrushOptions(q)

    Exit Sub

llblImage11_Click_Error:

    LogError "frmDrawNew", "lblImage11_Click", Err, Err.Description
End Sub

Private Sub imgImage11_Click()
    On Error GoTo limgImage11_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 11
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage11_Click_Error:

    LogError "frmDrawNew", "imgImage11_Click", Err, Err.Description
End Sub

Private Sub lblImage12_Click()
Dim q As Integer
    On Error GoTo llblImage12_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(12)
Call BrushOptions(q)

    Exit Sub

llblImage12_Click_Error:

    LogError "frmDrawNew", "lblImage12_Click", Err, Err.Description
End Sub

Private Sub imgImage12_Click()
    On Error GoTo limgImage12_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 12
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage12_Click_Error:

    LogError "frmDrawNew", "imgImage12_Click", Err, Err.Description
End Sub

Private Sub lblImage13_Click()
Dim q As Integer
    On Error GoTo llblImage13_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(13)
Call BrushOptions(q)

    Exit Sub

llblImage13_Click_Error:

    LogError "frmDrawNew", "lblImage13_Click", Err, Err.Description
End Sub

Private Sub imgImage13_Click()
    On Error GoTo limgImage13_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 13
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage13_Click_Error:

    LogError "frmDrawNew", "imgImage13_Click", Err, Err.Description
End Sub

Private Sub lblImage14_Click()
Dim q As Integer
    On Error GoTo llblImage14_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(14)
Call BrushOptions(q)

    Exit Sub

llblImage14_Click_Error:

    LogError "frmDrawNew", "lblImage14_Click", Err, Err.Description
End Sub

Private Sub imgImage14_Click()
    On Error GoTo limgImage14_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 14
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage14_Click_Error:

    LogError "frmDrawNew", "imgImage14_Click", Err, Err.Description
End Sub

Private Sub lblImage15_Click()
Dim q As Integer
    On Error GoTo llblImage15_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(15)
Call BrushOptions(q)

    Exit Sub

llblImage15_Click_Error:

    LogError "frmDrawNew", "lblImage15_Click", Err, Err.Description
End Sub

Private Sub imgImage15_Click()
    On Error GoTo limgImage15_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 15
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage15_Click_Error:

    LogError "frmDrawNew", "imgImage15_Click", Err, Err.Description
End Sub

Private Sub lblImage16_Click()
Dim q As Integer
    On Error GoTo llblImage16_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(16)
Call BrushOptions(q)

    Exit Sub

llblImage16_Click_Error:

    LogError "frmDrawNew", "lblImage16_Click", Err, Err.Description
End Sub

Private Sub imgImage16_Click()
    On Error GoTo limgImage16_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 16
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage16_Click_Error:

    LogError "frmDrawNew", "imgImage16_Click", Err, Err.Description
End Sub

Private Sub lblImage17_Click()
Dim q As Integer
    On Error GoTo llblImage17_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(17)
Call BrushOptions(q)

    Exit Sub

llblImage17_Click_Error:

    LogError "frmDrawNew", "lblImage17_Click", Err, Err.Description
End Sub

Private Sub imgImage17_Click()
    On Error GoTo limgImage17_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 17
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage17_Click_Error:

    LogError "frmDrawNew", "imgImage17_Click", Err, Err.Description
End Sub

Private Sub lblImage18_Click()
Dim q As Integer
    On Error GoTo llblImage18_Click_Error

If (CurrentImageId > 0) Then
    Call BurnTheImage
End If
q = FindALabel(18)
Call BrushOptions(q)

    Exit Sub

llblImage18_Click_Error:

    LogError "frmDrawNew", "lblImage18_Click", Err, Err.Description
End Sub

Private Sub imgImage18_Click()
    On Error GoTo limgImage18_Click_Error

If Not (PlacementInProgress) Then
    CurrentImageId = 18
    If (DropOnImage) Then
        Call SetButton(False)
    End If
End If

    Exit Sub

limgImage18_Click_Error:

    LogError "frmDrawNew", "imgImage18_Click", Err, Err.Description
End Sub

Private Sub SetButton(SelectNew As Boolean)
Dim BrushId As Integer
    On Error GoTo lSetButton_Error

BrushId = TheImage(CurrentImageId).ButtonNumber
Call ActivateBrush(BrushId, SelectNew)

    Exit Sub

lSetButton_Error:

    LogError "frmDrawNew", "SetButton", Err, Err.Description
End Sub

Private Function SelectImageId() As Integer
Dim i As Integer
    On Error GoTo lSelectImageId_Error

SelectImageId = 0
For i = 1 To MaxImagesAllowed
    If (Trim(TheImage(i).Name) = "") Or (Trim(TheImage(i).Name) = "-") Then
        SelectImageId = i
        Exit For
    End If
Next i

    Exit Function

lSelectImageId_Error:

    LogError "frmDrawNew", "SelectImageId", Err, Err.Description
End Function

Private Function TotalImagesSelected() As Integer
Dim i As Integer
    On Error GoTo lTotalImagesSelected_Error

TotalImagesSelected = 0
For i = 1 To MaxImagesAllowed
    If (Trim(TheImage(i).Name) <> "") And (Trim(TheImage(i).Name) <> "-") Then
        TotalImagesSelected = TotalImagesSelected + 1
    End If
Next i

    Exit Function

lTotalImagesSelected_Error:

    LogError "frmDrawNew", "TotalImagesSelected", Err, Err.Description
End Function

Private Function IsBrushAlreadyUsed(BrushId As Integer) As Integer
Dim i As Integer
Dim TheRef As Integer
    On Error GoTo lIsBrushAlreadyUsed_Error

IsBrushAlreadyUsed = 0
If 0 <= BrushId < TotalBrushButtons Then
    If Trim$(cmdBrush(BrushId).ToolTipText) <> "" Then
        TheRef = Val(Trim$(Mid$(cmdBrush(BrushId).ToolTipText, 2, Len(cmdBrush(BrushId).ToolTipText) - 1)))
    End If
Else
    Exit Function
End If
For i = 1 To MaxImagesAllowed
    If (TheImage(i).BrushReference.BrushArrayIndex = TheRef) Then
        IsBrushAlreadyUsed = i
        Exit For
    End If
Next i

    Exit Function

lIsBrushAlreadyUsed_Error:

    LogError "frmDrawNew", "IsBrushAlreadyUsed", Err, Err.Description
End Function

Private Function FindBrushInArray(BrushName As String, BrushType As String, AssId As String) As Integer
Dim i As Integer
Dim AssName As Long
    On Error GoTo lFindBrushInArray_Error

AssName = Val(AssId)
FindBrushInArray = -1
For i = 0 To MaxBrushsAllowed
    If (Trim(TheButtons(i).BrushName) = BrushName) And (TheButtons(i).BrushType = BrushType) And (TheButtons(i).AssociatedId = AssName) Then
        FindBrushInArray = i
        Exit For
    End If
Next i

    Exit Function

lFindBrushInArray_Error:

    LogError "frmDrawNew", "FindBrushInArray", Err, Err.Description
End Function

Private Sub InitializeImages(Ref As Integer, Init As Boolean)
Dim i As Integer
Dim RefId As Integer
Dim StartLocation As Integer
Dim EndLocation As Integer
    On Error GoTo lInitializeImages_Error

If (Ref < 1) Then
    RefId = 0
    StartLocation = 1
    EndLocation = MaxImagesAllowed
Else
    RefId = TheImage(Ref).LineId
    StartLocation = Ref
    EndLocation = Ref
End If
For i = StartLocation To EndLocation
    TheImage(i).ButtonNumber = 0
    TheImage(i).BrushId = 0
    If (Ref = 0) Then
        TheImage(i).ImageName = 0
        TheImage(i).LabelId = 0
        TheImage(i).LineId = 0
        TheImage(i).IsImageSet = False
        TheImage(i).DroppedImage = False
        TheImage(i).Name = ""
    Else
        TheImage(i).ImageName = 0
        TheImage(i).IsImageSet = False
        TheImage(i).DroppedImage = False
        TheImage(i).Name = "-"
    End If
    TheImage(i).Eye = ""
    TheImage(i).Burn = False
    TheImage(i).StatusPost = False
    TheImage(i).HistoryOf = False
    TheImage(i).RuleOut = False
    TheImage(i).CNF = False
    TheImage(i).Location = ""
    TheImage(i).LocalSide = ""
    TheImage(i).Size = 0
    TheImage(i).Rotate = ROTATE_None
    TheImage(i).Top = 0
    TheImage(i).Left = 0
    TheImage(i).OriginalLeft = 0
    TheImage(i).OriginalTop = 0
    TheImage(i).LabelId = 0
    TheImage(i).LabelName = ""
    TheImage(i).LabelTop = 0
    TheImage(i).LabelLeft = 0
    TheImage(i).LineId = 0
    TheImage(i).LineName = ""
    TheImage(i).BrushReference.BrushArrayIndex = -1
    TheImage(i).BrushReference.DiagnosisId = 0
    TheImage(i).BrushReference.BrushType = ""
    TheImage(i).BrushReference.BrushName = ""
    TheImage(i).BrushReference.AssociatedId = 0
    TheImage(i).BrushReference.BrushLingo = ""
    TheImage(i).BrushReference.BrushImageType = ""
    TheImage(i).BrushReference.DefaultColor = ""
    TheImage(i).BrushReference.DefaultWidth = ""
    TheImage(i).BrushReference.RotateOn = False
    TheImage(i).BrushReference.SizingOn = False
    TheImage(i).BrushReference.ScalingOn = False
    TheImage(i).BrushReference.LocateXOn = -1
    TheImage(i).BrushReference.LocateYOn = -1
    TheImage(i).BrushReference.FrameWidth = 0
    TheImage(i).BrushReference.FrameHeight = 0
    TheImage(i).BrushReference.TestTrigger = ""
    TheImage(i).BrushReference.TestTriggerText = ""
    TheImage(i).BrushReference.LocationSupport = ""
    TheImage(i).BrushReference.Quantifier = ""
Next i

If (Ref = 0) Then
    If (Init) Then
        Call ClearButtons
    End If
    LineA1.Visible = False
    LineA2.Visible = False
    LineA3.Visible = False
    LineA4.Visible = False
    LineA5.Visible = False
    LineA6.Visible = False
    LineA7.Visible = False
    LineA8.Visible = False
    LineA9.Visible = False
    LineA10.Visible = False
    LineA11.Visible = False
    LineA12.Visible = False
    LineA13.Visible = False
    LineA14.Visible = False
    LineA15.Visible = False
    LineA16.Visible = False
    LineA17.Visible = False
    LineA18.Visible = False
    lblImageA1.Visible = False
    lblImageA2.Visible = False
    lblImageA3.Visible = False
    lblImageA4.Visible = False
    lblImageA5.Visible = False
    lblImageA6.Visible = False
    lblImageA7.Visible = False
    lblImageA8.Visible = False
    lblImageA9.Visible = False
    lblImageA10.Visible = False
    lblImageA11.Visible = False
    lblImageA12.Visible = False
    lblImageA13.Visible = False
    lblImageA14.Visible = False
    lblImageA15.Visible = False
    lblImageA16.Visible = False
    lblImageA17.Visible = False
    lblImageA18.Visible = False
    imgImageA1.Visible = False
    imgImageA2.Visible = False
    imgImageA3.Visible = False
    imgImageA4.Visible = False
    imgImageA5.Visible = False
    imgImageA6.Visible = False
    imgImageA7.Visible = False
    imgImageA8.Visible = False
    imgImageA9.Visible = False
    imgImageA10.Visible = False
    imgImageA11.Visible = False
    imgImageA12.Visible = False
    imgImageA13.Visible = False
    imgImageA14.Visible = False
    imgImageA15.Visible = False
    imgImageA16.Visible = False
    imgImageA17.Visible = False
    imgImageA18.Visible = False
End If
If (Ref = 0) Or (RefId = 1) Then
    Line1.Visible = False
    lblImage1.Visible = False
    lblImage1.Caption = "Label1"
    imgImage1.Visible = False
End If
If (Ref = 0) Or (RefId = 2) Then
    Line2.Visible = False
    lblImage2.Visible = False
    lblImage2.Caption = "Label1"
    imgImage2.Visible = False
End If
If (Ref = 0) Or (RefId = 3) Then
    Line3.Visible = False
    lblImage3.Visible = False
    lblImage3.Caption = "Label1"
    imgImage3.Visible = False
End If
If (Ref = 0) Or (RefId = 4) Then
    Line4.Visible = False
    lblImage4.Visible = False
    lblImage4.Caption = "Label1"
    imgImage4.Visible = False
End If
If (Ref = 0) Or (RefId = 5) Then
    Line5.Visible = False
    lblImage5.Visible = False
    lblImage5.Caption = "Label1"
    imgImage5.Visible = False
End If
If (Ref = 0) Or (RefId = 6) Then
    Line6.Visible = False
    lblImage6.Visible = False
    lblImage6.Caption = "Label1"
    imgImage6.Visible = False
End If
If (Ref = 0) Or (RefId = 7) Then
    Line7.Visible = False
    lblImage7.Visible = False
    lblImage7.Caption = "Label1"
    imgImage7.Visible = False
End If
If (Ref = 0) Or (RefId = 8) Then
    Line8.Visible = False
    lblImage8.Visible = False
    lblImage8.Caption = "Label1"
    imgImage8.Visible = False
End If
If (Ref = 0) Or (RefId = 9) Then
    Line9.Visible = False
    lblImage9.Visible = False
    lblImage9.Caption = "Label1"
    imgImage9.Visible = False
End If
If (Ref = 0) Or (RefId = 10) Then
    Line10.Visible = False
    lblImage10.Visible = False
    lblImage10.Caption = "Label1"
    imgImage10.Visible = False
End If
If (Ref = 0) Or (RefId = 11) Then
    Line11.Visible = False
    lblImage11.Visible = False
    lblImage11.Caption = "Label1"
    imgImage11.Visible = False
End If
If (Ref = 0) Or (RefId = 12) Then
    Line12.Visible = False
    lblImage12.Visible = False
    lblImage12.Caption = "Label1"
    imgImage12.Visible = False
End If
If (Ref = 0) Or (RefId = 13) Then
    Line13.Visible = False
    lblImage13.Visible = False
    lblImage13.Caption = "Label1"
    imgImage13.Visible = False
End If
If (Ref = 0) Or (RefId = 14) Then
    Line14.Visible = False
    lblImage14.Visible = False
    lblImage14.Caption = "Label1"
    imgImage14.Visible = False
End If
If (Ref = 0) Or (RefId = 15) Then
    Line15.Visible = False
    lblImage15.Visible = False
    lblImage15.Caption = "Label1"
    imgImage15.Visible = False
End If
If (Ref = 0) Or (RefId = 16) Then
    Line16.Visible = False
    lblImage16.Visible = False
    lblImage16.Caption = "Label1"
    imgImage16.Visible = False
End If
If (Ref = 0) Or (RefId = 17) Then
    Line17.Visible = False
    lblImage17.Visible = False
    lblImage17.Caption = "Label1"
    imgImage17.Visible = False
End If
If (Ref = 0) Or (RefId = 18) Then
    Line18.Visible = False
    lblImage18.Visible = False
    lblImage18.Caption = "Label1"
    imgImage18.Visible = False
End If

    Exit Sub

lInitializeImages_Error:

    LogError "frmDrawNew", "InitializeImages", Err, Err.Description
End Sub

Private Function VerifyDraw(DiagId As Long, Images As String) As Boolean
Dim RetrievePrimary As DiagnosisMasterPrimary
    On Error GoTo lVerifyDraw_Error

Images = ""
VerifyDraw = False
Set RetrievePrimary = New DiagnosisMasterPrimary
RetrievePrimary.PrimaryId = DiagId
If (RetrievePrimary.RetrievePrimary) Then
    DiagnosisName = Trim(RetrievePrimary.PrimaryNextLevelDiagnosis) + "-" + Trim(RetrievePrimary.PrimaryName)
    If (Trim(RetrievePrimary.PrimaryLingo) <> "") Then
        DiagnosisName = Trim(RetrievePrimary.PrimaryNextLevelDiagnosis) + "-" + Trim(RetrievePrimary.PrimaryLingo)
    End If
    If (Trim(RetrievePrimary.PrimaryImage1) <> "") Then
        Images = Trim(RetrievePrimary.PrimaryImage1)
        If (Len(Images) >= 2) Then
            If (Len(Images) = 2) Then
                Images = Images + "9900"
            End If
            VerifyDraw = True
        Else
            Images = ""
        End If
    End If
End If
Set RetrievePrimary = Nothing

    Exit Function

lVerifyDraw_Error:

    LogError "frmDrawNew", "VerifyDraw", Err, Err.Description
End Function

Private Function TriggerInitialBrush(Idx As Integer) As Boolean
    On Error GoTo lTriggerInitialBrush_Error

TriggerInitialBrush = True
If 0 <= Idx < TotalBrushButtons Then
    cmdBrush_Click Idx
End If

    Exit Function

lTriggerInitialBrush_Error:

    LogError "frmDrawNew", "TriggerInitialBrush", Err, Err.Description
End Function

Private Sub MouseDown(AImage As FXImage, X As Single, Y As Single)
Dim x1 As Single
Dim y1 As Single
Dim EColor(20) As Long
Dim XFactor As Single
Dim YFactor As Single
Dim XOffset As Single
Dim YOffset As Single
    On Error GoTo lMouseDown_Error

lblLocView.Visible = False
If Not (DropOnImage) Then
    PrevX = 0
    PrevY = 0
    XFactor = 0
    YFactor = 0
    XOffset = 0
    YOffset = 0
    If (InStrPS(DrawFileName, "I-") = 0) Then
        XFactor = -10
        YFactor = (FxImage1.Top - 100) / YTwips
        XOffset = -10
        YOffset = -20
    End If
    x1 = (X / XTwips) - XFactor
    y1 = (Y / YTwips) + YFactor
    If (EraseOn) Then
        If Not (EraseWaitForClick) Then
            AImage.MousePointer = MP_Wand
            DoEvents
            EColor(1) = ImgOrgBase.GetImagePixel(x1, y1)
            EColor(2) = ImgOrgBase.GetImagePixel(x1, y1 + 1)
            EColor(3) = ImgOrgBase.GetImagePixel(x1, y1 + 2)
            EColor(4) = ImgOrgBase.GetImagePixel(x1, y1 + 3)
            EColor(5) = ImgOrgBase.GetImagePixel(x1, y1 + 4)
            EColor(6) = ImgOrgBase.GetImagePixel(x1, y1 - 1)
            EColor(7) = ImgOrgBase.GetImagePixel(x1, y1 - 2)
            EColor(8) = ImgOrgBase.GetImagePixel(x1, y1 - 3)
            EColor(9) = ImgOrgBase.GetImagePixel(x1, y1 - 4)
            EColor(10) = ImgOrgBase.GetImagePixel(x1 + 1, y1)
            EColor(11) = ImgOrgBase.GetImagePixel(x1 + 2, y1)
            EColor(12) = ImgOrgBase.GetImagePixel(x1 + 3, y1)
            EColor(13) = ImgOrgBase.GetImagePixel(x1 + 4, y1)
            EColor(14) = ImgOrgBase.GetImagePixel(x1 - 1, y1)
            EColor(15) = ImgOrgBase.GetImagePixel(x1 - 2, y1)
            EColor(16) = ImgOrgBase.GetImagePixel(x1 - 3, y1)
            EColor(17) = ImgOrgBase.GetImagePixel(x1 - 4, y1)
            AImage.SetImagePixel x1 + XOffset, y1 + YOffset, EColor(1)
            AImage.SetImagePixel x1 + XOffset, (y1 + 1) + YOffset, EColor(2)
            AImage.SetImagePixel x1 + XOffset, (y1 + 2) + YOffset, EColor(3)
            AImage.SetImagePixel x1 + XOffset, (y1 + 3) + YOffset, EColor(4)
            AImage.SetImagePixel x1 + XOffset, (y1 + 4) + YOffset, EColor(5)
            AImage.SetImagePixel x1 + XOffset, (y1 - 1) + YOffset, EColor(6)
            AImage.SetImagePixel x1 + XOffset, (y1 - 2) + YOffset, EColor(7)
            AImage.SetImagePixel x1 + XOffset, (y1 - 3) + YOffset, EColor(8)
            AImage.SetImagePixel x1 + XOffset, (y1 - 4) + YOffset, EColor(9)
            AImage.SetImagePixel (x1 + 1) + XOffset, y1 + YOffset, EColor(10)
            AImage.SetImagePixel (x1 + 2) + XOffset, y1 + YOffset, EColor(11)
            AImage.SetImagePixel (x1 + 3) + XOffset, y1 + YOffset, EColor(12)
            AImage.SetImagePixel (x1 + 4) + XOffset, y1 + YOffset, EColor(13)
            AImage.SetImagePixel (x1 - 1) + XOffset, y1 + YOffset, EColor(14)
            AImage.SetImagePixel (x1 - 2) + XOffset, y1 + YOffset, EColor(15)
            AImage.SetImagePixel (x1 - 3) + XOffset, y1 + YOffset, EColor(16)
            AImage.SetImagePixel (x1 - 4) + XOffset, y1 + YOffset, EColor(17)
            DoEvents
        End If
    Else
        AImage.MousePointer = MP_Pencil1
        AImage.SetImagePixel x1 + XOffset, y1 + YOffset, PencilColor
        If (CurrentImageId > 0) And (DropLineOn) Then
            TheImage(CurrentImageId).Left = (x1 * XTwips)
            TheImage(CurrentImageId).Top = (y1 * YTwips)
            If (TheImage(CurrentImageId).Left < ((FxImage1.Width / 2))) Then
                TheImage(CurrentImageId).Eye = "OD"
                If (TheImage(CurrentImageId).LabelId < 10) Then
                    Call ResetLabel(CurrentImageId)
                End If
            Else
                TheImage(CurrentImageId).Eye = "OS"
                Call ResetLabel(CurrentImageId)
            End If
            Call DropLine(CurrentImageId)
            DropLineOn = False
            If (LocPropOn) Then
                Call SetLocation(CurrentImageId, True)
            End If
            TheImage(CurrentImageId).Burn = True
        End If
    End If
End If

    Exit Sub

lMouseDown_Error:

    LogError "frmDrawNew", "MouseDown", Err, Err.Description
End Sub

Private Sub MouseMove(AImage As FXImage, X As Single, Y As Single)
Dim x1 As Single
Dim y1 As Single
Dim EColor(20) As Long
Dim XFactor As Single
Dim YFactor As Single
Dim XOffset As Single
Dim YOffset As Single
    On Error GoTo lMouseMove_Error

lblLocView.Visible = False
If Not (DropOnImage) Or (EraseOn) Then
    XOffset = 0
    YOffset = 0
    XFactor = 0
    YFactor = 0
    If (InStrPS(DrawFileName, "I-") = 0) Then
        XFactor = -10
        YFactor = (FxImage1.Top - 100) / YTwips
        XOffset = -10
        YOffset = -20
    End If
    x1 = (X / XTwips) - XFactor
    y1 = (Y / YTwips) + YFactor
    If (EraseOn) Then
        If Not (EraseWaitForClick) Then
            AImage.MousePointer = MP_Wand
            DoEvents
            EColor(1) = ImgOrgBase.GetImagePixel(x1, y1)
            EColor(2) = ImgOrgBase.GetImagePixel(x1, y1 + 1)
            EColor(3) = ImgOrgBase.GetImagePixel(x1, y1 + 2)
            EColor(4) = ImgOrgBase.GetImagePixel(x1, y1 + 3)
            EColor(5) = ImgOrgBase.GetImagePixel(x1, y1 + 4)
            EColor(6) = ImgOrgBase.GetImagePixel(x1, y1 - 1)
            EColor(7) = ImgOrgBase.GetImagePixel(x1, y1 - 2)
            EColor(8) = ImgOrgBase.GetImagePixel(x1, y1 - 3)
            EColor(9) = ImgOrgBase.GetImagePixel(x1, y1 - 4)
            EColor(10) = ImgOrgBase.GetImagePixel(x1 + 1, y1)
            EColor(11) = ImgOrgBase.GetImagePixel(x1 + 2, y1)
            EColor(12) = ImgOrgBase.GetImagePixel(x1 + 3, y1)
            EColor(13) = ImgOrgBase.GetImagePixel(x1 + 4, y1)
            EColor(14) = ImgOrgBase.GetImagePixel(x1 - 1, y1)
            EColor(15) = ImgOrgBase.GetImagePixel(x1 - 2, y1)
            EColor(16) = ImgOrgBase.GetImagePixel(x1 - 3, y1)
            EColor(17) = ImgOrgBase.GetImagePixel(x1 - 4, y1)
            AImage.SetImagePixel x1 + XOffset, y1 + YOffset, EColor(1)
            AImage.SetImagePixel x1 + XOffset, (y1 + 1) + YOffset, EColor(2)
            AImage.SetImagePixel x1 + XOffset, (y1 + 2) + YOffset, EColor(3)
            AImage.SetImagePixel x1 + XOffset, (y1 + 3) + YOffset, EColor(4)
            AImage.SetImagePixel x1 + XOffset, (y1 + 4) + YOffset, EColor(5)
            AImage.SetImagePixel x1 + XOffset, (y1 - 1) + YOffset, EColor(6)
            AImage.SetImagePixel x1 + XOffset, (y1 - 2) + YOffset, EColor(7)
            AImage.SetImagePixel x1 + XOffset, (y1 - 3) + YOffset, EColor(8)
            AImage.SetImagePixel x1 + XOffset, (y1 - 4) + YOffset, EColor(9)
            AImage.SetImagePixel (x1 + 1) + XOffset, y1 + YOffset, EColor(10)
            AImage.SetImagePixel (x1 + 2) + XOffset, y1 + YOffset, EColor(11)
            AImage.SetImagePixel (x1 + 3) + XOffset, y1 + YOffset, EColor(12)
            AImage.SetImagePixel (x1 + 4) + XOffset, y1 + YOffset, EColor(13)
            AImage.SetImagePixel (x1 - 1) + XOffset, y1 + YOffset, EColor(14)
            AImage.SetImagePixel (x1 - 2) + XOffset, y1 + YOffset, EColor(15)
            AImage.SetImagePixel (x1 - 3) + XOffset, y1 + YOffset, EColor(16)
            AImage.SetImagePixel (x1 - 4) + XOffset, y1 + YOffset, EColor(17)
            DoEvents
        End If
    Else
        If Not (DropOnImage) Then
            AImage.MousePointer = MP_Pencil1
            AImage.SetImagePixel x1 + XOffset, y1 + YOffset, PencilColor
            If (PrevX > 0) And (PrevY > 0) Then
                AImage.DrawLine PrevX + XOffset, PrevY + YOffset, x1 + XOffset, y1 + YOffset, PencilColor
            End If
        End If
    End If
End If
PrevX = x1
PrevY = y1

    Exit Sub

lMouseMove_Error:

    LogError "frmDrawNew", "MouseMove", Err, Err.Description
End Sub

Private Sub MouseUp(AImage As FXImage)
    On Error GoTo lMouseUp_Error

lblLocView.Visible = False
AImage.MousePointer = MP_Cross
If (Trim(ReplaceBase) = "") Then
    AImage.Refresh
    AImage.Paint
End If
DoEvents

    Exit Sub

lMouseUp_Error:

    LogError "frmDrawNew", "MouseUp", Err, Err.Description
End Sub

Private Function SetImageArray(Ref As Integer, BName As String) As Boolean
    On Error GoTo lSetImageArray_Error

SetImageArray = False
If (Ref > 0) And (Ref <= MaxImagesAllowed) And (Trim(BName) <> "") Then

    TheImage(Ref).BrushId = CurrentBrushSelection
    TheImage(Ref).Name = BName
    TheImage(Ref).BrushReference.BrushArrayIndex = LocalBrushIndex
    If (LocalBrushIndex >= 0) Then
        TheImage(Ref).BrushReference.DiagnosisId = TheButtons(LocalBrushIndex).DiagnosisId
        TheImage(Ref).BrushReference.BrushType = TheButtons(LocalBrushIndex).BrushType
        TheImage(Ref).BrushReference.BrushName = TheButtons(LocalBrushIndex).BrushName
        TheImage(Ref).BrushReference.AssociatedId = TheButtons(LocalBrushIndex).AssociatedId
        TheImage(Ref).BrushReference.BrushLingo = TheButtons(LocalBrushIndex).BrushLingo
        TheImage(Ref).BrushReference.BrushImageType = TheButtons(LocalBrushIndex).BrushImageType
        TheImage(Ref).BrushReference.DefaultColor = TheButtons(LocalBrushIndex).DefaultColor
        TheImage(Ref).BrushReference.DefaultWidth = TheButtons(LocalBrushIndex).DefaultWidth
        TheImage(Ref).BrushReference.RotateOn = TheButtons(LocalBrushIndex).RotateOn
        TheImage(Ref).BrushReference.SizingOn = TheButtons(LocalBrushIndex).SizingOn
        TheImage(Ref).BrushReference.ScalingOn = TheButtons(LocalBrushIndex).ScalingOn
        TheImage(Ref).BrushReference.LocateXOn = TheButtons(LocalBrushIndex).LocateXOn
        TheImage(Ref).BrushReference.LocateYOn = TheButtons(LocalBrushIndex).LocateYOn
        TheImage(Ref).BrushReference.FrameWidth = TheButtons(LocalBrushIndex).FrameWidth
        TheImage(Ref).BrushReference.FrameHeight = TheButtons(LocalBrushIndex).FrameHeight
        TheImage(Ref).BrushReference.TestTrigger = TheButtons(LocalBrushIndex).TestTrigger
        TheImage(Ref).BrushReference.TestTriggerText = ""
        TheImage(Ref).BrushReference.Quantifier = ""
        TheImage(Ref).BrushReference.LocationSupport = TheButtons(LocalBrushIndex).LocationSupport
    End If
    SetImageArray = True
End If

    Exit Function

lSetImageArray_Error:

    LogError "frmDrawNew", "SetImageArray", Err, Err.Description
End Function

Private Function LoadOtherDiags(TheText As String) As Boolean
Dim i As Integer
Dim p As Integer
Dim ATemp As String
Dim DisplayText As String
Dim RetrieveDiag As DiagnosisMasterPrimary
    On Error GoTo lLoadOtherDiags_Error

Set RetrieveDiag = New DiagnosisMasterPrimary
LoadOtherDiags = False
i = 1
p = 1
OtherDiags = TheText
lstOthers.Clear
lstOthers.AddItem "Close Other Diagnosis"
If (Trim(TheText) = "") Then
    TheText = Chr(1)
Else
    If (Left(TheText, 1) <> "*") Then
        TheText = "*" + TheText
    End If
End If
RetrieveDiag.PrimarySystem = ""
RetrieveDiag.PrimaryName = ""
RetrieveDiag.PrimaryDiagnosis = ""
RetrieveDiag.PrimaryNextLevelDiagnosis = ""
RetrieveDiag.PrimaryName = ""
RetrieveDiag.PrimaryLingo = TheText
RetrieveDiag.PrimaryRank = 0
RetrieveDiag.PrimaryLevel = 0
RetrieveDiag.PrimaryDiscipline = "OPHT"
RetrieveDiag.PrimaryBilling = False
TotalOtherDiags = RetrieveDiag.FindRealPrimarybyDiagnosis
If (TotalOtherDiags > 0) Then
    p = CurrentOtherIndex
    While (RetrieveDiag.SelectPrimary(p)) And (i < 17)
        DisplayText = Space(75)
        Mid(DisplayText, 1, 10) = Trim(RetrieveDiag.PrimaryNextLevelDiagnosis)
        Mid(DisplayText, 11, 1) = "-"
        Call frmHome.GetSystemByLetter(RetrieveDiag.PrimarySystem, ATemp)
        Mid(DisplayText, 12, 3) = ATemp
        If Not (RetrieveDiag.PrimaryBilling) Then
            Mid(DisplayText, 16, 4) = NotBillTag
        ElseIf (RetrieveDiag.PrimaryUnspecifiedOn) Then
            Mid(DisplayText, 16, 4) = UnspecTag
        End If
        Mid(DisplayText, 57, Len(RetrieveDiag.PrimaryNextLevelDiagnosis)) = RetrieveDiag.PrimaryNextLevelDiagnosis
        If (Len(Trim(RetrieveDiag.PrimaryLingo)) > 54) Then
            Mid(DisplayText, 20, 54) = Left(Trim(RetrieveDiag.PrimaryLingo), 55)
        Else
            Mid(DisplayText, 20, Len(Trim(RetrieveDiag.PrimaryLingo))) = Trim(RetrieveDiag.PrimaryLingo)
        End If
        DisplayText = DisplayText + Trim(str(RetrieveDiag.PrimaryId))
        lstOthers.AddItem DisplayText
        i = i + 1
        p = p + 1
    Wend
End If
If (lstOthers.ListCount > 1) Then
    For i = 1 To MaxImagesAllowed
        If (TheImage(i).ButtonNumber > -1) And ((TheImage(i).IsImageSet) Or (TheImage(i).DroppedImage)) Then
            Call SetImageVisible(i, False)
            DoEvents
        End If
    Next i
    FxImage1.Visible = False
    LoadOtherDiags = True
    cmdOther.Visible = True
    lstOthers.Visible = True
    cmdPrevOther.Visible = True
    cmdNextOther.Visible = True
    CurrentOtherIndex = p
End If
Set RetrieveDiag = Nothing

    Exit Function

lLoadOtherDiags_Error:

    LogError "frmDrawNew", "LoadOtherDiags", Err, Err.Description
End Function

Private Function GetStartingValue(TheText As String) As Boolean
    On Error GoTo lGetStartingValue_Error

TheText = ""
GetStartingValue = True
frmAlphaPad.NumPad_Field = "Starting with Diagnosis Alpha"
frmAlphaPad.NumPad_Result = ""
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.Show 1
If Not (frmAlphaPad.NumPad_Quit) Then
    TheText = Trim(frmAlphaPad.NumPad_Result)
Else
    TheText = ""
End If

    Exit Function

lGetStartingValue_Error:

    LogError "frmDrawNew", "GetStartingValue", Err, Err.Description
End Function

Private Sub lblLoc_Click()
Dim zz As String
    On Error GoTo llblLoc_Click_Error

If (PrevImageId > 0) Or (CurrentImageId > 0) Then
    zz = ""
    If (Left(BaseImage, 2) = "02") Then
        zz = "02"
    End If
    If (frmPalette.LoadPalette(2, zz)) Then
        If (PrevImageId < 1) Then
            PrevImageId = CurrentImageId
        End If
        If (CurrentImageId > 0) Then
            Call BurnTheImage
        End If
        frmPalette.Show 1
        If (Trim(frmPalette.ZoneDescription) <> "") Then
            lblLoc.Caption = Trim(frmPalette.ZoneDescription)
            TheImage(PrevImageId).Location = LocRef + Left(lblLoc.Caption, 2) + LocRefEnd
            If (PrevImageId = 1) Then
                If (Len(lblImage1.Caption) > 5) Then
                    lblImage1.Caption = Left(lblImage1.Caption, Len(lblImage1.Caption) - 5)
                End If
            ElseIf (PrevImageId = 2) Then
                If (Len(lblImage2.Caption) > 5) Then
                    lblImage2.Caption = Left(lblImage2.Caption, Len(lblImage2.Caption) - 5)
                End If
            ElseIf (PrevImageId = 3) Then
                If (Len(lblImage3.Caption) > 5) Then
                    lblImage3.Caption = Left(lblImage3.Caption, Len(lblImage3.Caption) - 5)
                End If
            ElseIf (PrevImageId = 4) Then
                If (Len(lblImage4.Caption) > 5) Then
                    lblImage4.Caption = Left(lblImage4.Caption, Len(lblImage4.Caption) - 5)
                End If
            ElseIf (PrevImageId = 5) Then
                If (Len(lblImage5.Caption) > 5) Then
                    lblImage5.Caption = Left(lblImage5.Caption, Len(lblImage5.Caption) - 5)
                End If
            ElseIf (PrevImageId = 6) Then
                If (Len(lblImage6.Caption) > 5) Then
                    lblImage6.Caption = Left(lblImage6.Caption, Len(lblImage6.Caption) - 5)
                End If
            ElseIf (PrevImageId = 7) Then
                If (Len(lblImage7.Caption) > 5) Then
                    lblImage7.Caption = Left(lblImage7.Caption, Len(lblImage7.Caption) - 5)
                End If
            ElseIf (PrevImageId = 8) Then
                If (Len(lblImage8.Caption) > 5) Then
                    lblImage8.Caption = Left(lblImage8.Caption, Len(lblImage8.Caption) - 5)
                End If
            ElseIf (PrevImageId = 9) Then
                If (Len(lblImage9.Caption) > 5) Then
                    lblImage9.Caption = Left(lblImage9.Caption, Len(lblImage9.Caption) - 5)
                End If
            ElseIf (PrevImageId = 10) Then
                If (Len(lblImage10.Caption) > 5) Then
                    lblImage10.Caption = Left(lblImage10.Caption, Len(lblImage10.Caption) - 5)
                End If
            ElseIf (PrevImageId = 11) Then
                If (Len(lblImage11.Caption) > 5) Then
                    lblImage11.Caption = Left(lblImage11.Caption, Len(lblImage11.Caption) - 5)
                End If
            ElseIf (PrevImageId = 12) Then
                If (Len(lblImage12.Caption) > 5) Then
                    lblImage12.Caption = Left(lblImage12.Caption, Len(lblImage12.Caption) - 5)
                End If
            ElseIf (PrevImageId = 13) Then
                If (Len(lblImage13.Caption) > 5) Then
                    lblImage13.Caption = Left(lblImage13.Caption, Len(lblImage13.Caption) - 5)
                End If
            ElseIf (PrevImageId = 14) Then
                If (Len(lblImage14.Caption) > 5) Then
                    lblImage14.Caption = Left(lblImage14.Caption, Len(lblImage14.Caption) - 5)
                End If
            ElseIf (PrevImageId = 15) Then
                If (Len(lblImage15.Caption) > 5) Then
                    lblImage15.Caption = Left(lblImage15.Caption, Len(lblImage15.Caption) - 5)
                End If
            ElseIf (PrevImageId = 16) Then
                If (Len(lblImage16.Caption) > 5) Then
                    lblImage16.Caption = Left(lblImage16.Caption, Len(lblImage16.Caption) - 5)
                End If
            ElseIf (PrevImageId = 17) Then
                If (Len(lblImage17.Caption) > 5) Then
                    lblImage17.Caption = Left(lblImage17.Caption, Len(lblImage17.Caption) - 5)
                End If
            ElseIf (PrevImageId = 18) Then
                If (Len(lblImage18.Caption) > 5) Then
                    lblImage18.Caption = Left(lblImage18.Caption, Len(lblImage18.Caption) - 5)
                End If
            End If
        End If
    End If
End If

    Exit Sub

llblLoc_Click_Error:

    LogError "frmDrawNew", "lblLoc_Click", Err, Err.Description
End Sub

Private Sub lstOthers_Click()
Dim i As Integer
Dim TrigBrush As Boolean
    On Error GoTo llstOthers_Click_Error

If (lstOthers.ListIndex >= 0) Then
    CurrentOtherIndex = 1
    OtherDiags = ""
    cmdPrevOther.Visible = False
    cmdNextOther.Visible = False
    lstOthers.Visible = False
    cmdOther.Visible = True
    FxImage1.Visible = True
    cmdDone.Enabled = True
    cmdNotes.Enabled = True
    cmdClear.Enabled = True
    cmdImageDraw.Enabled = True
    cmdOther.Text = "Add Finding"
    If (lstOthers.ListIndex > 0) Then
        i = MaxBrushsAvailable
        Call SetUpLocalAssociated(Trim(Mid(lstOthers.List(lstOthers.ListIndex), 76, Len(lstOthers.List(lstOthers.ListIndex)) - 75)), False)
        TrigBrush = False
        If (MaxBrushsAvailable > i) Then
            TrigBrush = True
        End If
    End If
    For i = 1 To MaxImagesAllowed
        If (TheImage(i).ButtonNumber > -1) And ((TheImage(i).IsImageSet) Or (TheImage(i).DroppedImage)) Then
            SetImageVisible i, True
        End If
    Next i
    If (TrigBrush) Then
        CurrentBrushIndex = MaxBrushsAvailable - 1
        Call cmdMoreBrushs_Click
        Call cmdBrush_Click(0)
    End If
End If

    Exit Sub

llstOthers_Click_Error:

    LogError "frmDrawNew", "lstOthers_Click", Err, Err.Description
End Sub

Private Sub CloseOtherSelection(TheItem As Integer)
Dim i As Integer
    On Error GoTo lCloseOtherSelection_Error

CurrentImageId = SelectImageId
If (CurrentImageId > 0) And (TheItem > 0) Then
    CurrentBrushSelection = 99
    LocalBrushIndex = -1 * (TheItem)
    CurrentEndXLocation = 0
    CurrentEndYLocation = 0
    Call SetImageArray(CurrentImageId, Trim(Mid(lstOthers.List(lstOthers.ListIndex), 10, 50)))
    TheImage(CurrentImageId).ButtonNumber = -1 * (lstOthers.ListIndex)
End If
CurrentOtherIndex = 1
OtherDiags = ""
cmdPrevOther.Visible = False
cmdNextOther.Visible = False
lstOthers.Visible = False
cmdOther.Visible = True
FxImage1.Visible = True
cmdDone.Enabled = True
cmdNotes.Enabled = True
cmdClear.Enabled = True
cmdImageDraw.Enabled = True
cmdOther.Text = "Add Finding"
For i = 1 To MaxImagesAllowed
    If (TheImage(i).ButtonNumber > -1) And ((TheImage(i).IsImageSet) Or (TheImage(i).DroppedImage)) Then
        Call SetImageVisible(i, True)
    End If
Next i

    Exit Sub

lCloseOtherSelection_Error:

    LogError "frmDrawNew", "CloseOtherSelection", Err, Err.Description
End Sub

Private Function SetUpLocalAssociated(sDiagId As String, fIsAssociated As Boolean) As Boolean
Dim MyDiag As Long
Dim Ref As Integer
Dim oPrimary As New DiagnosisMasterPrimary
    On Error GoTo lSetUpLocalAssociated_Error

    If sDiagId <> "" And sDiagId <> "0" Then
        If fIsAssociated Then
            MyDiag = GetPrimaryIdFromAssociated(Val(sDiagId))
            If (MyDiag < 1) Then
                Exit Function
            End If
        Else
            MyDiag = Val(sDiagId)
        End If
        oPrimary.PrimaryId = MyDiag
        If oPrimary.RetrievePrimary Then
            Ref = MaxBrushsAvailable
            If (Ref > 100) Then
                Ref = 100
            End If
            With TheButtons(Ref)
                .BrushArrayIndex = Ref
                .DiagnosisId = DiagnosisId
                .BrushType = "O"
                .AssociatedId = oPrimary.PrimaryId
                .BrushName = "9900"
                .BrushLingo = Trim(oPrimary.PrimaryLingo)
                If (Trim(oPrimary.PrimaryLingo) = "") Then
                    .BrushLingo = Trim(oPrimary.PrimaryName)
                End If
                .BrushImageType = ""
                .RotateOn = False
                .SizingOn = False
                .ScalingOn = False
                .LocateXOn = -1
                .LocateYOn = -1
                .FrameWidth = 0
                .FrameHeight = 0
                .TestTrigger = ""
                .TestTriggerText = ""
                .Quantifier = ""
            End With
            SetAssociatedColorAndLocation oPrimary.PrimaryNextLevelDiagnosis, Ref
            IsAlternateBrushReady oPrimary.PrimaryNextLevelDiagnosis, Ref
            LoadNewBrush
            SetUpLocalAssociated = True
            MaxBrushsAvailable = MaxBrushsAvailable + 1
            If Not (cmdMoreBrushs.Visible) Then
                If (MaxBrushsAvailable > MaxBrushVisible + 1) Then
                    cmdMoreBrushs.Visible = True
                End If
            End If
        End If
    End If
    Set oPrimary = Nothing
    
    Exit Function

lSetUpLocalAssociated_Error:

    LogError "frmDrawNew", "SetUpLocalAssociated", Err, Err.Description
End Function

Private Function SetAssociatedColorAndLocation(sDiag As String, iBrushIndex As Integer) As Boolean
Dim oAssociated As New DiagnosisMasterAssociated

    On Error GoTo lSetAssociatedColorAndLocation_Error

    With TheButtons(iBrushIndex)
        .DefaultColor = "N"
        .DefaultWidth = "1"
        oAssociated.AssociatedDiagnosis = sDiag
        If oAssociated.FindAssociated > 0 Then
            If oAssociated.SelectAssociated(1) Then
                .LocationSupport = oAssociated.AssociatedLocationSupport
                If Trim$(oAssociated.AssociatedDefaultColor) <> "" Then
                    .DefaultColor = Left$(oAssociated.AssociatedDefaultColor, 1)
                    .DefaultWidth = Mid$(oAssociated.AssociatedDefaultColor, 2, 1)
                End If
            End If
        End If
    End With
    Set oAssociated = Nothing

    Exit Function

lSetAssociatedColorAndLocation_Error:

    LogError "frmDrawNew", "SetAssociatedColorAndLocation", Err, Err.Description
End Function

Private Function GetPrimaryIdFromAssociated(iAssociatedId As Long) As Long
Dim oAssociated As New DiagnosisMasterAssociated
Dim oPrimary As New DiagnosisMasterPrimary
    On Error GoTo lGetPrimaryIdFromAssociated_Error

    oAssociated.AssociatedId = iAssociatedId
    If oAssociated.RetrieveAssociated Then
        With oPrimary
            .PrimaryNextLevelDiagnosis = Trim$(oAssociated.AssociatedDiagnosis)
            If .FindRealPrimarybyDiagnosis Then
                If .SelectPrimary(1) Then
                    GetPrimaryIdFromAssociated = .PrimaryId
                End If
            End If
        End With
    End If
    Set oPrimary = Nothing
    Set oAssociated = Nothing

    Exit Function

lGetPrimaryIdFromAssociated_Error:

    LogError "frmDrawNew", "GetPrimaryIdFromAssociated", Err, Err.Description
End Function

Private Sub LoadAlternateBrushs(TheFile As String)
Dim k As Integer
Dim Rec As String
Dim FileNum As Long
Dim BAss As String
Dim BType As String
Dim BrushName As String
Dim TheTag As String
Dim TheContent As String
    On Error GoTo lLoadAlternateBrushs_Error

If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
    While Not (EOF(FileNum))
        Line Input #FileNum, Rec
        k = InStrPS(Rec, "=")
        If (k <> 0) Then
            TheTag = Trim(Left(Rec, k - 1))
            TheContent = Trim(Mid(Rec, k + 1, Len(Rec) - k))
            If (TheTag = "DIAGNOSIS") Then
            
            ElseIf (TheTag = "NAME") Then
                BrushName = TheContent
            ElseIf (TheTag = "BRUSHTYPE") Then
                BType = TheContent
            ElseIf (TheTag = "ASSOCIATE") Then
                BAss = TheContent
            ElseIf (TheTag = "RULEOUT") Then
            
            ElseIf (TheTag = "CNF") Then
            
            ElseIf (TheTag = "STATUSPOST") Then
            
            ElseIf (TheTag = "SIZE") Then
            
            ElseIf (TheTag = "TOP") Then
            
            ElseIf (TheTag = "LEFT") Then
            
            ElseIf (TheTag = "ROTATE") Then
            
            ElseIf (TheTag = "DIAGNOSIS") Then
            
            ElseIf (TheTag = "OTHERS") Then
            
            ElseIf (TheTag = "ENDTOP") Then
            
            ElseIf (TheTag = "ENDLEFT") Then
                If (FindBrushInArray(BrushName, BType, BAss) < 0) Then
                    If (BType = "A") Then
                        Call SetUpLocalAssociated(BAss, True)
                    Else
                        Call SetUpLocalAssociated(BAss, False)
                    End If
                End If
            End If
        End If
    Wend
    FM.CloseFile CLng(FileNum)
End If

    Exit Sub

lLoadAlternateBrushs_Error:

    LogError "frmDrawNew", "LoadAlternateBrushs", Err, Err.Description
End Sub

Private Function IsAlternateBrushReady(RefId As String, Ref As Integer) As Boolean
Dim NBrush As DiagnosisMasterBrush
    On Error GoTo lIsAlternateBrushReady_Error

IsAlternateBrushReady = False
If (Trim(RefId) <> "") Then
    Set NBrush = New DiagnosisMasterBrush
    NBrush.BrushName = ""
    NBrush.BrushDiagnosis = RefId
    If (NBrush.FindBrushbyDiagnosis > 0) Then
        If (NBrush.SelectBrush(1)) Then
            TheButtons(Ref).BrushType = "D"
            TheButtons(Ref).AssociatedId = 0
            TheButtons(Ref).BrushName = Trim(NBrush.BrushName)
            TheButtons(Ref).BrushLingo = Trim(NBrush.BrushLingo)
            TheButtons(Ref).BrushImageType = "B"
            TheButtons(Ref).DefaultColor = NBrush.BrushDefaultColor
            TheButtons(Ref).DefaultWidth = NBrush.BrushDefaultWidth
            TheButtons(Ref).RotateOn = NBrush.BrushRotateOn
            TheButtons(Ref).SizingOn = NBrush.BrushSizingOn
            TheButtons(Ref).ScalingOn = NBrush.BrushScalingOn
            TheButtons(Ref).LocateXOn = NBrush.BrushLocateXOn
            TheButtons(Ref).LocateYOn = NBrush.BrushLocateYOn
            TheButtons(Ref).FrameWidth = NBrush.BrushFrameWidth
            TheButtons(Ref).FrameHeight = NBrush.BrushFrameHeight
            TheButtons(Ref).TestTrigger = NBrush.BrushTestTrigger
            TheButtons(Ref).TestTriggerText = ""
            TheButtons(Ref).Quantifier = ""
            IsAlternateBrushReady = True
        End If
    End If
    Set NBrush = Nothing
End If

    Exit Function

lIsAlternateBrushReady_Error:

    LogError "frmDrawNew", "IsAlternateBrushReady", Err, Err.Description
End Function
    
Private Function TriggerTest(TheOrder As String, TheResult As String) As Boolean
Dim i As Integer
Dim AHigh As Boolean
Dim Temp1 As String, Temp2 As String
Dim Temp3 As String, Temp4 As String
Dim AType As String, Apo As Integer
Dim AQuestion As String
Dim RetTest As DynamicClass
    On Error GoTo lTriggerTest_Error

TriggerTest = False
TheResult = ""
If Not UserLogin.HasPermission(epExamElements) Then
    frmEventMsgs.InfoMessage "Not Permissioned"
    frmEventMsgs.Show 1
    Exit Function
End If
If (Trim(TheOrder) <> "") Then
    Set RetTest = New DynamicClass
    RetTest.Question = ""
    RetTest.QuestionOrder = Trim(TheOrder)
    RetTest.QuestionParty = "T"
    RetTest.QuestionSet = "First Visit"
    If (RetTest.FindClassForms > 0) Then
        If (RetTest.SelectClassForm(1)) Then
            AQuestion = RetTest.Question
        End If
    End If
    Set RetTest = Nothing
    If (Trim(AQuestion) <> "") Then
        frmTestResults.ActivityId = 0
        frmTestResults.ChangeRequest = False
        frmTestResults.PatientId = PatientId
        frmTestResults.QuestionSet = "First Visit"
        frmTestResults.AppointmentId = AppointmentId
        frmTestResults.FormOn = True
        frmDrawNew.Enabled = False
        If (frmTestResults.DoTest(AQuestion, Trim(TheOrder), False, True)) Then
            frmDrawNew.Enabled = True
            frmDrawNew.ZOrder 0
            i = frmSystems1.FindTestResults(Trim(TheOrder))
            If (i > 0) Then
                Call frmSystems1.GetTestText(i, TheResult, Temp1, Temp2, Temp3, Temp4, AHigh, AType, Apo)
                Call StripCharacters(TheResult, Chr(13))
                Call ReplaceCharacters(TheResult, Chr(10), " ")
                If (Trim(TheResult) <> "") Then
                    TriggerTest = True
                End If
            End If
        Else
            frmDrawNew.Enabled = True
            frmDrawNew.ZOrder 0
        End If
    End If
End If

    Exit Function

lTriggerTest_Error:

    LogError "frmDrawNew", "TriggerTest", Err, Err.Description
End Function

Private Function AppendTextToImage(AOrder As String, ATest As String) As Boolean
Dim k As Integer
Dim p As Integer
Dim k1 As Integer, k2 As Integer
Dim k3 As Integer, k4 As Integer
Dim k5 As Integer, k6 As Integer
Dim k7 As Integer, k8 As Integer
Dim Temp As String
Dim TheText As String
Dim LeftText As String
Dim RightText As String
    On Error GoTo lAppendTextToImage_Error

AppendTextToImage = False
If (Trim(AOrder) <> "") And (Trim(ATest) <> "") Then
    If (Trim(AOrder) = "72A") Then
        lblRTest1.Visible = False
        lblRTest1.Caption = "Label1"
        lblRTest2.Visible = False
        lblRTest2.Caption = "Label2"
        lblRTest3.Visible = False
        lblRTest3.Caption = "Label3"
        lblRTest4.Visible = False
        lblRTest4.Caption = "Label4"
        lblLTest1.Visible = False
        lblLTest1.Caption = "Label1"
        lblLTest2.Visible = False
        lblLTest2.Caption = "Label2"
        lblLTest3.Visible = False
        lblLTest3.Caption = "Label3"
        lblLTest4.Visible = False
        lblLTest4.Caption = "Label4"
        TheText = ATest
        If (Trim(TheText) <> "") Then
            k = InStrPS(TheText, "OS:")
            If (k > 0) Then
                RightText = Left(TheText, k - 1)
                LeftText = Mid(TheText, k, Len(TheText) - (k - 1))
                If (Len(LeftText) > 5) Then
                    LeftText = Trim(Mid(LeftText, 5, Len(LeftText) - 4))
                End If
                k = InStrPS(RightText, "OD:")
                If (k > 0) Then
                    RightText = Trim(Mid(RightText, k + 4, Len(RightText) - (k - 3)))
                End If
                Call StripCharacters(RightText, Chr(13))
                Call StripCharacters(RightText, Chr(10))
                Call StripCharacters(LeftText, Chr(13))
                Call StripCharacters(LeftText, Chr(10))
' setup right eye labels
                k1 = InStrPS(UCase(RightText), "TEMP")
                k2 = InStrPS(UCase(RightText), "SUPR")
                k3 = InStrPS(UCase(RightText), "NASAL")
                k4 = InStrPS(UCase(RightText), "INFR")
                Temp = ""
                If (k1 > 0) And (k2 > 0) Then
                    Temp = Trim(Mid(RightText, k1, (k2 - 1) - (k1 - 1)))
                ElseIf (k1 > 0) And (k3 > 0) Then
                    Temp = Trim(Mid(RightText, k1, (k3 - 1) - (k1 - 1)))
                ElseIf (k1 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(RightText, k1, (k4 - 1) - (k1 - 1)))
                ElseIf (k1 > 0) Then
                    Temp = Trim(RightText)
                End If
                If (Trim(Temp) <> "") Then
                    p = InStrPS(Temp, " ")
                    If (p > 0) Then
                        Temp = Mid(Temp, p, Len(Temp) - (p - 1))
                    End If
                    Temp = Trim(Temp)
                    If (Temp <> "") Then
                        lblRTest1.Caption = Temp
                        lblRTest1.Visible = True
                        lblRTest1.ZOrder 0
                    End If
                End If
                
                Temp = ""
                If (k2 > 0) And (k3 > 0) Then
                    Temp = Trim(Mid(RightText, k2, (k3 - 1) - (k2 - 1)))
                ElseIf (k2 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(RightText, k2, (k4 - 1) - (k2 - 1)))
                ElseIf (k2 > 0) Then
                    Temp = Mid(RightText, k2, Len(RightText) - (k2 - 1))
                End If
                If (Trim(Temp) <> "") Then
                    p = InStrPS(Temp, " ")
                    If (p > 0) Then
                        Temp = Mid(Temp, p, Len(Temp) - (p - 1))
                    End If
                    Temp = Trim(Temp)
                    If (Temp <> "") Then
                        lblRTest2.Caption = Temp
                        lblRTest2.Visible = True
                        lblRTest2.ZOrder 0
                    End If
                End If
                
                Temp = ""
                If (k3 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(RightText, k3, (k4 - 1) - (k3 - 1)))
                ElseIf (k3 > 0) Then
                    Temp = Mid(RightText, k3, Len(RightText) - (k3 - 1))
                End If
                If (Trim(Temp) <> "") Then
                    p = InStrPS(Temp, " ")
                    If (p > 0) Then
                        Temp = Mid(Temp, p, Len(Temp) - (p - 1))
                    End If
                    Temp = Trim(Temp)
                    If (Temp <> "") Then
                        lblRTest3.Caption = Temp
                        lblRTest3.Visible = True
                        lblRTest3.ZOrder 0
                    End If
                End If
                
                Temp = ""
                If (k4 > 0) Then
                    Temp = Trim(Mid(RightText, k4, Len(RightText) - (k2 - 1)))
                End If
                If (Trim(Temp) <> "") Then
                    p = InStrPS(Temp, " ")
                    If (p > 0) Then
                        Temp = Mid(Temp, p, Len(Temp) - (p - 1))
                    End If
                    Temp = Trim(Temp)
                    If (Temp <> "") Then
                        lblRTest4.Caption = Temp
                        lblRTest4.Visible = True
                        lblRTest4.ZOrder 0
                    End If
                End If
     
' setup left eye labels
                k1 = InStrPS(UCase(LeftText), "TEMP")
                k2 = InStrPS(UCase(LeftText), "SUPR")
                k3 = InStrPS(UCase(LeftText), "NASAL")
                k4 = InStrPS(UCase(LeftText), "INFR")
                Temp = ""
                If (k1 > 0) And (k2 > 0) Then
                    Temp = Trim(Mid(LeftText, k1, (k2 - 1) - (k1 - 1)))
                ElseIf (k1 > 0) And (k3 > 0) Then
                    Temp = Trim(Mid(LeftText, k1, (k3 - 1) - (k1 - 1)))
                ElseIf (k1 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k1, (k4 - 1) - (k1 - 1)))
                ElseIf (k1 > 0) Then
                    Temp = Trim(LeftText)
                End If
                If (Trim(Temp) <> "") Then
                    p = InStrPS(Temp, " ")
                    If (p > 0) Then
                        Temp = Mid(Temp, p, Len(Temp) - (p - 1))
                    End If
                    Temp = Trim(Temp)
                    If (Temp <> "") Then
                        lblLTest1.Caption = Temp
                        lblLTest1.Visible = True
                        lblLTest1.ZOrder 0
                    End If
                End If
                
                Temp = ""
                If (k2 > 0) And (k3 > 0) Then
                    Temp = Trim(Mid(LeftText, k2, (k3 - 1) - (k2 - 1)))
                ElseIf (k2 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k2, (k4 - 1) - (k2 - 1)))
                ElseIf (k2 > 0) Then
                    Temp = Mid(LeftText, k2, Len(LeftText) - (k2 - 1))
                End If
                If (Trim(Temp) <> "") Then
                    p = InStrPS(Temp, " ")
                    If (p > 0) Then
                        Temp = Mid(Temp, p, Len(Temp) - (p - 1))
                    End If
                    Temp = Trim(Temp)
                    If (Temp <> "") Then
                        lblLTest2.Caption = Temp
                        lblLTest2.Visible = True
                        lblLTest2.ZOrder 0
                    End If
                End If
                
                Temp = ""
                If (k3 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k3, (k4 - 1) - (k3 - 1)))
                ElseIf (k3 > 0) Then
                    Temp = Mid(LeftText, k3, Len(LeftText) - (k3 - 1))
                End If
                If (Trim(Temp) <> "") Then
                    p = InStrPS(Temp, " ")
                    If (p > 0) Then
                        Temp = Mid(Temp, p, Len(Temp) - (p - 1))
                    End If
                    Temp = Trim(Temp)
                    If (Temp <> "") Then
                        lblLTest3.Caption = Temp
                        lblLTest3.Visible = True
                        lblLTest3.ZOrder 0
                    End If
                End If
            
                Temp = ""
                If (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k4, Len(LeftText) - (k2 - 1)))
                End If
                If (Trim(Temp) <> "") Then
                    p = InStrPS(Temp, " ")
                    If (p > 0) Then
                        Temp = Mid(Temp, p, Len(Temp) - (p - 1))
                    End If
                    Temp = Trim(Temp)
                    If (Temp <> "") Then
                        lblLTest4.Caption = Temp
                        lblLTest4.Visible = True
                        lblLTest4.ZOrder 0
                    End If
                End If
            End If
        End If
    ElseIf (Trim(AOrder) = "27A") Then
        lblRTestA1.Caption = ""
        lblRTestA1.Visible = False
        lblLTestA1.Caption = ""
        lblLTestA1.Visible = False
        TheText = ATest
        If (Trim(TheText) <> "") Then
            k = InStrPS(TheText, "OS:")
            If (k > 0) Then
                LeftText = Mid(TheText, k, Len(TheText) - (k - 1))
                If (Len(LeftText) > 5) Then
                    LeftText = Trim(Mid(LeftText, 5, Len(LeftText) - 4))
                    Call StripCharacters(LeftText, Chr(13))
                    Call StripCharacters(LeftText, Chr(10))
                    If (Trim(LeftText) <> "") Then
                        lblLTestA1.Caption = LeftText
                        lblLTestA1.Visible = True
                        lblLTestA1.ZOrder 0
                    End If
                End If
                If (k > 5) Then
                    RightText = Left(TheText, k - 1)
                    Call StripCharacters(RightText, Chr(13))
                    Call StripCharacters(RightText, Chr(10))
                    k = InStrPS(RightText, "OD:")
                    If (k > 0) Then
                        RightText = Trim(Mid(RightText, k + 3, Len(RightText) - (k - 2)))
                    End If
                    If (Trim(RightText) <> "") Then
                        lblRTestA1.Caption = RightText
                        lblRTestA1.Visible = True
                        lblRTestA1.ZOrder 0
                    End If
                End If
            Else
                k = InStrPS(TheText, "OD:")
                If (k > 0) Then
                    RightText = Trim(TheText)
                    Call StripCharacters(RightText, Chr(13))
                    Call StripCharacters(RightText, Chr(10))
                    If (Trim(RightText) <> "") Then
                        lblRTestA1.Caption = RightText
                        lblRTestA1.Visible = True
                        lblRTestA1.ZOrder 0
                    End If
                    LeftText = ""
                End If
            End If
        End If
    ElseIf (Trim(AOrder) = "32X") Then
        ClearRSenLabels
        TheText = ATest
        If (Trim(TheText) <> "") Then
            k = InStrPS(TheText, "V:")
            If (k > 0) Then
                RightText = Left(TheText, k - 1)
                LeftText = Mid(TheText, k, Len(TheText) - (k - 1))
                If (Len(LeftText) > 3) Then
                    LeftText = Trim(Mid(LeftText, 3, Len(LeftText) - 2))
                End If
                k = InStrPS(RightText, "H:")
                If (k > 0) Then
                    RightText = Trim(Mid(RightText, k + 3, Len(RightText) - (k - 2)))
                End If
                Call StripCharacters(RightText, Chr(13))
                Call StripCharacters(RightText, Chr(10))
                Call StripCharacters(LeftText, Chr(13))
                Call StripCharacters(LeftText, Chr(10))
' setup right eye labels
                k1 = InStrPS(UCase(RightText), "R")
                If (k1 > 0) And ((UCase(Left(RightText, 7)) = "PRIMARY") Or (InStrPS(UCase(RightText), "PRIMARY") = k1 - 1)) Then
                    k1 = 0
                ElseIf (k1 > 0) And ((UCase(Left(RightText, 6)) = "R TILT") Or (InStrPS(UCase(RightText), "R TILT") = k1 - 1)) Then
                    k1 = 0
                End If
                k2 = InStrPS(UCase(RightText), "L")
                If (k2 > 0) And (UCase(Left(RightText, 6)) = "L TILT") Then
                    k2 = 0
                End If
                k3 = InStrPS(UCase(RightText), "U")
                k4 = InStrPS(UCase(RightText), "D")
                k5 = InStrPS(UCase(RightText), "PRIMARY")
                k6 = InStrPS(UCase(RightText), "R TILT")
                k7 = InStrPS(UCase(RightText), "L TILT")
                k8 = InStrPS(UCase(RightText), "TEST")
                Temp = ""
                If (k1 > 0) And (k2 > k1) Then
                    Temp = Trim(Mid(RightText, k1 + 2, (k2 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k3 > k1) Then
                    Temp = Trim(Mid(RightText, k1 + 2, (k3 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k4 > k1) Then
                    Temp = Trim(Mid(RightText, k1 + 2, (k4 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k5 > k1) Then
                    Temp = Trim(Mid(RightText, k1 + 2, (k5 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k6 > k1) Then
                    Temp = Trim(Mid(RightText, k1 + 2, (k6 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k7 > k1) Then
                    Temp = Trim(Mid(RightText, k1 + 2, (k7 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k8 > k1) Then
                    Temp = Trim(Mid(RightText, k1 + 2, (k8 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) Then
                    Temp = Trim(Mid(RightText, k1 + 2, Len(RightText) - (k1 + 1)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen8.Caption = Temp
                    lblRSen8.Visible = True
                End If
     
                Temp = ""
                If (k2 > 0) And (k3 > k2) Then
                    Temp = Trim(Mid(RightText, k2 + 2, (k3 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k4 > k2) Then
                    Temp = Trim(Mid(RightText, k2 + 2, (k4 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k5 > k2) Then
                    Temp = Trim(Mid(RightText, k2 + 2, (k5 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k6 > k2) Then
                    Temp = Trim(Mid(RightText, k2 + 2, (k6 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k7 > k2) Then
                    Temp = Trim(Mid(RightText, k2 + 2, (k7 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k8 > k2) Then
                    Temp = Trim(Mid(RightText, k2 + 2, (k8 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) Then
                    Temp = Trim(Mid(RightText, k2 + 2, Len(RightText) - (k2 + 1)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen10.Caption = Temp
                    lblRSen10.Visible = True
                End If
     
                Temp = ""
                If (k3 > 0) And (k4 > k3) Then
                    Temp = Trim(Mid(RightText, k3 + 2, (k4 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) And (k5 > k3) Then
                    Temp = Trim(Mid(RightText, k3 + 2, (k5 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) And (k6 > k3) Then
                    Temp = Trim(Mid(RightText, k3 + 2, (k6 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) And (k7 > k3) Then
                    Temp = Trim(Mid(RightText, k3 + 2, (k7 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) And (k8 > k3) Then
                    Temp = Trim(Mid(RightText, k3 + 2, (k8 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) Then
                    Temp = Trim(Mid(RightText, k3 + 2, Len(RightText) - (k3 + 1)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen6.Caption = Temp
                    lblRSen6.Visible = True
                End If

                Temp = ""
                If (k4 > 0) And (k5 > k4) Then
                    Temp = Trim(Mid(RightText, k4 + 2, (k5 - 1) - (k4 + 1)))
                ElseIf (k4 > 0) And (k6 > k4) Then
                    Temp = Trim(Mid(RightText, k4 + 2, (k6 - 1) - (k4 + 1)))
                ElseIf (k4 > 0) And (k7 > k4) Then
                    Temp = Trim(Mid(RightText, k4 + 2, (k7 - 1) - (k4 + 1)))
                ElseIf (k4 > 0) And (k8 > k4) Then
                    Temp = Trim(Mid(RightText, k4 + 2, (k8 - 1) - (k4 + 1)))
                ElseIf (k4 > 0) Then
                    Temp = Trim(Mid(RightText, k4 + 2, Len(RightText) - (k4 + 1)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen14.Caption = Temp
                    lblRSen14.Visible = True
                End If

                Temp = ""
                If (k5 > 0) And (k6 > k5) Then
                    Temp = Trim(Mid(RightText, k5 + 8, (k6 - 1) - (k5 + 7)))
                ElseIf (k5 > 0) And (k7 > k5) Then
                    Temp = Trim(Mid(RightText, k5 + 8, (k7 - 1) - (k5 + 7)))
                ElseIf (k5 > 0) And (k8 > k5) Then
                    Temp = Trim(Mid(RightText, k5 + 8, (k8 - 1) - (k5 + 7)))
                ElseIf (k5 > 0) Then
                    Temp = Trim(Mid(RightText, k5 + 8, Len(RightText) - (k5 + 7)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen12.Caption = Temp
                    lblRSen12.Visible = True
                End If

                Temp = ""
                If (k6 > 0) And (k7 > k6) Then
                    Temp = Trim(Mid(RightText, k6 + 7, (k7 - 1) - (k6 + 6)))
                ElseIf (k6 > 0) And (k8 > k6) Then
                    Temp = Trim(Mid(RightText, k6 + 7, (k8 - 1) - (k6 + 6)))
                ElseIf (k6 > 0) Then
                    Temp = Trim(Mid(RightText, k6 + 7, Len(RightText) - (k6 + 6)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen1.Caption = Temp
                    lblRSen1.Visible = True
                End If

                Temp = ""
                If (k7 > 0) And (k8 > k7) Then
                    Temp = Trim(Mid(RightText, k7 + 7, (k8 - 1) - (k7 + 6)))
                ElseIf (k7 > 0) Then
                    Temp = Trim(Mid(RightText, k7 + 7, Len(RightText) - (k7 + 6)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen3.Caption = Temp
                    lblRSen3.Visible = True
                End If

                Temp = ""
                If (k8 > 0) Then
                    Temp = Trim(Mid(RightText, k8 + 5, Len(RightText) - (k8 + 4)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen5.Caption = Temp
                    lblRSen5.Visible = True
                End If

' setup left eye labels
                k1 = InStrPS(UCase(LeftText), "R")
                If (k1 > 0) And ((UCase(Left(LeftText, 7)) = "PRIMARY") Or (InStrPS(UCase(RightText), "PRIMARY") = k1 - 1)) Then
                    k1 = 0
                ElseIf (k1 > 0) And ((UCase(Left(LeftText, 6)) = "R TILT") Or (InStrPS(UCase(RightText), "R TILT") = k1 - 1)) Then
                    k1 = 0
                End If
                k2 = InStrPS(UCase(LeftText), "L")
                If (k2 > 0) And (UCase(Left(LeftText, 6)) = "R TILT") Then
                    k2 = 0
                End If
                k3 = InStrPS(UCase(LeftText), "U")
                k4 = InStrPS(UCase(LeftText), "D")
                k5 = InStrPS(UCase(LeftText), "PRIMARY")
                k6 = InStrPS(UCase(LeftText), "R TILT")
                k7 = InStrPS(UCase(LeftText), "L TILT")
                k8 = InStrPS(UCase(LeftText), "TEST")
                Temp = ""
                If (k1 > 0) And (k2 > k1) Then
                    Temp = Trim(Mid(LeftText, k1 + 2, (k2 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k3 > k1) Then
                    Temp = Trim(Mid(LeftText, k1 + 2, (k3 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k4 > k1) Then
                    Temp = Trim(Mid(LeftText, k1 + 2, (k4 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k5 > k1) Then
                    Temp = Trim(Mid(LeftText, k1 + 2, (k5 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k6 > k1) Then
                    Temp = Trim(Mid(LeftText, k1 + 2, (k6 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k7 > k1) Then
                    Temp = Trim(Mid(LeftText, k1 + 2, (k7 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) And (k8 > k1) Then
                    Temp = Trim(Mid(LeftText, k1 + 2, (k8 - 1) - (k1 + 1)))
                ElseIf (k1 > 0) Then
                    Temp = Trim(Mid(LeftText, k1 + 2, Len(LeftText) - (k1 + 1)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen9.Caption = Temp
                    lblRSen9.Visible = True
                End If

                Temp = ""
                If (k2 > 0) And (k3 > k2) Then
                    Temp = Trim(Mid(LeftText, k2 + 2, (k3 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k4 > k2) Then
                    Temp = Trim(Mid(LeftText, k2 + 2, (k4 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k5 > k2) Then
                    Temp = Trim(Mid(LeftText, k2 + 2, (k5 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k6 > k2) Then
                    Temp = Trim(Mid(LeftText, k2 + 2, (k6 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k7 > k2) Then
                    Temp = Trim(Mid(LeftText, k2 + 2, (k7 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) And (k8 > k2) Then
                    Temp = Trim(Mid(LeftText, k2 + 2, (k8 - 1) - (k2 + 1)))
                ElseIf (k2 > 0) Then
                    Temp = Trim(Mid(LeftText, k2 + 2, Len(LeftText) - (k2 + 1)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen11.Caption = Temp
                    lblRSen11.Visible = True
                End If
     
                Temp = ""
                If (k3 > 0) And (k4 > k3) Then
                    Temp = Trim(Mid(LeftText, k3 + 2, (k4 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) And (k5 > k3) Then
                    Temp = Trim(Mid(LeftText, k3 + 2, (k5 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) And (k6 > k3) Then
                    Temp = Trim(Mid(LeftText, k3 + 2, (k6 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) And (k7 > k3) Then
                    Temp = Trim(Mid(LeftText, k3 + 2, (k7 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) And (k8 > k3) Then
                    Temp = Trim(Mid(LeftText, k3 + 2, (k8 - 1) - (k3 + 1)))
                ElseIf (k3 > 0) Then
                    Temp = Trim(Mid(LeftText, k3 + 2, Len(LeftText) - (k3 + 1)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen7.Caption = Temp
                    lblRSen7.Visible = True
                End If

                Temp = ""
                If (k4 > 0) And (k5 > k4) Then
                    Temp = Trim(Mid(LeftText, k4 + 2, (k5 - 1) - (k4 + 1)))
                ElseIf (k4 > 0) And (k6 > k4) Then
                    Temp = Trim(Mid(LeftText, k4 + 2, (k6 - 1) - (k4 + 1)))
                ElseIf (k4 > 0) And (k7 > k4) Then
                    Temp = Trim(Mid(LeftText, k4 + 2, (k7 - 1) - (k4 + 1)))
                ElseIf (k4 > 0) And (k8 > k4) Then
                    Temp = Trim(Mid(LeftText, k4 + 2, (k8 - 1) - (k4 + 1)))
                ElseIf (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k4 + 2, Len(LeftText) - (k4 + 1)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen15.Caption = Temp
                    lblRSen15.Visible = True
                End If

                Temp = ""
                If (k5 > 0) And (k6 > k5) Then
                    Temp = Trim(Mid(LeftText, k5 + 8, (k6 - 1) - (k5 + 7)))
                ElseIf (k5 > 0) And (k7 > k5) Then
                    Temp = Trim(Mid(LeftText, k5 + 8, (k7 - 1) - (k5 + 7)))
                ElseIf (k5 > 0) And (k8 > k5) Then
                    Temp = Trim(Mid(LeftText, k5 + 8, (k8 - 1) - (k5 + 7)))
                ElseIf (k5 > 0) Then
                    Temp = Trim(Mid(LeftText, k5 + 8, Len(LeftText) - (k5 + 7)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen13.Caption = Temp
                    lblRSen13.Visible = True
                End If

                Temp = ""
                If (k6 > 0) And (k7 > k6) Then
                    Temp = Trim(Mid(LeftText, k6 + 7, (k7 - 1) - (k6 + 6)))
                ElseIf (k6 > 0) And (k8 > k6) Then
                    Temp = Trim(Mid(LeftText, k6 + 7, (k8 - 1) - (k6 + 6)))
                ElseIf (k6 > 0) Then
                    Temp = Trim(Mid(LeftText, k6 + 7, Len(LeftText) - (k6 + 6)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen2.Caption = Temp
                    lblRSen2.Visible = True
                End If

                Temp = ""
                If (k7 > 0) And (k8 > k7) Then
                    Temp = Trim(Mid(LeftText, k7 + 7, (k8 - 1) - (k7 + 6)))
                ElseIf (k7 > 0) Then
                    Temp = Trim(Mid(LeftText, k7 + 7, Len(LeftText) - (k7 + 6)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen4.Caption = Temp
                    lblRSen4.Visible = True
                End If

                Temp = ""
                If (k8 > 0) Then
                    Temp = Trim(Mid(LeftText, k8 + 5, Len(LeftText) - (k8 + 4)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRSen5.Caption = Temp
                    lblRSen5.Visible = True
                End If
            End If
        End If
    ElseIf (Trim(AOrder) = "62A") Then
        lblSterioD.Visible = False
        lblSterioD.Caption = "Label4"
        TheText = ATest
        If (Trim(TheText) <> "") Then
            k = InStrPS(TheText, "Animals:")
            If (k > 0) Then
                LeftText = Trim(Mid(TheText, k + 8, Len(TheText) - (k + 7)))
                Call StripCharacters(LeftText, Chr(13))
                Call StripCharacters(LeftText, Chr(10))
                If (Trim(LeftText) <> "") Then
                    lblSterioD.Caption = LeftText
                    lblSterioD.Visible = True
                End If
            End If
        End If
    ElseIf (Trim(AOrder) = "63A") Then
        lblSterioN.Visible = False
        lblSterioN.Caption = "Label3"
        TheText = ATest
        If (Trim(TheText) <> "") Then
            k = InStrPS(TheText, "Circles:")
            If (k > 0) Then
                LeftText = Trim(Mid(TheText, k + 8, Len(TheText) - (k + 7)))
                Call StripCharacters(LeftText, Chr(13))
                Call StripCharacters(LeftText, Chr(10))
                If (Trim(LeftText) <> "") Then
                    lblSterioN.Caption = LeftText
                    lblSterioN.Visible = True
                End If
            End If
        End If
    ElseIf (Trim(AOrder) = "62D") Then
        lblWD4N.Visible = False
        lblWD4N.Caption = "Label1"
        lblWD4D.Visible = False
        lblWD4D.Caption = "Label2"
        TheText = ATest
        If (Trim(TheText) <> "") Then
            k = InStrPS(TheText, "Near:")
            If (k > 0) Then
                RightText = Left(TheText, k - 1)
                LeftText = Mid(TheText, k, Len(TheText) - (k - 1))
                If (Len(LeftText) > 6) Then
                    LeftText = Trim(Mid(LeftText, 6, Len(LeftText) - 5))
                End If
                k = InStrPS(RightText, "Distance:")
                If (k > 0) Then
                    RightText = Trim(Mid(RightText, k + 10, Len(RightText) - (k - 9)))
                End If
                Call StripCharacters(RightText, Chr(13))
                Call StripCharacters(RightText, Chr(10))
                Call StripCharacters(LeftText, Chr(13))
                Call StripCharacters(LeftText, Chr(10))
                If (Trim(RightText) <> "") Then
                    lblWD4D.Caption = RightText
                    lblWD4D.Visible = True
                End If
                If (Trim(LeftText) <> "") Then
                    lblWD4N.Caption = LeftText
                    lblWD4N.Visible = True
                End If
            End If
        End If
    ElseIf (Trim(AOrder) = "32Y") Then
        lblRVers1.Visible = False
        lblRVers1.Caption = "Label1"
        lblRVers2.Visible = False
        lblRVers2.Caption = "Label2"
        lblRVers3.Visible = False
        lblRVers3.Caption = "Label3"
        lblRVers4.Visible = False
        lblRVers4.Caption = "Label4"
        lblRVers5.Visible = False
        lblRVers5.Caption = "Label5"
        lblRVers6.Visible = False
        lblRVers6.Caption = "Label6"
        lblLVers1.Visible = False
        lblLVers1.Caption = "Label1"
        lblLVers2.Visible = False
        lblLVers2.Caption = "Label2"
        lblLVers3.Visible = False
        lblLVers3.Caption = "Label3"
        lblLVers4.Visible = False
        lblLVers4.Caption = "Label4"
        lblLVers5.Visible = False
        lblLVers5.Caption = "Label5"
        lblLVers6.Visible = False
        lblLVers6.Caption = "Label6"
        TheText = ATest
        If (Trim(TheText) <> "") Then
            k = InStrPS(TheText, "OS:")
            If (k > 0) Then
                RightText = Left(TheText, k - 1)
                LeftText = Mid(TheText, k, Len(TheText) - (k - 1))
                If (Len(LeftText) > 5) Then
                    LeftText = Trim(Mid(LeftText, 4, Len(LeftText) - 3))
                End If
                k = InStrPS(RightText, "OD:")
                If (k > 0) Then
                    RightText = Trim(Mid(RightText, k + 4, Len(RightText) - (k - 3)))
                End If
                Call StripCharacters(RightText, Chr(13))
                Call StripCharacters(RightText, Chr(10))
                Call StripCharacters(LeftText, Chr(13))
                Call StripCharacters(LeftText, Chr(10))
' setup right eye labels
                k1 = InStrPS(UCase(RightText), "IO:")
                k2 = InStrPS(UCase(RightText), "MR:")
                k3 = InStrPS(UCase(RightText), "SO:")
                k4 = InStrPS(UCase(RightText), "IR:")
                k5 = InStrPS(UCase(RightText), "LR:")
                k6 = InStrPS(UCase(RightText), "SR:")
                Temp = ""
                If (k1 > 0) And (k2 > 0) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k2 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k3 > 0) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k3 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k4 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k5 > 0) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k5 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k6 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) Then
                    Temp = Trim(Mid(RightText, 4, Len(RightText) - 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRVers4.Caption = Temp
                    lblRVers4.Visible = True
                End If
                
                Temp = ""
                If (k2 > 0) And (k3 > 0) Then
                    Temp = Trim(Mid(RightText, k2 + 3, (k3 - 1) - (k2 + 2)))
                ElseIf (k2 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(RightText, k2 + 3, (k4 - 1) - (k2 + 2)))
                ElseIf (k2 > 0) And (k5 > 0) Then
                    Temp = Trim(Mid(RightText, k2 + 3, (k5 - 1) - (k2 + 2)))
                ElseIf (k2 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(RightText, k2 + 3, (k6 - 1) - (k2 + 2)))
                ElseIf (k2 > 0) Then
                    Temp = Mid(RightText, k2 + 3, Len(RightText) - (k2 + 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRVers5.Caption = Temp
                    lblRVers5.Visible = True
                End If
                
                Temp = ""
                If (k3 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(RightText, k3 + 3, (k4 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k5 > 0) Then
                    Temp = Trim(Mid(RightText, k3 + 3, (k5 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(RightText, k3 + 3, (k6 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) Then
                    Temp = Mid(RightText, k3 + 3, Len(RightText) - (k3 + 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRVers6.Caption = Temp
                    lblRVers6.Visible = True
                End If
                
                Temp = ""
                If (k4 > 0) And (k5 > 0) Then
                    Temp = Trim(Mid(RightText, k4 + 3, (k5 - 1) - (k4 + 2)))
                ElseIf (k4 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(RightText, k4 + 3, (k6 - 1) - (k4 + 2)))
                ElseIf (k4 > 0) Then
                    Temp = Mid(RightText, k4 + 3, Len(RightText) - (k4 + 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRVers1.Caption = Temp
                    lblRVers1.Visible = True
                End If
                
                Temp = ""
                If (k5 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(RightText, k5 + 3, (k6 - 1) - (k5 + 2)))
                ElseIf (k5 > 0) Then
                    Temp = Mid(RightText, k5 + 3, Len(RightText) - (k5 + 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRVers2.Caption = Temp
                    lblRVers2.Visible = True
                End If
                
                Temp = ""
                If (k6 > 0) Then
                    Temp = Mid(RightText, k6 + 3, Len(RightText) - (k6 + 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblRVers3.Caption = Temp
                    lblRVers3.Visible = True
                End If

' setup Left eye labels
                k1 = InStrPS(UCase(LeftText), "IO:")
                k2 = InStrPS(UCase(LeftText), "MR:")
                k3 = InStrPS(UCase(LeftText), "SO:")
                k4 = InStrPS(UCase(LeftText), "IR:")
                k5 = InStrPS(UCase(LeftText), "LR:")
                k6 = InStrPS(UCase(LeftText), "SR:")
                Temp = ""
                If (k1 > 0) And (k2 > 0) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k2 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k3 > 0) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k3 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k4 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k5 > 0) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k5 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k6 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, Len(LeftText) - (k1 + 2)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblLVers1.Caption = Temp
                    lblLVers1.Visible = True
                End If
                
                Temp = ""
                If (k2 > 0) And (k3 > 0) Then
                    Temp = Trim(Mid(LeftText, k2 + 3, (k3 - 1) - (k2 + 2)))
                ElseIf (k2 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k2 + 3, (k4 - 1) - (k2 + 2)))
                ElseIf (k2 > 0) And (k5 > 0) Then
                    Temp = Trim(Mid(LeftText, k2 + 3, (k5 - 1) - (k2 + 2)))
                ElseIf (k2 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(LeftText, k2 + 3, (k6 - 1) - (k2 + 2)))
                ElseIf (k2 > 0) Then
                    Temp = Mid(LeftText, k2 + 3, Len(LeftText) - (k2 + 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblLVers2.Caption = Temp
                    lblLVers2.Visible = True
                End If
                
                Temp = ""
                If (k3 > 0) And (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k3 + 3, (k4 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k5 > 0) Then
                    Temp = Trim(Mid(LeftText, k3 + 3, (k5 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(LeftText, k3 + 3, (k6 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) Then
                    Temp = Mid(LeftText, k3 + 3, Len(LeftText) - (k3 + 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblLVers3.Caption = Temp
                    lblLVers3.Visible = True
                End If
                
                Temp = ""
                If (k4 > 0) And (k5 > 0) Then
                    Temp = Trim(Mid(LeftText, k4 + 3, (k5 - 1) - (k4 + 2)))
                ElseIf (k4 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(LeftText, k4 + 3, (k6 - 1) - (k4 + 2)))
                ElseIf (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k4 + 3, Len(LeftText) - (k4 + 2)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblLVers4.Caption = Temp
                    lblLVers4.Visible = True
                End If
                
                Temp = ""
                If (k5 > 0) And (k6 > 0) Then
                    Temp = Trim(Mid(LeftText, k5 + 3, (k6 - 1) - (k5 + 2)))
                ElseIf (k5 > 0) Then
                    Temp = Mid(LeftText, k5 + 3, Len(LeftText) - (k5 + 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblLVers5.Caption = Temp
                    lblLVers5.Visible = True
                End If
                
                Temp = ""
                If (k6 > 0) Then
                    Temp = Mid(LeftText, k6 + 3, Len(LeftText) - (k6 + 2))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblLVers6.Caption = Temp
                    lblLVers6.Visible = True
                End If
            End If
        End If
    ElseIf (Trim(AOrder) = "32Z") Then
        ClearBMLabels
        TheText = ATest
        If (Trim(TheText) <> "") Then
            k = InStrPS(TheText, "V:")
            If (k > 0) Then
                RightText = Left(TheText, k - 1)
                LeftText = Mid(TheText, k, Len(TheText) - (k - 1))
                If (Len(LeftText) > 3) Then
                    LeftText = Trim(Mid(LeftText, 3, Len(LeftText) - 2))
                End If
                k = InStrPS(RightText, "H:")
                If (k > 0) Then
                    RightText = Trim(Mid(RightText, k + 3, Len(RightText) - (k - 2)))
                End If
                Call StripCharacters(RightText, Chr(13))
                Call StripCharacters(RightText, Chr(10))
                Call StripCharacters(LeftText, Chr(13))
                Call StripCharacters(LeftText, Chr(10))
' setup right eye labels
                k1 = InStrPS(UCase(RightText), "SC ")
                k2 = InStrPS(UCase(RightText), "SC'")
                k3 = InStrPS(UCase(RightText), "CC:")
                k4 = InStrPS(UCase(RightText), "CC'")
                k5 = InStrPS(UCase(RightText), "CA'")
                k6 = InStrPS(UCase(RightText), "CM:")
                k7 = InStrPS(UCase(RightText), "CM'")
                k8 = InStrPS(UCase(RightText), "CMA'")
                Temp = ""
                If (k1 > 0) And (k2 > 0) And (k1 < k2) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k2 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k3 > 0) And (k1 < k3) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k3 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k4 > 0) And (k1 < k4) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k4 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k5 > 0) And (k1 < k5) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k5 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k6 > 0) And (k1 < k6) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k6 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k7 > 0) And (k1 < k7) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k7 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k8 > 0) And (k1 < k8) Then
                    Temp = Trim(Mid(RightText, k1 + 3, (k8 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) Then
                    Temp = Trim(Mid(RightText, k1 + 3, Len(RightText) - (k1 + 2)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM1.Caption = Temp
                    lblBM1.Visible = True
                End If
     
                Temp = ""
                If (k2 > 0) And (k3 > 0) And (k2 < k3) Then
                    Temp = Trim(Mid(RightText, k2 + 4, (k3 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k4 > 0) And (k2 < k4) Then
                    Temp = Trim(Mid(RightText, k2 + 4, (k4 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k5 > 0) And (k2 < k5) Then
                    Temp = Trim(Mid(RightText, k2 + 4, (k5 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k6 > 0) And (k2 < k6) Then
                    Temp = Trim(Mid(RightText, k2 + 4, (k6 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k7 > 0) And (k2 < k7) Then
                    Temp = Trim(Mid(RightText, k2 + 4, (k7 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k8 > 0) And (k2 < k8) Then
                    Temp = Trim(Mid(RightText, k2 + 4, (k8 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) Then
                    Temp = Trim(Mid(RightText, k2 + 4, Len(RightText) - (k2 + 3)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM3.Caption = Temp
                    lblBM3.Visible = True
                End If
     
                Temp = ""
                If (k3 > 0) And (k4 > 0) And (k3 < k4) Then
                    Temp = Trim(Mid(RightText, k3 + 3, (k4 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k5 > 0) And (k3 < k5) Then
                    Temp = Trim(Mid(RightText, k3 + 3, (k5 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k6 > 0) And (k3 < k6) Then
                    Temp = Trim(Mid(RightText, k3 + 3, (k6 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k7 > 0) And (k3 < k7) Then
                    Temp = Trim(Mid(RightText, k3 + 3, (k7 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k8 > 0) And (k3 < k8) Then
                    Temp = Trim(Mid(RightText, k3 + 3, (k8 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) Then
                    Temp = Trim(Mid(RightText, k3 + 3, Len(RightText) - (k3 + 2)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM5.Caption = Temp
                    lblBM5.Visible = True
                End If

                Temp = ""
                If (k4 > 0) And (k5 > 0) And (k4 < k5) Then
                    Temp = Trim(Mid(RightText, k4 + 4, (k5 - 1) - (k4 + 3)))
                ElseIf (k4 > 0) And (k6 > 0) And (k4 < k6) Then
                    Temp = Trim(Mid(RightText, k4 + 4, (k6 - 1) - (k4 + 3)))
                ElseIf (k4 > 0) And (k7 > 0) And (k4 < k7) Then
                    Temp = Trim(Mid(RightText, k4 + 4, (k7 - 1) - (k4 + 3)))
                ElseIf (k4 > 0) And (k8 > 0) And (k4 < k8) Then
                    Temp = Trim(Mid(RightText, k4 + 4, (k8 - 1) - (k4 + 3)))
                ElseIf (k4 > 0) Then
                    Temp = Trim(Mid(RightText, k4 + 4, Len(RightText) - (k4 + 3)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM7.Caption = Temp
                    lblBM7.Visible = True
                End If

                Temp = ""
                If (k5 > 0) And (k6 > 0) And (k5 < k6) Then
                    Temp = Trim(Mid(RightText, k5 + 4, (k6 - 1) - (k5 + 3)))
                ElseIf (k5 > 0) And (k7 > 0) And (k5 < k7) Then
                    Temp = Trim(Mid(RightText, k5 + 4, (k7 - 1) - (k5 + 3)))
                ElseIf (k5 > 0) And (k8 > 0) And (k5 < k8) Then
                    Temp = Trim(Mid(RightText, k5 + 4, (k8 - 1) - (k5 + 3)))
                ElseIf (k5 > 0) Then
                    Temp = Trim(Mid(RightText, k5 + 4, Len(RightText) - (k5 + 3)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM9.Caption = Temp
                    lblBM9.Visible = True
                End If

                Temp = ""
                If (k6 > 0) And (k7 > 0) And (k6 < k7) Then
                    Temp = Trim(Mid(RightText, k6 + 3, (k7 - 1) - (k6 + 2)))
                ElseIf (k6 > 0) And (k8 > 0) And (k6 < k7) Then
                    Temp = Trim(Mid(RightText, k6 + 3, (k8 - 1) - (k6 + 2)))
                ElseIf (k6 > 0) Then
                    Temp = Trim(Mid(RightText, k6 + 3, Len(RightText) - (k6 + 2)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM11.Caption = Temp
                    lblBM11.Visible = True
                End If

                Temp = ""
                If (k7 > 0) And (k8 > 0) And (k7 < k8) Then
                    Temp = Trim(Mid(RightText, k7 + 4, (k8 - 1) - (k7 + 3)))
                ElseIf (k7 > 0) Then
                    Temp = Trim(Mid(RightText, k7 + 4, Len(RightText) - (k7 + 3)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM13.Caption = Temp
                    lblBM13.Visible = True
                End If

                Temp = ""
                If (k8 > 0) Then
                    Temp = Trim(Mid(RightText, k8 + 5, Len(RightText) - (k8 + 4)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM15.Caption = Temp
                    lblBM15.Visible = True
                End If

' setup left eye labels
                k1 = InStrPS(UCase(LeftText), "SC ")
                k2 = InStrPS(UCase(LeftText), "SC'")
                k3 = InStrPS(UCase(LeftText), "CC:")
                k4 = InStrPS(UCase(LeftText), "CC'")
                k5 = InStrPS(UCase(LeftText), "CA'")
                k6 = InStrPS(UCase(LeftText), "CM:")
                k7 = InStrPS(UCase(LeftText), "CM'")
                k8 = InStrPS(UCase(LeftText), "CMA'")
                Temp = ""
                If (k1 > 0) And (k2 > 0) And (k1 < k2) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k2 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k3 > 0) And (k1 < k3) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k3 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k4 > 0) And (k1 < k4) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k4 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k5 > 0) And (k1 < k5) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k5 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k6 > 0) And (k1 < k6) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k6 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k7 > 0) And (k1 < k7) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k7 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) And (k8 > 0) And (k1 < k8) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, (k8 - 1) - (k1 + 2)))
                ElseIf (k1 > 0) Then
                    Temp = Trim(Mid(LeftText, k1 + 3, Len(LeftText) - (k1 + 2)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM2.Caption = Temp
                    lblBM2.Visible = True
                End If
     
                Temp = ""
                If (k2 > 0) And (k3 > 0) And (k2 < k3) Then
                    Temp = Trim(Mid(LeftText, k2 + 4, (k3 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k4 > 0) And (k2 < k4) Then
                    Temp = Trim(Mid(LeftText, k2 + 4, (k4 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k5 > 0) And (k2 < k5) Then
                    Temp = Trim(Mid(LeftText, k2 + 4, (k5 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k6 > 0) And (k2 < k6) Then
                    Temp = Trim(Mid(LeftText, k2 + 4, (k6 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k7 > 0) And (k2 < k7) Then
                    Temp = Trim(Mid(LeftText, k2 + 4, (k7 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) And (k8 > 0) And (k2 < k8) Then
                    Temp = Trim(Mid(LeftText, k2 + 4, (k8 - 1) - (k2 + 3)))
                ElseIf (k2 > 0) Then
                    Temp = Trim(Mid(LeftText, k2 + 4, Len(LeftText) - (k2 + 3)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM4.Caption = Temp
                    lblBM4.Visible = True
                End If
     
                Temp = ""
                If (k3 > 0) And (k4 > 0) And (k3 < k4) Then
                    Temp = Trim(Mid(LeftText, k3 + 3, (k4 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k5 > 0) And (k3 < k5) Then
                    Temp = Trim(Mid(LeftText, k3 + 3, (k5 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k6 > 0) And (k3 < k6) Then
                    Temp = Trim(Mid(LeftText, k3 + 3, (k6 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k7 > 0) And (k3 < k7) Then
                    Temp = Trim(Mid(LeftText, k3 + 3, (k7 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) And (k8 > 0) And (k3 < k8) Then
                    Temp = Trim(Mid(LeftText, k3 + 3, (k8 - 1) - (k3 + 2)))
                ElseIf (k3 > 0) Then
                    Temp = Trim(Mid(LeftText, k3 + 3, Len(LeftText) - (k3 + 2)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM6.Caption = Temp
                    lblBM6.Visible = True
                End If

                Temp = ""
                If (k4 > 0) And (k5 > 0) And (k4 < k5) Then
                    Temp = Trim(Mid(LeftText, k4 + 4, (k5 - 1) - (k4 + 3)))
                ElseIf (k4 > 0) And (k6 > 0) And (k4 < k6) Then
                    Temp = Trim(Mid(LeftText, k4 + 4, (k6 - 1) - (k4 + 3)))
                ElseIf (k4 > 0) And (k7 > 0) And (k4 < k7) Then
                    Temp = Trim(Mid(LeftText, k4 + 4, (k7 - 1) - (k4 + 3)))
                ElseIf (k4 > 0) And (k8 > 0) And (k4 < k8) Then
                    Temp = Trim(Mid(LeftText, k4 + 4, (k8 - 1) - (k4 + 3)))
                ElseIf (k4 > 0) Then
                    Temp = Trim(Mid(LeftText, k4 + 4, Len(LeftText) - (k4 + 3)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM8.Caption = Temp
                    lblBM8.Visible = True
                End If

                Temp = ""
                If (k5 > 0) And (k6 > 0) And (k5 < k6) Then
                    Temp = Trim(Mid(LeftText, k5 + 4, (k6 - 1) - (k5 + 3)))
                ElseIf (k5 > 0) And (k7 > 0) And (k5 < k7) Then
                    Temp = Trim(Mid(LeftText, k5 + 4, (k7 - 1) - (k5 + 3)))
                ElseIf (k5 > 0) And (k8 > 0) And (k5 < k8) Then
                    Temp = Trim(Mid(LeftText, k5 + 4, (k8 - 1) - (k5 + 3)))
                ElseIf (k5 > 0) Then
                    Temp = Trim(Mid(LeftText, k5 + 4, Len(LeftText) - (k5 + 3)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM10.Caption = Temp
                    lblBM10.Visible = True
                End If

                Temp = ""
                If (k6 > 0) And (k7 > 0) And (k6 < k7) Then
                    Temp = Trim(Mid(LeftText, k6 + 3, (k7 - 1) - (k6 + 2)))
                ElseIf (k6 > 0) And (k8 > 0) And (k6 < k8) Then
                    Temp = Trim(Mid(LeftText, k6 + 3, (k8 - 1) - (k6 + 2)))
                ElseIf (k6 > 0) Then
                    Temp = Trim(Mid(LeftText, k6 + 3, Len(LeftText) - (k6 + 2)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM12.Caption = Temp
                    lblBM12.Visible = True
                End If

                Temp = ""
                If (k7 > 0) And (k8 > 0) And (k7 < k8) Then
                    Temp = Trim(Mid(LeftText, k7 + 4, (k8 - 1) - (k7 + 3)))
                ElseIf (k7 > 0) Then
                    Temp = Trim(Mid(LeftText, k7 + 4, Len(LeftText) - (k7 + 3)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM14.Caption = Temp
                    lblBM14.Visible = True
                End If

                Temp = ""
                If (k8 > 0) Then
                    Temp = Trim(Mid(LeftText, k8 + 5, Len(LeftText) - (k8 + 4)))
                End If
                If (Trim(Temp) <> "") Then
                    Call StripCharacters(Temp, ";")
                    lblBM16.Caption = Temp
                    lblBM16.Visible = True
                End If
            End If
        End If
    End If
    AppendTextToImage = True
End If

    Exit Function

lAppendTextToImage_Error:

    LogError "frmDrawNew", "AppendTextToImage", Err, Err.Description
End Function

Private Function SetPatientName(PatId As Long, ApptId As Long) As Boolean
Dim ADate As String
Dim RetPat As Patient
Dim RetAppt As SchedulerAppointment
    On Error GoTo lSetPatientName_Error

lblPat.Caption = ""
If (PatId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        lblPat.Caption = Trim(RetPat.FirstName) + " " + Trim(RetPat.LastName)
    End If
    Set RetPat = Nothing
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ADate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
        lblPat.Caption = lblPat.Caption + " " + ADate
    End If
    Set RetAppt = Nothing
End If

    Exit Function

lSetPatientName_Error:

    LogError "frmDrawNew", "SetPatientName", Err, Err.Description
End Function

Public Function ReloadBaseImage(AFile As String) As Boolean
Dim Pic As IPictureDisp
    On Error GoTo lReloadBaseImage_Error

ReloadBaseImage = False
If (Trim(AFile) <> "") And (FM.IsFileThere(AFile)) Then
    XTwips = 1
    YTwips = 1
    If (frmDrawNew.ScaleMode = 1) Then
        XTwips = Screen.TwipsPerPixelX
        YTwips = Screen.TwipsPerPixelY
    End If
    Set Pic = Nothing
    FxImage1.picture = Pic
    FxImage1.Width = 462 * XTwips
    FxImage1.Height = 351 * YTwips
    FxImage1.AutoSize = ISIZE_ResizeImageToControl
    FxImage1.Tag = ""
    FxImage1.Transparent = True
    FxImage1.TransparentColor = &HFFFFFF
    FxImage1.Visible = False
    FxImage1.Enabled = True
    FxImage1.picture = FM.LoadPicture(AFile)
    FxImage1.Visible = True
End If
ReloadBaseImage = True

    Exit Function

lReloadBaseImage_Error:

    LogError "frmDrawNew", "ReloadBaseImage", Err, Err.Description
End Function

Private Function LoadLocName(AName As String) As Boolean
Dim i As Integer
Dim k As Integer
Dim RetCode As PracticeCodes
    On Error GoTo lLoadLocName_Error

Erase LocNames
If (Trim(AName) <> "") Then
    Set RetCode = New PracticeCodes
    If (AName = "08") Then
        AName = ""
    End If
    RetCode.ReferenceType = "ZONES" + AName
    If (RetCode.FindCode > 0) Then
        i = 1
        While (RetCode.SelectCode(i))
            k = Val(Trim(Left(RetCode.ReferenceCode, 2)))
            If (Mid(RetCode.ReferenceCode, 3, 2) = "OD") Then
                LocNames(1, k) = Trim(RetCode.ReferenceAlternateCode)
            ElseIf (Mid(RetCode.ReferenceCode, 3, 2) = "OS") Then
                LocNames(2, k) = Trim(RetCode.ReferenceAlternateCode)
            End If
            i = i + 1
        Wend
    End If
    Set RetCode = Nothing
End If

    Exit Function

lLoadLocName_Error:

    LogError "frmDrawNew", "LoadLocName", Err, Err.Description
End Function

Private Function SetLocation(Idx As Integer, SetOn As Boolean) As Boolean
Dim z As Integer
Dim d As Single, Ix As Single, Iy As Single
Dim ALoc As String
    On Error GoTo lSetLocation_Error

SetLocation = True
If (TheImage(Idx).BrushReference.LocateXOn < 1) And (TheImage(Idx).BrushReference.LocateYOn < 1) Then
    lblLoc.Visible = False
'Ix = Int((SelectedX - FxImage1.Left) / XTwips)
'Iy = Int((SelectedY - FxImage1.Top) / YTwips)
    Ix = Int((SelectedX) / XTwips)
    Iy = Int((SelectedY) / YTwips)
    d = (FxImage1.Width / 2) / XTwips
    If (frmHome.SetLocation(Ix, Iy, d, BaseImage, ALoc)) Then
        TheImage(Idx).Location = LocRef + ALoc + LocRefEnd
        If (Ix <= d) Then
            z = 1
        Else
            z = 2
        End If
        lblLoc.Caption = ALoc + "-" + LocNames(z, Val(ALoc))
        If (Trim(ReplaceBase) = "") Then
            If (SetOn) Then
                lblLoc.Visible = True
            End If
        End If
    End If
End If

    Exit Function

lSetLocation_Error:

    LogError "frmDrawNew", "SetLocation", Err, Err.Description
End Function

Private Function SetLocationBasic(X As Single, Y As Single) As Boolean
Dim z As Integer
Dim d As Single, Ix As Single, Iy As Single
Dim ALoc As String
    On Error GoTo lSetLocationBasic_Error

SetLocationBasic = True
lblLocView.Visible = False
SelectedX = X
SelectedY = Y
'Ix = Int((SelectedX - FxImage1.Left) / XTwips)
'Iy = Int((SelectedY - FxImage1.Top) / YTwips)
Ix = Int(SelectedX / XTwips)
Iy = Int(SelectedY / YTwips)
d = (FxImage1.Width / 2) / XTwips
If (frmHome.SetLocation(Ix, Iy, d, BaseImage, ALoc)) Then
    If (Ix <= d) Then
        z = 1
    Else
        z = 2
    End If
    If (Trim(ReplaceBase) = "") Then
        lblLocView.Caption = ALoc + "-" + LocNames(z, Val(ALoc))
        lblLocView.Visible = True
        DoEvents
    End If
End If

    Exit Function

lSetLocationBasic_Error:

    LogError "frmDrawNew", "SetLocationBasic", Err, Err.Description
End Function

Private Function LoadUnifiedDrawDisplay(ABase As String, OrgFile As String, VerifyOnly As Boolean) As Boolean
Dim i As Integer, p As Integer
Dim FilesToVerify(20) As String
Dim ARec As String, AFile As String
Dim MyFile As String, TheFile As String
    On Error GoTo lLoadUnifiedDrawDisplay_Error

i = 0
Erase FilesToVerify
LoadUnifiedDrawDisplay = False
If (Trim(ABase) <> "") Then
    AFile = DoctorInterfaceDirectory + "I*" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
    MyFile = Dir(AFile)
    While (Trim(MyFile) <> "") And (i < 20)
        TheFile = DoctorInterfaceDirectory + MyFile
        If (TheFile <> OrgFile) Then
            FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
            Do Until EOF(101)
                Line Input #101, ARec
                If (Left(ARec, 10) = "MYDRAW=/I-") Then
                    If (Mid(ARec, 11, Len(ARec) - 10) = ABase) Then
                        i = i + 1
                        FilesToVerify(i) = TheFile
                    End If
                End If
                Exit Do
            Loop
            FM.CloseFile CLng(101)
        End If
        MyFile = Dir
    Wend
    If Not (VerifyOnly) Then
        For p = 1 To i
            If (Trim(FilesToVerify(p)) <> "") Then
                Call LoadOtherDiagBrushs(FilesToVerify(p))
            End If
            LoadUnifiedDrawDisplay = True
        Next p
    Else
        If (i > 0) Then
            LoadUnifiedDrawDisplay = True
        End If
    End If
End If

    Exit Function

lLoadUnifiedDrawDisplay_Error:

    LogError "frmDrawNew", "LoadUnifiedDrawDisplay", Err, Err.Description
End Function

Private Function LoadOtherDiagBrushs(AFile As String) As Integer
Dim sc As Integer
Dim BRH As Boolean
Dim CurrCnt As Integer
Dim FrameWidth As Integer, FrameHeight As Integer
Dim ARec As String
Dim BrushLingo As String, BrushImageType As String
Dim CheckBrush As String, CheckAssociated As String
Dim BRT As String
Dim HST As String, BRN As String, ODR As String, tst As String
Dim RLO As String, CNF As String, STP As String, TPI As String
Dim LFT As String, SIZ As String, QUT As String, LCT As String
Dim ETP As String, ELT As String, EYS As String
Dim RetAss As DiagnosisMasterAssociated
Dim RetDiag As DiagnosisMasterPrimary
    On Error GoTo lLoadOtherDiagBrushs_Error

If (Trim(AFile) <> "") Then
    CheckBrush = ""
    FM.OpenFile AFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    Do Until EOF(101)
        Line Input #101, ARec
        If (Left(ARec, 5) = "NAME=") Then
            CheckBrush = Trim(Mid(ARec, 6, Len(ARec) - 5))
        End If
        If (Left(ARec, 10) = "BRUSHTYPE=") Then
            BRT = Mid(ARec, 11, 1)
        End If
        If (Left(ARec, 10) = "ASSOCIATE=") Then
            CheckAssociated = Trim(Mid(ARec, 11, Len(ARec) - 10))
        End If
        If (Left(ARec, 5) = "TEST=") Then
            tst = Trim(Mid(ARec, 6, Len(ARec) - 5))
        End If
        If (Left(ARec, 6) = "ORDER=") Then
            ODR = Trim(Mid(ARec, 7, Len(ARec) - 6))
        End If
        If (Left(ARec, 8) = "RULEOUT=") Then
            RLO = Trim(Mid(ARec, 9, Len(ARec) - 8))
        End If
        If (Left(ARec, 4) = "CNF=") Then
            CNF = Trim(Mid(ARec, 5, Len(ARec) - 4))
        End If
        If (Left(ARec, 4) = "HST=") Then
            HST = Trim(Mid(ARec, 5, Len(ARec) - 4))
        End If
        If (Left(ARec, 4) = "EYE=") Then
            EYS = Trim(Mid(ARec, 5, Len(ARec) - 4))
        End If
        If (Left(ARec, 11) = "STATUSPOST=") Then
            STP = Trim(Mid(ARec, 12, Len(ARec) - 11))
        End If
        If (Left(ARec, 4) = "TOP=") Then
            TPI = Trim(Mid(ARec, 5, Len(ARec) - 4))
        End If
        If (Left(ARec, 5) = "LEFT=") Then
            LFT = Trim(Mid(ARec, 6, Len(ARec) - 5))
        End If
        If (Left(ARec, 5) = "SIZE=") Then
            SIZ = Trim(Mid(ARec, 6, Len(ARec) - 5))
            If (SIZ = "0") Then
                SIZ = ""
            End If
        End If
        If (Left(ARec, 11) = "QUANTIFIER=") Then
            QUT = Trim(Mid(ARec, 12, Len(ARec) - 11))
        End If
        If (Left(ARec, 4) = "LOC=") Then
            LCT = Trim(Mid(ARec, 5, Len(ARec) - 4))
        End If
        If (Left(ARec, 5) = "BURN=") Then
            BRN = Trim(Mid(ARec, 6, Len(ARec) - 5))
        End If
        If (Left(ARec, 7) = "ENDTOP=") Then
            ETP = Trim(Mid(ARec, 8, Len(ARec) - 7))
        End If
        If (Left(ARec, 8) = "ENDLEFT=") Then
            ELT = Trim(Mid(ARec, 9, Len(ARec) - 8))
            BrushLingo = ""
            BRH = True
            If (BRH) Then
                If (CheckBrush <> "9900") And (BRT = "D") Then
                    Set TheBrush = New DiagnosisMasterBrush
                    TheBrush.BrushDiagnosis = ""
                    TheBrush.BrushName = CheckBrush
                    If (TheBrush.FindBrushbyName > 0) Then
                        If (TheBrush.SelectBrush(1)) Then
                            BrushLingo = TheBrush.BrushLingo
                            BrushImageType = TheBrush.BrushType
                            sc = Val(SIZ)
                            If (sc < 1) Then
                                sc = 1
                            End If
                            If (TheBrush.BrushFrameWidth > 0) And (TheBrush.BrushFrameHeight > 0) Then
                                If ((TheBrush.BrushLocateXOn > -1) And (TheBrush.BrushLocateYOn > -1)) Or Not (TheBrush.BrushScalingOn) Then
                                    FrameWidth = TheBrush.BrushFrameWidth * XTwips
                                    FrameHeight = TheBrush.BrushFrameHeight * YTwips
                                Else
                                    FrameWidth = TheBrush.BrushFrameWidth * sc
                                    FrameHeight = TheBrush.BrushFrameHeight * sc
                                End If
                            Else
                                FrameWidth = TheBrush.BrushFrameWidth * sc
                                FrameHeight = TheBrush.BrushFrameHeight * sc
                            End If
                        End If
                    End If
                    Set TheBrush = Nothing
                End If
                If (BRT = "O") Then
                    If (Val(CheckAssociated) > 0) Then
                        Set RetDiag = New DiagnosisMasterPrimary
                        RetDiag.PrimaryId = Val(CheckAssociated)
                        If (RetDiag.RetrievePrimary) Then
                            BrushLingo = Trim(RetDiag.PrimaryLingo)
                            BrushImageType = ""
                        End If
                        Set RetDiag = Nothing
                    End If
                End If
                If (Trim(BrushLingo) = "") Then
                    If ((CheckBrush = "9900") And (Trim(BrushLingo) = "")) Or (Val(CheckAssociated) > 0) Then
                        If (Val(CheckAssociated) > 0) Then
                            Set RetAss = New DiagnosisMasterAssociated
                            RetAss.AssociatedId = Val(CheckAssociated)
                            If (RetAss.RetrieveAssociated) Then
                                BrushLingo = Trim(RetAss.AssociatedLingo)
                                BrushImageType = ""
                            End If
                            Set RetAss = Nothing
                        End If
                    End If
                End If
                If (Trim(BrushLingo) <> "") Then
                    GlobalLeft = Val(LFT) + 50
                    GlobalTop = Val(TPI) + 50
                    GlobalHeight = FrameHeight
                    GlobalWidth = FrameWidth
                    CurrCnt = GetNextOtherLabel(EYS)
                    If (Trim(tst) <> "") Then
                        If (Trim(ODR) = "72A") Then
                            Call AppendTextToImage(Trim(ODR), Trim(tst))
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        ElseIf (Trim(ODR) = "27A") Then
                            Call AppendTextToImage(Trim(ODR), Trim(tst))
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        ElseIf (Trim(ODR) = "32Z") Then
                            Call AppendTextToImage(Trim(ODR), Trim(tst))
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        ElseIf (Trim(ODR) = "32Y") Then
                            Call AppendTextToImage(Trim(ODR), Trim(tst))
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        ElseIf (Trim(ODR) = "32X") Then
                            Call AppendTextToImage(Trim(ODR), Trim(tst))
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        ElseIf (Trim(ODR) = "61A") Then
                            Call AppendTextToImage(Trim(ODR), Trim(tst))
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        ElseIf (Trim(ODR) = "62A") Then
                            Call AppendTextToImage(Trim(ODR), Trim(tst))
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        ElseIf (Trim(ODR) = "63A") Then
                            Call AppendTextToImage(Trim(ODR), Trim(tst))
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        ElseIf (Trim(ODR) = "62D") Then
                            Call AppendTextToImage(Trim(ODR), Trim(tst))
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        Else
                            Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, True)
                        End If
                    Else
                        If (BRN = "F") Then
                            If (GlobalLeft > 50) And (GlobalTop > 50) Then
' at this point nothing gets redropped anymore
'                                Call PlantOtherImage(CheckBrush, CurrCnt, BrushImageType, BRN)
                            End If
                        End If
                        Call DropOtherLabel(BrushLingo, RLO, CNF, STP, QUT, HST, CurrCnt, False)
                    End If
                End If
            End If
        End If
    Loop
    FM.CloseFile CLng(101)
    OtherImagesOn = True
    If Not (frmSystems1.EditPreviousOn) Then
        If (Trim(tst) = "") Then
            Call ClearBrushes(0)
        End If
    End If
    OtherImagesOn = False
End If

    Exit Function

lLoadOtherDiagBrushs_Error:

    LogError "frmDrawNew", "LoadOtherDiagBrushs", Err, Err.Description
End Function

Private Sub DropOtherLabel(TheText As String, RO As String, CF As String, SP As String, QUT As String, HO As String, k As Integer, TestOn As Boolean)
Dim ALabel As Label
    On Error GoTo lDropOtherLabel_Error

If (Trim(TheText) <> "") Then
    If (k = 1) Then
        Set ALabel = lblImageA1
    ElseIf (k = 2) Then
        Set ALabel = lblImageA2
    ElseIf (k = 3) Then
        Set ALabel = lblImageA3
    ElseIf (k = 4) Then
        Set ALabel = lblImageA4
    ElseIf (k = 5) Then
        Set ALabel = lblImageA5
    ElseIf (k = 6) Then
        Set ALabel = lblImageA6
    ElseIf (k = 7) Then
        Set ALabel = lblImageA7
    ElseIf (k = 8) Then
        Set ALabel = lblImageA8
    ElseIf (k = 9) Then
        Set ALabel = lblImageA9
    ElseIf (k = 10) Then
        Set ALabel = lblImageA10
    ElseIf (k = 11) Then
        Set ALabel = lblImageA11
    ElseIf (k = 12) Then
        Set ALabel = lblImageA12
    ElseIf (k = 13) Then
        Set ALabel = lblImageA13
    ElseIf (k = 14) Then
        Set ALabel = lblImageA14
    ElseIf (k = 15) Then
        Set ALabel = lblImageA15
    ElseIf (k = 16) Then
        Set ALabel = lblImageA16
    ElseIf (k = 17) Then
        Set ALabel = lblImageA17
    ElseIf (k = 18) Then
        Set ALabel = lblImageA18
    Else
        Exit Sub
    End If
    With ALabel
        .AutoSize = True
        .WordWrap = True
        .Caption = Trim(TheText)
        If (Len(Trim(TheText)) > 23) Then
            .Caption = Trim(Left(TheText, 23))
        End If
        If (RO = "T") Then
            .Caption = "RO - " + Trim(.Caption)
        End If
        If (CF = "T") Then
            .Caption = "NF - " + Trim(.Caption)
        End If
        If (SP = "T") Then
            .Caption = "SP - " + Trim(.Caption)
        End If
        If (HO = "T") Then
            .Caption = "HO - " + Trim(.Caption)
        End If
        If (QUT <> "") Then
            .Caption = Trim(QUT) + " " + Trim(.Caption)
        End If
        Dim CheckOtherDiagnosis As Boolean
        CheckOtherDiagnosis = CheckOtherDiagnosisExists(k)
        If CheckOtherDiagnosis Then
           If (Len(.Caption) >= 20) Then
               .Caption = Trim(Left(TheText, 20)) + "++"
           End If
        Else
           If (Len(.Caption) >= 55) Then
               .Caption = Trim(Left(TheText, 23)) + "++"
           End If
        End If
        .Enabled = True
        .Visible = True
        .ZOrder 0
        .Refresh
    End With
    Call DropOtherLine(k, TestOn, ALabel)
End If

    Exit Sub

lDropOtherLabel_Error:

    LogError "frmDrawNew", "DropOtherLabel", Err, Err.Description
End Sub

Private Sub DropOtherLine(Counter As Integer, TestOn As Boolean, ALabel As Label)
Dim AWidth As Single, IWidth As Single
Dim XFactor As Single, YFactor As Single
Dim XOffset As Single, YOffset As Single
Dim AHeight As Single, IHeight As Single
Dim lblLine As Line
    On Error GoTo lDropOtherLine_Error

If (Counter > 0) Then
    AHeight = 240
    AWidth = 2250
    IHeight = 240
    IWidth = 240
    If (Counter = 1) Then
        Set lblLine = LineA1
    ElseIf (Counter = 2) Then
        Set lblLine = LineA2
    ElseIf (Counter = 3) Then
        Set lblLine = LineA3
    ElseIf (Counter = 4) Then
        Set lblLine = LineA4
    ElseIf (Counter = 5) Then
        Set lblLine = LineA5
    ElseIf (Counter = 6) Then
        Set lblLine = LineA6
    ElseIf (Counter = 7) Then
        Set lblLine = LineA7
    ElseIf (Counter = 8) Then
        Set lblLine = LineA8
    ElseIf (Counter = 9) Then
        Set lblLine = LineA9
    ElseIf (Counter = 10) Then
        Set lblLine = LineA10
    ElseIf (Counter = 11) Then
        Set lblLine = LineA11
    ElseIf (Counter = 12) Then
        Set lblLine = LineA12
    ElseIf (Counter = 13) Then
        Set lblLine = LineA13
    ElseIf (Counter = 14) Then
        Set lblLine = LineA14
    ElseIf (Counter = 15) Then
        Set lblLine = LineA15
    ElseIf (Counter = 16) Then
        Set lblLine = LineA16
    ElseIf (Counter = 17) Then
        Set lblLine = LineA17
    ElseIf (Counter = 18) Then
        Set lblLine = LineA18
    Else
        Exit Sub
    End If
End If
If (ALabel.Left > 0) And (ALabel.Top > 0) Then
    With lblLine
        .BorderWidth = 2
        .y2 = ALabel.Top - 360
        If (Counter < 10) Then
            .x2 = FxImage1.Width
            XOffset = 50
            YOffset = 50
            XFactor = 0
            YFactor = 0
        Else
            .x2 = 2
            XOffset = 150
            YOffset = 100
            XFactor = (FxImage1.Width / 2.5)
            YFactor = (FxImage1.Height / 2.5)
        End If
        XOffset = XOffset * 2
        YOffset = YOffset * 2
        If (GlobalHeight > 0) Then
            .x1 = (GlobalLeft - (FxImage1.Left + (IWidth / 2))) + XOffset
            If (.x1 > (FxImage1.Width / 2)) And (XFactor > 0) Then
                .x1 = (GlobalLeft + XOffset) - XFactor
                If (GlobalLeft > FxImage1.Width) Then
                    .x1 = FxImage1.Width - ((GlobalLeft - FxImage1.Width) / 1.5)
                End If
            ElseIf (GlobalLeft > (FxImage1.Width / 2)) And (XFactor > 0) Then
                .x1 = (GlobalLeft + XOffset) - XFactor
                If (GlobalLeft > FxImage1.Width) Then
                    .x1 = FxImage1.Width - ((GlobalLeft - FxImage1.Width) / 1.5)
                End If
            End If
            .y1 = (GlobalTop - (FxImage1.Top + (IHeight / 2)))
            If (GlobalTop > (FxImage1.Height / 2)) Then
                .y1 = (GlobalTop - (FxImage1.Top + (IHeight / 2))) + YOffset
                If (GlobalTop > FxImage1.Height) Then
                    .y1 = (GlobalTop - (FxImage1.Top + (IHeight / 2))) + YOffset
                End If
            End If
            If (.y1 < 1) Then
                .y1 = 10
            End If
        Else
            .x1 = GlobalLeft
            .y1 = GlobalTop
            If (Counter >= 10) Then
                .x1 = GlobalLeft - (XFactor / 1.25)
                .y1 = GlobalTop - (YFactor / 4)
            ElseIf (GlobalLeft > FxImage1.Width) Then
                .x1 = GlobalLeft - (FxImage1.Width / 3)
                .y1 = GlobalTop - (FxImage1.Height / 10)
            End If
            If (.y1 < 1) Then
                .y1 = 10
            End If
        End If
        If (.y1 > 0) And (.x1 > 0) Then
            .Visible = StartLinesOn
        Else
            .Visible = False
        End If
        If (TestOn) Then
            .Visible = False
        End If
        .ZOrder 0
        .Refresh
    End With
End If

    Exit Sub

lDropOtherLine_Error:

    LogError "frmDrawNew", "DropOtherLine", Err, Err.Description
End Sub

Private Function BrushOptions(Idx As Integer) As Boolean
Dim IHow As Boolean
Dim CurIdx As Integer
Dim Temp As String
Dim Header As String
    On Error GoTo lBrushOptions_Error

If (Idx > 0) And (Idx <= MaxBrushsAllowed) Then
    CurIdx = TotalImagesSelected
    IHow = False
    Header = "Brush Action ?"
    Header = Header & vbCrLf & DisplayLabelContent(Idx)
    frmEventMsgs.Header = Header
    frmEventMsgs.AcceptText = "Delete Brush"
    If (TheImage(Idx).BrushReference.BrushName = "9900") Or (TheImage(Idx).BrushReference.BrushName = "") Then
        frmEventMsgs.AcceptText = "Erase Brush"
    End If
    frmEventMsgs.RejectText = ""
    If (TheImage(Idx).LineId <> 0) Then
        If (IsLineVisible(TheImage(Idx).LineId)) Then
            IHow = False
            frmEventMsgs.RejectText = "Line Invisible"
        Else
            IHow = True
            frmEventMsgs.RejectText = "Line   Visible"
        End If
    End If
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Quantifier"
    frmEventMsgs.Other1Text = "Rule Out"
    frmEventMsgs.Other2Text = "History Of"
    frmEventMsgs.Other3Text = "CNF"
    frmEventMsgs.Other4Text = "Status Post"
    cmdHome.Enabled = False
    cmdDone.Enabled = False
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        CurrentImageId = Idx
        If (frmEventMsgs.AcceptText = "Erase Brush") Then
            EraseOn = True
            lblErase.Visible = True
            Call LoadBackRoundBaseImage
        Else
            Call DeleteTheBrush(Idx)
        End If
        If 0 <= TheImage(Idx).ButtonNumber < TotalBrushButtons Then
            cmdBrush(TheImage(Idx).ButtonNumber).BackColor = OriginalColor
        End If
        Call InitializeImages(Idx, False)
        Call BuildStoreFile
        Call PrevImageName(DrawFileName)
        Call SetNewDrawFile(DrawFileName, False)
        PrevImageId = CurrentImageId
        CurrentImageId = 0
        cmdRotate.Visible = False
        cmdImageDraw.Visible = False
        cmdScaleUp.Visible = False
        cmdScaleUp.ToolTipText = ""
        cmdScaleDown.Visible = False
        cmdScaleDown.ToolTipText = ""
        If (frmEventMsgs.AcceptText = "Erase Brush") Then
            EraseOn = True
            lblErase.Visible = True
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        If (TheImage(Idx).Left > 0) And (TheImage(Idx).Top > 0) Then
            If (TheImage(Idx).BrushReference.TestTriggerText = "") Then
                Call SetLineVisible(TheImage(Idx).LineId, IHow)
            End If
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        CurrentImageId = Idx
        Call SetQuantifier(Idx)
    ElseIf (frmEventMsgs.Result = 5) Then
        CurrentImageId = Idx
        TheImage(Idx).RuleOut = Not (TheImage(Idx).RuleOut)
        Temp = TheImage(Idx).BrushReference.BrushLingo
        Call SetLabelCaption(Idx)
    ElseIf (frmEventMsgs.Result = 6) Then
        CurrentImageId = Idx
        TheImage(Idx).HistoryOf = Not (TheImage(Idx).HistoryOf)
        Temp = TheImage(Idx).BrushReference.BrushLingo
        Call SetLabelCaption(Idx)
    ElseIf (frmEventMsgs.Result = 7) Then
        CurrentImageId = Idx
        TheImage(Idx).CNF = Not (TheImage(Idx).CNF)
        Temp = TheImage(Idx).BrushReference.BrushLingo
        Call SetLabelCaption(Idx)
    ElseIf (frmEventMsgs.Result = 8) Then
        CurrentImageId = Idx
        TheImage(Idx).StatusPost = Not (TheImage(Idx).StatusPost)
        Temp = TheImage(Idx).BrushReference.BrushLingo
        Call SetLabelCaption(Idx)
    End If
    cmdDone.Enabled = True
    cmdHome.Enabled = True
End If

    Exit Function

lBrushOptions_Error:

    LogError "frmDrawNew", "BrushOptions", Err, Err.Description
End Function

Private Sub SetQuantifier(Idx As Integer)
Dim Qual As String
Dim AEye As String
Dim QuitOn As Boolean
    On Error GoTo lSetQuantifier_Error

If (Idx > 0) Then
    Qual = ""
    AEye = Trim(TheImage(Idx).Eye)
    If (AEye = "") Then
        AEye = "OU"
    End If
    Call frmHome.DoQualifier(Qual, AEye, QuitOn)
    If (Trim(Qual) <> "") And (Trim(Qual) <> "[]") And (Not QuitOn) Then
        TheImage(Idx).BrushReference.Quantifier = Qual
        Call SetLabelCaption(Idx)
    ElseIf (Trim(Qual) = "[]") And (Not QuitOn) Then
        TheImage(Idx).BrushReference.Quantifier = ""
        Call SetLabelCaption(Idx)
    End If
End If

    Exit Sub

lSetQuantifier_Error:

    LogError "frmDrawNew", "SetQuantifier", Err, Err.Description
End Sub

Private Function BuildStoreFile() As Boolean
Dim i As Integer
Dim p As Integer
Dim Temp As String
Dim OtherFile As String
Dim FileNum As Long, MaxImages As Long
    On Error GoTo lBuildStoreFile_Error

FileNum = FreeFile
If (FM.IsFileThere(StoreResults)) Then
    FM.Kill StoreResults
    DoEvents
End If
FM.OpenFile StoreResults, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
If (Trim(DrawFileName) <> "") Then
    If Not (FM.IsFileThere(DrawFileName)) Then
        Call SaveNewDrawFileName(DrawFileName, False)
    ElseIf Not (ReloadReplaceScanOn) Then
        Call SaveNewDrawFileName(DrawFileName, False)
    End If
    If (Trim(ReplaceBase) <> "") Then
        p = InStrPS(ReplaceBase, ".")
        If (p > 0) Then
            Temp = Mid(ReplaceBase, p, Len(ReplaceBase) - (p - 1))
            Print #FileNum, "MYDRAW=/R-NewBase-D" + Trim(str(DiagnosisId)) + "-A" + Trim(str(AppointmentId)) + Temp
        Else
            Print #FileNum, "MYDRAW=/" + Trim(BaseImage) + BaseImageRef
        End If
    ElseIf (InStrPS(DrawFileName, "I-") <> 0) Then
        Print #FileNum, "MYDRAW=/I-" + Trim(BaseImage) + BaseImageRef
    Else
        Print #FileNum, "MYDRAW=/" + Trim(BaseImage) + BaseImageRef
    End If
End If
Print #FileNum, "DIAGNOSIS=" + Trim(str(DiagnosisId))
MaxImages = TotalImagesSelected
For p = 1 To MaxImagesAllowed
    If (Trim(TheImage(p).Name) <> "") And (Trim(TheImage(p).Name) <> "-") Then
        Print #FileNum, "NAME=" + Trim(TheImage(p).Name)
        Print #FileNum, "TEST=" + Trim(TheImage(p).BrushReference.TestTriggerText)
        Print #FileNum, "ORDER=" + Trim(TheImage(p).BrushReference.TestTrigger)
        Print #FileNum, "BRUSHTYPE=" + Trim(TheImage(p).BrushReference.BrushType)
        Print #FileNum, "EYE=" + Trim(TheImage(p).Eye)
        Print #FileNum, "ASSOCIATE=" + Trim(str(TheImage(p).BrushReference.AssociatedId))
        If (TheImage(p).HistoryOf) Then
            Print #FileNum, "HST=T"
        Else
            Print #FileNum, "HST=F"
        End If
        If (TheImage(p).RuleOut) Then
            Print #FileNum, "RULEOUT=T"
        Else
            Print #FileNum, "RULEOUT=F"
        End If
        If (TheImage(p).CNF) Then
            Print #FileNum, "CNF=T"
        Else
            Print #FileNum, "CNF=F"
        End If
        If (TheImage(p).StatusPost) Then
            Print #FileNum, "STATUSPOST=T"
        Else
            Print #FileNum, "STATUSPOST=F"
        End If
        If (TheImage(p).DroppedImage) Then
            Print #FileNum, "DROPPEDIMAGE=T"
        Else
            Print #FileNum, "DROPPEDIMAGE=F"
        End If
        If (TheImage(p).Burn) Then
            Print #FileNum, "BURN=T"
        Else
            Print #FileNum, "BURN=F"
        End If
        Print #FileNum, "SIZE=" + Trim(str(TheImage(p).Size))
        Print #FileNum, "TOP=" + Trim(str(TheImage(p).Top))
        Print #FileNum, "LEFT=" + Trim(str(TheImage(p).Left))
        Print #FileNum, "ROTATE=" + Trim(str(TheImage(p).Rotate))
        Print #FileNum, "QUANTIFIER=" + Trim(TheImage(p).BrushReference.Quantifier)
        Print #FileNum, "LOC=" + Trim(TheImage(p).Location)
        Print #FileNum, "OTHERS=0"
        Print #FileNum, "ENDTOP=0"
        Print #FileNum, "ENDLEFT=0"
    End If
Next p
FM.CloseFile CLng(FileNum)
If (frmHome.BackUpOn) Then
    i = FM.InFileNameStartStr(StoreResults, "T")
    If (i > 0) Then
        OtherFile = Left(StoreResults, i - 1) + "Backup\" + Mid(StoreResults, i, Len(StoreResults) - i + 1)
        FM.FileCopy StoreResults, OtherFile
    End If
End If

    Exit Function

lBuildStoreFile_Error:

    LogError "frmDrawNew", "BuildStoreFile", Err, Err.Description
End Function

Private Function SetLabelCaption(Idx As Integer) As Boolean
Dim k As Integer
Dim Temp As String
Dim Text As String
    On Error GoTo lSetLabelCaption_Error

k = TheImage(Idx).LabelId
Dim TempTestTrigger As String
Dim FoundIt
TempTestTrigger = "72A" + " , " + "27A" + " , " + "32Z" + " , " + "32X" + " ," + "32Y" + " , " _
                    + "32X" + " , " + "61A" + " , " + "62A" + " , " + "63A" + " , " + "62D"
                    
FoundIt = InStrPS(1, TempTestTrigger, TheImage(Idx).BrushReference.TestTrigger)
If Trim(TheImage(Idx).BrushReference.TestTrigger) = "" Then
    Temp = Trim(TheImage(Idx).BrushReference.BrushLingo)
    Text = Temp
End If
If (k < 1) Then
    k = GetNextLabel(Idx)
End If
If (TheImage(Idx).RuleOut) Then
    Temp = "RO - " + Trim(Temp)
End If
If (TheImage(Idx).CNF) Then
    Temp = "NF - " + Trim(Temp)
End If
If (TheImage(Idx).HistoryOf) Then
    Temp = "HO - " + Trim(Temp)
End If
If (TheImage(Idx).StatusPost) Then
    Temp = "SP - " + Trim(Temp)
End If
If (Trim(TheImage(Idx).BrushReference.Quantifier) <> "") Then
    Temp = Trim(TheImage(Idx).BrushReference.Quantifier) + Trim(Temp)
End If

If Not Trim(TheImage(Idx).BrushReference.TestTrigger) = "" Then
    If FoundIt = 0 Then
        Temp = Temp + " " + Trim(TheImage(Idx).BrushReference.TestTriggerText)
        Text = Trim(TheImage(Idx).BrushReference.TestTriggerText)
    Else
        Temp = Temp + " " + Trim(TheImage(Idx).BrushReference.BrushLingo)
        Text = Trim(TheImage(Idx).BrushReference.BrushLingo)
    End If
End If
Dim CheckOtherDiagnosis As Boolean
CheckOtherDiagnosis = CheckOtherDiagnosisExists(k)
If CheckOtherDiagnosis Then
   If (Len(Temp) >= 20) Then
       Temp = Trim(Left(Text, 20)) + "++"
   End If
Else
   If (Len(Temp) >= 55) Then
       Temp = Trim(Left(Text, 23)) + "++"
   End If
End If
SetLabelCaption = True
If (k = 1) Then
        lblImage1.Caption = Temp
ElseIf (k = 2) Then
        lblImage2.Caption = Temp
ElseIf (k = 3) Then
        lblImage3.Caption = Temp
ElseIf (k = 4) Then
        lblImage4.Caption = Temp
ElseIf (k = 5) Then
        lblImage5.Caption = Temp
ElseIf (k = 6) Then
        lblImage6.Caption = Temp
ElseIf (k = 7) Then
        lblImage7.Caption = Temp
ElseIf (k = 8) Then
        lblImage8.Caption = Temp
ElseIf (k = 9) Then
        lblImage9.Caption = Temp
ElseIf (k = 10) Then
        lblImage10.Caption = Temp
ElseIf (k = 11) Then
        lblImage11.Caption = Temp
ElseIf (k = 12) Then
        lblImage12.Caption = Temp
ElseIf (k = 13) Then
        lblImage13.Caption = Temp
ElseIf (k = 14) Then
        lblImage14.Caption = Temp
ElseIf (k = 15) Then
        lblImage15.Caption = Temp
ElseIf (k = 16) Then
        lblImage16.Caption = Temp
ElseIf (k = 17) Then
        lblImage17.Caption = Temp
ElseIf (k = 18) Then
        lblImage18.Caption = Temp
End If

    Exit Function

lSetLabelCaption_Error:

    LogError "frmDrawNew", "SetLabelCaption", Err, Err.Description
End Function

Private Function SetLabelVisible(Idx As Integer, Visi As Boolean) As Boolean
    On Error GoTo lSetLabelVisible_Error

SetLabelVisible = True
If (Idx = 1) Or (Idx = 0) Then
    lblImage1.Visible = Visi
End If
If (Idx = 2) Or (Idx = 0) Then
    lblImage2.Visible = Visi
End If
If (Idx = 3) Or (Idx = 0) Then
    lblImage3.Visible = Visi
End If
If (Idx = 4) Or (Idx = 0) Then
    lblImage4.Visible = Visi
End If
If (Idx = 5) Or (Idx = 0) Then
    lblImage5.Visible = Visi
End If
If (Idx = 6) Or (Idx = 0) Then
    lblImage6.Visible = Visi
End If
If (Idx = 7) Or (Idx = 0) Then
    lblImage7.Visible = Visi
End If
If (Idx = 8) Or (Idx = 0) Then
    lblImage8.Visible = Visi
End If
If (Idx = 9) Or (Idx = 0) Then
    lblImage9.Visible = Visi
End If
If (Idx = 10) Or (Idx = 0) Then
    lblImage10.Visible = Visi
End If
If (Idx = 11) Or (Idx = 0) Then
    lblImage11.Visible = Visi
End If
If (Idx = 12) Or (Idx = 0) Then
    lblImage12.Visible = Visi
End If
If (Idx = 13) Or (Idx = 0) Then
    lblImage13.Visible = Visi
End If
If (Idx = 14) Or (Idx = 0) Then
    lblImage14.Visible = Visi
End If
If (Idx = 15) Or (Idx = 0) Then
    lblImage15.Visible = Visi
End If
If (Idx = 16) Or (Idx = 0) Then
    lblImage16.Visible = Visi
End If
If (Idx = 17) Or (Idx = 0) Then
    lblImage17.Visible = Visi
End If
If (Idx = 18) Or (Idx = 0) Then
    lblImage18.Visible = Visi
End If

    Exit Function

lSetLabelVisible_Error:

    LogError "frmDrawNew", "SetLabelVisible", Err, Err.Description
End Function

Private Function SetImageVisible(Idx As Integer, AddOn As Boolean) As Boolean
Dim k As Integer
    On Error GoTo lSetImageVisible_Error

SetImageVisible = True
k = Idx
If (k = 1) Then
    If Not (TheImage(k).Burn) Then
        imgImage1.Visible = AddOn
    End If
ElseIf (k = 2) Then
    If Not (TheImage(k).Burn) Then
        imgImage2.Visible = AddOn
    End If
ElseIf (k = 3) Then
    If Not (TheImage(k).Burn) Then
        imgImage3.Visible = AddOn
    End If
ElseIf (k = 4) Then
    If Not (TheImage(k).Burn) Then
        imgImage4.Visible = AddOn
    End If
ElseIf (k = 5) Then
    If Not (TheImage(k).Burn) Then
        imgImage5.Visible = AddOn
    End If
ElseIf (k = 6) Then
    If Not (TheImage(k).Burn) Then
        imgImage6.Visible = AddOn
    End If
ElseIf (k = 7) Then
    If Not (TheImage(k).Burn) Then
        imgImage7.Visible = AddOn
    End If
ElseIf (k = 8) Then
    If Not (TheImage(k).Burn) Then
        imgImage8.Visible = AddOn
    End If
ElseIf (k = 9) Then
    If Not (TheImage(k).Burn) Then
        imgImage9.Visible = AddOn
    End If
ElseIf (k = 10) Then
    If Not (TheImage(k).Burn) Then
        imgImage10.Visible = AddOn
    End If
ElseIf (k = 11) Then
    If Not (TheImage(k).Burn) Then
        imgImage11.Visible = AddOn
    End If
ElseIf (k = 12) Then
    If Not (TheImage(k).Burn) Then
        imgImage12.Visible = AddOn
    End If
ElseIf (k = 13) Then
    If Not (TheImage(k).Burn) Then
        imgImage13.Visible = AddOn
    End If
ElseIf (k = 14) Then
    If Not (TheImage(k).Burn) Then
        imgImage14.Visible = AddOn
    End If
ElseIf (k = 15) Then
    If Not (TheImage(k).Burn) Then
        imgImage15.Visible = AddOn
    End If
ElseIf (k = 16) Then
    If Not (TheImage(k).Burn) Then
        imgImage16.Visible = AddOn
    End If
ElseIf (k = 17) Then
    If Not (TheImage(k).Burn) Then
        imgImage17.Visible = AddOn
    End If
ElseIf (k = 18) Then
    If Not (TheImage(k).Burn) Then
        imgImage18.Visible = AddOn
    End If
End If
Call SetLabelVisible(TheImage(Idx).LabelId, AddOn)
Call SetLineVisible(TheImage(Idx).LineId, StartLinesOn)

    Exit Function

lSetImageVisible_Error:

    LogError "frmDrawNew", "SetImageVisible", Err, Err.Description
End Function

Private Function GetNextLabel(Idx As Integer) As Integer
Dim k As Integer
Dim AStart As Integer
    On Error GoTo lGetNextLabel_Error

GetNextLabel = 0
AStart = 10
If (TheImage(Idx).Eye = "OS") Then
    AStart = 1
End If
For k = AStart To AStart + 8
    If (k = 1) Then
        If Not (lblImage1.Visible) Then
            GetNextLabel = 1
            Exit For
        End If
    ElseIf (k = 2) Then
        If Not (lblImage2.Visible) Then
            GetNextLabel = 2
            Exit For
        End If
    ElseIf (k = 3) Then
        If Not (lblImage3.Visible) Then
            GetNextLabel = 3
            Exit For
        End If
    ElseIf (k = 4) Then
        If Not (lblImage4.Visible) Then
            GetNextLabel = 4
            Exit For
        End If
    ElseIf (k = 5) Then
        If Not (lblImage5.Visible) Then
            GetNextLabel = 5
            Exit For
        End If
    ElseIf (k = 6) Then
        If Not (lblImage6.Visible) Then
            GetNextLabel = 6
            Exit For
        End If
    ElseIf (k = 7) Then
        If Not (lblImage7.Visible) Then
            GetNextLabel = 7
            Exit For
        End If
    ElseIf (k = 8) Then
        If Not (lblImage8.Visible) Then
            GetNextLabel = 8
            Exit For
        End If
    ElseIf (k = 9) Then
        If Not (lblImage9.Visible) Then
            GetNextLabel = 9
            Exit For
        End If
    ElseIf (k = 10) Then
        If Not (lblImage10.Visible) Then
            GetNextLabel = 10
            Exit For
        End If
    ElseIf (k = 11) Then
        If Not (lblImage11.Visible) Then
            GetNextLabel = 11
            Exit For
        End If
    ElseIf (k = 12) Then
        If Not (lblImage12.Visible) Then
            GetNextLabel = 12
            Exit For
        End If
    ElseIf (k = 13) Then
        If Not (lblImage13.Visible) Then
            GetNextLabel = 13
            Exit For
        End If
    ElseIf (k = 14) Then
        If Not (lblImage14.Visible) Then
            GetNextLabel = 14
            Exit For
        End If
    ElseIf (k = 15) Then
        If Not (lblImage15.Visible) Then
            GetNextLabel = 15
            Exit For
        End If
    ElseIf (k = 16) Then
        If Not (lblImage16.Visible) Then
            GetNextLabel = 16
            Exit For
        End If
    ElseIf (k = 17) Then
        If Not (lblImage17.Visible) Then
            GetNextLabel = 17
            Exit For
        End If
    ElseIf (k = 18) Then
        If Not (lblImage18.Visible) Then
            GetNextLabel = 18
            Exit For
        End If
    End If
Next k

    Exit Function

lGetNextLabel_Error:

    LogError "frmDrawNew", "GetNextLabel", Err, Err.Description
End Function

Private Function ResetLabel(Idx As Integer) As Boolean
Dim k As Integer
Dim p As Integer
Dim Temp As String
    On Error GoTo lResetLabel_Error

If (Idx > 0) Then
    If (TheImage(Idx).LabelId > 0) Then
        Temp = TheImage(Idx).BrushReference.BrushLingo
        k = TheImage(Idx).LabelId
        If (k = 1) Then
            lblImage1.Visible = False
            Line1.Visible = False
        ElseIf (k = 2) Then
            lblImage2.Visible = False
            Line2.Visible = False
        ElseIf (k = 3) Then
            lblImage3.Visible = False
            Line3.Visible = False
        ElseIf (k = 4) Then
            lblImage4.Visible = False
            Line4.Visible = False
        ElseIf (k = 5) Then
            lblImage5.Visible = False
            Line5.Visible = False
        ElseIf (k = 6) Then
            lblImage6.Visible = False
            Line6.Visible = False
        ElseIf (k = 7) Then
            lblImage7.Visible = False
            Line7.Visible = False
        ElseIf (k = 8) Then
            lblImage8.Visible = False
            Line8.Visible = False
        ElseIf (k = 9) Then
            lblImage9.Visible = False
            Line9.Visible = False
        ElseIf (k = 10) Then
            lblImage10.Visible = False
            Line10.Visible = False
        ElseIf (k = 11) Then
            lblImage11.Visible = False
            Line11.Visible = False
        ElseIf (k = 12) Then
            lblImage12.Visible = False
            Line12.Visible = False
        ElseIf (k = 13) Then
            lblImage13.Visible = False
            Line13.Visible = False
        ElseIf (k = 14) Then
            lblImage14.Visible = False
            Line14.Visible = False
        ElseIf (k = 15) Then
            lblImage15.Visible = False
            Line15.Visible = False
        ElseIf (k = 16) Then
            lblImage16.Visible = False
            Line16.Visible = False
        ElseIf (k = 17) Then
            lblImage17.Visible = False
            Line17.Visible = False
        ElseIf (k = 18) Then
            lblImage18.Visible = False
            Line18.Visible = False
        End If
        p = GetNextLabel(Idx)
        TheImage(Idx).LabelId = p
        Call DropLabel(Temp, Idx)
    End If
End If

    Exit Function

lResetLabel_Error:

    LogError "frmDrawNew", "ResetLabel", Err, Err.Description
End Function

Private Function FindALabel(Idx As Integer) As Integer
Dim k As Integer
    On Error GoTo lFindALabel_Error

FindALabel = 0
If (Idx > 0) Then
    For k = 1 To MaxImagesAllowed
        If (TheImage(k).LabelId = Idx) Then
            FindALabel = k
            Exit For
        End If
    Next k
End If

    Exit Function

lFindALabel_Error:

    LogError "frmDrawNew", "FindALabel", Err, Err.Description
End Function

Private Function GetNextOtherLabel(ES As String) As Integer
Dim k As Integer
Dim AStart As Integer
    On Error GoTo lGetNextOtherLabel_Error

GetNextOtherLabel = 0
AStart = 10
If (ES = "OS") Then
    AStart = 1
End If
For k = AStart To AStart + 8
    If (k = 1) Then
        If Not (lblImageA1.Visible) Then
            GetNextOtherLabel = 1
            Exit For
        End If
    ElseIf (k = 2) Then
        If Not (lblImageA2.Visible) Then
            GetNextOtherLabel = 2
            Exit For
        End If
    ElseIf (k = 3) Then
        If Not (lblImageA3.Visible) Then
            GetNextOtherLabel = 3
            Exit For
        End If
    ElseIf (k = 4) Then
        If Not (lblImageA4.Visible) Then
            GetNextOtherLabel = 4
            Exit For
        End If
    ElseIf (k = 5) Then
        If Not (lblImageA5.Visible) Then
            GetNextOtherLabel = 5
            Exit For
        End If
    ElseIf (k = 6) Then
        If Not (lblImageA6.Visible) Then
            GetNextOtherLabel = 6
            Exit For
        End If
    ElseIf (k = 7) Then
        If Not (lblImageA7.Visible) Then
            GetNextOtherLabel = 7
            Exit For
        End If
    ElseIf (k = 8) Then
        If Not (lblImageA8.Visible) Then
            GetNextOtherLabel = 8
            Exit For
        End If
    ElseIf (k = 9) Then
        If Not (lblImageA9.Visible) Then
            GetNextOtherLabel = 9
            Exit For
        End If
    ElseIf (k = 10) Then
        If Not (lblImageA10.Visible) Then
            GetNextOtherLabel = 10
            Exit For
        End If
    ElseIf (k = 11) Then
        If Not (lblImageA11.Visible) Then
            GetNextOtherLabel = 11
            Exit For
        End If
    ElseIf (k = 12) Then
        If Not (lblImageA12.Visible) Then
            GetNextOtherLabel = 12
            Exit For
        End If
    ElseIf (k = 13) Then
        If Not (lblImageA13.Visible) Then
            GetNextOtherLabel = 13
            Exit For
        End If
    ElseIf (k = 14) Then
        If Not (lblImageA14.Visible) Then
            GetNextOtherLabel = 14
            Exit For
        End If
    ElseIf (k = 15) Then
        If Not (lblImageA15.Visible) Then
            GetNextOtherLabel = 15
            Exit For
        End If
    ElseIf (k = 16) Then
        If Not (lblImageA16.Visible) Then
            GetNextOtherLabel = 16
            Exit For
        End If
    ElseIf (k = 17) Then
        If Not (lblImageA17.Visible) Then
            GetNextOtherLabel = 17
            Exit For
        End If
    ElseIf (k = 18) Then
        If Not (lblImageA18.Visible) Then
            GetNextOtherLabel = 18
            Exit For
        End If
    End If
Next k

    Exit Function

lGetNextOtherLabel_Error:

    LogError "frmDrawNew", "GetNextOtherLabel", Err, Err.Description
End Function

Private Function ResetOtherLabels() As Boolean
Dim p As Integer, j As Integer
Dim GH As Single, GW As Single
Dim GT As Single, GL As Single
Dim BRN As String
    On Error GoTo lResetOtherLabels_Error

If (lblImageA1.Caption <> "Label1") Then
    LineA1.Visible = StartLinesOn
    lblImageA1.Visible = True
    imgImageA1.ZOrder 0
    If (imgImageA1.Tag <> "") Then
        p = InStrPS(imgImageA1.Tag, ",")
        GH = Val(Left(imgImageA1.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA1.Tag, ",")
        GW = Val(Mid(imgImageA1.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA1.Tag, ",")
        GT = Val(Mid(imgImageA1.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA1.Tag, p + 1, Len(imgImageA1.Tag) - p))
        BRN = Trim(imgImageA1.ToolTipText)
        If (BRN = "F") Then
            imgImageA1.Visible = True
        End If
        imgImageA1.Height = GH
        imgImageA1.Width = GW
        imgImageA1.Left = GL
        imgImageA1.Top = GT
    End If
    If (InStrPS(lblImageA1.Caption, "Cup to Disc") <> 0) Then
        LineA1.Visible = False
    End If
    If (InStrPS(lblImageA1.Caption, "Goniscopy") <> 0) Then
        LineA1.Visible = False
    End If
End If
If (lblImageA2.Caption <> "Label1") Then
    LineA2.Visible = StartLinesOn
    lblImageA2.Visible = True
    imgImageA2.ZOrder 0
    If (imgImageA2.Tag <> "") Then
        p = InStrPS(imgImageA2.Tag, ",")
        GH = Val(Left(imgImageA2.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA2.Tag, ",")
        GW = Val(Mid(imgImageA2.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA2.Tag, ",")
        GT = Val(Mid(imgImageA2.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA2.Tag, p + 1, Len(imgImageA2.Tag) - p))
        BRN = Trim(imgImageA2.ToolTipText)
        If (BRN = "F") Then
            imgImageA2.Visible = True
        End If
        imgImageA2.Height = GH
        imgImageA2.Width = GW
        imgImageA2.Left = GL
        imgImageA2.Top = GT
    End If
    If (InStrPS(lblImageA2.Caption, "Cup to Disc") <> 0) Then
        LineA2.Visible = False
    End If
    If (InStrPS(lblImageA2.Caption, "Goniscopy") <> 0) Then
        LineA2.Visible = False
    End If
End If
If (lblImageA3.Caption <> "Label1") Then
    LineA3.Visible = StartLinesOn
    lblImageA3.Visible = True
    imgImageA3.ZOrder 0
    If (imgImageA3.Tag <> "") Then
        p = InStrPS(imgImageA3.Tag, ",")
        GH = Val(Left(imgImageA3.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA3.Tag, ",")
        GW = Val(Mid(imgImageA3.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA3.Tag, ",")
        GT = Val(Mid(imgImageA3.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA3.Tag, p + 1, Len(imgImageA3.Tag) - p))
        BRN = Trim(imgImageA3.ToolTipText)
        If (BRN = "F") Then
            imgImageA3.Visible = True
        End If
        imgImageA3.Height = GH
        imgImageA3.Width = GW
        imgImageA3.Left = GL
        imgImageA3.Top = GT
    End If
    If (InStrPS(lblImageA3.Caption, "Cup to Disc") <> 0) Then
        LineA3.Visible = False
    End If
    If (InStrPS(lblImageA3.Caption, "Goniscopy") <> 0) Then
        LineA3.Visible = False
    End If
End If
If (lblImageA4.Caption <> "Label1") Then
    LineA4.Visible = StartLinesOn
    lblImageA4.Visible = True
    imgImageA4.ZOrder 0
    If (imgImageA4.Tag <> "") Then
        p = InStrPS(imgImageA4.Tag, ",")
        GH = Val(Left(imgImageA4.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA4.Tag, ",")
        GW = Val(Mid(imgImageA4.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA4.Tag, ",")
        GT = Val(Mid(imgImageA4.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA4.Tag, p + 1, Len(imgImageA4.Tag) - p))
        BRN = Trim(imgImageA4.ToolTipText)
        If (BRN = "F") Then
            imgImageA4.Visible = True
        End If
        imgImageA4.Height = GH
        imgImageA4.Width = GW
        imgImageA4.Left = GL
        imgImageA4.Top = GT
    End If
    If (InStrPS(lblImageA4.Caption, "Cup to Disc") <> 0) Then
        LineA4.Visible = False
    End If
    If (InStrPS(lblImageA4.Caption, "Goniscopy") <> 0) Then
        LineA4.Visible = False
    End If
End If
If (lblImageA5.Caption <> "Label1") Then
    LineA5.Visible = StartLinesOn
    lblImageA5.Visible = True
    imgImageA5.ZOrder 0
    If (imgImageA5.Tag <> "") Then
        p = InStrPS(imgImageA5.Tag, ",")
        GH = Val(Left(imgImageA5.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA5.Tag, ",")
        GW = Val(Mid(imgImageA5.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA5.Tag, ",")
        GT = Val(Mid(imgImageA5.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA5.Tag, p + 1, Len(imgImageA5.Tag) - p))
        BRN = Trim(imgImageA5.ToolTipText)
        If (BRN = "F") Then
            imgImageA5.Visible = True
        End If
        imgImageA5.Height = GH
        imgImageA5.Width = GW
        imgImageA5.Left = GL
        imgImageA5.Top = GT
    End If
    If (InStrPS(lblImageA5.Caption, "Cup to Disc") <> 0) Then
        LineA5.Visible = False
    End If
    If (InStrPS(lblImageA5.Caption, "Goniscopy") <> 0) Then
        LineA5.Visible = False
    End If
End If
If (lblImageA6.Caption <> "Label1") Then
    LineA6.Visible = StartLinesOn
    lblImageA6.Visible = True
    imgImageA6.ZOrder 0
    If (imgImageA6.Tag <> "") Then
        p = InStrPS(imgImageA6.Tag, ",")
        GH = Val(Left(imgImageA6.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA6.Tag, ",")
        GW = Val(Mid(imgImageA6.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA6.Tag, ",")
        GT = Val(Mid(imgImageA6.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA6.Tag, p + 1, Len(imgImageA6.Tag) - p))
        BRN = Trim(imgImageA6.ToolTipText)
        If (BRN = "F") Then
            imgImageA6.Visible = True
        End If
        imgImageA6.Height = GH
        imgImageA6.Width = GW
        imgImageA6.Left = GL
        imgImageA6.Top = GT
    End If
    If (InStrPS(lblImageA6.Caption, "Cup to Disc") <> 0) Then
        LineA6.Visible = False
    End If
    If (InStrPS(lblImageA6.Caption, "Goniscopy") <> 0) Then
        LineA6.Visible = False
    End If
End If
If (lblImageA7.Caption <> "Label1") Then
    LineA7.Visible = StartLinesOn
    lblImageA7.Visible = True
    imgImageA7.ZOrder 0
    If (imgImageA7.Tag <> "") Then
        p = InStrPS(imgImageA7.Tag, ",")
        GH = Val(Left(imgImageA7.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA7.Tag, ",")
        GW = Val(Mid(imgImageA7.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA7.Tag, ",")
        GT = Val(Mid(imgImageA7.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA7.Tag, p + 1, Len(imgImageA7.Tag) - p))
        BRN = Trim(imgImageA7.ToolTipText)
        If (BRN = "F") Then
            imgImageA7.Visible = True
        End If
        imgImageA7.Height = GH
        imgImageA7.Width = GW
        imgImageA7.Left = GL
        imgImageA7.Top = GT
    End If
    If (InStrPS(lblImageA7.Caption, "Cup to Disc") <> 0) Then
        LineA7.Visible = False
    End If
    If (InStrPS(lblImageA7.Caption, "Goniscopy") <> 0) Then
        LineA7.Visible = False
    End If
End If
If (lblImageA8.Caption <> "Label1") Then
    LineA8.Visible = StartLinesOn
    lblImageA8.Visible = True
    imgImageA8.ZOrder 0
    If (imgImageA8.Tag <> "") Then
        p = InStrPS(imgImageA8.Tag, ",")
        GH = Val(Left(imgImageA8.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA8.Tag, ",")
        GW = Val(Mid(imgImageA8.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA8.Tag, ",")
        GT = Val(Mid(imgImageA8.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA8.Tag, p + 1, Len(imgImageA8.Tag) - p))
        BRN = Trim(imgImageA8.ToolTipText)
        If (BRN = "F") Then
            imgImageA8.Visible = True
        End If
        imgImageA8.Height = GH
        imgImageA8.Width = GW
        imgImageA8.Left = GL
        imgImageA8.Top = GT
    End If
    If (InStrPS(lblImageA8.Caption, "Cup to Disc") <> 0) Then
        LineA8.Visible = False
    End If
    If (InStrPS(lblImageA8.Caption, "Goniscopy") <> 0) Then
        LineA8.Visible = False
    End If
End If
If (lblImageA9.Caption <> "Label1") Then
    LineA9.Visible = StartLinesOn
    lblImageA9.Visible = True
    imgImageA9.ZOrder 0
    If (imgImageA9.Tag <> "") Then
        p = InStrPS(imgImageA9.Tag, ",")
        GH = Val(Left(imgImageA9.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA9.Tag, ",")
        GW = Val(Mid(imgImageA9.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA9.Tag, ",")
        GT = Val(Mid(imgImageA9.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA9.Tag, p + 1, Len(imgImageA9.Tag) - p))
        BRN = Trim(imgImageA9.ToolTipText)
        If (BRN = "F") Then
            imgImageA9.Visible = True
        End If
        imgImageA9.Height = GH
        imgImageA9.Width = GW
        imgImageA9.Left = GL
        imgImageA9.Top = GT
    End If
    If (InStrPS(lblImageA9.Caption, "Cup to Disc") <> 0) Then
        LineA9.Visible = False
    End If
    If (InStrPS(lblImageA9.Caption, "Goniscopy") <> 0) Then
        LineA9.Visible = False
    End If
End If
If (lblImageA10.Caption <> "Label1") Then
    LineA10.Visible = StartLinesOn
    lblImageA10.Visible = True
    imgImageA10.ZOrder 0
    If (imgImageA10.Tag <> "") Then
        p = InStrPS(imgImageA10.Tag, ",")
        GH = Val(Left(imgImageA10.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA10.Tag, ",")
        GW = Val(Mid(imgImageA10.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA10.Tag, ",")
        GT = Val(Mid(imgImageA10.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA10.Tag, p + 1, Len(imgImageA10.Tag) - p))
        BRN = Trim(imgImageA10.ToolTipText)
        If (BRN = "F") Then
            imgImageA10.Visible = True
        End If
        imgImageA10.Height = GH
        imgImageA10.Width = GW
        imgImageA10.Left = GL
        imgImageA10.Top = GT
    End If
    If (InStrPS(lblImageA10.Caption, "Cup to Disc") <> 0) Then
        LineA10.Visible = False
    End If
    If (InStrPS(lblImageA10.Caption, "Goniscopy") <> 0) Then
        LineA10.Visible = False
    End If
End If
If (lblImageA11.Caption <> "Label1") Then
    LineA11.Visible = StartLinesOn
    lblImageA11.Visible = True
    imgImageA11.ZOrder 0
    If (imgImageA11.Tag <> "") Then
        p = InStrPS(imgImageA11.Tag, ",")
        GH = Val(Left(imgImageA11.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA11.Tag, ",")
        GW = Val(Mid(imgImageA11.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA11.Tag, ",")
        GT = Val(Mid(imgImageA11.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA11.Tag, p + 1, Len(imgImageA11.Tag) - p))
        BRN = Trim(imgImageA11.ToolTipText)
        If (BRN = "F") Then
            imgImageA11.Visible = True
        End If
        imgImageA11.Height = GH
        imgImageA11.Width = GW
        imgImageA11.Left = GL
        imgImageA11.Top = GT
    End If
    If (InStrPS(lblImageA11.Caption, "Cup to Disc") <> 0) Then
        LineA11.Visible = False
    End If
    If (InStrPS(lblImageA11.Caption, "Goniscopy") <> 0) Then
        LineA11.Visible = False
    End If
End If
If (lblImageA12.Caption <> "Label1") Then
    LineA12.Visible = StartLinesOn
    lblImageA12.Visible = True
    imgImageA12.ZOrder 0
    If (imgImageA12.Tag <> "") Then
        p = InStrPS(imgImageA12.Tag, ",")
        GH = Val(Left(imgImageA12.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA12.Tag, ",")
        GW = Val(Mid(imgImageA12.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA12.Tag, ",")
        GT = Val(Mid(imgImageA12.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA12.Tag, p + 1, Len(imgImageA12.Tag) - p))
        BRN = Trim(imgImageA12.ToolTipText)
        If (BRN = "F") Then
            imgImageA12.Visible = True
        End If
        imgImageA12.Height = GH
        imgImageA12.Width = GW
        imgImageA12.Left = GL
        imgImageA12.Top = GT
    End If
    If (InStrPS(lblImageA12.Caption, "Cup to Disc") <> 0) Then
        LineA12.Visible = False
    End If
    If (InStrPS(lblImageA12.Caption, "Goniscopy") <> 0) Then
        LineA12.Visible = False
    End If
End If
If (lblImageA13.Caption <> "Label1") Then
    LineA13.Visible = StartLinesOn
    lblImageA13.Visible = True
    imgImageA13.ZOrder 0
    If (imgImageA13.Tag <> "") Then
        p = InStrPS(imgImageA13.Tag, ",")
        GH = Val(Left(imgImageA13.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA13.Tag, ",")
        GW = Val(Mid(imgImageA13.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA13.Tag, ",")
        GT = Val(Mid(imgImageA13.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA13.Tag, p + 1, Len(imgImageA13.Tag) - p))
        BRN = Trim(imgImageA13.ToolTipText)
        If (BRN = "F") Then
            imgImageA13.Visible = True
        End If
        imgImageA13.Height = GH
        imgImageA13.Width = GW
        imgImageA13.Left = GL
        imgImageA13.Top = GT
    End If
    If (InStrPS(lblImageA13.Caption, "Cup to Disc") <> 0) Then
        LineA13.Visible = False
    End If
    If (InStrPS(lblImageA13.Caption, "Goniscopy") <> 0) Then
        LineA13.Visible = False
    End If
End If
If (lblImageA14.Caption <> "Label1") Then
    LineA14.Visible = StartLinesOn
    lblImageA14.Visible = True
    imgImageA14.ZOrder 0
    If (imgImageA14.Tag <> "") Then
        p = InStrPS(imgImageA14.Tag, ",")
        GH = Val(Left(imgImageA14.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA14.Tag, ",")
        GW = Val(Mid(imgImageA14.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA14.Tag, ",")
        GT = Val(Mid(imgImageA14.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA14.Tag, p + 1, Len(imgImageA14.Tag) - p))
        BRN = Trim(imgImageA14.ToolTipText)
        If (BRN = "F") Then
            imgImageA14.Visible = True
        End If
        imgImageA14.Height = GH
        imgImageA14.Width = GW
        imgImageA14.Left = GL
        imgImageA14.Top = GT
    End If
    If (InStrPS(lblImageA14.Caption, "Cup to Disc") <> 0) Then
        LineA14.Visible = False
    End If
    If (InStrPS(lblImageA14.Caption, "Goniscopy") <> 0) Then
        LineA14.Visible = False
    End If
End If
If (lblImageA15.Caption <> "Label1") Then
    LineA15.Visible = StartLinesOn
    lblImageA15.Visible = True
    imgImageA15.ZOrder 0
    If (imgImageA15.Tag <> "") Then
        p = InStrPS(imgImageA15.Tag, ",")
        GH = Val(Left(imgImageA15.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA15.Tag, ",")
        GW = Val(Mid(imgImageA15.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA15.Tag, ",")
        GT = Val(Mid(imgImageA15.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA15.Tag, p + 1, Len(imgImageA15.Tag) - p))
        BRN = Trim(imgImageA15.ToolTipText)
        If (BRN = "F") Then
            imgImageA15.Visible = True
        End If
        imgImageA15.Height = GH
        imgImageA15.Width = GW
        imgImageA15.Left = GL
        imgImageA15.Top = GT
    End If
    If (InStrPS(lblImageA15.Caption, "Cup to Disc") <> 0) Then
        LineA15.Visible = False
    End If
    If (InStrPS(lblImageA15.Caption, "Goniscopy") <> 0) Then
        LineA15.Visible = False
    End If
End If
If (lblImageA16.Caption <> "Label1") Then
    LineA16.Visible = StartLinesOn
    lblImageA16.Visible = True
    imgImageA16.ZOrder 0
    If (imgImageA16.Tag <> "") Then
        p = InStrPS(imgImageA16.Tag, ",")
        GH = Val(Left(imgImageA16.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA16.Tag, ",")
        GW = Val(Mid(imgImageA16.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA16.Tag, ",")
        GT = Val(Mid(imgImageA16.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA16.Tag, p + 1, Len(imgImageA16.Tag) - p))
        BRN = Trim(imgImageA16.ToolTipText)
        If (BRN = "F") Then
            imgImageA16.Visible = True
        End If
        imgImageA16.Height = GH
        imgImageA16.Width = GW
        imgImageA16.Left = GL
        imgImageA16.Top = GT
    End If
    If (InStrPS(lblImageA16.Caption, "Cup to Disc") <> 0) Then
        LineA16.Visible = False
    End If
    If (InStrPS(lblImageA16.Caption, "Goniscopy") <> 0) Then
        LineA16.Visible = False
    End If
End If
If (lblImageA17.Caption <> "Label1") Then
    LineA17.Visible = StartLinesOn
    lblImageA17.Visible = True
    imgImageA17.ZOrder 0
    If (imgImageA17.Tag <> "") Then
        p = InStrPS(imgImageA17.Tag, ",")
        GH = Val(Left(imgImageA17.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA17.Tag, ",")
        GW = Val(Mid(imgImageA17.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA17.Tag, ",")
        GT = Val(Mid(imgImageA17.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA17.Tag, p + 1, Len(imgImageA17.Tag) - p))
        BRN = Trim(imgImageA17.ToolTipText)
        If (BRN = "F") Then
            imgImageA17.Visible = True
        End If
        imgImageA17.Height = GH
        imgImageA17.Width = GW
        imgImageA17.Left = GL
        imgImageA17.Top = GT
    End If
    If (InStrPS(lblImageA17.Caption, "Cup to Disc") <> 0) Then
        LineA17.Visible = False
    End If
    If (InStrPS(lblImageA17.Caption, "Goniscopy") <> 0) Then
        LineA17.Visible = False
    End If
End If
If (lblImageA18.Caption <> "Label1") Then
    LineA18.Visible = StartLinesOn
    lblImageA18.Visible = True
    imgImageA18.ZOrder 0
    If (imgImageA18.Tag <> "") Then
        p = InStrPS(imgImageA18.Tag, ",")
        GH = Val(Left(imgImageA18.Tag, p - 1))
        j = InStrPS(p + 1, imgImageA18.Tag, ",")
        GW = Val(Mid(imgImageA18.Tag, p + 1, (j - 1) - p))
        p = InStrPS(j + 1, imgImageA18.Tag, ",")
        GT = Val(Mid(imgImageA18.Tag, j + 1, (p - 1) - j))
        GL = Val(Mid(imgImageA18.Tag, p + 1, Len(imgImageA18.Tag) - p))
        BRN = Trim(imgImageA18.ToolTipText)
        If (BRN = "F") Then
            imgImageA18.Visible = True
        End If
        imgImageA18.Height = GH
        imgImageA18.Width = GW
        imgImageA18.Left = GL
        imgImageA18.Top = GT
    End If
    If (InStrPS(lblImageA18.Caption, "Cup to Disc") <> 0) Then
        LineA18.Visible = False
    End If
    If (InStrPS(lblImageA18.Caption, "Goniscopy") <> 0) Then
        LineA18.Visible = False
    End If
End If

    Exit Function

lResetOtherLabels_Error:

    LogError "frmDrawNew", "ResetOtherLabels", Err, Err.Description
End Function

Private Function SetLineVisible(LineId As Integer, IHow As Boolean) As Boolean
    On Error GoTo lSetLineVisible_Error

If (LineId = 1) Then
    Line1.Visible = IHow
ElseIf (LineId = 2) Then
    Line2.Visible = IHow
ElseIf (LineId = 2) Then
    Line2.Visible = IHow
ElseIf (LineId = 3) Then
    Line3.Visible = IHow
ElseIf (LineId = 4) Then
    Line4.Visible = IHow
ElseIf (LineId = 5) Then
    Line5.Visible = IHow
ElseIf (LineId = 6) Then
    Line6.Visible = IHow
ElseIf (LineId = 7) Then
    Line7.Visible = IHow
ElseIf (LineId = 8) Then
    Line8.Visible = IHow
ElseIf (LineId = 9) Then
    Line9.Visible = IHow
ElseIf (LineId = 10) Then
    Line10.Visible = IHow
ElseIf (LineId = 11) Then
    Line11.Visible = IHow
ElseIf (LineId = 12) Then
    Line12.Visible = IHow
ElseIf (LineId = 13) Then
    Line13.Visible = IHow
ElseIf (LineId = 14) Then
    Line14.Visible = IHow
ElseIf (LineId = 15) Then
    Line15.Visible = IHow
ElseIf (LineId = 16) Then
    Line16.Visible = IHow
ElseIf (LineId = 17) Then
    Line17.Visible = IHow
ElseIf (LineId = 18) Then
    Line18.Visible = IHow
End If

    Exit Function

lSetLineVisible_Error:

    LogError "frmDrawNew", "SetLineVisible", Err, Err.Description
End Function

Private Function IsLineVisible(LineId As Integer) As Boolean
    On Error GoTo lIsLineVisible_Error

If (LineId = 1) Then
    IsLineVisible = Line1.Visible
ElseIf (LineId = 2) Then
    IsLineVisible = Line2.Visible
ElseIf (LineId = 3) Then
    IsLineVisible = Line3.Visible
ElseIf (LineId = 4) Then
    IsLineVisible = Line4.Visible
ElseIf (LineId = 5) Then
    IsLineVisible = Line5.Visible
ElseIf (LineId = 6) Then
    IsLineVisible = Line6.Visible
ElseIf (LineId = 7) Then
    IsLineVisible = Line7.Visible
ElseIf (LineId = 8) Then
    IsLineVisible = Line8.Visible
ElseIf (LineId = 9) Then
    IsLineVisible = Line9.Visible
ElseIf (LineId = 10) Then
    IsLineVisible = Line10.Visible
ElseIf (LineId = 11) Then
    IsLineVisible = Line11.Visible
ElseIf (LineId = 12) Then
    IsLineVisible = Line12.Visible
ElseIf (LineId = 13) Then
    IsLineVisible = Line13.Visible
ElseIf (LineId = 14) Then
    IsLineVisible = Line14.Visible
ElseIf (LineId = 15) Then
    IsLineVisible = Line15.Visible
ElseIf (LineId = 16) Then
    IsLineVisible = Line16.Visible
ElseIf (LineId = 17) Then
    IsLineVisible = Line17.Visible
ElseIf (LineId = 18) Then
    IsLineVisible = Line18.Visible
End If

    Exit Function

lIsLineVisible_Error:

    LogError "frmDrawNew", "IsLineVisible", Err, Err.Description
End Function

Private Function BurnImage(Idx As Integer, AImage As FXImage) As Boolean
Dim IBurn As Boolean
Dim ap As Long
Dim QRef As Integer, ScaleFactor As Integer
Dim IRef As Integer, PRef As Integer
Dim i As Integer, j As Integer
Dim p As Single, q As Single
    On Error GoTo lBurnImage_Error

IBurn = False
If (Idx > 0) Then
    PRef = 0
    QRef = 0
    ScaleFactor = TheImage(Idx).Size
    If (ScaleFactor < 1) Then
        ScaleFactor = 1
    End If
    imgStandard.AutoSize = ISIZE_ResizePreserveAspect
    If ((GlobalHeight / ScaleFactor) > 4000) Then
        imgStandard.Height = GlobalHeight
        imgStandard.Width = GlobalWidth
        IRef = 1
    ElseIf ((GlobalHeight / ScaleFactor) > 2000) Then
        imgStandard.Height = GlobalHeight
        imgStandard.Width = GlobalWidth
        IRef = 1.75
        PRef = -300
        QRef = -300
    ElseIf ((GlobalHeight / ScaleFactor) > 800) Then
        imgStandard.Height = GlobalHeight
        imgStandard.Width = GlobalWidth
        IRef = 1.75
        PRef = -300
        QRef = -300
    ElseIf ((GlobalHeight / ScaleFactor) >= 400) Then
        imgStandard.Height = GlobalHeight * 4
        imgStandard.Width = GlobalWidth * 4
        IRef = 3
        If (ScaleFactor > 1) Then
            IRef = IRef - (ScaleFactor + 2)
            If (IRef < 1) Then
                IRef = 1
            End If
        End If
    ElseIf ((GlobalHeight / ScaleFactor) < 250) And ((GlobalHeight / ScaleFactor) > 200) Then
        imgStandard.Height = GlobalHeight * 2
        imgStandard.Width = GlobalWidth * 2
        IRef = 2
        If (ScaleFactor > 1) Then
            IRef = IRef - (ScaleFactor + 2)
            If (IRef < 1) Then
                IRef = 1
            End If
        End If
    Else
        imgStandard.Height = GlobalHeight * 8
        imgStandard.Width = GlobalWidth * 8
        IRef = 6
        If (ScaleFactor > 1) Then
            IRef = IRef - (ScaleFactor + 2)
            If (IRef < 1) Then
                IRef = 1
            End If
        End If
    End If
    imgStandard.ShowRotated = TheImage(Idx).Rotate
    imgStandard.picture = FM.LoadPicture(AImage.FileName)
'    imgStandard.Visible = True
    For i = 0 To Int(imgStandard.Width / XTwips)
        For j = 0 To Int(imgStandard.Height / YTwips)
            ap = imgStandard.GetImagePixel(i, j)
            If (ap <> 16777215) And (ap > -1) Then
                p = ((TheImage(Idx).Left - FxImage1.Left + PRef) / XTwips) + Int(i / IRef)
                q = ((TheImage(Idx).Top - FxImage1.Top + QRef) / YTwips) + Int(j / IRef)
                FxImage1.SetImagePixel p, q, ap
                IBurn = True
            End If
        Next j
    Next i
    imgStandard.Visible = False
End If
If Not (TheImage(Idx).Burn) Then
    TheImage(Idx).Burn = IBurn
End If
If (IBurn) Then
    TheImage(Idx).Burn = True
    FxImage1.CaptureNow
    Call SaveNewDrawFileName(DrawFileName, False)
    AImage.Visible = False
End If

    Exit Function

lBurnImage_Error:

    LogError "frmDrawNew", "BurnImage", Err, Err.Description
End Function

Private Sub BurnTheImage()
    On Error GoTo lBurnTheImage_Error

    Select Case CurrentImageId
        Case 1
            BurnImage CurrentImageId, imgImage1
        Case 2
            BurnImage CurrentImageId, imgImage2
        Case 3
            BurnImage CurrentImageId, imgImage3
        Case 4
            BurnImage CurrentImageId, imgImage4
        Case 5
            BurnImage CurrentImageId, imgImage5
        Case 6
            BurnImage CurrentImageId, imgImage6
        Case 7
            BurnImage CurrentImageId, imgImage7
        Case 8
            BurnImage CurrentImageId, imgImage8
        Case 9
            BurnImage CurrentImageId, imgImage9
        Case 10
            BurnImage CurrentImageId, imgImage10
        Case 11
            BurnImage CurrentImageId, imgImage11
        Case 12
            BurnImage CurrentImageId, imgImage12
        Case 13
            BurnImage CurrentImageId, imgImage13
        Case 14
            BurnImage CurrentImageId, imgImage14
        Case 15
            BurnImage CurrentImageId, imgImage15
        Case 16
            BurnImage CurrentImageId, imgImage16
        Case 17
            BurnImage CurrentImageId, imgImage17
        Case 18
            BurnImage CurrentImageId, imgImage18
    End Select
    PrevImageId = CurrentImageId
    CurrentImageId = 0

    Exit Sub

lBurnTheImage_Error:

    LogError "frmDrawNew", "BurnTheImage", Err, Err.Description
End Sub

Private Sub SaveNewDrawFileName(TheFile As String, ReloadOn As Boolean)
On Error GoTo OuttaHere
Dim Retry As Integer
Dim NewFile As String
Retry = 1
If (Trim(TheFile) <> "") Then
    NewFile = TheFile
    Call NextImageName(NewFile)
    If (FM.IsFileThere(NewFile)) Then
        FM.Kill NewFile
    End If
    Call FxImage1.CaptureNow
AnotherAttempt:
    FM.SavePicture FxImage1.picture, Trim(NewFile)
    TheFile = NewFile
    Call SetNewDrawFile(NewFile, False)
    If (ReloadOn) Then
        Call ReloadForm(StoreResults)
    End If
End If
Exit Sub
OuttaHere:
    If (Err = 380) Then
        Retry = Retry + 1
        If (Retry < 3) Then
            Resume AnotherAttempt
        Else
'            MsgBox "Serious Problem Saving Image File, call 212-844-0105 !!!!"
        End If
    End If
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub SetNewDrawFile(TheFile As String, NewOn As Boolean)
    On Error GoTo lSetNewDrawFile_Error

If (Trim(TheFile) <> "") Then
    If (Trim(ReplaceBase) = "") Then
        If (NewOn) Then
            If (InStrPS(TheFile, "I-") = 0) Then
                FM.FileCopy ImageDir + "ClearSheet" + BaseExt, TheFile
            Else
                If (Trim(BaseImage + BaseImageRef) <> "") Then
                    FM.FileCopy ImageDir + BaseImage + BaseImageRef + BaseExt, TheFile
                Else
                    FM.FileCopy ImageDir + "ClearSheet" + BaseExt, TheFile
                End If
            End If
        Else
            If Not (FM.IsFileThere(TheFile)) Then
                If Not (frmSystems1.EditPreviousOn) Then
                    If (Trim(BaseImage + BaseImageRef) <> "") Then
                        FM.FileCopy ImageDir + BaseImage + BaseImageRef + BaseExt, TheFile
                    Else
                        FM.FileCopy ImageDir + "ClearSheet" + BaseExt, TheFile
                    End If
                End If
            End If
        End If
    End If
    If (Trim(ReplaceBase) = "") Then
        FxImage1.Width = 462 * XTwips
        FxImage1.Height = 351 * YTwips
        FxImage1.AutoSize = ISIZE_ResizeImageToControl
        FxImage1.Tag = TheFile
        FxImage1.Visible = False
        FxImage1.Enabled = True
        FxImage1.ShowRotated = ROTATE_None
        FxImage1.Transparent = False
        FxImage1.TransparentColor = 65535
        FxImage1.DrawMode = PEN_MaskPen
        FxImage1.DrawWidth = PencilWidth
        FxImage1.DrawStyle = STYLE_Solid
        FxImage1.picture = FM.LoadPicture(TheFile)
        FxImage1.Refresh
'        FxImage1.Capture = ICAP_CaptureNow
'        FxImage1.Update = True
'        FxImage1.CaptureNow
    End If
    cmdImageDraw.Visible = False
    cmdScaleUp.Visible = False
    cmdScaleDown.Visible = False
    cmdRotate.Visible = False
    cmdPalette.Visible = True
    FxImage1.Visible = True
End If

    Exit Sub

lSetNewDrawFile_Error:

    LogError "frmDrawNew", "SetNewDrawFile", Err, Err.Description
End Sub

Private Sub SetNewDrawFileName(TheFile As String, DrawType As Boolean, IFound As Boolean)
Dim TempName As String
Dim Ref As String
    On Error GoTo lSetNewDrawFileName_Error

If (DrawType) Then
    Ref = "I-"
Else
    Ref = ""
End If
If (Trim(TheFile) = "") Then
    If (CurrentBrushIndex >= 0) Then
        TempName = LocalMyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-B" + Ref + BaseImage + BaseImageRef + DrawExt
        IFound = FM.IsFileThere(TempName)
        TheFile = TempName
    End If
Else
    If (Trim(ReplaceBase) = "") Then
        TheFile = LocalMyImageDir + "MyDraw-A" + Trim(str(AppointmentId)) + "-B" + Ref + TheFile + DrawExt
        IFound = FM.IsFileThere(TheFile)
    Else
        TheFile = ReplaceBase
        IFound = FM.IsFileThere(TheFile)
    End If
End If

    Exit Sub

lSetNewDrawFileName_Error:

    LogError "frmDrawNew", "SetNewDrawFileName", Err, Err.Description
End Sub

Private Function UpdatePrimaryImage() As Boolean
Dim ATime As Integer
Dim j As Integer, k As Integer
Dim AFile As String, BFile As String
Dim GFile As String, MyFile As String
Dim MyTime As String, NewFile As String
    On Error GoTo lUpdatePrimaryImage_Error

If (Trim(FxImage1.Tag) <> "") Then
    AFile = Trim(FxImage1.Tag)
    If (ReplaceBase <> "") Then
        If (AFile = ReplaceBase) Then
            AFile = DrawFileName
        End If
    End If
    If (FM.IsFileThere(AFile)) Then
        Call FxImage1.CaptureNow
        FM.SavePicture FxImage1.picture, Trim(AFile)
        k = InStrPS(AFile, "--")
        If (k > 0) Then
            j = InStrPS(k, AFile, ".")
            If (j > 0) Then
                BFile = Left(AFile, k - 1)
                If (FM.IsFileThere(BFile + DrawExt)) Then
                    FM.Kill BFile + DrawExt
                End If
                If Not (FM.IsFileThere(BFile + DrawExt)) Then
                    FM.Name AFile, BFile + DrawExt
                    ' If path is local -> push drawing to server
                    If (InStr(1, BFile, LocalMyImageDir) = 1) Then
                        GFile = Right(BFile, Len(BFile) - Len(LocalMyImageDir)) + DrawExt
                        GFile = MyImageDir + GFile
                        If (BFile + DrawExt <> GFile) Then
                            Call PushDrawImageToServer(BFile + DrawExt, GFile)
                        End If
                    End If
                End If
            End If
        Else
            j = InStrPS(AFile, ".")
            If (j > 0) Then
                BFile = Left(AFile, j - 1)
            End If
        End If
    End If
    If (Trim(BFile) <> "") Then
        MyTime = ""
        Call FormatTimeNow(MyTime)
        ATime = ConvertTimeToMinutes(MyTime)
        MyTime = Trim(str(ATime))
        AFile = BFile + "--*" + DrawExt
        MyFile = Dir(AFile)
        While (MyFile <> "")
            If (FM.IsFileThere(LocalMyImageDir + MyFile)) Then
                NewFile = MyFile
                Call ReplaceCharacters(NewFile, "--", "__")
                NewFile = NewFile + "-00" + MyTime
                If (FM.IsFileThere(LocalMyImageDir + NewFile)) Then
                    FM.Kill LocalMyImageDir + NewFile
                End If
                FM.Name LocalMyImageDir + MyFile, LocalMyImageDir + NewFile
'                Kill LocalMyImageDir + MyFile
            End If
            MyFile = Dir
        Wend
    End If
End If

    Exit Function

lUpdatePrimaryImage_Error:

    LogError "frmDrawNew", "UpdatePrimaryImage", Err, Err.Description
End Function

Private Function PrevImageName(DrawName As String) As Boolean
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim CurrFile As String
Dim TempFile As String
    On Error GoTo lPrevImageName_Error

k = 0
CurrFile = ""
TempFile = ""
PrevImageName = False
If (Trim(DrawName) <> "") Then
    i = InStrPS(DrawName, ".")
    If (i > 0) Then
        TempFile = Left(DrawName, i + 3)
        j = InStrPS(i + 1, DrawName, "-")
        If (j > 0) Then
            CurrFile = Mid(DrawName, j, Len(DrawName) - (j - 1))
            k = Val(Mid(CurrFile, 2, Len(CurrFile) - 1))
            k = TotalImagesSelected
        End If
    End If
End If
If (k > 0) Then
    DrawName = TempFile + "-" + Trim(str(k))
Else
    DrawName = TempFile
End If
PrevImageName = True

    Exit Function

lPrevImageName_Error:

    LogError "frmDrawNew", "PrevImageName", Err, Err.Description
End Function

Private Function NextImageName(DrawName As String) As Boolean
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim TempFile As String
    On Error GoTo lNextImageName_Error

k = 0
TempFile = ""
NextImageName = False
If (Trim(DrawName) <> "") Then
    i = InStrPS(DrawName, "--")
    If (i > 0) Then
        j = InStrPS(i, DrawName, ".")
        If (j > 0) Then
            k = Val(Mid(DrawName, i + 2, (j - 1) - (i + 1)))
            TempFile = Left(DrawName, i - 1)
        End If
    Else
        j = InStrPS(DrawName, ".")
        If (j > 0) Then
            TempFile = Left(DrawName, j - 1)
        End If
    End If
    k = k + 1
End If
If (k > 0) Then
    DrawName = TempFile + "--" + Trim(str(k)) + DrawExt
End If
NextImageName = True

    Exit Function

lNextImageName_Error:

    LogError "frmDrawNew", "NextImageName", Err, Err.Description
End Function

Private Function InitializeOtherAssociatedImages(ABase As String) As Boolean
Dim CleanIt As Boolean
Dim Rec As String
Dim AFile As String
Dim MyFile As String
Dim TheFile As String
    On Error GoTo lInitializeOtherAssociatedImages_Error

InitializeOtherAssociatedImages = True
AFile = DoctorInterfaceDirectory + "I*" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
MyFile = Dir(AFile)
While (MyFile <> "")
    CleanIt = False
    TheFile = DoctorInterfaceDirectory + MyFile
    If (TheFile <> StoreResults) Then
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
        Line Input #101, Rec
        If (InStrPS(Rec, "MYDRAW=/I-" + ABase) > 0) Then
            CleanIt = True
        End If
        FM.CloseFile CLng(101)
        If (CleanIt) Then
            Call frmSystems1.PurgeBrushes(TheFile, True)
            Call frmSystems1.PurgeAssociated(TheFile)
            FM.Kill TheFile
            DoEvents
        End If
    End If
    MyFile = Dir
Wend

    Exit Function

lInitializeOtherAssociatedImages_Error:

    LogError "frmDrawNew", "InitializeOtherAssociatedImages", Err, Err.Description
End Function

Private Function PullDrawImageFromServer(AFile As String, BFile As String) As Boolean
Dim CFile As String
Dim DFile As String
    On Error GoTo lPullDrawImageFromServer_Error

PullDrawImageFromServer = True
If (Mid(AFile, Len(AFile) - 3, 3) <> Mid(BFile, Len(BFile) - 3, 3)) Then
    Mid(BFile, Len(BFile) - 2, 3) = Mid(AFile, Len(AFile) - 2, 3)
End If
If (Left(AFile, 2) <> "C:") Then
    If (FM.IsFileThere(AFile)) Then
        If (FM.IsFileThere(BFile)) Then
            FM.Kill BFile
        End If
        FM.FileCopy AFile, BFile
    Else
        If (InStrPS(AFile, ".bmp") > 0) Then
            CFile = Left(AFile, Len(AFile) - 3) + "jpg"
            DFile = Left(BFile, Len(BFile) - 3) + "jpg"
            If (FM.IsFileThere(CFile)) Then
                If (FM.IsFileThere(DFile)) Then
                    FM.Kill DFile
                End If
                FM.FileCopy CFile, DFile
                AFile = CFile
                BFile = DFile
            End If
        End If
    End If
End If

    Exit Function

lPullDrawImageFromServer_Error:

    LogError "frmDrawNew", "PullDrawImageFromServer", Err, Err.Description
End Function

Private Function PushDrawImageToServer(AFile As String, BFile As String) As Boolean
    
    Dim i As Integer
    On Error GoTo lPushDrawImageToServer_Error

PushDrawImageToServer = True

    If (FM.IsFileThere(AFile)) Then
               
        ' Store target files always as JPG
        If ((InStrPS(UCase(AFile), ".JPG") = 0) Or (UCase(BaseExt) <> ".JPG")) Then
        
            ' Ensure target file has .jpg extension
            If (InStrPS(UCase(BFile), ".JPG") = 0) Then
                ' Remove current extension
                i = InStrPS(BFile, ".")
                If (i > 0) Then
                    BFile = Left(BFile, i - 1)
            End If
    
                ' Use .jpg extension
                BFile = BFile + ".jpg"
            End If
            
            ' Convert and cleanup source (convert will write to destination)
            Call ConvertFileToJPEG(AFile, BFile)
            FM.Kill AFile
        Else
            ' Move to server location
            FM.Name AFile, BFile
    End If
End If

    Exit Function

lPushDrawImageToServer_Error:

    PushDrawImageToServer = False
    LogError "frmDrawNew", "PushDrawImageToServer", Err, Err.Description
End Function

Private Function ConvertFileToJPEG(AFile As String, BFile As String) As Boolean
    On Error GoTo lConvertFileToJPEG_Error

ConvertFileToJPEG = True

    ' Load image from file
    FXImage2.picture = FM.LoadPicture(AFile)
    
    ' Save it to specified file as JPG
    ' TODO: specify compression ratio?
    FXImage2.SaveFileType = FT_JPG
    FXImage2.SaveFileName = FM.GetPathForDirectIO(BFile, False)
    FXImage2.SaveFile
    FM.CommitDirectWrite (BFile)

    Exit Function

lConvertFileToJPEG_Error:

    LogError "frmDrawNew", "ConvertFileToJPEG", Err, Err.Description
End Function

Private Sub ClearBrushes(IType As Integer)
Dim i As Integer
Dim j As Integer
Dim ClearType As Boolean
Dim ATemp As String
Dim AFile As String
Dim MyFile As String
    On Error GoTo lClearBrushes_Error

ClearType = False
If (IType = 1) Then
    frmEventMsgs.Header = "Erase all brushes for this diagnosis?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Erase"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
Else
    frmEventMsgs.Result = 2
End If
If (frmEventMsgs.Result = 2) Then
    Call InitializeImages(0, False)
    For i = 0 To TotalBrushButtons - 1
        cmdBrush(i).BackColor = OriginalColor
    Next i
    lblImage1.Caption = "Label1"
    lblImage1.Visible = False
    lblImage2.Caption = "Label1"
    lblImage2.Visible = False
    lblImage3.Caption = "Label1"
    lblImage3.Visible = False
    lblImage4.Caption = "Label1"
    lblImage4.Visible = False
    lblImage5.Caption = "Label1"
    lblImage5.Visible = False
    lblImage6.Caption = "Label1"
    lblImage6.Visible = False
    lblImage7.Caption = "Label1"
    lblImage7.Visible = False
    lblImage8.Caption = "Label1"
    lblImage8.Visible = False
    lblImage9.Caption = "Label1"
    lblImage9.Visible = False
    lblImage10.Caption = "Label1"
    lblImage10.Visible = False
    lblImage11.Caption = "Label1"
    lblImage11.Visible = False
    lblImage12.Caption = "Label1"
    lblImage12.Visible = False
    lblImage13.Caption = "Label1"
    lblImage13.Visible = False
    lblImage14.Caption = "Label1"
    lblImage14.Visible = False
    lblImage15.Caption = "Label1"
    lblImage15.Visible = False
    lblImage16.Caption = "Label1"
    lblImage16.Visible = False
    lblImage17.Caption = "Label1"
    lblImage17.Visible = False
    lblImage18.Caption = "Label1"
    lblImage18.Visible = False
    Line1.Visible = False
    Line2.Visible = False
    Line3.Visible = False
    Line4.Visible = False
    Line5.Visible = False
    Line6.Visible = False
    Line7.Visible = False
    Line8.Visible = False
    Line9.Visible = False
    Line10.Visible = False
    Line11.Visible = False
    Line12.Visible = False
    Line13.Visible = False
    Line14.Visible = False
    Line15.Visible = False
    Line16.Visible = False
    Line17.Visible = False
    Line18.Visible = False
    lblRTest1.Visible = False
    If (lblRTest1.Caption <> "Label1") Then
        lblRTest1.Visible = True
    End If
    lblRTest2.Visible = False
    If (lblRTest2.Caption <> "Label2") Then
        lblRTest2.Visible = True
    End If
    lblRTest3.Visible = False
    If (lblRTest3.Caption <> "Label3") Then
        lblRTest3.Visible = True
    End If
    lblRTest4.Visible = False
    If (lblRTest4.Caption <> "Label4") Then
        lblRTest4.Visible = True
    End If
    lblLTest1.Visible = False
    If (lblLTest1.Caption <> "Label1") Then
        lblLTest1.Visible = True
    End If
    lblLTest2.Visible = False
    If (lblLTest2.Caption <> "Label2") Then
        lblLTest2.Visible = True
    End If
    lblLTest3.Visible = False
    If (lblLTest3.Caption <> "Label3") Then
        lblLTest3.Visible = True
    End If
    lblLTest4.Visible = False
    If (lblLTest4.Caption <> "Label4") Then
        lblLTest4.Visible = True
    End If
    lblRTestA1.Visible = False
    If (lblRTestA1.Caption <> "Label1") Then
        lblRTestA1.Visible = True
    End If
    lblLTestA1.Visible = False
    If (lblLTestA1.Caption <> "Label1") Then
        lblLTestA1.Visible = True
    End If
    lblLoc.Visible = False
    i = InStrPS(DrawFileName, "--")
    If (i > 0) Then
        j = InStrPS(i, DrawFileName, ".")
        If (j > 0) Then
            AFile = Left(DrawFileName, i + 1) + "*" + DrawExt
            MyFile = Dir(AFile)
            While (MyFile <> "")
                If (FM.IsFileThere(LocalMyImageDir + MyFile)) Then
                    FM.Kill LocalMyImageDir + MyFile
                End If
                MyFile = Dir
            Wend
            DrawFileName = Left(DrawFileName, i - 1) + DrawExt
        End If
    End If
    If (Trim(ReplaceBase) = "") Then
        If Not (LoadUnifiedDrawDisplay(BaseImage, ATemp, True)) Then
            If Not (OtherImagesOn) Then
                If (FM.IsFileThere(DrawFileName)) Then
                    FM.Kill DrawFileName
                    ClearType = True
                End If
            End If
        Else
            If Not (FM.IsFileThere(DrawFileName)) Then
                ClearType = True
            End If
        End If
    End If
    If (ControlClear) Then
        If (frmSystems1.EditPreviousOn) Then
            frmSystems1.EditPreviousOn = False
            Call SetNewDrawFile(DrawFileName, True)
            frmSystems1.EditPreviousOn = True
        Else
            Call SetNewDrawFile(DrawFileName, ClearType)
            lblRTest1.Visible = False
            lblRTest1.Caption = "Label1"
            lblRTest2.Visible = False
            lblRTest2.Caption = "Label2"
            lblRTest3.Visible = False
            lblRTest3.Caption = "Label3"
            lblRTest4.Visible = False
            lblRTest4.Caption = "Label4"
            lblLTest1.Visible = False
            lblLTest1.Caption = "Label1"
            lblLTest2.Visible = False
            lblLTest2.Caption = "Label2"
            lblLTest3.Visible = False
            lblLTest3.Caption = "Label3"
            lblLTest4.Visible = False
            lblLTest4.Caption = "Label4"
            lblRTestA1.Visible = False
            lblRTestA1.Caption = "Label1"
            lblLTestA1.Visible = False
            lblLTestA1.Caption = "Label1"
        End If
    End If
    PrevImageId = CurrentImageId
    CurrentImageId = 0
    Call ResetOtherLabels
    ControlClear = True
End If

    Exit Sub

lClearBrushes_Error:

    LogError "frmDrawNew", "ClearBrushes", Err, Err.Description
End Sub

Private Function IsLocationSupportOn() As Boolean
Dim i As Integer
    On Error GoTo lIsLocationSupportOn_Error

LocPropOn = False
For i = 1 To Len(frmHome.ImageLocationSupport) Step 2
    If (Left(BaseImage, 2) = Mid(frmHome.ImageLocationSupport, i, 2)) Then
        LocPropOn = True
        Exit For
    End If
Next i
IsLocationSupportOn = LocPropOn

    Exit Function

lIsLocationSupportOn_Error:

    LogError "frmDrawNew", "IsLocationSupportOn", Err, Err.Description
End Function

Private Function DeleteTheBrush(Idx As Integer) As Boolean
Dim w As Integer, z As Integer
Dim w1 As Integer, z1 As Integer
Dim c As Integer, d As Integer
Dim vp As Long, ColorPixel As Long
Dim AFile As String
    On Error GoTo lDeleteTheBrush_Error

If (Idx > 0) Then
    If (TheImage(Idx).Burn) Then
        If (Trim(ReplaceBase) = "") Then
            AFile = ImageDir + BaseImage + BaseImageRef
            If Not (FM.IsFileThere(AFile + BaseExt)) Then
                If (BaseExt = ".jpg") Then
                    If (FM.IsFileThere(AFile + ".bmp")) Then
                        BaseExt = ".bmp"
                    End If
                ElseIf (BaseExt = ".bmp") Then
                    If (FM.IsFileThere(AFile + ".jpg")) Then
                        BaseExt = ".jpg"
                    End If
                ElseIf (BaseExt = ".png") Then
                    If (FM.IsFileThere(AFile + ".jpg")) Then
                        BaseExt = ".jpg"
                    End If
                End If
            End If
            If Not (FM.IsFileThere(AFile + BaseExt)) Then
                BaseImageRef = ""
                AFile = ImageDir + BaseImage + BaseImageRef
            End If
            AFile = AFile + BaseExt
        Else
            AFile = ReplaceBase
        End If
        If (FM.IsFileThere(AFile)) Then
            ImgOrgBase.Height = FxImage1.Height
            ImgOrgBase.Width = FxImage1.Width
            ImgOrgBase.Left = FxImage1.Left
            ImgOrgBase.Top = FxImage1.Top
            ImgOrgBase.AutoSize = ISIZE_ResizeImageToControl
            ImgOrgBase.Transparent = True
            ImgOrgBase.TransparentColor = &HFFFFFF
            ImgOrgBase.picture = FM.LoadPicture(AFile)
            ImgOrgBase.FileName = AFile
            ImgOrgBase.Visible = False
            ImgOrgBase.Enabled = True
            If (TheImage(Idx).BrushReference.FrameWidth < 1) Then
                TheImage(Idx).BrushReference.FrameWidth = GlobalWidth + 50
            End If
            If (TheImage(Idx).BrushReference.FrameHeight < 1) Then
                TheImage(Idx).BrushReference.FrameHeight = GlobalHeight + 50
            End If
            w = Int((TheImage(Idx).Left - FxImage1.Left) / XTwips)
            w1 = Int((TheImage(Idx).Left + TheImage(Idx).BrushReference.FrameWidth - FxImage1.Left) / XTwips)
            z = Int((TheImage(Idx).Top - FxImage1.Top) / YTwips)
            z1 = Int(((TheImage(Idx).Top + TheImage(Idx).BrushReference.FrameHeight - FxImage1.Top)) / YTwips)
            If (TheImage(Idx).BrushReference.LocateXOn > 0) And (TheImage(Idx).BrushReference.LocateYOn > 0) Then
                z = Int((TheImage(Idx).BrushReference.LocateYOn) / YTwips)
                z1 = Int((TheImage(Idx).BrushReference.LocateYOn + GlobalHeight) / YTwips)
                w = Int((TheImage(Idx).BrushReference.LocateXOn) / XTwips)
                w1 = Int((TheImage(Idx).BrushReference.LocateXOn + GlobalWidth) / XTwips)
                If (TheImage(Idx).Eye = "OS") Then
                    w = Int((TheImage(Idx).BrushReference.LocateXOn + ((FxImage1.Width / 2) - 400)) / XTwips)
                    w1 = Int((TheImage(Idx).BrushReference.LocateXOn + ((FxImage1.Width / 2) - 400) + Int(GlobalWidth / 2)) / XTwips)
                ElseIf (TheImage(Idx).Eye = "OD") Then
                    w = Int((TheImage(Idx).BrushReference.LocateXOn - 200) / XTwips)
                    w1 = Int((TheImage(Idx).BrushReference.LocateXOn - 200 + Int(GlobalWidth / 1.5)) / XTwips)
                End If
            End If
            For c = w To w1
                For d = z To z1
                    ColorPixel = ImgOrgBase.GetImagePixel(c, d)
                    vp = FxImage1.GetImagePixel(c, d)
                    If (ColorPixel > -1) And (ColorPixel <> vp) Then
                        FxImage1.SetImagePixel c, d, ColorPixel
                    End If
                Next d
            Next c
            FxImage1.CaptureNow
            Call SaveNewDrawFileName(DrawFileName, False)
' One more time to be sure !!!!
            FxImage1.CaptureNow
            Call SaveNewDrawFileName(DrawFileName, False)
        End If
    End If
End If

    Exit Function

lDeleteTheBrush_Error:

    LogError "frmDrawNew", "DeleteTheBrush", Err, Err.Description
End Function

Private Function LoadBackRoundBaseImage() As Boolean
Dim AFile As String
    On Error GoTo lLoadBackRoundBaseImage_Error

AFile = ImageDir + BaseImage + BaseImageRef
If Not (FM.IsFileThere(AFile + BaseExt)) Then
    If (BaseExt = ".jpg") Then
        If (FM.IsFileThere(AFile + ".bmp")) Then
            BaseExt = ".bmp"
        End If
    ElseIf (BaseExt = ".bmp") Then
        If (FM.IsFileThere(AFile + ".jpg")) Then
            BaseExt = ".jpg"
        End If
    ElseIf (BaseExt = ".png") Then
        If (FM.IsFileThere(AFile + ".jpg")) Then
            BaseExt = ".jpg"
        End If
    End If
End If
If Not (FM.IsFileThere(AFile + BaseExt)) Then
    BaseImageRef = ""
    AFile = ImageDir + BaseImage + BaseImageRef
End If
If (InStrPS(FxImage1.Tag, "NewBase") > 0) Then
        ImgOrgBase.Height = FxImage1.Height
        ImgOrgBase.Width = FxImage1.Width
        ImgOrgBase.Left = FxImage1.Left
        ImgOrgBase.Top = FxImage1.Top
        ImgOrgBase.AutoSize = ISIZE_ResizeImageToControl
        ImgOrgBase.Transparent = False
        ImgOrgBase.TransparentColor = &HFFFFFF
        ImgOrgBase.picture = FM.LoadPicture(FxImage1.Tag)
        ImgOrgBase.FileName = FxImage1.Tag
        ImgOrgBase.Visible = False
        ImgOrgBase.Enabled = True
Else
    If (FM.IsFileThere(AFile + BaseExt)) Then
        ImgOrgBase.Height = FxImage1.Height
        ImgOrgBase.Width = FxImage1.Width
        ImgOrgBase.Left = FxImage1.Left
        ImgOrgBase.Top = FxImage1.Top
        ImgOrgBase.AutoSize = ISIZE_ResizeImageToControl
        ImgOrgBase.Transparent = True
        ImgOrgBase.TransparentColor = &HFFFFFF
        ImgOrgBase.picture = FM.LoadPicture(AFile + BaseExt)
        ImgOrgBase.FileName = AFile + BaseExt
        ImgOrgBase.Visible = False
        ImgOrgBase.Enabled = True
    End If
End If

    Exit Function

lLoadBackRoundBaseImage_Error:

    LogError "frmDrawNew", "LoadBackRoundBaseImage", Err, Err.Description
End Function

Private Function VerifyReplacementBase(TheFile As String) As Boolean
Dim FileNum As Long
Dim Rec As String
Dim Temp As String
    On Error GoTo lVerifyReplacementBase_Error

VerifyReplacementBase = False
If (Trim(TheFile) <> "") Then
    If (FM.IsFileThere(TheFile)) Then
        FileNum = FreeFile
        FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(FileNum)
        Line Input #FileNum, Rec
        FM.CloseFile CLng(FileNum)
        Temp = Trim(Mid(Rec, 11, Len(Rec) - 10))
        If (Left(Temp, 8) = "NewBase-") Then
            If (FM.IsFileThere(MyImageDir + Temp)) Then
                ReplaceBase = MyImageDir + Temp
                VerifyReplacementBase = True
            Else
                If (FM.IsFileThere(LocalMyImageDir + Temp)) Then
                    ReplaceBase = LocalMyImageDir + Temp
                    VerifyReplacementBase = True
                End If
            End If
        End If
    End If
End If

    Exit Function

lVerifyReplacementBase_Error:

    LogError "frmDrawNew", "VerifyReplacementBase", Err, Err.Description
End Function

Private Function ResizeScanImage(AFile As String) As Boolean
    On Error GoTo lResizeScanImage_Error

ResizeScanImage = False
If (AFile <> "") Then
    If (FM.IsFileThere(AFile)) Then
        ResizeScanImage = True
        Set FXImage2.picture = Nothing
        FXImage2.Height = 351
        FXImage2.Width = 462
        FXImage2.Left = FxImage1.Left
        FXImage2.Top = FxImage1.Top
        Set FXImage2.picture = FM.LoadPicture(AFile)
        FXImage2.AutoSize = ISIZE_ResizeImageToControl
        FXImage2.Visible = False
        FXImage2.Enabled = True
        FXImage2.ImagHeight = 351
        FXImage2.ImagWidth = 462
        FXImage2.Visible = True
        Call FXImage2.Resize(462, 351)
        FXImage2.SaveFileType = FT_JPG
        FXImage2.SaveFileName = FM.GetPathForDirectIO(AFile, False)
        FXImage2.SaveFile
        FM.CommitDirectWrite (AFile)
        FXImage2.Visible = False
    End If
End If

    Exit Function

lResizeScanImage_Error:

    LogError "frmDrawNew", "ResizeScanImage", Err, Err.Description
End Function

Private Function IsClickedonDiag(iAssociatedId As Long) As String
Dim oAssociatedDiagnosis As New DiagnosisMasterAssociated

    On Error GoTo lIsClickedonDiag_Error

    oAssociatedDiagnosis.AssociatedId = iAssociatedId
    If oAssociatedDiagnosis.RetrieveAssociated Then
        IsClickedonDiag = oAssociatedDiagnosis.AssociatedDiagnosis
    End If
    Set oAssociatedDiagnosis = Nothing

    Exit Function

lIsClickedonDiag_Error:

    LogError "frmDrawNew", "IsClickedonDiag", Err, Err.Description

End Function

Private Sub UnshownDiagMenu()
Dim fShowLine As Boolean
Dim iImgIndex As Integer

    On Error GoTo lUnshownDiagMenu_Error

    If iClickedDiagPos > 0 Then
        iImgIndex = iClickedDiagPos
        iClickedDiagPos = 0
        sClickedDiag = ""
        frmEventMsgs.Header = "The label '" & TheImage(iImgIndex).BrushReference.BrushLingo & "' is not shown on screen.  Action?"
        frmEventMsgs.AcceptText = "Delete Brush"
        If TheImage(iImgIndex).BrushReference.BrushName = "9900" Or TheImage(iImgIndex).BrushReference.BrushName = "" Then
            frmEventMsgs.AcceptText = "Erase Brush"
        End If
        frmEventMsgs.RejectText = ""
        If (TheImage(iImgIndex).LineId <> 0) Then
            If (IsLineVisible(TheImage(iImgIndex).LineId)) Then
                fShowLine = False
                frmEventMsgs.RejectText = "Line Invisible"
            Else
                fShowLine = True
                frmEventMsgs.RejectText = "Line   Visible"
            End If
        End If
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Quantifier"
        frmEventMsgs.Other1Text = "Rule Out"
        frmEventMsgs.Other2Text = "History Of"
        frmEventMsgs.Other3Text = "CNF"
        frmEventMsgs.Other4Text = "Status Post"
        cmdHome.Enabled = False
        cmdDone.Enabled = False
        frmEventMsgs.Show 1
        
        CurrentImageId = 0
        If (frmEventMsgs.Result = 1) Then
            CurrentImageId = iImgIndex
            If (frmEventMsgs.AcceptText = "Erase Brush") Then
                EraseOn = True
                lblErase.Visible = True
                Call LoadBackRoundBaseImage
            Else
                Call DeleteTheBrush(iImgIndex)
            End If
            If 0 <= TheImage(iImgIndex).ButtonNumber < TotalBrushButtons Then
                cmdBrush(TheImage(iImgIndex).ButtonNumber).BackColor = OriginalColor
            End If
            Call InitializeImages(iImgIndex, False)
            Call BuildStoreFile
            Call PrevImageName(DrawFileName)
            Call SetNewDrawFile(DrawFileName, False)
            PrevImageId = CurrentImageId
            CurrentImageId = 0
            cmdRotate.Visible = False
            cmdImageDraw.Visible = False
            cmdScaleUp.Visible = False
            cmdScaleUp.ToolTipText = ""
            cmdScaleDown.Visible = False
            cmdScaleDown.ToolTipText = ""
            If (frmEventMsgs.AcceptText = "Erase Brush") Then
                EraseOn = True
                lblErase.Visible = True
            End If
        ElseIf (frmEventMsgs.Result = 2) Then
            If (TheImage(iImgIndex).Left > 0) And (TheImage(iImgIndex).Top > 0) Then
                If (TheImage(iImgIndex).BrushReference.TestTriggerText = "") Then
                    Call SetLineVisible(TheImage(iImgIndex).LineId, fShowLine)
                End If
            End If
        ElseIf (frmEventMsgs.Result = 3) Then
            CurrentImageId = iImgIndex
            Call SetQuantifier(iImgIndex)
        ElseIf (frmEventMsgs.Result = 5) Then
            CurrentImageId = iImgIndex
            TheImage(iImgIndex).RuleOut = Not (TheImage(iImgIndex).RuleOut)
            Call SetLabelCaption(iImgIndex)
        ElseIf (frmEventMsgs.Result = 6) Then
            CurrentImageId = iImgIndex
            TheImage(iImgIndex).HistoryOf = Not (TheImage(iImgIndex).HistoryOf)
            Call SetLabelCaption(iImgIndex)
        ElseIf (frmEventMsgs.Result = 7) Then
            CurrentImageId = iImgIndex
            TheImage(iImgIndex).CNF = Not (TheImage(iImgIndex).CNF)
            Call SetLabelCaption(iImgIndex)
        ElseIf (frmEventMsgs.Result = 8) Then
            CurrentImageId = iImgIndex
            TheImage(iImgIndex).StatusPost = Not (TheImage(iImgIndex).StatusPost)
            Call SetLabelCaption(iImgIndex)
        End If
        cmdDone.Enabled = True
        cmdHome.Enabled = True
    End If

    Exit Sub

lUnshownDiagMenu_Error:

    LogError "frmDrawNew", "UnshownDiagMenu", Err, Err.Description
End Sub

Private Function GetDefaultPencilColor(iImageIndex As Integer) As Long

    On Error GoTo lGetDefaultPencilColor_Error

        Select Case Trim$(TheImage(iImageIndex).BrushReference.DefaultColor)
            Case "Y"
                GetDefaultPencilColor = frmPalette.cmdYellow.BackColor
            Case "G"
                GetDefaultPencilColor = frmPalette.cmdGreen.BackColor
            Case "B"
                GetDefaultPencilColor = frmPalette.cmdBlack.BackColor
            Case "R"
                GetDefaultPencilColor = frmPalette.cmdRed.BackColor
            Case "O"
                GetDefaultPencilColor = frmPalette.cmdOrange.BackColor
            Case "L"
                GetDefaultPencilColor = frmPalette.cmdBlue.BackColor
            Case "N"
                GetDefaultPencilColor = frmPalette.cmdBrown.BackColor
            Case "W"
                GetDefaultPencilColor = frmPalette.cmdWhite.BackColor
            Case "P"
                GetDefaultPencilColor = frmPalette.cmdPurple.BackColor
            Case "U"
                GetDefaultPencilColor = frmPalette.cmdLBlue.BackColor
            Case Else
                GetDefaultPencilColor = frmPalette.cmdBrown.BackColor
        End Select

    Exit Function

lGetDefaultPencilColor_Error:

    LogError "frmDrawNew", "GetDefaultPencilColor", Err, Err.Description
        
End Function

Private Function ClearBMLabels() As Boolean
    lblBM1.Visible = False
    lblBM1.Caption = "Label1"
    lblBM2.Visible = False
    lblBM2.Caption = "Label2"
    lblBM3.Visible = False
    lblBM3.Caption = "Label3"
    lblBM4.Visible = False
    lblBM4.Caption = "Label4"
    lblBM5.Visible = False
    lblBM5.Caption = "Label5"
    lblBM6.Visible = False
    lblBM6.Caption = "Label6"
    lblBM7.Visible = False
    lblBM7.Caption = "Label7"
    lblBM8.Visible = False
    lblBM8.Caption = "Label8"
    lblBM9.Visible = False
    lblBM9.Caption = "Label9"
    lblBM10.Visible = False
    lblBM10.Caption = "Label10"
    lblBM11.Visible = False
    lblBM11.Caption = "Label11"
    lblBM12.Visible = False
    lblBM12.Caption = "Label12"
    lblBM13.Visible = False
    lblBM13.Caption = "Label13"
    lblBM14.Visible = False
    lblBM14.Caption = "Label14"
    lblBM15.Visible = False
    lblBM15.Caption = "Label15"
    lblBM16.Visible = False
    lblBM16.Caption = "Label16"
End Function

Private Function ClearRSenLabels() As Boolean
    lblRSen1.Visible = False
    lblRSen1.Caption = "Label1"
    lblRSen2.Visible = False
    lblRSen2.Caption = "Label2"
    lblRSen3.Visible = False
    lblRSen3.Caption = "Label3"
    lblRSen4.Visible = False
    lblRSen4.Caption = "Label4"
    lblRSen5.Visible = False
    lblRSen5.Caption = "Label5"
    lblRSen6.Visible = False
    lblRSen6.Caption = "Label6"
    lblRSen7.Visible = False
    lblRSen7.Caption = "Label7"
    lblRSen8.Visible = False
    lblRSen8.Caption = "Label8"
    lblRSen9.Visible = False
    lblRSen9.Caption = "Label9"
    lblRSen10.Visible = False
    lblRSen10.Caption = "Label10"
    lblRSen11.Visible = False
    lblRSen11.Caption = "Label11"
    lblRSen12.Visible = False
    lblRSen12.Caption = "Label12"
    lblRSen13.Visible = False
    lblRSen13.Caption = "Label13"
    lblRSen14.Visible = False
    lblRSen14.Caption = "Label14"
    lblRSen15.Visible = False
    lblRSen15.Caption = "Label15"
End Function

Private Function ClearRVersLabels() As Boolean
    lblRVers1.Visible = False
    lblRVers1.Caption = "Label1"
    lblRVers2.Visible = False
    lblRVers2.Caption = "Label2"
    lblRVers3.Visible = False
    lblRVers3.Caption = "Label3"
    lblRVers4.Visible = False
    lblRVers4.Caption = "Label4"
    lblRVers5.Visible = False
    lblRVers5.Caption = "Label5"
    lblRVers6.Visible = False
    lblRVers6.Caption = "Label6"
    lblLVers1.Visible = False
    lblLVers1.Caption = "Label1"
    lblLVers2.Visible = False
    lblLVers2.Caption = "Label2"
    lblLVers3.Visible = False
    lblLVers3.Caption = "Label3"
    lblLVers4.Visible = False
    lblLVers4.Caption = "Label4"
    lblLVers5.Visible = False
    lblLVers5.Caption = "Label5"
    lblLVers6.Visible = False
    lblLVers6.Caption = "Label6"
End Function
Public Function FrmClose()
Call cmdDone_Click
 Unload Me
End Function
Private Function DisplayLabelContent(ByVal Idx As Integer) As String
Dim k As Integer
Dim Temp As String
Dim Text As String
    On Error GoTo lDisplayLabelContent_Error

k = TheImage(Idx).LabelId
Dim TempTestTrigger As String
Dim FoundIt
DisplayLabelContent = ""
TempTestTrigger = "72A" + " , " + "27A" + " , " + "32Z" + " , " + "32X" + " ," + "32Y" + " , " _
                    + "32X" + " , " + "61A" + " , " + "62A" + " , " + "63A" + " , " + "62D"
                    
FoundIt = InStrPS(1, TempTestTrigger, TheImage(Idx).BrushReference.TestTrigger)
If Trim(TheImage(Idx).BrushReference.TestTrigger) = "" Then
    Temp = Trim(TheImage(Idx).BrushReference.BrushLingo)
    Text = Temp
End If
If (k < 1) Then
    k = GetNextLabel(Idx)
End If
If (TheImage(Idx).RuleOut) Then
    Temp = "RO - " + Trim(Temp)
End If
If (TheImage(Idx).CNF) Then
    Temp = "NF - " + Trim(Temp)
End If
If (TheImage(Idx).HistoryOf) Then
    Temp = "HO - " + Trim(Temp)
End If
If (TheImage(Idx).StatusPost) Then
    Temp = "SP - " + Trim(Temp)
End If
If (Trim(TheImage(Idx).BrushReference.Quantifier) <> "") Then
    Temp = Trim(TheImage(Idx).BrushReference.Quantifier) + Trim(Temp)
End If

If Not Trim(TheImage(Idx).BrushReference.TestTrigger) = "" Then
    If FoundIt = 0 Then
        Temp = Temp + " " + Trim(TheImage(Idx).BrushReference.TestTriggerText)
        Text = Trim(TheImage(Idx).BrushReference.TestTriggerText)
    Else
        Temp = Temp + " " + Trim(TheImage(Idx).BrushReference.BrushLingo)
        Text = Trim(TheImage(Idx).BrushReference.BrushLingo)
    End If
End If
DisplayLabelContent = Temp
Exit Function
lDisplayLabelContent_Error:
 DisplayLabelContent = ""
End Function
Private Function CheckOtherDiagnosisExists(ByVal k As Integer) As Boolean
    On Error GoTo lCheckOtherDiagnosisExists_Error
    CheckOtherDiagnosisExists = False
    If (k = 1) Then
        If lblImageA1.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 2) Then
        If lblImageA2.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 3) Then
        If lblImageA3.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 4) Then
        If lblImageA4.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 5) Then
        If lblImageA5.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 6) Then
        If lblImageA6.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 7) Then
        If lblImageA7.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 8) Then
        If lblImageA8.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 9) Then
        If lblImageA9.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 10) Then
        If lblImageA10.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 11) Then
        If lblImageA11.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 12) Then
        If lblImageA12.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 13) Then
        If lblImageA13.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 14) Then
        If lblImageA14.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 15) Then
        If lblImageA15.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 16) Then
        If lblImageA16.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 17) Then
        If lblImageA17.Visible Then CheckOtherDiagnosisExists = True
    ElseIf (k = 18) Then
        If lblImageA18.Visible Then CheckOtherDiagnosisExists = True
    Else
        Exit Function
    End If
    Exit Function
lCheckOtherDiagnosisExists_Error:
    CheckOtherDiagnosisExists = False
End Function

