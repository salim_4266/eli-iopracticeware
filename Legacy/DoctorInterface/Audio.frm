VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{53EB33B2-B304-4211-BDF2-82977CAED010}#1.0#0"; "WaveExCtrl.dll"
Begin VB.Form frmAudio 
   BackColor       =   &H00505050&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Dictated Impression Linked Plan Note"
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6405
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Audio.frx":0000
   ScaleHeight     =   3090
   ScaleWidth      =   6405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   1800
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Audio.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRecord 
      Height          =   1095
      Left            =   2160
      TabIndex        =   0
      Top             =   1800
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Audio.frx":38AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPlay 
      Height          =   1095
      Left            =   4200
      TabIndex        =   4
      Top             =   1800
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Audio.frx":3A94
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdConfig 
      Height          =   735
      Left            =   4200
      TabIndex        =   5
      Top             =   960
      Visible         =   0   'False
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Audio.frx":3C81
   End
   Begin WAVEEXCONTROLLibCtl.WaveExCtrl wecAudio 
      Height          =   480
      Left            =   480
      OleObjectBlob   =   "Audio.frx":3E62
      TabIndex        =   6
      Top             =   240
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label lblRecord 
      Alignment       =   2  'Center
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   720
      Visible         =   0   'False
      Width           =   6135
   End
   Begin VB.Label lblFile 
      Alignment       =   2  'Center
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   5895
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmAudio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CaptureOn As Boolean
Public WavFileName As String
Private bSetScroll As Boolean
Private fActive As Boolean

Private Sub cmdConfig_Click()
     wecAudio.RecordFormat.ShowSelectionDialog
End Sub

Private Sub cmdHome_Click()
    If wecAudio.IsRecording Then
        cmdRecord.Text = "Start Recording"
        DoEvents
        If CaptureOn Then
            If FM.IsFileThere(WavFileName) Then
                KillFile WavFileName
            End If
        End If
    End If
    Unload frmAudio
End Sub

Private Sub cmdPlay_Click()
    On Error GoTo lcmdPlay_Click_Error

    If FM.IsFileThere(WavFileName) Then
        If wecAudio.IsPlaying Then
            cmdPlay.Text = "Playback Recording"
            cmdRecord.Enabled = True
            wecAudio.PlayStop
            Unload frmAudio
        Else
            If wecAudio.Play(WavFileName) Then   'Start Playing
                cmdPlay.Text = "Stop Playback"
                cmdRecord.Enabled = False
            End If
        End If
    End If

    Exit Sub

lcmdPlay_Click_Error:

    LogError "frmAudio", "cmdPlay_Click", Err, Err.Description
End Sub

Private Sub wecAudio_OnPlayDone()
    cmdRecord.Enabled = True
    cmdPlay.Enabled = True
    cmdPlay.Text = "Playback Recording"
End Sub

Private Sub cmdRecord_Click()
    On Error GoTo lcmdRecord_Click_Error

    If wecAudio.IsRecording Then
        wecAudio.RecordStop
        CaptureOn = True
        cmdRecord.Text = "Start Recording"
        cmdPlay.Enabled = True
        Unload frmAudio
    Else
        If Not PreserveExistingFile Then
            wecAudio.Record WavFileName
            cmdRecord.Text = "Stop Recording"
            cmdPlay.Enabled = False
        End If
    End If
    Exit Sub

lcmdRecord_Click_Error:
    If Err.Number = -2147467257 Then
        UnableToRecord
    Else
        LogError "frmAudio", "cmdRecord_Click", Err, Err.Description
    End If
End Sub

Private Function PreserveExistingFile() As Boolean
    On Error GoTo lOverwriteExistingFile_Error
    
    If FM.IsFileThere(WavFileName) Then
        frmEventMsgs.SetButtons "File already exists.  Create new file and overwrite?", "", "Yes", "No"
        frmEventMsgs.Show 1
        If frmEventMsgs.Result = 4 Then
            PreserveExistingFile = True
        End If
    End If

    Exit Function

lOverwriteExistingFile_Error:

    LogError "frmAudio", "OverwriteExistingFile", Err, Err.Description
End Function

Private Sub Form_Load()
    On Error GoTo lForm_Load_Error

    lblFile.Caption = "Capture Audio"
    lblFile.Visible = False
    CaptureOn = False
    wecAudio.RecordFormatID = "010001002256000044AC0000020010000000"

    Exit Sub

lForm_Load_Error:

    LogError "frmAudio", "Form_Load", Err, Err.Description
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    CaptureOn = True
    cmdHome_Click
End Sub

Private Sub UnableToRecord()
    frmEventMsgs.InfoMessage "Unable to record.  Please check settings."
    frmEventMsgs.Show 1
    cmdConfig.Visible = True
    DoEvents
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
