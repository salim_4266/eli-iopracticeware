VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{918E6E43-F23A-11D0-901E-0020AF7543C2}#5.0#0"; "fximg50g.ocx"
Begin VB.Form frmFreehand 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   Picture         =   "Freehand.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10320
      TabIndex        =   0
      Top             =   5280
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Freehand.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNew 
      Height          =   975
      Left            =   8760
      TabIndex        =   1
      Top             =   840
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Freehand.frx":38FC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   975
      Left            =   8760
      TabIndex        =   2
      Top             =   1920
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Freehand.frx":3B34
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   975
      Left            =   10320
      TabIndex        =   3
      Top             =   1920
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Freehand.frx":3D71
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   975
      Left            =   8760
      TabIndex        =   5
      Top             =   3600
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Freehand.frx":3FAA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdErase 
      Height          =   975
      Left            =   10320
      TabIndex        =   6
      Top             =   3600
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Freehand.frx":41E4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   975
      Left            =   8760
      TabIndex        =   8
      Top             =   5280
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Freehand.frx":4418
   End
   Begin FXIMG50GLib.FXImage FXImage 
      Height          =   7455
      Left            =   720
      TabIndex        =   9
      Top             =   840
      Width           =   7590
      _Version        =   327680
      _ExtentX        =   13388
      _ExtentY        =   13150
      _StockProps     =   65
      BackColor       =   12632256
      Persistence     =   -1  'True
      _StdProps       =   8185745
      ErrStr          =   "JAMHAOMAGIEOJPGEBPGLPDMAHAIJJIPNHIHEEAEBEMDHMD"
      ErrCode         =   1470129060
      ErrInfo         =   -63908794
      TransparentColor=   16777215
      TransparentMode =   1
      TGColor1        =   16777215
      DrawMode        =   1
      MousePointer    =   35
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   4440
      TabIndex        =   7
      Top             =   360
      Width           =   645
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   720
      TabIndex        =   4
      Top             =   360
      Width           =   645
   End
End
Attribute VB_Name = "frmFreehand"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public StoreResults As String

Private Const MaxNotes As Integer = 10

Private DrawOn As Boolean
Private EraseOn As Boolean
Private SheetOn(MaxNotes) As Boolean

Private PrevX As Single
Private PrevY As Single

Private XFactor As Integer
Private YFactor As Integer
Private TotalSheets As Integer
Private CurrentSheet As Integer

Private ClearSheet As String
Private CurrentSheetName As String
Private TempSheetName As String

Private WithEvents FXImage1 As FXImage
Attribute FXImage1.VB_VarHelpID = -1

Private WithEvents FXImage2 As FXImage
Attribute FXImage2.VB_VarHelpID = -1
Private WithEvents FxImage3 As FXImage
Attribute FxImage3.VB_VarHelpID = -1
Private WithEvents FxImage4 As FXImage
Attribute FxImage4.VB_VarHelpID = -1
Private WithEvents FxImage5 As FXImage
Attribute FxImage5.VB_VarHelpID = -1
Private WithEvents FxImage6 As FXImage
Attribute FxImage6.VB_VarHelpID = -1
Private WithEvents FxImage7 As FXImage
Attribute FxImage7.VB_VarHelpID = -1
Private WithEvents FxImage8 As FXImage
Attribute FxImage8.VB_VarHelpID = -1
Private WithEvents FxImage9 As FXImage
Attribute FxImage9.VB_VarHelpID = -1
Private WithEvents FxImage10 As FXImage
Attribute FxImage10.VB_VarHelpID = -1


Private Sub cmdQuit_Click()
If (InStrPS(TempSheetName, "Temp") <> 0) Then
    FM.Kill TempSheetName
End If
Unload frmFreehand
End Sub

Private Sub cmdDone_Click()
Call SaveCurrentSheet
Unload frmFreehand
End Sub

Private Sub cmdClear_Click()
Call ClearCurrentPicture
End Sub

Private Sub cmdErase_Click()
EraseOn = Not EraseOn
If (EraseOn) Then
    cmdErase.Text = "Erase   On"
Else
    cmdErase.Text = "Erase"
End If
End Sub

Private Sub cmdNew_Click()
Call SaveCurrentSheet
CurrentSheetName = CurrentFreehandPage(0, 0, TotalSheets)
TotalSheets = TotalSheets + 1
CurrentSheetName = NextFreehandPage(CurrentSheetName, TotalSheets)
Call TurnOnOnly(0)
If (LoadSheet(CurrentSheetName)) Then
    Call SetPageNumber(CurrentSheet, TotalSheets)
End If
End Sub

Private Sub cmdNext_Click()
Call SaveCurrentSheet
CurrentSheetName = NextFreehandPage(CurrentSheetName, TotalSheets)
Call TurnOnOnly(0)
If (LoadSheet(CurrentSheetName)) Then
    Call SetPageNumber(CurrentSheet, TotalSheets)
End If
End Sub

Private Sub cmdPrev_Click()
Call SaveCurrentSheet
CurrentSheetName = PrevFreehandPage(CurrentSheetName, TotalSheets)
Call TurnOnOnly(0)
If (LoadSheet(CurrentSheetName)) Then
    Call SetPageNumber(CurrentSheet, TotalSheets)
End If
End Sub

Private Sub MouseDown(AImage As FXImage, X As Single, Y As Single)
If Not (DrawOn) Then
    DrawOn = True
    PrevX = 0
    PrevY = 0
    AImage.MousePointer = MP_Pencil1
    AImage.SetImagePixel (X / XFactor), (Y / YFactor), 0
End If
End Sub

Private Sub MouseMove(AImage As FXImage, X As Single, Y As Single)
If (DrawOn) Then
    AImage.SetImagePixel (X / XFactor), (Y / YFactor), 0
    If (PrevX > 0) And (PrevY > 0) Then
        AImage.DrawLine PrevX, PrevY, (X / XFactor), (Y / YFactor)
    End If
    PrevX = (X / XFactor)
    PrevY = (Y / YFactor)
End If
End Sub

Private Sub MouseUp(AImage As FXImage, X As Single, Y As Single)
If (DrawOn) Then
    AImage.SetImagePixel (X / XFactor), (Y / YFactor), 0
End If
DrawOn = False
AImage.MousePointer = MP_Arrow
End Sub

Private Function LoadSheet(SheetName As String) As Boolean
LoadSheet = False
If Not (FM.IsFileThere(SheetName)) Then
    FM.FileCopy ClearSheet, SheetName
End If
TempSheetName = Replace(SheetName, ImageDir, ImageDir + "Temp")
If (Trim(TempSheetName) = "") Then
    TempSheetName = SheetName
Else
    FM.FileCopy SheetName, TempSheetName
End If
Call PlantNote(TempSheetName, CurrentSheet)
LoadSheet = True
End Function

Private Function SetSheetNumber(SheetName As String) As Integer
Dim k As Integer
Dim j As Integer
SetSheetNumber = 0
j = InStrPS(SheetName, ".bmp")
If (j <> 0) Then
    k = InStrPS(SheetName, "Notes-")
    If (k <> 0) Then
        SetSheetNumber = Val(Mid(SheetName, k + 6, j - (k + 6)))
        If (SetSheetNumber < 0) Then
            SetSheetNumber = 0
        End If
    End If
End If
End Function

Private Sub PlantNote(TheFile As String, Counter As Integer)
Dim ImageName As String
Dim LibType As String
LibType = "ImageFX.FXimage.Gold"
ImageName = "FxImage" + Trim(str(Counter))
frmFreehand.Refresh
If (Counter = 1) Then
    If Not (SheetOn(Counter)) Then
        Set FXImage1 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FXImage1, TheFile)
    Else
        FXImage1.Visible = True
        FXImage1.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
ElseIf (Counter = 2) Then
    If Not (SheetOn(Counter)) Then
        Set FXImage2 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FXImage2, TheFile)
    Else
        FXImage2.Visible = True
        FXImage2.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
ElseIf (Counter = 3) Then
    If Not (SheetOn(Counter)) Then
        Set FxImage3 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FxImage3, TheFile)
    Else
        FxImage3.Visible = True
        FxImage3.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
ElseIf (Counter = 4) Then
    If Not (SheetOn(Counter)) Then
        Set FxImage4 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FxImage4, TheFile)
    Else
        FxImage4.Visible = True
        FxImage4.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
ElseIf (Counter = 5) Then
    If Not (SheetOn(Counter)) Then
        Set FxImage5 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FxImage5, TheFile)
    Else
        FxImage5.Visible = True
        FxImage5.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
ElseIf (Counter = 6) Then
    If Not (SheetOn(Counter)) Then
        Set FxImage6 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FxImage6, TheFile)
    Else
        FxImage6.Visible = True
        FxImage6.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
ElseIf (Counter = 7) Then
    If Not (SheetOn(Counter)) Then
        Set FxImage7 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FxImage7, TheFile)
    Else
        FxImage7.Visible = True
        FxImage7.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
ElseIf (Counter = 8) Then
    If Not (SheetOn(Counter)) Then
        Set FxImage8 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FxImage8, TheFile)
    Else
        FxImage8.Visible = True
        FxImage8.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
ElseIf (Counter = 9) Then
    If Not (SheetOn(Counter)) Then
        Set FxImage9 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FxImage9, TheFile)
    Else
        FxImage9.Visible = True
        FxImage9.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
ElseIf (Counter = 10) Then
    If Not (SheetOn(Counter)) Then
        Set FxImage10 = frmFreehand.Controls.Add(LibType, ImageName)
        SheetOn(Counter) = True
        Call SetPicture(FxImage10, TheFile)
    Else
        FxImage10.Visible = True
        FxImage10.TransparentColor = 65535
    End If
    Call TurnOnOnly(Counter)
End If
End Sub


Private Function SetPageNumber(X As Integer, Y As Integer) As Boolean
Label2.Caption = "Page" + str(X) + " of" + str(Y)
End Function

Private Sub SaveCurrentSheet()
If (CurrentSheet >= TotalSheets) Then
    TotalSheets = CurrentSheet
End If
If (CurrentSheet = 1) Then
    FM.SavePicture FXImage1.picture, CurrentSheetName
ElseIf (CurrentSheet = 2) Then
    FM.SavePicture FXImage2.picture, CurrentSheetName
ElseIf (CurrentSheet = 3) Then
    FM.SavePicture FxImage3.picture, CurrentSheetName
ElseIf (CurrentSheet = 4) Then
    FM.SavePicture FxImage4.picture, CurrentSheetName
ElseIf (CurrentSheet = 5) Then
    FM.SavePicture FxImage5.picture, CurrentSheetName
ElseIf (CurrentSheet = 6) Then
    FM.SavePicture FxImage6.picture, CurrentSheetName
ElseIf (CurrentSheet = 7) Then
    FM.SavePicture FxImage7.picture, CurrentSheetName
ElseIf (CurrentSheet = 8) Then
    FM.SavePicture FxImage8.picture, CurrentSheetName
ElseIf (CurrentSheet = 9) Then
    FM.SavePicture FxImage9.picture, CurrentSheetName
ElseIf (CurrentSheet = 10) Then
    FM.SavePicture FxImage10.picture, CurrentSheetName
Else
    Exit Sub
End If
If (InStrPS(TempSheetName, "Temp") <> 0) Then
    If (FM.IsFileThere(TempSheetName)) Then
        FM.Kill TempSheetName
    End If
End If
End Sub

Public Function CurrentFreehandPage(PatientId As Long, AppointmentId As Long, Sheet As Integer) As String
Dim k As Integer
Dim j As Integer
Dim Total As Integer
Dim CurValue As Integer
Dim MyFile As String
Dim TheFile As String
Dim TheSheet As String
Dim TheResult As String
Total = 0
CurValue = 0
TheSheet = ""
TheResult = ""
ClearSheet = ImageDir + "ClearSheet.bmp"
CurrentFreehandPage = ""
If (PatientId <= 0) Then
    TheFile = CurrentSheetName
    If (Trim(TheFile) = "") Then
        Exit Function
    End If
Else
    If (AppointmentId > 0) Then
        TheFile = ImageDir + Trim(PatientId) + "-" + Trim(str(AppointmentId)) + "-Notes-*.bmp"
    Else
        TheFile = ImageDir + Trim(PatientId) + "-Notes-*.bmp"
    End If
End If
If (Sheet > 0) Then
    If (Sheet < 10) Then
        TheSheet = "0" + Trim(str(Sheet))
    Else
        TheSheet = Trim(str(Sheet))
    End If
    CurrentFreehandPage = Replace(TheFile, "-*", "-" + TheSheet)
    CurrentSheet = Sheet
Else
    MyFile = Dir(TheFile)
    While (MyFile <> "")
        k = InStrPS(MyFile, ".")
        If (k <> 0) Then
            j = InStrPS(MyFile, "-Notes-")
            If (j <> 0) Then
                j = j + 7
                CurValue = Val(Mid(MyFile, j, k - j))
                If (CurValue > Total) Then
                    TheResult = MyFile
                End If
            End If
        End If
        MyFile = Dir
    Wend
    If (Trim(TheResult) <> "") Then
        CurrentFreehandPage = ImageDir + TheResult
    Else
        TheResult = Replace(TheFile, "-*", "-01")
        FM.FileCopy ClearSheet, TheResult
        CurrentFreehandPage = TheResult
    End If
End If
End Function

Public Function NextFreehandPage(CurrentPageName As String, CurrentTotal As Integer) As String
Dim k As Integer
Dim Sheet As String
Dim TheName As String
NextFreehandPage = ""
k = InStrPS(CurrentPageName, "Notes-")
If (k <> 0) Then
    TheName = Left(CurrentPageName, k + 5)
    CurrentSheet = SetSheetNumber(CurrentPageName)
    CurrentSheet = CurrentSheet + 1
    If (CurrentSheet > CurrentTotal) Then
        CurrentSheet = CurrentTotal
    End If
    If (CurrentSheet < 10) Then
        Sheet = "0" + Trim(str(CurrentSheet))
    Else
        Sheet = Trim(str(CurrentSheet))
    End If
    NextFreehandPage = TheName + Sheet + ".bmp"
End If
End Function

Public Function PrevFreehandPage(CurrentPageName As String, CurrentTotal As Integer) As String
Dim k As Integer
Dim Sheet As String
Dim TheName As String
PrevFreehandPage = ""
k = InStrPS(CurrentPageName, "Notes-")
If (k <> 0) Then
    TheName = Left(CurrentPageName, k + 5)
    CurrentSheet = SetSheetNumber(CurrentPageName)
    CurrentSheet = CurrentSheet - 1
    If (CurrentSheet < 1) Then
        CurrentSheet = 1
    End If
    If (CurrentSheet < 10) Then
        Sheet = "0" + Trim(str(CurrentSheet))
    Else
        Sheet = Trim(str(CurrentSheet))
    End If
    PrevFreehandPage = TheName + Sheet + ".bmp"
End If
End Function

Public Function LoadFreehand() As Boolean
Dim Y As Integer, k As Integer
LoadFreehand = False
CurrentSheetName = Trim(StoreResults)
If (CurrentSheetName <> "") Then
    ClearSheet = ImageDir + "ClearSheet.bmp"
    DrawOn = False
    Label1.Visible = False
    Label1.Caption = ""
    EraseOn = False
    cmdErase.Text = "Erase"
    XFactor = Screen.TwipsPerPixelX
    YFactor = Screen.TwipsPerPixelY
    Erase SheetOn
    FXImage.FileName = FM.GetPathForDirectIO(ClearSheet)
    FXImage.DeleteCapture
    FXImage.CaptureNow
    Y = InStrPS(CurrentSheetName, ".bmp")
    If (Y <> 0) Then
        k = InStrPS(CurrentSheetName, "Notes-")
        If (k <> 0) Then
            CurrentSheet = Val(Mid(CurrentSheetName, k + 6, (Y - 1) - (k + 5)))
            LoadFreehand = LoadSheet(CurrentSheetName)
            TotalSheets = CurrentSheet
            Call SetPageNumber(CurrentSheet, TotalSheets)
        End If
    End If
End If
End Function

Private Sub Form_Activate()
FXImage.Visible = False
frmFreehand.Refresh
FXImage.Capture = ICAP_None
FXImage.AutoSize = ISIZE_ResizeImageToControl
FXImage.Transparent = True
FXImage.TransparentMode = TM_Overlay
FXImage.MultiEffect = False
End Sub

Private Sub FxImage1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FXImage1, X, Y)
End Sub

Private Sub FxImage1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FXImage1, X, Y)
End Sub

Private Sub FxImage1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FXImage1, X, Y)
End Sub


Private Sub FxImage2_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FXImage2, X, Y)

End Sub


Private Sub FxImage2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FXImage2, X, Y)

End Sub


Private Sub FxImage2_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FXImage2, X, Y)

End Sub

Private Sub FxImage3_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FxImage3, X, Y)
End Sub

Private Sub FxImage3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FxImage3, X, Y)
End Sub

Private Sub FxImage3_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FxImage3, X, Y)
End Sub

Private Sub FxImage4_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FxImage4, X, Y)
End Sub

Private Sub FxImage4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FxImage4, X, Y)
End Sub

Private Sub FxImage4_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FxImage4, X, Y)
End Sub

Private Sub FxImage5_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FxImage5, X, Y)
End Sub

Private Sub FxImage5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FxImage5, X, Y)
End Sub

Private Sub FxImage5_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FxImage5, X, Y)
End Sub

Private Sub FxImage6_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FxImage6, X, Y)
End Sub

Private Sub FxImage6_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FxImage6, X, Y)
End Sub

Private Sub FxImage6_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FxImage6, X, Y)
End Sub

Private Sub FxImage7_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FxImage7, X, Y)
End Sub

Private Sub FxImage7_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FxImage7, X, Y)
End Sub

Private Sub FxImage7_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FxImage7, X, Y)
End Sub

Private Sub FxImage8_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FxImage8, X, Y)
End Sub

Private Sub FxImage8_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FxImage8, X, Y)
End Sub

Private Sub FxImage8_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FxImage8, X, Y)
End Sub

Private Sub FxImage9_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FxImage9, X, Y)
End Sub

Private Sub FxImage9_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FxImage9, X, Y)
End Sub

Private Sub FxImage9_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FxImage9, X, Y)
End Sub

Private Sub FxImage10_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseDown(FxImage10, X, Y)
End Sub

Private Sub FxImage10_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseMove(FxImage10, X, Y)
End Sub

Private Sub FxImage10_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Call MouseUp(FxImage10, X, Y)
End Sub

Private Sub TurnOnOnly(TheSheet As Integer)
If (SheetOn(1)) Then
    FXImage1.Visible = False
    If (TheSheet = 1) Then
        FXImage1.Visible = True
    End If
End If
If (SheetOn(2)) Then
    FXImage2.Visible = False
    If (TheSheet = 2) Then
        FXImage2.Visible = True
    End If
End If
If (SheetOn(3)) Then
    FxImage3.Visible = False
    If (TheSheet = 3) Then
        FxImage3.Visible = True
    End If
End If
If (SheetOn(4)) Then
    FxImage4.Visible = False
    If (TheSheet = 4) Then
        FxImage4.Visible = True
    End If
End If
If (SheetOn(5)) Then
    FxImage5.Visible = False
    If (TheSheet = 5) Then
        FxImage5.Visible = True
    End If
End If
If (SheetOn(6)) Then
    FxImage6.Visible = False
    If (TheSheet = 6) Then
        FxImage6.Visible = True
    End If
End If
If (SheetOn(7)) Then
    FxImage7.Visible = False
    If (TheSheet = 7) Then
        FxImage7.Visible = True
    End If
End If
If (SheetOn(8)) Then
    FxImage8.Visible = False
    If (TheSheet = 8) Then
        FxImage8.Visible = True
    End If
End If
If (SheetOn(9)) Then
    FxImage9.Visible = False
    If (TheSheet = 9) Then
        FxImage9.Visible = True
    End If
End If
If (SheetOn(10)) Then
    FxImage10.Visible = False
    If (TheSheet = 10) Then
        FxImage10.Visible = True
    End If
End If
End Sub

Private Sub ClearCurrentPicture()
Dim TheFile As String
FM.FileCopy ClearSheet, CurrentSheetName
TheFile = CurrentSheetName
If (SheetOn(1)) Then
    If (FXImage1.Visible) Then
        FXImage1.Visible = False
        Call SetPicture(FXImage1, TheFile)
        FM.SavePicture FXImage1.picture, CurrentSheetName
    End If
End If
If (SheetOn(2)) Then
    If (FXImage2.Visible) Then
        FXImage2.Visible = False
        Call SetPicture(FXImage2, TheFile)
        FM.SavePicture FXImage2.picture, CurrentSheetName
    End If
End If
If (SheetOn(3)) Then
    If (FxImage3.Visible) Then
        FxImage3.Visible = False
        Call SetPicture(FxImage3, TheFile)
        FM.SavePicture FxImage3.picture, CurrentSheetName
    End If
End If
If (SheetOn(4)) Then
    If (FxImage4.Visible) Then
        FxImage4.Visible = False
        Call SetPicture(FxImage4, TheFile)
        FM.SavePicture FxImage4.picture, CurrentSheetName
    End If
End If
If (SheetOn(5)) Then
    If (FxImage5.Visible) Then
        FxImage5.Visible = False
        Call SetPicture(FxImage5, TheFile)
        FM.SavePicture FxImage5.picture, CurrentSheetName
    End If
End If
If (SheetOn(6)) Then
    If (FxImage6.Visible) Then
        FxImage6.Visible = False
        Call SetPicture(FxImage6, TheFile)
        FM.SavePicture FxImage6.picture, CurrentSheetName
    End If
End If
If (SheetOn(7)) Then
    If (FxImage7.Visible) Then
        FxImage7.Visible = False
        Call SetPicture(FxImage7, TheFile)
        FM.SavePicture FxImage7.picture, CurrentSheetName
    End If
End If
If (SheetOn(8)) Then
    If (FxImage8.Visible) Then
        FxImage8.Visible = False
        Call SetPicture(FxImage8, TheFile)
        FM.SavePicture FxImage8.picture, CurrentSheetName
    End If
End If
If (SheetOn(9)) Then
    If (FxImage9.Visible) Then
        FxImage9.Visible = False
        Call SetPicture(FxImage9, TheFile)
        FM.SavePicture FxImage9.picture, CurrentSheetName
    End If
End If
If (SheetOn(10)) Then
    If (FxImage10.Visible) Then
        FxImage10.Visible = False
        Call SetPicture(FxImage10, TheFile)
        FM.SavePicture FxImage10.picture, CurrentSheetName
    End If
End If
End Sub

Private Sub SetPicture(AImage As FXImage, TheFile As String)
With AImage
    If (Trim(TheFile) <> "") Then
        .FileName = TheFile
    End If
    .BackColor = FXImage.BackColor
    .Left = FXImage.Left
    .Top = FXImage.Top
    .Height = FXImage.Height
    .Width = FXImage.Width
    .AutoSize = FXImage.AutoSize
    .Capture = FXImage.Capture
    .TransparentColor = 65535
    .MultiEffect = False
    .ShowRotated = ROTATE_None
    .Tag = ""
    .Update = True
    .Transparent = False
    .TransparentMode = TM_OverlayPlusCapturedImage
    .DrawMode = PEN_Blackness
    .DrawWidth = 2
    .DrawStyle = STYLE_Dash
    .Enabled = True
    .Visible = True
    .ZOrder 0
    .Refresh
End With
AImage.DeleteCapture
AImage.CaptureNow
End Sub
Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
End Function
