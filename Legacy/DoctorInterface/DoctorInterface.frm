VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmDoctorInterface 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   Icon            =   "DoctorInterface.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdAssign 
      Height          =   1335
      Left            =   720
      TabIndex        =   0
      Top             =   7560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":0442
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExam 
      Height          =   1335
      Left            =   2880
      TabIndex        =   1
      Top             =   7560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":062B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1335
      Left            =   10080
      TabIndex        =   2
      Top             =   7560
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":080F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient1 
      Height          =   975
      Left            =   720
      TabIndex        =   6
      Top             =   960
      Visible         =   0   'False
      Width           =   4575
      _Version        =   131072
      _ExtentX        =   8070
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":09EE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient2 
      Height          =   975
      Left            =   720
      TabIndex        =   7
      Top             =   2040
      Visible         =   0   'False
      Width           =   4575
      _Version        =   131072
      _ExtentX        =   8070
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":0BD1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient3 
      Height          =   975
      Left            =   720
      TabIndex        =   8
      Top             =   3120
      Visible         =   0   'False
      Width           =   4575
      _Version        =   131072
      _ExtentX        =   8070
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":0DB4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient4 
      Height          =   975
      Left            =   720
      TabIndex        =   9
      Top             =   4200
      Visible         =   0   'False
      Width           =   4575
      _Version        =   131072
      _ExtentX        =   8070
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":0F97
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient5 
      Height          =   975
      Left            =   720
      TabIndex        =   10
      Top             =   5280
      Visible         =   0   'False
      Width           =   4575
      _Version        =   131072
      _ExtentX        =   8070
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":117A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient6 
      Height          =   975
      Left            =   720
      TabIndex        =   11
      Top             =   6360
      Visible         =   0   'False
      Width           =   4575
      _Version        =   131072
      _ExtentX        =   8070
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":135D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   1335
      Left            =   5160
      TabIndex        =   12
      Top             =   7560
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DoctorInterface.frx":1540
   End
   Begin VB.Label lblPatient6 
      AutoSize        =   -1  'True
      BackColor       =   &H00A39B6D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   5640
      TabIndex        =   18
      Top             =   6600
      Visible         =   0   'False
      Width           =   3825
   End
   Begin VB.Label lblPatient5 
      AutoSize        =   -1  'True
      BackColor       =   &H00A39B6D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   5640
      TabIndex        =   17
      Top             =   5520
      Visible         =   0   'False
      Width           =   3825
   End
   Begin VB.Label lblPatient4 
      AutoSize        =   -1  'True
      BackColor       =   &H00A39B6D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   5640
      TabIndex        =   16
      Top             =   4440
      Visible         =   0   'False
      Width           =   3825
   End
   Begin VB.Label lblPatient3 
      AutoSize        =   -1  'True
      BackColor       =   &H00A39B6D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   5640
      TabIndex        =   15
      Top             =   3360
      Visible         =   0   'False
      Width           =   3825
   End
   Begin VB.Label lblPatient2 
      AutoSize        =   -1  'True
      BackColor       =   &H00A39B6D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   5640
      TabIndex        =   14
      Top             =   2280
      Visible         =   0   'False
      Width           =   3825
   End
   Begin VB.Label lblPatient1 
      AutoSize        =   -1  'True
      BackColor       =   &H00A39B6D&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   5640
      TabIndex        =   13
      Top             =   1200
      Visible         =   0   'False
      Width           =   3825
   End
   Begin VB.Label lblTime 
      AutoSize        =   -1  'True
      BackColor       =   &H00A39B6D&
      Caption         =   "Time"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   6360
      TabIndex        =   5
      Top             =   480
      Width           =   810
   End
   Begin VB.Label lblDate 
      AutoSize        =   -1  'True
      BackColor       =   &H00A39B6D&
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   6360
      TabIndex        =   4
      Top             =   120
      Width           =   765
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00A39B6D&
      Caption         =   "Waiting Room"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   360
      TabIndex        =   3
      Top             =   240
      Width           =   2775
   End
End
Attribute VB_Name = "frmDoctorInterface"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentIndex As Integer
Public CloseDown As Boolean
Public ExitFast As Boolean
Public DebugOn As Boolean

'Private Const TheStationId = 102
Private Const TheStationId = 0

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long
Private ThePatientName As String
Private TheActivityId As Long

Private ResourceName As String
Private SignInType As Integer
Private PatientId As Long
Private AppointmentId As Long
Private PatientPhone As String
Private PatientBirthDate As String
Private PatientGender As String
Private Appointment As String
Private AppointmentType As String
Private SchedulerDoctor As String
Private DoctorColor As Long
Private RetrieveClass As DynamicClass

Private Sub SetDateTime()
Dim TheDate As String
Dim TheTime As String
Call FormatTodaysDate(TheDate, True)
Call FormatTimeNow(TheTime)
lblDate.Caption = TheDate
If (Left(TheTime, 1) = "0") Then
    lblTime.Caption = Mid(TheTime, 2, Len(TheTime) - 1)
Else
    lblTime.Caption = TheTime
End If
End Sub

Private Sub ClearButtons()
cmdPatient1.Text = ""
cmdPatient1.Tag = ""
cmdPatient1.Visible = False
lblPatient1.Caption = ""
lblPatient1.Visible = False
cmdPatient2.Text = ""
cmdPatient2.Tag = ""
cmdPatient2.Visible = False
lblPatient2.Caption = ""
lblPatient2.Visible = False
cmdPatient3.Text = ""
cmdPatient3.Tag = ""
cmdPatient3.Visible = False
lblPatient3.Caption = ""
lblPatient3.Visible = False
cmdPatient4.Text = ""
cmdPatient4.Tag = ""
cmdPatient4.Visible = False
lblPatient4.Caption = ""
lblPatient4.Visible = False
cmdPatient5.Text = ""
cmdPatient5.Tag = ""
cmdPatient5.Visible = False
lblPatient5.Caption = ""
lblPatient5.Visible = False
cmdPatient6.Text = ""
cmdPatient6.Tag = ""
cmdPatient6.Visible = False
lblPatient6.Caption = ""
lblPatient6.Visible = False
End Sub

Private Sub SetPatient(Ref As Integer)
If (Ref = 1) Then
    ThePatientName = cmdPatient1.Text
    TheActivityId = Val(Trim(cmdPatient1.Tag))
ElseIf (Ref = 2) Then
    ThePatientName = cmdPatient2.Text
    TheActivityId = Val(Trim(cmdPatient2.Tag))
ElseIf (Ref = 3) Then
    ThePatientName = cmdPatient3.Text
    TheActivityId = Val(Trim(cmdPatient3.Tag))
ElseIf (Ref = 4) Then
    ThePatientName = cmdPatient4.Text
    TheActivityId = Val(Trim(cmdPatient4.Tag))
ElseIf (Ref = 5) Then
    ThePatientName = cmdPatient5.Text
    TheActivityId = Val(Trim(cmdPatient5.Tag))
ElseIf (Ref = 6) Then
    ThePatientName = cmdPatient6.Text
    TheActivityId = Val(Trim(cmdPatient6.Tag))
Else
    ThePatientName = ""
    TheActivityId = 0
End If
End Sub

Private Function AcquirePatient(PatientId As Long, ApptId As Long, ActivityId As Long, Ref As Integer) As Boolean
On Error GoTo UI_ErrorHandler
Dim RetrievePatient As Patient
Dim RetrieveAppointment As SchedulerAppointment
Dim RetrieveAppointmentType As SchedulerAppointmentType
Dim RetrieveDoctor As SchedulerResource
Dim PatientName As String
Dim AppointmentType As String
Dim ApptType As Long
Dim ApptDoc As Long
Dim ApptTime As String
Dim DisplayTime As String
Dim DisplayText As String
AcquirePatient = False
Set RetrievePatient = New Patient
RetrievePatient.PatientId = PatientId
If (RetrievePatient.RetrievePatient) Then
    AcquirePatient = True
    PatientName = RetrievePatient.FirstName + " " _
                + RetrievePatient.MiddleInitial + " " _
                + RetrievePatient.LastName + " " _
                + RetrievePatient.NameRef
    If (Ref = 1) Then
        cmdPatient1.Text = PatientName
        cmdPatient1.Tag = Trim(Str(ActivityId))
        cmdPatient1.Visible = True
    ElseIf (Ref = 2) Then
        cmdPatient2.Text = PatientName
        cmdPatient2.Tag = Trim(Str(ActivityId))
        cmdPatient2.Visible = True
    ElseIf (Ref = 3) Then
        cmdPatient3.Text = PatientName
        cmdPatient3.Tag = Trim(Str(ActivityId))
        cmdPatient3.Visible = True
    ElseIf (Ref = 4) Then
        cmdPatient4.Text = PatientName
        cmdPatient4.Tag = Trim(Str(ActivityId))
        cmdPatient4.Visible = True
    ElseIf (Ref = 5) Then
        cmdPatient5.Text = PatientName
        cmdPatient5.Tag = Trim(Str(ActivityId))
        cmdPatient5.Visible = True
    ElseIf (Ref = 6) Then
        cmdPatient6.Text = PatientName
        cmdPatient6.Tag = Trim(Str(ActivityId))
        cmdPatient6.Visible = True
    Else
        ApptId = 0
    End If
    If (ApptId > 0) Then
        Set RetrieveAppointment = New SchedulerAppointment
        RetrieveAppointment.AppointmentId = ApptId
        If (RetrieveAppointment.RetrieveSchedulerAppointment) Then
            ApptType = RetrieveAppointment.AppointmentTypeId
            Appointment = RetrieveAppointment.AppointmentDate + " " _
                        + RetrieveAppointment.AppointmentTime
            ApptDoc = RetrieveAppointment.AppointmentResourceId1
            ApptTime = RetrieveAppointment.AppointmentTime
            Set RetrieveDoctor = New SchedulerResource
            RetrieveDoctor.ResourceId = ApptDoc
            If (RetrieveDoctor.RetrieveSchedulerResource) Then
                SchedulerDoctor = RetrieveDoctor.ResourceName
                DoctorColor = RetrieveDoctor.ResourceColor
            Else
                SchedulerDoctor = ""
                DoctorColor = 0
            End If
            Set RetrieveAppointmentType = New SchedulerAppointmentType
            RetrieveAppointmentType.AppointmentTypeId = ApptType
            If (RetrieveAppointmentType.RetrieveSchedulerAppointmentType) Then
                AppointmentType = RetrieveAppointmentType.AppointmentType
            Else
                AppointmentType = ""
            End If
            Call ConvertMinutesToTime(Val(ApptTime), DisplayTime)
            DisplayText = DisplayTime + ", " + SchedulerDoctor + ", " + Trim(AppointmentType)
            If (Ref = 1) Then
                lblPatient1.Caption = DisplayText
                lblPatient1.BackColor = DoctorColor
                lblPatient1.Visible = True
            ElseIf (Ref = 2) Then
                lblPatient2.Caption = DisplayText
                lblPatient2.BackColor = DoctorColor
                lblPatient2.Visible = True
            ElseIf (Ref = 3) Then
                lblPatient3.Caption = DisplayText
                lblPatient3.BackColor = DoctorColor
                lblPatient3.Visible = True
            ElseIf (Ref = 4) Then
                lblPatient4.Caption = DisplayText
                lblPatient4.BackColor = DoctorColor
                lblPatient4.Visible = True
            ElseIf (Ref = 5) Then
                lblPatient5.Caption = DisplayText
                lblPatient5.BackColor = DoctorColor
                lblPatient5.Visible = True
            ElseIf (Ref = 6) Then
                lblPatient6.Caption = DisplayText
                lblPatient6.BackColor = DoctorColor
                lblPatient6.Visible = True
            End If
        End If
    Else
        AcquirePatient = False
    End If
Else
    AcquirePatient = False
End If
Set RetrievePatient = Nothing
Set RetrieveAppointment = Nothing
Set RetrieveAppointmentType = Nothing
Set RetrieveDoctor = Nothing
Exit Function
UI_ErrorHandler:
    Set RetrievePatient = Nothing
    Set RetrieveAppointment = Nothing
    Set RetrieveAppointmentType = Nothing
    Set RetrieveDoctor = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Public Function LoadDoctorInterface() As Boolean
On Error GoTo UI_ErrorHandler
Dim k As Integer
Dim p As Integer
Dim TotalWaiting As Integer
Dim ApptDate As String
Dim LocalDate As ManagedDate
Dim RetrieveActivity As PracticeActivity
LoadDoctorInterface = False
ThePatientName = ""
TheActivityId = 0
SignInType = GetPracticeLoginType
Call ClearButtons
Call SetDateTime
Call FormatTodaysDate(ApptDate, False)
Set RetrieveActivity = New PracticeActivity
Set LocalDate = New ManagedDate
LocalDate.ExposedDate = ApptDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    RetrieveActivity.ActivityDate = LocalDate.ExposedDate
Else
    Set LocalDate = Nothing
    Set RetrieveActivity = Nothing
    Exit Function
End If
If (DebugOn) Then
    RetrieveActivity.ActivityDate = "19990527"
End If
RetrieveActivity.ActivityStationId = -1
RetrieveActivity.ActivityStatus = "M"
TotalWaiting = RetrieveActivity.FindActivity
If (TotalWaiting > 0) Then
    k = CurrentIndex
    p = 1
    While (RetrieveActivity.SelectActivity(k)) And (p < 6)
        Call AcquirePatient(RetrieveActivity.ActivityPatientId, RetrieveActivity.ActivityAppointmentId, RetrieveActivity.ActivityId, p)
        p = p + 1
        CurrentIndex = k
        k = k + 1
    Wend
End If
If (CurrentIndex > 0) Then
    LoadDoctorInterface = True
End If
If (TotalWaiting <= 6) Or (CurrentIndex >= TotalWaiting) Then
    cmdMore.Visible = False
End If
Set LocalDate = Nothing
Set RetrieveActivity = Nothing
Exit Function
UI_ErrorHandler:
    Set LocalDate = Nothing
    Set RetrieveActivity = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Sub cmdAssign_Click()
Dim LoginSucceed As Boolean
LoginSucceed = False
If (Trim(ThePatientName) = "") Then
    Exit Sub
End If
If (TheActivityId < 1) Then
    Exit Sub
End If
frmAssignRoom.ThePatientName = Trim(ThePatientName)
frmAssignRoom.TheActivityId = TheActivityId
If (frmAssignRoom.LoadAssignableRooms) Then
    frmAssignRoom.Show 1
    If (frmAssignRoom.TheResourceId <> 0) Then
        If (frmDoctorSignIn1.LoadStaff) Then
            frmDoctorSignIn1.Show 1
            LoginSucceed = frmDoctorSignIn1.IsValid
            If (LoginSucceed) And (SignInType = 1) Then
                frmDoctorSignIn.UserName = frmDoctorSignIn1.UserName
                frmDoctorSignIn.Show 1
                LoginSucceed = frmDoctorSignIn.IsValid
            End If
        End If
        If (LoginSucceed) Then
            frmSystems.ActivityId = TheActivityId
            If (frmSystems.LoadSummaryInfo) Then
                frmSystems.Show 1
            End If
        End If
        CurrentIndex = 1
        Call LoadDoctorInterface
    End If
End If
Call TurnOn
End Sub

Private Sub cmdExam_Click()
Dim LoginSucceed As Boolean
Dim ActivityId As Long
LoginSucceed = False
frmExamMonitor.ReadOnly = False
frmExamMonitor.WaitingOn = True
If (frmExamMonitor.LoadExamRoom) Then
    frmExamMonitor.Show 1
    If (frmExamMonitor.TheActivityId > 0) Then
        If (SignInType = 1) Then
            If (frmDoctorSignIn1.LoadStaff) Then
                frmDoctorSignIn1.Show 1
                LoginSucceed = frmDoctorSignIn1.IsValid
                ResourceName = frmDoctorSignIn1.UserName
            End If
        Else
            frmDoctorSignIn.Show 1
            LoginSucceed = frmDoctorSignIn.IsValid
            ResourceName = frmDoctorSignIn.UserName
        End If
        If (LoginSucceed) Then
            ActivityId = frmExamMonitor.TheActivityId
            frmSystems.ActivityId = ActivityId
            frmSystems.ResourceName = ResourceName
            If (frmSystems.LoadSummaryInfo) Then
                frmSystems.Show 1
            End If
        Else
            ResourceName = ""
        End If
    End If
Else
    MsgBox "No One In Exam Rooms"
End If
CurrentIndex = 1
Call LoadDoctorInterface
Call TurnOn
End Sub

Private Sub cmdHome_Click()
Unload frmDoctorInterface
End
End Sub

Private Sub cmdMore_Click()
Call LoadDoctorInterface
End Sub

Private Sub cmdPatient1_Click()
Call SelectPatient(cmdPatient1, 1)
End Sub

Private Sub cmdPatient2_Click()
Call SelectPatient(cmdPatient2, 2)
End Sub

Private Sub cmdPatient3_Click()
Call SelectPatient(cmdPatient3, 3)
End Sub

Private Sub cmdPatient4_Click()
Call SelectPatient(cmdPatient4, 4)
End Sub

Private Sub cmdPatient5_Click()
Call SelectPatient(cmdPatient5, 5)
End Sub

Private Sub cmdPatient6_Click()
Call SelectPatient(cmdPatient6, 6)
End Sub

Private Sub TurnOn()
    cmdPatient1.Enabled = True
    cmdPatient2.Enabled = True
    cmdPatient3.Enabled = True
    cmdPatient4.Enabled = True
    cmdPatient5.Enabled = True
    cmdPatient6.Enabled = True
End Sub

Private Sub TurnOff(Ref As Integer)
    If (Ref <> 1) Then
        cmdPatient1.Enabled = False
    End If
    If (Ref <> 2) Then
        cmdPatient2.Enabled = False
    End If
    If (Ref <> 3) Then
        cmdPatient3.Enabled = False
    End If
    If (Ref <> 4) Then
        cmdPatient4.Enabled = False
    End If
    If (Ref <> 5) Then
        cmdPatient5.Enabled = False
    End If
    If (Ref <> 6) Then
        cmdPatient6.Enabled = False
    End If
End Sub

Private Sub SelectPatient(AButton As fpBtn, Ref As Integer)
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    Call TurnOff(Ref)
    ThePatientName = Trim(AButton.Text)
    TheActivityId = Val(Trim(AButton.Tag))
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    Call TurnOn
    ThePatientName = ""
    TheActivityId = 0
End If
End Sub

Private Sub Form_Load()
DebugOn = True
CurrentIndex = 1
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdPatient1.BackColor
BaseTextColor = cmdPatient1.ForeColor
Call LoadDoctorInterface
End Sub

Private Function GetPracticeLoginType() As Integer
GetPracticeLoginType = 0
Dim RetrievePractice As PracticeName
Set RetrievePractice = New PracticeName
RetrievePractice.PracticeName = Chr(1)
RetrievePractice.PracticeType = "P"
If (RetrievePractice.FindPractice > 0) Then
    If (RetrievePractice.SelectPractice(1)) Then
        GetPracticeLoginType = Val(RetrievePractice.PracticeLoginType)
    End If
End If
Set RetrievePractice = Nothing
End Function
