VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmFavorites 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   1230
      Left            =   2040
      TabIndex        =   0
      Top             =   7440
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   2170
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1230
      Left            =   9840
      TabIndex        =   1
      Top             =   7440
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   2170
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":01E0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis1 
      Height          =   1095
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":03BF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis2 
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   2040
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":05A4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis3 
      Height          =   1095
      Left            =   120
      TabIndex        =   4
      Top             =   3360
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":0789
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis4 
      Height          =   1095
      Left            =   120
      TabIndex        =   5
      Top             =   4680
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":096E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis6 
      Height          =   1095
      Left            =   6120
      TabIndex        =   6
      Top             =   720
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":0B53
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis7 
      Height          =   1095
      Left            =   6120
      TabIndex        =   7
      Top             =   2040
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":0D38
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis8 
      Height          =   1095
      Left            =   6120
      TabIndex        =   8
      Top             =   3360
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":0F1D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStandardDrill 
      Height          =   1095
      Left            =   6120
      TabIndex        =   9
      Top             =   4680
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":1102
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis5 
      Height          =   1095
      Left            =   120
      TabIndex        =   10
      Top             =   6000
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":12F6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   1095
      Left            =   6120
      TabIndex        =   11
      Top             =   6000
      Visible         =   0   'False
      Width           =   5415
      _Version        =   131072
      _ExtentX        =   9551
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Favorites.frx":14DB
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "OD"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   360
      TabIndex        =   13
      Top             =   240
      Width           =   435
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Diagnosis"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   1800
      TabIndex        =   12
      Top             =   240
      Visible         =   0   'False
      Width           =   1380
   End
End
Attribute VB_Name = "frmFavorites"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public AppointmentId As Long
Public PatientId As Long
Public ActivityId As Long
Public ResourceName As String
Public MedicalSystem As String
Public Diagnosis As String
Public DiagnosisLevel As Integer
Public DiagnosisId As Long
Public SystemReference As String
Public EyeContext As String
Public FavoriteOn As Boolean

Private FindingsOn As Boolean
Private TheDiagnosis As String
Private CurrentActiveDiag As Integer
Private CurrentLevel As Integer
Private OriginalDiagnosis As String
Private CurrentIndex As Integer
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub TurnOthersOff(Ref As Integer)
If (Ref = 1) Then
    cmdDiagnosis1.BackColor = ButtonSelectionColor
    cmdDiagnosis1.ForeColor = TextSelectionColor
    cmdDiagnosis2.BackColor = BaseBackColor
    cmdDiagnosis2.ForeColor = BaseTextColor
    cmdDiagnosis3.BackColor = BaseBackColor
    cmdDiagnosis3.ForeColor = BaseTextColor
    cmdDiagnosis4.BackColor = BaseBackColor
    cmdDiagnosis4.ForeColor = BaseTextColor
    cmdDiagnosis5.BackColor = BaseBackColor
    cmdDiagnosis5.ForeColor = BaseTextColor
    cmdDiagnosis6.BackColor = BaseBackColor
    cmdDiagnosis6.ForeColor = BaseTextColor
    cmdDiagnosis7.BackColor = BaseBackColor
    cmdDiagnosis7.ForeColor = BaseTextColor
    cmdDiagnosis8.BackColor = BaseBackColor
    cmdDiagnosis8.ForeColor = BaseTextColor
ElseIf (Ref = 2) Then
    cmdDiagnosis1.BackColor = BaseBackColor
    cmdDiagnosis1.ForeColor = BaseTextColor
    cmdDiagnosis2.BackColor = ButtonSelectionColor
    cmdDiagnosis2.ForeColor = TextSelectionColor
    cmdDiagnosis3.BackColor = BaseBackColor
    cmdDiagnosis3.ForeColor = BaseTextColor
    cmdDiagnosis4.BackColor = BaseBackColor
    cmdDiagnosis4.ForeColor = BaseTextColor
    cmdDiagnosis5.BackColor = BaseBackColor
    cmdDiagnosis5.ForeColor = BaseTextColor
    cmdDiagnosis6.BackColor = BaseBackColor
    cmdDiagnosis6.ForeColor = BaseTextColor
    cmdDiagnosis7.BackColor = BaseBackColor
    cmdDiagnosis7.ForeColor = BaseTextColor
    cmdDiagnosis8.BackColor = BaseBackColor
    cmdDiagnosis8.ForeColor = BaseTextColor
ElseIf (Ref = 3) Then
    cmdDiagnosis1.BackColor = BaseBackColor
    cmdDiagnosis1.ForeColor = BaseTextColor
    cmdDiagnosis2.BackColor = BaseBackColor
    cmdDiagnosis2.ForeColor = BaseTextColor
    cmdDiagnosis3.BackColor = ButtonSelectionColor
    cmdDiagnosis3.ForeColor = TextSelectionColor
    cmdDiagnosis4.BackColor = BaseBackColor
    cmdDiagnosis4.ForeColor = BaseTextColor
    cmdDiagnosis5.BackColor = BaseBackColor
    cmdDiagnosis5.ForeColor = BaseTextColor
    cmdDiagnosis6.BackColor = BaseBackColor
    cmdDiagnosis6.ForeColor = BaseTextColor
    cmdDiagnosis7.BackColor = BaseBackColor
    cmdDiagnosis7.ForeColor = BaseTextColor
    cmdDiagnosis8.BackColor = BaseBackColor
    cmdDiagnosis8.ForeColor = BaseTextColor
ElseIf (Ref = 4) Then
    cmdDiagnosis1.BackColor = BaseBackColor
    cmdDiagnosis1.ForeColor = BaseTextColor
    cmdDiagnosis2.BackColor = BaseBackColor
    cmdDiagnosis2.ForeColor = BaseTextColor
    cmdDiagnosis3.BackColor = BaseBackColor
    cmdDiagnosis3.ForeColor = BaseTextColor
    cmdDiagnosis4.BackColor = ButtonSelectionColor
    cmdDiagnosis4.ForeColor = TextSelectionColor
    cmdDiagnosis5.BackColor = BaseBackColor
    cmdDiagnosis5.ForeColor = BaseTextColor
    cmdDiagnosis6.BackColor = BaseBackColor
    cmdDiagnosis6.ForeColor = BaseTextColor
    cmdDiagnosis7.BackColor = BaseBackColor
    cmdDiagnosis7.ForeColor = BaseTextColor
    cmdDiagnosis8.BackColor = BaseBackColor
    cmdDiagnosis8.ForeColor = BaseTextColor
ElseIf (Ref = 5) Then
    cmdDiagnosis1.BackColor = BaseBackColor
    cmdDiagnosis1.ForeColor = BaseTextColor
    cmdDiagnosis2.BackColor = BaseBackColor
    cmdDiagnosis2.ForeColor = BaseTextColor
    cmdDiagnosis3.BackColor = BaseBackColor
    cmdDiagnosis3.ForeColor = BaseTextColor
    cmdDiagnosis4.BackColor = BaseBackColor
    cmdDiagnosis4.ForeColor = BaseTextColor
    cmdDiagnosis5.BackColor = ButtonSelectionColor
    cmdDiagnosis5.ForeColor = TextSelectionColor
    cmdDiagnosis6.BackColor = BaseBackColor
    cmdDiagnosis6.ForeColor = BaseTextColor
    cmdDiagnosis7.BackColor = BaseBackColor
    cmdDiagnosis7.ForeColor = BaseTextColor
    cmdDiagnosis8.BackColor = BaseBackColor
    cmdDiagnosis8.ForeColor = BaseTextColor
ElseIf (Ref = 6) Then
    cmdDiagnosis1.BackColor = BaseBackColor
    cmdDiagnosis1.ForeColor = BaseTextColor
    cmdDiagnosis2.BackColor = BaseBackColor
    cmdDiagnosis2.ForeColor = BaseTextColor
    cmdDiagnosis3.BackColor = BaseBackColor
    cmdDiagnosis3.ForeColor = BaseTextColor
    cmdDiagnosis4.BackColor = BaseBackColor
    cmdDiagnosis4.ForeColor = BaseTextColor
    cmdDiagnosis5.BackColor = BaseBackColor
    cmdDiagnosis5.ForeColor = BaseTextColor
    cmdDiagnosis6.BackColor = ButtonSelectionColor
    cmdDiagnosis6.ForeColor = TextSelectionColor
    cmdDiagnosis7.BackColor = BaseBackColor
    cmdDiagnosis7.ForeColor = BaseTextColor
    cmdDiagnosis8.BackColor = BaseBackColor
    cmdDiagnosis8.ForeColor = BaseTextColor
ElseIf (Ref = 7) Then
    cmdDiagnosis1.BackColor = BaseBackColor
    cmdDiagnosis1.ForeColor = BaseTextColor
    cmdDiagnosis2.BackColor = BaseBackColor
    cmdDiagnosis2.ForeColor = BaseTextColor
    cmdDiagnosis3.BackColor = BaseBackColor
    cmdDiagnosis3.ForeColor = BaseTextColor
    cmdDiagnosis4.BackColor = BaseBackColor
    cmdDiagnosis4.ForeColor = BaseTextColor
    cmdDiagnosis5.BackColor = BaseBackColor
    cmdDiagnosis5.ForeColor = BaseTextColor
    cmdDiagnosis6.BackColor = BaseBackColor
    cmdDiagnosis6.ForeColor = BaseTextColor
    cmdDiagnosis7.BackColor = ButtonSelectionColor
    cmdDiagnosis7.ForeColor = TextSelectionColor
    cmdDiagnosis8.BackColor = BaseBackColor
    cmdDiagnosis8.ForeColor = BaseTextColor
ElseIf (Ref = 8) Then
    cmdDiagnosis1.BackColor = BaseBackColor
    cmdDiagnosis1.ForeColor = BaseTextColor
    cmdDiagnosis2.BackColor = BaseBackColor
    cmdDiagnosis2.ForeColor = BaseTextColor
    cmdDiagnosis3.BackColor = BaseBackColor
    cmdDiagnosis3.ForeColor = BaseTextColor
    cmdDiagnosis4.BackColor = BaseBackColor
    cmdDiagnosis4.ForeColor = BaseTextColor
    cmdDiagnosis5.BackColor = BaseBackColor
    cmdDiagnosis5.ForeColor = BaseTextColor
    cmdDiagnosis6.BackColor = BaseBackColor
    cmdDiagnosis6.ForeColor = BaseTextColor
    cmdDiagnosis7.BackColor = BaseBackColor
    cmdDiagnosis7.ForeColor = BaseTextColor
    cmdDiagnosis8.BackColor = ButtonSelectionColor
    cmdDiagnosis8.ForeColor = TextSelectionColor
ElseIf (Ref = 0) Then
    cmdDiagnosis1.BackColor = BaseBackColor
    cmdDiagnosis1.ForeColor = BaseTextColor
    cmdDiagnosis2.BackColor = BaseBackColor
    cmdDiagnosis2.ForeColor = BaseTextColor
    cmdDiagnosis3.BackColor = BaseBackColor
    cmdDiagnosis3.ForeColor = BaseTextColor
    cmdDiagnosis4.BackColor = BaseBackColor
    cmdDiagnosis4.ForeColor = BaseTextColor
    cmdDiagnosis5.BackColor = BaseBackColor
    cmdDiagnosis5.ForeColor = BaseTextColor
    cmdDiagnosis6.BackColor = BaseBackColor
    cmdDiagnosis6.ForeColor = BaseTextColor
    cmdDiagnosis7.BackColor = BaseBackColor
    cmdDiagnosis7.ForeColor = BaseTextColor
    cmdDiagnosis8.BackColor = BaseBackColor
    cmdDiagnosis8.ForeColor = BaseTextColor
End If
End Sub

Private Sub cmdDiagnosis1_Click()
Dim ASystem As String
Dim ADiag As String
Dim ALevel As Integer
Dim w, q As Integer
Call TriggerDiagnosis(cmdDiagnosis1)
If (cmdDiagnosis1.BackColor <> BaseBackColor) Then
    Call TurnOthersOff(1)
    ASystem = MedicalSystem
    q = InStrPS(cmdDiagnosis1.Tag, "/")
    If (q = 0) Then
        Call TriggerDiagnosis(cmdDiagnosis1)
        Exit Sub
    End If
    w = InStrPS(q + 1, cmdDiagnosis1.Tag, "/")
    If (w = 0) Then
        Exit Sub
    End If
    TheDiagnosis = Mid(cmdDiagnosis1.Tag, w + 1, Len(cmdDiagnosis1.Tag) - w)
    DiagnosisId = Val(Left(cmdDiagnosis1.Tag, q - 1))
    CurrentActiveDiag = 1
    SystemReference = Label2.Caption + cmdDiagnosis1.Text
    Call cmdDraw_Click
    Call cmdDone_Click
End If
End Sub

Private Sub cmdDiagnosis2_Click()
Dim ASystem As String
Dim ADiag As String
Dim ALevel As Integer
Dim w, q As Integer
Call TriggerDiagnosis(cmdDiagnosis2)
If (cmdDiagnosis2.BackColor <> BaseBackColor) Then
    Call TurnOthersOff(2)
    ASystem = MedicalSystem
    q = InStrPS(cmdDiagnosis2.Tag, "/")
    If (q = 0) Then
        Call TriggerDiagnosis(cmdDiagnosis2)
        Exit Sub
    End If
    w = InStrPS(q + 1, cmdDiagnosis2.Tag, "/")
    If (w = 0) Then
        Exit Sub
    End If
    TheDiagnosis = Mid(cmdDiagnosis2.Tag, w + 1, Len(cmdDiagnosis2.Tag) - w)
    DiagnosisId = Val(Left(cmdDiagnosis2.Tag, q - 1))
    CurrentActiveDiag = 2
    SystemReference = Label2.Caption + cmdDiagnosis2.Text
    Call cmdDraw_Click
    Call cmdDone_Click
End If
End Sub

Private Sub cmdDiagnosis3_Click()
Dim ASystem As String
Dim ADiag As String
Dim ALevel As Integer
Dim w, q As Integer
Call TriggerDiagnosis(cmdDiagnosis3)
If (cmdDiagnosis3.BackColor <> BaseBackColor) Then
    Call TurnOthersOff(3)
    ASystem = MedicalSystem
    q = InStrPS(cmdDiagnosis3.Tag, "/")
    If (q = 0) Then
        Call TriggerDiagnosis(cmdDiagnosis3)
        Exit Sub
    End If
    w = InStrPS(q + 1, cmdDiagnosis3.Tag, "/")
    If (w = 0) Then
        Exit Sub
    End If
    TheDiagnosis = Mid(cmdDiagnosis3.Tag, w + 1, Len(cmdDiagnosis3.Tag) - w)
    DiagnosisId = Val(Left(cmdDiagnosis3.Tag, q - 1))
    CurrentActiveDiag = 3
    SystemReference = Label2.Caption + cmdDiagnosis3.Text
    Call cmdDraw_Click
    Call cmdDone_Click
End If
End Sub

Private Sub cmdDiagnosis4_Click()
Dim ASystem As String
Dim ADiag As String
Dim ALevel As Integer
Dim w, q As Integer
Call TriggerDiagnosis(cmdDiagnosis4)
If (cmdDiagnosis4.BackColor <> BaseBackColor) Then
    Call TurnOthersOff(4)
    ASystem = MedicalSystem
    q = InStrPS(cmdDiagnosis4.Tag, "/")
    If (q = 0) Then
        Call TriggerDiagnosis(cmdDiagnosis4)
        Exit Sub
    End If
    w = InStrPS(q + 1, cmdDiagnosis4.Tag, "/")
    If (w = 0) Then
        Exit Sub
    End If
    TheDiagnosis = Mid(cmdDiagnosis4.Tag, w + 1, Len(cmdDiagnosis4.Tag) - w)
    DiagnosisId = Val(Left(cmdDiagnosis4.Tag, q - 1))
    CurrentActiveDiag = 4
    SystemReference = Label2.Caption + cmdDiagnosis4.Text
    Call cmdDraw_Click
    Call cmdDone_Click
End If
End Sub

Private Sub cmdDiagnosis5_Click()
Dim ASystem As String
Dim ADiag As String
Dim ALevel As Integer
Dim w, q As Integer
Call TriggerDiagnosis(cmdDiagnosis5)
If (cmdDiagnosis5.BackColor <> BaseBackColor) Then
    Call TurnOthersOff(5)
    ASystem = MedicalSystem
    q = InStrPS(cmdDiagnosis5.Tag, "/")
    If (q = 0) Then
        Call TriggerDiagnosis(cmdDiagnosis5)
        Exit Sub
    End If
    w = InStrPS(q + 1, cmdDiagnosis5.Tag, "/")
    If (w = 0) Then
        Exit Sub
    End If
    TheDiagnosis = Mid(cmdDiagnosis5.Tag, w + 1, Len(cmdDiagnosis5.Tag) - w)
    DiagnosisId = Val(Left(cmdDiagnosis5.Tag, q - 1))
    CurrentActiveDiag = 5
    SystemReference = Label2.Caption + cmdDiagnosis5.Text
    Call cmdDraw_Click
    Call cmdDone_Click
End If
End Sub

Private Sub cmdDiagnosis6_Click()
Dim ASystem As String
Dim ADiag As String
Dim ALevel As Integer
Dim w, q As Integer
Call TriggerDiagnosis(cmdDiagnosis6)
If (cmdDiagnosis6.BackColor <> BaseBackColor) Then
    Call TurnOthersOff(6)
    ASystem = MedicalSystem
    q = InStrPS(cmdDiagnosis6.Tag, "/")
    If (q = 0) Then
        Call TriggerDiagnosis(cmdDiagnosis6)
        Exit Sub
    End If
    w = InStrPS(q + 1, cmdDiagnosis6.Tag, "/")
    If (w = 0) Then
        Exit Sub
    End If
    TheDiagnosis = Mid(cmdDiagnosis6.Tag, w + 1, Len(cmdDiagnosis6.Tag) - w)
    DiagnosisId = Val(Left(cmdDiagnosis6.Tag, q - 1))
    CurrentActiveDiag = 6
    SystemReference = Label2.Caption + cmdDiagnosis6.Text
    Call cmdDraw_Click
    Call cmdDone_Click
End If
End Sub

Private Sub cmdDiagnosis7_Click()
Dim ASystem As String
Dim ADiag As String
Dim ALevel As Integer
Dim w, q As Integer
Call TriggerDiagnosis(cmdDiagnosis7)
If (cmdDiagnosis7.BackColor <> BaseBackColor) Then
    Call TurnOthersOff(7)
    ASystem = MedicalSystem
    q = InStrPS(cmdDiagnosis7.Tag, "/")
    If (q = 0) Then
        Call TriggerDiagnosis(cmdDiagnosis7)
        Exit Sub
    End If
    w = InStrPS(q + 1, cmdDiagnosis7.Tag, "/")
    If (w = 0) Then
        Exit Sub
    End If
    TheDiagnosis = Mid(cmdDiagnosis7.Tag, w + 1, Len(cmdDiagnosis7.Tag) - w)
    DiagnosisId = Val(Left(cmdDiagnosis7.Tag, q - 1))
    CurrentActiveDiag = 7
    SystemReference = Label2.Caption + cmdDiagnosis7.Text
    Call cmdDraw_Click
    Call cmdDone_Click
End If
End Sub

Private Sub cmdDiagnosis8_Click()
Dim ASystem As String
Dim ADiag As String
Dim ALevel As Integer
Dim w, q As Integer
Call TriggerDiagnosis(cmdDiagnosis8)
If (cmdDiagnosis8.BackColor <> BaseBackColor) Then
    Call TurnOthersOff(8)
    ASystem = MedicalSystem
    q = InStrPS(cmdDiagnosis8.Tag, "/")
    If (q = 0) Then
        Call TriggerDiagnosis(cmdDiagnosis8)
        Exit Sub
    End If
    w = InStrPS(q + 1, cmdDiagnosis8.Tag, "/")
    If (w = 0) Then
        Exit Sub
    End If
    TheDiagnosis = Mid(cmdDiagnosis8.Tag, w + 1, Len(cmdDiagnosis8.Tag) - w)
    DiagnosisId = Val(Left(cmdDiagnosis8.Tag, q - 1))
    CurrentActiveDiag = 8
    SystemReference = Label2.Caption + cmdDiagnosis8.Text
    Call cmdDraw_Click
    Call cmdDone_Click
End If
End Sub

Private Sub cmdDone_Click()
If (Trim(TheDiagnosis) <> Diagnosis) And (Trim(TheDiagnosis) <> "-") Then
    Diagnosis = TheDiagnosis
    FavoriteOn = True
End If
If (Trim(TheDiagnosis) = "") Then
    FavoriteOn = True
End If
Unload frmFavorites
End Sub

Private Sub cmdDraw_Click()
Dim q As Integer
Dim LocalDiagnosis As String
Dim ImageRef As String
Dim PrimaryId As Long
Dim RetrievePrimary As DiagnosisMasterPrimary
If (CurrentActiveDiag = 1) Then
    q = InStrPS(cmdDiagnosis1.Tag, "/")
    If (q = 0) Then
        Exit Sub
    End If
    PrimaryId = Val(Left(cmdDiagnosis1.Tag, q - 1))
ElseIf (CurrentActiveDiag = 2) Then
    q = InStrPS(cmdDiagnosis2.Tag, "/")
    If (q = 0) Then
        Exit Sub
    End If
    PrimaryId = Val(Left(cmdDiagnosis2.Tag, q - 1))
ElseIf (CurrentActiveDiag = 3) Then
    q = InStrPS(cmdDiagnosis3.Tag, "/")
    If (q = 0) Then
        Exit Sub
    End If
    PrimaryId = Val(Left(cmdDiagnosis3.Tag, q - 1))
ElseIf (CurrentActiveDiag = 4) Then
    q = InStrPS(cmdDiagnosis4.Tag, "/")
    If (q = 0) Then
        Exit Sub
    End If
    PrimaryId = Val(Left(cmdDiagnosis4.Tag, q - 1))
ElseIf (CurrentActiveDiag = 5) Then
    q = InStrPS(cmdDiagnosis5.Tag, "/")
    If (q = 0) Then
        Exit Sub
    End If
    PrimaryId = Val(Left(cmdDiagnosis5.Tag, q - 1))
ElseIf (CurrentActiveDiag = 6) Then
    q = InStrPS(cmdDiagnosis6.Tag, "/")
    If (q = 0) Then
        Exit Sub
    End If
    PrimaryId = Val(Left(cmdDiagnosis6.Tag, q - 1))
ElseIf (CurrentActiveDiag = 7) Then
    q = InStrPS(cmdDiagnosis7.Tag, "/")
    If (q = 0) Then
        Exit Sub
    End If
    PrimaryId = Val(Left(cmdDiagnosis7.Tag, q - 1))
ElseIf (CurrentActiveDiag = 8) Then
    q = InStrPS(cmdDiagnosis8.Tag, "/")
    If (q = 0) Then
        Exit Sub
    End If
    PrimaryId = Val(Left(cmdDiagnosis8.Tag, q - 1))
Else
    Exit Sub
End If
Set RetrievePrimary = New DiagnosisMasterPrimary
RetrievePrimary.PrimaryId = PrimaryId
If (RetrievePrimary.RetrievePrimary) Then
    ImageRef = RetrievePrimary.PrimaryImage1
    frmDraw.DiagnosisId = RetrievePrimary.PrimaryId
    frmDraw.DiagnosisName = TheDiagnosis + " " + RetrievePrimary.PrimaryName
    frmDraw.ImageRef = RetrievePrimary.PrimaryImage1
    frmDraw.EyeContext = Label1.Caption
    frmDraw.ActivityId = ActivityId
    frmDraw.ResourceName = ResourceName
Else
    Exit Sub
End If
Set RetrievePrimary = Nothing
If (Trim(ImageRef) <> "") And (Left(ImageRef, 2) <> "99") Then
    LocalDiagnosis = TheDiagnosis
    Call ReplaceCharacters(LocalDiagnosis, ".", "-")
    frmDraw.FormOn = True
    frmDraw.StoreResults = DoctorInterfaceDirectory + "I" + EyeContext + TheDiagnosis + "_" + Trim(Str(PatientId)) + ".txt"
    frmDraw.Show 1
End If
End Sub

Private Sub cmdMore_Click()
If (cmdMore.Text = "Start Over") Then
    CurrentIndex = 1
End If
Call LoadDiagnosisLevels
End Sub

Private Sub cmdNotes_Click()
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.ActivityId = ActivityId
frmNotes.ResourceName = ResourceName
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Public Function LoadDiagnosisLevels() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim TotalDiags As Integer
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
LoadDiagnosisLevels = False
TheDiagnosis = ""
FavoriteOn = False
If (MedicalSystem = "") Then
    Exit Function
End If
FindingsOn = frmExamMonitor.IsFindingsOn
Set RetrieveDiagnosis = New DiagnosisMasterPrimary
RetrieveDiagnosis.PrimaryFinding = FindingsOn
RetrieveDiagnosis.PrimarySystem = MedicalSystem
RetrieveDiagnosis.PrimaryDiagnosis = ""
RetrieveDiagnosis.PrimaryNextLevelDiagnosis = Diagnosis
RetrieveDiagnosis.PrimaryLevel = DiagnosisLevel
CurrentLevel = DiagnosisLevel
TotalDiags = RetrieveDiagnosis.FindPrimary
If (TotalDiags > 0) Then
    Call ClearButtons
    i = CurrentIndex
    ButtonCnt = 1
    While (RetrieveDiagnosis.SelectPrimary(i)) And (ButtonCnt < 9)
        If (ButtonCnt = 1) Then
            cmdDiagnosis1.Text = RetrieveDiagnosis.PrimaryName
            cmdDiagnosis1.Tag = Trim(Str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis1.Visible = True
        ElseIf (ButtonCnt = 2) Then
            cmdDiagnosis2.Text = RetrieveDiagnosis.PrimaryName
            cmdDiagnosis2.Tag = Trim(Str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis2.Visible = True
        ElseIf (ButtonCnt = 3) Then
            cmdDiagnosis3.Text = RetrieveDiagnosis.PrimaryName
            cmdDiagnosis3.Tag = Trim(Str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis3.Visible = True
        ElseIf (ButtonCnt = 4) Then
            cmdDiagnosis4.Text = RetrieveDiagnosis.PrimaryName
            cmdDiagnosis4.Tag = Trim(Str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis4.Visible = True
        ElseIf (ButtonCnt = 5) Then
            cmdDiagnosis5.Text = RetrieveDiagnosis.PrimaryName
            cmdDiagnosis5.Tag = Trim(Str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis5.Visible = True
        ElseIf (ButtonCnt = 6) Then
            cmdDiagnosis6.Text = RetrieveDiagnosis.PrimaryName
            cmdDiagnosis6.Tag = Trim(Str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis6.Visible = True
        ElseIf (ButtonCnt = 7) Then
            cmdDiagnosis7.Text = RetrieveDiagnosis.PrimaryName
            cmdDiagnosis7.Tag = Trim(Str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis7.Visible = True
        ElseIf (ButtonCnt = 8) Then
            cmdDiagnosis8.Text = RetrieveDiagnosis.PrimaryName
            cmdDiagnosis8.Tag = Trim(Str(RetrieveDiagnosis.PrimaryId)) + "/" + RetrieveDiagnosis.PrimaryDiagnosis + "/" + RetrieveDiagnosis.PrimaryNextLevelDiagnosis
            cmdDiagnosis8.Visible = True
        End If
        i = i + 1
        ButtonCnt = ButtonCnt + 1
        CurrentIndex = i
    Wend
    LoadDiagnosisLevels = True
End If
If (CurrentIndex < TotalDiags) Then
    cmdMore.Visible = True
    cmdMore.Text = "More"
Else
    If (TotalDiags > 9) Then
        cmdMore.Visible = True
        cmdMore.Text = "Start Over"
    Else
        cmdMore.Visible = False
    End If
End If
Set RetrieveDiagnosis = Nothing
End Function

Private Sub cmdStandardDrill_Click()
TheDiagnosis = "-"
Call cmdDone_Click
End Sub

Private Sub Form_Load()
CurrentIndex = 1
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdDiagnosis1.BackColor
BaseTextColor = cmdDiagnosis1.ForeColor
Label1.Caption = EyeContext
End Sub

Private Sub ClearButtons()
cmdDiagnosis1.BackColor = BaseBackColor
cmdDiagnosis1.ForeColor = BaseTextColor
cmdDiagnosis1.Text = ""
cmdDiagnosis1.Tag = ""
cmdDiagnosis1.Visible = False
cmdDiagnosis2.BackColor = BaseBackColor
cmdDiagnosis2.ForeColor = BaseTextColor
cmdDiagnosis2.Text = ""
cmdDiagnosis2.Tag = ""
cmdDiagnosis2.Visible = False
cmdDiagnosis3.BackColor = BaseBackColor
cmdDiagnosis3.ForeColor = BaseTextColor
cmdDiagnosis3.Text = ""
cmdDiagnosis3.Tag = ""
cmdDiagnosis3.Visible = False
cmdDiagnosis4.BackColor = BaseBackColor
cmdDiagnosis4.ForeColor = BaseTextColor
cmdDiagnosis4.Text = ""
cmdDiagnosis4.Tag = ""
cmdDiagnosis4.Visible = False
cmdDiagnosis5.BackColor = BaseBackColor
cmdDiagnosis5.ForeColor = BaseTextColor
cmdDiagnosis5.Text = ""
cmdDiagnosis5.Tag = ""
cmdDiagnosis5.Visible = False
cmdDiagnosis6.BackColor = BaseBackColor
cmdDiagnosis6.ForeColor = BaseTextColor
cmdDiagnosis6.Text = ""
cmdDiagnosis6.Tag = ""
cmdDiagnosis6.Visible = False
cmdDiagnosis7.BackColor = BaseBackColor
cmdDiagnosis7.ForeColor = BaseTextColor
cmdDiagnosis7.Text = ""
cmdDiagnosis7.Tag = ""
cmdDiagnosis7.Visible = False
cmdDiagnosis8.BackColor = BaseBackColor
cmdDiagnosis8.ForeColor = BaseTextColor
cmdDiagnosis8.Text = ""
cmdDiagnosis8.Tag = ""
cmdDiagnosis8.Visible = False
End Sub

Public Sub SetDiagnosisLabel()
Dim ADiagnosis As DiagnosisMasterPrimary
If (MedicalSystem = "") Then
    Exit Sub
End If
Set ADiagnosis = New DiagnosisMasterPrimary
ADiagnosis.PrimaryFinding = FindingsOn
ADiagnosis.PrimarySystem = MedicalSystem
ADiagnosis.PrimaryDiagnosis = Diagnosis
ADiagnosis.PrimaryLevel = 0
ADiagnosis.PrimaryRank = 0
If (ADiagnosis.FindPrimary > 0) Then
    If (ADiagnosis.SelectPrimary(1)) Then
        Label2.Caption = Label2.Caption + ADiagnosis.PrimaryName
    End If
End If
Set ADiagnosis = Nothing
Label2.Caption = frmSystems1.SystemReference
Label2.Visible = True
End Sub

Private Sub TriggerDiagnosis(AButton As fpBtn)
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    DiagnosisId = 0
    TheDiagnosis = ""
    CurrentActiveDiag = 0
End If
End Sub

