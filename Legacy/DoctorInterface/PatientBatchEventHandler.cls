VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientBatchEventHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IO_Practiceware_Interop.IEventHandler

Private m_PatientId As Integer

Dim answer As Integer
Dim frx As Form, fCount As Integer

Property Get PatientId() As Integer
      PatientId = m_PatientId
End Property

Private Sub IEventHandler_OnEvent(ByVal sender As Variant, ByVal e As Variant)
    m_PatientId = e.PatientId
    Exit Sub
End Sub
