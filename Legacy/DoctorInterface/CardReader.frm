VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmCardReader 
   BackColor       =   &H00505050&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2475
   ClientLeft      =   2010
   ClientTop       =   150
   ClientWidth     =   6240
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "CardReader.frx":0000
   ScaleHeight     =   2475
   ScaleWidth      =   6240
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmr 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   4800
      Top             =   2520
   End
   Begin VB.TextBox txt 
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   2520
      Width           =   4215
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCancel 
      Cancel          =   -1  'True
      Height          =   870
      Left            =   2100
      TabIndex        =   2
      Top             =   1440
      Width           =   2040
      _Version        =   131072
      _ExtentX        =   3598
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CardReader.frx":36C9
   End
   Begin VB.Label lblMessage 
      Alignment       =   2  'Center
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "       Please swipe card for e-Rx validation        or press the Space Bar on your keyboard"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1410
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   5505
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmCardReader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Entered As String

Private Sub cmdCancel_Click()
Entered = "CANCEL"
Unload Me
End Sub

Private Sub tmr_Timer()
If Not txt.Text = "" Then
    If txt.Text = Entered Then
        Unload Me
    End If
End If
End Sub

Private Sub txt_Change()
Entered = txt.Text
If Not txt.Text = "" Then
    If Not tmr.Enabled Then
        tmr.Interval = 1000
        tmr.Enabled = True
    End If
End If
End Sub
