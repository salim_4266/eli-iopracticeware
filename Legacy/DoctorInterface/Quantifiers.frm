VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmQuantifiers 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9495
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12375
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "Quantifiers.frx":0000
   ScaleHeight     =   9495
   ScaleWidth      =   12375
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt23 
      Height          =   990
      Left            =   6240
      TabIndex        =   38
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt31 
      Height          =   990
      Left            =   9840
      TabIndex        =   36
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":38AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt25 
      Height          =   990
      Left            =   8040
      TabIndex        =   30
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":3A8B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt9 
      Height          =   990
      Left            =   2400
      TabIndex        =   24
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":3C6C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt1 
      Height          =   990
      Left            =   480
      TabIndex        =   15
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":3E4C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt5 
      Height          =   990
      Left            =   480
      TabIndex        =   11
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":402C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt20 
      Height          =   990
      Left            =   6240
      TabIndex        =   0
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":420C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt19 
      Height          =   990
      Left            =   6240
      TabIndex        =   1
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":43ED
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt18 
      Height          =   990
      Left            =   4320
      TabIndex        =   2
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":45CE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt17 
      Height          =   990
      Left            =   4320
      TabIndex        =   3
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":47AF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt16 
      Height          =   990
      Left            =   4320
      TabIndex        =   4
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":4990
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt21 
      Height          =   990
      Left            =   6240
      TabIndex        =   5
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":4B71
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   9840
      TabIndex        =   6
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":4D52
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   480
      TabIndex        =   7
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":4F31
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt8 
      Height          =   990
      Left            =   2400
      TabIndex        =   8
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":5110
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt7 
      Height          =   990
      Left            =   2400
      TabIndex        =   9
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":52F0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt6 
      Height          =   990
      Left            =   480
      TabIndex        =   10
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":54D0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt4 
      Height          =   990
      Left            =   480
      TabIndex        =   12
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":56B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt3 
      Height          =   990
      Left            =   480
      TabIndex        =   13
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":5890
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt2 
      Height          =   990
      Left            =   480
      TabIndex        =   14
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":5A70
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt22 
      Height          =   990
      Left            =   6240
      TabIndex        =   17
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":5C50
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt15 
      Height          =   990
      Left            =   4320
      TabIndex        =   18
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":5E31
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt14 
      Height          =   990
      Left            =   4320
      TabIndex        =   19
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":6012
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt13 
      Height          =   990
      Left            =   4320
      TabIndex        =   20
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":61F3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt12 
      Height          =   990
      Left            =   2400
      TabIndex        =   21
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":63D4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt11 
      Height          =   990
      Left            =   2400
      TabIndex        =   22
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":65B5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt10 
      Height          =   990
      Left            =   2400
      TabIndex        =   23
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":6796
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt30 
      Height          =   990
      Left            =   8040
      TabIndex        =   25
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":6977
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt29 
      Height          =   990
      Left            =   8040
      TabIndex        =   26
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":6B58
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt28 
      Height          =   990
      Left            =   8040
      TabIndex        =   27
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":6D39
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt27 
      Height          =   990
      Left            =   8040
      TabIndex        =   28
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":6F1A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt26 
      Height          =   990
      Left            =   8040
      TabIndex        =   29
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":70FB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt36 
      Height          =   990
      Left            =   9840
      TabIndex        =   31
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":72DC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt35 
      Height          =   990
      Left            =   9840
      TabIndex        =   32
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":74BD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt34 
      Height          =   990
      Left            =   9840
      TabIndex        =   33
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":769E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt33 
      Height          =   990
      Left            =   9840
      TabIndex        =   34
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":787F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt32 
      Height          =   990
      Left            =   9840
      TabIndex        =   35
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":7A60
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt24 
      Height          =   990
      Left            =   6240
      TabIndex        =   37
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":7C41
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef23 
      Height          =   990
      Left            =   6240
      TabIndex        =   39
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":7E22
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef31 
      Height          =   990
      Left            =   9840
      TabIndex        =   40
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":8002
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef25 
      Height          =   990
      Left            =   8040
      TabIndex        =   41
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":81E2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef9 
      Height          =   990
      Left            =   2400
      TabIndex        =   42
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":83C2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef20 
      Height          =   990
      Left            =   6240
      TabIndex        =   45
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":85A1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef19 
      Height          =   990
      Left            =   6240
      TabIndex        =   46
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":8781
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef18 
      Height          =   990
      Left            =   4320
      TabIndex        =   47
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":8961
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef17 
      Height          =   990
      Left            =   4320
      TabIndex        =   48
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":8B41
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef16 
      Height          =   990
      Left            =   4320
      TabIndex        =   49
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":8D21
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef21 
      Height          =   990
      Left            =   6240
      TabIndex        =   50
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":8F01
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef8 
      Height          =   990
      Left            =   2400
      TabIndex        =   51
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":90E1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef7 
      Height          =   990
      Left            =   2400
      TabIndex        =   52
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":92C0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef22 
      Height          =   990
      Left            =   6240
      TabIndex        =   57
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":949F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef15 
      Height          =   990
      Left            =   4320
      TabIndex        =   58
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":967F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef14 
      Height          =   990
      Left            =   4320
      TabIndex        =   59
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":985F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef13 
      Height          =   990
      Left            =   4320
      TabIndex        =   60
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":9A3F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef12 
      Height          =   990
      Left            =   2400
      TabIndex        =   61
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":9C1F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef11 
      Height          =   990
      Left            =   2400
      TabIndex        =   62
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":9DFF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef10 
      Height          =   990
      Left            =   2400
      TabIndex        =   63
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":9FDF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef30 
      Height          =   990
      Left            =   8040
      TabIndex        =   64
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":A1BF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef29 
      Height          =   990
      Left            =   8040
      TabIndex        =   65
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":A39F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef28 
      Height          =   990
      Left            =   8040
      TabIndex        =   66
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":A57F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef27 
      Height          =   990
      Left            =   8040
      TabIndex        =   67
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":A75F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef26 
      Height          =   990
      Left            =   8040
      TabIndex        =   68
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":A93F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef36 
      Height          =   990
      Left            =   9840
      TabIndex        =   69
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":AB1F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef35 
      Height          =   990
      Left            =   9840
      TabIndex        =   70
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":ACFF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef34 
      Height          =   990
      Left            =   9840
      TabIndex        =   71
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":AEDF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef33 
      Height          =   990
      Left            =   9840
      TabIndex        =   72
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":B0BF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef32 
      Height          =   990
      Left            =   9840
      TabIndex        =   73
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":B29F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef24 
      Height          =   990
      Left            =   6240
      TabIndex        =   74
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":B47F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef2 
      Height          =   990
      Left            =   480
      TabIndex        =   56
      Top             =   1680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":B65F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef3 
      Height          =   990
      Left            =   480
      TabIndex        =   55
      Top             =   2760
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":B83E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef4 
      Height          =   990
      Left            =   480
      TabIndex        =   54
      Top             =   3840
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":BA1D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef6 
      Height          =   990
      Left            =   480
      TabIndex        =   53
      Top             =   6000
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":BBFC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef5 
      Height          =   990
      Left            =   480
      TabIndex        =   44
      Top             =   4920
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":BDDB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRef1 
      Height          =   990
      Left            =   480
      TabIndex        =   43
      Top             =   600
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":BFBA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   990
      Left            =   5400
      TabIndex        =   75
      Top             =   7680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":C199
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   990
      Left            =   2880
      TabIndex        =   76
      Top             =   7680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Quantifiers.frx":C379
   End
   Begin VB.Label lblField 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Quantifiers"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   4320
      TabIndex        =   16
      Top             =   120
      Width           =   1755
   End
End
Attribute VB_Name = "frmQuantifiers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TheResults As String
Public QuitOn As Boolean

Private Const MaxQuantifiers As Integer = 36

Private MainType As String
Private TotalCodes As Long
Private TotalIndex As Integer
Private CurrentIndex As Integer
Private CurrentRefIndex As Integer
Private BaseBackColor As Long
Private BaseTextColor As Long
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private CurrentContent As Integer
Private TheContent(MaxQuantifiers + 1) As String

Private Sub cmdClear_Click()
Dim i As Integer
If (CurrentContent > 0) Then
    TheContent(CurrentContent) = ""
    For i = CurrentContent To MaxQuantifiers
        TheContent(i) = TheContent(i + 1)
    Next i
    CurrentContent = CurrentContent - 1
    If (CurrentContent < 0) Then
        CurrentContent = 0
    End If
End If
End Sub

Private Sub cmdDone_Click()
Dim i As Integer
If (cmdAppt1.Visible) Then
    For i = 1 To CurrentContent
        If (Trim(TheContent(i)) <> "") Then
            TheResults = TheResults + " " + Trim(TheContent(i))
        End If
    Next i
    Call ReplaceCharacters(TheResults, ")", "),")
    TheResults = Trim(TheResults)
    If (Len(TheResults) > 1) Then
        If (Mid(TheResults, Len(TheResults), 1) = ",") Then
            TheResults = Left(TheResults, Len(TheResults) - 1)
        End If
    End If
    Call ReplaceCharacters(TheResults, " (", " ")
    Call ReplaceCharacters(TheResults, "), ", ", ")
    Call ReplaceCharacters(TheResults, ") ", " ")
    If (Trim(TheResults) <> "") Then
        If (Mid(TheResults, Len(TheResults), 1) = ")") Then
            TheResults = Left(TheResults, Len(TheResults) - 1)
        End If
        If (Mid(TheResults, 1, 1) = "(") Then
            TheResults = Mid(TheResults, 2, Len(TheResults) - 1)
        End If
    End If
    Call StripCharacters(TheResults, "(")
    Call StripCharacters(TheResults, ")")
    Call ReplaceCharacters(TheResults, ",,", ",")
    Call ReplaceCharacters(TheResults, ",,", ",")
    If (Trim(TheResults) <> "") Then
        If (Mid(TheResults, Len(TheResults), 1) = ",") Then
            TheResults = Left(TheResults, Len(TheResults) - 1)
        End If
    End If
    If (Len(TheResults) > 48) Then
        frmEventMsgs.Header = "Quantifiers Description Exceed allowable width. [48]"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        CurrentContent = 0
        Erase TheContent
        TheResults = ""
        Call InitAllButtons(0)
        Call LoadQuantifiers
    Else
        cmdClear.Visible = False
        cmdMore.Visible = False
        QuitOn = False
        Unload frmQuantifiers
    End If
Else
    cmdClear.Visible = False
    cmdMore.Visible = False
    Call ResetRefButtons(False)
    Call ResetButtons(True)
End If
End Sub

Private Sub cmdMore_Click()
If (CurrentRefIndex >= TotalCodes) Then
    CurrentRefIndex = 1
End If
Call TriggerSubQuantifiers(MainType)
End Sub

Private Sub cmdQuit_Click()
QuitOn = True
TheResults = ""
Unload frmQuantifiers
End Sub

Private Sub cmdAppt1_Click()
Call SetItem(cmdAppt1)
End Sub

Private Sub cmdAppt2_Click()
Call SetItem(cmdAppt2)
End Sub

Private Sub cmdAppt3_Click()
Call SetItem(cmdAppt3)
End Sub

Private Sub cmdAppt4_Click()
Call SetItem(cmdAppt4)
End Sub

Private Sub cmdAppt5_Click()
Call SetItem(cmdAppt5)
End Sub

Private Sub cmdAppt6_Click()
Call SetItem(cmdAppt6)
End Sub

Private Sub cmdAppt7_Click()
Call SetItem(cmdAppt7)
End Sub

Private Sub cmdAppt8_Click()
Call SetItem(cmdAppt8)
End Sub

Private Sub cmdAppt9_Click()
Call SetItem(cmdAppt9)
End Sub

Private Sub cmdAppt10_Click()
Call SetItem(cmdAppt10)
End Sub

Private Sub cmdAppt11_Click()
Call SetItem(cmdAppt11)
End Sub

Private Sub cmdAppt12_Click()
Call SetItem(cmdAppt12)
End Sub

Private Sub cmdAppt13_Click()
Call SetItem(cmdAppt13)
End Sub

Private Sub cmdAppt14_Click()
Call SetItem(cmdAppt14)
End Sub

Private Sub cmdAppt15_Click()
Call SetItem(cmdAppt15)
End Sub

Private Sub cmdAppt16_Click()
Call SetItem(cmdAppt16)
End Sub

Private Sub cmdAppt17_Click()
Call SetItem(cmdAppt17)
End Sub

Private Sub cmdAppt18_Click()
Call SetItem(cmdAppt18)
End Sub

Private Sub cmdAppt19_Click()
Call SetItem(cmdAppt19)
End Sub

Private Sub cmdAppt20_Click()
Call SetItem(cmdAppt20)
End Sub

Private Sub cmdAppt21_Click()
Call SetItem(cmdAppt21)
End Sub

Private Sub cmdAppt22_Click()
Call SetItem(cmdAppt22)
End Sub

Private Sub cmdAppt23_Click()
Call SetItem(cmdAppt23)
End Sub

Private Sub cmdAppt24_Click()
Call SetItem(cmdAppt24)
End Sub

Private Sub cmdAppt25_Click()
Call SetItem(cmdAppt25)
End Sub

Private Sub cmdAppt26_Click()
Call SetItem(cmdAppt26)
End Sub

Private Sub cmdAppt27_Click()
Call SetItem(cmdAppt27)
End Sub

Private Sub cmdAppt28_Click()
Call SetItem(cmdAppt28)
End Sub

Private Sub cmdAppt29_Click()
Call SetItem(cmdAppt29)
End Sub

Private Sub cmdAppt30_Click()
Call SetItem(cmdAppt30)
End Sub

Private Sub cmdAppt31_Click()
Call SetItem(cmdAppt31)
End Sub

Private Sub cmdAppt32_Click()
Call SetItem(cmdAppt32)
End Sub

Private Sub cmdAppt33_Click()
Call SetItem(cmdAppt33)
End Sub

Private Sub cmdAppt34_Click()
Call SetItem(cmdAppt34)
End Sub

Private Sub cmdAppt35_Click()
Call SetItem(cmdAppt35)
End Sub

Private Sub cmdAppt36_Click()
Call SetItem(cmdAppt36)
End Sub

Private Sub cmdRef1_Click()
Call SetRefItem(cmdRef1)
End Sub

Private Sub cmdRef2_Click()
Call SetRefItem(cmdRef2)
End Sub

Private Sub cmdRef3_Click()
Call SetRefItem(cmdRef3)
End Sub

Private Sub cmdRef4_Click()
Call SetRefItem(cmdRef4)
End Sub

Private Sub cmdRef5_Click()
Call SetRefItem(cmdRef5)
End Sub

Private Sub cmdRef6_Click()
Call SetRefItem(cmdRef6)
End Sub

Private Sub cmdRef7_Click()
Call SetRefItem(cmdRef7)
End Sub

Private Sub cmdRef8_Click()
Call SetRefItem(cmdRef8)
End Sub

Private Sub cmdRef9_Click()
Call SetRefItem(cmdRef9)
End Sub

Private Sub cmdRef10_Click()
Call SetRefItem(cmdRef10)
End Sub

Private Sub cmdRef11_Click()
Call SetRefItem(cmdRef11)
End Sub

Private Sub cmdRef12_Click()
Call SetRefItem(cmdRef12)
End Sub

Private Sub cmdRef13_Click()
Call SetRefItem(cmdRef13)
End Sub

Private Sub cmdRef14_Click()
Call SetRefItem(cmdRef14)
End Sub

Private Sub cmdRef15_Click()
Call SetRefItem(cmdRef15)
End Sub

Private Sub cmdRef16_Click()
Call SetRefItem(cmdRef16)
End Sub

Private Sub cmdRef17_Click()
Call SetRefItem(cmdRef17)
End Sub

Private Sub cmdRef18_Click()
Call SetRefItem(cmdRef18)
End Sub

Private Sub cmdRef19_Click()
Call SetRefItem(cmdRef19)
End Sub

Private Sub cmdRef20_Click()
Call SetRefItem(cmdRef20)
End Sub

Private Sub cmdRef21_Click()
Call SetRefItem(cmdRef21)
End Sub

Private Sub cmdRef22_Click()
Call SetRefItem(cmdRef22)
End Sub

Private Sub cmdRef23_Click()
Call SetRefItem(cmdRef23)
End Sub

Private Sub cmdRef24_Click()
Call SetRefItem(cmdRef24)
End Sub

Private Sub cmdRef25_Click()
Call SetRefItem(cmdRef25)
End Sub

Private Sub cmdRef26_Click()
Call SetRefItem(cmdRef26)
End Sub

Private Sub cmdRef27_Click()
Call SetRefItem(cmdRef27)
End Sub

Private Sub cmdRef28_Click()
Call SetRefItem(cmdRef28)
End Sub

Private Sub cmdRef29_Click()
Call SetRefItem(cmdRef29)
End Sub

Private Sub cmdRef30_Click()
Call SetRefItem(cmdRef30)
End Sub

Private Sub cmdRef31_Click()
Call SetRefItem(cmdRef31)
End Sub

Private Sub cmdRef32_Click()
Call SetRefItem(cmdRef32)
End Sub

Private Sub cmdRef33_Click()
Call SetRefItem(cmdRef33)
End Sub

Private Sub cmdRef34_Click()
Call SetRefItem(cmdRef34)
End Sub

Private Sub cmdRef35_Click()
Call SetRefItem(cmdRef35)
End Sub

Private Sub cmdRef36_Click()
Call SetRefItem(cmdRef36)
End Sub

Private Function LoadQuantifiers() As Boolean
Dim i As Integer
Dim AText As String
Dim BText As String
Dim ACat As Boolean
Dim AVal As Boolean
CurrentRefIndex = 1
CurrentIndex = 1
TotalIndex = 1
QuitOn = False
cmdClear.Visible = False
cmdMore.Visible = False
Call ResetButtons(False)
Call ResetRefButtons(False)
For i = 1 To MaxQuantifiers
    Call frmHome.GetQualifier(i, AText, BText, ACat, AVal)
    If (i = 1) Then
        Call SetButton(cmdAppt1, AText, BText, ACat, AVal)
    ElseIf (i = 2) Then
        Call SetButton(cmdAppt2, AText, BText, ACat, AVal)
    ElseIf (i = 3) Then
        Call SetButton(cmdAppt3, AText, BText, ACat, AVal)
    ElseIf (i = 4) Then
        Call SetButton(cmdAppt4, AText, BText, ACat, AVal)
    ElseIf (i = 5) Then
        Call SetButton(cmdAppt5, AText, BText, ACat, AVal)
    ElseIf (i = 6) Then
        Call SetButton(cmdAppt6, AText, BText, ACat, AVal)
    ElseIf (i = 7) Then
        Call SetButton(cmdAppt7, AText, BText, ACat, AVal)
    ElseIf (i = 8) Then
        Call SetButton(cmdAppt8, AText, BText, ACat, AVal)
    ElseIf (i = 9) Then
        Call SetButton(cmdAppt9, AText, BText, ACat, AVal)
    ElseIf (i = 10) Then
        Call SetButton(cmdAppt10, AText, BText, ACat, AVal)
    ElseIf (i = 11) Then
        Call SetButton(cmdAppt11, AText, BText, ACat, AVal)
    ElseIf (i = 12) Then
        Call SetButton(cmdAppt12, AText, BText, ACat, AVal)
    ElseIf (i = 13) Then
        Call SetButton(cmdAppt13, AText, BText, ACat, AVal)
    ElseIf (i = 14) Then
        Call SetButton(cmdAppt14, AText, BText, ACat, AVal)
    ElseIf (i = 15) Then
        Call SetButton(cmdAppt15, AText, BText, ACat, AVal)
    ElseIf (i = 16) Then
        Call SetButton(cmdAppt16, AText, BText, ACat, AVal)
    ElseIf (i = 17) Then
        Call SetButton(cmdAppt17, AText, BText, ACat, AVal)
    ElseIf (i = 18) Then
        Call SetButton(cmdAppt18, AText, BText, ACat, AVal)
    ElseIf (i = 19) Then
        Call SetButton(cmdAppt19, AText, BText, ACat, AVal)
    ElseIf (i = 20) Then
        Call SetButton(cmdAppt20, AText, BText, ACat, AVal)
    ElseIf (i = 21) Then
        Call SetButton(cmdAppt21, AText, BText, ACat, AVal)
    ElseIf (i = 22) Then
        Call SetButton(cmdAppt22, AText, BText, ACat, AVal)
    ElseIf (i = 23) Then
        Call SetButton(cmdAppt23, AText, BText, ACat, AVal)
    ElseIf (i = 24) Then
        Call SetButton(cmdAppt24, AText, BText, ACat, AVal)
    ElseIf (i = 25) Then
        Call SetButton(cmdAppt25, AText, BText, ACat, AVal)
    ElseIf (i = 26) Then
        Call SetButton(cmdAppt26, AText, BText, ACat, AVal)
    ElseIf (i = 27) Then
        Call SetButton(cmdAppt27, AText, BText, ACat, AVal)
    ElseIf (i = 28) Then
        Call SetButton(cmdAppt28, AText, BText, ACat, AVal)
    ElseIf (i = 29) Then
        Call SetButton(cmdAppt29, AText, BText, ACat, AVal)
    ElseIf (i = 30) Then
        Call SetButton(cmdAppt30, AText, BText, ACat, AVal)
    ElseIf (i = 31) Then
        Call SetButton(cmdAppt31, AText, BText, ACat, AVal)
    ElseIf (i = 32) Then
        Call SetButton(cmdAppt32, AText, BText, ACat, AVal)
    ElseIf (i = 33) Then
        Call SetButton(cmdAppt33, AText, BText, ACat, AVal)
    ElseIf (i = 34) Then
        Call SetButton(cmdAppt34, AText, BText, ACat, AVal)
    ElseIf (i = 35) Then
        Call SetButton(cmdAppt35, AText, BText, ACat, AVal)
    ElseIf (i = 36) Then
        Call SetButton(cmdAppt36, AText, BText, ACat, AVal)
    End If
Next i
End Function

Private Sub Form_Load()
MainType = ""
TotalCodes = 0
TotalIndex = 0
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdDone.BackColor
BaseTextColor = cmdDone.ForeColor
CurrentContent = 0
Erase TheContent
TheResults = ""
Call InitAllButtons(0)
Call LoadQuantifiers
End Sub

Private Function SetButton(ABut As fpBtn, AText As String, BText As String, ACat As Boolean, AVal As Boolean) As Boolean
Dim Temp As String
If (Trim(AText) <> "") Then
    ABut.Text = AText
    ABut.ToolTipText = BText
    Temp = "FF"
    If (ACat) Then
        Mid(Temp, 1, 1) = "T"
    End If
    If (AVal) Then
        Mid(Temp, 2, 1) = "T"
    End If
    ABut.Tag = Temp
    ABut.Visible = True
End If
End Function

Private Function SetRefButton(ABut As fpBtn, AText As String, BText As String, AVal As Boolean) As Boolean
Dim Temp As String
ABut.Text = AText
ABut.Tag = "F"
If (AVal) Then
    ABut.Tag = "T"
End If
ABut.ToolTipText = BText
ABut.Visible = True
End Function

Private Function ResetButtons(IType As Boolean) As Boolean
cmdAppt1.Visible = VerifyButton(cmdAppt1, IType)
cmdAppt2.Visible = VerifyButton(cmdAppt2, IType)
cmdAppt3.Visible = VerifyButton(cmdAppt3, IType)
cmdAppt4.Visible = VerifyButton(cmdAppt4, IType)
cmdAppt5.Visible = VerifyButton(cmdAppt5, IType)
cmdAppt6.Visible = VerifyButton(cmdAppt6, IType)
cmdAppt7.Visible = VerifyButton(cmdAppt7, IType)
cmdAppt8.Visible = VerifyButton(cmdAppt8, IType)
cmdAppt9.Visible = VerifyButton(cmdAppt9, IType)
cmdAppt10.Visible = VerifyButton(cmdAppt10, IType)
cmdAppt11.Visible = VerifyButton(cmdAppt11, IType)
cmdAppt12.Visible = VerifyButton(cmdAppt12, IType)
cmdAppt13.Visible = VerifyButton(cmdAppt13, IType)
cmdAppt14.Visible = VerifyButton(cmdAppt14, IType)
cmdAppt15.Visible = VerifyButton(cmdAppt15, IType)
cmdAppt16.Visible = VerifyButton(cmdAppt16, IType)
cmdAppt17.Visible = VerifyButton(cmdAppt17, IType)
cmdAppt18.Visible = VerifyButton(cmdAppt18, IType)
cmdAppt19.Visible = VerifyButton(cmdAppt19, IType)
cmdAppt20.Visible = VerifyButton(cmdAppt20, IType)
cmdAppt21.Visible = VerifyButton(cmdAppt21, IType)
cmdAppt22.Visible = VerifyButton(cmdAppt22, IType)
cmdAppt23.Visible = VerifyButton(cmdAppt23, IType)
cmdAppt24.Visible = VerifyButton(cmdAppt24, IType)
cmdAppt25.Visible = VerifyButton(cmdAppt25, IType)
cmdAppt26.Visible = VerifyButton(cmdAppt26, IType)
cmdAppt27.Visible = VerifyButton(cmdAppt27, IType)
cmdAppt28.Visible = VerifyButton(cmdAppt28, IType)
cmdAppt29.Visible = VerifyButton(cmdAppt29, IType)
cmdAppt30.Visible = VerifyButton(cmdAppt30, IType)
cmdAppt31.Visible = VerifyButton(cmdAppt31, IType)
cmdAppt32.Visible = VerifyButton(cmdAppt32, IType)
cmdAppt33.Visible = VerifyButton(cmdAppt33, IType)
cmdAppt34.Visible = VerifyButton(cmdAppt34, IType)
cmdAppt35.Visible = VerifyButton(cmdAppt35, IType)
cmdAppt36.Visible = VerifyButton(cmdAppt36, IType)
End Function

Private Function ResetRefButtons(IType As Boolean) As Boolean
cmdRef1.Visible = VerifyButton(cmdRef1, IType)
cmdRef2.Visible = VerifyButton(cmdRef2, IType)
cmdRef3.Visible = VerifyButton(cmdRef3, IType)
cmdRef4.Visible = VerifyButton(cmdRef4, IType)
cmdRef5.Visible = VerifyButton(cmdRef5, IType)
cmdRef6.Visible = VerifyButton(cmdRef6, IType)
cmdRef7.Visible = VerifyButton(cmdRef7, IType)
cmdRef8.Visible = VerifyButton(cmdRef8, IType)
cmdRef9.Visible = VerifyButton(cmdRef9, IType)
cmdRef10.Visible = VerifyButton(cmdRef10, IType)
cmdRef11.Visible = VerifyButton(cmdRef11, IType)
cmdRef12.Visible = VerifyButton(cmdRef12, IType)
cmdRef13.Visible = VerifyButton(cmdRef13, IType)
cmdRef14.Visible = VerifyButton(cmdRef14, IType)
cmdRef15.Visible = VerifyButton(cmdRef15, IType)
cmdRef16.Visible = VerifyButton(cmdRef16, IType)
cmdRef17.Visible = VerifyButton(cmdRef17, IType)
cmdRef18.Visible = VerifyButton(cmdRef18, IType)
cmdRef19.Visible = VerifyButton(cmdRef19, IType)
cmdRef20.Visible = VerifyButton(cmdRef20, IType)
cmdRef21.Visible = VerifyButton(cmdRef21, IType)
cmdRef22.Visible = VerifyButton(cmdRef22, IType)
cmdRef23.Visible = VerifyButton(cmdRef23, IType)
cmdRef24.Visible = VerifyButton(cmdRef24, IType)
cmdRef25.Visible = VerifyButton(cmdRef25, IType)
cmdRef26.Visible = VerifyButton(cmdRef26, IType)
cmdRef27.Visible = VerifyButton(cmdRef27, IType)
cmdRef28.Visible = VerifyButton(cmdRef28, IType)
cmdRef29.Visible = VerifyButton(cmdRef29, IType)
cmdRef30.Visible = VerifyButton(cmdRef30, IType)
cmdRef31.Visible = VerifyButton(cmdRef31, IType)
cmdRef32.Visible = VerifyButton(cmdRef32, IType)
cmdRef33.Visible = VerifyButton(cmdRef33, IType)
cmdRef34.Visible = VerifyButton(cmdRef34, IType)
cmdRef35.Visible = VerifyButton(cmdRef35, IType)
cmdRef36.Visible = VerifyButton(cmdRef36, IType)
End Function

Private Function VerifyButton(ABtn As fpBtn, AType As Boolean) As Boolean
VerifyButton = AType
If (Left(ABtn.Text, 3) = "Ref") Then
    VerifyButton = False
ElseIf (Left(ABtn.Text, 4) = "Appt") Then
    VerifyButton = False
End If
End Function

Private Sub SetItem(AButton As fpBtn)
Dim i As Integer
Dim j As Integer
Dim Qual As String
If (AButton.BackColor = BaseBackColor) Then
    If (Mid(AButton.Tag, 1, 1) = "T") Then
        CurrentRefIndex = 1
        MainType = Trim(AButton.Text)
        If (TriggerSubQuantifiers(Trim(AButton.Text))) Then
            AButton.BackColor = ButtonSelectionColor
            AButton.ForeColor = TextSelectionColor
            CurrentContent = CurrentContent + 1
            TheContent(CurrentContent) = Trim(AButton.ToolTipText)
        End If
    ElseIf (Mid(AButton.Tag, 2, 1) = "T") Then
        frmNumericPad.NumPad_Result = ""
        frmNumericPad.NumPad_Field = "Quantifier - " + Trim(AButton.Text)
        frmNumericPad.NumPad_Max = 0
        frmNumericPad.NumPad_Min = 0
        frmNumericPad.NumPad_EyesOn = False
        frmNumericPad.NumPad_CommentOn = False
        frmNumericPad.NumPad_TimeFrameOn = False
        frmNumericPad.NumPad_DisplayFull = True
        frmNumericPad.NumPad_Choice1 = ""
        frmNumericPad.NumPad_Choice2 = ""
        frmNumericPad.NumPad_Choice3 = ""
        frmNumericPad.NumPad_Choice4 = ""
        frmNumericPad.NumPad_Choice5 = ""
        frmNumericPad.NumPad_Choice6 = ""
        frmNumericPad.NumPad_Choice7 = ""
        frmNumericPad.NumPad_Choice8 = ""
        frmNumericPad.NumPad_Choice9 = ""
        frmNumericPad.NumPad_Choice10 = ""
        frmNumericPad.NumPad_Choice11 = ""
        frmNumericPad.NumPad_Choice12 = ""
        frmNumericPad.NumPad_Choice13 = ""
        frmNumericPad.NumPad_Choice14 = ""
        frmNumericPad.NumPad_Choice15 = ""
        frmNumericPad.NumPad_Choice16 = ""
        frmNumericPad.NumPad_Choice17 = ""
        frmNumericPad.NumPad_Choice18 = ""
        frmNumericPad.NumPad_Choice19 = ""
        frmNumericPad.NumPad_Choice20 = ""
        frmNumericPad.NumPad_Choice21 = ""
        frmNumericPad.NumPad_Choice22 = ""
        frmNumericPad.NumPad_Choice23 = ""
        frmNumericPad.NumPad_Choice24 = ""
        frmNumericPad.Show 1
        If Not (frmNumericPad.NumPad_Quit) Then
            Qual = Trim(frmNumericPad.NumPad_Result)
        End If
        If (frmNumericPad.ClearValueOn) Then
            Qual = ""
        End If
        If (Trim(Qual) <> "") Then
            AButton.BackColor = ButtonSelectionColor
            AButton.ForeColor = TextSelectionColor
            CurrentContent = CurrentContent + 1
            TheContent(CurrentContent) = Trim(Qual) + " " + Trim(AButton.ToolTipText)
        End If
    Else
        AButton.BackColor = ButtonSelectionColor
        AButton.ForeColor = TextSelectionColor
        CurrentContent = CurrentContent + 1
        TheContent(CurrentContent) = Trim(AButton.ToolTipText)
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    For i = 1 To CurrentContent
        If (InStrPS(TheContent(i), Trim(AButton.ToolTipText)) <> 0) Then
            For j = i To CurrentContent
                TheContent(j) = TheContent(j + 1)
            Next j
            CurrentContent = CurrentContent - 1
            If (CurrentContent < 0) Then
                CurrentContent = 0
            End If
            Exit For
        End If
    Next i
End If
End Sub

Private Sub SetRefItem(AButton As fpBtn)
Dim Qual As String
If (AButton.BackColor = BaseBackColor) Then
    If (AButton.Tag = "T") Then
        frmNumericPad.NumPad_Result = ""
        frmNumericPad.NumPad_Field = "SubQuantifier - " + Trim(AButton.Text)
        frmNumericPad.NumPad_Max = 0
        frmNumericPad.NumPad_Min = 0
        frmNumericPad.NumPad_EyesOn = False
        frmNumericPad.NumPad_CommentOn = False
        frmNumericPad.NumPad_TimeFrameOn = False
        frmNumericPad.NumPad_DisplayFull = True
        frmNumericPad.NumPad_Choice1 = ""
        frmNumericPad.NumPad_Choice2 = ""
        frmNumericPad.NumPad_Choice3 = ""
        frmNumericPad.NumPad_Choice4 = ""
        frmNumericPad.NumPad_Choice5 = ""
        frmNumericPad.NumPad_Choice6 = ""
        frmNumericPad.NumPad_Choice7 = ""
        frmNumericPad.NumPad_Choice8 = ""
        frmNumericPad.NumPad_Choice9 = ""
        frmNumericPad.NumPad_Choice10 = ""
        frmNumericPad.NumPad_Choice11 = ""
        frmNumericPad.NumPad_Choice12 = ""
        frmNumericPad.NumPad_Choice13 = ""
        frmNumericPad.NumPad_Choice14 = ""
        frmNumericPad.NumPad_Choice15 = ""
        frmNumericPad.NumPad_Choice16 = ""
        frmNumericPad.NumPad_Choice17 = ""
        frmNumericPad.NumPad_Choice18 = ""
        frmNumericPad.NumPad_Choice19 = ""
        frmNumericPad.NumPad_Choice20 = ""
        frmNumericPad.NumPad_Choice21 = ""
        frmNumericPad.NumPad_Choice22 = ""
        frmNumericPad.NumPad_Choice23 = ""
        frmNumericPad.NumPad_Choice24 = ""
        frmNumericPad.Show 1
        If Not (frmNumericPad.NumPad_Quit) Then
            Qual = Trim(frmNumericPad.NumPad_Result)
        End If
        If (Trim(Qual) <> "") Then
            AButton.BackColor = ButtonSelectionColor
            AButton.ForeColor = TextSelectionColor
            If (Trim(AButton.ToolTipText) <> "") Then
                TheContent(CurrentContent) = Trim(TheContent(CurrentContent)) + " (" + Qual + " " + Trim(AButton.ToolTipText) + ") "
            Else
                TheContent(CurrentContent) = Trim(TheContent(CurrentContent)) + " (" + Qual + " " + Trim(AButton.Text) + ") "
            End If
        End If
    Else
        AButton.BackColor = ButtonSelectionColor
        AButton.ForeColor = TextSelectionColor
        If (Trim(AButton.ToolTipText) <> "") Then
            TheContent(CurrentContent) = Trim(TheContent(CurrentContent)) + " (" + Trim(AButton.ToolTipText) + ") "
        Else
            TheContent(CurrentContent) = Trim(TheContent(CurrentContent)) + " (" + Trim(AButton.Text) + ") "
        End If
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    If (Trim(AButton.ToolTipText) <> "") Then
        Call ReplaceCharacters(TheContent(CurrentContent), " (" + Trim(AButton.ToolTipText) + ")", " ")
    Else
        Call ReplaceCharacters(TheContent(CurrentContent), " (" + Trim(AButton.Text) + ")", " ")
    End If
End If
End Sub

Private Function InitAllButtons(OType As Integer) As Boolean
If (OType = 0) Or (OType = 1) Then
    cmdAppt1.BackColor = BaseBackColor
    cmdAppt1.ForeColor = BaseTextColor
    cmdAppt2.BackColor = BaseBackColor
    cmdAppt2.ForeColor = BaseTextColor
    cmdAppt3.BackColor = BaseBackColor
    cmdAppt3.ForeColor = BaseTextColor
    cmdAppt4.BackColor = BaseBackColor
    cmdAppt4.ForeColor = BaseTextColor
    cmdAppt5.BackColor = BaseBackColor
    cmdAppt5.ForeColor = BaseTextColor
    cmdAppt6.BackColor = BaseBackColor
    cmdAppt6.ForeColor = BaseTextColor
    cmdAppt7.BackColor = BaseBackColor
    cmdAppt7.ForeColor = BaseTextColor
    cmdAppt8.BackColor = BaseBackColor
    cmdAppt8.ForeColor = BaseTextColor
    cmdAppt9.BackColor = BaseBackColor
    cmdAppt9.ForeColor = BaseTextColor
    cmdAppt10.BackColor = BaseBackColor
    cmdAppt10.ForeColor = BaseTextColor
    cmdAppt11.BackColor = BaseBackColor
    cmdAppt11.ForeColor = BaseTextColor
    cmdAppt12.BackColor = BaseBackColor
    cmdAppt12.ForeColor = BaseTextColor
    cmdAppt13.BackColor = BaseBackColor
    cmdAppt13.ForeColor = BaseTextColor
    cmdAppt14.BackColor = BaseBackColor
    cmdAppt14.ForeColor = BaseTextColor
    cmdAppt15.BackColor = BaseBackColor
    cmdAppt15.ForeColor = BaseTextColor
    cmdAppt16.BackColor = BaseBackColor
    cmdAppt16.ForeColor = BaseTextColor
    cmdAppt17.BackColor = BaseBackColor
    cmdAppt17.ForeColor = BaseTextColor
    cmdAppt18.BackColor = BaseBackColor
    cmdAppt18.ForeColor = BaseTextColor
    cmdAppt19.BackColor = BaseBackColor
    cmdAppt19.ForeColor = BaseTextColor
    cmdAppt20.BackColor = BaseBackColor
    cmdAppt20.ForeColor = BaseTextColor
    cmdAppt21.BackColor = BaseBackColor
    cmdAppt21.ForeColor = BaseTextColor
    cmdAppt22.BackColor = BaseBackColor
    cmdAppt22.ForeColor = BaseTextColor
    cmdAppt23.BackColor = BaseBackColor
    cmdAppt23.ForeColor = BaseTextColor
    cmdAppt24.BackColor = BaseBackColor
    cmdAppt24.ForeColor = BaseTextColor
    cmdAppt25.BackColor = BaseBackColor
    cmdAppt25.ForeColor = BaseTextColor
    cmdAppt26.BackColor = BaseBackColor
    cmdAppt26.ForeColor = BaseTextColor
    cmdAppt27.BackColor = BaseBackColor
    cmdAppt27.ForeColor = BaseTextColor
    cmdAppt28.BackColor = BaseBackColor
    cmdAppt28.ForeColor = BaseTextColor
    cmdAppt29.BackColor = BaseBackColor
    cmdAppt29.ForeColor = BaseTextColor
    cmdAppt30.BackColor = BaseBackColor
    cmdAppt30.ForeColor = BaseTextColor
    cmdAppt31.BackColor = BaseBackColor
    cmdAppt31.ForeColor = BaseTextColor
    cmdAppt32.BackColor = BaseBackColor
    cmdAppt32.ForeColor = BaseTextColor
    cmdAppt33.BackColor = BaseBackColor
    cmdAppt33.ForeColor = BaseTextColor
    cmdAppt34.BackColor = BaseBackColor
    cmdAppt34.ForeColor = BaseTextColor
    cmdAppt35.BackColor = BaseBackColor
    cmdAppt35.ForeColor = BaseTextColor
    cmdAppt36.BackColor = BaseBackColor
    cmdAppt36.ForeColor = BaseTextColor
End If

If (OType = 0) Or (OType = 2) Then
    cmdRef1.BackColor = BaseBackColor
    cmdRef1.ForeColor = BaseTextColor
    cmdRef1.Text = "Ref1"
    cmdRef2.BackColor = BaseBackColor
    cmdRef2.ForeColor = BaseTextColor
    cmdRef2.Text = "Ref2"
    cmdRef3.BackColor = BaseBackColor
    cmdRef3.ForeColor = BaseTextColor
    cmdRef3.Text = "Ref3"
    cmdRef4.BackColor = BaseBackColor
    cmdRef4.ForeColor = BaseTextColor
    cmdRef4.Text = "Ref4"
    cmdRef5.BackColor = BaseBackColor
    cmdRef5.ForeColor = BaseTextColor
    cmdRef5.Text = "Ref5"
    cmdRef6.BackColor = BaseBackColor
    cmdRef6.ForeColor = BaseTextColor
    cmdRef6.Text = "Ref6"
    cmdRef7.BackColor = BaseBackColor
    cmdRef7.ForeColor = BaseTextColor
    cmdRef7.Text = "Ref7"
    cmdRef8.BackColor = BaseBackColor
    cmdRef8.ForeColor = BaseTextColor
    cmdRef8.Text = "Ref8"
    cmdRef9.BackColor = BaseBackColor
    cmdRef9.ForeColor = BaseTextColor
    cmdRef9.Text = "Ref9"
    cmdRef10.BackColor = BaseBackColor
    cmdRef10.ForeColor = BaseTextColor
    cmdRef10.Text = "Ref10"
    cmdRef11.BackColor = BaseBackColor
    cmdRef11.ForeColor = BaseTextColor
    cmdRef11.Text = "Ref11"
    cmdRef12.BackColor = BaseBackColor
    cmdRef12.ForeColor = BaseTextColor
    cmdRef12.Text = "Ref12"
    cmdRef13.BackColor = BaseBackColor
    cmdRef13.ForeColor = BaseTextColor
    cmdRef13.Text = "Ref13"
    cmdRef14.BackColor = BaseBackColor
    cmdRef14.ForeColor = BaseTextColor
    cmdRef14.Text = "Ref14"
    cmdRef15.BackColor = BaseBackColor
    cmdRef15.ForeColor = BaseTextColor
    cmdRef15.Text = "Ref15"
    cmdRef16.BackColor = BaseBackColor
    cmdRef16.ForeColor = BaseTextColor
    cmdRef16.Text = "Ref16"
    cmdRef17.BackColor = BaseBackColor
    cmdRef17.ForeColor = BaseTextColor
    cmdRef17.Text = "Ref17"
    cmdRef18.BackColor = BaseBackColor
    cmdRef18.ForeColor = BaseTextColor
    cmdRef18.Text = "Ref18"
    cmdRef19.BackColor = BaseBackColor
    cmdRef19.ForeColor = BaseTextColor
    cmdRef19.Text = "Ref19"
    cmdRef20.BackColor = BaseBackColor
    cmdRef20.ForeColor = BaseTextColor
    cmdRef20.Text = "Ref20"
    cmdRef21.BackColor = BaseBackColor
    cmdRef21.ForeColor = BaseTextColor
    cmdRef21.Text = "Ref21"
    cmdRef22.BackColor = BaseBackColor
    cmdRef22.ForeColor = BaseTextColor
    cmdRef22.Text = "Ref22"
    cmdRef23.BackColor = BaseBackColor
    cmdRef23.ForeColor = BaseTextColor
    cmdRef23.Text = "Ref23"
    cmdRef24.BackColor = BaseBackColor
    cmdRef24.ForeColor = BaseTextColor
    cmdRef24.Text = "Ref24"
    cmdRef25.BackColor = BaseBackColor
    cmdRef25.ForeColor = BaseTextColor
    cmdRef25.Text = "Ref25"
    cmdRef26.BackColor = BaseBackColor
    cmdRef26.ForeColor = BaseTextColor
    cmdRef26.Text = "Ref26"
    cmdRef27.BackColor = BaseBackColor
    cmdRef27.ForeColor = BaseTextColor
    cmdRef27.Text = "Ref27"
    cmdRef28.BackColor = BaseBackColor
    cmdRef28.ForeColor = BaseTextColor
    cmdRef28.Text = "Ref28"
    cmdRef29.BackColor = BaseBackColor
    cmdRef29.ForeColor = BaseTextColor
    cmdRef29.Text = "Ref29"
    cmdRef30.BackColor = BaseBackColor
    cmdRef30.ForeColor = BaseTextColor
    cmdRef30.Text = "Ref30"
    cmdRef31.BackColor = BaseBackColor
    cmdRef31.ForeColor = BaseTextColor
    cmdRef31.Text = "Ref31"
    cmdRef32.BackColor = BaseBackColor
    cmdRef32.ForeColor = BaseTextColor
    cmdRef32.Text = "Ref32"
    cmdRef33.BackColor = BaseBackColor
    cmdRef33.ForeColor = BaseTextColor
    cmdRef33.Text = "Ref33"
    cmdRef34.BackColor = BaseBackColor
    cmdRef34.ForeColor = BaseTextColor
    cmdRef34.Text = "Ref34"
    cmdRef35.BackColor = BaseBackColor
    cmdRef35.ForeColor = BaseTextColor
    cmdRef35.Text = "Ref35"
    cmdRef36.BackColor = BaseBackColor
    cmdRef36.ForeColor = BaseTextColor
    cmdRef36.Text = "Ref36"
End If
End Function

Private Function TriggerSubQuantifiers(QType As String) As Boolean
Dim i As Integer
Dim b As Integer
Dim RetCode As PracticeCodes
Dim AText As String
Dim BText As String
Dim AVal As Boolean
TriggerSubQuantifiers = False
TotalCodes = 0
If (Trim(QType) <> "") Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "Quantifiers" + Trim(QType)
    TotalCodes = RetCode.FindCode
    If (TotalCodes > 0) Then
        b = 1
        i = CurrentRefIndex
        Call ResetRefButtons(False)
        Call InitAllButtons(2)
        While (RetCode.SelectCode(i)) And (b < 37)
            AText = Trim(RetCode.ReferenceCode)
            BText = Trim(RetCode.ReferenceAlternateCode)
            AVal = False
            If (Trim(RetCode.Signature) = "T") Then
                AVal = True
            End If
            If (b = 1) Then
                Call SetRefButton(cmdRef1, AText, BText, AVal)
            ElseIf (b = 2) Then
                Call SetRefButton(cmdRef2, AText, BText, AVal)
            ElseIf (b = 3) Then
                Call SetRefButton(cmdRef3, AText, BText, AVal)
            ElseIf (b = 4) Then
                Call SetRefButton(cmdRef4, AText, BText, AVal)
            ElseIf (b = 5) Then
                Call SetRefButton(cmdRef5, AText, BText, AVal)
            ElseIf (b = 6) Then
                Call SetRefButton(cmdRef6, AText, BText, AVal)
            ElseIf (b = 7) Then
                Call SetRefButton(cmdRef7, AText, BText, AVal)
            ElseIf (b = 8) Then
                Call SetRefButton(cmdRef8, AText, BText, AVal)
            ElseIf (b = 9) Then
                Call SetRefButton(cmdRef9, AText, BText, AVal)
            ElseIf (b = 10) Then
                Call SetRefButton(cmdRef10, AText, BText, AVal)
            ElseIf (b = 11) Then
                Call SetRefButton(cmdRef11, AText, BText, AVal)
            ElseIf (b = 12) Then
                Call SetRefButton(cmdRef12, AText, BText, AVal)
            ElseIf (b = 13) Then
                Call SetRefButton(cmdRef13, AText, BText, AVal)
            ElseIf (b = 14) Then
                Call SetRefButton(cmdRef14, AText, BText, AVal)
            ElseIf (b = 15) Then
                Call SetRefButton(cmdRef15, AText, BText, AVal)
            ElseIf (b = 16) Then
                Call SetRefButton(cmdRef16, AText, BText, AVal)
            ElseIf (b = 17) Then
                Call SetRefButton(cmdRef17, AText, BText, AVal)
            ElseIf (b = 18) Then
                Call SetRefButton(cmdRef18, AText, BText, AVal)
            ElseIf (b = 19) Then
                Call SetRefButton(cmdRef19, AText, BText, AVal)
            ElseIf (b = 20) Then
                Call SetRefButton(cmdRef20, AText, BText, AVal)
            ElseIf (b = 21) Then
                Call SetRefButton(cmdRef21, AText, BText, AVal)
            ElseIf (b = 22) Then
                Call SetRefButton(cmdRef22, AText, BText, AVal)
            ElseIf (b = 23) Then
                Call SetRefButton(cmdRef23, AText, BText, AVal)
            ElseIf (b = 24) Then
                Call SetRefButton(cmdRef24, AText, BText, AVal)
            ElseIf (b = 25) Then
                Call SetRefButton(cmdRef25, AText, BText, AVal)
            ElseIf (b = 26) Then
                Call SetRefButton(cmdRef26, AText, BText, AVal)
            ElseIf (b = 27) Then
                Call SetRefButton(cmdRef27, AText, BText, AVal)
            ElseIf (b = 28) Then
                Call SetRefButton(cmdRef28, AText, BText, AVal)
            ElseIf (b = 29) Then
                Call SetRefButton(cmdRef29, AText, BText, AVal)
            ElseIf (b = 30) Then
                Call SetRefButton(cmdRef30, AText, BText, AVal)
            ElseIf (b = 31) Then
                Call SetRefButton(cmdRef31, AText, BText, AVal)
            ElseIf (b = 32) Then
                Call SetRefButton(cmdRef32, AText, BText, AVal)
            ElseIf (b = 33) Then
                Call SetRefButton(cmdRef33, AText, BText, AVal)
            ElseIf (b = 34) Then
                Call SetRefButton(cmdRef34, AText, BText, AVal)
            ElseIf (b = 35) Then
                Call SetRefButton(cmdRef35, AText, BText, AVal)
            ElseIf (b = 36) Then
                Call SetRefButton(cmdRef36, AText, BText, AVal)
            End If
            i = i + 1
            b = b + 1
        Wend
        TriggerSubQuantifiers = True
        Call ResetButtons(False)
        Call ResetRefButtons(True)
        cmdClear.Visible = True
    End If
    Set RetCode = Nothing
End If
CurrentRefIndex = i - 1
cmdMore.Visible = False
If (TotalCodes > 36) Then
    cmdMore.Visible = True
End If
End Function

