VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmSurgFollow 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstSta 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3420
      ItemData        =   "SurgFollow.frx":0000
      Left            =   3240
      List            =   "SurgFollow.frx":0002
      TabIndex        =   0
      Top             =   2640
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   6720
      TabIndex        =   5
      Top             =   360
      Width           =   1695
   End
   Begin VB.ListBox lstDocs 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6060
      Left            =   360
      MultiSelect     =   1  'Simple
      TabIndex        =   4
      Top             =   1200
      Width           =   11535
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   8520
      TabIndex        =   3
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   10200
      TabIndex        =   2
      Top             =   360
      Width           =   1575
   End
   Begin VB.ListBox lstTests 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Left            =   4560
      TabIndex        =   1
      Top             =   360
      Width           =   2055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   10200
      TabIndex        =   6
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgFollow.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDisplay 
      Height          =   1110
      Left            =   3600
      TabIndex        =   7
      Top             =   7440
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SurgFollow.frx":01E3
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   240
      TabIndex        =   14
      Top             =   840
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo2 
      Height          =   375
      Left            =   2640
      TabIndex        =   15
      Top             =   840
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   2160
      TabIndex        =   16
      Top             =   840
      Width           =   345
   End
   Begin VB.Label lblLoad 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loading Patient Information"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   240
      TabIndex        =   13
      Top             =   480
      Visible         =   0   'False
      Width           =   3315
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Surgeries"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   240
      TabIndex        =   12
      Top             =   120
      Width           =   1230
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   8520
      TabIndex        =   11
      Top             =   120
      Width           =   570
   End
   Begin VB.Label lblLoc 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   10200
      TabIndex        =   10
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblStatus 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6720
      TabIndex        =   9
      Top             =   120
      Width           =   570
   End
   Begin VB.Label lblTests 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Surgery Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   4560
      TabIndex        =   8
      Top             =   120
      Width           =   1155
   End
End
Attribute VB_Name = "frmSurgFollow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ResourceName As String
Public QuitOn As Boolean
Private TriggerOn As Boolean
Private PatientId As Long
Private ResourceId As Long
Private ResourceLocId As Long
Private TheTest As String
Private TheStatus As String
Private StartDate As String
Private EndDate As String
Private PageCnt As Integer
Private TotalPageCnt As Integer
Private CurrentIndex As Integer
Private MidCurrentIndex As Integer
Private PrevCurrentIndex As Integer
Private TotalFound As Integer

Private TriggeredClnId As Long
Private ApplStartAt As Integer
Private ApplReturnCount As Integer
Private ApplLocId As Long
Private ApplApptId As Long
Private ApplPatientId As Long
Private ApplClinicalId As Long
Private ApplDate As String
Private ApplHPhone As String
Private ApplSymptom As String
Private ApplFinding As String
Private ApplDetail1 As String
Private ApplDetail2 As String
Private ApplClnType As String
Private ApplResource As String
Private ApplSurgery As String
Private ApplLastName As String
Private ApplApptDate As String
Private ApplFirstName As String
Private ApplStartDate As String
Private ApplEndDate As String
Private ApplClnStatus As String
Private ApplMiddleName As String
Private ApplConfirmStatus As String
Private ApplTransactionRef As String

Private ApplListTotal As Long
Private ApplListTbl As ADODB.Recordset
Private Const MaxCount As Integer = 25

Private Sub cmdDisplay_Click()
lblLoad.Caption = "Load Matching Results"
lblLoad.Visible = True
DoEvents
CurrentIndex = 1
Call LoadSurgeryList(False)
lblLoad.Caption = "Load Patient Information"
lblLoad.Visible = False
DoEvents
End Sub

Private Sub cmdDone_Click()
Unload frmSurgFollow
QuitOn = False
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo1.Date) <> "") Then
        EndDate = SSDateCombo1.DateValue
    Else
        ATemp = EndDate
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        EndDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        SSDateCombo1.Text = EndDate
    End If
    Call FormatTodaysDate(EndDate, False)
    If (EndDate = "") Then
        SSDateCombo1.Text = ""
    End If
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        EndDate = EndDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(EndDate) > 0) Then
        EndDate = Left(EndDate, Len(StartDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(EndDate) < 1) Then
        EndDate = ""
    End If
End If
End Sub

Private Sub SSDateCombo1_Validate(Cancel As Boolean)
Call SSDateCombo1_KeyPress(13)
End Sub

Private Sub SSDateCombo2_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo2.Date) <> "") Then
        StartDate = SSDateCombo2.DateValue
    Else
        ATemp = StartDate
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        StartDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        SSDateCombo1.Text = StartDate
    End If
    Call FormatTodaysDate(StartDate, False)
    If (StartDate = "") Then
        SSDateCombo1.Text = ""
    End If
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        StartDate = StartDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(StartDate) > 0) Then
        StartDate = Left(StartDate, Len(StartDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(StartDate) < 1) Then
        StartDate = ""
    End If
End If
End Sub

Private Sub SSDateCombo2_Validate(Cancel As Boolean)
Call SSDateCombo2_KeyPress(13)
End Sub

Public Function LoadSurgeryList(InitOn As Boolean) As Boolean
Dim u As Integer
Dim ADate As String
LoadSurgeryList = True
If (InitOn) Then
    ADate = ""
    Call FormatTodaysDate(ADate, False)
    SSDateCombo1.DateValue = ADate
    SSDateCombo2.DateValue = ADate
    StartDate = ADate
    EndDate = ADate
    TheStatus = ""
    TheTest = ""
    Call ApplLoadStaff(lstDr)
    u = InStrPS(ResourceName, "-")
    If (u > 0) Then
        ResourceId = Val(Mid(ResourceName, 2, u - 1))
    Else
        ResourceId = -1
    End If
    Call ApplLoadLocation(lstLoc)
    ResourceLocId = dbPracticeId
    Call ApplGetCodesbyList("SURGERYSTATUS", False, True, lstStatus)
    Call ApplGetCodesbyList("SURGERYTYPE", False, True, lstTests)
    TriggeredClnId = 0
    CurrentIndex = 1
    PrevCurrentIndex = 1
    MidCurrentIndex = 1
    Exit Function
End If
lstDocs.Clear
ApplLocId = 0
ApplApptId = 0
ApplPatientId = 0
ApplClinicalId = 0
ApplSymptom = ""
ApplFinding = ""
ApplApptDate = ""
ApplConfirmStatus = ""
ApplStartDate = Mid(StartDate, 7, 4) + Mid(StartDate, 1, 2) + Mid(StartDate, 4, 2)
ApplEndDate = Mid(EndDate, 7, 4) + Mid(EndDate, 1, 2) + Mid(EndDate, 4, 2)
ApplConfirmStatus = TheStatus
ApplTransactionRef = TheTest
ApplStartAt = CurrentIndex
MidCurrentIndex = CurrentIndex
TotalFound = ApplRetrieveReviewSurgeryList
lstDocs.ListIndex = -1
End Function

Private Sub lstDocs_Click()
Dim u As Integer, MyCheck As Boolean
Dim AptId As Long, ActId As Long
Dim PatId As Long, ClnId As Long
If (lstDocs.ListIndex >= 0) Then
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Surgical Request"
    frmEventMsgs.RejectText = "Go To Edit Previous"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Set Status"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "*")
        If (u > 0) Then
            ClnId = Val(Mid(lstDocs.List(lstDocs.ListIndex), u + 1, Len(lstDocs.List(lstDocs.ListIndex)) - u))
            If (ClnId > 0) Then
                AptId = Val(Mid(lstDocs.List(lstDocs.ListIndex), 96, (u - 1) - 95))
                If (AptId > 0) Then
                    ActId = ApplGetActivityId(AptId, MyCheck)
                    If (ActId > 0) Then
                        If (ApplSurgeryFollowUpTrigger(ClnId, ActId, MyCheck)) Then
                            TriggeredClnId = ClnId
                            CurrentIndex = MidCurrentIndex
                            Call LoadSurgeryList(False)
                        End If
                    End If
                End If
            End If
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "/")
        If (u > 0) Then
            AptId = Val(Mid(lstDocs.List(lstDocs.ListIndex), 96, (u - 1) - 95))
            If (AptId > 0) Then
                ActId = ApplGetActivityId(AptId, MyCheck)
                If (ActId > 0) Then
                    Call SelectSystems(ActId)
                    CurrentIndex = MidCurrentIndex
                    Call LoadSurgeryList(False)
                Else
                    frmEventMsgs.Header = "Patient has an active appointment, close it before editing"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                End If
            End If
        End If
        lblLoad.Visible = False
    ElseIf (frmEventMsgs.Result = 3) Then
        u = InStrPS(95, lstDocs.List(lstDocs.ListIndex), "*")
        If (u > 0) Then
            ClnId = Val(Mid(lstDocs.List(lstDocs.ListIndex), u + 1, Len(lstDocs.List(lstDocs.ListIndex)) - u))
            If (ClnId > 0) Then
                TriggeredClnId = ClnId
                lstSta.Clear
                lstSta.AddItem "Close FollowUp Status"
                For u = 1 To lstStatus.ListCount - 1
                    lstSta.AddItem lstStatus.List(u)
                Next u
                lstSta.Visible = True
                lstDr.Enabled = False
                lstLoc.Enabled = False
                lstDocs.Enabled = False
                lstTests.Enabled = False
            End If
        End If
    End If
End If
frmSurgFollow.ZOrder 0
End Sub

Private Sub lstDocs_dblClick()
If (lstDocs.ListIndex >= 0) Then
    Call lstDocs_Click
End If
End Sub

Private Sub lstLoc_Click()
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        ResourceLocId = -1
        lstLoc.ListIndex = -1
    Else
        ResourceLocId = ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
    End If
End If
End Sub

Private Sub lstDr_Click()
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        ResourceId = -1
        lstDr.ListIndex = -1
    Else
        ResourceId = ApplGetListResourceId(lstDr.List(lstDr.ListIndex))
    End If
End If
End Sub

Private Sub lstStatus_Click()
If (lstStatus.ListIndex >= 0) Then
    If (lstStatus.ListIndex = 0) Then
        TheStatus = ""
    Else
        TheStatus = Left(lstStatus.List(lstStatus.ListIndex), 1)
    End If
End If
End Sub

Private Sub lstTests_Click()
If (lstTests.ListIndex >= 0) Then
    If (TheTest = Trim(lstTests.List(lstTests.ListIndex))) Or (lstTests.ListIndex = 0) Then
        TheTest = ""
    Else
        TheTest = Trim(lstTests.List(lstTests.ListIndex))
    End If
End If
End Sub

Private Sub lstSta_Click()
Dim Temp As String
If (lstSta.ListIndex > 0) Then
    If (TriggeredClnId > 0) Then
        Temp = Left(lstSta.List(lstSta.ListIndex), 1)
        Call ApplSetSurgeryStatus(TriggeredClnId, Temp)
        TriggeredClnId = 0
        If (Temp <> TheStatus) Then
            CurrentIndex = 1
            Call LoadSurgeryList(False)
        End If
    End If
    lstSta.Visible = False
    lstDr.Enabled = True
    lstLoc.Enabled = True
    lstDocs.Enabled = True
    lstTests.Enabled = True
ElseIf (lstSta.ListIndex = 0) Then
    TriggeredClnId = 0
    lstSta.Visible = False
    lstDr.Enabled = True
    lstLoc.Enabled = True
    lstDocs.Enabled = True
    lstTests.Enabled = True
End If
End Sub

Public Function ApplSetSurgeryStatus(ClnId As Long, AStat As String) As Boolean
Dim RetCln As PatientClinical
ApplSetSurgeryStatus = False
If (ClnId > 0) And (Trim(AStat) <> "") Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        RetCln.ClinicalSurgery = AStat
        Call RetCln.ApplyPatientClinical
    End If
    Set RetCln = Nothing
End If
End Function

Public Function ApplGetListResourceId(TheItem As String) As Long
ApplGetListResourceId = 0
If (Len(Trim(TheItem)) > 75) Then
    ApplGetListResourceId = Val(Trim(Mid(TheItem, 76, Len(TheItem) - 75)))
ElseIf (Len(Trim(TheItem)) > 56) Then
    ApplGetListResourceId = Val(Trim(Mid(TheItem, 56, Len(TheItem) - 55)))
End If
End Function

Private Function ApplLoadLocation(ALstBox As ListBox) As Boolean
Dim i As Integer
Dim Appt As Integer
Dim DisplayText As String
Dim RetPrc As PracticeName
Dim RetDr As SchedulerResource
ApplLoadLocation = True
ALstBox.Clear
DisplayText = Space(56)
Mid(DisplayText, 1, 13) = "All Locations"
DisplayText = DisplayText + Trim(Str(-1))
ALstBox.AddItem DisplayText
Set RetPrc = New PracticeName
RetPrc.PracticeType = "P"
RetPrc.PracticeName = Chr(1)
If (RetPrc.FindPractice > 0) Then
    i = 1
    While (RetPrc.SelectPractice(i))
        DisplayText = Space(56)
        Mid(DisplayText, 1, 6) = "Office"
        If (Trim(RetPrc.PracticeLocationReference) <> "") Then
            Mid(DisplayText, 7, Len(Trim(RetPrc.PracticeLocationReference)) + 1) = "-" + Trim(RetPrc.PracticeLocationReference)
            DisplayText = DisplayText + Trim(Str(1000 + RetPrc.PracticeId))
        Else
            DisplayText = DisplayText + Trim(Str(0))
        End If
        ALstBox.AddItem DisplayText
        i = i + 1
    Wend
End If
Set RetPrc = Nothing
Set RetDr = New SchedulerResource
RetDr.ResourceName = Chr(1)
If (RetDr.FindResourcebyRoom) Then
    i = 1
    While (RetDr.SelectResource(i))
        If (RetDr.ResourceServiceCode = "02") Then
            DisplayText = Space(75)
            Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
            DisplayText = DisplayText + Trim(Str(RetDr.ResourceId))
            ALstBox.AddItem DisplayText
        End If
        i = i + 1
    Wend
End If
Set RetDr = Nothing
End Function

Private Function ApplLoadStaff(ALstBox As ListBox) As Boolean
Dim i As Integer
Dim Appt As Integer
Dim DisplayText As String
Dim RetDr As SchedulerResource
ApplLoadStaff = True
ALstBox.Clear
DisplayText = Space(75)
Mid(DisplayText, 1, 13) = "All Resources"
DisplayText = DisplayText + Trim(Str(0))
ALstBox.AddItem DisplayText
Set RetDr = New SchedulerResource
RetDr.ResourceName = Chr(1)
If (RetDr.FindResourcebyDoctor) Then
    i = 1
    While (RetDr.SelectResource(i))
        DisplayText = Space(75)
        Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
        DisplayText = DisplayText + Trim(Str(RetDr.ResourceId))
        ALstBox.AddItem DisplayText
        i = i + 1
    Wend
End If
Set RetDr = Nothing
End Function

Public Function ApplGetCodesbyList(CodeType As String, SetIt As Boolean, ViewAll As Boolean, ListBox As ListBox) As Boolean
Dim k As Integer
Dim DisplayText As String
Dim RetrieveCode As PracticeCodes
Set RetrieveCode = New PracticeCodes
ListBox.Clear
If (ViewAll) Then
    ListBox.AddItem "All Status"
End If
RetrieveCode.ReferenceType = Trim(CodeType)
RetrieveCode.ReferenceCode = ""
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        DisplayText = Space(40)
        If (SetIt) Then
            Mid(DisplayText, 1, Len(RetrieveCode.ReferenceCode) - 3) = Left(RetrieveCode.ReferenceCode, Len(RetrieveCode.ReferenceCode) - 3)
            Mid(DisplayText, 40, 1) = Mid(RetrieveCode.ReferenceCode, Len(RetrieveCode.ReferenceCode), 1)
        Else
            DisplayText = Trim(RetrieveCode.ReferenceCode)
        End If
        ListBox.AddItem DisplayText
        k = k + 1
    Loop
End If
Set RetrieveCode = Nothing
End Function

Public Function ApplRetrieveReviewSurgeryList() As Long
Dim i As Integer
Dim j As Integer
Dim w As Integer
Dim z As Integer
Dim PostIt As Boolean
Dim PatId As Long
Dim AptId As Long
Dim ARef1 As String
Dim ARem As String
Dim ALoc As String
Dim ADate As String
Dim Temp As String, CurTest As String
Dim OtherDisp As String
Dim ITemp As String
Dim MyTest As String
Dim PatName As String
Dim DisplayItem As String
Dim LocName As String
Dim SurgeryDate As String
Dim RetSCln As PatientClinicalSurgeryPlan
Dim RetApp As SchedulerAppointment
ApplRetrieveReviewSurgeryList = -1
MyTest = ApplTransactionRef
ApplRetrieveReviewSurgeryList = GetReviewSurgeryList
If (ApplRetrieveReviewSurgeryList < 1) Then
    Exit Function
End If
lstDocs.Clear
AptId = 0
PatId = 0
CurTest = ""
OtherDisp = ""
i = 1
While (SelectList(i))
    PostIt = True
    If (Trim(MyTest) <> "") Then
        PostIt = False
        If (InStrPS(UCase(ApplFinding), UCase(Trim(MyTest))) > 0) Then
            PostIt = True
        End If
    End If
    If ((Trim(TheStatus) = "") And (Trim(ApplSurgery) = "C")) Then
        PostIt = False
    ElseIf (PostIt) Then
        GoSub VerifyPost
    End If
    If (PostIt) Then
        ADate = ""
        ALoc = ""
        ARem = ""
        DisplayItem = Space(95)
        Mid(DisplayItem, 1, 1) = ApplSurgery
        Mid(DisplayItem, 3, 10) = Mid(ApplApptDate, 5, 2) + "/" + Mid(ApplApptDate, 7, 2) + "/" + Left(ApplApptDate, 4)
        PatName = Trim(ApplLastName) + ", " + Trim(ApplFirstName)
        Mid(DisplayItem, 14, Len(PatName)) = PatName
        ARef1 = Trim(ApplFinding)
        w = InStrPS(ARef1, "-3/")
        If (w > 0) Then
            ARef1 = Mid(ARef1, w + 3, Len(ARef1) - (w + 2))
        End If
        w = InStrPS(ARef1, "-F/")
        If (w > 0) Then
            ARem = Mid(ARef1, 1, w - 1)
        End If
        w = InStrPS(ARef1, "-1/")
        If (w > 0) Then
            j = InStrPS(ARef1, "-7/")
            If (j > 0) Then
                ADate = Mid(ARef1, w + 3, (j - 1) - (w + 2))
            Else
                ADate = Mid(ARef1, w + 3, Len(ARef1) - (w + 2))
            End If
        End If
        Call ReplaceCharacters(ADate, "- ", "~")
        Call StripCharacters(ADate, "~")
        w = InStrPS(ARef1, "-7/")
        If (w > 0) Then
            ALoc = Mid(ARef1, w + 3, Len(ARef1) - (w + 2))
        End If
        Mid(DisplayItem, 33, 22) = Trim(ARem)
        Mid(DisplayItem, 57, 10) = Left(ApplResource, 10)
        If (SurgeryDate <> "") Then
            Mid(DisplayItem, 67, 10) = SurgeryDate
        Else
            Mid(DisplayItem, 67, 10) = ADate
        End If
        If (LocName <> "") Then
            Mid(DisplayItem, 79, 15) = LocName
        Else
            Mid(DisplayItem, 79, 15) = ALoc
        End If
        DisplayItem = DisplayItem + Trim(Str(ApplApptId)) + "/" + Trim(Str(ApplPatientId)) + "*" + Trim(Str(ApplClinicalId))
        lstDocs.AddItem DisplayItem
    End If
    i = i + 1
Wend
Exit Function
VerifyPost:
    LocName = ""
    SurgeryDate = ""
    If (ApplClinicalId > 0) Then
        Set RetSCln = New PatientClinicalSurgeryPlan
        RetSCln.SurgeryClnId = ApplClinicalId
        RetSCln.SurgeryRefType = "F"
        RetSCln.SurgeryStatus = "A"
        If (RetSCln.FindPatientClinicalPlan > 0) Then
            If (RetSCln.SelectPatientClinicalPlan(1)) Then
                Set RetApp = New SchedulerAppointment
                RetApp.AppointmentId = Val(RetSCln.SurgeryValue)
                If (RetApp.RetrieveSchedulerAppointment) Then
                    SurgeryDate = Mid(RetApp.AppointmentDate, 5, 2) + "/" + Mid(RetApp.AppointmentDate, 7, 2) + "/" + Left(RetApp.AppointmentDate, 4)
                    Call GetLocation(RetApp.AppointmentResourceId2, LocName)
                End If
' Filter by Surgery Date
                If (Trim(ApplStartDate) <> "") And (Trim(ApplEndDate) <> "") And _
                   (Trim(ApplStartDate) <> Trim(ApplEndDate)) Then
                    If (ApplStartDate < RetApp.AppointmentDate) Or (ApplEndDate > RetApp.AppointmentDate) Then
                        PostIt = False
                    End If
                End If
' Filter by Location if necessary
                If (ResourceLocId > 0) Then
                    If (ResourceLocId <> RetApp.AppointmentResourceId2) Then
                        PostIt = False
                    End If
                End If
                Set RetApp = Nothing
            End If
        End If
        Set RetSCln = Nothing
    End If
    Return
End Function

Private Function GetReviewSurgeryList() As Long
On Error GoTo DbErrorHandler
Dim Ref As String
Dim Temp As String
Dim PTemp(6) As String
ApplListTotal = 0
PTemp(1) = "0"
PTemp(2) = "0"
PTemp(3) = "-1"
PTemp(4) = "-1"
PTemp(5) = "0"
GetReviewSurgeryList = -1
Set ApplListTbl = Nothing
Set ApplListTbl = New ADODB.Recordset
'If (Trim(ApplStartDate) <> "") Then
'    PTemp(1) = Trim(Str(ApplStartDate))
'End If
'If (Trim(ApplEndDate) <> "") Then
'    PTemp(2) = Trim(Str(ApplEndDate))
'End If
'If (ResourceLocId >= 0) Then
'    PTemp(3) = Trim(Str(ResourceLocId))
'End If
If (ResourceId > 0) Then
    PTemp(4) = Trim(Str(ResourceId))
End If
If (Trim(ApplConfirmStatus) <> "") Then
    PTemp(5) = Trim(ApplConfirmStatus)
End If
MyPracticeRepositoryCmd.CommandType = adCmdStoredProc
MyPracticeRepositoryCmd.CommandText = "usp_SurgeryTransactions"
Set MyParameters = Nothing
Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@SDate")
MyParameters.Type = adVarChar
MyParameters.Direction = adParamInput
MyParameters.Size = Len(Trim(PTemp(1)))
MyParameters.Value = Trim(PTemp(1))
MyPracticeRepositoryCmd.Parameters(1) = MyParameters
    
Set MyParameters = Nothing
Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@EDate")
MyParameters.Type = adVarChar
MyParameters.Direction = adParamInput
MyParameters.Size = Len(Trim(PTemp(2)))
If (Trim(PTemp(2)) = "") Then
    PTemp(2) = "-1"
End If
MyParameters.Value = Trim(PTemp(2))
MyPracticeRepositoryCmd.Parameters(2) = MyParameters
    
Set MyParameters = Nothing
Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@LocId")
MyParameters.Type = adInteger
MyParameters.Direction = adParamInput
MyParameters.Size = 4
If (Trim(PTemp(3)) = "") Then
    PTemp(3) = "-1"
End If
MyParameters.Value = Int(Val(Trim(PTemp(3))))
MyPracticeRepositoryCmd.Parameters(3) = MyParameters
    
Set MyParameters = Nothing
Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@ResId")
MyParameters.Type = adInteger
MyParameters.Direction = adParamInput
MyParameters.Size = 4
If (Trim(PTemp(4)) = "") Then
    PTemp(4) = "-1"
End If
MyParameters.Value = Int(Val(Trim(PTemp(4))))
MyPracticeRepositoryCmd.Parameters(4) = MyParameters

Set MyParameters = Nothing
Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@StaId")
MyParameters.Type = adVarChar
MyParameters.Direction = adParamInput
MyParameters.Size = 1
If (Trim(PTemp(5)) = "") Then
    PTemp(5) = "0"
End If
MyParameters.Value = Trim(PTemp(5))
MyPracticeRepositoryCmd.Parameters(5) = MyParameters
Call ApplListTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, adCmdStoredProc)
If Not (ApplListTbl.EOF) Then
    ApplListTbl.MoveLast
    ApplListTotal = ApplListTbl.RecordCount
    GetReviewSurgeryList = ApplListTotal
End If
Exit Function
DbErrorHandler:
    Call PinpointError("GetReviewSurgeryList", Err.Source + " " + Err.Description)
    Resume LeaveFast
LeaveFast:
End Function

Private Sub LoadApplList()
If (ApplListTbl("AppointmentId") <> "") Then
    ApplApptId = ApplListTbl("AppointmentId")
Else
    ApplApptId = 0
End If
If (ApplListTbl("PatientId") <> "") Then
    ApplPatientId = ApplListTbl("PatientId")
Else
    ApplPatientId = 0
End If
If (ApplListTbl("ClinicalId") <> "") Then
    ApplClinicalId = ApplListTbl("ClinicalId")
Else
    ApplClinicalId = 0
End If
If (ApplListTbl("LastName") <> "") Then
    ApplLastName = ApplListTbl("LastName")
Else
    ApplLastName = ""
End If
If (ApplListTbl("FirstName") <> "") Then
    ApplFirstName = ApplListTbl("FirstName")
Else
    ApplFirstName = ""
End If
If (ApplListTbl("MiddleInitial") <> "") Then
    ApplMiddleName = ApplListTbl("MiddleInitial")
Else
    ApplMiddleName = ""
End If
If (ApplListTbl("HomePhone") <> "") Then
    ApplHPhone = ApplListTbl("HomePhone")
Else
    ApplHPhone = ""
End If
If (ApplListTbl("Symptom") <> "") Then
    ApplSymptom = ApplListTbl("Symptom")
Else
    ApplSymptom = ""
End If
If (ApplListTbl("FindingDetail") <> "") Then
    ApplFinding = ApplListTbl("FindingDetail")
Else
    ApplFinding = ""
End If
If (ApplListTbl("ImageDescriptor") <> "") Then
    ApplDetail1 = ApplListTbl("ImageDescriptor")
Else
    ApplDetail1 = ""
End If
If (ApplListTbl("ImageInstructions") <> "") Then
    ApplDetail2 = ApplListTbl("ImageInstructions")
Else
    ApplDetail2 = ""
End If
If (ApplListTbl("ClinicalType") <> "") Then
    ApplClnType = ApplListTbl("ClinicalType")
Else
    ApplClnType = ""
End If
If (ApplListTbl("Surgery") <> "") Then
    ApplSurgery = ApplListTbl("Surgery")
Else
    ApplSurgery = ""
End If
If (ApplListTbl("Status") <> "") Then
    ApplClnStatus = ApplListTbl("Status")
Else
    ApplClnStatus = ""
End If
If (ApplListTbl("AppDate") <> "") Then
    ApplApptDate = ApplListTbl("AppDate")
Else
    ApplApptDate = ""
End If
If (ApplListTbl("ResourceId2") <> "") Then
    ApplLocId = ApplListTbl("ResourceId2")
Else
    ApplLocId = 0
End If
If (ApplListTbl("ResourceName") <> "") Then
    ApplResource = ApplListTbl("ResourceName")
Else
    ApplResource = ""
End If
End Sub

Private Function SelectList(ListItem As Integer) As Boolean
On Error GoTo DbErrorHandler
SelectList = False
If (ApplListTbl.EOF) Or (ListItem < 1) Or (ListItem > ApplListTotal) Then
    Exit Function
End If
ApplListTbl.MoveFirst
ApplListTbl.Move ListItem - 1
Call LoadApplList
SelectList = True
Exit Function
DbErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Sub SelectSystems(ActId As Long)
If (ActId > 0) Then
    frmSystems1.TheStationId = 999
    frmSystems1.ActivityId = ActId
    frmSystems1.ResourceName = ResourceName
    frmSystems1.EditPreviousOn = True
    lblLoad = "Loading Clinical Records"
    lblLoad.Visible = True
    frmFollowUp.Enabled = False
    DoEvents
    If (frmSystems1.LoadSummaryInfo(True)) Then
        frmSystems1.FormOn = True
        frmSystems1.Show
        While (frmSystems1.FormOn)
            DoEvents
        Wend
        frmSurgFollow.ZOrder 0
    End If
    frmSurgFollow.Enabled = True
    lblLoad.Visible = False
End If
End Sub

Private Function ApplGetActivityId(ApptId As Long, CheckedOut As Boolean) As Long
Dim PatId As Long
Dim DFile As String
Dim RetAct As PracticeActivity
ApplGetActivityId = 0
CheckedOut = True
If (ApptId > 0) Then
    Set RetAct = New PracticeActivity
    RetAct.ActivityAppointmentId = ApptId
    RetAct.ActivityLocationId = -1
    RetAct.ActivityStatus = ""
    If (RetAct.FindActivity > 0) Then
        If (RetAct.SelectActivity(1)) Then
            PatId = RetAct.ActivityPatientId
            If (RetAct.ActivityStatus = "H") Or (RetAct.ActivityStatus = "D") Then
                DFile = Dir(DoctorInterfaceDirectory + "*" + "_" + Trim(Str(ApptId)) + "_" + Trim(Str(PatId)) + ".txt")
                If (DFile <> "") Then
                    ApplGetActivityId = 0
                Else
                    ApplGetActivityId = RetAct.ActivityId
                End If
                If (RetAct.ActivityStatus = "H") Then
                    CheckedOut = False
                End If
            End If
        End If
    End If
    Set RetAct = Nothing
End If
End Function

Private Function ApplSurgeryFollowUpTrigger(ClnId As Long, ActId As Long, ACheck As Boolean) As Boolean
Dim u As Integer
Dim i As Integer, j As Integer
Dim ApptId As Long, PatId As Long
Dim AEye As String
Dim ARef As String, ALoc As String
Dim AName As String, ADate As String
Dim RetCln As PatientClinical
ApplSurgeryFollowUpTrigger = True
If (ClnId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        PatId = RetCln.PatientId
        ApptId = RetCln.AppointmentId
        If (RetCln.ClinicalType = "A") Then
            AEye = ""
            ALoc = ""
            ADate = ""
            AName = ""
            ARef = Trim(RetCln.Findings)
            i = InStrPS(ARef, "-1/")
            j = InStrPS(ARef, "-F/")
            If (i > 0) And (j > 0) And (j < i) Then
                ALoc = Mid(ARef, j + 3, (i - 1) - (j + 2))
            End If
            i = InStrPS(ARef, "-F/")
            j = InStrPS(ARef, "-3/")
            If (i > 0) And (j > 0) And (j < i) Then
                AName = Mid(ARef, j + 3, (i - 1) - (j + 2))
                i = InStrPS(AName, "(")
                If (i > 0) Then
                    AEye = Mid(AName, i + 1, 2)
                    AName = Trim(Left(AName, i - 1))
                End If
            End If
            j = InStrPS(ARef, "-1/")
            i = Len(ARef)
            If (i > 0) And (j > 0) And (j < i) Then
                ADate = Mid(ARef, j + 3, (i - 1) - (j + 2))
            End If
            frmSurgical.AppointmentId = ApptId
            frmSurgical.PatientId = PatId
            frmSurgical.ResourceName = ResourceName
            frmSurgical.ClinicalId = ClnId
            frmSurgical.ActivityId = ActId
            If (frmSurgical.LoadSurgical("", ALoc, ADate, AName, AEye)) Then
                frmSurgical.FormOn = True
                frmSurgical.Show
                frmSurgical.ZOrder 0
                While (frmSurgical.FormOn)
                    DoEvents
                Wend
                frmSurgFollow.ZOrder 0
                lstSta.Clear
                lstSta.AddItem "Close Surgery Status"
                For u = 1 To lstStatus.ListCount - 1
                    lstSta.AddItem lstStatus.List(u)
                Next u
                lstSta.Visible = True
                lstDr.Enabled = False
                lstLoc.Enabled = False
                lstDocs.Enabled = False
                lstTests.Enabled = False
            End If
        End If
    End If
    Set RetCln = Nothing
End If
End Function

Private Function GetLocation(LocId As Long, LocName As String) As Boolean
Dim RetPrc As PracticeName
Dim RetResource As SchedulerResource
GetLocation = False
If (LocId > 0) Then
    If (LocId < 1000) Then
        Set RetResource = New SchedulerResource
        RetResource.ResourceId = LocId
        If (RetResource.RetrieveSchedulerResource) Then
            LocName = RetResource.ResourceDescription
        End If
        Set RetResource = Nothing
    ElseIf (LocId > 1000) Then
        Set RetPrc = New PracticeName
        RetPrc.PracticeId = LocId
        If (RetPrc.RetrievePracticeName) Then
            LocName = RetPrc.PracticeLocationReference
        End If
        Set RetPrc = Nothing
    End If
Else
    LocName = "Office"
End If
GetLocation = True
End Function

