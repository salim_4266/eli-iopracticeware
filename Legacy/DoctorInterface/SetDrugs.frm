VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmSetDrugs 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdRx8 
      Height          =   1095
      Left            =   3840
      TabIndex        =   0
      Top             =   2880
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx6 
      Height          =   1095
      Left            =   3840
      TabIndex        =   1
      Top             =   480
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":01DE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx1 
      Height          =   1095
      Left            =   360
      TabIndex        =   2
      Top             =   480
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":03BC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx2 
      Height          =   1095
      Left            =   360
      TabIndex        =   3
      Top             =   1680
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":059A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx3 
      Height          =   1095
      Left            =   360
      TabIndex        =   4
      Top             =   2880
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":0778
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx4 
      Height          =   1095
      Left            =   360
      TabIndex        =   5
      Top             =   4080
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":0956
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx11 
      Height          =   1095
      Left            =   7320
      TabIndex        =   6
      Top             =   480
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":0B34
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx12 
      Height          =   1095
      Left            =   7320
      TabIndex        =   7
      Top             =   1680
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":0D13
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx13 
      Height          =   1095
      Left            =   7320
      TabIndex        =   8
      Top             =   2880
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":0EF2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx14 
      Height          =   1095
      Left            =   7320
      TabIndex        =   9
      Top             =   4080
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":10D1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx15 
      Height          =   1095
      Left            =   7320
      TabIndex        =   10
      Top             =   5280
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":12B0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx9 
      Height          =   1095
      Left            =   3840
      TabIndex        =   11
      Top             =   4080
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":148F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx10 
      Height          =   1095
      Left            =   3840
      TabIndex        =   12
      Top             =   5280
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":166D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   9600
      TabIndex        =   13
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":184C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreRx 
      Height          =   1095
      Left            =   360
      TabIndex        =   14
      Top             =   6480
      Width           =   10935
      _Version        =   131072
      _ExtentX        =   19288
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":1A7F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx5 
      Height          =   1095
      Left            =   360
      TabIndex        =   15
      Top             =   5280
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":1C61
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx7 
      Height          =   1095
      Left            =   3840
      TabIndex        =   16
      Top             =   1680
      Visible         =   0   'False
      Width           =   3375
      _Version        =   131072
      _ExtentX        =   5953
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetDrugs.frx":1E3F
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      Caption         =   "Assign An Rx"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0FFFF&
      Height          =   330
      Left            =   360
      TabIndex        =   17
      Top             =   120
      Width           =   1860
   End
End
Attribute VB_Name = "frmSetDrugs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Const MaxSetRx As Integer = 100
Private Type ExternalReference
    Id As String
    Diag As String
    Name As String
    Dosage As String
    Frequency As String
    Refills As String
    Ref As Long
End Type
Private AvailableRx(MaxSetRx) As ExternalReference
Private SelectedRx(MaxSetRx) As ExternalReference

Private MaxRx As Integer
Private CurrentIndex As Integer
Private CurrentRxIndex As Integer
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub ClearRxs()
cmdRx1.Visible = False
cmdRx1.BackColor = BaseBackColor
cmdRx1.ForeColor = BaseTextColor
cmdRx2.Visible = False
cmdRx2.BackColor = BaseBackColor
cmdRx2.ForeColor = BaseTextColor
cmdRx3.Visible = False
cmdRx3.BackColor = BaseBackColor
cmdRx3.ForeColor = BaseTextColor
cmdRx4.Visible = False
cmdRx4.BackColor = BaseBackColor
cmdRx4.ForeColor = BaseTextColor
cmdRx5.Visible = False
cmdRx5.BackColor = BaseBackColor
cmdRx5.ForeColor = BaseTextColor
cmdRx6.Visible = False
cmdRx6.BackColor = BaseBackColor
cmdRx6.ForeColor = BaseTextColor
cmdRx7.Visible = False
cmdRx7.BackColor = BaseBackColor
cmdRx7.ForeColor = BaseTextColor
cmdRx8.Visible = False
cmdRx8.BackColor = BaseBackColor
cmdRx8.ForeColor = BaseTextColor
cmdRx9.Visible = False
cmdRx9.BackColor = BaseBackColor
cmdRx9.ForeColor = BaseTextColor
cmdRx10.Visible = False
cmdRx10.BackColor = BaseBackColor
cmdRx10.ForeColor = BaseTextColor
cmdRx11.Visible = False
cmdRx11.BackColor = BaseBackColor
cmdRx11.ForeColor = BaseTextColor
cmdRx12.Visible = False
cmdRx12.BackColor = BaseBackColor
cmdRx12.ForeColor = BaseTextColor
cmdRx13.Visible = False
cmdRx13.BackColor = BaseBackColor
cmdRx13.ForeColor = BaseTextColor
cmdRx14.Visible = False
cmdRx14.BackColor = BaseBackColor
cmdRx14.ForeColor = BaseTextColor
cmdRx15.Visible = False
cmdRx15.BackColor = BaseBackColor
cmdRx15.ForeColor = BaseTextColor
End Sub

Private Sub cmdRx1_Click()
Call TriggerRx(cmdRx1)
End Sub

Private Sub cmdRx2_Click()
Call TriggerRx(cmdRx2)
End Sub

Private Sub cmdRx3_Click()
Call TriggerRx(cmdRx3)
End Sub

Private Sub cmdRx4_Click()
Call TriggerRx(cmdRx4)
End Sub

Private Sub cmdRx5_Click()
Call TriggerRx(cmdRx5)
End Sub

Private Sub cmdRx6_Click()
Call TriggerRx(cmdRx6)
End Sub

Private Sub cmdRx7_Click()
Call TriggerRx(cmdRx7)
End Sub

Private Sub cmdRx8_Click()
Call TriggerRx(cmdRx8)
End Sub

Private Sub cmdRx9_Click()
Call TriggerRx(cmdRx9)
End Sub

Private Sub cmdRx10_Click()
Call TriggerRx(cmdRx10)
End Sub

Private Sub cmdRx11_Click()
Call TriggerRx(cmdRx11)
End Sub

Private Sub cmdRx12_Click()
Call TriggerRx(cmdRx12)
End Sub

Private Sub cmdRx13_Click()
Call TriggerRx(cmdRx13)
End Sub

Private Sub cmdRx14_Click()
Call TriggerRx(cmdRx14)
End Sub

Private Sub cmdRx15_Click()
Call TriggerRx(cmdRx15)
End Sub

Private Sub cmdMoreRx_Click()
CurrentIndex = CurrentIndex + 1
If (CurrentIndex > MaxRx) Then
    CurrentIndex = 1
End If
Call LoadRx
End Sub

Private Sub cmdDone_Click()
Unload frmSetDrugs
End Sub

Public Function LoadAvailableRx(Init As Boolean) As Boolean
Dim p As Integer
Dim j As Integer
Dim i As Integer
Dim Id As String
Dim Diag As String
Dim TotalDiags As Integer
Dim RetrieveDrug As DiagnosisMasterDrugs
Set RetrieveDrug = New DiagnosisMasterDrugs
LoadAvailableRx = False
If (Init) Then
    MaxRx = 0
    CurrentIndex = 1
    CurrentRxIndex = 1
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = cmdRx1.BackColor
    BaseTextColor = cmdRx1.ForeColor
    For i = 1 To MaxSetRx
        AvailableRx(i).Id = ""
        AvailableRx(i).Diag = ""
        AvailableRx(i).Name = ""
        AvailableRx(i).Dosage = ""
        AvailableRx(i).Frequency = ""
        AvailableRx(i).Refills = ""
        AvailableRx(i).Ref = 0
        SelectedRx(i).Id = ""
        SelectedRx(i).Diag = ""
        SelectedRx(i).Name = ""
        SelectedRx(i).Dosage = ""
        SelectedRx(i).Frequency = ""
        SelectedRx(i).Refills = ""
        SelectedRx(i).Ref = 0
    Next i
End If
p = 1
j = 1
TotalDiags = frmSetDiagnosis.TotalBilledDiagnosis
For i = 1 To TotalDiags
    If (frmSetDiagnosis.GetBilledDiagnosis(i, Id, Diag)) Then
        RetrieveDrug.DrugDiagnosis = Id
        If (RetrieveDrug.FindPrimaryDrugs > 0) Then
            While (RetrieveDrug.SelectDrug(j)) And (p < MaxSetRx)
                If Not (IsDisplayDrugDuplicate(RetrieveDrug.DrugId)) Then
                    AvailableRx(p).Diag = Trim(RetrieveDrug.DrugDiagnosis)
                    AvailableRx(p).Name = Trim(RetrieveDrug.DrugManufacturedName)
                    AvailableRx(p).Ref = RetrieveDrug.DrugId
                    p = p + 1
                End If
                j = j + 1
            Wend
        End If
    End If
Next i
MaxRx = p - 1
If (MaxRx > 0) Then
    CurrentIndex = 1
    LoadAvailableRx = LoadRx
End If
Set RetrieveDrug = Nothing
End Function

Private Function LoadRx() As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
Dim TheText As String
LoadRx = False
Call ClearRxs
i = CurrentIndex
ButtonCnt = 1
While (i <= MaxRx) And (ButtonCnt < 16)
    DoEvents
    Call SetButton(ButtonCnt, i)
    ButtonCnt = ButtonCnt + 1
    i = i + 1
Wend
CurrentIndex = i
LoadRx = True
If (CurrentIndex < MaxRx) Then
    cmdMoreRx.Text = "More Rx"
    cmdMoreRx.Visible = True
Else
    If (MaxRx > 15) Then
        cmdMoreRx.Text = "Beginning of Rx List"
        cmdMoreRx.Visible = True
    Else
        cmdMoreRx.Visible = False
    End If
End If
End Function

Private Sub TriggerRx(AButton As fpBtn)
Dim j As Integer
Dim i As Integer
Dim ClearRx As Integer
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
' Check if present already
    ClearRx = 0
    For i = 1 To CurrentRxIndex
        If (Trim(AButton.Tag) = Trim(Str(SelectedRx(i).Ref))) Then
            ClearRx = i
            Exit For
        End If
    Next i
    If (ClearRx = 0) And (CurrentRxIndex < MaxSetRx) Then
        SelectedRx(CurrentRxIndex).Id = ""
        SelectedRx(CurrentRxIndex).Name = Trim(AButton.Text)
        SelectedRx(CurrentRxIndex).Diag = Trim(AButton.ToolTipText)
        SelectedRx(CurrentRxIndex).Ref = Trim(Str(AButton.Tag))
'Dosage
        frmNumericPad.NumPad_Result = ""
        frmNumericPad.NumPad_Field = "Dosage For " + Trim(SelectedRx(CurrentRxIndex).Name)
        frmNumericPad.NumPad_Max = 0
        frmNumericPad.NumPad_Min = 0
        frmNumericPad.NumPad_TimeFrameOn = False
        frmNumericPad.NumPad_TimeFrame = ""
        frmNumericPad.NumPad_DisplayFull = True
        frmNumericPad.NumPad_ChoiceOn1 = False
        frmNumericPad.NumPad_Choice1 = "mg"
        frmNumericPad.NumPad_ChoiceOn2 = False
        frmNumericPad.NumPad_Choice2 = "g"
        frmNumericPad.NumPad_ChoiceOn3 = False
        frmNumericPad.NumPad_Choice3 = "ml"
        frmNumericPad.NumPad_ChoiceOn4 = False
        frmNumericPad.NumPad_Choice4 = "IV"
        frmNumericPad.NumPad_ChoiceOn5 = False
        frmNumericPad.NumPad_Choice5 = "mcg"
        frmNumericPad.NumPad_ChoiceOn6 = False
        frmNumericPad.NumPad_Choice6 = "cc"
        frmNumericPad.NumPad_ChoiceOn7 = False
        frmNumericPad.NumPad_Choice7 = "T gtt"
        frmNumericPad.NumPad_ChoiceOn8 = False
        frmNumericPad.NumPad_Choice8 = "T tblt"
        frmNumericPad.NumPad_ChoiceOn9 = False
        frmNumericPad.NumPad_Choice9 = ""
        frmNumericPad.Show 1
        SelectedRx(CurrentRxIndex).Dosage = Trim(frmNumericPad.NumPad_Result)
        If (frmNumericPad.NumPad_ChoiceOn1) Then
            SelectedRx(CurrentRxIndex).Dosage = SelectedRx(CurrentRxIndex).Dosage + " (" + frmNumericPad.NumPad_Choice1 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn2) Then
            SelectedRx(CurrentRxIndex).Dosage = SelectedRx(CurrentRxIndex).Dosage + " (" + frmNumericPad.NumPad_Choice2 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn3) Then
            SelectedRx(CurrentRxIndex).Dosage = SelectedRx(CurrentRxIndex).Dosage + " (" + frmNumericPad.NumPad_Choice3 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn4) Then
            SelectedRx(CurrentRxIndex).Dosage = SelectedRx(CurrentRxIndex).Dosage + " (" + frmNumericPad.NumPad_Choice4 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn5) Then
            SelectedRx(CurrentRxIndex).Dosage = SelectedRx(CurrentRxIndex).Dosage + " (" + frmNumericPad.NumPad_Choice5 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn6) Then
            SelectedRx(CurrentRxIndex).Dosage = SelectedRx(CurrentRxIndex).Dosage + " (" + frmNumericPad.NumPad_Choice6 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn7) Then
            SelectedRx(CurrentRxIndex).Dosage = SelectedRx(CurrentRxIndex).Dosage + " (" + frmNumericPad.NumPad_Choice7 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn8) Then
            SelectedRx(CurrentRxIndex).Dosage = SelectedRx(CurrentRxIndex).Dosage + " (" + frmNumericPad.NumPad_Choice8 + ") "
        End If
'Frequency
        frmNumericPad.NumPad_Result = ""
        frmNumericPad.NumPad_Field = "Frequency per day for " + Trim(SelectedRx(CurrentRxIndex).Name)
        frmNumericPad.NumPad_Max = 0
        frmNumericPad.NumPad_Min = 0
        frmNumericPad.NumPad_TimeFrameOn = False
        frmNumericPad.NumPad_TimeFrame = ""
        frmNumericPad.NumPad_DisplayFull = True
        frmNumericPad.NumPad_ChoiceOn1 = False
        frmNumericPad.NumPad_Choice1 = "QD"
        frmNumericPad.NumPad_ChoiceOn2 = False
        frmNumericPad.NumPad_Choice2 = "BID"
        frmNumericPad.NumPad_ChoiceOn3 = False
        frmNumericPad.NumPad_Choice3 = "TID"
        frmNumericPad.NumPad_ChoiceOn4 = False
        frmNumericPad.NumPad_Choice4 = "QID"
        frmNumericPad.NumPad_ChoiceOn5 = False
        frmNumericPad.NumPad_Choice5 = "Qam"
        frmNumericPad.NumPad_ChoiceOn6 = False
        frmNumericPad.NumPad_Choice6 = "Qpm"
        frmNumericPad.NumPad_ChoiceOn7 = False
        frmNumericPad.NumPad_Choice7 = "PO"
        frmNumericPad.NumPad_ChoiceOn8 = False
        frmNumericPad.NumPad_Choice8 = ""
        frmNumericPad.NumPad_ChoiceOn9 = False
        frmNumericPad.NumPad_Choice9 = ""
        frmNumericPad.Show 1
        SelectedRx(CurrentRxIndex).Frequency = Trim(frmNumericPad.NumPad_Result)
        If (frmNumericPad.NumPad_ChoiceOn1) Then
            SelectedRx(CurrentRxIndex).Frequency = SelectedRx(CurrentRxIndex).Frequency + " (" + frmNumericPad.NumPad_Choice1 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn2) Then
            SelectedRx(CurrentRxIndex).Frequency = SelectedRx(CurrentRxIndex).Frequency + " (" + frmNumericPad.NumPad_Choice2 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn3) Then
            SelectedRx(CurrentRxIndex).Frequency = SelectedRx(CurrentRxIndex).Frequency + " (" + frmNumericPad.NumPad_Choice3 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn4) Then
            SelectedRx(CurrentRxIndex).Frequency = SelectedRx(CurrentRxIndex).Frequency + " (" + frmNumericPad.NumPad_Choice4 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn5) Then
            SelectedRx(CurrentRxIndex).Frequency = SelectedRx(CurrentRxIndex).Frequency + " (" + frmNumericPad.NumPad_Choice5 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn6) Then
            SelectedRx(CurrentRxIndex).Frequency = SelectedRx(CurrentRxIndex).Frequency + " (" + frmNumericPad.NumPad_Choice6 + ") "
        End If
        If (frmNumericPad.NumPad_ChoiceOn7) Then
            SelectedRx(CurrentRxIndex).Frequency = SelectedRx(CurrentRxIndex).Frequency + " (" + frmNumericPad.NumPad_Choice7 + ") "
        End If
        CurrentRxIndex = CurrentRxIndex + 1
'Refills
        frmNumericPad.NumPad_Result = ""
        frmNumericPad.NumPad_Field = "Refills For " + Trim(SelectedRx(CurrentRxIndex).Name)
        frmNumericPad.NumPad_Max = 0
        frmNumericPad.NumPad_Min = 0
        frmNumericPad.NumPad_TimeFrameOn = False
        frmNumericPad.NumPad_TimeFrame = ""
        frmNumericPad.NumPad_DisplayFull = True
        frmNumericPad.NumPad_ChoiceOn1 = False
        frmNumericPad.NumPad_Choice1 = "daw"
        frmNumericPad.NumPad_ChoiceOn2 = False
        frmNumericPad.NumPad_Choice2 = ""
        frmNumericPad.NumPad_ChoiceOn3 = False
        frmNumericPad.NumPad_Choice3 = ""
        frmNumericPad.NumPad_ChoiceOn4 = False
        frmNumericPad.NumPad_Choice4 = ""
        frmNumericPad.NumPad_ChoiceOn5 = False
        frmNumericPad.NumPad_Choice5 = ""
        frmNumericPad.NumPad_ChoiceOn6 = False
        frmNumericPad.NumPad_Choice6 = ""
        frmNumericPad.NumPad_ChoiceOn7 = False
        frmNumericPad.NumPad_Choice7 = ""
        frmNumericPad.NumPad_ChoiceOn8 = False
        frmNumericPad.NumPad_Choice8 = ""
        frmNumericPad.NumPad_ChoiceOn9 = False
        frmNumericPad.NumPad_Choice9 = ""
        frmNumericPad.Show 1
        SelectedRx(CurrentRxIndex).Refills = Trim(frmNumericPad.NumPad_Result)
        If (frmNumericPad.NumPad_ChoiceOn1) Then
            SelectedRx(CurrentRxIndex).Refills = SelectedRx(CurrentRxIndex).Refills + " (" + frmNumericPad.NumPad_Choice1 + ") "
        End If
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    For i = 1 To CurrentRxIndex
        If (Trim(AButton.Tag) = Trim(Str(SelectedRx(i).Ref))) Then
            SelectedRx(i).Id = ""
            SelectedRx(i).Diag = ""
            SelectedRx(i).Name = ""
            SelectedRx(i).Dosage = ""
            SelectedRx(i).Frequency = ""
            SelectedRx(i).Refills = ""
            SelectedRx(i).Ref = 0
            ClearRx = i
            Exit For
        End If
    Next i
    For i = ClearRx To CurrentRxIndex
        SelectedRx(i).Id = SelectedRx(i + 1).Id
        SelectedRx(i).Diag = SelectedRx(i + 1).Diag
        SelectedRx(i).Name = SelectedRx(i + 1).Name
        SelectedRx(i).Dosage = SelectedRx(i + 1).Dosage
        SelectedRx(i).Frequency = SelectedRx(i + 1).Frequency
        SelectedRx(i).Refills = SelectedRx(i + 1).Refills
        SelectedRx(i).Ref = SelectedRx(i + 1).Ref
    Next i
    CurrentRxIndex = CurrentRxIndex - 1
    If (CurrentRxIndex < 1) Then
        CurrentRxIndex = 1
    End If
End If
End Sub

Private Sub SetButton(Ref As Integer, ProcId As Integer)
Dim TheId As String
Dim TheDisplayName As String
Dim TheTag As String
TheId = AvailableRx(ProcId).Diag
TheDisplayName = AvailableRx(ProcId).Name
TheTag = Trim(Str(AvailableRx(ProcId).Ref))
If (Ref = 1) Then
    cmdRx1.Text = TheDisplayName
    cmdRx1.Tag = TheTag
    cmdRx1.ToolTipText = TheId
    cmdRx1.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx1.BackColor = ButtonSelectionColor
        cmdRx1.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 2) Then
    cmdRx2.Text = TheDisplayName
    cmdRx2.Tag = TheTag
    cmdRx2.ToolTipText = TheId
    cmdRx2.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx2.BackColor = ButtonSelectionColor
        cmdRx2.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 3) Then
    cmdRx3.Text = TheDisplayName
    cmdRx3.Tag = TheTag
    cmdRx3.ToolTipText = TheId
    cmdRx3.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx3.BackColor = ButtonSelectionColor
        cmdRx3.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 4) Then
    cmdRx4.Text = TheDisplayName
    cmdRx4.Tag = TheTag
    cmdRx4.ToolTipText = TheId
    cmdRx4.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx4.BackColor = ButtonSelectionColor
        cmdRx4.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 5) Then
    cmdRx5.Text = TheDisplayName
    cmdRx5.Tag = TheTag
    cmdRx5.ToolTipText = TheId
    cmdRx5.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx5.BackColor = ButtonSelectionColor
        cmdRx5.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 6) Then
    cmdRx6.Text = TheDisplayName
    cmdRx6.Tag = TheTag
    cmdRx6.ToolTipText = TheId
    cmdRx6.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx6.BackColor = ButtonSelectionColor
        cmdRx6.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 7) Then
    cmdRx7.Text = TheDisplayName
    cmdRx7.Tag = TheTag
    cmdRx7.ToolTipText = TheId
    cmdRx7.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx7.BackColor = ButtonSelectionColor
        cmdRx7.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 8) Then
    cmdRx8.Text = TheDisplayName
    cmdRx8.Tag = TheTag
    cmdRx8.ToolTipText = TheId
    cmdRx8.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx8.BackColor = ButtonSelectionColor
        cmdRx8.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 9) Then
    cmdRx9.Text = TheDisplayName
    cmdRx9.Tag = TheTag
    cmdRx9.ToolTipText = TheId
    cmdRx9.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx9.BackColor = ButtonSelectionColor
        cmdRx9.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 10) Then
    cmdRx10.Text = TheDisplayName
    cmdRx10.Tag = TheTag
    cmdRx10.ToolTipText = TheId
    cmdRx10.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx10.BackColor = ButtonSelectionColor
        cmdRx10.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 11) Then
    cmdRx11.Text = TheDisplayName
    cmdRx11.Tag = TheTag
    cmdRx11.ToolTipText = TheId
    cmdRx11.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx11.BackColor = ButtonSelectionColor
        cmdRx11.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 12) Then
    cmdRx12.Text = TheDisplayName
    cmdRx12.Tag = TheTag
    cmdRx12.ToolTipText = TheId
    cmdRx12.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx12.BackColor = ButtonSelectionColor
        cmdRx12.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 13) Then
    cmdRx13.Text = TheDisplayName
    cmdRx13.Tag = TheTag
    cmdRx13.ToolTipText = TheId
    cmdRx13.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx13.BackColor = ButtonSelectionColor
        cmdRx13.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 14) Then
    cmdRx14.Text = TheDisplayName
    cmdRx14.Tag = TheTag
    cmdRx14.ToolTipText = TheId
    cmdRx14.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx14.BackColor = ButtonSelectionColor
        cmdRx14.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 15) Then
    cmdRx15.Text = TheDisplayName
    cmdRx15.Tag = TheTag
    cmdRx15.ToolTipText = TheId
    cmdRx15.Visible = True
    If (IsRxSet(TheDisplayName)) Then
        cmdRx15.BackColor = ButtonSelectionColor
        cmdRx15.ForeColor = TextSelectionColor
    End If
End If
End Sub

Private Function IsDisplayDrugDuplicate(ADrug As Long) As Boolean
Dim i As Integer
IsDisplayDrugDuplicate = False
For i = 1 To MaxSetRx
    If (Trim(AvailableRx(i).Ref) = ADrug) Then
        IsDisplayDrugDuplicate = True
        Exit For
    End If
Next i
End Function

Public Function IsRxSet(TheRx As String) As Boolean
Dim i As Integer
IsRxSet = False
For i = 1 To TotalRx
    If (Trim(SelectedRx(i).Name) = TheRx) Then
        IsRxSet = True
        Exit For
    End If
Next i
End Function

Private Function SetRx(Ref As Integer, TheRx As String, TheDiag As String, TheDosage As String, TheFreq As String, TheRefills As String) As Boolean
Dim i As Integer
SetRx = False
TheRx = ""
If (Ref < 1) Then
    Exit Function
End If
If (Trim(SelectedRx(Ref).Name) <> "") Then
    TheRx = SelectedRx(Ref).Name
    TheDiag = SelectedRx(Ref).Diag
    TheDosage = SelectedRx(Ref).Dosage
    TheFreq = SelectedRx(Ref).Frequency
    TheRefills = SelectedRx(Ref).Refills
    SetRx = True
End If
End Function

Public Function InitializeRx() As Boolean
Dim i As Integer
InitializeRx = True
For i = 1 To MaxSetRx
    AvailableRx(i).Id = ""
    AvailableRx(i).Diag = ""
    AvailableRx(i).Name = ""
    AvailableRx(i).Ref = 0
    SelectedRx(i).Id = ""
    SelectedRx(i).Diag = ""
    SelectedRx(i).Name = ""
    SelectedRx(i).Ref = 0
Next i
End Function

Public Function TotalRx() As Integer
Dim i As Integer
TotalRx = 0
For i = 1 To MaxSetRx
    If (Trim(SelectedRx(i).Ref) = 0) Then
        TotalRx = i - 1
        Exit For
    End If
Next i
End Function

Public Function GetRx(RefId As Integer, ARx As String, ADiag As String, ADosage As String, AFrequency As String, ARefills As String) As Boolean
GetRx = SetRx(RefId, ARx, ADiag, ADosage, AFrequency, ARefills)
End Function

Private Function FindButton(Ref, fpButton As fpBtn) As Boolean
FindButton = False
If (cmdRx1.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx1
ElseIf (cmdRx2.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx2
ElseIf (cmdRx3.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx3
ElseIf (cmdRx4.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx4
ElseIf (cmdRx5.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx5
ElseIf (cmdRx6.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx6
ElseIf (cmdRx7.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx7
ElseIf (cmdRx8.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx8
ElseIf (cmdRx9.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx9
ElseIf (cmdRx10.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx10
ElseIf (cmdRx11.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx11
ElseIf (cmdRx12.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx12
ElseIf (cmdRx13.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx13
ElseIf (cmdRx14.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx14
ElseIf (cmdRx15.Tag = Trim(Str(SelectedRx(Ref).Ref))) Then
    FindButton = True
    Set fpButton = cmdRx15
End If
End Function

