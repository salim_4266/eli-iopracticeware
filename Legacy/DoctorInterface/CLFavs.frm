VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmCLFavs 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9570
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12570
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "CLFavs.frx":0000
   ScaleHeight     =   9570
   ScaleWidth      =   12570
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtCLFav 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2520
      TabIndex        =   26
      Top             =   240
      Width           =   6735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   990
      Index           =   1
      Left            =   9840
      TabIndex        =   25
      Top             =   7320
      Visible         =   0   'False
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   990
      Index           =   0
      Left            =   8760
      TabIndex        =   24
      Top             =   7800
      Visible         =   0   'False
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":38A2
   End
   Begin VB.ListBox lstBox 
      Height          =   450
      Left            =   3480
      TabIndex        =   22
      Top             =   480
      Visible         =   0   'False
      Width           =   735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch3 
      Height          =   1095
      Left            =   480
      TabIndex        =   1
      Top             =   3360
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":3A7D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch2 
      Height          =   1095
      Left            =   480
      TabIndex        =   2
      Top             =   2160
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":3C60
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch1 
      Height          =   1095
      Left            =   480
      TabIndex        =   3
      Top             =   960
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":3E3B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch7 
      Height          =   1095
      Left            =   4080
      TabIndex        =   4
      Top             =   2160
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":4018
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch8 
      Height          =   1095
      Left            =   4080
      TabIndex        =   5
      Top             =   3360
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":41F8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch9 
      Height          =   1095
      Left            =   4080
      TabIndex        =   6
      Top             =   4560
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":43D9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch6 
      Height          =   1095
      Left            =   4080
      TabIndex        =   7
      Top             =   960
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":45B8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch5 
      Height          =   1095
      Left            =   480
      TabIndex        =   8
      Top             =   5760
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":4799
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch4 
      Height          =   1095
      Left            =   480
      TabIndex        =   9
      Top             =   4560
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":4978
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch12 
      Height          =   1095
      Left            =   7680
      TabIndex        =   10
      Top             =   2160
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":4B53
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch13 
      Height          =   1095
      Left            =   7680
      TabIndex        =   11
      Top             =   3360
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":4D2E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch14 
      Height          =   1095
      Left            =   7680
      TabIndex        =   12
      Top             =   4560
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":4F0C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch11 
      Height          =   1095
      Left            =   7680
      TabIndex        =   13
      Top             =   960
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":50EF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch10 
      Height          =   1095
      Left            =   4080
      TabIndex        =   14
      Top             =   5760
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":52CE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   8760
      TabIndex        =   16
      Top             =   7320
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":54AB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   480
      TabIndex        =   17
      Top             =   7320
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":5686
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   975
      Left            =   2400
      TabIndex        =   18
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":5861
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   975
      Left            =   7080
      TabIndex        =   19
      Top             =   7320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":5A40
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch15 
      Height          =   1095
      Left            =   7680
      TabIndex        =   20
      Top             =   5760
      Width           =   3255
      _Version        =   131072
      _ExtentX        =   5741
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":5C1B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTint 
      Height          =   990
      Left            =   4080
      TabIndex        =   21
      Top             =   7320
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":5DF8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch 
      Height          =   990
      Left            =   5640
      TabIndex        =   23
      Top             =   7320
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLFavs.frx":5FD3
   End
   Begin VB.PictureBox SF 
      Height          =   255
      Left            =   8400
      ScaleHeight     =   195
      ScaleWidth      =   435
      TabIndex        =   0
      Top             =   360
      Width           =   495
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "CL Favorites"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   480
      TabIndex        =   15
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "frmCLFavs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public InventoryId As Long
Public InventoryDescription As String
Private CurrentSelectButtonIdx As Integer
Private TotalFavorites As Long

Private Sub cmd_Click(Index As Integer)
InventoryId = Val(txtCLFav.Tag)
InventoryDescription = Trim(txtCLFav.Text)
If (InventoryId < 1) Then
    InventoryId = 0
    'InventoryDescription = ""
End If
frmAssignWearNew.CommArea = Index & vbTab & InventoryId & vbTab & InventoryDescription & vbTab
Unload frmCLFavs

End Sub

Private Sub cmdDone_Click()
InventoryId = Val(txtCLFav.Tag)
InventoryDescription = Trim(txtCLFav.Text)
If (InventoryId < 1) Then
    InventoryId = 0
    InventoryDescription = ""
End If
Unload frmCLFavs
End Sub

Private Sub cmdHome_Click()
InventoryId = 0
InventoryDescription = ""
Unload frmCLFavs
End Sub

Private Sub cmdNext_Click()
Call LoadCLFavorites(False)
End Sub

Private Sub cmdPrev_Click()
CurrentSelectButtonIdx = CurrentSelectButtonIdx - 30
If (CurrentSelectButtonIdx < 1) Then
    CurrentSelectButtonIdx = 1
End If
Call LoadCLFavorites(False)
End Sub

Private Sub ClearSelectButtons()
cmdSearch1.Visible = False
cmdSearch1.Text = ""
cmdSearch2.Visible = False
cmdSearch2.Text = ""
cmdSearch3.Visible = False
cmdSearch3.Text = ""
cmdSearch4.Visible = False
cmdSearch4.Text = ""
cmdSearch5.Visible = False
cmdSearch5.Text = ""
cmdSearch6.Visible = False
cmdSearch6.Text = ""
cmdSearch7.Visible = False
cmdSearch7.Text = ""
cmdSearch8.Visible = False
cmdSearch8.Text = ""
cmdSearch9.Visible = False
cmdSearch9.Text = ""
cmdSearch10.Visible = False
cmdSearch10.Text = ""
cmdSearch11.Visible = False
cmdSearch11.Text = ""
cmdSearch12.Visible = False
cmdSearch12.Text = ""
cmdSearch13.Visible = False
cmdSearch13.Text = ""
cmdSearch14.Visible = False
cmdSearch14.Text = ""
cmdSearch15.Visible = False
cmdSearch15.Text = ""
End Sub

Public Function LoadCLFavorites(Init As Boolean) As Boolean
Dim i As Integer
Dim j As Integer
Dim DisplayText As String
Dim RetFav As PracticeFavorites
LoadCLFavorites = True
If (Init) Then
    CurrentSelectButtonIdx = 0
    InventoryId = 0
    InventoryDescription = ""
End If
Call ClearSelectButtons
Set RetFav = New PracticeFavorites
RetFav.FavoritesSystem = "{"
TotalFavorites = RetFav.FindFavorites
If (TotalFavorites > 0) Then
    'If (CurrentSelectButtonIdx >= TotalFavorites) Or (CurrentSelectButtonIdx < 1) Then
	If (CurrentSelectButtonIdx > TotalFavorites) Or (CurrentSelectButtonIdx < 1) Then
        CurrentSelectButtonIdx = 1
    End If
    j = 1
    i = CurrentSelectButtonIdx
    While (RetFav.SelectFavorites(i)) And (j < 16)
        DisplayText = GetCLText(Val(Trim(RetFav.FavoritesDiagnosis)))
        If (j = 1) Then
            cmdSearch1.Text = DisplayText
            cmdSearch1.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch1.Visible = True
        ElseIf (j = 2) Then
            cmdSearch2.Text = DisplayText
            cmdSearch2.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch2.Visible = True
        ElseIf (j = 3) Then
            cmdSearch3.Text = DisplayText
            cmdSearch3.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch3.Visible = True
        ElseIf (j = 4) Then
            cmdSearch4.Text = DisplayText
            cmdSearch4.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch4.Visible = True
        ElseIf (j = 5) Then
            cmdSearch5.Text = DisplayText
            cmdSearch5.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch5.Visible = True
        ElseIf (j = 6) Then
            cmdSearch6.Text = DisplayText
            cmdSearch6.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch6.Visible = True
        ElseIf (j = 7) Then
            cmdSearch7.Text = DisplayText
            cmdSearch7.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch7.Visible = True
        ElseIf (j = 8) Then
            cmdSearch8.Text = DisplayText
            cmdSearch8.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch8.Visible = True
        ElseIf (j = 9) Then
            cmdSearch9.Text = DisplayText
            cmdSearch9.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch9.Visible = True
        ElseIf (j = 10) Then
            cmdSearch10.Text = DisplayText
            cmdSearch10.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch10.Visible = True
        ElseIf (j = 11) Then
            cmdSearch11.Text = DisplayText
            cmdSearch11.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch11.Visible = True
        ElseIf (j = 12) Then
            cmdSearch12.Text = DisplayText
            cmdSearch12.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch12.Visible = True
        ElseIf (j = 13) Then
            cmdSearch13.Text = DisplayText
            cmdSearch13.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch13.Visible = True
        ElseIf (j = 14) Then
            cmdSearch14.Text = DisplayText
            cmdSearch14.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch14.Visible = True
        ElseIf (j = 15) Then
            cmdSearch15.Text = DisplayText
            cmdSearch15.Tag = Trim(RetFav.FavoritesDiagnosis)
            cmdSearch15.Visible = True
        End If
        i = i + 1
        j = j + 1
    Wend
    CurrentSelectButtonIdx = i
End If
cmdPrev.Visible = False
cmdNext.Visible = False
If (TotalFavorites > 15) Then
    cmdPrev.Visible = True
    cmdNext.Visible = True
End If
End Function

Private Function GetCLText(AId As Long) As String
Dim Temp As String
Dim RetInv As CLInventory
Temp = ""
If (AId > 0) Then
    Set RetInv = New CLInventory
    RetInv.InventoryId = AId
    If (RetInv.RetrieveCLInventory) Then
        If (Trim(RetInv.Series) <> "") Then
            Temp = Temp + Trim(RetInv.Series) + " "
        End If
'        If (Trim(RetInv.Tint) <> "") Then
'            Temp = Temp + Trim(RetInv.Tint) + " "
'        End If
'        If (Trim(RetInv.Manufacturer) <> "") Then
'            Temp = Temp + Trim(RetInv.Manufacturer) + " "
'        End If
        If (Trim(RetInv.Type_) <> "") Then
            Temp = Temp + Trim(RetInv.Type_) + " "
        End If
'        If (Trim(RetInv.Material) <> "") Then
'            Temp = Temp + Trim(RetInv.Material) + " "
'        End If
'        If (Trim(RetInv.Disposable) <> "") Then
'            Temp = Temp + Trim(RetInv.Disposable) + " "
'        End If
        If (Trim(RetInv.WearTime) <> "") Then
            Temp = Temp + Trim(RetInv.WearTime) + " "
        End If
    End If
    Set RetInv = Nothing
End If
GetCLText = Temp
End Function

Private Sub cmdSearch_Click()
frmCLSearch.cmd(0).Text = cmd(0).Text
frmCLSearch.cmd(0).Visible = cmd(0).Visible
frmCLSearch.cmd(1).Visible = cmd(0).Visible

frmCLSearch.Show 1
'If (frmCLSearch.InventoryId > 0) And (Trim(frmCLSearch.InventoryDescription) <> "") Then
If Trim(frmCLSearch.InventoryDescription) <> "" Then
    InventoryId = frmCLSearch.InventoryId
    InventoryDescription = frmCLSearch.InventoryDescription
    Unload frmCLFavs
End If
End Sub

Private Sub cmdSearch1_Click()
Call SelectCLFavorite(cmdSearch1.Tag, cmdSearch1.Text)
End Sub

Private Sub cmdSearch2_Click()
Call SelectCLFavorite(cmdSearch2.Tag, cmdSearch2.Text)
End Sub

Private Sub cmdSearch3_Click()
Call SelectCLFavorite(cmdSearch3.Tag, cmdSearch3.Text)
End Sub

Private Sub cmdSearch4_Click()
Call SelectCLFavorite(cmdSearch4.Tag, cmdSearch4.Text)
End Sub

Private Sub cmdSearch5_Click()
Call SelectCLFavorite(cmdSearch5.Tag, cmdSearch5.Text)
End Sub

Private Sub cmdSearch6_Click()
Call SelectCLFavorite(cmdSearch6.Tag, cmdSearch6.Text)
End Sub

Private Sub cmdSearch7_Click()
Call SelectCLFavorite(cmdSearch7.Tag, cmdSearch7.Text)
End Sub

Private Sub cmdSearch8_Click()
Call SelectCLFavorite(cmdSearch8.Tag, cmdSearch8.Text)
End Sub

Private Sub cmdSearch9_Click()
Call SelectCLFavorite(cmdSearch9.Tag, cmdSearch9.Text)
End Sub

Private Sub cmdSearch10_Click()
Call SelectCLFavorite(cmdSearch10.Tag, cmdSearch10.Text)
End Sub

Private Sub cmdSearch11_Click()
Call SelectCLFavorite(cmdSearch11.Tag, cmdSearch11.Text)
End Sub

Private Sub cmdSearch12_Click()
Call SelectCLFavorite(cmdSearch12.Tag, cmdSearch12.Text)
End Sub

Private Sub cmdSearch13_Click()
Call SelectCLFavorite(cmdSearch13.Tag, cmdSearch13.Text)
End Sub

Private Sub cmdSearch14_Click()
Call SelectCLFavorite(cmdSearch14.Tag, cmdSearch14.Text)
End Sub

Private Sub cmdSearch15_Click()
Call SelectCLFavorite(cmdSearch15.Tag, cmdSearch15.Text)
End Sub

Private Sub SelectCLFavorite(ATag As String, AText As String)
txtCLFav.Tag = ATag
txtCLFav.Text = AText
End Sub

Private Sub cmdTint_Click()
Dim AId As Long
Dim ATint As String
If (txtCLFav.Tag <> "") Then
    Set frmAppType.lstBox = lstBox
    frmAppType.ApptTypeName = txtCLFav.Tag
    If (frmAppType.LoadAppt("{", True)) Then
        frmAppType.Show 1
        If (Trim(frmAppType.ApptPeriod) <> "") Then
            ATint = Trim(frmAppType.ApptPeriod)
            AId = GetInventoryItem(Val(Trim(txtCLFav.Tag)), ATint)
            If (AId > 0) Then
                txtCLFav.Text = Trim(txtCLFav.Text) + " " + ATint
                txtCLFav.Tag = Trim(str(AId))
            End If
        End If
    Else
        frmEventMsgs.Header = "No Tints Available"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    frmEventMsgs.Header = "Select a favorite"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "OK"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Function GetInventoryItem(AId As Long, ATint As String) As Long
Dim ASeries As String
Dim RetInv As CLInventory
ASeries = ""
GetInventoryItem = 0
If (AId > 0) Then
    Set RetInv = New CLInventory
    RetInv.InventoryId = AId
    If (RetInv.RetrieveCLInventory) Then
        ASeries = Trim(RetInv.Series)
    End If
    Set RetInv = Nothing
    If (ASeries <> "") Then
        Set RetInv = New CLInventory
        RetInv.FieldSelect = "InventoryId"
        RetInv.Criteria = "Series = '" & ASeries & "' And Tint = '" & ATint & "' "
        If (RetInv.FindCLInventorySingleField) Then
            GetInventoryItem = Val(Trim(RetInv.SingleFieldValue))
        End If
        Set RetInv = Nothing
    End If
End If
End Function

Private Sub Form_Load()
cmd(0).Top = cmd(1).Top
End Sub

Private Sub txtCLFav_GotFocus()
SF.SetFocus
End Sub

Private Sub txtCLFav_KeyPress(KeyAscii As Integer)
KeyAscii = 0
End Sub
Public Function FrmClose()
 Call cmdHome_Click
 Unload Me
End Function

