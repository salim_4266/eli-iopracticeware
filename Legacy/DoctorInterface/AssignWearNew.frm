VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmAssignWearNew 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9405
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12420
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "AssignWearNew.frx":0000
   ScaleHeight     =   9405
   ScaleWidth      =   12420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame frLine 
      BackColor       =   &H000000FF&
      BorderStyle     =   0  'None
      Height          =   615
      Index           =   3
      Left            =   240
      TabIndex        =   63
      Top             =   4200
      Visible         =   0   'False
      Width           =   30
   End
   Begin VB.Frame frLine 
      BackColor       =   &H000000FF&
      BorderStyle     =   0  'None
      Height          =   855
      Index           =   2
      Left            =   120
      TabIndex        =   62
      Top             =   4200
      Visible         =   0   'False
      Width           =   30
   End
   Begin VB.Frame frLine 
      BackColor       =   &H000000FF&
      BorderStyle     =   0  'None
      Height          =   30
      Index           =   1
      Left            =   0
      TabIndex        =   61
      Top             =   4680
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Frame frLine 
      BackColor       =   &H000000FF&
      BorderStyle     =   0  'None
      Height          =   30
      Index           =   0
      Left            =   0
      TabIndex        =   60
      Top             =   4200
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.CommandButton cmdRH 
      BackColor       =   &H0080C0FF&
      Caption         =   "Compress"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10200
      Style           =   1  'Graphical
      TabIndex        =   202
      Top             =   120
      Width           =   1215
   End
   Begin VB.Frame frLast 
      BackColor       =   &H0077742D&
      Height          =   6540
      Left            =   5040
      TabIndex        =   180
      Top             =   1440
      Visible         =   0   'False
      Width           =   2535
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0FF&
         Caption         =   " Wearing"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   194
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         Caption         =   "ARF"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   193
         Top             =   1200
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         Caption         =   "ARF Wet"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   192
         Top             =   1800
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         Caption         =   "MRX Dry"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   3
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   191
         Top             =   2400
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         Caption         =   "Cyclo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   4
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   190
         Top             =   3000
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFF80&
         Caption         =   "OverM SP"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   7
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   189
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFF80&
         Caption         =   "OverM CL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   8
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   188
         Top             =   1200
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00FF8080&
         Caption         =   "Ret Dry"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   9
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   187
         Top             =   1800
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00FF8080&
         Caption         =   "Ret Wet"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   10
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   186
         Top             =   2400
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         Caption         =   "Dilated"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   11
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   185
         Top             =   3000
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H0080C0FF&
         Caption         =   "Rx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   12
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   184
         Top             =   3600
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0FF&
         Caption         =   " Wearing"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   13
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   183
         Top             =   4680
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         Caption         =   "Trial"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   14
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   182
         Top             =   5280
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H0080C0FF&
         Caption         =   "CL Rx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   17
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   181
         Top             =   5880
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         Caption         =   "Order Trial"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   16
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   197
         Top             =   5280
         Width           =   975
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000E&
         Caption         =   "sp15"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   198
         Top             =   4920
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000E&
         Caption         =   "sp5"
         Height          =   195
         Index           =   5
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   195
         Top             =   3000
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton cmdL 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000E&
         Caption         =   "sp6"
         Height          =   195
         Index           =   6
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   196
         Top             =   3240
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label Label4 
         BackColor       =   &H0077742D&
         Caption         =   "Contact Lenses"
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   240
         TabIndex        =   200
         Top             =   4320
         Width           =   2175
      End
      Begin VB.Label Label3 
         BackColor       =   &H0077742D&
         Caption         =   "Spectacles"
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   240
         TabIndex        =   199
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame frNumPad 
      BackColor       =   &H0077742D&
      Height          =   3645
      Left            =   9360
      TabIndex        =   70
      Top             =   4335
      Visible         =   0   'False
      Width           =   2535
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   7
         Left            =   0
         TabIndex        =   71
         Top             =   720
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":36C9
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   0
         Left            =   0
         TabIndex        =   72
         Top             =   2880
         Width           =   1695
         _Version        =   131072
         _ExtentX        =   2990
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":38A9
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   1
         Left            =   0
         TabIndex        =   73
         Top             =   2160
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":3A89
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   2
         Left            =   840
         TabIndex        =   74
         Top             =   2160
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":3C69
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   3
         Left            =   1680
         TabIndex        =   75
         Top             =   2160
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":3E49
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   4
         Left            =   0
         TabIndex        =   76
         Top             =   1440
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":4029
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   5
         Left            =   840
         TabIndex        =   77
         Top             =   1440
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":4209
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   6
         Left            =   1680
         TabIndex        =   78
         Top             =   1440
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":43E9
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   8
         Left            =   840
         TabIndex        =   79
         Top             =   720
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":45C9
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   9
         Left            =   1680
         TabIndex        =   80
         Top             =   720
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":47A9
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   10
         Left            =   1680
         TabIndex        =   81
         Top             =   2880
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":4989
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   12
         Left            =   840
         TabIndex        =   82
         Top             =   0
         Width           =   795
         _Version        =   131072
         _ExtentX        =   1402
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":4B69
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   11
         Left            =   0
         TabIndex        =   83
         Top             =   0
         Width           =   855
         _Version        =   131072
         _ExtentX        =   1508
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":4D49
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNP 
         Height          =   735
         Index           =   13
         Left            =   1635
         TabIndex        =   90
         Top             =   0
         Width           =   900
         _Version        =   131072
         _ExtentX        =   1587
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":4F2C
      End
   End
   Begin VB.Frame frFixed 
      BackColor       =   &H0077742D&
      Height          =   7935
      Left            =   7920
      TabIndex        =   91
      Top             =   45
      Visible         =   0   'False
      Width           =   3975
      Begin fpBtnAtlLibCtl.fpBtn cmdJSw 
         Height          =   735
         Left            =   1320
         TabIndex        =   201
         Top             =   7110
         Visible         =   0   'False
         Width           =   1095
         _Version        =   131072
         _ExtentX        =   1931
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":510C
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   22
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   124
         Top             =   240
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   23
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   123
         Top             =   930
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   24
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   122
         Top             =   1620
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   25
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   121
         Top             =   2310
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   26
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   120
         Top             =   3000
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   27
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   119
         Top             =   3690
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   28
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   118
         Top             =   4380
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   29
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   117
         Top             =   5070
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   30
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   116
         Top             =   5760
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   31
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   115
         Top             =   6450
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   32
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   114
         Top             =   7140
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00FFFF80&
         Height          =   615
         Index           =   11
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   113
         Top             =   240
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   12
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   112
         Top             =   930
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   13
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   111
         Top             =   1620
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   14
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   110
         Top             =   2310
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   15
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   109
         Top             =   3000
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   16
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   108
         Top             =   3690
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   17
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   107
         Top             =   4380
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   18
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   106
         Top             =   5070
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   19
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   105
         Top             =   5760
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   20
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   104
         Top             =   6450
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   21
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   103
         Top             =   7140
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   10
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   102
         Top             =   7140
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   9
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   101
         Top             =   6450
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   8
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   100
         Top             =   5760
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   7
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   99
         Top             =   5070
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   6
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   98
         Top             =   4380
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   5
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   97
         Top             =   3690
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   4
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   96
         Top             =   3000
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   3
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   95
         Top             =   2310
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00C0FFC0&
         Height          =   615
         Index           =   2
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   94
         Top             =   1620
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00FFFFFF&
         Height          =   615
         Index           =   1
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   93
         Top             =   930
         Width           =   1095
      End
      Begin VB.CheckBox chkF 
         BackColor       =   &H00FFFFC0&
         Height          =   615
         Index           =   0
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   92
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame frHist 
      BorderStyle     =   0  'None
      Height          =   4095
      Left            =   0
      TabIndex        =   137
      Top             =   2160
      Visible         =   0   'False
      Width           =   11895
      Begin VB.Frame frHCmd 
         BackColor       =   &H0077742D&
         BorderStyle     =   0  'None
         Caption         =   "Refraction"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   855
         Left            =   120
         TabIndex        =   138
         Top             =   3120
         Width           =   11775
         Begin fpBtnAtlLibCtl.fpBtn fpBtn2 
            Height          =   615
            Left            =   9360
            TabIndex        =   139
            Top             =   3240
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":52EC
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn3 
            Height          =   615
            Left            =   9360
            TabIndex        =   140
            Top             =   2520
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":54CE
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn4 
            Height          =   615
            Left            =   8040
            TabIndex        =   141
            Top             =   3240
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":56B0
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn5 
            Height          =   615
            Left            =   6720
            TabIndex        =   142
            Top             =   3240
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":5892
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn6 
            Height          =   615
            Left            =   5400
            TabIndex        =   143
            Top             =   3240
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":5A74
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn7 
            Height          =   615
            Left            =   4080
            TabIndex        =   144
            Top             =   3240
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":5C56
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn8 
            Height          =   615
            Left            =   2760
            TabIndex        =   145
            Top             =   3240
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":5E38
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn9 
            Height          =   615
            Left            =   1440
            TabIndex        =   146
            Top             =   3240
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":601A
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn10 
            Height          =   615
            Left            =   120
            TabIndex        =   147
            Top             =   3240
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":61FC
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn11 
            Height          =   615
            Left            =   8040
            TabIndex        =   148
            Top             =   2520
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":63DE
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn12 
            Height          =   615
            Left            =   6720
            TabIndex        =   149
            Top             =   2520
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":65C0
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn13 
            Height          =   615
            Left            =   5400
            TabIndex        =   150
            Top             =   2520
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":67A2
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn14 
            Height          =   615
            Left            =   4080
            TabIndex        =   151
            Top             =   2520
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":6984
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn15 
            Height          =   615
            Left            =   2760
            TabIndex        =   152
            Top             =   2520
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":6B66
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn16 
            Height          =   615
            Left            =   1440
            TabIndex        =   153
            Top             =   2520
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":6D48
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn17 
            Height          =   615
            Left            =   120
            TabIndex        =   154
            Top             =   2520
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1085
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":6F2A
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn18 
            Height          =   735
            Left            =   1440
            TabIndex        =   155
            Top             =   1680
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":710C
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn19 
            Height          =   735
            Left            =   4080
            TabIndex        =   156
            Top             =   1680
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":72F8
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn21 
            Height          =   735
            Left            =   5400
            TabIndex        =   157
            Top             =   1680
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":74E1
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn22 
            Height          =   735
            Left            =   10440
            TabIndex        =   158
            Top             =   1680
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":76CF
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn23 
            Height          =   735
            Left            =   9240
            TabIndex        =   159
            Top             =   1680
            Width           =   1095
            _Version        =   131072
            _ExtentX        =   1931
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":78B2
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn25 
            Height          =   735
            Left            =   6720
            TabIndex        =   160
            Top             =   1680
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":7A98
         End
         Begin fpBtnAtlLibCtl.fpBtn fpBtn26 
            Height          =   735
            Left            =   120
            TabIndex        =   161
            Top             =   1680
            Width           =   1215
            _Version        =   131072
            _ExtentX        =   2143
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":7C80
         End
         Begin fpBtnAtlLibCtl.fpBtn CmdHExit 
            Height          =   735
            Left            =   0
            TabIndex        =   162
            Top             =   0
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":7E6B
         End
         Begin fpBtnAtlLibCtl.fpBtn Cmd 
            Height          =   735
            Index           =   7
            Left            =   2160
            TabIndex        =   167
            Top             =   0
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":804E
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdHist 
            Height          =   735
            Index           =   2
            Left            =   4320
            TabIndex        =   168
            Top             =   0
            Width           =   615
            _Version        =   131072
            _ExtentX        =   1085
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":8234
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdHist 
            Height          =   735
            Index           =   1
            Left            =   3720
            TabIndex        =   169
            Top             =   0
            Width           =   615
            _Version        =   131072
            _ExtentX        =   1085
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":8414
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdHist 
            Height          =   735
            Index           =   3
            Left            =   6480
            TabIndex        =   170
            Top             =   0
            Width           =   615
            _Version        =   131072
            _ExtentX        =   1085
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":85F4
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdHist 
            Height          =   735
            Index           =   4
            Left            =   7080
            TabIndex        =   171
            Top             =   0
            Width           =   615
            _Version        =   131072
            _ExtentX        =   1085
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":87D4
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdHist 
            Height          =   735
            Index           =   5
            Left            =   4920
            TabIndex        =   172
            Top             =   0
            Width           =   1575
            _Version        =   131072
            _ExtentX        =   2778
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":89B4
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdShow 
            Height          =   735
            Left            =   1080
            TabIndex        =   175
            Top             =   0
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":8B93
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdSumm 
            Height          =   735
            Left            =   7800
            TabIndex        =   176
            Top             =   0
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   7828525
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":8D76
         End
      End
      Begin MSFlexGridLib.MSFlexGrid HlstRxs 
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   163
         Top             =   120
         Width           =   11775
         _ExtentX        =   20770
         _ExtentY        =   450
         _Version        =   393216
         Cols            =   12
         BackColor       =   7104808
         BackColorFixed  =   4210816
         ForeColorFixed  =   -2147483634
         BackColorBkg    =   7104808
         WordWrap        =   -1  'True
         Enabled         =   -1  'True
         Appearance      =   0
      End
      Begin MSFlexGridLib.MSFlexGrid lstRxs 
         Height          =   1455
         Index           =   2
         Left            =   120
         TabIndex        =   164
         Top             =   360
         Width           =   11775
         _ExtentX        =   20770
         _ExtentY        =   2566
         _Version        =   393216
         Rows            =   0
         Cols            =   14
         FixedRows       =   0
         FixedCols       =   0
         BackColor       =   16777215
         BackColorFixed  =   8421504
         ForeColorFixed  =   -2147483634
         BackColorSel    =   7104808
         ForeColorSel    =   -2147483640
         BackColorBkg    =   7828525
         GridColor       =   0
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         Enabled         =   -1  'True
         GridLinesFixed  =   1
         ScrollBars      =   2
         AllowUserResizing=   3
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid HlstRxs 
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   165
         Top             =   1800
         Width           =   11775
         _ExtentX        =   20770
         _ExtentY        =   450
         _Version        =   393216
         Cols            =   12
         BackColor       =   7104808
         BackColorFixed  =   4210816
         ForeColorFixed  =   -2147483634
         BackColorBkg    =   7104808
         WordWrap        =   -1  'True
         Enabled         =   -1  'True
         Appearance      =   0
      End
      Begin MSFlexGridLib.MSFlexGrid lstRxs 
         Height          =   975
         Index           =   3
         Left            =   120
         TabIndex        =   166
         Top             =   2040
         Width           =   11775
         _ExtentX        =   20770
         _ExtentY        =   1720
         _Version        =   393216
         Rows            =   0
         Cols            =   14
         FixedRows       =   0
         FixedCols       =   0
         BackColor       =   16777215
         BackColorFixed  =   8421504
         ForeColorFixed  =   -2147483634
         BackColorSel    =   7104808
         ForeColorSel    =   -2147483640
         BackColorBkg    =   7104808
         GridColor       =   0
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         Enabled         =   -1  'True
         GridLinesFixed  =   1
         ScrollBars      =   2
         AllowUserResizing=   3
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSFlexGridLib.MSFlexGrid lstVSC 
      Height          =   870
      Left            =   90
      TabIndex        =   132
      Top             =   480
      Visible         =   0   'False
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   1535
      _Version        =   393216
      Rows            =   3
      Cols            =   12
      FixedCols       =   0
      BackColor       =   16777215
      BackColorFixed  =   4210816
      ForeColorFixed  =   -2147483634
      BackColorSel    =   7104808
      ForeColorSel    =   -2147483640
      BackColorBkg    =   7104808
      GridColor       =   0
      GridColorFixed  =   16777215
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      Enabled         =   -1  'True
      ScrollBars      =   2
      AllowUserResizing=   3
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H0077742D&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   165
      Left            =   0
      TabIndex        =   134
      Top             =   7800
      Width           =   4935
      Begin VB.Label lblPat 
         BackColor       =   &H0077742D&
         Caption         =   "Label1"
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   120
         TabIndex        =   135
         Top             =   0
         Width           =   4215
      End
   End
   Begin VB.Frame frNewEl 
      BackColor       =   &H0077742D&
      Height          =   6540
      Left            =   1080
      TabIndex        =   35
      Top             =   1440
      Visible         =   0   'False
      Width           =   2535
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H0080C0FF&
         Caption         =   "CL Rx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   17
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   5880
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         Caption         =   "Trial"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   14
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   5280
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0FF&
         Caption         =   " Wearing"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   13
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   4680
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H0080C0FF&
         Caption         =   "Rx"
         Height          =   495
         Index           =   12
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   3600
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         Caption         =   "Dilated"
         Height          =   495
         Index           =   11
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   3000
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00FF8080&
         Caption         =   "Ret Wet"
         Height          =   495
         Index           =   10
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   50
         Top             =   2400
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00FF8080&
         Caption         =   "Ret Dry"
         Height          =   495
         Index           =   9
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   49
         Top             =   1800
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFF80&
         Caption         =   "OverM CL"
         Height          =   495
         Index           =   8
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   1200
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFF80&
         Caption         =   "OverM SP"
         Height          =   495
         Index           =   7
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFC0&
         Caption         =   "Cyclo"
         Height          =   495
         Index           =   4
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   3000
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFC0&
         Caption         =   "MRX Dry"
         Height          =   495
         Index           =   3
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   2400
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         Caption         =   "ARF Wet"
         Height          =   495
         Index           =   2
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   1800
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         Caption         =   "ARF"
         Height          =   495
         Index           =   1
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   1200
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0FF&
         Caption         =   " Wearing"
         Height          =   495
         Index           =   0
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000E&
         Caption         =   "sp5"
         Height          =   195
         Index           =   5
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   3000
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000E&
         Caption         =   "sp6"
         Height          =   195
         Index           =   6
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   3240
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFC0C0&
         Caption         =   "Order Trial"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   16
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   5280
         Width           =   975
      End
      Begin VB.CommandButton cmdH 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000E&
         Caption         =   "sp15"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   4920
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label Label2 
         BackColor       =   &H0077742D&
         Caption         =   "Spectacles"
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   240
         TabIndex        =   179
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label Label1 
         BackColor       =   &H0077742D&
         Caption         =   "Contact Lenses"
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   240
         TabIndex        =   178
         Top             =   4320
         Width           =   2175
      End
   End
   Begin VB.Frame frInfo 
      BackColor       =   &H0077742D&
      Height          =   975
      Left            =   0
      TabIndex        =   36
      Top             =   6120
      Visible         =   0   'False
      Width           =   11895
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   99
         Left            =   120
         TabIndex        =   37
         Top             =   180
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":8F5D
      End
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   8
         Left            =   3360
         TabIndex        =   174
         Top             =   180
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":9142
      End
      Begin VB.Label lblInfo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label1"
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   5040
         TabIndex        =   38
         Top             =   240
         Width           =   6135
      End
   End
   Begin VB.Frame frEdit 
      BackColor       =   &H0077742D&
      Height          =   975
      Left            =   0
      TabIndex        =   58
      Top             =   7320
      Visible         =   0   'False
      Width           =   12015
      Begin fpBtnAtlLibCtl.fpBtn cmdCCom 
         Height          =   735
         Index           =   2
         Left            =   8520
         TabIndex        =   129
         Top             =   480
         Visible         =   0   'False
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":932C
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdCCom 
         Height          =   735
         Index           =   1
         Left            =   7320
         TabIndex        =   128
         Top             =   480
         Visible         =   0   'False
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":9516
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdCCom 
         Height          =   735
         Index           =   0
         Left            =   6120
         TabIndex        =   127
         Top             =   480
         Visible         =   0   'False
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":9707
      End
      Begin VB.TextBox txtType 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   0
         MaxLength       =   500
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   126
         Top             =   0
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Frame frAdj 
         BackColor       =   &H0077742D&
         BorderStyle     =   0  'None
         Height          =   750
         Left            =   1200
         TabIndex        =   64
         Top             =   180
         Width           =   8655
         Begin fpBtnAtlLibCtl.fpBtn cmdOU 
            Height          =   735
            Left            =   5280
            TabIndex        =   173
            Top             =   0
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":98F9
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdAdj 
            Height          =   735
            Index           =   0
            Left            =   0
            TabIndex        =   65
            Top             =   0
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":9ADA
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdAdj 
            Height          =   735
            Index           =   1
            Left            =   1080
            TabIndex        =   66
            Top             =   0
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":9CBE
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdAdj 
            Height          =   735
            Index           =   2
            Left            =   1920
            TabIndex        =   67
            Top             =   0
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":9E9F
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdAdj 
            Height          =   735
            Index           =   3
            Left            =   3600
            TabIndex        =   68
            Top             =   0
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":A082
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdAdj 
            Height          =   735
            Index           =   4
            Left            =   4440
            TabIndex        =   69
            Top             =   0
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":A265
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdAdj 
            Height          =   735
            Index           =   5
            Left            =   5400
            TabIndex        =   85
            Top             =   0
            Width           =   855
            _Version        =   131072
            _ExtentX        =   1508
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":A446
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdAdj 
            Height          =   735
            Index           =   6
            Left            =   6480
            TabIndex        =   86
            Top             =   0
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":A62A
         End
         Begin fpBtnAtlLibCtl.fpBtn cmdNumPad 
            Height          =   735
            Left            =   7560
            TabIndex        =   87
            Top             =   0
            Width           =   975
            _Version        =   131072
            _ExtentX        =   1720
            _ExtentY        =   1296
            Enabled         =   -1  'True
            MousePointer    =   0
            Object.TabStop         =   -1  'True
            GrayAreaColor   =   12632256
            BorderShowDefault=   -1  'True
            ButtonType      =   0
            NoPointerFocus  =   0   'False
            Value           =   0   'False
            GroupID         =   0
            GroupSelect     =   0
            DrawFocusRect   =   2
            DrawFocusRectCell=   -1
            GrayAreaPictureStyle=   0
            Static          =   0   'False
            BackStyle       =   1
            AutoSize        =   0
            AutoSizeOffsetTop=   0
            AutoSizeOffsetBottom=   0
            AutoSizeOffsetLeft=   0
            AutoSizeOffsetRight=   0
            DropShadowOffsetX=   3
            DropShadowOffsetY=   3
            DropShadowType  =   0
            DropShadowColor =   0
            Redraw          =   -1  'True
            ButtonDesigner  =   "AssignWearNew.frx":A810
         End
         Begin MSFlexGridLib.MSFlexGrid FGCCell 
            Height          =   615
            Left            =   2880
            TabIndex        =   88
            Top             =   120
            Width           =   630
            _ExtentX        =   1111
            _ExtentY        =   1085
            _Version        =   393216
            BackColor       =   16777215
            BackColorFixed  =   8421504
            ForeColorFixed  =   -2147483634
            BackColorSel    =   7104808
            ForeColorSel    =   -2147483640
            BackColorBkg    =   7828525
            GridColor       =   0
            WordWrap        =   -1  'True
            ScrollTrack     =   -1  'True
            FocusRect       =   2
            GridLinesFixed  =   1
            AllowUserResizing=   3
            BorderStyle     =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   98
         Left            =   120
         TabIndex        =   59
         Top             =   180
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":A9F6
      End
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   4
         Left            =   10920
         TabIndex        =   84
         Top             =   180
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":ABDB
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdKeyBoard 
         Height          =   735
         Left            =   9840
         TabIndex        =   125
         Top             =   180
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":ADBE
      End
      Begin VB.Label lblCommNotes 
         Appearance      =   0  'Flat
         BackColor       =   &H0080C0FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Glasses Rx notes are not eye specific and are entered in the top comment box."
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   0
         TabIndex        =   203
         Top             =   0
         Visible         =   0   'False
         Width           =   4335
      End
   End
   Begin VB.ListBox lstBox 
      Height          =   255
      Left            =   10920
      Sorted          =   -1  'True
      TabIndex        =   39
      Top             =   0
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      Height          =   615
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   675
      TabIndex        =   34
      Top             =   3240
      Visible         =   0   'False
      Width           =   735
   End
   Begin MSFlexGridLib.MSFlexGrid HlstRxs 
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   450
      _Version        =   393216
      Cols            =   12
      BackColor       =   7104808
      BackColorFixed  =   4210816
      ForeColorFixed  =   -2147483634
      BackColorBkg    =   7104808
      WordWrap        =   -1  'True
      Appearance      =   0
   End
   Begin MSFlexGridLib.MSFlexGrid lstRxs 
      Height          =   1455
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   2566
      _Version        =   393216
      Rows            =   0
      Cols            =   14
      FixedRows       =   0
      FixedCols       =   0
      BackColor       =   16777215
      BackColorFixed  =   8421504
      ForeColorFixed  =   -2147483634
      BackColorSel    =   7104808
      ForeColorSel    =   -2147483640
      BackColorBkg    =   7828525
      GridColor       =   0
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      GridLinesFixed  =   1
      ScrollBars      =   2
      AllowUserResizing=   3
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid HlstRxs 
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   28
      Top             =   1800
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   450
      _Version        =   393216
      Cols            =   12
      BackColor       =   7104808
      BackColorFixed  =   4210816
      ForeColorFixed  =   -2147483634
      BackColorBkg    =   7104808
      WordWrap        =   -1  'True
      Appearance      =   0
   End
   Begin MSFlexGridLib.MSFlexGrid lstRxs 
      Height          =   975
      Index           =   1
      Left            =   120
      TabIndex        =   29
      Top             =   2040
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   1720
      _Version        =   393216
      Rows            =   0
      Cols            =   14
      FixedRows       =   0
      FixedCols       =   0
      BackColor       =   16777215
      BackColorFixed  =   8421504
      ForeColorFixed  =   -2147483634
      BackColorSel    =   7104808
      ForeColorSel    =   -2147483640
      BackColorBkg    =   7104808
      GridColor       =   0
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      GridLinesFixed  =   1
      ScrollBars      =   2
      AllowUserResizing=   3
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H0077742D&
      BorderStyle     =   0  'None
      Caption         =   "Refraction"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   855
      Left            =   120
      TabIndex        =   2
      Top             =   8040
      Width           =   11775
      Begin fpBtnAtlLibCtl.fpBtn cmdOS8 
         Height          =   615
         Left            =   9360
         TabIndex        =   27
         Top             =   3240
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":AFA1
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOD8 
         Height          =   615
         Left            =   9360
         TabIndex        =   26
         Top             =   2520
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":B183
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOS7 
         Height          =   615
         Left            =   8040
         TabIndex        =   25
         Top             =   3240
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":B365
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOS6 
         Height          =   615
         Left            =   6720
         TabIndex        =   24
         Top             =   3240
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":B547
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOS5 
         Height          =   615
         Left            =   5400
         TabIndex        =   23
         Top             =   3240
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":B729
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOS4 
         Height          =   615
         Left            =   4080
         TabIndex        =   22
         Top             =   3240
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":B90B
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOS3 
         Height          =   615
         Left            =   2760
         TabIndex        =   21
         Top             =   3240
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":BAED
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOS2 
         Height          =   615
         Left            =   1440
         TabIndex        =   20
         Top             =   3240
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":BCCF
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOS1 
         Height          =   615
         Left            =   120
         TabIndex        =   19
         Top             =   3240
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":BEB1
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOD7 
         Height          =   615
         Left            =   8040
         TabIndex        =   18
         Top             =   2520
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":C093
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOD6 
         Height          =   615
         Left            =   6720
         TabIndex        =   17
         Top             =   2520
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":C275
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOD5 
         Height          =   615
         Left            =   5400
         TabIndex        =   16
         Top             =   2520
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":C457
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOD4 
         Height          =   615
         Left            =   4080
         TabIndex        =   15
         Top             =   2520
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":C639
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOD3 
         Height          =   615
         Left            =   2760
         TabIndex        =   14
         Top             =   2520
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":C81B
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOD2 
         Height          =   615
         Left            =   1440
         TabIndex        =   13
         Top             =   2520
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":C9FD
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdOD1 
         Height          =   615
         Left            =   120
         TabIndex        =   12
         Top             =   2520
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1085
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":CBDF
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdNewRx 
         Height          =   735
         Left            =   1440
         TabIndex        =   11
         Top             =   1680
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":CDC1
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdHigh 
         Height          =   735
         Left            =   4080
         TabIndex        =   10
         Top             =   1680
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":CFAD
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdHist 
         Height          =   735
         Index           =   0
         Left            =   5400
         TabIndex        =   9
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":D196
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdEdit 
         Height          =   735
         Left            =   5400
         TabIndex        =   8
         Top             =   1680
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":D37C
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdApply 
         Height          =   735
         Left            =   10440
         TabIndex        =   7
         Top             =   1680
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":D56A
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdCopy 
         Height          =   735
         Left            =   9240
         TabIndex        =   6
         Top             =   1680
         Width           =   1095
         _Version        =   131072
         _ExtentX        =   1931
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":D74D
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
         Height          =   735
         Left            =   8640
         TabIndex        =   5
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":D933
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdRemove 
         Height          =   735
         Left            =   6720
         TabIndex        =   4
         Top             =   1680
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":DB17
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
         Height          =   735
         Left            =   120
         TabIndex        =   3
         Top             =   1680
         Width           =   1215
         _Version        =   131072
         _ExtentX        =   2143
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":DCFF
      End
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   0
         Left            =   0
         TabIndex        =   30
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":DEEA
      End
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   1
         Left            =   3240
         TabIndex        =   31
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":E0CD
      End
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   2
         Left            =   2160
         TabIndex        =   32
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":E2B7
      End
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   3
         Left            =   4320
         TabIndex        =   33
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":E49D
      End
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   5
         Left            =   1080
         TabIndex        =   89
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":E68B
      End
      Begin fpBtnAtlLibCtl.fpBtn Cmd 
         Height          =   735
         Index           =   6
         Left            =   7560
         TabIndex        =   131
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":E86F
      End
      Begin fpBtnAtlLibCtl.fpBtn CmdDone 
         Height          =   735
         Left            =   10800
         TabIndex        =   133
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":EA5A
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdHiL 
         Height          =   735
         Left            =   9720
         TabIndex        =   136
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":EC3D
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdLast 
         Height          =   735
         Index           =   0
         Left            =   6480
         TabIndex        =   177
         Top             =   0
         Width           =   975
         _Version        =   131072
         _ExtentX        =   1720
         _ExtentY        =   1296
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "AssignWearNew.frx":EE26
      End
   End
   Begin VB.FileListBox Files 
      Height          =   1065
      Left            =   4920
      TabIndex        =   130
      Top             =   360
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "frmAssignWearNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim AppInfoLoaded As Boolean

Dim fgc As MSFlexGrid
Dim bysys As Boolean
Dim CopyMode As Boolean
Dim CanLog As Boolean
Dim sFGC(3) As Integer
Dim mStr(3) As String
Dim CCom As Integer
Dim delim As String

Dim ExpendedMode As Boolean
Public CommArea As String

Public PID As String
Public ActivityId As Long
Public PatientId As Long
Public AppointmentId As Long
Public EditPreviousVisit As Boolean

Private Const TrialOrderColor As Long = &HC00000
Private Const TrialNormalColor As Long = &HC0
Private Const EnlargeOff = &H77742D
Private Const EnlargeOn = &HFF&

Private SelectedTest As Integer
Private CurrentEditTest As Integer
Private CurrRx As Integer
Private CurrRxCnt As Integer
Private CurrentVendors As Integer
Private ThePrinter As String
Private TheManifestUsed As Integer
Private MaxManifests As Integer

Private WearType As String
Private TheManifestResults As String

Private Const MaxSetCL As Integer = 120
Private Type ExternalReference
    EWType As String
    EWCompany As String
    ODManifest As String
    OSManifest As String
    InventoryIdOD As Long
    InventoryIdOS As Long
    RxNote As String
    OrderType As Integer
End Type

Private Type ODOSType
    SPHERE As String
    CYL As String
    Axis As String
    VA_DIST As String
    TADD As String
    INT_ADD As String
    VA_NEAR As String
    PSM_1 As String
    PSM_2 As String
    VERT As String
    TTYPE As String
    COMMENT As String
    BC As String
    DIA As String
    PC As String
    D_N As String
    Model As String
    Temp As String
End Type

Private Type tAppInfo
    AppId As Long
    AppDateTime As Date
    AppType As String
    ActId As Long
End Type

Dim AppInfo() As tAppInfo


Dim Titles As Scripting.Dictionary
' for Order Type 0=Rx 1=Trial 2=OrderTrial, 3=Wearing

Private EWType As String
Private EWCompany As String
Private TestQuestion As String
Private TestOrder As String

Private MyId(10) As String
Private MyQuestion(10) As String
Private CurrentManifest(10) As ExternalReference
Private SelectedCLRx(MaxSetCL) As ExternalReference
Private SelectedPCRx(MaxSetCL) As ExternalReference

Private MaxCL As Integer
Private CurrentIndex As Integer
Private MaxCLType As Integer
Private CurrentTypeIndex As Integer
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private TrialColor As Long
Private WearingColor As Long
Private OrderTrialColor As Long
Private RxColor As Long

' ++++++++++++++++++++++

Public StoreResults As String
Public FormOn As Boolean
Public Question As String
Public QuitOn As Boolean
Public ManifestResults As String
Public ManifestResultsOn As Boolean
Public CLFitOn As Boolean

Private Const MaxTestsAllowed = 6
Private Const MaxPadsAllowed = 8
Private Const MaxPadChoices = 24
Private Const MaxButtonsAllowed = 38

Private Type ButtonDetail
    Buttons As Boolean
    ButtonOn As Boolean
    ButtonName As String
    ButtonType As String
    ButtonText As String
    ButtonRange As String
    ButtonDeviceRef As String
    ButtonVisible As Boolean
    ButtonTrigger As String
    ButtonTriggerText As String
    ButtonIncrement As String
    ButtonMutual As Boolean
    ButtonPadOrder As Boolean
    ButtonUseOtherText As Boolean
    ButtonId As Long
    Result As String
End Type

Private TheTestId As Integer
Private TimeRef(MaxTestsAllowed) As ButtonDetail
Private MaxWriteButtonsForTests(MaxTestsAllowed) As Integer
Private MaxTests As Integer
Private MaxTestAudio As Integer
Private MaxTestWideBtns As Integer
Private MaxPadsForTests As Integer
Private MaxButtonsForTests As Integer
Private MaxButtonsForPads As Integer
Private MaxButtonsForButtons As Integer
Private DisplayTests(MaxTestsAllowed) As ButtonDetail
Private DisplayPads(MaxTestsAllowed, 2, MaxPadsAllowed) As ButtonDetail
Private DisplayBtns(MaxTestsAllowed, 2) As ButtonDetail
Private TestAudio(MaxTestsAllowed, 2) As ButtonDetail
Private TestWideButtons(MaxTestsAllowed, MaxButtonsAllowed) As ButtonDetail
Private PlantTriggerButtons(MaxTestsAllowed, 2, MaxButtonsAllowed) As ButtonDetail
Private PlantTriggerPads(MaxTestsAllowed, 2, MaxPadsAllowed, MaxPadChoices) As ButtonDetail

Private CurrentEye As String
Private CurrentOrder As String
Private ExitFast As Boolean
Private ScratchTestFile As String
Private PrevTestFile As String
Private CurrentFormId As Long
Private HistOn As Integer
Public SelVisit As Integer

Private Sub chkF_Click(Index As Integer)
If bysys Then Exit Sub

Dim i As Integer
Dim c As Integer
Dim v As String
Dim p As String
Dim tst As String

'uncheck other
bysys = True
For i = 0 To chkF.Count - 1
    If chkF(i).Tag = chkF(Index).Tag Then
        If Not i = Index Then
            chkF(i).Value = 0
        End If
    End If
Next
bysys = False
    
'get string from groups
For i = 0 To chkF.Count - 1
    If chkF(i).Value = 1 Then
        chkF(i).BackColor = vbWhite
        If Not v = "" Then
            If CCom = 3 Then v = v & ","
            v = v & " "
        End If
        v = v & chkF(i).Caption
    Else
        chkF(i).BackColor = &HFFFFC0
    End If
Next
        
'combine string with numpad
If CCom = 2 Then
    If Not v = "No Rotation" Then
        For i = 1 To Len(mStr(2))
            tst = Mid(mStr(2), i, 1)
            If IsNumeric(tst) Then
                p = p & tst
            Else
                Exit For
            End If
        Next
    End If
    v = Trim(p & " " & v)
End If

If CCom > 0 Then
    mStr(CCom) = v
    v = getMStr
    
End If

FGCCell.TextMatrix(1, 1) = v

End Sub

Private Function getMStr() As String

Dim mS(3) As String
Dim i As Integer
For i = 1 To 3
    mS(i) = mStr(i)
Next

If Not mS(1) = "" _
And (Not mS(2) = "" _
  Or Not mS(3) = "") Then
    mS(1) = mS(1) & delim
End If
If Not mS(2) = "" _
And Not mS(3) = "" Then
    mS(2) = mS(2) & delim
End If
getMStr = mS(1) & mS(2) & mS(3)

End Function

Private Sub cmd_Click(Index As Integer)
If Not UserLogin.HasPermission(epExamElements) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
Dim r As Integer
Dim r2 As Integer
Dim c As Integer
Select Case Index
Case 0

    frmEventMsgs.Header = ""
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Exit without saving"
    frmEventMsgs.CancelText = "Save and Exit"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1

    If (frmEventMsgs.Result = 2) Then
        Call frmSystems1.DisplayTestResults(True)
        Call frmSystems1.ViewTestResults(1)
        Unload Me
        FormOn = False
    Else
        cmdDone_Click
    End If

Case 1, 8
lstRxs(0).Enabled = False
    lstRxs(1).Enabled = False
    lblInfo.Caption = "Select or press 'Cancel' button"
    frInfo.Visible = True
    frNewEl.ZOrder
    frNewEl.Visible = True
    cmd(8).Visible = False
     
Case 2, 7
    If frLine(0).Visible Then
        CopyMode = True
        sFGC(0) = fgc.Index
        sFGC(1) = fgc.Row
        sFGC(2) = fgc.col
        lblInfo.Caption = "Select or press 'Cancel' button"
        frInfo.Visible = True
        cmd(8).Visible = True
        If Index = 7 Then CmdHExit_Click
    End If
Case 3
    If frLine(0).Visible Then
        If fgc.col = 0 Then
            r = Fix(fgc.Row / 2) * 2
            If IsDate(fgc.TextMatrix(r + 1, fgc.col)) Then
                frmEventMsgs.Result = 2
            Else
                frmEventMsgs.Header = "Are you sure ?"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Remove"
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            End If
            
            If (frmEventMsgs.Result = 2) Then
                If fgc.Rows = 2 Then
                    fgc.Rows = 0
                Else
                    fgc.RemoveItem r
                    fgc.RemoveItem r
                End If
                AdjustFGC
                Set fgc = Nothing
                mFocus False
            End If
        End If
    End If
Case 5
    frmEventMsgs.Header = "Are you sure ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Clear"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        If frLine(0).Visible Then
            r2 = fgc.Row
            If fgc.col = 0 Then r2 = r2 + 1
            For r = fgc.Row To r2
                For c = 2 To fgc.Cols - 1
                    fgc.TextMatrix(r, c) = ""
                Next
            Next
        End If
    End If
Case 6
'read only
    Dim v As Boolean
    v = lstVSC.Visible
    lstVSC.Visible = Not v
    setCmd cmd(6), Not v
Case 4, 98, 99
    Select Case Index
    Case 4
        mApply
    End Select
    setEditMode False
End Select
End Sub

Private Function CorrValue(inp As String) As String
Dim CV As String
Dim tmp As String
Dim i As Integer
Dim c As String

tmp = inp
inp = ""
For i = 1 To Len(tmp)
    c = Mid(tmp, i, 1)
    Select Case Asc(c)
    Case vbKeyReturn
        c = " "
    Case 10
        c = ""
    End Select
    inp = inp & c
Next

c = sFGC(0) & "_" & sFGC(2)
Select Case c
Case "0_2", "0_3", "0_6", "0_7", _
     "1_2", "1_3", "1_6"
        If IsNumeric(inp) Then
            CV = Format(inp, "##0.00")
            If inp > 0 Then CV = "+" & CV
            Select Case c
            Case "0_2", "1_2"
                If inp = 0 Then CV = "Plano"
            End Select
        End If
Case "0_4", _
     "1_4"
     
     For i = 1 To Len(inp)
        tmp = Mid(inp, i)
        If IsNumeric(tmp) Then
            If tmp < 0 Then
                CV = "x0"
            Else
                CV = "x" & tmp
            End If
            Exit For
        End If
     Next
End Select

If CV = "" Then
    CorrValue = inp
Else
    CorrValue = CV
End If
End Function


Private Sub cmdAdj_Click(Index As Integer)
Select Case Index
Case 0
    FGCCell.TextMatrix(1, 1) = ""
    If fgc.Index = 1 And fgc.col = 13 Then
        setCCom True
    End If
Case 1 To 4
    Dim n(1) As Single
    n(1) = cmdAdj(Index).Text
    Dim d As String
    d = FGCCell.TextMatrix(1, 1)
    If IsNumeric(d) Then
        n(0) = d
    Else
        Dim i As Integer
        Dim tmp As String
        For i = 1 To Len(d)
            tmp = Mid(d, i)
            If IsNumeric(tmp) Then
                n(0) = tmp
                Exit For
            End If
        Next
    End If
    n(0) = n(0) + n(1)
    FGCCell.TextMatrix(1, 1) = CorrValue(CStr(n(0)))
Case 5, 6
    FGCCell.TextMatrix(1, 1) = cmdAdj(Index).Text
End Select
End Sub


Private Sub cmdCCom_Click(Index As Integer)
CCom = 0

Dim i As Integer
Dim stat As Boolean
stat = Not (cmdCCom(Index).ThreeDShadowColor = &HD6CF56)
For i = 0 To 2
    If i = Index Then
        If stat Then
            txtType.Visible = True
            cmdKeyBoard_Click
        End If
        setCmd cmdCCom(i), stat
    Else
        setCmd cmdCCom(i), False
    End If
Next

frFixed.Visible = stat
If stat Then
    CCom = Index + 1
    setFixed
    frFixed.ZOrder
    frNumPad.ZOrder
End If
End Sub

Private Sub cmdCopy_Click()
Dim i As Integer
Dim IRef As Integer
Dim Idx As Integer
Dim MyIdx As Integer
Dim ANote As String
Dim ODT As String
Dim OST As String
If (CurrentEditTest >= 0) Then
    i = CurrentEditTest
    MyIdx = (CurrentEditTest / 2) + 1
    If (WearType = "PC") Then
        IRef = 1
        ODT = SelectedPCRx(MyIdx).ODManifest
        OST = SelectedPCRx(MyIdx).OSManifest
        ANote = SelectedPCRx(MyIdx).RxNote
    Else
        IRef = 2
        ODT = SelectedCLRx(MyIdx).ODManifest
        OST = SelectedCLRx(MyIdx).OSManifest
        ANote = SelectedCLRx(MyIdx).RxNote
    End If
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(ODT, OST, ANote, Idx, IRef)
End If
End Sub


Private Function getFileId(head As String, ind As Integer) As String
Dim FileId(17) As String
FileId(0) = "T07A"
FileId(1) = "T10A"
FileId(2) = "T10A_4"
FileId(3) = "T13A"
FileId(4) = "T13A_4"
FileId(5) = ""
FileId(6) = ""
FileId(7) = "T13A_5"
FileId(8) = "T13A_6"
FileId(9) = "T09A_1"
FileId(10) = "T09A_2"
FileId(11) = "T09D"
FileId(12) = "T07R"
FileId(13) = "T08A"
FileId(14) = "T07T"
FileId(15) = ""
FileId(16) = "T09G"
FileId(17) = "T07S"


Dim s(1) As Integer
If ind = 0 Then
    s(1) = 12
Else
    s(0) = 12
    s(1) = 17
End If

Dim i As Integer
For i = s(0) To s(1)
    If cmdH(i).Caption = head Then
        getFileId = FileId(i)
        Exit For
    End If
Next
End Function


Private Function getRecap(FileId As String, Cnt As Integer, c As Integer) As String
Dim FRecap As String

Select Case FileId
Case "T07A"
    FRecap = ""
Case "T08A"
    FRecap = ""
Case "T09A"
    FRecap = ""
Case "T10A"
    FRecap = ""
Case "T13A"
    Select Case c
    Case 0
        Select Case Cnt
        Case 0
            FRecap = "MANIFEST REFRACTION:"
        Case 1, 2, 3
            FRecap = "^MRX (Dry " & Cnt & ")"
        Case 4
            FRecap = "^MRX (Cyclo)"
        Case 5
            FRecap = "^MRX (Over CL)"
        Case 6
            FRecap = "^MRX (Over Glasses)"
        End Select
    Case 1
        Select Case Cnt
        Case 0
            FRecap = "%Manifest Refraction:^OD:"
        Case 1
            FRecap = "^OS:"
        End Select
    Case 5 To 11
        GoSub Common
    Case Else
        FRecap = ""
    End Select
Case "T07R"
    Select Case c
    Case 0
        Select Case Cnt
        Case 0
            FRecap = "SPEC RX:"
        Case Else
            FRecap = "^Spec Rx" & Cnt
        End Select
    Case 1
        Select Case Cnt
        Case 0
            FRecap = "^OD:"
        Case 1
            FRecap = "^OS:"
        End Select
    Case 5 To 11
        GoSub Common
    Case Else
        FRecap = ""
    End Select
Case "T07S"
    FRecap = ""
Case "T07T"
    FRecap = ""
Case "T07U"
    FRecap = ""
Case "T09D"
    FRecap = ""
End Select

getRecap = FRecap

Exit Function
Common:
Select Case c
Case 5
    FRecap = "~Va Near:~"
Case 6
    FRecap = "~ADD Reading:~"
Case 7
    FRecap = "~ADD Intermediate:~"
Case 8
    FRecap = "~Va Distance:~"
Case 9, 10
    FRecap = "~Prism:~"
Case 11
    FRecap = "~Vertex dist:~"
End Select
Return

End Function



Private Function GetTestName(FileId As String, Cnt As Integer) As String

Dim mTestName As String

Select Case FileId
Case "T07A"
    Select Case Cnt
    Case 1
        mTestName = "*Pair-1=T1859 <*Pair-1>"
    Case 2
        mTestName = "*Pair-2=T1860 <*Pair-2>"
    Case 3
        mTestName = "*Pair-3=T1861 <*Pair-3>"
    Case 4
        mTestName = "*Pair-4=T1862 <*Pair-4>"
    Case 5
        mTestName = "*Pair-5=T13136 <*Pair-5>"
    Case 6
        mTestName = "*Pair-6=T13137 <*Pair-6>"
    End Select
Case "T08A"
    Select Case Cnt
    Case 1
        mTestName = "*Pair-1=T1865 <*Pair-1>"
    Case 2
        mTestName = "*Pair-2=T1866 <*Pair-2>"
    Case 3
        mTestName = "*Pair-3=T1867 <*Pair-3>"
    Case 4
        mTestName = "*Pair-4=T1868 <*Pair-4>"
    End Select
Case "T09A"
    Select Case Cnt
    Case 1
        mTestName = "*RSY-1=T4158 <*RSY-1>"
    Case 2
        mTestName = "*RSY-2=T12114 <*RSY-2>"
    End Select
Case "T10A"
    Select Case Cnt
    Case 1
        mTestName = "*ARF-1=T4155 <*ARF-1>"
    Case 2
        mTestName = "*ARF-2=T7317 <*ARF-2>"
    Case 3
        mTestName = "*ARF-3=T7318 <*ARF-3>"
    Case 4
        mTestName = "*ARF-4=T7319 <*ARF-4>"
    End Select
Case "T13A"
    Select Case Cnt
    Case 1
        mTestName = "*MRX-1=T3965 <*MRX-1>"
    Case 2
        mTestName = "*MRX-2=T3966 <*MRX-2>"
    Case 3
        mTestName = "*MRX-3=T3967 <*MRX-3>"
    Case 4
        mTestName = "*MRX-4=T7230 <*MRX-4>"
    Case 5
        mTestName = "*MRX-5=T7345 <*MRX-5>"
    Case 6
        mTestName = "*MRX-6=T7231 <*MRX-6>"
    End Select
Case "T07R"
    Select Case Cnt
    Case 1
        mTestName = "*SpecRX1=T12273 <*SpecRX1>"
    Case 2
        mTestName = "*SpecRX2=T12274 <*SpecRX2>"
    Case 3
        mTestName = "*SpecRX3=T12275 <*SpecRX3>"
    Case 4
        mTestName = "*SpecRX4=T12276 <*SpecRX4>"
    Case 5
        mTestName = "*SpecRX5=T12277 <*SpecRX5>"
    Case 6
        mTestName = "*SpecRX5=T12291 <*SpecRX6>"
    End Select
Case "T07S"
    Select Case Cnt
    Case 1
        mTestName = "*CLRx1=T12310 <*CLRx1>"
    Case 2
        mTestName = "*CLRx2=T12311 <*CLRx2>"
    Case 3
        mTestName = "*CLRx3=T12312 <*CLRx3>"
    Case 4
        mTestName = "*CLRx4=T12313 <*CLRx4>"
    Case 5
        mTestName = "*CLRx5=T12314 <*CLRx5>"
    Case 6
        mTestName = "*CLRx6=T12331 <*CLRx6>"
    End Select
Case "T07T"
    Select Case Cnt
    Case 1
        mTestName = "*CLTrial11=T12350 <*CLTrial11>"
    Case 2
        mTestName = "*CLTrial12=T12351 <*CLTrial12>"
    Case 3
        mTestName = "*CLTrial13=T12352 <*CLTrial13>"
    Case 4
        mTestName = "*CLTrial14=T12353 <*CLTrial14>"
    Case 5
        mTestName = "*CLTrial15=T12354 <*CLTrial15>"
    Case 6
        mTestName = "*CLTrial16=T12355 <*CLTrial16>"
    End Select
Case "T07U"
    Select Case Cnt
    Case 1
        mTestName = "*CLTrial21=T12380 <*CLTrial21>"
    Case 2
        mTestName = "*CLTrial22=T12381 <*CLTrial22>"
    Case 3
        mTestName = "*CLTrial23=T12382 <*CLTrial23>"
    Case 4
        mTestName = "*CLTrial24=T12383 <*CLTrial24>"
    Case 5
        mTestName = "*CLTrial25=T12384 <*CLTrial25>"
    Case 6
        mTestName = "*CLTrial26=T12385 <*CLTrial26>"
    End Select
Case "T09G"
    Select Case Cnt
    Case 1
        mTestName = "*CLORDERTrial1=T13782 <*CLORDERTrial1>"
    Case 2
        mTestName = "*CLORDERTrial2=T13783 <*CLORDERTrial2>"
    Case 3
        mTestName = "*CLORDERTrial3=T13784 <*CLORDERTrial3>"
    Case 4
        mTestName = "*CLORDERTrial4=T13785 <*CLORDERTrial4>"
    Case 5
        mTestName = "*CLORDERTrial5=T13786 <*CLORDERTrial5>"
    Case 6
        mTestName = "*CLORDERTrial6=T13787 <*CLORDERTrial6>"
    End Select
Case "T09D"
    mTestName = "*=T0 <*>"
End Select

GetTestName = mTestName
End Function

Private Function getODOS(FileId As String, Cnt As Integer) As String

Dim mODOS As String

Select Case FileId
Case "T07A"
    If Cnt = 0 Then
        mODOS = "*OD-GL=T1575 <*OD-GL>"
    Else
        mODOS = "*OS-GL=T1576 <*OS-GL>"
    End If
Case "T08A"
    If Cnt = 0 Then
        mODOS = "*OD-ContactLens=T1582 <*OD-ContactLens>"
    Else
        mODOS = "*OS-ContactLens=T1583 <*OS-ContactLens>"
    End If
Case "T09A"
    If Cnt = 0 Then
        mODOS = "*OD-Retinoscopy=T1706 <*OD-Retinoscopy>"
    Else
        mODOS = "*OS-Retinoscopy=T1707 <*OS-Retinoscopy>"
    End If
Case "T10A"
    If Cnt = 0 Then
        mODOS = "*OD-Autorefraction=T1735 <*OD-Autorefraction>"
    Else
        mODOS = "*OS-Autorefraction=T1736 <*OS-Autorefraction>"
    End If
Case "T13A"
    If Cnt = 0 Then
        mODOS = "*OD-MR=T1247 <*OD-MR>"
    Else
        mODOS = "*OS-MR=T1248 <*OS-MR>"
    End If
Case "T07R"
    If Cnt = 0 Then
        mODOS = "*OD-GL=T12253 <*OD-GL>"
    Else
        mODOS = "*OS-GL=T12254 <*OS-GL>"
    End If
Case "T07S"
    If Cnt = 0 Then
        mODOS = "*OD-CLRx=T12296 <*OD-CLRx>"
    Else
        mODOS = "*OS-CLRx=T12297 <*OS-CLRx>"
    End If
Case "T07T"
    If Cnt = 0 Then
        mODOS = "*OD-CLTrial1=T12337 <*OD-CLTrial1>"
    Else
        mODOS = "*OS-CLTrial1=T12338 <*OS-CLTrial1>"
    End If
Case "T07U"
    If Cnt = 0 Then
        mODOS = "*OD-CLTrial2=T12367 <*OD-CLTrial2>"
    Else
        mODOS = "*OS-CLTrial2=T12368 <*OS-CLTrial2>"
    End If
Case "T09G"
    If Cnt = 0 Then
        mODOS = "*OD-CLORDERTrial=T13768 <*OD-CLORDERTrial>"
    Else
        mODOS = "*OS-CLORDERTrial=T13769 <*OS-CLORDERTrial>"
    End If
Case "T09D"
    If Cnt = 0 Then
        mODOS = "*OD-DilatedRefract=T12131 <*OD-DilatedRefract>"
    Else
        mODOS = "*OS-DilatedRefract=T12132 <*OS-DilatedRefract>"
    End If
End Select

getODOS = mODOS
End Function

Private Function getQuestion(FileId As String) As String

Select Case FileId
Case "T07A"
    getQuestion = "GLASSES"
Case "T08A"
    getQuestion = "CONTACT LENSES"
Case "T09A"
    getQuestion = "RETINOSCOPY"
Case "T10A"
    getQuestion = "AUTOREFRACTION"
Case "T13A"
    getQuestion = "MANIFEST REFRACTION"
Case "T07R"
    getQuestion = "SPEC RX"
Case "T07S"
    getQuestion = "CL RX"
Case "T07T"
    getQuestion = "CL TRIAL 1"
Case "T07U"
    getQuestion = "CL TRIAL 2"
Case "T09G"
    getQuestion = "CL ORDER TRIAL"
Case "T09D"
    getQuestion = "DILATED REFRACTION"
End Select

End Function

Private Function getBox(FileId As String, g As Integer, c As Integer) As String

Dim fBox As String

Select Case FileId
Case "T07A"
    Select Case c
    Case 2
        fBox = "*Sphere-Glasses=T1577"
    Case 3
        fBox = "*Cylinder-Glasses=T1578"
    Case 4
        fBox = "*Axis-Glasses=T1579"
    Case 5
        fBox = "*VAD-Glasses=T13071"
    Case 6
        fBox = "*ADD-Reading-Glasses=T1588"
    Case 7
        fBox = "*ADD-Intermediate-Glasses=T3905"
    Case 8
        fBox = "*VAN-Glasses=T13072"
    Case 9
        fBox = "*PrismOfAngle1-Glasses=T1803"
    Case 10
        fBox = "*PrismOfAngle2-Glasses=T1838"
    Case 11
        fBox = "*VertexDistance-Glasses=T5163"
    Case 12
        fBox = "*Type-Glasses=T13073"
    Case 13
        fBox = "*Comment-Glasses=T13074"
    End Select
Case "T08A"
    Select Case c
    Case 2
        fBox = "*Sphere-ContactLens=T1584"
    Case 3
        fBox = "*Cylinder-ContactLens=T1585"
    Case 4
        fBox = "*Axis-ContactLens=T1586"
    Case 5
        fBox = "*VAD-ContactLens=T13145"
    Case 6
        fBox = "*ADD-Reading-ContactLens=T1589"
    Case 7
        fBox = "*VAN-ContactLens=T13144"
    Case 8
        fBox = "*Base Curve-ContactLens=T1672"
    Case 9
        fBox = "*Diameter-ContactLens=T1711"
    Case 10
        fBox = "*Periph Curve-ContactLens=T4253"
    Case 11
        fBox = "*DN-ContactLens=T13147"
    Case 12
        fBox = "*Model-ContactLens=T13143"
    Case 13
        fBox = "*Comment-ContactLens=T13142"
    End Select
Case "T09A"
    Select Case c
    Case 2
        fBox = "*Sphere-Retinoscopy=T1708"
    Case 3
        fBox = "*Cylinder-Retinoscopy=T1709"
    Case 4
        fBox = "*Axis-Retinoscopy=T1710"
    Case 5
        fBox = "*VAD-MR=T13082"
    Case 8
        fBox = "*VAN-Retinoscopy=T13083"
    Case 13
        fBox = "*Comment-Retinoscopy=T13085"
    End Select
Case "T10A"
    Select Case c
    Case 2
        fBox = "*Sphere-Autorefraction=T1737"
    Case 3
        fBox = "*Cylinder-Autorefraction=T1738"
    Case 4
        fBox = "*Axis-Autorefraction=T1739"
    Case 5
        fBox = "*Va-Autorefraction=T11306"
    Case 8
        fBox = "*VAN-Autorefraction=T13076"
    Case 9
        fBox = "*Reliability-Autorefraction=T11250"
    Case 10
        fBox = "*PD-Autorefraction=T8081"
    Case 13
        fBox = "*Comment-Autorefraction=T13077"
    End Select
Case "T13A"
    Select Case c
    Case 2
        fBox = "*Sphere-Manifest Refraction=T1250"
    Case 3
        fBox = "*Cylinder-Manifest Refraction=T1254"
    Case 4
        fBox = "*Axis-Manifest Refraction=T1257"
    Case 5
        fBox = "*VAD-MR=T13081"
    Case 6
        fBox = "*ADD-Reading=T3969"
    Case 7
        fBox = "*ADD-Intermediate=T4224"
    Case 8
        fBox = "*VAN-MR=T13080"
    Case 9
        fBox = "*PrismofAngle1-MR=T1839"
    Case 10
        fBox = "*PrismofAngle2-MR=T1844"
    Case 11
        fBox = "*Vertex-MR=T5162"
    Case 12
        fBox = "*Type-MR=T13079"
    Case 13
        fBox = "*Comment-MR=T13078"
    End Select
Case "T07R"
    Select Case c
    Case 2
        fBox = "*Sphere-specrx=T12254"
    Case 3
        fBox = "*Cylinder-specrx=T12255"
    Case 4
        fBox = "*Axis-specrx=T12256"
    Case 5
        fBox = "*VAD-specrx=T12292"
    Case 6
        fBox = "*ADD-Read-specrx=T12257"
    Case 7
        fBox = "*ADD-Inter-specrx=T12258"
    Case 8
        fBox = "*VAN-specrx=T12293"
    Case 9
        fBox = "*POA1-specrx=T12259"
    Case 10
        fBox = "*POA2-specrx=T12260"
    Case 11
        fBox = "*Vertex-specrx=T12283"
    Case 12
        fBox = "*Type-specrx=T12294"
    Case 13
        fBox = "*Comment-specrx=T13095"
    End Select
Case "T07S"
    fBox = ""
    Select Case c
    Case 2
        fBox = "*Sphere-CLRx=T12298"
    Case 3
        fBox = "*Cylinder-CLRx=T12299"
    Case 4
        fBox = "*Axis-CLRx=T12300"
    Case 5
        fBox = "*VAD-CLRx=T12332"
    Case 6
        fBox = "*ADD-Reading-CLRx=T12301"
    Case 7
        fBox = "*VAN-CLRx=T12333"
    Case 8
        fBox = "*Base Curve-CLRx=T12302"
    Case 9
        fBox = "*Diameter-CLRx=T12303"
    Case 10
        fBox = "*Periph Curve-CLRx=T12304"
    Case 11
        fBox = "*DN-CLRx=T12334"
    Case 12
        fBox = "*Model-CLRx=T12335"
    Case 13
        fBox = "*Comment-CLRx=T13096"
    End Select
Case "T07T"
    Select Case c
    Case 2
        fBox = "*Sphere-CLTrial1=T12339"
    Case 3
        fBox = "*Cylinder-CLTrial1=T12340"
    Case 4
        fBox = "*Axis-CLTrial1=T12341"
    Case 5
        fBox = "*VAD-CLTrial1=T12346"
    Case 6
        fBox = "*ADD-Reading-CLTrial1=T12342"
    Case 7
        fBox = "*VAN-CLTrial1=T12347"
    Case 8
        fBox = "*Base Curve-CLTrial1=T12343"
    Case 9
        fBox = "*Diameter-CLTrial1=T12344"
    Case 10
        fBox = "*Periph Curve-CLTrial1=T12345"
    Case 11
        fBox = "*DN-CLTrial1=T12348"
    Case 12
        fBox = "*Model-CLTrial1=T12349"
    Case 13
        fBox = "*Comment-CLTrial1=T13097"
    End Select
Case "T07U"
    Select Case c
    Case 2
        fBox = "*Sphere-CLTrial2=T12369"
    Case 3
        fBox = "*Cylinder-CLTrial2=T12370"
    Case 4
        fBox = "*Axis-CLTrial2=T12371"
    Case 5
        fBox = "*VAD-CLTrial2=T12376"
    Case 6
        fBox = "*ADD-Reading-CLTrial2=T12372"
    Case 7
        fBox = "*VAN-CLTrial2=T12377"
    Case 8
        fBox = "*Base Curve-CLTrial2=T12373"
    Case 9
        fBox = "*Diameter-CLTrial2=T12374"
    Case 10
        fBox = "*Periph Curve-CLTrial2=T12375"
    Case 11
        fBox = "*DN-CLTrial2=T12378"
    Case 12
        fBox = "*Model-CLTrial2=T12379"
    Case 13
        fBox = "*Comment-CLTrial2=T13098"
    End Select
Case "T09G"
    Select Case c
    Case 2
        fBox = "*Sphere-CLORDERTrial=T13770"
    Case 3
        fBox = "*Cylinder-CLORDERTrial=T13771"
    Case 4
        fBox = "*Axis-CLORDERTrial=T13772"
    Case 5
        fBox = "*VAD-CLORDERTrial=T13777"
    Case 6
        fBox = "*ADD-Reading-CLORDERTrial=T13773"
    Case 7
        fBox = "*VAN-CLORDERTrial=T13778"
    Case 8
        fBox = "*Base Curve-CLORDERTrial=T13774"
    Case 9
        fBox = "*Diameter-CLORDERTrial=T13775"
    Case 10
        fBox = "*Periph Curve-CLORDERTrial=T13776"
    Case 11
        fBox = "*DN-CLORDERTrial=T13779"
    Case 12
        fBox = "*Model-CLORDERTrial=T13780"
    Case 13
        fBox = "*Comment-CLORDERTrial=T13781"
    End Select
Case "T09D"
    Select Case c
    Case 2
        fBox = "*Sphere-DilatedRefract=T12133"
    Case 3
        fBox = "*Cylinder-DilatedRefract=T12134"
    Case 4
        fBox = "*Axis-DilatedRefract=T12135"
    Case 5
        fBox = "*VAD-DilatedRefract=T13091"
    Case 6
        fBox = "*ADD-Reading-DilatedRefract=T13086"
    Case 7
        fBox = "*ADD-Intermediate-DilatedRefract=T13087"
    Case 8
        fBox = "*VAN-DilatedRefract=T13092"
    Case 9
        fBox = "*PrismofAngle1-DilatedRefract=T13088"
    Case 10
        fBox = "*PrismofAngle2-DilatedRefract=T13089"
    Case 11
        fBox = "*Vertex-DilatedRefract=T13090"
    Case 12
        fBox = "*Type-DilatedRefract=T13093"
    Case 13
        fBox = "*Comment-DilatedRefract=T13094"
    End Select
End Select

getBox = fBox
End Function

Private Sub cmdDone_Click()
Dim FileId(2) As String
Dim Cnt As Integer
Dim g As Integer
Dim r As Integer
Dim FileBase As String
Dim FileNum As Integer
Dim rc As Integer

KillFiles

For g = 0 To 1
    If g = 1 Then FileId(0) = ""
    For r = 0 To lstRxs(g).Rows Step 2
    
    
        If Not r < lstRxs(g).Rows - 1 Then
            GoSub finFile
            Exit For
        End If
        
        If IsDate(lstRxs(g).TextMatrix(r + 1, 0)) Then
            GoTo NextItem
        End If
        
        FileId(2) = getFileId(lstRxs(g).TextMatrix(r, 0), g)
        FileId(1) = Left(FileId(2), 4)
        
        If Len(FileId(2)) = 4 Then
            If FileId(0) = FileId(1) Then
                Cnt = Cnt + 1
                If Cnt > 6 Then
                    Cnt = Cnt - 6
                    FileId(1) = "T07U"
                    FileId(2) = "T07U"
                End If
            Else
                Cnt = 1
            End If
        Else
            Cnt = Right(FileId(2), 1)
        End If
        
        If Not FileId(0) = FileId(1) Then
            GoSub finFile
            ReDim Recap(0)
            FileId(0) = FileId(1)
            FileBase = DoctorInterfaceDirectory + FileId(1) + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId))  '+ ".txt"
            FileNum = FreeFile
            ScratchTestFile = FileBase & ".tmp"
            FM.OpenFile ScratchTestFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(FileNum)
            Print #FileNum, "QUESTION=" + "/"; getQuestion(FileId(1))
        End If
        Call WriteScratchTestFile(FileNum, g, r, FileId(1), Cnt)
NextItem:
    Next
Next

Call frmSystems1.ClearAllTestResults
Call frmSystems1.DisplayTestResults(True)
Call frmSystems1.ViewTestResults(1)
Unload Me
FormOn = False

'Call frmEvaluation.LoadFromRefractFile(True)
Call PostLogEntires
If EditPreviousVisit Then
    WriteRxToDataBase
End If
Exit Sub

finFile:
If Not ScratchTestFile = "" Then
    
'''''    For rc = 1 To UBound(Recap)
'''''        Print #FileNum, "Recap=" & Recap(rc) & "~"
'''''    Next
    'Print #FileNum, "Recap=Not displayable~"
    FM.CloseFile CLng(FileNum)
    StoreResults = FileBase & ".txt"
    FM.FileCopy ScratchTestFile, StoreResults
    FM.Kill ScratchTestFile
    ScratchTestFile = ""
    If (frmHome.BackUpOn) Then
        Dim i As Integer
        Dim OtherFile As String
        i = FM.InFileNameStartStr(StoreResults, "T")
        If (i > 0) Then
            OtherFile = Left(StoreResults, i - 1) + "Backup\" + Mid(StoreResults, i, Len(StoreResults) - i + 1)
            FM.FileCopy StoreResults, OtherFile
        End If
    End If
    PushList StoreResults
End If
Return
End Sub


Private Sub WriteRxToDataBase()
Dim q As Integer
Dim j As Integer
Dim i As Integer
Dim EN As Integer
Dim comm(18, 1) As String
Dim CN As Integer
Dim c As Integer
Dim ClinicalLoc As New PatientClinical
Dim recCntr As Integer

'delete "a" records
ClinicalLoc.PatientId = PatientId
ClinicalLoc.AppointmentId = AppointmentId
ClinicalLoc.ClinicalType = "A"
ClinicalLoc.Symptom = ""
ClinicalLoc.Findings = ""
ClinicalLoc.ImageDescriptor = ""
ClinicalLoc.ImageInstructions = ""
ClinicalLoc.EyeContext = ""
q = ClinicalLoc.FindPatientClinical
If (q > 0) Then
    For j = q To 1 Step -1
        i = 0
        If (ClinicalLoc.SelectPatientClinical(j)) Then
            i = i + InStrPS(1, ClinicalLoc.Findings, "DISPENSE SPECTACLE RX")
            i = i + InStrPS(1, ClinicalLoc.Findings, "DISPENSE CL RX")
            i = i + InStrPS(1, ClinicalLoc.Findings, "ORDER TRIAL CL")
            If i > 0 Then
                Call ClinicalLoc.KillPatientClinical
            End If
        End If
    Next j
End If

'write "a" records

Dim segment() As String
ReDim segment(0) As String
Dim RetCLInv As New CLInventory
Dim s As Integer
'------------------------------------------------------------------------------
'read T07R SpecRx
Dim Temp As String
Dim TheFile As String
Dim FileNum As Integer
Dim Rec As String
Dim ImageDesc As String

ImageDesc = "^"
EN = 0
TheFile = GetTestFileName("07R")
If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    Do Until EOF(FileNum)
        Line Input #FileNum, Rec
        Rec = Trim(Rec)
        Temp = UCase(Rec)
        
        If Mid(Rec, 1, 6) = "Recap=" Then
            Select Case True
            Case Mid(Temp, 1, 13) = "RECAP=SPEC RX"
                If IsNumeric(Mid(Rec, 14, 1)) Then
                    i = Mid(Rec, 14, 1)
                    If i > 1 Then GoSub getSpecCLRx
                    ReDim segment(7) As String
                    segment(0) = "Dispense Spectacle Rx-9/"
                Else
                    s = 2
                End If
            Case Mid(Temp, 1, 13) = "RECAP=*OS-GL~"
                s = 3
            Case Mid(Temp, 1, 11) = "RECAP=TYPE:"
                segment(1) = Mid(Rec, 12, Len(Rec) - 12)
            Case Else
                Temp = Mid(Rec, 7, Len(Rec) - 7)
                If Len(Temp) > 1 Then
                    If UCase(Left(Temp, 2)) = "XX" Then Temp = Mid(Temp, 2)
                End If
                segment(s) = segment(s) & " " & Temp
            End Select
        Else
            Select Case True
            Case UCase(Mid(Rec, 1, 4)) = "TIME"
                CN = CN + 1
            Case UCase(Mid(Rec, 1, 4)) = "*OD-"
                c = 0
            Case UCase(Mid(Rec, 1, 4)) = "*OS-"
                c = 1
            Case UCase(Mid(Rec, 1, 8)) = "*COMMENT"
                GoSub GetComm
            End Select
        End If
        If (EOF(FileNum)) Then GoSub getSpecCLRx
    Loop
    FM.CloseFile CLng(FileNum)
    KillFile TheFile
End If
'------------------------------------------------------------------------------
'read T07S CLRx
ImageDesc = "^"
TheFile = GetTestFileName("07S")
If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    Do Until EOF(FileNum)
        Line Input #FileNum, Rec
        Rec = Trim(Rec)
        Temp = UCase(Rec)
        
        If Mid(Rec, 1, 6) = "Recap=" Then
            Select Case True
            Case Mid(Temp, 1, 10) = "RECAP=CLRX"
                i = Mid(Rec, 11, 1)
                If i > 1 Then GoSub getSpecCLRx
                ReDim segment(7) As String
                segment(0) = "Dispense CL Rx-9/"
            Case Mid(Temp, 1, 17) = "RECAP=CL RX:^OD:~"
                s = 2
            Case Mid(Temp, 1, 11) = "RECAP=^OS:~"
                s = 3
            Case Mid(Temp, 1, 12) = "RECAP=MODEL:"
                segment(s + 3) = RetCLInv.GetCLInventoryIdGeneric(Mid(Rec, 13, Len(Rec) - 13))
            Case Else
                Temp = Mid(Rec, 7, Len(Rec) - 7)
                If Len(Temp) > 1 Then
                    If UCase(Left(Temp, 2)) = "XX" Then Temp = Mid(Temp, 2)
                End If
                segment(s) = segment(s) & " " & Temp
            End Select
        Else
            Select Case True
            Case UCase(Mid(Rec, 1, 4)) = "TIME"
                CN = CN + 1
            Case UCase(Mid(Rec, 1, 4)) = "*OD-"
                c = 0
            Case UCase(Mid(Rec, 1, 4)) = "*OS-"
                c = 1
            Case UCase(Mid(Rec, 1, 8)) = "*COMMENT"
                GoSub GetComm
            End Select
        End If
        If (EOF(FileNum)) Then GoSub getSpecCLRx
    Loop
    FM.CloseFile CLng(FileNum)
    KillFile TheFile
End If
'------------------------------------------------------------------------------
'read T09G  orderTrial
ImageDesc = "~"
TheFile = GetTestFileName("09G")
If (FM.IsFileThere(TheFile)) Then
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    Do Until EOF(FileNum)
        Line Input #FileNum, Rec
        Rec = Trim(Rec)
        Temp = UCase(Rec)
        
        If Mid(Rec, 1, 6) = "Recap=" Then
            Select Case True
            Case Mid(Temp, 1, 18) = "RECAP=CLORDERTRIAL"
                i = Mid(Rec, 19, 1)
                If i > 1 Then GoSub getSpecCLRx
                ReDim segment(7) As String
                segment(0) = "Order Trial CL-9/"
            Case Mid(Temp, 1, 26) = "RECAP=CL ORDER TRIAL:^OD:~"
                s = 2
            Case Mid(Temp, 1, 11) = "RECAP=^OS:~"
                s = 3
            Case Mid(Temp, 1, 12) = "RECAP=MODEL:"
                segment(s + 3) = RetCLInv.GetCLInventoryIdGeneric(Mid(Rec, 13, Len(Rec) - 13))
            Case Else
                Temp = Mid(Rec, 7, Len(Rec) - 7)
                If Len(Temp) > 1 Then
                    If UCase(Left(Temp, 2)) = "XX" Then Temp = Mid(Temp, 2)
                End If
                segment(s) = segment(s) & " " & Temp
            End Select
        Else
            Select Case True
            Case UCase(Mid(Rec, 1, 4)) = "TIME"
                CN = CN + 1
            Case UCase(Mid(Rec, 1, 4)) = "*OD-"
                c = 0
            Case UCase(Mid(Rec, 1, 4)) = "*OS-"
                c = 1
            Case UCase(Mid(Rec, 1, 8)) = "*COMMENT"
                GoSub GetComm
            End Select
        End If
        If (EOF(FileNum)) Then GoSub getSpecCLRx
    Loop
    FM.CloseFile CLng(FileNum)
    KillFile TheFile
End If
Set ClinicalLoc = Nothing
Set RetCLInv = Nothing
'------------------------------------------------------------------------------
Exit Sub
'==============================================================================
getSpecCLRx:
EN = EN + 1
If UBound(segment) > 0 Then
    Temp = segment(0)
    For s = 1 To 7
        Temp = Temp & "-" & s & "/" & Trim(segment(s))
        Select Case s
        Case 2, 3
            If ImageDesc = "" Then
                If Not comm(EN, s - 2) = "" Then
                    Temp = Temp & " Comment:" & comm(EN, s - 2)
                End If
            End If
        End Select
    Next
    GoSub WriteRec
End If
ReDim segment(0) As String
Temp = ""
Return
'=============================================
WriteRec:
ClinicalLoc.ClinicalId = 0
If (ClinicalLoc.RetrievePatientClinical) Then
    recCntr = recCntr + 1
    ClinicalLoc.AppointmentId = AppointmentId
    ClinicalLoc.PatientId = PatientId
    ClinicalLoc.ClinicalType = "A"
    ClinicalLoc.EyeContext = ""
    ClinicalLoc.Symptom = recCntr
    'Debug.Print Len(Temp)
    ClinicalLoc.Findings = Trim(Temp)
    Temp = ImageDesc
    If comm(EN, 0) = "" And comm(EN, 1) = "" Then
        Temp = ""
    Else
        Temp = Temp & "OD:" & comm(EN, 0) & "OS:" & comm(EN, 1)
    End If
    ClinicalLoc.ImageDescriptor = Temp
    ClinicalLoc.ImageInstructions = ""
    ClinicalLoc.ClinicalFollowup = ""
    If Not (ClinicalLoc.ApplyPatientClinical) Then
        Call PinpointError("DI-Submit", Err.Source + " " + Err.Description + " Plan " + Trim(Temp) + " Not Posted for ApptId = " + Trim(str(AppointmentId)))
    End If
End If

Return
'=============================================
GetComm:
Temp = Mid(Rec, 1, Len(Rec) - 1)
Do Until Temp = ""
    If Left(Temp, 1) = "<" Then
        Temp = Mid(Temp, 2)
        Exit Do
    End If
    Temp = Mid(Temp, 2)
Loop

comm(CN, c) = Trim(Temp)
Return

End Sub

Private Sub PushList(FileName As String)
On Error GoTo UI_ErrorHandler
Dim TheRecord As String
Dim ODOn As Boolean
Dim DoRecapCheck As Boolean
Dim z1 As Integer, z2 As Integer
Dim w As Integer, s As Integer
Dim q As Integer, r As Integer
Dim a As Integer, k As Integer
Dim Y As Integer, z As Integer
Dim FileNum As Integer
Dim MultiTestIndicator As String
Dim PadValue As String
Dim Question As String
Dim TheValue As String
Dim TestTime As String
Dim LocalText As String
Dim LocalQuestion As String
Dim LocalTestName As String
Dim UTemp As String
Dim Recap(200) As String
Dim PadOrderResult(200) As String
Dim CurrentLevel As Integer
Dim Level1 As Integer
Dim TestNameLoc As Integer
Dim TestWideName As String
Dim WhereTestNameIs As Integer
Dim ControlLevel1 As DynamicControls
Level1 = 0
FileNum = 0
WhereTestNameIs = 0
Erase Recap
LocalTestName = ""
TestTime = ""
Erase PadOrderResult
ODOn = False
DoRecapCheck = True
FileNum = FreeFile
FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
Do Until (EOF(FileNum))
    Line Input #FileNum, TheRecord
    If (InStrPS(TheRecord, "QUESTION=") <> 0) Then
        Question = Trim(Mid(TheRecord, 10, Len(TheRecord) - 9))
        If (UCase(Trim(Question)) = "/GENERAL MEDICAL OBSERVATION") Then
            DoRecapCheck = False
        End If
        If (UCase(Trim(Question)) = "/GENERAL IMPRESSIONS") Then
            DoRecapCheck = False
        End If
        If (UCase(Trim(Question)) = "/BLOOD COUNT") Then
            DoRecapCheck = False
        End If
    ElseIf (InStrPS(TheRecord, "TIME=") <> 0) Then
        TestTime = Trim(Mid(TheRecord, 6, Len(TheRecord) - 5))
    Else
        r = InStrPS(TheRecord, "=")
        If (r = 0) Then
            r = Len(TheRecord) + 1
        End If
        r = r - 1
        w = r + 3
        Y = InStrPS(w, TheRecord, " ")
        If (Y = 0) Then
            Y = Len(TheRecord) + 1
        End If
        If (Val(Trim(Mid(TheRecord, w, Y - w))) > 0) Then
            Set ControlLevel1 = New DynamicControls
            ControlLevel1.ControlId = Val(Trim(Mid(TheRecord, w, Y - w)))
            ControlLevel1.ControlName = ""
            ControlLevel1.IEChoiceName = ""
            If (ControlLevel1.RetrieveControl) Then
                MultiTestIndicator = ""
                PadValue = ""
                TheValue = ""
                If (ControlLevel1.ControlType = "N") Or (ControlLevel1.ControlType = "S") Then
                    Y = InStrPS(w, TheRecord, "<")
                    If (Y <> 0) Then
                        z = InStrPS(w, TheRecord, ">")
                        If (z <> 0) Then
                            PadValue = "@" + Mid(TheRecord, Y + 1, (z - 1) - Y)
                        End If
                    End If
                ElseIf (ControlLevel1.ControlType = "R") Then
                    MultiTestIndicator = ""
                End If
                Level1 = Level1 + 1
                If (Trim(ControlLevel1.DoctorLingo) <> "") Then
                    If (ControlLevel1.DecimalDefault = "T") And (ControlLevel1.ControlType = "N") Then
                        PadOrderResult(Level1) = Mid(PadValue, 2, Len(PadValue) - 1)
                        PadValue = "@##"
                    End If
                    Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.DoctorLingo) + " " + PadValue
                    Call GetPadValue(Recap(Level1), TheValue)
                    Call SetPadValue(Recap(Level1), TheValue)
                    s = InStrPS(Recap(Level1), "/Time/")
                    If (s > 0) Then
                        Recap(Level1) = Left(Recap(Level1), s) + TestTime + Mid(Recap(Level1), s + 5, Len(Recap(Level1)) - (s + 4))
                    End If
                    s = InStrPS(Recap(Level1), ":^OD")
                    z1 = InStrPS(Recap(Level1), ":/")
                    z2 = InStrPS(Recap(Level1), ":^")
                    If (s <> 0) Then
                        WhereTestNameIs = Level1
                        LocalTestName = Trim(Left(Recap(Level1), s - 1))
                        Call ReplaceCharacters(LocalTestName, "%", " ")
                        LocalTestName = Trim(LocalTestName)
                        ODOn = True
                    ElseIf (z1 <> 0) Then
                        WhereTestNameIs = Level1
                        LocalTestName = Trim(Left(Recap(Level1), z1 - 1))
                        s = InStrPS(Recap(Level1), "^OD")
                        If (s <> 0) Then
                            ODOn = True
                        End If
                    ElseIf (z2 <> 0) Then
                        If (Not (DoRecapCheck)) Then
                            WhereTestNameIs = Level1
                            LocalTestName = Trim(Left(Recap(Level1), z2 - 1))
                            ODOn = True
                        End If
                    Else
                        If (Not DoRecapCheck) Then
                            If (Trim(LocalTestName) = "") And (WhereTestNameIs = 0) Then
                                WhereTestNameIs = 1
                                LocalTestName = Trim(Mid(Question, 2, Len(Question) - 1)) + ":"
                            End If
                        End If
                    End If
                Else
                    Recap(Level1) = "-"
                End If
                If (DoRecapCheck) Then
                    If (Trim(LocalTestName) = "") Then
                        LocalTestName = Mid(Question, 2, Len(Question) - 1)
                        UTemp = LocalTestName
                        Call StripCharacters(UTemp, " ")
                        If (InStrPS(UCase(Recap(Level1)), UCase(LocalTestName)) = 0) And (InStrPS(UCase(Recap(Level1)), UCase(UTemp)) = 0) Then
                            Recap(Level1) = LocalTestName + ":" + Recap(Level1)
                            WhereTestNameIs = Level1
                        End If
                    End If
                    s = InStrPS(UCase(Recap(Level1)), UCase(LocalTestName))
                    If (s <> 0) And (Trim(LocalTestName) <> "") Then
                        s = InStrPS(Recap(Level1), "^OS")
                        If (s <> 0) And (ODOn) Then
                            Recap(Level1) = Mid(Recap(Level1), s, Len(Recap(Level1)) - (s - 1))
                        Else
'                            If (WhereTestNameIs > 0) And (WhereTestNameIs <> Level1) Then
'                                s = InStrPS(Recap(WhereTestNameIs), ":")
'                                If (s <> 0) Then
'                                    TestWideName = Trim(Recap(Level1))
'                                    k = InStrPS(Recap(Level1), ":")
'                                    If (k <> 0) Then
'                                        TestWideName = Trim(Mid(Recap(Level1), s + 1, Len(Recap(Level1)) - s))
'                                    End If
'                                    If (InStrPS(Recap(WhereTestNameIs), TestWideName) = 0) Then
'                                        Recap(WhereTestNameIs) = _
'                                        Left(Recap(WhereTestNameIs), s) + _
'                                        TestWideName + _
'                                        Mid(Recap(WhereTestNameIs), s + 1, Len(Recap(WhereTestNameIs)) - s)
'                                    End If
'                                    Recap(Level1) = "-"
'                                End If
'                            End If
                        End If
                    End If
                Else
                    s = InStrPS(UCase(Recap(WhereTestNameIs)), UCase(LocalTestName))
                    If (s = 0) Then
                        Recap(Level1) = LocalTestName + Recap(Level1)
                    End If
                End If
            End If
            Set ControlLevel1 = Nothing
        End If
    End If
Loop
FM.CloseFile CLng(FileNum)

' Build Recap Values
FM.OpenFile FileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
For s = 1 To Level1
    If (Trim(Recap(s)) <> "") And (Trim(Recap(s)) <> "-") Then
' Put in any switch for PadOrderResults here
        If (Trim(PadOrderResult(s)) <> "") Then
            z = s
            Call StripCharacters(Recap(s), "#")
            Print #FileNum, "Recap=" + Recap(s)
            s = s + 1
            Print #FileNum, "Recap=" + Recap(s)
            Print #FileNum, "Recap=" + PadOrderResult(z)
        Else
            Print #FileNum, "Recap=" + Recap(s)
        End If
    End If
Next s
FM.CloseFile CLng(FileNum)
Exit Sub
UI_ErrorHandler:
    If (FileNum <> 0) Then
        FM.CloseFile CLng(FileNum)
    End If
    Set ControlLevel1 = Nothing
    Call PinpointError("frmAssignWearNew.PushList", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume LeaveFast
LeaveFast:
End Sub


Private Sub KillFiles()
On Error Resume Next
    FM.Kill GetTestFileName("07A")
    FM.Kill GetTestFileName("07A")
    FM.Kill GetTestFileName("10A")
    FM.Kill GetTestFileName("13A")
    FM.Kill GetTestFileName("09A")
    FM.Kill GetTestFileName("09D")
    FM.Kill GetTestFileName("07R")
    FM.Kill GetTestFileName("08A")
    FM.Kill GetTestFileName("07T")
    FM.Kill GetTestFileName("07S")
    FM.Kill GetTestFileName("07U")
    FM.Kill GetTestFileName("09G")
End Sub



Private Sub WriteScratchTestFile(FileNum As Integer, g As Integer, r As Integer, FileId As String, Cnt As Integer)
Dim i As Integer
Dim c As Integer
Dim mBox As String
Dim mType As String

Print #FileNum, "TIME=" + Format(Now, "Hh:Mm AM/PM") + " (" + UserLogin.sNameShort + ")"
Print #FileNum, GetTestName(FileId, Cnt)

For i = r To r + 1
    Print #FileNum, getODOS(FileId, i - r)
    
    For c = 2 To lstRxs(g).Cols - 1
        If Not lstRxs(g).TextMatrix(i, c) = "" Then
            mBox = getBox(FileId, g, c)
            If Not mBox = "" Then
                Print #FileNum, mBox & " <" & lstRxs(g).TextMatrix(i, c) & ">"
            End If
        End If
    Next
Next
End Sub

Private Sub cmdEdit_Click()
Dim RId As Integer
If (SelectedTest > 0) Then
    RId = 0
    If (SelectedTest < 6) Then
' it has to match the R Test Configuration
        If (SelectedTest = 1) Then
            RId = 1
        ElseIf (SelectedTest = 2) Then
            RId = 4
        ElseIf (SelectedTest = 3) Then
            RId = 2
        ElseIf (SelectedTest = 4) Then
            RId = 5
        ElseIf (SelectedTest = 5) Then
            RId = 6
        End If
    End If
    Call DoOriginalTest(MyId(SelectedTest), MyQuestion(SelectedTest), RId)
End If
End Sub


Private Sub cmdH_Click(Index As Integer)
Dim ODOS(1) As String
Dim noComm As Boolean

CanLog = False
If CopyMode Then
    
'        sFGC(0) = mfgc.Index
'        sFGC(1) = mfgc.Row
'        sFGC(2) = mfgc.col
'
    Dim mFGC As MSFlexGrid
    Set mFGC = lstRxs(sFGC(0))
    
    Dim e As Integer
    Dim g(1) As Integer
    Dim r As Integer
    Dim c As Integer
    Dim i As Integer
    
    r = Fix(mFGC.Row / 2) * 2
    If mFGC.col = 0 Then
        g(0) = 0
        g(1) = 1
    Else
        g(0) = mFGC.Row - r
        g(1) = g(0)
    End If
    
    If Index > 12 Then i = 1
    
    Select Case Index
    Case 12, 17
        noComm = True
    End Select
    
    For e = g(0) To g(1)
        For c = 2 To mFGC.Cols - 1
            If c = 13 And noComm Then
                ODOS(e) = ODOS(e) & vbTab & ""
            Else
                If mFGC.Index = i _
                Or mFGC.Index - 2 = i Then
                    ODOS(e) = ODOS(e) & vbTab & mFGC.TextMatrix(r + e, c)
                Else
                    Select Case c
                    Case Is < 7
                        ODOS(e) = ODOS(e) & vbTab & mFGC.TextMatrix(r + e, c)
                    Case 7
                        If i = (0 Or 2) Then
                            ODOS(e) = vbTab & ODOS(e) & vbTab & mFGC.TextMatrix(r + e, 7)
                        Else
                            ODOS(e) = ODOS(e) & vbTab & mFGC.TextMatrix(r + e, 8) & vbTab
                        End If
                    Case 8
                    Case Else
                        ODOS(e) = ODOS(e) & vbTab
                    End Select
                End If
            End If
        Next
    Next
    CopyMode = False
    CanLog = True
End If

If InsertRow(Index, ODOS(0), ODOS(1)) Then
    ClearFields fgc.Row
    If CanLog Then Call InsertLog("frmAssignWearNew.frm", "Refract Brought Forward for Patient " & globalExam.iPatientId, LoginCatg.BringFindingsForwardCopy, "", "", 0, globalExam.iAppointmentId)
End If

End Sub

Private Sub ClearFields(r As Integer)
r = Fix(r / 2) * 2
'clean non-editable fields
Select Case fgc.TextMatrix(r, 0)
Case cmdH(1).Caption, cmdH(2).Caption
    fgc.TextMatrix(r, 5) = ""
    fgc.TextMatrix(r, 6) = ""
    fgc.TextMatrix(r, 7) = ""
    fgc.TextMatrix(r, 8) = ""
    fgc.TextMatrix(r, 11) = ""
    fgc.TextMatrix(r, 12) = ""
    fgc.TextMatrix(r + 1, 5) = ""
    fgc.TextMatrix(r + 1, 6) = ""
    fgc.TextMatrix(r + 1, 7) = ""
    fgc.TextMatrix(r + 1, 8) = ""
    fgc.TextMatrix(r + 1, 11) = ""
    fgc.TextMatrix(r + 1, 12) = ""
Case cmdH(9).Caption, cmdH(10).Caption
    fgc.TextMatrix(r, 6) = ""
    fgc.TextMatrix(r, 7) = ""
    fgc.TextMatrix(r, 9) = ""
    fgc.TextMatrix(r, 10) = ""
    fgc.TextMatrix(r, 11) = ""
    fgc.TextMatrix(r, 12) = ""
    fgc.TextMatrix(r + 1, 6) = ""
    fgc.TextMatrix(r + 1, 7) = ""
    fgc.TextMatrix(r + 1, 9) = ""
    fgc.TextMatrix(r + 1, 10) = ""
    fgc.TextMatrix(r + 1, 11) = ""
    fgc.TextMatrix(r + 1, 12) = ""
Case cmdH(12).Caption
    fgc.TextMatrix(r, 5) = ""
    fgc.TextMatrix(r, 8) = ""
    fgc.TextMatrix(r + 1, 5) = ""
    fgc.TextMatrix(r + 1, 8) = ""
    fgc.TextMatrix(r + 1, 13) = ""
    
    If fgc.TextMatrix(r, 9) = "" _
    And fgc.TextMatrix(r, 10) = "" _
    And fgc.TextMatrix(r + 1, 9) = "" _
    And fgc.TextMatrix(r + 1, 10) = "" Then
    Else
        fgc.TextMatrix(r, 11) = ""
        fgc.TextMatrix(r, 12) = ""
        fgc.TextMatrix(r + 1, 11) = ""
        fgc.TextMatrix(r + 1, 12) = ""
    End If
Case cmdH(16).Caption, cmdH(17).Caption
    fgc.TextMatrix(r, 5) = ""
    fgc.TextMatrix(r, 7) = ""
    fgc.TextMatrix(r + 1, 5) = ""
    fgc.TextMatrix(r + 1, 7) = ""
End Select

End Sub




Private Sub CmdHExit_Click()
mFocus False
frHist.Visible = False
HistOn = 0
End Sub

Private Sub cmdHigh_Click()
frmHighlights.HighlightLoadOn = frmSystems1.IsHighlightLoadOn
If (frmHighlights.LoadHighlights(PatientId, True, True, False)) Then
    frmHighlights.Show 1
    Call frmSystems1.SetHighlightLoad(frmHighlights.HighlightLoadOn)
End If
End Sub

Private Sub cmdHiL_Click()
cmdHiL.Enabled = False
frmHighlights.DisplayBasis = EditPreviousVisit
frmHighlights.HighlightLoadOn = frmSystems1.HighlightLoadOn
If (frmHighlights.LoadHighlights(PatientId, True, True, False)) Then
    frmHighlights.cmdRefract.Enabled = False
    frmHighlights.Show 1
    frmHighlights.cmdRefract.Enabled = True
    frmSystems1.HighlightLoadOn = frmHighlights.HighlightLoadOn
End If
cmdHiL.Enabled = True
End Sub


Private Sub cmdApply_Click()
Unload frmAssignWearNew
End Sub

Private Sub cmdHist_Click(Index As Integer)
If Not UserLogin.HasPermission(epPastVisits) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
Select Case Index
Case 0
    HistOn = 2
    LoadAppInfo
    setSelVisit 0
    frHist.Visible = True
    frHist.ZOrder
    CmdHExit.SetFocus
Case 1, 2, 3, 4
    setSelVisit Index
End Select
End Sub

Private Sub setSelVisit(ind As Integer)
Select Case ind
Case 1
    SelVisit = UBound(AppInfo)
Case 2
    SelVisit = SelVisit + 1
Case 3
    SelVisit = SelVisit - 1
Case 4
    SelVisit = 0
End Select

cmdHist(1).Enabled = (SelVisit < UBound(AppInfo))
cmdHist(2).Enabled = cmdHist(1).Enabled
cmdHist(3).Enabled = (SelVisit > 0)
cmdHist(4).Enabled = cmdHist(3).Enabled

Dim tst As String
tst = Format(AppInfo(SelVisit).AppDateTime, "mmm dd yyyy")
If tst = "Dec 30 1899" Then tst = ""
cmdHist(5).Text = tst
cmdHist(5).Enabled = Not (tst = "")
Call DisplayHistoryVisit

End Sub

Private Sub cmdJSw_Click()
Dim RetCon As New PracticeConfigureInterface
If RetCon.getFieldValue("NEARVISION") = "J" Then
    RetCon.ConfigureInterfaceValue = ""
Else
    RetCon.ConfigureInterfaceValue = "J"
End If
RetCon.ConfigureInterfaceField = "NEARVISION"
Call RetCon.ApplyConfigure
Set RetCon = Nothing
Call setFixed
End Sub

Private Sub cmdKeyBoard_Click()

Dim TxtOn As Boolean
TxtOn = txtType.Visible
Call setCmd(cmdKeyBoard, Not TxtOn)
Dim i As Integer
For i = 0 To 2
    setCmd cmdCCom(i), False
Next
txtType.Visible = Not TxtOn
frAdj.Visible = TxtOn

If Not TxtOn Then
    setCmd cmdNumPad, False
    frNumPad.Visible = False
    frFixed.Visible = False
    txtType.Top = FGCCell.Top + 180
    txtType.Left = FGCCell.Left + 1200
    txtType.Height = FGCCell.RowHeight(1) + 15
    txtType.Width = FGCCell.ColWidth(1) + 15 + 240
    txtType.Text = FGCCell.TextMatrix(1, 1)
    txtType.SetFocus
    lblCommNotes.Visible = False
    If fgc.TextMatrix(fgc.Row, 0) = cmdH(12).Caption Then
        If fgc.col = 13 Then
            lblCommNotes.Visible = True
            lblCommNotes.Left = txtType.Left + txtType.Width + 90
            lblCommNotes.Top = txtType.Top
            lblCommNotes.Width = 3090
        End If
    End If
End If

End Sub

Private Sub cmdL_Click(Index As Integer)
LoadAppInfo
HistOn = 2
'testonly
'frHist.Visible = True


Dim memSelVisit As Integer
memSelVisit = SelVisit
setSelVisit 4
'-----------------------------------------
'lookup for the element

Dim g As Integer
If Index > 12 Then
    g = 3
Else
    g = 2
End If
Dim r As Integer
Dim rr As Integer


Do
    Set fgc = lstRxs(g)
    rr = fgc.Rows - 1
    For r = 0 To rr Step 2
        If fgc.TextMatrix(r, 0) = cmdL(Index).Caption Then
            frLine(0).Width = 0
            frLine(0).Visible = True
            fgc.Row = r
            fgc.col = 0
            cmdShow_Click
            Set fgc = lstRxs(g)
            HistOn = 2
            SelVisit = UBound(AppInfo)
        End If
    Next
    
    If SelVisit = UBound(AppInfo) Then
        Exit Do
    Else
        setSelVisit 2
    End If
Loop
'-----------------------------------------
SelVisit = memSelVisit
setSelVisit 0

frInfo.Visible = False
frLast.Visible = False
lstRxs(0).Enabled = True
lstRxs(1).Enabled = True
mFocus False
HistOn = 0
End Sub

Private Sub cmdLast_Click(Index As Integer)
If Not UserLogin.HasPermission(epPastVisits) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
    lstRxs(0).Enabled = False
    lstRxs(1).Enabled = False
    
    lblInfo.Caption = "Select or press 'Cancel' button"
    frInfo.Visible = True
    frLast.ZOrder
    frLast.Visible = True
    cmd(8).Visible = False

End Sub

Private Sub cmdNP_Click(Index As Integer)
Dim v As String
Dim p(1) As String
Dim i As Integer
Dim tst As String

If CCom = 2 Then
    v = mStr(2)
    If v = "No Rotation" Then v = ""
    For i = 1 To Len(v)
        tst = Mid(v, i, 1)
        If IsNumeric(tst) Then
            p(0) = p(0) & tst
        Else
            p(1) = Trim(Mid(v, i))
            Exit For
        End If
    Next
    Select Case Index
    Case 11
        If Len(p(0)) > 0 Then
            p(0) = Mid(p(0), 1, Len(p(0)) - 1)
        End If
    Case Else
        p(0) = p(0) & cmdNP(Index).Text
    End Select
    v = Trim(p(0) & " " & p(1))
    mStr(2) = v
    v = getMStr
Else
    v = FGCCell.TextMatrix(1, 1)
    Select Case Index
    Case 11
        If Len(v) > 0 Then
            v = Mid(v, 1, Len(v) - 1)
        End If
    Case Else
        v = v & cmdNP(Index).Text
    End Select
End If

FGCCell.TextMatrix(1, 1) = v
End Sub

Private Sub cmdNumPad_Click()
Dim NumPadOn As Boolean
NumPadOn = frNumPad.Visible
Call setCmd(cmdNumPad, Not NumPadOn)
frNumPad.Visible = Not NumPadOn

If Not NumPadOn Then
    FGCCell.TextMatrix(1, 1) = ""
    setCmd cmdKeyBoard, False
    txtType.Visible = False
End If

End Sub

Private Sub setCmd(cmd As fpBtn, stat As Boolean)
Dim clr(1) As Long
clr(0) = &HD6CF56
clr(1) = &H76721D
With cmd
    If stat Then
        .ThreeDHighlightColor = clr(1)
        .ThreeDShadowColor = clr(0)
    Else
        .ThreeDHighlightColor = clr(0)
        .ThreeDShadowColor = clr(1)
    End If
End With
End Sub

Private Sub cmdOU_Click()
Dim r As Integer
r = Fix(sFGC(1) / 2) * 2

lstRxs(sFGC(0)).TextMatrix(r, sFGC(2)) = CorrValue(FGCCell.TextMatrix(1, 1))
lstRxs(sFGC(0)).TextMatrix(r + 1, sFGC(2)) = CorrValue(FGCCell.TextMatrix(1, 1))

End Sub

Private Sub cmdPrint_Click()
Dim g As Integer
Dim r As Integer
Dim Rxs() As String
'Dim RxsFirst As Boolean
'Dim RxsLast As Boolean
Dim RxsAll As Integer
For g = 0 To 1
    For r = 0 To lstRxs(g).Rows - 1 Step 2
        
        If IsDate(lstRxs(g).TextMatrix(r + 1, 0)) Then
            GoTo NextItem
        End If

        
        
        Select Case Elements(lstRxs(g).TextMatrix(r, 0), g, "Index")
        Case 12, 17
            RxsAll = RxsAll + 1
            ReDim Preserve Rxs(RxsAll)
            Rxs(RxsAll) = g & r
        End Select
NextItem:
    Next
Next

 
For r = 1 To RxsAll
'    RxsFirst = (r = 1)
'    RxsLast = (r = RxsAll)
    DoEyeWearPrint_6 Rxs(r), True, True, 1
Next

End Sub

Private Sub TriggerEWType(AButton As fpBtn, AText As String)
EWType = AButton.Text
If (WearType = "CL") Then
    SelectedCLRx(CurrentIndex).EWType = EWType
    If (CurrentIndex = 1) Then
        SelectedCLRx(CurrentIndex).RxNote = Trim(AText)
    ElseIf (CurrentIndex = 2) Then
        SelectedCLRx(CurrentIndex).RxNote = Trim(AText)
    ElseIf (CurrentIndex = 3) Then
        SelectedCLRx(CurrentIndex).RxNote = Trim(AText)
    End If
Else
    SelectedPCRx(CurrentIndex).EWType = EWType
    If (CurrentIndex = 1) Then
        SelectedPCRx(CurrentIndex).RxNote = Trim(AText)
    ElseIf (CurrentIndex = 2) Then
        SelectedPCRx(CurrentIndex).RxNote = Trim(AText)
    ElseIf (CurrentIndex = 3) Then
        SelectedPCRx(CurrentIndex).RxNote = Trim(AText)
    End If
End If
Call LoadWear
End Sub

Private Sub cmdNewRx_Click()
Dim i As Integer
Dim j As Integer
j = 0
For i = 1 To MaxSetCL
    If (WearType <> "PC") Then
        If (Trim(SelectedCLRx(i).ODManifest) = "") And (Trim(SelectedCLRx(i).OSManifest) = "") Then
            j = i
            Exit For
        End If
    Else
        If (Trim(SelectedPCRx(i).ODManifest) = "") And (Trim(SelectedPCRx(i).OSManifest) = "") Then
            j = i
            Exit For
        End If
    End If
Next i
If (j > 0) Then
    Call DoTest(TestQuestion, TestOrder, j, True)
Else
    frmEventMsgs.Header = "Only " + Trim(str(MaxSetCL)) + " pairs allowed"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
End Sub

Public Function LoadWear() As Boolean
Dim i As Long
For i = 0 To 3
    lstRxs(i).Rows = 0
Next
ExpendedMode = True
LoadFiles
If EditPreviousVisit Then
    Dim ClinicalRecords As New DI_ExamClinical
    Dim tmp As String
    Dim img() As String
    Dim d As Integer
    Dim v() As String
    Dim delim As String
    Dim ODOS() As ODOSType
    Dim tmpODOS() As String
    Dim c As Integer
    Dim RInd As Integer
    Dim Temp As String
    Dim RetInv As CLInventory
    If (ClinicalRecords.LoadPreviousClinicalImprPlan(AppointmentId)) Then
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        If (ClinicalRecords.LocateStartingRecord("A")) Then
            i = 1
            Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
                ReDim img(2)
                If (ClinicalRecords.ClinicalType <> "A") Then
                    Exit Do
                End If
                
                tmp = Trim(ClinicalRecords.ClinicalFindings)
                Select Case True
                Case UCase(Mid(tmp, 1, 21)) = "DISPENSE SPECTACLE RX"
                    RInd = 12
                Case UCase(Mid(tmp, 1, 14)) = "ORDER TRIAL CL"
                    RInd = 16
                Case UCase(Mid(tmp, 1, 14)) = "DISPENSE CL RX"
                    RInd = 17
                Case Else
                    GoTo NextItem
                End Select
                
                ' get comments from ImageDescriptor
                img(0) = Trim(ClinicalRecords.ClinicalDescriptor)
                Call frmSystems1.GetCommentsFromDB(img(0), img(1), img(2))
                If Not img(0) = "" Then
                    Select Case Left(img(0), 1)
                    Case "*"
                        RInd = 14
                    Case "~"
                        RInd = 16
                    End Select
                End If
                
                ReDim v(6) As String
                For d = 1 To 6
                    delim = "-" & d & "/"
                    Do Until Left(tmp, 3) = delim Or tmp = ""
                        tmp = Mid(tmp, 2)
                    Loop
                    delim = "-" & d + 1 & "/"
                    tmp = Trim(Mid(tmp, 4))
                    Do Until Left(tmp, 3) = delim Or tmp = ""
                        v(d) = v(d) & Left(tmp, 1)
                        tmp = Mid(tmp, 2)
                    Loop
                Next
                
                ReDim ODOS(1)
                ReDim tmpODOS(1)
                For c = 0 To 1
                    getValues v(c + 2), RInd, ODOS(c)
                    If Not img(0) = "" Then
                        If img(1) = "" And img(2) = "" Then
                            If c = 0 Then ODOS(0).COMMENT = img(0)
                        Else
                            ODOS(c).COMMENT = img(c + 1)
                        End If
                    End If
                    
                    If RInd < 13 Then
                        ODOS(c).TTYPE = v(1)
                        
                        'get comments for SpecRx from seg -4/
                        If RInd = 12 And c = 0 Then
                            ODOS(c).COMMENT = v(4) & ODOS(c).COMMENT
                        End If
                        
                        tmpODOS(c) = "" & _
                        vbTab & Trim(ODOS(c).SPHERE) & _
                        vbTab & Trim(ODOS(c).CYL) & _
                        vbTab & Trim(ODOS(c).Axis) & _
                        vbTab & Trim(ODOS(c).VA_DIST) & _
                        vbTab & Trim(ODOS(c).TADD) & _
                        vbTab & Trim(ODOS(c).INT_ADD) & _
                        vbTab & Trim(ODOS(c).VA_NEAR) & _
                        vbTab & Trim(ODOS(c).PSM_1) & _
                        vbTab & Trim(ODOS(c).PSM_2) & _
                        vbTab & Trim(ODOS(c).VERT) & _
                        vbTab & Trim(ODOS(c).TTYPE) & _
                        vbTab & Trim(ODOS(c).COMMENT) & _
                        ""
                    Else
                        Temp = ""
                        If IsNumeric(v(c + 5)) Then
                            If v(c + 5) > 0 Then
                                Set RetInv = New CLInventory
                                RetInv.InventoryId = v(c + 5)
                                If (RetInv.RetrieveCLInventory) Then
                                    If (Trim(RetInv.Series) <> "") Then
                                        Temp = Temp + Trim(RetInv.Series) + " "
                                    End If
                                    If (Trim(RetInv.Type_) <> "") Then
                                        Temp = Temp + Trim(RetInv.Type_) + " "
                                    End If
                                    If (Trim(RetInv.WearTime) <> "") Then
                                        Temp = Temp + Trim(RetInv.WearTime) + " "
                                    End If
                                End If
                                Set RetInv = Nothing
                            End If
                        End If
                        ODOS(c).Model = Temp
                        
                        tmpODOS(c) = "" & _
                        vbTab & Trim(ODOS(c).SPHERE) & _
                        vbTab & Trim(ODOS(c).CYL) & _
                        vbTab & Trim(ODOS(c).Axis) & _
                        vbTab & Trim(ODOS(c).VA_DIST) & _
                        vbTab & Trim(ODOS(c).TADD) & _
                        vbTab & Trim(ODOS(c).VA_NEAR) & _
                        vbTab & Trim(ODOS(c).BC) & _
                        vbTab & Trim(ODOS(c).DIA) & _
                        vbTab & Trim(ODOS(c).PC) & _
                        vbTab & Trim(ODOS(c).D_N) & _
                        vbTab & Trim(ODOS(c).Model) & _
                        vbTab & Trim(ODOS(c).COMMENT) & _
                        ""
                    End If
                Next
                InsertRow RInd, tmpODOS(0), tmpODOS(1)
NextItem:
                i = i + 1
            Loop
        End If
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    End If
    
    Set ClinicalRecords = Nothing
End If

Dim r As Integer
For i = 0 To 3
    Select Case i
    Case 0, 2
        HlstRxs(i).FormatString = "Spec||Sphere|Cyl|Axis|Va Dist|Add|Int Add|Va Near|P1/Rel|P2/PD|Vert|Type|Comment"
        HlstRxs(i).ColWidth(0) = 810
        HlstRxs(i).ColWidth(1) = 360
        HlstRxs(i).ColWidth(2) = 630
        HlstRxs(i).ColWidth(3) = HlstRxs(0).ColWidth(2)
        HlstRxs(i).ColWidth(4) = 510
        HlstRxs(i).ColWidth(5) = 900
        HlstRxs(i).ColWidth(6) = HlstRxs(0).ColWidth(2)
        HlstRxs(i).ColWidth(7) = HlstRxs(0).ColWidth(2)
        HlstRxs(i).ColWidth(8) = HlstRxs(0).ColWidth(5)
        HlstRxs(i).ColWidth(9) = HlstRxs(0).ColWidth(2)
        HlstRxs(i).ColWidth(10) = HlstRxs(0).ColWidth(2)
        HlstRxs(i).ColWidth(11) = 450
        HlstRxs(i).ColWidth(12) = 1200
        HlstRxs(i).ColWidth(13) = 2580
    Case Else
        HlstRxs(i).FormatString = "CL||Sphere|Cyl|Axis|Va Dist|Add|Va Near|BC|Dia.|PC|D/N|Model|Comment"
        HlstRxs(i).ColWidth(0) = 810
        HlstRxs(i).ColWidth(1) = 360
        HlstRxs(i).ColWidth(2) = 630
        HlstRxs(i).ColWidth(3) = HlstRxs(1).ColWidth(2)
        HlstRxs(i).ColWidth(4) = 510
        HlstRxs(i).ColWidth(5) = 900
        HlstRxs(i).ColWidth(6) = HlstRxs(1).ColWidth(2)
        HlstRxs(i).ColWidth(7) = 900
        HlstRxs(i).ColWidth(8) = 450
        HlstRxs(i).ColWidth(9) = 480
        HlstRxs(i).ColWidth(10) = HlstRxs(1).ColWidth(8)
        HlstRxs(i).ColWidth(11) = 450
        HlstRxs(i).ColWidth(12) = 1710
        HlstRxs(i).ColWidth(13) = 2580
    End Select
    For r = 0 To 13
        lstRxs(i).ColWidth(r) = HlstRxs(i).ColWidth(r)
    Next
Next

'================================================
'lstRxs(0).AddItem ("ARF" & vbTab & _
'                   "OD" & vbTab & _
'                  "+20.00" & vbTab & _
'                  "+20.00" & vbTab & _
'                  "x999" & vbTab & _
'                  "20/200 +2" & vbTab & _
'                  "+20.00" & vbTab & _
'                  "+20.00" & vbTab & _
'                  "99/999 +9" & vbTab & _
'                  "+20.00" & vbTab & _
'                  "+20.00" & vbTab & _
'                  "999" & vbTab & _
'                  "Exec. Bifocols" & vbTab & _
'                  "")
'lstRxs(0).AddItem ("ARF" & vbTab & "OS")
'lstRxs(0).AddItem ("ARF Wet" & vbTab & "OD")
'lstRxs(0).AddItem ("ARF Wet" & vbTab & "OS")
'lstRxs(0).AddItem (" Wearing" & vbTab & "OD")
'lstRxs(0).AddItem (" Wearing" & vbTab & "OS")
'lstRxs(0).AddItem ("MRX Dry" & vbTab & "OD")
'lstRxs(0).AddItem ("MRX Dry" & vbTab & "OS")
'lstRxs(0).AddItem ("MRX Wet" & vbTab & "OD")
'lstRxs(0).AddItem ("MRX Wet" & vbTab & "OS")
'lstRxs(0).AddItem ("Over CL" & vbTab & "OD")
'lstRxs(0).AddItem ("Over CL" & vbTab & "OS")
'lstRxs(0).AddItem ("Over SP" & vbTab & "OD")
'lstRxs(0).AddItem ("Over SP" & vbTab & "OS")
'lstRxs(0).AddItem ("Rx" & vbTab & "OD")
'lstRxs(0).AddItem ("Rx" & vbTab & "OS")
''''''
''''''
''''''
''''''
''''''
''''''
''''''
'lstRxs(1).AddItem (" Wearing" & vbTab & _
'                   "OD" & vbTab & _
'                  "+20.00" & vbTab & _
'                  "+20.00" & vbTab & _
'                  "x999" & vbTab & _
'                  "20/200 +2" & vbTab & _
'                  "+20.00" & vbTab & _
'                  "99/999 +9" & vbTab & _
'                  "9.9" & vbTab & _
'                  "99.9" & vbTab & _
'                  "9.9" & vbTab & _
'                  "N" & vbTab & _
'                  "Freash look daily 6 pack Sherical Monthly" & vbTab & _
'                  "")
'lstRxs(1).AddItem (" Wearing" & vbTab & "OS")
''''''lstRxs(1).AddItem ("Trial" & vbTab & "OD")
''''''lstRxs(1).AddItem ("Trial" & vbTab & "OS")
''''''lstRxs(1).AddItem ("CL Rx" & vbTab & "OD")
''''''lstRxs(1).AddItem ("CL Rx" & vbTab & "OS")


Call AdjustFGC

Dim g As Integer
For g = 0 To 1
    For r = 0 To lstRxs(g).Rows - 1 Step 2
        ColorLine lstRxs(g), r
    Next
Next


lstVSC.Top = frInfo.Top - lstVSC.Height + 120
lstVSC.Width = 9510
lstVSC.FormatString = "VSC|Dist|PH Dist|Near|PH Near||Auto K|R1|Axis1|R2|Axis2|Avg"
lstVSC.ColWidth(0) = 600
lstVSC.ColWidth(1) = 900
lstVSC.ColWidth(2) = lstVSC.ColWidth(1)
lstVSC.ColWidth(3) = lstVSC.ColWidth(1)
lstVSC.ColWidth(4) = lstVSC.ColWidth(1)
lstVSC.ColWidth(5) = 90
lstVSC.ColWidth(6) = lstVSC.ColWidth(0)
lstVSC.ColWidth(7) = lstVSC.ColWidth(1)
lstVSC.ColWidth(8) = lstVSC.ColWidth(1)
lstVSC.ColWidth(9) = lstVSC.ColWidth(1)
lstVSC.ColWidth(10) = lstVSC.ColWidth(1)
lstVSC.ColWidth(11) = lstVSC.ColWidth(1)

lstVSC.TextMatrix(1, 0) = "OD"
lstVSC.TextMatrix(1, 6) = "OD"
lstVSC.TextMatrix(2, 0) = "OS"
lstVSC.TextMatrix(2, 6) = "OS"
lstVSC.col = 5
For r = 1 To 2
    lstVSC.Row = r
    lstVSC.CellBackColor = lstVSC.BackColorFixed
Next
For c = 0 To lstVSC.Cols - 1
    lstVSC.ColAlignment(c) = flexAlignLeftCenter
Next
 

logPatientId PatientId
End Function

Private Function LoadEyeRx(Init As Boolean) As Boolean
Dim i As Integer
Dim u As Integer
Dim st As Integer
Dim ed As Integer
Dim MyIdx As Integer
Dim OrderOn As Boolean
Dim ATemp As String
Dim TheId As String
Dim TheNote As String
Dim TheDisplayName As String
Dim TrialOn As Boolean
Dim TrialOrderOn As Boolean
LoadEyeRx = False
OrderOn = False
If (Init) Then
    CurrRx = 1
    CurrRxCnt = 0
    For i = 1 To MaxSetCL
        If (WearType = "PC") Then
            If (Trim(SelectedPCRx(i).ODManifest) <> "") Or (Trim(SelectedPCRx(i).OSManifest) <> "") Then
                CurrRxCnt = CurrRxCnt + 1
            End If
        Else
            If (Trim(SelectedCLRx(i).ODManifest) <> "") Or (Trim(SelectedCLRx(i).OSManifest) <> "") Then
                CurrRxCnt = CurrRxCnt + 1
            End If
        End If
    Next i
End If
MyIdx = 1
st = 1
ed = CurrRxCnt
For i = st To ed
    If (WearType = "PC") Then
        If (Trim(SelectedPCRx(i).ODManifest) <> "") Or (Trim(SelectedPCRx(i).OSManifest) <> "") Then
            TheId = "OD: " + Trim(SelectedPCRx(i).ODManifest) + vbCrLf + "OS: " + Trim(SelectedPCRx(i).OSManifest)
            If (Trim(SelectedPCRx(i).EWType) <> "") Or (Trim(SelectedPCRx(i).EWCompany) <> "") Then
                TheId = TheId + vbCrLf + Trim(SelectedPCRx(i).EWType) + " " + Trim(SelectedPCRx(i).EWCompany)
                TheNote = Trim(SelectedPCRx(i).RxNote)
            End If
            Call SelectRx(SelectedPCRx(i).ODManifest, SelectedPCRx(i).OSManifest, "", MyIdx, 0)
            MyIdx = MyIdx + 2
        End If
    Else
        If (Trim(SelectedCLRx(i).ODManifest) <> "") Or (Trim(SelectedCLRx(i).OSManifest) <> "") Then
            TheId = "OD: " + Trim(SelectedCLRx(i).ODManifest) + vbCrLf + "OS: " + Trim(SelectedCLRx(i).OSManifest)
            If (SelectedCLRx(i).InventoryIdOD > 0) Then
                Call GetInventoryItem(SelectedCLRx(i).InventoryIdOD, ATemp)
                TheId = TheId + vbCrLf + Trim(ATemp)
            End If
            If (SelectedCLRx(i).InventoryIdOS > 0) Then
                Call GetInventoryItem(SelectedCLRx(i).InventoryIdOS, ATemp)
                TheId = TheId + vbCrLf + Trim(ATemp)
            End If
            Call SelectRx(SelectedCLRx(i).ODManifest, SelectedCLRx(i).OSManifest, "", MyIdx, 2)
            MyIdx = MyIdx + 2
        End If
    End If
Next i
CurrRxCnt = SetRxCnt
LoadEyeRx = True
End Function

Private Function PlaceCLRx(Ref As Integer, ARxT As String, ARxC As String, AOD As String, AOS As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
PlaceCLRx = False
If (Ref <= MaxSetCL) And (Ref > 0) Then
    PlaceCLRx = True
    SelectedCLRx(Ref).EWType = ARxT
    SelectedCLRx(Ref).EWCompany = ARxC
    SelectedCLRx(Ref).ODManifest = AOD
    SelectedCLRx(Ref).OSManifest = AOS
    SelectedCLRx(Ref).InventoryIdOD = AInvId1
    SelectedCLRx(Ref).InventoryIdOS = AInvId2
    SelectedCLRx(Ref).RxNote = ANote
    PlaceCLRx = True
End If
End Function

Private Function PlacePCRx(Ref As Integer, ARxT As String, ARxC As String, AOD As String, AOS As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
PlacePCRx = False
If (Ref <= MaxSetCL) And (Ref > 0) Then
    If (InStrPS(UCase(ARxT), "SPECTACLE") = 0) Then
        SelectedPCRx(Ref).EWCompany = ARxT
    Else
        SelectedPCRx(Ref).EWCompany = ""
    End If
        SelectedPCRx(Ref).EWType = ARxC
    SelectedPCRx(Ref).ODManifest = AOD
    SelectedPCRx(Ref).OSManifest = AOS
    SelectedPCRx(Ref).InventoryIdOD = AInvId1
    SelectedPCRx(Ref).InventoryIdOS = AInvId2
    SelectedPCRx(Ref).RxNote = ANote
    PlacePCRx = True
End If
End Function

Private Function SetCLRx(Ref As Integer, ARxT As String, ARxC As String, AOD As String, AOS As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
SetCLRx = False
ARxT = ""
ARxC = ""
AOD = ""
AOS = ""
AInvId1 = 0
AInvId2 = 0
ANote = ""
If (Ref <= MaxSetCL) Then
    If (Trim(SelectedCLRx(Ref).ODManifest) <> "") Or (Trim(SelectedCLRx(Ref).OSManifest) <> "") Then
        SetCLRx = True
        If (SelectedCLRx(Ref).InventoryIdOD = 0) And (SelectedCLRx(Ref).InventoryIdOS = 0) Then
            ARxT = SelectedCLRx(Ref).EWType
            ARxC = SelectedCLRx(Ref).EWCompany
        Else
            ARxT = ""
            ARxC = ""
        End If
        AOD = SelectedCLRx(Ref).ODManifest
        AOS = SelectedCLRx(Ref).OSManifest
        AInvId1 = SelectedCLRx(Ref).InventoryIdOD
        AInvId2 = SelectedCLRx(Ref).InventoryIdOS
        ANote = SelectedCLRx(Ref).RxNote
        SetCLRx = True
    End If
End If
End Function

Private Function SetPCRx(Ref As Integer, ARxT As String, ARxC As String, AOD As String, AOS As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
SetPCRx = False
ARxT = ""
ARxC = ""
AOD = ""
AOS = ""
AInvId1 = 0
AInvId2 = 0
ANote = ""
If (Ref <= TotalPCRx) Then
    If (Trim(SelectedPCRx(Ref).ODManifest) <> "") Or (Trim(SelectedPCRx(Ref).OSManifest) <> "") Then
        ARxT = SelectedPCRx(Ref).EWType
        ARxC = SelectedPCRx(Ref).EWCompany
        AOD = SelectedPCRx(Ref).ODManifest
        AOS = SelectedPCRx(Ref).OSManifest
        AInvId1 = SelectedPCRx(Ref).InventoryIdOD
        AInvId2 = SelectedPCRx(Ref).InventoryIdOS
        ANote = SelectedPCRx(Ref).RxNote
        SetPCRx = True
    End If
End If
End Function

Public Function InitializeCLRx() As Boolean
Dim i As Integer
InitializeCLRx = True
For i = 1 To MaxSetCL
    SelectedCLRx(i).EWType = ""
    SelectedCLRx(i).EWCompany = ""
    SelectedCLRx(i).ODManifest = ""
    SelectedCLRx(i).OSManifest = ""
    SelectedCLRx(i).InventoryIdOD = 0
    SelectedCLRx(i).InventoryIdOS = 0
    SelectedCLRx(i).RxNote = ""
Next i
End Function

Public Function InitializePCRx() As Boolean
Dim i As Integer
InitializePCRx = True
For i = 1 To MaxSetCL
    SelectedPCRx(i).EWType = ""
    SelectedPCRx(i).EWCompany = ""
    SelectedPCRx(i).ODManifest = ""
    SelectedPCRx(i).OSManifest = ""
    SelectedPCRx(i).InventoryIdOD = 0
    SelectedPCRx(i).InventoryIdOS = 0
    SelectedPCRx(i).RxNote = ""
Next i
End Function

Public Function TotalCLRx() As Integer
Dim i As Integer
TotalCLRx = 0
For i = 1 To MaxSetCL
    If (Trim(SelectedCLRx(i).ODManifest) = "") And (Trim(SelectedCLRx(i).OSManifest) = "") Then
        Exit For
    Else
        TotalCLRx = TotalCLRx + 1
    End If
Next i
End Function

Public Function TotalPCRx() As Integer
Dim i As Integer
Dim j As Integer
TotalPCRx = 0
For i = 0 To MaxSetCL
    If (Trim(SelectedPCRx(i).ODManifest) = "") And (Trim(SelectedPCRx(i).OSManifest) = "") Then
        Exit For
    Else
        TotalPCRx = TotalPCRx + 1
    End If
Next i
End Function

Public Function SetCL(RefId As Integer, ARxType As String, ARxCompany As String, AODManifest As String, AOSManifest As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
SetCL = PlaceCLRx(RefId, ARxType, ARxCompany, AODManifest, AOSManifest, AInvId1, AInvId2, ANote)
End Function

Public Function SetPC(RefId As Integer, ARxType As String, ARxCompany As String, AODManifest As String, AOSManifest As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
SetPC = PlacePCRx(RefId, ARxType, ARxCompany, AODManifest, AOSManifest, AInvId1, AInvId2, ANote)
End Function

Public Function GetCL(RefId As Integer, ARxType As String, ARxCompany As String, AODManifest As String, AOSManifest As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
GetCL = SetCLRx(RefId, ARxType, ARxCompany, AODManifest, AOSManifest, AInvId1, AInvId2, ANote)
End Function

Public Function GetPC(RefId As Integer, ARxType As String, ARxCompany As String, AODManifest As String, AOSManifest As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
GetPC = SetPCRx(RefId, ARxType, ARxCompany, AODManifest, AOSManifest, AInvId1, AInvId2, ANote)
End Function

Private Sub DoTest(Question As String, FileId As String, Ref As Integer, NewOn As Boolean)
Dim i As Integer
Dim IRef As Integer
Dim ODT As String
Dim OST As String
Dim Temp As String
Dim TestTime As String
Dim AFile As String
Dim TheFile As String
IRef = 1
If (WearType = "CL") Then
    IRef = 2
End If
Temp = ""
If (Ref > MaxSetCL) Or (Ref < 1) Then
    Exit Sub
End If
If (Trim(FileId) <> "") Then
    TheFile = DoctorInterfaceDirectory + "G" + FileId + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
    AFile = GetTestFileName("13A")
    If (TheManifestUsed = 7) Or (TheManifestUsed = 8) Then
        If (FileId = "01A") Then
            AFile = GetTestFileName("07A")
        Else
            AFile = GetTestFileName("08A")
        End If
    ElseIf (TheManifestUsed = 9) Then
        AFile = GetTestFileName("10A")
    End If
    If (FM.IsFileThere(AFile)) Then
        If (FileId = "01A") Then
            Call MakeMrxConvert(Question, "Glasses", TheManifestUsed, AFile, TheFile)
        Else
            Call MakeMrxConvert(Question, "ContactLens", TheManifestUsed, AFile, TheFile)
        End If
    End If
    If Not (FM.IsFileThere(TheFile)) Then
        FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(1)
        Print #1, "QUESTION=\" + UCase(Trim(Question))
        Call FormatTimeNow(TestTime)
        Print #1, "TIME=" + TestTime
        FM.CloseFile CLng(1)
    End If
    frmEditTests.ManifestResultsOn = True
    If (Trim(TheManifestResults) = "") Then
        frmEditTests.ManifestResultsOn = False
    End If
    frmEditTests.ManifestResults = TheManifestResults
    frmEditTests.StoreResults = TheFile
    frmEditTests.Question = UCase(Question)
    frmEditTests.PatientId = PatientId
    frmEditTests.AppointmentId = AppointmentId
    frmEditTests.FormOn = True
    frmAssignWearNew.Enabled = False
    frmEditTests.Show 1
    If (Trim(frmEditTests.StoreResults) = "") Or (frmEditTests.QuitOn) Then
        If (FM.IsFileThere(TheFile)) Then
            FM.Kill TheFile
        End If
        TheFile = ""
    End If
End If
frmAssignWearNew.ZOrder 0
frmAssignWearNew.Enabled = True
If (FM.IsFileThere(TheFile)) Then
    Call frmTestResults.PushList(TheFile)
    Call frmSystems1.ApplyTestResults(TheFile, FileId, Temp, False, True)
    If (Trim(Temp) <> "") Then
        If (InStrPS(Temp, "OD: " + Chr(13)) > 0) And (Mid(Temp, Len(Temp) - 3, 4) = "OS: ") Then
            If (frmEditTests.ManifestResultsOn) Then
                Temp = frmEditTests.ManifestResults
                Temp = ":  " + Temp
            End If
        End If
    End If
ElseIf (frmEditTests.ManifestResultsOn) Then
    Temp = frmEditTests.ManifestResults
    Temp = ":  " + Temp
Else
    Exit Sub
End If
If (Trim(Temp) <> "") Then
    i = InStrPS(Temp, ":")
    If (i > 0) Then
        Temp = Mid(Temp, i + 3, Len(Temp) - (i + 2))
    End If
    i = InStrPS(Temp, "OS")
    If (i > 0) Then
        ODT = Mid(Temp, 4, (i - 1) - 3)
        Call StripCharacters(ODT, Chr(13))
        Call StripCharacters(ODT, Chr(10))
        OST = Mid(Temp, i + 3, Len(Temp) - (i - 2))
        Call StripCharacters(OST, Chr(13))
        Call StripCharacters(OST, Chr(10))
    Else
        ODT = Temp
        OST = ""
    End If
    If (FM.IsFileThere(TheFile)) Then
        FM.Kill TheFile
    End If
    ODT = Trim(ODT)
    OST = Trim(OST)
    If (Len(ODT) > 2) Or (Len(OST) > 2) Then
        Call StripCharacters(ODT, Chr(10))
        Call StripCharacters(ODT, Chr(13))
        Call StripCharacters(OST, Chr(10))
        Call StripCharacters(OST, Chr(13))
        Ref = CurrRxCnt + 1
        If (WearType = "PC") Then
            SelectedPCRx(Ref).ODManifest = ODT
            SelectedPCRx(Ref).OSManifest = OST
        Else
            SelectedCLRx(Ref).ODManifest = ODT
            SelectedCLRx(Ref).OSManifest = OST
        End If
        Call SelectRx(ODT, OST, "", Ref, IRef)
        Call LoadWear
    End If
End If
End Sub

Private Sub SelectRx(ODRx As String, OSRx As String, ANote As String, Ref As Integer, IRef As Integer)

Dim i As Integer
Dim g1 As Integer
Dim Cnt As Integer
Dim IIdx As Integer
Dim RColor As Single
Dim I1Color As Single
Dim I2Color As Single
Dim s1 As String, s2 As String
Dim c1 As String, c2 As String
Dim a1 As String, a2 As String
Dim r1 As String, r2 As String
Dim i1 As String, i2 As String
Dim x1 As String, x2 As String
Dim x3 As String, x4 As String
Dim v1 As String, v2 As String
Dim t1 As String, t2 As String
Dim t3 As String, t4 As String
Dim DisplayText As String

IIdx = Int((lstRxs(0).Rows - 1) / 2)
I1Color = &H80&
I2Color = &H6C6928
RColor = I2Color
If ((IIdx - (Int(IIdx / 2) * 2)) = 0) Then
    RColor = I1Color
End If
Call ParseTest(IRef, ODRx, OSRx, s1, c1, a1, r1, i1, x1, x2, v1, s2, c2, a2, r2, i2, x3, x4, v2)
If Ref > 0 Then
    If (Trim(ODRx) <> "") Or (Trim(OSRx) <> "") Then
        t1 = "A1:"
        t2 = "A2:"
        t3 = "Vx:"
        If (IRef = 2) Then
            t1 = "BC:"
            t2 = "DI:"
            t3 = ""
        End If
        i = Ref
        Call StripCharacters(i1, Chr(10))
        Call StripCharacters(i1, Chr(13))
        Call StripCharacters(a1, Chr(10))
        Call StripCharacters(a1, Chr(13))
        Call StripCharacters(v1, Chr(10))
        Call StripCharacters(v1, Chr(13))
        If (r1 <> "") Then
            r1 = "Add:" + r1
        End If
        If (i1 <> "") Then
            i1 = "Int:" + i1
        End If
        If (x1 <> "") Then
            x1 = t1 + x1
        End If
        If (x2 <> "") Then
            x2 = t2 + x2
        End If
        If (v1 <> "") Then
            v1 = t3 + v1
        End If
        If (WearType = "CL") Then
            If (IRef = 2) Then
                DisplayText = "OD" + vbTab _
                            + s1 + vbTab _
                            + c1 + vbTab _
                            + a1 + vbTab _
                            + r1 + vbTab _
                            + i1 + vbTab _
                            + x1 + vbTab _
                            + x2 + vbTab _
                            + v1 + vbTab _
                            + "" + vbTab _
                            + "" + vbTab
            Else
                DisplayText = "OD" + vbTab _
                            + s1 + vbTab _
                            + c1 + vbTab _
                            + a1 + vbTab _
                            + r1 + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab
            End If
            DisplayText = DisplayText + "Trial" + vbTab
        Else
            If (IRef = 2) Then
                DisplayText = "OD" + vbTab _
                            + s1 + vbTab _
                            + c1 + vbTab _
                            + a1 + vbTab _
                            + r1 + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab
            Else
                DisplayText = "OD" + vbTab _
                            + s1 + vbTab _
                            + c1 + vbTab _
                            + a1 + vbTab _
                            + r1 + vbTab _
                            + i1 + vbTab _
                            + x1 + vbTab _
                            + x2 + vbTab _
                            + v1 + vbTab _
                            + "" + vbTab _
                            + "" + vbTab
            End If
            DisplayText = DisplayText + "" + vbTab
        End If
        lstRxs(0).AddItem DisplayText, i - 1
        Cnt = i - 1
        GoSub SetLineColor
    
        If (r2 <> "") Then
            r2 = "Add:" + r2
        End If
        If (i2 <> "") Then
            i2 = "Int:" + i2
        End If
        If (x3 <> "") Then
            x3 = t1 + x3
        End If
        If (x4 <> "") Then
            x4 = t2 + x4
        End If
        If (v2 <> "") Then
            v2 = t3 + v2
        End If
        Call StripCharacters(i2, Chr(10))
        Call StripCharacters(i2, Chr(13))
        Call StripCharacters(a2, Chr(10))
        Call StripCharacters(a2, Chr(13))
        Call StripCharacters(v2, Chr(10))
        Call StripCharacters(v2, Chr(13))
        If (WearType = "CL") Then
            If (IRef = 2) Then
                DisplayText = "OS" + vbTab _
                            + s2 + vbTab _
                            + c2 + vbTab _
                            + a2 + vbTab _
                            + r2 + vbTab _
                            + i2 + vbTab _
                            + x3 + vbTab _
                            + x4 + vbTab _
                            + v2 + vbTab _
                            + "" + vbTab _
                            + "" + vbTab
            Else
                DisplayText = "OS" + vbTab _
                            + s2 + vbTab _
                            + c2 + vbTab _
                            + a2 + vbTab _
                            + r2 + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab
            End If
            DisplayText = DisplayText + "" + vbTab
        Else
            If (IRef = 2) Then
                DisplayText = "OS" + vbTab _
                            + s2 + vbTab _
                            + c2 + vbTab _
                            + a2 + vbTab _
                            + r2 + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab _
                            + "" + vbTab
            Else
                DisplayText = "OS" + vbTab _
                            + s2 + vbTab _
                            + c2 + vbTab _
                            + a2 + vbTab _
                            + r2 + vbTab _
                            + i2 + vbTab _
                            + x3 + vbTab _
                            + x4 + vbTab _
                            + v2 + vbTab _
                            + "" + vbTab _
                            + "" + vbTab
            End If
            DisplayText = DisplayText + "" + vbTab
        End If
        lstRxs(0).AddItem DisplayText, i
        Cnt = i
        GoSub SetLineColor
        
        IIdx = Int((lstRxs(0).Rows - 1) / 2)
        If (WearType = "PC") Then
            SelectedPCRx(IIdx).ODManifest = ODRx
            SelectedPCRx(IIdx).OSManifest = OSRx
            SelectedCLRx(IIdx).OrderType = 0
        Else
            SelectedCLRx(IIdx).ODManifest = ODRx
            SelectedCLRx(IIdx).OSManifest = OSRx
            SelectedCLRx(IIdx).OrderType = 0
        End If
        GoSub SetColumnWidths
    End If
' do this set so that if they edit is at the current rx
    If ((lstRxs(0).Rows - 1) > 0) Then
'        lstRxs(0).RowSel = lstRxs(0).Rows - 1
'        lstRxs(0).ColSel = 0
        'CurrentEditTest = Int((lstRxs(0).Rows - 1) / 2)
    End If
'    Call AutoFitFlexGrid(lstRxs)
End If


Call AdjustFGC
Exit Sub
SetColumnWidths:
    lstRxs(0).Row = 1
    lstRxs(0).RowSel = 1
    For g1 = 0 To lstRxs(0).Cols - 1
        lstRxs(0).col = 1
        lstRxs(0).ColSel = 1
        If (g1 = 0) Then
            lstRxs(0).ColWidth(g1) = 25 * 15
        ElseIf (g1 = 1) Then
            lstRxs(0).ColWidth(g1) = 60 * 15
        ElseIf (g1 = 2) Then
            lstRxs(0).ColWidth(g1) = 60 * 15
        ElseIf (g1 = 3) Then
            lstRxs(0).ColWidth(g1) = 60 * 15
        ElseIf (g1 = 4) Then
            lstRxs(0).ColWidth(g1) = 60 * 15
        ElseIf (g1 = 5) Then
            lstRxs(0).ColWidth(g1) = 60 * 15
        ElseIf (g1 = 6) Then
            lstRxs(0).ColWidth(g1) = 60 * 15
        ElseIf (g1 = 7) Then
            lstRxs(0).ColWidth(g1) = 60 * 15
        ElseIf (g1 = 8) Then
            lstRxs(0).ColWidth(g1) = 60 * 15
        ElseIf (g1 = 9) Then
            lstRxs(0).ColWidth(g1) = 100 * 15
        ElseIf (g1 = 10) Then
            lstRxs(0).ColWidth(g1) = 100 * 15
        ElseIf (g1 = 11) Then
            lstRxs(0).ColWidth(g1) = 60 * 15
        End If
    Next g1
    Return
SetLineColor:
    lstRxs(0).Row = Cnt
    lstRxs(0).RowSel = Cnt
    For g1 = 0 To lstRxs(0).Cols - 1
        lstRxs(0).col = g1
        lstRxs(0).ColSel = g1
        If (WearType = "CL") Then
            If (g1 = lstRxs(0).Cols - 1) Then
                If (SelectedCLRx(IIdx).OrderType = 0) Then
                    lstRxs(0).CellBackColor = TrialColor
                ElseIf (SelectedCLRx(IIdx).OrderType = 1) Then
                    lstRxs(0).CellBackColor = OrderTrialColor
                ElseIf (SelectedCLRx(IIdx).OrderType = 2) Then
                    lstRxs(0).CellBackColor = RxColor
                End If
            Else
                lstRxs(0).CellBackColor = RColor
            End If
        Else
            lstRxs(0).CellBackColor = RColor
        End If
    Next g1
    Return
End Sub

Private Sub DoPrescriptions(Idx As Integer, RxOn As Boolean)
Dim ZTemp As String
If (Idx > 0) Then
    If (RxOn) Then
        If ((CurrRxCnt + 1) <= MaxSetCL) Then
            SelectedCLRx(CurrRxCnt + 1).EWCompany = SelectedCLRx(Idx).EWCompany
            SelectedCLRx(CurrRxCnt + 1).EWType = SelectedCLRx(Idx).EWType
            SelectedCLRx(CurrRxCnt + 1).InventoryIdOD = SelectedCLRx(Idx).InventoryIdOD
            SelectedCLRx(CurrRxCnt + 1).InventoryIdOS = SelectedCLRx(Idx).InventoryIdOS
            SelectedCLRx(CurrRxCnt + 1).ODManifest = SelectedCLRx(Idx).ODManifest
            SelectedCLRx(CurrRxCnt + 1).OSManifest = SelectedCLRx(Idx).OSManifest
            SelectedCLRx(CurrRxCnt + 1).RxNote = ""
            CurrRxCnt = CurrRxCnt + 1
            Call LoadEyeRx(False)
        Else
            frmEventMsgs.Header = "Only " + Trim(str(MaxSetCL)) + " pairs allowed"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
        End If
    Else
        If (Idx = 1) Then
'            lblTrial1.Visible = Not (lblTrial1.Visible)
'            lblTrial1.Caption = "Trial, Click to Order"
'            lblTrial1.BackColor = TrialNormalColor
'            If Not (lblTrial1.Visible) Then
                ZTemp = "^"
                GoSub SetValue
'            Else
                ZTemp = "*"
                If (OrderTrial) Then
                    ZTemp = "~"
'                    lblTrial1.Caption = "Order Trial, Click for Trial"
'                    lblTrial1.BackColor = TrialOrderColor
                End If
                GoSub SetValue
'            End If
        ElseIf (Idx = 2) Then
'            lblTrial2.Visible = Not (lblTrial2.Visible)
'            lblTrial2.Caption = "Trial, Click to Order"
'            lblTrial2.BackColor = TrialNormalColor
'            If Not (lblTrial2.Visible) Then
                ZTemp = "^"
                GoSub SetValue
'            Else
                ZTemp = "*"
'                lblTrial2.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
'                    lblTrial2.Caption = "Order Trial, Click for Trial"
'                    lblTrial2.BackColor = TrialOrderColor
'            End If
            GoSub SetValue
            End If
        ElseIf (Idx = 3) Then
'            lblTrial3.Visible = Not (lblTrial3.Visible)
'            lblTrial3.Caption = "Trial, Click to Order"
'            lblTrial3.BackColor = TrialNormalColor
'            If Not (lblTrial3.Visible) Then
                ZTemp = "^"
                GoSub SetValue
'            Else
                ZTemp = "*"
'                lblTrial3.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
'                    lblTrial3.Caption = "Order Trial, Click for Trial"
'                    lblTrial3.BackColor = TrialOrderColor
                End If
                GoSub SetValue
'            End If
        ElseIf (Idx = 4) Then
'            lblTrial4.Visible = Not (lblTrial4.Visible)
'            lblTrial4.Caption = "Trial, Click to Order"
'            lblTrial4.BackColor = TrialNormalColor
'            If Not (lblTrial4.Visible) Then
                ZTemp = "^"
                GoSub SetValue
'            Else
                ZTemp = "*"
'                lblTrial4.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
'                    lblTrial4.Caption = "Order Trial, Click for Trial"
'                    lblTrial4.BackColor = TrialOrderColor
                End If
                GoSub SetValue
'            End If
        ElseIf (Idx = 5) Then
'            lblTrial5.Visible = Not (lblTrial5.Visible)
'            lblTrial5.Caption = "Trial, Click to Order"
'            lblTrial5.BackColor = TrialNormalColor
'            If Not (lblTrial5.Visible) Then
                ZTemp = "^"
                GoSub SetValue
'            Else
                ZTemp = "*"
'                lblTrial5.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
'                    lblTrial5.Caption = "Order Trial, Click for Trial"
'                    lblTrial5.BackColor = TrialOrderColor
                End If
                GoSub SetValue
'            End If
        ElseIf (Idx = 6) Then
'            lblTrial6.Visible = Not (lblTrial6.Visible)
'            lblTrial6.Caption = "Trial, Click to Order"
'            lblTrial6.BackColor = TrialNormalColor
'            If Not (lblTrial6.Visible) Then
                ZTemp = "^"
                GoSub SetValue
'            Else
                ZTemp = "*"
'                lblTrial6.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
'                    lblTrial6.Caption = "Order Trial, Click for Trial"
'                    lblTrial6.BackColor = TrialOrderColor
                End If
                GoSub SetValue
'            End If
        ElseIf (Idx = 7) Then
'            lblTrial7.Visible = Not (lblTrial7.Visible)
'            lblTrial7.Caption = "Trial, Click to Order"
'            lblTrial7.BackColor = TrialNormalColor
'            If Not (lblTrial7.Visible) Then
                ZTemp = "^"
                GoSub SetValue
'            Else
                ZTemp = "*"
'                lblTrial7.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
'                    lblTrial7.Caption = "Order Trial, Click for Trial"
'                    lblTrial7.BackColor = TrialOrderColor
                End If
                GoSub SetValue
'            End If
        ElseIf (Idx = 8) Then
'            lblTrial8.Visible = Not (lblTrial8.Visible)
'            lblTrial8.Caption = "Trial, Click to Order"
'            lblTrial8.BackColor = TrialNormalColor
'            If Not (lblTrial8.Visible) Then
                ZTemp = "^"
                GoSub SetValue
'            Else
                ZTemp = "*"
'                lblTrial8.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
'                    lblTrial8.Caption = "Order Trial, Click for Trial"
'                    lblTrial8.BackColor = TrialOrderColor
                End If
                GoSub SetValue
'            End If
        End If
    End If
End If
Exit Sub
SetValue:
    If (Trim(SelectedCLRx(Idx).RxNote) = "") Then
        SelectedCLRx(Idx).RxNote = ZTemp
    Else
        SelectedCLRx(Idx).RxNote = ZTemp + Trim(SelectedCLRx(Idx).RxNote)
    End If
    Return
End Sub

Private Sub DoDescriptors(Idx As Integer)
Dim i As Integer
Dim Style As Boolean
Dim TrialType As String
Dim ODT As String
Dim OST As String
Dim TOrder As String
Dim TQuestion As String
Dim Temp As String, TestTime As String
Dim AFile As String, TheFile As String
Temp = ""
Style = False
TOrder = "56A"
TQuestion = "CLRX FIT DESCRIPTORS"
If (Idx > MaxSetCL) Or (Idx < 1) Then
    Exit Sub
End If
If (Trim(TOrder) <> "") Then
    TheFile = GetTestFileName(TOrder)
    If (FM.IsFileThere(TheFile)) Then
        FM.Kill TheFile
    End If
    If Not (FM.IsFileThere(TheFile)) Then
        If Not (Style) Then
            FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(1)
            Print #1, "QUESTION=\" + UCase(Trim(TQuestion))
            Call FormatTimeNow(TestTime)
            Print #1, "TIME=" + TestTime
            FM.CloseFile CLng(1)
        End If
    End If
    If (Style) Then
        frmTestEntry.ManifestResults = ""
        frmTestEntry.ActivityId = ActivityId
        frmTestEntry.PatientId = PatientId
        frmTestEntry.AppointmentId = AppointmentId
        frmTestEntry.ChangeOn = False
        frmTestEntry.StoreResults = TheFile
        frmTestEntry.Question = UCase(TQuestion)
        frmTestEntry.FormOn = True
        frmAssignWear.Enabled = False
        frmTestEntry.Show 1
        If (Trim(frmTestEntry.StoreResults) = "") Then
            TheFile = ""
        End If
    Else
        frmEditTests.CLFitOn = True
        frmEditTests.ManifestResultsOn = False
        frmEditTests.ManifestResults = ""
        frmEditTests.StoreResults = TheFile
        frmEditTests.Question = UCase(TQuestion)
        frmEditTests.PatientId = PatientId
        frmEditTests.AppointmentId = AppointmentId
        frmEditTests.FormOn = True
        frmAssignWear.Enabled = False
        frmEditTests.Show 1
        frmEditTests.CLFitOn = False
        If (Trim(frmEditTests.StoreResults) = "") Or (frmEditTests.QuitOn) Then
            If (FM.IsFileThere(TheFile)) Then
                FM.Kill TheFile
            End If
            TheFile = ""
        End If
    End If
End If



MsgBox ODT + vbCrLf + OST






''''''''''frmAssignWear.ZOrder 0
''''''''''frmAssignWear.Enabled = True
''''''''''If (FM.IsFileThere(TheFile)) Then
''''''''''    Call frmTestResults.PushList(TheFile)
''''''''''    Call frmSystems1.ApplyTestResults(TheFile, TOrder, Temp, False, True)
''''''''''    If (InStrPS(Temp, "OD: " + Chr(13)) > 0) And (Mid(Temp, Len(Temp) - 3, 4) = "OS: ") Then
''''''''''        If Not (Style) Then
''''''''''            Temp = frmEditTests.ManifestResults
''''''''''            Temp = ":  " + Temp
''''''''''        End If
''''''''''    End If
''''''''''Else
''''''''''    Exit Sub
''''''''''End If
''''''''''If (Trim(Temp) <> "") Then
''''''''''    i = InStrPS(Temp, ":")
''''''''''    If (i > 0) Then
''''''''''        Temp = Mid(Temp, i + 3, Len(Temp) - (i + 2))
''''''''''    End If
''''''''''    i = InStrPS(Temp, "OS")
''''''''''    If (i > 0) Then
''''''''''        ODT = Mid(Temp, 4, (i - 1) - 3)
''''''''''        Call StripCharacters(ODT, Chr(13))
''''''''''        Call StripCharacters(ODT, Chr(10))
''''''''''        OST = Mid(Temp, i + 3, Len(Temp) - (i - 2))
''''''''''        Call StripCharacters(OST, Chr(13))
''''''''''        Call StripCharacters(OST, Chr(10))
''''''''''    Else
''''''''''        ODT = Temp
''''''''''        OST = ""
''''''''''    End If
''''''''''    Call frmSystems1.ClearTestResults(TOrder)
''''''''''    If (FM.IsFileThere(TheFile)) Then
''''''''''        Kill TheFile
''''''''''    End If
''''''''''    If (Len(ODT) > 2) Or (Len(OST) > 2) Then
''''''''''        ODT = "OD:" + ODT
''''''''''        Call ReplaceCharacters(ODT, ",", " ")
''''''''''        ODT = Trim(ODT)
''''''''''        OST = "OS:" + OST
''''''''''        Call ReplaceCharacters(OST, ",", " ")
''''''''''        OST = Trim(OST)
''''''''''        If (Idx = 1) Then
''''''''''            TrialType = ""
'''''''''''            If (lblTrial1.Visible) Then
'''''''''''                If (lblTrial1.BackColor = TrialOrderColor) Then
'''''''''''                    TrialType = "~"
'''''''''''                ElseIf (lblTrial1.BackColor = TrialNormalColor) Then
'''''''''''                    TrialType = "*"
'''''''''''                End If
'''''''''''            End If
'''''''''''            txtNote1.Text = ODT + vbCrLf + OST
'''''''''''            lblNote1.BackColor = EnlargeOff
''''''''''            If (IsNoteBig(ODT + vbCrLf + OST)) Then
'''''''''''                lblNote1.BackColor = EnlargeOn
''''''''''            End If
'''''''''''            txtNote1.Visible = True
''''''''''            SelectedCLRx(Idx).RxNote = TrialType + ODT + " " + OST
''''''''''        ElseIf (Idx = 2) Then
''''''''''            TrialType = ""
'''''''''''            If (lblTrial2.Visible) Then
'''''''''''                If (lblTrial2.BackColor = TrialOrderColor) Then
'''''''''''                    TrialType = "~"
'''''''''''                ElseIf (lblTrial2.BackColor = TrialNormalColor) Then
'''''''''''                    TrialType = "*"
'''''''''''                End If
'''''''''''            End If
'''''''''''            txtNote2.Text = ODT + vbCrLf + OST
'''''''''''            lblNote2.BackColor = EnlargeOff
''''''''''            If (IsNoteBig(ODT + vbCrLf + OST)) Then
'''''''''''                lblNote2.BackColor = EnlargeOn
''''''''''            End If
'''''''''''            txtNote2.Visible = True
''''''''''            SelectedCLRx(Idx).RxNote = TrialType + ODT + " " + OST
''''''''''        ElseIf (Idx = 3) Then
''''''''''            TrialType = ""
'''''''''''            If (lblTrial3.Visible) Then
'''''''''''                If (lblTrial3.BackColor = TrialOrderColor) Then
'''''''''''                    TrialType = "~"
'''''''''''                ElseIf (lblTrial3.BackColor = TrialNormalColor) Then
'''''''''''                    TrialType = "*"
'''''''''''                End If
'''''''''''            End If
'''''''''''            txtNote3.Text = ODT + vbCrLf + OST
'''''''''''            lblNote3.BackColor = EnlargeOff
''''''''''            If (IsNoteBig(ODT + vbCrLf + OST)) Then
'''''''''''                lblNote3.BackColor = EnlargeOn
''''''''''            End If
'''''''''''            txtNote3.Visible = True
''''''''''            SelectedCLRx(Idx).RxNote = TrialType + ODT + " " + OST
''''''''''        ElseIf (Idx = 4) Then
''''''''''            TrialType = ""
'''''''''''            If (lblTrial4.Visible) Then
'''''''''''                If (lblTrial4.BackColor = TrialOrderColor) Then
'''''''''''                    TrialType = "~"
'''''''''''                ElseIf (lblTrial4.BackColor = TrialNormalColor) Then
'''''''''''                    TrialType = "*"
'''''''''''                End If
'''''''''''            End If
'''''''''''            txtNote4.Text = ODT + vbCrLf + OST
'''''''''''            lblNote4.BackColor = EnlargeOff
''''''''''            If (IsNoteBig(ODT + vbCrLf + OST)) Then
'''''''''''                lblNote4.BackColor = EnlargeOn
''''''''''            End If
'''''''''''            txtNote4.Visible = True
''''''''''            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
''''''''''        ElseIf (Idx = 5) Then
''''''''''            TrialType = ""
'''''''''''            If (lblTrial5.Visible) Then
'''''''''''                If (lblTrial5.BackColor = TrialOrderColor) Then
'''''''''''                    TrialType = "~"
'''''''''''                ElseIf (lblTrial5.BackColor = TrialNormalColor) Then
'''''''''''                    TrialType = "*"
'''''''''''                End If
'''''''''''            End If
'''''''''''            txtNote5.Text = ODT + vbCrLf + OST
'''''''''''            lblNote1.BackColor = EnlargeOff
''''''''''            If (IsNoteBig(ODT + vbCrLf + OST)) Then
'''''''''''                lblNote1.BackColor = EnlargeOn
''''''''''            End If
'''''''''''            txtNote5.Visible = True
''''''''''            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
''''''''''        ElseIf (Idx = 6) Then
''''''''''            TrialType = ""
'''''''''''            If (lblTrial6.Visible) Then
'''''''''''                If (lblTrial6.BackColor = TrialOrderColor) Then
'''''''''''                    TrialType = "~"
'''''''''''                ElseIf (lblTrial6.BackColor = TrialNormalColor) Then
'''''''''''                    TrialType = "*"
'''''''''''                End If
'''''''''''            End If
'''''''''''            txtNote6.Text = ODT + vbCrLf + OST
'''''''''''            lblNote2.BackColor = EnlargeOff
''''''''''            If (IsNoteBig(ODT + vbCrLf + OST)) Then
'''''''''''                lblNote2.BackColor = EnlargeOn
''''''''''            End If
'''''''''''            txtNote6.Visible = True
''''''''''            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
''''''''''        ElseIf (Idx = 7) Then
''''''''''            TrialType = ""
'''''''''''            If (lblTrial7.Visible) Then
'''''''''''                If (lblTrial7.BackColor = TrialOrderColor) Then
'''''''''''                    TrialType = "~"
'''''''''''                ElseIf (lblTrial7.BackColor = TrialNormalColor) Then
'''''''''''                    TrialType = "*"
'''''''''''                End If
'''''''''''            End If
'''''''''''            txtNote7.Text = ODT + vbCrLf + OST
'''''''''''            lblNote3.BackColor = EnlargeOff
''''''''''            If (IsNoteBig(ODT + vbCrLf + OST)) Then
'''''''''''                lblNote3.BackColor = EnlargeOn
''''''''''            End If
'''''''''''            txtNote7.Visible = True
''''''''''            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
''''''''''        ElseIf (Idx = 8) Then
''''''''''            TrialType = ""
'''''''''''            If (lblTrial8.Visible) Then
'''''''''''                If (lblTrial8.BackColor = TrialOrderColor) Then
'''''''''''                    TrialType = "~"
'''''''''''                ElseIf (lblTrial8.BackColor = TrialNormalColor) Then
'''''''''''                    TrialType = "*"
'''''''''''                End If
'''''''''''            End If
'''''''''''            txtNote8.Text = ODT + vbCrLf + OST
'''''''''''            lblNote4.BackColor = EnlargeOff
''''''''''            If (IsNoteBig(ODT + vbCrLf + OST)) Then
'''''''''''                lblNote4.BackColor = EnlargeOn
''''''''''            End If
'''''''''''            txtNote8.Visible = True
''''''''''            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
''''''''''        End If
''''''''''    End If
''''''''''End If
End Sub

Private Sub SetPairs(TheText As String)
Dim u As Integer
Dim p As Integer
Dim i As Integer
Dim TheLen As Integer
Dim ODT As String
Dim OST As String
Dim ATemp As String
Dim Temp(MaxSetCL) As String
Dim TheFile As String
ATemp = ""
For i = 1 To MaxSetCL
    Temp(i) = ""
Next i
If (Trim(TheText) <> "") Then
    u = InStrPS(TheText, "Pair 1")
    If (u > 0) Then
        p = InStrPS(u, TheText, "Pair 2")
        If (p = 0) Then
            p = Len(TheText) - (u + 8)
        End If
        Temp(1) = Mid(TheText, u + 9, p)
    End If
    u = InStrPS(TheText, "Pair 2")
    If (u > 0) Then
        p = InStrPS(u, TheText, "Pair 3")
        If (p = 0) Then
            p = Len(TheText) - (u + 8)
        End If
        Temp(2) = Mid(TheText, u + 9, p)
    End If
    u = InStrPS(TheText, "Pair 3")
    If (u > 0) Then
        p = InStrPS(u, TheText, "Pair 4")
        If (p = 0) Then
            p = Len(TheText) - (u + 8)
        End If
        Temp(3) = Mid(TheText, u + 9, p)
    End If
    u = InStrPS(TheText, "Pair 4")
    If (u > 0) Then
        p = Len(TheText) - (u + 8)
        Temp(4) = Mid(TheText, u + 9, p)
    End If
    For u = 1 To 3
        ATemp = Trim(Temp(u))
        If (ATemp <> "") Then
            i = InStrPS(ATemp, "OS")
            If (i > 0) Then
                ODT = Left(ATemp, i - 1)
                ODT = Trim(Mid(ODT, 4, Len(ODT) - 3))
                OST = Mid(ATemp, i, Len(ATemp) - (i - 1))
                OST = Trim(Mid(OST, 4, Len(OST) - 3))
            Else
                ODT = ATemp
                OST = ""
            End If
            Call StripCharacters(ODT, Chr(10))
            Call StripCharacters(ODT, Chr(13))
            Call StripCharacters(OST, Chr(10))
            Call StripCharacters(OST, Chr(13))
            If (WearType = "PC") Then
                SelectedPCRx(u).ODManifest = ODT
                SelectedPCRx(u).OSManifest = OST
            Else
                SelectedCLRx(u).ODManifest = ODT
                SelectedCLRx(u).OSManifest = OST
            End If
        End If
    Next u
End If
End Sub

Private Function DoEyeWearPrint(Index As Integer, FirstOn As Boolean, LastOn As Boolean, TotalRxs As Integer) As Boolean
Dim p As Integer
Dim k As Integer
Dim CLOn As Boolean
Dim ProceedOn As Boolean
Dim Temp As String
Dim ODRx As String, OSRx As String
Dim RNote As String, TheFile As String
Dim TempFile As String, SigFile As String
Dim LensType As String, LensCompany As String
Dim CLInv As CLInventory
DoEyeWearPrint = False
ODRx = ""
OSRx = ""
RNote = ""
ThePrinter = GetPrinter(PRINTER_Wear)
CLOn = False
If (WearType = "CL") Then
    CLOn = True
End If
If (Index >= 0) Then
    If (WearType = "PC") Then
        ODRx = Trim(SelectedPCRx(Index).ODManifest)
        OSRx = Trim(SelectedPCRx(Index).OSManifest)
        LensType = Trim(SelectedPCRx(Index).EWType)
        LensCompany = Trim(SelectedPCRx(Index).EWCompany)
        RNote = Trim(SelectedPCRx(Index).RxNote)
    Else
        ODRx = Trim(SelectedCLRx(Index).ODManifest)
        OSRx = Trim(SelectedCLRx(Index).OSManifest)
        If (SelectedCLRx(Index).InventoryIdOD > 0) Then
            Call GetInventoryItem(SelectedCLRx(Index).InventoryIdOD, LensType)
        Else
            LensType = Trim(SelectedCLRx(Index).EWType)
        End If
        If (SelectedCLRx(Index).InventoryIdOS > 0) Then
            Call GetInventoryItem(SelectedCLRx(Index).InventoryIdOD, LensCompany)
        Else
            LensCompany = Trim(SelectedCLRx(Index).EWCompany)
        End If
        RNote = Trim(SelectedCLRx(Index).RxNote)
        Call ReplaceCharacters(RNote, vbCrLf, "-&-")
        If (Mid(RNote, 1, 1) = "~") Then
            RNote = "Order Trial:" + Trim(Mid(RNote, 2, Len(RNote) - 1))
        ElseIf (Mid(RNote, 1, 1) = "*") Then
            RNote = "Trial:" + Trim(Mid(RNote, 2, Len(RNote) - 1))
        ElseIf (Mid(RNote, 1, 1) = "^") Then
            RNote = Trim(Mid(RNote, 2, Len(RNote) - 1))
        End If
    End If
    ProceedOn = True
    If (Trim(RNote) <> "") Then
        If (Left(RNote, 5) = "Trial") Then
            ProceedOn = False
        ElseIf (Left(RNote, 11) = "Order Trial") Then
            ProceedOn = False
        End If
    End If
    If (PID = "D") Then
        If (WearType = "PC") Then
            TheFile = DoctorInterfaceDirectory + "EyeWearRx" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        Else
            TheFile = DoctorInterfaceDirectory + "CLRx" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        End If
    Else
        If (WearType = "PC") Then
            TheFile = SchedulerInterfaceDirectory + "EyeWearRx" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        Else
            TheFile = DoctorInterfaceDirectory + "CLRx" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        End If
    End If
    If (FirstOn) Then
        If (FM.IsFileThere(TheFile)) Then
            Call KillDocumentProcessing(TheFile, True, True)
        End If
    End If
    If (WearType = "PC") Then
        TempFile = TemplateDirectory + "EyeWearRx.doc"
    Else
        TempFile = TemplateDirectory + "CLRx.doc"
    End If
    If (ProceedOn) Then
        If Not (FM.IsFileThere(TheFile)) Then
            FirstOn = True
        End If
        'If (CreateEyeWear(PatientId, AppointmentId, ActivityId, TheFile, ODRx, OSRx, LensType, LensCompany, RNote, CLOn, FirstOn, LastOn)) Then
            DoEyeWearPrint = True
            DoEvents
            If (LastOn) Then
                Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
' use a special signature file setup for Rxs
                If (Trim(SigFile) <> "") Then
                    p = InStrPS(SigFile, ".")
                    If (p > 0) Then
                        SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
                    End If
                End If
                Call PrintTemplate(TheFile, TempFile, True, True, 1, ThePrinter, SigFile, TotalRxs, True)
            End If
        'End If
    Else
        DoEvents
        If (LastOn) Then
            Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
' use a special signature file setup for Rxs
            If (Trim(SigFile) <> "") Then
                p = InStrPS(SigFile, ".")
                If (p > 0) Then
                    SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
                End If
            End If
            If (FM.IsFileThere(TheFile)) Then
                Call PrintTemplate(TheFile, TempFile, True, True, 1, ThePrinter, SigFile, TotalRxs, True)
            End If
        End If
    End If
End If
End Function

Private Function CreateEyeWear(PatId As Long, ApptId As Long, ActId As Long, ResultFile As String, info As String, LensType As String, LensCompany As String, ANote As String, CLOn As Boolean, FirstOn As Boolean, LastOn As Boolean) As Boolean
Dim FileNum As Integer, Idx As Integer
Dim i As Integer, q As Integer
Dim u As Integer, w As Integer
Dim RscId As Long, RscId1 As Long
Dim KeyWords(5)
Dim TName As String, ATemp As String
Dim Temp As String, ADate As String
Dim NewFile As String, TempZip As String
Dim Resultfeild As String
Dim Resultvalue As String
Dim resultstring As String
Dim RetAct As PracticeActivity
Dim RetAppt As SchedulerAppointment
Dim RetDr As SchedulerResource
Dim RetPrc As PracticeName
Dim RetPat As Patient
ADate = ""
Dim ATmp As String

CreateEyeWear = False
lstBox.Clear
KeyWords(1) = "ADD-READING:"
KeyWords(2) = "ADD-INTERMEDIATE:"
KeyWords(3) = "PRISM OF ANGLE 1:"
KeyWords(4) = "PRISM OF ANGLE 2:"
KeyWords(5) = "Vertex dist:"
If (CLOn) Then
    KeyWords(1) = "ADD-READING:"
    KeyWords(2) = "BASE CURVE:"
    KeyWords(3) = "DIAMETER:"
    KeyWords(4) = "PERIPH CURVE:"
    KeyWords(5) = ""
End If
If (PatId > 0) And (ApptId > 0) Then
'    If (InStrPS(UCase(ODRx), "ANGLE1") <> 0) Then
'        Call ReplaceCharacters(UCase(ODRx), "ANGLE1", "ANGLE 1")
'    ElseIf (InStrPS(UCase(ODRx), "ANGLE2") <> 0) Then
'        Call ReplaceCharacters(UCase(ODRx), "ANGLE2", "ANGLE 2")
'    End If
'    If (InStrPS(UCase(OSRx), "ANGLE1") <> 0) Then
'        Call ReplaceCharacters(UCase(OSRx), "ANGLE1", "ANGLE 1")
'    ElseIf (InStrPS(UCase(OSRx), "ANGLE2") <> 0) Then
'        Call ReplaceCharacters(UCase(OSRx), "ANGLE2", "ANGLE 2")
'    End If
    Set RetPat = New Patient
    Set RetPrc = New PracticeName
    Set RetDr = New SchedulerResource
    Set RetAct = New PracticeActivity
    Set RetAppt = New SchedulerAppointment
    RetPat.PatientId = PatId
    Call RetPat.RetrievePatient
    
    RscId1 = 0
    If (ActId > 0) Then
        RetAct.ActivityId = ActId
        If (RetAct.RetrieveActivity) Then
            If (RetAct.ActivityCurrResId > 0) Then
                RetDr.ResourceId = RetAct.ActivityCurrResId
                If (RetDr.RetrieveSchedulerResource) Then
                    If (Trim(RetDr.ResourceSignatureFile) <> "") Then
                        RscId1 = RetDr.ResourceId
                    End If
                End If
            End If
        End If
    End If
    If (RscId1 < 1) Then
        RetAppt.AppointmentId = ApptId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            If (RetAppt.AppointmentResourceId1 > 0) Then
                RetDr.ResourceId = RetAppt.AppointmentResourceId1
                If (RetDr.RetrieveSchedulerResource) Then
                
                End If
            End If
        End If
    End If
' Load the Merge File
    lstBox.Clear
    If (RetDr.ResourceId > 0) Then
        If (Trim(RetDr.ResourceDescription) <> "") Then
            ATmp = (Trim(RetDr.ResourceDescription))
            Call ConvertOldZeros(ATmp)
            lstBox.AddItem "01:" + ATmp
        Else
            lstBox.AddItem "01:  "
        End If
        If (Trim(RetDr.ResourceLic) <> "") Then
            ATmp = (Trim(RetDr.ResourceLic))
            Call ConvertOldZeros(ATmp)
            lstBox.AddItem "02:" + ATmp
        Else
            lstBox.AddItem "02:  "
        End If
        If (RetDr.PracticeId > 0) Then
            RetPrc.PracticeId = RetDr.PracticeId
            If (RetPrc.RetrievePracticeName) Then
                ATmp = (Trim(RetPrc.PracticeAddress))
                Call ConvertOldZeros(ATmp)
                lstBox.AddItem "03:" + ATmp
                Temp = Trim(RetPrc.PracticeCity) + ", " + RetPrc.PracticeState + " " + RetPrc.PracticeZip
                ATmp = (Temp)
                Call ConvertOldZeros(ATmp)
                lstBox.AddItem "04:" + ATmp
                TempZip = ""
                Call DisplayPhone(RetPrc.PracticePhone, TempZip)
                ATmp = (TempZip)
                Call ConvertOldZeros(ATmp)
                lstBox.AddItem "05:" + ATmp
            Else
                lstBox.AddItem "03:    "
                lstBox.AddItem "04:    "
                lstBox.AddItem "05:    "
            End If
        Else
            lstBox.AddItem "03:    "
            lstBox.AddItem "04:    "
            lstBox.AddItem "05     "
        End If
    Else
        lstBox.AddItem "01:    "
        lstBox.AddItem "02:    "
        lstBox.AddItem "03:    "
        lstBox.AddItem "04:    "
        lstBox.AddItem "05:    "
    End If
    Call FormatTodaysDate(ADate, False)
    ATmp = (ADate)
    Call ConvertOldZeros(ATmp)
    lstBox.AddItem "06:" + ATmp
    Temp = Trim(Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial) + " " + Trim(RetPat.LastName) + " " + Trim(RetPat.NameRef))
    Call StripCharacters(Temp, Chr(34))
    ATmp = (Temp)
    Call ConvertOldZeros(ATmp)
    lstBox.AddItem "07:" + ATmp
    lstBox.AddItem "04a:" + Trim(RetPat.Address)
    Call DisplayZip(RetPat.Zip, Temp)
    ATmp = (Trim(RetPat.City) + ", " + Trim(RetPat.State) + " " + Trim(Temp))
    Call ConvertOldZeros(ATmp)
    lstBox.AddItem "04b:" + ATmp
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'OD
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 2))
        Select Case UCase(ATmp)
        Case "BAL"
            ATmp = "Balance Lens"
        End Select
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "07a:    " + ATmp
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 3))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "08:    " + ATmp
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 4))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "08a:   " + ATmp
        
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 6))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "16:" + ATmp
        
        If CLOn Then
            ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 8))
        Else
            ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 7))
        End If
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "09:" + ATmp
        
        
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 9))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "09a:" + ATmp
        
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 10))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "17:" + ATmp
    
        lstBox.AddItem "09b:    "
        lstBox.AddItem "10:    "
        
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 11))
        If CLOn Then
            Select Case ATmp
            Case "D"
                ATmp = "OD: Dominant"
            Case "N"
                ATmp = "OD: Normal"
            Case Else
                ATmp = ""
            End Select
        End If
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "23:" + ATmp
    

'OS
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 2))
        Select Case UCase(ATmp)
        Case "BAL"
            ATmp = "Balance Lens"
        End Select
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "11:    " + ATmp
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 3))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "11a:    " + ATmp
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 4))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "11b:   " + ATmp
            
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 6))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "18:" + ATmp
        
        If CLOn Then
            ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 8))
        Else
            ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 7))
        End If
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "12:" + ATmp
        
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 9))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "13:" + ATmp
        
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 10))
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "19:" + ATmp
        
        ATmp = (lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 11))
        If CLOn Then
            Select Case ATmp
            Case "D"
                ATmp = "OS: Dominant"
            Case "N"
                ATmp = "OS: Normal"
            Case Else
                ATmp = ""
            End Select
        End If
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "24:" + ATmp
'''''''''
        lstBox.AddItem "14:    "
        lstBox.AddItem "15:    "
        lstBox.AddItem "18a:    "
        lstBox.AddItem "18b:    "
        lstBox.AddItem "18c:    "
        lstBox.AddItem "18d:    "
        lstBox.AddItem "18e:    "
'''''''''
        'type
        Dim tmp As String
        tmp = Trim(lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 12))
        Call ConvertOldZeros(ATmp)
        If tmp = "" Then
            lstBox.AddItem "21:    "
        Else
            lstBox.AddItem "21:    " & tmp
        End If
        If CLOn Then
            tmp = Trim(lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 12))
        Else
            tmp = ""
        End If
        If tmp = "" Then
            lstBox.AddItem "20:    "
        Else
            lstBox.AddItem "20:    " & tmp
        End If
        '''''
        'comm
        tmp = Trim(lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2), 13))
        If CLOn Then
            If tmp = "" Then
                ATmp = ""
            Else
                ATmp = "OD: " & tmp
            End If
            tmp = Trim(lstRxs(Left(info, 1)).TextMatrix(Mid(info, 2) + 1, 13))
            If Not tmp = "" Then
                ATmp = ATmp & " OS: " & tmp
            End If
            tmp = Trim(ATmp)
        End If
        If tmp = "" Then
            lstBox.AddItem "22:    "
        Else
            lstBox.AddItem "22:    " & tmp
        End If
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    If (Trim(RetPat.BirthDate) <> "") Then
        ADate = Mid(RetPat.BirthDate, 5, 2) + "/" + Mid(RetPat.BirthDate, 7, 2) + "/" + Left(RetPat.BirthDate, 4)
        ATmp = (ADate)
        Call ConvertOldZeros(ATmp)
        lstBox.AddItem "25:" + ATmp
    Else
        lstBox.AddItem "25:    "
    End If
' Add the appointment date
    ADate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
    ATmp = (ADate)
    Call ConvertOldZeros(ATmp)
    lstBox.AddItem "26:" + ATmp
    
    lstBox.AddItem "27:" & PatientId
    
    lstBox.AddItem "28:   "
    lstBox.AddItem "29:   "
    lstBox.AddItem "30:   "
    lstBox.AddItem "31:   "
    lstBox.AddItem "32:   "
    lstBox.AddItem "33:   "
    lstBox.AddItem "34:   "
    lstBox.AddItem "35:   "
    lstBox.AddItem "36:   "
    lstBox.AddItem "37:   "
    lstBox.AddItem "38:   "
    lstBox.AddItem "39:   "
    lstBox.AddItem "40:   "
    lstBox.AddItem "41:   "
    lstBox.AddItem "42:   "
    lstBox.AddItem "43:   "
    lstBox.AddItem "44:   "
    lstBox.AddItem "45:   "
    lstBox.AddItem "46:   "
    lstBox.AddItem "47:   "
    lstBox.AddItem "48:   "
    lstBox.AddItem "49:   "
    lstBox.AddItem "50:   "
    lstBox.AddItem "51:   "
    lstBox.AddItem "52:   "
    lstBox.AddItem "53:   "
    lstBox.AddItem "54:   "
    lstBox.AddItem "55:   "
    lstBox.AddItem "56:   "
    lstBox.AddItem "57:   "
    lstBox.AddItem "58:   "
    lstBox.AddItem "59:   "
    lstBox.AddItem "60:   "
    lstBox.AddItem "61:   "
    lstBox.AddItem "62:   "
    lstBox.AddItem "63:   "
    lstBox.AddItem "64:   "
    lstBox.AddItem "65:   "
    lstBox.AddItem "66:   "
    lstBox.AddItem "67:   "
    lstBox.AddItem "68:   "
    lstBox.AddItem "69:   "
    lstBox.AddItem "05a:    "
    lstBox.AddItem "06a:    "
    lstBox.AddItem "18f:    "
    If (FirstOn) Then
        If (FM.IsFileThere(ResultFile)) Then
            FM.Kill ResultFile
        End If
        FM.FileCopy TemplateDirectory + "Letters.txt", ResultFile
        FileNum = FreeFile
        FM.OpenFile ResultFile, FileOpenMode.Binary, FileAccess.ReadWrite, CLng(FileNum)
        FM.CloseFile CLng(FileNum)
    Else
        FileNum = FreeFile
    End If
    FM.OpenFile ResultFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
    For i = 0 To lstBox.ListCount - 1
    
        q = InStrPS(lstBox.List(i), ":")
        If (q > 0) Then
        
         If Resultvalue = "" Then
         
         
            
             Resultvalue = Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q) & vbTab
            Call ReplaceCharacters(Resultvalue, vbCrLf, "-&-")
             Else
                Call ReplaceCharacters(Resultvalue, vbCrLf, "-&-")
                Resultvalue = Resultvalue & Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q) & vbTab
            
             End If
                
            Temp = Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q)
            Call ReplaceCharacters(Temp, vbCrLf, "-&-")
            Print #FileNum, Temp; vbTab;
        End If
        
    Next i
    
    
    Print #FileNum, ""
    
    FM.CloseFile CLng(FileNum)
    
    
         Call createresultfield(Resultfeild)
         
          resultstring = Resultfeild & vbCrLf & Resultvalue
    
    Set RetAct = Nothing
    Set RetAppt = Nothing
    Set RetPat = Nothing
    Set RetPrc = Nothing
    Set RetDr = Nothing
    If (LastOn) Then
    
    
        NewFile = Left(ResultFile, Len(ResultFile) - 4) + ".xls"
'        If (FM.IsFileThere(NewFile)) Then
'            Kill NewFile
'        End If
'        Name ResultFile As NewFile
      '  ResultFile = NewFile
       ResultFile = resultstring & "@" & NewFile
    End If
    CreateEyeWear = True
End If
End Function
Private Sub createresultfield(Resultfeild As String)

        Resultfeild = ":1:" & vbTab & ":2:" & vbTab & ":3:" & vbTab & ":4:" & vbTab & ":4a:" & vbTab & ":4b:" & vbTab & ":5:" & vbTab & ":5a:" & vbTab & ":6:" & vbTab & ":6a:" & vbTab & ":7:" & vbTab & ":7a:" & vbTab & ":8:" & vbTab & ":8a:" & vbTab & ":9:" & vbTab & ":9a:" & vbTab & ":9b:" & vbTab & ":10:" & vbTab & ":11:" & vbTab & ":11a:" & vbTab & ":11b:" & vbTab & ":12:" & vbTab & ":13:" & vbTab & ":14:" & vbTab & ":15:" & vbTab & ":16:" & vbTab & ":17:" & vbTab & ":18:" & vbTab & ":18a:" & vbTab & ":18b:" & vbTab & ":18c:" & vbTab & ":18d:" & vbTab & ":18e:" & vbTab & ":18f:" & vbTab & ":19:" & vbTab & ":20:" & vbTab & ":21:" & vbTab & ":22:" & vbTab & ":23:" & vbTab & ":24:" & vbTab & ":25:" & vbTab & ":26:" & vbTab & ":27:" & vbTab & ":28:" & vbTab & ":29:"
        Resultfeild = Resultfeild + vbTab & ":30:" & vbTab & ":31:" & vbTab & ":32:" & vbTab & ":33:" & vbTab & ":34:" & vbTab & ":35:" & vbTab & ":36:" & vbTab & ":37:" & vbTab & ":38:" & vbTab & ":39:" & vbTab & ":40:" & vbTab & ":41:" & vbTab & ":42:" & vbTab & ":43:" & vbTab & ":44:" & vbTab & ":45:" & vbTab & ":46:" & vbTab & ":47:" & vbTab & ":48:" & vbTab & ":49:" & vbTab & ":50:" & vbTab & ":51:" & vbTab & ":52:" & vbTab & ":53:" & vbTab & ":54:" & vbTab & ":55:" & vbTab & ":56:" & vbTab & ":57:" & vbTab & ":58:" & vbTab & ":59:" & vbTab & ":60:" & vbTab & ":61:" & vbTab & ":62:" & vbTab & ":63:" & vbTab & ":64:" & vbTab & ":65:" & vbTab & ":66:" & vbTab & ":67:" & vbTab & ":68:" & vbTab & ":69:" & vbTab & ":70:" & vbTab & ":70a:"
        Resultfeild = Resultfeild + vbTab & ":71:" & vbTab & ":71a:" & vbTab & ":72:" & vbTab & ":72a:" & vbTab & ":73:" & vbTab & ":73a:" & vbTab & ":74:" & vbTab & ":74a:" & vbTab & ":75:" & vbTab & ":75a:" & vbTab & ":76:" & vbTab & ":76a:" & vbTab & ":77:" & vbTab & ":77a:" & vbTab & ":78:" & vbTab & ":78a:" & vbTab & ":79:" & vbTab & ":79a:" & vbTab & ":80:" & vbTab & ":80a:" & vbTab & ":81:" & vbTab & ":81a:" & vbTab & ":82:" & vbTab & ":82a:" & vbTab & ":83:" & vbTab & ":83a:" & vbTab & ":84:" & vbTab & ":84a:" & vbTab & ":85:" & vbTab & ":85a:" & vbTab & ":86:" & vbTab & ":86a:" & vbTab & ":87:" & vbTab & ":87a:" & vbTab & ":88:" & vbTab & ":88a:" & vbTab & ":89:" & vbTab & ":89a:" & vbTab & ":90:" & vbTab & ":90a:" & vbTab & ":91:" & vbTab & ":91a:" & vbTab & ":92:" & vbTab & ":92a:"
        Resultfeild = Resultfeild + vbTab & ":93:" & vbTab & ":93a" & vbTab & ":94:" & vbTab & ":94a:" & vbTab & ":95:" & vbTab & ":95a:" & vbTab & ":96:" & vbTab & ":96a:" & vbTab & ":97:" & vbTab & ":97a:" & vbTab & ":98:" & vbTab & ":98a:" & vbTab & ":99:" & vbTab & ":99a:" & vbTab & ":100:" & vbTab & ":100a:" & vbTab & ":101:" & vbTab & ":101a" & vbTab & ":102:" & vbTab & ":102a:" & vbTab & ":103:" & vbTab & ":103a" & vbTab & ":104:" & vbTab & ":104a" & vbTab & ":105:" & vbTab & ":105a" & vbTab & ":106:" & vbTab & ":106a:" & vbTab & ":107:" & vbTab & ":107a" & vbTab & ":108:" & vbTab & ":108a:" & vbTab & ":109:" & vbTab & ":109a:" & vbTab & ":110:" & vbTab & ":110a:" & vbTab & ":111:" & vbTab & ":111a:" & vbTab & ":112:" & vbTab & ":112a:" & vbTab & ":113:" & vbTab & ":113a:" & vbTab & ":114:" & vbTab & ":114a:" & vbTab & ":115:" & vbTab & ":115a:" & vbTab & ":116:" & vbTab & ":116a:" & vbTab & ":117:" & vbTab & ":117a:" & vbTab & ":118:" & vbTab & ":118a:" & vbTab & ":119:" & vbTab & ":119a:"
        Resultfeild = Resultfeild + vbTab & ":120:" & vbTab & ":120a:" & vbTab & ":121:" & vbTab & ":121a:" & vbTab & ":122:" & vbTab & ":122a:" & vbTab & ":123:" & vbTab & ":123a:" & vbTab & ":124:" & vbTab & ":124a:" & vbTab & ":125:" & vbTab & ":125a:" & vbTab & ":126:" & vbTab & ":126a:" & vbTab & ":127:" & vbTab & ":127a:" & vbTab & ":128:" & vbTab & ":128a:" & vbTab & ":129:" & vbTab & ":129a:" & vbTab & ":130:" & vbTab & ":130a:" & vbTab & ":131:" & vbTab & ":131a:" & vbTab & ":132:" & vbTab & ":132a:" & vbTab & ":133:" & vbTab & ":133a:" & vbTab & ":134:" & vbTab & ":134a:" & vbTab & ":135:" & vbTab & ":136:" & vbTab & ":137:" & vbTab & ":138:" & vbTab & ":139:" & vbTab & ":140:" & vbTab & ":141:" & vbTab & ":142:" & vbTab & ":143:" & vbTab & ":144:" & vbTab & ":145:" & vbTab & ":146:"
        Resultfeild = Resultfeild + vbTab & ":147:" & vbTab & ":148:" & vbTab & ":149:" & vbTab & ":150:" & vbTab & ":151:" & vbTab & ":152:" & vbTab & ":153:" & vbTab & ":154:" & vbTab & ":155:" & vbTab & ":156:" & vbTab & ":157:" & vbTab & ":158:" & vbTab & ":159:" & vbTab & ":160:" & vbTab & ":161:" & vbTab & ":162:" & vbTab & ":163:" & vbTab & ":164:" & vbTab & ":165:" & vbTab & ":166:" & vbTab & ":167:" & vbTab & ":168:" & vbTab & ":169:" & vbTab & ":170:" & vbTab & ":171:" & vbTab & ":172:" & vbTab & ":173:"

End Sub


Private Function TriggerRx(ALabel As Label, Idx As Integer) As Boolean
Dim j As Integer
Dim IType As Integer
Dim AEye As String
Dim ATemp As String
Dim MyRef As String
Dim ODInvIdNew As Long
Dim OSInvIdNew As Long
Dim ODInvIdDesc As String
Dim OSInvIdDesc As String
Dim InventoryOn As Boolean
If (Trim(ALabel.Caption) = "") Then
    CurrentIndex = Idx
    If (WearType = "PC") Then
'        Call LoadCL(WearType)
    End If
Else
    If (WearType = "CL") Then
        InventoryOn = Not (CheckConfigCollection("INVENTORYON") = "F")
        frmEventMsgs.Header = "Action"
        frmEventMsgs.AcceptText = "CL Favorite"
        frmEventMsgs.RejectText = "CL Search"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Print"
        frmEventMsgs.Other1Text = "Remove RX"
        frmEventMsgs.Other2Text = "Remove Lens"
        MyRef = ""
        If (WearType = "CL") Then
            MyRef = "Trial"
            If (Idx = 1) Then
'                If (lblTrial1.Visible) Then
                    MyRef = "Change    to RX"
'                End If
            ElseIf (Idx = 2) Then
'                If (lblTrial2.Visible) Then
                    MyRef = "Change    to RX"
'                End If
            ElseIf (Idx = 3) Then
'                If (lblTrial3.Visible) Then
                    MyRef = "Change    to RX"
'                End If
            ElseIf (Idx = 4) Then
'                If (lblTrial4.Visible) Then
                    MyRef = "Change    to RX"
'                End If
            ElseIf (Idx = 5) Then
'                If (lblTrial5.Visible) Then
                    MyRef = "Change    to RX"
'                End If
            ElseIf (Idx = 6) Then
'                If (lblTrial6.Visible) Then
                    MyRef = "Change    to RX"
'                End If
            ElseIf (Idx = 7) Then
'                If (lblTrial7.Visible) Then
                    MyRef = "Change    to RX"
'                End If
            ElseIf (Idx = 8) Then
'                If (lblTrial8.Visible) Then
                    MyRef = "Change    to RX"
'                End If
            End If
        End If
        frmEventMsgs.Other3Text = MyRef
        frmEventMsgs.Other4Text = ""
        If (WearType = "CL") Then
            If (Idx = 1) Then
'                If (lblTrial1.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
'                End If
            ElseIf (Idx = 2) Then
'                If (lblTrial2.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
'                End If
            ElseIf (Idx = 3) Then
'                If (lblTrial3.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
'                End If
            ElseIf (Idx = 4) Then
'                If (lblTrial4.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
'                End If
            ElseIf (Idx = 5) Then
'                If (lblTrial5.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
'                End If
            ElseIf (Idx = 6) Then
'                If (lblTrial6.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
'                End If
            ElseIf (Idx = 7) Then
'                If (lblTrial7.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
'                End If
            ElseIf (Idx = 8) Then
'                If (lblTrial8.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
'                End If
            End If
        End If
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Or (frmEventMsgs.Result = 2) Then
            IType = frmEventMsgs.Result
            frmEventMsgs.Header = "Eye"
            frmEventMsgs.AcceptText = "OD"
            frmEventMsgs.RejectText = "OS"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = "OU"
            frmEventMsgs.Other1Text = "Clear"
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            AEye = ""
            If (frmEventMsgs.Result = 1) Then
                AEye = "OD"
            ElseIf (frmEventMsgs.Result = 2) Then
                AEye = "OS"
            ElseIf (frmEventMsgs.Result = 3) Then
                AEye = "OU"
            ElseIf (frmEventMsgs.Result = 5) Then
                SelectedCLRx(Idx).EWCompany = ""
                SelectedCLRx(Idx).EWType = ""
                SelectedCLRx(Idx).InventoryIdOD = 0
                SelectedCLRx(Idx).InventoryIdOS = 0
                Call LoadWear
                Exit Function
            End If
            If (IType = 5) Then ' old way no longer in use
                frmCLWear.PatientId = PatientId
                frmCLWear.AppointmentId = AppointmentId
                frmCLWear.CLInventoryId = 0
                frmCLWear.CLPrescription = ""
                frmCLWear.EyeContext = AEye
                frmCLWear.AddReading = ""
                frmCLWear.BaseCurve = ""
                frmCLWear.PeriphCurve = ""
                frmCLWear.Diameter = ""
                frmCLWear.Show 1
                If (frmCLWear.CLInventoryId > 0) And (Trim(frmCLWear.CLPrescription) <> "") Then
                    If (frmCLWear.EyeContext = "OU") Then
                        SelectedCLRx(Idx).EWType = frmCLWear.CLPrescription
                        Mid(SelectedCLRx(Idx).EWType, 2, 1) = "D"
                        SelectedCLRx(Idx).InventoryIdOD = frmCLWear.CLInventoryId
                        SelectedCLRx(Idx).EWCompany = frmCLWear.CLPrescription
                        Mid(SelectedCLRx(Idx).EWCompany, 2, 1) = "S"
                        SelectedCLRx(Idx).InventoryIdOS = frmCLWear.CLInventoryId
                    ElseIf (frmCLWear.EyeContext = "OD") Then
                        SelectedCLRx(Idx).EWType = frmCLWear.CLPrescription
                        SelectedCLRx(Idx).InventoryIdOD = frmCLWear.CLInventoryId
                    ElseIf (frmCLWear.EyeContext = "OS") Then
                        SelectedCLRx(Idx).EWCompany = frmCLWear.CLPrescription
                        SelectedCLRx(Idx).InventoryIdOS = frmCLWear.CLInventoryId
                    End If
                    Call LoadWear
                End If
            ElseIf (IType = 2) Then
                frmCLSearch.Show 1
                If (frmCLSearch.InventoryId > 0) And (Trim(frmCLSearch.InventoryDescription) <> "") Then
                    If (AEye = "OU") Then
                        SelectedCLRx(Idx).EWType = "OD:" + frmCLSearch.InventoryDescription
                        SelectedCLRx(Idx).InventoryIdOD = frmCLSearch.InventoryId
                        SelectedCLRx(Idx).EWCompany = "OS:" + frmCLSearch.InventoryDescription
                        SelectedCLRx(Idx).InventoryIdOS = frmCLSearch.InventoryId
                    ElseIf (AEye = "OD") Then
                        SelectedCLRx(Idx).EWType = "OD:" + frmCLSearch.InventoryDescription
                        SelectedCLRx(Idx).InventoryIdOD = frmCLSearch.InventoryId
                    ElseIf (AEye = "OS") Then
                        SelectedCLRx(Idx).EWCompany = "OS:" + frmCLSearch.InventoryDescription
                        SelectedCLRx(Idx).InventoryIdOS = frmCLSearch.InventoryId
                    End If
                    Call LoadWear
                End If
            ElseIf (IType = 1) Then
                If (frmCLFavs.LoadCLFavorites(True)) Then
                    frmCLFavs.Show 1
                    If (frmCLFavs.InventoryId > 0) And (Trim(frmCLFavs.InventoryDescription) <> "") Then
                        Call GetExactInvItem(Idx, AEye, frmCLFavs.InventoryId, ODInvIdNew, ODInvIdDesc, OSInvIdNew, OSInvIdDesc)
                        If (AEye = "OU") Then
                            If (ODInvIdNew < 1) Then
                                If Not (UseFavorite("OD")) Then
                                    Exit Function
                                End If
                            End If
                            If (OSInvIdNew < 1) Then
                                If Not (UseFavorite("OS")) Then
                                    Exit Function
                                End If
                            End If
                        ElseIf (AEye = "OD") Then
                            If (ODInvIdNew < 1) Then
                                If Not (UseFavorite("OD")) Then
                                    Exit Function
                                End If
                            End If
                        ElseIf (AEye = "OS") Then
                            If (OSInvIdNew < 1) Then
                                If Not (UseFavorite("OS")) Then
                                    Exit Function
                                End If
                            End If
                        End If
                        If (AEye = "OU") Then
                            If (ODInvIdNew > 0) Then
                                SelectedCLRx(Idx).EWType = "OD:" + ODInvIdDesc
                                SelectedCLRx(Idx).InventoryIdOD = ODInvIdNew
                            Else
                                SelectedCLRx(Idx).EWType = "OD:" + frmCLFavs.InventoryDescription
                                SelectedCLRx(Idx).InventoryIdOD = frmCLFavs.InventoryId
                            End If
                            If (OSInvIdNew > 0) Then
                                SelectedCLRx(Idx).EWCompany = "OS:" + OSInvIdDesc
                                SelectedCLRx(Idx).InventoryIdOS = OSInvIdNew
                            Else
                                SelectedCLRx(Idx).EWCompany = "OS:" + frmCLFavs.InventoryDescription
                                SelectedCLRx(Idx).InventoryIdOS = frmCLFavs.InventoryId
                            End If
                        ElseIf (AEye = "OD") Then
                            If (ODInvIdNew > 0) Then
                                SelectedCLRx(Idx).EWType = "OD:" + ODInvIdDesc
                                SelectedCLRx(Idx).InventoryIdOD = ODInvIdNew
                            Else
                                SelectedCLRx(Idx).EWType = "OD:" + frmCLFavs.InventoryDescription
                                SelectedCLRx(Idx).InventoryIdOD = frmCLFavs.InventoryId
                            End If
                        ElseIf (AEye = "OS") Then
                            If (OSInvIdNew > 0) Then
                                SelectedCLRx(Idx).EWCompany = "OS:" + OSInvIdDesc
                                SelectedCLRx(Idx).InventoryIdOS = OSInvIdNew
                            Else
                                SelectedCLRx(Idx).EWCompany = "OS:" + frmCLFavs.InventoryDescription
                                SelectedCLRx(Idx).InventoryIdOS = frmCLFavs.InventoryId
                            End If
                        End If
                        Call LoadWear
                    End If
                End If
            End If
        ElseIf (frmEventMsgs.Result = 3) Then
            Call DoEyeWearPrint(Idx, True, True, 1)
        ElseIf (frmEventMsgs.Result = 5) Then
            ALabel.Caption = ""
            ALabel.Visible = False
            For j = Idx To MaxSetCL - 1
                SelectedCLRx(j) = SelectedCLRx(j + 1)
            Next j
            SelectedCLRx(MaxSetCL).EWCompany = ""
            SelectedCLRx(MaxSetCL).EWType = ""
            SelectedCLRx(MaxSetCL).ODManifest = ""
            SelectedCLRx(MaxSetCL).OSManifest = ""
            SelectedCLRx(MaxSetCL).RxNote = ""
            CurrRx = CurrRx - 1
            If (CurrRx < 1) Then
                CurrRx = 1
            End If
            CurrRxCnt = CurrRxCnt - 1
            If (CurrRxCnt < 1) Then
                CurrRxCnt = 1
            End If
            Call LoadWear
        ElseIf (frmEventMsgs.Result = 6) Then
            SelectedCLRx(Idx).EWType = ""
            SelectedCLRx(Idx).InventoryIdOD = 0
            SelectedCLRx(Idx).EWCompany = ""
            SelectedCLRx(Idx).InventoryIdOS = 0
            Call LoadWear
        ElseIf (frmEventMsgs.Result = 7) Then
            If (frmEventMsgs.Other3Text = "Change    to RX") Then
                Call DoPrescriptions(Idx, True)
            Else
                Call DoPrescriptions(Idx, False)
            End If
        ElseIf (frmEventMsgs.Result = 8) Then
            Call DoDescriptors(Idx)
        End If
    ElseIf (WearType = "PC") Then
            frmEventMsgs.Header = "Action"
            frmEventMsgs.AcceptText = "Print"
            frmEventMsgs.RejectText = "Remove"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                Call DoEyeWearPrint(Idx, True, True, 1)
            ElseIf (frmEventMsgs.Result = 2) Then
                ALabel.Caption = ""
                ALabel.Visible = False
                For j = Idx To 2
                    SelectedPCRx(j) = SelectedPCRx(j + 1)
                Next j
                SelectedPCRx(3).EWCompany = ""
                SelectedPCRx(3).EWType = ""
                SelectedPCRx(3).ODManifest = ""
                SelectedPCRx(3).OSManifest = ""
                SelectedPCRx(3).RxNote = ""
                If (TotalPCRx < 1) Then
                    Call InitializePCRx
                    Call LoadEyeRx(True)
                Else
                    Call LoadEyeRx(False)
                End If
        Else
' new editting screens
        End If
    End If
End If
End Function

Private Function MakeMrxConvert(AQuestion As String, RTag As String, ATest As Integer, AFile As String, TheFile As String) As Boolean
Dim AForm As Long
Dim ADone As Boolean
Dim TestOn As Boolean
Dim i As Integer, j As Integer
Dim q As Integer, ThePtr As Integer
Dim CTemp As String
Dim OTag As String, ATag As String
Dim ARec As String, Temp As String
Dim ATemp As String, BTemp As String
Dim JTemp As String, TestTime As String
Dim RetCtl As DynamicControls
TestOn = False
OTag = "ContactLens"
ATag = "CE"
AForm = 203
If (RTag = "Glasses") Then
    OTag = "Glasses"
    ATag = "GL"
    AForm = 202
End If
FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(1)
Print #1, "QUESTION=\" + UCase(Trim(AQuestion))
Call FormatTimeNow(TestTime)
Print #1, "TIME=" + TestTime
FM.OpenFile AFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(2)
Line Input #2, ARec
If Not (EOF(2)) Then
    Line Input #2, ARec
    If (ATest = 7) Or (ATest = 8) Then
        Line Input #2, ARec
        If (InStrPS(ARec, "Pair-" + Trim(str(ATest - 6))) <> 0) Then
            TestOn = True
        ElseIf (InStrPS(ARec, "PAIR-" + Trim(str(ATest - 6))) <> 0) Then
            TestOn = True
        End If
    End If
End If
If (ATest = 9) Then
    If (InStrPS(ARec, "ARF-" + Trim(str(ATest - 8))) <> 0) Then
        TestOn = True
    ElseIf (InStrPS(ARec, "Arf-" + Trim(str(ATest - 8))) <> 0) Then
        TestOn = True
    End If
End If
Do Until (EOF(2)) Or (TestOn)
    Line Input #2, ARec
    If (InStrPS(ARec, "MRX-" + Trim(str(ATest))) <> 0) Then
        TestOn = True
    ElseIf (InStrPS(ARec, "RefractCL-" + Trim(str(ATest - 1))) <> 0) Then
        TestOn = True
    ElseIf (InStrPS(ARec, "ARF-" + Trim(str(ATest - 8))) <> 0) Then
        TestOn = True
    ElseIf (InStrPS(ARec, "Pair-" + Trim(str(ATest - 6))) <> 0) Then
        TestOn = True
    End If
Loop
If (TestOn) Then
    ADone = False
    Do Until (EOF(2)) Or (ADone)
        Line Input #2, ARec
        If (ATest < 7) Then
            If (InStrPS(ARec, "MRX") = 0) Or (Left(ARec, 5) = "Recap") Then
                Temp = UCase(ARec)
                Call ReplaceCharacters(Temp, "MANIFEST REFRACTION", UCase(RTag))
                Call ReplaceCharacters(Temp, "MR", ATag)
                GoSub GetRecPtr
                If (ThePtr > 0) Then
                    i = InStrPS(Temp, "=T")
                    If (i > 0) Then
                        j = InStrPS(i, Temp, " ")
                        If (j = 0) Then
                            j = Len(Temp)
                        End If
                        Temp = Left(Temp, i + 1) + Trim(str(ThePtr)) + Mid(Temp, j, Len(Temp) - (j - 1))
                        Print #1, Temp
                    End If
                End If
            Else
                ADone = True
            End If
        ElseIf (ATest = 7) Or (ATest = 8) Then
            If (InStrPS(ARec, "Pair") = 0) Or (Left(ARec, 5) = "Recap") Then
                Temp = UCase(ARec)
                If (RTag <> "ContactLens") Then
                    Call ReplaceCharacters(Temp, "GLASSES", UCase(RTag))
                    Call ReplaceCharacters(Temp, "GL", ATag)
                Else
                    If (Left(Temp, 3) = "*OD") Or (Left(Temp, 3) = "*OS") Then
                        Call ReplaceCharacters(Temp, "CONTACTLENS", UCase(ATag))
                    Else
                        Call ReplaceCharacters(Temp, "CONTACTLENS", UCase(RTag))
                        Call ReplaceCharacters(Temp, "CE", ATag)
                    End If
                End If
                GoSub GetRecPtr
                If (ThePtr > 0) Then
                    i = InStrPS(Temp, "=T")
                    If (i > 0) Then
                        j = InStrPS(i, Temp, " ")
                        If (j = 0) Then
                            j = Len(Temp)
                        End If
                        Temp = Left(Temp, i + 1) + Trim(str(ThePtr)) + Mid(Temp, j, Len(Temp) - (j - 1))
                        Print #1, Temp
                    End If
                End If
            Else
                ADone = True
            End If
        ElseIf (ATest = 9) Then
            If (InStrPS(ARec, "ARF") = 0) Or (Left(ARec, 5) = "Recap") Then
                Temp = UCase(ARec)
                Call ReplaceCharacters(Temp, "AUTOREFRACTION", UCase(RTag))
                Call ReplaceCharacters(Temp, "AUTOREFRACTION", ATag)
                GoSub GetRecPtr
                If (ThePtr > 0) Then
                    i = InStrPS(Temp, "=T")
                    If (i > 0) Then
                        j = InStrPS(i, Temp, " ")
                        If (j = 0) Then
                            j = Len(Temp)
                        End If
                        Temp = Left(Temp, i + 1) + Trim(str(ThePtr)) + Mid(Temp, j, Len(Temp) - (j - 1))
                        Print #1, Temp
                    End If
                End If
            Else
                ADone = True
            End If
        End If
    Loop
End If
FM.CloseFile CLng(1)
FM.CloseFile CLng(2)
Exit Function
GetRecPtr:
    ThePtr = 0
    i = InStrPS(Temp, "-" + UCase(ATag))
    If (i < 1) Then
        i = InStrPS(Temp, "-" + UCase(RTag))
        If (i < 1) Then
            i = InStrPS(Temp, "=T")
            If (i > 0) Then
                Temp = Left(Temp, i - 1) + "-" + RTag + Mid(Temp, i, Len(Temp) - (i - 1))
                Temp = UCase(Temp)
            End If
        End If
    End If
    i = InStrPS(Temp, "=T")
    If (i > 0) Then
        ATemp = Left(Temp, i - 1)
        BTemp = ATemp
        If (Left(ATemp, 1) = "*") Then
            ATemp = Mid(ATemp, 2, Len(ATemp) - 1)
            If (UCase(Left(ATemp, 8)) = "BALANCEL") Then
                BTemp = Left(ATemp, 7) + "-" + Mid(ATemp, 8, Len(ATemp) - 7)
                If (InStrPS(BTemp, "GL") > 0) Then
                    Call ReplaceCharacters(BTemp, "GL", "GLASSES")
                End If
            Else
                BTemp = ATemp
            End If
        End If
        Call StripCharacters(ATemp, " ")
        Call StripCharacters(BTemp, " ")
        CTemp = BTemp
        If (InStrPS(CTemp, "-" + ATag) > 0) Then
            Call ReplaceCharacters(CTemp, "-" + ATag, "-" + UCase(OTag))
        ElseIf (InStrPS(CTemp, "-" + OTag) > 0) Then
            Call ReplaceCharacters(CTemp, "-" + OTag, "-" + UCase(ATag))
        End If
        Set RetCtl = New DynamicControls
        RetCtl.FormId = AForm
        RetCtl.ControlName = ""
        If (RetCtl.FindControl > 0) Then
            q = 1
            Do Until Not (RetCtl.SelectControl(q)) Or (ThePtr > 0)
                JTemp = Trim(RetCtl.ControlName)
                Call StripCharacters(JTemp, " ")
                If (UCase(JTemp) = ATemp) Then
                    ThePtr = RetCtl.ControlId
                    Exit Do
                ElseIf (UCase(JTemp) = BTemp) Then
                    ThePtr = RetCtl.ControlId
                    Exit Do
                ElseIf (UCase(JTemp) = CTemp) Then
                    ThePtr = RetCtl.ControlId
                    Exit Do
                End If
                q = q + 1
            Loop
        End If
        Set RetCtl = Nothing
    End If
    Return
End Function

Public Function PrintEyeWearRx(AType As String, APrinter As String) As Boolean
PrintEyeWearRx = True
If (Trim(AType) <> "") Then
    Call LoadWear
    ThePrinter = APrinter
    If (Trim(ThePrinter) = "") Then
        ThePrinter = "X"
    End If
    Call cmdPrint_Click
End If
' sets trials
'    If (Trim(SelectedCLRx(1).RxNote) <> "") Then
'        Mid(SelectedCLRx(1).RxNote, 1, 1) = "*"
'    Else
'        SelectedCLRx(1).RxNote = "*"
'    End If
End Function

Private Sub cmdRemove_Click()
Dim g As Integer
g = SelFgc("g")
If g < 0 Then Exit Sub

Dim r As Integer
r = SelFgc("r")
lstRxs(g).RemoveItem r
If lstRxs(g).Rows = 1 Then
    lstRxs(g).Rows = 0
Else
    lstRxs(g).RemoveItem r
End If

Call AdjustFGC




'Exit Sub
Dim i As Integer
Dim MyIdx As Integer
    MyIdx = (CurrentEditTest / 2) + 1
    If (WearType = "PC") Then
        For i = MyIdx To CurrRxCnt - 1
            SelectedPCRx(i).EWCompany = SelectedPCRx(i + 1).EWCompany
            SelectedPCRx(i).EWType = SelectedPCRx(i + 1).EWType
            SelectedPCRx(i).InventoryIdOD = SelectedPCRx(i + 1).InventoryIdOD
            SelectedPCRx(i).InventoryIdOS = SelectedPCRx(i + 1).InventoryIdOS
            SelectedPCRx(i).ODManifest = SelectedPCRx(i + 1).ODManifest
            SelectedPCRx(i).OSManifest = SelectedPCRx(i + 1).OSManifest
            SelectedPCRx(i).RxNote = SelectedPCRx(i + 1).RxNote
        Next i
        If (CurrRxCnt > 0) Then
            SelectedPCRx(CurrRxCnt).EWCompany = ""
            SelectedPCRx(CurrRxCnt).EWType = ""
            SelectedPCRx(CurrRxCnt).InventoryIdOD = 0
            SelectedPCRx(CurrRxCnt).InventoryIdOS = 0
            SelectedPCRx(CurrRxCnt).ODManifest = ""
            SelectedPCRx(CurrRxCnt).OSManifest = ""
            SelectedPCRx(CurrRxCnt).RxNote = ""
        End If
        CurrRxCnt = CurrRxCnt - 1
        If (CurrRxCnt < 0) Then
            CurrRxCnt = 0
        End If
    Else
        For i = MyIdx To CurrRxCnt - 1
            SelectedCLRx(i).EWCompany = SelectedCLRx(i + 1).EWCompany
            SelectedCLRx(i).EWType = SelectedCLRx(i + 1).EWType
            SelectedCLRx(i).InventoryIdOD = SelectedCLRx(i + 1).InventoryIdOD
            SelectedCLRx(i).InventoryIdOS = SelectedCLRx(i + 1).InventoryIdOS
            SelectedCLRx(i).ODManifest = SelectedCLRx(i + 1).ODManifest
            SelectedCLRx(i).OSManifest = SelectedCLRx(i + 1).OSManifest
            SelectedCLRx(i).RxNote = SelectedCLRx(i + 1).RxNote
        Next i
        If (CurrRxCnt > 0) Then
            SelectedCLRx(CurrRxCnt).EWCompany = ""
            SelectedCLRx(CurrRxCnt).EWType = ""
            SelectedCLRx(CurrRxCnt).InventoryIdOD = 0
            SelectedCLRx(CurrRxCnt).InventoryIdOS = 0
            SelectedCLRx(CurrRxCnt).ODManifest = ""
            SelectedCLRx(CurrRxCnt).OSManifest = ""
            SelectedCLRx(CurrRxCnt).RxNote = ""
        End If
        CurrRxCnt = CurrRxCnt - 1
        If (CurrRxCnt < 0) Then
            CurrRxCnt = 0
        End If
    End If
End Sub

Private Function SelFgc(mode As String) As Integer
SelFgc = -1
Dim g As Integer
Dim r As Integer
For g = 0 To 1
    For r = 0 To lstRxs(g).Rows - 1
        If lstRxs(g).RowHeight(r) > 255 Then
            Select Case mode
            Case "g"
                SelFgc = g
            Case "r"
                SelFgc = r
            End Select
            Exit Function
        End If
    Next
Next
End Function

Private Sub cmdRH_Click()
ExpendedMode = Not ExpendedMode
If ExpendedMode Then
    cmdRH.Caption = "Compress"
Else
    cmdRH.Caption = "Expand"
End If
setEditMode False
ExpendRow
End Sub

Private Sub cmdShow_Click()
If Not frLine(0).Visible Then Exit Sub

If fgc.col > 0 Then Exit Sub

Dim ODOS(1) As String
Dim r As Integer
Dim c As Integer
Dim i As Integer
Dim e As Integer
r = Fix(fgc.Row / 2) * 2
For e = 0 To 1
    For c = 2 To fgc.Cols - 1
        ODOS(e) = ODOS(e) & vbTab & fgc.TextMatrix(r + e, c)
    Next
Next

Dim st As Integer
If fgc.Index = 3 Then st = 13
For i = st To 17
    If cmdH(i).Caption = fgc.TextMatrix(r, 0) Then
        Exit For
    End If
Next
CmdHExit_Click
InsertRow i, ODOS(0), ODOS(1), fgc.TextMatrix(r + 1, 0)
End Sub

Private Sub cmdSumm_Click()
Call frmSystems1.cmdSumm_Click
End Sub

Private Sub Form_Load()
AppInfoLoaded = False
delim = "; "

frInfo.Top = 7860
frEdit.Top = 7860
cmdCCom(0).Top = 180
cmdCCom(1).Top = 180
cmdCCom(2).Top = 180

FGCCell.ColWidth(0) = 15
FGCCell.RowHeight(0) = 15
FGCCell.Row = 1
FGCCell.col = 1
FGCCell.CellAlignment = flexAlignLeftTop

frHist.BackColor = Me.BackColor
frHist.Top = 0
frHist.Left = 0
frHist.Width = Me.Width
frHist.Height = Me.Height
frHCmd.Top = 8040
frHCmd.Left = 120
End Sub

Private Sub cmdQuit_Click()
Unload frmAssignWearNew
End Sub

Private Sub lstCLRx_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 10
TempOD = CurrentManifest(10).ODManifest
TempOS = CurrentManifest(10).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 2)
End If
End Sub

Private Sub lstCurRx1_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 1
TempOD = CurrentManifest(1).ODManifest
TempOS = CurrentManifest(1).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 0)
End If
End Sub

Private Sub lstCurRx2_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 2
TempOD = CurrentManifest(2).ODManifest
TempOS = CurrentManifest(2).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 0)
End If
End Sub

Private Sub lstCurRx3_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 3
TempOD = CurrentManifest(3).ODManifest
TempOS = CurrentManifest(3).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 0)
End If
End Sub

Private Sub lstCurRx4_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 4
TempOD = CurrentManifest(4).ODManifest
TempOS = CurrentManifest(4).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 0)
End If
End Sub

Private Sub lstCurRx5_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 5
TempOD = CurrentManifest(5).ODManifest
TempOS = CurrentManifest(5).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 0)
End If
End Sub

Private Sub lstCurRx6_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 6
TempOD = CurrentManifest(6).ODManifest
TempOS = CurrentManifest(6).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 0)
End If
End Sub

Private Sub lstCurRx7_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 7
TempOD = CurrentManifest(7).ODManifest
TempOS = CurrentManifest(7).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 1)
End If
End Sub

Private Sub lstCurRx8_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 8
TempOD = CurrentManifest(8).ODManifest
TempOS = CurrentManifest(8).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 1)
End If
End Sub

Private Sub lstCurRx9_Click()
Dim i As Integer
Dim Idx As Integer
Dim TempOD As String
Dim TempOS As String
SelectedTest = 9
TempOD = CurrentManifest(9).ODManifest
TempOS = CurrentManifest(9).OSManifest
If (Trim(TempOD) <> "") Or (Trim(TempOS) <> "") Then
    Idx = lstRxs(0).Rows + 1
    Call SelectRx(TempOD, TempOS, "", Idx, 3)
End If
End Sub

Private Sub SetNote(ANote As String)
Dim Temp As String
If (ANote <> "") Then
    Temp = Trim(ANote)
    Call StripCharacters(Temp, "*")
    ANote = Temp
    If (WearType = "CL") Then
        Temp = Mid(SelectedCLRx(1).RxNote, 1, 1)
        If (Trim(Temp) <> "") Then
            If (InStrPS("*^~", Temp) > 0) Then
                SelectedCLRx(1).RxNote = Temp + Trim(ANote)
            Else
                SelectedCLRx(1).RxNote = Trim(ANote)
            End If
        Else
            SelectedCLRx(1).RxNote = Trim(ANote)
        End If
    Else
        SelectedPCRx(1).RxNote = Trim(ANote)
    End If
'    lblNote1.BackColor = EnlargeOff
'    If (IsNoteBig(txtNote1.Text)) Then
'        lblNote1.BackColor = EnlargeOn
'    End If
End If
End Sub

Private Function GetExactInvItem(itm As Integer, AEye As String, InvId As Long, ODInvId As Long, ODInvIdDesc As String, OSInvId As Long, OSInvIdDesc As String) As Boolean
Dim Idx As Integer
Dim u As Integer, q As Integer
Dim i As Integer, w As Integer
Dim MyId As Long
Dim KeyWords(5) As String
Dim Rx As String
Dim ASeries As String, TName As String
Dim Temp As String, ATemp As String
Dim strRef1 As String, strRef2 As String
Dim strRef3 As String, strRef4 As String
Dim strRef5 As String, strAxis As String
Dim strSphere As String, strCyl As String
Dim RetInv As CLInventory
ODInvId = 0
ODInvIdDesc = ""
OSInvId = 0
OSInvIdDesc = ""
GetExactInvItem = False
If (itm > 0) And (InvId > 0) Then
    KeyWords(1) = "ADD-READING:"
    KeyWords(2) = "BASE CURVE:"
    KeyWords(3) = "DIAMETER:"
    KeyWords(4) = "PERIPH CURVE:"
    KeyWords(5) = ""
    If (AEye = "OD") Or (AEye = "OU") Then
        Rx = Trim(SelectedCLRx(itm).ODManifest)
        GoSub GetData
        GoSub GetSeries
        ODInvId = MyId
        ODInvIdDesc = ASeries
    End If
    If (AEye = "OS") Or (AEye = "OU") Then
        If (Trim(SelectedCLRx(itm).ODManifest) <> Trim(SelectedCLRx(itm).OSManifest)) Then
            Rx = Trim(SelectedCLRx(itm).OSManifest)
            GoSub GetData
            GoSub GetSeries
            OSInvId = MyId
            OSInvIdDesc = ASeries
        Else
            OSInvId = ODInvId
            OSInvIdDesc = ODInvIdDesc
        End If
    Else
    End If
    If (ODInvId > 0) Or (OSInvId > 0) Then
        GetExactInvItem = True
    End If
End If
Exit Function
GetData:
    strSphere = ""
    strCyl = ""
    strAxis = ""
    strRef1 = ""
    strRef2 = ""
    strRef3 = ""
    strRef4 = ""
    strRef5 = ""
    If (Left(Rx, 2) = "OD") Or (Left(Rx, 2) = "OS") Then
        Rx = Trim(Mid(Rx, 4, Len(Rx) - 3))
    End If
    q = InStrPS(Rx, " ")
    If (q > 0) Then
        Temp = Trim(Left(Rx, q))
        If (Temp = "NE") Then
            Temp = ""
        End If
        w = InStrPS(q + 1, Rx, " ")
        If (w > 0) Then
            TName = Mid(Rx, q + 1, (w - 1) - q)
            If (UCase(TName) = "BALANCE") Then
                q = w + 5
            ElseIf (UCase(TName) = "VERTEX") Then
            
            End If
        End If
    ElseIf (Trim(Rx) <> "") Then
        Temp = Trim(Rx)
    End If
    strSphere = Temp
    
    If (UCase(TName) <> "VERTEX") Then
        i = InStrPS(q + 1, Rx, " ")
        If (i > 0) Then
            Temp = Trim(Mid(Rx, q + 1, i - q))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            If (UCase(Left(Temp, 3)) <> "ADD") And (UCase(Left(Temp, 3)) <> "PRI") And (UCase(Left(Temp, 3)) <> "BAS") And (UCase(Left(Temp, 3)) <> "DIA") And (UCase(Left(Temp, 1)) <> "X") Then
                If (UCase(Temp) = "VERTEX") Then
                    Temp = ""
                End If
            End If
            strCyl = Temp
        End If
        
        q = InStrPS(Rx, "X")
        If (q < 1) Then
            q = InStrPS(Rx, "x")
        End If
        If (q > 0) Then
            i = InStrPS(q, Rx, " ")
            If (i > 0) Then
                Temp = Trim(Mid(Rx, q, i - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                If (UCase(Temp) = "X") Then
                    Temp = "    "
                End If
            Else
                Temp = Trim(Mid(Rx, q, Len(Rx) - (q - 1)))
                If (Temp = "NE") Then
                    Temp = ""
                End If
                If (UCase(Temp) = "X") Then
                    Temp = ""
                End If
            End If
            strAxis = Temp
        End If
    End If
' Rx Data
    Idx = 1
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef1 = ATemp
        If (strRef1 = "NE") Then
            strRef1 = ""
        End If
    End If
    
    Idx = 2
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef2 = ATemp
        If (strRef2 = "NE") Then
            strRef2 = ""
        End If
    End If
    
    Idx = 3
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef3 = ATemp
        If (strRef3 = "NE") Then
            strRef3 = ""
        End If
    End If
    
    Idx = 4
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef4 = ATemp
        If (strRef4 = "NE") Then
            strRef4 = ""
        End If
    End If

    Idx = 5
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef5 = ATemp
        If (strRef5 = "NE") Then
            strRef5 = ""
        End If
    End If
    Return
GetField:
    ATemp = ""
    If (Idx > 0) Then
        If (KeyWords(Idx) <> "") Then
            i = InStrPS(UCase(Temp), UCase(KeyWords(Idx)))
            If (i > 0) Then
                GoSub PutEnd
                If (u > 0) Then
                    ATemp = Mid(Temp, i, (u - 1) - (i - 1))
                Else
                    ATemp = Mid(Temp, i, Len(Temp) - (i - 1))
                End If
            End If
        End If
    End If
    If (Trim(ATemp) <> "") Then
        i = InStrPS(ATemp, ":")
        If (i > 0) Then
            ATemp = Trim(Mid(ATemp, i + 1, Len(ATemp) - i))
        End If
    End If
    Return
PutEnd:
    u = 0
    For w = Idx + 1 To 5
        If (Trim(KeyWords(w)) <> "") Then
            u = InStrPS(UCase(Temp), UCase(KeyWords(w)))
            If (u > 0) Then
                Exit For
            End If
        End If
    Next w
    Return
GetSeries:
    MyId = 0
    ASeries = ""
    Set RetInv = New CLInventory
    RetInv.InventoryId = InvId
    If (RetInv.RetrieveCLInventory) Then
        ASeries = Trim(RetInv.Series)
    End If
    Set RetInv = Nothing
    If (ASeries <> "") Then
        Set RetInv = New CLInventory
        RetInv.FieldSelect = "InventoryId"
        RetInv.Criteria = "Series = '" & ASeries & "' "
        If (Trim(strSphere) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And SpherePower = '" & strSphere & "' "
        End If
        If (Trim(strCyl) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And CylinderPower = '" & strCyl & "' "
        End If
        If (Trim(strAxis) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And Axis = '" & strAxis & "' "
        End If
        If (Trim(strRef1) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And AddReading = '" & strRef1 & "' "
        End If
        If (Trim(strRef2) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And BaseCurve = '" & strRef2 & "' "
        End If
        If (Trim(strRef3) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And Diameter = '" & strRef3 & "' "
        End If
        If (Trim(strRef4) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And PeriphCurve = '" & strRef4 & "' "
        End If
        If (RetInv.FindCLInventorySingleField) Then
            MyId = Val(Trim(RetInv.SingleFieldValue))
        End If
        Set RetInv = Nothing
    End If
    Return
End Function

Private Function UseFavorite(AEye As String) As Boolean
UseFavorite = False
frmEventMsgs.Header = "Inventory does not match " + AEye + " prescription. Use the selection ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    UseFavorite = True
End If
End Function

Private Function GetInventoryItem(AId As Long, ADisplay As String) As Boolean
Dim RetInv As CLInventory
ADisplay = ""
GetInventoryItem = False
If (AId > 0) Then
    Set RetInv = New CLInventory
    RetInv.InventoryId = AId
    If (RetInv.RetrieveCLInventory) Then
        GetInventoryItem = True
        ADisplay = Trim(RetInv.Series) + " " + Trim(RetInv.Material) + " " + Trim(RetInv.Disposable) + " " + Trim(RetInv.Tint) + " " + Trim(RetInv.Type_)
    End If
    Set RetInv = Nothing
End If
End Function

Private Sub LoadAppInfo()
If AppInfoLoaded Then Exit Sub
AppInfoLoaded = True


Dim i As Integer
i = -1
Dim tme As String
ReDim AppInfo(0)
Dim EAL As New DI_ExamActivityList
If EAL.PreviousActivity(PatientId, Format(Date, "yyyymmdd"), "D,G,H") Then
    Do
        If EAL.AppointmentId > 0 Then
            If EAL.Comments <> "ADD VIA BILLING" Then
                i = i + 1
                ReDim Preserve AppInfo(i)
                AppInfo(i).AppId = EAL.AppointmentId
                If EAL.AppointmentTime < 0 Then
                    tme = ""
                Else
                    ConvertMinutesToTime EAL.AppointmentTime, tme
                End If
                AppInfo(i).AppDateTime = CDate( _
                                         Mid(EAL.AppointmentDate, 5, 2) & "/" & _
                                         Mid(EAL.AppointmentDate, 7, 2) & "/" & _
                                         Mid(EAL.AppointmentDate, 1, 4) & " " & _
                                         tme)
                AppInfo(i).AppType = EAL.AppointmentType
                AppInfo(i).ActId = EAL.ActivityId
            End If
        End If
    Loop While EAL.NextRec
End If
Set EAL = Nothing

End Sub


Private Sub DisplayHistoryVisit()
lstRxs(2).Visible = False
lstRxs(3).Visible = False
HlstRxs(2).Visible = False
HlstRxs(3).Visible = False
DoEvents

lstRxs(2).Rows = 0
lstRxs(3).Rows = 0
Dim TestId As String
Dim FLine() As String
Dim l As Integer
Dim CurrentTestName As String
Dim i As Long
i = 1
Dim ClinicalRecords As New DI_ExamClinical
If (ClinicalRecords.LoadClinicalData(PatientId, AppInfo(SelVisit).AppId, Format(AppInfo(SelVisit).AppDateTime, "yyyymmdd"))) Then
    Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
        If (ClinicalRecords.ClinicalType = "F") Then
            Select Case UCase(Trim(ClinicalRecords.ClinicalSymptom))
            Case "/GLASSES", "/AUTOREFRACTION", "/MANIFEST REFRACTION", _
                 "/RETINOSCOPY", "/DILATED REFRACTION", "/CONTACT LENSES", _
                 "/CL TRIAL 1", "/CL TRIAL 2"
                If Mid(ClinicalRecords.ClinicalSymptom, 1, 1) = "/" Then
                    If Not CurrentTestName = ClinicalRecords.ClinicalSymptom Then
                        GoSub mProcFile
                        l = l + 1
                        ReDim Preserve FLine(l)
                        FLine(l) = "QUESTION=" & Trim(ClinicalRecords.ClinicalSymptom)
                        CurrentTestName = ClinicalRecords.ClinicalSymptom
                    End If
                    l = l + 1
                    ReDim Preserve FLine(l)
                    FLine(l) = Trim(ClinicalRecords.ClinicalFindings) & " " & _
                               Trim(ClinicalRecords.ClinicalInstructions)
                End If
            End Select
        End If
        i = i + 1
    Loop
End If


Dim tmp As String
Dim d As Integer
Dim v() As String
Dim delim As String
Dim ODOS() As ODOSType
Dim NoteTemp As String
Dim tmpODOS() As String
Dim c As Integer
Dim RInd As Integer
Dim Temp As String
Dim RetInv As CLInventory
If (ClinicalRecords.LoadPreviousClinicalImprPlan(AppInfo(SelVisit).AppId)) Then
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        If (ClinicalRecords.LocateStartingRecord("A")) Then
            i = 1
            Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
                ReDim img(2)
                If (ClinicalRecords.ClinicalType <> "A") Then
                    Exit Do
                End If
                
                tmp = Trim(ClinicalRecords.ClinicalFindings)
                Select Case True
                Case UCase(Mid(tmp, 1, 21)) = "DISPENSE SPECTACLE RX"
                    RInd = 12
                Case UCase(Mid(tmp, 1, 14)) = "ORDER TRIAL CL"
                    RInd = 16
                Case UCase(Mid(tmp, 1, 14)) = "DISPENSE CL RX"
                    RInd = 17
                Case Else
                    GoTo NextItem
                End Select
                
                ' get comments from ImageDescriptor
                img(0) = Trim(ClinicalRecords.ClinicalDescriptor)
                Select Case img(0)
                Case "^OD:^OS:", "^OD: OS:", "~OD: OS:", "^", "*", "~"
                    img(0) = ""
                End Select
                If Not img(0) = "" Then
                    Select Case Left(img(0), 1)
                    Case "*"
                        RInd = 14
                    Case "~"
                        RInd = 16
                    End Select
                    
                    If Len(img(0)) > 3 Then
                        Select Case Mid(img(0), 2, 3)
                        Case "OD:"
                            img(1) = Trim(Mid(img(0), 5))
                            delim = InStrPS(1, img(1), "OS:")
                            If delim > 0 Then
                                img(2) = Trim(Mid(img(1), delim + 3))
                                img(1) = Trim(Left(img(1), delim - 1))
                            End If
                            If Right(img(1), 3) = "-&-" Then
                                img(1) = Left(img(1), Len(img(1)) - 3)
                            End If
                        Case "OS:"
                            img(2) = Mid(img(0), 5)
                        Case Else
                            img(0) = Mid(img(0), 2)
                        End Select
                    End If
                End If
                
                ReDim v(6) As String
                For d = 1 To 6
                    delim = "-" & d & "/"
                    Do Until Left(tmp, 3) = delim Or tmp = ""
                        tmp = Mid(tmp, 2)
                    Loop
                    delim = "-" & d + 1 & "/"
                    tmp = Trim(Mid(tmp, 4))
                    Do Until Left(tmp, 3) = delim Or tmp = ""
                        v(d) = v(d) & Left(tmp, 1)
                        tmp = Mid(tmp, 2)
                    Loop
                Next
                
                ReDim ODOS(1)
                ReDim tmpODOS(1)
                NoteTemp = ""
                For c = 0 To 1
                    getValues v(c + 2), RInd, ODOS(c)
                    If Not img(0) = "" Then
                        If img(1) = "" And img(2) = "" Then
                            If c = 0 Then ODOS(0).COMMENT = img(0)
                        Else
                            ODOS(c).COMMENT = img(c + 1)
                        End If
                    End If
                    If RInd < 13 Then
                        ODOS(c).TTYPE = v(1)
                        
                        'get comments for SpecRx from seg -4/
                        If RInd = 12 And c = 0 Then
                            ODOS(c).COMMENT = v(4) & ODOS(c).COMMENT
                        End If
                        
                        tmpODOS(c) = "" & _
                        vbTab & Trim(ODOS(c).SPHERE) & _
                        vbTab & Trim(ODOS(c).CYL) & _
                        vbTab & Trim(ODOS(c).Axis) & _
                        vbTab & Trim(ODOS(c).VA_DIST) & _
                        vbTab & Trim(ODOS(c).TADD) & _
                        vbTab & Trim(ODOS(c).INT_ADD) & _
                        vbTab & Trim(ODOS(c).VA_NEAR) & _
                        vbTab & Trim(ODOS(c).PSM_1) & _
                        vbTab & Trim(ODOS(c).PSM_2) & _
                        vbTab & Trim(ODOS(c).VERT) & _
                        vbTab & Trim(ODOS(c).TTYPE) & _
                        vbTab & Trim(ODOS(c).COMMENT) & _
                        ""
                    Else
                        Temp = ""
                        If IsNumeric(v(c + 5)) Then
                            If v(c + 5) > 0 Then
                                Set RetInv = New CLInventory
                                RetInv.InventoryId = v(c + 5)
                                If (RetInv.RetrieveCLInventory) Then
                                    If (Trim(RetInv.Series) <> "") Then
                                        Temp = Temp + Trim(RetInv.Series) + " "
                                    End If
                                    If (Trim(RetInv.Type_) <> "") Then
                                        Temp = Temp + Trim(RetInv.Type_) + " "
                                    End If
                                    If (Trim(RetInv.WearTime) <> "") Then
                                        Temp = Temp + Trim(RetInv.WearTime) + " "
                                    End If
                                End If
                                Set RetInv = Nothing
                            End If
                        End If
                        ODOS(c).Model = Temp
                        
                        tmpODOS(c) = "" & _
                        vbTab & Trim(ODOS(c).SPHERE) & _
                        vbTab & Trim(ODOS(c).CYL) & _
                        vbTab & Trim(ODOS(c).Axis) & _
                        vbTab & Trim(ODOS(c).VA_DIST) & _
                        vbTab & Trim(ODOS(c).TADD) & _
                        vbTab & Trim(ODOS(c).VA_NEAR) & _
                        vbTab & Trim(ODOS(c).BC) & _
                        vbTab & Trim(ODOS(c).DIA) & _
                        vbTab & Trim(ODOS(c).PC) & _
                        vbTab & Trim(ODOS(c).D_N) & _
                        vbTab & Trim(ODOS(c).Model) & _
                        vbTab & Trim(ODOS(c).COMMENT) & _
                        ""
                    End If
                Next
                InsertRow RInd, tmpODOS(0), tmpODOS(1)
NextItem:
                i = i + 1
            Loop
        End If
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
End If
Set ClinicalRecords = Nothing
GoSub mProcFile
AdjustFGC

lstRxs(2).Visible = True
lstRxs(3).Visible = True
HlstRxs(2).Visible = True
HlstRxs(3).Visible = True

Exit Sub
'--------------------------------------------------------------------------------
mProcFile:
If Not CurrentTestName = "" Then
    Select Case CurrentTestName
    Case "/GLASSES"
        TestId = "T07A"
    Case "/CONTACT LENSES"
        TestId = "08A"
    Case "/RETINOSCOPY"
        TestId = "09A"
    Case "/AUTOREFRACTION"
        TestId = "10A"
    Case "/MANIFEST REFRACTION"
        TestId = "13A"
    Case "/CL TRIAL 1"
        TestId = "07T"
    Case "/CL TRIAL 2"
        TestId = "07U"
    Case "/DILATED REFRACTION"
        TestId = "09G"
    Case Else
        TestId = ""
    End Select

    Call GetNotesForTests(PatientId, AppointmentId, TestId, NoteTemp)
    ProcFile FLine, NoteTemp
    NoteTemp = ""
End If
ReDim FLine(0)
l = 0
Return
'--------------------------------------------------------------------------------
End Sub

Private Function OrderTrial() As Boolean
OrderTrial = False
frmEventMsgs.Header = "Order Trial ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    OrderTrial = True
End If
End Function

Private Sub getValues(inp As String, RInd As Integer, ODOS As ODOSType)

Dim BrWord(5) As String
If RInd < 13 Then
    BrWord(0) = "COMMENT:"
    BrWord(1) = "VERTEX DIST:"
    BrWord(2) = "PRISM OF ANGLE 2:"
    BrWord(3) = "PRISM OF ANGLE 1:"
    BrWord(4) = "ADD-INTERMEDIATE:"
    BrWord(5) = "ADD-READING:"
Else
    BrWord(0) = "COMMENT:"
    BrWord(1) = "D OR N:"
    BrWord(2) = "PERIPH CURVE:"
    BrWord(3) = "DIAMETER:"
    BrWord(4) = "BASE CURVE:"
    BrWord(5) = "ADD-READING:"
End If
Dim BrP As Integer
Dim b As Integer
Dim tmp(5) As String

For b = 0 To UBound(BrWord)
    BrP = InStrPS(1, inp, BrWord(b))
    If BrP > 0 Then
        tmp(b) = (Mid(inp, BrP + Len(BrWord(b))))
        inp = Trim(Mid(inp, 1, Len(inp) - Len(tmp(b)) - Len(BrWord(b))))
    End If
Next

With ODOS
    If RInd < 13 Then
        .COMMENT = Trim(tmp(0))
        .VERT = Trim(tmp(1))
        .PSM_2 = Trim(tmp(2))
        .PSM_1 = Trim(tmp(3))
        .INT_ADD = Trim(tmp(4))
        .TADD = Trim(tmp(5))
    Else
        .COMMENT = Trim(tmp(0))
        .D_N = Trim(tmp(1))
        .PC = Trim(tmp(2))
        .DIA = Trim(tmp(3))
        .BC = Trim(tmp(4))
        .TADD = Trim(tmp(5))
    End If
    For b = 0 To 3
        tmp(b) = ""
    Next
    b = 0
    
    Do Until inp = "" Or b > 3
        BrP = InStrPS(1, inp, " ")
        If BrP > 0 Then
            tmp(b) = Trim(Mid(inp, 1, BrP - 1))
            inp = Trim(Mid(inp, BrP + 1))
            b = b + 1
        Else
            If Len(inp) > 1 Then
                tmp(b) = inp
                inp = ""
            End If
        End If
    Loop
    
    .SPHERE = tmp(0)
    If tmp(2) = "" Then
        If tmp(1) = "" Then
            .CYL = ""
            .Axis = ""
        Else
            If UCase(Left(tmp(1), 1)) = "X" Then
                .CYL = ""
                .Axis = tmp(1)
            Else
                .CYL = tmp(1)
                .Axis = ""
            End If
        End If
    Else
        .CYL = tmp(1)
        .Axis = tmp(2)
    End If
End With
End Sub




Private Function IsNoteBig(AText As String) As Boolean
Dim i As Integer
Dim j As Integer
IsNoteBig = False
j = InStrPS(AText, vbCrLf)
If (j > 0) Then
    i = j + 2
    j = InStrPS(i, AText, vbCrLf)
    If (j > 0) Then
        IsNoteBig = True
    End If
End If
If Not (IsNoteBig) Then
    If (Len(AText) > 154) Then
        IsNoteBig = True
    End If
End If
End Function

Private Sub ButtonAction(AButton As fpBtn, Ref As Integer, IValue As String)
Dim i As Integer
Dim q As Integer
Dim u As Integer
Dim z As Integer
Dim TestId As Integer
Dim Eye As Integer
Dim Cnt As Integer
IValue = ""
TestId = 1
Eye = 1
If (Mid(AButton.Name, 4, 2) = "OS") Then
    Eye = 2
End If
Cnt = Val(Mid(AButton.Name, 6, 1))
TheTestId = TestId
DisplayPads(TestId, Eye, Cnt).ButtonOn = Not (DisplayPads(TestId, Eye, Cnt).ButtonOn)
DisplayBtns(TestId, Eye).ButtonOn = Not (DisplayBtns(TestId, Eye).ButtonOn)
If (Trim(DisplayPads(TestId, Eye, Cnt).ButtonTriggerText) <> "") Then
    If (InStrPS(DisplayPads(TestId, Eye, Cnt).ButtonTriggerText, "Pad") <> 0) Then
        Call PadAction(AButton, Ref)
    ElseIf (InStrPS(UCase(DisplayPads(TestId, Eye, Cnt).ButtonTriggerText), "EPAD") <> 0) Then
        Call PadAction(AButton, Ref)
    End If
Else
    Call PadAction(AButton, Ref)
End If
IValue = PlantTriggerPads(TestId, Eye, Cnt, 1).Result
If (WearType = "PC") Then
    If (Cnt = 4) Then
        IValue = "Add:" + IValue
    ElseIf (Cnt = 5) Then
        IValue = "Int:" + IValue
    ElseIf (Cnt = 6) Then
        IValue = "A2:" + IValue
    ElseIf (Cnt = 7) Then
        IValue = "A2:" + IValue
    ElseIf (Cnt = 8) Then
        IValue = "Vx:" + IValue
    End If
Else
    If (Cnt = 4) Then
        IValue = "Add:" + IValue
    ElseIf (Cnt = 5) Then
        IValue = "BC:" + IValue
    ElseIf (Cnt = 6) Then
        IValue = "DI:" + IValue
    ElseIf (Cnt = 7) Then
        IValue = "PC:" + IValue
    End If
End If
For i = 1 To 24
    If (PlantTriggerPads(TestId, Eye, Cnt, i).ButtonOn) Then
        IValue = IValue + " " + PlantTriggerPads(TestId, Eye, Cnt, i).ButtonText
    End If
Next i
End Sub

Private Sub PadAction(APad As fpBtn, Ref As Integer)
Dim ATemp As String
Dim i As Integer, q As Integer
Dim u As Integer, g As Integer
Dim TestId As Integer, Temp As Integer
Dim Eye As Integer, Cnt As Integer
Dim iMax As Double, iMin As Double, iDefault As Double
g = InStrPS(APad.Tag, "/")
If (g = 0) Then
    iMax = 0
    iMin = 0
    iDefault = 0
Else
    iMax = Val(Left(APad.Tag, g - 1))
    q = InStrPS(g + 1, APad.Tag, "/")
    If (q = 0) Then
        iMin = Val(Mid(APad.Tag, g + 1, Len(APad.Tag) - g))
        iDefault = iMin
    Else
        iMin = Val(Mid(APad.Tag, g + 1, (q - 1) - g))
        If (Mid(APad.Tag, q + 1, Len(APad.Tag) - q) = "?") Then
            iDefault = -9999
        Else
            iDefault = Val(Mid(APad.Tag, q + 1, Len(APad.Tag) - q))
        End If
    End If
End If
q = InStrPS(APad.CellTag, "/")
If (q <> 0) Then
    TestId = Val(Left(APad.CellTag, q - 1))
    u = InStrPS(q + 1, APad.CellTag, "/")
    If (u <> 0) Then
        Eye = Val(Mid(APad.CellTag, q + 1, (u - 1) - q))
        Cnt = Val(Mid(APad.CellTag, u + 1, Len(APad.CellTag) - u))
    Else
        Exit Sub
    End If
    TheTestId = TestId
Else
    Exit Sub
End If
CurrentEye = Trim(str(Eye))
ATemp = Trim(APad.Text)
Call ReplaceCharacters(ATemp, vbCrLf, " ")
If (Trim(DisplayPads(TestId, Eye, Cnt).ButtonTriggerText) <> "Epad") Then
    frmNumericPad.NumPad_Result = ""
    frmNumericPad.NumPad_Field = Trim(ATemp)
    frmNumericPad.NumPad_Max = iMax
    frmNumericPad.NumPad_Min = iMin
    frmNumericPad.NumPad_Default = iDefault
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_Increment = DisplayPads(TestId, Eye, Cnt).ButtonIncrement
    If (Left(PlantTriggerPads(TestId, Eye, Cnt, 1).ButtonText, 2) = "OD") Then
        frmNumericPad.NumPad_Choice1 = ""
    Else
        frmNumericPad.NumPad_Choice1 = PlantTriggerPads(TestId, Eye, Cnt, 1).ButtonText
    End If
    If (Left(PlantTriggerPads(TestId, Eye, Cnt, 2).ButtonText, 2) = "OS") Then
        frmNumericPad.NumPad_Choice2 = ""
    Else
        frmNumericPad.NumPad_Choice2 = PlantTriggerPads(TestId, Eye, Cnt, 2).ButtonText
    End If
    frmNumericPad.NumPad_Choice3 = PlantTriggerPads(TestId, Eye, Cnt, 3).ButtonText
    frmNumericPad.NumPad_Choice4 = PlantTriggerPads(TestId, Eye, Cnt, 4).ButtonText
    frmNumericPad.NumPad_Choice5 = PlantTriggerPads(TestId, Eye, Cnt, 5).ButtonText
    frmNumericPad.NumPad_Choice6 = PlantTriggerPads(TestId, Eye, Cnt, 6).ButtonText
    frmNumericPad.NumPad_Choice7 = PlantTriggerPads(TestId, Eye, Cnt, 7).ButtonText
    frmNumericPad.NumPad_Choice8 = PlantTriggerPads(TestId, Eye, Cnt, 8).ButtonText
    frmNumericPad.NumPad_Choice9 = PlantTriggerPads(TestId, Eye, Cnt, 9).ButtonText
    frmNumericPad.NumPad_Choice10 = PlantTriggerPads(TestId, Eye, Cnt, 10).ButtonText
    frmNumericPad.NumPad_Choice11 = PlantTriggerPads(TestId, Eye, Cnt, 11).ButtonText
    frmNumericPad.NumPad_Choice12 = PlantTriggerPads(TestId, Eye, Cnt, 12).ButtonText
    frmNumericPad.NumPad_Choice13 = PlantTriggerPads(TestId, Eye, Cnt, 13).ButtonText
    frmNumericPad.NumPad_Choice14 = PlantTriggerPads(TestId, Eye, Cnt, 14).ButtonText
    frmNumericPad.NumPad_Choice15 = PlantTriggerPads(TestId, Eye, Cnt, 15).ButtonText
    frmNumericPad.NumPad_Choice16 = PlantTriggerPads(TestId, Eye, Cnt, 16).ButtonText
    frmNumericPad.NumPad_Choice17 = PlantTriggerPads(TestId, Eye, Cnt, 17).ButtonText
    frmNumericPad.NumPad_Choice18 = PlantTriggerPads(TestId, Eye, Cnt, 18).ButtonText
    frmNumericPad.NumPad_Choice19 = PlantTriggerPads(TestId, Eye, Cnt, 19).ButtonText
    frmNumericPad.NumPad_Choice20 = PlantTriggerPads(TestId, Eye, Cnt, 20).ButtonText
    frmNumericPad.NumPad_Choice21 = PlantTriggerPads(TestId, Eye, Cnt, 21).ButtonText
    frmNumericPad.NumPad_Choice22 = PlantTriggerPads(TestId, Eye, Cnt, 22).ButtonText
    frmNumericPad.NumPad_Choice23 = PlantTriggerPads(TestId, Eye, Cnt, 23).ButtonText
    frmNumericPad.NumPad_Choice24 = PlantTriggerPads(TestId, Eye, Cnt, 24).ButtonText
    frmNumericPad.Show 1
    If Not (frmNumericPad.NumPad_Quit) Then
        If (Trim(frmNumericPad.NumPad_Result) <> "") Then
            If (frmNumericPad.NumPad_Result <> "-") Then
                If Not (frmNumericPad.NumPad_NoValue) Then
                    PlantTriggerPads(TestId, Eye, Cnt, 1).Result = frmNumericPad.NumPad_Result
                Else
                    PlantTriggerPads(TestId, Eye, Cnt, 1).Result = "-"
                End If
            Else
                PlantTriggerPads(TestId, Eye, Cnt, 1).Result = ""
                DisplayPads(TestId, Eye, Cnt).Result = ""
                DisplayPads(TestId, Eye, Cnt).ButtonOn = False
                DisplayPads(TestId, Eye, Cnt).Buttons = False
            End If
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 1).Result = ""
            DisplayPads(TestId, Eye, Cnt).Result = ""
            DisplayPads(TestId, Eye, Cnt).ButtonOn = False
            DisplayPads(TestId, Eye, Cnt).Buttons = False
        End If
        For i = 1 To 24
            PlantTriggerPads(TestId, Eye, Cnt, i).Buttons = False
            PlantTriggerPads(TestId, Eye, Cnt, i).ButtonOn = False
        Next i
        If (frmNumericPad.NumPad_ChoiceOn1) Then
            PlantTriggerPads(TestId, Eye, Cnt, 1).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 1).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn2) Then
            PlantTriggerPads(TestId, Eye, Cnt, 2).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 2).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn3) Then
            PlantTriggerPads(TestId, Eye, Cnt, 3).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 3).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn4) Then
            PlantTriggerPads(TestId, Eye, Cnt, 4).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 4).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn5) Then
            PlantTriggerPads(TestId, Eye, Cnt, 5).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 5).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn6) Then
            PlantTriggerPads(TestId, Eye, Cnt, 6).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 6).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn7) Then
            PlantTriggerPads(TestId, Eye, Cnt, 7).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 7).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn8) Then
            PlantTriggerPads(TestId, Eye, Cnt, 8).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 8).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn9) Then
            PlantTriggerPads(TestId, Eye, Cnt, 9).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 9).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn10) Then
            PlantTriggerPads(TestId, Eye, Cnt, 10).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 10).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn11) Then
            PlantTriggerPads(TestId, Eye, Cnt, 11).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 11).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn12) Then
            PlantTriggerPads(TestId, Eye, Cnt, 12).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 12).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn13) Then
            PlantTriggerPads(TestId, Eye, Cnt, 13).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 13).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn14) Then
            PlantTriggerPads(TestId, Eye, Cnt, 14).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 14).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn15) Then
            PlantTriggerPads(TestId, Eye, Cnt, 15).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 15).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn16) Then
            PlantTriggerPads(TestId, Eye, Cnt, 16).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 16).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn17) Then
            PlantTriggerPads(TestId, Eye, Cnt, 17).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 17).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn18) Then
            PlantTriggerPads(TestId, Eye, Cnt, 18).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 18).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn19) Then
            PlantTriggerPads(TestId, Eye, Cnt, 19).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 19).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn20) Then
            PlantTriggerPads(TestId, Eye, Cnt, 20).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 20).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn21) Then
            PlantTriggerPads(TestId, Eye, Cnt, 21).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 21).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn22) Then
            PlantTriggerPads(TestId, Eye, Cnt, 22).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 22).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn23) Then
            PlantTriggerPads(TestId, Eye, Cnt, 23).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 23).ButtonOn = False
        End If
        If (frmNumericPad.NumPad_ChoiceOn24) Then
            PlantTriggerPads(TestId, Eye, Cnt, 24).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).ButtonOn = True
            DisplayPads(TestId, Eye, Cnt).Buttons = True
        Else
            PlantTriggerPads(TestId, Eye, Cnt, 24).ButtonOn = False
        End If
    Else
        ExitFast = True
    End If
End If
End Sub

Private Sub cmdOD1_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOD1, 1, RetValue)
    Call ResetValue("OD", 1, RetValue)
End If
End Sub

Private Sub cmdOD2_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOD2, 2, RetValue)
    Call ResetValue("OD", 2, RetValue)
End If
End Sub

Private Sub cmdOD3_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOD3, 3, RetValue)
    If (Trim(RetValue) <> "") Then
        RetValue = "X" + RetValue
    End If
    Call ResetValue("OD", 3, RetValue)
End If
End Sub

Private Sub cmdOD4_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOD4, 4, RetValue)
    Call ResetValue("OD", 4, RetValue)
End If
End Sub

Private Sub cmdOD5_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOD5, 5, RetValue)
    Call ResetValue("OD", 5, RetValue)
End If
End Sub

Private Sub cmdOD6_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOD6, 6, RetValue)
    Call ResetValue("OD", 6, RetValue)
End If
End Sub

Private Sub cmdOD7_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOD7, 7, RetValue)
    Call ResetValue("OD", 7, RetValue)
End If
End Sub

Private Sub cmdOD8_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOD8, 8, RetValue)
    Call ResetValue("OD", 8, RetValue)
End If
End Sub

Private Sub cmdOS1_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOS1, 1, RetValue)
    Call ResetValue("OS", 1, RetValue)
End If
End Sub

Private Sub cmdOS2_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOS2, 2, RetValue)
    Call ResetValue("OS", 2, RetValue)
End If
End Sub

Private Sub cmdOS3_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOS3, 3, RetValue)
    If (Trim(RetValue) <> "") Then
        RetValue = "X" + RetValue
    End If
    Call ResetValue("OS", 3, RetValue)
End If
End Sub

Private Sub cmdOS4_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOS4, 4, RetValue)
    Call ResetValue("OS", 4, RetValue)
End If
End Sub

Private Sub cmdOS5_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOS5, 5, RetValue)
    Call ResetValue("OS", 5, RetValue)
End If
End Sub

Private Sub cmdOS6_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOS6, 6, RetValue)
    Call ResetValue("OS", 6, RetValue)
End If
End Sub

Private Sub cmdOS7_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOS7, 7, RetValue)
    Call ResetValue("OS", 7, RetValue)
End If
End Sub

Private Sub cmdOS8_Click()
Dim RetValue As String
If (CurrentEditTest >= 0) Then
    Call ButtonAction(cmdOS8, 8, RetValue)
    Call ResetValue("OS", 8, RetValue)
End If
End Sub

Private Sub ResetValue(AEye As String, IRef As Integer, IValue As String)
Dim MyIdx As Integer
If (IRef > 0) And (Trim(IValue) <> "") Then
    If (AEye = "OD") Then
        lstRxs(0).TextMatrix(CurrentEditTest, IRef) = IValue
        Call RebuildRx
    ElseIf (AEye = "OS") Then
        lstRxs(0).TextMatrix(CurrentEditTest + 1, IRef) = IValue
        Call RebuildRx
    End If
End If
End Sub

Private Sub RebuildRx()
Dim i As Integer
Dim MyIdx As Integer
Dim TempOD As String
Dim TempOS As String
MyIdx = Int(CurrentEditTest / 2) + 1
If (MyIdx > 0) Then
    TempOD = ""
    TempOS = ""
    For i = 1 To lstRxs(0).Cols - 1
        If (lstRxs(0).TextMatrix(CurrentEditTest, i) <> "") Then
            TempOD = TempOD + " " + Trim(lstRxs(0).TextMatrix(CurrentEditTest, i))
        End If
        If (lstRxs(0).TextMatrix(CurrentEditTest + 1, i) <> "") Then
            TempOS = TempOS + " " + Trim(lstRxs(0).TextMatrix(CurrentEditTest + 1, i))
        End If
    Next i
    If (WearType = "PC") Then
        SelectedPCRx(MyIdx).ODManifest = Trim(TempOD)
        SelectedPCRx(MyIdx).OSManifest = Trim(TempOS)
    Else
        SelectedCLRx(MyIdx).ODManifest = Trim(TempOD)
        SelectedCLRx(MyIdx).OSManifest = Trim(TempOS)
    End If
End If
End Sub

Private Sub PlantButton(Ref As Integer, Counter As Integer, Eye As Integer, TestId As Integer, IType As String, TheText As String)
Dim AEye As String
Dim AButton As fpBtn
If (Eye = 1) Then
    If (Counter = 1) Then
        Set AButton = cmdOD1
    ElseIf (Counter = 2) Then
        Set AButton = cmdOD2
    ElseIf (Counter = 3) Then
        Set AButton = cmdOD3
    ElseIf (Counter = 4) Then
        Set AButton = cmdOD4
    ElseIf (Counter = 5) Then
        Set AButton = cmdOD5
    ElseIf (Counter = 6) Then
        Set AButton = cmdOD6
    ElseIf (Counter = 7) Then
        Set AButton = cmdOD7
    ElseIf (Counter = 8) Then
        Set AButton = cmdOD8
    End If
Else
    If (Counter = 1) Then
        Set AButton = cmdOS1
    ElseIf (Counter = 2) Then
        Set AButton = cmdOS2
    ElseIf (Counter = 3) Then
        Set AButton = cmdOS3
    ElseIf (Counter = 4) Then
        Set AButton = cmdOS4
    ElseIf (Counter = 5) Then
        Set AButton = cmdOS5
    ElseIf (Counter = 6) Then
        Set AButton = cmdOS6
    ElseIf (Counter = 7) Then
        Set AButton = cmdOS7
    ElseIf (Counter = 8) Then
        Set AButton = cmdOS8
    End If
End If
With AButton
    AEye = "OD"
    If (Eye = 2) Then
        AEye = "OS"
    End If
    .CellTag = Trim(str(TestId)) + "/" + Trim(str(Eye)) + "/" + Trim(str(Counter))
    .Text = AEye + "-" + DisplayPads(TestId, Eye, Counter).ButtonText
    .Tag = DisplayPads(TestId, Eye, Counter).ButtonRange
    .ToolTipText = "N"
End With
End Sub

Private Function DynamicEntry(Question As String, CurrentOrder As String) As Boolean
Dim i As Integer
Dim AltEyeOD As String
Dim AltEyeOS As String
Dim TheForm As DynamicForms
DynamicEntry = False
MaxTests = 0
MaxPadsForTests = 0
MaxButtonsForTests = 0
MaxButtonsForPads = 0
MaxButtonsForButtons = 0
MaxTestWideBtns = 0
AltEyeOD = "OD-"
AltEyeOS = "OS-"
Set TheForm = New DynamicForms
TheForm.Question = UCase(Trim(Question))
If (TheForm.RetrieveForm) Then
    If (TheForm.FormId > 0) Then
        CurrentFormId = TheForm.FormId
        AltEyeOD = Trim(DisplayBtns(1, 1).ButtonText) + "-"
        AltEyeOS = Trim(DisplayBtns(1, 2).ButtonText) + "-"
        TheTestId = 1
        Call GetPadsForTest(CurrentFormId, TheTestId, MaxPadsForTests)
        For i = 1 To MaxPadsForTests
            Call PlantButton(6, i, 1, 1, "N", AltEyeOD)
        Next i
        For i = 1 To MaxPadsForTests
            Call PlantButton(14, i, 2, 1, "N", AltEyeOS)
        Next i
        If (MaxWriteButtonsForTests(1) > 0) Then
            Call PlantButton(22, 1, 1, 1, "B", "")
            Call PlantButton(22, 2, 2, 1, "B", "")
        End If
        DynamicEntry = True
    End If
End If
Set TheForm = Nothing
End Function

Private Function GetTimeRef() As Boolean
Dim i As Integer
GetTimeRef = True
For i = 1 To MaxTestsAllowed
    TimeRef(i).Buttons = False
    TimeRef(i).ButtonOn = False
    TimeRef(i).ButtonName = ""
    TimeRef(i).ButtonText = ""
    TimeRef(i).ButtonRange = ""
    TimeRef(i).ButtonTrigger = ""
    TimeRef(i).ButtonTriggerText = ""
    TimeRef(i).ButtonId = 0
    TimeRef(i).Result = ""
Next i
End Function

Private Function GetTestOccurrences(FormId As Long, Counter As Integer) As Boolean
Dim i As Integer
Dim j As Integer
Dim k As Integer
Dim l As Integer
Dim q As Integer
Dim VisualText As String
Dim TheControl As DynamicControls
For i = 1 To MaxTestsAllowed
    DisplayTests(i).Buttons = False
    DisplayTests(i).ButtonOn = False
    DisplayTests(i).ButtonName = ""
    DisplayTests(i).ButtonText = ""
    DisplayTests(i).ButtonRange = ""
    DisplayTests(i).ButtonTrigger = ""
    DisplayTests(i).ButtonTriggerText = ""
    DisplayTests(i).ButtonId = 0
    DisplayTests(i).ButtonMutual = False
    DisplayTests(i).ButtonPadOrder = False
    DisplayTests(i).ButtonUseOtherText = False
    DisplayTests(i).ButtonIncrement = ""
    DisplayTests(i).ButtonDeviceRef = ""
    DisplayTests(i).Result = ""
    MaxWriteButtonsForTests(i) = 0
    For j = 1 To 2
        For k = 1 To MaxButtonsAllowed
            PlantTriggerButtons(i, j, k).Buttons = False
            PlantTriggerButtons(i, j, k).ButtonOn = False
            PlantTriggerButtons(i, j, k).ButtonName = ""
            PlantTriggerButtons(i, j, k).ButtonText = ""
            PlantTriggerButtons(i, j, k).ButtonRange = ""
            PlantTriggerButtons(i, j, k).ButtonTrigger = ""
            PlantTriggerButtons(i, j, k).ButtonTriggerText = ""
            PlantTriggerButtons(i, j, k).ButtonId = 0
            PlantTriggerButtons(i, j, k).ButtonMutual = False
            PlantTriggerButtons(i, j, k).ButtonPadOrder = False
            PlantTriggerButtons(i, j, k).ButtonUseOtherText = False
            PlantTriggerButtons(i, j, k).ButtonIncrement = ""
            PlantTriggerButtons(i, j, k).ButtonDeviceRef = ""
            PlantTriggerButtons(i, j, k).Result = ""
        Next k
    Next j
Next i
For i = 1 To MaxTestsAllowed
    For j = 1 To 2
        For k = 1 To MaxPadsAllowed
            For l = 1 To MaxPadChoices
                PlantTriggerPads(i, j, k, l).Buttons = False
                PlantTriggerPads(i, j, k, l).ButtonOn = False
                PlantTriggerPads(i, j, k, l).ButtonName = ""
                PlantTriggerPads(i, j, k, l).ButtonText = ""
                PlantTriggerPads(i, j, k, l).ButtonRange = ""
                PlantTriggerPads(i, j, k, l).ButtonTrigger = ""
                PlantTriggerPads(i, j, k, l).ButtonTriggerText = ""
                PlantTriggerPads(i, j, k, l).ButtonId = 0
                PlantTriggerPads(i, j, k, l).ButtonMutual = False
                PlantTriggerPads(i, j, k, l).ButtonPadOrder = False
                PlantTriggerPads(i, j, k, l).ButtonUseOtherText = False
                PlantTriggerPads(i, j, k, l).ButtonIncrement = ""
                PlantTriggerPads(i, j, k, l).ButtonDeviceRef = ""
                PlantTriggerPads(i, j, k, l).Result = ""
            Next l
        Next k
    Next j
Next i
If (FormId > 0) Then
    Counter = 1
    DisplayTests(Counter).ButtonOn = True
    DisplayTests(Counter).Buttons = True
    DisplayTests(Counter).ButtonName = "*"
    Call GetPadsForTest(FormId, Counter, MaxPadsForTests)
    Call GetButtonsForTests(FormId, Counter, MaxButtonsForTests)
End If
End Function

Private Function GetPadsForTest(FormId As Long, TestId As Integer, Counter As Integer) As Boolean
Dim u As Integer
Dim i As Integer
Dim q As Integer
Dim g As Integer
Dim TheControl As DynamicControls
Dim VisualText As String
Dim iDefault As Double
Dim iMax As Double
Dim iMin As Double
For q = 1 To 2
    For i = 1 To MaxPadsAllowed
        DisplayPads(TestId, q, i).Buttons = False
        DisplayPads(TestId, q, i).ButtonOn = False
        DisplayPads(TestId, q, i).ButtonName = ""
        DisplayPads(TestId, q, i).ButtonText = ""
        DisplayPads(TestId, q, i).ButtonRange = ""
        DisplayPads(TestId, q, i).ButtonTrigger = ""
        DisplayPads(TestId, q, i).ButtonTriggerText = ""
        DisplayPads(TestId, q, i).ButtonId = 0
        DisplayPads(TestId, q, i).ButtonMutual = False
        DisplayPads(TestId, q, i).ButtonPadOrder = False
        DisplayPads(TestId, q, i).ButtonUseOtherText = False
        DisplayPads(TestId, q, i).ButtonIncrement = ""
        DisplayPads(TestId, q, i).ButtonDeviceRef = ""
        DisplayPads(TestId, q, i).Result = ""
    Next i
Next q
Set TheControl = New DynamicControls
For u = 1 To 2
    If (FormId > 0) Then
        TheControl.FormId = FormId
        TheControl.ControlType = "N"
        TheControl.ControlAlternateText = ""
        If (TheControl.FindTriggerControl > 0) Then
            i = 1
            Counter = 0
            While (TheControl.SelectControl(i))
                If Not (TheControl.IgnoreIt) Then
                    Counter = Counter + 1
                    If (Trim(TheControl.ControlText) <> "") Then
                        q = InStrPS(TheControl.ControlText, "^")
                        If (q <> 0) Then
                            VisualText = Left(TheControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(TheControl.ControlText, q + 1, Len(TheControl.ControlText) - q)
                        Else
                            VisualText = TheControl.ControlText
                        End If
                    Else
                        VisualText = TheControl.ControlName
                    End If
                    q = InStrPS(TheControl.ControlTemplateName, "/")
                    If (q <> 0) Then
                        iMin = Val(Trim(Left(TheControl.ControlTemplateName, q - 1)))
                        g = InStrPS(q + 1, TheControl.ControlTemplateName, "/")
                        If (g = 0) Then
                            iMax = Val(Trim(Mid(TheControl.ControlTemplateName, q + 1, Len(TheControl.ControlTemplateName) - q)))
                            iDefault = iMin
                        Else
                            iMax = Val(Trim(Mid(TheControl.ControlTemplateName, q + 1, (g - 1) - q)))
                            If (Mid(TheControl.ControlTemplateName, g + 1, Len(TheControl.ControlTemplateName) - g) = "?") Then
                                iDefault = -9999
                            Else
                                iDefault = Val(Trim(Mid(TheControl.ControlTemplateName, g + 1, Len(TheControl.ControlTemplateName) - g)))
                            End If
                        End If
                    Else
                        iMin = 0
                        iMax = Val(Trim(Mid(TheControl.ControlTemplateName, q + 1, Len(TheControl.ControlTemplateName) - q)))
                        iDefault = iMin
                    End If
                    If (iMax < iMin) Then
                        q = iMin
                        iMin = iMax
                        iMax = q
                    End If
                    If (iDefault < iMin) And (iDefault <> -9999) Then
                        iDefault = iMin
                    End If
                    DisplayPads(TestId, u, Counter).ButtonRange = Trim(str(iMax)) + "/" + Trim(str(iMin)) + "/" + Trim(iDefault)
                    DisplayPads(TestId, u, Counter).ButtonId = TheControl.ControlId
                    DisplayPads(TestId, u, Counter).ButtonText = VisualText
                    If (Trim(TheControl.IEChoiceName) <> "") Then
                        DisplayPads(TestId, u, Counter).ButtonName = Trim(TheControl.IEChoiceName)
                    Else
                        DisplayPads(TestId, u, Counter).ButtonName = "*" + Trim(TheControl.ControlName)
                    End If
                    If (Trim(TheControl.DecimalDefault) = "T") Then
                        DisplayPads(TestId, u, Counter).ButtonPadOrder = True
                    End If
                    If (Trim(TheControl.ICDAlias4) = "T") Then
                        DisplayPads(TestId, u, Counter).ButtonUseOtherText = True
                    End If
                    DisplayPads(TestId, u, Counter).ButtonType = Trim(TheControl.ControlType)
                    DisplayPads(TestId, u, Counter).ButtonTrigger = Trim(TheControl.ControlAlternateTrigger)
                    DisplayPads(TestId, u, Counter).ButtonTriggerText = Trim(TheControl.ControlAlternateText)
                    DisplayPads(TestId, u, Counter).ButtonOn = False
                    DisplayPads(TestId, u, Counter).Buttons = False
                    DisplayPads(TestId, u, Counter).ButtonMutual = False
                    DisplayPads(TestId, u, Counter).ButtonIncrement = Trim(TheControl.AIncrement)
                    DisplayPads(TestId, u, Counter).ButtonDeviceRef = Trim(TheControl.ICDAlias4)
                    MaxButtonsForPads = 0
                    If (TheControl.ControlVisibleAfterTrigger) Then
                        Call GetButtonsforPad(FormId, TestId, Counter, DisplayPads(TestId, u, Counter).ButtonTriggerText, TheControl.ControlVisibleAfterTrigger, MaxButtonsForPads)
                    End If
                End If
                i = i + 1
            Wend
        End If
    End If
Next u
Set TheControl = Nothing
For i = 1 To Counter
    DisplayPads(TestId, 2, i) = DisplayPads(TestId, 1, i)
Next i
End Function

Private Function GetButtonsforPad(FormId As Long, TestId As Integer, Cntr As Integer, Trigger As String, Ilook As Boolean, Counter As Integer) As Boolean
Dim i As Integer
Dim k As Integer
Dim q As Integer
Dim AControl As DynamicControls
Dim VisualText As String
k = 0
Set AControl = New DynamicControls
If (FormId > 0) Then
    AControl.FormId = FormId
    AControl.ControlAlternateTrigger = ""
    AControl.ControlAlternateText = Trigger
    AControl.ControlVisibleAfterTrigger = Ilook
    AControl.ControlType = "B"
    If (AControl.FindTriggerControl > 0) Then
        i = 1
        While (AControl.SelectControl(i))
            If Not (AControl.IgnoreIt) Then
                k = k + 1
                If (Trim(AControl.ControlText) <> "") Then
                    q = InStrPS(AControl.ControlText, "^")
                    If (q <> 0) Then
                        VisualText = Left(AControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(AControl.ControlText, q + 1, Len(AControl.ControlText) - q)
                    Else
                        VisualText = AControl.ControlText
                    End If
                Else
                    VisualText = AControl.ControlName
                End If
                For q = 1 To 2
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonId = AControl.ControlId
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonText = VisualText
                    If (Trim(AControl.IEChoiceName) <> "") Then
                        PlantTriggerPads(TestId, q, Cntr, k).ButtonName = Trim(AControl.IEChoiceName)
                    Else
                        PlantTriggerPads(TestId, q, Cntr, k).ButtonName = "*" + Trim(AControl.ControlName)
                    End If
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonType = Trim(AControl.ControlType)
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonTrigger = Trim(AControl.ControlAlternateTrigger)
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonTriggerText = Trim(AControl.ControlAlternateText)
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonOn = False
                    PlantTriggerPads(TestId, q, Cntr, k).Buttons = False
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonMutual = False
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonPadOrder = False
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonUseOtherText = False
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonIncrement = Trim(AControl.AIncrement)
                    PlantTriggerPads(TestId, q, Cntr, k).ButtonDeviceRef = Trim(AControl.ICDAlias4)
                Next q
            End If
            i = i + 1
        Wend
    End If
End If
Set AControl = Nothing
End Function

Private Function GetButtonsForTests(FormId As Long, TestId As Integer, Counter As Integer) As Boolean
Dim i As Integer
Dim q As Integer
Dim VisualText As String
Dim AControl As DynamicControls
Counter = 0
GetButtonsForTests = True
For i = 1 To 2
    DisplayBtns(TestId, i).Buttons = False
    DisplayBtns(TestId, i).ButtonOn = False
    DisplayBtns(TestId, i).ButtonName = ""
    DisplayBtns(TestId, i).ButtonText = ""
    DisplayBtns(TestId, i).ButtonRange = ""
    DisplayBtns(TestId, i).ButtonVisible = False
    DisplayBtns(TestId, i).ButtonTrigger = ""
    DisplayBtns(TestId, i).ButtonTriggerText = ""
    DisplayBtns(TestId, i).ButtonId = 0
    DisplayBtns(TestId, i).ButtonMutual = False
    DisplayBtns(TestId, i).ButtonPadOrder = False
    DisplayBtns(TestId, i).ButtonUseOtherText = False
    DisplayBtns(TestId, i).ButtonIncrement = ""
    DisplayBtns(TestId, i).ButtonDeviceRef = ""
    DisplayBtns(TestId, i).Result = ""
Next i
Set AControl = New DynamicControls
If (FormId > 0) Then
    AControl.FormId = FormId
    AControl.ControlType = "B"
    AControl.ControlAlternateText = ""
    AControl.ControlVisibleAfterTrigger = False
    If (AControl.FindTriggerControl > 0) Then
        i = 1
        While (AControl.SelectControl(i))
            If Not (AControl.IgnoreIt) Then
                If (Left(AControl.ControlName, 3) = "OS-") Or (Left(AControl.ControlName, 3) = "OD-") Then
                    Counter = Counter + 1
                    If (Trim(AControl.ControlText) <> "") Then
                        q = InStrPS(AControl.ControlText, "^")
                        If (q <> 0) Then
                            VisualText = Left(AControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(AControl.ControlText, q + 1, Len(AControl.ControlText) - q)
                        Else
                            VisualText = AControl.ControlText
                        End If
                    Else
                        VisualText = AControl.ControlName
                    End If
                    DisplayBtns(TestId, Counter).ButtonId = AControl.ControlId
                    DisplayBtns(TestId, Counter).ButtonText = VisualText
                    If (Trim(AControl.IEChoiceName) <> "") Then
                        DisplayBtns(TestId, Counter).ButtonName = Trim(AControl.IEChoiceName)
                    Else
                        DisplayBtns(TestId, Counter).ButtonName = "*" + Trim(AControl.ControlName)
                    End If
                    DisplayBtns(TestId, Counter).ButtonType = Trim(AControl.ControlType)
                    DisplayBtns(TestId, Counter).ButtonTrigger = Trim(AControl.ControlAlternateTrigger)
                    DisplayBtns(TestId, Counter).ButtonTriggerText = "Btn1"
                    DisplayBtns(TestId, Counter).Buttons = False
                    DisplayBtns(TestId, Counter).ButtonVisible = AControl.ControlVisible
                    DisplayBtns(TestId, Counter).ButtonMutual = False
                    DisplayBtns(TestId, Counter).ButtonPadOrder = False
                    DisplayBtns(TestId, Counter).ButtonUseOtherText = False
                    DisplayBtns(TestId, Counter).ButtonIncrement = Trim(AControl.AIncrement)
                    DisplayBtns(TestId, Counter).ButtonDeviceRef = Trim(AControl.ICDAlias4)
                    MaxButtonsForButtons = 0
                    Call GetButtonsforButtons(FormId, TestId, Counter, DisplayBtns(TestId, Counter).ButtonTriggerText, AControl.ControlVisibleAfterTrigger, MaxButtonsForButtons)
                    MaxWriteButtonsForTests(TestId) = MaxButtonsForButtons
                End If
            End If
            i = i + 1
        Wend
    End If
End If
Set AControl = Nothing
End Function

Private Function GetButtonsforButtons(FormId As Long, TestId As Integer, Cntr As Integer, Trigger As String, Ilook As Boolean, Counter As Integer) As Boolean
Dim i As Integer
Dim q As Integer
Dim VisualText As String
Dim AControl As DynamicControls
Set AControl = New DynamicControls
If (FormId > 0) Then
    AControl.FormId = FormId
    AControl.ControlAlternateText = Trigger
    AControl.ControlVisibleAfterTrigger = Ilook
    AControl.ControlType = "B"
    If (AControl.FindTriggerControl > 0) Then
        i = 1
        While (AControl.SelectControl(i))
            If Not (AControl.IgnoreIt) Then
                If (Left(AControl.ControlName, 3) <> "OS-") And (Left(AControl.ControlName, 3) <> "OD-") Then
                    Counter = Counter + 1
                    If (Trim(AControl.ControlText) <> "") Then
                        q = InStrPS(AControl.ControlText, "^")
                        If (q <> 0) Then
                            VisualText = Left(AControl.ControlText, q - 1) + Chr(13) + Chr(10) + Mid(AControl.ControlText, q + 1, Len(AControl.ControlText) - q)
                        Else
                            VisualText = AControl.ControlText
                        End If
                    Else
                        VisualText = AControl.ControlName
                    End If
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonId = AControl.ControlId
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonText = VisualText
                    If (Trim(AControl.IEChoiceName) <> "") Then
                        PlantTriggerButtons(TestId, Cntr, Counter).ButtonName = Trim(AControl.IEChoiceName)
                    Else
                        PlantTriggerButtons(TestId, Cntr, Counter).ButtonName = "*" + Trim(AControl.ControlName)
                    End If
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonType = Trim(AControl.ControlType)
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonTrigger = Trim(AControl.ControlAlternateTrigger)
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonTriggerText = Trim(AControl.ControlAlternateText)
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonOn = False
                    PlantTriggerButtons(TestId, Cntr, Counter).Buttons = False
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonMutual = False
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonPadOrder = False
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonUseOtherText = False
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonIncrement = Trim(AControl.AIncrement)
                    PlantTriggerButtons(TestId, Cntr, Counter).ButtonDeviceRef = Trim(AControl.ICDAlias4)
                End If
            End If
            i = i + 1
        Wend
    End If
End If
Set AControl = Nothing
End Function

Private Sub PostReference(TheEye As String, TheRecNum As Integer, TheValue As String, TheTest As Integer)
Dim i As Integer
Dim k As Integer
Dim l As Integer
Dim z As Integer
Dim q As Integer
Dim StartEye As Integer
Dim EndEye As Integer
Dim Eye As Integer
Dim Found As Boolean
Eye = 1
If (TheEye = "OD") Then
    Eye = 1
ElseIf (TheEye = "OS") Then
    Eye = 2
Else
    Eye = 3
End If
Found = False
If (Eye = 3) Then
    StartEye = 1
    EndEye = 2
Else
    StartEye = Eye
    EndEye = Eye
End If
If (TheTest < 1) Then
    TheTest = 1
End If
For i = 1 To MaxTests
    If (DisplayTests(i).ButtonId = TheRecNum) Then
        DisplayTests(i).Buttons = True
        DisplayTests(i).Result = TheValue
        TheTest = i
        Found = True
        Exit For
    End If
Next i
If Not Found Then
    For i = TheTest To TheTest
        For k = 1 To MaxPadsForTests
            For q = StartEye To EndEye
                If (TheRecNum = DisplayPads(i, q, k).ButtonId) Then
                    If Not (DisplayPads(i, q, k).Buttons) Then
                        DisplayPads(i, q, k).Buttons = True
                        DisplayPads(i, q, k).Result = TheValue
                        Found = True
                        Exit For
                    End If
                End If
                For l = 1 To MaxPadChoices
                    If (DisplayPads(i, q, k).Buttons) Then
                        If (TheRecNum = PlantTriggerPads(i, q, k, l).ButtonId) Then
                            If Not (PlantTriggerPads(i, q, k, l).Buttons) Then
                                PlantTriggerPads(i, q, k, l).Buttons = True
                                PlantTriggerPads(i, q, k, l).Result = TheValue
                                Found = True
                                Exit For
                            End If
                        End If
                    End If
                Next l
                If (Found) Then
                    Exit For
                End If
            Next q
        Next k
        If (Found) Then
            Exit For
        End If
    Next i
End If
If Not Found Then
    For i = TheTest To TheTest
        For q = StartEye To EndEye
            If (TheRecNum = DisplayBtns(i, q).ButtonId) Then
                If Not (DisplayBtns(i, q).Buttons) Then
                    DisplayBtns(i, q).Buttons = True
                    DisplayBtns(i, q).Result = TheValue
                    Found = True
                    Exit For
                End If
            End If
            For k = 1 To MaxButtonsAllowed
                If (TheRecNum = PlantTriggerButtons(i, q, k).ButtonId) Then
                    If Not (PlantTriggerButtons(i, q, k).Buttons) Then
                        PlantTriggerButtons(i, q, k).Buttons = True
                        PlantTriggerButtons(i, q, k).Result = TheValue
                        Found = True
                        Exit For
                    End If
                End If
            Next k
            If (Found) Then
                Exit For
            End If
        Next q
    Next i
End If
If Not Found Then
    For q = TheTest To TheTest
        For i = 1 To MaxTestWideBtns
            If (TheRecNum = TestWideButtons(q, i).ButtonId) Then
                TestWideButtons(q, i).Buttons = True
                TestWideButtons(q, i).ButtonOn = False
                If (TestWideButtons(q, i).ButtonMutual) Then
                    TestWideButtons(q, i).ButtonOn = True
                End If
                Found = True
                Exit For
            End If
        Next i
    Next q
End If
End Sub

Private Function VerifyScratchFile(TheName As String) As Boolean
Dim q As Integer
Dim Rec As String
VerifyScratchFile = True
q = 0
If (FM.IsFileThere(TheName)) Then
    FM.OpenFile TheName, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    While Not (EOF(101))
        Line Input #101, Rec
        q = q + 1
    Wend
    FM.CloseFile CLng(101)
End If
If (q < 3) Then
    VerifyScratchFile = False
End If
End Function

Private Function ParseTest(IType As Integer, ODRx As String, OSRx As String, _
                           SphereOD As String, CylinderOD As String, AxisOD As String, _
                           ReadingOD As String, ReadingIOD As String, Angle1OD As String, _
                           Angle2OD As String, VertexOD As String, _
                           SphereOS As String, CylinderOS As String, AxisOS As String, _
                           ReadingOS As String, ReadingIOS As String, Angle1OS As String, _
                           Angle2OS As String, VertexOS As String) As Boolean

Dim Idx As Integer
Dim i As Integer, q As Integer
Dim u As Integer, w As Integer
Dim Temp As String
Dim TName As String
Dim ATemp As String
Dim KeyWords(5) As String
If (IType = 0) Then
    KeyWords(1) = "ADD READING:"
    KeyWords(2) = "ADD INTERMEDIATE:"
    KeyWords(3) = "PRISM OF ANGLE 1:"
    KeyWords(4) = "PRISM OF ANGLE 2:"
    KeyWords(5) = "Vertex dist:"
ElseIf (IType = 1) Then
    KeyWords(1) = "ADD-READING:"
    KeyWords(2) = "ADD-INTERMEDIATE:"
    KeyWords(3) = "PRISM OF ANGLE 1:"
    KeyWords(4) = "PRISM OF ANGLE 2:"
    KeyWords(5) = "Vertex dist:"
ElseIf (IType = 2) Then
    KeyWords(1) = "ADD-READING:"
    KeyWords(2) = "BASE CURVE:"
    KeyWords(3) = "DIAMETER:"
    KeyWords(4) = "PERIPH CURVE:"
    KeyWords(5) = ""
Else
    KeyWords(1) = ""
    KeyWords(2) = ""
    KeyWords(3) = ""
    KeyWords(4) = ""
    KeyWords(5) = ""
End If
SphereOD = ""
CylinderOD = ""
AxisOD = ""
ReadingOD = ""
ReadingIOD = ""
Angle1OD = ""
Angle2OD = ""
VertexOD = ""
SphereOS = ""
CylinderOS = ""
AxisOS = ""
ReadingOS = ""
ReadingIOS = ""
Angle1OS = ""
Angle2OS = ""
VertexOS = ""
If (Trim(ODRx) <> "") Then
    If (Left(ODRx, 2) = "OD") Then
        ODRx = Trim(Mid(ODRx, 4, Len(ODRx) - 3))
    End If
    q = InStrPS(ODRx, " ")
    If (q > 0) Then
        Temp = Trim(Left(ODRx, q))
        If (Temp = "NE") Then
            Temp = "    "
        End If
        w = InStrPS(q + 1, ODRx, " ")
        If (w > 0) Then
            TName = Mid(ODRx, q + 1, (w - 1) - q)
            If (UCase(TName) = "BALANCE") Then
                Temp = Temp + " " + Trim(TName) + " LENS"
                q = w + 5
            ElseIf (UCase(TName) = "VERTEX") Then
            
            End If
        End If
        SphereOD = Temp
    ElseIf (Trim(ODRx) <> "") Then
        Temp = Trim(ODRx)
        SphereOD = Temp
    End If
    If (UCase(TName) <> "VERTEX") Then
        i = InStrPS(q + 1, ODRx, " ")
        If (i > 0) Then
            Temp = Trim(Mid(ODRx, q + 1, i - q))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            If (UCase(Left(Temp, 3)) <> "ADD") And (UCase(Left(Temp, 3)) <> "PRI") And (UCase(Left(Temp, 3)) <> "BAS") And (UCase(Left(Temp, 3)) <> "DIA") And (UCase(Left(Temp, 1)) <> "X") Then
                If (UCase(Temp) = "VERTEX") Then
                    Temp = "    "
                End If
                CylinderOD = Temp
            End If
        End If
        q = InStrPS(ODRx, "X")
        If (q < 1) Then
            q = InStrPS(ODRx, "x")
        End If
        If (q > 0) Then
            i = InStrPS(q, ODRx, " ")
            If (i > 0) Then
                Temp = Trim(Mid(ODRx, q, i - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                If (UCase(Temp) = "X") Then
                
                Else
                    AxisOD = Temp
                End If
            Else
                Temp = Trim(Mid(ODRx, q, Len(ODRx) - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                If (UCase(Temp) = "X") Then

                Else
                    AxisOD = Temp
                End If
            End If
        End If
    End If
' OD Data
    Idx = 1
    Temp = ODRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        ReadingOD = ATemp
    End If
    
    Idx = 2
    Temp = ODRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        ReadingIOD = ATemp
    End If
    
    Idx = 3
    Temp = ODRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        Angle1OD = ATemp
    End If
    
    Idx = 4
    Temp = ODRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        Angle2OD = ATemp
    End If

    Idx = 5
    Temp = ODRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        VertexOD = ATemp
    End If
End If
If (Trim(OSRx) <> "") Then
' OS Data
    If (Left(OSRx, 2) = "OS") Then
        OSRx = Trim(Mid(OSRx, 4, Len(OSRx) - 3))
    End If
    q = InStrPS(OSRx, " ")
    If (q > 0) Then
        Temp = Trim(Left(OSRx, q))
        If (Temp = "NE") Then
            Temp = "    "
        End If
        w = InStrPS(q + 1, OSRx, " ")
        If (w > 0) Then
            TName = Mid(OSRx, q + 1, (w - 1) - q)
            If (UCase(TName) = "BALANCE") Then
                Temp = Temp + " " + Trim(TName) + " LENS"
                q = w + 5
            ElseIf (UCase(TName) = "VERTEX") Then
            
            End If
        End If
        SphereOS = Temp
    ElseIf (Trim(OSRx) <> "") Then
        Temp = Trim(OSRx)
        SphereOS = Temp
    End If
    If (UCase(TName) <> "VERTEX") Then
        i = InStrPS(q + 1, OSRx, " ")
        If (i > 0) Then
            Temp = Trim(Mid(OSRx, q + 1, i - q))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            If (UCase(Left(Temp, 3)) <> "ADD") And (UCase(Left(Temp, 3)) <> "PRI") And (UCase(Left(Temp, 3)) <> "BAS") And (UCase(Left(Temp, 3)) <> "DIA") And (UCase(Left(Temp, 1)) <> "X") Then
                CylinderOS = Temp
            End If
        End If
        q = InStrPS(OSRx, "X")
        If (q < 1) Then
            q = InStrPS(OSRx, "x")
        End If
        If (q > 0) Then
            i = InStrPS(q, OSRx, " ")
            If (i > 0) Then
                Temp = Trim(Mid(OSRx, q, i - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                If (UCase(Temp) = "X") Then

                Else
                    AxisOS = Temp
                End If
            Else
                Temp = Trim(Mid(OSRx, q, Len(OSRx) - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                If (UCase(Temp) = "X") Then

                Else
                    AxisOS = Temp
                End If
            End If
        End If
    End If
    
    Idx = 1
    Temp = OSRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        ReadingOS = ATemp
    End If
    
    Idx = 2
    Temp = OSRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        ReadingIOS = ATemp
    End If
    
    Idx = 3
    Temp = OSRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        Angle1OS = ATemp
    End If
    
    Idx = 4
    Temp = OSRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        Angle2OS = ATemp
    End If
    
    Idx = 5
    Temp = OSRx
    GoSub GetField
    If (ATemp <> "") Then
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        VertexOS = ATemp
    End If
End If
Exit Function
GetField:
    ATemp = ""
    If (Idx > 0) Then
        If (KeyWords(Idx) <> "") Then
            i = InStrPS(UCase(Temp), UCase(KeyWords(Idx)))
            If (i > 0) Then
                GoSub PutEnd
                If (u > 0) Then
                    ATemp = Mid(Temp, i, (u - 1) - (i - 1))
                Else
                    ATemp = Mid(Temp, i, Len(Temp) - (i - 1))
                End If
            End If
        End If
    End If
    If (Trim(ATemp) <> "") Then
        i = InStrPS(ATemp, ":")
        If (i > 0) Then
            ATemp = Trim(Mid(ATemp, i + 1, Len(ATemp) - i))
        End If
    End If
    Return
PutEnd:
    u = 0
    For w = Idx + 1 To 5
        If (Trim(KeyWords(w)) <> "") Then
            u = InStrPS(UCase(Temp), UCase(KeyWords(w)))
            If (u > 0) Then
                Exit For
            End If
        End If
    Next w
    Return
End Function

Private Sub AutoFitFlexGrid(ALst As MSFlexGrid)
Dim r As Long
Dim c As Long
Dim cell_wid As Single
Dim col_wid As Single
For c = 0 To ALst.Cols - 1
    col_wid = 0
    For r = 0 To ALst.Rows - 1
        cell_wid = TextWidth(ALst.TextMatrix(r, c))
        If col_wid < cell_wid Then col_wid = cell_wid
    Next r
    ALst.ColWidth(c) = col_wid + 120
Next c
End Sub

Private Function SetRxCnt() As Integer
Dim i As Integer
SetRxCnt = 0
For i = 0 To lstRxs(0).Rows - 1 Step 2
    If (lstRxs(0).TextMatrix(i, 0) <> "") Then
        SetRxCnt = SetRxCnt + 1
    End If
Next i
End Function


Private Sub frNewEl_focus_LostFocus()
frNewEl.Visible = False
End Sub


Private Sub lstRxs_Click(i As Integer)
Set fgc = lstRxs(i)

'hist
If i > 1 And fgc.col > 1 Then
    mFocus False
    Exit Sub
End If
'==============================================


Dim ExpRow As Boolean
If i = 0 Then
    ExpRow = (fgc.RowHeight(fgc.Row) = 510)
End If

If Not ExpendedMode Then
    If Not ExpRow Then
        ExpendRow
        If i = 0 Then
            If frLine(0).Visible Then
                mApply
                setEditMode False
            End If
            Exit Sub
        End If
    End If
End If

If CopyMode Then
    mPaste
Else
    If Not bysys Then
    Dim r As Integer
        r = Fix(fgc.Row / 2) * 2
        If IsDate(fgc.TextMatrix(r + 1, 0)) Then
            If fgc.col > 1 Then
                mFocus False
                Exit Sub
            End If
        End If
        If frLine(0).Visible And i < 2 Then
            mApply
            setEditMode False
        End If
        Select Case i
        Case 0
            Select Case fgc.TextMatrix(r, 0)
            Case cmdH(1).Caption, cmdH(2).Caption
                Select Case fgc.col
                Case 6, 7, 11, 12
                    Exit Sub
                End Select
            Case cmdH(9).Caption, cmdH(10).Caption
                Select Case fgc.col
                Case 6, 7, 9, 10, 11, 12
                    Exit Sub
                End Select
                        Case cmdH(12).Caption
                            Select Case fgc.col
                                Case 5, 8
                                    Exit Sub
                                Case 9, 10, 11, 12
                                    If NotAllowed(fgc) Then
                                        Exit Sub
                                    End If
                                Case 13
                                    If Not Fix(fgc.Row / 2) * 2 = fgc.Row Then
                                        Exit Sub
                                    End If
                            End Select
                    End Select
        Case 1
            Select Case fgc.TextMatrix(r, 0)
            Case cmdH(16).Caption, cmdH(17).Caption
                Select Case fgc.col
                Case 5, 7
                    Exit Sub
                End Select
            End Select
        End Select
        
        setEditMode True
    End If
End If

End Sub

Private Function NotAllowed(fgc As MSFlexGrid) As Boolean
Dim e(1, 3) As Boolean
Dim r As Integer
r = Fix(fgc.Row / 2) * 2

e(0, 0) = Not (fgc.TextMatrix(r, 9) = "")
e(0, 1) = Not (fgc.TextMatrix(r, 10) = "")
e(0, 2) = Not (fgc.TextMatrix(r, 11) = "")
e(0, 3) = Not (fgc.TextMatrix(r, 12) = "")
e(1, 0) = Not (fgc.TextMatrix(r + 1, 9) = "")
e(1, 1) = Not (fgc.TextMatrix(r + 1, 10) = "")
e(1, 2) = Not (fgc.TextMatrix(r + 1, 11) = "")
e(1, 3) = Not (fgc.TextMatrix(r + 1, 12) = "")

Select Case fgc.col
Case 9, 10
    If e(0, 2) _
    Or e(0, 3) _
    Or e(1, 2) _
    Or e(1, 3) Then
        NotAllowed = True
    End If
Case 11, 12
    If e(0, 0) _
    Or e(0, 1) _
    Or e(1, 0) _
    Or e(1, 1) Then
        NotAllowed = True
    End If
End Select

End Function

Private Sub mApply()
If Not frEdit.Visible Then Exit Sub

If sFGC(2) > 1 Then
    lstRxs(sFGC(0)).TextMatrix(sFGC(1), sFGC(2)) = CorrValue(FGCCell.TextMatrix(1, 1))
    If sFGC(0) = 0 And sFGC(2) = 12 Then
        Dim v As String
        Dim r As Integer
        v = lstRxs(sFGC(0)).TextMatrix(sFGC(1), sFGC(2))
        r = Fix(sFGC(1) / 2) * 2
        lstRxs(0).TextMatrix(r, 12) = v
        lstRxs(0).TextMatrix(r + 1, 12) = v
    End If
End If
End Sub

Private Sub ExpendRow()
If fgc Is Nothing Then Exit Sub
Dim SelRow As Integer
If fgc.Index = 0 Then
    SelRow = Fix(fgc.Row / 2) * 2
Else
    SelRow = -10
End If

Dim r As Integer
Dim h As Integer

For r = 0 To lstRxs(0).Rows - 1
    h = 255
    Select Case r
    Case SelRow To SelRow + 1
        h = 510
    End Select
    lstRxs(0).RowHeight(r) = h
Next

AdjustFGC

End Sub

Private Sub mPaste()
Dim noComm As Boolean
Dim r1 As Integer
Dim r2 As Integer
Dim c As Integer
Dim stopper As Boolean
frInfo.Visible = False
With lstRxs(sFGC(0))
    r1 = sFGC(1)
    r2 = fgc.Row
    If sFGC(2) = 0 Then
        r2 = Fix(r2 / 2) * 2
    End If
    
    Select Case fgc.TextMatrix(r2, 0)
    Case cmdH(12).Caption, cmdH(17).Caption
        noComm = True
    End Select
    Do
        For c = 2 To .Cols - 1
            If c = 13 And noComm Then
                fgc.TextMatrix(r2, c) = ""
            Else
                If fgc.Index = sFGC(0) _
                Or fgc.Index = sFGC(0) - 2 Then
                    fgc.TextMatrix(r2, c) = .TextMatrix(r1, c)
                Else
                    Select Case c
                    Case Is < 7
                        fgc.TextMatrix(r2, c) = .TextMatrix(r1, c)
                    Case 7
                        If sFGC(0) = 0 _
                        Or sFGC(0) = 2 Then
                            fgc.TextMatrix(r2, 7) = .TextMatrix(r1, 8)
                            fgc.TextMatrix(r2, 8) = ""
                        Else
                            fgc.TextMatrix(r2, 8) = .TextMatrix(r1, 7)
                            fgc.TextMatrix(r2, 7) = ""
                        End If
                    Case 8
                    Case Else
                        fgc.TextMatrix(r2, c) = ""
                    End Select
                End If
            End If
        Next
        If stopper Then Exit Do
        If sFGC(2) = 0 Then
            r1 = r1 + 1
            r2 = r2 + 1
            stopper = True
        Else
            Exit Do
        End If
    Loop
End With

ClearFields r2 - 1

CopyMode = False
mFocus False

End Sub


Private Sub setEditMode(stat As Boolean)

If stat Then
    If fgc.col > 1 Then
        sFGC(0) = fgc.Index
        sFGC(1) = fgc.Row
        sFGC(2) = fgc.col
        CCom = 0
        
        Dim i As Integer
        Dim c As String
        c = fgc.Index & "_" & fgc.col
        
        cmdCCom(0).Visible = (c = "1_13")
        cmdCCom(1).Visible = (c = "1_13")
        cmdCCom(2).Visible = (c = "1_13")
        
        cmdOU.Visible = False
        Select Case c
        Case "0_6", "0_8", "0_12"
            Select Case fgc.TextMatrix(Fix(fgc.Row / 2) * 2, 0)
            Case cmdH(0).Caption, cmdH(3).Caption, cmdH(4).Caption, cmdH(12).Caption
                cmdOU.Visible = True
            End Select
        End Select
        
        Select Case c
        Case "1_11", "1_12"
        Case Else
            frEdit.Visible = True
            With FGCCell
                .RowHeight(1) = fgc.RowHeight(fgc.Row)
                .ColWidth(1) = fgc.ColWidth(fgc.col)
                .Width = .ColWidth(1) + 60
                .Left = ((cmdAdj(2).Left + cmdAdj(2).Width + cmdAdj(3).Left) / 2) _
                        - (.ColWidth(1) / 2)
                .CellBackColor = fgc.CellBackColor
                .TextMatrix(1, 1) = fgc.Text
            End With
        End Select
        
        Select Case c
        Case "0_2", "0_3", "0_4", "0_6", "0_7", _
             "1_2", "1_3", "1_4", "1_6", "1_9"
            Select Case fgc.col
            Case 2, 3, 6, 7
                cmdAdj(1).Text = "-1"
                cmdAdj(2).Text = "-0.25"
                cmdAdj(3).Text = "+0.25"
                cmdAdj(4).Text = "+1"
            Case 4, 9
                cmdAdj(1).Text = "-10"
                cmdAdj(2).Text = "-1"
                cmdAdj(3).Text = "+1"
                cmdAdj(4).Text = "+10"
                Call cmdNumPad_Click
                FGCCell.TextMatrix(1, 1) = fgc.Text
            End Select
            For i = 1 To 4
                cmdAdj(i).Visible = True
            Next
            
            Select Case fgc.col
            Case 2
                cmdAdj(5).Text = "Plano"
                cmdAdj(6).Text = "Bal"
                cmdAdj(5).Visible = True
                cmdAdj(6).Visible = True
            Case 4
                cmdAdj(5).Text = "x90"
                cmdAdj(6).Text = "x180"
                cmdAdj(5).Visible = True
                cmdAdj(6).Visible = True
            Case Else
                cmdAdj(5).Visible = False
                cmdAdj(6).Visible = False
            End Select
            
            cmdNumPad.Visible = True
            
        Case "0_5", "0_8", "0_9", "0_10", "0_11", "0_12", _
             "1_5", "1_7", "1_8", "1_10"
            
            For i = 1 To 6
                cmdAdj(i).Visible = False
            Next
            
            Select Case c
            Case "0_9", "0_10"
                Select Case fgc.TextMatrix(Fix(fgc.Row / 2) * 2, 0)
                Case cmdH(1).Caption, cmdH(2).Caption
                    cmdNumPad.Visible = True
                    Call cmdNumPad_Click
                    GoTo endCase
                End Select
            End Select
            
            setFixed
            cmdNumPad.Visible = False
            frFixed.Visible = True
            frFixed.ZOrder
            
endCase:
        Case "0_13"
            For i = 1 To 6
                cmdAdj(i).Visible = False
            Next
            Call cmdKeyBoard_Click
            cmdNumPad.Visible = False
        Case "1_11"
            Select Case fgc.Text
            Case ""
                fgc.Text = "D"
            Case "D"
                fgc.Text = "N"
            Case "N"
                 fgc.Text = ""
            End Select
            Exit Sub
        Case "1_12"
            CommArea = ""
            If (frmCLFavs.LoadCLFavorites(True)) Then
                frmCLFavs.cmd(0).Text = fgc.TextMatrix(fgc.Row, 1) & " Only"
                frmCLFavs.cmd(0).Visible = True
                frmCLFavs.cmd(1).Visible = True
                frmCLFavs.Show 1
                Dim CA(3) As String
                Dim del As Integer
                If Not CommArea = "" Then
                    CA(0) = CommArea
                    For i = 1 To 3
                        del = InStrPS(1, CA(0), vbTab)
                        CA(i) = Mid(CA(0), 1, del - 1)
                        CA(0) = Mid(CA(0), del + 1)
                    Next
                    Dim r As Integer
                    r = fgc.Row
                    If CA(1) = 1 Then
                        r = Fix(r / 2) * 2
                    End If
                    For i = r To r + CA(1)
                        fgc.TextMatrix(i, 12) = CA(3)
                    Next
                End If
            End If
            Exit Sub
        Case "1_13"
            'setCCom (fgc.Text = "")
            mStr(1) = ""
            mStr(2) = ""
            mStr(3) = ""
            For i = 1 To 6
                cmdAdj(i).Visible = False
            Next
            Call cmdKeyBoard_Click
            cmdNumPad.Visible = False
        End Select
        
        'refresh
        Select Case c
        Case "1_11", "1_12"
        Case Else
'            frEdit.Visible = True
'            With FGCCell
'                .RowHeight(1) = fgc.RowHeight(fgc.Row)
'                .ColWidth(1) = fgc.ColWidth(fgc.col)
'                .Width = .ColWidth(1) + 60
'                .Left = ((cmdAdj(2).Left + cmdAdj(2).Width + cmdAdj(3).Left) / 2) _
'                        - (.ColWidth(1) / 2)
'                .CellBackColor = fgc.CellBackColor
                FGCCell.TextMatrix(1, 1) = fgc.Text
'            End With
        End Select

        Select Case c
        Case "0_13"
            txtType.MaxLength = 500
        Case "1_13"
            txtType.MaxLength = 250
        Case Else
            txtType.MaxLength = 0
        End Select
    End If
Else
    CopyMode = False
    setCmd cmdNumPad, False
    frNumPad.Visible = False
    setCmd cmdKeyBoard, False
    txtType.Visible = False
    frAdj.Visible = True
    frEdit.Visible = False
    frNewEl.Visible = False
    frLast.Visible = False
    frFixed.Visible = False
    frInfo.Visible = False
    lstRxs(0).Enabled = True
    lstRxs(1).Enabled = True
End If

mFocus stat
End Sub

Private Sub setCCom(stat As Boolean)
cmdCCom(0).Visible = stat
cmdCCom(1).Visible = stat
cmdCCom(2).Visible = stat
End Sub

Private Sub setFixed()
Dim c As String
Dim i As Integer

bysys = True
For i = 0 To chkF.Count - 1
    chkF(i).Value = 0
    chkF(i).BackColor = &HFFFFC0
    chkF(i).Caption = ""
    chkF(i).Tag = ""
Next
bysys = False

c = fgc.Index & "_" & fgc.col
cmdJSw.Visible = False
Select Case c
Case "0_5", "0_8", _
     "1_5", "1_7"
        chkF(0).Caption = "20/15"
        chkF(1).Caption = "20/20"
        chkF(2).Caption = "20/25"
        chkF(3).Caption = "20/30"
        chkF(4).Caption = "20/40"
        chkF(5).Caption = "20/50"
        chkF(6).Caption = "20/60"
        chkF(7).Caption = "20/70"
        chkF(8).Caption = "20/80"
        chkF(9).Caption = "20/100"
        chkF(10).Caption = "20/150"
        chkF(11).Caption = "20/200"
        chkF(12).Caption = "20/300"
        chkF(13).Caption = "20/400"
        chkF(14).Caption = "20/800"
        chkF(15).Caption = "FC 1'"
        chkF(16).Caption = "FC 3'"
        chkF(17).Caption = "FC 5'"
        chkF(18).Caption = "HM"
        chkF(19).Caption = "Patient Unable"
        chkF(20).Caption = "Not Done"
        chkF(21).Caption = "Prosthesis"
        For i = 0 To 20
            chkF(i).Tag = "g1"
        Next
        
    Select Case c
    Case "0_8", "1_7"
        cmdJSw.Visible = True
        Dim RetCon As New PracticeConfigureInterface
        If RetCon.getFieldValue("NEARVISION") = "J" Then
            cmdJSw.Text = "Snellen"
            chkF(0).Caption = "J1+"
            chkF(1).Caption = "J1"
            chkF(2).Caption = "J2"
            chkF(3).Caption = "J3"
            chkF(4).Caption = "J5"
            chkF(5).Caption = "J7"
            chkF(6).Caption = "J10"
            chkF(7).Caption = "J16"
            chkF(8).Caption = "J20"
            For i = 9 To 13
                chkF(i).Caption = ""
            Next
        Else
            cmdJSw.Text = "Jaeger"
        End If
        Set RetCon = Nothing
    End Select
        
        
        
        chkF(22).Caption = "-1"
        chkF(23).Caption = "-2"
        chkF(24).Caption = "+1"
        chkF(25).Caption = "+2"
        For i = 22 To 25
            chkF(i).Tag = "g2"
        Next
        chkF(26).Caption = ""
        
        chkF(27).Caption = "NI"
        chkF(27).Tag = "g3"
        
        chkF(30).Caption = "R>G"
        chkF(31).Caption = "R=G"
        chkF(32).Caption = "G>R"
        For i = 30 To 32
            chkF(i).Tag = "g4"
        Next
Case "0_9", "0_10"
        chkF(0).Caption = "1"
        chkF(1).Caption = "2"
        chkF(2).Caption = "3"
        chkF(3).Caption = "4"
        chkF(4).Caption = "5"
        chkF(5).Caption = "6"
        chkF(6).Caption = "7"
        chkF(7).Caption = "8"
        chkF(8).Caption = "9"
        For i = 0 To 9
            chkF(i).Tag = "g1"
        Next
        
        chkF(22).Caption = "BU"
        chkF(23).Caption = "BD"
        chkF(24).Caption = "BI"
        chkF(25).Caption = "BO"
        For i = 22 To 25
            chkF(i).Tag = "g2"
        Next
Case "0_11"
        chkF(0).Caption = "10"
        chkF(1).Caption = "11"
        chkF(2).Caption = "12"
        chkF(3).Caption = "13"
        chkF(4).Caption = "14"
        chkF(5).Caption = "15"
        chkF(6).Caption = "16"
        chkF(7).Caption = "17"
        chkF(8).Caption = "18"
        chkF(9).Caption = "19"
        chkF(10).Caption = "20"
        For i = 0 To 10
            chkF(i).Tag = "g1"
        Next
Case "0_12"
    Dim PCode As New PracticeCodes
    PCode.ReferenceType = "PCCHOICES"
    For i = 1 To PCode.FindCode
        If PCode.SelectCode(i) Then
            chkF(i - 1).Caption = Trim(PCode.ReferenceCode)
            chkF(i - 1).Tag = "g1"
        End If
    Next
Case "1_6"
        chkF(0).Caption = "Heavy Blend"
        chkF(1).Caption = "Lignt Blend"
        chkF(2).Caption = "Normal Blend"
        For i = 0 To 2
            chkF(i).Tag = "g1"
        Next
        
        chkF(4).Caption = "Distance Lens"
        chkF(5).Caption = "Near Lens"
        For i = 4 To 5
            chkF(i).Tag = "g3"
        Next
        
        chkF(11).Caption = "High Add"
        chkF(12).Caption = "Medium Add"
        chkF(13).Caption = "Low Add"
        For i = 11 To 13
            chkF(i).Tag = "g2"
        Next
        

Case "1_8"
        chkF(0).Caption = "8.0"
        chkF(1).Caption = "8.1"
        chkF(2).Caption = "8.2"
        chkF(3).Caption = "8.3"
        chkF(4).Caption = "8.4"
        chkF(5).Caption = "8.5"
        chkF(6).Caption = "8.6"
        chkF(7).Caption = "8.7"
        chkF(8).Caption = "8.8"
        chkF(9).Caption = "8.9"
        chkF(11).Caption = "9.0"
        chkF(12).Caption = "9.1"
        chkF(13).Caption = "9.2"
        chkF(14).Caption = "9.3"
        chkF(15).Caption = "9.4"
        chkF(16).Caption = "9.5"
        chkF(17).Caption = "9.6"
        chkF(18).Caption = "9.7"
        chkF(19).Caption = "9.8"
        chkF(20).Caption = "9.9"
        chkF(21).Caption = "10"
        For i = 0 To 21
            chkF(i).Tag = "g1"
        Next
Case "1_10"
        chkF(0).Caption = "Steep"
        chkF(1).Caption = "Flat"
        chkF(2).Caption = "Skirt"
        For i = 0 To 2
            chkF(i).Tag = "g1"
        Next
Case "1_13"
    frNumPad.Visible = (CCom = 2)
    Select Case CCom
    Case 1
        chkF(0).Caption = "Prefer Red (-0.50)"
        chkF(1).Caption = "Prefer Red (-0.25)"
        chkF(2).Caption = "Red = Green"
        chkF(3).Caption = "Prefer Red (+0.25)"
        chkF(4).Caption = "Prefer Red (+0.50)"
        For i = 0 To 4
            If Not chkF(i).Caption = "" Then chkF(i).Tag = "g1"
        Next
    Case 2
        chkF(0).Caption = "deg. nasal"
        chkF(1).Caption = "deg. temp"
        chkF(2).Caption = "deg. left"
        chkF(3).Caption = "deg. right"
        chkF(11).Caption = "deg. clockwise"
        chkF(12).Caption = "deg. counter clockwise"
        chkF(13).Caption = "No Rotation"
        For i = 0 To 13
            If Not chkF(i).Caption = "" Then chkF(i).Tag = "g1"
        Next
        frNumPad.Visible = True
    Case 3
        chkF(0).Caption = "Good Centration, Movement"
        chkF(1).Caption = "Poor Movement"
        chkF(2).Caption = "Excess Movement"
        chkF(3).Caption = "Rides Low"
        chkF(4).Caption = "Rides High"
        chkF(5).Caption = "Surface Defect"
        chkF(6).Caption = "Rides Nasal"
        chkF(7).Caption = "Rides Temporal"
        chkF(8).Caption = "Inferior Lift"
        chkF(9).Caption = "Aligned"
        chkF(10).Caption = "Steep"
        chkF(11).Caption = "Air Bubble"
        chkF(12).Caption = "Flat"
        chkF(13).Caption = "Protein Deposits"
        chkF(14).Caption = "Multiple Deposits"
        chkF(15).Caption = "Coated"
        chkF(16).Caption = "Soft"
        chkF(17).Caption = "Poor Wetting Surface"
        chkF(18).Caption = "Rips"
        chkF(19).Caption = "Nicks"
        chkF(20).Caption = "Scratch"
        chkF(21).Caption = "Warp"
        For i = 0 To 21
            If Not chkF(i).Caption = "" Then chkF(i).Tag = "g" & i
        Next
    End Select
End Select

For i = 0 To chkF.Count - 1
    chkF(i).Visible = Not (chkF(i).Caption = "")
Next

Dim f As String
f = "F"
For i = 11 To 21
    If Not chkF(i).Caption = "" Then
        f = "T"
        Exit For
    End If
Next

f = f & Left(CStr(Not chkF(22).Caption = ""), 1)
Select Case f
Case "FF"
    frFixed.Width = 1290
Case "TF"
    frFixed.Width = 2490
Case "FT"
    frFixed.Width = 2730
    For i = 22 To 32
        chkF(i).Left = 1560
    Next
Case "TT"
    frFixed.Width = 3930
    For i = 22 To 32
        chkF(i).Left = 2760
    Next
End Select

frFixed.Left = Me.Width - frFixed.Width - 105

End Sub

Private Sub mFocus(stat As Boolean)
If stat Then
    If fgc.col = 0 Then
        fgc.Row = Fix(fgc.Row / 2) * 2
    End If
    
    frLine(0).Top = fgc.Top + fgc.CellTop
    frLine(0).Left = fgc.Left + fgc.CellLeft
    Select Case fgc.col
    Case 0, 1
        frLine(0).Width = fgc.Width - fgc.Left - frLine(0).Left - 45
    Case Else
        frLine(0).Width = fgc.CellWidth
    End Select
    
    frLine(2).Top = frLine(0).Top
    frLine(2).Left = frLine(0).Left
    If fgc.col = 0 Then
        frLine(2).Height = fgc.CellHeight * 2 + 30
    Else
        frLine(2).Height = fgc.CellHeight
    End If
    
    frLine(1).Top = frLine(0).Top + frLine(2).Height - 30
    frLine(1).Left = frLine(0).Left
    frLine(1).Width = frLine(0).Width
    
    frLine(3).Top = frLine(2).Top
    Select Case fgc.col
    Case 0, 1
        frLine(3).Left = frLine(0).Left + frLine(0).Width - 30
    Case Else
        frLine(3).Left = frLine(2).Left + fgc.CellWidth - 30
    End Select
    frLine(3).Height = frLine(2).Height
End If

Dim f As Integer
For f = 0 To 3
    frLine(f).Visible = stat
    frLine(f).ZOrder
Next
End Sub


Private Sub AdjustFGC()
Dim g As Integer
Dim r As Integer
Dim h(3) As Integer


For g = HistOn To HistOn + 1
    For r = 0 To lstRxs(g).Rows - 1
        If g = HistOn + 1 Or ExpendedMode Then
            lstRxs(g).RowHeight(r) = 255 * 2
        End If
        h(g) = h(g) + lstRxs(g).RowHeight(r)
    Next
Next

h(HistOn) = h(HistOn) + 45
h(HistOn + 1) = h(HistOn + 1) + 45

Dim limH As Integer
limH = 7240

limit:
If h(HistOn) + h(HistOn + 1) > limH Then
    If h(HistOn) < 2000 Then
        h(HistOn + 1) = limH - h(HistOn)
    Else
        h(HistOn) = h(HistOn) - (255 * 2)
    End If
    GoTo limit
End If

lstRxs(HistOn).Height = h(HistOn)
lstRxs(HistOn + 1).Height = h(HistOn + 1)
HlstRxs(HistOn + 1).Top = lstRxs(HistOn).Top + lstRxs(HistOn).Height
lstRxs(HistOn + 1).Top = HlstRxs(HistOn + 1).Top + HlstRxs(HistOn + 1).Height

Dim v As Boolean
v = ((lstRxs(0).Rows + lstRxs(1).Rows > 14) And (lstRxs(0).Rows > 0))
cmdRH.Visible = v
End Sub



Private Sub DoOriginalTest(TestId As String, Question As String, RId As Integer)
Dim i As Integer
Dim Style As Boolean
Dim Temp As String
Dim TheFile As String
Dim TestTime As String
TheFile = GetTestFileName(TestId)
If Not (FM.IsFileThere(TheFile)) Then
    If Not (Style) Then
        FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(1)
        Print #1, "QUESTION=\" + UCase(Trim(Question))
        Call FormatTimeNow(TestTime)
        Print #1, "TIME=" + TestTime
        FM.CloseFile CLng(1)
    End If
End If
frmEditTests.TriggerRTestId = RId
frmEditTests.ManifestResults = ""
frmEditTests.ManifestResultsOn = False
frmEditTests.StoreResults = TheFile
frmEditTests.Question = UCase(Question)
frmEditTests.PatientId = PatientId
frmEditTests.AppointmentId = AppointmentId
frmEditTests.FormOn = True
frmAssignWearNew.Enabled = False
frmEditTests.Show 1
If (frmTestResults.RecapTest(TheFile)) Then
    Call frmTestResults.PushList(TheFile)
    Call frmSystems1.ApplyTestResults(TheFile, TestId, Temp, True, True)
Else
    If Not (FM.IsFileThere(TheFile)) Then
        Call frmSystems1.ClearTestResults(TestId)
    Else
        Call frmSystems1.ApplyTestResults(TheFile, TestId, Temp, True, True)
    End If
End If
frmAssignWearNew.Enabled = True
frmAssignWearNew.ZOrder 0
If (frmAssignWearNew.BackColor <> 0) Then
    frmAssignWearNew.BackColor = frmAssignWearNew.BackColor
End If
Call LoadWear
End Sub


Private Sub ColorLine(mFGC As MSFlexGrid, r As Integer)

Dim c As Integer
Dim baseClr As Long
Dim darkClr As Long
If IsDate(mFGC.TextMatrix(r + 1, 0)) And mFGC.Index < 2 Then
    baseClr = vbWhite
Else
    baseClr = Elements(mFGC.TextMatrix(r, 0), mFGC.Index, "BackColor")
End If
darkClr = DarkColor(baseClr)

Dim mRow As Integer

For mRow = r To r + 1
    mFGC.Row = mRow
    For c = 0 To mFGC.Cols - 1
        mFGC.col = c
        If r = mRow Then
            mFGC.CellBackColor = baseClr
        Else
            mFGC.CellBackColor = darkClr
        End If
        mFGC.CellAlignment = flexAlignLeftTop
    Next
Next
End Sub

Private Function Elements(txt As String, ind As Integer, prop As String) As String
Dim s(1) As Integer
Select Case ind
Case 0, 2
    s(1) = 12
Case Else
    s(0) = 12
    s(1) = 17
End Select

Dim i As Integer

For i = s(0) To s(1)
    If cmdH(i).Caption = txt Then Exit For
Next

Select Case prop
Case "BackColor"
    Elements = cmdH(i).BackColor
Case "Index"
    Elements = i
End Select
End Function


Private Function DarkColor(BC As Long) As Long
Dim hBC As String
hBC = Right("000000" & Hex(BC), 6)

Dim cRed As Integer
Dim cGreen As Integer
Dim cBlue As Integer
cBlue = CInt("&H" & Mid(hBC, 1, 2))
cGreen = CInt("&H" & Mid(hBC, 3, 2))
cRed = CInt("&H" & Mid(hBC, 5, 2))

Dim cShift As Integer
cShift = 40

cBlue = cBlue - cShift
cGreen = cGreen - cShift
cRed = cRed - cShift
If cBlue < 0 Then cBlue = 0
If cGreen < 0 Then cGreen = 0
If cRed < 0 Then cRed = 0
DarkColor = RGB(cRed, cGreen, cBlue)
End Function

Private Sub lstRxs_Scroll(Index As Integer)
setEditMode False
End Sub

Private Sub lstVSC_Click()
lstVSC.Visible = False
setCmd cmd(6), False
End Sub

Private Sub txtType_Change()
FGCCell.TextMatrix(1, 1) = txtType.Text
End Sub

Private Sub txtType_KeyPress(KeyAscii As Integer)
'setCCom False
End Sub


Private Sub LoadFiles()
If Titles Is Nothing Then
    Set Titles = New Scripting.Dictionary
End If
Titles.removeAll

Titles.Add "ARF", ""
Titles.Add "MRX", ""
Titles.Add "RSY", ""
Titles.Add "=T0", ""
Titles.Add "SPECRX", ""
Titles.Add "PAIR-", ""
Titles.Add "CLTRIAL", ""
Titles.Add "CLORDER", ""
Titles.Add "CLRX", ""
Titles.Add "OD", 0
Titles.Add "OS", 0
Titles.Add "SPHERE", 1
Titles.Add "CYLINDER", 1
Titles.Add "AXIS", 1
Titles.Add "ADD-READ", 1
Titles.Add "ADD-INTER", 1
Titles.Add "PRISMOFANGLE1", 1
Titles.Add "PRISMOFANGLE2", 1
Titles.Add "POA1", 1
Titles.Add "POA2", 1
Titles.Add "VERTEX", 1
Titles.Add "VAD", 1
Titles.Add "VAN", 1
Titles.Add "VA-", 1
Titles.Add "TYPE", 1
Titles.Add "COMMENT", 1
Titles.Add "BASE CURVE", 1
Titles.Add "PERIPH CURVE", 1
Titles.Add "DIAMETER", 1
Titles.Add "MODEL", 1
Titles.Add "READERS", 2
Titles.Add "DISTANCE", 2
Titles.Add "PROGRESSIVES", 2
Titles.Add "EXEC BIFOCALS", 2
Titles.Add "BIFOCALS", 2
Titles.Add "TRIFOCALS", 2
Titles.Add "COMPUTER", 2
Titles.Add "RELIABILITY", 2
Titles.Add "PD", 2
Titles.Add "DN", 2

Titles.Add "PH-DISTANCE", "W"
Titles.Add "NEAR", "W"
Titles.Add "PH-NEAR", "W"
Titles.Add "MEASUREMENT1", "W"
Titles.Add "AXIS1", "W"
Titles.Add "MEASUREMENT2", "W"
Titles.Add "AXIS2", "W"
Titles.Add "AVE", "W"
Titles.Add "CYL", "W"
Titles.Add "DIOPTERS", "W"
Titles.Add "RADIUS", "W"


Dim FType As String
Dim f As String
f = Dir(DoctorInterfaceDirectory & "T*_" & Trim(str(AppointmentId)) & "_" + Trim(str(PatientId)) & ".txt")

Do While (f <> "")
    FType = Mid(f, 2, 3)
    Select Case FType
    Case "07A", "08A", "09A", "09D", "10A", "13A", "07R", "07S", "07T", "07U", "09G"
        ReadFile f
    Case "02A", "12A"
        ReadFileVSC f
    End Select
    f = Dir
Loop

End Sub

Private Sub ReadFile(FileName As String)
Dim FLine() As String
Dim l As Integer
FM.OpenFile DoctorInterfaceDirectory & FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(1)

ReDim Preserve FLine(0)
FLine(0) = FileName
Do Until EOF(1)
    l = l + 1
    ReDim Preserve FLine(l)
    Line Input #1, FLine(l)
Loop
FM.CloseFile CLng(1)
        
Dim NoteTemp As String
Dim NoteId As Long
Dim TestId As String
Dim RetrieveNotes As PatientNotes

TestId = Mid(FLine(0), 2, 3)
If (GetNotesForTests(PatientId, AppointmentId, TestId, NoteTemp, NoteId)) Then
    Set RetrieveNotes = New PatientNotes
    RetrieveNotes.NotesId = NoteId
    If (RetrieveNotes.RetrieveNotes) Then
        RetrieveNotes.DeleteNotes
    End If
    Set RetrieveNotes = Nothing
End If
        
ProcFile FLine, NoteTemp
        
End Sub

Private Sub ProcFile(FLine() As String, NoteTemp As String)
        
Dim l As Integer
Dim RIndex As Integer
Dim mLine As String
        
Dim Values(1) As String
        
For l = 1 To UBound(FLine)
    Select Case True
    Case UCase(Mid(FLine(l), 1, 5)) = "*ARF-" 'AUTO REFRACTION
        Select Case Mid(FLine(l), 6, 1)
        Case "1", "2", "3"
            RIndex = 1
        Case "4"
            RIndex = 2
        Case Else
            RIndex = -1
            MsgBox "Error"
        End Select
    Case UCase(Mid(FLine(l), 1, 5)) = "*MRX-" 'MANIFEST REFRACTION
        Select Case Mid(FLine(l), 6, 1)
        Case "1", "2", "3"
            RIndex = 3
        Case "4"
            RIndex = 4
        Case "5"
            RIndex = 7
        Case "6"
            RIndex = 8
        Case Else
            RIndex = -1
            MsgBox "Error"
        End Select
    Case UCase(Mid(FLine(l), 1, 5)) = "*RSY-" '
        Select Case Mid(FLine(l), 6, 1)
        Case "1"
            RIndex = 9
        Case "2"
            RIndex = 10
        Case Else
            RIndex = -1
            MsgBox "Error"
        End Select
    Case UCase(Mid(FLine(l), 1, 4)) = "*=T0" 'DILATED REFRACTION
        RIndex = 11
    Case UCase(Mid(FLine(l), 1, 6)) = "*PAIR-" 'GLASSES / CONTACT LENSES
        Select Case UCase(Mid(FLine(l), 7, 1))
        Case "1", "2", "3", "4", "5", "6"
            Select Case Mid(FLine(1), 11)
            Case "GLASSES"
                RIndex = 0
            Case "CONTACT LENSES"
                RIndex = 13
            Case Else
                RIndex = -1
                MsgBox "Error"
            End Select
        Case Else
            RIndex = -1
            'MsgBox "Error"
        End Select
    Case UCase(Mid(FLine(l), 1, 7)) = "*SPECRX" '/SPEC RX
        Select Case UCase(Mid(FLine(l), 8, 1))
        Case "1", "2", "3", "4", "5", "6"
            RIndex = 12
        Case Else
            RIndex = -1
            MsgBox "Error"
        End Select
    Case UCase(Mid(FLine(l), 1, 8)) = "*CLTRIAL" '/CL TRIAL
        Select Case UCase(Mid(FLine(l), 9, 1))
        Case "1", "2"
            RIndex = 14
        Case Else
            RIndex = -1
            MsgBox "Error"
        End Select
    Case UCase(Mid(FLine(l), 1, 13)) = "*CLORDERTRIAL" '/CL ORDER TRIAL
        Select Case UCase(Mid(FLine(l), 14, 1))
        Case "1", "2", "3", "4", "5", "6"
            RIndex = 16
        Case Else
            RIndex = -1
            MsgBox "Error"
        End Select
    Case UCase(Mid(FLine(l), 1, 5)) = "*CLRX" '/CL RX
        Select Case UCase(Mid(FLine(l), 6, 1))
        Case "1", "2", "3", "4", "5", "6"
            RIndex = 17
        Case Else
            RIndex = -1
            MsgBox "Error"
        End Select
    Case Else
        RIndex = -1
    End Select
    
    If Not RIndex < 0 Then GoSub GetData
Next
        
Exit Sub

GetData:
mLine = ""
Do While Left(FLine(l), 1) = "*"
    If Left(FLine(l), 1) = "*" Then
        mLine = mLine & vbTab & FLine(l)
    End If
    l = l + 1
    If l > UBound(FLine) Then Exit Do
Loop
l = l - 1
Dim OD As String
Dim OS As String

LoadLine RIndex, mLine, OD, OS
OD = OD & " " & NoteTemp
InsertRow RIndex, OD, OS
Return
End Sub

Private Function GetNotesForTests(PatId As Long, ApptId As Long, TstId As String, _
                                  TheNote As String, Optional TheNoteId As Long) As Boolean
Dim i As Integer
Dim RetNote As PatientNotes
GetNotesForTests = False
TheNote = ""
If (PatId > 0) And (ApptId > 0) Then
    Set RetNote = New PatientNotes
    RetNote.NotesAppointmentId = ApptId
    RetNote.NotesPatientId = PatId
    RetNote.NotesSystem = "T" + TstId
    RetNote.NotesType = "C"
    If (RetNote.FindNotes > 0) Then
        i = 1
        While (RetNote.SelectNotes(i))
            If (Trim(RetNote.NotesSystem) = "T" + TstId) And (Trim(RetNote.NotesOffDate) = "") Then
                TheNote = Trim(TheNote) + Trim(RetNote.NotesText1) + Trim(RetNote.NotesText2) + Trim(RetNote.NotesText3) + Trim(RetNote.NotesText4)
                TheNoteId = RetNote.NotesId
                GetNotesForTests = True
            End If
            i = i + 1
        Wend
    End If
    Set RetNote = Nothing
End If
End Function



Private Sub LoadLine(RInd As Integer, inp As String, OD As String, OS As String)
Dim mLine As String
mLine = Mid(inp, 2)
Dim mL() As String
Dim tstL As String

Dim l As Integer
Dim sDelim As Integer
Do Until mLine = ""
    sDelim = InStrPS(1, mLine, Chr(9))
    If sDelim > 0 Then
        tstL = Mid(mLine, 1, sDelim - 1)
        mLine = Mid(mLine, sDelim + 1)
    Else
        tstL = mLine
        mLine = ""
    End If
    ReDim Preserve mL(l)
    mL(l) = tstL
    l = l + 1
Loop

Dim mTitle As String
Dim mVal As String

Dim ODOS(1) As ODOSType
Dim t As Integer
'======================================================================================
'parse input
'    Sphere
'    Cyl
'    Axis
'    Va_Dist
'    tAdd
'    Int_Add
'    Va_Near
'    Psm_1
'    Psm_2
'    Vert
'    tType
'    Comment
'    BC
'    Dia
'    PC
'    D_N
'    Model
'    Temp
For l = 0 To UBound(mL)
    getTitleValue mL(l), mTitle, mVal
    If Not mTitle = "" Then
        If IsNumeric(Titles(mTitle)) Then
            Select Case mTitle
            Case "OD"
                t = 0
            Case "OS"
                t = 1
            Case "SPHERE"
                ODOS(t).SPHERE = mVal
            Case "CYLINDER"
                ODOS(t).CYL = mVal
            Case "AXIS"
                If IsNumeric(mVal) Then mVal = "x" & mVal
                ODOS(t).Axis = mVal
            Case "ADD-READ"
                ODOS(t).TADD = mVal
            Case "ADD-INTER"
                ODOS(t).INT_ADD = mVal
            Case "PRISMOFANGLE1", "POA1", "RELIABILITY"
                ODOS(t).PSM_1 = ODOS(t).PSM_1 & mVal & " "
            Case "PRISMOFANGLE2", "POA2", "PD"
                ODOS(t).PSM_2 = ODOS(t).PSM_2 & mVal & " "
            Case "VERTEX"
                ODOS(t).VERT = mVal
            Case "VAD", "VA-"
                ODOS(t).VA_DIST = ODOS(t).VA_DIST & mVal & " "
            Case "VAN"
                ODOS(t).VA_NEAR = ODOS(t).VA_NEAR & mVal & " "
            Case "TYPE"
                ODOS(t).TTYPE = mVal
            Case "COMMENT"
                ODOS(t).COMMENT = mVal
            Case "BASE CURVE"
                ODOS(t).BC = ODOS(t).BC & mVal & " "
            Case "DIAMETER"
                ODOS(t).DIA = mVal
            Case "PERIPH CURVE"
                ODOS(t).PC = ODOS(t).PC & mVal & " "
            Case "MODEL"
                ODOS(t).Model = mVal
            Case "READERS", "DISTANCE", "PROGRESSIVES", "EXEC BIFOCALS", "BIFOCALS", "TRIFOCALS", "COMPUTER"
                ODOS(t).TTYPE = mTitle
            Case "DN"
                ODOS(t).D_N = mVal
            Case "_"
                ODOS(t).Temp = mVal
            Case Else
                'Debug.Print mL(l)
                MsgBox mL(l)
            End Select
        End If
    End If
Next

'======================================================================================
'load line
Dim tmpODOS(1) As String
Dim i As Integer
For i = 0 To 1
    If RInd < 13 Then
        tmpODOS(i) = "" & _
        vbTab & Trim(ODOS(i).SPHERE) & _
        vbTab & Trim(ODOS(i).CYL) & _
        vbTab & Trim(ODOS(i).Axis) & _
        vbTab & Trim(ODOS(i).VA_DIST) & _
        vbTab & Trim(ODOS(i).TADD) & _
        vbTab & Trim(ODOS(i).INT_ADD) & _
        vbTab & Trim(ODOS(i).VA_NEAR) & _
        vbTab & Trim(ODOS(i).PSM_1) & _
        vbTab & Trim(ODOS(i).PSM_2) & _
        vbTab & Trim(ODOS(i).VERT) & _
        vbTab & Trim(ODOS(i).TTYPE) & _
        vbTab & Trim(ODOS(i).COMMENT) & _
        ""
    Else
        tmpODOS(i) = "" & _
        vbTab & Trim(ODOS(i).SPHERE) & _
        vbTab & Trim(ODOS(i).CYL) & _
        vbTab & Trim(ODOS(i).Axis) & _
        vbTab & Trim(ODOS(i).VA_DIST) & _
        vbTab & Trim(ODOS(i).TADD) & _
        vbTab & Trim(ODOS(i).VA_NEAR) & _
        vbTab & Trim(ODOS(i).BC) & _
        vbTab & Trim(ODOS(i).DIA) & _
        vbTab & Trim(ODOS(i).PC) & _
        vbTab & Trim(ODOS(i).D_N) & _
        vbTab & Trim(ODOS(i).Model) & _
        vbTab & Trim(ODOS(i).COMMENT) & _
        ""
    End If
Next
OD = tmpODOS(0)
OS = tmpODOS(1)
End Sub

Private Sub getTitleValue(inp As String, mTitle As String, mVal As String, Optional vTitles As String)
Static memTitle

mTitle = ""
mVal = ""
Dim i As Integer
Dim AV As Boolean
'=========================================
'value
Dim b As String
For i = Len(inp) To 1 Step -1
    b = Mid(inp, i, 1)
    Select Case b
    Case "<"
        Exit For
    Case ">"
    Case Else
        mVal = b & mVal
    End Select
Next

If Left(mVal, 1) = "*" Then
    mVal = Mid(mVal, 2)
End If
'=========================================
'title
Dim k
For Each k In Titles.Keys
    If Len(inp) > Len(k) + 1 Then
        If UCase(Mid(inp, 2, Len(k))) = k Then
            mTitle = k
            If vTitles = "" Then
                Exit For
            Else
                If Titles(mTitle) = vTitles Then
                    Exit For
                End If
            End If
        End If
    End If
Next

If mTitle = "" Then
    Select Case memTitle
    Case "SPHERE", "AXIS"
        mTitle = "VAD"
    Case "ADD-READ", "ADD-INTER"
        mTitle = "VAN"
    Case Else
        mTitle = memTitle
    End Select
'correct value------------------------------------------------
    For i = Len(mVal) To 1 Step -1
        b = Mid(mVal, i, 1)
        If b = "-" Then
            mVal = Mid(mVal, 1, i - 1)
            Exit For
        End If
    Next
    If UCase(Mid(mVal, 1, 1)) = "S" Then
        mVal = Mid(mVal, 2)
    End If
    
    Select Case UCase(mVal)
    Case "PLANO", "PLANO2"
        mTitle = memTitle
        mVal = "Plano"
    Case "BALANCELENS", "BALANCE-LENS"
        mTitle = memTitle
        mVal = "Bal"
    Case "ERROR"
        mTitle = ""
        mVal = ""
    Case Else
    End Select

'correct value------------------------------------------------

Else
    memTitle = mTitle
End If
If Not vTitles = "W" Then
    mVal = TransValue(mTitle, mVal)
End If
End Sub

Private Function InsertRow(Index As Integer, Optional ValuesOD As String, Optional ValuesOS As String, Optional ADate As String) As Boolean

Dim mFGC As MSFlexGrid
Dim l As Integer
Dim nextL As Integer
Dim FGCInd As Integer
Dim OLimit As Integer

If Index < 13 Then
    Set mFGC = lstRxs(0 + HistOn)
Else
    Set mFGC = lstRxs(1 + HistOn)
    FGCInd = 1
End If

Dim newLineNum As Integer
newLineNum = Index

Dim curLineNum As Integer
nextL = 1
For l = 0 To mFGC.Rows - 1 Step 2
    curLineNum = Elements(mFGC.TextMatrix(l, 0), FGCInd, "Index")
    Select Case curLineNum
    Case newLineNum
        If Not IsDate(mFGC.TextMatrix(l + 1, 0)) Then
            nextL = nextL + 1
        End If
    Case Is > newLineNum
        Exit For
    End Select
Next
For l = mFGC.Rows - 1 To 0 Step -2
    curLineNum = Elements(mFGC.TextMatrix(l - 1, 0), FGCInd, "Index")
    If Not curLineNum > newLineNum Then
        Exit For
    End If
Next
l = l + 1

If Not IsDate(ADate) Then
    OLimit = 0
    Select Case Index
    Case 0, 12, 16, 17
        OLimit = 6
    Case 1, 3
        OLimit = 3
    Case 2, 4, 7, 8, 9, 10, 11
        OLimit = 1
    Case 13
        OLimit = 4
    Case 14
        OLimit = 12
    End Select
    If nextL > OLimit Then
        GoTo OverLimit
    End If
End If

mFGC.AddItem cmdH(Index).Caption & vbTab & "OD" & ValuesOD, l
If mFGC.Index < 2 Then
    If ADate = "" Then
        mFGC.AddItem cmdH(Index).Caption & vbTab & "OS" & ValuesOS, l + 1
    Else
        mFGC.AddItem ADate & vbTab & "OS" & ValuesOS, l + 1
    End If
Else
    'mFGC.AddItem Format(CDate(cmdHist(5).Text), "mm/dd/yyyy") & vbTab & "OS" & ValuesOS, l + 1
    mFGC.AddItem cmdHist(5).Text & vbTab & "OS" & ValuesOS, l + 1
End If

ColorLine mFGC, l
AdjustFGC
mFGC.Row = l
mFGC.col = 0

bysys = True
lstRxs_Click mFGC.Index
bysys = False

frInfo.Visible = False
frNewEl.Visible = False
lstRxs(0).Enabled = True
lstRxs(1).Enabled = True
mFocus False

InsertRow = True

Exit Function

OverLimit:
    Dim h As String
    Select Case Index
    Case 0
        h = " pairs of glasses per visit"
    Case Else
        h = " " & cmdH(Index).Caption & " allowed per visit"
    End Select
    
    frmEventMsgs.Header = "Only " & OLimit & h
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1

End Function

Private Function TransValue(fTitle As String, inp As String) As String
Select Case fTitle
Case "BASE CURVE"
    Select Case UCase(Trim(inp))
    Case "80"
        TransValue = "8.0"
    Case "81"
        TransValue = "8.1"
    Case "82"
        TransValue = "8.2"
    Case "83"
        TransValue = "8.3"
    Case "84"
        TransValue = "8.4"
    Case "85"
        TransValue = "8.5"
    Case "86"
        TransValue = "8.6"
    Case "87"
        TransValue = "8.7"
    Case "88"
        TransValue = "8.8"
    Case "89"
        TransValue = "8.9"
    Case "90"
        TransValue = "9.0"
    Case Else
        TransValue = Trim(inp)
    End Select
Case Else
    Select Case UCase(Trim(inp))
    Case ";"
        TransValue = ""
    Case "BASEUP"
        TransValue = "BU"
    Case "BASEDOWN"
        TransValue = "BD"
    Case "BASEIN"
        TransValue = "BI"
    Case "BASEOUT"
        TransValue = "BO"
    Case "RG1"
        TransValue = "R>G"
    Case "RG2"
        TransValue = "R=G"
    Case "RG3"
        TransValue = "G>R"
    Case "FC1"
        TransValue = "FC 1'"
    Case "FC2"
        TransValue = "FC 2'"
    Case "FC3"
        TransValue = "FC 3'"
    Case "FC5"
        TransValue = "FC 5'"
    Case "FC13"
        TransValue = "FC 1'"
    Case "FC33"
        TransValue = "FC 3'"
    Case "FC11"
        TransValue = "FC 1'"
    Case "FC31"
        TransValue = "FC 3'"
    Case "HM1"
        TransValue = "HM"
    Case "HM3"
        TransValue = "HM"
    Case "LP1"
        TransValue = "LP"
    Case "LP3"
        TransValue = "LP"
    Case "RG"
        TransValue = "R=G"
    Case "PICT-VC"
        TransValue = "P"
    Case "IllE-VC"
        TransValue = "E"
    Case Else
        TransValue = Trim(inp)
    End Select
End Select
End Function

Private Function DoEyeWearPrint_6(info As String, FirstOn As Boolean, LastOn As Boolean, TotalRxs As Integer) As Boolean
Dim p As Integer
'Dim k As Integer
Dim CLOn As Boolean
'Dim ProceedOn As Boolean
'Dim Temp As String
'Dim ODRx As String, OSRx As String
Dim RNote As String
Dim TheFile As String
Dim TempFile As String
Dim SigFile As String
Dim LensType As String
Dim LensCompany As String
'Dim CLInv As CLInventory
'DoEyeWearPrint_6 = False
'ODRx = ""
'OSRx = ""
'RNote = ""
ThePrinter = GetPrinter(PRINTER_Wear)

If Left(info, 1) = 0 Then
    WearType = "PC"
    CLOn = False
    TempFile = TemplateDirectory + "EyeWearRx.doc"
    TheFile = "EyeWearRx"
Else
    WearType = "CL"
    CLOn = True
    TempFile = TemplateDirectory + "CLRx.doc"
    TheFile = "CLRx"
End If
        
TheFile = DoctorInterfaceDirectory + TheFile + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        
        
If (CreateEyeWear(PatientId, AppointmentId, ActivityId, TheFile, info, LensType, LensCompany, RNote, CLOn, FirstOn, LastOn)) Then
    DoEyeWearPrint_6 = True
    If (LastOn) Then
        Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
' use a special signature file setup for Rxs
        If (Trim(SigFile) <> "") Then
            p = InStrPS(SigFile, ".")
            If (p > 0) Then
                SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
            End If
        End If
        Call PrintTemplate(TheFile, TempFile, True, True, 1, ThePrinter, SigFile, TotalRxs, True)
    End If
End If
End Function


Private Sub ReadFileVSC(FileName As String)
Dim FLine As String
Dim mTitle As String
Dim mVal As String
Dim r As Integer
Dim c As Integer

FM.OpenFile DoctorInterfaceDirectory & FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(1)
Do Until EOF(1)
    Line Input #1, FLine
    
    If Left(FLine, 1) = "*" Then
        Select Case True
        Case UCase(Mid(FLine, 2, 2)) = "OD"
            r = 1
        Case UCase(Mid(FLine, 2, 2)) = "OS"
            r = 2
        Case Else
            getTitleValue FLine, mTitle, mVal, "W"
            GoSub CorrectVal
            Select Case mTitle
            Case "DISTANCE"
                c = 1
            Case "PH-DISTANCE"
                c = 2
            Case "NEAR"
                c = 3
            Case "PH-NEAR"
                c = 4
            Case "MEASUREMENT1"
                c = 7
            Case "AXIS1"
                c = 8
                If IsNumeric(mVal) Then mVal = "x " & mVal
            Case "MEASUREMENT2"
                c = 9
            Case "AXIS2"
                c = 10
                If IsNumeric(mVal) Then mVal = "x " & mVal
            Case "AVE"
                c = 11
            Case Else
                c = 0
            End Select
            If r > 0 And c > 0 Then
                lstVSC.TextMatrix(r, c) = Trim(lstVSC.TextMatrix(r, c) & " " & mVal)
            End If
        End Select
    Else
        r = 0
        c = 0
    End If
    
Loop
FM.CloseFile CLng(1)

Exit Sub

CorrectVal:
Select Case Trim(UCase(mVal))
Case ";"
    mVal = ""
Case "MIRESREG-"
    mVal = "Mires Reg"
Case "MIRESIRREG-"
    mVal = "Mires Irreg"
End Select
Return

End Sub

Public Function FrmClose()
  'frmEventMsgs.Result = 2
 Call cmd_Click(-1)
  Unload Me
End Function

Private Sub PostLogEntires()
Dim TheFile As String

'read T07R SpecRx
TheFile = GetTestFileName("07R")
If (FM.IsFileThere(TheFile)) Then
    Call InsertLog("FrmAssignWearNew.frm", "Ordered Refractive Rx", LoginCatg.OrderSet, "", "", 0, globalExam.iAppointmentId)
End If

'read T07S CLRx
TheFile = GetTestFileName("07S")
If (FM.IsFileThere(TheFile)) Then
    Call InsertLog("FrmAssignWearNew.frm", "Contact Lens Rx Written", LoginCatg.OrderSet, "", "", 0, globalExam.iAppointmentId)
End If
End Sub
