VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientTaskEventHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IO_Practiceware_Interop.IEventHandler

Private m_PatientId As Long
Private m_PatientScreenToOpen As String
Dim pat As New Patient
Dim currentActivityid As Long
Dim answer As Integer
Dim frx As Form, fCount As Integer
Dim EAL As New DI_ExamActivityList

Property Get PatientId() As Long
      PatientId = m_PatientId
End Property
Property Get PatientScreenToOpen() As String
      PatientScreenToOpen = m_PatientScreenToOpen
End Property

Private Sub IEventHandler_OnEvent(ByVal sender As Variant, ByVal e As Variant)
fCount = 0
m_PatientId = e.task.PatientId
m_PatientScreenToOpen = e.ScreenToOpen

If PatientScreenToOpen = "Chart" Then
  For Each frx In Forms()
     If frx.Name = frmSystems1.Name Then
     fCount = 1
    End If
    Next

If fCount > 0 Then
    answer = MsgBox("The PatientChart is already open Do you want to save changes before closing?", vbInformation + vbYesNo, "Add Confirm")
    If answer = vbYes Then
    frmSystems1.FrmClose
     End If
End If

currentActivityid = EAL.CurrentActivity(PatientId)
globalExam.ResetPatient
globalExam.SetExamIds PatientId, 0, currentActivityid
If (frmSystems1.LoadSummaryInfo(True)) Then
        frmSystems1.FormOn = True
        frmSystems1.Show 1, frmHome
End If

Exit Sub
End If

If PatientScreenToOpen = "Financials" Then
    MsgBox "you must be in AdminInterface to click through to Patient Financials"
Exit Sub
End If

If PatientScreenToOpen = "Info" Then
    Dim PatientDemographics As New PatientDemographics
    PatientDemographics.PatientId = PatientId
    Call PatientDemographics.DisplayPatientInfoScreen
    Set PatientDemographics = Nothing
Exit Sub
End If

End Sub


