VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmHome 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Exam"
   ClientHeight    =   9060
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12045
   ForeColor       =   &H00000000&
   Icon            =   "ExamMonitor.frx":0000
   LinkTopic       =   "Form1"
   Picture         =   "ExamMonitor.frx":0442
   ScaleHeight     =   9060
   ScaleWidth      =   12045
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   240
      Top             =   6960
   End
   Begin VB.ListBox lstPatient 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3840
      ItemData        =   "ExamMonitor.frx":3B0B
      Left            =   840
      List            =   "ExamMonitor.frx":3B0D
      TabIndex        =   8
      Top             =   2640
      Visible         =   0   'False
      Width           =   10455
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      ItemData        =   "ExamMonitor.frx":3B0F
      Left            =   10080
      List            =   "ExamMonitor.frx":3B11
      TabIndex        =   15
      Top             =   120
      Width           =   1815
   End
   Begin VB.ListBox lstRoom 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      ItemData        =   "ExamMonitor.frx":3B13
      Left            =   8160
      List            =   "ExamMonitor.frx":3B15
      TabIndex        =   14
      Top             =   120
      Width           =   1815
   End
   Begin VB.Timer Timer1 
      Interval        =   10000
      Left            =   120
      Top             =   960
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLogin 
      Height          =   855
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   2535
      _Version        =   131072
      _ExtentX        =   4471
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":3B17
   End
   Begin VB.ListBox lstMessages 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1320
      ItemData        =   "ExamMonitor.frx":3D01
      Left            =   2760
      List            =   "ExamMonitor.frx":3D03
      TabIndex        =   4
      Top             =   120
      Visible         =   0   'False
      Width           =   3375
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1095
      Left            =   0
      TabIndex        =   0
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":3D05
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   615
      Left            =   3000
      TabIndex        =   1
      Top             =   7080
      Visible         =   0   'False
      Width           =   6135
      _Version        =   131072
      _ExtentX        =   10821
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":3EE1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEdit 
      Height          =   1095
      Left            =   3600
      TabIndex        =   7
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":40BC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   1095
      Left            =   10800
      TabIndex        =   9
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":42A6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPL 
      Height          =   1095
      Left            =   9600
      TabIndex        =   10
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":448B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFU 
      Height          =   1095
      Left            =   8400
      TabIndex        =   13
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":4671
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFL 
      Height          =   1095
      Left            =   6000
      TabIndex        =   16
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":4851
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdWait 
      Height          =   615
      Left            =   6360
      TabIndex        =   17
      Top             =   720
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":4A36
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExam 
      Height          =   615
      Left            =   6360
      TabIndex        =   18
      Top             =   1320
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":4C11
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQues 
      Height          =   615
      Left            =   6360
      TabIndex        =   19
      Top             =   120
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1085
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":4DEC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSurg 
      Height          =   1095
      Left            =   7200
      TabIndex        =   20
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":4FCB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRx 
      Height          =   1095
      Left            =   1200
      TabIndex        =   21
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":51AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   2
      Left            =   3000
      TabIndex        =   24
      Top             =   4200
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":538A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   3
      Left            =   3000
      TabIndex        =   25
      Top             =   5160
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":5569
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   4
      Left            =   3000
      TabIndex        =   26
      Top             =   6120
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":5748
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   5
      Left            =   6120
      TabIndex        =   27
      Top             =   2280
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":5927
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   6
      Left            =   6120
      TabIndex        =   28
      Top             =   3240
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":5B06
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   7
      Left            =   6120
      TabIndex        =   29
      Top             =   4200
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":5CE5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   8
      Left            =   6120
      TabIndex        =   30
      Top             =   5160
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":5EC4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   9
      Left            =   6120
      TabIndex        =   31
      Top             =   6120
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":60A3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   1
      Left            =   3000
      TabIndex        =   23
      Top             =   3240
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":6283
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPatient 
      Height          =   855
      Index           =   0
      Left            =   3000
      TabIndex        =   22
      Top             =   2280
      Visible         =   0   'False
      Width           =   3015
      _Version        =   131072
      _ExtentX        =   5318
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   4473924
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":6462
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtn1 
      Height          =   1095
      Left            =   2400
      TabIndex        =   42
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":6641
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   855
      Left            =   9840
      TabIndex        =   43
      Top             =   6480
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":6823
   End
   Begin fpBtnAtlLibCtl.fpBtn fpBtnBG 
      Height          =   1095
      Left            =   4800
      TabIndex        =   45
      Top             =   7800
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExamMonitor.frx":6A0A
   End
   Begin VB.Label lblMultiPrint 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Printing Appointment Details..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2160
      TabIndex        =   44
      Top             =   1800
      Visible         =   0   'False
      Width           =   6915
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   9
      Left            =   9240
      TabIndex        =   41
      Top             =   6120
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   8
      Left            =   9240
      TabIndex        =   40
      Top             =   5160
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   7
      Left            =   9240
      TabIndex        =   39
      Top             =   4200
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   6
      Left            =   9240
      TabIndex        =   38
      Top             =   3240
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   5
      Left            =   9240
      TabIndex        =   37
      Top             =   2280
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   4
      Left            =   120
      TabIndex        =   36
      Top             =   6120
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   3
      Left            =   120
      TabIndex        =   35
      Top             =   5160
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   2
      Left            =   120
      TabIndex        =   34
      Top             =   4200
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   1
      Left            =   120
      TabIndex        =   33
Top = 3240
Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblResource 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      Caption         =   "Appointment Summary"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   0
      Left            =   120
      TabIndex        =   32
      Top             =   2280
      Visible         =   0   'False
      Width           =   2730
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPrc 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Practice"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   12
      Top             =   1080
      Width           =   780
   End
   Begin VB.Label lblVersion 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Version"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   10080
      TabIndex        =   11
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label lblLoad 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Loading Patient Information"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2160
      TabIndex        =   6
      Top             =   1800
      Visible         =   0   'False
      Width           =   6915
   End
   Begin VB.Label lblDate 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Date:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      Width           =   465
   End
   Begin VB.Label lblTime 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Time:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   2
      Top             =   1800
      Width           =   480
   End
End
Attribute VB_Name = "frmHome"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Option Explicit
Public TheActivityId As Long
Public patientRuns As Long
Public currPatId As Long

Public FollowUpOn As Boolean
Public BackUpOn As Boolean
Public ImageLocationSupport As String

Public ViewLoginFullScreen As Boolean

Private Type QualifierDetails
    QCode As String
    QAltC As String
    QValue As Boolean
    QCat As Boolean
End Type

Private Type ZoneDetails
    TheX As Long
    TheY As Long
    Reference As String * 2
End Type

Private Type LockedTestDetails
    TestText(8) As String
    TestOrder(8) As String
    TestQuestion(8) As String
    TestStyle(8) As Boolean
End Type

Private Type INetSites
    DisplayText As String
    WebSite As String
End Type

Private Type ExternalReference
    Name As String * 64
    Trigger(3) As String * 1
End Type
Private StandardAction(100) As ExternalReference

Private Const MaxQualifiers As Integer = 60
Private Qualifiers(MaxQualifiers) As QualifierDetails

Private Const MaxZones02 As Long = 106000
Private Zone02(MaxZones02) As ZoneDetails
Private Const MaxZones08 As Long = 58000
Private Zone08(MaxZones08) As ZoneDetails

Private Const MaxSites As Long = 50
Private WebSites(MaxSites) As INetSites

Private Const MaxSystems As Integer = 18

Private TotalEntries As Integer
Private TotalExams As Integer
Private TotalExamsforTest As Integer
Private TheStationId As Integer
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private RoomType As String
Private RoomId As Long
Private DoctorId As Long
Public EditPreviousAppointmentOn As Boolean
Public LatestAppointmentOn As Boolean
Private TestDisplayOn As Boolean
Private LockedTests As LockedTestDetails
Private SystemDefaultOn(22) As Boolean
Private SystemDefaultText(22) As String
Private PatientName As String

Private CurrentIndex As Integer
Private CurrentPage As Integer
Public pat As New Patient
Public UpdatePid As Long
Private AutoLogOff As Boolean


Private Type LASTINPUTINFO
cbSize As Long
dwTime As Long
End Type
Private Declare Function GetTickCount Lib "kernel32" () As Long
Private Declare Function GetLastInputInfo Lib "user32" (plii As Any) As Long
Private Declare Sub ExitProcess Lib "kernel32" (ByVal uExitCode As Long)
Private Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long
Private Declare Function GetCurrentProcess Lib "kernel32" () As Long

Private Sub SetDateTime()
Dim dtToday As New CIODateTime
    On Error GoTo lSetDateTime_Error

    dtToday.MyDateTime = Now
    lblDate.Caption = dtToday.GetDisplayDate(True)
    lblTime.Caption = dtToday.GetDisplayTime
    Set dtToday = Nothing

    Exit Sub

lSetDateTime_Error:

    LogError "frmHome", "SetDateTime", Err, Err.Description
End Sub

Private Sub ClearButtons()
Dim i As Integer
    On Error GoTo lClearButtons_Error

    For i = 0 To 9
        With cmdPatient(i)
            .Text = ""
            .Tag = ""
            .Visible = False
            .BackColor = BaseBackColor
            .ForeColor = BaseTextColor
        End With
        With lblResource(i)
            .Caption = ""
            .BackColor = frmHome.BackColor
            .Visible = False
        End With
    Next i

    Exit Sub

lClearButtons_Error:

    LogError "frmHome", "ClearButtons", Err, Err.Description
End Sub

Private Function ActivityInUse(ActivityId As Long, TheRoom As String) As Integer
Dim PatId As Long
Dim RetRes As SchedulerResource
Dim RetrieveActivity As PracticeActivity
ActivityInUse = 0
TheRoom = ""
Set RetrieveActivity = New PracticeActivity
RetrieveActivity.ActivityId = ActivityId
If (RetrieveActivity.RetrieveActivity) Then
    PatId = RetrieveActivity.ActivityPatientId
    If (RetrieveActivity.ActivityStationId <> 0) Then
        ActivityInUse = -1
        If (RetrieveActivity.ActivityStationId = 998) Then
            ActivityInUse = -2
        End If
        If (RetrieveActivity.ActivityResourceId > 0) Then
            Set RetRes = New SchedulerResource
            RetRes.ResourceId = RetrieveActivity.ActivityLocationId
            If (RetRes.RetrieveSchedulerResource) Then
                TheRoom = Trim(RetRes.ResourceDescription)
            End If
            Set RetRes = Nothing
        End If
    End If
    If (ActivityInUse = -1) Then
        If (IsFilesOnServer(RetrieveActivity.ActivityPatientId)) Then
            ActivityInUse = 0
            TheRoom = ""
            RetrieveActivity.ActivityStationId = 0
            Call RetrieveActivity.ApplyActivity
        End If
    End If
End If
Set RetrieveActivity = Nothing
End Function

Private Function AcquirePatient(PatientId As Long, ApptId As Long, ActivityId As Long, ResourceId As Long, ActivityTimeIn As String, Ref As Integer) As Boolean
Dim Proceed As Boolean
Dim ApptType As Long
Dim ApptDoc(10) As Long
Dim DoctorColor As Long
Dim TheAge As Integer
Dim ResId As Long
Dim ApptTime As String
Dim Appointment As String
Dim DisplayTime As String
Dim DisplayText As String
Dim SchedulerDoctor As String
Dim AppointmentType As String
Dim RetrievePatient As Patient
Dim RetrieveResource As SchedulerResource
Dim RetrieveAppointment As SchedulerAppointment
Dim RetrieveAppointmentType As SchedulerAppointmentType
Dim RetrieveDoctor As SchedulerResource
AcquirePatient = True
Set RetrieveAppointmentType = New SchedulerAppointmentType
Set RetrieveAppointment = New SchedulerAppointment
Set RetrieveResource = New SchedulerResource
Set RetrieveDoctor = New SchedulerResource
Set RetrievePatient = New Patient
RetrievePatient.PatientId = PatientId
If (RetrievePatient.RetrievePatient) Then
    TheAge = GetAge(RetrievePatient.BirthDate)
    PatientName = RetrievePatient.FirstName + " " _
                + RetrievePatient.MiddleInitial + " " _
                + RetrievePatient.LastName + " " _
                + RetrievePatient.NameRef + " " _
                + Trim(str(TheAge))
    If (Trim(PatientName) = "-1") Then
        Set RetrieveAppointmentType = Nothing
        Set RetrieveAppointment = Nothing
        Set RetrieveResource = Nothing
        Set RetrieveDoctor = Nothing
        AcquirePatient = False
        Exit Function
    End If
    RetrieveResource.ResourceId = ResourceId
    If Not (RetrieveResource.RetrieveSchedulerResource) Then
        AcquirePatient = False
    End If
    If (ApptId > 0) And (AcquirePatient) Then
        RetrieveAppointment.AppointmentId = ApptId
        If (RetrieveAppointment.RetrieveSchedulerAppointment) Then
            Proceed = True
            If ((DoctorId <> RetrieveAppointment.AppointmentResourceId1) And (DoctorId > 0)) Then
                Proceed = False
            End If
            If (Proceed) Then
                ApptType = RetrieveAppointment.AppointmentTypeId
                ApptTime = RetrieveAppointment.AppointmentTime
                Appointment = RetrieveAppointment.AppointmentDate + vbCrLf + ApptTime
                RetrieveDoctor.ResourceId = RetrieveAppointment.AppointmentResourceId1
                If (RetrieveDoctor.RetrieveSchedulerResource) Then
                    SchedulerDoctor = RetrieveDoctor.ResourceName
                    DoctorColor = RetrieveDoctor.ResourceColor
                Else
                    SchedulerDoctor = ""
                    DoctorColor = 0
                End If
                RetrieveAppointmentType.AppointmentTypeId = ApptType
                If (RetrieveAppointmentType.RetrieveSchedulerAppointmentType) Then
                    AppointmentType = RetrieveAppointmentType.AppointmentType
                Else
                    AppointmentType = ""
                End If
                ApptDoc(1) = RetrieveAppointmentType.AppointmentTypeResourceId1
                ApptDoc(2) = RetrieveAppointmentType.AppointmentTypeResourceId2
                ApptDoc(3) = RetrieveAppointmentType.AppointmentTypeResourceId3
                ApptDoc(4) = RetrieveAppointmentType.AppointmentTypeResourceId4
                ApptDoc(5) = RetrieveAppointmentType.AppointmentTypeResourceId5
                ApptDoc(6) = RetrieveAppointmentType.AppointmentTypeResourceId6
                ApptDoc(7) = RetrieveAppointmentType.AppointmentTypeResourceId7
                If UserLogin.IsLoggedIn Then
                    If UserLogin.sType <> "T" Then 'REFACTOR What's ResourceType T?
                        ResId = UserLogin.iId
                        AcquirePatient = True
                    Else
                        AcquirePatient = True
                    End If
                End If
                Call ConvertMinutesToTime(Val(ApptTime), DisplayTime)
                DisplayText = SchedulerDoctor + vbCrLf + DisplayTime + ", " + Trim(AppointmentType)
            Else
                AcquirePatient = False
            End If
        Else
            AcquirePatient = False
        End If
    Else
        AcquirePatient = False
    End If
    If (AcquirePatient) Then
        If UserLogin.IsLoggedIn Then
            cmdPatient(Ref).Text = PatientName
        Else
            cmdPatient(Ref).Text = "**********"
        End If
        cmdPatient(Ref).Tag = Trim$(str$(ActivityId))
        cmdPatient(Ref).ToolTipText = Trim$(str$(PatientId))
        cmdPatient(Ref).Visible = True
        If cmdPatient(Ref).BackColor = 0 Then
            cmdPatient(Ref).BackColor = 10196518
            cmdPatient(Ref).ForeColor = 16777215
            BaseBackColor = 10196518
            BaseTextColor = 16777215
        End If
        lblResource(Ref).Caption = ActivityTimeIn & " " & Trim$(RetrieveResource.ResourceDescription) & vbCrLf & Trim$(DisplayText)
        lblResource(Ref).BackColor = DoctorColor
        lblResource(Ref).Visible = True
    End If
Else
    AcquirePatient = False
End If
Set RetrievePatient = Nothing
Set RetrieveResource = Nothing
Set RetrieveAppointment = Nothing
Set RetrieveAppointmentType = Nothing
Set RetrieveDoctor = Nothing
End Function

Public Function LoadExamRoom(ARoom As String) As Boolean
Dim k As Integer
Dim p As Integer
Dim TheDate As String
Dim TheTime As String
Dim Proceed As Boolean
Dim RetrieveActivity As New PracticeActivity
    On Error GoTo lLoadExamRoom_Error

    TotalEntries = 0
    TotalExamsforTest = 0
    TheActivityId = 0
    RoomType = ARoom
    Timer1.Enabled = False
    Call ClearButtons
    SetDateTime
    'Call LoadMessageBox(lstMessages, UserLogin.iId)
    If CurrentIndex = 0 Then
        CurrentIndex = 1
    End If
    RetrieveActivity.ActivityAppointmentId = 0
    RetrieveActivity.ActivityStationId = 0
    RetrieveActivity.ActivityResourceId = 0
    RetrieveActivity.ActivityLocationId = dbPracticeId
    RetrieveActivity.ActivityStatus = ARoom
    RetrieveActivity.ActivityDate = Chr(1)
    TotalExams = RetrieveActivity.FindActivity
    If (TotalExams > 0) Then
        If (CurrentIndex > TotalExams) Or (CurrentIndex < 1) Then
            CurrentIndex = 1
        End If
        k = CurrentIndex
        p = 0
        While (RetrieveActivity.SelectActivity(k)) And (p < 10)
            Proceed = True
            If (RetrieveActivity.ActivityStatusTime = "") Then
                Proceed = False
            End If
            If ((RoomId <> RetrieveActivity.ActivityResourceId) And (RoomId > 0)) Then
                Proceed = False
            End If
            If (Proceed) Then
                If (AcquirePatient(RetrieveActivity.ActivityPatientId, RetrieveActivity.ActivityAppointmentId, RetrieveActivity.ActivityId, RetrieveActivity.ActivityResourceId, RetrieveActivity.ActivityStatusTime, p)) Then
                    p = p + 1
                End If
            End If
            k = k + 1
        Wend
        CurrentIndex = k
        PatientName = ""
    End If
    TotalEntries = TotalExams
    If (TotalEntries < 11) Then
        cmdMore.Visible = False
    End If
    If (TotalEntries > 10) And (CurrentIndex - 1 < TotalEntries) Then
        cmdMore.Text = "More"
        cmdMore.Visible = True
    ElseIf (TotalEntries > 10) And (CurrentIndex >= TotalEntries) Then
        cmdMore.Text = "List Start"
        cmdMore.Visible = True
    End If
    If (TotalEntries < 11) Then
        cmdMore.Visible = False
    End If
    If (CurrentIndex > 0) Then
        LoadExamRoom = True
    End If
    Set RetrieveActivity = Nothing
    Timer1.Enabled = True

    Exit Function

lLoadExamRoom_Error:
    Set RetrieveActivity = Nothing
    Timer1.Enabled = True
    LogError "frmHome", "LoadExamRoom", Err, Err.Description
End Function
Private Sub cmdEdit_Click()
Dim PatId As Long
Dim ReturnArguments As Object

If Not UserLogin.IsLoggedIn Then
    If Not (Login) Then
        Exit Sub
    End If
End If
   If Not UserLogin.HasPermission(epEditPreviousVisit) Then
    frmEventMsgs.InfoMessage "Not Permissioned"
    frmEventMsgs.Show 1
    Exit Sub
End If

PatId = 0
Timer1.Enabled = False
Set ReturnArguments = DisplayPatientSearchScreen(False)
Timer1.Enabled = True
If Not (ReturnArguments Is Nothing) Then
    PatId = ReturnArguments.PatientId
    If PatId > 0 Then
        EditPreviousAppointmentOn = False
        patientRuns = pat.GetPatientAccess(PatId)
        UpdatePid = PatId
        If patientRuns = -1 Then
            frmEventMsgs.InfoMessage "Access Blocked"
            frmEventMsgs.Show 1
            Exit Sub
        End If
        Call LoadPatientActivity(PatId, lstPatient, False)
    End If
End If
End Sub

Private Sub cmdExam_Click()
lstRoom.ListIndex = 0
End Sub

Private Sub cmdPatient_Click(Index As Integer)
    Dim memSelectedPatient As Long
    memSelectedPatient = cmdPatient(Index).ToolTipText
    
    SelectPatient Index
    If RoomType = "E" Then
        If cmdPatient(Index).BackColor <> BaseBackColor Then
            PatientName = Trim$(cmdPatient(Index).Text)
            currPatId = memSelectedPatient
            If FirstAction(PatientName) Then
                patientRuns = pat.GetPatientAccess(memSelectedPatient)
                If patientRuns = -1 Then
                 frmEventMsgs.Header = " Access Blocked"
                 frmEventMsgs.AcceptText = ""
                 frmEventMsgs.RejectText = "Ok"
                 frmEventMsgs.CancelText = ""
                 frmEventMsgs.Other0Text = ""
                 frmEventMsgs.Other1Text = ""
                 frmEventMsgs.Other2Text = ""
                 frmEventMsgs.Other3Text = ""
                 frmEventMsgs.Other4Text = ""
                 frmEventMsgs.Show 1
                 Call TurnOn
                 cmdPatient(Index).BackColor = BaseBackColor
                cmdPatient(Index).ForeColor = BaseTextColor
                Exit Sub
                Else
                    CheckTest Val(memSelectedPatient), lblResource(Index).Caption
                    SelectSystems Val(memSelectedPatient)
                End If
            Else
                cmdPatient(Index).BackColor = BaseBackColor
                cmdPatient(Index).ForeColor = BaseTextColor
                TurnOn
                PatientName = ""
            End If
        Else
            PatientName = ""
        End If
    Else
        PatientName = Trim$(cmdPatient(Index).Text)
        If FirstAction(PatientName) Then
            If RoomType = "E" Then
                CheckTest Val(memSelectedPatient), lblResource(Index).Caption
                SelectSystems Val(memSelectedPatient)
                lstRoom.ListIndex = 0
            End If
            cmdPatient(Index).BackColor = BaseBackColor
            cmdPatient(Index).ForeColor = BaseTextColor
            TurnOn
        Else
            cmdPatient(Index).BackColor = BaseBackColor
            cmdPatient(Index).ForeColor = BaseTextColor
            TurnOn
            PatientName = ""
        End If
        PatientName = ""
    End If
End Sub

Private Sub cmdPrint_Click()
Dim i As Integer
Dim ActId As Long

On Error GoTo cmdPrint_Error

frmHome.Enabled = False
lblMultiPrint.Caption = "Printing Appointment Details..."
lblMultiPrint.Visible = True
Timer1.Enabled = False

For i = 1 To lstPatient.ListCount - 1
    lblLoad.Visible = False
    ActId = Val(Trim(Mid(lstPatient.List(i), 76, Len(lstPatient.List(i)) - 75)))
    Dim Activity As New PracticeActivity
    Activity.ActivityId = ActId
    If Activity.RetrieveActivity Then
        frmSystems1.TheStationId = Activity.ActivityStationId
        frmSystems1.PrintAll = True
        globalExam.ResetPatient
        globalExam.SetExamIds Activity.ActivityPatientId, 0, Activity.ActivityId
        frmSystems1.EditPreviousOn = True
        frmSystems1.LatestAppointmentOn = True
        If frmSystems1.LoadSummaryInfo(True) Then
            Call frmSystems1.PrintDIAppointment(Activity.ActivityAppointmentId, False, True, True)
        End If
    End If
    Set Activity = Nothing
Next

frmHome.Enabled = True
lblMultiPrint.Visible = False
Timer1.Enabled = True
Exit Sub

cmdPrint_Error:
    lblMultiPrint.Visible = False
    frmHome.Enabled = True
    Timer1.Enabled = True
End Sub

Private Sub cmdRx_Click()
If Not UserLogin.IsLoggedIn Then
    If Not (Login) Then
        Exit Sub
    End If
End If
'Dim oEPLibHelper As New EPLib.Helper
'Dim oRenewalForm As New EPLib.frmRenewals
'    oEPLibHelper.Destination = "renewal"
'    oRenewalForm.Show
'    Set oEPLibHelper = Nothing
'    Set oRenewalForm = Nothing
    
Dim oeRx As New CeRx
oeRx.strUserId = UserLogin.iId
oeRx.nLocID = dbPracticeId
oeRx.StatusSummary

End Sub
'
'=======
'Private Sub cmdPatient_Click(Index As Integer)
'    SelectPatient Index
'    If RoomType = "E" Then
'        If cmdPatient(Index).BackColor <> BaseBackColor Then
'            PatientName = Trim$(cmdPatient(Index).Text)
'            If FirstAction(PatientName) Then
'                patientRuns = Pat.GetPatientAccess(cmdPatient(Index).ToolTipText)
'                If patientRuns = -1 Then
'                 frmEventMsgs.Header = " Access Blocked"
'                 frmEventMsgs.AcceptText = ""
'                 frmEventMsgs.RejectText = "Ok"
'                 frmEventMsgs.CancelText = ""
'                 frmEventMsgs.Other0Text = ""
'                 frmEventMsgs.Other1Text = ""
'                 frmEventMsgs.Other2Text = ""
'                 frmEventMsgs.Other3Text = ""
'                 frmEventMsgs.Other4Text = ""
'                 frmEventMsgs.Show 1
'                 Call TurnOn
'                 cmdPatient(Index).BackColor = BaseBackColor
'                cmdPatient(Index).ForeColor = BaseTextColor
'                Exit Sub
'                Else
'                    CheckTest Val(memSelectedPatient), lblResource(Index).Caption
'                    SelectSystems Val(memSelectedPatient)
'                End If
'            Else
'                cmdPatient(Index).BackColor = BaseBackColor
'                cmdPatient(Index).ForeColor = BaseTextColor
'                TurnOn
'                PatientName = ""
'            End If
'        Else
'            PatientName = ""
'        End If
'    Else
'        PatientName = Trim$(cmdPatient(Index).Text)
'        If FirstAction(PatientName) Then
'            If RoomType = "E" Then
'                CheckTest Val(memSelectedPatient), lblResource(Index).Caption
'                SelectSystems Val(memSelectedPatient)
'                lstRoom.ListIndex = 0
'            End If
'            cmdPatient(Index).BackColor = BaseBackColor
'            cmdPatient(Index).ForeColor = BaseTextColor
'            TurnOn
'        Else
'            cmdPatient(Index).BackColor = BaseBackColor
'            cmdPatient(Index).ForeColor = BaseTextColor
'            TurnOn
'            PatientName = ""
'        End If
'        PatientName = ""
'    End If
'End Sub
'
'Private Sub cmdRx_Click()
'If Not UserLogin.IsLoggedIn Then
'    If Not (Login) Then
'        Exit Sub
'    End If
'End If
''Dim oEPLibHelper As New EPLib.Helper
''Dim oRenewalForm As New EPLib.frmRenewals
''    oEPLibHelper.Destination = "renewal"
''    oRenewalForm.Show
''    Set oEPLibHelper = Nothing
''    Set oRenewalForm = Nothing
'
'Dim oeRx As New CeRx
'oeRx.strUserId = UserLogin.iId
'oeRx.nLocID = dbPracticeId
'oeRx.StatusSummary
'
'End Sub
'
'
Private Sub cmdSurg_Click()
If Not UserLogin.IsLoggedIn Then
    If Not (Login) Then
        Exit Sub
    End If
End If
If Not UserLogin.HasPermission(epFollowUpAndSurgery) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
If (frmDIListFacAdm.LoadFacilityAdminList(True, False, False)) Then
    frmDIListFacAdm.FormOn = True
    frmDIListFacAdm.Show 1
End If
End Sub

Private Sub cmdWait_Click()
        ' REFACTOR: if no list, then rte
lstRoom.ListIndex = 2
End Sub

Private Sub cmdQues_Click()
lstRoom.ListIndex = 1
End Sub

Private Sub cmdFL_Click()
Dim i As Integer
lstPatient.Clear
lstPatient.AddItem "Close Favorite Links"
For i = 1 To MaxSites
    If (Trim(WebSites(i).DisplayText) <> "") Then
        lstPatient.AddItem WebSites(i).DisplayText
    End If
Next i
If (lstPatient.ListCount > 1) Then
    lstPatient.Visible = True
End If
End Sub

Private Sub cmdFU_Click()
If Not UserLogin.IsLoggedIn Then
    If Not (Login) Then
        Exit Sub
    End If
End If
If Not UserLogin.HasPermission(epFollowUpAndSurgery) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
If (frmFollowUp.LoadClinicalList(True)) Then
    'frmFollowUp.QuitOn - refactor missing line?
    frmFollowUp.GetPatid = globalExam.iPatientId
    frmFollowUp.Show 1
End If
End Sub


Private Sub cmdHome_Click()
    Call InsertLog("FrmHome.frm", "Closed Doctor Interface", LoginCatg.AppExit, "", "", 0, 0)
    
    ' Ensure no lock for exam session
    Call KillLockFile(globalExam.iPatientId, globalExam.iActivityId)
    
    If AutoLogOff Then Call CloseAllForms
        
    'Ensure all active forms are closed
    Call CloseAllActiveForms
    
    ' Log out
    If UserLogin.IsLoggedIn Then
       UserLogin.LogoffUser
    End If
    TheActivityId = 0
    
    ' Cleanup & close active forms
    Call dbPinpointClose
     
    'Stop the Timers
    Timer1.Enabled = False
    Timer2.Enabled = False
    Unload Me
    
    If AutoLogOff Then
        'Starting New Instance
        Call StartNewInstanceOfThisApp
        'Kill the CurrentProcess,Statement "END" is throwing an exception when an Event Call is made from .Net to Vb6
        Call KillCurrentProcess
    Else: End
    End If
End Sub

Private Sub cmdLogin_Click()
    If Not UserLogin.IsLoggedIn Then
        Login
        CurrentIndex = 1
        Timer1_Timer
    Else
        If Not AutoLogOff Then frmHome.Hide
        Call Login
    End If
End Sub

Private Sub cmdMore_Click()
Call LoadExamRoom(RoomType)
End Sub

Public Function CheckTest(PatId As Long, TheText As String) As Boolean
If (PatId > 0) Then
    If (InStrPS(TheText, "TEST EXAM ROOM") <> 0) Then
        frmEventMsgs.SetButtons "Clear Current Exam Content", "", "Ok", "Cancel"
        frmEventMsgs.Show 1
        If frmEventMsgs.Result = 2 Then
            CleanUp DoctorInterfaceDirectory, "*_" + Trim(str(PatId)) + ".txt", BackUpOn
        End If
    End If
End If
End Function

Private Sub cmdNotes_Click()
If Not UserLogin.IsLoggedIn Then
    If Not (Login) Then
        Exit Sub
    End If
End If
If UserLogin.IsLoggedIn Then
    If UserLogin.HasPermission(epPracticeNotes) Then
        frmNotes.NoteId = 0
        frmNotes.MaintainOn = True
        frmNotes.SystemReference = ""
        frmNotes.PatientId = -1
        frmNotes.AppointmentId = -1
        frmNotes.CurrentAction = ""
        frmNotes.SetTo = "T"
        If (frmNotes.LoadNotes) Then
            frmNotes.Show 1
        End If
    Else
        frmEventMsgs.InfoMessage "Not Permissioned"
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub TurnOn()
Dim i As Integer
    For i = 0 To 9
        cmdPatient(i).Enabled = True
    Next i
End Sub

Private Sub TurnOff(Ref As Integer)
Dim i As Integer
    For i = 0 To 9
        If Ref <> i Then
            cmdPatient(i).Enabled = False
        End If
    Next i
End Sub

Private Sub SelectPatient(Ref As Integer)
Dim ActInUse As Integer
Dim PatId As Long
Dim ARoom As String
Dim AHostName As String

If (cmdPatient(Ref).BackColor = BaseBackColor) Then
    If Not UserLogin.IsLoggedIn Then
        If Not (Login) Then
            Exit Sub
        End If
    End If
    TheActivityId = Val(Trim(cmdPatient(Ref).Tag))
    If (RoomType <> "E") Then
        cmdPatient(Ref).BackColor = ButtonSelectionColor
        cmdPatient(Ref).ForeColor = TextSelectionColor
        Call TurnOff(Ref)
        Exit Sub
    End If
    ActInUse = ActivityInUse(TheActivityId, ARoom)
    If (ActInUse <> 0) Then
        AHostName = Trim(GetLockFileHostName(TheActivityId))
        If (Trim(ARoom) <> "") Then
            If (Trim(AHostName) <> "") Then
                frmEventMsgs.InfoMessage "Patient currently in use in " & ARoom & " on " & AHostName & ".  Status " & Trim$(str$(ActInUse))
            Else
                frmEventMsgs.InfoMessage "Patient currently in use in " & ARoom & ".  Status " & Trim$(str$(ActInUse))
            End If
        Else
            If (Trim(AHostName) <> "") Then
                frmEventMsgs.InfoMessage "Patient currently in use on " & AHostName & ".  Status " & Trim$(str$(ActInUse))
            Else
                frmEventMsgs.InfoMessage "Patient currently in use.  Status " & Trim$(str$(ActInUse))
            End If
        End If
        frmEventMsgs.Show 1
' now ask about resetting
        frmEventMsgs.SetButtons "Do you want to reset patient ?", "", "Reset", "Cancel"
        frmEventMsgs.Show 1
        If frmEventMsgs.Result = 2 Then
            UnlockChart TheActivityId
            cmdPatient(Ref).BackColor = ButtonSelectionColor
            cmdPatient(Ref).ForeColor = TextSelectionColor
            TurnOff Ref
        Else
            TheActivityId = 0
        End If
    Else
        cmdPatient(Ref).BackColor = ButtonSelectionColor
        cmdPatient(Ref).ForeColor = TextSelectionColor
        TurnOff Ref
    End If
    PatId = Val(Trim(cmdPatient(Ref).ToolTipText))
Else
    cmdPatient(Ref).BackColor = BaseBackColor
    cmdPatient(Ref).ForeColor = BaseTextColor
    Call TurnOn
    TheActivityId = 0
End If
End Sub

Public Sub SelectSystems(PatId As Long)
Dim RetAct As PracticeActivity
If UserLogin.IsLoggedIn Then
    If (Not EditPreviousAppointmentOn) Then
        If (PatId < 1) Then
            frmEventMsgs.InfoMessage "No Patient Id"
            frmEventMsgs.Show 1
            Call TurnOn
            TheActivityId = 0
            CurrentIndex = 1
            Call LoadExamRoom(RoomType)
            lblLoad.Visible = False
            frmHome.Enabled = True
            Exit Sub
        End If
        DoEvents
        If IsChartLocked(TheActivityId) Then
            frmEventMsgs.InfoMessage "Patient is Locked. Please wait a moment before retrying. " & GetLockFileName(PatId, TheActivityId) & " is present."
            frmEventMsgs.Show 1
' now ask about resetting
            frmEventMsgs.SetButtons "Do you want to unlock the chart?", "", "Unlock", "Cancel"
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 2) Then
                UnlockChart TheActivityId
            Else
                Call TurnOn
                TheActivityId = 0
                CurrentIndex = 1
                Call LoadExamRoom(RoomType)
                lblLoad.Visible = False
                frmHome.Enabled = True
                Exit Sub
            End If
        Else
            If Not CreateLockFile(PatId, TheActivityId) Then
                'Todo: Insert Message box here to restart software.
                Exit Sub
            'Else
                
            End If
        End If
    End If
    Timer1.Enabled = False
    frmSystems1.TheStationId = TheStationId
    globalExam.ResetPatient
    globalExam.SetExamIds PatId, 0, TheActivityId
    frmSystems1.EditPreviousOn = EditPreviousAppointmentOn
    If (EditPreviousAppointmentOn) Then
        frmSystems1.LatestAppointmentOn = LatestAppointmentOn
    Else
        frmSystems1.LatestAppointmentOn = True
    End If
    lblLoad = "Load Clinical Records"
    lblLoad.Visible = True
    DoEvents
    frmHome.Enabled = False
    frmSystems1.PrintAll = False
    If (frmSystems1.LoadSummaryInfo(True)) Then
        frmSystems1.FormOn = True
        frmSystems1.cmdPrint = False
        frmSystems1.Show 1
        If Not (EditPreviousAppointmentOn) Then
            If (PatId < 1) Then
                GoSub FailOver
            End If
            KillLockFile PatId, TheActivityId
        End If
        If Not UserLogin.IsLoggedIn Then
            cmdLogin.Text = "Press Here To Login"
        Else
            cmdLogin.Text = UserLogin.sNameLong
        End If
        If CheckConfigCollection("CHECKOUTLOG") = "T" Then
            UserLogin.LogoffUser
            cmdLogin.Text = "Press Here To Login"
            Call Login
        End If
        CurrentPage = 1
        KillLockFile PatId, TheActivityId
    Else
        KillLockFile PatId, TheActivityId
    End If
    Call TurnOn
    TheActivityId = 0
    CurrentIndex = 1
    Call LoadExamRoom(RoomType)
    lblLoad.Visible = False
End If
frmHome.Enabled = True
Timer1.Enabled = True
lstMessages.Visible = True
If patientRuns = 1 Then
Call pat.GetPatientUpdate(PatId, pat.runscmplted)
End If
Exit Sub
FailOver:
    If (TheActivityId > 0) Then
        Set RetAct = New PracticeActivity
        RetAct.ActivityId = TheActivityId
        If (RetAct.RetrieveActivity) Then
            PatId = RetAct.ActivityPatientId
        End If
        Set RetAct = Nothing
    End If
    Return
End Sub

Private Sub cmdPL_Click()
'Patient Locator
Dim PatId As Long
Dim dtDate As New CIODateTime
    If Not UserLogin.IsLoggedIn Then
        If Not Login Then
            Exit Sub
        End If
    End If
    If Not UserLogin.HasPermission(epPatientLocator) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
    dtDate.MyDateTime = Now
    frmPatientLocator.AppointmentDate = dtDate.GetDisplayDate(False)
    If frmPatientLocator.AppointmentLoadList Then
        frmPatientLocator.Show 1
        If frmPatientLocator.PatientId > 0 Then
            PatId = frmPatientLocator.PatientId
                 patientRuns = pat.GetPatientAccess(PatId)
                If patientRuns = -1 Then
                 frmEventMsgs.Header = " Access Blocked"
                 frmEventMsgs.AcceptText = ""
                 frmEventMsgs.RejectText = "Ok"
                 frmEventMsgs.CancelText = ""
                 frmEventMsgs.Other0Text = ""
                 frmEventMsgs.Other1Text = ""
                 frmEventMsgs.Other2Text = ""
                 frmEventMsgs.Other3Text = ""
                 frmEventMsgs.Other4Text = ""
                 frmEventMsgs.Show 1
                 Call TurnOn
                 Exit Sub
                End If
            If Trim$(frmPatientLocator.AppointmentDate) <> "" Then
                dtDate.SetDateFromDisplay frmPatientLocator.AppointmentDate
            End If
            frmPatientLocator.AppointmentDate = ""
            If Not CheckActiveAppointments(PatId, dtDate) Then
                If LoadActivityList(PatId, dtDate, lstPatient) Then
                    If lstPatient.ListCount > 1 Then
                        LatestAppointmentOn = True
                        LoadActivityEdit Val(Trim(Mid(lstPatient.List(1), 76, Len(lstPatient.List(1)) - 75)))
                    End If
                End If
            End If
        End If
    End If
    Set dtDate = Nothing
End Sub
Private Sub form_load()
' Start showing the splash screen
frmSplash.Whom = "D"
frmSplash.Show
DoEvents

Timer2.Enabled = True
#Const SHORTCUT = False
#Const SCInd = 1

Dim i As Integer
Dim TheRef As String
Dim Practice As PracticeName
    On Error GoTo lForm_Load_Error

AutoLogOff = False
If IsDIRunning Then
    End
End If
Call EstablishDirectory

Set UserLogin = New CUserLogin
CurrentPage = 1
RoomType = "E"
RoomId = 0
DoctorId = 0
CurrentIndex = 1
EditPreviousAppointmentOn = False
LatestAppointmentOn = False
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdPatient(0).BackColor
BaseTextColor = cmdPatient(0).ForeColor
TheStationId = 999
FollowUpOn = False

If Not (UserLogin.IsLoggedIn) Then
    If (Login) Then
        Call InitializeComWrapper
        LoadConfigCollection
        UserLogin.SetTaskMonitor
        SetGlobalDefaults
        BackUpOn = CheckConfigCollection("BACKUPON") = "T"
        For i = 1 To 7
            TheRef = "TESTFILE" + Trim(str(i))
            LockedTests.TestOrder(i) = CheckConfigCollection(TheRef)
            TheRef = "TESTSHORT" + Trim(str(i))
            LockedTests.TestText(i) = CheckConfigCollection(TheRef)
            TheRef = "TESTLONG" + Trim(str(i))
            LockedTests.TestQuestion(i) = CheckConfigCollection(TheRef)
            LockedTests.TestStyle(i) = GetTestStyle(CheckConfigCollection(TheRef))
        Next i
        Call InsertLog("frmHome.frm", "Opened Doctor Interface", LoginCatg.AppOpen, "", "", 0, 0)
        Call LoadSystemNormals("")
        TestDisplayOn = True
        lblVersion.Caption = "Version - " + "9.1"
        Call LoadExamRoom(RoomType)
        Call LoadZones
        Call LoadWebSites
        Call LoadActions
        Call LoadQualifiers
        Call ApplLoadStaff
        Call ApplLoadRooms
        Unload frmSplash
        DoEvents
        ViewLoginFullScreen = True
    End If
End If

#If SHORTCUT Then
    Unload Me
    Select Case SCInd
    Case 1
        frmAssignWearNew.AppointmentId = 38400
        frmAssignWearNew.PatientId = 1023
        Call frmAssignWearNew.LoadWear
        frmAssignWearNew.Show 1
    Case 2
        frmDrugNotes.PatientId = 6211
        frmDrugNotes.AppointmentId = 38416
        frmDrugNotes.NotesSystem = "Alodox Convienience Kit"
        frmDrugNotes.LoadDrugNotes
        frmDrugNotes.Show 1
    End Select
#End If

    Exit Sub

lForm_Load_Error:
    LogError "frmHome", "Form_Load", Err, Err.Description
    MsgBox Error(Err)

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim comWrapper As New comWrapper
    comWrapper.Uninitialize
End Sub

Private Sub fpBtn1_Click()
If Not UserLogin.IsLoggedIn Then
    If Not (Login) Then
        Exit Sub
    End If
End If

Dim oeRx As New CeRx
oeRx.strProviderID = UserLogin.iId
oeRx.nLocID = dbPracticeId
oeRx.RenewalRequests

End Sub

Private Sub fpBtnBG_Click()
Dim objPA As New PracticeActivity
Dim UserId As String
Dim access As Boolean
   UserId = UserLogin.iId
   access = objPA.SetPermission(UserId)
    If (access) Then
        MsgBox "Emergency Access activated. Please note all the actions will be recorded in the AuditLog!", 64, "Emergency Access Alert"
            UserLogin.AuthenticateUser (UserId)
    Else
        MsgBox "You don't have Emergency Access. Please contact your administrator!", 48, "Emergency Access Alert"
 End If
 
End Sub

'Private Function LoadMessageBox(lstBox As ListBox, ReceiverId As Long) As Boolean
'Dim i As Integer
'Dim k1, k2, DisplayTextPos As Integer
'Dim TheTime As String
'Dim DisplayText, DisplayText1 As String
'Dim RetrieveMessages As PracticeMessages
'Dim oDate As New CIODateTime
'lstBox.Clear
'oDate.MyDateTime = Now
'Set RetrieveMessages = New PracticeMessages
'RetrieveMessages.MessagesStatus = "R"
'RetrieveMessages.MessagesDate = oDate.GetIODate
'If (ReceiverId > 0) Then
'    RetrieveMessages.MessagesReceiverId = ReceiverId
'End If
'If (RetrieveMessages.FindMessages > 0) Then
'    i = 1
'    While (RetrieveMessages.SelectMessages(i))
'        Call ConvertMinutesToTime(RetrieveMessages.MessagesTime, TheTime)
'        DisplayText = Trim(TheTime + " <" + Trim(Str(RetrieveMessages.MessagesId)) + "> " + RetrieveMessages.MessagesText)
'' word wrap on listbox entries
'        DisplayTextPos = 0
'        While (Len(DisplayText) > 30)
'            DisplayText1 = Left(DisplayText, 30)
'            For k1 = Len(DisplayText1) To 1 Step -1
'                If (Mid(DisplayText1, k1, 1) = " ") Then
'                    k2 = k1
'                    k1 = 1
'                End If
'            Next k1
'            DisplayText1 = Left(DisplayText1, k2)
'            lstBox.AddItem DisplayText1
'            DisplayTextPos = k2 + 1
'            DisplayText = Mid(DisplayText, DisplayTextPos, Len(DisplayText) - (DisplayTextPos - 1))
'        Wend
'        If (Trim(DisplayText) <> "") Then
'            lstBox.AddItem DisplayText
'        End If
'        i = i + 1
'    Wend
'End If
'Set RetrieveMessages = Nothing
'Exit Function
'End Function

Private Sub lblPrc_Click()
Dim i As Integer
Dim Temp As String
Dim Practice As PracticeName
Dim RetRes As SchedulerResource
If CheckConfigCollection("MULTIOFFICE") = "T" Then
    lstPatient.Visible = False
    lstPatient.Clear
    lstPatient.AddItem "Close Locations"
    lstPatient.AddItem "All   Locations"
    Set Practice = New PracticeName
    Practice.PracticeType = "P"
    Practice.PracticeName = Chr(1)
    Practice.PracticeLocationReference = ""
    If (Practice.FindPractice > 0) Then
        i = 1
        While (Practice.SelectPractice(i))
            Temp = Space(75)
            Mid(Temp, 1, 10) = Trim(Practice.PracticeLocationReference)
            If (Trim(Temp) = "") Then
                Mid(Temp, 1, 6) = "Office"
                Temp = Temp + Trim(str(0))
            Else
                Temp = Temp + Trim(str(Practice.PracticeId + 1000))
            End If
            lstPatient.AddItem Temp
            i = i + 1
        Wend
    End If
    Set Practice = Nothing
    Set RetRes = New SchedulerResource
    RetRes.ResourceType = "R"
    RetRes.ResourceName = Chr(1)
    If (RetRes.FindResourcebyRoom > 0) Then
        i = 1
        While (RetRes.SelectResource(i))
            If (RetRes.ResourceServiceCode = "02") Then
                Temp = Space(75)
                Mid(Temp, 1, 10) = Trim(RetRes.ResourceName)
                Temp = Temp + Trim(str(RetRes.ResourceId))
                lstPatient.AddItem Temp
            End If
            i = i + 1
        Wend
    End If
    Set RetRes = Nothing
    If (lstPatient.ListCount - 1 > 1) Then
        lstPatient.Visible = True
    End If
End If
End Sub

Private Sub lstMessages_DblClick()
Dim g, q As Integer
Dim msgID As Long
Dim RetrieveMessages As PracticeMessages
If (lstMessages.ListIndex < 0) Or (UserLogin.iId < 1) Then
    Exit Sub
End If
q = InStrPS(lstMessages.List(lstMessages.ListIndex), "<")
g = InStrPS(lstMessages.List(lstMessages.ListIndex), ">")
If (q <> 0) And (g <> 0) Then
    msgID = Val(Mid(lstMessages.List(lstMessages.ListIndex), q + 1, (g - q) - 1))
    Set RetrieveMessages = New PracticeMessages
    RetrieveMessages.MessagesId = msgID
    If (RetrieveMessages.RetrieveMessages) Then
        RetrieveMessages.MessagesStatus = "X"
'        If (RetrieveMessages.ApplyMessages) Then
'            Call LoadMessageBox(lstMessages, UserLogin.iId)
'        End If
    End If
End If
Set RetrieveMessages = Nothing
End Sub

Private Sub lstPatient_Click()
Dim ActId As Long
Dim PatId As Long
cmdPrint.Visible = False
If (lstPatient.ListIndex = 0) Then
    EditPreviousAppointmentOn = False
    lstPatient.Visible = False
    lstPatient.Clear
    cmdPrint.Visible = False
ElseIf (lstPatient.List(0) = "Close Favorite Links") Then
    Call TriggerINet(Trim(WebSites(lstPatient.ListIndex).WebSite))
    lstPatient.Visible = False
    lstPatient.Clear
ElseIf (lstPatient.List(0) = "Close Patient Selection") Then
    EditPreviousAppointmentOn = False
    PatId = Val(Trim(Mid(lstPatient.List(lstPatient.ListIndex), 76, Len(lstPatient.List(lstPatient.ListIndex)) - 75)))
    patientRuns = pat.GetPatientAccess(PatId)
    UpdatePid = PatId
    If patientRuns = -1 Then
        frmEventMsgs.Header = " Access Blocked"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        'Call TurnOn
        Exit Sub
    End If
    Call LoadPatientActivity(PatId, lstPatient, False)
ElseIf (lstPatient.List(0) = "Close Activity Selection") Then
    ActId = Val(Trim(Mid(lstPatient.List(lstPatient.ListIndex), 76, Len(lstPatient.List(lstPatient.ListIndex)) - 75)))
    LatestAppointmentOn = False
    If (lstPatient.ListIndex = 1) Then
        LatestAppointmentOn = True
    End If
    Call LoadActivityEdit(ActId)
ElseIf (lstPatient.List(0) = "Close Locations") Then
    If (lstPatient.ListIndex = 1) Then
        dbPracticeId = -1
        dbSoftwareLocation = Trim(Left(lstPatient.List(2), 10))
        lblPrc.Caption = dbSoftwareLocation
    Else
        dbPracticeId = Val(Mid(lstPatient.List(lstPatient.ListIndex), 76, Len(lstPatient.List(lstPatient.ListIndex)) - 75))
        dbSoftwareLocation = Trim(Left(lstPatient.List(lstPatient.ListIndex), 10))
        lblPrc.Caption = dbSoftwareLocation
    End If
    lstPatient.Visible = False
    CurrentIndex = 1
    Call LoadExamRoom(RoomType)
End If
End Sub

Private Sub lstDr_Click()
If (lstDr.ListIndex >= 0) Then
    DoctorId = Val(Trim(Mid(lstDr.List(lstDr.ListIndex), 76, Len(lstDr.List(lstDr.ListIndex)) - 75)))
    CurrentIndex = 1
    Call LoadExamRoom(RoomType)
End If
End Sub

Private Sub lstRoom_Click()
If (lstRoom.ListIndex >= 0) Then
    RoomId = Val(Trim(Mid(lstRoom.List(lstRoom.ListIndex), 56, Len(lstRoom.List(lstRoom.ListIndex)) - 55)))
    If (lstRoom.ListIndex = 1) Then
        RoomType = "W"
    ElseIf (lstRoom.ListIndex = 2) Then
        RoomType = "M"
    Else
        RoomType = "E"
    End If
    CurrentIndex = 1
    Call LoadExamRoom(RoomType)
End If
End Sub

Private Sub Timer1_Timer()
    On Error GoTo lTimer1_Timer_Error
    Dim ARType As String
    Dim MyName As String
    Dim MyActId As Long
    
    ' Record current context values
    MyActId = TheActivityId
    MyName = PatientName
    ARType = RoomType

    SetDateTime
    'If UserLogin.IsLoggedIn Then
    '    Call LoadMessageBox(lstMessages, UserLogin.iId)
    'End If
    frmHome.Enabled = False
    CurrentIndex = 0
    
    If UserLogin.IsLoggedIn Then
        LoadExamRoom ARType
        
        ' Restore context values after refresh
        PatientName = MyName
        TheActivityId = MyActId
        RoomType = ARType
    End If
    
    frmHome.Enabled = True

Exit Sub

lTimer1_Timer_Error:

    LogError "frmHome", "Timer1_Timer", Err, Err.Description

End Sub

Public Sub GetSystemDefaultText(SysId As Long, DefaultText As String)
DefaultText = ""
If (SysId <= MaxSystems) And (SysId > 0) Then
    DefaultText = SystemDefaultText(SysId)
End If
If (SysId = 19) Or (SysId = 20) Then
    DefaultText = SystemDefaultText(SysId)
End If
End Sub

Public Function IsSystemDefaultOn(SysId As Long) As Boolean
IsSystemDefaultOn = False
If (SysId <= MaxSystems) And (SysId > 0) Then
    IsSystemDefaultOn = SystemDefaultOn(SysId)
End If
If (SysId = 19) Or (SysId = 20) Then
    IsSystemDefaultOn = SystemDefaultOn(SysId)
End If
End Function

Public Sub SetSystemDefaultOn(SysId As Long)
If (SysId <= MaxSystems) And (SysId > 0) Then
    SystemDefaultOn(SysId) = True
End If
If (SysId = 19) Or (SysId = 20) Then
    SystemDefaultOn(SysId) = True
End If
End Sub

Public Sub GetLockedText(SysId As Long, DefaultText As String)
DefaultText = ""
If (SysId <= 8) Then
    DefaultText = LockedTests.TestText(SysId)
End If
End Sub

Public Sub GetLockedOrder(SysId As Long, DefaultText As String, TheQuestion As String, TheStyle As Boolean)
DefaultText = ""
If (SysId <= 8) Then
    DefaultText = LockedTests.TestOrder(SysId)
    TheQuestion = LockedTests.TestQuestion(SysId)
    TheStyle = LockedTests.TestStyle(SysId)
End If
End Sub

Public Function FindLockedOrder(DefaultText As String) As Long
Dim i As Long
FindLockedOrder = 0
For i = 1 To 8
    If (DefaultText = LockedTests.TestOrder(i)) Then
        FindLockedOrder = i
        Exit For
    End If
Next i
End Function

Public Function IsTestDisplayOn() As Boolean
IsTestDisplayOn = TestDisplayOn
End Function

Private Function GetTestStyle(Ques As String) As Boolean
Dim RetQues As DynamicForms
GetTestStyle = False
Set RetQues = New DynamicForms
RetQues.FormInterface = "T"
RetQues.Question = Trim(Ques)
If (RetQues.RetrieveForm) Then
    GetTestStyle = RetQues.EditType
End If
Set RetQues = Nothing
End Function

Private Function Login() As Boolean
    Timer1.Enabled = False
    UserLogin.LogoffUser
    Login = UserLogin.DisplayLoginScreen(ViewLoginFullScreen)
    If Not Login Then End
    
    'Show if frmHome is hidden
    If Not (frmHome.Visible) Then
       If Not AutoLogOff Then frmHome.Show
    End If

    'Refresh Location
    Dim Practice As New PracticeName
    Practice.PracticeType = "P"
    Practice.PracticeName = Chr(1)
    Practice.PracticeLocationReference = Replace(dbSoftwareLocation, "Office-", "")
    If (Practice.FindPractice > 0) Then
        If (Practice.SelectPractice(1)) Then
            iPracticeID = Practice.PracticeId
            If (Trim(dbSoftwareLocation) <> "") Then
                dbPracticeId = Practice.PracticeId + 1000
                lblPrc.Caption = Trim(Practice.PracticeName) + " " + Trim(Practice.PracticeLocationReference)
            Else
                dbPracticeId = 0
                lblPrc.Caption = Trim(Practice.PracticeName)
            End If
        End If
    End If
    Set Practice = Nothing
        
    If UserLogin.IsLoggedIn Then
        Login = True
        cmdLogin.Text = UserLogin.sNameLong
        'Call LoadMessageBox(lstMessages, UserLogin.iId)
    Else
        cmdLogin.Text = "Press Here to Login"
    End If
    Timer1.Enabled = True
End Function

Public Function LoadSystemNormals(NormalName) As Boolean
Dim i As Integer
Dim k As Integer
Dim TheRef As String
Dim RetCode As PracticeCodes
If (Trim(NormalName) = "") Then
    i = 0
    Erase SystemDefaultText
    Erase SystemDefaultOn
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "SYSTEMNORMAL"
    If (RetCode.FindCode > 0) Then
        i = 1
        While (RetCode.SelectCode(i))
            k = Val(Trim(Left(RetCode.ReferenceCode, 2)))
            If (k = 98) Then
                k = 19
            ElseIf (k = 99) Then
                k = 20
            End If
            SystemDefaultText(k) = Trim(Mid(RetCode.ReferenceCode, 6, Len(RetCode.ReferenceCode) - 5))
            SystemDefaultOn(k) = True
            i = i + 1
        Wend
    End If
    Set RetCode = Nothing
    If (i = 0) Then
        For i = 1 To 18
            TheRef = "SYSTEMNORMAL" + Trim(str(i))
            SystemDefaultText(i) = CheckConfigCollection(TheRef)
            SystemDefaultOn(i) = True
        Next i
        For i = 98 To 99
            TheRef = "SYSTEMNORMAL" + Trim(str(i))
            SystemDefaultText(i - 79) = CheckConfigCollection(TheRef)
            SystemDefaultOn(i - 79) = True
        Next i
    End If
Else
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = NormalName
    RetCode.ReferenceCode = Chr(1)
    RetCode.ReferenceAlternateCode = ""
    If (RetCode.FindCode > 0) Then
        i = 1
        While (RetCode.SelectCode(i))
            k = Val(Trim(Mid(RetCode.ReferenceCode, 2)))
            If (k = 98) Then
                k = 19
            ElseIf (k = 99) Then
                k = 20
            End If
            SystemDefaultText(k) = Trim(Mid(RetCode.ReferenceCode, 6, Len(RetCode.ReferenceCode) - 5))
            SystemDefaultOn(k) = True
            i = i + 1
        Wend
    End If
    Set RetCode = Nothing
End If
End Function

Public Function GetSystemLabelAbbr(LabelName As String, AbbrName As String) As Boolean
Dim i As Integer
Dim TheName(22) As String
GetSystemLabelAbbr = True
AbbrName = ""
If (Trim(LabelName) <> "") Then
    TheName(1) = "External       |Ext    "
    TheName(2) = "Pupils         |Pupils "
    TheName(3) = "EOM            |EOM    "
'    TheName(4) = "VF Conf        |VF     "
    TheName(4) = "VF Conf        |VF Conf"
    TheName(5) = "Lids Lacrimal  |Lid/Lac"
    TheName(6) = "Conj Sclera    |Cnj/Scl"
    TheName(7) = "Cornea         |Cornea "
    TheName(8) = "AC             |AC     "
    TheName(9) = "Iris           |Iris   "
    TheName(10) = "Lens           |Lens   "
'    TheName(11) = "Vitreous       |Vit    "
'    TheName(12) = "ON   Glauc     |ON/Glau"
'    TheName(13) = "BV             |BV     "
'    TheName(14) = "Macula         |Mac    "
'    TheName(15) = "Retina Choroid |Ret/Cho"
'    TheName(16) = "Periph Retina  |Periph "
    TheName(11) = "Vitreous       |Fun    "
    TheName(12) = "ON   Glauc     |Fun    "
    TheName(13) = "BV             |Fun    "
    TheName(14) = "Macula         |Fun    "
    TheName(15) = "Retina Choroid |Fun    "
    TheName(16) = "Periph Retina  |Fun    "
    TheName(17) = "Systemic       |Systmc "
    TheName(18) = "Vision Problems|VA/Prob"
    TheName(19) = "SLE            |SLE    "
    TheName(20) = "Fundus         |Fundus "
    TheName(21) = "Slit Lamp Exam |SLE    "
    TheName(22) = "Fundus Exam    |Fundus "
    For i = 1 To 22
        If (Trim(Left(TheName(i), 15)) = Trim(LabelName)) Then
            AbbrName = Trim(Mid(TheName(i), 17, Len(TheName(i)) - 16))
            Exit For
        End If
    Next i
End If
End Function

Public Function GetSystembyAbbr(TheSys As String, AbbrName As String) As Boolean
Dim i As Integer
Dim TheName(20) As String
GetSystembyAbbr = True
TheSys = ""
If (Trim(AbbrName) <> "") Then
    TheName(1) = "External       |Ext    "
    TheName(2) = "Pupils         |Pupils "
    TheName(3) = "EOM            |EOM    "
    TheName(4) = "VF Conf        |VF     "
    TheName(5) = "Lids Lacrimal  |Lid/Lac"
    TheName(6) = "Conj Sclera    |Cnj/Scl"
    TheName(7) = "Cornea         |Cornea "
    TheName(8) = "AC             |AC     "
    TheName(9) = "Iris           |Iris   "
    TheName(10) = "Lens           |Lens   "
    TheName(11) = "Vitreous       |Vit    "
    TheName(12) = "ON   Glauc     |ON/Glau"
    TheName(13) = "BV             |BV     "
    TheName(14) = "Macula         |Mac    "
    TheName(15) = "Retina Choroid |Ret/Cho"
    TheName(16) = "Periph Retina  |Periph "
    TheName(17) = "Systemic       |Systmc "
    TheName(18) = "Vision Problems|VA/Prob"
    TheName(19) = "SLE            |SLE    "
    TheName(20) = "Fundus Exam    |Fundus "
    For i = 1 To 20
        If (UCase(Trim(Mid(TheName(i), 17, Len(TheName(i)) - 16))) = UCase(Trim(AbbrName))) Then
            TheSys = Chr(i + 64)
            If (i = 19) Then
                TheSys = "Y"
            End If
            If (i = 20) Then
                TheSys = "X"
            End If
            Exit For
        End If
    Next i
End If
End Function

Public Function GetSystemByLetter(ALtr As String, SysName As String) As Boolean
Dim i As Integer
Dim TheName(22) As String
GetSystemByLetter = True
SysName = "---"
If (Trim(ALtr) <> "") Then
    ALtr = Trim(ALtr)
    i = Asc(ALtr) - 64
    If (i > 0) And (i < 19) Then
        TheName(1) = "External       |Ext    "
        TheName(2) = "Pupils         |Pupils "
        TheName(3) = "EOM            |EOM    "
        TheName(4) = "VF Conf        |VF Conf"
        TheName(5) = "Lids Lacrimal  |Lid/Lac"
        TheName(6) = "Conj Sclera    |Cnj/Scl"
        TheName(7) = "Cornea         |Cornea "
        TheName(8) = "AC             |AC     "
        TheName(9) = "Iris           |Iris   "
        TheName(10) = "Lens           |Lens   "
        TheName(11) = "Vitreous       |Fun    "
        TheName(12) = "ON   Glauc     |Fun    "
        TheName(13) = "BV             |Fun    "
        TheName(14) = "Macula         |Fun    "
        TheName(15) = "Retina Choroid |Fun    "
        TheName(16) = "Periph Retina  |Fun    "
        TheName(17) = "Systemic       |Systmc "
        TheName(18) = "Vision Problems|VA/Prob"
        SysName = Left(TheName(i), 3)
    End If
End If
End Function

Private Function LoadPatients(TheLName As String, TheFName As String, PatId As Long) As Boolean

Call StripCharacters(TheLName, "'")
Call StripCharacters(TheFName, "'")

Dim i As Integer
Dim ThePhone As String
Dim DisplayText As String
Dim RetPat As Patient
LoadPatients = False
lstPatient.Visible = False
lstPatient.Clear
cmdPrint.Visible = False
lstPatient.AddItem "Close Patient Selection"
If (Trim(TheLName) <> "") Then
    Set RetPat = New Patient
    RetPat.LastName = TheLName
    RetPat.FirstName = TheFName
    RetPat.SocialSecurity = ""
    RetPat.PatNumber = 0
    If (RetPat.FindPatient > 0) Then
        i = 1
        While (RetPat.SelectPatient(i)) And (i < 31)
            DisplayText = Space(75)
            Mid(DisplayText, 1, 24) = Trim(RetPat.LastName) + " " + Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial)
            Mid(DisplayText, 26, 8) = Trim(str(RetPat.PatientId))
            Mid(DisplayText, 36, 10) = Mid(RetPat.BirthDate, 5, 2) + "/" + Mid(RetPat.BirthDate, 7, 2) + "/" + Left(RetPat.BirthDate, 4)
            Call DisplayPhone(RetPat.HomePhone, ThePhone)
            Mid(DisplayText, 48, Len(Trim(ThePhone))) = Trim(ThePhone)
            DisplayText = DisplayText + Trim(str(RetPat.PatientId))
            lstPatient.AddItem DisplayText
            i = i + 1
        Wend
    End If
    If (lstPatient.ListCount > 0) Then
        lstPatient.Visible = True
        LoadPatients = True
    End If
    Set RetPat = Nothing
ElseIf (PatId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        DisplayText = Space(75)
        Mid(DisplayText, 1, 24) = Trim(RetPat.LastName) + " " + Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial)
        Mid(DisplayText, 26, 8) = Trim(str(RetPat.PatientId))
        Mid(DisplayText, 36, 10) = Mid(RetPat.BirthDate, 5, 2) + "/" + Mid(RetPat.BirthDate, 7, 2) + "/" + Left(RetPat.BirthDate, 4)
        Call DisplayPhone(RetPat.HomePhone, ThePhone)
        Mid(DisplayText, 48, Len(Trim(ThePhone))) = Trim(ThePhone)
        DisplayText = DisplayText + Trim(str(RetPat.PatientId))
        lstPatient.AddItem DisplayText
    End If
    If (lstPatient.ListCount > 0) Then
        lstPatient.Visible = True
        LoadPatients = True
    End If
    Set RetPat = Nothing
End If
End Function

Public Function LoadPatientActivity(PatId As Long, AListbox As ListBox, IgnoreOn As Boolean) As Boolean
Dim oDate As New CIODateTime
    If (PatId > 0) Then
        oDate.MyDateTime = Now
        If Not IgnoreOn Then
            If CheckActiveAppointments(PatId, oDate) Then
                Exit Function
            End If
        End If
        If LoadActivityList(PatId, oDate, AListbox) Then
            If lstPatient.ListCount > 1 Then
                cmdPrint.Visible = True
            End If
            AListbox.Visible = True
            LoadPatientActivity = True
        End If
    End If
    Set oDate = Nothing
End Function

Private Function CheckActiveAppointments(iPatientId As Long, oDate As CIODateTime) As Boolean
Dim EAL As New DI_ExamActivityList
    
    If EAL.PreviousActivity(iPatientId, oDate.GetIODate, "E,W,M") Then
        frmEventMsgs.InfoMessage "Patient has an active appointment, close it before editing"
        frmEventMsgs.Show 1
        CheckActiveAppointments = True
    End If
    Set EAL = Nothing
End Function

Private Function LoadActivityList(iPatientId As Long, oDate As CIODateTime, myListBox As ListBox) As Boolean
Dim sDisplayText As String
Dim iPrevApptId As Long
Dim EAL As New DI_ExamActivityList
Dim oApptdt As New CIODateTime
    myListBox.Clear
    myListBox.AddItem "Close Activity Selection"
    If EAL.PreviousActivity(iPatientId, oDate.GetIODate, "D,G,H") Then
        Do
            sDisplayText = Space(100)
            With EAL
                If .AppointmentId > 0 Then
                    If iPrevApptId <> .AppointmentId Then
                        If .Comments <> "ADD VIA BILLING" Then
                            iPrevApptId = .AppointmentId
                            oApptdt.SetDateFromIO .AppointmentDate
                            oApptdt.SetTimeFromIO .AppointmentTime
                            Mid$(sDisplayText, 1, 10) = oApptdt.GetDisplayDate(False)
                            Mid$(sDisplayText, 12, Len(oApptdt.GetDisplayTime)) = oApptdt.GetDisplayTime
                            Mid$(sDisplayText, 24, Len(Trim$(.AppointmentType))) = Trim$(.AppointmentType)
                            
                            Mid$(sDisplayText, 24 + Len(Trim$(.AppointmentType)) + 2, Len(Trim$(.ScheduledDoctor))) = Trim$(.ScheduledDoctor)
                            Mid$(sDisplayText, 76, Len(Trim$(str(.ActivityId)))) = Trim$(str(.ActivityId))
                            sDisplayText = Trim$(sDisplayText)
                            myListBox.AddItem sDisplayText
                        End If
                    End If
                End If
            End With
        Loop While EAL.NextRec
    End If
    Call CheckDateOrder(myListBox)
    LoadActivityList = True
    Set oApptdt = Nothing
    Set EAL = Nothing
End Function

Public Function LoadActivityEdit(ActId As Long) As Boolean
LoadActivityEdit = True
EditPreviousAppointmentOn = False
If (ActId > 0) Then
    lstPatient.Clear
    lstPatient.Visible = False
    cmdPrint.Visible = False
    TheActivityId = ActId
    EditPreviousAppointmentOn = True
    Call SelectSystems(0)
    If patientRuns = 1 Then
    Call pat.GetPatientUpdate(UpdatePid, pat.runscmplted)
    End If
    EditPreviousAppointmentOn = False
    LatestAppointmentOn = False
End If
End Function

Private Function LoadZones() As Boolean
Dim i As Long
Dim Rec As String
Dim iFreeFile As Long
On Error GoTo II
LoadZones = True
ImageLocationSupport = "0208"
Erase Zone02
For i = 1 To MaxZones02
    Zone02(i).Reference = ""
Next i
Erase Zone08
For i = 1 To MaxZones08
    Zone08(i).Reference = ""
Next i
If (FM.IsFileThere(PinPointDirectory + "Zones08.txt")) Then
    iFreeFile = FreeFile
    FM.OpenFile PinPointDirectory + "Zones08.txt", FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(iFreeFile)
    i = 1
    Do Until (EOF(iFreeFile))
        Line Input #iFreeFile, Rec
        If (i > 1) Then
            Zone08(i - 1).Reference = Mid(Rec, 1, 2)
            Zone08(i - 1).TheX = Val(Mid(Rec, 3, 5))
            Zone08(i - 1).TheY = Val(Mid(Rec, 8, 5))
        End If
        i = i + 1
        If (i > MaxZones08 + 1) Then
            Exit Do
        End If
    Loop
    FM.CloseFile CLng(iFreeFile), False
End If
If (FM.IsFileThere(PinPointDirectory + "Zones02.txt")) Then
    iFreeFile = FreeFile
    FM.OpenFile PinPointDirectory + "Zones02.txt", FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(iFreeFile)
    i = 1
    Do Until (EOF(iFreeFile))
        Line Input #iFreeFile, Rec
        If (i > 1) Then
            Zone02(i - 1).Reference = Mid(Rec, 11, 2)
            Zone02(i - 1).TheX = Val(Mid(Rec, 1, 5))
            Zone02(i - 1).TheY = Val(Mid(Rec, 6, 5))
        End If
        i = i + 1
        If (i > MaxZones02 + 1) Then
            Exit Do
        End If
    Loop
    FM.CloseFile CLng(iFreeFile), False
End If
Exit Function
II:
    Resume LeaveFast
LeaveFast:
End Function

Private Function LoadActions() As Boolean
Dim i As Integer, j As Integer, t As Integer
Dim RetCode As PracticeCodes
    On Error GoTo lLoadActions_Error

For i = 1 To 100
    StandardAction(i).Name = ""
    StandardAction(i).Trigger(1) = ""
    StandardAction(i).Trigger(2) = ""
    StandardAction(i).Trigger(3) = ""
Next i
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "Action"
If (RetCode.FindCode > 0) Then
    i = 1
    While (RetCode.SelectCode(i))
        j = InStrPS(RetCode.ReferenceCode, "!")
        If (j <> 0) Then
            StandardAction(i).Name = Left(RetCode.ReferenceCode, j - 1)
            t = InStrPS(StandardAction(i).Name, "-")
            If (t > 0) Then
                StandardAction(i).Name = Trim(Mid(StandardAction(i).Name, t + 1, Len(StandardAction(i).Name) - t))
            End If
            StandardAction(i).Trigger(1) = Mid(RetCode.ReferenceCode, j + 1, 1)
            StandardAction(i).Trigger(2) = Mid(RetCode.ReferenceCode, j + 2, 1)
            StandardAction(i).Trigger(3) = Mid(RetCode.ReferenceCode, j + 3, 1)
        Else
            StandardAction(i).Name = Trim(RetCode.ReferenceCode)
            t = InStrPS(StandardAction(i).Name, "-")
            If (t > 0) Then
                StandardAction(i).Name = Trim(Mid(StandardAction(i).Name, t + 1, Len(StandardAction(i).Name) - t))
            End If
            If (Trim(RetCode.ReferenceAlternateCode) <> "") Then
                StandardAction(i).Trigger(1) = Mid(RetCode.ReferenceAlternateCode, 1, 1)
                StandardAction(i).Trigger(2) = Mid(RetCode.ReferenceAlternateCode, 2, 1)
                StandardAction(i).Trigger(3) = Mid(RetCode.ReferenceAlternateCode, 3, 1)
            Else
                StandardAction(i).Trigger(1) = ""
                StandardAction(i).Trigger(2) = ""
                StandardAction(i).Trigger(3) = ""
            End If
        End If
        i = i + 1
    Wend
End If
Set RetCode = Nothing

    Exit Function

lLoadActions_Error:

    LogError "frmHome", "LoadActions", Err, Err.Description
End Function

Public Function GetAction(Idx As Integer, AText As String, BText As String, CText As String, DText As String) As Boolean
GetAction = False
AText = ""
BText = ""
CText = ""
DText = ""
If (Idx > 0) And (Idx < 51) Then
    AText = Trim(StandardAction(Idx).Name)
    BText = Trim(StandardAction(Idx).Trigger(1))
    CText = Trim(StandardAction(Idx).Trigger(2))
    DText = Trim(StandardAction(Idx).Trigger(3))
    If (AText <> "") Then
        GetAction = True
    End If
End If
End Function

Private Function LoadWebSites() As Boolean
Dim i As Integer
Dim k As Integer
Dim u As Integer
Dim Temp As String
Dim RetCode As PracticeCodes
    On Error GoTo lLoadWebSites_Error

For i = 1 To 50
    WebSites(i).DisplayText = ""
    WebSites(i).WebSite = ""
Next i
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "AccessAds"
If (RetCode.FindCode > 0) Then
    u = 0
    i = 1
    While (RetCode.SelectCode(i))
        If Trim$(RetCode.LetterLanguage) <> "" Then ' Supports new (top) and old way of storing AccessAds
            u = u + 1
            WebSites(u).DisplayText = RetCode.ReferenceCode
            WebSites(u).WebSite = RetCode.LetterLanguage
        Else
            Temp = Trim(RetCode.ReferenceCode)
            k = InStrPS(Temp, ":")
            If (k > 0) Then
                u = u + 1
                WebSites(u).DisplayText = Trim(Mid(Temp, 5, (k - 1) - 4))
                WebSites(u).WebSite = Trim(Mid(Temp, k + 1, Len(Temp) - k))
            End If
        End If
        i = i + 1
    Wend
End If
Set RetCode = Nothing

    Exit Function

lLoadWebSites_Error:

    LogError "frmHome", "LoadWebSites", Err, Err.Description
End Function

Public Function GetWebSite(Idx As Integer, AText As String, BText As String) As Boolean
GetWebSite = False
AText = ""
BText = ""
If (Idx > 0) And (Idx < 51) Then
    AText = Trim(WebSites(Idx).DisplayText)
    BText = Trim(WebSites(Idx).WebSite)
    If (AText <> "") And (BText <> "") Then
        GetWebSite = True
    End If
End If
End Function

Private Function LoadQualifiers() As Boolean
Dim i As Integer
Dim RetCode As PracticeCodes
    On Error GoTo lLoadQualifiers_Error

For i = 1 To MaxQualifiers
    Qualifiers(i).QCode = ""
    Qualifiers(i).QAltC = ""
    Qualifiers(i).QValue = False
    Qualifiers(i).QCat = False
Next i
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "QUANTIFIERS"
If (RetCode.FindCode > 0) Then
    i = 1
    While (RetCode.SelectCode(i))
        Qualifiers(i).QCode = Trim(RetCode.ReferenceCode)
        Qualifiers(i).QAltC = Trim(RetCode.ReferenceAlternateCode)
        Qualifiers(i).QCat = False
        If (Trim(RetCode.Exclusion) = "T") Then
            Qualifiers(i).QCat = True
        End If
        Qualifiers(i).QValue = False
        If (Trim(RetCode.Signature) = "T") Then
            Qualifiers(i).QValue = True
        End If
        i = i + 1
    Wend
Else
    Qualifiers(1).QCode = "mm"
    Qualifiers(1).QAltC = "mm "
    Qualifiers(2).QCode = "cm"
    Qualifiers(2).QAltC = "cm "
    Qualifiers(3).QCode = "trace"
    Qualifiers(3).QAltC = "trace"
    Qualifiers(4).QCode = "mild"
    Qualifiers(4).QAltC = "mild"
    Qualifiers(5).QCode = "severe"
    Qualifiers(5).QAltC = "severe"
    Qualifiers(6).QCode = "moderate"
    Qualifiers(6).QAltC = "moderate"
    Qualifiers(7).QCode = "%"
    Qualifiers(7).QAltC = "%  "
    Qualifiers(8).QCode = "dense"
    Qualifiers(8).QAltC = "dense"
    Qualifiers(9).QCode = "light"
    Qualifiers(9).QAltC = "light"
    Qualifiers(10).QCode = "diffuse"
    Qualifiers(10).QAltC = "diffuse"
    Qualifiers(11).QCode = "central"
    Qualifiers(11).QAltC = "central"
    Qualifiers(12).QCode = "stage"
    Qualifiers(12).QAltC = "stage"
    Qualifiers(13).QCode = "stable"
    Qualifiers(13).QAltC = "stable"
    Qualifiers(14).QCode = "rare"
    Qualifiers(14).QAltC = "rare"
    Qualifiers(15).QCode = "Questionable"
    Qualifiers(15).QAltC = "que"
    Qualifiers(16).QCode = "RU"
    Qualifiers(16).QAltC = "RU "
    Qualifiers(17).QCode = "RL"
    Qualifiers(17).QAltC = "RL "
    Qualifiers(18).QCode = "LU"
    Qualifiers(18).QAltC = "LU "
    Qualifiers(19).QCode = "LL"
    Qualifiers(19).QAltC = "LL "
    Qualifiers(20).QCode = "flat"
    Qualifiers(20).QAltC = "flat"
    Qualifiers(21).QCode = "raised"
    Qualifiers(21).QAltC = "raised"
    Qualifiers(22).QCode = "Disc Area"
    Qualifiers(22).QAltC = "DA "
    Qualifiers(23).QCode = "Disc Diameter"
    Qualifiers(23).QAltC = "DD "
    Qualifiers(24).QCode = ""
    Qualifiers(24).QAltC = ""
End If
Set RetCode = Nothing

    Exit Function

lLoadQualifiers_Error:

    LogError "frmHome", "LoadQualifiers", Err, Err.Description
End Function

Public Sub DoQualifier(Qual As String, TheEye As String, QuitOn As Boolean)
Dim i As Integer
Dim QntRef As String
Dim QntRefEnd As String
Dim AText As String, BText As String
Dim ACat As Boolean, APad As Boolean
Dim RefText(36) As String
Qual = ""
Erase RefText
QntRef = "["
QntRefEnd = "]"
QuitOn = False
If CheckConfigCollection("NEWQUANTIFIERS") <> "T" Then
    frmNumericPad.NumPad_Result = ""
    frmNumericPad.NumPad_Field = "Quantifier - " + TheEye
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_DisplayFull = True
    For i = 1 To 24
        Call GetQualifier(i, AText, BText, ACat, APad)
        If (i = 1) Then
            frmNumericPad.NumPad_Choice1 = AText
            RefText(i) = BText
        ElseIf (i = 2) Then
            frmNumericPad.NumPad_Choice2 = AText
            RefText(i) = BText
        ElseIf (i = 3) Then
            frmNumericPad.NumPad_Choice3 = AText
            RefText(i) = BText
        ElseIf (i = 4) Then
            frmNumericPad.NumPad_Choice4 = AText
            RefText(i) = BText
        ElseIf (i = 5) Then
            frmNumericPad.NumPad_Choice5 = AText
            RefText(i) = BText
        ElseIf (i = 6) Then
            frmNumericPad.NumPad_Choice6 = AText
            RefText(i) = BText
        ElseIf (i = 7) Then
            frmNumericPad.NumPad_Choice7 = AText
            RefText(i) = BText
        ElseIf (i = 8) Then
            frmNumericPad.NumPad_Choice8 = AText
            RefText(i) = BText
        ElseIf (i = 9) Then
            frmNumericPad.NumPad_Choice9 = AText
            RefText(i) = BText
        ElseIf (i = 10) Then
            frmNumericPad.NumPad_Choice10 = AText
            RefText(i) = BText
        ElseIf (i = 11) Then
            frmNumericPad.NumPad_Choice11 = AText
            RefText(i) = BText
        ElseIf (i = 12) Then
            frmNumericPad.NumPad_Choice12 = AText
            RefText(i) = BText
        ElseIf (i = 13) Then
            frmNumericPad.NumPad_Choice13 = AText
            RefText(i) = BText
        ElseIf (i = 14) Then
            frmNumericPad.NumPad_Choice14 = AText
            RefText(i) = BText
        ElseIf (i = 15) Then
            frmNumericPad.NumPad_Choice15 = AText
            RefText(i) = BText
        ElseIf (i = 16) Then
            frmNumericPad.NumPad_Choice16 = AText
            RefText(i) = BText
        ElseIf (i = 17) Then
            frmNumericPad.NumPad_Choice17 = AText
            RefText(i) = BText
        ElseIf (i = 18) Then
            frmNumericPad.NumPad_Choice18 = AText
            RefText(i) = BText
        ElseIf (i = 19) Then
            frmNumericPad.NumPad_Choice19 = AText
            RefText(i) = BText
        ElseIf (i = 20) Then
            frmNumericPad.NumPad_Choice20 = AText
            RefText(i) = BText
        ElseIf (i = 21) Then
            frmNumericPad.NumPad_Choice21 = AText
            RefText(i) = BText
        ElseIf (i = 22) Then
            frmNumericPad.NumPad_Choice22 = AText
            RefText(i) = BText
        ElseIf (i = 23) Then
            frmNumericPad.NumPad_Choice23 = AText
            RefText(i) = BText
        ElseIf (i = 24) Then
            frmNumericPad.NumPad_Choice24 = AText
            RefText(i) = BText
        End If
    Next i
    frmNumericPad.Show 1
    If Not (frmNumericPad.NumPad_Quit) Then
        Qual = QntRef + Trim(frmNumericPad.NumPad_Result)
        If (Len(Qual) > 7) Then
            Qual = Left(Qual, 7)
        End If
        If (frmNumericPad.NumPad_ChoiceOn1) Then
            Qual = Qual + RefText(1)
        End If
        If (frmNumericPad.NumPad_ChoiceOn2) Then
            Qual = Qual + RefText(2)
        End If
        If (frmNumericPad.NumPad_ChoiceOn3) Then
            Qual = Qual + RefText(3)
        End If
        If (frmNumericPad.NumPad_ChoiceOn4) Then
            Qual = Qual + RefText(4)
        End If
        If (frmNumericPad.NumPad_ChoiceOn5) Then
            Qual = Qual + RefText(5)
        End If
        If (frmNumericPad.NumPad_ChoiceOn6) Then
            Qual = Qual + RefText(6)
        End If
        If (frmNumericPad.NumPad_ChoiceOn7) Then
            Qual = Qual + RefText(7)
        End If
        If (frmNumericPad.NumPad_ChoiceOn8) Then
            Qual = Qual + RefText(8)
        End If
        If (frmNumericPad.NumPad_ChoiceOn9) Then
            Qual = Qual + RefText(9)
        End If
        If (frmNumericPad.NumPad_ChoiceOn10) Then
            Qual = Qual + RefText(10)
        End If
        If (frmNumericPad.NumPad_ChoiceOn11) Then
            Qual = Qual + RefText(11)
        End If
        If (frmNumericPad.NumPad_ChoiceOn12) Then
            Qual = Qual + RefText(12)
        End If
        If (frmNumericPad.NumPad_ChoiceOn13) Then
            Qual = Qual + RefText(13)
        End If
        If (frmNumericPad.NumPad_ChoiceOn14) Then
            Qual = Qual + RefText(14)
        End If
        If (frmNumericPad.NumPad_ChoiceOn15) Then
            Qual = Qual + RefText(15)
        End If
        If (frmNumericPad.NumPad_ChoiceOn16) Then
            Qual = Qual + RefText(16)
        End If
        If (frmNumericPad.NumPad_ChoiceOn17) Then
            Qual = Qual + RefText(17)
        End If
        If (frmNumericPad.NumPad_ChoiceOn18) Then
            Qual = Qual + RefText(18)
        End If
        If (frmNumericPad.NumPad_ChoiceOn19) Then
            Qual = Qual + RefText(19)
        End If
        If (frmNumericPad.NumPad_ChoiceOn20) Then
            Qual = Qual + RefText(20)
        End If
        If (frmNumericPad.NumPad_ChoiceOn21) Then
            Qual = Qual + RefText(21)
        End If
        If (frmNumericPad.NumPad_ChoiceOn22) Then
            Qual = Qual + RefText(22)
        End If
        If (frmNumericPad.NumPad_ChoiceOn23) Then
            Qual = Qual + RefText(23)
        End If
        Qual = Trim(Qual) + QntRefEnd
        If (Qual = QntRef + QntRefEnd) And Not (frmNumericPad.ClearValueOn) Then
            Qual = ""
        End If
    End If
Else
    Qual = ""
        
    frmQuantifiers.Show 1
    If Not (frmQuantifiers.QuitOn) Then
        If (Trim(frmQuantifiers.TheResults) <> "") Then
            If (Qual = QntRef + QntRefEnd) And Not (frmNumericPad.ClearValueOn) Then
                Qual = ""
            End If
            If (Trim(Qual) = "") Then
                Qual = QntRef + Trim(frmQuantifiers.TheResults) + QntRefEnd
            End If
        Else
            Qual = "[]"
        End If
    Else
        QuitOn = True
    End If
End If
End Sub

Public Function SetLocation(Ix As Single, Iy As Single, BaseWidth As Single, ABase As String, ALoc As String) As Boolean
Dim i As Long
Dim AStart As Long
Dim AEnd As Long
SetLocation = False
ALoc = ""
If (ABase = "08") Then
    For i = 1 To MaxZones08
        If (Trim(Zone08(i).Reference) <> "") Then
            If (Ix = Zone08(i).TheX) And (Iy = Zone08(i).TheY) Then
                ALoc = Zone08(i).Reference
                SetLocation = True
                Exit For
            End If
        End If
    Next i
ElseIf (ABase = "02") Then
    AStart = MaxZones02 / 2
    AEnd = MaxZones02
    If (Ix < Zone02(AStart).TheX) Then
        AStart = 1
        AEnd = MaxZones02 / 2
    End If
    For i = AStart To AEnd
        If (Trim(Zone02(i).Reference) <> "") Then
            If (Ix = Zone02(i).TheX) And (Iy = Zone02(i).TheY) Then
                ALoc = Zone02(i).Reference
                SetLocation = True
                Exit For
            ElseIf (Ix < Zone02(i).TheX) Then
                Exit For
            End If
        End If
    Next i
    If Not (SetLocation) Then
        ALoc = "27"
        SetLocation = True
    End If
End If
End Function

Private Function ApplLoadStaff() As Boolean
Dim i As Integer
Dim DisplayText As String
Dim RetDr As SchedulerResource
ApplLoadStaff = True
lstDr.Clear
DisplayText = Space(75)
Mid(DisplayText, 1, 13) = "All Resources"
DisplayText = DisplayText + Trim(str(0))
lstDr.AddItem DisplayText
Set RetDr = New SchedulerResource
RetDr.ResourceName = Chr(1)
If (RetDr.FindResourcebyDoctor) Then
    i = 1
    While (RetDr.SelectResource(i))
        DisplayText = Space(75)
        Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
        DisplayText = DisplayText + Trim(str(RetDr.ResourceId))
        lstDr.AddItem DisplayText
        i = i + 1
    Wend
End If
Set RetDr = Nothing
End Function

Private Function ApplLoadRooms() As Boolean
Dim i As Integer
Dim DisplayText As String
Dim RetDr As SchedulerResource
ApplLoadRooms = True
lstRoom.Clear
DisplayText = Space(75)
Mid(DisplayText, 1, 13) = "All Rooms"
DisplayText = DisplayText + Trim(str(0))
lstRoom.AddItem DisplayText
DisplayText = Space(75)
Mid(DisplayText, 1, 13) = "Question"
DisplayText = DisplayText + Trim(str(0))
lstRoom.AddItem DisplayText
DisplayText = Space(75)
Mid(DisplayText, 1, 13) = "Wait"
DisplayText = DisplayText + Trim(str(0))
lstRoom.AddItem DisplayText
Set RetDr = New SchedulerResource
'RetDr.ResourceName = Chr(1)
RetDr.PracticeId = iPracticeID
If (RetDr.FindResourcebyRoom) Then
    i = 1
    While (RetDr.SelectResource(i))
        DisplayText = Space(75)
        Mid(DisplayText, 1, Len(Trim(RetDr.ResourceName))) = Trim(RetDr.ResourceName)
        DisplayText = DisplayText + Trim(str(RetDr.ResourceId))
        lstRoom.AddItem DisplayText
        i = i + 1
    Wend
End If
Set RetDr = Nothing
End Function

Private Function FirstAction(PName As String) As Boolean
Dim AHostName As String
FirstAction = False
If RoomType = "E" Then
    frmEventMsgs.SetButtons "Action ?", "Change Room", "Open Chart", "Cancel", "Move to the Top", "Change Room, Open Chart"
Else
    frmEventMsgs.SetButtons "Action ?", "Change Room", "", "Cancel", "", "Change Room, Open Chart"
End If
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    Call AssignRoom(PName, TheActivityId, False)
ElseIf (frmEventMsgs.Result = 2) Then
If Not UserLogin.HasPermission(epOpenChart) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Function
 
End If
        If Not (IsPatientAccessible(TheActivityId)) Then
            AHostName = Trim(GetLockFileHostName(TheActivityId))
            If AHostName = "" Then
                frmEventMsgs.InfoMessage "Patient Chart currently in use."
            Else
                frmEventMsgs.InfoMessage "Patient Chart currently in use on " & AHostName & "."
            End If
            frmEventMsgs.Show 1
        Else
            FirstAction = True
        End If
        'runscmplted = Pat.GetPatientAccess(PatId)
    ElseIf (frmEventMsgs.Result = 3) Then
        Call MoveToTop(TheActivityId)
    ElseIf (frmEventMsgs.Result = 5) Then
        If (AssignRoom(PName, TheActivityId, True)) Then
            RoomType = "E"
            If Not (IsPatientAccessible(TheActivityId)) Then
                AHostName = Trim(GetLockFileHostName(TheActivityId))
                If AHostName = "" Then
                    frmEventMsgs.InfoMessage "Patient Chart currently in use."
                Else
                    frmEventMsgs.InfoMessage "Patient Chart currently in use on " & AHostName & "."
                End If
                frmEventMsgs.Show 1
            Else
                FirstAction = True
            End If
        End If
    End If
End Function

Private Function AssignRoom(PName As String, ActId As Long, NextStep As Boolean) As Boolean
Dim ATime As String
Dim BTime As Integer
Dim RetAct As PracticeActivity
AssignRoom = False
If (Trim(PName) <> "") And (ActId > 0) Then
    frmHome.Enabled = False
    frmAssignRoom.ThePatientName = Trim(PName)
    frmAssignRoom.TheActivityId = ActId
    If (frmAssignRoom.LoadAssignableRooms(True)) Then
        frmAssignRoom.Show 1
        If (frmAssignRoom.TheResourceId > 0) Then
            If Not (NextStep) Then
                CurrentIndex = 1
                Call LoadExamRoom(RoomType)
            End If
            AssignRoom = True
        End If
    Else
        Set RetAct = New PracticeActivity
        RetAct.ActivityId = ActId
        If (RetAct.RetrieveActivity) Then
            RetAct.ActivityResourceId = RetAct.ActivityLocationId
            ATime = ""
            Call FormatTimeNow(ATime)
            BTime = ConvertTimeToMinutes(ATime)
            RetAct.ActivityStatusTime = ATime
            RetAct.ActivityStatusTRef = BTime
            RetAct.ActivityStatus = "E"
            If (RetAct.ApplyActivity) Then
                If Not (NextStep) Then
                    CurrentIndex = 1
                    Call LoadExamRoom(RoomType)
                End If
                AssignRoom = True
            End If
        End If
        Set RetAct = Nothing
    End If
    frmHome.Enabled = True
    Call TurnOn
End If
End Function

Private Function MoveToTop(ActId As Long) As Boolean
Dim RetAct As PracticeActivity
MoveToTop = False
If (ActId > 0) Then
    Set RetAct = New PracticeActivity
    RetAct.ActivityId = ActId
    If (RetAct.RetrieveActivity) Then
        RetAct.ActivityStatusTime = "00:10 AM"
        RetAct.ActivityStatusTRef = ConvertTimeToMinutes("00:10 AM")
        Call RetAct.ApplyActivity
        MoveToTop = True
    End If
    Set RetAct = Nothing
    If (MoveToTop) Then
        CurrentIndex = 1
        Call LoadExamRoom(RoomType)
    End If
End If
End Function

Public Function GetQualifier(Idx As Integer, AText As String, BText As String, TheCat As Boolean, ThePad As Boolean) As Boolean
    AText = ""
    BText = ""
    TheCat = False
    ThePad = False
    If (Idx > 0) And (Idx <= MaxQualifiers) Then
        AText = Qualifiers(Idx).QCode
        BText = Qualifiers(Idx).QAltC
        TheCat = Qualifiers(Idx).QCat
        ThePad = Qualifiers(Idx).QValue
        GetQualifier = True
    End If
End Function

Public Function IsPatientAccessible(ActId As Long) As Boolean
Dim ApptId As Long
Dim RetAct As PracticeActivity
Dim RetAppt As SchedulerAppointment
IsPatientAccessible = True
If (ActId > 0) Then
    Set RetAct = New PracticeActivity
    RetAct.ActivityId = ActId
    If (RetAct.RetrieveActivity) Then
        ApptId = RetAct.ActivityAppointmentId
'        If (InStrPS("DHGU", RetAct.ActivityStatus) > 0) Then
'            If (RetAct.ActivityStationId = 0) Then
'                IsPatientAccessible = True
'            End If
'        End If
    End If
    Set RetAct = Nothing
End If
If (IsPatientAccessible) Then
    If (ApptId > 0) Then
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentId = ApptId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            If (InStrPS("PAR", RetAppt.AppointmentStatus) = 0) Then
                IsPatientAccessible = False
            End If
        End If
        Set RetAppt = Nothing
    Else
        IsPatientAccessible = False
    End If
End If
End Function

Private Function IsFilesOnServer(PatId As Long) As Boolean
' Tests to see if the lock condition is real
' Done this way to give the other user a chance to upload any files
' in the process of being copies to the server when the look occurs.
'
Dim FilesAreThere As Boolean
Dim MyFile As String
Dim TheFile As String
Dim TempDir As String
IsFilesOnServer = True
FilesAreThere = False
If (PatId > 0) Then
    TempDir = ServerDoctorInterfaceDirectory
    MyFile = TempDir + "*_" + Trim(str(PatId)) + ".txt"
    TheFile = Dir(MyFile)
    While (TheFile <> "")
        TheFile = Dir
        FilesAreThere = True
    Wend
    IsFilesOnServer = FilesAreThere
End If
End Function

Private Function IsChartLocked(iActivityId As Long) As Boolean
Dim Activity As New PracticeActivity
    If iActivityId > 0 Then
        With Activity
            .ActivityId = iActivityId
            If .RetrieveActivity Then
                If .ActivityStationId > 0 Then
                    IsChartLocked = True
                End If
                If Dir(GetLockFileName(.ActivityPatientId, iActivityId)) <> "" Then
                    IsChartLocked = True
                End If
            Else
                IsChartLocked = True
            End If
        End With
    End If
    Set Activity = Nothing
End Function

Private Sub UnlockChart(iActId As Long)
Dim RetAct As New PracticeActivity
    If iActId > 0 Then
        frmHome.Enabled = False
        With RetAct
            .ActivityId = iActId
            If .RetrieveActivity Then
                .ActivityStationId = 0
                .ApplyActivity
                KillLockFile .ActivityPatientId, iActId
            End If
        End With
        frmHome.Enabled = True
    End If
    Set RetAct = Nothing
End Sub

Private Function CreateLockFile(iPatientId As Long, iActivityId As Long) As Boolean
Dim sFreeFile As Integer
    On Error GoTo lCreateLockFile_Error
    sFreeFile = FreeFile
    FM.OpenFile GetLockFileName(iPatientId, iActivityId), FileOpenMode.Output, FileAccess.ReadWrite, CLng(sFreeFile)
    FM.CloseFile CLng(sFreeFile)
    CreateLockFile = True
    Exit Function

lCreateLockFile_Error:

    LogError "frmHome", "CreateLockFile", Err, Err.Description
End Function

Private Sub KillLockFile(iPatientId As Long, iActivityId As Long)
Dim sFileName As String
    On Error GoTo lKillLockFile_Error
    
    sFileName = GetLockFileName(iPatientId, iActivityId)
    If iPatientId > 0 Then
        If iActivityId > 0 Then
            FM.Kill sFileName
        End If
    End If

    Exit Sub

lKillLockFile_Error:

    LogError "frmHome", "KillLockFile", Err, Err.Description, sFileName
End Sub

Private Function GetLockFileName(iPatientId As Long, iActivityId As Long) As String
    GetLockFileName = ServerDoctorInterfaceDirectory & "Locked-" & Trim$(str$(iActivityId)) & "-" & Trim$(str$(iPatientId)) & ".txt"
End Function

Private Sub Timer2_Timer()
    If UserLogin.IsLoggedIn Then
        Dim lii As LASTINPUTINFO
        lii.cbSize = Len(lii)
        Dim LogoffTime As String
        LogoffTime = CheckConfigCollection("AUTOLOGOFF")
        If LogoffTime <> "" Then
            Call GetLastInputInfo(lii)
            If (FormatNumber((GetTickCount() - lii.dwTime) / 1000, 3)) >= LogoffTime * 60 Then
                AutoLogOff = True
                ' Sign out
                Call cmdLogin_Click
            End If
        End If
    End If
End Sub
Public Sub CloseAllForms()
Dim iLoop As Integer
Dim iHighestForm As Integer
If Not Screen.ActiveForm Is Nothing Then
    iHighestForm = Screen.ActiveForm.Count
    iLoop = iHighestForm
    On Error Resume Next
    Do Until iLoop = 0
        Call Forms(iLoop).FrmClose
        iLoop = iLoop - 1
    Loop
End If
 End Sub
Public Sub CheckDateOrder(AListbox As ListBox)
If AListbox.ListCount > 0 Then
    Dim strDateFirst As String
    Dim strTimeFirst As String
    Dim strDateSecond As String
    Dim strTimeSecond As String
    Dim Temp As String
    Dim i As Integer
    For i = 1 To AListbox.ListCount - 1
        strDateFirst = Mid(AListbox.List(i), 1, 10)
        strTimeFirst = Mid$(AListbox.List(i), 12, 7)
        If i < AListbox.ListCount - 1 Then
        strDateSecond = Mid(AListbox.List(i + 1), 1, 10)
        strTimeSecond = Mid$(AListbox.List(i + 1), 12, 7)
            If CompareDate(strDateFirst, strDateSecond) = 0 Then
                If CompareTime(strTimeFirst, strTimeSecond) < 0 Then
                    Temp = AListbox.List(i)
                    AListbox.RemoveItem i
                    AListbox.AddItem AListbox.List(i), i
                    AListbox.RemoveItem i + 1
                    AListbox.AddItem Temp, i + 1
                End If
            End If
        End If
    Next
End If
End Sub
Public Function GetLockFileHostName(ByVal ActivityId As Long)
GetLockFileHostName = ""
Dim SqlStr As String
Dim RS As Recordset

    SqlStr = " Select TOP 1 HostName from AuditEntries ae " & _
             " INNER JOIN AuditEntryChanges aec on aec.AuditEntryId=ae.Id " & _
             " WHERE ae.ObjectName='dbo.PracticeActivity' and ae.KeyValueNumeric= " & ActivityId & " " & _
             " order by ae.AuditDateTime desc "

Set RS = GetRS(SqlStr)
If (Not (RS Is Nothing) And (RS.RecordCount > 0)) Then
    GetLockFileHostName = RS("HostName")
End If

End Function
