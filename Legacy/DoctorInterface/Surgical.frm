VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{4BD5A3A1-7FFE-11D4-A13A-004005FA6275}#1.0#0"; "ImagXpr6.dll"
Begin VB.Form frmSurgical 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9195
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12165
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "Surgical.frx":0000
   ScaleHeight     =   9195
   ScaleWidth      =   12165
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstAllTests 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5580
      ItemData        =   "Surgical.frx":36C9
      Left            =   1920
      List            =   "Surgical.frx":36CB
      TabIndex        =   58
      Top             =   1320
      Visible         =   0   'False
      Width           =   7335
   End
   Begin VB.TextBox txtAim 
      Height          =   375
      Left            =   3840
      MaxLength       =   12
      TabIndex        =   62
      Top             =   5280
      Width           =   1095
   End
   Begin VB.ComboBox lstIOLb 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3360
      Sorted          =   -1  'True
      TabIndex        =   53
      Top             =   3720
      Width           =   1575
   End
   Begin VB.ComboBox lstIOL 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3360
      Sorted          =   -1  'True
      TabIndex        =   52
      Top             =   3120
      Width           =   1575
   End
   Begin VB.TextBox txtIOLP 
      Height          =   375
      Left            =   5040
      TabIndex        =   42
      Top             =   3120
      Width           =   975
   End
   Begin VB.TextBox txtArc 
      Height          =   375
      Left            =   5400
      TabIndex        =   38
      Top             =   5280
      Width           =   615
   End
   Begin VB.TextBox txtAxis 
      Height          =   375
      Left            =   5400
      TabIndex        =   37
      Top             =   4800
      Width           =   615
   End
   Begin VB.TextBox txtInc 
      Height          =   375
      Left            =   5400
      TabIndex        =   36
      Top             =   4320
      Width           =   615
   End
   Begin VB.TextBox txtIOLbP 
      Height          =   375
      Left            =   5040
      TabIndex        =   33
      Top             =   3720
      Width           =   975
   End
   Begin VB.TextBox txtPO 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6120
      TabIndex        =   31
      Top             =   4200
      Width           =   2775
   End
   Begin VB.TextBox txtNotes 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   3840
      MaxLength       =   1020
      TabIndex        =   25
      Top             =   5880
      Width           =   7935
   End
   Begin VB.ListBox lstSurgReq 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2490
      Left            =   360
      TabIndex        =   23
      Top             =   600
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.ListBox lstMedClr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "Surgical.frx":36CD
      Left            =   9000
      List            =   "Surgical.frx":36CF
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   19
      Top             =   3240
      Width           =   2775
   End
   Begin VB.ListBox lstORMedSup 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "Surgical.frx":36D1
      Left            =   9000
      List            =   "Surgical.frx":36D3
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   21
      Top             =   4080
      Width           =   2775
   End
   Begin VB.ListBox lstTrans 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "Surgical.frx":36D5
      Left            =   9000
      List            =   "Surgical.frx":36D7
      Sorted          =   -1  'True
      TabIndex        =   17
      Top             =   4920
      Width           =   2775
   End
   Begin VB.ListBox lstAType 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "Surgical.frx":36D9
      Left            =   6120
      List            =   "Surgical.frx":36DB
      Sorted          =   -1  'True
      TabIndex        =   15
      Top             =   3360
      Width           =   2775
   End
   Begin VB.ListBox lstResource2 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "Surgical.frx":36DD
      Left            =   6120
      List            =   "Surgical.frx":36DF
      Sorted          =   -1  'True
      TabIndex        =   10
      Top             =   2520
      Width           =   2775
   End
   Begin VB.ListBox lstResource1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "Surgical.frx":36E1
      Left            =   6120
      List            =   "Surgical.frx":36E3
      Sorted          =   -1  'True
      TabIndex        =   9
      Top             =   1680
      Width           =   2775
   End
   Begin VB.ListBox lstPreMeds 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      ItemData        =   "Surgical.frx":36E5
      Left            =   9000
      List            =   "Surgical.frx":36E7
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   7
      Top             =   840
      Width           =   2775
   End
   Begin VB.ListBox lstORIns 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      ItemData        =   "Surgical.frx":36E9
      Left            =   9000
      List            =   "Surgical.frx":36EB
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   2160
      Width           =   2775
   End
   Begin VB.ListBox lstResource0 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      ItemData        =   "Surgical.frx":36ED
      Left            =   6120
      List            =   "Surgical.frx":36EF
      Sorted          =   -1  'True
      TabIndex        =   8
      Top             =   840
      Width           =   2775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10200
      TabIndex        =   2
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Surgical.frx":36F1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   0
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Surgical.frx":38D0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDocs 
      Height          =   990
      Left            =   6840
      TabIndex        =   3
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Surgical.frx":3AAF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReview 
      Height          =   990
      Left            =   8520
      TabIndex        =   24
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Surgical.frx":3C93
   End
   Begin IMAGXPR6LibCtl.ImagXpress FxImage1 
      Height          =   2895
      Left            =   120
      TabIndex        =   44
      Top             =   4920
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   5106
      ErrStr          =   "MDYC0090GEP-0B3060SXEP"
      ErrCode         =   379523170
      ErrInfo         =   -1073235311
      Persistence     =   -1  'True
      _cx             =   1
      _cy             =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   16777215
      AutoSize        =   4
      BorderType      =   2
      ScrollBarLargeChangeH=   10
      ScrollBarSmallChangeH=   1
      OLEDropMode     =   0
      ScrollBarLargeChangeV=   10
      ScrollBarSmallChangeV=   1
      DisplayProgressive=   -1  'True
      SaveTIFByteOrder=   0
      LoadRotated     =   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      Begin VB.Label lblDrawDate 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Label1"
         Height          =   255
         Left            =   0
         TabIndex        =   45
         Top             =   0
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   990
      Left            =   5160
      TabIndex        =   54
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Surgical.frx":3E7D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNextPic 
      Height          =   990
      Left            =   1800
      TabIndex        =   55
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Surgical.frx":405D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSurgReq 
      Height          =   990
      Left            =   3480
      TabIndex        =   57
      Top             =   7920
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Surgical.frx":4243
   End
   Begin VB.ComboBox lstCT 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3360
      Sorted          =   -1  'True
      TabIndex        =   59
      Top             =   4320
      Width           =   1575
   End
   Begin VB.Label lblAim 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Objective"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   3840
      TabIndex        =   63
      Top             =   5040
      Width           =   900
   End
   Begin VB.Label lblLoc1 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Loc:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   345
      Left            =   120
      TabIndex        =   61
      Top             =   4440
      Width           =   3285
   End
   Begin VB.Label lblCT 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Case Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   3360
      TabIndex        =   60
      Top             =   4080
      Width           =   990
   End
   Begin VB.Label lblSurgReq 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Surgical Request:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   360
      TabIndex        =   56
      Top             =   360
      Visible         =   0   'False
      Width           =   1680
   End
   Begin VB.Label lblIOL 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "IOL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   3360
      TabIndex        =   51
      Top             =   2880
      Width           =   330
   End
   Begin VB.Label lblMRX 
      BackColor       =   &H00404000&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   945
      Left            =   120
      TabIndex        =   50
      Top             =   3000
      Width           =   2925
   End
   Begin VB.Label lblIOLM 
      BackColor       =   &H00404000&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   945
      Left            =   120
      TabIndex        =   49
      Top             =   1920
      Width           =   2925
   End
   Begin VB.Label lblAScan 
      BackColor       =   &H00404000&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   945
      Left            =   120
      TabIndex        =   48
      Top             =   840
      Width           =   2925
   End
   Begin VB.Label lblKero 
      BackColor       =   &H00AD8A46&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   945
      Left            =   3120
      TabIndex        =   47
      Top             =   1920
      Width           =   2925
   End
   Begin VB.Label lblAKero 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   945
      Left            =   3120
      TabIndex        =   46
      Top             =   840
      Width           =   2925
   End
   Begin VB.Label lblPower1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Power"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   5040
      TabIndex        =   43
      Top             =   2880
      Width           =   615
   End
   Begin VB.Label lblInc 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Incisions"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   5160
      TabIndex        =   41
      Top             =   4080
      Width           =   825
   End
   Begin VB.Label lblAxis 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Axis"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   4920
      TabIndex        =   40
      Top             =   4800
      Width           =   405
   End
   Begin VB.Label lblArc 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Arc"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   5040
      TabIndex        =   39
      Top             =   5280
      Width           =   315
   End
   Begin VB.Label lblLRI 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "LRI"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   4800
      TabIndex        =   35
      Top             =   4080
      Width           =   315
   End
   Begin VB.Label lblPower2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Power"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   240
      Left            =   5040
      TabIndex        =   34
      Top             =   3480
      Width           =   615
   End
   Begin VB.Label lblPO 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Patient Orders"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   6120
      TabIndex        =   32
      Top             =   3960
      Width           =   1365
   End
   Begin VB.Label lblTime 
      BackColor       =   &H00AD8A46&
      Caption         =   "PostOp:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   6120
      TabIndex        =   30
      Top             =   4920
      Width           =   2805
   End
   Begin VB.Label lblDate 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Surgery:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   345
      Left            =   120
      TabIndex        =   29
      Top             =   4080
      Width           =   3285
   End
   Begin VB.Label lblPrec 
      BackColor       =   &H00404000&
      BackStyle       =   0  'Transparent
      Caption         =   "PreCert:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   345
      Left            =   6120
      TabIndex        =   28
      Top             =   240
      Width           =   2805
   End
   Begin VB.Label lblLoc2 
      BackColor       =   &H00AD8A46&
      Caption         =   "Loc:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   6120
      TabIndex        =   27
      Top             =   5280
      Width           =   2805
   End
   Begin VB.Label lblNotes 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Notes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   3840
      TabIndex        =   26
      Top             =   5640
      Width           =   525
   End
   Begin VB.Label lblORMedSup 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "OR Meds/Supplies"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9000
      TabIndex        =   22
      Top             =   3840
      Width           =   1725
   End
   Begin VB.Label lblMedClr 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Medical Clearance"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9000
      TabIndex        =   20
      Top             =   3000
      Width           =   1785
   End
   Begin VB.Label lblTrans 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Transportation"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9000
      TabIndex        =   18
      Top             =   4680
      Width           =   1380
   End
   Begin VB.Label lblAType 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Anesthesia"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   6120
      TabIndex        =   16
      Top             =   3120
      Width           =   1035
   End
   Begin VB.Label lblIOLb 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "IOL Backup"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   3360
      TabIndex        =   14
      Top             =   3480
      Width           =   1095
   End
   Begin VB.Label lblResource2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Surgical Assistant"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   6120
      TabIndex        =   13
      Top             =   2280
      Width           =   1680
   End
   Begin VB.Label lblResource1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Assisting Surgeon"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   6120
      TabIndex        =   12
      Top             =   1440
      Width           =   1695
   End
   Begin VB.Label lblPreMeds 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Pre-Op Meds"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9000
      TabIndex        =   11
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label lblORIns 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "OR Instruments"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   9000
      TabIndex        =   6
      Top             =   1920
      Width           =   1440
   End
   Begin VB.Label lblResource0 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Supervising Surgeon"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   6120
      TabIndex        =   5
      Top             =   600
      Width           =   1995
   End
   Begin VB.Label lblHeader 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Setup Surgical Requests"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   2835
   End
End
Attribute VB_Name = "frmSurgical"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ClinicalId As Long
Public PatientId As Long
Public AppointmentId As Long
Public FormOn As Boolean
Public TheLoc As String
Public TheDate As String
Public TheProcs As String
Public TheEye As String
Private ReplaceOn As Boolean
Private DrawFiles(20) As String
Private DrawFilesCln(20) As Long
Private CurrentImageIdx As Integer
Public patientRunsUsed  As Long
Dim pat As New Patient

Private Type TestDetails
    TestApptId As Long
    TestDate As String
    TestTime As String
    TestParty As String
    TestText As String
    TestQuestion As String
    TestOrderNum As String
    TestFollowupNum As String
    TestMove As Boolean
    TestNoteOn As Boolean
    TestHightlightOn As Boolean
    TestFollowupOn As Boolean
    TestPrintOrder As Integer
    TestType As String * 1
End Type
Private Const MaxTestsAllowed As Integer = 70
Private TestResultsIndex As Integer
Private TestResultsStorage(MaxTestsAllowed) As TestDetails
Private CurrentTestId As Integer
Private CurrentTestNum(5) As Integer
Private OtherTestsToInclude(5, 4) As Long

Private Sub cmdDone_Click()
If (AppointmentId > 0) And (PatientId > 0) Then
    Call PostSurgical
End If
Unload frmSurgical
FormOn = False
End Sub

Private Sub cmdHome_Click()
Unload frmSurgical
FormOn = False
End Sub

Private Sub cmdDocs_Click()
If (PatientId > 0) Then
    frmScan.PatientId = PatientId
    frmScan.AppointmentId = 0
    If (frmScan.LoadScan) Then
        frmScan.Show 1
    End If
End If
End Sub

Private Sub cmdNextPic_Click()
Call LoadNextImage(CurrentImageIdx)
End Sub

Private Sub cmdNotes_Click()
 If Not UserLogin.HasPermission(epPatientNotes) Then
            frmEventMsgs.Header = "Not Permissioned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
            End If
If (PatientId > 0) Then
    frmNotes.NoteId = 0
    frmNotes.SystemReference = ""
    frmNotes.CurrentAction = ""
    frmNotes.PatientId = PatientId
    frmNotes.AppointmentId = AppointmentId
    frmNotes.SetTo = "S"
    frmNotes.MaintainOn = True
    If (frmNotes.LoadNotes) Then
        frmNotes.Show 1
    End If
End If
End Sub

Private Sub cmdReview_Click()
 If Not UserLogin.HasPermission(epEditPreviousVisit) Then
            frmEventMsgs.Header = "Not Permissioned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
            End If
            patientRunsUsed = pat.GetPatientAccess(PatientId)
If patientRunsUsed = -1 Then
            frmEventMsgs.Header = "Accessed Blocked"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
    End If
If (globalExam.iActivityId > 0) Then
    frmSystems1.TheStationId = 999
    frmSystems1.EditPreviousOn = True
    frmSurgical.Enabled = False
    DoEvents
    If (frmSystems1.LoadSummaryInfo(True)) Then
      frmSystems1.FormOn = True
        frmSystems1.Show 1
        frmSurgical.ZOrder 0
        If patientRunsUsed = 1 Then
        Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
        End If
    End If
    frmSurgical.Enabled = True
End If
End Sub

Public Function LoadSurgical(IName As String, iLoc As String, IDate As String, IProcs As String, AEye As String) As Boolean
Dim i As Integer
Dim Temp As String
Dim PatName As String
Dim TheText As String
Dim TheOrder As String
Dim DisplayText As String
Dim RetPat As Patient
LoadSurgical = False
TheLoc = iLoc
TheDate = IDate
TheProcs = IProcs
TheEye = AEye
CurrentImageIdx = 1
cmdReview.Visible = False
lblPrec.Visible = False
lblDate.Visible = False
lblTime.Visible = False
lblLoc1.Visible = False
lblLoc2.Visible = False
lblNotes.Visible = False
txtNotes.Visible = False
lblPO.Visible = False
txtPO.Visible = False
lblAim.Visible = False
txtAim.Visible = False
lblHeader.Caption = "Setup Surgical Requests"
If (AppointmentId > 0) And (PatientId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatientId
    If (RetPat.RetrievePatient) Then
        PatName = Trim(RetPat.FirstName) + " " + Trim(RetPat.LastName)
    End If
    Set RetPat = Nothing
    lblHeader.Caption = "Setup " + Trim(IProcs) + " for " + Trim(PatName)
    lblSurgReq.Visible = False
    lstSurgReq.Visible = False
    cmdReview.Visible = True
    lblDate.Visible = True
    lblDate.Caption = "Surgery: " + IDate
    lblLoc1.Visible = True
    lblLoc1.Caption = "Loc: " + iLoc
    lblTime.Visible = True
    lblLoc2.Visible = True
    lblLoc2.Caption = "Loc: "
    lblPrec.Visible = True
    lblPO.Visible = True
    txtPO.Visible = True
    lblAim.Visible = True
    txtAim.Visible = True
    lblNotes.Visible = True
    txtNotes.Visible = True
End If
Call ApplLoadCodes("SurgicalRequests", False, True, lstSurgReq)
Call ApplLoadCodes("SurgicalResources", False, True, lstResource0)
Call ApplLoadCodes("SurgicalResources", False, True, lstResource1)
Call ApplLoadCodes("SurgicalResources", False, True, lstResource2)

Call ApplLoadCodes("SurgicalPreOpMeds", False, True, lstPreMeds)
Call ApplLoadCodes("SurgicalMedicalClearance", False, True, lstMedClr)
Call ApplLoadCodes("SurgicalInstrumentation", False, True, lstORIns)
Call ApplLoadCodes("SurgicalMedsAndSupplies", False, True, lstORMedSup)
Call ApplLoadCodes("SurgicalTransportation", False, True, lstTrans)
Call ApplLoadCodes("SurgicalAnesthesiaType", False, True, lstAType)

Call ApplLoadCodesA("SurgicalIOL", False, True, lstIOL)
Call ApplLoadCodesA("SurgicalIOL", False, True, lstIOLb)
Call ApplLoadCodesA("SurgeryCaseType", False, True, lstCT)

' Load Tests
'32A AScan A SCAN BIOMETRY
'12A AKero, AUTOKERATOMETRY
'91B IOLM, IOL MASTER
'11A Kero, KERATOMETRY
'13A MRX, MANIFEST REFRACTION
Call BuildTestResults(PatientId, AppointmentId, 1, False, False)
i = 1
While (AccessTestResults(i, TheText, False, False))
    Call AccessTestOrder(i, TheOrder)
    If (TheOrder = "32A") Then
        lblAScan.Caption = TheText
    ElseIf (TheOrder = "12A") Then
        lblAKero.Caption = TheText
    ElseIf (TheOrder = "11A") Then
        lblKero.Caption = TheText
    ElseIf (TheOrder = "13A") Then
        lblMRX.Caption = TheText
    ElseIf (TheOrder = "91B") Then
        lblIOLM.Caption = TheText
    End If
    i = i + 1
Wend

' Load Draw
Call SetDrawing(AppointmentId)

If (Trim(IName) <> "") Or ((AppointmentId > 0) And (PatientId > 0)) Then
    TheEye = AEye
    LoadSurgical = True
    If (ClinicalId > 0) Then
        Call SetupSurgical(ClinicalId)
        If (Trim(Left(IName, 50)) <> "") Then
            LoadSurgical = LoadList(Trim(Left(IName, 50)), ReplaceOn)
            Call ApplLoadCodes("SurgicalRequests", False, True, lstSurgReq)
        End If
    Else
        LoadSurgical = LoadList(Trim(Left(IName, 50)), ReplaceOn)
        Call ApplLoadCodes("SurgicalRequests", False, True, lstSurgReq)
    End If
    lblAType.Visible = True
    lstAType.Visible = True
    lblResource0.Visible = True
    lstResource0.Visible = True
    lblResource1.Visible = True
    lstResource1.Visible = True
    lblResource2.Visible = True
    lstResource2.Visible = True
    lblPreMeds.Visible = True
    lstPreMeds.Visible = True
    lblORIns.Visible = True
    lstORIns.Visible = True
    lblTrans.Visible = True
    lstTrans.Visible = True
    lblORMedSup.Visible = True
    lstORMedSup.Visible = True
    lblMedClr.Visible = True
    lstMedClr.Visible = True
    lblIOL.Visible = True
    lstIOL.Visible = True
    lblIOLb.Visible = True
    lstIOLb.Visible = True
    cmdDocs.Visible = True
    cmdNotes.Visible = True
Else
    lblAType.Visible = False
    lstAType.Visible = False
    lblResource0.Visible = False
    lstResource0.Visible = False
    lblResource1.Visible = False
    lstResource1.Visible = False
    lblResource2.Visible = False
    lstResource2.Visible = False
    lblPreMeds.Visible = False
    lstPreMeds.Visible = False
    lblORIns.Visible = False
    lstORIns.Visible = False
    lblTrans.Visible = False
    lstTrans.Visible = False
    lblORMedSup.Visible = False
    lstORMedSup.Visible = False
    lblMedClr.Visible = False
    lstMedClr.Visible = False
    lblIOL.Visible = False
    lstIOL.Visible = False
    lblIOLb.Visible = False
    lstIOLb.Visible = False
    cmdDocs.Visible = False
    cmdNotes.Visible = False
    LoadSurgical = True
End If
End Function

Private Sub cmdSurgReq_Click()
Dim TheReq As String
Call frmSelectDialogue.BuildSelectionDialogue("SurgicalRequests")
frmSelectDialogue.Show 1
If (frmSelectDialogue.SelectionResult) Then
    TheReq = Trim(Left(frmSelectDialogue.Selection, 50))
    If (Trim(TheReq) <> "") Then
        Call PostSurgical
        ReplaceOn = False
        Call LoadSurgical(TheReq, TheLoc, TheDate, TheProcs, TheEye)
    End If
End If
End Sub

Private Sub Form_Load()
ReplaceOn = True
Erase CurrentTestNum
Erase OtherTestsToInclude
Call LoadSurgical("", TheLoc, TheDate, TheProcs, TheEye)
End Sub

Private Sub FxImage1_Click()
Dim ClnId As Long
ClnId = Val(Trim(FXImage1.Tag))
If (ClnId > 0) Then
    If (frmViewDraw.LoadDraw(ClnId, FXImage1.FileName)) Then
        frmViewDraw.Show 1
    End If
End If
End Sub

Private Function LoadList(TheName As String, MyReplace As Boolean) As Boolean
Dim i As Integer, j As Integer
Dim DisplayText As String
Dim Temp As String, BoxId As String
Dim ByPassP As Boolean
Dim ByPassU As Boolean
Dim ByPassT As Boolean
Dim ByPassA As Boolean
Dim ByPassO As Boolean
Dim ByPassM As Boolean
LoadList = False
ByPassP = False
ByPassU = False
ByPassT = False
ByPassA = False
ByPassO = False
ByPassM = False
lstSurgReq.Clear
LoadList = ApplLoadCodes(Trim(TheName), False, True, lstSurgReq)
For i = 0 To lstSurgReq.ListCount - 1
    BoxId = Mid(lstSurgReq.List(i), 63, 2)
    If (BoxId = "1S") Then
        If (lstResource0.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstResource0.ListCount - 1
                If (Trim(Left(lstResource0.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstResource0.ListIndex = j
                    lstResource0.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "2S") Then
        If (lstResource1.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstResource1.ListCount - 1
                If (Trim(Left(lstResource1.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstResource1.ListIndex = j
                    lstResource1.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "3S") Then
        If (lstResource2.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstResource2.ListCount - 1
                If (Trim(Left(lstResource2.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstResource2.ListIndex = j
                    lstResource2.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "CT") Then
        If (lstCT.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstCT.ListCount - 1
                If (Trim(Left(lstCT.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstCT.ListIndex = j
                End If
            Next j
        End If
    ElseIf (BoxId = "PM") Then
        If (lstPreMeds.SelCount < 1) Or (MyReplace) Or (ByPassP) Then
            ByPassP = True
            For j = 0 To lstPreMeds.ListCount - 1
                If (Trim(Left(lstPreMeds.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstPreMeds.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "MC") Then
        If (lstMedClr.SelCount < 1) Or (MyReplace) Or (ByPassM) Then
            ByPassM = True
            For j = 0 To lstMedClr.ListCount - 1
                If (Trim(Left(lstMedClr.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstMedClr.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "IO") Then
        If (lstIOL.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstIOL.ListCount - 1
                If (Trim(Left(lstIOL.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstIOL.ListIndex = j
                End If
            Next j
        End If
    ElseIf (BoxId = "IB") Then
        If (lstIOLb.ListIndex < 0) Or (MyReplace) Then
            For j = 0 To lstIOLb.ListCount - 1
                If (Trim(Left(lstIOLb.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstIOLb.ListIndex = j
                End If
            Next j
        End If
    ElseIf (BoxId = "OI") Then
        If (lstORIns.SelCount < 1) Or (MyReplace) Or (ByPassO) Then
            ByPassO = True
            For j = 0 To lstORIns.ListCount - 1
                If (Trim(Left(lstORIns.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstORIns.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "OM") Then
        If (lstORMedSup.SelCount < 1) Or (MyReplace) Or (ByPassU) Then
            ByPassU = True
            For j = 0 To lstORMedSup.ListCount - 1
                If (Trim(Left(lstORMedSup.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstORMedSup.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "TR") Then
        If (lstTrans.SelCount < 1) Or (MyReplace) Or (ByPassT) Then
            ByPassT = True
            For j = 0 To lstTrans.ListCount - 1
                If (Trim(Left(lstTrans.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstTrans.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "AT") Then
        If (lstAType.SelCount < 1) Or (MyReplace) Or (ByPassA) Then
            ByPassA = True
            For j = 0 To lstAType.ListCount - 1
                If (Trim(Left(lstAType.List(j), 62)) = Trim(Left(lstSurgReq.List(i), 62))) Then
                    lstAType.Selected(j) = True
                End If
            Next j
        End If
    ElseIf (BoxId = "P1") Then
        If (Trim(txtIOLP.Text) = "") Or (MyReplace) Then
            txtIOLP.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    ElseIf (BoxId = "P2") Then
        If (Trim(txtIOLbP.Text) = "") Or (MyReplace) Then
            txtIOLbP.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    ElseIf (BoxId = "IN") Then
        If (Trim(txtInc.Text) = "") Or (MyReplace) Then
            txtInc.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    ElseIf (BoxId = "AX") Then
        If (Trim(txtAxis.Text) = "") Or (MyReplace) Then
            txtAxis.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    ElseIf (BoxId = "AC") Then
        If (Trim(txtArc.Text) = "") Or (MyReplace) Then
            txtArc.Text = Trim(Left(lstSurgReq.List(i), 62))
        End If
    End If
Next i
LoadList = True
End Function

Private Function GetDoctor() As String
Dim i As Integer
Dim j As Integer
GetDoctor = ""
For i = 0 To lstResource0.ListCount - 1
    If (lstResource0.Selected(i)) Then
        j = InStrPS(lstResource0.List(i), ",")
        If (j > 0) Then
            GetDoctor = UCase(Trim(Left(lstResource0.List(i), j - 1)))
        End If
        Exit For
    End If
Next i
End Function

Private Function GetExpectedDate(TheRange As String) As String
Dim i As Integer
Dim DayAhead As Integer
Dim ADate As String
Dim NDate As String
Dim RetAppt As SchedulerAppointment
ADate = ""
NDate = ""
GetExpectedDate = ""
If (AppointmentId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = AppointmentId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ADate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
    End If
    Set RetAppt = Nothing
    If (ADate <> "") Then
        DayAhead = 7
        i = InStrPS(TheRange, "DAYS")
        If (i > 0) Then
            DayAhead = Val(Trim(Left(TheRange, i - 1)))
        End If
        i = InStrPS(TheRange, "WEEKS")
        If (i > 0) Then
            DayAhead = Val(Trim(Left(TheRange, i - 1))) * 7
        End If
        i = InStrPS(TheRange, "MONTHS")
        If (i > 0) Then
            DayAhead = Val(Trim(Left(TheRange, i - 1))) * 31
        End If
        i = InStrPS(TheRange, "YEAR")
        If (i > 0) Then
            DayAhead = Val(Trim(Left(TheRange, i - 1))) * 365
        End If
        Call AdjustDate(ADate, DayAhead, NDate)
        If (Trim(NDate) <> "") Then
            NDate = Mid(NDate, 7, 4) + Mid(NDate, 1, 2) + Mid(NDate, 4, 2)
        End If
        GetExpectedDate = NDate
    End If
End If
End Function

Private Function GetAppointmentDetails(TheId As Long, AWhich As Boolean) As Boolean
Dim RscId As Long
Dim PrecId As Long
Dim ADate As String
Dim ATime As String
Dim RetAppt As SchedulerAppointment
Dim RetRes As SchedulerResource
Dim RetPrc As PracticeName
Dim RetPre As PatientPreCerts
GetAppointmentDetails = False
If (TheId > 0) Then
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = TheId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ADate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
        Call ConvertMinutesToTime(RetAppt.AppointmentTime, ATime)
        RscId = RetAppt.AppointmentResourceId2
        PrecId = RetAppt.AppointmentPreCertId
    End If
    Set RetAppt = Nothing
    If (AWhich) Then
        lblDate.Caption = "Surgery: " + ADate + " " + ATime
        lblDate.Tag = Trim(str(TheId))
    Else
        lblTime.Caption = "PostOp: " + ADate + " " + ATime
        lblTime.Tag = Trim(str(TheId))
    End If
    If (RscId > 0) And (RscId < 1000) Then
        Set RetRes = New SchedulerResource
        RetRes.ResourceId = RscId
        If (RetRes.RetrieveSchedulerResource) Then
            If (AWhich) Then
                lblLoc1.Caption = "Loc:" + RetRes.ResourceDescription
            Else
                lblLoc2.Caption = "Loc:" + RetRes.ResourceDescription
            End If
        End If
        Set RetRes = Nothing
    ElseIf (RscId > 1000) Then
        Set RetPrc = New PracticeName
        RetPrc.PracticeId = RscId - 1000
        If (RetPrc.RetrievePracticeName) Then
            If (AWhich) Then
                lblLoc1.Caption = "Loc:" + RetPrc.PracticeLocationReference
            Else
                lblLoc2.Caption = "Loc:" + RetPrc.PracticeLocationReference
            End If
        End If
        Set RetPrc = Nothing
    Else
        If (AWhich) Then
            lblLoc1.Caption = "Loc:Office"
        Else
            lblLoc2.Caption = "Loc:Office"
        End If
    End If
    If (PrecId > 0) Then
        Set RetPre = New PatientPreCerts
        RetPre.PreCertsId = PrecId
        If (RetPre.RetrievePatientPreCerts) Then
            lblPrec.Caption = "Prec: " + Trim(RetPre.PreCerts)
        End If
        Set RetPre = Nothing
    End If
End If
End Function

Private Function SetDrawing(ApptId As Long) As Boolean
Dim m As Integer
Dim i As Integer, u As Integer
Dim Temp As String, ADate As String
Dim ADiag As String, ADraw As String
Dim MyFile As String, TheFile As String
Dim RetCln As PatientClinical
Dim RetAppt As SchedulerAppointment
On Error GoTo UI_ErrorHandler
SetDrawing = False
Erase DrawFiles
Erase DrawFilesCln
FXImage1.Tag = ""
Set FXImage1.picture = Nothing
lblDrawDate.Visible = False
lblDrawDate.Caption = ""
If (ApptId > 0) Then
    u = 0
    Set RetAppt = New SchedulerAppointment
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        ADate = RetAppt.AppointmentDate
        ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Mid(ADate, 1, 4)
    End If
    Set RetAppt = Nothing
    Set RetCln = New PatientClinical
    RetCln.AppointmentId = ApptId
    RetCln.ClinicalType = "Q"
    RetCln.ClinicalStatus = "A"
    If (RetCln.FindPatientClinical > 0) Then
        i = 1
        Do Until Not (RetCln.SelectPatientClinical(i))
            ADiag = Trim(RetCln.Findings)
            ADraw = Trim(RetCln.ClinicalDraw)
            If (ADraw <> "") Then
                Temp = ADraw
                If (Left(Temp, 2) = "R-") Then
                    MyFile = MyImageDir + Mid(Temp, 3, Len(Temp) - 2)
                Else
                    MyFile = MyImageDir + "MyDraw-A" + Trim(str(ApptId)) + "*" + Temp + ".jpg"
                End If
                TheFile = Dir(MyFile)
                If (Trim(TheFile) <> "") Then
                    MyFile = MyImageDir + TheFile
                Else
                    MyFile = MyImageDir + "MyDraw-A" + Trim(str(ApptId)) + "*" + Temp + ".bmp"
                    TheFile = Dir(MyFile)
                    If (Trim(TheFile) <> "") Then
                        MyFile = MyImageDir + TheFile
                    Else
                        MyFile = ""
                    End If
                End If
                If (Trim(MyFile) <> "") Then
                    For m = 1 To u
                        If (DrawFiles(m) = MyFile) Then
                            m = -1
                            Exit For
                        End If
                    Next m
                    If (m <> -1) Then
                        u = u + 1
                        DrawFiles(u) = MyFile
                        DrawFilesCln(u) = RetCln.ClinicalId
                    End If
                End If
            End If
            i = i + 1
        Loop
        If (Trim(DrawFiles(1)) <> "") Then
            CurrentImageIdx = 1
            MyFile = DrawFiles(CurrentImageIdx)
            If (InStrPS(MyFile, ".bmp") = 0) Then
                FXImage1.FileName = FM.GetPathForDirectIO(MyFile)
            Else
                FXImage1.picture = FM.LoadPicture(MyFile)
            End If
            FXImage1.Tag = Trim(str(DrawFilesCln(CurrentImageIdx)))
            lblDrawDate.Caption = ADate
            lblDrawDate.Visible = True
        End If
    End If
    Set RetCln = Nothing
End If
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Private Sub LoadNextImage(MyIdx As Integer)
Dim MyFile As String
CurrentImageIdx = CurrentImageIdx + 1
If (Trim(DrawFiles(CurrentImageIdx)) = "") Then
    CurrentImageIdx = 1
End If
If (Trim(DrawFiles(CurrentImageIdx)) <> "") Then
    MyFile = DrawFiles(CurrentImageIdx)
    If (InStrPS(MyFile, ".bmp") = 0) Then
        FXImage1.FileName = FM.GetPathForDirectIO(MyFile)
    Else
        FXImage1.picture = FM.LoadPicture(MyFile)
    End If
    FXImage1.Tag = Trim(str(DrawFilesCln(CurrentImageIdx)))
End If
End Sub

Private Function PostSurgical() As Boolean
Dim i As Integer
Dim z As Integer
Dim NoteId As Long
Dim RType As String
Dim RValue As String
Dim RetNote As PatientNotes
Dim RetSCln As PatientClinicalSurgeryPlan
PostSurgical = True
GoSub CleanOldRecords
If (lstResource0.ListIndex >= 0) Then
    RType = "S"
    RValue = Trim(Left(lstResource0.List(lstResource0.ListIndex), 62))
    GoSub PostRecord
End If
If (lstResource1.ListIndex >= 0) Then
    RType = "A"
    RValue = Trim(Left(lstResource1.List(lstResource1.ListIndex), 62))
    GoSub PostRecord
End If
If (lstResource2.ListIndex >= 0) Then
    RType = "O"
    RValue = Trim(Left(lstResource2.List(lstResource2.ListIndex), 62))
    GoSub PostRecord
End If
If (lstIOL.ListIndex >= 0) And (lstIOL.Text <> "") Then
    RType = "C"
    RValue = Trim(Left(lstIOL.List(lstIOL.ListIndex), 62))
    RValue = RValue + "Power:" + Trim(txtIOLP.Text)
    GoSub PostRecord
End If
If (lstIOLb.ListIndex >= 0) And (lstIOLb.Text <> "") Then
    RType = "B"
    RValue = Trim(Left(lstIOLb.List(lstIOLb.ListIndex), 62))
    RValue = RValue + "Power:" + Trim(txtIOLbP.Text)
    GoSub PostRecord
End If
If (lstCT.ListIndex >= 0) And (lstCT.Text <> "") Then
    RType = "Q"
    RValue = Trim(Left(lstCT.List(lstCT.ListIndex), 62))
    GoSub PostRecord
End If
If (lstAType.ListIndex >= 0) Then
    RType = "N"
    RValue = Trim(Left(lstAType.List(lstAType.ListIndex), 62))
    GoSub PostRecord
End If
If (lstTrans.ListIndex >= 0) Then
    RType = "T"
    RValue = Trim(Left(lstTrans.List(lstTrans.ListIndex), 62))
    GoSub PostRecord
End If
For i = 0 To lstPreMeds.ListCount - 1
    If (lstPreMeds.Selected(i)) Then
        RType = "M"
        RValue = Trim(Left(lstPreMeds.List(i), 62))
        GoSub PostRecord
    End If
Next i
For i = 0 To lstORIns.ListCount - 1
    If (lstORIns.Selected(i)) Then
        RType = "I"
        RValue = Trim(Left(lstORIns.List(i), 62))
        GoSub PostRecord
    End If
Next i
For i = 0 To lstMedClr.ListCount - 1
    If (lstMedClr.Selected(i)) Then
        RType = "Y"
        RValue = Trim(Left(lstMedClr.List(i), 62))
        GoSub PostRecord
    End If
Next i
For i = 0 To lstORMedSup.ListCount - 1
    If (lstORMedSup.Selected(i)) Then
        RType = "Z"
        RValue = Trim(Left(lstORMedSup.List(i), 62))
        GoSub PostRecord
    End If
Next i
If (Trim(txtPO.Text) <> "") Then
    RType = "P"
    RValue = Trim(txtPO.Text)
    GoSub PostRecord
End If
RValue = ""
If (Trim(txtInc.Text) <> "") Then
    RValue = "Inc:" + Trim(txtInc.Text) + " "
End If
If (Trim(txtAxis.Text) <> "") Then
    RValue = RValue + "Axis:" + Trim(txtAxis.Text) + " "
End If
If (Trim(txtArc.Text) <> "") Then
    RValue = RValue + "Arc:" + Trim(txtArc.Text)
End If
If (Trim(RValue) <> "") Then
    RType = "D"
    GoSub PostRecord
End If
If (Trim(txtAim.Text) <> "") Then
    RType = "E"
    RValue = Trim(txtAim.Text)
    GoSub PostRecord
End If
If (Trim(lblDate.Tag) <> "") Then
    RType = "F"
    RValue = Trim(lblDate.Tag)
    GoSub PostRecord
End If
If (Trim(lblTime.Tag) <> "") Then
    RType = "G"
    RValue = Trim(lblTime.Tag)
    GoSub PostRecord
End If
' Post older Tests that are to be included as part of the SR
For i = 1 To 5
    For z = 1 To 4
        If (OtherTestsToInclude(i, z) > 0) Then
            RType = Trim(str(i))
            RValue = Trim(str(OtherTestsToInclude(i, z)))
            GoSub PostRecord
        End If
    Next z
Next i
If (Trim(txtNotes.Text) <> "") Then
    If (Trim(txtNotes.Tag) <> "") Then
        If (Val(Trim(txtNotes.Tag)) > 0) Then
            Set RetNote = New PatientNotes
            RetNote.NotesId = Val(Trim(txtNotes.Tag))
            If (RetNote.RetrieveNotes) Then
                Call RetNote.DeleteNotes
            End If
            Set RetNote = Nothing
        End If
    End If
    Set RetNote = New PatientNotes
    RetNote.NotesId = 0
    If (RetNote.RetrieveNotes) Then
        RetNote.NotesAppointmentId = AppointmentId
        RetNote.NotesPatientId = PatientId
        RetNote.NotesCategory = ""
        RetNote.NotesAudioOn = False
        RetNote.NotesCommentOn = False
        RetNote.NotesClaimOn = False
        RetNote.NotesDate = ""
        RetNote.NotesEye = TheEye
        RetNote.NotesHighlight = ""
        RetNote.NotesILPNRef = ""
        RetNote.NotesSystem = ""
        RetNote.NotesOffDate = ""
        RetNote.NotesUser = UserLogin.iId
        RetNote.NotesType = "S"
        RetNote.NotesText1 = Left(txtNotes.Text, 250)
        If (Len(txtNotes.Text) > 250) Then
            RetNote.NotesText2 = Mid(txtNotes.Text, 251, 250)
            If (Len(txtNotes.Text) > 511) Then
                RetNote.NotesText3 = Mid(txtNotes.Text, 511, 250)
                If (Len(txtNotes.Text) > 761) Then
                    RetNote.NotesText4 = Mid(txtNotes.Text, 761, 250)
                End If
            End If
        End If
        Call RetNote.ApplyNotes
    End If
    Set RetNote = Nothing
End If
Exit Function
CleanOldRecords:
    If (ClinicalId > 0) Then
        Set RetSCln = New PatientClinicalSurgeryPlan
        RetSCln.SurgeryClnId = ClinicalId
        RetSCln.SurgeryRefType = ""
        If (RetSCln.FindPatientClinicalPlan > 0) Then
            z = 1
            While (RetSCln.SelectPatientClinicalPlan(z))
                Call RetSCln.KillPatientClinicalPlan
                z = z + 1
            Wend
        End If
        Set RetSCln = Nothing
    End If
    Return
PostRecord:
    If (Trim(RType) <> "") And (Trim(RValue) <> "") Then
        Set RetSCln = New PatientClinicalSurgeryPlan
        RetSCln.SurgeryId = 0
        If (RetSCln.RetrievePatientClinicalPlan) Then
            RetSCln.SurgeryClnId = ClinicalId
            RetSCln.SurgeryRefType = RType
            RetSCln.SurgeryValue = RValue
            RetSCln.SurgeryStatus = "A"
            Call RetSCln.ApplyPatientClinicalPlan
        End If
        Set RetSCln = Nothing
    End If
    Return
End Function

Private Function SetupSurgical(ClnId As Long) As Boolean
Dim i As Integer, z As Integer
Dim p As Integer, q As Integer
Dim RValue As String
Dim RetNote As PatientNotes
Dim RetSCln As PatientClinicalSurgeryPlan
SetupSurgical = True
If (ClinicalId > 0) Then
    Set RetSCln = New PatientClinicalSurgeryPlan
    RetSCln.SurgeryClnId = ClinicalId
    RetSCln.SurgeryRefType = ""
    RetSCln.SurgeryStatus = "A"
    If (RetSCln.FindPatientClinicalPlan > 0) Then
        z = 1
        While (RetSCln.SelectPatientClinicalPlan(z))
            If (RetSCln.SurgeryRefType = "S") Then
                For i = 0 To lstResource0.ListCount - 1
                    If (UCase(Trim(Left(lstResource0.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
'                        lstResource0.ListIndex = i
                        lstResource0.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "A") Then
                For i = 0 To lstResource1.ListCount - 1
                    If (UCase(Trim(Left(lstResource1.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
'                        lstResource1.ListIndex = i
                        lstResource1.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "O") Then
                For i = 0 To lstResource2.ListCount - 1
                    If (UCase(Trim(Left(lstResource2.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
'                        lstResource2.ListIndex = i
                        lstResource2.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "Q") Then
                For i = 0 To lstCT.ListCount - 1
                    If (UCase(Trim(Left(lstCT.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstCT.ListIndex = i
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "C") Then
                p = InStrPS(RetSCln.SurgeryValue, "POWER:")
                If (p > 0) Then
                    RValue = Trim(Left(RetSCln.SurgeryValue, p - 1))
                    For i = 0 To lstIOL.ListCount - 1
                        If (UCase(Trim(Left(lstIOL.List(i), 62))) = Trim(RValue)) Then
                            lstIOL.ListIndex = i
                            Exit For
                        End If
                    Next i
                    RValue = Trim(Mid(RetSCln.SurgeryValue, p + 6, Len(RetSCln.SurgeryValue) - (p + 5)))
                    txtIOLP.Text = RValue
                Else
                    For i = 0 To lstIOL.ListCount - 1
                        If (UCase(Trim(Left(lstIOL.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                            lstIOL.ListIndex = i
                            Exit For
                        End If
                    Next i
                    txtIOLP.Text = ""
                End If
            ElseIf (RetSCln.SurgeryRefType = "B") Then
                p = InStrPS(RetSCln.SurgeryValue, "POWER:")
                If (p > 0) Then
                    RValue = Trim(Left(RetSCln.SurgeryValue, p - 1))
                    For i = 0 To lstIOLb.ListCount - 1
                        If (UCase(Trim(Left(lstIOLb.List(i), 62))) = Trim(RValue)) Then
                            lstIOLb.ListIndex = i
                            Exit For
                        End If
                    Next i
                    RValue = Trim(Mid(RetSCln.SurgeryValue, p + 6, Len(RetSCln.SurgeryValue) - (p + 5)))
                    txtIOLbP.Text = RValue
                Else
                    For i = 0 To lstIOLb.ListCount - 1
                        If (UCase(Trim(Left(lstIOLb.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                            lstIOLb.ListIndex = i
                            Exit For
                        End If
                    Next i
                    txtIOLbP.Text = ""
                End If
            ElseIf (RetSCln.SurgeryRefType = "N") Then
                For i = 0 To lstAType.ListCount - 1
                    If (UCase(Trim(Left(lstAType.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstAType.ListIndex = i
                        lstAType.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "T") Then
                For i = 0 To lstTrans.ListCount - 1
                    If (UCase(Trim(Left(lstTrans.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstTrans.ListIndex = i
                        lstTrans.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "M") Then
                For i = 0 To lstPreMeds.ListCount - 1
                    If (UCase(Trim(Left(lstPreMeds.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstPreMeds.ListIndex = i
                        lstPreMeds.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "I") Then
                For i = 0 To lstORIns.ListCount - 1
                    If (UCase(Trim(Left(lstORIns.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstORIns.ListIndex = i
                        lstORIns.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "Y") Then
                For i = 0 To lstMedClr.ListCount - 1
                    If (UCase(Trim(Left(lstMedClr.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstMedClr.ListIndex = i
                        lstMedClr.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "Z") Then
                For i = 0 To lstORMedSup.ListCount - 1
                    If (UCase(Trim(Left(lstORMedSup.List(i), 62))) = Trim(RetSCln.SurgeryValue)) Then
                        lstORMedSup.ListIndex = i
                        lstORMedSup.Selected(i) = True
                        Exit For
                    End If
                Next i
            ElseIf (RetSCln.SurgeryRefType = "P") Then
                txtPO.Text = Trim(RetSCln.SurgeryValue)
            ElseIf (RetSCln.SurgeryRefType = "F") Then
                Call GetAppointmentDetails(Val(Trim(RetSCln.SurgeryValue)), True)
            ElseIf (RetSCln.SurgeryRefType = "G") Then
                Call GetAppointmentDetails(Val(Trim(RetSCln.SurgeryValue)), False)
            ElseIf (RetSCln.SurgeryRefType = "E") Then
                txtAim.Text = Trim(RetSCln.SurgeryValue)
            ElseIf (RetSCln.SurgeryRefType = "D") Then
                p = InStrPS(RetSCln.SurgeryValue, "AXIS:")
                If (p > 0) Then
                    q = InStrPS(p, RetSCln.SurgeryValue, "ARC:")
                    If (q = 0) Then
                        q = Len(RetSCln.SurgeryValue)
                    End If
                    txtAxis.Text = Trim(Mid(RetSCln.SurgeryValue, p + 5, (q - 1) - (p + 4)))
                End If
                p = InStrPS(RetSCln.SurgeryValue, "INC:")
                If (p > 0) Then
                    q = InStrPS(p, RetSCln.SurgeryValue, "AXIS:")
                    If (q = 0) Then
                        q = InStrPS(p, RetSCln.SurgeryValue, "ARC:")
                        If (q = 0) Then
                            q = Len(RetSCln.SurgeryValue)
                        End If
                    End If
                    txtInc.Text = Trim(Mid(RetSCln.SurgeryValue, p + 4, (q - 1) - (p + 3)))
                End If
                p = InStrPS(RetSCln.SurgeryValue, "ARC:")
                If (p > 0) Then
                    q = Len(RetSCln.SurgeryValue) + 1
                    txtArc.Text = Trim(Mid(RetSCln.SurgeryValue, p + 4, (q - 1) - (p + 3)))
                End If
            ElseIf (RetSCln.SurgeryRefType = "1") Then
                p = 1
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = Val(Trim(RetSCln.SurgeryValue))
                End If
            ElseIf (RetSCln.SurgeryRefType = "2") Then
                p = 2
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = Val(Trim(RetSCln.SurgeryValue))
                End If
            ElseIf (RetSCln.SurgeryRefType = "3") Then
                p = 3
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = Val(Trim(RetSCln.SurgeryValue))
                End If
            ElseIf (RetSCln.SurgeryRefType = "4") Then
                p = 4
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = Val(Trim(RetSCln.SurgeryValue))
                End If
            ElseIf (RetSCln.SurgeryRefType = "5") Then
                p = 5
                If (CurrentTestNum(p) < 4) Then
                    CurrentTestNum(p) = CurrentTestNum(p) + 1
                    OtherTestsToInclude(p, CurrentTestNum(p)) = Val(Trim(RetSCln.SurgeryValue))
                End If
            End If
            z = z + 1
        Wend
    End If
    Set RetSCln = Nothing
' Get The Surgery Notes
    txtNotes.Text = ""
    txtNotes.Tag = ""
    Set RetNote = New PatientNotes
    RetNote.NotesPatientId = PatientId
    RetNote.NotesAppointmentId = AppointmentId
    RetNote.NotesType = "S"
    If (RetNote.FindNotes > 0) Then
        If (RetNote.SelectNotes(1)) Then
            txtNotes.Text = Trim(txtNotes.Text) _
                          + " " + Trim(RetNote.NotesText1) _
                          + " " + Trim(RetNote.NotesText2) _
                          + " " + Trim(RetNote.NotesText3) _
                          + " " + Trim(RetNote.NotesText4)
            txtNotes.Tag = Trim(str(RetNote.NotesId))
        End If
    End If
    Set RetNote = Nothing
End If
End Function

Private Function ApplLoadCodes(CodeType As String, AllOn As Boolean, BothFields As Boolean, AList As ListBox) As Boolean
Dim k As Integer
Dim DisplayText As String
Dim RetrieveCode As PracticeCodes
ApplLoadCodes = False
Set RetrieveCode = New PracticeCodes
AList.Clear
RetrieveCode.ReferenceType = Trim(CodeType)
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        DisplayText = Space(512)
        If (BothFields) Then
            Mid(DisplayText, 1, Len(Trim(RetrieveCode.ReferenceCode))) = Trim(RetrieveCode.ReferenceCode)
            Mid(DisplayText, 65, Len(Trim(RetrieveCode.ReferenceAlternateCode))) = Trim(RetrieveCode.ReferenceAlternateCode)
            Mid(DisplayText, 128, Len(Trim(RetrieveCode.LetterLanguage))) = Trim(RetrieveCode.LetterLanguage)
            Mid(DisplayText, 240, 7) = Trim(str(RetrieveCode.Rank))
            Mid(DisplayText, 248, 1) = " "
            If (RetrieveCode.FollowUp = "F") Then
                Mid(DisplayText, 248, 1) = "F"
            End If
            Mid(DisplayText, 249, Len(Trim(RetrieveCode.TestOrder))) = Trim(RetrieveCode.TestOrder)
            Mid(DisplayText, 254, 1) = "F"
            If (RetrieveCode.Signature = "T") Then
                Mid(DisplayText, 254, 1) = "T"
            End If
            Mid(DisplayText, 255, 1) = "F"
            If (RetrieveCode.Exclusion = "T") Then
                Mid(DisplayText, 255, 1) = "T"
            End If
            Mid(DisplayText, 256, Len(Trim(RetrieveCode.LetterLanguage1))) = Trim(RetrieveCode.LetterLanguage1)
        Else
            Mid(DisplayText, 1, Len(Trim(RetrieveCode.ReferenceAlternateCode))) = Trim(RetrieveCode.ReferenceAlternateCode)
        End If
        AList.AddItem DisplayText
        k = k + 1
    Loop
    ApplLoadCodes = True
End If
Set RetrieveCode = Nothing
End Function

Private Function ApplLoadCodesA(CodeType As String, AllOn As Boolean, BothFields As Boolean, AList As ComboBox) As Boolean
Dim k As Integer
Dim DisplayText As String
Dim RetrieveCode As PracticeCodes
ApplLoadCodesA = False
Set RetrieveCode = New PracticeCodes
AList.Clear
RetrieveCode.ReferenceType = Trim(CodeType)
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        DisplayText = Space(512)
        If (BothFields) Then
            Mid(DisplayText, 1, Len(Trim(RetrieveCode.ReferenceCode))) = Trim(RetrieveCode.ReferenceCode)
            Mid(DisplayText, 65, Len(Trim(RetrieveCode.ReferenceAlternateCode))) = Trim(RetrieveCode.ReferenceAlternateCode)
            Mid(DisplayText, 128, Len(Trim(RetrieveCode.LetterLanguage))) = Trim(RetrieveCode.LetterLanguage)
            Mid(DisplayText, 240, 7) = Trim(str(RetrieveCode.Rank))
            Mid(DisplayText, 248, 1) = " "
            If (RetrieveCode.FollowUp = "F") Then
                Mid(DisplayText, 248, 1) = "F"
            End If
            Mid(DisplayText, 249, Len(Trim(RetrieveCode.TestOrder))) = Trim(RetrieveCode.TestOrder)
            Mid(DisplayText, 254, 1) = "F"
            If (RetrieveCode.Signature = "T") Then
                Mid(DisplayText, 254, 1) = "T"
            End If
            Mid(DisplayText, 255, 1) = "F"
            If (RetrieveCode.Exclusion = "T") Then
                Mid(DisplayText, 255, 1) = "T"
            End If
            Mid(DisplayText, 256, Len(Trim(RetrieveCode.LetterLanguage1))) = Trim(RetrieveCode.LetterLanguage1)
        Else
            Mid(DisplayText, 1, Len(Trim(RetrieveCode.ReferenceAlternateCode))) = Trim(RetrieveCode.ReferenceAlternateCode)
        End If
        AList.AddItem DisplayText
        k = k + 1
    Loop
    ApplLoadCodesA = True
End If
Set RetrieveCode = Nothing
End Function

Private Sub lblAKero_Click()
CurrentTestId = 1
Call SetupHistoricalTests("12A")
End Sub

Private Sub lblAScan_Click()
CurrentTestId = 2
Call SetupHistoricalTests("32A")
End Sub

Private Sub lblIOLM_Click()
CurrentTestId = 3
Call SetupHistoricalTests("91B")
End Sub

Private Sub lblKero_Click()
CurrentTestId = 4
Call SetupHistoricalTests("11A")
End Sub

Private Sub lblMRX_Click()
CurrentTestId = 5
Call SetupHistoricalTests("13A")
End Sub

Private Sub lstAllTests_Click()
Dim i As Integer
If (lstAllTests.ListIndex > 0) Then
    If (CurrentTestNum(CurrentTestId) + 1 > 4) Then
        frmEventMsgs.Header = "You may retain up to 4 past tests ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Clear All Past Test"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 2) Then
            For i = 1 To 4
                OtherTestsToInclude(CurrentTestId, CurrentTestNum(CurrentTestId)) = 0
            Next i
            CurrentTestNum(CurrentTestId) = 0
        End If
    Else
        For i = 1 To 4
            If (OtherTestsToInclude(CurrentTestId, i) = lstAllTests.ItemData(lstAllTests.ListIndex)) Then
                Exit Sub
            End If
        Next i
        CurrentTestNum(CurrentTestId) = CurrentTestNum(CurrentTestId) + 1
        OtherTestsToInclude(CurrentTestId, CurrentTestNum(CurrentTestId)) = lstAllTests.ItemData(lstAllTests.ListIndex)
    End If
ElseIf (lstAllTests.ListIndex = 0) Then
    lstAllTests.Clear
    lstAllTests.Visible = False
End If
End Sub

Private Sub lstCT_Click()
If (lstCT.ListIndex >= 0) Then
    SendKeys "{Home}"
End If
End Sub

Private Sub lstIOL_Click()
If (lstIOL.ListIndex >= 0) Then
    SendKeys "{Home}"
End If
End Sub

Private Sub lstIOLb_Click()
If (lstIOLb.ListIndex >= 0) Then
    SendKeys "{Home}"
End If
End Sub

Private Sub lstSurgReq_Click()
Dim TheReq As String
If (lstSurgReq.ListIndex >= 0) Then
    TheReq = Trim(lstSurgReq.List(lstSurgReq.ListIndex))
    Call LoadSurgical(TheReq, TheLoc, TheDate, TheProcs, TheEye)
End If
lblSurgReq.Visible = False
lstSurgReq.Visible = False
End Sub

Private Function BuildTestResults(PatId As Long, ApptId As Long, LtrTransOn As Integer, TimeOn As Boolean, LTestsOn As Boolean) As Boolean
Dim i As Integer
Dim MyAppt As Long
Dim ApptDate As String
Dim RetAppt As SchedulerAppointment
BuildTestResults = False
If (PatId > 0) Then
    If (AppointmentId < 1) Then
        AppointmentId = ApptId
    End If
    Erase TestResultsStorage
    TestResultsIndex = 0
    If (LTestsOn) Then
        Call SetTestFiles(PatId, LtrTransOn, TimeOn, LTestsOn)
        MyAppt = AppointmentId
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentId = AppointmentId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            ApptDate = RetAppt.AppointmentDate
        End If
        Set RetAppt = Nothing
'
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentPatientId = PatId
        RetAppt.AppointmentDate = ApptDate
        RetAppt.AppointmentStatus = ""
        If (RetAppt.FindAppointmentPreviousbyDate > 0) Then
            i = 1
            While (RetAppt.SelectAppointment(i))
                If (RetAppt.AppointmentStatus = "D") And (RetAppt.AppointmentId <> MyAppt) Then
                    AppointmentId = RetAppt.AppointmentId
                    Call SetTestFiles(PatId, LtrTransOn, TimeOn, LTestsOn)
                End If
                i = i + 1
            Wend
        End If
        AppointmentId = MyAppt
    Else
        Call SetTestFiles(PatId, LtrTransOn, TimeOn, LTestsOn)
    End If
    BuildTestResults = True
End If
End Function

Private Function AccessTestResults(TestIdx As Integer, TheText As String, ExcludePinhole As Boolean, ExcludeEmpty As Boolean) As Boolean
Dim i As Integer
Dim j As Integer
Dim ATemp As String
Dim BTemp As String
Dim Temp1 As String
Dim Temp2 As String
Dim RetNote As PatientNotes
TheText = ""
AccessTestResults = False
If (TestIdx > 0) Then
    If (Trim(TestResultsStorage(TestIdx).TestText) <> "") Then
        TheText = Trim(TestResultsStorage(TestIdx).TestText)
        If (ExcludePinhole) Then
            i = InStrPS(UCase(TheText), "PINHOLE")
            If (i > 0) Then
                TheText = Left(TheText, i - 1) + vbCrLf + Mid(TheText, i, Len(TheText) - (i - 1))
                i = i + 2
                For j = i - 1 To (i - 7)
                    If (Mid(TheText, j, 1) = " ") Then
                        Mid(TheText, j, 1) = "~"
                    End If
                Next j
                Call StripCharacters(TheText, "~")
                            
            End If
        End If
        If (Trim(TestResultsStorage(TestIdx).TestOrderNum) = "55A") Then
            i = InStrPS(TheText, "OS:")
            If (i > 0) Then
                Temp1 = Left(TheText, i - 1)
                Temp2 = Mid(TheText, i, Len(TheText) - (i - 1))
            Else
                Temp1 = TheText
                Temp2 = ""
            End If
            If (Trim(Temp1) <> "") Then
                i = InStrPS(Temp1, "Early phase")
                j = InStrPS(Temp1, "Late phase")
                If (i > 0) Then
                    If (j > 0) Then
                        ATemp = Mid(Temp1, i + 1, (j - 1) - i)
                        ATemp = Mid(Temp1, i, 1) + LCase(ATemp)
                    Else
                        ATemp = Mid(Temp1, i + 1, Len(Temp1) - i)
                        ATemp = Mid(Temp1, i, 1) + LCase(ATemp)
                    End If
                End If
                j = InStrPS(Temp1, "Late phase")
                If (j > 0) Then
                    BTemp = Mid(Temp1, j + 1, Len(Temp1) - j)
                    BTemp = Mid(Temp1, j, 1) + LCase(BTemp)
                Else
                    BTemp = ""
                End If
                If (i > 0) Then
                    Temp1 = Left(Temp1, i - 1) + ATemp + BTemp
                End If
                If (j > 0) Then
                    Temp1 = Left(Temp1, j - 1) + ATemp + BTemp
                End If
            End If
            If (Trim(Temp2) <> "") Then
                i = InStrPS(Temp2, "Early phase")
                j = InStrPS(Temp2, "Late phase")
                If (i > 0) Then
                    If (j > 0) Then
                        ATemp = Mid(Temp2, i + 1, (j - 1) - i)
                        ATemp = Mid(Temp2, i, 1) + LCase(ATemp)
                    Else
                        ATemp = Mid(Temp2, i + 1, Len(Temp2) - i)
                        ATemp = Mid(Temp2, i, 1) + LCase(ATemp)
                    End If
                End If
                j = InStrPS(Temp2, "Late phase")
                If (j > 0) Then
                    BTemp = Mid(Temp2, j + 1, Len(Temp2) - j)
                    BTemp = Mid(Temp2, j, 1) + LCase(BTemp)
                Else
                    BTemp = ""
                End If
                If (i > 0) Then
                    Temp2 = Left(Temp2, i - 1) + ATemp + BTemp
                End If
            End If
            TheText = Temp1 + Temp2
        End If
        If (ExcludeEmpty) And (Trim(TheText) <> "") Then
        
        End If
        AccessTestResults = True
    End If
End If
End Function

Private Function AccessTestOrder(TestIdx As Integer, TheText As String) As Boolean
TheText = ""
AccessTestOrder = False
If (TestIdx > 0) And (TestIdx <= MaxTestsAllowed) Then
    If (Trim(TestResultsStorage(TestIdx).TestOrderNum) <> "") Then
        TheText = Trim(TestResultsStorage(TestIdx).TestOrderNum)
        AccessTestOrder = True
    End If
End If
End Function

Private Function SetTestFiles(PatId As Long, LtrTransOn As Integer, TimeOn As Boolean, LTestsOn As Boolean) As Boolean
Dim i As Integer, p As Integer
Dim Rec As String, OrdNum As String
Dim Temp As String, TheLingo As String
Dim TheText As String, LocalText As String
Dim TestOn As Boolean, NewTest As Boolean
Dim TestFileNum As Integer
Dim CurrentTestName As String
Dim SchedulerTestFile As String
Dim RetrievalClinical As PatientClinical
On Error GoTo UI_ErrorHandler
SetTestFiles = False
TestFileNum = FreeFile
SchedulerTestFile = SchedulerInterfaceDirectory + Trim(str(PatId)) + "TestFile.txt"
Set RetrievalClinical = New PatientClinical
RetrievalClinical.PatientId = PatId
RetrievalClinical.AppointmentId = AppointmentId
RetrievalClinical.ClinicalType = "F"
If (RetrievalClinical.FindPatientClinical > 0) Then
    i = 1
    TestOn = False
    CurrentTestName = ""
    While (RetrievalClinical.SelectPatientClinical(i))
        If (Mid(RetrievalClinical.Symptom, 1, 1) = "/") And _
           (CurrentTestName <> RetrievalClinical.Symptom) And (TestOn) Then
            FM.CloseFile CLng(TestFileNum)
            GoSub VerifyTest
            If (NewTest) Then
                Call PushList(SchedulerTestFile, LtrTransOn)
                Call ApplyTestResults(SchedulerTestFile, "", "", RetrievalClinical.PatientId, Temp, True, LTestsOn)
            End If
            FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
            Print #TestFileNum, "QUESTION=" + Trim(RetrievalClinical.Symptom)
            If (TimeOn) Then
                Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
            End If
            CurrentTestName = RetrievalClinical.Symptom
            TestOn = True
        ElseIf (Not TestOn) Then
            FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
            Print #TestFileNum, "QUESTION=" + Trim(RetrievalClinical.Symptom)
            If (TimeOn) Then
                Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
            End If
            CurrentTestName = RetrievalClinical.Symptom
            TestOn = True
        Else
            If (TestOn) Then
                If (InStrPS(RetrievalClinical.Findings, "TIME") <> 0) Then
                    If (TimeOn) Then
                        Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
                    End If
                Else
                    Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
                End If
            End If
        End If
        i = i + 1
    Wend
End If
If (TestOn) Then
    FM.CloseFile CLng(TestFileNum)
    GoSub VerifyTest
    If (NewTest) Then
        Call PushList(SchedulerTestFile, LtrTransOn)
        Call ApplyTestResults(SchedulerTestFile, "", "", RetrievalClinical.PatientId, Temp, True, LTestsOn)
    End If
End If
SetTestFiles = True
Set RetrievalClinical = Nothing
Exit Function
VerifyTest:
    NewTest = True
    If (LTestsOn) Then
        For p = 1 To MaxTestsAllowed
            If (Trim(TestResultsStorage(p).TestQuestion) = Trim(Mid(CurrentTestName, 2, Len(CurrentTestName) - 1))) Then
                NewTest = False
                Exit For
            ElseIf (Trim(TestResultsStorage(p).TestQuestion) = "") Then
                Exit For
            End If
        Next p
    End If
    Return
UI_ErrorHandler:
    SetTestFiles = True
    Set RetrievalClinical = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Function FindTestResults(ATestId As String) As Integer
Dim i As Integer
FindTestResults = 0
If (Trim(ATestId) <> "") Then
    For i = 1 To MaxTestsAllowed
        If (TestResultsStorage(i).TestOrderNum = ATestId) Then
            FindTestResults = i
            Exit For
        End If
    Next i
End If
End Function

Private Function ApplyTestResults(TheFileName As String, ATestId As String, ADate As String, PatId As Long, ReturnString As String, PostResults As Boolean, LTestsOn As Boolean) As Boolean
Dim q As Integer, FileNum As Integer
Dim ATestFId As String
Dim Rec As String, ATime As String
Dim AParty As String, TheText As String
Dim ATestType As String, ATestPO As Integer
Dim TheLingo As String, PrevLine As String
Dim LocalText As String, AQuestion As String
Dim RetCls As DynamicClass
Dim RetNote As PatientNotes
ApplyTestResults = False
ATime = ""
AParty = ""
TheText = ""
ATestType = ""
ATestPO = 0
ReturnString = ""
FileNum = FreeFile
If (FM.IsFileThere(TheFileName)) Then
    ApplyTestResults = True
    PrevLine = ""
    FM.OpenFile TheFileName, FileOpenMode.InputFileOpenMode, FileAccess.Read, CLng(FileNum)
    While (Not (EOF(FileNum)))
        LocalText = ""
        Line Input #FileNum, Rec
        If (Left(Rec, 10) = "QUESTION=/") Then
            AQuestion = Trim(Mid(Rec, 11, Len(Rec) - 10))
        End If
        If (Left(Rec, 5) = "TIME=") Then
            q = InStrPS(Rec, "(")
            If (q > 0) Then
                AParty = Mid(Rec, q, Len(Rec) - (q - 1))
                ATime = Trim(Mid(Rec, 6, (q - 1) - 5))
            Else
                ATime = Trim(Mid(Rec, 6, Len(Rec) - 5))
            End If
        End If
        If (Left(Rec, 6) = "Recap=") Then
            LocalText = LocalText + Trim(Mid(Rec, 7, Len(Rec) - 6))
            q = InStrPS(LocalText, "/Time/")
            If (q <> 0) And (Trim(ATime) <> "") Then
                LocalText = Left(LocalText, q) + ATime + Mid(LocalText, q + 5, Len(LocalText) - (q + 4))
            End If
            Call ConvertButtonDisplay(LocalText, TheLingo)
            If (Len(TheText) > 0) And (Left(TheLingo, 1) = "%") Then
                q = InStrPS(TheLingo, ":")
                If (q <> 0) Then
                    TheLingo = Mid(TheLingo, q + 1, Len(TheLingo) - q)
                    TheText = TheText + Trim(TheLingo)
                Else
                    TheText = TheText + Chr(13) + Chr(10) + Trim(TheLingo)
                End If
            Else
                TheText = TheText + TheLingo
            End If
        End If
        PrevLine = TheLingo
    Wend
    If (PostResults) Then
        Set RetCls = New DynamicClass
        RetCls.QuestionParty = "T"
        RetCls.Question = AQuestion
        If (RetCls.RetrieveClassbyQuestion) Then
            ATestFId = Trim(RetCls.FollowUp)
            ATestPO = RetCls.DIPrintOrder
            ATestType = RetCls.QuestionIndicator
            ATestId = Trim(RetCls.QuestionOrder)
        Else
            Set RetCls = Nothing
            Set RetCls = New DynamicClass
            RetCls.QuestionParty = "Y"
            RetCls.Question = AQuestion
            If (RetCls.RetrieveClassbyQuestion) Then
                ATestFId = Trim(RetCls.FollowUp)
                ATestPO = RetCls.DIPrintOrder
                ATestType = RetCls.QuestionIndicator
                ATestId = Trim(RetCls.QuestionOrder)
            End If
        End If
        Set RetCls = Nothing
        If (InStrPS(UCase(TheText), UCase(AQuestion)) = 0) Then
            TheText = AQuestion + ":" + vbCrLf + TheText
        End If
        Call ReplaceCharacters(TheText, ":  ", ": ")
        Call ReplaceCharacters(TheText, "//", " ")
        If (Mid(TheText, Len(TheText) - 1, 2) = ", ") Then
            TheText = Left(TheText, Len(TheText) - 2)
        End If
        Call ReplaceCharacters(TheText, ", " + vbCrLf + "OS:", vbCrLf + "OS:")
        If (LTestsOn) Then
            Set RetNote = New PatientNotes
            RetNote.NotesAppointmentId = AppointmentId
            RetNote.NotesType = "C"
            RetNote.NotesSystem = "T" + ATestId
            If (RetNote.FindNotes > 0) Then
                If (RetNote.SelectNotes(1)) Then
                    TheText = Trim(TheText + vbCrLf + Trim(RetNote.NotesText1) + " " _
                            + Trim(RetNote.NotesText2) + " " + Trim(RetNote.NotesText3) + " " _
                            + Trim(RetNote.NotesText4))
                End If
            End If
            Set RetNote = Nothing
        End If
        Call StoreTestResults(TheText, ATestId, AQuestion, ATime, AParty, False, ADate, AppointmentId, ATestFId, ATestType, ATestPO, PatId)
    End If
    ReturnString = TheText
    FM.CloseFile CLng(FileNum)
End If
End Function

Private Function StoreTestResults(TestResult As String, TestId As String, TestQuestion As String, TestTime As String, TestParty As String, TestHigh As Boolean, TestDate As String, TestApptId As Long, TestFollowId As String, TestType As String, TestPO As Integer, PatId As Long) As Boolean
Dim i As Integer
Dim q As Integer
Dim NoteTemp As String
StoreTestResults = True
If (Trim(TestResult) <> "") Then
    q = FindTestResults(TestId)
    If (q < 1) Then
        For i = 1 To 60
            If (Trim(TestResultsStorage(i).TestOrderNum) = "") Then
                q = i
                Exit For
            End If
        Next i
        If (q < 1) Then
            Exit Function
        End If
    End If
    If (Mid(TestResult, 1, 3) = " " + Chr(13) + Chr(10)) Then
        TestResult = Mid(TestResult, 4, Len(TestResult) - 3)
    End If
    TestResultsStorage(q).TestApptId = TestApptId
    TestResultsStorage(q).TestDate = TestDate
    TestResultsStorage(q).TestTime = TestTime
    TestResultsStorage(q).TestParty = TestParty
    Call ReplaceCharacters(TestResult, "OD:", "OD: ")
    TestResultsStorage(q).TestText = TestResult
    TestResultsStorage(q).TestOrderNum = TestId
    TestResultsStorage(q).TestFollowupNum = TestFollowId
    TestResultsStorage(q).TestQuestion = TestQuestion
    TestResultsStorage(q).TestHightlightOn = TestHigh
    TestResultsStorage(q).TestFollowupOn = False
    TestResultsStorage(q).TestMove = False
    TestResultsStorage(q).TestType = TestType
    TestResultsStorage(q).TestPrintOrder = TestPO
    TestResultsStorage(q).TestNoteOn = GetNotesForTests(PatId, AppointmentId, TestId, NoteTemp)
End If
End Function

Private Sub PushList(FileName As String, LtrTransOn As Integer)
On Error GoTo UI_ErrorHandler
Dim TheRecord As String
Dim DoRecapCheck As Boolean
Dim z1 As Integer
Dim w As Integer, s As Integer
Dim q As Integer, r As Integer
Dim a As Integer, k As Integer
Dim Y As Integer, z As Integer
Dim FileNum As Integer
Dim MultiTestIndicator As String
Dim PadValue As String
Dim Question As String
Dim TheValue As String
Dim TestTime As String
Dim LocalText As String
Dim LocalQuestion As String
Dim LocalTestName As String
Dim Recap(50) As String
Dim CurrentLevel As Integer
Dim Level1 As Integer
Dim TestNameLoc As Integer
Dim TestWideName As String
Dim WhereTestNameIs As Integer
Dim ControlLevel1 As DynamicControls
Level1 = 0
FileNum = 0
WhereTestNameIs = 0
Erase Recap
LocalTestName = ""
TestTime = ""
DoRecapCheck = True
FileNum = FreeFile
FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
Do Until (EOF(FileNum))
    Line Input #FileNum, TheRecord
    If (InStrPS(TheRecord, "QUESTION=") <> 0) Then
        Question = Trim(Mid(TheRecord, 10, Len(TheRecord) - 9))
        If (UCase(Trim(Question)) = "/NATIONAL ORIGIN") Then
            DoRecapCheck = False
        End If
    ElseIf (InStrPS(TheRecord, "TIME=") <> 0) Then
        TestTime = Trim(Mid(TheRecord, 6, Len(TheRecord) - 5))
    Else
        r = InStrPS(TheRecord, "=")
        If (r = 0) Then
            r = Len(TheRecord) + 1
        End If
        r = r - 1
        w = r + 3
        Y = InStrPS(w, TheRecord, " ")
        If (Y = 0) Then
            Y = Len(TheRecord) + 1
        End If
        Set ControlLevel1 = New DynamicControls
        ControlLevel1.ControlId = Val(Trim(Mid(TheRecord, w, Y - w)))
        ControlLevel1.ControlName = ""
        ControlLevel1.IEChoiceName = ""
        If (ControlLevel1.RetrieveControl) Then
            MultiTestIndicator = ""
            PadValue = ""
            TheValue = ""
            If (ControlLevel1.ControlType = "N") Or (ControlLevel1.ControlType = "S") Then
                Y = InStrPS(w, TheRecord, "<")
                If (Y <> 0) Then
                    z = InStrPS(w, TheRecord, ">")
                    If (z <> 0) Then
                        PadValue = "@" + Mid(TheRecord, Y + 1, (z - 1) - Y)
                    End If
                End If
            ElseIf (ControlLevel1.ControlType = "R") Then
                MultiTestIndicator = ""
            End If
            Level1 = Level1 + 1
            If (Trim(ControlLevel1.DoctorLingo) <> "") Then
                If (LtrTransOn = 2) Then
                    If (ControlLevel1.Exclusion <> "T") Then
                        If (Trim(ControlLevel1.LetterLanguage1) <> "") Then
                            Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.LetterLanguage1) + " " + PadValue
                        Else
                            Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.DoctorLingo) + " " + PadValue
                        End If
                    End If
                ElseIf (LtrTransOn = 1) Then
                    If (ControlLevel1.Exclusion <> "T") Then
                        If (Trim(ControlLevel1.LetterLanguage) <> "") Then
                            Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.LetterLanguage) + " " + PadValue
                        Else
                            Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.DoctorLingo) + " " + PadValue
                        End If
                    End If
                Else
                    Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.DoctorLingo) + " " + PadValue
                End If
                Call GetPadValue(Recap(Level1), TheValue)
                Call SetPadValue(Recap(Level1), TheValue)
                s = InStrPS(Recap(Level1), "/Time/")
                If (s <> 0) Then
                    Recap(Level1) = Left(Recap(Level1), s) + TestTime + Mid(Recap(Level1), s + 5, Len(Recap(Level1)) - (s + 4))
                End If
                s = InStrPS(Recap(Level1), ":^OD")
                z1 = InStrPS(Recap(Level1), ":/")
                If (s <> 0) Then
                    WhereTestNameIs = Level1
                    LocalTestName = Trim(Left(Recap(Level1), s - 1))
                ElseIf (z1 <> 0) Then
                    WhereTestNameIs = Level1
                    LocalTestName = Trim(Left(Recap(Level1), z1 - 1))
                End If
            Else
                Recap(Level1) = "-"
            End If
            If (DoRecapCheck) Then
                s = InStrPS(Recap(Level1), LocalTestName)
                If (s <> 0) Then
                    s = InStrPS(Recap(Level1), "^OS")
                    If (s <> 0) Then
                        Recap(Level1) = Mid(Recap(Level1), s, Len(Recap(Level1)) - (s - 1))
                    Else
                        If (WhereTestNameIs > 0) And (WhereTestNameIs <> Level1) Then
                            s = InStrPS(Recap(WhereTestNameIs), ":")
                            If (s <> 0) Then
                                TestWideName = Trim(Recap(Level1))
                                k = InStrPS(Recap(Level1), ":")
                                If (k <> 0) Then
                                    TestWideName = Trim(Mid(Recap(Level1), s + 1, Len(Recap(Level1)) - s))
                                End If
                                Recap(WhereTestNameIs) = Left(Recap(WhereTestNameIs), s) + TestWideName + Mid(Recap(WhereTestNameIs), s + 1, Len(Recap(WhereTestNameIs)) - s)
                                Recap(Level1) = "-"
                            End If
                        End If
                    End If
                End If
            End If
        End If
        Set ControlLevel1 = Nothing
    End If
Loop
FM.CloseFile CLng(FileNum)

' Build Recap Values
FM.OpenFile FileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
For s = 1 To Level1
    If (Trim(Recap(s)) <> "") And (Trim(Recap(s)) <> "-") Then
        Print #FileNum, "Recap=" + Trim(Recap(s))
    End If
Next s
FM.CloseFile CLng(FileNum)
Exit Sub
UI_ErrorHandler:
    If (FileNum <> 0) Then
        FM.CloseFile CLng(FileNum)
    End If
    Set ControlLevel1 = Nothing
    Resume LeaveFast
LeaveFast:
End Sub

Private Function GetNotesForTests(PatId As Long, ApptId As Long, TstId As String, TheNote As String) As Boolean
Dim i As Integer
Dim RetNote As PatientNotes
GetNotesForTests = False
TheNote = ""
If (PatId > 0) And (ApptId > 0) Then
    Set RetNote = New PatientNotes
    RetNote.NotesAppointmentId = ApptId
    RetNote.NotesPatientId = PatId
    RetNote.NotesSystem = "T" + TstId
    RetNote.NotesType = "C"
    If (RetNote.FindNotes > 0) Then
        i = 1
        While (RetNote.SelectNotes(i))
            If (Trim(RetNote.NotesSystem) = "T" + TstId) And (Trim(RetNote.NotesOffDate) = "") Then
                TheNote = Trim(TheNote) + Trim(RetNote.NotesText1) + Trim(RetNote.NotesText2) + Trim(RetNote.NotesText3) + Trim(RetNote.NotesText4)
                If (Trim(RetNote.NotesEye) <> "") Then
                    TheNote = Trim(RetNote.NotesEye) + ":" + TheNote
                Else
                    TheNote = "OU:" + TheNote
                End If
                GetNotesForTests = True
            End If
            i = i + 1
        Wend
    End If
    Set RetNote = Nothing
End If
End Function

Private Function SetupHistoricalTests(TheOrder As String) As Boolean
Dim i As Integer
Dim j As Integer
Dim ADate As String
Dim PDate As String
Dim Temp As String
Dim TestResult As String
Dim RetAppt As SchedulerAppointment
SetupHistoricalTests = False
lstAllTests.Clear
lstAllTests.Tag = TheOrder
lstAllTests.AddItem "Close Test List"
CurrentTestNum(CurrentTestId) = 0
For i = 1 To 4
    OtherTestsToInclude(CurrentTestId, i) = 0
Next i
Set RetAppt = New SchedulerAppointment
RetAppt.AppointmentPatientId = PatientId
RetAppt.AppointmentStatus = "D"
If (RetAppt.FindAppointment > 0) Then
    i = 1
    While (RetAppt.SelectAppointment(i))
        PDate = RetAppt.AppointmentDate
        ADate = Mid(PDate, 5, 2) + "/" + Mid(PDate, 7, 2) + "/" + Left(PDate, 4)
        TestResult = BuildSpecificTestResults(PatientId, RetAppt.AppointmentId, TheOrder)
        If (Trim(TestResult) <> "") Then
            j = InStrPS(TestResult, vbCrLf)
            Temp = ADate + " " + Left(TestResult, j - 1)
            lstAllTests.AddItem Temp
            lstAllTests.ItemData(lstAllTests.NewIndex) = RetAppt.AppointmentId
            TestResult = Mid(TestResult, j + 2, Len(TestResult) - (j + 1))
            j = InStrPS(TestResult, vbCrLf)
            While (j > 0)
                Temp = Space(11) + Trim(Left(TestResult, j - 1))
                lstAllTests.AddItem Temp
                lstAllTests.ItemData(lstAllTests.NewIndex) = RetAppt.AppointmentId
                TestResult = Mid(TestResult, j + 2, Len(TestResult) - (j + 1))
                j = InStrPS(TestResult, vbCrLf)
            Wend
            Temp = Space(11) + Trim(TestResult)
            lstAllTests.AddItem Temp
            lstAllTests.ItemData(lstAllTests.NewIndex) = RetAppt.AppointmentId
        End If
        i = i + 1
    Wend
    If (lstAllTests.ListCount > 0) Then
        lstAllTests.Visible = True
    End If
End If
Set RetAppt = Nothing
SetupHistoricalTests = True
End Function

Private Function BuildSpecificTestResults(PatId As Long, ApptId As Long, TestId As String) As String
Dim i As Integer
Dim MyAppt As Long
Dim RetAppt As SchedulerAppointment
BuildSpecificTestResults = ""
If (PatId > 0) Then
    If (ApptId < 1) Then
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentPatientId = PatId
        RetAppt.AppointmentDate = ""
        RetAppt.AppointmentStatus = "D"
        If (RetAppt.FindAppointmentPreviousbyDate > 0) Then
            i = 1
            Do Until Not (RetAppt.SelectAppointment(i))
                AppointmentId = RetAppt.AppointmentId
                BuildSpecificTestResults = SetSpecificTestFiles(PatId, ApptId, TestId)
                If (BuildSpecificTestResults <> "") Then
                    ApptId = AppointmentId
                    Exit Do
                End If
                i = i + 1
            Loop
        End If
        Set RetAppt = Nothing
    Else
        BuildSpecificTestResults = SetSpecificTestFiles(PatId, ApptId, TestId)
    End If
End If
End Function

Private Function SetSpecificTestFiles(PatId As Long, ApptId As Long, ATestId As String) As String
Dim i As Integer
Dim TestOn As Boolean
Dim AQues As String
Dim TestFileNum As Integer
Dim SchedulerTestFile As String
Dim RetrievalClinical As PatientClinical
On Error GoTo UI_ErrorHandler
SetSpecificTestFiles = ""
If (GetTestQuestion(ATestId, AQues)) Then
    TestOn = False
    TestFileNum = FreeFile
    SchedulerTestFile = SchedulerInterfaceDirectory + Trim(str(PatId)) + "TestFile.txt"
    If (FM.IsFileThere(SchedulerTestFile)) Then
        FM.Kill SchedulerTestFile
    End If
    Set RetrievalClinical = New PatientClinical
    RetrievalClinical.PatientId = PatId
    RetrievalClinical.AppointmentId = ApptId
    RetrievalClinical.ClinicalType = "F"
    RetrievalClinical.Symptom = "/" + AQues
    RetrievalClinical.Findings = ""
    RetrievalClinical.ImageDescriptor = ""
    RetrievalClinical.ImageInstructions = ""
    If (RetrievalClinical.FindPatientClinical > 0) Then
        i = 1
        While (RetrievalClinical.SelectPatientClinical(i))
            If (Mid(RetrievalClinical.Symptom, 1, 1) = "/") And (Not TestOn) Then
                FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
                Print #TestFileNum, "QUESTION=" + Trim(RetrievalClinical.Symptom)
                Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
                TestOn = True
            Else
                Print #TestFileNum, Trim(RetrievalClinical.Findings) + " " + Trim(RetrievalClinical.ImageInstructions)
            End If
            i = i + 1
        Wend
    End If
    FM.CloseFile CLng(TestFileNum)
    If (FM.IsFileThere(SchedulerTestFile)) Then
        Call PushList(SchedulerTestFile, 1)
        SetSpecificTestFiles = ApplySpecificTestResults(SchedulerTestFile)
        FM.Kill SchedulerTestFile
    End If
    Set RetrievalClinical = Nothing
End If
Exit Function
UI_ErrorHandler:
    SetSpecificTestFiles = ""
    Set RetrievalClinical = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Function ApplySpecificTestResults(TheFileName As String) As String
Dim q As Integer, FileNum As Integer
Dim ATestFId As String
Dim Rec As String, ATime As String
Dim AParty As String, TheText As String
Dim ATestType As String, ATestPO As Integer
Dim TheLingo As String, PrevLine As String
Dim LocalText As String, AQuestion As String
Dim RetCls As DynamicClass
Dim RetNote As PatientNotes
ApplySpecificTestResults = ""
FileNum = FreeFile
If (FM.IsFileThere(TheFileName)) Then
    PrevLine = ""
    FM.OpenFile TheFileName, FileOpenMode.InputFileOpenMode, FileAccess.Read, CLng(FileNum)
    While (Not (EOF(FileNum)))
        LocalText = ""
        Line Input #FileNum, Rec
        If (Left(Rec, 10) = "QUESTION=/") Then
            AQuestion = Trim(Mid(Rec, 11, Len(Rec) - 10))
        End If
        If (Left(Rec, 5) = "TIME=") Then
            q = InStrPS(Rec, "(")
            If (q > 0) Then
                AParty = Mid(Rec, q, Len(Rec) - (q - 1))
                ATime = Trim(Mid(Rec, 6, (q - 1) - 5))
            Else
                ATime = Trim(Mid(Rec, 6, Len(Rec) - 5))
            End If
        End If
        If (Left(Rec, 6) = "Recap=") Then
            LocalText = LocalText + Trim(Mid(Rec, 7, Len(Rec) - 6))
            q = InStrPS(LocalText, "/Time/")
            If (q <> 0) And (Trim(ATime) <> "") Then
                LocalText = Left(LocalText, q) + ATime + Mid(LocalText, q + 5, Len(LocalText) - (q + 4))
            End If
            Call ConvertButtonDisplay(LocalText, TheLingo)
            If (Len(TheText) > 0) And (Left(TheLingo, 1) = "%") Then
                q = InStrPS(TheLingo, ":")
                If (q <> 0) Then
                    TheLingo = Mid(TheLingo, q + 1, Len(TheLingo) - q)
                    TheText = TheText + Trim(TheLingo)
                Else
                    TheText = TheText + Chr(13) + Chr(10) + Trim(TheLingo)
                End If
            Else
                TheText = TheText + TheLingo
            End If
        End If
        PrevLine = TheLingo
    Wend
    FM.CloseFile CLng(FileNum)
    If (InStrPS(UCase(TheText), UCase(AQuestion)) = 0) Then
        TheText = AQuestion + ":" + vbCrLf + TheText
    End If
    Call ReplaceCharacters(TheText, ":  ", ": ")
    Call ReplaceCharacters(TheText, "//", " ")
    If (Mid(TheText, Len(TheText) - 1, 2) = ", ") Then
        TheText = Left(TheText, Len(TheText) - 2)
    End If
    Call ReplaceCharacters(TheText, ", " + vbCrLf + "OS:", vbCrLf + "OS:")
    ApplySpecificTestResults = TheText
End If
End Function

Private Function GetTestQuestion(ATestId As String, AQuestion As String) As Boolean
Dim RetQ As DynamicClass
GetTestQuestion = False
AQuestion = ""
If (Trim(ATestId) <> "") Then
    Set RetQ = New DynamicClass
    RetQ.QuestionParty = "T"
    RetQ.QuestionOrder = ATestId
    If (RetQ.FindClassForms > 0) Then
        If (RetQ.SelectClassForm(1)) Then
            AQuestion = RetQ.Question
            GetTestQuestion = True
        End If
    End If
    Set RetQ = Nothing
End If
End Function
Public Function FrmClose()
Call cmdDone_Click
 Unload Me
End Function
