VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmApptType 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt1 
      Height          =   1095
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt2 
      Height          =   1095
      Left            =   225
      TabIndex        =   1
      Top             =   1680
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":01E1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt3 
      Height          =   1095
      Left            =   225
      TabIndex        =   3
      Top             =   2880
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":03C2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt4 
      Height          =   1095
      Left            =   225
      TabIndex        =   4
      Top             =   4080
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":05A3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt5 
      Height          =   1095
      Left            =   225
      TabIndex        =   5
      Top             =   5280
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":0784
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt6 
      Height          =   1095
      Left            =   225
      TabIndex        =   6
      Top             =   6480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":0965
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   990
      Left            =   6600
      TabIndex        =   7
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":0B46
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt7 
      Height          =   1095
      Left            =   4200
      TabIndex        =   8
      Top             =   480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":0D25
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt8 
      Height          =   1095
      Left            =   4200
      TabIndex        =   9
      Top             =   1680
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":0F06
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt9 
      Height          =   1095
      Left            =   4200
      TabIndex        =   10
      Top             =   2880
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":10E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt10 
      Height          =   1095
      Left            =   4200
      TabIndex        =   11
      Top             =   4080
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":12C8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt11 
      Height          =   1095
      Left            =   4200
      TabIndex        =   12
      Top             =   5280
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":14A9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt12 
      Height          =   1095
      Left            =   4200
      TabIndex        =   13
      Top             =   6480
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":168A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt13 
      Height          =   1095
      Left            =   8160
      TabIndex        =   14
      Top             =   480
      Width           =   3600
      _Version        =   131072
      _ExtentX        =   6350
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":186B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt14 
      Height          =   1110
      Left            =   8160
      TabIndex        =   15
      Top             =   1680
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":1A4C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt15 
      Height          =   1110
      Left            =   8160
      TabIndex        =   16
      Top             =   2880
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":1C2D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt16 
      Height          =   1110
      Left            =   8160
      TabIndex        =   17
      Top             =   4080
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":1E0E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt17 
      Height          =   1095
      Left            =   8160
      TabIndex        =   18
      Top             =   5280
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":1FEF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAppt18 
      Height          =   1110
      Left            =   8160
      TabIndex        =   19
      Top             =   6480
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":21D0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdQuit 
      Height          =   990
      Left            =   240
      TabIndex        =   20
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":23B1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10200
      TabIndex        =   21
      Top             =   7680
      Visible         =   0   'False
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":2590
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   990
      Left            =   4200
      TabIndex        =   22
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":276F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAnother 
      Height          =   990
      Left            =   2640
      TabIndex        =   23
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":295C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   990
      Left            =   8160
      TabIndex        =   24
      Top             =   7680
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7828525
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ApptType.frx":2B42
   End
   Begin VB.Label lblField 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Select An Appointment Type"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   3210
   End
End
Attribute VB_Name = "frmApptType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientID As Long
Public ResourceName As String
Public AppointmentId As Long
Public DeleteOn As Boolean
Public ApptTypeName As String
Public ApptTypeId As Long
Public ApptPeriod As String
Public lstBox As ListBox
Public NoteOn As Boolean

Private Reload As Boolean
Private SetBackColor As Long
Private SetForeColor As Long
Private BaseBackColor As Long
Private BaseForeColor As Long
Private MaxItems As Integer
Private CurrentAdmin As Integer
Private AdminList(40) As String
Private DisplayType As String
Private CurrentIndex As Integer
Private TheComplaintCategory As String
Private TotalAppts As Long

Private Sub cmdNotes_Click()
frmNotes.NoteId = 0
frmNotes.ResourceName = ResourceName
frmNotes.PatientID = PatientID
frmNotes.AppointmentId = AppointmentId
frmNotes.SystemReference = "8"
frmNotes.EyeContext = ""
frmNotes.NoteOn = False
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    NoteOn = frmNotes.NoteOn
    Call cmdDone_Click
End If
End Sub

Private Sub cmdAnother_Click()
ResourceName = ""
PatientID = 0
AppointmentId = 0
ApptTypeId = -3
ApptTypeName = ""
Unload frmApptType
End Sub

Private Sub cmdDone_Click()
Dim i As Integer
If Not (cmdQuit.Visible) Then
    ApptTypeId = -1
    ApptTypeName = ""
    If (Reload) Then
        ApptTypeId = -2
    End If
End If
If (CurrentAdmin > 0) Then
    For i = 1 To CurrentAdmin
        If (Trim(AdminList(i)) <> "") Then
            ApptTypeName = ApptTypeName + Trim(AdminList(i)) + "-f/"
        End If
    Next i
End If
ResourceName = ""
PatientID = 0
AppointmentId = 0
Unload frmApptType
End Sub

Private Sub cmdQuit_Click()
ResourceName = ""
PatientID = 0
AppointmentId = 0
ApptTypeId = -1
ApptTypeName = ""
If (Reload) Then
    ApptTypeId = -2
End If
Unload frmApptType
End Sub

Private Sub cmdDelete_Click()
frmEventMsgs.Header = "Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Delete"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    DeleteOn = True
    ApptTypeId = -1
    ApptTypeName = ""
    If (UCase(cmdDelete.Text) = UCase("De-Select ALL")) Then
        ApptTypeId = -2
    End If
    ResourceName = ""
    PatientID = 0
    AppointmentId = 0
    Unload frmApptType
End If
End Sub

Private Sub cmdMore_Click()
If (CurrentIndex > TotalAppts - 1) Then
    CurrentIndex = 1
End If
CurrentIndex = CurrentIndex + 1
Call LoadAppt(DisplayType, False)
End Sub

Private Sub cmdAppt1_Click()
Call SetItem(cmdAppt1)
End Sub

Private Sub cmdAppt2_Click()
Call SetItem(cmdAppt2)
End Sub

Private Sub cmdAppt3_Click()
Call SetItem(cmdAppt3)
End Sub

Private Sub cmdAppt4_Click()
Call SetItem(cmdAppt4)
End Sub

Private Sub cmdAppt5_Click()
Call SetItem(cmdAppt5)
End Sub

Private Sub cmdAppt6_Click()
Call SetItem(cmdAppt6)
End Sub

Private Sub cmdAppt7_Click()
Call SetItem(cmdAppt7)
End Sub

Private Sub cmdAppt8_Click()
Call SetItem(cmdAppt8)
End Sub

Private Sub cmdAppt9_Click()
Call SetItem(cmdAppt9)
End Sub

Private Sub cmdAppt10_Click()
Call SetItem(cmdAppt10)
End Sub

Private Sub cmdAppt11_Click()
Call SetItem(cmdAppt11)
End Sub

Private Sub cmdAppt12_Click()
Call SetItem(cmdAppt12)
End Sub

Private Sub cmdAppt13_Click()
Call SetItem(cmdAppt13)
End Sub

Private Sub cmdAppt14_Click()
Call SetItem(cmdAppt14)
End Sub

Private Sub cmdAppt15_Click()
Call SetItem(cmdAppt15)
End Sub

Private Sub cmdAppt16_Click()
Call SetItem(cmdAppt16)
End Sub

Private Sub cmdAppt17_Click()
Call SetItem(cmdAppt17)
End Sub

Private Sub cmdAppt18_Click()
Call SetItem(cmdAppt18)
End Sub

Private Sub ClearAppt()
cmdAppt1.Text = ""
cmdAppt1.BackColor = BaseBackColor
cmdAppt1.ForeColor = BaseForeColor
cmdAppt1.AlignTextH = fpAlignTextHCenter
cmdAppt1.AlignTextV = fpAlignTextVCenter
cmdAppt1.Visible = False
cmdAppt2.Text = ""
cmdAppt2.BackColor = BaseBackColor
cmdAppt2.ForeColor = BaseForeColor
cmdAppt2.AlignTextH = fpAlignTextHCenter
cmdAppt2.AlignTextV = fpAlignTextVCenter
cmdAppt2.Visible = False
cmdAppt3.Text = ""
cmdAppt3.BackColor = BaseBackColor
cmdAppt3.ForeColor = BaseForeColor
cmdAppt3.AlignTextH = fpAlignTextHCenter
cmdAppt3.AlignTextV = fpAlignTextVCenter
cmdAppt3.Visible = False
cmdAppt4.Text = ""
cmdAppt4.BackColor = BaseBackColor
cmdAppt4.ForeColor = BaseForeColor
cmdAppt4.AlignTextH = fpAlignTextHCenter
cmdAppt4.AlignTextV = fpAlignTextVCenter
cmdAppt4.Visible = False
cmdAppt5.Text = ""
cmdAppt5.BackColor = BaseBackColor
cmdAppt5.ForeColor = BaseForeColor
cmdAppt5.AlignTextH = fpAlignTextHCenter
cmdAppt5.AlignTextV = fpAlignTextVCenter
cmdAppt5.Visible = False
cmdAppt6.Text = ""
cmdAppt6.BackColor = BaseBackColor
cmdAppt6.ForeColor = BaseForeColor
cmdAppt6.AlignTextH = fpAlignTextHCenter
cmdAppt6.AlignTextV = fpAlignTextVCenter
cmdAppt6.Visible = False
cmdAppt7.Text = ""
cmdAppt7.BackColor = BaseBackColor
cmdAppt7.ForeColor = BaseForeColor
cmdAppt7.AlignTextH = fpAlignTextHCenter
cmdAppt7.AlignTextV = fpAlignTextVCenter
cmdAppt7.Visible = False
cmdAppt8.Text = ""
cmdAppt8.BackColor = BaseBackColor
cmdAppt8.ForeColor = BaseForeColor
cmdAppt8.AlignTextH = fpAlignTextHCenter
cmdAppt8.AlignTextV = fpAlignTextVCenter
cmdAppt8.Visible = False
cmdAppt9.Text = ""
cmdAppt9.BackColor = BaseBackColor
cmdAppt9.ForeColor = BaseForeColor
cmdAppt9.AlignTextH = fpAlignTextHCenter
cmdAppt9.AlignTextV = fpAlignTextVCenter
cmdAppt9.Visible = False
cmdAppt10.Text = ""
cmdAppt10.BackColor = BaseBackColor
cmdAppt10.ForeColor = BaseForeColor
cmdAppt10.AlignTextH = fpAlignTextHCenter
cmdAppt10.AlignTextV = fpAlignTextVCenter
cmdAppt10.Visible = False
cmdAppt11.Text = ""
cmdAppt11.BackColor = BaseBackColor
cmdAppt11.ForeColor = BaseForeColor
cmdAppt11.AlignTextH = fpAlignTextHCenter
cmdAppt11.AlignTextV = fpAlignTextVCenter
cmdAppt11.Visible = False
cmdAppt12.Text = ""
cmdAppt12.BackColor = BaseBackColor
cmdAppt12.ForeColor = BaseForeColor
cmdAppt12.AlignTextH = fpAlignTextHCenter
cmdAppt12.AlignTextV = fpAlignTextVCenter
cmdAppt12.Visible = False
cmdAppt13.Text = ""
cmdAppt13.BackColor = BaseBackColor
cmdAppt13.ForeColor = BaseForeColor
cmdAppt13.AlignTextH = fpAlignTextHCenter
cmdAppt13.AlignTextV = fpAlignTextVCenter
cmdAppt13.Visible = False
cmdAppt14.Text = ""
cmdAppt14.BackColor = BaseBackColor
cmdAppt14.ForeColor = BaseForeColor
cmdAppt14.AlignTextH = fpAlignTextHCenter
cmdAppt14.AlignTextV = fpAlignTextVCenter
cmdAppt14.Visible = False
cmdAppt15.Text = ""
cmdAppt15.BackColor = BaseBackColor
cmdAppt15.ForeColor = BaseForeColor
cmdAppt15.AlignTextH = fpAlignTextHCenter
cmdAppt15.AlignTextV = fpAlignTextVCenter
cmdAppt15.Visible = False
cmdAppt16.Text = ""
cmdAppt16.BackColor = BaseBackColor
cmdAppt16.ForeColor = BaseForeColor
cmdAppt16.AlignTextH = fpAlignTextHCenter
cmdAppt16.AlignTextV = fpAlignTextVCenter
cmdAppt16.Visible = False
cmdAppt17.Text = ""
cmdAppt17.BackColor = BaseBackColor
cmdAppt17.ForeColor = BaseForeColor
cmdAppt17.AlignTextH = fpAlignTextHCenter
cmdAppt17.AlignTextV = fpAlignTextVCenter
cmdAppt17.Visible = False
cmdAppt18.Text = ""
cmdAppt18.BackColor = BaseBackColor
cmdAppt18.ForeColor = BaseForeColor
cmdAppt18.AlignTextH = fpAlignTextHCenter
cmdAppt18.AlignTextV = fpAlignTextVCenter
cmdAppt18.Visible = False
cmdMore.Text = "More"
cmdMore.Visible = False
End Sub

Private Function IsReferenceMatch(AMatch) As Boolean
Dim i As Integer
IsReferenceMatch = False
For i = 1 To CurrentAdmin
    If (AMatch = Left(AdminList(i), Len(AMatch))) Then
        IsReferenceMatch = True
        Exit For
    End If
Next i
End Function

Private Sub SetItemOn(AButton As fpBtn)
AButton.BackColor = SetBackColor
AButton.ForeColor = SetForeColor
End Sub

Private Sub SetItem(AButton As fpBtn)
Dim i As Integer
Dim j As Integer
Dim p As Integer
Dim ITemp As String
Dim TheEyes As String
If (DisplayType = "v") Then
    TheComplaintCategory = Trim(AButton.Text)
    CurrentIndex = 1
    Call LoadAppt("V", True)
ElseIf (DisplayType <> "Z") And (DisplayType <> "&") And (DisplayType <> "C") And (DisplayType <> "O") And (DisplayType <> "B") And (DisplayType <> "W") Then
    If (DisplayType = "V") Or (DisplayType = "J") Then
        TheEyes = ""
        Call GetEyes(TheEyes)
    End If
    If (DisplayType = "J") Then
        ApptTypeId = 0
        If (TheEyes = " (OD)") Then
            ApptTypeId = 1
        ElseIf (TheEyes = " (OS)") Then
            ApptTypeId = 2
        End If
        ApptTypeName = AButton.Tag
        ApptPeriod = AButton.ToolTipText
    Else
        ApptTypeId = Val(Str(AButton.Tag))
        ApptTypeName = AButton.Text + TheEyes
        ApptPeriod = AButton.ToolTipText
    End If
    Unload frmApptType
Else
    If (AButton.BackColor = BaseBackColor) Then
        If (MaxItems > 0) Then
            If (CurrentAdmin + 1 > MaxItems) Then
                frmEventMsgs.Header = "Too Many Selections"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                Exit Sub
            End If
        End If
        AButton.BackColor = SetBackColor
        AButton.ForeColor = SetForeColor
        CurrentAdmin = CurrentAdmin + 1
        TheEyes = ""
        If (DisplayType = "C") Then
            Call GetEyes(TheEyes)
        End If
        AdminList(CurrentAdmin) = AButton.Text + TheEyes
    Else
        AButton.BackColor = BaseBackColor
        AButton.ForeColor = BaseForeColor
        For i = 1 To CurrentAdmin
            j = InStrPS(AdminList(i), "(")
            If (j > 0) Then
                ITemp = Left(AdminList(i), j - 1)
            Else
                ITemp = AdminList(i)
            End If
            If (UCase(Trim(AButton.Text)) = UCase(Trim(ITemp))) Then
                For p = i To CurrentAdmin - 1
                    AdminList(p) = AdminList(p + 1)
                Next p
                AdminList(CurrentAdmin) = ""
            End If
        Next i
        CurrentAdmin = CurrentAdmin - 1
        If (CurrentAdmin < 1) Then
            CurrentAdmin = 1
        End If
    End If
End If
End Sub

Public Function LoadAppt(TheType As String, Init As Boolean) As Boolean
Dim k As Integer, i As Integer, z As Integer
Dim BreakPoint As Integer, ButtonCnt As Integer
Dim Temp As String
Dim TrapText As String, DisplayText As String
Dim DontCare As Boolean, GoodDr As Boolean
Dim RetQues As DynamicClass
Dim RetCode As PracticeCodes
Dim RetFavs As PracticeFavorites
Dim RetrieveReferral As PracticeVendors
Dim RetrieveResource As SchedulerResource
Dim RetApptType As SchedulerAppointmentType
LoadAppt = False
Reload = False
MaxItems = 0
BreakPoint = 19
ApptPeriod = ""
DisplayType = TheType
Call ClearAppt
DeleteOn = False
NoteOn = False
cmdQuit.Visible = True
cmdMore.Visible = False
cmdNotes.Visible = False
cmdDelete.Visible = False
cmdDone.Visible = False
cmdAnother.Visible = False
If (TheType = "A") Or (TheType = "a") Then
    lblField.Caption = "Select an Appointment Type"
    Set RetApptType = New SchedulerAppointmentType
    RetApptType.AppointmentType = Chr(1)
    If (TheType = "a") Then
        TotalAppts = RetApptType.FindAppointmentTypebyRankInPlan
    Else
        TotalAppts = RetApptType.FindAppointmentTypebyRank
    End If
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetApptType.SelectAppointmentType(i)) And (ButtonCnt < 19)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = RetApptType.AppointmentType
                cmdAppt1.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt1.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = RetApptType.AppointmentType
                cmdAppt2.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt2.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = RetApptType.AppointmentType
                cmdAppt3.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt3.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = RetApptType.AppointmentType
                cmdAppt4.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt4.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = RetApptType.AppointmentType
                cmdAppt5.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt5.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = RetApptType.AppointmentType
                cmdAppt6.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt6.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = RetApptType.AppointmentType
                cmdAppt7.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt7.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = RetApptType.AppointmentType
                cmdAppt8.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt8.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = RetApptType.AppointmentType
                cmdAppt9.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt9.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = RetApptType.AppointmentType
                cmdAppt10.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt10.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = RetApptType.AppointmentType
                cmdAppt11.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt11.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = RetApptType.AppointmentType
                cmdAppt12.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt12.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = RetApptType.AppointmentType
                cmdAppt13.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt13.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = RetApptType.AppointmentType
                cmdAppt14.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt14.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = RetApptType.AppointmentType
                cmdAppt15.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt15.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = RetApptType.AppointmentType
                cmdAppt16.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt16.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = RetApptType.AppointmentType
                cmdAppt17.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt17.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = RetApptType.AppointmentType
                cmdAppt18.Tag = Trim(Str(RetApptType.AppointmentTypeId))
                cmdAppt18.ToolTipText = Trim(RetApptType.AppointmentTypePeriod)
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetApptType = Nothing
    End If
ElseIf (TheType = "J") Then
    lblField.Caption = "Select Super Favorite Diagnosis"
    Set RetFavs = New PracticeFavorites
    RetFavs.FavoritesSystem = "!"
    RetFavs.FavoritesDiagnosis = ""
    TotalAppts = RetFavs.FindFavorites
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetFavs.SelectFavorites(i)) And (ButtonCnt < 19)
            Call ApplGetDiag(Trim(Left(RetFavs.FavoritesDiagnosis, 1)), Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10)), Temp)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = Trim(Temp)
                cmdAppt1.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt1.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = Trim(Temp)
                cmdAppt2.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt2.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = Trim(Temp)
                cmdAppt3.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt3.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = Trim(Temp)
                cmdAppt4.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt4.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = Trim(Temp)
                cmdAppt5.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt5.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = Trim(Temp)
                cmdAppt6.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt6.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = Trim(Temp)
                cmdAppt7.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt7.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = Trim(Temp)
                cmdAppt8.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt8.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = Trim(Temp)
                cmdAppt9.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt9.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = Trim(Temp)
                cmdAppt10.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt10.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = Trim(Temp)
                cmdAppt11.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt11.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = Trim(Temp)
                cmdAppt12.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt12.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = Trim(Temp)
                cmdAppt13.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt13.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = Trim(Temp)
                cmdAppt14.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt14.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = Trim(Temp)
                cmdAppt15.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt15.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = Trim(Temp)
                cmdAppt16.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt16.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = Trim(Temp)
                cmdAppt17.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt17.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = Trim(Temp)
                cmdAppt18.Tag = Trim(Mid(RetFavs.FavoritesDiagnosis, 3, 10))
                cmdAppt18.ToolTipText = Trim(Left(RetFavs.FavoritesDiagnosis, 1))
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetApptType = Nothing
    End If
ElseIf (TheType = "D") Or (TheType = "H") Or (TheType = "R") Or (TheType = "Q") Then
    lblField.Caption = "Select a Doctor"
    If (TheType = "H") Then
        lblField.Caption = "Select a Hospital"
    ElseIf (TheType = "R") Then
        lblField.Caption = "Select a Referring Doctor"
    End If
    Set RetrieveReferral = New PracticeVendors
    RetrieveReferral.VendorName = ""
    If (Trim(ApptTypeName) <> "") Then
        RetrieveReferral.VendorLastName = Trim(ApptTypeName)
    Else
        RetrieveReferral.VendorLastName = Chr(1)
    End If
    RetrieveReferral.VendorType = TheType
    If (TheType = "Q") Then
        RetrieveReferral.VendorType = "D"
    End If
    If (TheType <> "H") Then
        TotalAppts = RetrieveReferral.FindVendorbyLastNameT
    Else
        TotalAppts = RetrieveReferral.FindVendorT
    End If
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetrieveReferral.SelectVendor(i)) And (ButtonCnt < 19)
            DisplayText = Trim(RetrieveReferral.VendorLastName) + " " + Trim(RetrieveReferral.VendorTitle) + " " + Trim(RetrieveReferral.VendorFirstName)
            If (TheType = "H") Or (TheType = "Q") Then
                DisplayText = Trim(RetrieveReferral.VendorName)
            End If
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = Trim(Str(RetrieveReferral.VendorId))
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetrieveReferral = Nothing
    End If
ElseIf (TheType = "S") Then
    lblField.Caption = "Select a Doctor"
    DontCare = False
    Set RetApptType = New SchedulerAppointmentType
    If (ApptTypeId > 0) Then
        RetApptType.AppointmentTypeId = ApptTypeId
        If (RetApptType.RetrieveSchedulerAppointmentType) Then
            If (RetApptType.AppointmentTypeResourceId1 = 0) And _
               (RetApptType.AppointmentTypeResourceId2 = 0) And _
               (RetApptType.AppointmentTypeResourceId3 = 0) And _
               (RetApptType.AppointmentTypeResourceId4 = 0) And _
               (RetApptType.AppointmentTypeResourceId5 = 0) And _
               (RetApptType.AppointmentTypeResourceId6 = 0) And _
               (RetApptType.AppointmentTypeResourceId7 = 0) Then
                DontCare = True
            End If
        End If
    End If
    Set RetrieveResource = New SchedulerResource
    RetrieveResource.ResourceName = Chr(1)
    RetrieveResource.ResourceType = "D"
    TotalAppts = RetrieveResource.FindResource
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetrieveResource.SelectResource(i)) And (ButtonCnt < 19)
            GoodDr = False
            If (DontCare) Then
                GoodDr = True
            Else
                If (RetApptType.AppointmentTypeResourceId1 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId2 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId3 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId4 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId5 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId6 = RetrieveResource.ResourceId) Or _
                   (RetApptType.AppointmentTypeResourceId7 = RetrieveResource.ResourceId) Or _
                   (ApptTypeId < 1) Then
                    GoodDr = True
                End If
            End If
            If (GoodDr) Then
                If (ButtonCnt = 1) Then
                    cmdAppt1.Text = RetrieveResource.ResourceDescription
                    cmdAppt1.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt1.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt1.Visible = True
                ElseIf (ButtonCnt = 2) Then
                    cmdAppt2.Text = RetrieveResource.ResourceDescription
                    cmdAppt2.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt2.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt2.Visible = True
                ElseIf (ButtonCnt = 3) Then
                    cmdAppt3.Text = RetrieveResource.ResourceDescription
                    cmdAppt3.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt3.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt3.Visible = True
                ElseIf (ButtonCnt = 4) Then
                    cmdAppt4.Text = RetrieveResource.ResourceDescription
                    cmdAppt4.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt4.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt4.Visible = True
                ElseIf (ButtonCnt = 5) Then
                    cmdAppt5.Text = RetrieveResource.ResourceDescription
                    cmdAppt5.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt5.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt5.Visible = True
                ElseIf (ButtonCnt = 6) Then
                    cmdAppt6.Text = RetrieveResource.ResourceDescription
                    cmdAppt6.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt6.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt6.Visible = True
                ElseIf (ButtonCnt = 7) Then
                    cmdAppt7.Text = RetrieveResource.ResourceDescription
                    cmdAppt7.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt7.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt7.Visible = True
                ElseIf (ButtonCnt = 8) Then
                    cmdAppt8.Text = RetrieveResource.ResourceDescription
                    cmdAppt8.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt8.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt8.Visible = True
                ElseIf (ButtonCnt = 9) Then
                    cmdAppt9.Text = RetrieveResource.ResourceDescription
                    cmdAppt9.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt9.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt9.Visible = True
                ElseIf (ButtonCnt = 10) Then
                    cmdAppt10.Text = RetrieveResource.ResourceDescription
                    cmdAppt10.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt10.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt10.Visible = True
                ElseIf (ButtonCnt = 11) Then
                    cmdAppt11.Text = RetrieveResource.ResourceDescription
                    cmdAppt11.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt11.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt11.Visible = True
                ElseIf (ButtonCnt = 12) Then
                    cmdAppt12.Text = RetrieveResource.ResourceDescription
                    cmdAppt12.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt12.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt12.Visible = True
                ElseIf (ButtonCnt = 13) Then
                    cmdAppt13.Text = RetrieveResource.ResourceDescription
                    cmdAppt13.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt13.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt13.Visible = True
                ElseIf (ButtonCnt = 14) Then
                    cmdAppt14.Text = RetrieveResource.ResourceDescription
                    cmdAppt14.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt14.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt14.Visible = True
                ElseIf (ButtonCnt = 15) Then
                    cmdAppt15.Text = RetrieveResource.ResourceDescription
                    cmdAppt15.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt15.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt15.Visible = True
                ElseIf (ButtonCnt = 16) Then
                    cmdAppt16.Text = RetrieveResource.ResourceDescription
                    cmdAppt16.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt16.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt16.Visible = True
                ElseIf (ButtonCnt = 17) Then
                    cmdAppt17.Text = RetrieveResource.ResourceDescription
                    cmdAppt17.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt17.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt17.Visible = True
                ElseIf (ButtonCnt = 18) Then
                    cmdAppt18.Text = RetrieveResource.ResourceDescription
                    cmdAppt18.Tag = Trim(Str(RetrieveResource.ResourceId))
                    cmdAppt18.ToolTipText = Trim(RetrieveResource.ResourceName)
                    cmdAppt18.Visible = True
                End If
                LoadAppt = True
            End If
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetApptType = Nothing
        Set RetrieveResource = Nothing
    End If
ElseIf (TheType = "C") Or (TheType = "O") Or (TheType = "B") Or (TheType = "W") Then
    If (Init) Then
        Erase AdminList
        CurrentAdmin = 0
    End If
    cmdDone.Visible = True
    Set RetCode = New PracticeCodes
    If (TheType = "C") Then
        lblField.Caption = "Select the Type of Surgery"
        RetCode.ReferenceType = "SURGERYTYPE"
        RetCode.ReferenceCode = ""
    ElseIf (TheType = "O") Then
        MaxItems = 5
        lblField.Caption = "Office Procedures"
        RetCode.ReferenceType = "OfficeProcedures"
        RetCode.ReferenceCode = ""
    ElseIf (TheType = "B") Then
        MaxItems = 5
        lblField.Caption = ApptTypeName
        RetCode.ReferenceType = "ExternalTestLinks"
        RetCode.ReferenceCode = ApptTypeName + ":"
    ElseIf (TheType = "W") Then
        MaxItems = 5
        lblField.Caption = "Administrative CheckList"
        RetCode.ReferenceType = "PatientAdmin"
    End If
    If (TheType = "B") Then
        TotalAppts = RetCode.FindCodePartial
        Call RetCode.SelectCode(1)
        If (InStrPS(RetCode.ReferenceCode, ApptTypeName) = 0) Then
            TotalAppts = 0
        End If
    Else
        TotalAppts = RetCode.FindCode
    End If
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetCode.SelectCode(i)) And (ButtonCnt < 19)
            DontCare = False
            If (TheType = "B") Then
                If (Left(RetCode.ReferenceCode, Len(ApptTypeName)) = ApptTypeName) Then
                    DontCare = True
                End If
            Else
                DontCare = True
            End If
            If (DontCare) Then
                DisplayText = Trim(RetCode.ReferenceCode)
                If (ButtonCnt = 1) Then
                    cmdAppt1.Text = DisplayText
                    cmdAppt1.Tag = "0"
                    cmdAppt1.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt1)
                    End If
                ElseIf (ButtonCnt = 2) Then
                    cmdAppt2.Text = DisplayText
                    cmdAppt2.Tag = "0"
                    cmdAppt2.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt2)
                    End If
                ElseIf (ButtonCnt = 3) Then
                    cmdAppt3.Text = DisplayText
                    cmdAppt3.Tag = "0"
                    cmdAppt3.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt3)
                    End If
                ElseIf (ButtonCnt = 4) Then
                    cmdAppt4.Text = DisplayText
                    cmdAppt4.Tag = "0"
                    cmdAppt4.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt4)
                    End If
                ElseIf (ButtonCnt = 5) Then
                    cmdAppt5.Text = DisplayText
                    cmdAppt5.Tag = "0"
                    cmdAppt5.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt5)
                    End If
                ElseIf (ButtonCnt = 6) Then
                    cmdAppt6.Text = DisplayText
                    cmdAppt6.Tag = "0"
                    cmdAppt6.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt6)
                    End If
                ElseIf (ButtonCnt = 7) Then
                    cmdAppt7.Text = DisplayText
                    cmdAppt7.Tag = "0"
                    cmdAppt7.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt7)
                    End If
                ElseIf (ButtonCnt = 8) Then
                    cmdAppt8.Text = DisplayText
                    cmdAppt8.Tag = "0"
                    cmdAppt8.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt8)
                    End If
                ElseIf (ButtonCnt = 9) Then
                    cmdAppt9.Text = DisplayText
                    cmdAppt9.Tag = "0"
                    cmdAppt9.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt9)
                    End If
                ElseIf (ButtonCnt = 10) Then
                    cmdAppt10.Text = DisplayText
                    cmdAppt10.Tag = "0"
                    cmdAppt10.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt10)
                    End If
                ElseIf (ButtonCnt = 11) Then
                    cmdAppt11.Text = DisplayText
                    cmdAppt11.Tag = "0"
                    cmdAppt11.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt11)
                    End If
                ElseIf (ButtonCnt = 12) Then
                    cmdAppt12.Text = DisplayText
                    cmdAppt12.Tag = "0"
                    cmdAppt12.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt12)
                    End If
                ElseIf (ButtonCnt = 13) Then
                    cmdAppt13.Text = DisplayText
                    cmdAppt13.Tag = "0"
                    cmdAppt13.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt13)
                    End If
                ElseIf (ButtonCnt = 14) Then
                    cmdAppt14.Text = DisplayText
                    cmdAppt14.Tag = "0"
                    cmdAppt14.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt14)
                    End If
                ElseIf (ButtonCnt = 15) Then
                    cmdAppt15.Text = DisplayText
                    cmdAppt15.Tag = "0"
                    cmdAppt15.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt15)
                    End If
                ElseIf (ButtonCnt = 16) Then
                    cmdAppt16.Text = DisplayText
                    cmdAppt16.Tag = "0"
                    cmdAppt16.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt16)
                    End If
                ElseIf (ButtonCnt = 17) Then
                    cmdAppt17.Text = DisplayText
                    cmdAppt17.Tag = "0"
                    cmdAppt17.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt17)
                    End If
                ElseIf (ButtonCnt = 18) Then
                    cmdAppt18.Text = DisplayText
                    cmdAppt18.Tag = "0"
                    cmdAppt18.Visible = True
                    If (IsReferenceMatch(DisplayText)) Then
                        Call SetItemOn(cmdAppt18)
                    End If
                End If
                ButtonCnt = ButtonCnt + 1
                LoadAppt = True
            End If
            i = i + 1
        Wend
        CurrentIndex = i - 1
        Set RetCode = Nothing
    End If
ElseIf (TheType = "v") Then
    TheComplaintCategory = ""
    cmdDone.Visible = False
    lblField.Caption = "Reason For Visit Category"
    Set RetQues = New DynamicClass
    RetQues.QuestionFamily = 10
    RetQues.QuestionSet = "First Visit"
    TotalAppts = RetQues.FindQuestionbyFamily
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetQues.SelectClassForm(i)) And (ButtonCnt < 19)
            DisplayText = Trim(RetQues.Question)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = "0"
                cmdAppt1.ToolTipText = ""
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = "0"
                cmdAppt2.ToolTipText = ""
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = "0"
                cmdAppt3.Visible = True
                cmdAppt3.ToolTipText = ""
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = "0"
                cmdAppt4.Visible = True
                cmdAppt4.ToolTipText = ""
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = "0"
                cmdAppt5.ToolTipText = ""
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = "0"
                cmdAppt6.ToolTipText = ""
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = "0"
                cmdAppt7.ToolTipText = ""
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = "0"
                cmdAppt8.ToolTipText = ""
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = "0"
                cmdAppt9.ToolTipText = ""
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = "0"
                cmdAppt10.ToolTipText = ""
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = "0"
                cmdAppt11.ToolTipText = ""
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = "0"
                cmdAppt12.ToolTipText = ""
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = "0"
                cmdAppt13.ToolTipText = ""
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = "0"
                cmdAppt14.ToolTipText = ""
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = "0"
                cmdAppt15.ToolTipText = ""
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = "0"
                cmdAppt16.ToolTipText = ""
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = "0"
                cmdAppt17.ToolTipText = ""
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = "0"
                cmdAppt18.ToolTipText = ""
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetQues = Nothing
    End If
ElseIf (TheType = "Z") Or (TheType = "h") Or (TheType = "&") Or (TheType = "T") Or (TheType = "V") Or (TheType = "v") Then
    If (Init) Then
        Erase AdminList
        CurrentAdmin = 0
    End If
    cmdDone.Visible = True
    lblField.Caption = "Administrative CheckList"
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "PatientAdmin"
    RetCode.ReferenceCode = ""
    If (TheType = "h") Then
        lblField.Caption = "Facility Admission Forms"
        RetCode.ReferenceType = "FacilityAdmission"
    ElseIf (TheType = "&") Then
        lblField.Caption = "Surgical Requests"
        RetCode.ReferenceType = "SurgicalRequests"
    ElseIf (TheType = "T") Then
        lblField.Caption = "External Tests"
        RetCode.ReferenceType = "External Tests"
    ElseIf (TheType = "V") Then
        lblField.Caption = "Reason For Visit"
        RetCode.ReferenceType = "CHIEFCOMPLAINTS"
        RetCode.ReferenceAlternateCode = TheComplaintCategory
    End If
    If (TheType = "V") Then
        TotalAppts = RetCode.FindAlternateCode
    Else
        TotalAppts = RetCode.FindCode
    End If
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetCode.SelectCode(i)) And (ButtonCnt < 19)
            DisplayText = Trim(RetCode.ReferenceCode)
            If (TheType <> "V") Then
                If (Left(DisplayText, 1) >= "0") And (Left(DisplayText, 1) <= "9") Then
                    k = InStrPS(DisplayText, "-")
                    If (k > 0) Then
                        DisplayText = Trim(Mid(DisplayText, k + 1, Len(DisplayText) - k))
                    End If
                End If
            End If
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = "0"
                cmdAppt1.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt1.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt1)
                End If
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = "0"
                cmdAppt2.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt2.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt2)
                End If
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = "0"
                cmdAppt3.Visible = True
                cmdAppt3.ToolTipText = RetCode.ReferenceAlternateCode
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt3)
                End If
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = "0"
                cmdAppt4.Visible = True
                cmdAppt4.ToolTipText = RetCode.ReferenceAlternateCode
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt4)
                End If
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = "0"
                cmdAppt5.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt5.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt5)
                End If
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = "0"
                cmdAppt6.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt6.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt6)
                End If
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = "0"
                cmdAppt7.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt7.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt7)
                End If
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = "0"
                cmdAppt8.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt8.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt8)
                End If
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = "0"
                cmdAppt9.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt9.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt9)
                End If
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = "0"
                cmdAppt10.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt10.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt10)
                End If
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = "0"
                cmdAppt11.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt11.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt11)
                End If
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = "0"
                cmdAppt12.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt12.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt12)
                End If
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = "0"
                cmdAppt13.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt13.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt13)
                End If
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = "0"
                cmdAppt14.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt14.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt14)
                End If
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = "0"
                cmdAppt15.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt15.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt15)
                End If
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = "0"
                cmdAppt16.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt16.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt16)
                End If
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = "0"
                cmdAppt17.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt17.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt17)
                End If
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = "0"
                cmdAppt18.ToolTipText = RetCode.ReferenceAlternateCode
                cmdAppt18.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt18)
                End If
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetCode = Nothing
    End If
ElseIf (TheType = "!") Or (TheType = "'") Then
    cmdDone.Visible = False
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "CLINICALTEMPLATES"
    RetCode.ReferenceCode = ""
    If (TheType = "!") Then
        RetCode.ReferenceAlternateCode = "S"
        lblField.Caption = "Post Status Templates"
    Else
        RetCode.ReferenceAlternateCode = "F"
        lblField.Caption = "Favorite Findings Templates"
    End If
    TotalAppts = RetCode.FindAlternateCode
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetCode.SelectCode(i)) And (ButtonCnt < 19)
            DisplayText = Trim(RetCode.ReferenceCode)
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = "0"
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = "0"
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = "0"
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = "0"
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = "0"
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = "0"
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = "0"
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = "0"
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = "0"
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = "0"
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = "0"
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = "0"
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = "0"
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = "0"
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = "0"
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = "0"
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = "0"
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = "0"
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetCode = Nothing
    End If
ElseIf (TheType = "~") And (lstBox.ListCount > 0) Then
    cmdDelete.Visible = True
    If (lstBox.Name = "lstOS") Then
        lblField.Caption = "Select an OS Finding"
        If (ApptTypeId = -1) Then
            cmdDelete.Visible = False
        End If
    ElseIf (lstBox.Name = "lstOD") Then
        lblField.Caption = "Select an OD Finding"
        If (ApptTypeId = -1) Then
            cmdDelete.Visible = False
        End If
    ElseIf (lstBox.Name = "lstNonOcular") Then
        lblField.Caption = "Select a NonOcular Drug"
        cmdDelete.Visible = False
    End If
    TotalAppts = lstBox.ListCount
    If (TotalAppts > 0) Then
        If (CurrentIndex < 1) Then
            CurrentIndex = 1
        End If
        i = CurrentIndex - 1
        ButtonCnt = 1
        While (ButtonCnt < 19) And (Trim(lstBox.List(i)) <> "")
            DisplayText = Trim(Left(lstBox.List(i), 40))
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = Trim(Str(i))
                cmdAppt1.Visible = True
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = Trim(Str(i))
                cmdAppt2.Visible = True
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = Trim(Str(i))
                cmdAppt3.Visible = True
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = Trim(Str(i))
                cmdAppt4.Visible = True
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = Trim(Str(i))
                cmdAppt5.Visible = True
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = Trim(Str(i))
                cmdAppt6.Visible = True
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = Trim(Str(i))
                cmdAppt7.Visible = True
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = Trim(Str(i))
                cmdAppt8.Visible = True
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = Trim(Str(i))
                cmdAppt9.Visible = True
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = Trim(Str(i))
                cmdAppt10.Visible = True
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = Trim(Str(i))
                cmdAppt11.Visible = True
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = Trim(Str(i))
                cmdAppt12.Visible = True
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = Trim(Str(i))
                cmdAppt13.Visible = True
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = Trim(Str(i))
                cmdAppt14.Visible = True
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = Trim(Str(i))
                cmdAppt15.Visible = True
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = Trim(Str(i))
                cmdAppt16.Visible = True
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = Trim(Str(i))
                cmdAppt17.Visible = True
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = Trim(Str(i))
                cmdAppt18.Visible = True
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
'        CurrentIndex = i - 1
        CurrentIndex = i
    End If
ElseIf (TheType = "?") And (lstBox.ListCount > 1) Then
    cmdQuit.Visible = False
    cmdDelete.Visible = False
    cmdDone.Visible = True
    cmdAnother.Visible = True
    lblField.Caption = "Select an Item"
    TotalAppts = lstBox.ListCount
    If (TotalAppts > 0) Then
        i = CurrentIndex
        If (CurrentIndex >= TotalAppts) Then
            i = 1
        End If
        ButtonCnt = 1
        While (ButtonCnt < 19) And (Trim(lstBox.List(i - 1)) <> "")
            If (Len(lstBox.List(i - 1)) > 10) And _
               ((InStrPS(UCase(lstBox.List(i - 1)), "DRUGS TAKEN") > 0) Or _
                (InStrPS(UCase(lstBox.List(i - 1)), "OTHER DIAGNOSIS") > 0) Or _
                (Left(lstBox.List(i - 1), 3) = "(H)") Or _
                (InStrPS(lstBox.List(i - 1), ":") = 0)) Then
                k = i - 1
                DisplayText = Trim(Left(lstBox.List(i - 1), 40))
                TrapText = Mid(lstBox.List(i - 1), 76, 5)
                If (Trim(TrapText) = "") Then
                    TrapText = Mid(lstBox.List(i - 1), 61, 5)
                End If
                If ((Mid(lstBox.List(i), 76, 5) = TrapText) Or (Mid(lstBox.List(i), 61, 5) = TrapText)) And (Left(lstBox.List(i), 3) <> "(T)") And (Left(lstBox.List(i), 3) <> "(H)") Then
                    DisplayText = DisplayText + Trim(Left(lstBox.List(i), 40))
                    i = i + 1
                    If (i < TotalAppts) Then
                        If ((Mid(lstBox.List(i), 76, 5) = TrapText) Or (Mid(lstBox.List(i), 61, 5) = TrapText)) And (Left(lstBox.List(i), 3) <> "(T)") And (Left(lstBox.List(i), 3) <> "(H)") Then
                            DisplayText = DisplayText + Trim(Left(lstBox.List(i), 40))
                            i = i + 1
                            If (i < TotalAppts) Then
                                If ((Mid(lstBox.List(i), 76, 5) = TrapText) Or (Mid(lstBox.List(i), 61, 5) = TrapText)) And (Left(lstBox.List(i), 3) <> "(T)") And (Left(lstBox.List(i), 3) <> "(H)") Then
                                    DisplayText = DisplayText + Trim(Left(lstBox.List(i + 1), 40))
                                End If
                            End If
                        End If
                    End If
                End If
                If (ButtonCnt = 1) Then
                    cmdAppt1.Text = DisplayText
                    cmdAppt1.Tag = Trim(Str(k))
                    cmdAppt1.Visible = True
                ElseIf (ButtonCnt = 2) Then
                    cmdAppt2.Text = DisplayText
                    cmdAppt2.Tag = Trim(Str(k))
                    cmdAppt2.Visible = True
                ElseIf (ButtonCnt = 3) Then
                    cmdAppt3.Text = DisplayText
                    cmdAppt3.Tag = Trim(Str(k))
                    cmdAppt3.Visible = True
                ElseIf (ButtonCnt = 4) Then
                    cmdAppt4.Text = DisplayText
                    cmdAppt4.Tag = Trim(Str(k))
                    cmdAppt4.Visible = True
                ElseIf (ButtonCnt = 5) Then
                    cmdAppt5.Text = DisplayText
                    cmdAppt5.Tag = Trim(Str(k))
                    cmdAppt5.Visible = True
                ElseIf (ButtonCnt = 6) Then
                    cmdAppt6.Text = DisplayText
                    cmdAppt6.Tag = Trim(Str(k))
                    cmdAppt6.Visible = True
                ElseIf (ButtonCnt = 7) Then
                    cmdAppt7.Text = DisplayText
                    cmdAppt7.Tag = Trim(Str(k))
                    cmdAppt7.Visible = True
                ElseIf (ButtonCnt = 8) Then
                    cmdAppt8.Text = DisplayText
                    cmdAppt8.Tag = Trim(Str(k))
                    cmdAppt8.Visible = True
                ElseIf (ButtonCnt = 9) Then
                    cmdAppt9.Text = DisplayText
                    cmdAppt9.Tag = Trim(Str(k))
                    cmdAppt9.Visible = True
                ElseIf (ButtonCnt = 10) Then
                    cmdAppt10.Text = DisplayText
                    cmdAppt10.Tag = Trim(Str(k))
                    cmdAppt10.Visible = True
                ElseIf (ButtonCnt = 11) Then
                    cmdAppt11.Text = DisplayText
                    cmdAppt11.Tag = Trim(Str(k))
                    cmdAppt11.Visible = True
                ElseIf (ButtonCnt = 12) Then
                    cmdAppt12.Text = DisplayText
                    cmdAppt12.Tag = Trim(Str(k))
                    cmdAppt12.Visible = True
                ElseIf (ButtonCnt = 13) Then
                    cmdAppt13.Text = DisplayText
                    cmdAppt13.Tag = Trim(Str(k))
                    cmdAppt13.Visible = True
                ElseIf (ButtonCnt = 14) Then
                    cmdAppt14.Text = DisplayText
                    cmdAppt14.Tag = Trim(Str(k))
                    cmdAppt14.Visible = True
                ElseIf (ButtonCnt = 15) Then
                    cmdAppt15.Text = DisplayText
                    cmdAppt15.Tag = Trim(Str(k))
                    cmdAppt15.Visible = True
                ElseIf (ButtonCnt = 16) Then
                    cmdAppt16.Text = DisplayText
                    cmdAppt16.Tag = Trim(Str(k))
                    cmdAppt16.Visible = True
                ElseIf (ButtonCnt = 17) Then
                    cmdAppt17.Text = DisplayText
                    cmdAppt17.Tag = Trim(Str(k))
                    cmdAppt17.Visible = True
                ElseIf (ButtonCnt = 18) Then
                    cmdAppt18.Text = DisplayText
                    cmdAppt18.Tag = Trim(Str(k))
                    cmdAppt18.Visible = True
                End If
                LoadAppt = True
                ButtonCnt = ButtonCnt + 1
                If (Len(lstBox.List(i)) >= 70) Then
                    If (Left(lstBox.List(i), 3) <> "(T)") And (Left(lstBox.List(i), 3) <> "(H)") And (Left(lstBox.List(i), 1) <> "-") And (Len(lstBox.List(i)) < 70) Then
                        i = i + 1
                    End If
                End If
            End If
            i = i + 1
        Wend
        CurrentIndex = i - 1
    End If
ElseIf (TheType = "`") Or (TheType = ")") Then
    TrapText = ""
    cmdDelete.Visible = False
    BreakPoint = BreakPoint * 3
    If (TheType = ")") Then
        cmdAnother.Visible = True
    End If
    lblField.Caption = "Select an Item"
    TotalAppts = lstBox.ListCount
    If (TotalAppts > 1) Or (((Left(lstBox.List(0), 2) = "RV") Or (Left(lstBox.List(0), 2) = "CC")) And (Len(Trim(lstBox.List(0))) > 3)) Then
        i = CurrentIndex
        If (CurrentIndex >= TotalAppts) Or (CurrentIndex < 1) Then
            i = 1
        End If
        ButtonCnt = 1
        While (ButtonCnt < 19) And (Trim(lstBox.List(i - 1)) <> "")
            If (InStrPS(UCase(lstBox.List(i - 1)), UCase(ApptTypeName)) <> 0) And _
               ((Len(lstBox.List(i - 1)) > 60)) And (Left(lstBox.List(i - 1), 3) <> "(P)") Then
                k = i - 1
                k = i - 1
                TrapText = Mid(lstBox.List(i - 1), 76, 5)
                If (Trim(TrapText) = "") Then
                    TrapText = Mid(lstBox.List(i - 1), 61, 5)
                End If
                DisplayText = Trim(Left(lstBox.List(i - 1), 40))
                If ((Mid(lstBox.List(i), 76, 5) = TrapText) Or (Mid(lstBox.List(i), 61, 5) = TrapText)) Then
                    If (Left(lstBox.List(i), 1) = "(") Then
                        DisplayText = DisplayText + " " + Trim(Mid(lstBox.List(i), 4, 40))
                    Else
                        DisplayText = DisplayText + Trim(Left(lstBox.List(i), 40))
                    End If
                    i = i + 1
                    If (i < TotalAppts) Then
                        If (Mid(lstBox.List(i), 76, 5) = TrapText) And (Left(lstBox.List(i), 3) <> "(T)") And (Left(lstBox.List(i), 3) <> "(H)") Then
                            DisplayText = DisplayText + Trim(Left(lstBox.List(i), 40))
                            i = i + 1
                            If (i < TotalAppts) Then
                                If (Mid(lstBox.List(i), 76, 5) = TrapText) And (Left(lstBox.List(i), 3) <> "(T)") And (Left(lstBox.List(i), 3) <> "(H)") Then
                                    DisplayText = DisplayText + Trim(Left(lstBox.List(i + 1), 40))
                                    i = i + 1
                                End If
                            End If
                        End If
                    End If
                End If
                If (ButtonCnt = 1) Then
                    cmdAppt1.Text = DisplayText
                    cmdAppt1.Tag = Trim(Str(k))
                    cmdAppt1.Visible = True
                ElseIf (ButtonCnt = 2) Then
                    cmdAppt2.Text = DisplayText
                    cmdAppt2.Tag = Trim(Str(k))
                    cmdAppt2.Visible = True
                ElseIf (ButtonCnt = 3) Then
                    cmdAppt3.Text = DisplayText
                    cmdAppt3.Tag = Trim(Str(k))
                    cmdAppt3.Visible = True
                ElseIf (ButtonCnt = 4) Then
                    cmdAppt4.Text = DisplayText
                    cmdAppt4.Tag = Trim(Str(k))
                    cmdAppt4.Visible = True
                ElseIf (ButtonCnt = 5) Then
                    cmdAppt5.Text = DisplayText
                    cmdAppt5.Tag = Trim(Str(k))
                    cmdAppt5.Visible = True
                ElseIf (ButtonCnt = 6) Then
                    cmdAppt6.Text = DisplayText
                    cmdAppt6.Tag = Trim(Str(k))
                    cmdAppt6.Visible = True
                ElseIf (ButtonCnt = 7) Then
                    cmdAppt7.Text = DisplayText
                    cmdAppt7.Tag = Trim(Str(k))
                    cmdAppt7.Visible = True
                ElseIf (ButtonCnt = 8) Then
                    cmdAppt8.Text = DisplayText
                    cmdAppt8.Tag = Trim(Str(k))
                    cmdAppt8.Visible = True
                ElseIf (ButtonCnt = 9) Then
                    cmdAppt9.Text = DisplayText
                    cmdAppt9.Tag = Trim(Str(k))
                    cmdAppt9.Visible = True
                ElseIf (ButtonCnt = 10) Then
                    cmdAppt10.Text = DisplayText
                    cmdAppt10.Tag = Trim(Str(k))
                    cmdAppt10.Visible = True
                ElseIf (ButtonCnt = 11) Then
                    cmdAppt11.Text = DisplayText
                    cmdAppt11.Tag = Trim(Str(k))
                    cmdAppt11.Visible = True
                ElseIf (ButtonCnt = 12) Then
                    cmdAppt12.Text = DisplayText
                    cmdAppt12.Tag = Trim(Str(k))
                    cmdAppt12.Visible = True
                ElseIf (ButtonCnt = 13) Then
                    cmdAppt13.Text = DisplayText
                    cmdAppt13.Tag = Trim(Str(k))
                    cmdAppt13.Visible = True
                ElseIf (ButtonCnt = 14) Then
                    cmdAppt14.Text = DisplayText
                    cmdAppt14.Tag = Trim(Str(k))
                    cmdAppt14.Visible = True
                ElseIf (ButtonCnt = 15) Then
                    cmdAppt15.Text = DisplayText
                    cmdAppt15.Tag = Trim(Str(k))
                    cmdAppt15.Visible = True
                ElseIf (ButtonCnt = 16) Then
                    cmdAppt16.Text = DisplayText
                    cmdAppt16.Tag = Trim(Str(k))
                    cmdAppt16.Visible = True
                ElseIf (ButtonCnt = 17) Then
                    cmdAppt17.Text = DisplayText
                    cmdAppt17.Tag = Trim(Str(k))
                    cmdAppt17.Visible = True
                ElseIf (ButtonCnt = 18) Then
                    cmdAppt18.Text = DisplayText
                    cmdAppt18.Tag = Trim(Str(k))
                    cmdAppt18.Visible = True
                End If
                LoadAppt = True
                ButtonCnt = ButtonCnt + 1
            End If
            i = i + 1
        Wend
        CurrentIndex = i - 1
        ApptTypeName = ""
    ElseIf (InStrPS(lstBox.List(0), "MED") <> 0) Then
        LoadAppt = True
    End If
ElseIf (TheType = "]") And (lstBox.ListCount > 1) Then
    BreakPoint = BreakPoint * 3
    cmdQuit.Visible = False
    cmdDelete.Visible = False
    cmdDelete.Text = "De-Select ALL"
    cmdDone.Visible = True
    cmdAnother.Visible = True
    lblField.Caption = "Select an Item"
    TotalAppts = lstBox.ListCount
    If (TotalAppts > 0) Then
        i = CurrentIndex
        If (CurrentIndex >= TotalAppts) Then
            i = 1
        End If
        ButtonCnt = 1
        While (ButtonCnt < 19) And (Trim(lstBox.List(i - 1)) <> "")
            If ((i - 1) > 0) Then
                DisplayText = ""
                If (Left(lstBox.List(i - 1), 2) <> "  ") And (Left(lstBox.List(i - 1), 5) <> "Close") Then
                    k = i
                    DisplayText = Trim(Left(lstBox.List(i - 1), 120))
                    z = InStrPS(DisplayText, "/")
                    While (z > 0)
                        If (Mid(DisplayText, z - 2, 1) = "-") Then
                            DisplayText = Left(DisplayText, z - 2) + Mid(DisplayText, z + 1, Len(DisplayText) - z)
                        Else
                            DisplayText = Left(DisplayText, z - 1) + Mid(DisplayText, z + 1, Len(DisplayText) - z)
                        End If
                        z = InStrPS(DisplayText, "/")
                    Wend
                    Call ReplaceCharacters(DisplayText, "--", " ")
                    Call ReplaceCharacters(DisplayText, ">-", "> -")
                End If
                If (DisplayText <> "") Then
                    If (ButtonCnt = 1) Then
                        cmdAppt1.Text = DisplayText
                        cmdAppt1.Tag = Trim(Str(k - 1))
                        cmdAppt1.AlignTextH = fpAlignTextHLeft
                        cmdAppt1.AlignTextV = fpAlignTextVTop
                        cmdAppt1.Visible = True
                    ElseIf (ButtonCnt = 2) Then
                        cmdAppt2.Text = DisplayText
                        cmdAppt2.Tag = Trim(Str(k - 1))
                        cmdAppt2.AlignTextH = fpAlignTextHLeft
                        cmdAppt2.AlignTextV = fpAlignTextVTop
                        cmdAppt2.Visible = True
                    ElseIf (ButtonCnt = 3) Then
                        cmdAppt3.Text = DisplayText
                        cmdAppt3.Tag = Trim(Str(k - 1))
                        cmdAppt3.AlignTextH = fpAlignTextHLeft
                        cmdAppt3.AlignTextV = fpAlignTextVTop
                        cmdAppt3.Visible = True
                    ElseIf (ButtonCnt = 4) Then
                        cmdAppt4.Text = DisplayText
                        cmdAppt4.Tag = Trim(Str(k - 1))
                        cmdAppt4.AlignTextH = fpAlignTextHLeft
                        cmdAppt4.AlignTextV = fpAlignTextVTop
                        cmdAppt4.Visible = True
                    ElseIf (ButtonCnt = 5) Then
                        cmdAppt5.Text = DisplayText
                        cmdAppt5.Tag = Trim(Str(k - 1))
                        cmdAppt5.AlignTextH = fpAlignTextHLeft
                        cmdAppt5.AlignTextV = fpAlignTextVTop
                        cmdAppt5.Visible = True
                    ElseIf (ButtonCnt = 6) Then
                        cmdAppt6.Text = DisplayText
                        cmdAppt6.Tag = Trim(Str(k - 1))
                        cmdAppt6.AlignTextH = fpAlignTextHLeft
                        cmdAppt6.AlignTextV = fpAlignTextVTop
                        cmdAppt6.Visible = True
                    ElseIf (ButtonCnt = 7) Then
                        cmdAppt7.Text = DisplayText
                        cmdAppt7.Tag = Trim(Str(k - 1))
                        cmdAppt7.AlignTextH = fpAlignTextHLeft
                        cmdAppt7.AlignTextV = fpAlignTextVTop
                        cmdAppt7.Visible = True
                    ElseIf (ButtonCnt = 8) Then
                        cmdAppt8.Text = DisplayText
                        cmdAppt8.Tag = Trim(Str(k - 1))
                        cmdAppt8.AlignTextH = fpAlignTextHLeft
                        cmdAppt8.AlignTextV = fpAlignTextVTop
                        cmdAppt8.Visible = True
                    ElseIf (ButtonCnt = 9) Then
                        cmdAppt9.Text = DisplayText
                        cmdAppt9.Tag = Trim(Str(k - 1))
                        cmdAppt9.AlignTextH = fpAlignTextHLeft
                        cmdAppt9.AlignTextV = fpAlignTextVTop
                        cmdAppt9.Visible = True
                    ElseIf (ButtonCnt = 10) Then
                        cmdAppt10.Text = DisplayText
                        cmdAppt10.Tag = Trim(Str(k - 1))
                        cmdAppt10.AlignTextH = fpAlignTextHLeft
                        cmdAppt10.AlignTextV = fpAlignTextVTop
                        cmdAppt10.Visible = True
                    ElseIf (ButtonCnt = 11) Then
                        cmdAppt11.Text = DisplayText
                        cmdAppt11.Tag = Trim(Str(k - 1))
                        cmdAppt11.AlignTextH = fpAlignTextHLeft
                        cmdAppt11.AlignTextV = fpAlignTextVTop
                        cmdAppt11.Visible = True
                    ElseIf (ButtonCnt = 12) Then
                        cmdAppt12.Text = DisplayText
                        cmdAppt12.Tag = Trim(Str(k - 1))
                        cmdAppt12.AlignTextH = fpAlignTextHLeft
                        cmdAppt12.AlignTextV = fpAlignTextVTop
                        cmdAppt12.Visible = True
                    ElseIf (ButtonCnt = 13) Then
                        cmdAppt13.Text = DisplayText
                        cmdAppt13.Tag = Trim(Str(k - 1))
                        cmdAppt13.AlignTextH = fpAlignTextHLeft
                        cmdAppt13.AlignTextV = fpAlignTextVTop
                        cmdAppt13.Visible = True
                    ElseIf (ButtonCnt = 14) Then
                        cmdAppt14.Text = DisplayText
                        cmdAppt14.Tag = Trim(Str(k - 1))
                        cmdAppt14.AlignTextH = fpAlignTextHLeft
                        cmdAppt14.AlignTextV = fpAlignTextVTop
                        cmdAppt14.Visible = True
                    ElseIf (ButtonCnt = 15) Then
                        cmdAppt15.Text = DisplayText
                        cmdAppt15.Tag = Trim(Str(k - 1))
                        cmdAppt15.AlignTextH = fpAlignTextHLeft
                        cmdAppt15.AlignTextV = fpAlignTextVTop
                        cmdAppt15.Visible = True
                    ElseIf (ButtonCnt = 16) Then
                        cmdAppt16.Text = DisplayText
                        cmdAppt16.Tag = Trim(Str(k - 1))
                        cmdAppt16.AlignTextH = fpAlignTextHLeft
                        cmdAppt16.AlignTextV = fpAlignTextVTop
                        cmdAppt16.Visible = True
                    ElseIf (ButtonCnt = 17) Then
                        cmdAppt17.Text = DisplayText
                        cmdAppt17.Tag = Trim(Str(k - 1))
                        cmdAppt17.AlignTextH = fpAlignTextHLeft
                        cmdAppt17.AlignTextV = fpAlignTextVTop
                        cmdAppt17.Visible = True
                    ElseIf (ButtonCnt = 18) Then
                        cmdAppt18.Text = DisplayText
                        cmdAppt18.Tag = Trim(Str(k - 1))
                        cmdAppt18.AlignTextH = fpAlignTextHLeft
                        cmdAppt18.AlignTextV = fpAlignTextVTop
                        cmdAppt18.Visible = True
                    End If
                    LoadAppt = True
                    ButtonCnt = ButtonCnt + 1
                End If
            End If
            i = i + 1
        Wend
        CurrentIndex = i - 1
        ApptTypeName = ""
    End If
ElseIf (TheType = "=") Then
    If (Init) Then
        Erase AdminList
        CurrentAdmin = 0
    End If
    cmdDone.Visible = True
    lblField.Caption = "Administrative CheckList"
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "PatientAdmin"
    RetCode.ReferenceCode = ""
    If (TheType = "h") Then
        lblField.Caption = "Facility Admission Forms"
        RetCode.ReferenceType = "FacilityAdmission"
    ElseIf (TheType = "&") Then
        lblField.Caption = "Surgical Requests"
        RetCode.ReferenceType = "SurgicalRequests"
    ElseIf (TheType = "T") Then
        lblField.Caption = "External Tests"
        RetCode.ReferenceType = "External Tests"
    ElseIf (TheType = "W") Then
        lblField.Caption = "Administrative CheckList"
        RetCode.ReferenceType = "PatientAdmin"
    End If
    TotalAppts = RetCode.FindCode
    If (TotalAppts > 0) Then
        i = CurrentIndex
        ButtonCnt = 1
        While (RetCode.SelectCode(i)) And (ButtonCnt < 19)
            DisplayText = Trim(RetCode.ReferenceCode)
            If (Left(DisplayText, 1) >= "0") And (Left(DisplayText, 1) <= "9") Then
                k = InStrPS(DisplayText, "-")
                If (k > 0) Then
                    DisplayText = Trim(Mid(DisplayText, k + 1, Len(DisplayText) - k))
                End If
            End If
            If (ButtonCnt = 1) Then
                cmdAppt1.Text = DisplayText
                cmdAppt1.Tag = "0"
                cmdAppt1.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt1)
                End If
            ElseIf (ButtonCnt = 2) Then
                cmdAppt2.Text = DisplayText
                cmdAppt2.Tag = "0"
                cmdAppt2.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt2)
                End If
            ElseIf (ButtonCnt = 3) Then
                cmdAppt3.Text = DisplayText
                cmdAppt3.Tag = "0"
                cmdAppt3.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt3)
                End If
            ElseIf (ButtonCnt = 4) Then
                cmdAppt4.Text = DisplayText
                cmdAppt4.Tag = "0"
                cmdAppt4.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt4)
                End If
            ElseIf (ButtonCnt = 5) Then
                cmdAppt5.Text = DisplayText
                cmdAppt5.Tag = "0"
                cmdAppt5.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt5)
                End If
            ElseIf (ButtonCnt = 6) Then
                cmdAppt6.Text = DisplayText
                cmdAppt6.Tag = "0"
                cmdAppt6.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt6)
                End If
            ElseIf (ButtonCnt = 7) Then
                cmdAppt7.Text = DisplayText
                cmdAppt7.Tag = "0"
                cmdAppt7.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt7)
                End If
            ElseIf (ButtonCnt = 8) Then
                cmdAppt8.Text = DisplayText
                cmdAppt8.Tag = "0"
                cmdAppt8.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt8)
                End If
            ElseIf (ButtonCnt = 9) Then
                cmdAppt9.Text = DisplayText
                cmdAppt9.Tag = "0"
                cmdAppt9.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt9)
                End If
            ElseIf (ButtonCnt = 10) Then
                cmdAppt10.Text = DisplayText
                cmdAppt10.Tag = "0"
                cmdAppt10.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt10)
                End If
            ElseIf (ButtonCnt = 11) Then
                cmdAppt11.Text = DisplayText
                cmdAppt11.Tag = "0"
                cmdAppt11.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt11)
                End If
            ElseIf (ButtonCnt = 12) Then
                cmdAppt12.Text = DisplayText
                cmdAppt12.Tag = "0"
                cmdAppt12.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt12)
                End If
            ElseIf (ButtonCnt = 13) Then
                cmdAppt13.Text = DisplayText
                cmdAppt13.Tag = "0"
                cmdAppt13.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt13)
                End If
            ElseIf (ButtonCnt = 14) Then
                cmdAppt14.Text = DisplayText
                cmdAppt14.Tag = "0"
                cmdAppt14.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt14)
                End If
            ElseIf (ButtonCnt = 15) Then
                cmdAppt15.Text = DisplayText
                cmdAppt15.Tag = "0"
                cmdAppt15.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt15)
                End If
            ElseIf (ButtonCnt = 16) Then
                cmdAppt16.Text = DisplayText
                cmdAppt16.Tag = "0"
                cmdAppt16.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt16)
                End If
            ElseIf (ButtonCnt = 17) Then
                cmdAppt17.Text = DisplayText
                cmdAppt17.Tag = "0"
                cmdAppt17.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt17)
                End If
            ElseIf (ButtonCnt = 18) Then
                cmdAppt18.Text = DisplayText
                cmdAppt18.Tag = "0"
                cmdAppt18.Visible = True
                If (IsReferenceMatch(DisplayText)) Then
                    Call SetItemOn(cmdAppt18)
                End If
            End If
            LoadAppt = True
            i = i + 1
            ButtonCnt = ButtonCnt + 1
        Wend
        CurrentIndex = i - 1
        Set RetCode = Nothing
    End If
End If
If (CurrentIndex <= TotalAppts) And (TotalAppts > 18) Then
    cmdMore.Text = "More"
    cmdMore.Visible = True
Else
    cmdMore.Text = "Beginning of List"
    cmdMore.Visible = True
End If
If (TotalAppts < BreakPoint - 1) Then
    cmdMore.Visible = False
End If
End Function

Private Sub Form_Load()
CurrentIndex = 1
BaseBackColor = cmdAppt1.BackColor
BaseForeColor = cmdAppt1.ForeColor
SetBackColor = 14745312
SetForeColor = 0
End Sub

Public Function GetAdminItem(Ref As Integer, Item As String) As Boolean
GetAdminItem = False
Item = ""
If (Ref > 0) And (Ref < 40) Then
    Item = Trim(AdminList(Ref))
    If (Item <> "") Then
        GetAdminItem = True
    End If
End If
End Function

Private Function GetEyes(AEye As String) As Boolean
GetEyes = True
AEye = ""
frmEventMsgs.Header = "Which Eye ?"
frmEventMsgs.AcceptText = "OD"
frmEventMsgs.RejectText = "OS"
frmEventMsgs.CancelText = "Not Needed"
frmEventMsgs.Other0Text = "OU"
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    AEye = " (OD)"
ElseIf (frmEventMsgs.Result = 2) Then
    AEye = " (OS)"
ElseIf (frmEventMsgs.Result = 3) Then
    AEye = " (OU)"
End If
End Function

Private Function ApplGetDiag(TheSystem As String, TheDiagnosis As String, TheName As String) As Boolean
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
ApplGetDiag = False
TheName = ""
If (Trim(TheDiagnosis) <> "") Then
    Set RetrieveDiagnosis = New DiagnosisMasterPrimary
    RetrieveDiagnosis.PrimarySystem = TheSystem
    RetrieveDiagnosis.PrimaryDiagnosis = ""
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = TheDiagnosis
    RetrieveDiagnosis.PrimaryLevel = 0
    RetrieveDiagnosis.PrimaryRank = 0
    RetrieveDiagnosis.PrimaryBilling = False
    If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
        If (RetrieveDiagnosis.SelectPrimary(1)) Then
            If (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
                TheName = Trim(RetrieveDiagnosis.PrimaryLingo)
            Else
                TheName = Trim(RetrieveDiagnosis.PrimaryName)
            End If
            ApplGetDiag = True
        End If
    End If
    Set RetrieveDiagnosis = Nothing
End If
End Function

