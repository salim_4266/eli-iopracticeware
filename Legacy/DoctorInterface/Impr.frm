VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmImpr 
   BackColor       =   &H00505050&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Impression Order"
   ClientHeight    =   2910
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5985
   ForeColor       =   &H0077742D&
   Icon            =   "Impr.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Impr.frx":000C
   ScaleHeight     =   2910
   ScaleWidth      =   5985
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtOrder 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2760
      MaxLength       =   2
      TabIndex        =   0
      Top             =   1080
      Width           =   855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   4320
      TabIndex        =   1
      Top             =   1800
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Impr.frx":36D5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   4
      Top             =   1800
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Impr.frx":3904
   End
   Begin VB.Label lblImpr 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Impression Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000009&
      Height          =   285
      Left            =   1995
      TabIndex        =   3
      Top             =   120
      Width           =   2025
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Enter the Order Number"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000009&
      Height          =   285
      Left            =   1800
      TabIndex        =   2
      Top             =   600
      Width           =   2715
   End
End
Attribute VB_Name = "frmImpr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public OrderNumber As Integer
Public MaxOrder As Integer
Public ImprName As String

Private Sub cmdDone_Click()
OrderNumber = Val(txtOrder.Text)
Unload frmImpr
End Sub

Private Sub cmdHome_Click()
OrderNumber = 0
Unload frmImpr
End Sub

Private Sub Form_Load()
lblImpr.Caption = ImprName
End Sub

Private Sub txtOrder_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    If (Val(txtOrder.Text) > MaxOrder) Then
        frmEventMsgs.Header = "Order Number Cannot Exceed" + str(MaxOrder)
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        txtOrder.Text = ""
        txtOrder.SetFocus
    End If
ElseIf Not (IsNumeric(Chr(KeyAscii))) Then
    frmEventMsgs.Header = "Must be a number [0,1,2,3,4,5,6,7,8,9]"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    txtOrder.Text = ""
    txtOrder.SetFocus
End If
End Sub
Public Function FrmClose()
 Call cmdHome_Click
 Unload Me
 End Function
