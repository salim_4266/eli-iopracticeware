VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmRxDetails 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   ClientHeight    =   9990
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12945
   ForeColor       =   &H00A39B6D&
   LinkTopic       =   "Form1"
   Picture         =   "RxDetails.frx":0000
   ScaleHeight     =   9990
   ScaleWidth      =   12945
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn cmdPO 
      Height          =   990
      Index           =   1
      Left            =   10080
      TabIndex        =   54
      Top             =   2400
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":36C9
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00C19B49&
      Height          =   855
      Index           =   0
      Left            =   2040
      ScaleHeight     =   795
      ScaleWidth      =   1755
      TabIndex        =   45
      Top             =   1080
      Width           =   1815
      Begin VB.Label lblDosage 
         Alignment       =   2  'Center
         BackColor       =   &H00C19B49&
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   0
         TabIndex        =   46
         Top             =   240
         Width           =   1725
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLT 
      Height          =   990
      Left            =   10560
      TabIndex        =   13
      Top             =   3840
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":38AF
   End
   Begin VB.TextBox txtResults 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   3120
      MultiLine       =   -1  'True
      TabIndex        =   10
      Top             =   7320
      Width           =   4575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSamStat 
      Height          =   990
      Index           =   1
      Left            =   10560
      TabIndex        =   9
      Top             =   6240
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":3A95
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOver 
      Height          =   990
      Left            =   7680
      TabIndex        =   8
      Top             =   6240
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":3C73
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRefills 
      Height          =   990
      Left            =   1560
      TabIndex        =   5
      Top             =   6240
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":3E5A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPills 
      Height          =   990
      Left            =   4440
      TabIndex        =   3
      Top             =   6240
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":4038
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDuration 
      Height          =   990
      Left            =   120
      TabIndex        =   4
      Top             =   6240
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":4224
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPO 
      Height          =   990
      Index           =   2
      Left            =   11040
      TabIndex        =   6
      Top             =   2400
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":4403
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAsPrescribed 
      Height          =   990
      Left            =   8760
      TabIndex        =   7
      Top             =   3840
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":45DC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1095
      Left            =   120
      TabIndex        =   11
      Top             =   7800
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":47C0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1095
      Left            =   10320
      TabIndex        =   12
      Top             =   7800
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":49F3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEye 
      Height          =   990
      Index           =   2
      Left            =   10560
      TabIndex        =   14
      Top             =   960
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":4C26
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEye 
      Height          =   990
      Index           =   0
      Left            =   7680
      TabIndex        =   15
      Top             =   960
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":4DFF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdEye 
      Height          =   990
      Index           =   1
      Left            =   9120
      TabIndex        =   16
      Top             =   960
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":4FD8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   1095
      Left            =   7680
      TabIndex        =   18
      Top             =   7560
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":51B1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrn 
      Height          =   990
      Left            =   10560
      TabIndex        =   19
      Top             =   4920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":53E9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRmvEyes 
      Height          =   990
      Left            =   10560
      TabIndex        =   20
      Top             =   0
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":55C3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDose 
      Height          =   870
      Index           =   0
      Left            =   120
      TabIndex        =   21
      Top             =   1080
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":57AA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDose 
      Height          =   870
      Index           =   1
      Left            =   1080
      TabIndex        =   22
      Top             =   1080
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":5982
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDose 
      Height          =   870
      Index           =   2
      Left            =   3840
      TabIndex        =   23
      Top             =   1080
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":5B5A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDose 
      Height          =   870
      Index           =   3
      Left            =   4800
      TabIndex        =   24
      Top             =   1080
      Width           =   975
      _Version        =   131072
      _ExtentX        =   1720
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":5D32
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStr 
      Height          =   990
      Index           =   0
      Left            =   120
      TabIndex        =   26
      Top             =   2400
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":5F0A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   975
      Index           =   0
      Left            =   120
      TabIndex        =   27
      Top             =   3840
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":60E1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   1
      Left            =   1560
      TabIndex        =   28
      Top             =   3840
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":62BB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   2
      Left            =   3000
      TabIndex        =   29
      Top             =   3840
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":6495
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   3
      Left            =   4440
      TabIndex        =   30
      Top             =   3840
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":666F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   4
      Left            =   5880
      TabIndex        =   31
      Top             =   3840
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":6848
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   5
      Left            =   7320
      TabIndex        =   32
      Top             =   3840
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":6A22
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   6
      Left            =   7320
      TabIndex        =   33
      Top             =   4920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":6BFC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   7
      Left            =   120
      TabIndex        =   34
      Top             =   4920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":6DD6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   8
      Left            =   1560
      TabIndex        =   35
      Top             =   4920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":6FB0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   9
      Left            =   3000
      TabIndex        =   36
      Top             =   4920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":718A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   10
      Left            =   4440
      TabIndex        =   37
      Top             =   4920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":7367
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFrequency 
      Height          =   990
      Index           =   11
      Left            =   5880
      TabIndex        =   38
      Top             =   4920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":7541
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSamStat 
      Height          =   990
      Index           =   0
      Left            =   9120
      TabIndex        =   39
      Top             =   6240
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":771B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDaw 
      Height          =   990
      Left            =   3000
      TabIndex        =   40
      Top             =   6240
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":78F6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDaysSupply 
      Height          =   990
      Left            =   5880
      TabIndex        =   47
      Top             =   6240
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":7AD0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStr 
      Height          =   990
      Index           =   1
      Left            =   1560
      TabIndex        =   48
      Top             =   2400
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":7CB2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStr 
      Height          =   990
      Index           =   2
      Left            =   3000
      TabIndex        =   49
      Top             =   2400
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":7E89
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStr 
      Height          =   990
      Index           =   3
      Left            =   4440
      TabIndex        =   50
      Top             =   2400
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":8060
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStr 
      Height          =   990
      Index           =   4
      Left            =   5880
      TabIndex        =   51
      Top             =   2400
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":8237
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdForm 
      Height          =   990
      Index           =   1
      Left            =   4680
      TabIndex        =   43
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":840E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdForm 
      Height          =   990
      Index           =   0
      Left            =   3240
      TabIndex        =   41
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":85E9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdForm 
      Height          =   990
      Index           =   2
      Left            =   6120
      TabIndex        =   25
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":87C0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPO 
      Height          =   990
      Index           =   0
      Left            =   9120
      TabIndex        =   53
      Top             =   2400
      Width           =   855
      _Version        =   131072
      _ExtentX        =   1508
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":899B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPharmNotes 
      Height          =   990
      Left            =   8760
      TabIndex        =   56
      Top             =   4920
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":8B7F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPO 
      Height          =   990
      Index           =   3
      Left            =   7680
      TabIndex        =   57
      Top             =   2400
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxDetails.frx":8D66
   End
   Begin VB.Label lblPO 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Route of Administration"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   7680
      TabIndex        =   55
      Top             =   2040
      Width           =   2700
   End
   Begin VB.Label lblPatient 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Name, age, dob "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   4320
      TabIndex        =   52
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label lblStrength 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Strength"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   120
      TabIndex        =   42
      Top             =   2040
      Width           =   990
   End
   Begin VB.Label lblEye 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Eye"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   7680
      TabIndex        =   17
      Top             =   600
      Width           =   435
   End
   Begin VB.Label lblFrequency 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Frequency"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   120
      TabIndex        =   2
      Top             =   3480
      Width           =   1230
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Dosage"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   945
   End
   Begin VB.Label lblRx 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "The Drug"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1110
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackColor       =   &H00A39B6D&
      Caption         =   "Form"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Index           =   2
      Left            =   840
      TabIndex        =   44
      Top             =   2880
      Visible         =   0   'False
      Width           =   600
   End
End
Attribute VB_Name = "frmRxDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public DIList As ListBox
Public EditPreviousOn As Boolean
'Public PatientId As Long
Public AppointmentId As Long
'Public DrugId As Long
'Public Dosage As String
'Public Frequency As String
'Public Duration As String
'Public Refills As String
'Public Notes As String
'Public AsPrescribedOn As Boolean
'Public DrugName As String

Public moDrugRx As CDrugRx
Public OutputStat As Integer

Public WriteRx As Boolean
Public ChangeRx As Boolean
Public ChangeRxInd As Integer

Private MED_STRENGTH_UOM As String
Private MED_DOSAGE As String
Private Form() As String
Private Strength() As String

Private Loaded As Boolean

Private Sub cmdClear_Click()
moDrugRx.Clear False
lblDosage.Caption = "1 " & cmdForm(0).Text
DisplayResults

Dim i As Integer
Dim RetrieveNotes As New PatientNotes
RetrieveNotes.NotesPatientId = globalExam.iPatientId
RetrieveNotes.NotesType = "R"
RetrieveNotes.NotesSystem = lblRx.Caption
If (RetrieveNotes.FindNotesbyPatient > 0) Then
    i = 1
    Do While (RetrieveNotes.SelectNotes(i))
        Call RetrieveNotes.DeleteNotes
        i = i + 1
    Loop
End If
RetrieveNotes.NotesPatientId = globalExam.iPatientId
RetrieveNotes.NotesType = "X"
RetrieveNotes.NotesSystem = lblRx.Caption
If (RetrieveNotes.FindNotesbyPatient > 0) Then
    i = 1
    Do While (RetrieveNotes.SelectNotes(i))
        Call RetrieveNotes.DeleteNotes
        i = i + 1
    Loop
End If
Set RetrieveNotes = Nothing

End Sub

Private Sub cmdDaw_Click()
moDrugRx.Daw = Not moDrugRx.Daw
DisplayResults
End Sub

Private Sub cmdDaysSupply_Click()
    frmNumericPad.SetDisplay "Days Supply for " & Trim$(lblRx.Caption)
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_Limit = 3
    frmNumericPad.cmdLTR.Visible = False
    frmNumericPad.cmdPlus.Visible = False
    frmNumericPad.cmdMinus.Visible = False
    frmNumericPad.cmdMultiply.Visible = False
    frmNumericPad.cmdDivide.Visible = False
    frmNumericPad.cmdPoint.Visible = False
    frmNumericPad.cmdColon.Visible = False
    frmNumericPad.Show 1
    frmNumericPad.NumPad_Limit = 0
    
    moDrugRx.DaysSupply = Trim(frmNumericPad.NumPad_Result + " " + frmNumericPad.NumPad_TimeFrame)
    If moDrugRx.DaysSupply <> "" Then
        moDrugRx.DaysSupply = moDrugRx.DaysSupply & " Days Supply"
    End If
    DisplayResults
End Sub

Private Sub cmdEye_Click(Index As Integer)

If moDrugRx.EyeRef = Index + 1 Then
    moDrugRx.EyeRef = eerNone
Else
    moDrugRx.EyeRef = Index + 1
End If

DisplayResults
End Sub

Private Sub cmdForm_Click(Index As Integer)
Dim r As Integer
Dim tmp(1) As String
Dim stopper As Integer

If Index = 2 And IsNumeric(cmdForm(2).Tag) Then
    Dim i As Integer
    Dim st As Integer
    st = cmdForm(2).Tag + 2
    If st > UBound(Form) Then st = 0
    
    For i = 0 To 1
        If i + st > UBound(Form) Then
            cmdForm(i).Text = ""
        Else
            cmdForm(i).Text = Form(i + st)
        End If
        cmdForm(i).Visible = Not (cmdForm(i).Text = "")
    Next
    cmdForm(2).Tag = st
    Exit Sub
End If


tmp(0) = myTrim(lblDosage.Caption)
If Not Len(tmp(0)) = Len(lblDosage.Caption) Then
    tmp(1) = myTrim(lblDosage.Caption, 1)
End If

ReDim Strength(0)
If tmp(1) = cmdForm(Index).Text Then
    MED_STRENGTH_UOM = ""
    moDrugRx.Strength = ""
    lblDosage.Caption = tmp(0)
Else
    MED_STRENGTH_UOM = cmdForm(Index).ToolTipText
    tmp(1) = " " & AbrForm(MED_STRENGTH_UOM)
    lblDosage.Caption = tmp(0) & tmp(1)
    'LoadStr
    Dim rsDrug As Recordset
    Dim clsDrug As New CDrug
    clsDrug.DisplayName = lblRx.Caption
    Set rsDrug = clsDrug.GetDrugStrAll(cmdForm(Index).ToolTipText)
    Set clsDrug = Nothing
    
    If rsDrug.RecordCount > 0 Then
        ReDim Strength(rsDrug.RecordCount - 1) As String
        For r = 0 To rsDrug.RecordCount - 1
            Strength(r) = rsDrug(0)
            rsDrug.MoveNext
        Next
    End If
End If

If UBound(Strength) > 4 Then
    stopper = 3
    cmdStr(4).Tag = 0
    cmdStr(4).Text = "More..."
    cmdStr(4).Visible = True
Else
    cmdStr(4).Tag = ""
    stopper = 4
End If
For r = 0 To stopper
    cmdStr(r).Text = ""
    If Not r > UBound(Strength) Then
        cmdStr(r).Text = Strength(r)
    End If
    cmdStr(r).Visible = Not (cmdStr(r).Text = "")
Next
DisplayResults
End Sub

Private Sub cmdHome_Click()
moDrugRx.Clear True
Call frmAssignDrugs.InitializeRx
Unload frmRxDetails
End Sub

Private Sub cmdDone_Click()
Dim RefillsNum As Integer

'check for duplication
If Not DIList Is Nothing Then
    Dim i As Integer
    Dim l As String
    Dim Dupl As Boolean
    For i = 0 To DIList.ListCount - 1
        If ChangeRx And i = ChangeRxInd Then GoTo NextItem
        l = DIList.List(i)
        If UCase(Mid(l, 1, Len(lblRx.Caption))) = UCase(lblRx.Caption) Then
            l = Mid(l, InStrPS(1, l, vbNewLine) + 2)
            l = Mid(l, 1, InStrPS(1, l, vbNewLine) - 1)
            If Len(l) > 3 Then
                l = Right(l, 3)
                l = UCase(Left(l, 2))
                If l = UCase(DisplayEyeRef(moDrugRx.EyeRef)) Then
                    Dupl = True
                End If
            Else
                If DisplayEyeRef(moDrugRx.EyeRef) = "" Then
                    Dupl = True
                End If
            End If
        End If
NextItem:
    Next
    If Dupl Then
        frmEventMsgs.Header = "Drug already active.  Use Discontinue or Change Rx."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
End If

'Dosage = moDrugRx.GetLegacyDosage
'Frequency = moDrugRx.GetLegacyFrequency
'Duration = moDrugRx.GetLegacyDuration
'Refills = moDrugRx.GetLegacyRefills


''''''''''''Call GetRxNotes(AppointmentId, PatientId, Trim(lblRx.Caption), moDrugRx.Notes, False)

'OutputStat = 9 : eSend
'OutputStat = 8 to 2 : eSend not permited
'OutputStat = 1 : not found
'OutputStat = 0 : no output

If WriteRx Then
    If moDrugRx.Samples <> "" _
    Or moDrugRx.stat <> "" _
    Or ChangeRx Then
        frmEventMsgs.Header = ""
        frmEventMsgs.AcceptText = "Script Needed"
        frmEventMsgs.RejectText = "No Script Needed"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    
        Select Case frmEventMsgs.Result
        Case 1
            OutputStat = 9
        Case 2
            OutputStat = 0
        Case 4
            Exit Sub
        End Select
    Else
        OutputStat = 9
    End If
Else
    If EditPreviousOn Then
        OutputStat = 9
    Else
        OutputStat = 0
    End If
End If

If OutputStat = 9 Then
    Dim tstDrug As String
    
    Dim clsDrug As New CDrug
    clsDrug.DrugId = moDrugRx.ID
    If clsDrug.SelectDrug(0) Then
        If moDrugRx.Strength = "" _
        And cmdStr(0).Visible Then
            frmEventMsgs.Header = "Rx needs a strength to be e-prescribed or considered for contraindication."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Proceed w/o strength"
            frmEventMsgs.CancelText = "Specify strength"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            
            If frmEventMsgs.Result = 4 Then Exit Sub
        End If
        tstDrug = IsE_Send(moDrugRx.Name, moDrugRx.Strength)
        If ChangeRx = True Then
            Call SavePrescribedNotes
            Call InsertLog("FrmRxDetails.frm", " Modified Medication For Patient " & CStr(globalExam.iPatientId) & " ", LoginCatg.MedicationRenew, "", "", 0, globalExam.iAppointmentId)
        Else
            Call InsertLog("FrmRxDetails.frm", " Prescribed New Medication For Patient " & CStr(globalExam.iPatientId) & " ", LoginCatg.MedicationInsert, "", "", 0, globalExam.iAppointmentId)
        End If
    End If
    Set clsDrug = Nothing
        
    frmEventMsgs.Header = ""
    
    'check for refill
    Select Case tstDrug
    Case 2
        If Not moDrugRx.Refills = "" Then
            frmEventMsgs.Header = "No refills for Schedule 2 drugs"
        End If
    Case 3 To 5
        RefillsNum = Val(moDrugRx.Refills)
        If RefillsNum > 5 Then
            frmEventMsgs.Header = "Max refills for Sched 3-5 drugs is 5"
        End If
    End Select
    
    If Not frmEventMsgs.Header = "" Then
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
    
    If Not EditPreviousOn Then
        Select Case tstDrug
        Case 1
            tstDrug = 1
            frmEventMsgs.Header = "Prescriptions cannot be sent electronically. " & _
            "Drug not found in the database."
        Case 2
            frmEventMsgs.Header = "Prescriptions for Schedule II medications cannot be sent electronically. " & _
            "They need to be printed and handed to the patient."
        Case 3 To 5
            frmEventMsgs.Header = "Because the medication is a controlled substance, " & _
            "it cannot be sent electronically. " & _
            "The Rx will be printed and needs a wet signature before " & _
            "it can be faxed to the pharmacy or handed to the patient."
        Case Else
            If (moDrugRx.EyeRef = eerNone And moDrugRx.PO = "") _
            Or (moDrugRx.Strength = "" And cmdStr(0).Visible) _
            Or (moDrugRx.Frequency = "" And moDrugRx.AsPrescribed = False) _
            Or (moDrugRx.PillPackage = "") Then
                tstDrug = 1
                frmEventMsgs.Header = "Prescriptions cannot be sent electronically. " & _
                "Not enough information to ePrescribe."
            End If
        End Select
        
        If Not frmEventMsgs.Header = "" Then
            OutputStat = tstDrug
            
            If tstDrug > 1 Then
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = ""
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = "Print now"
                frmEventMsgs.Other2Text = "Print later"
                frmEventMsgs.Other3Text = "Cancel"
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.cmdResult(3).Visible = False
            Else
                frmEventMsgs.AcceptText = "" '"Print now"
                frmEventMsgs.RejectText = "Print later"
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
            End If
            frmEventMsgs.Show 1
            
            Select Case frmEventMsgs.Result
            Case 1, 5
                moDrugRx.PrintRx
            Case 4, 7
                Exit Sub
            End Select
        End If
    End If
End If
            
Unload frmRxDetails

End Sub

Private Sub cmdDose_Click(Index As Integer)
Dim tmp(1) As String

tmp(0) = Val(myTrim(lblDosage.Caption))
If Not Len(tmp(0)) = Len(lblDosage.Caption) Then
    tmp(1) = " " & cmdForm(0).Text
End If

Select Case Index
Case 0
    tmp(0) = CLng(tmp(0)) - 10
Case 1
    tmp(0) = CLng(tmp(0)) - 1
Case 2
    tmp(0) = CLng(tmp(0)) + 1
Case 3
    tmp(0) = CLng(tmp(0)) + 10
End Select

If tmp(0) > 0 Then
    If UCase(Trim(tmp(1))) = "TABLET" And lblDosage.Caption = "" Then
        lblDosage.Caption = "0.5" & tmp(1)
    Else
        lblDosage.Caption = tmp(0) & tmp(1)
    End If
Else
    If UCase(Trim(tmp(1))) = "TABLET" Then
        Select Case lblDosage.Caption
        Case "0.5" & tmp(1), ""
            lblDosage.Caption = ""
        Case Else
            lblDosage.Caption = "0.5" & tmp(1)
        End Select
    Else
        lblDosage.Caption = ""
    End If
End If

moDrugRx.Dosage = lblDosage.Caption
DisplayResults
End Sub

Private Sub cmdOver_Click()
    moDrugRx.OverTheCounter = Not moDrugRx.OverTheCounter
    DisplayResults
End Sub


Private Sub cmdFrequency_Click(Index As Integer)
    If moDrugRx.AsPrescribed Then
        frmEventMsgs.Header = "As prescribed will be replaced.  OK?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "OK"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 2) Then
            moDrugRx.AsPrescribed = False
            Dim i As Integer
            Dim RetrieveNotes As New PatientNotes
            
            RetrieveNotes.NotesPatientId = globalExam.iPatientId
            RetrieveNotes.NotesType = "R"
            RetrieveNotes.NotesSystem = lblRx.Caption
            If (RetrieveNotes.FindNotesbyPatient > 0) Then
                i = 1
                Do While (RetrieveNotes.SelectNotes(i))
                    Call RetrieveNotes.DeleteNotes
                    i = i + 1
                Loop
            End If
            
            RetrieveNotes.NotesPatientId = globalExam.iPatientId
            RetrieveNotes.NotesType = "X"
            RetrieveNotes.NotesSystem = lblRx.Caption
            If (RetrieveNotes.FindNotesbyPatient > 0) Then
                i = 1
                Do While (RetrieveNotes.SelectNotes(i))
                    Call RetrieveNotes.DeleteNotes
                    i = i + 1
                Loop
            End If
            
            
            
            Set RetrieveNotes = Nothing
        Else
            Exit Sub
        End If
    End If
    
    If moDrugRx.Frequency = Trim$(cmdFrequency(Index).Text) Then
        moDrugRx.Frequency = ""
    Else
        moDrugRx.Frequency = Trim$(cmdFrequency(Index).Text)
    End If
    DisplayResults
End Sub

Private Sub cmdAsPrescribed_Click()
frmDrugNotes.PatientId = globalExam.iPatientId
frmDrugNotes.AppointmentId = AppointmentId
frmDrugNotes.NotesSystem = lblRx.Caption
frmDrugNotes.LoadDrugNotes
frmDrugNotes.frAsPrescribed.Enabled = True
frmDrugNotes.Show 1
If moDrugRx.AsPrescribed Then
    moDrugRx.Frequency = ""
End If
DisplayResults
End Sub

Private Function SavePrescribedNotes()
frmDrugNotes.PatientId = globalExam.iPatientId
frmDrugNotes.AppointmentId = AppointmentId
frmDrugNotes.NotesSystem = lblRx.Caption
frmDrugNotes.LoadDrugNotes
frmDrugNotes.frAsPrescribed.Enabled = True
Call frmDrugNotes.cmdDone_Click
If moDrugRx.AsPrescribed Then
    moDrugRx.Frequency = ""
End If
End Function

Private Sub cmdPharmNotes_Click()
frmDrugNotes.PatientId = globalExam.iPatientId
frmDrugNotes.AppointmentId = AppointmentId
frmDrugNotes.NotesSystem = lblRx.Caption
frmDrugNotes.LoadDrugNotes

frmDrugNotes.frAsPrescribed.Enabled = False

frmDrugNotes.Show 1

End Sub

Private Sub cmdPO_Click(Index As Integer)
If moDrugRx.PO = cmdPO(Index).Text Then
    moDrugRx.PO = ""
Else
    moDrugRx.PO = cmdPO(Index).Text
End If
DisplayResults

End Sub

Private Sub cmdPRN_Click()
If Not moDrugRx.Refills = "" Then
    frmEventMsgs.Header = "PRN not allowed with refills"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If


    moDrugRx.PRN = Not moDrugRx.PRN
    DisplayResults
End Sub

Private Sub cmdDosage_Click()
Dim sDosage As String
    frmNumericPad.SetDisplay "Custom Dosage For " & Trim$(lblRx.Caption), "mg", "gm", "ml", "IV", "mcg", "cc", "gtt", "tblt", "%"
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_Limit = 15
    frmNumericPad.cmdLTR.Visible = False
    frmNumericPad.Show 1
    frmNumericPad.NumPad_Limit = 0
    sDosage = frmNumericPad.GetResultString
    If Trim(sDosage) <> "" Then
        moDrugRx.Dosage = sDosage
    End If
    DisplayResults
End Sub

Private Sub cmdDuration_Click()
    frmNumericPad.SetDisplay "Duration for " & Trim$(lblRx.Caption)
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = True
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.cmdPrn.Visible = False
    frmNumericPad.Show 1
    moDrugRx.Duration = Trim(frmNumericPad.NumPad_Result) & " " & frmNumericPad.NumPad_TimeFrame
    DisplayResults
End Sub

Private Sub cmdLT_Click()
Dim sLastTaken As String
    frmNumericPad.SetDisplay Trim$(lblRx.Caption) & " was last taken", "AM", "PM", "Before", "Approx", "", "Ago"
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.Show 1
    If Not frmNumericPad.NumPad_Quit Then
        sLastTaken = frmNumericPad.GetResultString
        Call ReplaceCharacters(sLastTaken, "/", ".")
        If sLastTaken <> "" Then
            moDrugRx.LastTaken = "Last Taken " & sLastTaken
        Else
            moDrugRx.LastTaken = ""
        End If
    End If
    DisplayResults
End Sub

Private Sub cmdPills_Click()

Dim ATemp As String
Dim PillTemp As String
Dim Pack1 As String, Pack2 As String, Pack3 As String
Dim Pack4 As String, Pack5 As String, Pack6 As String
Dim lst As String
Call GetDrugPackage(moDrugRx.ID, Pack1, Pack2, Pack3, Pack4, Pack5, Pack6)
frmNumericPad.SetDisplay "Pills Count or Package Size For " & Trim$(lblRx.Caption), "mg", "gm", "ml", "IV", "mcg", "cc", "gtt", "%", "tablets", "tube", "capsules", "bottle", Pack1, Pack2, Pack3, Pack4, Pack5, Pack6
frmNumericPad.NumPad_Max = 0
frmNumericPad.NumPad_Min = 0
frmNumericPad.NumPad_EyesOn = False
frmNumericPad.NumPad_CommentOn = False
frmNumericPad.NumPad_TimeFrameOn = True
frmNumericPad.NumPad_TimeFrame = ""
frmNumericPad.NumPad_DisplayFull = True
frmNumericPad.frPack.Visible = True
frmNumericPad.frPack.ZOrder
frmNumericPad.NumPad_Limit = 15
frmNumericPad.cmdPoint.Visible = True
frmNumericPad.cmdLTR.Visible = False
frmNumericPad.NumPad_frPack = ""

Dim RetCon As New PracticeConfigureInterface
If RetCon.getFieldValue("ERXVALID") = "T" Then
    lst = _
    "<Bag>" & _
    "<Bottle>" & _
    "<Box>" & _
    "<Cartridge>" & _
    "<Container>" & _
    "<Drop>" & _
    "<Each>" & _
    "<Kit>" & _
    "<Mutually Defined>" & _
    "<Not Specified>" & _
    "<Pack>" & _
    "<Packet>" & _
    "<Tube>"
    Call frmNumericPad.DisableButtons(lst)
End If
Set RetCon = Nothing


frmNumericPad.Show 1
frmNumericPad.NumPad_Limit = 0
If Not (frmNumericPad.NumPad_Quit) Then
    moDrugRx.PillPackage = Trim$(frmNumericPad.NumPad_Result & " " & frmNumericPad.NumPad_frPack)
'    If moDrugRx.PillPackage <> "" Then
'        moDrugRx.PillPackage = "Pills-Package " & moDrugRx.PillPackage
'    End If
End If
DisplayResults
End Sub

Private Sub cmdRefills_Click()
If moDrugRx.PRN Then
    frmEventMsgs.Header = "Refills not allowed with PRN"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If



Dim ATemp As String
Dim PTemp As String
    frmNumericPad.SetDisplay "Refills For " & Trim$(lblRx.Caption)
    frmNumericPad.NumPad_Max = 0
    frmNumericPad.NumPad_Min = 0
    frmNumericPad.NumPad_EyesOn = False
    frmNumericPad.NumPad_CommentOn = False
    frmNumericPad.NumPad_TimeFrameOn = False
    frmNumericPad.NumPad_TimeFrame = ""
    frmNumericPad.NumPad_DisplayFull = True
    frmNumericPad.NumPad_Limit = 3
    frmNumericPad.cmdLTR.Visible = False
    frmNumericPad.Show 1
    frmNumericPad.NumPad_Limit = 0
    ATemp = Trim$(frmNumericPad.NumPad_Result)
    If Trim$(ATemp) <> "" Then
        moDrugRx.Refills = ATemp & " Refills"
    Else
        moDrugRx.Refills = ""
    End If
    DisplayResults
End Sub

Public Function LoadRxDetails(IType As Boolean, LastOn As Boolean, fLoadRx As Boolean) As Boolean
Dim i As Integer
Dim j As Integer
Dim r As Integer
Dim stopper As Integer
ReDim Form(0) As String
Dim iDoseCount As Integer
Dim RetDrug As DiagnosisMasterDrugs
Dim clsDrug As New CDrug
Dim rsDrug As Recordset

LoadRxDetails = False
If (Val(moDrugRx.ID) < 1) Then
    Exit Function
End If

cmdLT.Visible = LastOn
txtResults.Text = ""
If (fLoadRx) Then
    DisplayResults
End If

'---------
'load forms
clsDrug.DrugId = Val(moDrugRx.ID)
clsDrug.DisplayName = ""
Set rsDrug = clsDrug.GetDrugFormsAll
moDrugRx.Name = clsDrug.DisplayName

If rsDrug.RecordCount > 0 Then
    ReDim Form(rsDrug.RecordCount - 1) As String
End If
For r = 0 To rsDrug.RecordCount - 1
    Form(r) = Trim(rsDrug(0))
    rsDrug.MoveNext
Next
Set clsDrug = Nothing

If UBound(Form) > 2 Then
    stopper = 1
    cmdForm(2).Tag = 0
    cmdForm(2).Text = "More..."
    cmdForm(2).Visible = True
Else
    cmdForm(2).Tag = ""
    stopper = 2
End If
For r = 0 To stopper
    cmdForm(r).Text = ""
    cmdForm(r).ToolTipText = ""
    If Not r > UBound(Form) Then
        cmdForm(r).Text = AbrForm(Form(r))
        cmdForm(r).ToolTipText = Form(r)
    End If
    'cmdForm(r).Visible = Not (cmdForm(r).Text = "")
Next

For r = 0 To 4
    'REFACTOR: cmdStr - Is that cmd String?  You can use a few more letters to be clear.
    cmdStr(r).Visible = False
Next
'---------
If moDrugRx.Name = "" Then
    Set RetDrug = New DiagnosisMasterDrugs
    RetDrug.DrugId = Val(moDrugRx.ID)
    If (RetDrug.RetrieveDrug) Then
        lblRx.Caption = Trim(RetDrug.DrugName)
    End If
    Set RetDrug = Nothing
Else
    lblRx.Caption = moDrugRx.Name
End If

'autoclick
If Not cmdForm(0).Text = "" Then
    Call cmdForm_Click(0)
End If

Loaded = True
If moDrugRx.Dosage = "" Then
    moDrugRx.Dosage = lblDosage.Caption
    DisplayResults
Else
    lblDosage.Caption = moDrugRx.Dosage
End If

LoadRxDetails = True
cmdDuration.Visible = Not IType
cmdRefills.Visible = Not IType
cmdPills.Visible = Not IType
' Prevent Notes
cmdAsPrescribed.Visible = Not IType
cmdPharmNotes.Visible = Not IType
If (AppointmentId = 0) And (globalExam.iPatientId = 0) Then
    cmdAsPrescribed.Visible = False
    cmdPharmNotes.Visible = False
End If

End Function

Private Function AbrForm(inp As String) As String
Dim RetCode As PracticeCodes
AbrForm = inp
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "DOSAGEFORMMAP"
RetCode.ReferenceCode = inp
If (RetCode.FindCodeLike > 0) Then
    If (RetCode.SelectCode(1)) Then
        AbrForm = Trim(RetCode.ReferenceAlternateCode)
    End If
End If
Set RetCode = Nothing
End Function


Private Sub DisplayResults()
txtResults.Text = moDrugRx.GetDrugRxFormat("display")

Dim txtColor(1) As Long
'txtColor(0) = &HFFFFFF
txtColor(0) = &HC000&
txtColor(1) = &H800000

If moDrugRx.EyeRef = eerNone _
And moDrugRx.PO = "" Then
    lblEye.ForeColor = txtColor(1)
    lblPO.ForeColor = txtColor(1)
Else
    lblEye.ForeColor = txtColor(0)
    lblPO.ForeColor = txtColor(0)
End If

If moDrugRx.Strength = "" And Not cmdStr(0).Text = "" Then
    lblStrength.ForeColor = txtColor(1)
Else
    lblStrength.ForeColor = txtColor(0)
End If

If moDrugRx.Frequency = "" _
And moDrugRx.AsPrescribed = False Then
    lblFrequency.ForeColor = txtColor(1)
Else
    lblFrequency.ForeColor = txtColor(0)
End If

If moDrugRx.PillPackage = "" Then
    cmdPills.ForeColor = txtColor(1)
Else
    cmdPills.ForeColor = txtColor(0)
End If

End Sub

Public Function GetRxNotes(ByVal ApptId As Long, ByVal PatId As Long, ByVal DrugName As String, TheNote As String, TheNote2 As String, ByVal OverrideOn As Boolean, _
Optional ByVal toPrintOn As Boolean = False) As Boolean
Dim i As Integer
Dim RetNote As PatientNotes
TheNote = ""
If DrugName Like "*%" Then
    i = InStrPS(1, DrugName, "%")
    If i > 0 Then
        i = InStrRev(DrugName, " ", i - 2)
        DrugName = Mid(DrugName, 1, i)
    End If
End If
GetRxNotes = False
If (ApptId > 0) And (PatId > 0) And (Trim(DrugName) <> "") Then
    Set RetNote = New PatientNotes
    RetNote.NotesPatientId = PatId
    RetNote.NotesType = "R"
    RetNote.NotesAppointmentId = 0
    RetNote.NotesSystem = ""
    RetNote.NotesEye = ""
    If (RetNote.FindNotesbyPatient > 0) Then
        i = 1
        While (RetNote.SelectNotes(i)) And (Len(Trim(TheNote)) < 255)
            If (UCase(Trim(RetNote.NotesSystem)) = UCase(Trim(DrugName))) Then
                If (RetNote.NotesCommentOn) Or (OverrideOn) Then
                    If (Trim(RetNote.NotesText1) <> "") Then
                        TheNote = TheNote + Trim(RetNote.NotesText1) + " "
                    End If
                    If (Trim(RetNote.NotesText2) <> "") Then
                        TheNote = TheNote + Trim(RetNote.NotesText2) + " "
                    End If
                    If (Trim(RetNote.NotesText3) <> "") Then
                        TheNote = TheNote + Trim(RetNote.NotesText3) = " "
                    End If
                    If (Trim(RetNote.NotesText4) <> "") Then
                        TheNote = TheNote + Trim(RetNote.NotesText4) = " "
                    End If
                Else
                    If Not toPrintOn Then
                        TheNote = TheNote + "As prescribed "
                    End If
                End If
            End If
            i = i + 1
        Wend
    End If
    
'---------------------------------------------------------------------------------
'---------------------------------------------------------------------------------
    Set RetNote = New PatientNotes
    RetNote.NotesPatientId = PatId
    RetNote.NotesType = "X"
    RetNote.NotesAppointmentId = 0
    RetNote.NotesSystem = ""
    RetNote.NotesEye = ""
    If (RetNote.FindNotesbyPatient > 0) Then
        i = 1
        While (RetNote.SelectNotes(i)) And (Len(Trim(TheNote2)) < 255)
            If (UCase(Trim(RetNote.NotesSystem)) = UCase(Trim(DrugName))) Then
                If (RetNote.NotesCommentOn) Or (OverrideOn) Then
                    If (Trim(RetNote.NotesText1) <> "") Then
                        TheNote2 = TheNote2 + Trim(RetNote.NotesText1) + " "
                    End If
                    If (Trim(RetNote.NotesText2) <> "") Then
                        TheNote2 = TheNote2 + Trim(RetNote.NotesText2) + " "
                    End If
                    If (Trim(RetNote.NotesText3) <> "") Then
                        TheNote2 = TheNote2 + Trim(RetNote.NotesText3) = " "
                    End If
                    If (Trim(RetNote.NotesText4) <> "") Then
                        TheNote2 = TheNote2 + Trim(RetNote.NotesText4) = " "
                    End If
                Else
                    If Not toPrintOn Then
                        TheNote2 = TheNote2 + "As prescribed "
                    End If
                End If
            End If
            i = i + 1
        Wend
    End If
'---------------------------------------------------------------------------------
    If TheNote <> "" Then
        TheNote = "As prescribed Note:" & TheNote
    End If
    Set RetNote = Nothing
    GetRxNotes = True
End If
End Function

Private Function GetDrugPackage(TheId As Long, Pack1 As String, Pack2 As String, Pack3 As String, Pack4 As String, Pack5 As String, Pack6 As String) As Boolean
Dim RetDrug As DiagnosisMasterDrugs
Pack1 = ""
Pack2 = ""
Pack3 = ""
Pack4 = ""
Pack5 = ""
Pack6 = ""
GetDrugPackage = False
If (TheId > 0) Then
    Set RetDrug = New DiagnosisMasterDrugs
    RetDrug.DrugId = TheId
    If (RetDrug.RetrieveDrug) Then
        Pack1 = Trim(RetDrug.DrugPackage1)
        Pack2 = Trim(RetDrug.DrugPackage2)
        Pack3 = Trim(RetDrug.DrugPackage3)
        Pack4 = Trim(RetDrug.DrugPackage4)
        Pack5 = Trim(RetDrug.DrugPackage5)
        Pack6 = Trim(RetDrug.DrugPackage6)
        GetDrugPackage = True
    End If
    Set RetDrug = Nothing
End If
End Function


Private Sub cmdSamStat_Click(Index As Integer)
If Index = 0 Then
    If moDrugRx.stat = "" Then
        moDrugRx.stat = "STAT"
    Else
        moDrugRx.stat = ""
    End If
Else
    If moDrugRx.Samples = "" Then
        moDrugRx.Samples = "Samples"
    Else
        moDrugRx.Samples = ""
    End If
End If

DisplayResults


End Sub

Private Sub cmdStr_Click(Index As Integer)

If Index = 4 And IsNumeric(cmdStr(4).Tag) Then
    Dim i As Integer
    Dim st As Integer
    st = cmdStr(4).Tag + 4
    If st > UBound(Strength) Then st = 0
    
    For i = 0 To 3
        If i + st > UBound(Strength) Then
            cmdStr(i).Text = ""
        Else
            cmdStr(i).Text = Strength(i + st)
        End If
        cmdStr(i).Visible = Not (cmdStr(i).Text = "")
    Next
    cmdStr(4).Tag = st
    Exit Sub
End If


If moDrugRx.Strength = cmdStr(Index).Text Then
    moDrugRx.Strength = ""
Else
    moDrugRx.Strength = cmdStr(Index).Text
End If
'moDrugRx.Dosage = getDosage
DisplayResults

End Sub

Private Sub form_load()
MED_STRENGTH_UOM = ""
MED_DOSAGE = ""
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
WriteRx = False
ChangeRx = False
Loaded = False
EditPreviousOn = False
End Sub

Private Sub lblDosage_Change()
If Loaded Then
    moDrugRx.Dosage = lblDosage.Caption
    DisplayResults
End If
End Sub

