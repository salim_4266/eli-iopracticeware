VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmNotes 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9075
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12180
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "Notes.frx":0000
   ScaleHeight     =   9075
   ScaleWidth      =   12180
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Frame FrameSearch 
      BackColor       =   &H009C8100&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   1520
      TabIndex        =   46
      Top             =   80
      Visible         =   0   'False
      Width           =   4570
      Begin VB.TextBox txtSearch 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   0
         TabIndex        =   49
         Top             =   0
         Width           =   3615
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdFrwd 
         Height          =   400
         Left            =   4080
         TabIndex        =   48
         Top             =   0
         Width           =   495
         _Version        =   131072
         _ExtentX        =   873
         _ExtentY        =   706
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "Notes.frx":36C9
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdBack 
         Height          =   405
         Left            =   3600
         TabIndex        =   47
         Top             =   0
         Width           =   495
         _Version        =   131072
         _ExtentX        =   873
         _ExtentY        =   714
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   7828525
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "Notes.frx":38A6
      End
   End
   Begin VB.TextBox txtMoreView 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   4335
      HideSelection   =   0   'False
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   27
      Top             =   2040
      Visible         =   0   'False
      Width           =   9015
   End
   Begin VB.CheckBox chkClaim 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Include Note on Claim"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   6600
      TabIndex        =   38
      Top             =   0
      Visible         =   0   'False
      Width           =   2295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStdRxNotes 
      Height          =   975
      Left            =   7680
      TabIndex        =   24
      Top             =   2520
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   10256640
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":3A83
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStandardNote 
      Height          =   975
      Left            =   7680
      TabIndex        =   15
      Top             =   2520
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   10256640
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":3CC3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdStdCRMNote 
      Height          =   975
      Left            =   7680
      TabIndex        =   35
      Top             =   2520
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   10256640
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":3F02
   End
   Begin VB.CheckBox chkInclude 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Include Note on Statement"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   6600
      TabIndex        =   20
      Top             =   240
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.ListBox lstSelect 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6000
      ItemData        =   "Notes.frx":414A
      Left            =   240
      List            =   "Notes.frx":414C
      TabIndex        =   16
      Top             =   1560
      Width           =   7335
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrivateNotes 
      Height          =   975
      Left            =   10680
      TabIndex        =   4
      Top             =   600
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":414E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdGeneralNotes 
      Height          =   975
      Left            =   9360
      TabIndex        =   3
      Top             =   1560
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":4386
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   9
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":45C0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10320
      TabIndex        =   8
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":47F3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMessages 
      Height          =   975
      Left            =   9360
      TabIndex        =   2
      Top             =   5760
      Visible         =   0   'False
      Width           =   2595
      _Version        =   131072
      _ExtentX        =   4577
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":4A26
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdToDo 
      Height          =   975
      Left            =   9360
      TabIndex        =   1
      Top             =   4560
      Width           =   2595
      _Version        =   131072
      _ExtentX        =   4577
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":4C68
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFreehand 
      Height          =   975
      Left            =   10680
      TabIndex        =   6
      Top             =   2520
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":4EA5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBillNotes 
      Height          =   975
      Left            =   9360
      TabIndex        =   5
      Top             =   2520
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":50E2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdScan 
      Height          =   975
      Left            =   9360
      TabIndex        =   7
      Top             =   6840
      Width           =   2595
      _Version        =   131072
      _ExtentX        =   4577
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":531E
   End
   Begin VB.TextBox txtChartNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4335
      HideSelection   =   0   'False
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   10
      Top             =   3480
      Width           =   9015
   End
   Begin VB.TextBox txtPrivateNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4335
      HideSelection   =   0   'False
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   11
      Top             =   3480
      Width           =   9015
   End
   Begin VB.TextBox txtToDo 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4335
      HideSelection   =   0   'False
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   12
      Top             =   3480
      Width           =   9015
   End
   Begin VB.TextBox txtBillNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4335
      HideSelection   =   0   'False
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   18
      Top             =   3480
      Width           =   9015
   End
   Begin VB.TextBox txtNewCNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2895
      HideSelection   =   0   'False
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   600
      Width           =   9015
   End
   Begin VB.TextBox txtNewPNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2895
      HideSelection   =   0   'False
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   13
      Top             =   600
      Width           =   9015
   End
   Begin VB.TextBox txtNewBNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2895
      HideSelection   =   0   'False
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   17
      Top             =   600
      Width           =   9015
   End
   Begin VB.TextBox txtNewTNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2895
      HideSelection   =   0   'False
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   14
      Top             =   600
      Width           =   9015
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRxNotes 
      Height          =   975
      Left            =   10680
      TabIndex        =   21
      Top             =   1560
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":5556
   End
   Begin VB.TextBox txtRxNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4335
      HideSelection   =   0   'False
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   22
      Top             =   3480
      Width           =   9015
   End
   Begin VB.TextBox txtNewRNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2895
      HideSelection   =   0   'False
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   23
      Top             =   600
      Width           =   9015
   End
   Begin VB.CheckBox chkRxInclude 
      BackColor       =   &H0077742D&
      Caption         =   "Include Note on Rx"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6600
      TabIndex        =   25
      Top             =   240
      Visible         =   0   'False
      Width           =   2175
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   975
      Left            =   5760
      TabIndex        =   26
      Top             =   7920
      Visible         =   0   'False
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":578F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCat 
      Height          =   975
      Left            =   7680
      TabIndex        =   28
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":59C8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCRMNotes 
      Height          =   975
      Left            =   10680
      TabIndex        =   32
      Top             =   3480
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":5C01
   End
   Begin VB.TextBox txtCRMNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4335
      HideSelection   =   0   'False
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   33
      Top             =   3480
      Width           =   9015
   End
   Begin VB.TextBox txtNewCRMNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2895
      HideSelection   =   0   'False
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   34
      Top             =   600
      Width           =   9015
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAlert 
      Height          =   975
      Left            =   2040
      TabIndex        =   36
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":5E3A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSurgeryNotes 
      Height          =   975
      Left            =   9360
      TabIndex        =   39
      Top             =   600
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":6072
   End
   Begin VB.TextBox txtNewSNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2895
      HideSelection   =   0   'False
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   40
      Top             =   600
      Width           =   9015
   End
   Begin VB.TextBox txtSurgeryNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4335
      HideSelection   =   0   'False
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   41
      Top             =   3480
      Width           =   9015
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCollNotes 
      Height          =   975
      Left            =   9360
      TabIndex        =   42
      Top             =   3480
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Notes.frx":62AE
   End
   Begin VB.TextBox txtNewYNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   2895
      HideSelection   =   0   'False
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   43
      Top             =   600
      Width           =   9015
   End
   Begin VB.TextBox txtCollNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   4335
      HideSelection   =   0   'False
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   44
      Top             =   3480
      Width           =   9015
   End
   Begin MSFlexGridLib.MSFlexGrid SortFgc 
      Height          =   855
      Left            =   10200
      TabIndex        =   45
      Top             =   240
      Visible         =   0   'False
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   1508
      _Version        =   393216
      FixedRows       =   0
      FixedCols       =   0
   End
   Begin VB.Label lblAlert 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Alert:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   4080
      TabIndex        =   37
      Top             =   0
      Width           =   4815
   End
   Begin VB.Label lblCatReminder 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Please remember to categorize PMHX Notes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   120
      TabIndex        =   31
      Top             =   360
      Visible         =   0   'False
      Width           =   3660
   End
   Begin VB.Label lblRetired 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Retired"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   9000
      TabIndex        =   30
      Top             =   240
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.Label lblCat 
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Category:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   9000
      TabIndex        =   29
      Top             =   0
      Width           =   3015
   End
   Begin VB.Label lblPatName 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   120
      TabIndex        =   19
      Top             =   0
      Width           =   600
   End
End
Attribute VB_Name = "frmNotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public AppointmentId As Long
Public CurrentAction As String
Public SystemReference As String
Public EyeContext As String
Public MaintainOn As Boolean
Public NoteOn As Boolean
Public NoteId As Long
Public SetTo As String

Private Const CategorizationTrigger As String = "4"
Private Const SelectionForeColor As Long = 0
Private Const SelectionBackColor As Long = &HEAFFFE
Private Const BaseForeColor As Long = &HFFFFFF
Private Const BaseBackColor As Long = &HC19B49
Private Const LineMax As Integer = 17

Private TriggerOn As Boolean
Private MaintainAlertOn As Boolean
Private ILPNRef As String
Private NoteSelectAlertOn As Boolean
Private NoteAlertSelected As String
Private NoteAlertDisplay As String
Private NoteSelectCatOn As Boolean
Private NoteCategorySelected As String
Private TheNoteId As Long
Private LocalSysRef As String
Private BSDisplay(10) As String
Private LoadOn As Boolean
Private TodaysDate As String
Private ApptDate As String
Private UserId As String
Public PatRunsUsed As Long
Dim pat As New Patient
Private CurrentTextBox As TextBox

Private Sub SetNotesButtons()
cmdToDo.BackColor = BaseBackColor
cmdToDo.ForeColor = BaseForeColor
cmdMessages.BackColor = BaseBackColor
cmdMessages.ForeColor = BaseForeColor
cmdFreehand.BackColor = BaseBackColor
cmdFreehand.ForeColor = BaseForeColor
cmdGeneralNotes.BackColor = BaseBackColor
cmdGeneralNotes.ForeColor = BaseForeColor
cmdPrivateNotes.BackColor = BaseBackColor
cmdPrivateNotes.ForeColor = BaseForeColor
cmdCollNotes.BackColor = BaseBackColor
cmdCollNotes.ForeColor = BaseForeColor
cmdBillNotes.BackColor = BaseBackColor
cmdBillNotes.ForeColor = BaseForeColor
cmdRxNotes.BackColor = BaseBackColor
cmdRxNotes.ForeColor = BaseForeColor
cmdCRMNotes.BackColor = BaseBackColor
cmdCRMNotes.ForeColor = BaseForeColor
cmdSurgeryNotes.BackColor = BaseBackColor
cmdSurgeryNotes.ForeColor = BaseForeColor
cmdScan.BackColor = BaseBackColor
cmdScan.ForeColor = BaseForeColor
Call HideSearchControl
End Sub

Private Sub cmdAlert_Click()
lstSelect.Visible = False
Call HideSearchControl
Call LoadSelectionList("AutoAlertType", lstSelect, True, "")
lstSelect.Visible = True
NoteSelectAlertOn = True
End Sub

Private Sub cmdBack_Click()
If CurrentTextBox Is Nothing Then Exit Sub
Call SearchText(Mid(CurrentTextBox.Text, 1, CurrentTextBox.SelStart), Len(CurrentTextBox.Text) - CurrentTextBox.SelStart - 1, True)
End Sub

Private Sub cmdBillNotes_Click()
If (cmdBillNotes.BackColor = BaseBackColor) Then
    Call SetNotesButtons
    cmdBillNotes.BackColor = SelectionBackColor
    cmdBillNotes.ForeColor = SelectionForeColor
    lstSelect.Visible = False
    chkInclude.Visible = True
    chkClaim.Visible = True
    chkRxInclude.Visible = False
    txtToDo.Visible = False
    txtChartNotes.Visible = False
    txtSurgeryNotes.Visible = False
    txtPrivateNotes.Visible = False
    txtRxNotes.Visible = False
    txtCRMNotes.Visible = False
    txtBillNotes.Visible = True
    txtNewYNotes.Visible = False
    txtNewCNotes.Visible = False
    txtNewTNotes.Visible = False
    txtNewPNotes.Visible = False
    txtNewRNotes.Visible = False
    txtNewCRMNotes.Visible = False
    txtNewSNotes.Visible = False
    txtNewBNotes.Visible = True
    cmdMore.Visible = IsNotesDisplayFull(txtBillNotes.Text)
    cmdMore.Tag = "B"
    If Not (LoadOn) Then
        txtNewBNotes.SetFocus
        SendKeys "{End}"
    End If
    cmdStandardNote.Visible = True
    cmdStdRxNotes.Visible = False
    SetTo = "B"
End If
Call DisplayNotesScreen(PatientId)
End Sub

Private Sub cmdCat_Click()
lstSelect.Visible = False
Call HideSearchControl
Call LoadSelectionList("NoteCategory", lstSelect, True, "")
lstSelect.Visible = True
NoteSelectCatOn = True
End Sub

Private Sub cmdCollNotes_Click()
If (cmdCollNotes.BackColor = BaseBackColor) Then
    Call SetNotesButtons
    cmdCollNotes.BackColor = SelectionBackColor
    cmdCollNotes.ForeColor = SelectionForeColor
    lstSelect.Visible = False
    chkInclude.Visible = True
    chkClaim.Visible = True
    chkRxInclude.Visible = False
    txtBillNotes.Visible = False
    txtSurgeryNotes.Visible = False
    txtCollNotes.Visible = True
    txtToDo.Visible = False
    txtChartNotes.Visible = False
    txtPrivateNotes.Visible = False
    txtRxNotes.Visible = False
    txtCRMNotes.Visible = False
    txtNewCNotes.Visible = False
    txtNewTNotes.Visible = False
    txtNewPNotes.Visible = False
    txtNewRNotes.Visible = False
    txtNewCRMNotes.Visible = False
    txtNewSNotes.Visible = False
    txtNewBNotes.Visible = False
    txtNewYNotes.Visible = True
    cmdMore.Visible = IsNotesDisplayFull(txtBillNotes.Text)
    cmdMore.Tag = "Y"
    If Not (LoadOn) Then
        txtNewYNotes.SetFocus
        SendKeys "{End}"
    End If
    cmdStandardNote.Visible = True
    cmdStdRxNotes.Visible = False
    SetTo = "Y"
End If
End Sub

Private Sub cmdCRMNotes_Click()
If (cmdCRMNotes.BackColor = BaseBackColor) Then
    Call SetNotesButtons
    cmdCRMNotes.BackColor = SelectionBackColor
    cmdCRMNotes.ForeColor = SelectionForeColor
    lstSelect.Visible = False
    chkInclude.Visible = False
    chkClaim.Visible = False
    chkRxInclude.Visible = False
    txtCollNotes.Visible = False
    txtToDo.Visible = False
    txtBillNotes.Visible = False
    txtSurgeryNotes.Visible = False
    txtPrivateNotes.Visible = False
    txtChartNotes.Visible = False
    txtRxNotes.Visible = False
    txtCRMNotes.Visible = True
    txtNewYNotes.Visible = False
    txtNewPNotes.Visible = False
    txtNewTNotes.Visible = False
    txtNewBNotes.Visible = False
    txtNewCNotes.Visible = False
    txtNewRNotes.Visible = False
    txtNewSNotes.Visible = False
    txtNewCRMNotes.Visible = True
    cmdMore.Visible = IsNotesDisplayFull(txtCRMNotes.Text)
    cmdMore.Tag = "D"
    If Not (LoadOn) Then
        txtNewCRMNotes.SetFocus
        SendKeys "{End}"
    End If
    cmdStandardNote.Visible = False
    cmdStdRxNotes.Visible = False
    cmdStdCRMNote.Visible = True
    SetTo = "D"
End If
End Sub

Private Sub cmdFreeHand_Click()
    On Error GoTo lcmdFreeHand_Click_Error

frmFreehand.StoreResults = frmFreehand.CurrentFreehandPage(PatientId, AppointmentId, 0)
If (frmFreehand.LoadFreehand) Then
    frmFreehand.Show 1
    Call cmdGeneralNotes_Click
End If

    Exit Sub

lcmdFreeHand_Click_Error:

    LogError "frmNotes", "cmdFreeHand_Click", Err, Err.Description, , PatientId, AppointmentId
End Sub

Private Sub cmdFrwd_Click()
If CurrentTextBox Is Nothing Then Exit Sub
Call SearchText(CurrentTextBox.Text, CurrentTextBox.SelStart + 2, False)
End Sub

Private Sub cmdMore_Click()
If (cmdMore.Text = "More Notes") Then
    txtMoreView.Text = ""
    If (cmdMore.Tag = "B") Then
        txtMoreView.Text = txtBillNotes.Text
    ElseIf (cmdMore.Tag = "G") Then
        txtMoreView.Text = txtChartNotes.Text
    ElseIf (cmdMore.Tag = "R") Then
        txtMoreView.Text = txtRxNotes.Text
    ElseIf (cmdMore.Tag = "D") Then
        txtMoreView.Text = txtCRMNotes.Text
    ElseIf (cmdMore.Tag = "T") Then
        txtMoreView.Text = txtToDo.Text
    ElseIf (cmdMore.Tag = "P") Then
        txtMoreView.Text = txtPrivateNotes.Text
    Else
        Exit Sub
    End If
    cmdMore.Text = "Close More"
    txtMoreView.Visible = True
    txtMoreView.SetFocus
    SendKeys "{Home}"
Else
    cmdMore.Text = "More Notes"
    txtMoreView.Text = ""
    txtMoreView.Visible = False
End If
End Sub

Private Sub cmdRxNotes_Click()
PatRunsUsed = pat.GetPatientAccess(PatientId)
If PatRunsUsed = -1 Then
                 frmEventMsgs.Header = " Access Blocked"
                 frmEventMsgs.AcceptText = ""
                 frmEventMsgs.RejectText = "Ok"
                 frmEventMsgs.CancelText = ""
                 frmEventMsgs.Other0Text = ""
                 frmEventMsgs.Other1Text = ""
                 frmEventMsgs.Other2Text = ""
                 frmEventMsgs.Other3Text = ""
                 frmEventMsgs.Other4Text = ""
                 frmEventMsgs.Show 1
  Exit Sub
 End If
If (cmdRxNotes.BackColor = BaseBackColor) Then
    Call SetNotesButtons
    cmdRxNotes.BackColor = SelectionBackColor
    cmdRxNotes.ForeColor = SelectionForeColor
    lstSelect.Visible = False
    chkInclude.Visible = False
    chkClaim.Visible = False
    chkRxInclude.Visible = True
    txtToDo.Visible = False
    txtCollNotes.Visible = False
    txtBillNotes.Visible = False
    txtSurgeryNotes.Visible = False
    txtPrivateNotes.Visible = False
    txtChartNotes.Visible = False
    txtRxNotes.Visible = True
    txtCRMNotes.Visible = False
    txtNewYNotes.Visible = False
    txtNewPNotes.Visible = False
    txtNewTNotes.Visible = False
    txtNewBNotes.Visible = False
    txtNewCNotes.Visible = False
    txtNewCRMNotes.Visible = False
    txtNewSNotes.Visible = False
    txtNewRNotes.Visible = True
    cmdMore.Visible = IsNotesDisplayFull(txtRxNotes.Text)
    cmdMore.Tag = "R"
    If Not (LoadOn) Then
        txtNewRNotes.SetFocus
        SendKeys "{End}"
    End If
    cmdStandardNote.Visible = False
    cmdStdRxNotes.Visible = True
    SetTo = "R"
End If
End Sub

Private Sub cmdScan_Click()
Call HideSearchControl
frmScan.Whom = ""
frmScan.PatientId = PatientId
frmScan.AppointmentId = AppointmentId
If (frmScan.LoadScan) Then
    frmScan.Show 1
End If
End Sub

Private Sub cmdStandardNote_Click()
Dim Temp As String
lstSelect.Visible = False
Call HideSearchControl
If (txtPrivateNotes.Visible) Then
    Call LoadSelectionList("REDNOTES", lstSelect, True, "")
    If (lstSelect.ListCount > 0) Then
        lstSelect.Visible = True
    End If
ElseIf (txtChartNotes.Visible) Then
    If (SystemReference = "IMPR") Then
        Call LoadSelectionList("IMPRESSIONNOTES", lstSelect, True, "")
    ElseIf (Left(SystemReference, 1) = "T") Then
        Call LoadSelectionList("TESTNOTES", lstSelect, True, "")
    ElseIf (Left(SystemReference, 5) = "IMPR-") Then
        Temp = Mid(SystemReference, 6, Len(SystemReference) - 5)
        Call LoadSelectionList("IMPRESSIONLINKPLANNOTES", lstSelect, True, Temp, SortFgc)
    ElseIf (Left(SystemReference, 5) = "IMDN-") Then
        Temp = Mid(SystemReference, 6, Len(SystemReference) - 5)
        Call LoadSelectionList("IMPRESSIONLINKDISCUSSION", lstSelect, True, Temp)
    Else
        Call LoadSelectionList("CHART", lstSelect, True, "")
    End If
    If (lstSelect.ListCount > 1) Then
        lstSelect.Visible = True
    End If
ElseIf (txtBillNotes.Visible) Then
    Call LoadSelectionList("BillingNotes", lstSelect, True, "")
    If (lstSelect.ListCount > 0) Then
        lstSelect.Visible = True
    End If
ElseIf (txtSurgeryNotes.Visible) Then
    Call LoadSelectionList("SurgeryNotes", lstSelect, True, "")
    If (lstSelect.ListCount > 0) Then
        lstSelect.Visible = True
    End If
ElseIf (txtCollNotes.Visible) Then
    Call LoadSelectionList("DELINQUENTBILLINGNOTES", lstSelect, True, "")
    If (lstSelect.ListCount > 1) Then
        lstSelect.Visible = True
    End If
End If
End Sub

Private Sub cmdStdRxNotes_Click()
lstSelect.Visible = False
Call HideSearchControl
If (txtRxNotes.Visible) Then
    Call LoadSelectionList("RXNOTES", lstSelect, True, "")
    If (lstSelect.ListCount > 0) Then
        lstSelect.Visible = True
    End If
End If
End Sub

Private Sub cmdStdCRMNote_Click()
lstSelect.Visible = False
Call HideSearchControl
If (txtCRMNotes.Visible) Then
    Call LoadSelectionList("DEMOGRAPHICSNOTES", lstSelect, True, "")
    If (lstSelect.ListCount > 0) Then
        lstSelect.Visible = True
    End If
End If
End Sub

Private Sub cmdSurgeryNotes_Click()
PatRunsUsed = pat.GetPatientAccess(PatientId)
If PatRunsUsed = -1 Then
    frmEventMsgs.Header = " Access Blocked"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
  Exit Sub
 End If
If (cmdSurgeryNotes.BackColor = BaseBackColor) Then
    Call SetNotesButtons
    cmdSurgeryNotes.BackColor = SelectionBackColor
    cmdSurgeryNotes.ForeColor = SelectionForeColor
    lstSelect.Visible = False
    chkInclude.Visible = False
    chkClaim.Visible = False
    chkRxInclude.Visible = False
    txtToDo.Visible = False
    txtBillNotes.Visible = False
    txtCollNotes.Visible = False
    txtChartNotes.Visible = False
    txtRxNotes.Visible = False
    txtCRMNotes.Visible = False
    txtPrivateNotes.Visible = False
    txtSurgeryNotes.Visible = True
    txtNewYNotes.Visible = False
    txtNewCNotes.Visible = False
    txtNewTNotes.Visible = False
    txtNewBNotes.Visible = False
    txtNewRNotes.Visible = False
    txtNewCRMNotes.Visible = False
    txtNewPNotes.Visible = False
    txtNewSNotes.Visible = True
    cmdMore.Visible = IsNotesDisplayFull(txtSurgeryNotes.Text)
    cmdMore.Tag = "S"
    If Not (LoadOn) Then
        txtNewSNotes.SetFocus
        SendKeys "{End}"
    End If
    cmdStandardNote.Visible = True
    cmdStandardNote.Text = "Standard Surgery Notes"
    cmdStdRxNotes.Visible = False
    SetTo = "S"
End If
End Sub

Private Sub cmdToDo_Click()
If (cmdToDo.BackColor = BaseBackColor) Or (lblPatName.Caption = "Practice Notes") Then
    Call SetNotesButtons
    cmdToDo.BackColor = SelectionBackColor
    cmdToDo.ForeColor = SelectionForeColor
    lstSelect.Visible = False
    chkInclude.Visible = False
    chkClaim.Visible = False
    chkRxInclude.Visible = False
    txtBillNotes.Visible = False
    txtSurgeryNotes.Visible = False
    txtCollNotes.Visible = False
    txtPrivateNotes.Visible = False
    txtChartNotes.Visible = False
    txtRxNotes.Visible = False
    txtCRMNotes.Visible = False
    txtSurgeryNotes.Visible = False
    txtToDo.Visible = True
    txtNewCNotes.Visible = False
    txtNewYNotes.Visible = False
    txtNewPNotes.Visible = False
    txtNewBNotes.Visible = False
    txtNewRNotes.Visible = False
    txtNewCRMNotes.Visible = False
    txtNewSNotes.Visible = False
    txtNewSNotes.Visible = False
    txtNewTNotes.Visible = True
    cmdMore.Visible = IsNotesDisplayFull(txtToDo.Text)
    cmdMore.Tag = "T"
    If Not (LoadOn) Then
        txtNewTNotes.SetFocus
        SendKeys "{End}"
    End If
    cmdStandardNote.Visible = False
    cmdStdRxNotes.Visible = False
    SetTo = "T"
End If
End Sub

Private Sub cmdMessages_Click()
If (cmdMessages.BackColor = BaseBackColor) Then
    Call SetNotesButtons
    cmdMessages.BackColor = SelectionBackColor
    cmdMessages.ForeColor = SelectionForeColor
    lstSelect.Visible = False
    cmdStandardNote.Visible = False
    cmdStdRxNotes.Visible = False
    frmSendMessage.PatientId = PatientId
    If (frmSendMessage.LoadNotes) Then
        frmSendMessage.Show 1
    End If
    Call cmdGeneralNotes_Click
End If
End Sub

Private Sub cmdPrivateNotes_Click()
If (cmdPrivateNotes.BackColor = BaseBackColor) Then
    Call SetNotesButtons
    cmdPrivateNotes.BackColor = SelectionBackColor
    cmdPrivateNotes.ForeColor = SelectionForeColor
    lstSelect.Visible = False
    chkInclude.Visible = False
    chkClaim.Visible = False
    chkRxInclude.Visible = False
    txtToDo.Visible = False
    txtCollNotes.Visible = False
    txtBillNotes.Visible = False
    txtSurgeryNotes.Visible = False
    txtChartNotes.Visible = False
    txtRxNotes.Visible = False
    txtCRMNotes.Visible = False
    txtPrivateNotes.Visible = True
    txtNewCNotes.Visible = False
    txtNewYNotes.Visible = False
    txtNewTNotes.Visible = False
    txtNewBNotes.Visible = False
    txtNewRNotes.Visible = False
    txtNewCRMNotes.Visible = False
    txtNewSNotes.Visible = False
    txtNewPNotes.Visible = True
    cmdMore.Visible = IsNotesDisplayFull(txtPrivateNotes.Text)
    cmdMore.Tag = "P"
    If Not (LoadOn) Then
        txtNewPNotes.SetFocus
        SendKeys "{End}"
    End If
    cmdStandardNote.Visible = True
    cmdStandardNote.Text = "Standard Notes"
    cmdStdRxNotes.Visible = False
    SetTo = "P"
End If
End Sub

Private Sub cmdGeneralNotes_Click()
Dim Temp As String
If (cmdGeneralNotes.BackColor = BaseBackColor) Then
    Call SetNotesButtons
    cmdGeneralNotes.BackColor = SelectionBackColor
    cmdGeneralNotes.ForeColor = SelectionForeColor
    lstSelect.Visible = False
    chkInclude.Visible = False
    chkClaim.Visible = False
    chkRxInclude.Visible = False
    txtToDo.Visible = False
    txtCollNotes.Visible = False
    txtBillNotes.Visible = False
    txtSurgeryNotes.Visible = False
    txtPrivateNotes.Visible = False
    txtRxNotes.Visible = False
    txtCRMNotes.Visible = False
    txtChartNotes.Visible = True
    txtNewPNotes.Visible = False
    txtNewYNotes.Visible = False
    txtNewTNotes.Visible = False
    txtNewBNotes.Visible = False
    txtNewRNotes.Visible = False
    txtNewCRMNotes.Visible = False
    txtNewSNotes.Visible = False
    txtNewCNotes.Visible = True
    cmdMore.Visible = IsNotesDisplayFull(txtChartNotes.Text)
    cmdMore.Tag = "G"
    If Not (LoadOn) Then
        txtNewCNotes.SetFocus
        SendKeys "{End}"
    End If
    cmdStdRxNotes.Visible = False
    cmdStdCRMNote.Visible = False
    cmdStandardNote.Visible = True
    cmdStandardNote.Text = "Standard Notes"
    If (SystemReference = "IMPR") Then
        cmdStandardNote.Text = "Impression Notes"
    ElseIf (Left(SystemReference, 1) = "T") Then
        cmdStandardNote.Text = "Exam Element Notes"
    ElseIf (Left(SystemReference, 5) = "IMPR-") Then
        Temp = Trim(Mid(SystemReference, 6, Len(SystemReference) - 5))
        Call LoadSelectionList("IMPRESSIONLINKPLANNOTES", lstSelect, True, Temp, SortFgc)
        If (lstSelect.ListCount > 0) Then
            cmdStandardNote.Text = "Impr Link Notes"
            lstSelect.Visible = True
        Else
            cmdStandardNote.Text = "Impression Notes"
        End If
    ElseIf (Left(SystemReference, 5) = "IMDN-") Then
        Temp = Mid(SystemReference, 6, Len(SystemReference) - 4)
        Call LoadSelectionList("IMPRESSIONLINKDISCUSSIONS", lstSelect, True, Temp)
        cmdStandardNote.Text = "Impr Discussions"
    End If
    SetTo = "C"
Else
    PatRunsUsed = pat.GetPatientAccess(PatientId)
    If PatRunsUsed = -1 Then
        frmEventMsgs.Header = " Access Blocked"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
 End If
End Sub

Private Sub cmdDone_Click()
Dim p As Integer
Dim r As Integer
Dim TheNote As String
Dim Notes(4) As String
Dim RetrieveNotes As PatientNotes
Set RetrieveNotes = New PatientNotes

  If PatRunsUsed = 1 Then
       Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
    End If
    
lstSelect.Visible = False
If (SetTo = "R") And (chkRxInclude.Value = 0) And (Trim(SystemReference) <> "") Then
    frmEventMsgs.Header = "Print this note on script?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        chkRxInclude.Value = 1
    End If
End If
If (SetTo <> "T") Then
    TheNote = "C"
    For p = 1 To 4
        Notes(p) = ""
    Next p
    r = (Len(txtNewCNotes.Text) / 255) + 1
    If (r > 4) Then
        r = 4
    End If
    For p = 1 To r
        Notes(p) = Trim(Mid(txtNewCNotes.Text, ((p - 1) * 255 + 1), 255))
    Next p
    GoSub PostIt

    TheNote = "P"
    For p = 1 To 4
        Notes(p) = ""
    Next p
    r = (Len(txtNewPNotes.Text) / 255) + 1
    If (r > 4) Then
        r = 4
    End If
    For p = 1 To r
        Notes(p) = Trim(Mid(txtNewPNotes.Text, ((p - 1) * 255 + 1), 255))
    Next p
    GoSub PostIt

    TheNote = "R"
    For p = 1 To 4
        Notes(p) = ""
    Next p
    r = (Len(txtNewRNotes.Text) / 255) + 1
    If (r > 4) Then
        r = 4
    End If
    For p = 1 To r
        Notes(p) = Trim(Mid(txtNewRNotes.Text, ((p - 1) * 255 + 1), 255))
    Next p
    GoSub PostIt

    TheNote = "D"
    For p = 1 To 4
        Notes(p) = ""
    Next p
    r = (Len(txtNewCRMNotes.Text) / 255) + 1
    If (r > 4) Then
        r = 4
    End If
    For p = 1 To r
        Notes(p) = Trim(Mid(txtNewCRMNotes.Text, ((p - 1) * 255 + 1), 255))
    Next p
    GoSub PostIt
    
    TheNote = "S"
    For p = 1 To 4
        Notes(p) = ""
    Next p
    r = (Len(txtNewSNotes.Text) / 255) + 1
    If r > 4 Then
        r = 4
    End If
    For p = 1 To r
        Notes(p) = Trim(Mid(txtNewSNotes.Text, ((p - 1) * 255 + 1), 255))
    Next p
    GoSub PostIt
    
    TheNote = "Y"
    For p = 1 To 4
        Notes(p) = ""
    Next p
    r = (Len(txtNewYNotes.Text) / 255) + 1
    If r > 4 Then
        r = 4
    End If
    For p = 1 To r
        Notes(p) = Trim(Mid(txtNewYNotes.Text, ((p - 1) * 255 + 1), 255))
    Next p
    GoSub PostIt
Else
    TheNote = "T"
    For p = 1 To 4
        Notes(p) = ""
    Next p
    r = (Len(txtNewTNotes.Text) / 255) + 1
    If (r > 4) Then
        r = 4
    End If
    For p = 1 To r
        Notes(p) = Trim(Mid(txtNewTNotes.Text, ((p - 1) * 255 + 1), 255))
    Next p
    GoSub PostIt
End If

Set RetrieveNotes = Nothing
NoteId = 0
EyeContext = ""
CurrentAction = ""
Unload frmNotes
Exit Sub
PostIt:
    If (Trim(Notes(1)) <> "") Then
        RetrieveNotes.NotesId = TheNoteId
        If (RetrieveNotes.RetrieveNotes) Then
            If (TheNoteId = 0) Then
                RetrieveNotes.NotesPatientId = PatientId
                RetrieveNotes.NotesAppointmentId = AppointmentId
                RetrieveNotes.NotesDate = TodaysDate
            End If
            If (SetTo = "T") Then
                If (TheNote = "T") Then
                    If (PatientId < 1) And (AppointmentId < 1) Then
                        RetrieveNotes.NotesPatientId = -1
                        RetrieveNotes.NotesAppointmentId = -1
                    End If
                End If
            End If
            RetrieveNotes.NotesType = TheNote
            RetrieveNotes.NotesText1 = Trim(Notes(1))
            RetrieveNotes.NotesText2 = Trim(Notes(2))
            RetrieveNotes.NotesText3 = Trim(Notes(3))
            RetrieveNotes.NotesText4 = Trim(Notes(4))
            RetrieveNotes.NotesUser = Trim(UserId)
            RetrieveNotes.NotesSystem = Trim(LocalSysRef)
            RetrieveNotes.NotesEye = Trim(EyeContext)
            RetrieveNotes.NotesCategory = Trim(Left(NoteCategorySelected, 1))
            RetrieveNotes.NotesAlertMask = NoteAlertSelected
            RetrieveNotes.NotesCommentOn = False
            If (chkInclude.Value = 1) Or (chkRxInclude.Value = 1) Then
                RetrieveNotes.NotesCommentOn = True
            End If
            RetrieveNotes.NotesClaimOn = False
            If (chkClaim.Value = 1) Then
                RetrieveNotes.NotesClaimOn = True
            End If
            RetrieveNotes.NotesILPNRef = ""
            If (Left(SystemReference, 5) = "IMPR-") Then
                RetrieveNotes.NotesILPNRef = ILPNRef
            ElseIf (Left(SystemReference, 5) = "IMDN-") Then
                RetrieveNotes.NotesILPNRef = ILPNRef
            End If
            If (TheNoteId = 0) Then
                RetrieveNotes.NotesHighlight = ""
            End If
            If (Trim(RetrieveNotes.NotesSystem) = "") Then
                RetrieveNotes.NotesHighlight = "V"
            End If
            If Not (RetrieveNotes.ApplyNotes) Then
                Call PinpointError("frmNotes-Done", "Update Failed")
            End If
            If (TheNote = "C") Then
                NoteOn = True
            End If
        End If
    End If
    Return
End Sub

Private Sub cmdHome_Click()
If PatRunsUsed = 1 Then
    Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
 End If
CurrentAction = "Home"
EyeContext = ""
Unload frmNotes
End Sub

Public Function LoadNotes() As Boolean
Dim i As Integer
Dim p As Integer
Dim Temp As String
Dim Temp9 As String
Dim txtChartNotesText As String
Dim LocalDate As ManagedDate
Dim RetPat As Patient
Dim RetrieveAppointment As SchedulerAppointment
Dim RetrieveResource As SchedulerResource
Dim RetrieveNotes As PatientNotes

If Not UserLogin.HasPermission(epPatientNotes) Then
    frmEventMsgs.InfoMessage "Not Permissioned"
    frmEventMsgs.Show 1
    LoadNotes = False
    Exit Function
End If
LoadNotes = True
If (SetTo = "ILPN") Or (SetTo = "IMDN") Then
    Call LoadILPNotesOnly
    Exit Function
End If
LoadOn = True
UserId = ""
ILPNRef = ""
NoteOn = False
TheNoteId = 0
NoteSelectCatOn = False
NoteCategorySelected = ""
lblCat.Caption = "Category:"
NoteAlertDisplay = ""
NoteAlertSelected = String(32, "0")
LocalSysRef = SystemReference
BSDisplay(1) = "HPI"
BSDisplay(2) = ""
BSDisplay(3) = "OHX"
BSDisplay(4) = "MHX"
BSDisplay(5) = "FMX"
BSDisplay(6) = ""
BSDisplay(7) = "ROS"
BSDisplay(8) = "Meds"
BSDisplay(9) = "ALG"
BSDisplay(10) = "CC"
cmdMore.Visible = False
lstSelect.Visible = False
txtNewBNotes.Text = ""
txtNewBNotes.Visible = False
txtNewPNotes.Text = ""
txtNewPNotes.Visible = False
txtNewCNotes.Text = ""
txtNewCNotes.Visible = False
txtNewTNotes.Text = ""
txtNewTNotes.Visible = False
txtNewRNotes.Text = ""
txtNewRNotes.Visible = False
txtNewCRMNotes.Text = ""
txtNewCRMNotes.Visible = False
txtNewYNotes.Text = ""
txtNewYNotes.Visible = False
txtNewSNotes.Text = ""
txtNewSNotes.Visible = False
txtChartNotes.Text = ""
txtPrivateNotes.Text = ""
txtBillNotes.Text = ""
txtToDo.Text = ""
txtRxNotes.Text = ""
txtSurgeryNotes.Text = ""
txtCRMNotes.Text = ""
If (PatientId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatientId
    If (RetPat.RetrievePatient) Then
        lblPatName.Caption = Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial) + " " _
                           + Trim(RetPat.LastName) + " " + Trim(RetPat.NameRef)
    End If
    Set RetPat = Nothing
Else
    lblPatName.Caption = "Practice Notes"
    lblPatName.Visible = True
End If
MaintainAlertOn = False
Set RetrieveResource = New SchedulerResource
RetrieveResource.ResourceId = UserLogin.iId
If (RetrieveResource.RetrieveSchedulerResource) Then
    UserId = Trim(RetrieveResource.ResourceName)
End If
If UserLogin.HasPermission(epSetAlerts) Then
    MaintainAlertOn = True
End If
Set LocalDate = New ManagedDate
TodaysDate = ""
Call FormatTodaysDate(TodaysDate, False)
LocalDate.ExposedDate = TodaysDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    TodaysDate = LocalDate.ExposedDate
Else
    TodaysDate = ""
End If
Set RetrieveAppointment = New SchedulerAppointment
Set RetrieveNotes = New PatientNotes
If (PatientId > 0) Then
    txtChartNotes.Text = ""
    txtCollNotes.Text = ""
    txtPrivateNotes.Text = ""
    txtRxNotes.Text = ""
    txtBillNotes.Text = ""
    txtCRMNotes.Text = ""
    RetrieveNotes.NotesPatientId = PatientId
    RetrieveNotes.NotesType = ""
    If (RetrieveNotes.FindNotesbyPatient > 0) Then
        i = 1
        While (RetrieveNotes.SelectNotes(i))
            If (RetrieveNotes.NotesHighlight <> "]") Then
                GoSub LoadIt
            End If
            i = i + 1
        Wend
    End If
    If (SetTo = "C") Then
        txtCollNotes.Locked = True
        txtCollNotes.Visible = False
        txtChartNotes.Locked = True
        txtChartNotes.Visible = True
        txtPrivateNotes.Locked = True
        txtPrivateNotes.Visible = False
        txtBillNotes.Locked = True
        txtBillNotes.Visible = False
        txtRxNotes.Locked = True
        txtRxNotes.Visible = False
        txtSurgeryNotes.Locked = True
        txtSurgeryNotes.Visible = False
        txtCRMNotes.Locked = True
        txtCRMNotes.Visible = False
    ElseIf (SetTo = "R") Then
        txtCollNotes.Locked = True
        txtCollNotes.Visible = False
        txtChartNotes.Locked = True
        txtChartNotes.Visible = False
        txtPrivateNotes.Locked = True
        txtPrivateNotes.Visible = False
        txtBillNotes.Locked = True
        txtBillNotes.Visible = False
        txtRxNotes.Locked = True
        txtRxNotes.Visible = True
        txtSurgeryNotes.Locked = True
        txtSurgeryNotes.Visible = False
        txtCRMNotes.Locked = True
        txtCRMNotes.Visible = False
    ElseIf (SetTo = "P") Then
        txtCollNotes.Locked = True
        txtCollNotes.Visible = False
        txtChartNotes.Locked = True
        txtChartNotes.Visible = False
        txtPrivateNotes.Locked = True
        txtPrivateNotes.Visible = True
        txtBillNotes.Locked = True
        txtBillNotes.Visible = False
        txtRxNotes.Locked = True
        txtRxNotes.Visible = False
        txtSurgeryNotes.Locked = True
        txtSurgeryNotes.Visible = False
        txtCRMNotes.Locked = True
        txtCRMNotes.Visible = False
    ElseIf (SetTo = "B") Then
        txtCollNotes.Locked = True
        txtCollNotes.Visible = False
        txtChartNotes.Locked = True
        txtChartNotes.Visible = False
        txtPrivateNotes.Locked = True
        txtPrivateNotes.Visible = False
        txtBillNotes.Locked = True
        txtBillNotes.Visible = True
        txtRxNotes.Locked = True
        txtRxNotes.Visible = False
        txtSurgeryNotes.Locked = True
        txtSurgeryNotes.Visible = False
        txtCRMNotes.Locked = True
        txtCRMNotes.Visible = False
    ElseIf (SetTo = "D") Then
        txtCollNotes.Locked = True
        txtCollNotes.Visible = False
        txtChartNotes.Locked = True
        txtChartNotes.Visible = False
        txtPrivateNotes.Locked = True
        txtPrivateNotes.Visible = False
        txtBillNotes.Locked = True
        txtBillNotes.Visible = False
        txtRxNotes.Locked = True
        txtRxNotes.Visible = False
        txtSurgeryNotes.Locked = True
        txtSurgeryNotes.Visible = False
        txtCRMNotes.Locked = True
        txtCRMNotes.Visible = True
    ElseIf (SetTo = "S") Then
        txtCollNotes.Locked = True
        txtCollNotes.Visible = False
        txtChartNotes.Locked = True
        txtChartNotes.Visible = False
        txtPrivateNotes.Locked = True
        txtPrivateNotes.Visible = False
        txtBillNotes.Locked = True
        txtBillNotes.Visible = False
        txtRxNotes.Locked = True
        txtRxNotes.Visible = False
        txtCRMNotes.Locked = True
        txtCRMNotes.Visible = False
        txtSurgeryNotes.Locked = True
        txtSurgeryNotes.Visible = True
    ElseIf (SetTo = "Y") Then
        txtCollNotes.Locked = True
        txtCollNotes.Visible = True
        txtChartNotes.Locked = True
        txtChartNotes.Visible = False
        txtPrivateNotes.Locked = True
        txtPrivateNotes.Visible = False
        txtBillNotes.Locked = True
        txtBillNotes.Visible = False
        txtRxNotes.Locked = True
        txtRxNotes.Visible = False
        txtCRMNotes.Locked = True
        txtCRMNotes.Visible = False
        txtSurgeryNotes.Locked = True
        txtSurgeryNotes.Visible = False
    End If
Else
    txtToDo.Text = ""
    If (SetTo = "T") Then
        RetrieveNotes.NotesPatientId = -1
        RetrieveNotes.NotesAppointmentId = -1
        RetrieveNotes.NotesType = "T"
        RetrieveNotes.NotesSystem = ""
        If (RetrieveNotes.FindNotesbyPatient > 0) Then
            i = 1
            While (RetrieveNotes.SelectNotes(i))
                GoSub LoadIt
                i = i + 1
            Wend
        End If
    End If
    txtToDo.Locked = True
    txtToDo.Visible = False
End If
Set RetrieveNotes = Nothing
chkRxInclude.Visible = False
chkRxInclude.Value = 0
chkInclude.Visible = False
chkInclude.Value = 0
chkClaim.Visible = False
chkClaim.Value = 0

' Means we are editting an Existing Note
If (NoteId > 0) Then
    TheNoteId = NoteId
    Set RetrieveNotes = New PatientNotes
    RetrieveNotes.NotesId = NoteId
    If (RetrieveNotes.RetrieveNotes) Then
        NoteAlertDisplay = ""
        NoteAlertSelected = RetrieveNotes.NotesAlertMask
        If (Trim(NoteAlertSelected) = "") Then
            NoteAlertSelected = String(32, "0")
        End If
        For p = 1 To Len(NoteAlertSelected)
            If (Mid(NoteAlertSelected, p, 1) <> "0") Then
                If (NoteAlertDisplay = "") Then
                    NoteAlertDisplay = NoteAlertDisplay + Trim(str(p))
                Else
                    NoteAlertDisplay = NoteAlertDisplay + ", " + Trim(str(p))
                End If
            End If
        Next p
        lblAlert.Caption = "Alert: " + NoteAlertDisplay
        lblRetired.Visible = False
        If (Trim(RetrieveNotes.NotesOffDate) <> "") Then
            lblRetired.Visible = True
        End If
        If ((RetrieveNotes.NotesSystem) <> "") Then
            LocalSysRef = RetrieveNotes.NotesSystem
        End If
        EyeContext = RetrieveNotes.NotesEye
        If (RetrieveNotes.NotesType = "B") Then
            txtBillNotes.Visible = True
            txtSurgeryNotes.Visible = False
            txtRxNotes.Visible = False
            txtCRMNotes.Visible = False
            txtPrivateNotes.Visible = False
            txtChartNotes.Visible = False
            txtToDo.Visible = False
            txtCollNotes.Visible = False
            chkInclude.Visible = True
            If (RetrieveNotes.NotesCommentOn) Then
                chkInclude.Value = 1
            End If
            chkClaim.Visible = True
            If (RetrieveNotes.NotesClaimOn) Then
                chkClaim.Value = 1
            End If
            SetTo = "B"
        ElseIf (RetrieveNotes.NotesType = "R") Then
            txtNewRNotes.Text = Trim(RetrieveNotes.NotesText1) _
                              + Trim(RetrieveNotes.NotesText2) _
                              + Trim(RetrieveNotes.NotesText3) _
                              + Trim(RetrieveNotes.NotesText4)
            txtRxNotes.Visible = True
            txtSurgeryNotes.Visible = False
            txtCRMNotes.Visible = False
            txtBillNotes.Visible = False
            txtPrivateNotes.Visible = False
            txtChartNotes.Visible = False
            txtToDo.Visible = False
            txtCollNotes.Visible = False
            chkRxInclude.Visible = True
            If (RetrieveNotes.NotesCommentOn) Then
                chkRxInclude.Value = 1
            End If
            SetTo = "R"
        ElseIf (RetrieveNotes.NotesType = "Y") Then
            txtNewYNotes.Text = Trim(RetrieveNotes.NotesText1) _
                              + Trim(RetrieveNotes.NotesText2) _
                              + Trim(RetrieveNotes.NotesText3) _
                              + Trim(RetrieveNotes.NotesText4)
            txtNewYNotes.Visible = True
            txtCRMNotes.Visible = False
            txtSurgeryNotes.Visible = False
            txtRxNotes.Visible = False
            txtBillNotes.Visible = False
            txtPrivateNotes.Visible = False
            txtChartNotes.Visible = False
            txtToDo.Visible = False
            SetTo = "Y"
        ElseIf (RetrieveNotes.NotesType = "D") Then
            txtNewCRMNotes.Text = Trim(RetrieveNotes.NotesText1) _
                                + Trim(RetrieveNotes.NotesText2) _
                                + Trim(RetrieveNotes.NotesText3) _
                                + Trim(RetrieveNotes.NotesText4)
            txtCRMNotes.Visible = True
            txtSurgeryNotes.Visible = False
            txtRxNotes.Visible = False
            txtBillNotes.Visible = False
            txtPrivateNotes.Visible = False
            txtChartNotes.Visible = False
            txtToDo.Visible = False
            txtCollNotes.Visible = False
            SetTo = "D"
        ElseIf (RetrieveNotes.NotesType = "C") Then
            txtNewCNotes.Text = Trim(RetrieveNotes.NotesText1) _
                              + Trim(RetrieveNotes.NotesText2) _
                              + Trim(RetrieveNotes.NotesText3) _
                              + Trim(RetrieveNotes.NotesText4)
            txtChartNotes.Visible = True
            txtSurgeryNotes.Visible = False
            txtRxNotes.Visible = False
            txtCRMNotes.Visible = False
            txtBillNotes.Visible = False
            txtPrivateNotes.Visible = False
            txtToDo.Visible = False
            txtCollNotes.Visible = False
            NoteCategorySelected = RetrieveNotes.NotesCategory
            lblCat.Caption = "Category:" + NoteCategorySelected
            lblCat.Visible = True
            SetTo = "C"
        ElseIf (RetrieveNotes.NotesType = "P") Then
            txtNewPNotes.Text = Trim(RetrieveNotes.NotesText1) _
                              + Trim(RetrieveNotes.NotesText2) _
                              + Trim(RetrieveNotes.NotesText3) _
                              + Trim(RetrieveNotes.NotesText4)
            txtPrivateNotes.Visible = True
            txtSurgeryNotes.Visible = False
            txtRxNotes.Visible = False
            txtCRMNotes.Visible = False
            txtBillNotes.Visible = False
            txtChartNotes.Visible = False
            txtToDo.Visible = False
            txtCollNotes.Visible = False
            SetTo = "P"
        ElseIf (RetrieveNotes.NotesType = "T") Then
            txtNewTNotes.Text = Trim(RetrieveNotes.NotesText1) _
                              + Trim(RetrieveNotes.NotesText2) _
                              + Trim(RetrieveNotes.NotesText3) _
                              + Trim(RetrieveNotes.NotesText4)
            txtToDo.Visible = True
            txtSurgeryNotes.Visible = False
            txtRxNotes.Visible = False
            txtCRMNotes.Visible = False
            txtBillNotes.Visible = False
            txtPrivateNotes.Visible = False
            txtChartNotes.Visible = False
            txtCollNotes.Visible = False
            SetTo = "T"
        ElseIf (RetrieveNotes.NotesType = "S") Then
            txtNewSNotes.Text = Trim(RetrieveNotes.NotesText1) _
                              + Trim(RetrieveNotes.NotesText2) _
                              + Trim(RetrieveNotes.NotesText3) _
                              + Trim(RetrieveNotes.NotesText4)
            txtSurgeryNotes.Visible = True
            txtRxNotes.Visible = False
            txtToDo.Visible = False
            txtCRMNotes.Visible = False
            txtBillNotes.Visible = False
            txtPrivateNotes.Visible = False
            txtChartNotes.Visible = False
            txtCollNotes.Visible = False
            SetTo = "S"
        End If
    End If
End If
Set RetrieveResource = Nothing
Set RetrieveNotes = Nothing
Set RetrieveAppointment = Nothing
Set LocalDate = Nothing
If (SetTo = "B") Then
    Call cmdBillNotes_Click
ElseIf (SetTo = "P") Then
    Call cmdPrivateNotes_Click
ElseIf (SetTo = "Y") Then
    Call cmdCollNotes_Click
ElseIf (SetTo = "T") Then
    Call cmdToDo_Click
ElseIf (SetTo = "R") Then
    Call cmdRxNotes_Click
ElseIf (SetTo = "D") Then
    Call cmdCRMNotes_Click
ElseIf (SetTo = "S") Then
    Call cmdSurgeryNotes_Click
Else
    SetTo = "C"
    Call cmdGeneralNotes_Click
End If
Call SetVisibleOptions
If (txtChartNotes.Visible) Then
    txtNewCNotes.Visible = True
    txtNewCNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtPrivateNotes.Visible) Then
    txtNewPNotes.Visible = True
    txtNewPNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtToDo.Visible) Then
    txtNewTNotes.Visible = True
    txtNewTNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtRxNotes.Visible) Then
    txtNewRNotes.Visible = True
    txtNewRNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtCRMNotes.Visible) Then
    txtNewCRMNotes.Visible = True
    txtNewCRMNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtBillNotes.Visible) Then
    txtNewBNotes.Visible = True
    txtNewBNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtCollNotes.Visible) Then
    txtNewYNotes.Visible = True
    txtNewYNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtSurgeryNotes.Visible) Then
    txtNewSNotes.Visible = True
    txtNewSNotes.SetFocus
    SendKeys "{Home}"
End If
LoadOn = False
txtChartNotes.Text = txtChartNotes.Text + txtChartNotesText
Exit Function
LoadIt:
    LocalDate.ExposedDate = RetrieveNotes.NotesDate
    If (LocalDate.ConvertManagedDateToDisplayDate) Then
        ApptDate = LocalDate.ExposedDate
    Else
        ApptDate = ""
    End If
    Temp = Trim$(str(RetrieveNotes.NotesId))
    If (Len(Temp) < 6) Then
        Temp = String(5 - Len(Temp), 48) + Temp
    Else
        Temp = Trim$(Temp)
    End If
    
    Temp = "[" + Temp + "]"
    Temp9 = ""
    If (Trim$(RetrieveNotes.NotesOffDate) <> "") Then
        Temp9 = "(Retired) "
    End If
    If (RetrieveNotes.NotesType = "C") Then
        txtChartNotesText = txtChartNotesText + Temp + Temp9 + ApptDate + ":"
        If (Trim$(RetrieveNotes.NotesSystem) <> "") Then
            If (Len(Trim$(RetrieveNotes.NotesSystem)) < 2) Or (Trim$(RetrieveNotes.NotesSystem) = "10") Then
                txtChartNotesText = txtChartNotesText + BSDisplay(Val(Trim$(RetrieveNotes.NotesSystem))) + ":"
                If (Trim$(RetrieveNotes.NotesEye) <> "") Then
                    txtChartNotesText = txtChartNotesText + Trim$(RetrieveNotes.NotesEye) + ":"
                Else
                    txtChartNotesText = txtChartNotesText + ":"
                End If
            Else
                txtChartNotesText = txtChartNotesText + Trim$(RetrieveNotes.NotesSystem) + ":"
            End If
        Else        ' now accomodate plan notes with other general notes
            txtChartNotesText = txtChartNotesText + "<Plan>"
        End If
        txtChartNotesText = txtChartNotesText _
                           + Trim$(RetrieveNotes.NotesText1) _
                           + Trim$(RetrieveNotes.NotesText2) _
                           + Trim$(RetrieveNotes.NotesText3) _
                           + Trim$(RetrieveNotes.NotesText4)
        If (Len(txtChartNotesText) > 3) Then
            If (Mid$(txtChartNotesText, Len(txtChartNotesText), 1) <> Chr(10)) Then
                txtChartNotesText = txtChartNotesText + Chr(13) + Chr(10)
            End If
        End If
    ElseIf (RetrieveNotes.NotesType = "P") Then
        txtPrivateNotes.Text = txtPrivateNotes.Text + Temp + Temp9 + ApptDate + ":" _
                             + Trim$(RetrieveNotes.NotesText1) _
                             + Trim$(RetrieveNotes.NotesText2) _
                             + Trim$(RetrieveNotes.NotesText3) _
                             + Trim$(RetrieveNotes.NotesText4)
        If (Len(txtPrivateNotes.Text) > 3) Then
            If (Mid$(txtPrivateNotes.Text, Len(txtPrivateNotes.Text), 1) <> Chr(10)) Then
                txtPrivateNotes.Text = txtPrivateNotes.Text + Chr(13) + Chr(10)
            End If
        End If
    ElseIf (RetrieveNotes.NotesType = "Y") Then
        txtCollNotes.Text = txtCollNotes.Text + Temp + Temp9 + ApptDate + ":" _
                          + Trim$(RetrieveNotes.NotesText1) _
                          + Trim$(RetrieveNotes.NotesText2) _
                          + Trim$(RetrieveNotes.NotesText3) _
                          + Trim$(RetrieveNotes.NotesText4)
        If (Len(txtCollNotes.Text) > 3) Then
            If (Mid$(txtCollNotes.Text, Len(txtCollNotes.Text), 1) <> Chr(10)) Then
                txtCollNotes.Text = txtCollNotes.Text + Chr(13) + Chr(10)
            End If
        End If
    ElseIf (RetrieveNotes.NotesType = "R") Or (RetrieveNotes.NotesType = "X") Then
        txtRxNotes.Text = txtRxNotes.Text + Temp + Temp9 + ApptDate + ":"
        If (Trim$(RetrieveNotes.NotesSystem) <> "") Then
            If (Len(Trim$(RetrieveNotes.NotesSystem)) <= 2) Or (Trim$(RetrieveNotes.NotesSystem) = "10") Then
                txtRxNotes.Text = txtRxNotes.Text + BSDisplay(Val(Trim$(RetrieveNotes.NotesSystem))) + ":"
                If (Trim$(RetrieveNotes.NotesEye) <> "") Then
                    txtRxNotes.Text = txtRxNotes.Text + Trim$(RetrieveNotes.NotesEye) + ":"
                End If
            Else
                txtRxNotes.Text = txtRxNotes.Text + Trim$(RetrieveNotes.NotesSystem) + ":"
            End If
        End If
        txtRxNotes.Text = txtRxNotes.Text _
                        + Trim$(RetrieveNotes.NotesText1) _
                        + Trim$(RetrieveNotes.NotesText2) _
                        + Trim$(RetrieveNotes.NotesText3) _
                        + Trim$(RetrieveNotes.NotesText4)
        If (Len(txtRxNotes.Text) > 3) Then
            If (Mid$(txtRxNotes.Text, Len(txtRxNotes.Text), 1) <> Chr(10)) Then
                txtRxNotes.Text = txtRxNotes.Text + Chr(13) + Chr(10)
            End If
        End If
    
    
    ElseIf (RetrieveNotes.NotesType = "D") Then
        txtCRMNotes.Text = txtCRMNotes.Text + Temp + Temp9 + ApptDate + ":"
        txtCRMNotes.Text = txtCRMNotes.Text _
                         + Trim$(RetrieveNotes.NotesText1) _
                         + Trim$(RetrieveNotes.NotesText2) _
                         + Trim$(RetrieveNotes.NotesText3) _
                         + Trim$(RetrieveNotes.NotesText4)
        If (Len(txtCRMNotes.Text) > 3) Then
            If (Mid$(txtCRMNotes.Text, Len(txtCRMNotes.Text), 1) <> Chr(10)) Then
                txtCRMNotes.Text = txtCRMNotes.Text + Chr(13) + Chr(10)
            End If
        End If
    ElseIf (RetrieveNotes.NotesType = "S") Then
        txtSurgeryNotes.Text = txtSurgeryNotes.Text + Temp + Temp9 + ApptDate + ":"
        txtSurgeryNotes.Text = txtSurgeryNotes.Text _
                         + Trim$(RetrieveNotes.NotesText1) _
                         + Trim$(RetrieveNotes.NotesText2) _
                         + Trim$(RetrieveNotes.NotesText3) _
                         + Trim$(RetrieveNotes.NotesText4)
        If (Len(txtSurgeryNotes.Text) > 3) Then
            If (Mid$(txtSurgeryNotes.Text, Len(txtSurgeryNotes.Text), 1) <> Chr(10)) Then
                txtSurgeryNotes.Text = txtSurgeryNotes.Text + Chr(13) + Chr(10)
            End If
        End If
    ElseIf (RetrieveNotes.NotesType = "T") Then
        txtToDo.Text = txtToDo.Text + Temp + Temp9 + ApptDate + ":" _
                     + Trim$(RetrieveNotes.NotesText1) _
                     + Trim$(RetrieveNotes.NotesText2) _
                     + Trim$(RetrieveNotes.NotesText3) _
                     + Trim$(RetrieveNotes.NotesText4)
        If (Len(txtToDo.Text) > 3) Then
            If (Mid$(txtToDo.Text, Len(txtToDo.Text), 1) <> Chr(10)) Then
                txtToDo.Text = txtToDo.Text + Chr(13) + Chr(10)
            End If
        End If
    End If
    Return
End Function

Private Function LoadILPNotesOnly() As Boolean
Dim LocalDate As ManagedDate
Dim RetPat As Patient
LoadILPNotesOnly = True
LoadOn = True
UserId = ""
ILPNRef = ""
NoteOn = False
TheNoteId = 0
MaintainAlertOn = False
NoteSelectCatOn = False
NoteCategorySelected = ""
lblCat.Caption = "Category:"
NoteAlertDisplay = ""
NoteAlertSelected = String(32, "0")
LocalSysRef = SystemReference
cmdMore.Visible = False
lstSelect.Visible = False
txtNewYNotes.Text = ""
txtNewYNotes.Visible = False
txtNewBNotes.Text = ""
txtNewBNotes.Visible = False
txtNewPNotes.Text = ""
txtNewPNotes.Visible = False
txtNewCNotes.Text = ""
txtNewCNotes.Visible = False
txtNewTNotes.Text = ""
txtNewTNotes.Visible = False
txtNewRNotes.Text = ""
txtNewRNotes.Visible = False
txtNewCRMNotes.Text = ""
txtNewCRMNotes.Visible = False
txtNewSNotes.Text = ""
txtNewSNotes.Visible = False
txtChartNotes.Text = ""
txtPrivateNotes.Text = ""
txtBillNotes.Text = ""
txtToDo.Text = ""
txtRxNotes.Text = ""
txtCRMNotes.Text = ""
txtSurgeryNotes.Text = ""
chkRxInclude.Visible = False
chkRxInclude.Value = 0
chkInclude.Visible = False
chkInclude.Value = 0
chkClaim.Visible = False
chkClaim.Value = 0
Set LocalDate = New ManagedDate
TodaysDate = ""
Call FormatTodaysDate(TodaysDate, False)
LocalDate.ExposedDate = TodaysDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    TodaysDate = LocalDate.ExposedDate
Else
    TodaysDate = ""
End If
If (PatientId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatientId
    If (RetPat.RetrievePatient) Then
        lblPatName.Caption = Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial) + " " _
                           + Trim(RetPat.LastName) + " " + Trim(RetPat.NameRef)
    End If
    Set RetPat = Nothing
Else
    Exit Function
End If
Call cmdGeneralNotes_Click
SetTo = "ILPN"
Call SetVisibleOptions
End Function

Public Function NotesforButton(AButton As fpBtn) As Boolean
Dim i As Integer
Dim LocalDate As ManagedDate
Dim RetrieveNotes As PatientNotes
NotesforButton = True
AButton.Text = "Notes"
Set LocalDate = New ManagedDate
TodaysDate = ""
Call FormatTodaysDate(TodaysDate, False)
LocalDate.ExposedDate = TodaysDate
If (LocalDate.ConvertDisplayDateToManagedDate) Then
    TodaysDate = LocalDate.ExposedDate
Else
    TodaysDate = ""
End If
Set RetrieveNotes = New PatientNotes
RetrieveNotes.NotesPatientId = PatientId
RetrieveNotes.NotesType = "C"
If (RetrieveNotes.FindNotesbyPatient > 0) Then
    i = 1
    While (RetrieveNotes.SelectNotes(i))
        LocalDate.ExposedDate = RetrieveNotes.NotesDate
        If (LocalDate.ConvertManagedDateToDisplayDate) Then
            ApptDate = LocalDate.ExposedDate
        Else
            ApptDate = ""
        End If
        If (InStrPS(AButton.Text, "*") = 0) Then
            AButton.Text = AButton.Text + " *"
        End If
        i = i + 1
    Wend
Else
    AButton.Text = "Notes"
End If
Set RetrieveNotes = Nothing


Set RetrieveNotes = New PatientNotes
RetrieveNotes.NotesPatientId = PatientId
RetrieveNotes.NotesType = "P"
If (RetrieveNotes.FindNotesbyPatient > 0) Then
    AButton.ThreeDHighlightColor = &HFF
    AButton.ThreeDShadowColor = &HFF
Else
    If AButton.Text = "Notes" Then
        AButton.ThreeDHighlightColor = &H79451E
        AButton.ThreeDShadowColor = &H462711
    Else
        AButton.ThreeDHighlightColor = &H462711
        AButton.ThreeDShadowColor = &H79451E
    End If
End If
Set RetrieveNotes = Nothing
Set LocalDate = Nothing
End Function

Private Sub Form_Activate()
If Not (TriggerOn) Then
    Exit Sub
End If
If (txtChartNotes.Visible) Then
    txtNewCNotes.Visible = True
    txtNewCNotes.SetFocus
    SendKeys "{Home}"
    If (InStrPS(SystemReference, "IMPR-") <> 0) And (InStrPS(SystemReference, "IMDN-") <> 0) Then
        Call cmdStandardNote_Click
    End If
ElseIf (txtPrivateNotes.Visible) Then
    txtNewPNotes.Visible = True
    txtNewPNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtCollNotes.Visible) Then
    txtNewYNotes.Visible = True
    txtNewYNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtRxNotes.Visible) Then
    txtNewRNotes.Visible = True
    txtNewRNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtCRMNotes.Visible) Then
    txtNewCRMNotes.Visible = True
    txtNewCRMNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtSurgeryNotes.Visible) Then
    txtNewSNotes.Visible = True
    txtNewSNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtToDo.Visible) Then
    txtNewTNotes.Visible = True
    txtNewTNotes.SetFocus
    SendKeys "{Home}"
ElseIf (txtBillNotes.Visible) Then
    txtNewBNotes.Visible = True
    txtNewBNotes.SetFocus
    SendKeys "{Home}"
End If
End Sub

Private Sub Form_Load()
TriggerOn = True
If (txtChartNotes.Visible) Then
    txtNewCNotes.SetFocus
ElseIf (txtPrivateNotes.Visible) Then
    txtNewPNotes.SetFocus
ElseIf (txtCollNotes.Visible) Then
    txtNewYNotes.SetFocus
ElseIf (txtBillNotes.Visible) Then
    txtNewBNotes.SetFocus
ElseIf (txtRxNotes.Visible) Then
    txtNewRNotes.SetFocus
ElseIf (txtCRMNotes.Visible) Then
    txtNewCRMNotes.SetFocus
ElseIf (txtSurgeryNotes.Visible) Then
    txtNewSNotes.SetFocus
ElseIf (txtToDo.Visible) Then
    txtNewTNotes.SetFocus
End If
End Sub

Private Sub lstSelect_Click()
Dim i As Integer
Dim NoteId As Long
Dim Rec As String
Dim Temp As String
Dim NoteFile As String
Dim RetrieveNotes As PatientNotes
If (NoteSelectCatOn) Then
    If (lstSelect.ListIndex > 0) Then
        NoteCategorySelected = lstSelect.List(lstSelect.ListIndex)
    End If
    lblCat.Caption = "Category:" + NoteCategorySelected
    lstSelect.Visible = False
    NoteSelectCatOn = False
ElseIf (NoteSelectAlertOn) Then
    If (lstSelect.ListIndex > 0) Then
        Temp = Trim(Left(lstSelect.List(lstSelect.ListIndex), 2))
        i = Val(Temp)
        If (i > 0) And (i < 33) Then
            Mid(NoteAlertSelected, i, 1) = "1"
            If (NoteAlertDisplay = "") Then
                NoteAlertDisplay = NoteAlertDisplay + Temp
            Else
                NoteAlertDisplay = NoteAlertDisplay + ", " + Temp
            End If
        End If
    End If
    lblAlert.Caption = "Alert:" + NoteAlertDisplay
    lstSelect.Visible = False
    NoteSelectAlertOn = False
ElseIf (txtPrivateNotes.Visible) Then
    If (lstSelect.ListIndex > 0) Then
        NoteFile = ImpressionNotesDirectory + Trim(lstSelect.List(lstSelect.ListIndex)) + ".txt"
        If Not (FM.IsFileThere(NoteFile)) Then
            txtNewPNotes.Text = Trim(lstSelect.List(lstSelect.ListIndex)) + vbCrLf + txtNewPNotes
        Else
            FM.OpenFile NoteFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(101)
            While Not (EOF(101))
                Line Input #101, Rec
                If (Trim(Rec) <> "") Then
                    txtNewPNotes.Text = Trim(Rec) + vbCrLf + txtNewPNotes.Text
                End If
            Wend
            FM.CloseFile CLng(101)
        End If
        txtNewPNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    ElseIf (lstSelect.ListIndex = 0) Then
        txtNewPNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    End If
ElseIf (txtChartNotes.Visible) Then
    ILPNRef = ""
    If (lstSelect.ListIndex > 0) Then
        If (InStrPS(UCase(lstSelect.List(lstSelect.ListIndex)), "AUDIO") <> 0) Then
            Set RetrieveNotes = New PatientNotes
            RetrieveNotes.NotesId = 0
            If (RetrieveNotes.RetrieveNotes) Then
                RetrieveNotes.NotesPatientId = PatientId
                RetrieveNotes.NotesAppointmentId = AppointmentId
                RetrieveNotes.NotesType = "C"
                RetrieveNotes.NotesText1 = Trim(lstSelect.List(lstSelect.ListIndex))
                RetrieveNotes.NotesText2 = ""
                RetrieveNotes.NotesText3 = ""
                RetrieveNotes.NotesText4 = ""
                RetrieveNotes.NotesUser = Trim(UserId)
                RetrieveNotes.NotesDate = TodaysDate
                RetrieveNotes.NotesSystem = Trim(LocalSysRef)
                RetrieveNotes.NotesEye = Trim(EyeContext)
                RetrieveNotes.NotesCategory = Trim(Left(NoteCategorySelected, 1))
                RetrieveNotes.NotesAlertMask = NoteAlertSelected
                RetrieveNotes.NotesCommentOn = False
                RetrieveNotes.NotesILPNRef = ""
                If (Left(SystemReference, 5) = "IMPR-") Then
                    RetrieveNotes.NotesILPNRef = ILPNRef
                ElseIf (Left(SystemReference, 5) = "IMDN-") Then
                    RetrieveNotes.NotesILPNRef = ILPNRef
                End If
                RetrieveNotes.NotesAudioOn = True
                Call RetrieveNotes.ApplyNotes
                NoteId = RetrieveNotes.NotesId
                Set RetrieveNotes = Nothing
                TriggerOn = False
                frmAudio.WavFileName = DocumentDirectory + "Audio-" + Trim(str(NoteId)) + ".wav"
                frmAudio.Show 1
                If Not (frmAudio.CaptureOn) Then
                    Set RetrieveNotes = New PatientNotes
                    RetrieveNotes.NotesId = NoteId
                    If (RetrieveNotes.RetrieveNotes) Then
                        RetrieveNotes.DeleteNotes
                    End If
                    Set RetrieveNotes = Nothing
                Else
                    Call LoadNotes
                End If
                TriggerOn = True
            End If
        Else
            If (Left(SystemReference, 5) = "IMPR-") Or (Left(SystemReference, 5) = "IMDN-") Then
                ILPNRef = lstSelect.List(lstSelect.ListIndex)
            End If
            NoteFile = ImpressionNotesDirectory + Trim(lstSelect.List(lstSelect.ListIndex)) + ".txt"
            If Not (FM.IsFileThere(NoteFile)) Then
                txtNewCNotes.Text = Trim(lstSelect.List(lstSelect.ListIndex)) + vbCrLf + txtNewCNotes.Text
            Else
                FM.OpenFile NoteFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(101)
                While Not (EOF(101))
                    Line Input #101, Rec
                    If (Trim(Rec) <> "") Then
                        txtNewCNotes.Text = Trim(Rec) + vbCrLf + txtNewCNotes.Text
                    End If
                Wend
                FM.CloseFile CLng(101)
            End If
            txtNewCNotes.SetFocus
            SendKeys "{End}"
        End If
        lstSelect.Visible = False
    ElseIf (lstSelect.ListIndex = 0) Then
        txtNewCNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    End If
ElseIf (txtRxNotes.Visible) Then
    If (lstSelect.ListIndex > 0) Then
        NoteFile = ImpressionNotesDirectory + Trim(lstSelect.List(lstSelect.ListIndex)) + ".txt"
        If Not (FM.IsFileThere(NoteFile)) Then
            txtNewRNotes.Text = Trim(lstSelect.List(lstSelect.ListIndex)) + vbCrLf + txtNewRNotes.Text
        Else
            FM.OpenFile NoteFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(101)
            While Not (EOF(101))
                Line Input #101, Rec
                If (Trim(Rec) <> "") Then
                    txtNewRNotes.Text = Trim(Rec) + vbCrLf + txtNewRNotes.Text
                End If
            Wend
            FM.CloseFile CLng(101)
        End If
        txtNewRNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    ElseIf (lstSelect.ListIndex = 0) Then
        txtNewRNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    End If
ElseIf (txtCRMNotes.Visible) Then
    If (lstSelect.ListIndex > 0) Then
        NoteFile = ImpressionNotesDirectory + Trim(lstSelect.List(lstSelect.ListIndex)) + ".txt"
        If Not (FM.IsFileThere(NoteFile)) Then
            txtNewCRMNotes.Text = Trim(lstSelect.List(lstSelect.ListIndex)) + vbCrLf + txtNewCRMNotes.Text
        Else
            FM.OpenFile NoteFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(101)
            While Not (EOF(101))
                Line Input #101, Rec
                If (Trim(Rec) <> "") Then
                    txtNewCRMNotes.Text = Trim(Rec) + vbCrLf + txtNewCRMNotes.Text
                End If
            Wend
            FM.CloseFile CLng(101)
        End If
        txtNewCRMNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    ElseIf (lstSelect.ListIndex = 0) Then
        txtNewCRMNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    End If
ElseIf (txtBillNotes.Visible) Then
    If (lstSelect.ListIndex > 0) Then
        NoteFile = ImpressionNotesDirectory + Trim(lstSelect.List(lstSelect.ListIndex)) + ".txt"
        If Not (FM.IsFileThere(NoteFile)) Then
            txtNewBNotes.Text = Trim(lstSelect.List(lstSelect.ListIndex)) + vbCrLf + txtNewBNotes.Text
        Else
            FM.OpenFile NoteFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(101)
            While Not (EOF(101))
                Line Input #101, Rec
                If (Trim(Rec) <> "") Then
                    txtNewBNotes.Text = Trim(Rec) + vbCrLf + txtNewBNotes.Text
                End If
            Wend
            FM.CloseFile CLng(101)
        End If
        txtNewBNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    ElseIf (lstSelect.ListIndex = 0) Then
        txtNewBNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    End If
ElseIf (txtSurgeryNotes.Visible) Then
    If (lstSelect.ListIndex > 0) Then
        NoteFile = ImpressionNotesDirectory + Trim(lstSelect.List(lstSelect.ListIndex)) + ".txt"
        If Not (FM.IsFileThere(NoteFile)) Then
            txtNewSNotes.Text = Trim(lstSelect.List(lstSelect.ListIndex)) + vbCrLf + txtNewSNotes.Text
        Else
            FM.OpenFile NoteFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(101)
            While Not (EOF(101))
                Line Input #101, Rec
                If (Trim(Rec) <> "") Then
                    txtNewSNotes.Text = Trim(Rec) + vbCrLf + txtNewSNotes.Text
                End If
            Wend
            FM.CloseFile CLng(101)
        End If
        txtNewSNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    ElseIf (lstSelect.ListIndex = 0) Then
        txtNewSNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    End If
ElseIf (txtCollNotes.Visible) Then
    If (lstSelect.ListIndex > 0) Then
        NoteFile = ImpressionNotesDirectory + Trim(lstSelect.List(lstSelect.ListIndex)) + ".txt"
        If Not (FM.IsFileThere(NoteFile)) Then
            txtNewYNotes.Text = Trim(lstSelect.List(lstSelect.ListIndex)) + vbCrLf + txtNewYNotes.Text
        Else
            FM.OpenFile NoteFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(101)
            While Not (EOF(101))
                Line Input #101, Rec
                If (Trim(Rec) <> "") Then
                    txtNewYNotes.Text = Trim(Rec) + vbCrLf + txtNewYNotes.Text
                End If
            Wend
            FM.CloseFile CLng(101)
        End If
        txtNewYNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    ElseIf (lstSelect.ListIndex = 0) Then
        txtNewYNotes.SetFocus
        SendKeys "{End}"
        lstSelect.Visible = False
    End If

End If
End Sub

Private Sub txtChartNotes_Click()
Call MaintainNotes(txtChartNotes)
End Sub

Private Sub txtChartNotes_KeyPress(KeyAscii As Integer)
If Not SetSearchControl(KeyAscii, txtChartNotes) Then
    Call txtChartNotes_Click
    KeyAscii = 0
End If
End Sub

Private Sub txtBillNotes_Click()
Call MaintainNotes(txtBillNotes)
End Sub

Private Sub txtBillNotes_KeyPress(KeyAscii As Integer)
If Not SetSearchControl(KeyAscii, txtBillNotes) Then
    Call txtBillNotes_Click
    KeyAscii = 0
End If
End Sub

Private Sub txtCollNotes_Click()
Call MaintainNotes(txtCollNotes)
End Sub

Private Sub txtCollNotes_KeyPress(KeyAscii As Integer)
If Not SetSearchControl(KeyAscii, txtCollNotes) Then
    Call txtCollNotes_Click
    KeyAscii = 0
End If
End Sub

Private Sub txtMoreView_KeyPress(KeyAscii As Integer)
If (UCase(Chr(KeyAscii)) = "E") Or (UCase(Chr(KeyAscii)) = "D") Then
    Call MaintainNotes(txtMoreView)
    KeyAscii = 0
Else
    Call SetSearchControl(KeyAscii, txtMoreView)
End If
End Sub

Private Sub txtNewBNotes_KeyPress(KeyAscii As Integer)
Call SetSearchControl(KeyAscii, txtNewBNotes)
End Sub

Private Sub txtNewCNotes_KeyPress(KeyAscii As Integer)
Call SetSearchControl(KeyAscii, txtNewCNotes)
End Sub

Private Sub txtNewCRMNotes_KeyPress(KeyAscii As Integer)
Call SetSearchControl(KeyAscii, txtNewCRMNotes)
End Sub

Private Sub txtNewPNotes_KeyPress(KeyAscii As Integer)
Call SetSearchControl(KeyAscii, txtNewPNotes)
End Sub

Private Sub txtNewRNotes_KeyPress(KeyAscii As Integer)
Call SetSearchControl(KeyAscii, txtNewRNotes)
End Sub

Private Sub txtNewSNotes_KeyPress(KeyAscii As Integer)
Call SetSearchControl(KeyAscii, txtNewSNotes)
End Sub

Private Sub txtNewTNotes_KeyPress(KeyAscii As Integer)
Call SetSearchControl(KeyAscii, txtNewTNotes)
End Sub

Private Sub txtNewYNotes_KeyPress(KeyAscii As Integer)
Call SetSearchControl(KeyAscii, txtNewYNotes)
End Sub

Private Sub txtPrivateNotes_Click()
Call MaintainNotes(txtPrivateNotes)
End Sub

Private Sub txtPrivateNotes_KeyPress(KeyAscii As Integer)
If Not SetSearchControl(KeyAscii, txtPrivateNotes) Then
    Call txtPrivateNotes_Click
    KeyAscii = 0
End If
End Sub

Private Sub txtSearch_Change()
If Trim$(CurrentTextBox.Text) <> "" Then
    Call SearchText(CurrentTextBox.Text, 1)
End If
End Sub
'
Private Sub txtSearch_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
    Case 27
        Call HideSearchControl
        KeyAscii = 0
End Select
End Sub
Private Sub txtSurgeryNotes_Click()
Call MaintainNotes(txtSurgeryNotes)
End Sub

Private Sub txtSurgeryNotes_KeyPress(KeyAscii As Integer)
If Not SetSearchControl(KeyAscii, txtSurgeryNotes) Then
    Call txtSurgeryNotes_Click
    KeyAscii = 0
End If
End Sub

Private Sub txtToDo_Click()
Call MaintainNotes(txtToDo)
End Sub

Private Sub txtToDo_KeyPress(KeyAscii As Integer)
If Not SetSearchControl(KeyAscii, txtToDo) Then
    Call txtToDo_Click
    KeyAscii = 0
End If
End Sub

Private Sub txtRxNotes_Click()
Call MaintainNotes(txtRxNotes)
End Sub

Private Sub txtRxNotes_KeyPress(KeyAscii As Integer)
If Not SetSearchControl(KeyAscii, txtRxNotes) Then
    Call txtRxNotes_Click
    KeyAscii = 0
End If
End Sub

Private Sub txtCRMNotes_Click()
Call MaintainNotes(txtCRMNotes)
End Sub

Private Sub txtCRMNotes_KeyPress(KeyAscii As Integer)
If Not SetSearchControl(KeyAscii, txtCRMNotes) Then
    Call txtCRMNotes_Click
    KeyAscii = 0
End If
End Sub

Private Function SetSearchControl(ByVal KeyAscii As Integer, ByRef TextBox As TextBox) As Boolean
SetSearchControl = False
If KeyAscii = 6 Then
    Call ShowSearchControl(TextBox)
    SetSearchControl = True
ElseIf KeyAscii = 27 Then
    Call HideSearchControl
    SetSearchControl = True
End If
End Function
Private Sub MaintainNotes(ATextBox As TextBox)
Dim ANoteId As Long
Dim AUser As String
Dim AudioOn As Boolean
Dim RetiredOn As Boolean
Dim i As Long, k As Long
ANoteId = 0
MaintainOn = True
If (MaintainOn) Then
    i = 0
    If Not UserLogin.HasPermission(epBillingNotes) And SetTo = "B" Then
        frmEventMsgs.InfoMessage "Not Permissioned to Maintain Billing Notes."
        frmEventMsgs.Show 1
        Exit Sub
    End If
    For k = ATextBox.SelStart To 1 Step -1
        If (Mid(ATextBox.Text, k, 1) = "[") Then
            i = k
            k = InStrPS(i, ATextBox.Text, "]")
            If (k <= i) Then
                k = i + 5
            End If
            ANoteId = Val(Mid(ATextBox.Text, i + 1, (k - 1) - i))
            Exit For
        End If
    Next k
    If (ANoteId > 0) Then
        If (IsNoteEditable(ANoteId, AudioOn, RetiredOn, UserId, AUser, SetTo)) Then
            TriggerOn = True
            frmEventMsgs.Header = "Maintain Note"
            frmEventMsgs.AcceptText = "Edit"
            frmEventMsgs.RejectText = "Delete"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = "Retire"
            If (RetiredOn) Then
                frmEventMsgs.Other0Text = "Activate"
            End If
            frmEventMsgs.Other1Text = ""
            If (AudioOn) Then
                frmEventMsgs.Other1Text = "Playback"
            End If
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            TriggerOn = False
            If (frmEventMsgs.Result = 1) Then
                NoteId = ANoteId
                Call LoadNotes
                NoteOn = True
                If (txtMoreView.Visible) Then
                    txtMoreView.Visible = False
                    cmdMore.Text = "More Notes"
                End If
                lblRetired.Visible = IsNoteRetired(NoteId)
            ElseIf (frmEventMsgs.Result = 2) Then
                If (SetTo = "P") Then
                    frmEventMsgs.Header = "Action ?"
                    frmEventMsgs.AcceptText = "Make Chart Note"
                    frmEventMsgs.RejectText = "Delete"
                    frmEventMsgs.CancelText = "Cancel"
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    If (frmEventMsgs.Result = 1) Then
                        Call ChangeNoteType(ANoteId)
                        SetTo = "C"
                    ElseIf (frmEventMsgs.Result = 2) Then
                        Call DeleteNote(ANoteId)
                        SetTo = ""
                    ElseIf (frmEventMsgs.Result = 4) Then
                        Exit Sub
                    End If
                Else
                    Call DeleteNote(ANoteId)
                End If
                Call LoadNotes
                NoteOn = True
                If (txtMoreView.Visible) Then
                    txtMoreView.Visible = False
                    cmdMore.Text = "More Notes"
                End If
            ElseIf (frmEventMsgs.Result = 3) Then
                If (frmEventMsgs.Other0Text = "Activate") Then
                    Call RetireNote(ANoteId, False)
                Else
                    Call RetireNote(ANoteId, True)
                End If
                Call LoadNotes
                NoteOn = True
                If (txtMoreView.Visible) Then
                    txtMoreView.Visible = False
                    cmdMore.Text = "More Notes"
                End If
            ElseIf (frmEventMsgs.Result = 5) Then
                frmAudio.WavFileName = DocumentDirectory + "Audio-" + Trim(str(ANoteId)) + ".wav"
                frmAudio.CaptureOn = False
                frmAudio.Show 1
            End If
        Else
            frmEventMsgs.Header = "You are not " + Trim(AUser) + " maintaining this note is denied"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
    End If
Else
    frmEventMsgs.Header = "User is not permissioned to maintain notes."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub DeleteNote(NoteId As Long)
Dim RetNote As PatientNotes
If (NoteId > 0) Then
    If (FM.IsFileThere(DocumentDirectory + "Audio-" + Trim(str(NoteId)) + ".wav")) Then
        FM.Kill DocumentDirectory + "Audio-" + Trim(str(NoteId)) + ".wav"
    End If
    Set RetNote = New PatientNotes
    RetNote.NotesId = NoteId
    If (RetNote.RetrieveNotes) Then
        Call RetNote.DeleteNotes
    End If
    Set RetNote = Nothing
End If
End Sub

Private Sub ChangeNoteType(NoteId As Long)
Dim RetNote As PatientNotes
If (NoteId > 0) Then
    Set RetNote = New PatientNotes
    RetNote.NotesId = NoteId
    If (RetNote.RetrieveNotes) Then
        RetNote.NotesType = "C"
        Call RetNote.ApplyNotes
    End If
    Set RetNote = Nothing
End If
End Sub

Private Sub SetVisibleOptions()
If (SetTo = "T") Or (SetTo = "ILPN") Or (SetTo = "IMDN") Then
    cmdCollNotes.Visible = False
    cmdBillNotes.Visible = False
    cmdGeneralNotes.Visible = False
    cmdPrivateNotes.Visible = False
    cmdRxNotes.Visible = False
    cmdCRMNotes.Visible = False
    cmdStdCRMNote.Visible = False
    cmdFreehand.Visible = False
    cmdScan.Visible = False
    cmdMessages.Visible = False
    cmdToDo.Visible = False
    cmdSurgeryNotes.Visible = False
    cmdCat.Visible = False
    lblCat.Visible = False
    cmdAlert.Visible = False
    lblAlert.Visible = False
Else
    cmdCollNotes.Visible = True
    cmdBillNotes.Visible = True
    cmdGeneralNotes.Visible = True
    cmdPrivateNotes.Visible = True
    cmdRxNotes.Visible = True
    cmdCRMNotes.Visible = True
    cmdFreehand.Visible = False
    cmdScan.Visible = True
    'cmdMessages.Visible = True
    cmdSurgeryNotes.Visible = True
    cmdToDo.Visible = False
    cmdCat.Visible = False
    lblCat.Visible = False
    If (SetTo = "C") Then
        cmdCat.Visible = True
        lblCat.Visible = True
    End If
    cmdAlert.Visible = True
    lblAlert.Visible = True
End If
lblCatReminder.Visible = False
Call HideSearchControl
If (SystemReference = CategorizationTrigger) Then
    lblCatReminder.Visible = True
End If
If Not (MaintainAlertOn) Then
    cmdAlert.Visible = False
    lblAlert.Visible = False
End If
End Sub

Private Function IsNotesDisplayFull(TheText As String) As Boolean
Dim p As Long, j As Long, k As Long
IsNotesDisplayFull = False
k = 0
j = 1
p = InStrPS(1, TheText, vbCrLf)
While (p > 0)
    k = k + 1
    p = InStrPS(j, TheText, vbCrLf)
    j = p + 2
Wend
If (k > LineMax) Then
    IsNotesDisplayFull = True
End If
End Function

Private Function IsNoteEditable(ANote As Long, AudioOn As Boolean, RetiredOn As Boolean, TheUser As String, NoteUser As String, ASet As String) As Boolean
Dim RetrieveNotes As PatientNotes
IsNoteEditable = False
AudioOn = False
RetiredOn = False
If (ANote > 0) Then
    Set RetrieveNotes = New PatientNotes
    RetrieveNotes.NotesId = ANote
    If (RetrieveNotes.RetrieveNotes) Then
        AudioOn = RetrieveNotes.NotesAudioOn
        If (Trim(RetrieveNotes.NotesOffDate) <> "") Then
            RetiredOn = True
        End If
        NoteUser = Trim(RetrieveNotes.NotesUser)
        If (NoteUser = TheUser) Then
            IsNoteEditable = True
        ElseIf (NoteUser = "") Then
            IsNoteEditable = True
        End If
        IsNoteEditable = True
    End If
    Set RetrieveNotes = Nothing
End If
End Function

Private Function IsNoteRetired(ANote As Long) As Boolean
Dim RetrieveNotes As PatientNotes
IsNoteRetired = False
If (ANote > 0) Then
    Set RetrieveNotes = New PatientNotes
    RetrieveNotes.NotesId = ANote
    If (RetrieveNotes.RetrieveNotes) Then
        If (Trim(RetrieveNotes.NotesOffDate) <> "") Then
            IsNoteRetired = True
        End If
    End If
    Set RetrieveNotes = Nothing
End If
End Function

Private Sub RetireNote(NoteId As Long, AType As Boolean)
Dim ADate As String
Dim RetNote As PatientNotes
If (NoteId > 0) Then
    Set RetNote = New PatientNotes
    RetNote.NotesId = NoteId
    If (RetNote.RetrieveNotes) Then
        If (AType) Then
            Call FormatTodaysDate(ADate, False)
            ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
            RetNote.NotesOffDate = ADate
        Else
            RetNote.NotesOffDate = ""
        End If
        Call RetNote.ApplyNotes
    End If
    Set RetNote = Nothing
End If
End Sub

Public Function PurgeNotes(PatId As Long, ApptId As Long, AType As String, SRef As String) As Boolean
Dim i As Integer
Dim RetNote As PatientNotes
PurgeNotes = False
If (PatId > 0) And (ApptId > 0) And (Trim(AType) <> "") And (Trim(SRef) <> "") Then
    Set RetNote = New PatientNotes
    RetNote.NotesAppointmentId = ApptId
    RetNote.NotesPatientId = PatId
    RetNote.NotesType = AType
    RetNote.NotesSystem = SRef
    If (RetNote.FindNotes > 0) Then
        i = 1
        While (RetNote.SelectNotes(i))
            Call DeleteNote(RetNote.NotesId)
            i = i + 1
        Wend
    End If
    Set RetNote = Nothing
End If
End Function

Public Function GetGlobalNote(ADate As String, ANote As String) As Long
Dim RetNote As PatientNotes
GetGlobalNote = 0
If (Trim(ADate) <> "") Then
    Set RetNote = New PatientNotes
    RetNote.NotesAppointmentId = -1
    RetNote.NotesPatientId = -1
    RetNote.NotesType = "/"
    RetNote.NotesDate = ADate
    If (RetNote.FindNotes > 0) Then
        If (RetNote.SelectNotes(1)) Then
            ANote = Trim(RetNote.NotesText1)
            GetGlobalNote = RetNote.NotesId
        End If
    End If
    Set RetNote = Nothing
End If
End Function

Public Function SetGlobalNote(ANoteId As Long, ADate As String, ANote As String) As Boolean
Dim RetNote As PatientNotes
SetGlobalNote = False
If (Trim(ADate) <> "") Then
    If (ANoteId > 0) Then
        Set RetNote = New PatientNotes
        RetNote.NotesId = ANoteId
        If (RetNote.RetrieveNotes) Then
            If (InStrPS(Trim(UCase(RetNote.NotesText1)), Trim(UCase(ANote))) = 0) Or (Trim(ANote) = "") Or (Len(ANote) < Len(Trim(RetNote.NotesText1))) Then
                If (Trim(ANote) = "") Then
                    Call RetNote.DeleteNotes
                Else
                    RetNote.NotesUser = Trim(UserLogin.iId)
                    RetNote.NotesText1 = Trim(ANote)
                    Call RetNote.ApplyNotes
                End If
            End If
        End If
        Set RetNote = Nothing
    Else
        If (Trim(ANote) <> "") Then
            Set RetNote = New PatientNotes
            RetNote.NotesId = 0
            If (RetNote.RetrieveNotes) Then
                RetNote.NotesPatientId = -1
                RetNote.NotesAppointmentId = -1
                RetNote.NotesDate = ADate
                RetNote.NotesType = "/"
                RetNote.NotesUser = Trim(UserLogin.iId)
                RetNote.NotesSystem = ""
                RetNote.NotesEye = ""
                RetNote.NotesCategory = ""
                RetNote.NotesAlertMask = ""
                RetNote.NotesCommentOn = False
                RetNote.NotesClaimOn = False
                RetNote.NotesILPNRef = ""
                RetNote.NotesHighlight = ""
                RetNote.NotesText1 = Trim(ANote)
                RetNote.NotesText2 = ""
                RetNote.NotesText3 = ""
                RetNote.NotesText4 = ""
                Call RetNote.ApplyNotes
            End If
            Set RetNote = Nothing
        End If
    End If
End If
End Function
Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
End Function

Private Sub ShowSearchControl(txtBox As TextBox)
lstSelect.Visible = False
FrameSearch.Visible = True
txtSearch.Text = ""
Set CurrentTextBox = txtBox
txtSearch.SetFocus
End Sub
Private Sub HideSearchControl()
If Not (CurrentTextBox Is Nothing) Then
    CurrentTextBox.SelLength = 0
    CurrentTextBox.SelText = vbNullString
    CurrentTextBox.SelStart = 0
    If CurrentTextBox.Visible Then CurrentTextBox.SetFocus
End If
FrameSearch.Visible = False
txtSearch.Text = vbNullString
End Sub
Public Function SearchText(ByVal textToSearch As String, ByVal startPosition As Integer, Optional ByVal IsBackWard As Boolean = False)
Dim i As Integer
Dim Temp As String
Dim textToFind As String

If Trim$(txtSearch.Text) = vbNullString Then Exit Function

If startPosition < 1 Then
    startPosition = startPosition + 1
End If

textToFind = LCase(txtSearch.Text)

textToSearch = LCase(textToSearch)

If IsBackWard Then
    i = InStrRev(textToSearch, textToFind)
Else
    i = InStrPS(startPosition, textToSearch, textToFind, vbTextCompare)
End If

If i > 0 Then
    CurrentTextBox.SetFocus
    CurrentTextBox.SelStart = i - 1
    CurrentTextBox.SelLength = Len(textToFind)
ElseIf Len(textToFind) <> CurrentTextBox.SelLength Then
    CurrentTextBox.SelStart = 0
    CurrentTextBox.SelLength = 0
End If
txtSearch.SetFocus
End Function
