VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmRxManage 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00B60000&
   LinkTopic       =   "Form1"
   Picture         =   "RxManage.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin fpBtnAtlLibCtl.fpBtn fpE 
      Height          =   990
      Left            =   3120
      TabIndex        =   22
      Top             =   7800
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":36C9
   End
   Begin VB.ListBox lstRxs 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   0
      TabIndex        =   12
      Top             =   4200
      Visible         =   0   'False
      Width           =   12015
   End
   Begin VB.TextBox txtZoom 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5415
      Left            =   1440
      MultiLine       =   -1  'True
      TabIndex        =   16
      Top             =   1080
      Visible         =   0   'False
      Width           =   6735
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDrug 
      Height          =   2055
      Index           =   5
      Left            =   4320
      TabIndex        =   29
      Top             =   4680
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   3625
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":38AA
      Begin fpBtnAtlLibCtl.fpBtn cmdDN 
         Height          =   480
         Index           =   5
         Left            =   3360
         TabIndex        =   35
         Top             =   1560
         Visible         =   0   'False
         Width           =   495
         _Version        =   131072
         _ExtentX        =   873
         _ExtentY        =   847
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "RxManage.frx":3A89
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDrug 
      Height          =   2055
      Index           =   4
      Left            =   4320
      TabIndex        =   28
      Top             =   2520
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   3625
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":4D53
      Begin fpBtnAtlLibCtl.fpBtn cmdDN 
         Height          =   480
         Index           =   4
         Left            =   3360
         TabIndex        =   34
         Top             =   1560
         Visible         =   0   'False
         Width           =   495
         _Version        =   131072
         _ExtentX        =   873
         _ExtentY        =   847
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "RxManage.frx":4F32
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDrug 
      Height          =   2055
      Index           =   3
      Left            =   4320
      TabIndex        =   27
      Top             =   360
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   3625
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":61FC
      Begin fpBtnAtlLibCtl.fpBtn cmdDN 
         Height          =   480
         Index           =   3
         Left            =   3360
         TabIndex        =   33
         Top             =   1560
         Visible         =   0   'False
         Width           =   495
         _Version        =   131072
         _ExtentX        =   873
         _ExtentY        =   847
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "RxManage.frx":63DB
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDrug 
      Height          =   2055
      Index           =   2
      Left            =   240
      TabIndex        =   26
      Top             =   4680
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   3625
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":76A5
      Begin fpBtnAtlLibCtl.fpBtn cmdDN 
         Height          =   480
         Index           =   2
         Left            =   3360
         TabIndex        =   32
         Top             =   1560
         Visible         =   0   'False
         Width           =   495
         _Version        =   131072
         _ExtentX        =   873
         _ExtentY        =   847
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "RxManage.frx":7884
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDrug 
      Height          =   2055
      Index           =   1
      Left            =   240
      TabIndex        =   25
      Top             =   2520
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   3625
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":8B4E
      Begin fpBtnAtlLibCtl.fpBtn cmdDN 
         Height          =   480
         Index           =   1
         Left            =   3360
         TabIndex        =   31
         Top             =   1560
         Visible         =   0   'False
         Width           =   495
         _Version        =   131072
         _ExtentX        =   873
         _ExtentY        =   847
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "RxManage.frx":8D2D
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDrug 
      Height          =   2055
      Index           =   0
      Left            =   240
      TabIndex        =   24
      Top             =   360
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   3625
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":9FF7
      Begin fpBtnAtlLibCtl.fpBtn cmdDN 
         Height          =   480
         Index           =   0
         Left            =   3360
         TabIndex        =   30
         Top             =   1560
         Width           =   495
         _Version        =   131072
         _ExtentX        =   873
         _ExtentY        =   847
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   12632256
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "RxManage.frx":A1D6
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCancel 
      Height          =   990
      Left            =   120
      TabIndex        =   23
      Top             =   7800
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":B4A0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnify 
      Height          =   480
      Left            =   7800
      TabIndex        =   17
      Top             =   6840
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":B67D
   End
   Begin VB.ListBox lstBox 
      Height          =   840
      Left            =   6360
      Sorted          =   -1  'True
      TabIndex        =   14
      Top             =   1080
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox txtNotes 
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   855
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   13
      Top             =   6840
      Width           =   8055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAnother 
      Height          =   990
      Left            =   1560
      TabIndex        =   3
      Top             =   7800
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":C947
   End
   Begin VB.ListBox lstAllergy 
      BackColor       =   &H000000C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   3900
      ItemData        =   "RxManage.frx":CB26
      Left            =   8280
      List            =   "RxManage.frx":CB28
      TabIndex        =   8
      Top             =   3840
      Width           =   3615
   End
   Begin VB.ListBox lstNonOcular 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3420
      ItemData        =   "RxManage.frx":CB2A
      Left            =   8280
      List            =   "RxManage.frx":CB2C
      TabIndex        =   7
      Top             =   360
      Width           =   3615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   990
      Left            =   4560
      TabIndex        =   1
      Top             =   7800
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   128
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":CB2E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10560
      TabIndex        =   2
      Top             =   7800
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":CD09
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   990
      Left            =   7440
      TabIndex        =   4
      Top             =   7800
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":CEE4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   990
      Left            =   9000
      TabIndex        =   5
      Top             =   7800
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":D0C8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHx 
      Height          =   990
      Left            =   6000
      TabIndex        =   6
      Top             =   7800
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":D2A4
   End
   Begin VB.ListBox lstHx 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   7260
      Left            =   240
      TabIndex        =   18
      Top             =   360
      Visible         =   0   'False
      Width           =   11655
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpr 
      Height          =   990
      Left            =   8760
      TabIndex        =   19
      Top             =   7800
      Visible         =   0   'False
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "RxManage.frx":D482
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H006C6928&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   900
      Left            =   8280
      TabIndex        =   20
      Top             =   6840
      Width           =   3615
      Begin VB.Label Label43 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00C19B49&
         BackStyle       =   0  'Transparent
         Caption         =   "Pharmacy"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   0
         TabIndex        =   21
         Top             =   0
         Width           =   720
      End
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Drug Management"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   240
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   2100
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Printing Rxs"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   4800
      TabIndex        =   15
      Top             =   0
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.Label lblPatient 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   3360
      TabIndex        =   36
      Top             =   0
      Width           =   60
   End
   Begin VB.Label lblHx 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Drug History"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   240
      TabIndex        =   11
      Top             =   0
      Visible         =   0   'False
      Width           =   1560
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Allergies"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   8280
      TabIndex        =   10
      Top             =   3480
      Width           =   1020
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Non-ocular meds"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   8280
      TabIndex        =   9
      Top             =   120
      Width           =   1965
   End
End
Attribute VB_Name = "frmRxManage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public CurrentAction As String
Public PatientId As Long
Public ActivityId As Long
Public AppointmentId As Long
Public EditPreviousOn As Boolean
Public PrescribeOn As Boolean
Public lstExternal As ListBox
Public ApptDate As String
Public LatestAppointmentOn As Boolean

Private Const NewOn As Boolean = True
Private ActiveOnly As Boolean
Private CurrentIndex As Integer

Private Const MAX_DRUG_BUTTONS As Long = 6

Dim DrugNote1() As String
Dim DrugNote2() As String

Private moDrugRx As CDrugRx
Private ColDrugRx As New Collection
Private MedDates(20) As String

Private Sub cmdAnother_Click()
Dim z As Integer
Dim Rx As String
Dim RxId As String
Dim TheAction As String
frmAssignDrugs.Diagnosis = ""
frmAssignDrugs.PatientId = PatientId
Dim pat As New Patient
frmAssignDrugs.lblPatient = pat.GetPatientInfo(PatientId)
Set pat = Nothing
frmAssignDrugs.AppointmentId = AppointmentId
frmAssignDrugs.LastOn = True
frmAssignDrugs.FavoriteOn = False
If (Left(cmdAnother.Text, 5) = "Write") Then
    If Not UserLogin.HasPermission(epWritePrescriptions) Then
            frmEventMsgs.Header = "Not Permissioned for Write Prescriptions"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
    End If
    frmAssignDrugs.LastOn = False
    frmAssignDrugs.FavoriteOn = True
    frmRxDetails.WriteRx = True
Else
    frmRxDetails.WriteRx = False
End If
frmAssignDrugs.frmRxManage_Write_RX = (frmRxManage.cmdAnother.Text = "Write RX")

If (frmAssignDrugs.LoadAvailableRx(True)) Then
If (Left(cmdAnother.Text, 5) = "Write") Then

        Dim objPracticeCode As PracticeCodes
        Dim sqlConString As String
        Dim PracticeToken As String
        sqlConString = ConnectionString
        Set objPracticeCode = New PracticeCodes
        PracticeToken = objPracticeCode.GetPracticeToken("PracticeAuthToken")
        If (CStr(PatientId) <> "") Then
        Dim frmErx As New ClinicalIntegration.frmErxHelper
        frmErx.PatientId = CStr(PatientId)
        frmErx.PracticeToken = PracticeToken
        'frmErx.sqlConString = ""
        frmErx.UserId = CStr(UserLogin.iId)
        frmErx.AppointmentId = AppointmentId
        frmErx.DbObject = IdbConnection
        frmErx.ShowDialog
        frmErx.Close
        frmErx.Dispose
        End If
    Else
    
        frmAssignDrugs.Show 1

    For z = 1 To frmAssignDrugs.TotalRx
        'If (frmAssignDrugs.getRx(z, Rx, RxDiag, RxDose, RxFreq, RxRefill, RxDuration, RxNote, RxId)) Then
        '    TheAction = Rx + "-2/" + RxDose + "-3/" + RxFreq + "-4/" + RxRefill + "-5/" + RxDuration + "-6/"
        If frmAssignDrugs.getRx_6(z, Rx, "record2") Then
            TheAction = Rx
        'End If
            If (PrescribeOn) Then
                TheAction = TheAction & "P-7/"
            End If
            setOutputStat TheAction

            If (EditPreviousOn) Then
                Call PostToDB(0, "", "", TheAction, RxId)
            Else
                Call frmEvaluation.AddAnAction(TheAction, "8", "Rx")
            End If

            Call LoadRxs(True)
        End If
    Next z
    End If
End If
End Sub

Private Sub cmdDN_Click(Index As Integer)
Dim i As Integer
i = Index + cmdDrug(0).ToolTipText
txtZoom.Text = cmdDrug(Index).Text & vbCrLf & _
              "As prescribed: " & DrugNote1(i + 1) & vbCrLf & _
              "Pharm. Notes : " & DrugNote2(i + 1)
txtZoom.Visible = True
End Sub

Private Sub cmdDrug_Click(Index As Integer)
    TriggerRx Index
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Function newPrescr() As Boolean
Dim i As Integer
For i = 0 To lstRxs.ListCount - 1
    If frmRxManage.GetOutputStat(lstRxs.List(i)) > 1 Then
        newPrescr = True
        Exit For
    End If
Next
End Function


Private Sub cmdDone_Click()
If PrescribeOn Then
    If newPrescr Then
        If Not eRxValid Then
            Exit Sub
        End If
    End If
End If

Dim i As Integer

If (lstHx.Visible) Then
    lblHx.Visible = False
    lstHx.Visible = False
    cmdImpr.Visible = False
    cmdAnother.Visible = True
    cmdNotes.Visible = True
    cmdPrint.Visible = True
    cmdHx.Visible = True
    txtNotes.Visible = True
    If (Trim(txtNotes.Text) <> "") Then
        cmdMagnify.Visible = True
    End If
    For i = 0 To MAX_DRUG_BUTTONS - 1
        cmdDrug(i).Visible = cmdDrug(i).Text <> ""
    Next i
    If (lstRxs.ListCount - 1 > 5) Then
        cmdMore.Visible = True
    Else
        cmdMore.Visible = False
    End If
    lstNonOcular.Visible = True
    lstAllergy.Visible = True
    Label1.Visible = True
    Label2.Visible = True
    Label4.Visible = True
Else
    CurrentAction = ""
    Unload frmRxManage
End If
End Sub


Private Function eRxValid() As Boolean
Dim RetRes  As SchedulerResource
Dim feRxValid As Boolean
Dim i As Integer
Dim EnteredCode As String
Dim ResourceName As String
Dim ResourceERxCode As String

feRxValid = True

For i = 0 To lstRxs.ListCount - 1
    If GetOutputStat(lstRxs.List(i)) > 1 Then
        feRxValid = False
        Exit For
    End If
Next

If Not feRxValid Then
    Dim RetCon As New PracticeConfigureInterface
    If Not RetCon.getFieldValue("ERXVALID") = "T" Then
        feRxValid = True
    End If
    Set RetCon = Nothing
End If

CheckResource:
If Not feRxValid Then
    frmCardReader.txt = ""
    frmCardReader.Show 1
    
    EnteredCode = UCase(Trim(frmCardReader.Entered))
    Set RetRes = New SchedulerResource
    RetRes.ResourceId = UserLogin.iId
    If (RetRes.RetrieveSchedulerResource) Then
        ResourceName = RetRes.ResourceName
        ResourceERxCode = UCase(Trim(RetRes.ResourceStartTime7))
    End If
    Set RetRes = Nothing

    Select Case EnteredCode
    Case ""
        'MsgBox "error reading"
    Case "CANCEL"
        Exit Function
    Case Else
        'for show only!!!!!!!!!!!
        If Mid(EnteredCode, 1, 8) = ResourceERxCode Then
        'If EnteredCode= ResourceERxCode Then
            feRxValid = True
        End If
        Set RetRes = Nothing
    End Select
End If

If Not feRxValid Then
    frmEventMsgs.Header = "Validation ID Incorrect"
    frmEventMsgs.AcceptText = "Try Again"
    frmEventMsgs.RejectText = "" '"Print Only"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ResourceName
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Select Case frmEventMsgs.Result
    Case 1
        GoTo CheckResource
    Case 2
        Call ChangeOutputStat("9", "8")
        feRxValid = True
    Case 3
        Set FSignIn.FormLogin = UserLogin
        FSignIn.Show 1
    End Select
End If

eRxValid = feRxValid
End Function



Private Sub cmdHx_Click()
If (NewOn) Then
    frmRxHistory.CurrentAction = CurrentAction
    frmRxHistory.EditPrevOn = frmSystems1.EditPreviousOn
    frmRxHistory.ActivityId = ActivityId
    If (frmRxHistory.LoadHistoryRx(PatientId, False)) Then
        frmRxHistory.Show 1
    End If
Else
    Call LoadPreviousRx(PatientId, lstHx, True)
End If
End Sub

Private Sub cmdImpr_Click()
Dim LDate As String
Dim ApptId As Long
Dim PatId As Long
Dim RetActivity As PracticeActivity
Dim RetrievalActivity As PracticeActivity
PatId = PatientId
ApptId = 0
Set RetActivity = New PracticeActivity
RetActivity.ActivityId = ActivityId
If (RetActivity.RetrieveActivity) Then
    LDate = RetActivity.ActivityDate
    Set RetrievalActivity = New PracticeActivity
    RetrievalActivity.ActivityDate = LDate
    RetrievalActivity.ActivityPatientId = PatId
    RetrievalActivity.ActivityStatus = "DH"
    If (RetrievalActivity.RetrievePreviousActivity) Then
        ApptId = RetrievalActivity.ActivityAppointmentId
        ActivityId = RetrievalActivity.ActivityId
    End If
    Set RetrievalActivity = Nothing
End If
Set RetActivity = Nothing
If (ApptId > 0) And (PatId > 0) And (ActivityId > 0) Then
    frmImpression.EditPreviousOn = False
    frmImpression.ActivityId = ActivityId
    frmImpression.PatientId = PatId
    frmImpression.AppointmentId = ApptId
    If (frmImpression.LoadImpression(True)) Then
        frmImpression.Show 1
    Else
        frmEventMsgs.Header = "No Impressions/Plans"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
End Sub

Private Sub cmdMore_Click()
CurrentIndex = CurrentIndex + 1
If (CurrentIndex > lstRxs.ListCount) Then
    CurrentIndex = 1
End If
Call LoadRxButtons
End Sub

Private Sub cmdNotes_Click()
Dim ANote As String
frmNotes.NoteId = 0
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.SystemReference = ""
frmNotes.SetTo = "R"
frmNotes.EyeContext = ""
frmNotes.NoteOn = False
frmNotes.MaintainOn = True
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
    txtNotes.BackColor = lstNonOcular.BackColor
    Call GetMedsNote(PatientId, AppointmentId, "", ANote, "")
    cmdMagnify.Visible = False
    If (Trim(ANote) <> "") Then
        txtNotes.Text = Trim(ANote)
        If (Len(ANote) > 0) Then
            txtNotes.BackColor = lstAllergy.BackColor
            cmdMagnify.Visible = True
        End If
    End If
End If
End Sub

Private Sub cmdPrint_Click()


If PrescribeOn Then
    If newPrescr Then
        If Not eRxValid Then
            Exit Sub
        End If
    End If
End If

Dim i As Integer
Dim txt As String
Dim s As Integer
Dim nxt As Integer
Dim sTag(5) As String
frmEventMsgs.Header = "Print Rxs."
If frmSystems1.EditPreviousOn Then
    frmEventMsgs.AcceptText = ""
Else
    frmEventMsgs.AcceptText = "Print New"
End If
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.RejectText = ""
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""

nxt = -1
If cmdDrug(0).Visible Then
    s = cmdDrug(0).ToolTipText
    For i = s To s + 5
        txt = ""
        If i < lstRxs.ListCount Then
            If frmSystems1.EditPreviousOn Then
                If cmdDrug(i - s).FontBold Then
                    txt = lstRxs.List(i)
                    nxt = nxt + 1
                End If
            Else
                If GetOutputStat(lstRxs.List(i)) > 0 Then
                    txt = lstRxs.List(i)
                    nxt = nxt + 1
                End If
            End If
        End If
        If Not txt = "" Then
            sTag(nxt) = cmdDrug(i - s).Tag
            Select Case nxt
            Case 0
                frmEventMsgs.RejectText = txt
            Case 1
                frmEventMsgs.Other0Text = txt
            Case 2
                frmEventMsgs.Other1Text = txt
            Case 3
                frmEventMsgs.Other2Text = txt
            Case 4
                frmEventMsgs.Other3Text = txt
            Case 5
                frmEventMsgs.Other4Text = txt
            End Select
        End If
    Next
End If

If nxt < 0 Then
    frmEventMsgs.Header = "No Rxs to print."
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If

frmEventMsgs.Show 1

If Not frmEventMsgs.Result = 4 Then
    If newPrescr Then
        If Not eRxValid Then
            Exit Sub
        End If
    End If
End If

Dim res As Integer
If frmEventMsgs.Result = 1 Then
    Call DoDrugPrint(True, "", "", False, True, False)
    Call InsertLog("FrmRxManage.frm", "Printed  Medication for Patient " & PatientId & "", LoginCatg.PrnMedication, "", "", 0, globalExam.iAppointmentId)
Else
    Select Case frmEventMsgs.Result
    Case 4
        Exit Sub
    Case 2, 3
        res = frmEventMsgs.Result - 2
    Case 5, 6, 7, 8
        res = frmEventMsgs.Result - 3
    End Select
    
    'Call DoDrugPrint(True, "", Trim(cmdDrug(res).Tag), True, False, False)
    Call DoDrugPrint(True, "", Trim(sTag(res)), True, False, False)
    Call InsertLog("FrmRxManage.frm", "Printed  Medication for Patient " & PatientId & "", LoginCatg.PrnMedication, "", "", 0, globalExam.iAppointmentId)
    'cmdDrug(res).ThreeDShadowColor = cmdCancel.ThreeDShadowColor
End If
End Sub

Private Sub ColorBorder()
Dim s As Integer
Dim i As Integer
Dim OutStat As Integer

s = cmdDrug(0).ToolTipText
For i = s To s + 5
    If i < lstRxs.ListCount Then
        OutStat = GetOutputStat(lstRxs.List(i))
        Select Case OutStat
        Case 0
            cmdDrug(i - s).ThreeDHighlightColor = cmdDone.ThreeDHighlightColor
            cmdDrug(i - s).ThreeDShadowColor = cmdDone.ThreeDShadowColor
        Case 9
            cmdDrug(i - s).ThreeDHighlightColor = vbWhite
            cmdDrug(i - s).ThreeDShadowColor = vbWhite
        Case Else
            cmdDrug(i - s).ThreeDHighlightColor = vbBlue
            cmdDrug(i - s).ThreeDShadowColor = vbBlue
        End Select
    End If
Next
End Sub

Private Function TriggerRx(iIndex As Integer) As Boolean
Dim ClnId As Long
Dim DrgId As Long
Dim MyTrig As Integer
Dim p As Integer
Dim k As Integer, j As Integer
Dim OrgP As Boolean
Dim OrgPrescribeOn As Boolean
Dim RxHigh As Boolean, RetFollow As Boolean
Dim Temp As String, TheAction As String
Dim RxId As String
Dim RxName As String, Rx As String
Dim RxDose As String, RxFreq As String
Dim RxDate As String, RxRefill As String
Dim RxDuration As String, RxNote As String
Dim WasPrinted As Boolean
Dim fIndex As Integer
fIndex = cmdDrug(iIndex).ToolTipText
TriggerRx = True
Set frmRxDetails.moDrugRx = New CDrugRx
frmRxDetails.moDrugRx.Copy ColDrugRx(fIndex + 1)
RxName = frmRxDetails.moDrugRx.Name

OrgPrescribeOn = PrescribeOn

'to change "Add a Drug" to "Write RX"
'PrescribeOn = False
OrgP = False
If (cmdDrug(iIndex).Font.Bold) Then
    OrgP = True
    'PrescribeOn = True
End If
WasPrinted = (cmdDrug(iIndex).ForeColor = vbWhite)

If (EditPreviousOn) Then
    If Not (LatestAppointmentOn) Then
        frmEventMsgs.Header = "Use most recent visit for any changes to Rx " + RxName
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Other5Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 2) Then
            MyTrig = 1
            Rx = ""
            RxDose = ""
            RxFreq = ""
            RxRefill = ""
            RxNote = ""
            RxDate = ""
            Call FormatTodaysDate(RxDate, False)
            k = InStrPS(cmdDrug(iIndex).Text, vbCrLf)
            If (k > 0) Then
                Rx = Trim(Left(cmdDrug(iIndex).Text, k - 1))
                j = InStrPS(k + 2, cmdDrug(iIndex).Text, vbCrLf)
                If (j > 0) Then
                    RxDose = Trim(Mid(cmdDrug(iIndex).Text, k + 2, (j - 1) - (k + 1)))
                    k = InStrPS(j + 2, cmdDrug(iIndex).Text, vbCrLf)
                    If (k > 0) Then
                        RxFreq = Trim(Mid(cmdDrug(iIndex).Text, j + 2, (k - 1) - (j + 1)))
                       j = InStrPS(k + 2, cmdDrug(iIndex).Text, vbCrLf)
                        If (j > 0) Then
                            RxRefill = Trim(Mid(cmdDrug(iIndex).Text, k + 2, (j - 1) - (k + 1)))
                            k = InStrPS(j + 2, cmdDrug(iIndex).Text, vbCrLf)
                            If (k > 0) Then
                                RxDuration = Trim(Mid(cmdDrug(iIndex).Text, j + 2, (k - 1) - (j + 1)))
                            Else
                                RxDuration = Trim(Mid(cmdDrug(iIndex).Text, j + 2, Len(cmdDrug(iIndex).Text) - (j + 1)))
                            End If
                        End If
                    End If
                End If
            End If
            frmRxDetails.AppointmentId = AppointmentId
            If (frmRxDetails.LoadRxDetails(False, True, True)) Then
                frmRxDetails.Show 1
                If (frmRxDetails.moDrugRx.Id > 0) Then
                        ' Allow only changes to the underlying drug record for corrections
                        TheAction = RxName + "-2/" + Trim(frmRxDetails.moDrugRx.Dosage) + "-3/" + Trim(frmRxDetails.moDrugRx.Frequency) + "-4/" + Trim(frmRxDetails.moDrugRx.Refills) + "-5/" + Trim(frmRxDetails.moDrugRx.Duration) + "-6/"
                        If (OrgP) Then
                            TheAction = RxName + "-2/" + Trim(frmRxDetails.moDrugRx.Dosage) + "-3/" + Trim(frmRxDetails.moDrugRx.Frequency) + "-4/" + Trim(frmRxDetails.moDrugRx.Refills) + "-5/" + Trim(frmRxDetails.moDrugRx.Duration) + "-6/P-7/"
                        End If
                        ClnId = Val(Trim(cmdDrug(iIndex).Tag))
                        If (ClnId > 0) Then
                            Temp = ConvertToApptDate(ClnId)
                            Call PostToDB(ClnId, "", "", TheAction, RxId)
                            Call LoadRxs(True)
                        End If
                    End If
                End If
        End If
        Exit Function
    End If
End If
'==================================
frmEventMsgs.Header = "Review " + RxName

frmEventMsgs.AcceptText = ""
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.RejectText = ""
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Other5Text = "Info" 'Info
frmEventMsgs.IsInfo = True
If (Left(cmdAnother.Text, 5) = "Write") _
Or frmSystems1.EditPreviousOn Then
    frmEventMsgs.RejectText = "Change Rx"
    frmEventMsgs.Other0Text = "Discontinue"
    frmEventMsgs.Other1Text = "Delete"
    If (UCase(Left(cmdDrug(iIndex).Tag, 1)) <> "E") Then
        frmEventMsgs.Other2Text = "Refill" & vbNewLine & "(no script)"
        frmEventMsgs.Other3Text = "Continue"
    End If
Else
    If OrgP Then
        frmEventMsgs.Header = frmEventMsgs.Header & _
        vbNewLine & "Other choices available from Plan Rx screen."
    Else
        frmEventMsgs.RejectText = "Change Rx"
        frmEventMsgs.Other1Text = "Delete"
    End If
End If
If (NewOn) Then
    frmEventMsgs.Other4Text = "Patient Discontinue"
    frmEventMsgs.Other0Text = "Discontinue"
End If
frmEventMsgs.Show 1

Dim Action As Integer
Action = frmEventMsgs.Result

Select Case Action
Case 2, 6
    frmEventMsgs.IsInfo = False
    Dim tst As Boolean
    Dim clsDrug As New CDrug
    clsDrug.DisplayName = RxName
    tst = clsDrug.SelectDrug(1)
    Set clsDrug = Nothing
    If Not tst Then
        frmEventMsgs.Header = "Please match to certified drug table."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Lookup"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        
        If frmEventMsgs.Result = 2 Then
            frmAlphaPad.NumPad_Field = "Name of Drug"
            frmAlphaPad.NumPad_DisplayFull = True
            Dim lookup As String
            lookup = myTrim(Mid(RxName, 1, 5))
            frmAlphaPad.txtResult.Text = lookup
            frmAlphaPad.Show 1
            If (Trim(frmAlphaPad.NumPad_Result) = "") Then
                Exit Function
            Else
                Set clsDrug = New CDrug
                clsDrug.DisplayName = Trim(frmAlphaPad.NumPad_Result)
                Set frmAssignDrugs.RSAll = clsDrug.GetDrugListAll
                Set clsDrug = Nothing
                'StartNum = 0
                Call frmAssignDrugs.LoadRxAll
                
                frmAssignDrugs.NameOnly = True
                frmAssignDrugs.Show 1
                RxName = frmAssignDrugs.SelectedDrug
                
                If RxName = "" Then Exit Function
            End If
        Else
            Exit Function
        End If
        
        Action = 1
    End If
End Select
    
Select Case Action
Case 1, 2                                           'Change
    frmEventMsgs.IsInfo = False
    MyTrig = 2
    If (Mid(RxName, Len(RxName), 1) = "*") Then
        RxName = Left(RxName, Len(RxName) - 1)
    End If
    DrgId = GetDrug_6(RxName)
    If (DrgId = 0) Then
        DrgId = GetDrug(RxName)
    End If
    If (DrgId > 0) Then
        Set frmRxDetails.moDrugRx = New CDrugRx
        If Action = 2 Then
            frmRxDetails.moDrugRx.Copy ColDrugRx(fIndex + 1)
        End If
        frmRxDetails.ChangeRx = True
        frmRxDetails.ChangeRxInd = iIndex
                frmRxDetails.moDrugRx.Id = DrgId
                frmRxDetails.AppointmentId = AppointmentId
        Dim pat As New Patient
        frmRxDetails.lblPatient = pat.GetPatientInfo(PatientId)
        Set pat = Nothing
        
        If (frmRxDetails.LoadRxDetails(False, True, True)) Then
            If (Left(cmdAnother.Text, 5) = "Write") Then
                frmRxDetails.WriteRx = True
            Else
                frmRxDetails.WriteRx = False
            End If
            Set frmRxDetails.DIList = lstRxs
            frmRxDetails.Show 1

            If (Val(frmRxDetails.moDrugRx.Id) > 0) Then
                        'TheAction = RxName + "-2/" + Trim(frmRxDetails.moDrugRx.Dosage) + "-3/" + Trim(frmRxDetails.moDrugRx.Frequency) + "-4/" + Trim(frmRxDetails.moDrugRx.Refills) + "-5/" + Trim(frmRxDetails.moDrugRx.Duration) + "-6/"
                        TheAction = frmRxDetails.moDrugRx.GetDrugRxFormat("record2")
                    Else
                        Exit Function
            End If
        Else
            Exit Function
        End If
        cmdDrug(iIndex).Tag = Trim(cmdDrug(iIndex).Tag)
        If (UCase(Left(cmdDrug(iIndex).Tag, 1)) = "E") Then
' Current Appointment Logic or a new Rx in EPV
            ClnId = Val(Trim(Mid(cmdDrug(iIndex).Tag, 2, Len(cmdDrug(iIndex).Tag))))
            If (ClnId > 0) Then
                Call frmEvaluation.DeleteAnAction(ClnId)
                If OrgP Then
                    TheAction = TheAction + "P-7/"
                Else
                    If (MyTrig = 2) And (Left(cmdAnother.Text, 5) = "Write") Then
                        TheAction = TheAction + "P-7/"
                    End If
                End If
                setOutputStat TheAction
                If (EditPreviousOn) Then
                    Call PostToDB(ClnId, "", "", TheAction, RxId)
                Else
                    Call frmEvaluation.AddAnAction(TheAction, "8", "Rx")
                End If
            Else
                If (EditPreviousOn) Then
                    Call PostToDB(0, "", "", TheAction, RxId)
                End If
            End If
            If (MyTrig = 1) Then
                Temp = lstRxs.List(Val(Trim(cmdDrug(iIndex).ToolTipText)))
                Mid(Temp, 109, 1) = " "
                lstRxs.List(Val(Trim(cmdDrug(iIndex).ToolTipText))) = Temp
            Else
                Temp = lstRxs.List(Val(Trim(cmdDrug(iIndex).ToolTipText)))
                Mid(Temp, 109, 1) = "N"
                lstRxs.List(Val(Trim(cmdDrug(iIndex).ToolTipText))) = Temp
            End If
        Else
' EPV exclusive
            ClnId = Val(Trim(cmdDrug(iIndex).Tag))
            If (ClnId > 0) Then
                
                'setActivity (ClnId)

                Call FormatTodaysDate(Temp, False)
                Call SetRxStatus(ClnId, "C", Temp, "")
                If (MyTrig = 2) And (OrgP) Then
                    TheAction = TheAction + "P-7/"
                End If
                setOutputStat TheAction
                If (EditPreviousOn) Then
                    If Action = 1 Then
                        Call PostToDB(0, "R", Temp, TheAction, RxId)
                    Else
                                Call PostToDB(ClnId, "!", Temp, TheAction, RxId)
                            End If
                Else
                    Call frmEvaluation.AddAnAction(TheAction, "8", "Rx")
                End If
            Else
                If (EditPreviousOn) Then
                    Call PostToDB(0, "", "", TheAction, RxId)
                End If
            End If
            If (MyTrig = 1) Then
                Temp = lstRxs.List(Val(Trim(cmdDrug(iIndex).ToolTipText)))
                Mid(Temp, 109, 1) = " "
                lstRxs.List(Val(Trim(cmdDrug(iIndex).ToolTipText))) = Temp
            Else
                Temp = lstRxs.List(Val(Trim(cmdDrug(iIndex).ToolTipText)))
                Mid(Temp, 109, 1) = "N"
                lstRxs.List(Val(Trim(cmdDrug(iIndex).ToolTipText))) = Temp
            End If
        End If
    Else
        MsgBox "drug not found"
    End If
    PrescribeOn = OrgPrescribeOn
    Call LoadRxs(True)
Case 3, 8                                             ' Office Discontinue / Patient Discontinue
    frmEventMsgs.IsInfo = False
    If (Left(cmdDrug(iIndex).Tag, 1) <> "E") Then
        ClnId = Val(Trim(cmdDrug(iIndex).Tag))
        If (ClnId > 0) Then
            Call FormatTodaysDate(Temp, False)
            If Action = 3 Then
                Call SetRxStatus(ClnId, "D", Temp, "")
            Else
                Call SetRxStatus(ClnId, "P", Temp, "")
            End If
        End If
    Else
        ClnId = Val(Trim(Mid(cmdDrug(iIndex).Tag, 2, Len(cmdDrug(iIndex).Tag))))
        If (ClnId > 0) Then
            p = ClnId
            If (frmEvaluation.GetActionEvaluation(p, TheAction, RxHigh, RetFollow)) Then
                Call ReplaceCharacters(TheAction, "*-2/", "-2/")
                Call ReplaceCharacters(TheAction, "-2/", "*-2/")
                Call ReplaceCharacters(TheAction, "Rx-8/", " ")
                TheAction = Trim(TheAction)
                Call frmEvaluation.DeleteAnAction(ClnId)
                Call frmEvaluation.AddAnAction(TheAction, "8", "Rx")
            End If
        End If
    End If
    PrescribeOn = OrgPrescribeOn
    Call LoadRxs(True)
    Call InsertLog("FrmRxManage.frm", "Medication Has Been Discontinued For Patient " & CStr(PatientId) & " ", LoginCatg.MedicationDiscontinue, "", "", 0, globalExam.iAppointmentId)
Case 5                                             ' Delete
    frmEventMsgs.IsInfo = False
    If (AreYouSure) Then
        If (Left(Trim(cmdDrug(iIndex).Tag), 1) <> "E") And (Left(Trim(cmdDrug(iIndex).Tag), 1) <> "T") Then
            ClnId = Val(Trim(cmdDrug(iIndex).Tag))
            If (ClnId > 0) Then
                Call FormatTodaysDate(Temp, False)
                If (EditPreviousOn) Then
                    Temp = ConvertToApptDate(ClnId)
                End If
                Call SetRxStatus(ClnId, "X", Temp, RxName)
            Else
                If (ClnId > 0) Then
                    Call SetRxStatus(ClnId, "X", "", RxName)
                End If
            End If
        Else
            ClnId = Val(Trim(Mid(Trim(cmdDrug(iIndex).Tag), 2, Len(Trim(cmdDrug(iIndex).Tag)))))
            If (ClnId = 0) And (Len(Trim(cmdDrug(iIndex).Tag)) > 2) Then
                ClnId = Val(Trim(Mid(Trim(cmdDrug(iIndex).Tag), 3, Len(Trim(cmdDrug(iIndex).Tag)))))
            End If
            If (ClnId > 0) Then
                Call frmEvaluation.DeleteAnAction(ClnId)
            End If
            Call SetRxStatus(ClnId, "X", "", RxName)
        End If
        Call LoadRxs(True)
        Call InsertLog("FrmRxManage.frm", " Medication Has Been Deleted For Patient" & CStr(PatientId) & " ", LoginCatg.MedicationDel, "", "", 0, globalExam.iAppointmentId)
    End If
Case 6                                            ' Refill
    frmEventMsgs.IsInfo = False
    If (Left(cmdDrug(iIndex).Tag, 1) <> "E") Then
        ClnId = Val(Trim(cmdDrug(iIndex).Tag))
        If (ClnId > 0) Then
            Call FormatTodaysDate(Temp, False)
            Call SetRxStatus(ClnId, "R", Temp, "")
            Call LoadRxs(True)
            Call InsertLog("FrmRxDetails.frm", " Medication Has Been Renewed  For " & CStr(PatientId) & "", LoginCatg.MedicationRenew, "", "", 0, globalExam.iAppointmentId)
        End If
    End If
Case 7                                            ' Continue
   frmEventMsgs.IsInfo = False
    If (Left(cmdDrug(iIndex).Tag, 1) <> "E") Then
        ClnId = Val(Trim(cmdDrug(iIndex).Tag))
        If (ClnId > 0) Then
            Call FormatTodaysDate(Temp, False)
            Call SetRxStatus(ClnId, "K", Temp, "")
            Call LoadRxs(True)
            Call InsertLog("FrmRxManage.frm", " Medication Has Been Continued  For " & CStr(PatientId) & "", LoginCatg.MedicationContinue, "", "", 0, globalExam.iAppointmentId)
        End If
    End If
    Call InsertLog("FrmRxManage.frm", " Medication Has Been Discontinued  By The  Patient  " & CStr(PatientId) & "", LoginCatg.MedicationDiscontinue, "", "", 0, globalExam.iAppointmentId)
Case 9
ClnId = Val(Trim(cmdDrug(iIndex).Tag)) 
Dim ClinicalManager As New ClinicalIntegration.frmInfo
 ClinicalManager.PatientId = CStr(globalExam.iPatientId)
 ClinicalManager.IsMedList = True
 ClinicalManager.DBObject = IdbConnection
 ClinicalManager.ExternalId = ClnId
 ClinicalManager.AppointmentId = CStr(globalExam.iAppointmentId)
 ClinicalManager.ResourceId = CStr(UserLogin.iId)
 Dim OrderString As String
 Dim RS As Recordset
 Dim strArr() As String
 Dim RxCUI As String
 strArr = Split(RxName, " ")
 RxCUI = strArr(0)
 ClinicalManager.DrugName = RxCUI
 OrderString = "select count (rx.RxNorm) as Count from dbo.patientclinical pc inner join [dbo].[FDBtoRxNorm1] rx on pc.drawfilename = rx.FDBId where clinicaltype = 'A' and pc.findingdetail like 'Rx-8/" + RxCUI + "%'"
 Set RS = GetRS(OrderString)
 If RS.RecordCount > 0 And RS("Count") > 0 Then
    ClinicalManager.ShowDialog
 Else
    MsgBox "Information not Found"
 End If
    frmEventMsgs.IsInfo = False
 End Select
 frmEventMsgs.IsInfo = False
End Function

Public Function LoadRxs(Init As Boolean) As Boolean
Dim AHigh As Boolean
Dim PresOn As Boolean
Dim RetFollow As Boolean
Dim z As Integer
Dim Y As Integer
Dim q As Integer, j As Integer, k As Integer
Dim UTemp1 As String
Dim RxName As String, LclDate As String
Dim ATemp1 As String, ATemp2 As String
Dim Rx As String, Dt As String, Ref As String
Dim NewDt As String
Dim DBMedCount As Integer
Dim ANote As String, FNote As String, Ix As String

Dim RetrieveNotes As New PatientNotes
ReDim DrugNote1(0)
ReDim DrugNote2(0)
Dim r As Integer

LoadRxs = False
DBMedCount = 0
    If (Init) Then
        CurrentIndex = 1
        lstRxs.Clear
        lstNonOcular.Clear
        lstAllergy.Clear
        ATemp1 = Mid(frmSystems1.ActiveActivityDate, 5, 2) + "/" + Mid(frmSystems1.ActiveActivityDate, 7, 2) + "/" + Mid(frmSystems1.ActiveActivityDate, 1, 4)
        If (EditPreviousOn) Then
            Label4.Caption = "Drug Management (Rx Snapshot at completion of " + ATemp1 + " Appt)"
        Else
            Label4.Caption = "Drug Management"
        End If
        LclDate = ATemp1
        ATemp1 = ""
    End If
    ' must come first so we overwrite new ones within the current appointment
    Call LoadRxActiveOnly(PatientId)
DBMedCount = lstRxs.ListCount
q = 1
While (frmEvaluation.GetActionEvaluation(q, Ref, AHigh, RetFollow))
    If (UCase(Left(Ref, 2)) = "RX") Then
        RxName = ""
        Dt = ""
        j = InStrPS(Ref, "-8/")
        k = InStrPS(Ref, "-2/")
        If (j > 0) And (k > 1) Then
            Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
            RxName = Rx
            Dt = Rx + vbCrLf
            j = InStrPS(Ref, "-3/")
            If (j > 0) Then
                Rx = Mid(Ref, k + 3, (j - 1) - (k + 2))
                Dt = Dt + Rx + vbCrLf
                k = InStrPS(Ref, "-4/")
                If (k > 0) Then
                    Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
                    Dt = Dt + Rx + vbCrLf
                    j = InStrPS(Ref, "-5/")
                    If (j > 0) Then
                        Rx = Mid(Ref, k + 3, (j - 1) - (k + 2))
                        Dt = Dt + Rx + vbCrLf
                        k = InStrPS(Ref, "-6/")
                        If (k > 0) Then
                            Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
                            Dt = Dt + Rx + vbCrLf
                        Else
                            Dt = Dt + "" + vbCrLf
                        End If
                    Else
                        Dt = Dt + "" + vbCrLf
                    End If
                Else
                    Dt = Dt + "" + vbCrLf
                End If
            Else
                Dt = Dt + "" + vbCrLf
            End If
        Else
            Dt = Trim(Ref) + vbCrLf
        End If
        Dt = Left((Dt & Space(110)), 110) + "E" + Trim(str(q))
        If (InStrPS(Ref, "P-7/") = 0) Then
            Mid(Dt, 110, 1) = "T"
        Else
            Mid(Dt, 110, 1) = " "
'            Dt = Dt & "+"
        End If
        If (RxName <> "") Then
            Call GetMedsNote(PatientId, 0, RxName, ANote, FNote)
        End If
        If (Trim(ANote) <> "") Then
            j = InStrPS(Dt, "As Prescribed")
            If (j <> 0) Then
                ATemp2 = Left(ANote, 13)
                If (Len(ATemp2) < 13) Then
                    ATemp2 = ATemp2 + Space(13 - Len(ATemp2))
                End If
                ATemp1 = Left(Dt, j - 1) + ATemp2 + Mid(Dt, j + 13, Len(Dt) - (j + 12))
                Dt = ATemp1
            End If
        End If
        Ix = IsDrugOkay(RxName)
        If (Ix = "O") Then
            ATemp1 = Dt
            Y = InStrPS(Dt, vbCrLf)
            If (Y > 0) Then
                Y = InStrPS(Y + 1, Dt, vbCrLf)
                If (Y > 0) Then
                    ATemp1 = Left(Dt, Y)
                End If
            End If
            If (Mid(ATemp1, Len(ATemp1) - 1, 1) = "*") Then
                ATemp1 = Left(ATemp1, Len(ATemp1) - 2) + Mid(ATemp1, Len(ATemp1), 1)
            End If
            PresOn = False
            For z = 0 To lstRxs.ListCount - 1
                ATemp2 = lstRxs.List(z)
                Y = InStrPS(lstRxs.List(z), vbCrLf)
                If (Y > 0) Then
                    Y = InStrPS(Y + 1, lstRxs.List(z), vbCrLf)
                    If (Y > 0) Then
                        ATemp2 = Left(lstRxs.List(z), Y)
                    End If
                End If
                If (ATemp1 = ATemp2) Then
                    PresOn = True
                    If (Len(Dt) > 117) Then
                        UTemp1 = Trim(Mid(Dt, 117, Len(Dt) - 116))
                        Dt = Trim(Left(Dt, 116)) + vbCrLf + "Rvisd " + LclDate
                        If (Len(Dt) > 116) Then
                            Dt = Left(Dt, 116) + UTemp1
                        Else
                            Dt = Dt + Space(116 - Len(Dt)) + UTemp1
                        End If
                        lstRxs.List(z) = Dt
                    Else
                        Dt = Trim(Dt) + vbCrLf + "Rvisd " + LclDate
                        lstRxs.List(z) = Dt
                    End If
                    Exit For
                End If
            Next z
            'If Not (PresOn) Then
                ''''lstRxs.AddItem Dt
                Set moDrugRx = New CDrugRx
                Call moDrugRx.LoadRxFromDB(Ref)
                NewDt = moDrugRx.GetDrugRxFormat("display") & vbCrLf
                NewDt = Mid(NewDt, 1, 109)
                NewDt = NewDt + Space(109 - Len(NewDt)) + Mid(Dt, 110)
                ColDrugRx.Add moDrugRx, Mid(Dt, 110)
                lstRxs.AddItem NewDt
            'End If
        Else
            If (Mid(Dt, 110, 1) = " ") Then
                ATemp1 = Dt
                Y = InStrPS(Dt, vbCrLf)
                If (Y > 0) Then
                    ATemp1 = Left(Dt, Y - 1)
                End If
                PresOn = False
                For z = 0 To lstRxs.ListCount - 1
                    ATemp2 = lstRxs.List(z)
                    Y = InStrPS(lstRxs.List(z), vbCrLf)
                    If (Y > 0) Then
                        ATemp2 = Left(lstRxs.List(z), Y - 1)
                    End If
                    If (ATemp1 = ATemp2) Then
                        PresOn = True
                        If (Len(Dt) > 117) Then
                            UTemp1 = Mid(Dt, 117, Len(Dt) - 116)
                            Dt = Trim(Left(Dt, 116)) + vbCrLf + "Rvisd " + LclDate
                            Dt = Dt + Space(116 - Len(Dt)) + UTemp1
                            lstRxs.List(z) = Dt
                        Else
                            Dt = Trim(Dt) + vbCrLf + "Rvisd " + LclDate
                            lstRxs.List(z) = Dt
                        End If
                        Exit For
                    End If
                Next z
                If Not (PresOn) Then
                    Set moDrugRx = New CDrugRx
                    Call moDrugRx.LoadRxFromDB(Ref)
                    NewDt = moDrugRx.GetDrugRxFormat("display") & vbCrLf
                    NewDt = Mid(NewDt, 1, 109)
                    NewDt = NewDt + Space(109 - Len(NewDt)) + Mid(Dt, 110)
                    ColDrugRx.Add moDrugRx, Mid(Dt, 110)
                    ''''lstRxs.AddItem Dt
                    lstRxs.AddItem NewDt
                End If
            Else
                Call ReplaceCharacters(Dt, Chr(13), " ")
                Call ReplaceCharacters(Dt, Chr(10), " ")
                k = InStrPS(Dt, "Disc")
                If (k > 0) Then
                    Rx = "D"
                Else
                    Rx = ""
                End If
                k = InStrPS(Dt, vbCrLf)
                If (k > 0) Then
                    Dt = Left(Dt, k - 1) + " " + Rx + Space((108 - Len(Rx)) - (k - 1)) + Mid(Dt, 110, Len(Dt) - 109)
                End If
                ATemp1 = Dt
                Y = InStrPS(Dt, vbCrLf)
                If (Y > 0) Then
                    ATemp1 = Left(Dt, Y - 1)
                End If
                PresOn = False
                For z = 0 To lstNonOcular.ListCount - 1
                    ATemp2 = lstNonOcular.List(z)
                    Y = InStrPS(lstNonOcular.List(z), vbCrLf)
                    If (Y > 0) Then
                        ATemp2 = Left(lstNonOcular.List(z), Y - 1)
                    End If
                    If (ATemp1 = ATemp2) Then
                        PresOn = True
                        lstNonOcular.List(z) = Dt
                        Exit For
                    End If
                Next z
                If Not (PresOn) Then
                    '''lstNonOcular.AddItem Dt
                    Set moDrugRx = New CDrugRx
                    Call moDrugRx.LoadRxFromDB(Ref)
                    NewDt = moDrugRx.GetDrugRxFormat("list")
                    NewDt = Mid(NewDt, 1, 109)
                    NewDt = NewDt + Space(109 - Len(NewDt)) + Mid(Dt, 110)
                    '''ColDrugRx.Add moDrugRx, Mid(Dt, 110)
                    lstNonOcular.AddItem NewDt
                End If
            End If
        End If
    End If
    
    
    If lstRxs.ListCount > UBound(DrugNote1) Then
        ReDim Preserve DrugNote1(lstRxs.ListCount)
        ReDim Preserve DrugNote2(lstRxs.ListCount)
    
        RetrieveNotes.NotesPatientId = PatientId
        RetrieveNotes.NotesSystem = RxName
        RetrieveNotes.NotesType = "R"
        If (RetrieveNotes.FindNotesbyPatient > 0) Then
            r = 1
            While (RetrieveNotes.SelectNotes(r))
                DrugNote1(lstRxs.ListCount) = RetrieveNotes.NotesText1
                r = r + 1
            Wend
        End If
        
        RetrieveNotes.NotesPatientId = PatientId
        RetrieveNotes.NotesSystem = RxName
        RetrieveNotes.NotesType = "X"
        If (RetrieveNotes.FindNotesbyPatient > 0) Then
            r = 1
            While (RetrieveNotes.SelectNotes(r))
                DrugNote2(lstRxs.ListCount) = RetrieveNotes.NotesText1
                r = r + 1
            Wend
        End If

    End If
    
    q = q + 1
Wend

Set RetrieveNotes = Nothing


txtNotes.BackColor = lstNonOcular.BackColor
Call GetMedsNote(PatientId, AppointmentId, "", ANote, "")
cmdMagnify.Visible = False
If (Trim(ANote) <> "") Then
    txtNotes.Text = Trim(ANote)
    If (Trim(ANote) <> "") Then
        txtNotes.Text = Trim(ANote)
        If (Len(ANote) > 0) Then
            txtNotes.BackColor = lstAllergy.BackColor
            cmdMagnify.Visible = True
        End If
    End If
End If
txtNotes.Locked = True
LoadRxs = True
If (lstRxs.ListCount > 0) Then
    If (lstRxs.ListCount > DBMedCount) Then
        Dim RetApptDateRet As New SchedulerAppointment
        RetApptDateRet.AppointmentId = AppointmentId
        If (RetApptDateRet.RetrieveSchedulerAppointment) Then
            Dim jMed As Integer
            jMed = 0
            Dim jMedR As Integer
            If (DBMedCount = 0) Then
                jMedR = 0
            ElseIf (DBMedCount = 1) Then
                jMedR = 1
            Else
                jMedR = DBMedCount
            End If

                For jMed = jMedR To (lstRxs.ListCount - 1)
                    MedDates(jMed) = RetApptDateRet.AppointmentDate
                Next
            End If
        
    End If
    Call LoadRxButtons
Else
    Call ClearRxs
End If
If (PrescribeOn) Then
    cmdAnother.Text = "Write RX"
Else
    cmdAnother.Text = "Add a Drug"
End If
End Function

Private Function LoadRxActiveOnly(PatId As Long) As Boolean
Dim MyTestDateOn As Boolean
Dim IncludeOn As Boolean
Dim DateOkay As Boolean
Dim PresOn As Boolean
Dim z As Integer
Dim Y As Integer
Dim i As Integer, k As Integer, j As Integer
Dim TDate As String
Dim ATemp1 As String, ATemp2 As String
Dim MyEye As String
Dim Rx As String, Dt As String, Ix As String
Dim NewDt As String
Dim Ref As String
Dim MyTestDate As Integer
Dim DisplayTestDate As String
Dim RxNote As String
Dim RxNote2 As String
Dim RxName As String, NowDate As String
Dim RetCln As PatientClinical
Dim RetAppt As SchedulerAppointment
Dim q As Integer
Dim r As Integer
Dim RetrieveNotes As New PatientNotes
ReDim DrugNote1(0)
ReDim DrugNote2(0)
Do Until ColDrugRx.Count = 0
    ColDrugRx.Remove 1
Loop

If (PatId > 0) Then
    NowDate = ""
    Call FormatTodaysDate(NowDate, False)
    If (Trim(ApptDate) = "") Then
        ApptDate = Mid(NowDate, 7, 4) + Mid(NowDate, 1, 2) + Mid(NowDate, 4, 2)
    End If
    If (EditPreviousOn) Then
        NowDate = Mid(ApptDate, 5, 2) + "/" + Mid(ApptDate, 7, 2) + "/" + Left(ApptDate, 4)
    Else
        NowDate = Mid(NowDate, 7, 4) + Mid(NowDate, 1, 2) + Mid(NowDate, 4, 2)
    End If
    Set RetCln = New PatientClinical
    RetCln.PatientId = PatId
    RetCln.AppointmentId = 0
    RetCln.ClinicalType = "A"
    RetCln.EyeContext = ""
    RetCln.Findings = ""
    RetCln.Symptom = ""
    RetCln.ImageDescriptor = ""
    RetCln.ImageInstructions = ""
    If (RetCln.FindPatientClinical > 0) Then
        i = 1
        While (RetCln.SelectPatientClinical(i))
            If (Left(RetCln.Findings, 2) = "RX") And (RetCln.ClinicalStatus = "A") Then
                If Not Left(RetCln.ImageDescriptor, 1) = "C" Then
'-------------------------------------------------------------------------------------------
                DateOkay = True
                ' REFACTOR: Properly indent this
                If (RetCln.AppointmentId <> AppointmentId) Then
                    DateOkay = False
                    Set RetAppt = New SchedulerAppointment
                    RetAppt.AppointmentId = RetCln.AppointmentId
                    If (RetAppt.RetrieveSchedulerAppointment) Then
                        If (RetAppt.AppointmentDate <= ApptDate) Then
                            TDate = RetAppt.AppointmentDate
                            If (Trim(RetCln.ImageDescriptor) <> "") Then
                                TDate = Mid(RetCln.ImageDescriptor, 2, 10)
                                TDate = Mid(TDate, 7, 4) + Mid(TDate, 1, 2) + Mid(TDate, 4, 2)
                            End If
                            DateOkay = True
                        End If
                    End If
                    Set RetAppt = Nothing
                End If
                MyTestDate = 2
                MyTestDateOn = False
                DisplayTestDate = ""
                If (Trim(RetCln.ImageDescriptor) <> "") Then
                    MyTestDate = InStrPS(RetCln.ImageDescriptor, NowDate)
                    If (MyTestDate <> 0) Then
                        MyTestDateOn = True
                        DisplayTestDate = Mid(RetCln.ImageDescriptor, MyTestDate, 10)
                        If (EditPreviousOn) Then
                            TDate = Mid(DisplayTestDate, 7, 4) + Mid(DisplayTestDate, 1, 2) + Mid(DisplayTestDate, 4, 2)
                        End If
                    Else
                        MyTestDate = 2
                    End If
                Else
                    MyTestDateOn = False
                    MyTestDate = 2
                End If
'
' Rx Rules
'
' Plan (PL):
'   Drugs Prescribed in Current Visit (Ocular or NonOocular)
'   Drugs Touched in Current Visit (Ocular or Non-Ocular)
'       Exclude any Patient Discontined
'
' Ongoing (ON):
'   Any Patient Discontinued Drug
'   Non-Prescribed drugs whether ocular or non-ocular
'   Active Prescribed Drugs Only
'
                IncludeOn = False
                If (AppointmentId = RetCln.AppointmentId) Then
                    If (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) <> "P") And _
                       (InStrPS(RetCln.Findings, "P-7") <> 0) Then
                        If (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) <> "C") Then
                            IncludeOn = True
                        End If
                    End If
                Else
                    If (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) <> "!") And _
                       (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) <> "P") And _
                       (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) <> "C") And _
                       (ApptDate <= TDate) Then
                        IncludeOn = True
                    End If
                    If ((Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "!") And _
                        (Len(Trim(RetCln.ImageDescriptor)) < 12)) Or _
                       (Trim(RetCln.ImageDescriptor) = "") Then
                        IncludeOn = True
                    End If
                End If
                If (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "P") And _
                   (ApptDate = TDate) Then
                    IncludeOn = True
                End If
                If (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "!") And _
                   (InStrPS(RetCln.Findings, "P-7") <> 0) And _
                   (ApptDate >= TDate) Then
                    IncludeOn = True
                End If
                If (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "!") And _
                   (ApptDate >= TDate) Then
                    IncludeOn = True
                End If
                If (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "R") And _
                   (ApptDate >= TDate) Then
                    IncludeOn = True
                End If
                If (InStrPS(RetCln.Findings, "P-7") = 0) And _
                   (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "C") And _
                   (ApptDate = TDate) Then
                    IncludeOn = True
                End If
                If (InStrPS("!KR^", Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1)) <> 0) And _
                   (ApptDate >= TDate) Then
                    IncludeOn = True
                End If
                If (IncludeOn) And (DateOkay) Then
                    Dt = ""
                    RxName = ""
                    Ref = Trim(RetCln.Findings)
                    j = InStrPS(Ref, "-1/")
                    If (j < 1) Then
                        j = InStrPS(Ref, "-8/")
                    Else
                        Call ReplaceCharacters(Ref, "-8/", " ")
                    End If
                    k = InStrPS(Ref, "-2/")
                    If (k > 0) And (k < j) Then
                        j = 1
                    End If
                    If (j > 0) And ((k - 1) > (j + 2)) Then
                        Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
                        Call ReplaceCharacters(Rx, "-1/", "~")
                        Call StripCharacters(Rx, "~")
                        Rx = Trim(Rx)
                        RxName = Rx
                        
                        q = q + 1

                        Dt = Rx + vbCrLf
                        j = InStrPS(Ref, "-3/")
                        If (j > 0) Then
                            Rx = Mid(Ref, k + 3, (j - 1) - (k + 2))
                            Dt = Dt + Rx + vbCrLf
                            k = InStrPS(Ref, "-4/")
                            If (k > 0) Then
                                Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
                                ' REFACTOR: This can't match anymore, as it's getting the full "(AS PRESCRIBED)()(PO)()()()" now.
                                If (UCase(Rx) = "(AS PRESCRIBED)") Then
                                    Call frmRxDetails.GetRxNotes(AppointmentId, PatientId, Trim(RxName), RxNote, RxNote2, True)
                                    Call StripCharacters(RxNote, Chr(13))
                                    Call ReplaceCharacters(RxNote, Chr(10), " ")
                                    Dt = Dt + RxNote + vbCrLf
                                Else
                                    Dt = Dt + Rx + vbCrLf
                                End If
                                j = InStrPS(Ref, "-5/")
                                If (j > 0) Then
                                    Rx = Mid(Ref, k + 3, (j - 1) - (k + 2))
                                    Dt = Dt + Rx + vbCrLf
                                    k = InStrPS(Ref, "-6/")
                                    If (k > 0) Then
                                        Rx = Mid(Ref, j + 3, (k - 1) - (j + 2))
                                        Dt = Dt + Rx + vbCrLf
                                    Else
                                        Dt = Dt + "" + vbCrLf
                                    End If
                                Else
                                    Dt = Dt + "" + vbCrLf
                                End If
                            Else
                                Dt = Dt + "" + vbCrLf
                            End If
                        Else
                            Dt = Dt + "" + vbCrLf
                        End If
                    Else
                        If (Len(Dt) >= Len(Trim(Ref))) Then
                            Mid(Dt, 1, Len(Trim(Ref))) = Trim(Ref) + vbCrLf
                            RxName = Ref
                        End If
                    End If
                    
                    'override to display latest status
                    MyTestDate = 2
                    '----------------------------------
                    If (Trim(RetCln.ImageDescriptor) <> "") Then
                        If (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "D") Then
                            Rx = "Disc   "
                        ElseIf (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "R") Then
                            Rx = "Refill "
                        ElseIf (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "C") Then
                            Rx = "Rvisd  "
                        ElseIf (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "P") Then
                            Rx = "Pdisc  "
                        ElseIf (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "K") Then
                            Rx = "Cont   "
                        ElseIf (Mid(RetCln.ImageDescriptor, MyTestDate - 1, 1) = "X") Then
                            Rx = "Deleted "
                        Else
                            Rx = "Active  "
                            If ((MyTestDate + 11) > Len(Trim(RetCln.ImageDescriptor))) And (RetCln.AppointmentId <> AppointmentId) Then
                                Rx = "Start   "
                            ElseIf ((MyTestDate + 11) > Len(Trim(RetCln.ImageDescriptor))) Then
                                Rx = "Start   "
                            End If
                        End If
                        If (Trim(Rx) <> "Active") Then
                            If (EditPreviousOn) Then
                                'override to display latest status
                                'Rx = Rx + DisplayTestDate
                                Rx = Rx + Mid(RetCln.ImageDescriptor, 2, 10)
                                '--------------------------------------------
                            Else
                                Rx = Rx + Mid(RetCln.ImageDescriptor, 2, 10)
                            End If
                        End If
                    Else
                        Rx = "Start   "
                    End If
                    Dt = Dt + Rx + vbCrLf
                    If (Len(Dt) > 110) Then
                        Dt = Left(Dt, 110)
                        Dt = Dt + Space(110 - Len(Dt)) + Trim(str(RetCln.ClinicalId))
                    Else
                        Dt = Dt + Space(110 - Len(Dt)) + Trim(str(RetCln.ClinicalId))
                    End If
                    Mid(Dt, 110, 1) = " "
                    If (InStrPS(RetCln.Findings, "P-7") = 0) Then
                        Mid(Dt, 110, 1) = "T"
                    Else
                        Mid(Dt, 110, 1) = "E"
                        If (InStrPS(Dt, "Start") <> 0) Then
                            Mid(Dt, 110, 1) = " "
                        End If
                    End If
                    If (InStrPS(Rx, "Refill") <> 0) Then
                        Mid(Dt, 110, 1) = " "
                    End If
                    Ix = IsDrugOkay(RxName)
                    If (Ix = "O") Or (InStrPS(RetCln.Findings, "P-7") <> 0) Then
                        ATemp1 = Dt
                        Y = InStrPS(Dt, vbCrLf)
                        If (Y > 0) Then
                            ATemp1 = Left(Dt, Y - 1)
                        End If
                        PresOn = False
                        MyEye = ""
                        If (InStrPS(Dt, "(OS)") <> 0) Then
                            MyEye = "OS"
                        ElseIf (InStrPS(Dt, "(OD)") <> 0) Then
                            MyEye = "OD"
                        ElseIf (InStrPS(Dt, "(OU)") <> 0) Then
                            MyEye = "OU"
                        End If
                        For z = 0 To lstRxs.ListCount - 1
                            ATemp2 = lstRxs.List(z)
                            Y = InStrPS(lstRxs.List(z), vbCrLf)
                            If (Y > 0) Then
                                ATemp2 = Left(lstRxs.List(z), Y - 1)
                            End If
                            If (ATemp1 = ATemp2) Then
                                If (InStrPS(lstRxs.List(z), MyEye) <> 0) And (MyEye <> "") Then
                                    PresOn = True
                                    If (Trim(Left(Rx, 6)) = "Active") And (Len(Trim(RetCln.ImageDescriptor)) > 0) Then
                                        Call ReplaceCharacters(Dt, "Active", "Rvisd ")
                                    ElseIf (Trim(Left(Rx, 5)) = "Start") And (Len(Trim(RetCln.ImageDescriptor)) > 0) Then
                                        Call ReplaceCharacters(Dt, "Start", "Rvisd ")
                                    End If
                                    lstRxs.List(z) = Dt
                                End If
                            End If
                        Next z
                        If Not (PresOn) Then
                            Set moDrugRx = New CDrugRx
                            Call moDrugRx.LoadRxFromDB(RetCln.Findings)
                            NewDt = moDrugRx.GetDrugRxFormat("display") & Rx & vbCrLf
                            NewDt = Mid(NewDt, 1, 109)
                            NewDt = NewDt + Space(109 - Len(NewDt)) + Mid(Dt, 110)
                            ColDrugRx.Add moDrugRx, Mid(Dt, 110)
                            ''''lstRxs.AddItem Dt
                            lstRxs.AddItem NewDt
                            ''
                            Dim RetApptDateRet As New SchedulerAppointment
                            RetApptDateRet.AppointmentId = RetCln.AppointmentId
                            If (RetApptDateRet.RetrieveSchedulerAppointment) Then
                            ''
                                MedDates(lstRxs.ListCount() - 1) = RetApptDateRet.AppointmentDate
                            End If
                            
                            If lstRxs.ListCount > UBound(DrugNote1) Then
                                ReDim Preserve DrugNote1(lstRxs.ListCount)
                                ReDim Preserve DrugNote2(lstRxs.ListCount)
                            
                                RetrieveNotes.NotesPatientId = PatientId
                                RetrieveNotes.NotesType = "R"
                                RetrieveNotes.NotesSystem = RxName
                                If (RetrieveNotes.FindNotesbyPatient > 0) Then
                                    r = 1
                                    While (RetrieveNotes.SelectNotes(r))
                                        If RetrieveNotes.NotesCommentOn Then
                                            DrugNote1(lstRxs.ListCount) = RetrieveNotes.NotesText1
                                        Else
                                            DrugNote2(lstRxs.ListCount) = RetrieveNotes.NotesText1
                                        End If
                                        r = r + 1
                                    Wend
                                End If
                            End If
                        End If
                    Else
                        k = InStrPS(Dt, "Disc")
                        If (k > 0) Then
                            Rx = "D"
                        Else
                            Rx = ""
                        End If
                        k = InStrPS(Dt, vbCrLf)
                        If (k > 0) Then
                            Dt = Left(Dt, k - 1) + " " + Rx + Space((108 - Len(Rx)) - (k - 1)) + Mid(Dt, 110, Len(Dt) - 109)
                        End If
                        ATemp1 = Dt
                        Y = InStrPS(Dt, vbCrLf)
                        If (Y > 0) Then
                            ATemp1 = Left(Dt, Y - 1)
                        End If
                        PresOn = False
                        For z = 0 To lstNonOcular.ListCount - 1
                            ATemp2 = lstNonOcular.List(z)
                            Y = InStrPS(lstNonOcular.List(z), vbCrLf)
                            If (Y > 0) Then
                                ATemp2 = Left(lstNonOcular.List(z), Y - 1)
                            End If
                            If (ATemp1 = ATemp2) Then
                                PresOn = True
                                lstNonOcular.List(z) = Dt
                            End If
                        Next z
                        If Not (PresOn) Then
                            lstNonOcular.AddItem Dt
                        End If
                    End If
                End If
            End If
'-------------------------------------------------------------------------------------------
            End If
            i = i + 1
        Wend
    End If
    Set RetCln = Nothing
    If (lstExternal.ListCount > 1) Then
        For i = 1 To lstExternal.ListCount - 1
            If (Trim(lstExternal.List(i)) <> "") Then
                lstAllergy.AddItem lstExternal.List(i)
            End If
        Next i
    End If
End If
If (lstRxs.ListCount > 0) Then
    LoadRxActiveOnly = True
End If
Set RetrieveNotes = Nothing
End Function

Private Sub ClearRxs()
Dim i As Integer
    For i = 0 To MAX_DRUG_BUTTONS - 1
        With cmdDrug(i)
            .Visible = False
            .Text = ""
            .Tag = ""
            .ToolTipText = ""
            .Font.Bold = False
        End With
    Next i
End Sub

Private Function LoadRxButtons() As Boolean
    Dim i As Integer
    Dim ButtonCnt As Integer
    Dim dateStr As String
    Dim preStr As String
    LoadRxButtons = True
    Call ClearRxs
    ButtonCnt = 1
    i = CurrentIndex
    While (i <= lstRxs.ListCount) And (ButtonCnt < 7)
        If (Trim(lstRxs.List(i - 1)) <> "") Then
            If (InStrPS(Trim(Left(lstRxs.List(i - 1), 109)), "Start") <> 0) Then
                preStr = Trim(Left(lstRxs.List(i - 1), 109))
                preStr = Replace(preStr, "Start", "StartDT:")
                dateStr = "ApptDT:" + Mid(MedDates(i - 1), 5, 2) + "/" + Mid(MedDates(i - 1), 7, 2) + "/" + Mid(MedDates(i - 1), 1, 4)
            Else
                preStr = Trim(Left(lstRxs.List(i - 1), 109))
                dateStr = "StartDT:" + Mid(MedDates(i - 1), 5, 2) + "/" + Mid(MedDates(i - 1), 7, 2) + "/" + Mid(MedDates(i - 1), 1, 4) + Space(20) + "ApptDT:" + Mid(MedDates(i - 1), 5, 2) + "/" + Mid(MedDates(i - 1), 7, 2) + "/" + Mid(MedDates(i - 1), 1, 4)
            End If
            With cmdDrug(ButtonCnt - 1)
                .Text = preStr + dateStr
                .Tag = Mid(lstRxs.List(i - 1), 111, Len(lstRxs.List(i - 1)) - 110)
                .ToolTipText = Trim(str(i - 1))
                .Visible = True
                .Font.Bold = Mid$(lstRxs.List(i - 1), 110, 1) <> "T"
                cmdDN(ButtonCnt - 1).Visible = Not (Trim(DrugNote1(i)) = "" And Trim(DrugNote2(i)) = "")
            End With
            ButtonCnt = ButtonCnt + 1
        End If
        i = i + 1
Wend
CurrentIndex = i - 1
    If (lstRxs.ListCount - 1 > 5) Then
        cmdMore.Visible = True
    Else
        cmdMore.Visible = False
    End If
    Label4.Visible = True

    ColorBorder
    ColorPrescribedMeds
    'ColorDiscontinueMeds

End Function

Private Sub ColorPrescribedMeds()
Dim s As Integer
Dim i As Integer
Dim OutStat As Integer
Dim OrderString As String
Dim RS As Recordset
Dim ClinId As String
 OrderString = "select * from patientclinical where patientId ='" & PatientId & "' and (activity like 'com' or activity like 'pen')"
Set RS = GetRS(OrderString)

            If RS.RecordCount > 0 Then
                With RS
                If Not .BOF And Not .EOF Then
                .MoveLast
                .MoveFirst
            While (Not .EOF)
             ClinId = CStr(RS.Fields("ClinicalId"))
             s = cmdDrug(0).ToolTipText
            For i = s To s + 5
            
            If i < lstRxs.ListCount Then
                If cmdDrug(i - s).Tag = ClinId Then
                If CStr(RS.Fields("Activity")) = "pen" Then
                    cmdDrug(i - s).ThreeDHighlightColor = vbWhite
                    cmdDrug(i - s).ThreeDShadowColor = vbWhite
                ElseIf (CStr(RS.Fields("Activity")) = "com") Then
                    cmdDrug(i - s).ThreeDHighlightColor = vbGreen
                    cmdDrug(i - s).ThreeDShadowColor = vbGreen
                End If
                End If
            End If
            Next
            .MoveNext
            Wend
        End If
    .Close
    'Make sure you close the recordset...
End With
End If

End Sub

Private Sub ColorDiscontinueMeds()
Dim s As Integer
Dim i As Integer
Dim OutStat As Integer
Dim OrderString As String
Dim RS As Recordset
Dim ClinId As String

OrderString = "Select * from patientclinical where   patientid=" & PatientId & " And  Findingdetail like 'Rx%' and Status='A' AND left(ImageDescriptor,1) ='D' AND Convert(varchar(20),Right(Substring(ImageDescriptor,1,11),10))<=convert(varchar(20), getdate(),101)"
Set RS = GetRS(OrderString)

            If RS.RecordCount > 0 Then
                With RS
                If Not .BOF And Not .EOF Then
                .MoveLast
                .MoveFirst
            While (Not .EOF)
             ClinId = CStr(RS.Fields("ClinicalId"))
             s = cmdDrug(0).ToolTipText
            For i = s To s + 5
            
            If i < lstRxs.ListCount Then
                If cmdDrug(i - s).Tag = ClinId Then
                    cmdDrug(i - s).ThreeDHighlightColor = vbRed
                    cmdDrug(i - s).ThreeDShadowColor = vbRed
                
                End If
            End If
            Next
            .MoveNext
            Wend
        End If
    .Close
    
End With
End If

End Sub

Private Function GetMedsNote(PatId As Long, ApptId As Long, RxName As String, ANote As String, FNote As String) As Boolean
Dim i As Integer
Dim Temp As String
Dim RetNotes As PatientNotes
GetMedsNote = False
Temp = ""
ANote = ""
If (PatId > 0) Then
    If (RxName <> "") Then
        Temp = ""
        Set RetNotes = New PatientNotes
        RetNotes.NotesPatientId = PatId
        RetNotes.NotesAppointmentId = ApptId
        RetNotes.NotesSystem = RxName
        
        RetNotes.NotesType = "R"
        If (RetNotes.FindNotes > 0) Then
            i = 1
            While (RetNotes.SelectNotes(i))
                If (Trim(RetNotes.NotesOffDate) = "") And (UCase(Trim(RetNotes.NotesSystem)) = UCase(RxName)) And (RetNotes.NotesDate <= ApptDate) Then
                    If (Temp <> "") Then
                        Temp = Temp + vbCrLf
                    End If
                    If (Trim(RetNotes.NotesText1) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText1) + " " + Trim(RetNotes.NotesText2) + " " + Trim(RetNotes.NotesText3) + " " + Trim(RetNotes.NotesText4)
                        Temp = Trim(Temp)
                    End If
                End If
                i = i + 1
            Wend
        End If
        ANote = ANote + Trim(Temp)
        
        Temp = ""
        RetNotes.NotesType = "X"
        If (RetNotes.FindNotes > 0) Then
            i = 1
            While (RetNotes.SelectNotes(i))
                If (Trim(RetNotes.NotesOffDate) = "") And (UCase(Trim(RetNotes.NotesSystem)) = UCase(RxName)) And (RetNotes.NotesDate <= ApptDate) Then
                    If (Temp <> "") Then
                        Temp = Temp + vbCrLf
                    End If
                    If (Trim(RetNotes.NotesText1) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText1) + " " + Trim(RetNotes.NotesText2) + " " + Trim(RetNotes.NotesText3) + " " + Trim(RetNotes.NotesText4)
                        Temp = Trim(Temp)
                    End If
                End If
                i = i + 1
            Wend
        End If
        FNote = FNote + Trim(Temp)
        
        
        Set RetNotes = Nothing
    Else
        Temp = ""
        Set RetNotes = New PatientNotes
        RetNotes.NotesPatientId = PatId
        RetNotes.NotesAppointmentId = 0
        RetNotes.NotesSystem = ""
        RetNotes.NotesType = "R"
        If (RetNotes.FindNotes > 0) Then
            i = 1
            While (RetNotes.SelectNotes(i))
                If (Trim(RetNotes.NotesOffDate) = "") And (Trim(RetNotes.NotesSystem) = "") And ((RetNotes.NotesDate <= ApptDate) Or (ApptId = RetNotes.NotesAppointmentId)) Then
                    If (Temp <> "") Then
                        Temp = Temp + vbCrLf
                    End If
                    If (Trim(RetNotes.NotesText1) <> "") Then
                        Temp = Temp + Trim(RetNotes.NotesText1) + " " + Trim(RetNotes.NotesText2) + " " + Trim(RetNotes.NotesText3) + " " + Trim(RetNotes.NotesText4)
                        Temp = Trim(Temp)
                    End If
                End If
                i = i + 1
            Wend
        End If
        ANote = ANote + Trim(Temp)
        Set RetNotes = Nothing
        If (Len(ANote) > 0) Then
            GetMedsNote = True
        End If
    End If
End If
End Function

Private Function SetRxStatus(ClnId As Long, IType As String, TheDate As String, RxName As String) As Boolean
Dim i As Integer
Dim Cnt As Long
Dim MyTemp As String
Dim UrTemp As String
Dim RetCln As PatientClinical
Dim RetNote As PatientNotes
SetRxStatus = False
If (ClnId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        If (IType = "X") Then
            If (Trim(RxName) <> "") Then
                Set RetNote = New PatientNotes
''''''                RetNote.NotesAppointmentId = RetCln.AppointmentId
''''''                RetNote.NotesSystem = RxName
''''''                If (RetNote.FindNotes > 0) Then
''''''                    Cnt = 1
''''''                    While (RetNote.SelectNotes(Cnt))
''''''                        If (Trim(RetNote.NotesSystem) = RxName) Then
''''''                            Call RetNote.DeleteNotes
''''''                        End If
''''''                        Cnt = Cnt + 1
''''''                    Wend
''''''                End If
            
                RetNote.NotesPatientId = PatientId
                RetNote.NotesSystem = RxName
                RetNote.NotesType = "R"
                If (RetNote.FindNotesbyPatient > 0) Then
                    i = 1
                    While (RetNote.SelectNotes(i))
                        RetNote.DeleteNotes
                        i = i + 1
                    Wend
                End If
                
                RetNote.NotesPatientId = PatientId
                RetNote.NotesSystem = RxName
                RetNote.NotesType = "X"
                If (RetNote.FindNotesbyPatient > 0) Then
                    i = 1
                    While (RetNote.SelectNotes(i))
                        RetNote.DeleteNotes
                        i = i + 1
                    Wend
                End If
                Set RetNote = Nothing
            End If
            
            Call RetCln.DeletePatientClinical
        Else
            MyTemp = Trim(RetCln.ImageDescriptor)
            i = InStrPS(MyTemp, TheDate)
            If (i > 2) Then
                MyTemp = Trim(Left(MyTemp, i - 2) + Mid(MyTemp, i + 10, Len(MyTemp) - (i + 9)))
            ElseIf (i = 2) Then
                MyTemp = Trim(Mid(MyTemp, i + 10, Len(MyTemp) - (i + 9)))
            End If
            RetCln.ClinicalHighlights = ""
            RetCln.ImageDescriptor = IType + TheDate + MyTemp
            If (IType = "R") Then
                MyTemp = ""
                UrTemp = Trim(RetCln.ImageInstructions)
                If (Len(UrTemp) > 10) Then
                    MyTemp = Trim(Mid(UrTemp, 11, Len(UrTemp) - 10))
                    UrTemp = Trim(Left(UrTemp, 10))
                End If
                Cnt = Val(Trim(UrTemp))
                Cnt = Cnt + 1
                UrTemp = Trim(str(Cnt))
                UrTemp = UrTemp + Space(10 - Len(UrTemp))
                'ImageInstructions will be used to post eRx respond
                'RetCln.ImageInstructions = UrTemp + MyTemp
            End If
            Call RetCln.ApplyPatientClinical
        End If
        SetRxStatus = True
    End If
    Set RetCln = Nothing
ElseIf (IType = "N") And (Trim(RxName) <> "") And (ClnId < 0) Then
    Set RetNote = New PatientNotes
    RetNote.NotesAppointmentId = -1 * ClnId
    RetNote.NotesSystem = RxName
    If (RetNote.FindNotes > 0) Then
        i = 1
        While (RetNote.SelectNotes(i))
            If (Trim(RetNote.NotesSystem) = RxName) Then
                Call RetNote.DeleteNotes
            End If
            i = i + 1
        Wend
    End If
    Set RetNote = Nothing
End If
End Function

Private Function GetDrug(TheName As String) As Long
Dim RetDrug As DiagnosisMasterDrugs
GetDrug = 0
If (Trim(TheName) <> "") Then
    Set RetDrug = New DiagnosisMasterDrugs
    RetDrug.DrugName = Trim(TheName)
    If (RetDrug.FindDrugbyName > 0) Then
        If (RetDrug.SelectDrug(1)) Then
            GetDrug = RetDrug.DrugId
        End If
    End If
    Set RetDrug = Nothing
End If
End Function

Private Function GetDrug_6(TheName As String) As Long
Dim clsDrug As New CDrug
clsDrug.DisplayName = TheName
clsDrug.SelectDrug 1
GetDrug_6 = clsDrug.DrugId
Set clsDrug = Nothing
End Function

Private Function IsDrugOkay(TheName As String) As String
If (Trim(TheName) <> "") Then
    If (Mid(TheName, Len(TheName), 1) = "*") Then
        TheName = Left(TheName, Len(TheName) - 1)
    End If
    
    Dim clsDrug As New CDrug
    clsDrug.DisplayName = TheName
    If clsDrug.SelectDrug(1) Then
        If clsDrug.Focus Then IsDrugOkay = "O"
    Else
        Dim RetDrug As DiagnosisMasterDrugs
        Set RetDrug = New DiagnosisMasterDrugs
        RetDrug.DrugName = Trim(TheName)
        If (RetDrug.FindDrugbyName > 0) Then
            If (RetDrug.SelectDrug(1)) Then
                IsDrugOkay = Trim(RetDrug.DrugSpecialtyType)
            End If
        End If
        Set RetDrug = Nothing
    End If
End If
End Function

Private Sub form_load()
    ActiveOnly = False
End Sub

Private Sub fpE_Click()
If fpE.Text = "Exam Screen" Then
    frmSystems1.TheStationId = 999
    globalExam.SetExamIds 0, 0, ActivityId
    frmSystems1.EditPreviousOn = True

    If (frmSystems1.LoadSummaryInfo(True)) Then
        frmSystems1.FormOn = True
        frmSystems1.fromRxManage = True
        frmSystems1.Show 1
        frmSystems1.fromRxManage = False
        Me.ZOrder 0
    End If
End If

End Sub

Private Sub lstNonOcular_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
Dim i As Integer
Dim Idx As Integer
Dim ClnId As Long
Dim Temp As String
Dim RxName As String
frmAppType.ApptPeriod = ""
frmAppType.ApptTypeId = 0
frmAppType.ApptTypeName = ""
Set frmAppType.lstBox = lstNonOcular
If (frmAppType.LoadAppt("~", True)) Then
    frmAppType.Show 1
    If (frmAppType.ApptTypeId >= 0) Then
        Idx = frmAppType.ApptTypeId
        RxName = Trim(Left(lstNonOcular.List(Idx), 109))
        frmEventMsgs.Header = "Review " + RxName
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = ""
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Discontinue"
        frmEventMsgs.Other1Text = "Delete"
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Other5Text = "Info" 'Info
            frmEventMsgs.IsInfo = True
            If (NewOn) Then
            If (PrescribeOn) Then
                frmEventMsgs.Other3Text = "Continue"
            End If
            frmEventMsgs.Other4Text = "Patient Discontinue"
        End If
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 3) Then
        frmEventMsgs.IsInfo = False
            If (UCase(Mid(lstNonOcular.List(Idx), 111, 1)) = "E") Then
                ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 97, 10)))
                If (ClnId > 0) Then
                    Call frmEvaluation.DeleteAnAction(ClnId)
                Else
                    ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 97, 10)))
                    Call frmEvaluation.DeleteAnAction(ClnId)
                    lstNonOcular.RemoveItem Idx
                End If
            Else
                ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 111, 10)))
                If (ClnId > 0) Then
                    Call FormatTodaysDate(Temp, False)
                    Call SetRxStatus(ClnId, "D", Temp, "")
                Else
                    ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 97, 10)))
                    Call frmEvaluation.DeleteAnAction(ClnId)
                    lstNonOcular.RemoveItem Idx
                End If
            End If
            Call LoadRxs(True)
        ElseIf (frmEventMsgs.Result = 5) Then
        frmEventMsgs.IsInfo = False
            If (AreYouSure) Then
                If (UCase(Mid(lstNonOcular.List(Idx), 111, 1)) <> "E") Then
                    ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 111, 10)))
                    If (ClnId > 0) Then
                        Call FormatTodaysDate(Temp, False)
                        i = InStrPS(RxName, "(As ")
                        If (i > 0) Then
                            RxName = Trim(Left(RxName, i - 1))
                        End If
                        Call SetRxStatus(ClnId, "X", Temp, RxName)
                    End If
                    Call LoadRxs(True)
                Else
                    ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 97, 10)))
                    If (ClnId = 0) Then
                        ClnId = Val(Mid(lstNonOcular.List(Idx), 112, Len(lstNonOcular.List(Idx)) - 111))
                    End If
                    Call frmEvaluation.DeleteAnAction(ClnId)
                    i = InStrPS(RxName, "(As ")
                    If (i > 0) Then
                        RxName = Trim(Left(RxName, i - 1))
                    End If
                    Call SetRxStatus(-1 * AppointmentId, "N", "", RxName)
                    lstNonOcular.RemoveItem Idx
                End If
            End If
        ElseIf (frmEventMsgs.Result = 7) Then
        frmEventMsgs.IsInfo = False
            If (UCase(Mid(lstNonOcular.List(Idx), 111, 1)) <> "E") Then
                ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 111, 10)))
                If (ClnId > 0) Then
                    Call FormatTodaysDate(Temp, False)
                    i = InStrPS(RxName, "(As ")
                    If (i > 0) Then
                        RxName = Trim(Left(RxName, i - 1))
                    End If
                    Call SetRxStatus(ClnId, "K", Temp, RxName)
                    Call LoadRxs(True)
                End If
            End If
        ElseIf (frmEventMsgs.Result = 8) Then
        frmEventMsgs.IsInfo = False
            If (UCase(Mid(lstNonOcular.List(Idx), 111, 1)) = "E") Then
                ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 97, 10)))
                If (ClnId > 0) Then
                    Call frmEvaluation.DeleteAnAction(ClnId)
                Else
                    ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 97, 10)))
                    Call frmEvaluation.DeleteAnAction(ClnId)
                    lstNonOcular.RemoveItem Idx
                End If
            Else
                ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 111, 10)))
                If (ClnId > 0) Then
                    Call FormatTodaysDate(Temp, False)
                    Call SetRxStatus(ClnId, "P", Temp, "")
                Else
                    ClnId = Val(Trim(Mid(lstNonOcular.List(Idx), 97, 10)))
                    Call frmEvaluation.DeleteAnAction(ClnId)
                    lstNonOcular.RemoveItem Idx
                End If
            End If
        ElseIf (frmEventMsgs.Result = 9) Then
             Dim ClinicalManager As New ClinicalIntegration.frmInfo
             ClinicalManager.PatientId = CStr(globalExam.iPatientId)
             ClinicalManager.IsMedList = True
             ClinicalManager.DBObject = IdbConnection
             ClinicalManager.ResourceId = CStr(UserLogin.iId)
             ClinicalManager.AppointmentId = CStr(globalExam.iAppointmentId)
             Dim OrderString As String
             Dim RS As Recordset
             
             Dim strArr() As String
             Dim RxCUI As String
             strArr = Split(RxName, " ")
             RxCUI = strArr(0)
             ClinicalManager.DrugName = RxCUI
                'OrderString = "select count(rx.RxCUI) as Count from dbo.drug d inner join fdb.tblCompositeDrug nc on d.DisplayName = nc.MED_ROUTED_DF_MED_ID_DESC inner join [fdb].[RXCUI2MEDID] rx on nc.Medid = rx.MedId where d.Drugcode = 'T' and d.DisplayName like '" + RxCUI + "%'"
                OrderString = "select count (rx.RxNorm) as Count from dbo.patientclinical pc inner join [dbo].[FDBtoRxNorm1] rx on pc.drawfilename = rx.FDBId where clinicaltype = 'A' and pc.findingdetail like 'Rx-8/" + RxCUI + "%'"
             Set RS = GetRS(OrderString)
                If RS.RecordCount > 0 And RS("Count") > 0 Then
                  ClinicalManager.ShowDialog
                Else
                    MsgBox "Information not Found"
                End If
                frmEventMsgs.IsInfo = False
                Call LoadRxs(True)
        End If
    End If
End If
End Sub

Private Sub DoDrugPrint(TrigOn As Boolean, ThePrinter As String, TheRx As String, PrintAnyway As Boolean, NewOnly As Boolean, OurRxOnly As Boolean)
Dim TotalRx As Integer
Dim r As Integer, myR As Integer
Dim w As Integer, p As Integer
Dim i As Integer, q As Integer
Dim k As Integer, j As Integer
Dim ItemsToPrint(200) As Integer
Dim SigFile As String
Dim MyTemp As String
Dim MyTempDate As String
Dim LocalTest As String
Dim Rx As String, RxDura As String
Dim RxName As String, RxStr As String
Dim RxDose As String, RxFreq As String
Dim RxDate As String, RxRefill As String
Dim RxDaw As String
Dim RxNote As String, RxNote2 As String
Dim TheFile As String
Dim TempFile As String
Dim RefillOn As Boolean
Dim tstDrug As String
If (Trim(ThePrinter) = "") Then
    ThePrinter = GetPrinter(PRINTER_Rx)
End If
r = 0
q = 0
TotalRx = 0
RxDate = ""
Call FormatTodaysDate(RxDate, False)
For i = 0 To lstRxs.ListCount - 1
    MyTemp = lstRxs.List(i)
    RefillOn = False
    If (InStrPS(MyTemp, "Refill " + RxDate) <> 0) Then
        RefillOn = True
    End If
    LocalTest = Trim(Mid(MyTemp, 110, 30))
    If ((Trim(MyTemp) <> "") And (Mid(LocalTest, 1, 1) = "E")) Or (RefillOn) Then
        TotalRx = TotalRx + 1
    End If
    If (NewOnly) Then
        If (InStrPS(UCase(MyTemp), "SAMPLES") <> 0) Then
' skip them
        ElseIf (Left(LocalTest, 1) = "T") And (OurRxOnly) Then
            If (PrintAnyway) _
            Or ( _
                    (frmSystems1.EditPreviousOn) _
                And ((InStrPS(MyTemp, "Start") <> 0) _
                And (InStrPS(MyTemp, "Active") = 0) _
                And (InStrPS(MyTemp, "Disc") = 0) _
                And (InStrPS(MyTemp, "Cont") = 0)) _
                And (InStrPS(MyTemp, "*" + vbCr) = 0) _
                And (InStrPS(MyTemp, "Pdisc") = 0) _
                ) Then
                r = r + 1
                ItemsToPrint(r) = i
            End If
        Else
            If (PrintAnyway) _
            Or ( _
                    (InStrPS(MyTemp, "Start") = 0) _
                And (InStrPS(MyTemp, "Active") = 0) _
                And (InStrPS(MyTemp, "Disc") = 0) _
                And (InStrPS(MyTemp, "Cont") = 0) _
                And (InStrPS(MyTemp, "*" + vbCr) = 0) _
                And (InStrPS(MyTemp, "Pdisc") = 0) _
                ) Then
                    If (Left(LocalTest, 1) <> "T") Then
                        r = r + 1
                        ItemsToPrint(r) = i
                    End If
            ElseIf (PrintAnyway) Or ((frmSystems1.EditPreviousOn) And ((InStrPS(MyTemp, "Start") <> 0) And (InStrPS(MyTemp, "Active") = 0) And InStrPS(MyTemp, "Disc") = 0) And (InStrPS(MyTemp, "Cont") = 0) And (InStrPS(MyTemp, "*" + vbCr) = 0) And (InStrPS(MyTemp, "Pdisc") = 0)) Then
                If (Left(LocalTest, 1) <> "T") Then
                    r = r + 1
                    ItemsToPrint(r) = i
                End If
            End If
        End If
    Else
        If (Mid(LocalTest, 2, Len(LocalTest) - 1) = TheRx) Or (LocalTest = TheRx) Or (LocalTest = "E" + TheRx) Then
            r = r + 1
            ItemsToPrint(r) = i
        End If
    End If
Next i
myR = r
If (myR > 0) Or (TheRx <> "") Then
    lblPrint.Visible = True
    lblPrint.ZOrder 0
    DoEvents
End If
If (TheRx <> "") Then
    TotalRx = 1
End If
r = 1
For i = 1 To myR
    MyTemp = lstRxs.List(ItemsToPrint(i))
    RxName = ColDrugRx(ItemsToPrint(i) + 1).Name
    RxStr = ColDrugRx(ItemsToPrint(i) + 1).Strength
    tstDrug = IsE_Send(RxName, RxStr)

    RefillOn = False
    LocalTest = Trim(Mid(MyTemp, 100, 30))
    If (InStrPS(MyTemp, "Refill " + RxDate) <> 0) And (Trim(RxDate) <> "") Then
        MyTempDate = Mid(RxDate, 7, 4) + Mid(RxDate, 1, 2) + Mid(RxDate, 4, 2)
        If (MyTempDate = frmSystems1.ActiveActivityDate) Then
            RefillOn = True
        End If
    End If
    RxDate = ""
    ' If somehow we messed up and we're not looking at the drug we're supposed to be printing, reset.
    If (Trim(TheRx) <> "") Then
        If (TheRx <> Mid(MyTemp, Len(MyTemp) - (Len(TheRx) - 1), Len(TheRx))) Then
            MyTemp = ""
        End If
    End If
    If (Trim(MyTemp) <> "") Then
        q = q + 1
        Rx = ""
        RxDose = ""
        RxFreq = ""
        RxDaw = ""
        RxDura = ""
        RxRefill = ""
        RxNote = ""
        RxNote2 = ""
        RxDate = ""
        Call FormatTodaysDate(RxDate, False)
        k = InStrPS(MyTemp, vbCrLf)
        If (k > 0) Then
            Rx = Trim(Left(MyTemp, k - 1))
            j = InStrPS(k + 2, MyTemp, vbCrLf)
            If (j > 0) Then
                RxDose = Trim(Mid(MyTemp, k + 2, (j - 1) - (k + 1)))
                k = InStrPS(j + 2, MyTemp, vbCrLf)
                If (k > 0) Then
                    RxFreq = Trim(Mid(MyTemp, j + 2, (k - 1) - (j + 1)))
                    j = InStrPS(k + 2, MyTemp, vbCrLf)
                    If (j > 0) Then
                        RxRefill = Trim(Mid(MyTemp, k + 2, (j - 1) - (k + 1)))
                        w = InStrPS(RxRefill, "[Last")
                        If (w > 0) Then
                            RxRefill = Left(RxRefill, w - 1)
                        End If
                        k = InStrPS(j + 2, MyTemp, vbCrLf)
                        If (k > 0) Then
                            RxDura = Trim(Mid(MyTemp, j + 2, (k - 1) - (j + 1)))
                            k = InStrPS(UCase(RxRefill), "(DAW")
                            If (k <> 0) Then
                                RxDaw = "daw"
                                If (k = 1) Then
                                    RxRefill = ""
                                Else
                                    If (Len(RxRefill) - (k + 4) > 0) Then
                                        RxRefill = Left(RxRefill, k - 1) + Mid(RxRefill, k + 5, Len(RxRefill) - (k + 4))
                                    Else
                                        RxRefill = Left(RxRefill, k - 1)
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            If (InStrPS(UCase(RxRefill), "PILLS-PACKAGE") = 0) Then
                k = InStrPS(UCase(MyTemp), "(PILLS-PACKAGE")
                If (k > 0) Then
                    k = k + 14
                    RxRefill = RxRefill + " " + Mid(MyTemp, k, Len(MyTemp) - (k - 1))
                    k = InStrPS(RxRefill, vbCrLf)
                    If (k > 0) Then
                        RxRefill = Left(RxRefill, k - 2)
                    End If
                End If
            End If
        End If
        
        
        If (InStrPS(RxFreq, "(Over the Counter)") <> 0) Then
            Call ReplaceCharacters(RxFreq, "(Over the Counter)", " ")
        End If
        'REFACTOR: These are already being skipped above.  Don't think they need to be here.
        If (InStrPS(RxFreq, "(Samples)") <> 0) Then
            Call ReplaceCharacters(RxFreq, "(Samples)", " ")
        End If
        If ("(" + Trim(RxNote) + ")" = Trim(RxFreq)) Then
            RxFreq = ""
        End If
        TheFile = DoctorInterfaceDirectory + "DrugRx" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        If (FM.IsFileThere(TheFile)) Then
            If (i = 0) Then
                Call KillDocumentProcessing(TheFile, True, True)
            End If
        End If
        If (i = 0) Then
            If (FM.IsFileThere(TheFile)) Then
                FM.Kill TheFile
            End If
        End If
        
        Call frmRxDetails.GetRxNotes(AppointmentId, PatientId, ColDrugRx(ItemsToPrint(i) + 1).Name, _
        RxNote, RxNote2, False, True)
                
        If RxNote2 <> "" Then
            RxNote2 = ", " & "Note to Pharmacist: " & RxNote2
        End If
        RxNote = Trim(RxNote & RxNote2)
             
        Call StripCharacters(RxNote, Chr(13))
        Call ReplaceCharacters(RxNote, Chr(10), " ")
        Call CreateDrugs(PatientId, AppointmentId, ActivityId, TheFile, _
                               ColDrugRx(ItemsToPrint(i) + 1), _
                               RxNote, _
                               True, True)
'        If (i = 1) And (myR < 2) Then
'            Call CreateDrugs(PatientId, AppointmentId, ActivityId, TheFile, _
'                               ColDrugRx(ItemsToPrint(i) + 1), _
'                               RxNote, _
'                               True, True)
'        ElseIf (i = 1) Then
'            Call CreateDrugs(PatientId, AppointmentId, ActivityId, TheFile, _
'                               ColDrugRx(ItemsToPrint(i) + 1), _
'                               RxNote, _
'            True, False)
'        ElseIf (i = myR) Then
'            Call CreateDrugs(PatientId, AppointmentId, ActivityId, TheFile, _
'                               ColDrugRx(ItemsToPrint(i) + 1), _
'                               RxNote, _
'            False, True)
'        ElseIf (i < myR) Then
'            Call CreateDrugs(PatientId, AppointmentId, ActivityId, TheFile, _
'                               ColDrugRx(ItemsToPrint(i) + 1), _
'                               RxNote, _
'            False, False)
'        End If
    End If

If (lblPrint.Visible) Then
'    If (InStrPS(TheFile, ".txt") > 0) Then
'        Call ReplaceCharacters(TheFile, ".txt", ".xls")
'    End If
'
'    If (FM.IsFileThere(TheFile)) Then
'        Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
'        Select Case tstDrug
'        Case "2", "3", "4", "5"
'                 SigFile = Mid(SigFile, 1, Len(SigFile) - Len(Dir(SigFile))) & "NONE.doc"
'        End Select
'
'
'' use a special signature file setup for Rxs
'        If (Trim(SigFile) <> "") Then
'            p = InStrPS(SigFile, ".")
'            If (p > 0) Then
'                SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
'            End If
'        End If
'        TempFile = TemplateDirectory + "DrugRx.doc"
'        Call PrintTemplate(TheFile, TempFile, True, False, 1, ThePrinter, SigFile, myR, False)
'    End If

  If TheFile <> "" Then
     Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
        Select Case tstDrug
        Case "2", "3", "4", "5"
                 SigFile = Mid(SigFile, 1, Len(SigFile) - Len(Dir(SigFile))) & "NONE.doc"
        End Select


' use a special signature file setup for Rxs
        If (Trim(SigFile) <> "") Then
            p = InStrPS(SigFile, ".")
            If (p > 0) Then
                SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
            End If
        End If
        TempFile = TemplateDirectory + "DrugRx.doc"
        Call PrintTemplate(TheFile, TempFile, True, False, 1, ThePrinter, SigFile, myR, False)
   End If
 End If
Next i
If TheFile <> "" Then
Else
    If (TrigOn) Then
        frmEventMsgs.Header = "No Rxs to print."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
End If
lblPrint.Visible = False
End Sub

Private Function getSpNumber(inp As String)

Dim part(2) As String
Dim tran(1) As String
Dim i As Integer
Dim b As String
Dim p As Integer
Dim l As Integer
Dim RetCode As PracticeCodes

For i = 1 To Len(inp)
    b = Mid(inp, i, 1)
    If IsNumeric(b) Then
        part(p) = part(p) & b
        l = l + 1
    Else
        If b = "." Then
            p = 1
            part(1) = "."
            l = l + 1
        Else
            Exit For
        End If
    End If
Next


part(2) = Mid(inp, l + 1)
part(0) = Trim(part(0))

If Not part(0) = "" Then
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "SPELLNUMBER"
    RetCode.ReferenceCode = part(0)
    If (RetCode.FindCode > 0) Then
        If (RetCode.SelectCode(1)) Then
            tran(0) = RetCode.ReferenceAlternateCode
        End If
    End If
    If Not part(1) = "" Then
        Set RetCode = New PracticeCodes
        RetCode.ReferenceType = "SPELLFRACTION"
        RetCode.ReferenceCode = part(1)
        If (RetCode.FindCode > 0) Then
            If (RetCode.SelectCode(1)) Then
                tran(1) = RetCode.ReferenceAlternateCode
            End If
        End If
    End If
    Set RetCode = Nothing
End If

part(0) = part(0) & part(1)
If Not tran(0) = "" Then
    part(0) = "#" & part(0) & "(" & tran(0)
    If Not tran(1) = "" Then
        part(0) = part(0) & " and " & tran(1)
    End If
    part(0) = part(0) & ")"
End If

getSpNumber = part(0) & part(2)

End Function



Private Function CreateDrugs(PatId As Long, ApptId As Long, ActId As Long, ResultFile As String, _
fCDrugRx As CDrugRx, _
ANote As String, _
FirstOn As Boolean, LastOn As Boolean) As Boolean

'11  drug Name + Strength + OTC
'12  Dosage + Eye + PO/Apply to lids/Instill in Eyes + Frequency + PRN + Duration
'13  Refills
'14  Daw
'16  drug Note
'20  Pills/Package + Days supply
Dim MF11 As String
Dim MF12 As String
Dim MF13 As String
Dim MF14 As String
Dim MF20 As String
Dim mPillPackage As String
Dim Resultfeild As String
Dim Resultvalue As String
Dim resultstring As String

MF11 = (fCDrugRx.Name & " " & fCDrugRx.Strength)

MF12 = fCDrugRx.Dosage
MF12 = Trim(MF12) & " " & DisplayEyeRef(fCDrugRx.EyeRef)
MF12 = Trim(MF12) & " " & fCDrugRx.PO
MF12 = Trim(MF12) & " " & fCDrugRx.Frequency
If fCDrugRx.PRN Then MF12 = Trim(MF12) & " PRN"
MF12 = Trim(MF12) & " " & fCDrugRx.Duration

MF13 = fCDrugRx.Refills
If fCDrugRx.Daw Then MF14 = "DAW"



Dim RetCon As New PracticeConfigureInterface
If RetCon.getFieldValue("ERXVALID") = "T" Then
    'insert spellnumber
    mPillPackage = getSpNumber(fCDrugRx.PillPackage)
Else
    mPillPackage = fCDrugRx.PillPackage
End If
Set RetCon = Nothing
MF20 = Trim(mPillPackage & " " & fCDrugRx.DaysSupply)


Dim FileNum As Integer
Dim h As Integer
Dim i As Integer, q As Integer
Dim RscId As Long
Dim Temp As String, ADate As String
Dim NewFile As String, TempZip As String
Dim RetAppt As SchedulerAppointment
Dim RetDr As SchedulerResource
Dim RetAct As PracticeActivity
Dim RetPat As Patient
Dim RetPrc As PracticeName
Dim dte As String
lstBox.Clear
If (PatId > 0) Then
    Set RetPat = New Patient
    Set RetPrc = New PracticeName
    Set RetDr = New SchedulerResource
    Set RetAppt = New SchedulerAppointment
    Set RetAct = New PracticeActivity
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        If (ActId > 0) Then
            RscId = 0
            RetAct.ActivityId = ActId
            If (RetAct.RetrieveActivity) Then
                RscId = RetAct.ActivityCurrResId
                If (RscId > 0) Then
                    RetDr.ResourceId = RscId
                    If (RetDr.RetrieveSchedulerResource) Then
                        If (Trim(RetDr.ResourceSignatureFile) = "") Then
                            RscId = 0
                        End If
                    End If
                End If
            End If
            RetAppt.AppointmentId = ApptId
            If (RetAppt.RetrieveSchedulerAppointment) Then
                If (RscId < 1) Then
                    If (RscId = 0) Then
                        If (RetAppt.AppointmentResourceId1 > 0) Then
                            RetDr.ResourceId = RetAppt.AppointmentResourceId1
                            If (RetDr.RetrieveSchedulerResource) Then
                    
                            End If
                        ElseIf (RetPat.SchedulePrimaryDoctor > 0) Then
                            RetDr.ResourceId = RetPat.SchedulePrimaryDoctor
                            If (RetDr.RetrieveSchedulerResource) Then
                    
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
' Load the Merge File
    lstBox.Clear
    If (RetDr.ResourceId > 0) Then
        lstBox.AddItem "01:" + Trim(RetDr.ResourceDescription)
        lstBox.AddItem "03:" + Trim(RetDr.ResourceDea)
        lstBox.AddItem "04:" + Trim(RetDr.ResourceLic)
        If (RetDr.PracticeId > 0) Then
            RetPrc.PracticeId = RetDr.PracticeId
            If (RetPrc.RetrievePracticeName) Then
                TempZip = ""
                Call DisplayPhone(RetPrc.PracticePhone, TempZip)
                lstBox.AddItem "02:" + TempZip
                lstBox.AddItem "05:" + Trim(RetPrc.PracticeAddress)
                Temp = Trim(RetPrc.PracticeCity) + ", " + RetPrc.PracticeState + " " + RetPrc.PracticeZip
                lstBox.AddItem "06:" + Temp
            Else
                lstBox.AddItem "02:    "
                lstBox.AddItem "05:    "
                lstBox.AddItem "06:    "
            End If
        Else
            lstBox.AddItem "02:    "
            lstBox.AddItem "05:    "
            lstBox.AddItem "06:    "
        End If
    Else
        lstBox.AddItem "01:    "
        lstBox.AddItem "02:    "
        lstBox.AddItem "03:    "
        lstBox.AddItem "04:    "
        lstBox.AddItem "05:    "
        lstBox.AddItem "06:    "
    End If
    Temp = Trim(Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial) + " " + Trim(RetPat.LastName) + " " + Trim(RetPat.NameRef))
    Call StripCharacters(Temp, "'")
    Call StripCharacters(Temp, Chr(34))
    lstBox.AddItem "07:" + Temp
    i = GetAge(RetPat.BirthDate)
    lstBox.AddItem "08:" + Trim(str(i))
    lstBox.AddItem "09:" + Trim(RetPat.Address)
    TempZip = ""
    Call DisplayZip(RetPat.Zip, TempZip)
    Temp = Trim(RetPat.City) + ", " + RetPat.State + " " + TempZip
    lstBox.AddItem "10:" + Temp
    
    If MF11 <> "" Then
        lstBox.AddItem "11:" + MF11
    Else
        lstBox.AddItem "11:    "
    End If
    If MF12 <> "" Then
        lstBox.AddItem "12:" + MF12
    Else
        lstBox.AddItem "12:    "
    End If
    If MF13 <> "" Then
        lstBox.AddItem "13:" + MF13
    Else
        lstBox.AddItem "13:    "
    End If
    If MF20 <> "" Then
        lstBox.AddItem "20:" + MF20
    Else
        lstBox.AddItem "20:    "
    End If
    If MF14 <> "" Then
        lstBox.AddItem "14:" + MF14
    Else
        lstBox.AddItem "14:    "
    End If
    
    Call FormatTodaysDate(ADate, False)
    lstBox.AddItem "15:" + ADate
    If (Trim(ANote) <> "") Then
        Call StripCharacters(ANote, Chr(13))
        Call StripCharacters(ANote, Chr(10))
        lstBox.AddItem "16:" + Trim(ANote)
    Else
        lstBox.AddItem "16:    "
    End If
    If (Trim(RetDr.ResourceSignatureFile) <> "") Then
        If (FM.IsFileThere(Trim(RetDr.ResourceSignatureFile))) Then
            lstBox.AddItem "17:" + Trim(RetDr.ResourceSignatureFile)
        Else
            lstBox.AddItem "17:    "
        End If
    Else
        lstBox.AddItem "17:    "
    End If
    If (Trim(RetPat.BirthDate) <> "") Then
        lstBox.AddItem "18:" + Mid(RetPat.BirthDate, 5, 2) + "/" + Mid(RetPat.BirthDate, 7, 2) + "/" + Left(RetPat.BirthDate, 4)
    Else
        lstBox.AddItem "18:    "
    End If
    If (Trim(RetPat.Gender) <> "") Then
        lstBox.AddItem "19:" + RetPat.Gender
    Else
        lstBox.AddItem "19:"
    End If
    lstBox.AddItem "04a:    "
    lstBox.AddItem "04b:    "
    lstBox.AddItem "05a:    "
    lstBox.AddItem "06a:    "
    lstBox.AddItem "07a:    "
    lstBox.AddItem "08a:    "
    lstBox.AddItem "09a:    "
    lstBox.AddItem "09b:    "
    lstBox.AddItem "11a:    "
    lstBox.AddItem "11b:    "
    lstBox.AddItem "18a:    "
    lstBox.AddItem "18b:    "
    lstBox.AddItem "18c:    "
    lstBox.AddItem "18d:    "
    lstBox.AddItem "18e:    "
    lstBox.AddItem "18f:    "
    
    If (Trim(RetDr.ResourceNPI) <> "") Then
        lstBox.AddItem "21:" + Trim(RetDr.ResourceNPI)
    Else
        lstBox.AddItem "21:    "
    End If
    
    If (Trim(RetAppt.AppointmentDate) <> "") Then
        dte = Trim(RetAppt.AppointmentDate)
        lstBox.AddItem "22:" & Mid(dte, 5, 2) & "/" & Mid(dte, 7, 2) & "/" & Mid(dte, 1, 4)
    Else
        lstBox.AddItem "22:    "
    End If
    
    lstBox.AddItem "23:    "
    lstBox.AddItem "24:    "
    lstBox.AddItem "25:    "
    lstBox.AddItem "26:    "
    lstBox.AddItem "27:    "
    lstBox.AddItem "28:    "
    lstBox.AddItem "29:    "
    lstBox.AddItem "30:    "
    lstBox.AddItem "31:    "
    lstBox.AddItem "32:    "
    lstBox.AddItem "33:    "
    lstBox.AddItem "34:    "
    lstBox.AddItem "35:    "
    lstBox.AddItem "36:    "
    lstBox.AddItem "37:    "
    lstBox.AddItem "38:    "
    lstBox.AddItem "39:    "
    lstBox.AddItem "40:    "
    lstBox.AddItem "41:    "
    lstBox.AddItem "42:    "
    lstBox.AddItem "43:    "
    lstBox.AddItem "44:    "
    lstBox.AddItem "45:    "
    lstBox.AddItem "46:    "
    lstBox.AddItem "47:    "
    lstBox.AddItem "48:    "
    lstBox.AddItem "49:    "
    lstBox.AddItem "50:    "
    lstBox.AddItem "51:   "
    lstBox.AddItem "52:   "
    lstBox.AddItem "53:   "
    lstBox.AddItem "54:   "
    lstBox.AddItem "55:   "
    lstBox.AddItem "56:   "
    lstBox.AddItem "57:   "
    lstBox.AddItem "58:   "
    lstBox.AddItem "59:   "
    lstBox.AddItem "60:   "
    lstBox.AddItem "61:   "
    lstBox.AddItem "62:   "
    lstBox.AddItem "63:   "
    lstBox.AddItem "64:   "
    lstBox.AddItem "65:   "
    lstBox.AddItem "66:   "
    lstBox.AddItem "67:   "
    lstBox.AddItem "68:   "
    lstBox.AddItem "69:   "
    If (FirstOn) Then
        If (FM.IsFileThere(ResultFile)) Then
            FM.Kill ResultFile
        End If
        'FileCopy TemplateDirectory + "Letters.txt", ResultFile
        FileNum = FreeFile
        FM.OpenFile ResultFile, FileOpenMode.Binary, FileAccess.ReadWrite, CLng(FileNum)
        FM.CloseFile CLng(FileNum)
    Else
        FileNum = FreeFile
    End If
    FM.OpenFile ResultFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
    For i = 0 To lstBox.ListCount - 1
   
        q = InStrPS(lstBox.List(i), ":")
        If (q > 0) Then
         If Resultvalue = "" Then
            
             Resultvalue = Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q) & vbTab
             Call ReplaceCharacters(Resultvalue, vbCrLf, "-&-")
             Else
                  Call ReplaceCharacters(Resultvalue, vbCrLf, "-&-")
                Resultvalue = Resultvalue & Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q) & vbTab
            
             End If
            Temp = Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q)
            Call ReplaceCharacters(Temp, vbCrLf, "-&-")
            Print #FileNum, Temp; vbTab;
            'Print #FileNum, Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q); vbTab;
        End If
    Next i
    Print #FileNum, ""
    FM.CloseFile CLng(FileNum)
    
       Call createresultfield(Resultfeild)
        resultstring = Resultfeild & vbCrLf & Resultvalue
         
    Set RetAppt = Nothing
    Set RetAct = Nothing
    Set RetPat = Nothing
    Set RetPrc = Nothing
    Set RetDr = Nothing
    If (LastOn) Then
        NewFile = Left(ResultFile, Len(ResultFile) - 4) + ".xls"
'        If (FM.IsFileThere(NewFile)) Then
'            Kill NewFile
'        End If
'        Name ResultFile As NewFile
'        ResultFile = NewFile
      ResultFile = resultstring & "@" & NewFile
    End If
    CreateDrugs = True
End If

End Function
Private Sub createresultfield(Resultfeild As String)

        Resultfeild = ":1:" & vbTab & ":2:" & vbTab & ":3:" & vbTab & ":4:" & vbTab & ":4a:" & vbTab & ":4b:" & vbTab & ":5:" & vbTab & ":5a:" & vbTab & ":6:" & vbTab & ":6a:" & vbTab & ":7:" & vbTab & ":7a:" & vbTab & ":8:" & vbTab & ":8a:" & vbTab & ":9:" & vbTab & ":9a:" & vbTab & ":9b:" & vbTab & ":10:" & vbTab & ":11:" & vbTab & ":11a:" & vbTab & ":11b:" & vbTab & ":12:" & vbTab & ":13:" & vbTab & ":14:" & vbTab & ":15:" & vbTab & ":16:" & vbTab & ":17:" & vbTab & ":18:" & vbTab & ":18a:" & vbTab & ":18b:" & vbTab & ":18c:" & vbTab & ":18d:" & vbTab & ":18e:" & vbTab & ":18f:" & vbTab & ":19:" & vbTab & ":20:" & vbTab & ":21:" & vbTab & ":22:" & vbTab & ":23:" & vbTab & ":24:" & vbTab & ":25:" & vbTab & ":26:" & vbTab & ":27:" & vbTab & ":28:" & vbTab & ":29:"
        Resultfeild = Resultfeild + vbTab & ":30:" & vbTab & ":31:" & vbTab & ":32:" & vbTab & ":33:" & vbTab & ":34:" & vbTab & ":35:" & vbTab & ":36:" & vbTab & ":37:" & vbTab & ":38:" & vbTab & ":39:" & vbTab & ":40:" & vbTab & ":41:" & vbTab & ":42:" & vbTab & ":43:" & vbTab & ":44:" & vbTab & ":45:" & vbTab & ":46:" & vbTab & ":47:" & vbTab & ":48:" & vbTab & ":49:" & vbTab & ":50:" & vbTab & ":51:" & vbTab & ":52:" & vbTab & ":53:" & vbTab & ":54:" & vbTab & ":55:" & vbTab & ":56:" & vbTab & ":57:" & vbTab & ":58:" & vbTab & ":59:" & vbTab & ":60:" & vbTab & ":61:" & vbTab & ":62:" & vbTab & ":63:" & vbTab & ":64:" & vbTab & ":65:" & vbTab & ":66:" & vbTab & ":67:" & vbTab & ":68:" & vbTab & ":69:" & vbTab & ":70:" & vbTab & ":70a:"
        Resultfeild = Resultfeild + vbTab & ":71:" & vbTab & ":71a:" & vbTab & ":72:" & vbTab & ":72a:" & vbTab & ":73:" & vbTab & ":73a:" & vbTab & ":74:" & vbTab & ":74a:" & vbTab & ":75:" & vbTab & ":75a:" & vbTab & ":76:" & vbTab & ":76a:" & vbTab & ":77:" & vbTab & ":77a:" & vbTab & ":78:" & vbTab & ":78a:" & vbTab & ":79:" & vbTab & ":79a:" & vbTab & ":80:" & vbTab & ":80a:" & vbTab & ":81:" & vbTab & ":81a:" & vbTab & ":82:" & vbTab & ":82a:" & vbTab & ":83:" & vbTab & ":83a:" & vbTab & ":84:" & vbTab & ":84a:" & vbTab & ":85:" & vbTab & ":85a:" & vbTab & ":86:" & vbTab & ":86a:" & vbTab & ":87:" & vbTab & ":87a:" & vbTab & ":88:" & vbTab & ":88a:" & vbTab & ":89:" & vbTab & ":89a:" & vbTab & ":90:" & vbTab & ":90a:" & vbTab & ":91:" & vbTab & ":91a:" & vbTab & ":92:" & vbTab & ":92a:"
        Resultfeild = Resultfeild + vbTab & ":93:" & vbTab & ":93a" & vbTab & ":94:" & vbTab & ":94a:" & vbTab & ":95:" & vbTab & ":95a:" & vbTab & ":96:" & vbTab & ":96a:" & vbTab & ":97:" & vbTab & ":97a:" & vbTab & ":98:" & vbTab & ":98a:" & vbTab & ":99:" & vbTab & ":99a:" & vbTab & ":100:" & vbTab & ":100a:" & vbTab & ":101:" & vbTab & ":101a" & vbTab & ":102:" & vbTab & ":102a:" & vbTab & ":103:" & vbTab & ":103a" & vbTab & ":104:" & vbTab & ":104a" & vbTab & ":105:" & vbTab & ":105a" & vbTab & ":106:" & vbTab & ":106a:" & vbTab & ":107:" & vbTab & ":107a" & vbTab & ":108:" & vbTab & ":108a:" & vbTab & ":109:" & vbTab & ":109a:" & vbTab & ":110:" & vbTab & ":110a:" & vbTab & ":111:" & vbTab & ":111a:" & vbTab & ":112:" & vbTab & ":112a:" & vbTab & ":113:" & vbTab & ":113a:" & vbTab & ":114:" & vbTab & ":114a:" & vbTab & ":115:" & vbTab & ":115a:" & vbTab & ":116:" & vbTab & ":116a:" & vbTab & ":117:" & vbTab & ":117a:" & vbTab & ":118:" & vbTab & ":118a:" & vbTab & ":119:" & vbTab & ":119a:"
        Resultfeild = Resultfeild + vbTab & ":120:" & vbTab & ":120a:" & vbTab & ":121:" & vbTab & ":121a:" & vbTab & ":122:" & vbTab & ":122a:" & vbTab & ":123:" & vbTab & ":123a:" & vbTab & ":124:" & vbTab & ":124a:" & vbTab & ":125:" & vbTab & ":125a:" & vbTab & ":126:" & vbTab & ":126a:" & vbTab & ":127:" & vbTab & ":127a:" & vbTab & ":128:" & vbTab & ":128a:" & vbTab & ":129:" & vbTab & ":129a:" & vbTab & ":130:" & vbTab & ":130a:" & vbTab & ":131:" & vbTab & ":131a:" & vbTab & ":132:" & vbTab & ":132a:" & vbTab & ":133:" & vbTab & ":133a:" & vbTab & ":134:" & vbTab & ":134a:" & vbTab & ":135:" & vbTab & ":136:" & vbTab & ":137:" & vbTab & ":138:" & vbTab & ":139:" & vbTab & ":140:" & vbTab & ":141:" & vbTab & ":142:" & vbTab & ":143:" & vbTab & ":144:" & vbTab & ":145:" & vbTab & ":146:"
        Resultfeild = Resultfeild + vbTab & ":147:" & vbTab & ":148:" & vbTab & ":149:" & vbTab & ":150:" & vbTab & ":151:" & vbTab & ":152:" & vbTab & ":153:" & vbTab & ":154:" & vbTab & ":155:" & vbTab & ":156:" & vbTab & ":157:" & vbTab & ":158:" & vbTab & ":159:" & vbTab & ":160:" & vbTab & ":161:" & vbTab & ":162:" & vbTab & ":163:" & vbTab & ":164:" & vbTab & ":165:" & vbTab & ":166:" & vbTab & ":167:" & vbTab & ":168:" & vbTab & ":169:" & vbTab & ":170:" & vbTab & ":171:" & vbTab & ":172:" & vbTab & ":173:"

End Sub

Public Function GetRxItem(RefId As Integer, RxItem As String) As Boolean
Dim Temp As Integer
GetRxItem = False
RxItem = ""
Temp = RefId
If (Temp > 0) Then
    If (Temp <= lstRxs.ListCount - 1) And (lstRxs.ListCount > 0) Then
        RxItem = lstRxs.List(Temp - 1)
        GetRxItem = True
    Else
        Temp = Temp - (lstRxs.ListCount - 1)
        If (Temp <= lstNonOcular.ListCount - 1) Then
            RxItem = lstNonOcular.List(Temp - 1)
            GetRxItem = True
        End If
    End If
End If
End Function

Public Function PrintActiveRxs(APrinter As String, NewOnly As Boolean) As Boolean
If (PatientId > 0) And (AppointmentId > 0) And (ActivityId > 0) Then
    If (LoadRxs(True)) Then
        Call DoDrugPrint(False, APrinter, "", False, NewOnly, True)
    End If
End If
End Function

Private Function PostToDB(ClnId As Long, IType As String, IDate As String, act As String, RxId As String) As Boolean
Dim i As Integer
Dim Cnt As Integer
Dim Temp As String
Dim AAct As String
Dim MyTemp As String
Dim UrTemp As String
Dim RetCln As PatientClinical
If (Trim(act) <> "") Then
    AAct = act
    If (Left(act, 2) <> "RX") Then
        AAct = "RX-8/" + act
    End If
    Set RetCln = New PatientClinical
    If (ClnId > 0) Then
        RetCln.ClinicalId = ClnId
        If (RetCln.RetrievePatientClinical) Then
            If (Trim(IDate) <> "") Then
                MyTemp = Trim(RetCln.ImageDescriptor)
                i = InStrPS(MyTemp, IDate)
                If (i > 2) Then
                    MyTemp = Trim(Left(MyTemp, i - 2) + Mid(MyTemp, i + 10, Len(MyTemp) - (i + 9)))
                Else
                    MyTemp = Trim(Mid(MyTemp, i + 10, Len(MyTemp) - (i + 9)))
                End If
                RetCln.ImageDescriptor = IType + IDate + MyTemp
            End If
            If (IType = "R") Then
                Cnt = Val(Trim(RetCln.ImageInstructions))
                Cnt = Cnt + 1
                RetCln.ImageInstructions = Trim(str(Cnt))
            ElseIf (IType = "C") Then
                If (EditPreviousOn) And Not (LatestAppointmentOn) Then
                    MyTemp = Trim(RetCln.Findings)
                    UrTemp = Trim(RetCln.ImageInstructions)
                    If (Len(UrTemp) > 10) Then
                        MyTemp = Trim(Mid(UrTemp, 11, Len(UrTemp) - 10))
                        UrTemp = Trim(Left(UrTemp, 10))
                    End If
                    RetCln.ImageInstructions = UrTemp + MyTemp
                End If
            ElseIf (IType = "") And (AAct <> Trim(RetCln.Findings)) Then
                If EditPreviousOn Then
                    If Not (LatestAppointmentOn) Then
                        MyTemp = Trim(RetCln.Findings)
                        UrTemp = Trim(RetCln.ImageInstructions)
                        If (Len(UrTemp) > 10) Then
                            MyTemp = Trim(Mid(UrTemp, 11, Len(UrTemp) - 10))
                            UrTemp = Trim(Left(UrTemp, 10))
                        End If
                        RetCln.ImageInstructions = UrTemp + MyTemp
                    Else
                        
                    End If
                End If
            End If
            RetCln.Findings = AAct
            Call RetCln.ApplyPatientClinical
        End If
    Else
        RetCln.ClinicalId = 0
        If (RetCln.RetrievePatientClinical) Then
            RetCln.PatientId = PatientId
            RetCln.AppointmentId = AppointmentId
            RetCln.ClinicalType = "A"
            RetCln.ClinicalStatus = "A"
            RetCln.EyeContext = ""
            RetCln.Symptom = "99"
            RetCln.Findings = AAct
            If IDate = "" Then
                Temp = BringForwardEPVNewDrugs(PatientId, AppointmentId)
            Else
                Temp = IType & IDate
            End If
            RetCln.ImageDescriptor = Temp
            RetCln.ImageInstructions = ""
            RetCln.ClinicalDraw = frmSystems1.GetDrugId(AAct)
            Call RetCln.ApplyPatientClinical
        End If
    End If
    Set RetCln = Nothing
    PostToDB = True
End If
End Function

Private Function ConvertToApptDate(ClnId As Long) As String
Dim RetCln As PatientClinical
Dim RetAppt As SchedulerAppointment
ConvertToApptDate = ""
If (ClnId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.ClinicalId = ClnId
    If (RetCln.RetrievePatientClinical) Then
        Set RetAppt = New SchedulerAppointment
        RetAppt.AppointmentId = RetCln.AppointmentId
        If (RetAppt.RetrieveSchedulerAppointment) Then
            ConvertToApptDate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
        End If
        Set RetAppt = Nothing
    End If
    Set RetCln = Nothing
End If
End Function

Private Sub txtZoom_Click()
txtZoom.Visible = False
txtZoom.Text = ""
End Sub

Private Sub cmdMagnify_Click()
txtZoom.Text = txtNotes.Text
txtZoom.Visible = True
End Sub

Private Function LoadPreviousRx(PatId As Long, AList As ListBox, Init As Boolean) As Boolean
Dim i As Integer
Dim k As Integer
Dim j As Integer
Dim Temp As String
Dim TheDate As String
Dim TheStat As String
Dim DisplayText As String
Dim RetCln As PatientClinical
Dim RetAppt As SchedulerAppointment
LoadPreviousRx = False
lstBox.Clear
If (Init) Then
    AList.Clear
End If
If (PatId > 0) Then
    Set RetCln = New PatientClinical
    RetCln.PatientId = PatId
    RetCln.AppointmentId = 0
    RetCln.ClinicalType = "A"
    RetCln.EyeContext = ""
    RetCln.Findings = ""
    RetCln.Symptom = ""
    RetCln.ImageDescriptor = ""
    RetCln.ImageInstructions = ""
    If (RetCln.FindPatientClinical > 0) Then
        i = 1
        While (RetCln.SelectPatientClinical(i))
            If (Left(RetCln.Findings, 2) = "RX") Then
                k = InStrPS(RetCln.Findings, "-8/")
                If (k <= 0) Then
                    k = InStrPS(RetCln.Findings, "-2/")
                End If
                If (k > 0) Then
                    TheDate = Trim(Mid(RetCln.Findings, 3, (k - 1) - 2))
                    If (Trim(TheDate) = "") Then
                        Set RetAppt = New SchedulerAppointment
                        RetAppt.AppointmentId = RetCln.AppointmentId
                        If (RetAppt.RetrieveSchedulerAppointment) Then
                            TheDate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
                        End If
                        Set RetAppt = Nothing
                    End If
                    TheStat = "Active  "
                    If (Left(RetCln.ImageDescriptor, 1) = "R") Then
                        TheStat = "Refill  " + Trim(Mid(RetCln.ImageDescriptor, 2, 10))
                    ElseIf (Left(RetCln.ImageDescriptor, 1) = "D") Then
                        TheStat = "Disc    " + Trim(Mid(RetCln.ImageDescriptor, 2, 10))
                    ElseIf (Left(RetCln.ImageDescriptor, 1) = "C") Then
                        TheStat = "Rvisd   " + Trim(Mid(RetCln.ImageDescriptor, 2, 10))
                    ElseIf (Left(RetCln.ImageDescriptor, 1) = "P") Then
                        TheStat = "Pdisc   " + Trim(Mid(RetCln.ImageDescriptor, 2, 10))
                    ElseIf (Left(RetCln.ImageDescriptor, 1) = "X") Then
                        TheStat = "Deleted " + Trim(Mid(RetCln.ImageDescriptor, 2, 10))
                    ElseIf (Left(RetCln.ImageDescriptor, 1) = "K") Then
                        TheStat = "Cont    " + Trim(Mid(RetCln.ImageDescriptor, 2, 10))
                    End If
                    k = InStrPS(RetCln.Findings, "-1/")
                    j = InStrPS(RetCln.Findings, "-2/")
                    If (k >= j) Or (k = 0) Then
                        k = InStrPS(RetCln.Findings, "-8/")
                    End If
                    Temp = TheDate + " " + Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                    DisplayText = Space(240)
                    Mid(DisplayText, 1, Len(Temp)) = Temp
                    k = InStrPS(RetCln.Findings, "-2/")
                    j = InStrPS(RetCln.Findings, "-3/")
                    Temp = Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                    Mid(DisplayText, 35, Len(Temp)) = Temp
                    k = InStrPS(RetCln.Findings, "-3/")
                    j = InStrPS(RetCln.Findings, "-4/")
                    Temp = Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                    Mid(DisplayText, 50, Len(Temp)) = Temp
                    k = InStrPS(RetCln.Findings, "-4/")
                    j = InStrPS(RetCln.Findings, "-5/")
                    Temp = Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                    Mid(DisplayText, 61, Len(Temp)) = Temp
                    k = InStrPS(RetCln.Findings, "-5/")
                    j = InStrPS(RetCln.Findings, "-6/")
                    Temp = Trim(Mid(RetCln.Findings, k + 3, (j - 3) - k))
                    Mid(DisplayText, 69, Len(Temp)) = Temp
                    Mid(DisplayText, 78, Len(TheStat)) = TheStat
                    Mid(DisplayText, 225, 10) = Trim(str(RetCln.ClinicalId))
                    lstBox.AddItem DisplayText
                End If
            End If
            i = i + 1
        Wend
    End If
    Set RetCln = Nothing
    If (lstBox.ListCount > 0) Then
        For i = lstBox.ListCount - 1 To 0 Step -1
            AList.AddItem lstBox.List(i)
        Next i
    End If
End If
lstBox.Clear
If (AList.ListCount > 2) Then
    LoadPreviousRx = True
    lblHx.Visible = True
    lstHx.Visible = True
    cmdImpr.Visible = True
    cmdAnother.Visible = False
    cmdNotes.Visible = False
    cmdPrint.Visible = False
    cmdHx.Visible = False
    For i = 0 To MAX_DRUG_BUTTONS - 1
        cmdDrug(i).Visible = False
    Next i
    txtNotes.Visible = False
    cmdMore.Visible = False
    cmdMagnify.Visible = False
    lstNonOcular.Visible = False
    lstAllergy.Visible = False
    Label1.Visible = False
    Label2.Visible = False
    Label4.Visible = False
End If
End Function

Private Function AreYouSure() As Boolean
frmEventMsgs.Header = "Warning you are about to delete this drug from all visits !"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Continue"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    AreYouSure = True
End If
End Function

Private Function BringForwardEPVNewDrugs(PatId As Long, ApptId As Long) As String
Dim i As Integer
Dim Temp As String
Dim LDate As String
Dim MyDate As String
Dim RetApt As SchedulerAppointment
BringForwardEPVNewDrugs = ""
If (PatId > 0) And (ApptId > 0) Then
    Set RetApt = New SchedulerAppointment
    RetApt.AppointmentId = ApptId
    If (RetApt.RetrieveSchedulerAppointment) Then
        LDate = RetApt.AppointmentDate
    End If
    Set RetApt = Nothing
    
    Set RetApt = New SchedulerAppointment
    RetApt.AppointmentDate = LDate
    RetApt.AppointmentPatientId = PatId
    RetApt.AppointmentStatus = "DH"
    If (RetApt.FindAppointmentNextbyDate > 0) Then
        i = 1
        While (RetApt.SelectAppointment(i))
            If (RetApt.AppointmentId <> ApptId) And (InStrPS("DH", RetApt.AppointmentStatus) <> 0) And (RetApt.AppointmentComments <> "ADD VIA BILLING") Then
                MyDate = RetApt.AppointmentDate
                MyDate = Mid(MyDate, 5, 2) + "/" + Mid(MyDate, 7, 2) + "/" + Left(MyDate, 4)
                If (InStrPS(Temp, "!" + MyDate) = 0) Then
                    Temp = Temp + "!" + MyDate
                End If
            End If
            i = i + 1
        Wend
    End If
    Set RetApt = Nothing
    MyDate = Mid(LDate, 5, 2) + "/" + Mid(LDate, 7, 2) + "/" + Left(LDate, 4)
    If (InStrPS(Temp, "!" + MyDate) = 0) Then
        Temp = Temp + "!" + MyDate
    End If
    BringForwardEPVNewDrugs = Temp
End If
End Function

Public Sub setOutputStat(Rx As String)
Rx = Rx & frmRxDetails.OutputStat & "-9/"
End Sub

Public Function GetOutputStat(Rx As String) As Integer
Dim DrugName(1) As String
Dim mRx As String
Dim ClRx As String
Dim f(1) As Boolean
Dim i As Integer
i = InStrPS(1, Rx, vbNewLine)
DrugName(0) = Mid(Rx, 1, i - 1)

Dim s As Integer
s = 1
Do
    Call frmEvaluation.GetActionEvaluation(s, mRx, f(0), f(1), ClRx)
    If mRx = "" And ClRx = "" Then
        Exit Do
    Else
        If Not mRx = "" Then
            i = InStrPS(1, mRx, "-2/")
            If i > 6 Then
                DrugName(1) = Mid(mRx, 6, i - 6)
            End If
            If Not DrugName(1) = "" Then
                If Mid(DrugName(0), 1, Len(DrugName(1))) = DrugName(1) Then
                    Exit Do
                End If
            End If
        End If
    End If
    s = s + 1
Loop



i = InStrPS(1, mRx, "-9/")
If i > 0 Then
    GetOutputStat = Mid(mRx, i - 1, 1)
Else
    GetOutputStat = 0
End If
End Function

Public Function ChangeOutputStat(oridStatus As String, NewStatus As String) As Integer
Dim mRx As String
Dim ClRx As String
Dim i As Integer
Dim s As Integer
s = 1
Do
    Call frmEvaluation.GetActionEvaluation(s, mRx, False, False, ClRx)
    If mRx = "" And ClRx = "" Then
        Exit Do
    Else
        If Not mRx = "" Then
            i = InStrPS(1, mRx, "-9/")
            If i > 0 Then
                If Mid(mRx, i - 1, 1) = oridStatus Then
                    Call frmEvaluation.UpdateActionStatus(s, NewStatus)
                End If
            End If
        End If
    End If
    s = s + 1
Loop
End Function

Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
End Function


