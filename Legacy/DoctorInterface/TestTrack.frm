VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmTestTrack 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00817C30&
   LinkTopic       =   "Form1"
   Picture         =   "TestTrack.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdFind11 
      Height          =   1095
      Left            =   3120
      TabIndex        =   25
      Top             =   4200
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind1 
      Height          =   1095
      Left            =   240
      TabIndex        =   17
      Top             =   600
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":38AD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10200
      TabIndex        =   0
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":3A90
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression5 
      Height          =   1095
      Left            =   6000
      TabIndex        =   1
      Top             =   5400
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":3CC3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression4 
      Height          =   1095
      Left            =   6000
      TabIndex        =   2
      Top             =   4200
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":3EA9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression3 
      Height          =   1095
      Left            =   6000
      TabIndex        =   3
      Top             =   3000
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":408F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression2 
      Height          =   1095
      Left            =   6000
      TabIndex        =   4
      Top             =   1800
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":4275
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression1 
      Height          =   1095
      Left            =   6000
      TabIndex        =   5
      Top             =   600
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":445B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression10 
      Height          =   1095
      Left            =   9000
      TabIndex        =   6
      Top             =   5400
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":4641
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression9 
      Height          =   1095
      Left            =   9000
      TabIndex        =   7
      Top             =   4200
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":4828
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression8 
      Height          =   1095
      Left            =   9000
      TabIndex        =   8
      Top             =   3000
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":4A0E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression7 
      Height          =   1095
      Left            =   9000
      TabIndex        =   9
      Top             =   1800
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":4BF4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpression6 
      Height          =   1095
      Left            =   9000
      TabIndex        =   10
      Top             =   600
      Visible         =   0   'False
      Width           =   2895
      _Version        =   131072
      _ExtentX        =   5106
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":4DDA
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreImpr 
      Height          =   1095
      Left            =   6000
      TabIndex        =   11
      Top             =   6600
      Width           =   5895
      _Version        =   131072
      _ExtentX        =   10398
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":4FC0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind5 
      Height          =   1095
      Left            =   240
      TabIndex        =   13
      Top             =   5400
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":51AB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind4 
      Height          =   1095
      Left            =   240
      TabIndex        =   14
      Top             =   4200
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":538E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind3 
      Height          =   1095
      Left            =   240
      TabIndex        =   15
      Top             =   3000
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":5571
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind2 
      Height          =   1095
      Left            =   240
      TabIndex        =   16
      Top             =   1800
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":5754
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind10 
      Height          =   1095
      Left            =   3120
      TabIndex        =   18
      Top             =   3000
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":5937
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind9 
      Height          =   1095
      Left            =   3120
      TabIndex        =   19
      Top             =   1800
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":5B1B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind8 
      Height          =   1095
      Left            =   3120
      TabIndex        =   20
      Top             =   600
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":5CFE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind7 
      Height          =   1095
      Left            =   240
      TabIndex        =   21
      Top             =   7800
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":5EE1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind6 
      Height          =   1095
      Left            =   240
      TabIndex        =   22
      Top             =   6600
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":60C4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreFind 
      Height          =   1095
      Left            =   3120
      TabIndex        =   23
      Top             =   7800
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":62A7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind12 
      Height          =   1095
      Left            =   3120
      TabIndex        =   24
      Top             =   5400
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":648F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdFind13 
      Height          =   1095
      Left            =   3120
      TabIndex        =   26
      Top             =   6600
      Visible         =   0   'False
      Width           =   2775
      _Version        =   131072
      _ExtentX        =   4895
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "TestTrack.frx":6673
   End
   Begin VB.Label lblImpr 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Select Findings"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   240
      TabIndex        =   12
      Top             =   120
      Width           =   2115
   End
End
Attribute VB_Name = "frmTestTrack"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public EyeContext As String
Public AppointmentId As Long

Private Const InActiveRef As String = "$"
Private Const QntRef As String = "["
Private Const QntRefEnd As String = "]"
Private Const CntRef As String = "-NF"
Private Const CntRefSet As String = "!"
Private Const RORef As String = "-RO"
Private Const RORefSet As String = "#"
Private Const HORef As String = "-HO"
Private Const HORefSet As String = "%"

Private Const MaxImpressions As Integer = 100
Private Const MaxImpressionsAllowed As Integer = 100
Private Const MaxFindings As Integer = 50
Private Const MaxFindingsAllowed As Integer = 50

Private Type ExternalReference
    Id As String
    Name As String
    Eye As String
    Quant As String
    Results As String
    Active As Boolean
    IgnoreEye As Boolean
    RuleOutOn As Boolean
    HistoryOfOn As Boolean
    NegativeFinding As Boolean
End Type

Private FormId As Long
Private SelectedImpressions(MaxImpressionsAllowed * 2) As ExternalReference
Private CurrentImpIndex As Integer
Private TotalAvailableImpressions As Integer
Private SelectedFindings(MaxFindingsAllowed * 2) As ExternalReference
Private CurrentFndIndex As Integer
Private TotalAvailableFindings As Integer
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Public Function LoadTestTrack(AFormId As Long) As Boolean
LoadTestTrack = False
If (AFormId > 0) Then
    FormId = AFormId
    Call LoadImpressions(True)
    LoadTestTrack = LoadFindings(True)
End If
End Function

Private Sub ClearImpr()
cmdImpression1.Text = ""
cmdImpression1.Tag = ""
cmdImpression1.Visible = False
cmdImpression1.BackColor = BaseBackColor
cmdImpression1.ForeColor = BaseTextColor
cmdImpression2.Text = ""
cmdImpression2.Tag = ""
cmdImpression2.Visible = False
cmdImpression2.BackColor = BaseBackColor
cmdImpression2.ForeColor = BaseTextColor
cmdImpression3.Text = ""
cmdImpression3.Tag = ""
cmdImpression3.Visible = False
cmdImpression3.BackColor = BaseBackColor
cmdImpression3.ForeColor = BaseTextColor
cmdImpression4.Text = ""
cmdImpression4.Tag = ""
cmdImpression4.Visible = False
cmdImpression4.BackColor = BaseBackColor
cmdImpression4.ForeColor = BaseTextColor
cmdImpression5.Text = ""
cmdImpression5.Tag = ""
cmdImpression5.Visible = False
cmdImpression5.BackColor = BaseBackColor
cmdImpression5.ForeColor = BaseTextColor
cmdImpression6.Text = ""
cmdImpression6.Tag = ""
cmdImpression6.Visible = False
cmdImpression6.BackColor = BaseBackColor
cmdImpression6.ForeColor = BaseTextColor
cmdImpression7.Text = ""
cmdImpression7.Tag = ""
cmdImpression7.Visible = False
cmdImpression7.BackColor = BaseBackColor
cmdImpression7.ForeColor = BaseTextColor
cmdImpression8.Text = ""
cmdImpression8.Tag = ""
cmdImpression8.Visible = False
cmdImpression8.BackColor = BaseBackColor
cmdImpression8.ForeColor = BaseTextColor
cmdImpression9.Text = ""
cmdImpression9.Tag = ""
cmdImpression9.Visible = False
cmdImpression9.BackColor = BaseBackColor
cmdImpression9.ForeColor = BaseTextColor
cmdImpression10.Text = ""
cmdImpression10.Tag = ""
cmdImpression10.Visible = False
cmdImpression10.BackColor = BaseBackColor
cmdImpression10.ForeColor = BaseTextColor
End Sub

Private Sub cmdImpression1_Click()
Call TriggerImpression(cmdImpression1)
End Sub

Private Sub cmdImpression2_Click()
Call TriggerImpression(cmdImpression2)
End Sub

Private Sub cmdImpression3_Click()
Call TriggerImpression(cmdImpression3)
End Sub

Private Sub cmdImpression4_Click()
Call TriggerImpression(cmdImpression4)
End Sub

Private Sub cmdImpression5_Click()
Call TriggerImpression(cmdImpression5)
End Sub

Private Sub cmdImpression6_Click()
Call TriggerImpression(cmdImpression6)
End Sub

Private Sub cmdImpression7_Click()
Call TriggerImpression(cmdImpression7)
End Sub

Private Sub cmdImpression8_Click()
Call TriggerImpression(cmdImpression8)
End Sub

Private Sub cmdImpression9_Click()
Call TriggerImpression(cmdImpression9)
End Sub

Private Sub cmdImpression10_Click()
Call TriggerImpression(cmdImpression10)
End Sub

Private Sub cmdMoreImpr_Click()
If (CurrentImpIndex >= TotalAvailableImpressions) Then
    CurrentImpIndex = 0
End If
CurrentImpIndex = CurrentImpIndex + 1
Call LoadImpressions(False)
End Sub

Private Sub ClearFind()
cmdFind1.Text = ""
cmdFind1.Tag = ""
cmdFind1.Visible = False
cmdFind1.BackColor = BaseBackColor
cmdFind1.ForeColor = BaseTextColor
cmdFind2.Text = ""
cmdFind2.Tag = ""
cmdFind2.Visible = False
cmdFind2.BackColor = BaseBackColor
cmdFind2.ForeColor = BaseTextColor
cmdFind3.Text = ""
cmdFind3.Tag = ""
cmdFind3.Visible = False
cmdFind3.BackColor = BaseBackColor
cmdFind3.ForeColor = BaseTextColor
cmdFind4.Text = ""
cmdFind4.Tag = ""
cmdFind4.Visible = False
cmdFind4.BackColor = BaseBackColor
cmdFind4.ForeColor = BaseTextColor
cmdFind5.Text = ""
cmdFind5.Tag = ""
cmdFind5.Visible = False
cmdFind5.BackColor = BaseBackColor
cmdFind5.ForeColor = BaseTextColor
cmdFind6.Text = ""
cmdFind6.Tag = ""
cmdFind6.Visible = False
cmdFind6.BackColor = BaseBackColor
cmdFind6.ForeColor = BaseTextColor
cmdFind7.Text = ""
cmdFind7.Tag = ""
cmdFind7.Visible = False
cmdFind7.BackColor = BaseBackColor
cmdFind7.ForeColor = BaseTextColor
cmdFind8.Text = ""
cmdFind8.Tag = ""
cmdFind8.Visible = False
cmdFind8.BackColor = BaseBackColor
cmdFind8.ForeColor = BaseTextColor
cmdFind9.Text = ""
cmdFind9.Tag = ""
cmdFind9.Visible = False
cmdFind9.BackColor = BaseBackColor
cmdFind9.ForeColor = BaseTextColor
cmdFind10.Text = ""
cmdFind10.Tag = ""
cmdFind10.Visible = False
cmdFind10.BackColor = BaseBackColor
cmdFind10.ForeColor = BaseTextColor
cmdFind11.Text = ""
cmdFind11.Tag = ""
cmdFind11.Visible = False
cmdFind11.BackColor = BaseBackColor
cmdFind11.ForeColor = BaseTextColor
cmdFind12.Text = ""
cmdFind12.Tag = ""
cmdFind12.Visible = False
cmdFind12.BackColor = BaseBackColor
cmdFind12.ForeColor = BaseTextColor
cmdFind13.Text = ""
cmdFind13.Tag = ""
cmdFind13.Visible = False
cmdFind13.BackColor = BaseBackColor
cmdFind13.ForeColor = BaseTextColor
End Sub

Private Sub cmdFind1_Click()
Call TriggerFinding(cmdFind1)
End Sub

Private Sub cmdFind2_Click()
Call TriggerFinding(cmdFind2)
End Sub

Private Sub cmdFind3_Click()
Call TriggerFinding(cmdFind3)
End Sub

Private Sub cmdFind4_Click()
Call TriggerFinding(cmdFind4)
End Sub

Private Sub cmdFind5_Click()
Call TriggerFinding(cmdFind5)
End Sub

Private Sub cmdFind6_Click()
Call TriggerFinding(cmdFind6)
End Sub

Private Sub cmdFind7_Click()
Call TriggerFinding(cmdFind7)
End Sub

Private Sub cmdFind8_Click()
Call TriggerFinding(cmdFind8)
End Sub

Private Sub cmdFind9_Click()
Call TriggerFinding(cmdFind9)
End Sub

Private Sub cmdFind10_Click()
Call TriggerFinding(cmdFind10)
End Sub

Private Sub cmdFind11_Click()
Call TriggerFinding(cmdFind11)
End Sub

Private Sub cmdFind12_Click()
Call TriggerFinding(cmdFind12)
End Sub

Private Sub cmdFind13_Click()
Call TriggerFinding(cmdFind13)
End Sub

Private Sub cmdMoreFind_Click()
If (CurrentFndIndex >= TotalAvailableFindings) Then
    CurrentFndIndex = 0
End If
CurrentFndIndex = CurrentFndIndex + 1
Call LoadFindings(False)
End Sub

Private Sub cmdDone_Click()
Unload frmTestTrack
End Sub

Private Function LoadImpressions(Init As Boolean) As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
LoadImpressions = False
Call ClearImpr
If (Init) Then
    TotalAvailableImpressions = 0
    CurrentImpIndex = 1
    Call InitializeImpressions
    If (frmHome.FollowUpOn) Then
        Call LoadAvailableImpressionsDisk
    Else
        Call LoadAvailableImpressions
    End If
End If
i = CurrentImpIndex
ButtonCnt = 1
While (i <= TotalAvailableImpressions) And (ButtonCnt < 11)
    DoEvents
    If (SelectedImpressions(i).IgnoreEye) Then
        Call SetButtonImpr(ButtonCnt, SelectedImpressions(i).Id, Trim(SelectedImpressions(i).Name) + " ")
    Else
        Call SetButtonImpr(ButtonCnt, SelectedImpressions(i).Id, Trim(SelectedImpressions(i).Name) + " " + Trim(SelectedImpressions(i).Eye))
    End If
    ButtonCnt = ButtonCnt + 1
    i = i + 1
Wend
CurrentImpIndex = i - 1
LoadImpressions = True
cmdMoreImpr.Visible = False
If (CurrentImpIndex < TotalAvailableImpressions) Then
    cmdMoreImpr.Text = "More Impressions"
Else
    cmdMoreImpr.Text = "Beginning of Impressions List"
End If
If (TotalAvailableImpressions > 10) Then
    cmdMoreImpr.Visible = True
End If
End Function

Private Sub TriggerImpression(AButton As fpBtn)
Dim i As Integer
Dim AEye As String
If (AButton.BackColor = BaseBackColor) Then
    AEye = "OU"
    i = InStrPS(AButton.Text, "OS")
    If (i > 0) Then
        AEye = "OS"
    End If
    i = InStrPS(AButton.Text, "OD")
    If (i > 0) Then
        AEye = "OD"
    End If
    Call IsImprSetWhere(Trim(AButton.Tag), AEye, i)
    If (i > 0) Then
        frmEyeTypes.TheResults = ""
        frmEyeTypes.Show 1
        If (Trim(frmEyeTypes.TheResults) <> "") Then
            AButton.BackColor = ButtonSelectionColor
            AButton.ForeColor = TextSelectionColor
            SelectedImpressions(i).Active = True
            If (Trim(frmEyeTypes.TheResults) <> "N/A") Then
                SelectedImpressions(i).Results = Trim(frmEyeTypes.TheResults)
            End If
        End If
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    For i = 1 To CurrentImpIndex
        If (Trim(AButton.Tag) = Trim(SelectedImpressions(i).Id)) And (Mid(AButton.Text, Len(AButton.Text) - 1, 2) = SelectedImpressions(i).Eye) Then
            SelectedImpressions(i).Active = False
            SelectedImpressions(i).Results = ""
            Exit For
        End If
    Next i
End If
End Sub

Private Sub SetButtonImpr(Ref As Integer, TheId As String, TheDisplayName As String)
Dim i As Integer
Dim TheEye As String
If (Trim(TheDisplayName) = "") Then
    Exit Sub
End If
TheEye = Mid(TheDisplayName, Len(TheDisplayName) - 1, 2)
If (TheEye <> "OU") And (TheEye <> "OS") And (TheEye <> "OD") Then
    TheEye = ""
End If
If (Ref = 1) Then
    cmdImpression1.BackColor = BaseBackColor
    cmdImpression1.ForeColor = BaseTextColor
    cmdImpression1.Text = TheDisplayName
    cmdImpression1.Tag = TheId
    cmdImpression1.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression1.BackColor = ButtonSelectionColor
            cmdImpression1.ForeColor = TextSelectionColor
        End If
    End If
ElseIf (Ref = 2) Then
    cmdImpression2.BackColor = BaseBackColor
    cmdImpression2.ForeColor = BaseTextColor
    cmdImpression2.Text = TheDisplayName
    cmdImpression2.Tag = TheId
    cmdImpression2.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression2.BackColor = ButtonSelectionColor
            cmdImpression2.ForeColor = TextSelectionColor
        End If
    End If
ElseIf (Ref = 3) Then
    cmdImpression3.BackColor = BaseBackColor
    cmdImpression3.ForeColor = BaseTextColor
    cmdImpression3.Text = TheDisplayName
    cmdImpression3.Tag = TheId
    cmdImpression3.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression3.BackColor = ButtonSelectionColor
            cmdImpression3.ForeColor = TextSelectionColor
        End If
    End If
ElseIf (Ref = 4) Then
    cmdImpression4.BackColor = BaseBackColor
    cmdImpression4.ForeColor = BaseTextColor
    cmdImpression4.Text = TheDisplayName
    cmdImpression4.Tag = TheId
    cmdImpression4.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression4.BackColor = ButtonSelectionColor
            cmdImpression4.ForeColor = TextSelectionColor
        End If
    End If
ElseIf (Ref = 5) Then
    cmdImpression5.BackColor = BaseBackColor
    cmdImpression5.ForeColor = BaseTextColor
    cmdImpression5.Text = TheDisplayName
    cmdImpression5.Tag = TheId
    cmdImpression5.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression5.BackColor = ButtonSelectionColor
            cmdImpression5.ForeColor = TextSelectionColor
        End If
    End If
ElseIf (Ref = 6) Then
    cmdImpression6.BackColor = BaseBackColor
    cmdImpression6.ForeColor = BaseTextColor
    cmdImpression6.Text = TheDisplayName
    cmdImpression6.Tag = TheId
    cmdImpression6.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression6.BackColor = ButtonSelectionColor
            cmdImpression6.ForeColor = TextSelectionColor
        End If
    End If
ElseIf (Ref = 7) Then
    cmdImpression7.BackColor = BaseBackColor
    cmdImpression7.ForeColor = BaseTextColor
    cmdImpression7.Text = TheDisplayName
    cmdImpression7.Tag = TheId
    cmdImpression7.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression7.BackColor = ButtonSelectionColor
            cmdImpression7.ForeColor = TextSelectionColor
        End If
    End If
ElseIf (Ref = 8) Then
    cmdImpression8.BackColor = BaseBackColor
    cmdImpression8.ForeColor = BaseTextColor
    cmdImpression8.Text = TheDisplayName
    cmdImpression8.Tag = TheId
    cmdImpression8.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression8.BackColor = ButtonSelectionColor
            cmdImpression8.ForeColor = TextSelectionColor
        End If
    End If
ElseIf (Ref = 9) Then
    cmdImpression9.BackColor = BaseBackColor
    cmdImpression9.ForeColor = BaseTextColor
    cmdImpression9.Text = TheDisplayName
    cmdImpression9.Tag = TheId
    cmdImpression9.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression9.BackColor = ButtonSelectionColor
            cmdImpression9.ForeColor = TextSelectionColor
        End If
    End If
ElseIf (Ref = 10) Then
    cmdImpression10.BackColor = BaseBackColor
    cmdImpression10.ForeColor = BaseTextColor
    cmdImpression10.Text = TheDisplayName
    cmdImpression10.Tag = TheId
    cmdImpression10.Visible = True
    If (TheId = "N") Then
        TheId = "N-" + TheDisplayName
    End If
    i = IsImprSet(TheId, TheEye)
    If (i > 0) Then
        If (SelectedImpressions(i).Active) Then
            cmdImpression10.BackColor = ButtonSelectionColor
            cmdImpression10.ForeColor = TextSelectionColor
        End If
    End If
End If
End Sub

Private Function IsImprSet(TheId As String, Eye As String) As Integer
Dim i As Integer
IsImprSet = 0
If (Trim(TheId) <> "") Then
    For i = 1 To TotalImpressions
        If (Trim(SelectedImpressions(i).Id) = Trim(TheId)) And ((Trim(Eye) = SelectedImpressions(i).Eye) Or (Trim(Eye) = "")) Then
            IsImprSet = i
            Exit For
        End If
    Next i
End If
End Function

Private Function IsImprSetWhere(TheId As String, Eye As String, Loc As Integer) As Boolean
Dim i As Integer
Loc = 0
IsImprSetWhere = False
If (Trim(TheId) <> "") Then
    For i = 1 To TotalImpressions
        If (Trim(SelectedImpressions(i).Id) = Trim(TheId)) And ((Trim(Eye) = SelectedImpressions(i).Eye) Or (Trim(Eye) = "")) Then
            Loc = i
            IsImprSetWhere = True
            Exit For
        End If
    Next i
End If
End Function

Private Function InitializeImpressions() As Boolean
Dim i As Integer
InitializeImpressions = True
For i = 1 To MaxImpressions
    SelectedImpressions(i).Id = ""
    SelectedImpressions(i).Name = ""
    SelectedImpressions(i).Eye = ""
    SelectedImpressions(i).Results = ""
    SelectedImpressions(i).Active = False
    SelectedImpressions(i).IgnoreEye = False
    SelectedImpressions(i).NegativeFinding = False
    SelectedImpressions(i).RuleOutOn = False
    SelectedImpressions(i).HistoryOfOn = False
Next i
End Function

Private Function LoadFindings(Init As Boolean) As Boolean
Dim i As Integer
Dim ButtonCnt As Integer
LoadFindings = False
Call ClearFind
If (Init) Then
    TotalAvailableFindings = 0
    CurrentFndIndex = 1
    Call InitializeFindings
    Call LoadAvailableFindings(FormId)
End If
i = CurrentFndIndex
ButtonCnt = 1
While (i <= TotalAvailableFindings) And (ButtonCnt < 14)
    DoEvents
    If (SelectedFindings(i).IgnoreEye) Then
        Call SetButtonFind(ButtonCnt, SelectedFindings(i).Id, Trim(SelectedFindings(i).Name) + " ")
    Else
        Call SetButtonFind(ButtonCnt, SelectedFindings(i).Id, Trim(SelectedFindings(i).Name) + " " + Trim(SelectedFindings(i).Eye))
    End If
    ButtonCnt = ButtonCnt + 1
    i = i + 1
Wend
CurrentFndIndex = i - 1
LoadFindings = True
cmdMoreFind.Visible = False
If (CurrentFndIndex < TotalAvailableFindings) Then
    cmdMoreFind.Text = "More Findings"
Else
    cmdMoreFind.Text = "Beginning of Findings List"
End If
If (TotalAvailableFindings > 13) Then
    cmdMoreFind.Visible = True
End If
End Function

Private Sub TriggerFinding(AButton As fpBtn)
Dim i As Integer
If (AButton.BackColor = BaseBackColor) Then
    Call IsFindSetWhere(Trim(AButton.Tag), i)
    If (i > 0) Then
        frmEyeTypes.TheResults = ""
        frmEyeTypes.Show 1
        If (Trim(frmEyeTypes.TheResults) <> "") Then
            AButton.BackColor = ButtonSelectionColor
            AButton.ForeColor = TextSelectionColor
            SelectedFindings(i).Active = True
            If (Trim(frmEyeTypes.TheResults) <> "N/A") Then
                SelectedFindings(i).Results = Trim(frmEyeTypes.TheResults)
            End If
        End If
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    For i = 1 To CurrentFndIndex
        If (Trim(AButton.Tag) = Trim(SelectedFindings(i).Id)) Then
            SelectedFindings(i).Active = False
            SelectedFindings(i).Results = ""
            Exit For
        End If
    Next i
End If
End Sub

Private Sub SetButtonFind(Ref As Integer, TheId As String, TheDisplayName As String)
Dim TheEye As String
If (Trim(TheDisplayName) = "") Then
    Exit Sub
End If
TheEye = Mid(TheDisplayName, Len(TheDisplayName) - 1, 2)
If (TheEye <> "OU") And (TheEye <> "OS") And (TheEye <> "OD") Then
    TheEye = ""
End If
If (Ref = 1) Then
    cmdFind1.BackColor = BaseBackColor
    cmdFind1.ForeColor = BaseTextColor
    cmdFind1.Text = TheDisplayName
    cmdFind1.Tag = TheId
    cmdFind1.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind1.BackColor = ButtonSelectionColor
        cmdFind1.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 2) Then
    cmdFind2.BackColor = BaseBackColor
    cmdFind2.ForeColor = BaseTextColor
    cmdFind2.Text = TheDisplayName
    cmdFind2.Tag = TheId
    cmdFind2.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind2.BackColor = ButtonSelectionColor
        cmdFind2.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 3) Then
    cmdFind3.BackColor = BaseBackColor
    cmdFind3.ForeColor = BaseTextColor
    cmdFind3.Text = TheDisplayName
    cmdFind3.Tag = TheId
    cmdFind3.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind3.BackColor = ButtonSelectionColor
        cmdFind3.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 4) Then
    cmdFind4.BackColor = BaseBackColor
    cmdFind4.ForeColor = BaseTextColor
    cmdFind4.Text = TheDisplayName
    cmdFind4.Tag = TheId
    cmdFind4.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind4.BackColor = ButtonSelectionColor
        cmdFind4.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 5) Then
    cmdFind5.BackColor = BaseBackColor
    cmdFind5.ForeColor = BaseTextColor
    cmdFind5.Text = TheDisplayName
    cmdFind5.Tag = TheId
    cmdFind5.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind5.BackColor = ButtonSelectionColor
        cmdFind5.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 6) Then
    cmdFind6.BackColor = BaseBackColor
    cmdFind6.ForeColor = BaseTextColor
    cmdFind6.Text = TheDisplayName
    cmdFind6.Tag = TheId
    cmdFind6.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind6.BackColor = ButtonSelectionColor
        cmdFind6.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 7) Then
    cmdFind7.BackColor = BaseBackColor
    cmdFind7.ForeColor = BaseTextColor
    cmdFind7.Text = TheDisplayName
    cmdFind7.Tag = TheId
    cmdFind7.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind7.BackColor = ButtonSelectionColor
        cmdFind7.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 8) Then
    cmdFind8.BackColor = BaseBackColor
    cmdFind8.ForeColor = BaseTextColor
    cmdFind8.Text = TheDisplayName
    cmdFind8.Tag = TheId
    cmdFind8.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind8.BackColor = ButtonSelectionColor
        cmdFind8.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 9) Then
    cmdFind9.BackColor = BaseBackColor
    cmdFind9.ForeColor = BaseTextColor
    cmdFind9.Text = TheDisplayName
    cmdFind9.Tag = TheId
    cmdFind9.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind9.BackColor = ButtonSelectionColor
        cmdFind9.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 10) Then
    cmdFind10.BackColor = BaseBackColor
    cmdFind10.ForeColor = BaseTextColor
    cmdFind10.Text = TheDisplayName
    cmdFind10.Tag = TheId
    cmdFind10.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind10.BackColor = ButtonSelectionColor
        cmdFind10.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 11) Then
    cmdFind11.BackColor = BaseBackColor
    cmdFind11.ForeColor = BaseTextColor
    cmdFind11.Text = TheDisplayName
    cmdFind11.Tag = TheId
    cmdFind11.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind11.BackColor = ButtonSelectionColor
        cmdFind11.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 12) Then
    cmdFind12.BackColor = BaseBackColor
    cmdFind12.ForeColor = BaseTextColor
    cmdFind12.Text = TheDisplayName
    cmdFind12.Tag = TheId
    cmdFind12.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind12.BackColor = ButtonSelectionColor
        cmdFind12.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 13) Then
    cmdFind13.BackColor = BaseBackColor
    cmdFind13.ForeColor = BaseTextColor
    cmdFind13.Text = TheDisplayName
    cmdFind13.Tag = TheId
    cmdFind13.Visible = True
    If (SelectedFindings(Ref).Active) Then
        cmdFind13.BackColor = ButtonSelectionColor
        cmdFind13.ForeColor = TextSelectionColor
    End If
End If
End Sub

Private Function InitializeFindings() As Boolean
Dim i As Integer
InitializeFindings = True
For i = 1 To MaxFindings
    SelectedFindings(i).Id = ""
    SelectedFindings(i).Name = ""
    SelectedFindings(i).Eye = ""
    SelectedFindings(i).Results = ""
    SelectedFindings(i).Active = False
    SelectedFindings(i).IgnoreEye = False
    SelectedFindings(i).NegativeFinding = False
    SelectedFindings(i).RuleOutOn = False
    SelectedFindings(i).HistoryOfOn = False
Next i
End Function

Private Function IsFindSetWhere(TheId As String, Loc As Integer) As Boolean
Dim i As Integer
Loc = 0
IsFindSetWhere = False
If (Trim(TheId) <> "") Then
    For i = 1 To TotalFindings
        If (Trim(SelectedFindings(i).Id) = Trim(TheId)) Then
            Loc = i
            IsFindSetWhere = True
            Exit For
        End If
    Next i
End If
End Function

Public Function LoadAvailableImpressionsDisk() As Boolean
Dim i As Integer, j As Integer
Dim p As Integer, q As Integer
Dim AId As Long
Dim Temp As String, UTemp As String
Dim TheText As String, LDiag As String
Dim TheSystem As String, ZTemp As String
Dim RetrieveClinical As PatientClinical
j = 0
LoadAvailableImpressionsDisk = False
If (AppointmentId > 0) And (PatientId > 0) Then
    Set RetrieveClinical = New PatientClinical
    RetrieveClinical.PatientId = PatientId
    RetrieveClinical.AppointmentId = AppointmentId
    RetrieveClinical.ClinicalType = "Q"
    RetrieveClinical.EyeContext = ""
    RetrieveClinical.Symptom = ""
    RetrieveClinical.Findings = ""
    RetrieveClinical.ImageDescriptor = ""
    RetrieveClinical.ImageInstructions = ""
    If (RetrieveClinical.FindDoctorClinical > 0) Then
        i = 1
        While (RetrieveClinical.SelectPatientClinical(i))
            Temp = ""
            UTemp = Trim(RetrieveClinical.Symptom)
            LDiag = Trim(RetrieveClinical.Findings)
            TheSystem = ""
            TheText = ""
            ZTemp = ""
            If (InStrPS(UTemp, "!") <> 0) Then
                ZTemp = ZTemp + "NF-"
            End If
            If (InStrPS(UTemp, "%") <> 0) Then
                ZTemp = ZTemp + "HO-"
            End If
            If (InStrPS(UTemp, "#") <> 0) Then
                ZTemp = ZTemp + "RO-"
            End If
            If (InStrPS(UTemp, "[") <> 0) Then
                p = InStrPS(UTemp, "[")
                q = InStrPS(UTemp, "]")
                If (p > 0) And (q > 0) Then
                    ZTemp = ZTemp + Mid(UTemp, p, q - (p - 1))
                End If
            End If
            If (GetPrimaryDiagnosis(LDiag, TheSystem, TheText, False, AId)) Then
                If (RetrieveClinical.EyeContext = "OD") Or (RetrieveClinical.EyeContext = "OU") Then
                    j = j + 1
                    TotalAvailableImpressions = TotalAvailableImpressions + 1
                    SelectedImpressions(j).Id = LDiag
                    SelectedImpressions(j).Name = ZTemp + TheText
                    SelectedImpressions(j).Eye = "OD"
                    SelectedImpressions(j).Active = False
                    SelectedImpressions(j).IgnoreEye = False
                    SelectedImpressions(j).NegativeFinding = False
                    If (InStrPS(ZTemp, "NF-") <> 0) Then
                        SelectedImpressions(j).NegativeFinding = True
                    End If
                    If (InStrPS(ZTemp, "RO-") <> 0) Then
                        SelectedImpressions(j).RuleOutOn = True
                    End If
                    If (InStrPS(ZTemp, "HO-") <> 0) Then
                        SelectedImpressions(j).HistoryOfOn = True
                    End If
                End If
                If (RetrieveClinical.EyeContext = "OS") Or (RetrieveClinical.EyeContext = "OU") Then
                    j = j + 1
                    TotalAvailableImpressions = TotalAvailableImpressions + 1
                    SelectedImpressions(j).Id = LDiag
                    SelectedImpressions(j).Name = ZTemp + TheText
                    SelectedImpressions(j).Eye = "OS"
                    SelectedImpressions(j).Active = False
                    SelectedImpressions(j).IgnoreEye = False
                    SelectedImpressions(j).NegativeFinding = False
                    If (InStrPS(ZTemp, "NF-") <> 0) Then
                        SelectedImpressions(j).NegativeFinding = True
                    End If
                    If (InStrPS(ZTemp, "RO-") <> 0) Then
                        SelectedImpressions(j).RuleOutOn = True
                    End If
                    If (InStrPS(ZTemp, "HO-") <> 0) Then
                        SelectedImpressions(j).HistoryOfOn = True
                    End If
                End If
            End If
            i = i + 1
        Wend
    End If
    Set RetrieveClinical = Nothing
    LoadAvailableImpressionsDisk = True
End If
End Function

Private Sub LoadAvailableImpressions()
Dim i As Integer, j As Integer
Dim q As Integer, z As Integer
Dim SysId As Integer
Dim TheId As Long
Dim Rec As String, TheText As String
Dim RQuan As String
Dim RSys As String, RText As String
Dim RightDiagnosis As String
Dim LQuan As String
Dim LSys As String, LText As String
Dim LeftDiagnosis As String
Dim OtherText As String, GeneralImpressions As String
If (EyeContext = "") Then
    EyeContext = "OU"
End If
j = TotalAvailableImpressions
SysId = 1
i = 1
While (frmSystems1.GetSystemDiagnosis(i, RSys, RightDiagnosis, RText, RQuan, LSys, LeftDiagnosis, LText, LQuan))
    If (EyeContext = "OD") Or (EyeContext = "OU") Then
        OtherText = ""
        If (Left(RightDiagnosis, 1) = CntRefSet) Then
            OtherText = "NF-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Left(RightDiagnosis, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (Left(RightDiagnosis, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            RightDiagnosis = Mid(RightDiagnosis, 2, Len(RightDiagnosis) - 1)
        End If
        If (IsImprSet(RightDiagnosis, "OD") = 0) Then
            If (GetPrimaryDiagnosis(RightDiagnosis, RSys, TheText, False, TheId)) Then
                j = j + 1
                TotalAvailableImpressions = TotalAvailableImpressions + 1
                SelectedImpressions(j).Id = RightDiagnosis
                SelectedImpressions(j).Name = OtherText + TheText
                If (InStrPS(RText, "C/D:") > 0) Then
                    SelectedImpressions(j).Name = OtherText + RText
                End If
                SelectedImpressions(j).Eye = "OD"
                SelectedImpressions(j).Active = False
                SelectedImpressions(j).IgnoreEye = False
                SelectedImpressions(j).NegativeFinding = False
                If (InStrPS(OtherText, "NF-") <> 0) Then
                    SelectedImpressions(j).NegativeFinding = True
                End If
                If (InStrPS(OtherText, "RO-") <> 0) Then
                    SelectedImpressions(j).RuleOutOn = True
                End If
                If (InStrPS(OtherText, "HO-") <> 0) Then
                    SelectedImpressions(j).HistoryOfOn = True
                End If
            End If
        End If
    End If
    If (EyeContext = "OS") Or (EyeContext = "OU") Then
        OtherText = ""
        If (Left(LeftDiagnosis, 1) = CntRefSet) Then
            OtherText = "NF-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Left(LeftDiagnosis, 1) = HORefSet) Then
            OtherText = OtherText + "HO-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (Left(LeftDiagnosis, 1) = RORefSet) Then
            OtherText = OtherText + "RO-"
            LeftDiagnosis = Mid(LeftDiagnosis, 2, Len(LeftDiagnosis) - 1)
        End If
        If (IsImprSet(LeftDiagnosis, "OS") = 0) Then
            If (GetPrimaryDiagnosis(LeftDiagnosis, LSys, TheText, False, TheId)) Then
                j = j + 1
                TotalAvailableImpressions = TotalAvailableImpressions + 1
                SelectedImpressions(j).Id = LeftDiagnosis
                SelectedImpressions(j).Name = OtherText + TheText
                If (InStrPS(LText, "C/D:") > 0) Then
                    SelectedImpressions(j).Name = OtherText + LText
                End If
                SelectedImpressions(j).Eye = "OS"
                SelectedImpressions(j).Active = False
                SelectedImpressions(j).IgnoreEye = False
                SelectedImpressions(j).NegativeFinding = False
                If (InStrPS(OtherText, "NF-") <> 0) Then
                    SelectedImpressions(j).NegativeFinding = True
                End If
                If (InStrPS(OtherText, "RO-") <> 0) Then
                    SelectedImpressions(j).RuleOutOn = True
                End If
                If (InStrPS(OtherText, "HO-") <> 0) Then
                    SelectedImpressions(j).HistoryOfOn = True
                End If
            End If
        End If
    End If
    i = i + 1
Wend
GeneralImpressions = GetTestFileName("69A")
If (FM.IsFileThere(GeneralImpressions)) Then
    FM.OpenFile GeneralImpressions, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(109)
    Do Until (EOF(109))
        Line Input #109, Rec
        If (Mid(Rec, 10, 1) <> "/") And (Left(Rec, 6) <> "Recap=") Then
            q = InStrPS(Rec, "=")
            If (q > 0) Then
                z = InStrPS(q, Rec, " ")
                If (z > 0) Then
                    TheId = Val(Trim(Mid(Rec, q + 2, (z - 1) - (q + 1))))
                Else
                    TheId = Val(Trim(Mid(Rec, q + 2, 5)))
                End If
                If (RetrieveGeneralImpression(TheId, TheText)) Then
                    If (InStrPS(TheText, ":") = 0) Then
                        TotalAvailableImpressions = TotalAvailableImpressions + 1
                        j = TotalAvailableImpressions
                        SelectedImpressions(j).Id = Trim(str((-1) * TheId))
                        SelectedImpressions(j).Name = TheText
                        SelectedImpressions(j).Eye = "OU"
                        SelectedImpressions(j).Active = False
                        SelectedImpressions(j).IgnoreEye = True
                        SelectedImpressions(j).NegativeFinding = False
                        SelectedImpressions(j).RuleOutOn = False
                        SelectedImpressions(j).HistoryOfOn = False
                    End If
                End If
            End If
        End If
    Loop
    FM.CloseFile CLng(109)
End If
End Sub

Private Sub LoadAvailableFindings(FormId As Long)
Dim i As Integer
Dim RetDFC As DynamicControls
If (FormId > 0) Then
    Set RetDFC = New DynamicControls
    RetDFC.FormId = FormId
    If (RetDFC.FindControl > 0) Then
        i = 1
        While (RetDFC.SelectControl(i))
            If (RetDFC.ControlType = "T") Then
                TotalAvailableFindings = TotalAvailableFindings + 1
                SelectedFindings(TotalAvailableFindings).Id = Trim(str(RetDFC.ControlId))
                SelectedFindings(TotalAvailableFindings).Name = Trim(RetDFC.DoctorLingo)
                SelectedFindings(TotalAvailableFindings).Active = False
            End If
            i = i + 1
        Wend
    End If
    Set RetDFC = Nothing
End If
End Sub

Private Sub Form_Load()
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = &H9B9626
BaseTextColor = &HFFFFFF
End Sub

Private Function RetrieveGeneralImpression(RecId As Long, AText As String) As Boolean
Dim RetDF As DynamicControls
AText = ""
RetrieveGeneralImpression = False
If (RecId > 0) Then
    Set RetDF = New DynamicControls
    RetDF.ControlId = RecId
    If (RetDF.RetrieveControl) Then
        If (Trim(RetDF.DoctorLingo) <> "") Then
            AText = Trim(RetDF.DoctorLingo)
            Call StripCharacters(AText, "~")
            If (Trim(AText) <> "") Then
                RetrieveGeneralImpression = True
            End If
        End If
    End If
    Set RetDF = Nothing
End If
End Function

Private Function GetPrimaryDiagnosis(Id As String, ASys As String, TheName As String, BillOn As Boolean, AId As Long) As Boolean
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
GetPrimaryDiagnosis = False
TheName = ""
If (Id <> "") And (Len(Id) > 1) Then
    Set RetrieveDiagnosis = New DiagnosisMasterPrimary
    RetrieveDiagnosis.PrimarySystem = ASys
    RetrieveDiagnosis.PrimaryDiagnosis = ""
    RetrieveDiagnosis.PrimaryNextLevelDiagnosis = Id
    RetrieveDiagnosis.PrimaryLevel = 0
    RetrieveDiagnosis.PrimaryRank = 0
    RetrieveDiagnosis.PrimaryBilling = BillOn
    RetrieveDiagnosis.PrimaryDiscipline = ""
    If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
        If (RetrieveDiagnosis.SelectPrimary(1)) Then
            TheName = Trim(RetrieveDiagnosis.PrimaryLingo)
            If (Trim(TheName) = "") Then
                TheName = Trim(RetrieveDiagnosis.PrimaryName)
            End If
            AId = RetrieveDiagnosis.PrimaryId
            GetPrimaryDiagnosis = True
        End If
    Else
        RetrieveDiagnosis.PrimarySystem = ""
        If (RetrieveDiagnosis.FindPrimarybyDiagnosis > 0) Then
            If (RetrieveDiagnosis.SelectPrimary(1)) Then
                TheName = Trim(RetrieveDiagnosis.PrimaryLingo)
                If (Trim(TheName) = "") Then
                    TheName = Trim(RetrieveDiagnosis.PrimaryName)
                End If
                AId = RetrieveDiagnosis.PrimaryId
                GetPrimaryDiagnosis = True
            End If
        End If
    End If
    Set RetrieveDiagnosis = Nothing
End If
End Function

Public Function TotalImpressions() As Integer
Dim i As Integer
TotalImpressions = 0
For i = 1 To MaxImpressions
    If (Trim(SelectedImpressions(i).Id) = "") Then
        TotalImpressions = i - 1
        Exit For
    Else
        TotalImpressions = i
    End If
Next i
End Function

Public Function TotalFindings() As Integer
Dim i As Integer
TotalFindings = 0
For i = 1 To MaxFindings
    If (Trim(SelectedFindings(i).Id) = "") Then
        TotalFindings = i - 1
        Exit For
    Else
        TotalFindings = i
    End If
Next i
End Function

Public Function GetFindingValue(Item As Integer, TheResults As String) As Boolean
GetFindingValue = False
TheResults = ""
If (SelectedFindings(Item).Active) Then
    TheResults = Trim(SelectedFindings(Item).Name) + " " + Trim(SelectedFindings(Item).Results) + " "
    GetFindingValue = True
End If
End Function

Public Function GetImpressionValue(Item As Integer, TheResults As String) As Boolean
GetImpressionValue = False
TheResults = ""
If (SelectedImpressions(Item).Active) Then
    TheResults = Trim(SelectedImpressions(Item).Name) + " " + Trim(SelectedImpressions(Item).Results) + " "
    GetImpressionValue = True
End If
End Function

