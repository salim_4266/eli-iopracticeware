VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmClinicalHistory 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9450
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12375
   ForeColor       =   &H00AA6650&
   LinkTopic       =   "Form1"
   Picture         =   "ClinicalHistory.frx":0000
   ScaleHeight     =   9450
   ScaleWidth      =   12375
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lsthMeds 
      Height          =   450
      Left            =   0
      TabIndex        =   36
      Top             =   1680
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ListBox lstZoom 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5970
      ItemData        =   "ClinicalHistory.frx":36C9
      Left            =   1680
      List            =   "ClinicalHistory.frx":36D0
      TabIndex        =   33
      Top             =   1320
      Visible         =   0   'False
      Width           =   8415
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnifyMeds 
      Height          =   480
      Left            =   7080
      TabIndex        =   32
      Top             =   3840
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":36DD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnifyCC 
      Height          =   480
      Left            =   3240
      TabIndex        =   31
      Top             =   480
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":49A7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnifyOHX 
      Height          =   480
      Left            =   3240
      TabIndex        =   30
      Top             =   2400
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":5C71
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnifyFMX 
      Height          =   480
      Left            =   7080
      TabIndex        =   29
      Top             =   2400
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":6F3B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnifyALG 
      Height          =   480
      Left            =   3240
      TabIndex        =   28
      Top             =   3840
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":8205
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnifyMHX 
      Height          =   480
      Left            =   7080
      TabIndex        =   27
      Top             =   480
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":94CF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMagnifyHPI 
      Height          =   480
      Left            =   7680
      TabIndex        =   26
      Top             =   0
      Visible         =   0   'False
      Width           =   495
      _Version        =   131072
      _ExtentX        =   873
      _ExtentY        =   847
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":A799
   End
   Begin VB.ListBox lstTests 
      Appearance      =   0  'Flat
      BackColor       =   &H00EFEBE2&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   4350
      Left            =   7560
      TabIndex        =   23
      Top             =   480
      Width           =   4335
   End
   Begin VB.TextBox txtExamDate 
      Appearance      =   0  'Flat
      BackColor       =   &H00C19B49&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   390
      Left            =   7560
      MultiLine       =   -1  'True
      TabIndex        =   22
      Text            =   "ClinicalHistory.frx":BA63
      Top             =   5040
      Width           =   4335
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOSOnly 
      Height          =   1335
      Left            =   9720
      TabIndex        =   20
      Top             =   5400
      Visible         =   0   'False
      Width           =   2115
      _Version        =   131072
      _ExtentX        =   3731
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":BA73
   End
   Begin VB.ListBox lstMeds 
      Appearance      =   0  'Flat
      BackColor       =   &H00F5F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   990
      ItemData        =   "ClinicalHistory.frx":BCC6
      Left            =   3720
      List            =   "ClinicalHistory.frx":BCCD
      TabIndex        =   16
      Top             =   3840
      Width           =   3855
   End
   Begin VB.ListBox lstFMX 
      Appearance      =   0  'Flat
      BackColor       =   &H00D9BE88&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1470
      ItemData        =   "ClinicalHistory.frx":BCDA
      Left            =   3720
      List            =   "ClinicalHistory.frx":BCE1
      TabIndex        =   15
      Top             =   2400
      Width           =   3855
   End
   Begin VB.ListBox lstOHX 
      Appearance      =   0  'Flat
      BackColor       =   &H00D9BE88&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1470
      IntegralHeight  =   0   'False
      ItemData        =   "ClinicalHistory.frx":BCED
      Left            =   120
      List            =   "ClinicalHistory.frx":BCF4
      TabIndex        =   14
      Top             =   2400
      Width           =   3615
   End
   Begin VB.ListBox lstALG 
      Appearance      =   0  'Flat
      BackColor       =   &H00F5F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   990
      ItemData        =   "ClinicalHistory.frx":BD00
      Left            =   120
      List            =   "ClinicalHistory.frx":BD07
      TabIndex        =   13
      Top             =   3840
      Width           =   3615
   End
   Begin VB.ListBox lstCC 
      Appearance      =   0  'Flat
      BackColor       =   &H00D9BE88&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1950
      IntegralHeight  =   0   'False
      ItemData        =   "ClinicalHistory.frx":BD13
      Left            =   120
      List            =   "ClinicalHistory.frx":BD1A
      TabIndex        =   12
      Top             =   480
      Width           =   3615
   End
   Begin VB.TextBox txtTime 
      Appearance      =   0  'Flat
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   10320
      TabIndex        =   7
      Top             =   120
      Width           =   1575
   End
   Begin VB.TextBox txtSummary 
      Appearance      =   0  'Flat
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   2295
   End
   Begin VB.ListBox lstMHX 
      Appearance      =   0  'Flat
      BackColor       =   &H00D9BE88&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1950
      ItemData        =   "ClinicalHistory.frx":BD25
      Left            =   3720
      List            =   "ClinicalHistory.frx":BD2C
      TabIndex        =   5
      Top             =   480
      Width           =   3855
   End
   Begin VB.ListBox lstHPI 
      Appearance      =   0  'Flat
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      IntegralHeight  =   0   'False
      ItemData        =   "ClinicalHistory.frx":BD38
      Left            =   4920
      List            =   "ClinicalHistory.frx":BD3F
      TabIndex        =   4
      Top             =   120
      Width           =   3255
   End
   Begin VB.TextBox txtSum1 
      Appearance      =   0  'Flat
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2400
      TabIndex        =   3
      Top             =   120
      Width           =   495
   End
   Begin VB.TextBox txtSum2 
      Appearance      =   0  'Flat
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   8160
      TabIndex        =   2
      Top             =   120
      Width           =   2175
   End
   Begin VB.TextBox txtGen 
      Appearance      =   0  'Flat
      BackColor       =   &H00444444&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   2880
      TabIndex        =   1
      Top             =   120
      Width           =   2055
   End
   Begin VB.ListBox lstOD 
      Appearance      =   0  'Flat
      BackColor       =   &H00EFEBE2&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2760
      ItemData        =   "ClinicalHistory.frx":BD4B
      Left            =   120
      List            =   "ClinicalHistory.frx":BD4D
      TabIndex        =   0
      Top             =   5280
      Width           =   3615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrevVisit 
      Height          =   975
      Left            =   120
      TabIndex        =   9
      Top             =   8040
      Width           =   2595
      _Version        =   131072
      _ExtentX        =   4577
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":BD4F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNone 
      Height          =   975
      Left            =   9240
      TabIndex        =   10
      Top             =   8040
      Width           =   2715
      _Version        =   131072
      _ExtentX        =   4789
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":BF90
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNextVisit 
      Height          =   975
      Left            =   2760
      TabIndex        =   11
      Top             =   8040
      Width           =   2595
      _Version        =   131072
      _ExtentX        =   4577
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":C1D5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImpr 
      Height          =   975
      Left            =   6480
      TabIndex        =   19
      Top             =   8040
      Width           =   2715
      _Version        =   131072
      _ExtentX        =   4789
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":C412
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdODOnly 
      Height          =   1335
      Left            =   7560
      TabIndex        =   21
      Top             =   5400
      Visible         =   0   'False
      Width           =   2115
      _Version        =   131072
      _ExtentX        =   3731
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":C659
   End
   Begin VB.ListBox lstOS 
      Appearance      =   0  'Flat
      BackColor       =   &H00EFEBE2&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2760
      ItemData        =   "ClinicalHistory.frx":C8AC
      Left            =   3720
      List            =   "ClinicalHistory.frx":C8AE
      TabIndex        =   8
      Top             =   5280
      Width           =   3735
   End
   Begin VB.ListBox lstSort 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFC0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2550
      ItemData        =   "ClinicalHistory.frx":C8B0
      Left            =   1920
      List            =   "ClinicalHistory.frx":C8B2
      Sorted          =   -1  'True
      TabIndex        =   24
      Top             =   5400
      Visible         =   0   'False
      Width           =   3615
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   975
      Left            =   5400
      TabIndex        =   34
      Top             =   8040
      Width           =   1035
      _Version        =   131072
      _ExtentX        =   1826
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":C8B4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOUOnly 
      Height          =   1335
      Left            =   9720
      TabIndex        =   35
      Top             =   6720
      Width           =   2115
      _Version        =   131072
      _ExtentX        =   3731
      _ExtentY        =   2355
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ClinicalHistory.frx":CAE8
   End
   Begin VB.Label lblMessage 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Clinical Posting"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   7560
      TabIndex        =   25
      Top             =   7560
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "OD"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   18
      Top             =   5040
      Width           =   285
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0FFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "OS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3720
      TabIndex        =   17
      Top             =   5040
      Width           =   270
   End
End
Attribute VB_Name = "frmClinicalHistory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ActivityId As Long
Public EditPreviousOn As Boolean
Public DxMoveOn As Boolean
Public EyeToBringForward As String
Public AppointmentToBringForward As Long

Private Type TestDetails
    TestTime As String
    TestParty As String
    TestText As String
    TestQuestion As String
    TestOrderNum As String
    TestMove As Boolean
    TestNoteOn As Boolean
End Type

Private Type ComplaintDetails
    TheId As Long
    TheText As String
    TheEye As String
    ComplaintDate As String
    CurComp As Boolean
    ApptId As Long
    HighlightOn As Boolean
    Ros As String * 1
End Type

Private Const InActiveRef As String = "$"
Private Const QntRef As String = "["
Private Const QntRefEnd As String = "]"
Private Const LocRef As String = "{"
Private Const LocRefEnd As String = "}"
Private Const CntRef As String = "-NF"
Private Const CntRefSet As String = "!"
Private Const RORef As String = "-RO"
Private Const RORefSet As String = "#"
Private Const HORef As String = "-HO"
Private Const HORefSet As String = "%"
Private Const EyeDilated = "+D "
Private Const EyeDilatedSet = "&"
Private Const MaxElements As Integer = 500
Private Const MaxSystems As Integer = 18
Private Const MaxTestResults As Integer = 200

Private PatientId As Long
Private ActiveActivityDate As String
Private ActiveActivityTime As String
Private EyeContext As String
Private AppointmentId As Long
Private QuestionSet As String
Private TheOrder As String

Private MaxPILines(10) As Integer
Private CurrentLine(10) As Integer
Private PILines(20, 50) As String
Private PILoaded(MaxElements) As String
Private CurrentPILoaded As Integer
Private TestResultsIndex As Integer
Private TestResultsStorage(MaxTestResults) As TestDetails

Private MaxComplaints As Integer
Private Complaints(MaxElements) As ComplaintDetails
Private ClinicalRecords As DI_ExamClinical
Private ClinicalRecordsNotes As DI_ExamClinical

Private Sub cmdImpr_Click()
frmImpression.ActivityId = ActivityId
frmImpression.PatientId = PatientId
frmImpression.AppointmentId = AppointmentId
If (frmImpression.LoadImpression(True)) Then
    frmImpression.Show 1
Else
    frmEventMsgs.Header = "No Impressions/Plans"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdMagnifyCC_Click()
lstZoom.List(0) = lstCC.List(0)
Call LoadPIList(lstZoom, 1, MaxPILines(1))
lstZoom.Visible = True
End Sub

Private Sub cmdMagnifyOHX_Click()
lstZoom.List(0) = lstOHX.List(0)
Call LoadPIList(lstZoom, 3, MaxPILines(3))
lstZoom.Visible = True
End Sub

Private Sub cmdMagnifyMHX_Click()
lstZoom.List(0) = lstMHX.List(0)
Call LoadPIList(lstZoom, 4, MaxPILines(4))
lstZoom.Visible = True
End Sub

Private Sub cmdMagnifyFMX_Click()
lstZoom.List(0) = lstFMX.List(0)
Call LoadPIList(lstZoom, 5, MaxPILines(5))
lstZoom.Visible = True
End Sub
'
'Private Sub cmdMagnifyMeds_Click()
'lstZoom.List(0) = lstMeds.List(0)
'Call LoadMeds(lstZoom, PatientId, AppointmentId)
'lstZoom.Visible = True
'End Sub

Private Sub cmdMagnifyALG_Click()
lstZoom.List(0) = lstALG.List(0)
Call LoadPIList(lstZoom, 9, MaxPILines(9))
lstZoom.Visible = True
End Sub

Private Sub cmdMagnifyHPI_Click()
lstZoom.List(0) = lstHPI.List(0)
Call LoadPIList(lstZoom, 10, MaxPILines(10))
lstZoom.Visible = True
End Sub

Private Sub cmdNotes_Click()
If Not UserLogin.HasPermission(epPatientNotes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.SystemReference = ""
frmNotes.EyeContext = ""
frmNotes.MaintainOn = False
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Private Sub lstZoom_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
lstZoom.Clear
lstZoom.Visible = False
End Sub

Private Sub cmdNextVisit_Click()
Dim RetrieveActivity As PracticeActivity
Set RetrieveActivity = New PracticeActivity
RetrieveActivity.ActivityDate = ActiveActivityDate
RetrieveActivity.ActivityPatientId = PatientId
RetrieveActivity.ActivityStatus = "DH"
If (RetrieveActivity.RetrieveNextActivity) Then
    ActivityId = RetrieveActivity.ActivityId
    Call LoadSummaryInfo(PatientId, 0)
Else
    frmEventMsgs.Header = "Current Visit is next"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Set RetrieveActivity = Nothing
End Sub

Private Sub cmdPrevVisit_Click()
Dim q As Integer
Dim TempDate As String
Dim RetrieveActivity As PracticeActivity
Set RetrieveActivity = New PracticeActivity
q = InStrPS(txtExamDate.Text, ":")
If (q < Len(txtExamDate.Text)) Then
    q = q + 1
    TempDate = Mid(txtExamDate.Text, q, Len(txtExamDate.Text) - (q - 1))
    TempDate = Mid(TempDate, 7, 4) + Mid(TempDate, 1, 2) + Mid(TempDate, 4, 2)
Else
    TempDate = ActiveActivityDate
End If
RetrieveActivity.ActivityId = ActivityId
RetrieveActivity.ActivityDate = TempDate
RetrieveActivity.ActivityPatientId = PatientId
RetrieveActivity.ActivityStatus = "DGH"
If (RetrieveActivity.RetrievePreviousActivity) Then
    TempDate = Mid(RetrieveActivity.ActivityDate, 5, 2) + "/" + Mid(RetrieveActivity.ActivityDate, 7, 2) + "/" + Left(RetrieveActivity.ActivityDate, 4)
    If (ActivityId <> RetrieveActivity.ActivityId) And _
       (ActiveActivityDate <> RetrieveActivity.ActivityDate) And _
       (ActiveActivityTime <> RetrieveActivity.ActivityStatusTime) Then
        ActivityId = RetrieveActivity.ActivityId
        Call LoadSummaryInfo(PatientId, 0)
    ElseIf (ActivityId <> RetrieveActivity.ActivityId) Then
        ActivityId = RetrieveActivity.ActivityId
        Call LoadSummaryInfo(PatientId, 0)
    Else
        frmEventMsgs.Header = "No further Previous Visits"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    frmEventMsgs.Header = "No further Previous Visits"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
Set RetrieveActivity = Nothing
End Sub

Private Sub cmdNone_Click()
Unload frmClinicalHistory
End Sub

Private Sub cmdOUOnly_Click()
Call TriggerIt(AppointmentId, "OU")
End Sub

Private Sub cmdODOnly_Click()
Call TriggerIt(AppointmentId, "OD")
End Sub

Private Sub cmdOSOnly_Click()
Call TriggerIt(AppointmentId, "OS")
End Sub

Private Function GetDiagnosisDirect(TheDiagnosis As Long, TheName As String) As Boolean
Dim RetrieveDiagnosis As DiagnosisMasterPrimary
GetDiagnosisDirect = False
TheName = ""
If (TheDiagnosis > 0) Then
    Set RetrieveDiagnosis = New DiagnosisMasterPrimary
    RetrieveDiagnosis.PrimaryId = TheDiagnosis
    If (RetrieveDiagnosis.RetrievePrimary) Then
        If (Trim(RetrieveDiagnosis.PrimaryShortName) <> "") Then
            TheName = Trim(RetrieveDiagnosis.PrimaryShortName)
        ElseIf (Trim(RetrieveDiagnosis.PrimaryLingo) <> "") Then
            TheName = Trim(RetrieveDiagnosis.PrimaryLingo)
        Else
            TheName = Trim(RetrieveDiagnosis.PrimaryName)
        End If
        GetDiagnosisDirect = True
    End If
    Set RetrieveDiagnosis = Nothing
End If
End Function

Private Function SetFamilybyControl(ControlId As Long, LevelType As Integer, TheQuestion As String, QuestionSet As String) As Integer
Dim TheFormId As Long
Dim AQuestion As String
Dim RetrieveForm1 As DynamicForms
Dim RetrieveForm2 As DynamicSecondaryForms
Dim RetrieveForm3 As DynamicThirdForms
Dim RetrieveForm4 As DynamicFourthForms
Dim RetrieveControl1 As DynamicControls
Dim RetrieveControl2 As DynamicSecondaryControls
Dim RetrieveControl3 As DynamicThirdControls
Dim RetrieveControl4 As DynamicFourthControls
SetFamilybyControl = 0
TheQuestion = ""
SetFamilybyControl = 0
If (LevelType = 1) Then
    Set RetrieveControl1 = New DynamicControls
    RetrieveControl1.ControlId = ControlId
    If (RetrieveControl1.RetrieveControl) Then
        TheFormId = RetrieveControl1.FormId
    End If
    Set RetrieveControl1 = Nothing
ElseIf (LevelType = 2) Then
    Set RetrieveForm1 = New DynamicForms
    Set RetrieveForm2 = New DynamicSecondaryForms
    Set RetrieveControl2 = New DynamicSecondaryControls
    RetrieveControl2.SecondaryControlId = ControlId
    If (RetrieveControl2.RetrieveControl) Then
        RetrieveForm2.SecondaryFormId = RetrieveControl2.SecondaryFormId
        If (RetrieveForm2.RetrieveForm) Then
            RetrieveForm1.FormId = RetrieveForm2.FormId
            If (RetrieveForm1.RetrieveForm) Then
                TheFormId = RetrieveForm4.FormId
            End If
        End If
    End If
    Set RetrieveForm2 = Nothing
    Set RetrieveControl2 = Nothing
ElseIf (LevelType = 3) Then
    Set RetrieveForm3 = New DynamicThirdForms
    Set RetrieveControl3 = New DynamicThirdControls
    RetrieveControl3.ThirdControlId = ControlId
    If (RetrieveControl3.RetrieveControl) Then
        RetrieveForm3.ThirdFormId = RetrieveControl3.ThirdFormId
        If (RetrieveForm3.RetrieveForm) Then
            RetrieveForm1.FormId = RetrieveForm3.FormId
            If (RetrieveForm1.RetrieveForm) Then
                TheFormId = RetrieveForm4.FormId
            End If
        End If
    End If
    Set RetrieveForm1 = Nothing
    Set RetrieveForm3 = Nothing
    Set RetrieveControl3 = Nothing
ElseIf (LevelType = 4) Then
    Set RetrieveForm4 = New DynamicFourthForms
    Set RetrieveControl4 = New DynamicFourthControls
    RetrieveControl4.FourthControlId = ControlId
    If (RetrieveControl4.RetrieveControl) Then
        RetrieveForm4.FourthFormId = RetrieveControl4.FourthFormId
        If (RetrieveForm4.RetrieveForm) Then
            TheFormId = RetrieveForm4.FormId
        End If
    End If
    Set RetrieveForm4 = Nothing
    Set RetrieveControl4 = Nothing
End If

If (TheFormId > 0) Then
    Set RetrieveForm1 = New DynamicForms
    RetrieveForm1.FormId = TheFormId
    If (RetrieveForm1.RetrieveForm) Then
        AQuestion = RetrieveForm1.Question
        SetFamilybyControl = SetFamilybyQuestion(AQuestion, QuestionSet)
    End If
    Set RetrieveForm1 = Nothing
End If
End Function

Private Function SetFamilybyQuestion(TheQuestion As String, TheQuestionSet As String) As Integer
Dim Family As Integer
Dim RetrieveQuestion As DynamicClass
SetFamilybyQuestion = 0
Family = 0
Set RetrieveQuestion = New DynamicClass
RetrieveQuestion.QuestionSet = ""
RetrieveQuestion.Question = Trim(TheQuestion)
If (RetrieveQuestion.RetrieveClassbyQuestion) Then
    Family = RetrieveQuestion.QuestionFamily
    If (Family = 2) Then
        Family = 3
    End If
Else
    Family = 0
End If
If (Family > 10) Then
    Family = 10
End If
SetFamilybyQuestion = Family
Set RetrieveQuestion = Nothing
End Function

Private Function LoadCurrentDiag(PatId As Long) As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Long
Dim f As Integer
Dim g As Integer
Dim MyCnt As Integer
Dim Qual As String
Dim ImgOn As String
Dim Active As String
Dim LDiag As String
Dim Temp1 As String
Dim SysRef As String
Dim TheText As String
Dim TheSystem As String
Dim DisplayText As String
Dim ABut As fpBtn
Dim DrawOk As Boolean
Dim XOn As Boolean
Dim YOn As Boolean
Dim DilOn As Boolean
Dim NegOn As Boolean
Dim HstOn As Boolean
Dim RulOn As Boolean
Dim RetrievalClinical As PatientClinical
LoadCurrentDiag = False
If (PatId < 1) Then
    Exit Function
End If
NegOn = False
RulOn = False
HstOn = False
DilOn = False
i = 1
Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
    If (ClinicalRecords.ClinicalType = "Q") And (ClinicalRecords.ClinicalStatus = "A") Then
        ImgOn = ""
        NegOn = False
        RulOn = False
        HstOn = False
        DilOn = False
        Temp1 = ""
        Active = " "
        DisplayText = Space(70)
        LDiag = Trim(ClinicalRecords.ClinicalFindings)
        EyeContext = Trim(ClinicalRecords.ClinicalEyeContext)
        If (EyeContext = "") Then
            EyeContext = "OU"
        End If
        TheSystem = Trim(ClinicalRecords.ClinicalDescriptor)
        If (Len(TheSystem) > 1) Then
            TheSystem = ""
        End If
        If (InStrPS(ClinicalRecords.ClinicalSymptom, InActiveRef) <> 0) Then
            Active = InActiveRef
        End If
        MyCnt = InStrPS(ClinicalRecords.ClinicalSymptom, CntRefSet)
        If (MyCnt <> 0) Then
            If (Mid(ClinicalRecords.ClinicalSymptom, MyCnt + 1, 1) <> "]") Then
                NegOn = True
            End If
        End If
        MyCnt = InStrPS(ClinicalRecords.ClinicalSymptom, RORefSet)
        If (MyCnt <> 0) Then
            If (Mid(ClinicalRecords.ClinicalSymptom, MyCnt + 1, 1) <> "]") Then
                RulOn = True
            End If
        End If
        MyCnt = InStrPS(ClinicalRecords.ClinicalSymptom, HORefSet)
        If (MyCnt <> 0) Then
            If (Mid(ClinicalRecords.ClinicalSymptom, MyCnt + 1, 1) <> "]") Then
                HstOn = True
            End If
            If (InStrPS(ClinicalRecords.ClinicalSymptom, "[") < MyCnt) And (InStrPS(ClinicalRecords.ClinicalSymptom, "]") > MyCnt) Then
                HstOn = False
            End If
        End If
        MyCnt = InStrPS(ClinicalRecords.ClinicalSymptom, EyeDilatedSet)
        If (MyCnt <> 0) Then
            If (Mid(ClinicalRecords.ClinicalSymptom, MyCnt + 1, 1) <> "]") Then
                DilOn = True
            End If
        End If
        Qual = ""
        f = InStrPS(ClinicalRecords.ClinicalSymptom, QntRef)
        g = InStrPS(ClinicalRecords.ClinicalSymptom, QntRefEnd)
        If (f <> 0) And (g <> 0) Then
            Qual = Mid(ClinicalRecords.ClinicalSymptom, f, g - (f - 1))
        End If
        If (Mid(LDiag, 2, 1) = "N") And (Len(LDiag) <= 10) Then
            TheSystem = Left(LDiag, 1)
            TheText = ""
        Else
            If (Len(LDiag) <= 10) Then
                Call frmSystems1.GetDiagnosis(TheSystem, LDiag, TheText, 0, 0, True, False, True, "", False, DrawOk)
                If (Trim(ClinicalRecords.ClinicalDraw) <> "") Then
                    ImgOn = "+"
                End If
            End If
        End If
        Temp1 = ""
        If (NegOn) Then
            Temp1 = Temp1 + CntRef
        End If
        If (DilOn) Then
            Temp1 = Temp1 + EyeDilated
        End If
        If (RulOn) Then
            Temp1 = Temp1 + RORef
        End If
        If (HstOn) Then
            Temp1 = Temp1 + HORef
        End If
        If (Trim(TheText) <> "") Then
            TheText = Qual + Temp1 + " " + TheText
        End If
        If (Trim(TheText) <> "") And (Len(LDiag) > 2) Then
            If Not (CheckSystemDiagnosis(TheSystem, EyeContext, LDiag)) Then
                If (TheSystem <> "") Then
                    Call ListDiagnosis(TheSystem, TheText, EyeContext, Active, ImgOn, LDiag)
                End If
            End If
        ElseIf (ClinicalRecords.ClinicalType = "R") Or (Mid(LDiag, 2, 1) = "N") Then
            Active = ""
            f = InStrPS(LDiag, ":")
            If (f > 0) Then
                If (Mid(LDiag, f + 1, 1) = "+") Then
                    Mid(LDiag, f + 1, 1) = " "
                End If
                Call ListDiagnosis(TheSystem, Mid(LDiag, f + 1, Len(LDiag) - f), EyeContext, Active, ImgOn, LDiag)
            ElseIf (Len(LDiag) < 3) Then
                If (TheSystem = "X") Then
                    Call frmHome.GetSystemDefaultText(19, TheText)
                ElseIf (TheSystem = "Y") Then
                    Call frmHome.GetSystemDefaultText(20, TheText)
                Else
                    Call frmHome.GetSystemDefaultText(Asc(TheSystem) - 64, TheText)
                End If
                Call ListDiagnosis(TheSystem, TheText, EyeContext, Active, ImgOn, LDiag)
            End If
        End If
    End If
    i = i + 1
Loop
Call PlantNotesLikeDiag(PatId, AppointmentId)
LoadCurrentDiag = True
Exit Function
UI_ErrorHandler:
    Call PinpointError("LoadCurrentDiag", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume LeaveFast
LeaveFast:
End Function

Private Function LoadExamInfo() As Boolean
On Error GoTo UI_ErrorHandler
Dim BalDue As Single
Dim i As Integer, Age As Integer
Dim PatientName As String, PatType As String
Dim Sex As String, Origin As String
Dim TheDate As String, TheTime As String
Dim LDate As String
Dim ApptType As String
Dim TheBalance As String, IName As String
Dim PCPDoctor As String, ReferralDoctor As String
Dim AppointmentVisitReason As String
Dim RetExamPat As DI_ExamPat
Dim oApptDateTime As New CIODateTime
Set RetExamPat = New DI_ExamPat
If (RetExamPat.RetrieveExamPatInfo(ActivityId)) Then
    ActiveActivityDate = RetExamPat.ActivityDate
    AppointmentId = RetExamPat.AppointmentId
    LDate = RetExamPat.ActivityDate
    PatientId = RetExamPat.PatientId
    QuestionSet = RetExamPat.QuestionSet
    ApptType = ""
    If (Trim(RetExamPat.MiddleInitial) <> "") Then
        PatientName = Trim(RetExamPat.FirstName) + " " _
                    + Trim(RetExamPat.MiddleInitial) + " " _
                    + Trim(RetExamPat.LastName) + " " + Trim(RetExamPat.NameReference)
    Else
        PatientName = Trim(RetExamPat.FirstName) + " " _
                    + Trim(Trim(RetExamPat.LastName) + " " + Trim(RetExamPat.NameReference))
    End If
    Age = GetAge(RetExamPat.BirthDate)
    Sex = RetExamPat.Gender
    Origin = RetExamPat.NationalOrigin
    PatType = Trim(RetExamPat.PatType)
    txtSummary.Text = PatientName + "," + str(Age)
    txtSum1.Text = Origin + ", " + Sex
'    txtSum4.Text = PatType
    PCPDoctor = ""
    AppointmentVisitReason = ""
    AppointmentId = RetExamPat.AppointmentId
    AppointmentVisitReason = Trim(RetExamPat.VisitReason)
    oApptDateTime.SetDateUnknown RetExamPat.AppDate
    oApptDateTime.SetTimeFromIO RetExamPat.AppTime
    PCPDoctor = Trim(RetExamPat.VendorName)
    ApptType = Trim(RetExamPat.AppointmentType)
    If (Trim(QuestionSet) = "") Then
        QuestionSet = RetExamPat.QuestionSet
    End If
    Call ReplaceCharacters(ApptType, ",", " ")
    txtSum2.Text = ApptType
    If (Trim(PCPDoctor) <> "") Then
        txtSum2.Text = Trim(txtSum2.Text) + ", " + Trim(PCPDoctor)
    End If
    txtTime.Text = oApptDateTime.GetDisplayDate(False) & " " & oApptDateTime.GetDisplayTime
    txtTime.Locked = True
    txtExamDate.Text = "This exam date:" + oApptDateTime.GetDisplayDate(False)
    txtExamDate.Locked = True
End If
Set RetExamPat = Nothing
txtSummary.Locked = True
txtSum1.Locked = True
txtSum2.Locked = True
txtGen.Locked = True
Set oApptDateTime = Nothing
LoadExamInfo = True
Exit Function
UI_ErrorHandler:
    Set RetExamPat = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Public Function LoadSummaryInfo(PatId As Long, ApptId As Long) As Boolean
Dim TestIt As Boolean
TestIt = False
LoadSummaryInfo = False
DxMoveOn = False
EyeToBringForward = ""
AppointmentToBringForward = 0
AppointmentId = 0
QuestionSet = ""
lstOS.Clear
lstOD.Clear
lstTests.Clear
lstCC.Clear
lstHPI.Clear
lstOHX.Clear
lstMHX.Clear
lstFMX.Clear
lstMeds.Clear
lstALG.Clear
CurrentPILoaded = 0
Erase PILoaded
TestResultsIndex = 0
Erase TestResultsStorage
MaxComplaints = 0
Erase Complaints
If (EditPreviousOn) Then
    cmdOSOnly.Enabled = False
    cmdODOnly.Enabled = False
End If
lblMessage.Caption = "Loading ..."
lblMessage.Visible = True
PatientId = PatId
DoEvents
If (LoadExamInfo) Then
    If (ActivityId < 1) Then
        AppointmentId = 0
    End If
    TestIt = LoadPrimaryInfo("")
End If
LoadSummaryInfo = True
lblMessage.Visible = False
End Function

Private Function ListDiagnosis(ASystem As String, AText As String, AEye As String, AActive As String, AImgOn As String, ADiag As String) As Boolean
Dim SysId As Long
Dim DText As String
Dim SysRef As String
Dim SysText As String
Dim LclText As String
Dim DefaultText As String
Dim DisplayText As String
If (AEye = "") Then
    AEye = "OU"
End If
SysRef = ""
SysId = Asc(ASystem) - 64
If (ASystem = "X") Then
    SysId = 19
ElseIf (ASystem = "Y") Then
    SysId = 20
End If
Call frmHome.SetSystemDefaultOn(SysId)
Call frmHome.GetSystemDefaultText(SysId, DefaultText)
If (SysId = 1) Then
    SysRef = frmSystems1.cmdA.Text
ElseIf (SysId = 2) Then
    SysRef = frmSystems1.cmdB.Text
ElseIf (SysId = 3) Then
    SysRef = frmSystems1.cmdC.Text
ElseIf (SysId = 4) Then
    SysRef = frmSystems1.cmdD.Text
ElseIf (SysId = 5) Then
    SysRef = frmSystems1.cmdE.Text
ElseIf (SysId = 6) Then
    SysRef = frmSystems1.cmdF.Text
ElseIf (SysId = 7) Then
    SysRef = frmSystems1.cmdG.Text
ElseIf (SysId = 8) Then
    SysRef = frmSystems1.cmdH.Text
ElseIf (SysId = 9) Then
    SysRef = frmSystems1.cmdI.Text
ElseIf (SysId = 10) Then
    SysRef = frmSystems1.cmdJ.Text
ElseIf (SysId = 11) Then
    SysRef = frmSystems1.cmdK.Text
ElseIf (SysId = 12) Then
    SysRef = frmSystems1.cmdL.Text
ElseIf (SysId = 13) Then
    SysRef = frmSystems1.cmdM.Text
ElseIf (SysId = 14) Then
    SysRef = frmSystems1.cmdN.Text
ElseIf (SysId = 15) Then
    SysRef = frmSystems1.cmdO.Text
ElseIf (SysId = 16) Then
    SysRef = frmSystems1.cmdP.Text
ElseIf (SysId = 17) Then
    SysRef = frmSystems1.cmdQ.Text
ElseIf (SysId = 18) Then
    SysRef = frmSystems1.cmdR.Text
ElseIf (SysId = 19) Then
    SysRef = frmSystems1.cmdSLE.Text
ElseIf (SysId = 20) Then
    SysRef = frmSystems1.cmdFundus.Text
End If
Call frmHome.GetSystemLabelAbbr(SysRef, SysText)
DText = Trim(SysText + ":" + DefaultText)
If (ADiag <> "") Then
    DisplayText = Space(70)
    LclText = SysText + ":" + AImgOn + AActive + AText
    Mid(DisplayText, 1, Len(LclText)) = LclText
    DisplayText = DisplayText + ASystem + ADiag
Else
    DisplayText = AText
End If

If (AEye = "OD") Or (AEye = "OU") Then
    If (DisplayText <> "") Then
        lstOD.AddItem DisplayText
    End If
End If
If (AEye = "OS") Or (AEye = "OU") Then
    If (DisplayText <> "") Then
        lstOS.AddItem DisplayText
    End If
End If
End Function

Private Function CheckSystemDiagnosis(TheSys As String, TheEye As String, ADiagnosis As String) As Boolean
Dim ASys As String
Dim ADiag As String
Dim k As Integer
CheckSystemDiagnosis = False
If (Trim(ADiagnosis) <> "") And (TheSys >= "A") And (TheSys <= "R") Then
    If (TheEye = "OD") Then
        For k = 0 To lstOD.ListCount - 1
            ASys = Mid(lstOD.List(k), 71, 1)
            If (ASys = TheSys) Then
                ADiag = Trim(Mid(lstOD.List(k), 72, Len(lstOD.List(k)) - 71))
                If (ADiag = Trim(ADiagnosis)) Then
                    CheckSystemDiagnosis = True
                    Exit For
                End If
            End If
        Next k
    ElseIf (TheEye = "OS") Then
        For k = 0 To lstOS.ListCount - 1
            ASys = Mid(lstOS.List(k), 71, 1)
            If (ASys = TheSys) Then
                ADiag = Trim(Mid(lstOS.List(k), 72, Len(lstOS.List(k)) - 71))
                If (ADiag = Trim(ADiagnosis)) Then
                    CheckSystemDiagnosis = True
                    Exit For
                End If
            End If
        Next k
    ElseIf (TheEye = "OU") Or (TheEye = "") Then
        If (TheEye = "") Then
            TheEye = "OU"
        End If
        For k = 0 To lstOD.ListCount - 1
            ASys = Mid(lstOD.List(k), 71, 1)
            If (ASys = TheSys) Then
                ADiag = Trim(Mid(lstOD.List(k), 72, Len(lstOD.List(k)) - 71))
                If (ADiag = Trim(ADiagnosis)) Then
                    CheckSystemDiagnosis = True
                    Exit For
                End If
            End If
        Next k
        For k = 0 To lstOS.ListCount - 1
            ASys = Mid(lstOS.List(k), 71, 1)
            If (ASys = TheSys) Then
                ADiag = Trim(Mid(lstOS.List(k), 72, Len(lstOS.List(k)) - 71))
                If (ADiag = Trim(ADiagnosis)) Then
                    CheckSystemDiagnosis = True
                    Exit For
                End If
            End If
        Next k
    End If
End If
End Function

Private Function SetTestFiles(PatId As Long) As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Long
Dim Rec As String
Dim Temp As String
Dim TheLingo As String
Dim TheText As String
Dim LocalText As String
Dim TestOn As Boolean
Dim TestFileNum As Integer
Dim CurrentTestName As String
Dim SchedulerTestFile As String
Dim RetrievalClinical As PatientClinical
SetTestFiles = False
TestFileNum = FreeFile
TestFileNum = TestFileNum + 100
SchedulerTestFile = DoctorInterfaceDirectory + Trim(str(PatId)) + "TestFile.txt"
i = 1
TestOn = False
CurrentTestName = ""
Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
    If (ClinicalRecords.ClinicalType = "F") Then
        If (Mid(ClinicalRecords.ClinicalSymptom, 1, 1) = "/") And _
            (CurrentTestName <> ClinicalRecords.ClinicalSymptom) And (TestOn) Then
            FM.CloseFile CLng(TestFileNum)
            Call PushList(SchedulerTestFile)
            Call ApplyTestResults(SchedulerTestFile, "", Temp, True)
            FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
            Print #TestFileNum, "QUESTION=" + Trim(ClinicalRecords.ClinicalSymptom)
            Print #TestFileNum, Trim(ClinicalRecords.ClinicalFindings) + " " + Trim(ClinicalRecords.ClinicalInstructions)
            CurrentTestName = ClinicalRecords.ClinicalSymptom
            TestOn = True
        ElseIf (Not TestOn) Then
            FM.OpenFile SchedulerTestFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(TestFileNum)
            Print #TestFileNum, "QUESTION=" + Trim(ClinicalRecords.ClinicalSymptom)
            Print #TestFileNum, Trim(ClinicalRecords.ClinicalFindings) + " " + Trim(ClinicalRecords.ClinicalInstructions)
            CurrentTestName = ClinicalRecords.ClinicalSymptom
            TestOn = True
        Else
            If (TestOn) Then
                Print #TestFileNum, Trim(ClinicalRecords.ClinicalFindings) + " " + Trim(ClinicalRecords.ClinicalInstructions)
            End If
        End If
    End If
    i = i + 1
Loop
If (TestOn) Then
    FM.CloseFile CLng(TestFileNum)
    Call PushList(SchedulerTestFile)
    Call ApplyTestResults(SchedulerTestFile, "", Temp, True)
End If
If (FM.IsFileThere(SchedulerTestFile)) Then
    FM.Kill SchedulerTestFile
End If
SetTestFiles = True
Exit Function
UI_ErrorHandler:
    SetTestFiles = True
    Call PinpointError("SetTestFiles", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume LeaveFast
LeaveFast:
End Function

Private Function ApplyTestResults(TheFileName As String, ATestId As String, ReturnString As String, PostResults As Boolean) As Boolean
Dim q As Integer
Dim FileNum As Integer
Dim Rec As String
Dim ATime As String
Dim TheText As String
Dim TheLingo As String
Dim PrevLine As String
Dim LocalText As String
Dim AQuestion As String
ApplyTestResults = False
ATime = ""
TheText = ""
ReturnString = ""
FileNum = FreeFile
If (FM.IsFileThere(TheFileName)) Then
    ApplyTestResults = True
    PrevLine = ""
    FM.OpenFile TheFileName, FileOpenMode.InputFileOpenMode, FileAccess.Read, CLng(FileNum)
    While (Not (EOF(FileNum)))
        LocalText = ""
        Line Input #FileNum, Rec
        If (Left(Rec, 10) = "QUESTION=/") Then
            AQuestion = Trim(Mid(Rec, 11, Len(Rec) - 10))
        End If
        If (Left(Rec, 5) = "TIME=") Then
            ATime = Trim(Mid(Rec, 6, Len(Rec) - 5))
        End If
        If (Left(Rec, 6) = "Recap=") Then
            LocalText = LocalText + Trim(Mid(Rec, 7, Len(Rec) - 6))
            q = InStrPS(LocalText, "/Time/")
            If (q <> 0) And (Trim(ATime) <> "") Then
                LocalText = Left(LocalText, q) + ATime + Mid(LocalText, q + 5, Len(LocalText) - (q + 4))
            End If
            Call ConvertButtonDisplay(LocalText, TheLingo)
            
            If (Len(TheText) > 0) And (Left(TheLingo, 1) = "%") Then
                q = InStrPS(TheLingo, ":")
                If (q <> 0) Then
                    TheLingo = Mid(TheLingo, q + 1, Len(TheLingo) - q)
                    TheText = TheText + Trim(TheLingo)
                Else
                    TheText = TheText + Chr(13) + Chr(10) + Trim(TheLingo)
                End If
            Else
                TheText = TheText + TheLingo
            End If
        End If
        PrevLine = TheLingo
    Wend
    If (Trim(TheText) <> "") And (PostResults) Then
        TestResultsIndex = TestResultsIndex + 1
        If (TestResultsIndex > MaxTestResults) Then
            TestResultsIndex = MaxTestResults
        End If
        TestResultsStorage(TestResultsIndex).TestOrderNum = ATestId
        TestResultsStorage(TestResultsIndex).TestQuestion = AQuestion
        TestResultsStorage(TestResultsIndex).TestText = TheText
    End If
    ReturnString = TheText
    FM.CloseFile CLng(FileNum)
End If
End Function

Private Sub PushList(FileName As String)
On Error GoTo UI_ErrorHandler
Dim TheRecord As String
Dim ODOn As Boolean
Dim DoRecapCheck As Boolean
Dim z1 As Integer, z2 As Integer
Dim w As Integer, s As Integer
Dim q As Integer, r As Integer
Dim a As Integer, k As Integer
Dim Y As Integer, z As Integer
Dim FileNum As Integer
Dim MultiTestIndicator As String
Dim PadValue As String
Dim Question As String
Dim TheValue As String
Dim TestTime As String
Dim LocalText As String
Dim LocalQuestion As String
Dim LocalTestName As String
Dim UTemp As String
Dim Recap(50) As String
Dim CurrentLevel As Integer
Dim Level1 As Integer
Dim TestNameLoc As Integer
Dim TestWideName As String
Dim WhereTestNameIs As Integer
Dim ControlLevel1 As DynamicControls
Level1 = 0
FileNum = 0
WhereTestNameIs = 0
Erase Recap
LocalTestName = ""
TestTime = ""
ODOn = False
DoRecapCheck = True
FileNum = FreeFile
FM.OpenFile FileName, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
Do Until (EOF(FileNum))
    Line Input #FileNum, TheRecord
    If (InStrPS(TheRecord, "QUESTION=") <> 0) Then
        Question = Trim(Mid(TheRecord, 10, Len(TheRecord) - 9))
        If (UCase(Trim(Question)) = "/GENERAL MEDICAL OBSERVATION") Then
            DoRecapCheck = False
        End If
        If (UCase(Trim(Question)) = "/GENERAL IMPRESSIONS") Then
            DoRecapCheck = False
        End If
        If (UCase(Trim(Question)) = "/BLOOD COUNT") Then
            DoRecapCheck = False
        End If
    ElseIf (InStrPS(TheRecord, "TIME=") <> 0) Then
        TestTime = Trim(Mid(TheRecord, 6, Len(TheRecord) - 5))
    Else
        r = InStrPS(TheRecord, "=")
        If (r = 0) Then
            r = Len(TheRecord) + 1
        End If
        r = r - 1
        w = r + 3
        Y = InStrPS(w, TheRecord, " ")
        If (Y = 0) Then
            Y = Len(TheRecord) + 1
        End If
        If (Val(Trim(Mid(TheRecord, w, Y - w))) > 0) Then
            Set ControlLevel1 = New DynamicControls
            ControlLevel1.ControlId = Val(Trim(Mid(TheRecord, w, Y - w)))
            ControlLevel1.ControlName = ""
            ControlLevel1.IEChoiceName = ""
            If (ControlLevel1.RetrieveControl) Then
                MultiTestIndicator = ""
                PadValue = ""
                TheValue = ""
                If (ControlLevel1.ControlType = "N") Or (ControlLevel1.ControlType = "S") Then
                    Y = InStrPS(w, TheRecord, "<")
                    If (Y <> 0) Then
                        z = InStrPS(w, TheRecord, ">")
                        If (z <> 0) Then
                            PadValue = "@" + Mid(TheRecord, Y + 1, (z - 1) - Y)
                        End If
                    End If
                ElseIf (ControlLevel1.ControlType = "R") Then
                    MultiTestIndicator = ""
                End If
                Level1 = Level1 + 1
                If (Trim(ControlLevel1.DoctorLingo) <> "") Then
                    Recap(Level1) = MultiTestIndicator + Trim(ControlLevel1.DoctorLingo) + " " + PadValue
                    Call GetPadValue(Recap(Level1), TheValue)
                    Call SetPadValue(Recap(Level1), TheValue)
                    s = InStrPS(Recap(Level1), "/Time/")
                    If (s > 0) Then
                        Recap(Level1) = Left(Recap(Level1), s) + TestTime + Mid(Recap(Level1), s + 5, Len(Recap(Level1)) - (s + 4))
                    End If
                    s = InStrPS(Recap(Level1), ":^OD")
                    z1 = InStrPS(Recap(Level1), ":/")
                    z2 = InStrPS(Recap(Level1), ":^")
                    If (s <> 0) Then
                        WhereTestNameIs = Level1
                        LocalTestName = Trim(Left(Recap(Level1), s - 1))
                        Call ReplaceCharacters(LocalTestName, "%", " ")
                        LocalTestName = Trim(LocalTestName)
                        ODOn = True
                    ElseIf (z1 <> 0) Then
                        WhereTestNameIs = Level1
                        LocalTestName = Trim(Left(Recap(Level1), z1 - 1))
                        s = InStrPS(Recap(Level1), "^OD")
                        If (s <> 0) Then
                            ODOn = True
                        End If
                    ElseIf (z2 <> 0) Then
                        If (Not (DoRecapCheck)) Then
                            WhereTestNameIs = Level1
                            LocalTestName = Trim(Left(Recap(Level1), z2 - 1))
                            ODOn = True
                        End If
                    Else
                        If (Not DoRecapCheck) Then
                            If (Trim(LocalTestName) = "") And (WhereTestNameIs = 0) Then
                                WhereTestNameIs = 1
                                LocalTestName = Trim(Mid(Question, 2, Len(Question) - 1)) + ":"
                            End If
                        End If
                    End If
                Else
                    Recap(Level1) = "-"
                End If
                If (DoRecapCheck) Then
                    If (Trim(LocalTestName) = "") Then
                        LocalTestName = Mid(Question, 2, Len(Question) - 1)
                        UTemp = LocalTestName
                        Call StripCharacters(UTemp, " ")
                        If (InStrPS(UCase(Recap(Level1)), UCase(LocalTestName)) = 0) And (InStrPS(UCase(Recap(Level1)), UCase(UTemp)) = 0) Then
                            Recap(Level1) = LocalTestName + ":" + Recap(Level1)
                            WhereTestNameIs = Level1
                        End If
                    End If
                    s = InStrPS(UCase(Recap(Level1)), UCase(LocalTestName))
                    If (s <> 0) And (Trim(LocalTestName) <> "") Then
                        s = InStrPS(Recap(Level1), "^OS")
                        If (s <> 0) And (ODOn) Then
                            Recap(Level1) = Mid(Recap(Level1), s, Len(Recap(Level1)) - (s - 1))
                        Else
                            If (WhereTestNameIs > 0) And (WhereTestNameIs <> Level1) Then
                                s = InStrPS(Recap(WhereTestNameIs), ":")
                                If (s <> 0) Then
                                    TestWideName = Trim(Recap(Level1))
                                    k = InStrPS(Recap(Level1), ":")
                                    If (k <> 0) Then
                                        TestWideName = Trim(Mid(Recap(Level1), s + 1, Len(Recap(Level1)) - s))
                                    End If
                                    If (InStrPS(Recap(WhereTestNameIs), TestWideName) = 0) Then
                                        Recap(WhereTestNameIs) = Left(Recap(WhereTestNameIs), s) + TestWideName + Mid(Recap(WhereTestNameIs), s + 1, Len(Recap(WhereTestNameIs)) - s)
                                    End If
                                    Recap(Level1) = "-"
                                End If
                            End If
                        End If
                    End If
                Else
                    s = InStrPS(UCase(Recap(WhereTestNameIs)), UCase(LocalTestName))
                    If (s = 0) Then
                        Recap(Level1) = LocalTestName + Recap(Level1)
                    End If
                End If
            End If
            Set ControlLevel1 = Nothing
        End If
    End If
Loop
FM.CloseFile CLng(FileNum)

' Build Recap Values
FM.OpenFile FileName, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
For s = 1 To Level1
    If (Trim(Recap(s)) <> "") And (Trim(Recap(s)) <> "-") Then
        Print #FileNum, "Recap=" + Recap(s)
    End If
Next s
FM.CloseFile CLng(FileNum)
Exit Sub
UI_ErrorHandler:
    If (FileNum <> 0) Then
        FM.CloseFile CLng(FileNum)
    End If
    Set ControlLevel1 = Nothing
    Resume LeaveFast
LeaveFast:
End Sub

Private Sub AddItemToList(AListbox As ListBox, AText As String)
Dim k As Integer
Dim Temp As String
Dim CurrentPos As Integer
CurrentPos = 1
k = InStrPS(CurrentPos, AText, Chr(13) + Chr(10))
While (k > 0)
    If (k <> CurrentPos) Then
        Temp = Mid(AText, CurrentPos, k - CurrentPos)
        Call ReplaceCharacters(Temp, Chr(13), " ")
        Call ReplaceCharacters(Temp, Chr(10), " ")
        If (Len(Trim(Temp)) > 2) Then
            AListbox.AddItem Temp
        End If
    End If
    CurrentPos = k + 2
    k = InStrPS(CurrentPos, AText, Chr(13) + Chr(10))
Wend
If (CurrentPos < Len(AText)) Then
    Temp = Mid(AText, CurrentPos, Len(AText) - (CurrentPos - 1))
    Call ReplaceCharacters(Temp, Chr(13), " ")
    Call ReplaceCharacters(Temp, Chr(10), " ")
    If (Len(Trim(Temp)) > 2) Then
        AListbox.AddItem Temp
    End If
End If
End Sub

Private Function GetTestResults(PatId As Long, ApptId As Long, AListbox As ListBox) As Boolean
Dim i As Integer
GetTestResults = False
If (PatId > 0) Then
    If (AppointmentId < 1) Then
        AppointmentId = ApptId
    End If
    AListbox.Clear
    AListbox.AddItem "Exam Elements"
    Erase TestResultsStorage
    TestResultsIndex = 0
    Call SetTestFiles(PatId)
    For i = 1 To TestResultsIndex
        If (Trim(TestResultsStorage(i).TestText) <> "") Then
            Call AddItemToList(AListbox, TestResultsStorage(i).TestText)
        End If
    Next i
    GetTestResults = True
End If
End Function

Private Function LoadTestNotes() As Boolean
Dim i As Integer
Dim TestId As String
Dim NoteTemp As String
LoadTestNotes = True
For i = 1 To MaxTestResults
    If (Trim(TestResultsStorage(i).TestOrderNum) <> "") Then
        TestId = Trim(TestResultsStorage(i).TestOrderNum)
        TestResultsStorage(i).TestNoteOn = GetNotesForTests(PatientId, AppointmentId, TestId, NoteTemp)
    End If
Next i
End Function

Private Function GetNotesForTests(PatId As Long, ApptId As Long, TstId As String, TheNote As String) As Boolean
Dim i As Integer
Dim RetNote As PatientNotes
GetNotesForTests = False
TheNote = ""
If (PatId > 0) And (ApptId > 0) Then
    Set RetNote = New PatientNotes
    RetNote.NotesAppointmentId = ApptId
    RetNote.NotesPatientId = PatId
    RetNote.NotesSystem = "T" + TstId
    RetNote.NotesType = "C"
    If (RetNote.FindNotes > 0) Then
        i = 1
        While (RetNote.SelectNotes(i))
            If (Trim(RetNote.NotesSystem) = "T" + TstId) And (Trim(RetNote.NotesOffDate) = "") Then
                TheNote = Trim(TheNote) + Trim(RetNote.NotesText1) + Trim(RetNote.NotesText2) + Trim(RetNote.NotesText3) + Trim(RetNote.NotesText4)
                GetNotesForTests = True
            End If
            i = i + 1
        Wend
    End If
    Set RetNote = Nothing
End If
End Function

Private Function IsComplaintPresent(TheComplaint As String, Family As Integer) As Boolean
Dim i As Integer
IsComplaintPresent = False
If (Family > 10) Then
    Exit Function
End If
If (Trim(TheComplaint) <> "") Then
    For i = 1 To MaxPILines(Family)
        If (Trim(TheComplaint) = Left(PILines(Family, i), Len(Trim(TheComplaint)))) Then
            IsComplaintPresent = True
            Exit For
        ElseIf (Trim(Left(TheComplaint, Len(Trim(Left(PILines(Family, i), 38)))) = Trim(Left(PILines(Family, i), 38)))) Then
            IsComplaintPresent = True
            Exit For
        End If
    Next i
End If
End Function

Private Sub LoadPIList(AList As ListBox, Item As Integer, BoxMax As Integer)
Dim i As Integer
Dim Temp As String
Dim NewTemp As String
Temp = AList.List(0)
i = InStrPS(Temp, ":")
If (i > 0) Then
    NewTemp = Left(Temp, i)
Else
    NewTemp = Temp
End If
AList.Clear
AList.AddItem Temp
For i = 1 To MaxPILines(Item)
    If (Trim(PILines(Item, i)) <> "") Then
        If (i <= BoxMax) Then
            AList.AddItem PILines(Item, i)
        End If
    End If
Next i
If (AList.ListCount > 1) Then
    If (Item <> 10) Then
        AList.List(0) = NewTemp
    Else
        AList.List(0) = NewTemp + AList.List(1)
        AList.RemoveItem 1
    End If
End If
End Sub

Private Sub StuffPINote(Item As Integer, Note As String, NoteId As Long, NoteCat As String)
Dim k As Integer
Dim u As Integer
Dim Temp As String
Dim BuildText As String
Dim ATemp As String
ATemp = ""
BuildText = ""
If (Trim(Note) <> "") Then
    Call StripCharacters(Note, Chr(13))
    Call StripCharacters(Note, Chr(10))
    For u = 1 To Len(Note)
        Temp = Mid(Note, u, 1)
        If (Temp <> "") Then
            If (Len(BuildText) >= 30) Then
                GoSub CheckIt
                BuildText = ATemp
            End If
            BuildText = BuildText + Temp
        End If
    Next u
    If (Len(BuildText) <= 30) And (Trim(BuildText) <> "") Then
        BuildText = BuildText + Space(30 - Len(BuildText))
        BuildText = BuildText + Space(30) + Trim(str(NoteId))
        Mid(BuildText, 59, 1) = NoteCat
        If (MaxPILines(Item) < 50) Then
            PILines(Item, MaxPILines(Item) + 1) = BuildText
            CurrentLine(Item) = CurrentLine(Item) + 1
            MaxPILines(Item) = MaxPILines(Item) + 1
        End If
    End If
End If
Exit Sub
CheckIt:
    ATemp = ""
    If (Mid(BuildText, Len(BuildText), 1) <> " ") Then
        For k = Len(BuildText) To 1 Step -1
            If (Mid(BuildText, k, 1) = " ") Then
                Exit For
            End If
        Next k
        If (k > 0) Then
            ATemp = Mid(BuildText, k, Len(BuildText) - (k - 1))
        Else
            k = Len(BuildText)
        End If
        BuildText = Left(BuildText, k)
    End If
    If (Len(BuildText) < 30) Then
        BuildText = BuildText + Space(30 - Len(BuildText))
    End If
    
    If (Trim(NoteCat) <> "") Then
        BuildText = BuildText + Space(29) + Trim(NoteCat) + Trim(str(NoteId))
    Else
        BuildText = BuildText + Space(30) + Trim(str(NoteId))
    End If
    If (MaxPILines(Item) < 50) Then
        PILines(Item, MaxPILines(Item) + 1) = BuildText
        CurrentLine(Item) = CurrentLine(Item) + 1
        MaxPILines(Item) = MaxPILines(Item) + 1
    End If
    Return
End Sub

Private Function SetSummaryInfoFromComplaints(QSet As String, Side As String, DIEntered As Boolean) As Boolean
On Error GoTo UI_ErrorHandler
Dim StartI As Integer
Dim i As Integer, q As Integer
Dim s As Integer, w As Integer
Dim s1 As Long, ApptId As Long
Dim MaxRead As Integer
Dim PrevApptId As Long
Dim PrevFamily As Integer
Dim Family As Integer
Dim HighOn As Boolean
Dim z As Boolean, CurComp As Boolean
Dim PrevHighOn As Boolean, PrevComp As Boolean
Dim CompDate As String
Dim QuestionSet As String
Dim TheQuestion As String
Dim TheComplaint As String
Dim DoctorLingo As String
Dim ControlId As Long
Dim QuestionType As String * 1
Dim LinkQuestion As String
Dim DisplayString As String
Dim RetrieveControl1 As DynamicControls
Dim RetrieveControl2 As DynamicSecondaryControls
Dim RetrieveControl3 As DynamicThirdControls
Dim RetrieveControl4 As DynamicFourthControls
SetSummaryInfoFromComplaints = False
QuestionSet = QSet
i = 1
StartI = 1
MaxRead = 0
Family = 0
PrevFamily = 0
MaxRead = MaxComplaints + 1
TheQuestion = ""
DisplayString = ""
While (i < MaxRead)
    DoctorLingo = ""
    LinkQuestion = ""
    If (i = 1) Then
        PrevApptId = Complaints(i).ApptId
        PrevComp = Complaints(i).CurComp
        PrevHighOn = Complaints(i).HighlightOn
    Else
        PrevApptId = ApptId
        PrevComp = CurComp
        PrevHighOn = HighOn
    End If
    TheComplaint = Complaints(i).TheText
    CompDate = Complaints(i).ComplaintDate
    CurComp = Complaints(i).CurComp
    ApptId = Complaints(i).ApptId
    HighOn = Complaints(i).HighlightOn
    If (Left(TheComplaint, 1) = "/") Then
        TheQuestion = TheComplaint
        QuestionType = Left(TheComplaint, 1)
        If (Trim(DisplayString) <> "") Then
            If ((Family <> 10) And (Family <> 1)) Then
                Call PostToLists(Family, DisplayString, DIEntered, HighOn)
                PrevComp = CurComp
                PrevHighOn = HighOn
                PrevApptId = ApptId
            ElseIf ((CurComp) And (ApptId = AppointmentId)) Or ((PrevComp) And (PrevApptId = AppointmentId)) Then
                If PrevApptId = AppointmentId Then
                    Call PostToLists(Family, DisplayString, DIEntered, HighOn)
                End If
                PrevComp = CurComp
                PrevHighOn = HighOn
                PrevApptId = ApptId
            ElseIf (PrevComp) Then
                If (PrevFamily < 1) Then
                    Call PostToLists(Family, DisplayString, DIEntered, PrevHighOn)
                Else
                    Call PostToLists(PrevFamily, DisplayString, DIEntered, PrevHighOn)
                End If
                PrevComp = CurComp
                PrevHighOn = HighOn
                PrevApptId = ApptId
            End If
        End If
    ElseIf (InStrPS("?[]", Left(TheComplaint, 1)) <> 0) Then
        QuestionType = Left(TheComplaint, 1)
    ElseIf (Left(TheComplaint, 3) = "CC:") Then
        DoctorLingo = Mid(TheComplaint, 4, Len(TheComplaint) - 3)
        DoctorLingo = Mid(TheComplaint, 4, Len(TheComplaint) - 3)
        If (InStrPS(DoctorLingo, "<") = 0) Then
            DoctorLingo = DoctorLingo + " <" + Complaints(i).TheEye + "> "
        End If
        LinkQuestion = Mid(TheQuestion, 2, Len(TheQuestion) - 1)
        Family = 10
    ElseIf (Left(TheComplaint, 8) = "DRUGTBL:") Then
        q = InStrPS(TheComplaint, "=")
        If (q = 0) Then
            q = Len(TheComplaint)
        Else
            q = q - 1
        End If
        DoctorLingo = "-" + Mid(TheComplaint, 9, q - 8)
        q = InStrPS(TheComplaint, "<")
        If (q > 0) Then
            s = InStrPS(TheComplaint, ">")
            If (s > 0) Then
                DoctorLingo = DoctorLingo + " " + Mid(TheComplaint, q + 1, (s - 1) - q)
            End If
        End If
        LinkQuestion = Mid(TheQuestion, 2, Len(TheQuestion) - 1)
        Family = 8
    ElseIf (Left(TheComplaint, 8) = "NEWDIAG:") Then
        q = InStrPS(TheComplaint, "=T")
        If (q = 0) Then
            q = Len(TheComplaint)
        End If
        s1 = Val(Mid(TheComplaint, q + 2, 4))
        Call GetDiagnosisDirect(s1, DoctorLingo)
        DoctorLingo = "-" + DoctorLingo
        LinkQuestion = Mid(TheQuestion, 2, Len(TheQuestion) - 1)
        Family = 3
    Else
        q = InStrPS(TheComplaint, "NOTSURE")
        If (q <> 0) Then
            DoctorLingo = "Not Sure - " + Mid(TheQuestion, 2, Len(TheQuestion) - 1)
            LinkQuestion = Mid(TheQuestion, 2, Len(TheQuestion) - 1)
            Family = SetFamilybyQuestion(LinkQuestion, QuestionSet)
        Else
            Set RetrieveControl1 = New DynamicControls
            Set RetrieveControl2 = New DynamicSecondaryControls
            Set RetrieveControl3 = New DynamicThirdControls
            Set RetrieveControl4 = New DynamicFourthControls
            q = InStrPS(TheComplaint, "=T")
            If (q <> 0) Then
                ControlId = Val(Trim(Mid(TheComplaint, q + 2, Len(TheComplaint) - (q + 1))))
                If (QuestionType = "/") Then
                    PrevFamily = Family
                    Family = SetFamilybyControl(ControlId, 1, LinkQuestion, QuestionSet)
                    RetrieveControl1.FormId = 0
                    RetrieveControl1.ControlId = ControlId
                    RetrieveControl1.ControlName = ""
                    RetrieveControl1.IEChoiceName = ""
                    If (RetrieveControl1.RetrieveControl) Then
                        If (Trim(RetrieveControl1.DoctorLingo) <> "") Then
                            DoctorLingo = RetrieveControl1.DoctorLingo
                        Else
                            DoctorLingo = ""
                        End If
                    End If
                    If (Trim(CompDate) <> "") And (Trim(DoctorLingo) <> "") Then
                        DoctorLingo = DoctorLingo + " " + Trim(CompDate)
                    End If
                ElseIf (QuestionType = "?") Then
                    RetrieveControl2.SecondaryFormId = 0
                    RetrieveControl2.SecondaryControlId = ControlId
                    RetrieveControl2.ControlName = ""
                    RetrieveControl2.IEChoiceName = ""
                    If (RetrieveControl2.RetrieveControl) Then
                        If (Trim(RetrieveControl2.DoctorLingo) <> "") Then
                            DoctorLingo = RetrieveControl2.DoctorLingo
                        Else
                            DoctorLingo = ""
                        End If
                    End If
                    If (Trim(CompDate) <> "") And (Trim(DoctorLingo) <> "") Then
                        If (InStrPS(DoctorLingo, "OD ") > 0) Then
                            DoctorLingo = "-OD " + Trim(CompDate) + " "
                        ElseIf (InStrPS(DoctorLingo, "OS ") > 0) Then
                            DoctorLingo = "-OS " + Trim(CompDate) + " "
                        ElseIf (InStrPS(DoctorLingo, "OU ") > 0) Then
                            DoctorLingo = "-OU " + Trim(CompDate) + " "
                        Else
                            DoctorLingo = "- " + Trim(CompDate) + " "
                        End If
                    End If
                ElseIf (QuestionType = "]") Then
                    RetrieveControl3.ThirdFormId = 0
                    RetrieveControl3.ThirdControlId = ControlId
                    RetrieveControl3.ControlName = ""
                    RetrieveControl3.IEChoiceName = ""
                    If (RetrieveControl3.RetrieveControl) Then
                        If (Trim(RetrieveControl3.DoctorLingo) <> "") Then
                            DoctorLingo = RetrieveControl3.DoctorLingo
                        Else
                            DoctorLingo = ""
                        End If
                    End If
                    If (Trim(CompDate) <> "") And (Trim(DoctorLingo) <> "") Then
                        If (InStrPS(DoctorLingo, "OD ") > 0) Then
                            DoctorLingo = "-OD " + Trim(CompDate) + " "
                        ElseIf (InStrPS(DoctorLingo, "OS ") > 0) Then
                            DoctorLingo = "-OS " + Trim(CompDate) + " "
                        ElseIf (InStrPS(DoctorLingo, "OU ") > 0) Then
                            DoctorLingo = "-OU " + Trim(CompDate) + " "
                        Else
                            DoctorLingo = "- " + Trim(CompDate) + " "
                        End If
                    End If
                ElseIf (QuestionType = "[") Then
                    RetrieveControl4.FourthFormId = 0
                    RetrieveControl4.FourthControlId = ControlId
                    RetrieveControl4.ControlName = ""
                    RetrieveControl4.IEChoiceName = ""
                    If (RetrieveControl4.RetrieveControl) Then
                        If (Trim(RetrieveControl4.DoctorLingo) <> "") Then
                            DoctorLingo = RetrieveControl4.DoctorLingo
                        Else
                            DoctorLingo = ""
                        End If
                    End If
                    If (Trim(CompDate) <> "") And (Trim(DoctorLingo) <> "") Then
                        If (InStrPS(DoctorLingo, "OD ") > 0) Then
                            DoctorLingo = "-OD " + Trim(CompDate) + " "
                        ElseIf (InStrPS(DoctorLingo, "OS ") > 0) Then
                            DoctorLingo = "-OS " + Trim(CompDate) + " "
                        ElseIf (InStrPS(DoctorLingo, "OU ") > 0) Then
                            DoctorLingo = "-OU " + Trim(CompDate) + " "
                        Else
                            DoctorLingo = "- " + Trim(CompDate) + " "
                        End If
                    End If
                Else
                    DoctorLingo = ""
                End If
            Else
                DoctorLingo = ""
            End If
            Set RetrieveControl1 = Nothing
            Set RetrieveControl2 = Nothing
            Set RetrieveControl3 = Nothing
            Set RetrieveControl4 = Nothing
        End If
    End If
    If (DoctorLingo <> "") Then
        If (QuestionType = "/") Then
            Call ReplaceCharacters(DisplayString, "$", " ")
            DisplayString = Space(255)
            Mid(DisplayString, 1, Len(DoctorLingo) + 1) = DoctorLingo + "$"
            Mid(DisplayString, 129, Len(Trim(TheQuestion)) - 1) = Trim(Mid(TheQuestion, 2, Len(TheQuestion) - 1))
            Mid(DisplayString, 200, 5) = Side + Trim(str(i))
        Else
            q = InStrPS(DisplayString, "$")
            If (q <> 0) Then
                Mid(DisplayString, q, Len(DoctorLingo) + 2) = " " + DoctorLingo + "$"
                Mid(DisplayString, 129, Len(Trim(TheQuestion)) - 1) = Trim(Mid(TheQuestion, 2, Len(TheQuestion) - 1))
                Mid(DisplayString, 200, 5) = Side + Trim(str(i))
            End If
        End If
    End If
    i = i + 1
Wend
If (Trim(DisplayString) <> "") Then
    If ((Family <> 10) And (Family <> 1)) Then
        Call PostToLists(Family, DisplayString, DIEntered, HighOn)
    ElseIf (CurComp) And (ApptId = AppointmentId) Then
        Call PostToLists(Family, DisplayString, DIEntered, HighOn)
    End If
End If
SetSummaryInfoFromComplaints = True
Exit Function
UI_ErrorHandler:
    SetSummaryInfoFromComplaints = False
    Set RetrieveControl1 = Nothing
    Set RetrieveControl2 = Nothing
    Set RetrieveControl3 = Nothing
    Set RetrieveControl4 = Nothing
    Call PinpointError("SettSummaryInfoFromComplaints", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume LeaveFast
LeaveFast:
End Function

Private Function IsListed(TheText As String) As Boolean
Dim i As Integer
Dim AString As String
Dim TestString As String
IsListed = False
TestString = Trim(Left(TheText, 128))
Call ReplaceCharacters(TestString, "OU", " ")
Call ReplaceCharacters(TestString, "OD", " ")
Call ReplaceCharacters(TestString, "OS", " ")
Call ReplaceCharacters(TestString, "(H)", " ")
Call ReplaceCharacters(TestString, "(T)", " ")
Call ReplaceCharacters(TestString, "(P)", " ")
For i = 1 To MaxComplaints
    AString = Trim(Left(Complaints(i).TheText, 128))
    Call ReplaceCharacters(AString, "OU", " ")
    Call ReplaceCharacters(AString, "OD", " ")
    Call ReplaceCharacters(AString, "OS", " ")
    Call ReplaceCharacters(AString, "(H)", " ")
    Call ReplaceCharacters(AString, "(T)", " ")
    Call ReplaceCharacters(AString, "(P)", " ")
    If (UCase(TestString) = UCase(AString)) Then
        IsListed = True
        Exit For
    End If
Next i
End Function

Private Sub PostToLists(Family As Integer, TheText As String, DIEntered As Boolean, HighOn As Boolean)
Dim z As Integer
Dim q As Integer
Dim TheOrder As String
Dim TheLoc As Integer
Dim LocalQues As String
Dim LocalText As String
Dim LocalText1 As String
Dim DisplayString As String
DisplayString = TheText
If (Trim(DisplayString) <> "") Then
    q = InStrPS(DisplayString, "$")
    If (q > 0) Then
        LocalText = Trim(Left(DisplayString, q - 1))
    Else
        LocalText = Trim(DisplayString)
    End If
    If (IsListed(LocalText)) Then
        TheText = ""
        Exit Sub
    End If
    If (IsComplaintPresent(LocalText, Family)) Then
        TheText = ""
        Exit Sub
    End If
    LocalText = ""
    LocalText1 = ""
    Call ReplaceCharacters(DisplayString, "$", " ")
    CurrentPILoaded = CurrentPILoaded + 1
    PILoaded(CurrentPILoaded) = Trim(DisplayString)
    If (HighOn) Then
        LocalText = "*" + Trim(Left(DisplayString, 127))
    Else
        LocalText = Trim(Left(DisplayString, 128))
    End If
    LocalQues = Trim(Mid(DisplayString, 129, 64))
    If (DIEntered) Then
        LocalText = "(T)" + LocalText1
    End If
    TheOrder = Mid(DisplayString, 200, 4) + " "
    For z = 2 To Len(TheOrder)
        If (Mid(TheOrder, z, 1) < "0") Or (Mid(TheOrder, z, 1) > "9") Then
            Mid(TheOrder, z, 1) = " "
        End If
    Next z
    TheLoc = 1
    TheLoc = ParseLine(TheLoc, 36, LocalText, LocalText1)
    If (LocalText1 <> "") Then
        LocalText1 = LocalText1 + Space(75 - Len(LocalText1)) + TheOrder + LocalQues
        MaxPILines(Family) = MaxPILines(Family) + 1
        PILines(Family, MaxPILines(Family)) = LocalText1
    End If
    TheLoc = ParseLine(TheLoc, 36, LocalText, LocalText1)
    If (LocalText1 <> "") Then
        LocalText1 = LocalText1 + Space(75 - Len(LocalText1)) + TheOrder + LocalQues
        MaxPILines(Family) = MaxPILines(Family) + 1
        PILines(Family, MaxPILines(Family)) = LocalText1
    End If
    TheLoc = ParseLine(TheLoc, 36, LocalText, LocalText1)
    If (LocalText1 <> "") Then
        LocalText1 = LocalText1 + Space(75 - Len(LocalText1)) + TheOrder + LocalQues
        MaxPILines(Family) = MaxPILines(Family) + 1
        PILines(Family, MaxPILines(Family)) = LocalText1
    End If
    TheLoc = ParseLine(TheLoc, 36, LocalText, LocalText1)
    If (LocalText1 <> "") Then
        LocalText1 = LocalText1 + Space(75 - Len(LocalText1)) + TheOrder + LocalQues
        MaxPILines(Family) = MaxPILines(Family) + 1
        PILines(Family, MaxPILines(Family)) = LocalText1
    End If
    DisplayString = ""
End If
TheText = DisplayString
End Sub

Private Sub Form_Load()
ActiveActivityDate = ""
ActiveActivityTime = ""
End Sub

Private Sub lstCC_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If (lstCC.ListIndex + 1 > lstCC.ListCount - 1) Then
    lstCC.ListIndex = lstCC.ListCount - 1
Else
    lstCC.ListIndex = lstCC.ListIndex + 1
End If
End Sub

Private Sub lstOHX_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If (lstOHX.ListIndex + 1 > lstOHX.ListCount - 1) Then
    lstOHX.ListIndex = lstOHX.ListCount - 1
Else
    lstOHX.ListIndex = lstOHX.ListIndex + 1
End If
End Sub

Private Sub lstFMX_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If (lstFMX.ListIndex + 1 > lstFMX.ListCount - 1) Then
    lstFMX.ListIndex = lstFMX.ListCount - 1
Else
    lstFMX.ListIndex = lstFMX.ListIndex + 1
End If
End Sub

Private Sub lstALG_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If (lstALG.ListIndex + 1 > lstALG.ListCount - 1) Then
    lstALG.ListIndex = lstALG.ListCount - 1
Else
    lstALG.ListIndex = lstALG.ListIndex + 1
End If
End Sub

Private Sub lstMeds_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If (lstMeds.ListIndex + 1 > lstMeds.ListCount - 1) Then
    lstMeds.ListIndex = lstMeds.ListCount - 1
Else
    lstMeds.ListIndex = lstMeds.ListIndex + 1
End If
End Sub

Private Sub lstHPI_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If (lstHPI.ListIndex + 1 > lstHPI.ListCount - 1) Then
    lstHPI.ListIndex = lstHPI.ListCount - 1
Else
    lstHPI.ListIndex = lstHPI.ListIndex + 1
End If
End Sub

Private Sub lstTests_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim i As Integer
If (lstTests.ListIndex + 1 > lstTests.ListCount - 1) Then
    lstTests.ListIndex = lstTests.ListCount - 1
Else
    lstZoom.Clear
    For i = 0 To lstTests.ListCount - 1
        lstZoom.AddItem lstTests.List(i)
    Next i
    lstZoom.Visible = True
'    lstTests.ListIndex = lstTests.ListIndex + 1
End If
End Sub

Public Function GetPIBoxItem(BoxId As Integer, Item As Integer, RosType As String) As String
Dim Ref As Integer
Dim ATemp As String
RosType = ""
ATemp = PILines(BoxId, Item)
Ref = Val(Trim(Mid(ATemp, 77, 3)))
If (Ref > 0) Then
    RosType = Complaints(Ref).Ros
    If (Trim(RosType) = "") And (Ref > 2) Then
        RosType = Complaints(Ref - 2).Ros
    End If
End If
GetPIBoxItem = ATemp
End Function

Public Function GetComplaint(IdRef As Integer, ArrayType As String, TheComp As String, TheCompId As Long, RosType As String) As Boolean
GetComplaint = False
TheComp = ""
TheCompId = 0
If (IdRef > 0) Then
    TheComp = Complaints(IdRef).TheText
    TheCompId = Complaints(IdRef).TheId
    RosType = Complaints(IdRef).Ros
    GetComplaint = True
End If
End Function

Public Function ClearComplaint(IdRef As Integer, ArrayType As String) As Boolean
ClearComplaint = False
If (IdRef > 0) Then
    Complaints(IdRef).ApptId = 0
    Complaints(IdRef).ComplaintDate = ""
    Complaints(IdRef).CurComp = False
    Complaints(IdRef).HighlightOn = False
    Complaints(IdRef).TheEye = ""
    Complaints(IdRef).TheText = ""
    Complaints(IdRef).TheId = 0
    Complaints(IdRef).Ros = ""
    ClearComplaint = True
End If
End Function

Private Sub PlantNotesLikeDiag(PatId As Long, ApptId As Long)
Dim k As Integer
Dim i As Integer
Dim Temp2 As String
Dim TheSys As String
Dim TheEye As String
Dim RetNotes As PatientNotes
' kill all notes in the listboxs
i = 0
k = lstOD.ListCount - 1
While i <= k
    If (Len(lstOD.List(i)) < 70) Then
        lstOD.RemoveItem i
        i = -1
        k = lstOD.ListCount - 1
    End If
    i = i + 1
Wend
i = 0
k = lstOS.ListCount - 1
While i <= k
    If (Len(lstOS.List(i)) < 70) Then
        lstOS.RemoveItem i
        i = -1
        k = lstOS.ListCount - 1
    End If
    i = i + 1
Wend
Set RetNotes = New PatientNotes
RetNotes.NotesPatientId = PatId
RetNotes.NotesAppointmentId = ApptId
RetNotes.NotesType = "C"
RetNotes.NotesSystem = ""
If (RetNotes.FindNotes > 0) Then
    i = 1
    While (RetNotes.SelectNotes(i))
        If (Trim(RetNotes.NotesSystem) <> "") And (Len(Trim(RetNotes.NotesSystem)) > 1) And (Trim(RetNotes.NotesSystem) <> "10") Then
            TheSys = GetSystemLetter(Trim(RetNotes.NotesSystem))
            TheEye = Trim(RetNotes.NotesEye)
            If (TheSys <> "") Then
                If (Trim(RetNotes.NotesText1) <> "") Then
                    Temp2 = Trim(RetNotes.NotesText1)
                    If (Len(Temp2) > 28) Then
                        Temp2 = Left(Temp2, 27) + "->"
                    End If
                    Call StuffDiagNote(TheSys, Trim(RetNotes.NotesSystem), RetNotes.NotesId, Trim(Temp2), TheEye)
                End If
            End If
        End If
        i = i + 1
    Wend
End If
Set RetNotes = Nothing
End Sub

Private Sub StuffDiagNote(ASys As String, RSys As String, AId As Long, Note As String, TheEye As String)
Dim k As Integer
Dim u As Integer
Dim Temp As String
Dim Temp1 As String
Dim TempA As String
Dim BuildText As String
Dim ATemp As String
ATemp = ""
BuildText = ""
If (Trim(Note) <> "") Then
    Call StripCharacters(Note, Chr(13))
    Call StripCharacters(Note, Chr(10))
    For u = 1 To Len(Note)
        Temp = Mid(Note, u, 1)
        If (Temp <> "") Then
            If (Len(BuildText) >= 30) Then
                GoSub ACheckIt
                BuildText = ATemp
            End If
            BuildText = BuildText + Temp
        End If
    Next u
    If (Len(BuildText) < 30) And (Trim(BuildText) <> "") Then
'        Temp1 = Trim(RSys) + ":" + BuildText
        Call frmHome.GetSystemLabelAbbr(RSys, TempA)
        Temp1 = Trim(TempA) + ":" + BuildText
        Call NSFPadValue(Temp1, 60)
        Temp1 = Temp1 + Trim(str(AId))
        If Not (IsNotePresent(TheEye, Temp1)) Then
            Call ListDiagnosis(ASys, Temp1, TheEye, "", "", "")
        End If
    End If
End If
Exit Sub
ACheckIt:
    ATemp = ""
    If (Mid(BuildText, Len(BuildText), 1) <> " ") Then
        For k = Len(BuildText) To 1 Step -1
            If (Mid(BuildText, k, 1) = " ") Then
                Exit For
            End If
        Next k
        ATemp = Mid(BuildText, k, Len(BuildText) - (k - 1))
        BuildText = Left(BuildText, k)
    End If
    Temp1 = Trim(RSys) + ":" + BuildText
    Call NSFPadValue(Temp1, 60)
    Temp1 = Temp1 + Trim(str(AId))
    If Not (IsNotePresent(TheEye, Temp1)) Then
        Call ListDiagnosis(ASys, Temp1, TheEye, "", "", "")
    End If
    Return
End Sub

Private Function IsNotePresent(TheEye As String, AText As String) As Boolean
Dim i As Integer
IsNotePresent = False
If (TheEye = "OD") Or (TheEye = "") Then
    For i = 0 To lstOD.ListCount - 1
        If (AText = Trim(lstOD.List(i))) Then
            IsNotePresent = True
        End If
    Next i
End If
If (TheEye = "OS") Or (TheEye = "") Then
    For i = 0 To lstOS.ListCount - 1
        If (AText = Trim(lstOS.List(i))) Then
            IsNotePresent = True
        End If
    Next i
End If
End Function

Private Function GetSystemLetter(AName As String) As String
GetSystemLetter = ""
If (AName = frmSystems1.cmdA.Text) Then
    GetSystemLetter = "A"
ElseIf (AName = frmSystems1.cmdB.Text) Then
    GetSystemLetter = "B"
ElseIf (AName = frmSystems1.cmdC.Text) Then
    GetSystemLetter = "C"
ElseIf (AName = frmSystems1.cmdD.Text) Then
    GetSystemLetter = "D"
ElseIf (AName = frmSystems1.cmdE.Text) Then
    GetSystemLetter = "E"
ElseIf (AName = frmSystems1.cmdF.Text) Then
    GetSystemLetter = "F"
ElseIf (AName = frmSystems1.cmdG.Text) Then
    GetSystemLetter = "G"
ElseIf (AName = frmSystems1.cmdH.Text) Then
    GetSystemLetter = "H"
ElseIf (AName = frmSystems1.cmdI.Text) Then
    GetSystemLetter = "I"
ElseIf (AName = frmSystems1.cmdJ.Text) Then
    GetSystemLetter = "J"
ElseIf (AName = frmSystems1.cmdK.Text) Then
    GetSystemLetter = "K"
ElseIf (AName = frmSystems1.cmdL.Text) Then
    GetSystemLetter = "L"
ElseIf (AName = frmSystems1.cmdM.Text) Then
    GetSystemLetter = "M"
ElseIf (AName = frmSystems1.cmdN.Text) Then
    GetSystemLetter = "N"
ElseIf (AName = frmSystems1.cmdO.Text) Then
    GetSystemLetter = "O"
ElseIf (AName = frmSystems1.cmdP.Text) Then
    GetSystemLetter = "P"
ElseIf (AName = frmSystems1.cmdQ.Text) Then
    GetSystemLetter = "Q"
ElseIf (AName = frmSystems1.cmdR.Text) Then
    GetSystemLetter = "R"
ElseIf (AName = "SLE") Then
    GetSystemLetter = "Y"
ElseIf (AName = "Fundus") Then
    GetSystemLetter = "X"
End If
End Function

Private Function GetComplaintRos(AFinding As String, ASymptom As String, Ros As String) As Boolean
Dim i As Integer
Dim j As Integer
Dim Id As Long
Dim ALevel As Integer
Dim RetCtrl1 As DynamicControls
Dim RetCtrl2 As DynamicSecondaryControls
Dim RetCtrl3 As DynamicThirdControls
Dim RetCtrl4 As DynamicFourthControls
Id = 0
ALevel = 0
Ros = ""
GetComplaintRos = False
If (Trim(AFinding) <> "") And (Trim(ASymptom) <> "") Then
    i = InStrPS(AFinding, "=T")
    If (i > 0) Then
        j = InStrPS(i, AFinding, " ")
        If (j > 0) Then
            Id = Val(Trim(Mid(AFinding, i + 2, j - (i + 1))))
        Else
            Id = Val(Trim(Mid(AFinding, i + 2, Len(AFinding) - (i + 1))))
        End If
    End If
    If (Mid(ASymptom, 1, 1) = "/") Then
        ALevel = 1
    ElseIf (Mid(ASymptom, 1, 1) = "?") Then
        ALevel = 2
    ElseIf (Mid(ASymptom, 1, 1) = "[") Then
        ALevel = 3
    ElseIf (Mid(ASymptom, 1, 1) = "]") Then
        ALevel = 4
    End If
    If (Id > 0) Then
        If (ALevel = 1) Then
            Set RetCtrl1 = New DynamicControls
            RetCtrl1.ControlId = Id
            If (RetCtrl1.RetrieveControl) Then
                Ros = Trim(RetCtrl1.Ros)
                GetComplaintRos = True
            End If
            Set RetCtrl1 = Nothing
        ElseIf (ALevel = 2) Then
            Set RetCtrl2 = New DynamicSecondaryControls
            RetCtrl2.SecondaryControlId = Id
            If (RetCtrl2.RetrieveControl) Then
                Ros = Trim(RetCtrl2.Ros)
                GetComplaintRos = True
            End If
            Set RetCtrl2 = Nothing
        ElseIf (ALevel = 3) Then
            Set RetCtrl3 = New DynamicThirdControls
            RetCtrl3.ThirdControlId = Id
            If (RetCtrl3.RetrieveControl) Then
                Ros = Trim(RetCtrl3.Ros)
                GetComplaintRos = True
            End If
            Set RetCtrl3 = Nothing
        ElseIf (ALevel = 4) Then
            Set RetCtrl4 = New DynamicFourthControls
            RetCtrl4.FourthControlId = Id
            If (RetCtrl4.RetrieveControl) Then
                Ros = Trim(RetCtrl4.Ros)
                GetComplaintRos = True
            End If
            Set RetCtrl4 = Nothing
        End If
    End If
End If
End Function

Private Sub TriggerIt(Id As Long, AEye As String)
If (Id > 0) And (Trim(AEye) <> "") Then
    frmEventMsgs.Header = "Bring Forward"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Yes"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        If (AEye = "OD") Then
            DxMoveOn = True
            EyeToBringForward = "OD"
            AppointmentToBringForward = Id
        ElseIf (AEye = "OS") Then
            DxMoveOn = True
            EyeToBringForward = "OS"
            AppointmentToBringForward = Id
        ElseIf (AEye = "OU") Then
            DxMoveOn = True
            EyeToBringForward = "OU"
            AppointmentToBringForward = Id
        End If
    End If
End If
End Sub

Private Function LoadPrimaryInfo(ClnType As String) As Boolean
'
' 1 round trip to load the primary exam complaints (usp_DI_ExamContents)
' 1 round trip to load the primary exam notes      (usp_DI_ExamContentNotes)
'
' Pulled from the recordsets as follows:
' 1. Patient Complaints (C)  1. Complaint Notes
' 2. Patient Complaints (H)
' 3. Med Actions (A)         2. Med Notes
' 4. Tests (F)               3. Test Notes
' 5. Images (I)
' 6. Findings (Q)            4. Finding Notes
'
On Error GoTo UI_ErrorHandler
Dim Temp As String, Temp1 As String
Dim MyTemp As String, UNote As String
Dim QSet As String, RosType As String
Dim ComplaintOn As Boolean
Dim TriggerMeds As Boolean
Dim TotalLocal As Integer
Dim u As Integer, f As Integer
Dim i As Long, b As Integer
Dim l As Integer, r As Integer

' we do this for reload of complaints
LoadPrimaryInfo = False
If (ClnType = "H") Or (ClnType = "C") Or (ClnType = "") Then
    TriggerMeds = False
    ComplaintOn = False
    QSet = QuestionSet
    CurrentPILoaded = 0
    Erase PILoaded
    Erase PILines
    MaxPILines(1) = 0
    lstCC.Clear
    If CheckConfigCollection("RVON") = "T" Then
        lstCC.AddItem "HPI/CC:"
    Else
        lstCC.AddItem "HPI:"
    End If
    MaxPILines(3) = 0
    lstOHX.Clear
    lstOHX.AddItem "OHX:"
    MaxPILines(4) = 0
    lstMHX.Clear
    lstMHX.AddItem "PMHx/SocHx/ROS:"
    MaxPILines(5) = 0
    lstFMX.Clear
    lstFMX.AddItem "FMX:"
    MaxPILines(9) = 0
    lstALG.Clear
    lstALG.AddItem "ALL:"
    MaxPILines(10) = 0
    lstHPI.Clear
    If CheckConfigCollection("RVON") = "T" Then
        lstHPI.AddItem "RV:"
    Else
        lstHPI.AddItem "CC:"
    End If
    QSet = "First Visit"
    MaxComplaints = 0
    For i = 1 To MaxElements
        Complaints(i).ApptId = 0
        Complaints(i).TheId = 0
        Complaints(i).Ros = ""
        Complaints(i).TheEye = ""
        Complaints(i).TheText = ""
        Complaints(i).ComplaintDate = ""
        Complaints(i).HighlightOn = False
    Next i
' Complaints
    Set ClinicalRecords = New DI_ExamClinical
    If (ClinicalRecords.LoadClinicalData(PatientId, AppointmentId, ActiveActivityDate)) Then
' Do C Complaints
        If (ClinicalRecords.LocateStartingRecord("C")) Then
            i = 1
            Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
                If (ClinicalRecords.ClinicalType = "C") And (ClinicalRecords.ClinicalStatus <> "D") Then
                    Call GetComplaintRos(ClinicalRecords.ClinicalFindings, ClinicalRecords.ClinicalSymptom, RosType)
                    MaxComplaints = MaxComplaints + 1
                    If MaxComplaints > MaxElements Then
                        Exit Do
                    Else
                        Complaints(MaxComplaints).TheId = ClinicalRecords.ClinicalId
                        Complaints(MaxComplaints).TheEye = ClinicalRecords.ClinicalEyeContext
                        If (Trim(ClinicalRecords.ClinicalEyeContext) = "") Then
                            Complaints(MaxComplaints).TheEye = "OU"
                        End If
                        Complaints(MaxComplaints).TheText = Trim(ClinicalRecords.ClinicalSymptom)
                        Complaints(MaxComplaints).ApptId = ClinicalRecords.ClinicalApptId
                        Complaints(MaxComplaints).Ros = RosType
                        Complaints(MaxComplaints).CurComp = False
                        If (AppointmentId = Complaints(MaxComplaints).ApptId) Or (ClinicalRecords.ClinicalApptDate = ActiveActivityDate) Then
                            Complaints(MaxComplaints).CurComp = True
                        End If
                        Complaints(MaxComplaints).ComplaintDate = Trim(ClinicalRecords.ClinicalDescriptor)
                        Complaints(MaxComplaints).HighlightOn = False
                        If (Trim(ClinicalRecords.ClinicalHighlights) <> "") Then
                            Complaints(MaxComplaints).HighlightOn = True
                        End If
    ' the only difference between these is that text contains Findings
                        MaxComplaints = MaxComplaints + 1
                        Complaints(MaxComplaints) = Complaints(MaxComplaints - 1)
                        Complaints(MaxComplaints).TheText = Trim(ClinicalRecords.ClinicalFindings)
                    End If
                Else
                    If (ClinicalRecords.ClinicalType <> "C") Then
                        Exit Do
                    End If
                End If
                i = i + 1
            Loop
        End If
Dim IsSubUrban As Boolean
Dim IsIncluded As Boolean
If (CheckConfigCollection("MergeField19")) = "T" Then IsSubUrban = True
' Do H Complaints
        If (ClinicalRecords.LocateStartingRecord("H")) Then
            i = 1
            Do Until Not (ClinicalRecords.RetrieveClinicalItem(i))
                IsIncluded = False
                If (IsSubUrban And ClinicalRecords.ClinicalStatus = "D") Then
                    IsIncluded = CheckPatientHistoryStatus(ActiveActivityDate, ClinicalRecords.ClinicalId)
                End If
                If (ClinicalRecords.ClinicalType = "H") And (ClinicalRecords.ClinicalStatus <> "D" Or IsIncluded) Then
                    Call GetComplaintRos(ClinicalRecords.ClinicalFindings, ClinicalRecords.ClinicalSymptom, RosType)
                    MaxComplaints = MaxComplaints + 1
                    If MaxComplaints > MaxElements Then
                        Exit Do
                    Else
                        Complaints(MaxComplaints).TheId = ClinicalRecords.ClinicalId
                        Complaints(MaxComplaints).TheEye = ClinicalRecords.ClinicalEyeContext
                        If (Trim(ClinicalRecords.ClinicalEyeContext) = "") Then
                            Complaints(MaxComplaints).TheEye = "OU"
                        End If
                        Complaints(MaxComplaints).TheText = Trim(ClinicalRecords.ClinicalSymptom)
                        Complaints(MaxComplaints).ApptId = ClinicalRecords.ClinicalApptId
                        Complaints(MaxComplaints).Ros = RosType
                        Complaints(MaxComplaints).CurComp = False
                        If (AppointmentId = Complaints(MaxComplaints).ApptId) Or (ClinicalRecords.ClinicalApptDate = ActiveActivityDate) Then
                            Complaints(MaxComplaints).CurComp = True
                        End If
                        Complaints(MaxComplaints).ComplaintDate = Trim(ClinicalRecords.ClinicalDescriptor)
                        Complaints(MaxComplaints).HighlightOn = False
                        If (Trim(ClinicalRecords.ClinicalHighlights) <> "") Then
                            Complaints(MaxComplaints).HighlightOn = True
                        End If
    ' the only difference between these is that text contains Findings
                        MaxComplaints = MaxComplaints + 1
                        Complaints(MaxComplaints) = Complaints(MaxComplaints - 1)
                        Complaints(MaxComplaints).TheText = Trim(ClinicalRecords.ClinicalFindings)
                    End If
                Else
                    If (ClinicalRecords.ClinicalType <> "H") Then
                        Exit Do
                    End If
                End If
                i = i + 1
            Loop
        End If
    End If
' Complaint Notes
    Set ClinicalRecordsNotes = New DI_ExamClinical
    If (ClinicalRecordsNotes.LoadClinicalNoteData(PatientId, AppointmentId, ActiveActivityDate)) Then
        If (ClinicalRecordsNotes.LocateStartingNoteRecord("C")) Then
            i = 1
            Do Until Not (ClinicalRecordsNotes.RetrieveClinicalNoteItem(i))
                If (ClinicalRecordsNotes.ClinicalNotesType = "C") And ((Trim(ClinicalRecordsNotes.ClinicalNotesSystem) >= "1") And (Trim(ClinicalRecordsNotes.ClinicalNotesSystem) <> "8") And (Trim(ClinicalRecords.ClinicalNotesSystem) <= "9")) Or (Trim(ClinicalRecordsNotes.ClinicalNotesSystem) = "10") Then
                    If (Trim(ClinicalRecordsNotes.ClinicalNotesOffDate) = "") Then
                        If ((Trim(ClinicalRecordsNotes.ClinicalNotesSystem) >= "1") And (Trim(ClinicalRecordsNotes.ClinicalNotesSystem) <> "8") And (Trim(ClinicalRecordsNotes.ClinicalNotesSystem) <= "9")) Or (Trim(ClinicalRecordsNotes.ClinicalNotesSystem) = "10") Then
                            If ((ClinicalRecordsNotes.ClinicalNotesSystem <> "1") And (ClinicalRecordsNotes.ClinicalNotesSystem <> "10")) And (ClinicalRecordsNotes.ClinicalApptDate <= ActiveActivityDate) Then
                                f = Val(Trim(ClinicalRecordsNotes.ClinicalNotesSystem))
                                If (f > 0) Then
                                    MyTemp = Trim(ClinicalRecordsNotes.ClinicalNotesText1) + " " + Trim(ClinicalRecordsNotes.ClinicalNotesText2) + " " + Trim(ClinicalRecordsNotes.ClinicalNotesText3) + " " + Trim(ClinicalRecordsNotes.ClinicalNotesText4)
                                    If (Trim(MyTemp) <> "") Then
                                        Call StripCharacters(MyTemp, Chr(13))
                                        Call ReplaceCharacters(MyTemp, Chr(10), " ")
                                        Call StuffPINote(f, MyTemp, ClinicalRecords.ClinicalNotesId, ClinicalRecords.ClinicalNotesCategory)
                                    End If
                                End If
                            ElseIf (ClinicalRecordsNotes.ClinicalNotesAppointmentId = AppointmentId) Then
                                f = Val(Trim(ClinicalRecordsNotes.ClinicalNotesSystem))
                                If (f > 0) Then
                                    MyTemp = Trim(ClinicalRecordsNotes.ClinicalNotesText1) + " " + Trim(ClinicalRecordsNotes.ClinicalNotesText2) + " " + Trim(ClinicalRecordsNotes.ClinicalNotesText3) + " " + Trim(ClinicalRecordsNotes.ClinicalNotesText4)
                                    If (Trim(MyTemp) <> "") Then
                                        Call StripCharacters(MyTemp, Chr(13))
                                        Call ReplaceCharacters(MyTemp, Chr(10), " ")
                                        Call StuffPINote(f, MyTemp, ClinicalRecords.ClinicalNotesId, ClinicalRecords.ClinicalNotesCategory)
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    Exit Do
                End If
                i = i + 1
            Loop
        End If
    End If
' Current Complaints
    ComplaintOn = True
    QSet = "First Visit"
    Call SetSummaryInfoFromComplaints(QSet, "", False)

' Display Them
    CurrentLine(1) = 1
    Call LoadPIList(lstCC, 1, 8)
    CurrentLine(3) = 1
    Call LoadPIList(lstOHX, 3, 5)
    CurrentLine(4) = 1
    Call LoadPIList(lstMHX, 4, 8)
    CurrentLine(5) = 1
    Call LoadPIList(lstFMX, 5, 5)
    CurrentLine(9) = 1
    Call LoadPIList(lstALG, 9, 4)
    CurrentLine(10) = 1
    Call LoadPIList(lstHPI, 10, 1)

    If (MaxPILines(1) > 8) Then
        cmdMagnifyCC.Visible = True
    End If
    If (lstCC.ListCount < 2) Then
        lstCC.Clear
        If CheckConfigCollection("RVON") = "T" Then
            lstCC.AddItem "HPI/CC: NONE"
        Else
            lstCC.AddItem "HPI: NONE"
        End If
    End If
    If (MaxPILines(3) > 5) Then
        cmdMagnifyOHX.Visible = True
    End If
    If (lstOHX.ListCount < 2) Then
        lstOHX.Clear
        lstOHX.AddItem "OHX: NONE"
    End If
    If (MaxPILines(4) > 8) Then
        cmdMagnifyMHX.Visible = True
    End If
    If (lstMHX.ListCount < 2) Then
        lstMHX.Clear
        lstMHX.AddItem "PMHx/SocHx/ROS: NONE"
    End If
    If (MaxPILines(5) > 5) Then
        cmdMagnifyFMX.Visible = True
    End If
    If (lstFMX.ListCount < 2) Then
        lstFMX.Clear
        lstFMX.AddItem "FMX: NON-CONTRIBUTORY"
    End If
    If (MaxPILines(9) > 4) Then
        cmdMagnifyALG.Visible = True
    End If
    If (lstALG.ListCount < 2) Then
        lstALG.Clear
        lstALG.AddItem "ALL: NKDA"
    End If
    If (MaxPILines(10) > 1) Then
        cmdMagnifyHPI.Visible = True
    End If
    
' Med Actions
    If (ClinicalRecords.LocateStartingRecord("A")) Then
        Call LoadMeds(lstMeds)
    Else
        If (lstMeds.ListCount < 2) Then
            lstMeds.Clear
            lstMeds.AddItem "MEDS: NONE"
        End If
    End If
End If

If (ClnType = "") Then
' Tests
    If (ClinicalRecords.LocateStartingRecord("F")) Then
        Call GetTestResults(PatientId, AppointmentId, lstTests)
    Else
        lstTests.AddItem "Exam Elements:"
    End If
' Findings
    If (ClinicalRecords.LocateStartingRecord("Q")) Then
        Call LoadCurrentDiag(PatientId)
    End If
    ComplaintOn = False
    Set ClinicalRecords = Nothing
    Set ClinicalRecordsNotes = Nothing
' Load Test Notes
    Call LoadTestNotes
    LoadPrimaryInfo = True
End If
Exit Function
UI_ErrorHandler:
    ComplaintOn = False
    Resume LeaveFast
LeaveFast:
End Function

Private Function LoadMeds(AListbox As ListBox) As Boolean
Dim k As Long
Dim z As Integer
Dim i As Integer, u As Integer
Dim q As Integer, p As Integer
Dim IncludeOn As Boolean
Dim PostIt As Boolean
Dim MyTemp As String
Dim UrTemp As String
Dim Temp As String, AName As String
Dim NowDate As String
Dim DisplayText As String
Dim MyTestDate As Integer
Dim MyTestDateOn As String
Dim DisplayTestDate As String
Dim oDate As New CIODateTime
LoadMeds = False
oDate.MyDateTime = Now
If Not (EditPreviousOn) Then
    If (oDate.GetIODate > ActiveActivityDate) Then
        oDate.SetDateFromIO ActiveActivityDate
    End If
Else
    oDate.SetDateFromIO ActiveActivityDate
End If
lstMeds.Clear
lsthMeds.Clear
' this section gets any meds in any appointment
'
' For EPV
' It shows everything active up to and including the relative date
' As reflected in ImageDescriptor (ClinicalDescriptor)
' However for discontinued drugs on the same day,
' It will show them as being Active.
'
k = 1
Do Until Not (ClinicalRecords.RetrieveClinicalItem(k))
    If (ClinicalRecords.ClinicalType = "A") And (ClinicalRecords.ClinicalStatus = "A") Then
        MyTestDate = 2
        DisplayTestDate = ""
        MyTestDateOn = Mid(ActiveActivityDate, 5, 2) + "/" + Mid(ActiveActivityDate, 7, 2) + "/" + Left(ActiveActivityDate, 4)
        NowDate = Mid(MyTestDateOn, 7, 4) + Mid(MyTestDateOn, 1, 2) + Mid(MyTestDateOn, 4, 2)
        If (Trim(ClinicalRecords.ClinicalDescriptor) <> "") Then
            MyTestDate = InStrPS(ClinicalRecords.ClinicalDescriptor, MyTestDateOn)
            If (MyTestDate <> 0) Then
                DisplayTestDate = Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate, 10)
                NowDate = Mid(DisplayTestDate, 7, 4) + Mid(DisplayTestDate, 1, 2) + Mid(DisplayTestDate, 4, 2)
            Else
                MyTestDate = 2
                NowDate = Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate, 10)
                NowDate = Mid(NowDate, 7, 4) + Mid(NowDate, 1, 2) + Mid(NowDate, 4, 2)
            End If
        End If
'
' Rx Rules
'
' Plan (PL):
'   Drugs Prescribed in Current Visit (Ocular or NonOocular)
'   Drugs Touched in Current Visit (Ocular or Non-Ocular)
'       Exclude any Patient Discontined
'
' Ongoing (ON):
'   Any Patient Discontinued Drug
'   Non-Prescribed drugs whether ocular or non-ocular
'   Active Prescribed Drugs Only
'
        IncludeOn = False
        If (Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) = "P") And (oDate.GetIODate = NowDate) Then
            IncludeOn = True
        End If
        If ((Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) = "!") Or _
            (Trim(ClinicalRecords.ClinicalDescriptor) = "")) And _
           (InStrPS(ClinicalRecords.ClinicalFindings, "P-7") <> 0) And _
           (oDate.GetIODate <= NowDate) Then
            IncludeOn = True
            If ((EditPreviousOn) And (oDate.GetIODate = NowDate)) And _
                (Len(Trim(ClinicalRecords.ClinicalDescriptor)) < 12) Then
                IncludeOn = False
            End If
        End If
        If ((Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) = "!") Or _
            (Trim(ClinicalRecords.ClinicalDescriptor) = "")) And _
           (oDate.GetIODate >= NowDate) Then
            IncludeOn = True
            If ((EditPreviousOn) And (oDate.GetIODate = NowDate)) And _
                (Len(Trim(ClinicalRecords.ClinicalDescriptor)) < 12) And _
                (InStrPS(ClinicalRecords.ClinicalFindings, "P-7") <> 0) Then
                IncludeOn = False
            End If
        End If
        If (Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) = "R") And _
           (oDate.GetIODate <= NowDate) Then
            IncludeOn = True
            If ((EditPreviousOn) And (oDate.GetIODate = NowDate)) And (Len(Trim(ClinicalRecords.ClinicalDescriptor)) < 12) Then
                IncludeOn = False
            End If
        End If
        If (InStrPS(ClinicalRecords.ClinicalFindings, "P-7") = 0) And _
           ((Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) = "C")) And _
           (oDate.GetIODate = NowDate) Then
            IncludeOn = True
        End If
        If (InStrPS("!KR^", Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1)) <> 0) And _
           (oDate.GetIODate <= NowDate) And ((MyTestDate + 11) < Len(Trim(ClinicalRecords.ClinicalDescriptor))) Then
            IncludeOn = True
        End If
        If (AppointmentId = ClinicalRecords.ClinicalApptId) Then
            If (Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) <> "P") And _
               (InStrPS(ClinicalRecords.ClinicalFindings, "P-7") <> 0) Then
                IncludeOn = True
'                If (EditPreviousOn) Then
'                    IncludeOn = False
'                End If
            End If
        Else
            If (Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) <> "P") And _
               (InStrPS(ClinicalRecords.ClinicalFindings, "P-7") <> 0) And _
               (oDate.GetIODate = NowDate) Then
                IncludeOn = True
            End If
            If (Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) <> "!") And _
               (Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) <> "P") And _
               (oDate.GetIODate <= NowDate) Then
                IncludeOn = True
            End If
            If ((Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1) = "!") And _
               (Len(Trim(ClinicalRecords.ClinicalDescriptor)) < 12)) Or _
               (Trim(ClinicalRecords.ClinicalDescriptor) = "") Then
                IncludeOn = True
            End If
        End If
        If (IncludeOn) Then
            q = InStrPS(ClinicalRecords.ClinicalFindings, "-8/")
            If (q < 1) Then
                q = InStrPS(ClinicalRecords.ClinicalFindings, "-1/")
            End If
            If (q > 0) Then
                p = InStrPS(ClinicalRecords.ClinicalFindings, "-2/")
                If (p > 0) Then
                    DisplayText = Space(75)
                    AName = Trim(Mid(ClinicalRecords.ClinicalFindings, q + 3, (p - 1) - (q + 2)))
                    Call ReplaceCharacters(AName, "-1/", " ")
                    AName = Trim(AName)
                    Mid(DisplayText, 1, Len(AName)) = Trim(AName)
                    If (InStrPS("PD", Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1)) <> 0) Then
                        Mid(DisplayText, 36, 1) = Mid(ClinicalRecords.ClinicalDescriptor, MyTestDate - 1, 1)
                    End If
                    DisplayText = DisplayText + Trim(str(ClinicalRecords.ClinicalId))
                    If (ClinicalRecords.ClinicalApptDate = ActiveActivityDate) Then
                        lsthMeds.AddItem DisplayText
                    ElseIf (ClinicalRecords.ClinicalApptDate < ActiveActivityDate) Then
                        lsthMeds.AddItem "(H)" + DisplayText
                    End If
                End If
            End If
        End If
    ElseIf (ClinicalRecords.ClinicalType <> "A") Then
        Exit Do
    End If
    k = k + 1
Loop
    
' Med Notes
If (lsthMeds.ListCount > 0) Then
    If (ClinicalRecordsNotes.LocateStartingNoteRecord("A")) Then
        Temp = ""
        AName = ""
        k = 1
        Do Until Not (ClinicalRecordsNotes.RetrieveClinicalNoteItem(k))
            If (Trim(ClinicalRecordsNotes.ClinicalNotesOffDate) = "") And (Trim(ClinicalRecordsNotes.ClinicalNotesSystem) = "") And (ClinicalRecordsNotes.ClinicalNotesDate <= ActiveActivityDate) Then
                If (Trim(ClinicalRecordsNotes.ClinicalNotesText1) <> "") Then
                    If (Trim(Temp) <> "") Then
                        Temp = Temp + vbCrLf + Trim(ClinicalRecordsNotes.ClinicalNotesText1) + " " _
                             + Trim(ClinicalRecordsNotes.ClinicalNotesText2) + " " _
                             + Trim(ClinicalRecordsNotes.ClinicalNotesText3) + " " _
                             + Trim(ClinicalRecordsNotes.ClinicalNotesText4)
                    Else
                        Temp = Trim(ClinicalRecordsNotes.ClinicalNotesText1) + " " _
                             + Trim(ClinicalRecordsNotes.ClinicalNotesText2) + " " _
                             + Trim(ClinicalRecordsNotes.ClinicalNotesText3) + " " _
                             + Trim(ClinicalRecordsNotes.ClinicalNotesText4)
                    End If
                    Temp = Trim(Temp)
                End If
            Else
                Exit Do
            End If
            k = k + 1
        Loop
        Call ReplaceCharacters(Temp, Chr(13), "~")
        Call StripCharacters(Temp, Chr(10))
        Temp = Trim(Temp)
        If (Trim(Temp) <> "") Then
            If (Len(Trim(Temp)) < 30) Then
                lsthMeds.AddItem Trim(Temp)
            Else
                AName = Left(Temp, 30)
                If (Mid(AName, 30, 1) <> " ") Then
                    For i = 30 To 1 Step -1
                        If (Mid(AName, i, 1) = " ") Then
                            AName = Left(AName, i)
                            Exit For
                        End If
                    Next i
                End If
                lsthMeds.AddItem AName + ">"
            End If
        End If
    End If
End If

AListbox.Clear
AListbox.AddItem "MEDS:"
z = 0
u = lsthMeds.ListCount - 1
For i = 0 To u
    If (Trim(lsthMeds.List(i)) <> "") Then
        GoSub CheckIt
        If (PostIt) Then
            z = z + 1
            If (z < 7) Then
                AListbox.AddItem lsthMeds.List(i)
            End If
        End If
    End If
Next i
If (AListbox.ListCount < 2) Then
    AListbox.Clear
    AListbox.AddItem "MEDS:NONE"
End If
cmdMagnifyMeds.Visible = False
If (AListbox.ListCount > 5) Then
    cmdMagnifyMeds.Visible = True
End If
Set oDate = Nothing
Exit Function
CheckIt:
    PostIt = True
    MyTemp = lsthMeds.List(i)
    Call ReplaceCharacters(MyTemp, "(T)", "~")
    Call StripCharacters(MyTemp, "~")
    Call ReplaceCharacters(MyTemp, "(H)", "~")
    Call StripCharacters(MyTemp, "~")
    For q = 1 To AListbox.ListCount - 1
        UrTemp = AListbox.List(q)
        Call ReplaceCharacters(UrTemp, "(T)", "~")
        Call StripCharacters(UrTemp, "~")
        Call ReplaceCharacters(UrTemp, "(H)", "~")
        Call StripCharacters(UrTemp, "~")
        If (MyTemp = UrTemp) Then
            PostIt = False
            Exit For
        ElseIf (Trim(Left(MyTemp, 30)) = Trim(Left(UrTemp, 30))) Then
            PostIt = False
            Exit For
        End If
    Next q
    Return
End Function
Public Function FrmClose()
 Call cmdNone_Click
 Unload Me
 End Function
Private Function CheckPatientHistoryStatus(ByVal CurrentAppointmentDate As String, ByVal ComplaintId As Long)
On Error GoTo lCheckPatientHistoryStatus_Error
Dim StrSql As String
Dim RS As Recordset
Dim AppointmentDate As String

CheckPatientHistoryStatus = False
StrSql = " Select TOP 1 AppDate from dbo.Appointments WHERE AppointmentID  IN(Select TOP 1 AppointmentId from dbo.PatientHistoryStatus phs WHERE phs.ComplaintId = " & ComplaintId & ")"
Set RS = GetRS(StrSql)
If RS Is Nothing Then Exit Function
If (RS.RecordCount > 0) Then
    AppointmentDate = RS("AppDate")
    If (CurrentAppointmentDate < AppointmentDate) Then
        CheckPatientHistoryStatus = True
    End If
End If
Exit Function
lCheckPatientHistoryStatus_Error:
    CheckPatientHistoryStatus = False
End Function