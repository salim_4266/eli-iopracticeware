VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmExtProcs 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9150
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12120
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "ExtProcs.frx":0000
   ScaleHeight     =   9150
   ScaleWidth      =   12120
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtDr8 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   34
      Top             =   4440
      Width           =   3255
   End
   Begin VB.TextBox txtDr7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   33
      Top             =   3960
      Width           =   3255
   End
   Begin VB.TextBox txtDr6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   32
      Top             =   3480
      Width           =   3255
   End
   Begin VB.TextBox txtDr5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   31
      Top             =   3000
      Width           =   3255
   End
   Begin VB.TextBox txtDr4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   30
      Top             =   2520
      Width           =   3255
   End
   Begin VB.TextBox txtDr3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   29
      Top             =   2040
      Width           =   3255
   End
   Begin VB.TextBox txtDr2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   28
      Top             =   1560
      Width           =   3255
   End
   Begin VB.TextBox txtDr1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8520
      TabIndex        =   27
      Top             =   1080
      Width           =   3255
   End
   Begin VB.TextBox txtProc8 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      TabIndex        =   26
      Top             =   4440
      Width           =   4695
   End
   Begin VB.TextBox txtProc7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      TabIndex        =   25
      Top             =   3960
      Width           =   4695
   End
   Begin VB.TextBox txtProc6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      TabIndex        =   24
      Top             =   3480
      Width           =   4695
   End
   Begin VB.TextBox txtProc5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      TabIndex        =   23
      Top             =   3000
      Width           =   4695
   End
   Begin VB.TextBox txtProc4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      TabIndex        =   22
      Top             =   2520
      Width           =   4695
   End
   Begin VB.TextBox txtProc3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      TabIndex        =   21
      Top             =   2040
      Width           =   4695
   End
   Begin VB.TextBox txtProc2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      TabIndex        =   20
      Top             =   1560
      Width           =   4695
   End
   Begin VB.TextBox txtProc1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      TabIndex        =   19
      Top             =   1080
      Width           =   4695
   End
   Begin VB.TextBox txtEye8 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   18
      Top             =   4440
      Width           =   735
   End
   Begin VB.TextBox txtEye7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   17
      Top             =   3960
      Width           =   735
   End
   Begin VB.TextBox txtEye6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   16
      Top             =   3480
      Width           =   735
   End
   Begin VB.TextBox txtEye5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   15
      Top             =   3000
      Width           =   735
   End
   Begin VB.TextBox txtEye4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   14
      Top             =   2520
      Width           =   735
   End
   Begin VB.TextBox txtEye3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   13
      Top             =   2040
      Width           =   735
   End
   Begin VB.TextBox txtEye2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   12
      Top             =   1560
      Width           =   735
   End
   Begin VB.TextBox txtEye1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   11
      Top             =   1080
      Width           =   735
   End
   Begin VB.TextBox txtDate8 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   10
      Top             =   4440
      Width           =   1335
   End
   Begin VB.TextBox txtDate7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   9
      Top             =   3960
      Width           =   1335
   End
   Begin VB.TextBox txtDate6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   8
      Top             =   3480
      Width           =   1335
   End
   Begin VB.TextBox txtDate5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   7
      Top             =   3000
      Width           =   1335
   End
   Begin VB.TextBox txtDate4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   6
      Top             =   2520
      Width           =   1335
   End
   Begin VB.TextBox txtDate3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   5
      Top             =   2040
      Width           =   1335
   End
   Begin VB.TextBox txtDate2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   4
      Top             =   1560
      Width           =   1335
   End
   Begin VB.TextBox txtDate1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      TabIndex        =   3
      Top             =   1080
      Width           =   1335
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1080
      Left            =   10440
      TabIndex        =   0
      Top             =   7800
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExtProcs.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   1080
      Left            =   960
      TabIndex        =   1
      Top             =   7800
      Width           =   1395
      _Version        =   131072
      _ExtentX        =   2461
      _ExtentY        =   1905
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ExtProcs.frx":38A8
   End
   Begin VB.Label lblDr 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   8520
      TabIndex        =   38
      Top             =   720
      Width           =   750
   End
   Begin VB.Label lblProc 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "External Procedure"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3600
      TabIndex        =   37
      Top             =   720
      Width           =   2145
   End
   Begin VB.Label lblEye 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Eye"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   2640
      TabIndex        =   36
      Top             =   720
      Width           =   420
   End
   Begin VB.Label lblDate 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   960
      TabIndex        =   35
      Top             =   720
      Width           =   525
   End
   Begin VB.Label lblPat 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Patient Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   960
      TabIndex        =   2
      Top             =   240
      Width           =   1515
   End
End
Attribute VB_Name = "frmExtProcs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TheResults As Boolean
Private SystemReference As String
Private AppointmentId As Long
Private PatientId As Long
Private CurrentCol As Integer
Private CurrentRow As Integer

Private Sub cmdDone_Click()
Call PostNotes
Unload frmExtProcs
End Sub

Private Sub cmdHome_Click()
Unload frmExtProcs
End Sub

Private Function PostNotes() As Boolean
Dim i As Integer
Dim UDate As String
Dim UserId As String
Dim AEye(8) As String
Dim ADate(8) As String
Dim AProc(8) As String
Dim ADocs(8) As String
Dim RetNotes As PatientNotes
Dim RetrieveResource As SchedulerResource
Set RetrieveResource = New SchedulerResource
RetrieveResource.ResourceId = UserLogin.iId
If (RetrieveResource.RetrieveSchedulerResource) Then
    UserId = Trim(RetrieveResource.ResourceName)
End If
Set RetrieveResource = Nothing
UDate = ""
Call FormatTodaysDate(UDate, False)
UDate = Mid(UDate, 7, 4) + Mid(UDate, 1, 2) + Mid(UDate, 4, 2)
AEye(1) = Trim(txtEye1.Text)
AEye(2) = Trim(txtEye2.Text)
AEye(3) = Trim(txtEye3.Text)
AEye(4) = Trim(txtEye4.Text)
AEye(5) = Trim(txtEye5.Text)
AEye(6) = Trim(txtEye6.Text)
AEye(7) = Trim(txtEye7.Text)
AEye(8) = Trim(txtEye8.Text)
ADate(1) = Trim(txtDate1.Text)
ADate(2) = Trim(txtDate2.Text)
ADate(3) = Trim(txtDate3.Text)
ADate(4) = Trim(txtDate4.Text)
ADate(5) = Trim(txtDate5.Text)
ADate(6) = Trim(txtDate6.Text)
ADate(7) = Trim(txtDate7.Text)
ADate(8) = Trim(txtDate8.Text)
AProc(1) = Trim(txtProc1.Text)
AProc(2) = Trim(txtProc2.Text)
AProc(3) = Trim(txtProc3.Text)
AProc(4) = Trim(txtProc4.Text)
AProc(5) = Trim(txtProc5.Text)
AProc(6) = Trim(txtProc6.Text)
AProc(7) = Trim(txtProc7.Text)
AProc(8) = Trim(txtProc8.Text)
ADocs(1) = Trim(txtDr1.Text)
ADocs(2) = Trim(txtDr2.Text)
ADocs(3) = Trim(txtDr3.Text)
ADocs(4) = Trim(txtDr4.Text)
ADocs(5) = Trim(txtDr5.Text)
ADocs(6) = Trim(txtDr6.Text)
ADocs(7) = Trim(txtDr7.Text)
ADocs(8) = Trim(txtDr8.Text)
For i = 1 To 8
    If (AProc(i) <> "") Then
        Set RetNotes = New PatientNotes
        RetNotes.NotesId = 0
        If (RetNotes.RetrieveNotes) Then
            RetNotes.NotesType = "C"
            RetNotes.NotesAppointmentId = AppointmentId
            RetNotes.NotesPatientId = PatientId
            RetNotes.NotesAudioOn = False
            RetNotes.NotesCategory = ""
            RetNotes.NotesAlertMask = ""
            RetNotes.NotesClaimOn = False
            RetNotes.NotesCommentOn = False
            RetNotes.NotesDate = UDate
            RetNotes.NotesEye = AEye(i)
            RetNotes.NotesHighlight = "]"
            RetNotes.NotesILPNRef = ""
            RetNotes.NotesOffDate = ""
            RetNotes.NotesSystem = SystemReference
            RetNotes.NotesText1 = AProc(i) + " (" + ADocs(i) + ") [" + ADate(i) + "]"
            RetNotes.NotesText2 = ""
            RetNotes.NotesText3 = ""
            RetNotes.NotesText4 = ""
            RetNotes.NotesUser = UserId
            Call RetNotes.ApplyNotes
        End If
        Set RetNotes = Nothing
        TheResults = True
    End If
Next i
End Function

Public Function LoadExternalProcedures(PatId As Long, ApptId As Long, ASys As Integer) As Boolean
Dim RetPat As Patient
TheResults = False
LoadExternalProcedures = False
If (PatId > 0) And (ApptId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        lblPat.Caption = "External Procedures for " + Trim(RetPat.FirstName) + " " + Trim(RetPat.LastName)
    End If
    Set RetPat = Nothing
    AppointmentId = ApptId
    PatientId = PatId
    SystemReference = Trim(str(ASys))
    LoadExternalProcedures = True
End If
End Function

Private Function EnterDate() As String
EnterDate = ""
frmNumericPad.NumPad_Result = ""
frmNumericPad.NumPad_Field = "Date of Procedure"
frmNumericPad.NumPad_Max = 0
frmNumericPad.NumPad_Min = 0
frmNumericPad.NumPad_CommentOn = True
frmNumericPad.NumPad_EyesOn = False
frmNumericPad.NumPad_TimeFrameOn = False
frmNumericPad.NumPad_TimeFrame = ""
frmNumericPad.NumPad_DisplayFull = True
frmNumericPad.NumPad_ChoiceOn1 = False
frmNumericPad.NumPad_Choice1 = ""
frmNumericPad.NumPad_ChoiceOn2 = False
frmNumericPad.NumPad_Choice2 = ""
frmNumericPad.NumPad_ChoiceOn3 = False
frmNumericPad.NumPad_Choice3 = ""
frmNumericPad.NumPad_ChoiceOn4 = False
frmNumericPad.NumPad_Choice4 = ""
frmNumericPad.NumPad_ChoiceOn5 = False
frmNumericPad.NumPad_Choice5 = ""
frmNumericPad.NumPad_ChoiceOn6 = False
frmNumericPad.NumPad_Choice6 = ""
frmNumericPad.NumPad_ChoiceOn7 = False
frmNumericPad.NumPad_Choice7 = ""
frmNumericPad.NumPad_ChoiceOn8 = False
frmNumericPad.NumPad_Choice8 = ""
frmNumericPad.NumPad_ChoiceOn9 = False
frmNumericPad.NumPad_Choice9 = ""
frmNumericPad.NumPad_ChoiceOn10 = False
frmNumericPad.NumPad_Choice10 = ""
frmNumericPad.NumPad_ChoiceOn11 = False
frmNumericPad.NumPad_Choice11 = ""
frmNumericPad.NumPad_ChoiceOn12 = False
frmNumericPad.NumPad_Choice12 = ""
frmNumericPad.NumPad_ChoiceOn13 = False
frmNumericPad.NumPad_Choice13 = ""
frmNumericPad.NumPad_ChoiceOn14 = False
frmNumericPad.NumPad_Choice14 = ""
frmNumericPad.NumPad_ChoiceOn15 = False
frmNumericPad.NumPad_Choice15 = ""
frmNumericPad.NumPad_ChoiceOn16 = False
frmNumericPad.NumPad_Choice16 = ""
frmNumericPad.NumPad_ChoiceOn17 = False
frmNumericPad.NumPad_Choice17 = ""
frmNumericPad.NumPad_ChoiceOn18 = False
frmNumericPad.NumPad_Choice18 = ""
frmNumericPad.NumPad_ChoiceOn19 = False
frmNumericPad.NumPad_Choice19 = ""
frmNumericPad.NumPad_ChoiceOn20 = False
frmNumericPad.NumPad_Choice20 = ""
frmNumericPad.NumPad_ChoiceOn21 = False
frmNumericPad.NumPad_Choice21 = ""
frmNumericPad.NumPad_ChoiceOn22 = False
frmNumericPad.NumPad_Choice22 = ""
frmNumericPad.NumPad_ChoiceOn23 = False
frmNumericPad.NumPad_Choice23 = ""
frmNumericPad.NumPad_ChoiceOn24 = False
frmNumericPad.NumPad_Choice24 = ""
frmNumericPad.Show 1
If Not (frmNumericPad.NumPad_Quit) Then
    If (Trim(frmNumericPad.NumPad_Result) <> "") Then
        If (Len(Trim(frmNumericPad.NumPad_Result)) <> 10) Then
            frmEventMsgs.Header = "Dates must be entered MM/DD/YYYY"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            EnterDate = ""
        Else
            EnterDate = Trim(frmNumericPad.NumPad_Result)
        End If
    End If
End If
End Function

Private Function EnterText(IRef As Boolean) As String
EnterText = ""
frmAlphaPad.NumPad_Field = "External Procedure"
If (IRef) Then
    frmAlphaPad.NumPad_Field = "Doctor"
End If
frmAlphaPad.NumPad_DisplayFull = True
frmAlphaPad.Show 1
If (Trim(frmAlphaPad.NumPad_Result) <> "") Then
    EnterText = Trim(frmAlphaPad.NumPad_Result)
End If
End Function

Private Function EnterEye() As String
EnterEye = ""
frmEventMsgs.Header = "Which Eye ?"
frmEventMsgs.AcceptText = "OD"
frmEventMsgs.RejectText = "OS"
frmEventMsgs.CancelText = "OU"
frmEventMsgs.Other0Text = "NA"
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    EnterEye = "OD"
ElseIf (frmEventMsgs.Result = 2) Then
    EnterEye = "OS"
ElseIf (frmEventMsgs.Result = 4) Then
    EnterEye = "OU"
ElseIf (frmEventMsgs.Result = 3) Then
    EnterEye = ""
End If
End Function

Private Sub txtDate1_Click()
txtDate1.Text = EnterDate
End Sub

Private Sub txtDate1_KeyPress(KeyAscii As Integer)
Call txtDate1_Click
End Sub

Private Sub txtDate2_Click()
txtDate2.Text = EnterDate
End Sub

Private Sub txtDate2_KeyPress(KeyAscii As Integer)
Call txtDate2_Click
End Sub

Private Sub txtDate3_Click()
txtDate3.Text = EnterDate
End Sub

Private Sub txtDate3_KeyPress(KeyAscii As Integer)
Call txtDate3_Click
End Sub

Private Sub txtDate4_Click()
txtDate4.Text = EnterDate
End Sub

Private Sub txtDate4_KeyPress(KeyAscii As Integer)
Call txtDate4_Click
End Sub

Private Sub txtDate5_Click()
txtDate5.Text = EnterDate
End Sub

Private Sub txtDate5_KeyPress(KeyAscii As Integer)
Call txtDate5_Click
End Sub

Private Sub txtDate6_Click()
txtDate6.Text = EnterDate
End Sub

Private Sub txtDate6_KeyPress(KeyAscii As Integer)
Call txtDate6_Click
End Sub

Private Sub txtDate7_Click()
txtDate7.Text = EnterDate
End Sub

Private Sub txtDate7_KeyPress(KeyAscii As Integer)
Call txtDate7_Click
End Sub

Private Sub txtDate8_Click()
txtDate8.Text = EnterDate
End Sub

Private Sub txtDate8_KeyPress(KeyAscii As Integer)
Call txtDate8_Click
End Sub

Private Sub txtEye1_Click()
txtEye1.Text = EnterEye
End Sub

Private Sub txtEye1_KeyPress(KeyAscii As Integer)
Call txtEye1_Click
End Sub

Private Sub txtEye2_Click()
txtEye2.Text = EnterEye
End Sub

Private Sub txtEye2_KeyPress(KeyAscii As Integer)
Call txtEye2_Click
End Sub

Private Sub txtEye3_Click()
txtEye3.Text = EnterEye
End Sub

Private Sub txtEye3_KeyPress(KeyAscii As Integer)
Call txtEye3_Click
End Sub

Private Sub txtEye4_Click()
txtEye4.Text = EnterEye
End Sub

Private Sub txtEye4_KeyPress(KeyAscii As Integer)
Call txtEye4_Click
End Sub

Private Sub txtEye5_Click()
txtEye5.Text = EnterEye
End Sub

Private Sub txtEye5_KeyPress(KeyAscii As Integer)
Call txtEye5_Click
End Sub

Private Sub txtEye6_Click()
txtEye6.Text = EnterEye
End Sub

Private Sub txtEye6_KeyPress(KeyAscii As Integer)
Call txtEye6_Click
End Sub

Private Sub txtEye7_Click()
txtEye7.Text = EnterEye
End Sub

Private Sub txtEye7_KeyPress(KeyAscii As Integer)
Call txtEye7_Click
End Sub

Private Sub txtEye8_Click()
txtEye8.Text = EnterEye
End Sub

Private Sub txtEye8_KeyPress(KeyAscii As Integer)
Call txtEye8_Click
End Sub

Private Sub txtProc1_Click()
txtProc1.Text = EnterText(False)
End Sub

Private Sub txtProc1_KeyPress(KeyAscii As Integer)
Call txtProc1_Click
End Sub

Private Sub txtProc2_Click()
txtProc2.Text = EnterText(False)
End Sub

Private Sub txtProc2_KeyPress(KeyAscii As Integer)
Call txtProc2_Click
End Sub

Private Sub txtProc3_Click()
txtProc3.Text = EnterText(False)
End Sub

Private Sub txtProc3_KeyPress(KeyAscii As Integer)
Call txtProc3_Click
End Sub

Private Sub txtProc4_Click()
txtProc4.Text = EnterText(False)
End Sub

Private Sub txtProc4_KeyPress(KeyAscii As Integer)
Call txtProc4_Click
End Sub

Private Sub txtProc5_Click()
txtProc5.Text = EnterText(False)
End Sub

Private Sub txtProc5_KeyPress(KeyAscii As Integer)
Call txtProc5_Click
End Sub

Private Sub txtProc6_Click()
txtProc6.Text = EnterText(False)
End Sub

Private Sub txtProc6_KeyPress(KeyAscii As Integer)
Call txtProc6_Click
End Sub

Private Sub txtProc7_Click()
txtProc7.Text = EnterText(False)
End Sub

Private Sub txtProc7_KeyPress(KeyAscii As Integer)
Call txtProc7_Click
End Sub

Private Sub txtProc8_Click()
txtProc8.Text = EnterText(False)
End Sub

Private Sub txtProc8_KeyPress(KeyAscii As Integer)
Call txtProc8_Click
End Sub

Private Sub txtDr1_Click()
txtDr1.Text = EnterText(True)
End Sub

Private Sub txtDr1_KeyPress(KeyAscii As Integer)
Call txtDr1_Click
End Sub

Private Sub txtDr2_Click()
txtDr2.Text = EnterText(True)
End Sub

Private Sub txtDr2_KeyPress(KeyAscii As Integer)
Call txtDr2_Click
End Sub

Private Sub txtDr3_Click()
txtDr3.Text = EnterText(True)
End Sub

Private Sub txtDr3_KeyPress(KeyAscii As Integer)
Call txtDr3_Click
End Sub

Private Sub txtDr4_Click()
txtDr4.Text = EnterText(True)
End Sub

Private Sub txtDr4_KeyPress(KeyAscii As Integer)
Call txtDr4_Click
End Sub

Private Sub txtDr5_Click()
txtDr5.Text = EnterText(True)
End Sub

Private Sub txtDr5_KeyPress(KeyAscii As Integer)
Call txtDr5_Click
End Sub

Private Sub txtDr6_Click()
txtDr6.Text = EnterText(True)
End Sub

Private Sub txtDr6_KeyPress(KeyAscii As Integer)
Call txtDr6_Click
End Sub

Private Sub txtDr7_Click()
txtDr7.Text = EnterText(True)
End Sub

Private Sub txtDr7_KeyPress(KeyAscii As Integer)
Call txtDr7_Click
End Sub

Private Sub txtDr8_Click()
txtDr8.Text = EnterText(True)
End Sub

Private Sub txtDr8_KeyPress(KeyAscii As Integer)
Call txtDr8_Click
End Sub














