VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmAssignWear 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9420
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12720
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "AssignWear.frx":0000
   ScaleHeight     =   9420
   ScaleWidth      =   12720
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtMagnify 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   3000
      MultiLine       =   -1  'True
      TabIndex        =   57
      Top             =   3000
      Visible         =   0   'False
      Width           =   5775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLType7 
      Height          =   735
      Left            =   9240
      TabIndex        =   21
      Top             =   6720
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":36C9
   End
   Begin VB.TextBox txtNote8 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   51
      Top             =   7200
      Width           =   5775
   End
   Begin VB.ListBox lstHx 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   3435
      ItemData        =   "AssignWear.frx":38A8
      Left            =   240
      List            =   "AssignWear.frx":38AA
      TabIndex        =   43
      Top             =   480
      Visible         =   0   'False
      Width           =   11295
   End
   Begin VB.TextBox txtNote3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   24
      Top             =   7200
      Width           =   5775
   End
   Begin VB.TextBox txtNote1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   28
      Top             =   5160
      Width           =   5775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurRx3 
      Height          =   1095
      Left            =   8040
      TabIndex        =   16
      Top             =   360
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":38AC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurRx2 
      Height          =   1095
      Left            =   4080
      TabIndex        =   15
      Top             =   360
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":3A87
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurRx1 
      Height          =   1095
      Left            =   120
      TabIndex        =   14
      Top             =   360
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":3C62
   End
   Begin VB.ListBox lstBox 
      Height          =   450
      Left            =   10920
      Sorted          =   -1  'True
      TabIndex        =   11
      Top             =   0
      Visible         =   0   'False
      Width           =   975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10320
      TabIndex        =   0
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":3E3D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   975
      Left            =   6720
      TabIndex        =   12
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":4070
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLType1 
      Height          =   735
      Left            =   9240
      TabIndex        =   5
      Top             =   3840
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":42AE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLType5 
      Height          =   735
      Left            =   10560
      TabIndex        =   6
      Top             =   4560
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":448D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLType4 
      Height          =   735
      Left            =   10560
      TabIndex        =   3
      Top             =   3840
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":4670
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLType3 
      Height          =   735
      Left            =   9240
      TabIndex        =   2
      Top             =   5280
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":4855
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLType2 
      Height          =   735
      Left            =   9240
      TabIndex        =   1
      Top             =   4560
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":4A35
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLType6 
      Height          =   735
      Left            =   10560
      TabIndex        =   13
      Top             =   5280
      Visible         =   0   'False
      Width           =   1335
      _Version        =   131072
      _ExtentX        =   2355
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":4C14
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurRx7 
      Height          =   1095
      Left            =   120
      TabIndex        =   17
      Top             =   2520
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":4DF7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNewRx 
      Height          =   975
      Left            =   1920
      TabIndex        =   18
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":4FD2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurRx8 
      Height          =   1095
      Left            =   4080
      TabIndex        =   19
      Top             =   2520
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":5207
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurRx9 
      Height          =   1095
      Left            =   8040
      TabIndex        =   20
      Top             =   2520
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":53E2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImp 
      Height          =   975
      Left            =   8520
      TabIndex        =   23
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":55BD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurRx6 
      Height          =   1095
      Left            =   8040
      TabIndex        =   29
      Top             =   1440
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":57FD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurRx5 
      Height          =   1095
      Left            =   4080
      TabIndex        =   30
      Top             =   1440
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":59D8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCurRx4 
      Height          =   1095
      Left            =   120
      TabIndex        =   31
      Top             =   1440
      Width           =   3855
      _Version        =   131072
      _ExtentX        =   6800
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":5BB3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   120
      TabIndex        =   32
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":5D8E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCLType8 
      Height          =   735
      Left            =   9240
      TabIndex        =   33
      Top             =   6000
      Visible         =   0   'False
      Width           =   2655
      _Version        =   131072
      _ExtentX        =   4683
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":5FC1
   End
   Begin VB.TextBox txtNote4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   35
      Top             =   7200
      Width           =   5775
   End
   Begin VB.TextBox txtNote2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   26
      Top             =   5160
      Width           =   5775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHist 
      Height          =   975
      Left            =   3720
      TabIndex        =   37
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":61A0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   975
      Left            =   5400
      TabIndex        =   44
      Top             =   7920
      Width           =   1155
      _Version        =   131072
      _ExtentX        =   2037
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "AssignWear.frx":63D6
   End
   Begin VB.TextBox txtNote5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   45
      Top             =   5160
      Width           =   5775
   End
   Begin VB.TextBox txtNote7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   47
      Top             =   7200
      Width           =   5775
   End
   Begin VB.TextBox txtNote6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6120
      MaxLength       =   1020
      MultiLine       =   -1  'True
      TabIndex        =   49
      Top             =   5160
      Width           =   5775
   End
   Begin VB.Label lblRx8 
      BackColor       =   &H00AD8A46&
      Caption         =   "Prescription 4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1095
      Left            =   6120
      TabIndex        =   52
      Top             =   5880
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblNote1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Comment, Click to Enlarge"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   120
      TabIndex        =   42
      Top             =   4920
      Width           =   2190
   End
   Begin VB.Label lblTrial4 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      Caption         =   "Trial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   11505
      TabIndex        =   41
      Top             =   6960
      Width           =   360
   End
   Begin VB.Label lblTrial3 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      Caption         =   "Trial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5505
      TabIndex        =   40
      Top             =   6960
      Width           =   360
   End
   Begin VB.Label lblTrial2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      Caption         =   "Trial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   11505
      TabIndex        =   39
      Top             =   4920
      Width           =   360
   End
   Begin VB.Label lblTrial1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      Caption         =   "Trial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5505
      TabIndex        =   38
      Top             =   4920
      Width           =   360
   End
   Begin VB.Label lblNote4 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Comment, Click to Enlarge"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   6120
      TabIndex        =   36
      Top             =   6960
      Width           =   2190
   End
   Begin VB.Label lblRx4 
      BackColor       =   &H00999900&
      Caption         =   "Prescription 4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1095
      Left            =   6120
      TabIndex        =   34
      Top             =   5880
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblRx1 
      BackColor       =   &H00AD8A46&
      Caption         =   "Prescription 1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1095
      Left            =   120
      TabIndex        =   7
      Top             =   3840
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblRx3 
      BackColor       =   &H00AD8A46&
      Caption         =   "Prescription 3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1095
      Left            =   120
      TabIndex        =   9
      Top             =   5880
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblRx2 
      BackColor       =   &H00AD8A46&
      Caption         =   "Prescription 2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1095
      Left            =   6120
      TabIndex        =   8
      Top             =   3840
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Printing In Progress"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5520
      TabIndex        =   10
      Top             =   120
      Visible         =   0   'False
      Width           =   2280
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Assign A"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   1020
   End
   Begin VB.Label lblPRx1 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Today's prescriptions."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   120
      TabIndex        =   22
      Top             =   3600
      Width           =   2400
   End
   Begin VB.Label lblNote3 
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Comment, Click to Enlarge"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   120
      TabIndex        =   25
      Top             =   6960
      Width           =   825
   End
   Begin VB.Label lblNote2 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      Caption         =   "Comment, Click to Enlarge"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   6120
      TabIndex        =   27
      Top             =   4920
      Width           =   2190
   End
   Begin VB.Label lblRx5 
      BackColor       =   &H00999900&
      Caption         =   "Prescription 1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1095
      Left            =   120
      TabIndex        =   46
      Top             =   3840
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblRx7 
      BackColor       =   &H00999900&
      Caption         =   "Prescription 3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1095
      Left            =   120
      TabIndex        =   48
      Top             =   5880
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblRx6 
      BackColor       =   &H00999900&
      Caption         =   "Prescription 2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1095
      Left            =   6120
      TabIndex        =   50
      Top             =   3840
      Width           =   5775
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblTrial5 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      Caption         =   "Trial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5505
      TabIndex        =   54
      Top             =   4920
      Width           =   360
   End
   Begin VB.Label lblTrial7 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      Caption         =   "Trial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5505
      TabIndex        =   53
      Top             =   6960
      Width           =   360
   End
   Begin VB.Label lblTrial6 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      Caption         =   "Trial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   11505
      TabIndex        =   56
      Top             =   4920
      Width           =   360
   End
   Begin VB.Label lblTrial8 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      Caption         =   "Trial"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   11505
      TabIndex        =   55
      Top             =   6960
      Width           =   360
   End
End
Attribute VB_Name = "frmAssignWear"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PID As String
Public ActivityId As Long
Public PatientId As Long
Public AppointmentId As Long
Public ChangeOn As Boolean
Public EditPreviousVisit As Boolean

Private Const TrialOrderColor As Long = &HC00000
Private Const TrialNormalColor As Long = &HC0
Private Const EnlargeOff = &H77742D
Private Const EnlargeOn = &HFF&

Private NewWearOn As Boolean
Private CurrRx As Integer
Private CurrRxCnt As Integer
Private CurrentVendors As Integer
Private ThePrinter As String
Private TheManifestUsed As Integer
Private TheManifestResults As String
Private MaxManifests As Integer
Private WearType As String
Private Const MaxSetCL As Integer = 8
Private Type ExternalReference
    EWType As String
    EWCompany As String
    ODManifest As String
    OSManifest As String
    InventoryIdOD As Long
    InventoryIdOS As Long
    RxNote As String
End Type
Private EWType As String
Private EWCompany As String
Private TestQuestion As String
Private TestOrder As String
Private CurrentManifest(9) As ExternalReference
Private SelectedCLRx(MaxSetCL) As ExternalReference
Private SelectedPCRx(MaxSetCL) As ExternalReference
Private MaxCL As Integer
Private CurrentIndex As Integer
Private MaxCLType As Integer
Private CurrentTypeIndex As Integer
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub cmdCurRx1_Click()
Dim TempOD As String
Dim TempOS As String
TempOD = CurrentManifest(1).ODManifest
TempOS = CurrentManifest(1).OSManifest
TheManifestUsed = 1
TheManifestResults = TempOD + vbCrLf + TempOS
If (Len(TheManifestResults) > 2) Then
    Call cmdNewRx_Click
End If
End Sub

Private Sub cmdCurRx2_Click()
Dim TempOD As String
Dim TempOS As String
TempOD = CurrentManifest(2).ODManifest
TempOS = CurrentManifest(2).OSManifest
TheManifestUsed = 2
TheManifestResults = TempOD + vbCrLf + TempOS
If (Len(TheManifestResults) > 2) Then
    Call cmdNewRx_Click
End If
End Sub

Private Sub cmdCurRx3_Click()
Dim TempOD As String
Dim TempOS As String
TempOD = CurrentManifest(3).ODManifest
TempOS = CurrentManifest(3).OSManifest
TheManifestUsed = 3
TheManifestResults = TempOD + vbCrLf + TempOS
If (Len(TheManifestResults) > 2) Then
    Call cmdNewRx_Click
End If
End Sub

Private Sub cmdCurRx4_Click()
Dim TempOD As String
Dim TempOS As String
TempOD = CurrentManifest(4).ODManifest
TempOS = CurrentManifest(4).OSManifest
TheManifestUsed = 4
TheManifestResults = TempOD + vbCrLf + TempOS
If (Len(TheManifestResults) > 2) Then
    Call cmdNewRx_Click
End If
End Sub

Private Sub cmdCurRx5_Click()
Dim TempOD As String
Dim TempOS As String
TempOD = CurrentManifest(5).ODManifest
TempOS = CurrentManifest(5).OSManifest
TheManifestUsed = 5
TheManifestResults = TempOD + vbCrLf + TempOS
If (Len(TheManifestResults) > 2) Then
    Call cmdNewRx_Click
End If
End Sub

Private Sub cmdCurRx6_Click()
Dim TempOD As String
Dim TempOS As String
TempOD = CurrentManifest(6).ODManifest
TempOS = CurrentManifest(6).OSManifest
TheManifestUsed = 6
TheManifestResults = TempOD + vbCrLf + TempOS
If (Len(TheManifestResults) > 2) Then
    Call cmdNewRx_Click
End If
End Sub

Private Sub cmdCurRx7_Click()
Dim TempOD As String
Dim TempOS As String
TempOD = CurrentManifest(7).ODManifest
TempOS = CurrentManifest(7).OSManifest
TheManifestUsed = 7
TheManifestResults = TempOD + vbCrLf + TempOS
If (Len(TheManifestResults) > 2) Then
    Call cmdNewRx_Click
End If
End Sub

Private Sub cmdCurRx8_Click()
Dim TempOD As String
Dim TempOS As String
TempOD = CurrentManifest(8).ODManifest
TempOS = CurrentManifest(8).OSManifest
TheManifestUsed = 8
TheManifestResults = TempOD + vbCrLf + TempOS
If (Len(TheManifestResults) > 2) Then
    Call cmdNewRx_Click
End If
End Sub

Private Sub cmdCurRx9_Click()
Dim TempOD As String
Dim TempOS As String
TempOD = CurrentManifest(9).ODManifest
TempOS = CurrentManifest(9).OSManifest
TheManifestUsed = 9
TheManifestResults = TempOD + vbCrLf + TempOS
If (Len(TheManifestResults) > 2) Then
    Call cmdNewRx_Click
End If
End Sub

Private Sub cmdHist_Click()
    Call ControlHistory(False)
    lstHx.Clear
    Call DisplayHistory
End Sub

Private Sub cmdImp_Click()
Dim LDate As String
Dim ApptId As Long
Dim PatId As Long
Dim RetrievalActivity As PracticeActivity
PatId = PatientId
ApptId = 0
If (frmSystems1.EditPreviousOn) Then
    ApptId = AppointmentId
    PatId = PatientId
Else
    LDate = Mid(frmSystems1.txtTime.Text, 7, 4) + Mid(frmSystems1.txtTime.Text, 1, 2) + Mid(frmSystems1.txtTime.Text, 4, 2)
    Set RetrievalActivity = New PracticeActivity
    RetrievalActivity.ActivityDate = LDate
    RetrievalActivity.ActivityPatientId = PatId
    RetrievalActivity.ActivityStatus = "DH"
    If (RetrievalActivity.RetrievePreviousActivity) Then
        ApptId = RetrievalActivity.ActivityAppointmentId
    End If
    Set RetrievalActivity = Nothing
End If
If (ApptId > 0) And (PatId > 0) Then
    frmImpression.EditPreviousOn = frmSystems1.EditPreviousOn
    frmImpression.ActivityId = ActivityId
    frmImpression.PatientId = PatId
    frmImpression.AppointmentId = ApptId
    If (frmImpression.LoadImpression(True)) Then
        frmImpression.Show 1
    Else
        frmEventMsgs.Header = "No Impressions/Plans"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    frmEventMsgs.Header = "No Impressions/Plans"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdMore_Click()
CurrRx = CurrRx + 4
If (CurrRx > CurrRxCnt) Then
    CurrRx = 1
End If
Call LoadWear(False, WearType)
End Sub

Private Sub cmdPrint_Click()
Dim i As Integer
Dim j As Integer
Dim Prtj As Integer
If (WearType = "PC") Then
    j = TotalPCRx
Else
    j = TotalCLRx
End If
Prtj = 1
For i = 1 To j
    If (WearType = "PC") Then
        If (Trim(SelectedPCRx(i).ODManifest) <> "") Or (Trim(SelectedPCRx(i).OSManifest) <> "") Then
            If (i = 1) And (j = 1) Then
                Call DoEyeWearPrint(i, True, True, j)
            ElseIf (i = 1) Then
                Call DoEyeWearPrint(i, True, False, j)
            ElseIf (i = j) Then
                Call DoEyeWearPrint(i, False, True, j)
            ElseIf (i <> j) Then
                Call DoEyeWearPrint(i, False, False, j)
            End If
        End If
    Else
        If (Trim(SelectedCLRx(i).ODManifest) <> "") Or (Trim(SelectedCLRx(i).OSManifest) <> "") Then
            If (i = 1) And (j = 1) Then
                If (DoEyeWearPrint(i, True, True, Prtj)) Then
                    Prtj = Prtj + 1
                End If
            ElseIf (i = 1) Then
                If (DoEyeWearPrint(i, True, False, Prtj)) Then
                    Prtj = Prtj + 1
                End If
            ElseIf (i = j) Then
                If (DoEyeWearPrint(i, False, True, Prtj)) Then
                    Prtj = Prtj + 1
                End If
            ElseIf (i <> j) Then
                If (DoEyeWearPrint(i, False, False, Prtj)) Then
                    Prtj = Prtj + 1
                End If
            End If
        End If
    End If
Next i
End Sub

Private Sub cmdHome_Click()
Unload frmAssignWear
End Sub

Private Sub cmdDone_Click()
If (cmdCLType1.Visible) Then
    Call cmdCLType8_Click
Else
    Unload frmAssignWear
End If
End Sub

Private Function LoadCLType(IRef As String, Init As Boolean) As Boolean
Dim i As Integer
Dim Ref As Integer
Dim Temp1 As String
Dim PCode As PracticeCodes
LoadCLType = False
MaxCLType = 0
cmdDone.Visible = False
cmdNewRx.Visible = False
cmdPrint.Visible = False
cmdImp.Visible = False
cmdCurRx1.Visible = False
cmdCurRx2.Visible = False
cmdCurRx3.Visible = False
cmdCurRx4.Visible = False
cmdCurRx5.Visible = False
cmdCurRx6.Visible = False
cmdCurRx7.Visible = False
cmdCurRx8.Visible = False
cmdCurRx9.Visible = False
Call ClearRxs
Call ClearEWType
lblPRx1.Visible = False
Set PCode = New PracticeCodes
If (IRef = "CL") Then
    PCode.ReferenceType = "CLMATERIALS"
Else
    PCode.ReferenceType = "PCCHOICES"
End If
MaxCLType = PCode.FindCode
If (CurrentTypeIndex >= MaxCLType) Then
    CurrentTypeIndex = 1
Else
    CurrentTypeIndex = CurrentTypeIndex + 1
End If
If (Init) Then
    CurrentTypeIndex = 1
End If
If (MaxCLType > 0) Then
    i = 1
    Ref = CurrentTypeIndex
    While (PCode.SelectCode(Ref)) And (i < 7)
        Temp1 = Trim(PCode.ReferenceCode)
        If (i = 1) Then
            cmdCLType1.Text = Temp1
            cmdCLType1.Visible = True
            cmdCLType1.BackColor = BaseBackColor
            cmdCLType1.ForeColor = BaseTextColor
        ElseIf (i = 2) Then
            cmdCLType2.Text = Temp1
            cmdCLType2.Visible = True
            cmdCLType2.BackColor = BaseBackColor
            cmdCLType2.ForeColor = BaseTextColor
        ElseIf (i = 3) Then
            cmdCLType3.Text = Temp1
            cmdCLType3.Visible = True
            cmdCLType3.BackColor = BaseBackColor
            cmdCLType3.ForeColor = BaseTextColor
        ElseIf (i = 4) Then
            cmdCLType4.Text = Temp1
            cmdCLType4.Visible = True
            cmdCLType4.BackColor = BaseBackColor
            cmdCLType4.ForeColor = BaseTextColor
        ElseIf (i = 5) Then
            cmdCLType5.Text = Temp1
            cmdCLType5.Visible = True
            cmdCLType5.BackColor = BaseBackColor
            cmdCLType5.ForeColor = BaseTextColor
        ElseIf (i = 6) Then
            cmdCLType6.Text = Temp1
            cmdCLType6.Visible = True
            cmdCLType6.BackColor = BaseBackColor
            cmdCLType6.ForeColor = BaseTextColor
        End If
        i = i + 1
        Ref = Ref + 1
        CurrentTypeIndex = CurrentTypeIndex + 1
    Wend
    CurrentTypeIndex = CurrentTypeIndex - 1
    If (MaxCLType > 6) Then
        cmdCLType7.Visible = True
    Else
        cmdCLType7.Visible = False
    End If
    cmdCLType8.Visible = True
    LoadCLType = True
End If
Set PCode = Nothing
If Not (LoadCLType) Then
    Call LoadWear(False, WearType)
End If
End Function

Private Sub ClearEWType()
cmdCLType1.Visible = False
cmdCLType1.BackColor = BaseBackColor
cmdCLType1.ForeColor = BaseTextColor
cmdCLType2.Visible = False
cmdCLType2.BackColor = BaseBackColor
cmdCLType2.ForeColor = BaseTextColor
cmdCLType3.Visible = False
cmdCLType3.BackColor = BaseBackColor
cmdCLType3.ForeColor = BaseTextColor
cmdCLType4.Visible = False
cmdCLType4.BackColor = BaseBackColor
cmdCLType4.ForeColor = BaseTextColor
cmdCLType5.Visible = False
cmdCLType5.BackColor = BaseBackColor
cmdCLType5.ForeColor = BaseTextColor
cmdCLType6.Visible = False
cmdCLType6.BackColor = BaseBackColor
cmdCLType6.ForeColor = BaseTextColor
cmdCLType7.Visible = False
cmdCLType7.BackColor = BaseBackColor
cmdCLType7.ForeColor = BaseTextColor
cmdCLType8.Visible = False
cmdCLType8.BackColor = BaseBackColor
cmdCLType8.ForeColor = BaseTextColor
lblPRx1.Visible = True
cmdDone.Visible = True
cmdNewRx.Visible = True
cmdPrint.Visible = True
cmdImp.Visible = True
If (Left(cmdCurRx1.Text, 2) <> "No") Then
    cmdCurRx1.Visible = True
End If
If (Left(cmdCurRx2.Text, 2) <> "No") Then
    cmdCurRx2.Visible = True
End If
If (Left(cmdCurRx3.Text, 2) <> "No") Then
    cmdCurRx3.Visible = True
End If
If (Left(cmdCurRx4.Text, 2) <> "No") Then
    cmdCurRx4.Visible = True
End If
If (Left(cmdCurRx5.Text, 2) <> "No") Then
    cmdCurRx5.Visible = True
End If
If (Left(cmdCurRx6.Text, 2) <> "No") Then
    cmdCurRx6.Visible = True
End If
If (Left(cmdCurRx7.Text, 2) <> "No") Then
    cmdCurRx7.Visible = True
End If
If (Left(cmdCurRx8.Text, 2) <> "No") Then
    cmdCurRx8.Visible = True
End If
If (Left(cmdCurRx9.Text, 2) <> "No") Then
    cmdCurRx9.Visible = True
End If
End Sub

Private Sub cmdCLType1_Click()
Call TriggerEWType(cmdCLType1)
End Sub

Private Sub cmdCLType2_Click()
Call TriggerEWType(cmdCLType2)
End Sub

Private Sub cmdCLType3_Click()
Call TriggerEWType(cmdCLType3)
End Sub

Private Sub cmdCLType4_Click()
Call TriggerEWType(cmdCLType4)
End Sub

Private Sub cmdCLType5_Click()
Call TriggerEWType(cmdCLType5)
End Sub

Private Sub cmdCLType6_Click()
Call TriggerEWType(cmdCLType6)
End Sub

Private Sub cmdCLType8_Click()
Call TriggerEWType(cmdCLType8)
End Sub

Private Sub cmdCLType7_Click()
Call LoadCLType(WearType, False)
End Sub

Private Sub TriggerEWType(AButton As fpBtn)
ChangeOn = True
EWType = AButton.Text
If (WearType = "CL") Then
    SelectedCLRx(CurrentIndex).EWType = EWType
    If (CurrentIndex = 1) Then
        SelectedCLRx(CurrentIndex).RxNote = Trim(txtNote1.Text)
    ElseIf (CurrentIndex = 2) Then
        SelectedCLRx(CurrentIndex).RxNote = Trim(txtNote2.Text)
    ElseIf (CurrentIndex = 3) Then
        SelectedCLRx(CurrentIndex).RxNote = Trim(txtNote3.Text)
    End If
Else
    SelectedPCRx(CurrentIndex).EWType = EWType
    If (CurrentIndex = 1) Then
        SelectedPCRx(CurrentIndex).RxNote = Trim(txtNote1.Text)
    ElseIf (CurrentIndex = 2) Then
        SelectedPCRx(CurrentIndex).RxNote = Trim(txtNote2.Text)
    ElseIf (CurrentIndex = 3) Then
        SelectedPCRx(CurrentIndex).RxNote = Trim(txtNote3.Text)
    End If
End If
Call LoadWear(False, WearType)
End Sub

Private Function LoadCL(IRef As String) As Boolean
LoadCL = True
lblPRx1.Visible = False
cmdDone.Visible = False
cmdNewRx.Visible = False
cmdPrint.Visible = False
cmdImp.Visible = False
cmdCurRx1.Visible = False
cmdCurRx2.Visible = False
cmdCurRx3.Visible = False
cmdCurRx4.Visible = False
cmdCurRx5.Visible = False
cmdCurRx6.Visible = False
cmdCurRx7.Visible = False
cmdCurRx8.Visible = False
cmdCurRx9.Visible = False
Call ClearRxs
End Function

Private Sub ClearRxs()
lblRx1.Caption = ""
lblRx1.Visible = False
lblNote1.Visible = False
lblNote1.BackColor = EnlargeOff
txtNote1.Text = ""
txtNote1.Visible = False
lblRx2.Caption = ""
lblRx2.Visible = False
lblNote2.Visible = False
lblNote2.BackColor = EnlargeOff
txtNote2.Text = ""
txtNote2.Visible = False
lblRx3.Caption = ""
lblRx3.Visible = False
lblNote3.Visible = False
lblNote3.BackColor = EnlargeOff
txtNote3.Text = ""
txtNote3.Visible = False
lblRx4.Caption = ""
lblRx4.Visible = False
lblNote4.Visible = False
lblNote4.BackColor = EnlargeOff
txtNote4.Text = ""
txtNote4.Visible = False
lblRx5.Caption = ""
lblRx5.Visible = False
txtNote5.Visible = False
txtNote5.Text = ""
lblRx6.Caption = ""
lblRx6.Visible = False
txtNote6.Visible = False
txtNote6.Text = ""
lblRx7.Caption = ""
lblRx7.Visible = False
txtNote7.Visible = False
txtNote7.Text = ""
lblRx8.Caption = ""
lblRx8.Visible = False
txtNote8.Visible = False
txtNote8.Text = ""
lblTrial1.Visible = False
lblTrial1.Caption = "Trial, Click to Order"
lblTrial1.BackColor = TrialNormalColor
lblTrial2.Visible = False
lblTrial2.Caption = "Trial, Click to Order"
lblTrial2.BackColor = TrialNormalColor
lblTrial3.Visible = False
lblTrial3.Caption = "Trial, Click to Order"
lblTrial3.BackColor = TrialNormalColor
lblTrial4.Visible = False
lblTrial4.Caption = "Trial, Click to Order"
lblTrial4.BackColor = TrialNormalColor
lblTrial5.Visible = False
lblTrial5.Caption = "Trial, Click to Order"
lblTrial5.BackColor = TrialNormalColor
lblTrial6.Visible = False
lblTrial6.Caption = "Trial, Click to Order"
lblTrial6.BackColor = TrialNormalColor
lblTrial7.Visible = False
lblTrial7.Caption = "Trial, Click to Order"
lblTrial7.BackColor = TrialNormalColor
lblTrial8.Visible = False
lblTrial8.Caption = "Trial, Click to Order"
lblTrial8.BackColor = TrialNormalColor
End Sub

Private Sub cmdNewRx_Click()
Dim i As Integer
Dim j As Integer
j = 0
For i = 1 To MaxSetCL
    If (WearType <> "PC") Then
        If (Trim(SelectedCLRx(i).ODManifest) = "") And (Trim(SelectedCLRx(i).OSManifest) = "") Then
            j = i
            Exit For
        End If
    Else
        If (Trim(SelectedPCRx(i).ODManifest) = "") And (Trim(SelectedPCRx(i).OSManifest) = "") Then
            j = i
            Exit For
        End If
    End If
Next i
If (j > 0) Then
    Call DoTest(TestQuestion, TestOrder, j, False, True)
Else
    frmEventMsgs.Header = "Only " + Trim(str(MaxSetCL)) + " pairs allowed"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    Exit Sub
End If
End Sub

Public Function LoadWear(Init As Boolean, TheType As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim Ref As Integer
Dim i As Integer, k As Integer
Dim d As Integer, c As Integer
Dim w As Integer, f As Integer
Dim t As Integer, u As Integer
Dim a As Integer, b As Integer
Dim z1 As Integer, zz As Integer
Dim j As Integer, p As Integer, p1 As Integer
Dim AHigh As Boolean
Dim Temp As String
Dim MrxFrame(6) As String
Dim BaseManifest As String
Dim AType As String, Apo As Integer
Dim WType As String, Temp5 As String
Dim Temp1 As String, Temp2 As String
Dim Temp3 As String, Temp4 As String
Dim ATest1 As String, ATest2 As String
Dim ATest3 As String, ATest4 As String
Dim ATemp As String, TempText As String
Dim RInvId1 As Long, RInvId2 As Long
Dim Rx As String, RxOrder As String, RxDiag As String
Dim RxFreq As String, RxDose As String, RNote As String
Dim RetFollow As Boolean
Dim RetTest As DynamicClass
LoadWear = False
NewWearOn = CheckConfigCollection("NEWEYEWEAR") = "T"
MrxFrame(1) = "MRX (Dry 1)"
MrxFrame(2) = "MRX (Dry 2)"
MrxFrame(3) = "MRX (Dry 3)"
MrxFrame(4) = "MRX (Cyclo)"
MrxFrame(5) = "MRX (Over CL)"
MrxFrame(6) = "MRX (Over Glasses)"
Temp1 = ""
Temp2 = ""
Temp3 = ""
Temp4 = ""
Temp5 = ""
TheManifestUsed = 0
TheManifestResults = ""
WType = TheType
WearType = TheType
If (WearType <> "CL") Then
    WearType = "PC"
End If
If (WearType = "PC") Then
    Label1.Caption = "Spectacle RX"
Else
    Label1.Caption = "Contact Lens RX"
End If
CurrentIndex = 1
CurrentTypeIndex = 1
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = &H9B9626
BaseTextColor = &HFFFFFF
If (Init) Then
    ThePrinter = ""
    ChangeOn = False
    If (WearType = "PC") Then
        Call InitializePCRx
    ElseIf (WearType = "CL") Then
        Call InitializeCLRx
    End If
' Reload from Evaluations only on Init = True
    zz = 0
    For z1 = 1 To frmEvaluation.TotalActionEvaluation(True)
        Call frmEvaluation.GetActionEvaluation(z1, TempText, AHigh, RetFollow)
        ATemp = False
        If (UCase(Left(TempText, 9 + Len(WType))) = UCase("DISPENSE " + WType)) Then
            ATemp = True
        End If
        If (ATemp) Then
            u = InStrPS(TempText, "-9/")
            t = InStrPS(TempText, "-1/")
            i = InStrPS(TempText, "-2/")
            j = InStrPS(TempText, "-3/")
            f = InStrPS(TempText, "-4/")
            w = InStrPS(TempText, "-5/")
            a = InStrPS(TempText, "-6/")
            b = InStrPS(TempText, "-7/")
            If (i > 0) And (j > 0) Then
                zz = zz + 1
                Rx = Left(TempText, u - 1)
                If ((u + 3) <> t - (u + 3)) Then
                    Rx = Mid(TempText, u + 3, t - (u + 3))
                End If
                RxDiag = Mid(TempText, t + 3, i - (t + 2))
                RxDiag = Left(RxDiag, Len(RxDiag) - 1)
                RxDose = Mid(TempText, i + 3, j - (i + 2))
                RxDose = Left(RxDose, Len(RxDose) - 1)
                RxFreq = Mid(TempText, j + 3, f - (j + 2))
                RxFreq = Left(RxFreq, Len(RxFreq) - 1)
                If (f > 0) And (w < 1) Then
                    w = Len(TempText)
                End If
                RNote = Mid(TempText, f + 3, w - (f + 2))
                RNote = Left(RNote, Len(RNote) - 1)
                If (a > 0) Then
                    RInvId1 = Val(Mid(TempText, w + 3, a - (w + 2)))
                    If (b > 0) Then
                        RInvId2 = Val(Mid(TempText, a + 3, b - (a + 2)))
                    End If
                End If
                If (WearType = "PC") Then
                    Call SetPC(zz, Rx, RxDiag, RxDose, RxFreq, RInvId1, RInvId2, RNote)
                Else
                    Call SetCL(zz, Rx, RxDiag, RxDose, RxFreq, RInvId1, RInvId2, RNote)
                End If
            End If
        End If
    Next z1
End If
' Setup Questions
Set RetTest = New DynamicClass
If (WearType = "PC") Then
    TestQuestion = "GLASSES-DISPENSE"
ElseIf (WearType = "CL") Then
    TestQuestion = "CONTACT LENSES-DISPENSE"
End If
RetTest.Question = TestQuestion
RetTest.QuestionParty = "G"
RetTest.QuestionSet = ""
RetTest.QuestionOrder = ""
If (RetTest.RetrieveClassbyQuestion) Then
    TestOrder = RetTest.QuestionOrder
End If
' Setup Tests
Set RetTest = Nothing
Set RetTest = New DynamicClass
RetTest.Question = "MANIFEST REFRACTION"
RetTest.QuestionParty = "T"
RetTest.QuestionSet = ""
RetTest.QuestionOrder = ""
If (RetTest.RetrieveClassbyQuestion) Then
    ATest1 = RetTest.QuestionOrder
End If
Set RetTest = Nothing
Set RetTest = New DynamicClass
If (WearType = "CL") Then
    RetTest.Question = "CONTACT LENSES"
Else
    RetTest.Question = "GLASSES"
End If
RetTest.QuestionParty = "T"
RetTest.QuestionSet = ""
RetTest.QuestionOrder = ""
If (RetTest.RetrieveClassbyQuestion) Then
    ATest2 = RetTest.QuestionOrder
End If
Set RetTest = Nothing
Set RetTest = New DynamicClass
RetTest.Question = "AUTOREFRACTION"
RetTest.QuestionParty = "T"
RetTest.QuestionSet = ""
RetTest.QuestionOrder = ""
If (RetTest.RetrieveClassbyQuestion) Then
    ATest3 = RetTest.QuestionOrder
End If
'
cmdCurRx1.Text = "No Manifest Taken"
cmdCurRx1.Visible = True
cmdCurRx2.Text = "No Manifest Taken"
cmdCurRx2.Visible = False
cmdCurRx3.Text = "No Manifest Taken"
cmdCurRx3.Visible = False
cmdCurRx4.Text = "No Manifest Taken"
cmdCurRx4.Visible = False
cmdCurRx5.Text = "No Manifest Taken"
cmdCurRx5.Visible = False
cmdCurRx6.Text = "No Manifest Taken"
cmdCurRx6.Visible = False
If (WearType = "PC") Then
    cmdCurRx7.Text = "No Glasses Taken"
    cmdCurRx7.Visible = False
    cmdCurRx8.Text = "No Glasses Taken"
    cmdCurRx8.Visible = False
Else
    cmdCurRx7.Text = "No Contacts Taken"
    cmdCurRx7.Visible = False
    cmdCurRx8.Text = "No Contacts Taken"
    cmdCurRx8.Visible = False
End If
cmdCurRx9.Text = "No AutoRefraction Taken"
cmdCurRx9.Visible = False

Temp1 = ""
Temp2 = ""
Temp3 = ""
Temp4 = ""
Temp5 = ""
i = frmSystems1.FindTestResults(ATest1)
If (i > 0) Then
    Call frmSystems1.GetTestText(i, Temp1, Temp2, Temp3, Temp4, Temp5, AHigh, AType, Apo)
'    cmdCurRx1.Text = Temp1
End If
BaseManifest = Temp1
For p1 = 1 To 6
    Ref = p1
    GoSub FrameMRX
    If (Trim(Temp1) <> "") Then
        If (p1 = 1) Then
            cmdCurRx1.Text = Temp1
            GoSub SetItUp
        ElseIf (p1 = 2) Then
            cmdCurRx2.Text = Temp1
            GoSub SetItUp
        ElseIf (p1 = 3) Then
            cmdCurRx3.Text = Temp1
            GoSub SetItUp
        ElseIf (p1 = 4) Then
            cmdCurRx4.Text = Temp1
            GoSub SetItUp
        ElseIf (p1 = 5) Then
            cmdCurRx5.Text = Temp1
            GoSub SetItUp
        ElseIf (p1 = 6) Then
            cmdCurRx6.Text = Temp1
            GoSub SetItUp
        End If
    End If
Next p1

Temp1 = ""
Temp2 = ""
Temp3 = ""
Temp4 = ""
Temp5 = ""
i = frmSystems1.FindTestResults(ATest2)
If (i > 0) Then
    Call frmSystems1.GetTestText(i, Temp1, Temp2, Temp3, Temp4, Temp5, AHigh, AType, Apo)
    cmdCurRx7.Text = Temp1
    cmdCurRx7.Visible = True
End If
Ref = 7
BaseManifest = Temp1
d = InStrPS(UCase(BaseManifest), "PAIR 2")
If (d > 0) Then
    Temp1 = Left(BaseManifest, d - 1)
    cmdCurRx7.Text = Temp1
    cmdCurRx7.Visible = True
    GoSub SetItUp
    MaxManifests = 7
    c = d
    d = InStrPS(UCase(BaseManifest), "Pair 3")
    If (d > 0) Then
        Ref = 8
        Temp1 = Mid(BaseManifest, c, d - (c - 1))
        cmdCurRx8.Text = Temp1
        cmdCurRx8.Visible = True
        GoSub SetItUp
        MaxManifests = 8
    Else
        Ref = 8
        Temp1 = Mid(BaseManifest, c, Len(BaseManifest) - (c - 1))
        cmdCurRx8.Text = Temp1
        cmdCurRx8.Visible = True
        GoSub SetItUp
        MaxManifests = 8
    End If
Else
    If (Trim(BaseManifest) <> "") Then
        Temp1 = BaseManifest
        GoSub SetItUp
        cmdCurRx8.Visible = True
        MaxManifests = 8
    End If
End If

Temp1 = ""
Temp2 = ""
Temp3 = ""
Temp4 = ""
Temp5 = ""
i = frmSystems1.FindTestResults(ATest3)
If (i > 0) Then
    Call frmSystems1.GetTestText(i, Temp1, Temp2, Temp3, Temp4, Temp5, AHigh, AType, Apo)
    cmdCurRx9.Text = Temp1
    cmdCurRx9.Visible = True
End If
Ref = 9
BaseManifest = Temp1
d = InStrPS(UCase(BaseManifest), "ARF-2")
If (d < 1) Then
    d = InStrPS(UCase(BaseManifest), "ARF 2")
End If
If (d > 0) Then
    Temp1 = Left(BaseManifest, d - 1)
    cmdCurRx9.Text = Temp1
    cmdCurRx9.Visible = True
    GoSub SetItUp
    MaxManifests = 9
Else
    If (Trim(BaseManifest) <> "") Then
        cmdCurRx9.Text = Temp1
        cmdCurRx9.Visible = True
        GoSub SetItUp
        MaxManifests = 9
    End If
End If

Set RetTest = Nothing
If (cmdCurRx1.Text = "No Manifest Taken") Then
    cmdCurRx1.Visible = False
End If
LoadWear = LoadEyeRx(Init)
cmdImp.Visible = Not EditPreviousVisit
Exit Function
FrameMRX:
    d = 0
    Temp1 = ""
    c = InStrPS(BaseManifest, MrxFrame(p1))
    If (c > 0) Then
        For p = p1 + 1 To 6
            d = InStrPS(BaseManifest, MrxFrame(p))
            If (d > 0) Then
                Exit For
            End If
        Next p
        If (c > 0) And (d > 0) Then
            Temp1 = Mid(BaseManifest, c, (d - 1) - (c - 1))
        ElseIf (c > 0) Then
            Temp1 = Mid(BaseManifest, c, Len(BaseManifest) - (c - 1))
        End If
    End If
    Return
SetItUp:
    CurrentManifest(Ref).EWCompany = ""
    CurrentManifest(Ref).EWType = ""
    i = InStrPS(Temp1, Chr(13) + Chr(10))
    If (i > 0) Then
        Temp2 = Mid(Temp1, i + 2, Len(Temp1) - (i + 1))
        i = InStrPS(Temp2, Chr(13) + Chr(10))
        If (i > 0) Then
            CurrentManifest(Ref).ODManifest = Left(Temp2, i - 1)
            k = InStrPS(i + 2, Temp2, Chr(13) + Chr(10))
            If (k > 0) Then
                CurrentManifest(Ref).OSManifest = Mid(Temp2, i + 2, (k - 1) - (i - 1))
            Else
                CurrentManifest(Ref).OSManifest = Mid(Temp2, i + 2, Len(Temp2) - (i - 1))
            End If
        Else
            CurrentManifest(Ref).ODManifest = Temp2
            CurrentManifest(Ref).OSManifest = ""
        End If
    Else
        CurrentManifest(Ref).ODManifest = Temp1
        CurrentManifest(Ref).OSManifest = ""
    End If
    Return
UI_ErrorHandler:
    LoadWear = False
    Call PinpointError("AssignWear", Err.Source + " " + Err.Description + " ApptId = " + Trim(str(AppointmentId)))
    Resume LeaveFast
LeaveFast:
End Function

Private Function LoadEyeRx(Init As Boolean) As Boolean
Dim i As Integer
Dim u As Integer
Dim st As Integer
Dim ed As Integer
Dim OrderOn As Boolean
Dim ATemp As String
Dim TheId As String
Dim TheNote As String
Dim TheDisplayName As String
Dim TrialOn As Boolean
Dim TrialOrderOn As Boolean
LoadEyeRx = False
OrderOn = False
If (Init) Then
    ChangeOn = False
    CurrRx = 1
    CurrRxCnt = 0
    For i = 1 To MaxSetCL
        If (WearType = "PC") Then
            If (Trim(SelectedPCRx(i).ODManifest) <> "") Or (Trim(SelectedPCRx(i).OSManifest) <> "") Then
                CurrRxCnt = CurrRxCnt + 1
            End If
        Else
            If (Trim(SelectedCLRx(i).ODManifest) <> "") Or (Trim(SelectedCLRx(i).OSManifest) <> "") Then
                CurrRxCnt = CurrRxCnt + 1
            End If
        End If
    Next i
End If
Call ClearEWType
Call ClearRxs
cmdMore.Visible = False
If (CurrRxCnt > 4) Then
    cmdMore.Visible = True
End If
cmdNewRx.Visible = True
st = CurrRx
ed = CurrRx + 3
For i = st To ed
    TrialOn = False
    TheId = ""
    If (WearType = "PC") Then
        If (Trim(SelectedPCRx(i).ODManifest) <> "") Or (Trim(SelectedPCRx(i).OSManifest) <> "") Then
            TheId = "OD: " + Trim(SelectedPCRx(i).ODManifest) + vbCrLf + "OS: " + Trim(SelectedPCRx(i).OSManifest)
            If (Trim(SelectedPCRx(i).EWType) <> "") Or (Trim(SelectedPCRx(i).EWCompany) <> "") Then
                TheId = TheId + vbCrLf + Trim(SelectedPCRx(i).EWType) + " " + Trim(SelectedPCRx(i).EWCompany)
                TheNote = Trim(SelectedPCRx(i).RxNote)
            End If
        End If
    Else
        If (Trim(SelectedCLRx(i).ODManifest) <> "") Or (Trim(SelectedCLRx(i).OSManifest) <> "") Then
            TheId = "OD: " + Trim(SelectedCLRx(i).ODManifest) + vbCrLf + "OS: " + Trim(SelectedCLRx(i).OSManifest)
            If (SelectedCLRx(i).InventoryIdOD > 0) Then
                Call GetInventoryItem(SelectedCLRx(i).InventoryIdOD, ATemp)
                TheId = TheId + vbCrLf + Trim(ATemp)
            End If
            If (SelectedCLRx(i).InventoryIdOS > 0) Then
                Call GetInventoryItem(SelectedCLRx(i).InventoryIdOS, ATemp)
                TheId = TheId + vbCrLf + Trim(ATemp)
            End If
            TheNote = Trim(SelectedCLRx(i).RxNote)
            If (Trim(TheNote) <> "") Then
                If (Mid(TheNote, 1, 1) = "*") Or (Mid(TheNote, 1, 1) = "~") Or (InStrPS(TheNote, "OD:") <> 0) Or (InStrPS(TheNote, "OS:") <> 0) Then
                    TrialOn = True
                    If (Mid(TheNote, 1, 1) = "^") Then
                        TrialOn = False
                    End If
                    TrialOrderOn = False
                    If (Mid(TheNote, 1, 1) = "~") Then
                        TrialOrderOn = True
                    End If
                    If (Mid(TheNote, 1, 1) = "*") Or (Mid(TheNote, 1, 1) = "~") Or (Mid(TheNote, 1, 1) = "^") Then
                        TheNote = Trim(Mid(TheNote, 2, Len(TheNote) - 1))
                    End If
                    u = InStrPS(TheNote, "OS:")
                    If (u > 0) Then
                        If (Mid(TheNote, u - 2, 2) <> vbCrLf) Then
                            TheNote = Left(TheNote, u - 1) + vbCrLf + Mid(TheNote, u, Len(TheNote) - (u - 1))
                        End If
                    End If
                End If
            End If
        End If
    End If
    If (Trim(TheId) <> "") Then
        If (i = 1) Then
            lblRx1.Caption = TheId
            lblRx1.Visible = True
            lblNote1.Visible = True
            If (IsNoteBig(TheNote)) Then
                lblNote1.BackColor = EnlargeOn
            End If
            txtNote1.Visible = True
            txtNote1.Text = TheNote
            lblTrial1.Caption = "Trial, Click to Order"
            lblTrial1.Visible = False
            If (TrialOn) Then
                lblTrial1.Visible = True
            End If
            lblTrial1.BackColor = TrialNormalColor
            If (TrialOrderOn) Then
                lblTrial1.BackColor = TrialOrderColor
                lblTrial1.Caption = "Order Trial, Click for Trial"
            End If
        ElseIf (i = 2) Then
            lblRx2.Caption = TheId
            lblRx2.Visible = True
            lblNote2.Visible = True
            If (IsNoteBig(TheNote)) Then
                lblNote2.BackColor = EnlargeOn
            End If
            txtNote2.Visible = True
            txtNote2.Text = TheNote
            lblTrial2.Caption = "Trial, Click to Order"
            lblTrial2.Visible = False
            If (TrialOn) Then
                lblTrial2.Visible = True
            End If
            lblTrial2.BackColor = TrialNormalColor
            If (TrialOrderOn) Then
                lblTrial2.Caption = "Order Trial, Click for Trial"
                lblTrial2.BackColor = TrialOrderColor
            End If
        ElseIf (i = 3) Then
            lblRx3.Caption = TheId
            lblRx3.Visible = True
            lblNote3.Visible = True
            If (IsNoteBig(TheNote)) Then
                lblNote3.BackColor = EnlargeOn
            End If
            txtNote3.Visible = True
            txtNote3.Text = TheNote
            lblTrial3.Caption = "Trial, Click to Order"
            lblTrial3.Visible = False
            If (TrialOn) Then
                lblTrial3.Visible = True
            End If
            lblTrial3.BackColor = TrialNormalColor
            If (TrialOrderOn) Then
                lblTrial3.Caption = "Order Trial, Click for Trial"
                lblTrial3.BackColor = TrialOrderColor
            End If
        ElseIf (i = 4) Then
            lblRx4.Caption = TheId
            lblRx4.Visible = True
            lblNote4.Visible = True
            If (IsNoteBig(TheNote)) Then
                lblNote4.BackColor = EnlargeOn
            End If
            txtNote4.Visible = True
            txtNote4.Text = TheNote
            lblTrial4.Caption = "Trial, Click to Order"
            lblTrial4.Visible = False
            If (TrialOn) Then
                lblTrial4.Visible = True
            End If
            lblTrial4.BackColor = TrialNormalColor
            If (TrialOrderOn) Then
                lblTrial4.Caption = "Order Trial, Click for Trial"
                lblTrial4.BackColor = TrialOrderColor
            End If
        ElseIf (i = 5) Then
            lblRx5.Caption = TheId
            lblRx5.Visible = True
            lblNote1.Visible = True
            If (IsNoteBig(TheNote)) Then
                lblNote1.BackColor = EnlargeOn
            End If
            txtNote5.Visible = True
            txtNote5.Text = TheNote
            lblTrial5.Caption = "Trial, Click to Order"
            lblTrial5.Visible = False
            If (TrialOn) Then
                lblTrial5.Visible = True
            End If
            lblTrial5.BackColor = TrialNormalColor
            If (TrialOrderOn) Then
                lblTrial5.Caption = "Order Trial, Click for Trial"
                lblTrial5.BackColor = TrialOrderColor
            End If
        ElseIf (i = 6) Then
            lblRx6.Caption = TheId
            lblRx6.Visible = True
            lblNote2.Visible = True
            If (IsNoteBig(TheNote)) Then
                lblNote2.BackColor = EnlargeOn
            End If
            txtNote6.Visible = True
            txtNote6.Text = TheNote
            lblTrial6.Caption = "Trial, Click to Order"
            lblTrial6.Visible = False
            If (TrialOn) Then
                lblTrial6.Visible = True
            End If
            lblTrial6.BackColor = TrialNormalColor
            If (TrialOrderOn) Then
                lblTrial6.Caption = "Order Trial, Click for Trial"
                lblTrial6.BackColor = TrialOrderColor
            End If
        ElseIf (i = 7) Then
            lblRx7.Caption = TheId
            lblRx7.Visible = True
            lblNote3.Visible = True
            If (IsNoteBig(TheNote)) Then
                lblNote3.BackColor = EnlargeOn
            End If
            txtNote7.Visible = True
            txtNote7.Text = TheNote
            lblTrial7.Caption = "Trial, Click to Order"
            lblTrial7.Visible = False
            If (TrialOn) Then
                lblTrial7.Visible = True
            End If
            lblTrial7.BackColor = TrialNormalColor
            If (TrialOrderOn) Then
                lblTrial7.Caption = "Order Trial, Click for Trial"
                lblTrial7.BackColor = TrialOrderColor
            End If
        ElseIf (i = 8) Then
            lblRx8.Caption = TheId
            lblRx8.Visible = True
            lblNote4.Visible = True
            If (IsNoteBig(TheNote)) Then
                lblNote4.BackColor = EnlargeOn
            End If
            txtNote8.Visible = True
            txtNote8.Text = TheNote
            lblTrial8.Caption = "Trial, Click to Order"
            lblTrial8.Visible = False
            If (TrialOn) Then
                lblTrial8.Visible = True
            End If
            lblTrial8.BackColor = TrialNormalColor
            If (TrialOrderOn) Then
                lblTrial8.Caption = "Order Trial, Click for Trial"
                lblTrial8.BackColor = TrialOrderColor
            End If
        End If
    End If
Next i
LoadEyeRx = True
End Function

Private Function PlaceCLRx(Ref As Integer, ARxT As String, ARxC As String, AOD As String, AOS As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
PlaceCLRx = False
If (Ref <= MaxSetCL) And (Ref > 0) Then
    PlaceCLRx = True
    SelectedCLRx(Ref).EWType = ARxT
    SelectedCLRx(Ref).EWCompany = ARxC
    SelectedCLRx(Ref).ODManifest = AOD
    SelectedCLRx(Ref).OSManifest = AOS
    SelectedCLRx(Ref).InventoryIdOD = AInvId1
    SelectedCLRx(Ref).InventoryIdOS = AInvId2
    SelectedCLRx(Ref).RxNote = ANote
    PlaceCLRx = True
End If
End Function

Private Function PlacePCRx(Ref As Integer, ARxT As String, ARxC As String, AOD As String, AOS As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
PlacePCRx = False
If (Ref <= MaxSetCL) And (Ref > 0) Then
    If (InStrPS(UCase(ARxT), "SPECTACLE") = 0) Then
        SelectedPCRx(Ref).EWCompany = ARxT
    Else
        SelectedPCRx(Ref).EWCompany = ""
    End If
        SelectedPCRx(Ref).EWType = ARxC
    SelectedPCRx(Ref).ODManifest = AOD
    SelectedPCRx(Ref).OSManifest = AOS
    SelectedPCRx(Ref).InventoryIdOD = AInvId1
    SelectedPCRx(Ref).InventoryIdOS = AInvId2
    SelectedPCRx(Ref).RxNote = ANote
    PlacePCRx = True
End If
End Function

Private Function SetCLRx(Ref As Integer, ARxT As String, ARxC As String, AOD As String, AOS As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
SetCLRx = False
ARxT = ""
ARxC = ""
AOD = ""
AOS = ""
AInvId1 = 0
AInvId2 = 0
ANote = ""
If (Ref <= MaxSetCL) Then
    If (Trim(SelectedCLRx(Ref).ODManifest) <> "") Or (Trim(SelectedCLRx(Ref).OSManifest) <> "") Then
        SetCLRx = True
        If (SelectedCLRx(Ref).InventoryIdOD = 0) And (SelectedCLRx(Ref).InventoryIdOS = 0) Then
            ARxT = SelectedCLRx(Ref).EWType
            ARxC = SelectedCLRx(Ref).EWCompany
        Else
            ARxT = ""
            ARxC = ""
        End If
        AOD = SelectedCLRx(Ref).ODManifest
        AOS = SelectedCLRx(Ref).OSManifest
        AInvId1 = SelectedCLRx(Ref).InventoryIdOD
        AInvId2 = SelectedCLRx(Ref).InventoryIdOS
        ANote = SelectedCLRx(Ref).RxNote
        SetCLRx = True
    End If
End If
End Function

Private Function SetPCRx(Ref As Integer, ARxT As String, ARxC As String, AOD As String, AOS As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
SetPCRx = False
ARxT = ""
ARxC = ""
AOD = ""
AOS = ""
AInvId1 = 0
AInvId2 = 0
ANote = ""
If (Ref <= TotalPCRx) Then
    If (Trim(SelectedPCRx(Ref).ODManifest) <> "") Or (Trim(SelectedPCRx(Ref).OSManifest) <> "") Then
        ARxT = SelectedPCRx(Ref).EWType
        ARxC = SelectedPCRx(Ref).EWCompany
        AOD = SelectedPCRx(Ref).ODManifest
        AOS = SelectedPCRx(Ref).OSManifest
        AInvId1 = SelectedPCRx(Ref).InventoryIdOD
        AInvId2 = SelectedPCRx(Ref).InventoryIdOS
        ANote = SelectedPCRx(Ref).RxNote
        SetPCRx = True
    End If
End If
End Function

Public Function InitializeCLRx() As Boolean
Dim i As Integer
InitializeCLRx = True
For i = 1 To MaxSetCL
    SelectedCLRx(i).EWType = ""
    SelectedCLRx(i).EWCompany = ""
    SelectedCLRx(i).ODManifest = ""
    SelectedCLRx(i).OSManifest = ""
    SelectedCLRx(i).InventoryIdOD = 0
    SelectedCLRx(i).InventoryIdOS = 0
    SelectedCLRx(i).RxNote = ""
Next i
End Function

Public Function InitializePCRx() As Boolean
Dim i As Integer
InitializePCRx = True
For i = 1 To MaxSetCL
    SelectedPCRx(i).EWType = ""
    SelectedPCRx(i).EWCompany = ""
    SelectedPCRx(i).ODManifest = ""
    SelectedPCRx(i).OSManifest = ""
    SelectedPCRx(i).InventoryIdOD = 0
    SelectedPCRx(i).InventoryIdOS = 0
    SelectedPCRx(i).RxNote = ""
Next i
End Function

Public Function TotalCLRx() As Integer
Dim i As Integer
TotalCLRx = 0
For i = 1 To MaxSetCL
    If (Trim(SelectedCLRx(i).ODManifest) = "") And (Trim(SelectedCLRx(i).OSManifest) = "") Then
        Exit For
    Else
        TotalCLRx = TotalCLRx + 1
    End If
Next i
End Function

Public Function TotalPCRx() As Integer
Dim i As Integer
Dim j As Integer
TotalPCRx = 0
For i = 1 To MaxSetCL
    If (Trim(SelectedPCRx(i).ODManifest) = "") And (Trim(SelectedPCRx(i).OSManifest) = "") Then
        Exit For
    Else
        TotalPCRx = TotalPCRx + 1
    End If
Next i
End Function

Public Function SetCL(RefId As Integer, ARxType As String, ARxCompany As String, AODManifest As String, AOSManifest As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
SetCL = PlaceCLRx(RefId, ARxType, ARxCompany, AODManifest, AOSManifest, AInvId1, AInvId2, ANote)
End Function

Public Function SetPC(RefId As Integer, ARxType As String, ARxCompany As String, AODManifest As String, AOSManifest As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
SetPC = PlacePCRx(RefId, ARxType, ARxCompany, AODManifest, AOSManifest, AInvId1, AInvId2, ANote)
End Function

Public Function GetCL(RefId As Integer, ARxType As String, ARxCompany As String, AODManifest As String, AOSManifest As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
GetCL = SetCLRx(RefId, ARxType, ARxCompany, AODManifest, AOSManifest, AInvId1, AInvId2, ANote)
End Function

Public Function GetPC(RefId As Integer, ARxType As String, ARxCompany As String, AODManifest As String, AOSManifest As String, AInvId1 As Long, AInvId2 As Long, ANote As String) As Boolean
GetPC = SetPCRx(RefId, ARxType, ARxCompany, AODManifest, AOSManifest, AInvId1, AInvId2, ANote)
End Function

Private Sub DoTest(Question As String, FileId As String, Ref As Integer, Style As Boolean, NewOn As Boolean)
Dim i As Integer
Dim ODT As String
Dim OST As String
Dim Temp As String
Dim TestTime As String
Dim AFile As String
Dim TheFile As String
Temp = ""
If (Ref > MaxSetCL) Or (Ref < 1) Then
    Exit Sub
End If
If (Trim(FileId) <> "") Then
    TheFile = DoctorInterfaceDirectory + "G" + FileId + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
    AFile = GetTestFileName("13A")
    If (TheManifestUsed = 7) Or (TheManifestUsed = 8) Then
        If (FileId = "01A") Then
            AFile = GetTestFileName("07A")
        Else
            AFile = GetTestFileName("08A")
        End If
    ElseIf (TheManifestUsed = 9) Then
        AFile = GetTestFileName("10A")
    End If
    If (FM.IsFileThere(AFile)) Then
        If (FileId = "01A") Then
            Call MakeMrxConvert(Question, "Glasses", TheManifestUsed, AFile, TheFile)
        Else
            Call MakeMrxConvert(Question, "ContactLens", TheManifestUsed, AFile, TheFile)
        End If
    End If
    If Not (FM.IsFileThere(TheFile)) Then
        If Not (Style) Then
            FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(1)
            Print #1, "QUESTION=\" + UCase(Trim(Question))
            Call FormatTimeNow(TestTime)
            Print #1, "TIME=" + TestTime
            FM.CloseFile CLng(1)
        End If
    End If
    If Not (NewWearOn) Or (NewOn) Then
        frmEditTests.ManifestResultsOn = True
        If (Trim(TheManifestResults) = "") Then
            frmEditTests.ManifestResultsOn = False
        End If
        frmEditTests.ManifestResults = TheManifestResults
        frmEditTests.StoreResults = TheFile
        frmEditTests.Question = UCase(Question)
        frmEditTests.PatientId = PatientId
        frmEditTests.AppointmentId = AppointmentId
        frmEditTests.FormOn = True
        frmAssignWear.Enabled = False
        frmEditTests.Show 1
        If (Trim(frmEditTests.StoreResults) = "") Or (frmEditTests.QuitOn) Then
            If (FM.IsFileThere(TheFile)) Then
                FM.Kill TheFile
            End If
            TheFile = ""
        End If
    End If
End If
frmAssignWear.ZOrder 0
frmAssignWear.Enabled = True
If (FM.IsFileThere(TheFile)) Then
    Call frmTestResults.PushList(TheFile)
    Call frmSystems1.ApplyTestResults(TheFile, FileId, Temp, False, True)
    If (Trim(Temp) <> "") Then
        If (InStrPS(Temp, "OD: " + Chr(13)) > 0) And (Mid(Temp, Len(Temp) - 3, 4) = "OS: ") Then
            If (frmEditTests.ManifestResultsOn) And Not (Style) Then
                Temp = frmEditTests.ManifestResults
                Temp = ":  " + Temp
            End If
        End If
    End If
ElseIf (frmEditTests.ManifestResultsOn) And Not (Style) Then
    Temp = frmEditTests.ManifestResults
    Temp = ":  " + Temp
Else
    Exit Sub
End If
If (Trim(Temp) <> "") Then
    i = InStrPS(Temp, ":")
    If (i > 0) Then
        Temp = Mid(Temp, i + 3, Len(Temp) - (i + 2))
    End If
    i = InStrPS(Temp, "OS")
    If (i > 0) Then
        ODT = Mid(Temp, 4, (i - 1) - 3)
        Call StripCharacters(ODT, Chr(13))
        Call StripCharacters(ODT, Chr(10))
        OST = Mid(Temp, i + 3, Len(Temp) - (i - 2))
        Call StripCharacters(OST, Chr(13))
        Call StripCharacters(OST, Chr(10))
    Else
        ODT = Temp
        OST = ""
    End If
    If (FM.IsFileThere(TheFile)) Then
        FM.Kill TheFile
    End If
    If (Len(ODT) > 2) Or (Len(OST) > 2) Then
        If (Ref = 1) Then
            lblRx1.Caption = Temp
            lblRx1.Visible = True
            lblNote1.Visible = True
            lblNote1.BackColor = EnlargeOff
            txtNote1.Text = ""
            txtNote1.Visible = True
        ElseIf (Ref = 2) Then
            lblRx2.Caption = Temp
            lblRx2.Visible = True
            lblNote2.Visible = True
            lblNote2.BackColor = EnlargeOff
            txtNote2.Text = ""
            txtNote2.Visible = True
        ElseIf (Ref = 3) Then
            lblRx3.Caption = Temp
            lblRx3.Visible = True
            lblNote3.Visible = True
            lblNote3.BackColor = EnlargeOff
            txtNote3.Text = ""
            txtNote3.Visible = True
        ElseIf (Ref = 4) Then
            lblRx4.Caption = Temp
            lblRx4.Visible = True
            lblNote4.Visible = True
            lblNote4.BackColor = EnlargeOff
            txtNote4.Text = ""
            txtNote4.Visible = True
        ElseIf (Ref = 5) Then
            lblRx5.Caption = Temp
            lblRx5.Visible = True
            lblNote1.Visible = True
            lblNote1.BackColor = EnlargeOff
            txtNote5.Text = ""
            txtNote5.Visible = True
        ElseIf (Ref = 6) Then
            lblRx6.Caption = Temp
            lblRx6.Visible = True
            lblNote2.Visible = True
            lblNote2.BackColor = EnlargeOff
            txtNote6.Text = ""
            txtNote6.Visible = True
        ElseIf (Ref = 7) Then
            lblRx7.Caption = Temp
            lblRx7.Visible = True
            lblNote3.Visible = True
            lblNote3.BackColor = EnlargeOff
            txtNote7.Text = ""
            txtNote7.Visible = True
        ElseIf (Ref = 8) Then
            lblRx8.Caption = Temp
            lblRx8.Visible = True
            lblNote4.Visible = True
            lblNote4.BackColor = EnlargeOff
            txtNote8.Text = ""
            txtNote8.Visible = True
        End If
        Call StripCharacters(ODT, Chr(10))
        Call StripCharacters(ODT, Chr(13))
        Call StripCharacters(OST, Chr(10))
        Call StripCharacters(OST, Chr(13))
        ChangeOn = True
        If (WearType = "PC") Then
            SelectedPCRx(Ref).ODManifest = ODT
            SelectedPCRx(Ref).OSManifest = OST
        Else
            SelectedCLRx(Ref).ODManifest = ODT
            SelectedCLRx(Ref).OSManifest = OST
        End If
        CurrentIndex = Ref
        If (WearType <> "PC") Then
            CurrRxCnt = TotalCLRx
        Else
            CurrRxCnt = TotalPCRx
        End If
        If (CurrRxCnt > 4) Then
            cmdMore.Visible = True
        Else
            cmdMore.Visible = False
        End If
        If Not (NewWearOn) Then
            CurrentVendors = 1
            If (WearType <> "PC") Then
'            Call LoadCL(WearType)
'            Call LoadCLType(WearType, True)
                If (Ref = 1) Then
                    Call TriggerRx(lblRx1, 1)
                ElseIf (Ref = 2) Then
                    Call TriggerRx(lblRx2, 2)
                ElseIf (Ref = 3) Then
                    Call TriggerRx(lblRx3, 3)
                ElseIf (Ref = 4) Then
                    Call TriggerRx(lblRx4, 4)
                ElseIf (Ref = 5) Then
                    Call TriggerRx(lblRx5, 5)
                ElseIf (Ref = 6) Then
                    Call TriggerRx(lblRx6, 6)
                ElseIf (Ref = 7) Then
                    Call TriggerRx(lblRx7, 7)
                ElseIf (Ref = 8) Then
                    Call TriggerRx(lblRx8, 8)
                End If
            ElseIf (WearType = "PC") Then
                Call LoadCL(WearType)
                Call LoadCLType(WearType, True)
            End If
        End If
    End If
End If
End Sub

Private Sub DoPrescriptions(Idx As Integer, RxOn As Boolean)
Dim ZTemp As String
If (Idx > 0) Then
    If (RxOn) Then
        If ((CurrRxCnt + 1) <= MaxSetCL) Then
            SelectedCLRx(CurrRxCnt + 1).EWCompany = SelectedCLRx(Idx).EWCompany
            SelectedCLRx(CurrRxCnt + 1).EWType = SelectedCLRx(Idx).EWType
            SelectedCLRx(CurrRxCnt + 1).InventoryIdOD = SelectedCLRx(Idx).InventoryIdOD
            SelectedCLRx(CurrRxCnt + 1).InventoryIdOS = SelectedCLRx(Idx).InventoryIdOS
            SelectedCLRx(CurrRxCnt + 1).ODManifest = SelectedCLRx(Idx).ODManifest
            SelectedCLRx(CurrRxCnt + 1).OSManifest = SelectedCLRx(Idx).OSManifest
            SelectedCLRx(CurrRxCnt + 1).RxNote = ""
            CurrRxCnt = CurrRxCnt + 1
            Call LoadEyeRx(False)
        Else
            frmEventMsgs.Header = "Only " + Trim(str(MaxSetCL)) + " pairs allowed"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
        End If
    Else
        If (Idx = 1) Then
            lblTrial1.Visible = Not (lblTrial1.Visible)
            lblTrial1.Caption = "Trial, Click to Order"
            lblTrial1.BackColor = TrialNormalColor
            If Not (lblTrial1.Visible) Then
                ZTemp = "^"
                GoSub SetValue
            Else
                ZTemp = "*"
                If (OrderTrial) Then
                    ZTemp = "~"
                    lblTrial1.Caption = "Order Trial, Click for Trial"
                    lblTrial1.BackColor = TrialOrderColor
                End If
                GoSub SetValue
            End If
        ElseIf (Idx = 2) Then
            lblTrial2.Visible = Not (lblTrial2.Visible)
            lblTrial2.Caption = "Trial, Click to Order"
            lblTrial2.BackColor = TrialNormalColor
            If Not (lblTrial2.Visible) Then
                ZTemp = "^"
                GoSub SetValue
            Else
                ZTemp = "*"
                lblTrial2.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
                    lblTrial2.Caption = "Order Trial, Click for Trial"
                    lblTrial2.BackColor = TrialOrderColor
            End If
            GoSub SetValue
            End If
        ElseIf (Idx = 3) Then
            lblTrial3.Visible = Not (lblTrial3.Visible)
            lblTrial3.Caption = "Trial, Click to Order"
            lblTrial3.BackColor = TrialNormalColor
            If Not (lblTrial3.Visible) Then
                ZTemp = "^"
                GoSub SetValue
            Else
                ZTemp = "*"
                lblTrial3.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
                    lblTrial3.Caption = "Order Trial, Click for Trial"
                    lblTrial3.BackColor = TrialOrderColor
                End If
                GoSub SetValue
            End If
        ElseIf (Idx = 4) Then
            lblTrial4.Visible = Not (lblTrial4.Visible)
            lblTrial4.Caption = "Trial, Click to Order"
            lblTrial4.BackColor = TrialNormalColor
            If Not (lblTrial4.Visible) Then
                ZTemp = "^"
                GoSub SetValue
            Else
                ZTemp = "*"
                lblTrial4.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
                    lblTrial4.Caption = "Order Trial, Click for Trial"
                    lblTrial4.BackColor = TrialOrderColor
                End If
                GoSub SetValue
            End If
        ElseIf (Idx = 5) Then
            lblTrial5.Visible = Not (lblTrial5.Visible)
            lblTrial5.Caption = "Trial, Click to Order"
            lblTrial5.BackColor = TrialNormalColor
            If Not (lblTrial5.Visible) Then
                ZTemp = "^"
                GoSub SetValue
            Else
                ZTemp = "*"
                lblTrial5.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
                    lblTrial5.Caption = "Order Trial, Click for Trial"
                    lblTrial5.BackColor = TrialOrderColor
                End If
                GoSub SetValue
            End If
        ElseIf (Idx = 6) Then
            lblTrial6.Visible = Not (lblTrial6.Visible)
            lblTrial6.Caption = "Trial, Click to Order"
            lblTrial6.BackColor = TrialNormalColor
            If Not (lblTrial6.Visible) Then
                ZTemp = "^"
                GoSub SetValue
            Else
                ZTemp = "*"
                lblTrial6.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
                    lblTrial6.Caption = "Order Trial, Click for Trial"
                    lblTrial6.BackColor = TrialOrderColor
                End If
                GoSub SetValue
            End If
        ElseIf (Idx = 7) Then
            lblTrial7.Visible = Not (lblTrial7.Visible)
            lblTrial7.Caption = "Trial, Click to Order"
            lblTrial7.BackColor = TrialNormalColor
            If Not (lblTrial7.Visible) Then
                ZTemp = "^"
                GoSub SetValue
            Else
                ZTemp = "*"
                lblTrial7.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
                    lblTrial7.Caption = "Order Trial, Click for Trial"
                    lblTrial7.BackColor = TrialOrderColor
                End If
                GoSub SetValue
            End If
        ElseIf (Idx = 8) Then
            lblTrial8.Visible = Not (lblTrial8.Visible)
            lblTrial8.Caption = "Trial, Click to Order"
            lblTrial8.BackColor = TrialNormalColor
            If Not (lblTrial8.Visible) Then
                ZTemp = "^"
                GoSub SetValue
            Else
                ZTemp = "*"
                lblTrial8.BackColor = TrialNormalColor
                If (OrderTrial) Then
                    ZTemp = "~"
                    lblTrial8.Caption = "Order Trial, Click for Trial"
                    lblTrial8.BackColor = TrialOrderColor
                End If
                GoSub SetValue
            End If
        End If
    End If
End If
Exit Sub
SetValue:
    If (Trim(SelectedCLRx(Idx).RxNote) = "") Then
        SelectedCLRx(Idx).RxNote = ZTemp
    Else
        SelectedCLRx(Idx).RxNote = ZTemp + Trim(SelectedCLRx(Idx).RxNote)
    End If
    Return
End Sub

Private Sub DoDescriptors(Idx As Integer)
Dim i As Integer
Dim Style As Boolean
Dim TrialType As String
Dim ODT As String
Dim OST As String
Dim TOrder As String
Dim TQuestion As String
Dim Temp As String, TestTime As String
Dim AFile As String, TheFile As String
Temp = ""
Style = False
TOrder = "56A"
TQuestion = "CLRX FIT DESCRIPTORS"
If (Idx > MaxSetCL) Or (Idx < 1) Then
    Exit Sub
End If
If (Trim(TOrder) <> "") Then
    TheFile = GetTestFileName(TOrder)
    If (FM.IsFileThere(TheFile)) Then
        FM.Kill TheFile
    End If
    If Not (FM.IsFileThere(TheFile)) Then
        If Not (Style) Then
            FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(1)
            Print #1, "QUESTION=\" + UCase(Trim(TQuestion))
            Call FormatTimeNow(TestTime)
            Print #1, "TIME=" + TestTime
            FM.CloseFile CLng(1)
        End If
    End If
    If (Style) Then
        frmTestEntry.ManifestResults = ""
        frmTestEntry.ActivityId = ActivityId
        frmTestEntry.PatientId = PatientId
        frmTestEntry.AppointmentId = AppointmentId
        frmTestEntry.ChangeOn = False
        frmTestEntry.StoreResults = TheFile
        frmTestEntry.Question = UCase(TQuestion)
        frmTestEntry.FormOn = True
        frmAssignWear.Enabled = False
        frmTestEntry.Show 1
        If (Trim(frmTestEntry.StoreResults) = "") Then
            TheFile = ""
        End If
    Else
        frmEditTests.CLFitOn = True
        frmEditTests.ManifestResultsOn = False
        frmEditTests.ManifestResults = ""
        frmEditTests.StoreResults = TheFile
        frmEditTests.Question = UCase(TQuestion)
        frmEditTests.PatientId = PatientId
        frmEditTests.AppointmentId = AppointmentId
        frmEditTests.FormOn = True
        frmAssignWear.Enabled = False
        frmEditTests.Show 1
        frmEditTests.CLFitOn = False
        If (Trim(frmEditTests.StoreResults) = "") Or (frmEditTests.QuitOn) Then
            If (FM.IsFileThere(TheFile)) Then
                FM.Kill TheFile
            End If
            TheFile = ""
        End If
    End If
End If
frmAssignWear.ZOrder 0
frmAssignWear.Enabled = True
If (FM.IsFileThere(TheFile)) Then
    Call frmTestResults.PushList(TheFile)
    Call frmSystems1.ApplyTestResults(TheFile, TOrder, Temp, False, True)
    If (InStrPS(Temp, "OD: " + Chr(13)) > 0) And (Mid(Temp, Len(Temp) - 3, 4) = "OS: ") Then
        If Not (Style) Then
            Temp = frmEditTests.ManifestResults
            Temp = ":  " + Temp
        End If
    End If
Else
    Exit Sub
End If
If (Trim(Temp) <> "") Then
    i = InStrPS(Temp, ":")
    If (i > 0) Then
        Temp = Mid(Temp, i + 3, Len(Temp) - (i + 2))
    End If
    i = InStrPS(Temp, "OS")
    If (i > 0) Then
        ODT = Mid(Temp, 4, (i - 1) - 3)
        Call StripCharacters(ODT, Chr(13))
        Call StripCharacters(ODT, Chr(10))
        OST = Mid(Temp, i + 3, Len(Temp) - (i - 2))
        Call StripCharacters(OST, Chr(13))
        Call StripCharacters(OST, Chr(10))
    Else
        ODT = Temp
        OST = ""
    End If
    Call frmSystems1.ClearTestResults(TOrder)
    If (FM.IsFileThere(TheFile)) Then
        FM.Kill TheFile
    End If
    If (Len(ODT) > 2) Or (Len(OST) > 2) Then
        ODT = "OD:" + ODT
        Call ReplaceCharacters(ODT, ",", " ")
        ODT = Trim(ODT)
        OST = "OS:" + OST
        Call ReplaceCharacters(OST, ",", " ")
        OST = Trim(OST)
        If (Idx = 1) Then
            TrialType = ""
            If (lblTrial1.Visible) Then
                If (lblTrial1.BackColor = TrialOrderColor) Then
                    TrialType = "~"
                ElseIf (lblTrial1.BackColor = TrialNormalColor) Then
                    TrialType = "*"
                End If
            End If
            txtNote1.Text = ODT + vbCrLf + OST
            lblNote1.BackColor = EnlargeOff
            If (IsNoteBig(ODT + vbCrLf + OST)) Then
                lblNote1.BackColor = EnlargeOn
            End If
            txtNote1.Visible = True
            SelectedCLRx(Idx).RxNote = TrialType + ODT + " " + OST
        ElseIf (Idx = 2) Then
            TrialType = ""
            If (lblTrial2.Visible) Then
                If (lblTrial2.BackColor = TrialOrderColor) Then
                    TrialType = "~"
                ElseIf (lblTrial2.BackColor = TrialNormalColor) Then
                    TrialType = "*"
                End If
            End If
            txtNote2.Text = ODT + vbCrLf + OST
            lblNote2.BackColor = EnlargeOff
            If (IsNoteBig(ODT + vbCrLf + OST)) Then
                lblNote2.BackColor = EnlargeOn
            End If
            txtNote2.Visible = True
            SelectedCLRx(Idx).RxNote = TrialType + ODT + " " + OST
        ElseIf (Idx = 3) Then
            TrialType = ""
            If (lblTrial3.Visible) Then
                If (lblTrial3.BackColor = TrialOrderColor) Then
                    TrialType = "~"
                ElseIf (lblTrial3.BackColor = TrialNormalColor) Then
                    TrialType = "*"
                End If
            End If
            txtNote3.Text = ODT + vbCrLf + OST
            lblNote3.BackColor = EnlargeOff
            If (IsNoteBig(ODT + vbCrLf + OST)) Then
                lblNote3.BackColor = EnlargeOn
            End If
            txtNote3.Visible = True
            SelectedCLRx(Idx).RxNote = TrialType + ODT + " " + OST
        ElseIf (Idx = 4) Then
            TrialType = ""
            If (lblTrial4.Visible) Then
                If (lblTrial4.BackColor = TrialOrderColor) Then
                    TrialType = "~"
                ElseIf (lblTrial4.BackColor = TrialNormalColor) Then
                    TrialType = "*"
                End If
            End If
            txtNote4.Text = ODT + vbCrLf + OST
            lblNote4.BackColor = EnlargeOff
            If (IsNoteBig(ODT + vbCrLf + OST)) Then
                lblNote4.BackColor = EnlargeOn
            End If
            txtNote4.Visible = True
            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
        ElseIf (Idx = 5) Then
            TrialType = ""
            If (lblTrial5.Visible) Then
                If (lblTrial5.BackColor = TrialOrderColor) Then
                    TrialType = "~"
                ElseIf (lblTrial5.BackColor = TrialNormalColor) Then
                    TrialType = "*"
                End If
            End If
            txtNote5.Text = ODT + vbCrLf + OST
            lblNote1.BackColor = EnlargeOff
            If (IsNoteBig(ODT + vbCrLf + OST)) Then
                lblNote1.BackColor = EnlargeOn
            End If
            txtNote5.Visible = True
            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
        ElseIf (Idx = 6) Then
            TrialType = ""
            If (lblTrial6.Visible) Then
                If (lblTrial6.BackColor = TrialOrderColor) Then
                    TrialType = "~"
                ElseIf (lblTrial6.BackColor = TrialNormalColor) Then
                    TrialType = "*"
                End If
            End If
            txtNote6.Text = ODT + vbCrLf + OST
            lblNote2.BackColor = EnlargeOff
            If (IsNoteBig(ODT + vbCrLf + OST)) Then
                lblNote2.BackColor = EnlargeOn
            End If
            txtNote6.Visible = True
            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
        ElseIf (Idx = 7) Then
            TrialType = ""
            If (lblTrial7.Visible) Then
                If (lblTrial7.BackColor = TrialOrderColor) Then
                    TrialType = "~"
                ElseIf (lblTrial7.BackColor = TrialNormalColor) Then
                    TrialType = "*"
                End If
            End If
            txtNote7.Text = ODT + vbCrLf + OST
            lblNote3.BackColor = EnlargeOff
            If (IsNoteBig(ODT + vbCrLf + OST)) Then
                lblNote3.BackColor = EnlargeOn
            End If
            txtNote7.Visible = True
            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
        ElseIf (Idx = 8) Then
            TrialType = ""
            If (lblTrial8.Visible) Then
                If (lblTrial8.BackColor = TrialOrderColor) Then
                    TrialType = "~"
                ElseIf (lblTrial8.BackColor = TrialNormalColor) Then
                    TrialType = "*"
                End If
            End If
            txtNote8.Text = ODT + vbCrLf + OST
            lblNote4.BackColor = EnlargeOff
            If (IsNoteBig(ODT + vbCrLf + OST)) Then
                lblNote4.BackColor = EnlargeOn
            End If
            txtNote8.Visible = True
            SelectedCLRx(Idx).RxNote = "*" + ODT + " " + OST
        End If
    End If
End If
End Sub

Private Sub SetPairs(TheText As String)
Dim u As Integer
Dim p As Integer
Dim i As Integer
Dim TheLen As Integer
Dim ODT As String
Dim OST As String
Dim ATemp As String
Dim Temp(MaxSetCL) As String
Dim TheFile As String
ATemp = ""
For i = 1 To MaxSetCL
    Temp(i) = ""
Next i
If (Trim(TheText) <> "") Then
    u = InStrPS(TheText, "Pair 1")
    If (u > 0) Then
        p = InStrPS(u, TheText, "Pair 2")
        If (p = 0) Then
            p = Len(TheText) - (u + 8)
        End If
        Temp(1) = Mid(TheText, u + 9, p)
    End If
    u = InStrPS(TheText, "Pair 2")
    If (u > 0) Then
        p = InStrPS(u, TheText, "Pair 3")
        If (p = 0) Then
            p = Len(TheText) - (u + 8)
        End If
        Temp(2) = Mid(TheText, u + 9, p)
    End If
    u = InStrPS(TheText, "Pair 3")
    If (u > 0) Then
        p = InStrPS(u, TheText, "Pair 4")
        If (p = 0) Then
            p = Len(TheText) - (u + 8)
        End If
        Temp(3) = Mid(TheText, u + 9, p)
    End If
    u = InStrPS(TheText, "Pair 4")
    If (u > 0) Then
        p = Len(TheText) - (u + 8)
        Temp(4) = Mid(TheText, u + 9, p)
    End If
    For u = 1 To 3
        ATemp = Trim(Temp(u))
        If (ATemp <> "") Then
            i = InStrPS(ATemp, "OS")
            If (i > 0) Then
                ODT = Left(ATemp, i - 1)
                ODT = Trim(Mid(ODT, 4, Len(ODT) - 3))
                OST = Mid(ATemp, i, Len(ATemp) - (i - 1))
                OST = Trim(Mid(OST, 4, Len(OST) - 3))
            Else
                ODT = ATemp
                OST = ""
            End If
            Call StripCharacters(ODT, Chr(10))
            Call StripCharacters(ODT, Chr(13))
            Call StripCharacters(OST, Chr(10))
            Call StripCharacters(OST, Chr(13))
            If (WearType = "PC") Then
                SelectedPCRx(u).ODManifest = ODT
                SelectedPCRx(u).OSManifest = OST
            Else
                SelectedCLRx(u).ODManifest = ODT
                SelectedCLRx(u).OSManifest = OST
            End If
        End If
    Next u
End If
End Sub

Private Function DoEyeWearPrint(Index As Integer, FirstOn As Boolean, LastOn As Boolean, TotalRxs As Integer) As Boolean
Dim p As Integer
Dim k As Integer
Dim CLOn As Boolean
Dim ProceedOn As Boolean
Dim Temp As String
Dim ODRx As String, OSRx As String
Dim RNote As String, TheFile As String
Dim TempFile As String, SigFile As String
Dim LensType As String, LensCompany As String
Dim CLInv As CLInventory
DoEyeWearPrint = False
ODRx = ""
OSRx = ""
RNote = ""
ThePrinter = GetPrinter(PRINTER_Wear)
CLOn = False
If (WearType = "CL") Then
    CLOn = True
End If
If (Index >= 0) Then
    If (WearType = "PC") Then
        ODRx = Trim(SelectedPCRx(Index).ODManifest)
        OSRx = Trim(SelectedPCRx(Index).OSManifest)
        LensType = Trim(SelectedPCRx(Index).EWType)
        LensCompany = Trim(SelectedPCRx(Index).EWCompany)
        RNote = Trim(SelectedPCRx(Index).RxNote)
    Else
        ODRx = Trim(SelectedCLRx(Index).ODManifest)
        OSRx = Trim(SelectedCLRx(Index).OSManifest)
        If (SelectedCLRx(Index).InventoryIdOD > 0) Then
            Call GetInventoryItem(SelectedCLRx(Index).InventoryIdOD, LensType)
        Else
            LensType = Trim(SelectedCLRx(Index).EWType)
        End If
        If (SelectedCLRx(Index).InventoryIdOS > 0) Then
            Call GetInventoryItem(SelectedCLRx(Index).InventoryIdOD, LensCompany)
        Else
            LensCompany = Trim(SelectedCLRx(Index).EWCompany)
        End If
        RNote = Trim(SelectedCLRx(Index).RxNote)
        Call ReplaceCharacters(RNote, vbCrLf, "-&-")
        If (Mid(RNote, 1, 1) = "~") Then
            RNote = "Order Trial:" + Trim(Mid(RNote, 2, Len(RNote) - 1))
        ElseIf (Mid(RNote, 1, 1) = "*") Then
            RNote = "Trial:" + Trim(Mid(RNote, 2, Len(RNote) - 1))
        ElseIf (Mid(RNote, 1, 1) = "^") Then
            RNote = Trim(Mid(RNote, 2, Len(RNote) - 1))
        End If
    End If
    ProceedOn = True
    If (Trim(RNote) <> "") Then
        If (Left(RNote, 5) = "Trial") Then
            ProceedOn = False
        ElseIf (Left(RNote, 11) = "Order Trial") Then
            ProceedOn = False
        End If
    End If
    If (PID = "D") Then
        If (WearType = "PC") Then
            TheFile = DoctorInterfaceDirectory + "EyeWearRx" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        Else
            TheFile = DoctorInterfaceDirectory + "CLRx" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        End If
    Else
        If (WearType = "PC") Then
            TheFile = SchedulerInterfaceDirectory + "EyeWearRx" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        Else
            TheFile = DoctorInterfaceDirectory + "CLRx" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
        End If
    End If
    If (FirstOn) Then
        If (FM.IsFileThere(TheFile)) Then
            Call KillDocumentProcessing(TheFile, True, True)
        End If
    End If
    If (WearType = "PC") Then
        TempFile = TemplateDirectory + "EyeWearRx.doc"
    Else
        TempFile = TemplateDirectory + "CLRx.doc"
    End If
    If (ProceedOn) Then
        If Not (FM.IsFileThere(TheFile)) Then
            FirstOn = True
        End If
        If (CreateEyeWear(PatientId, AppointmentId, ActivityId, TheFile, ODRx, OSRx, LensType, LensCompany, RNote, CLOn, FirstOn, LastOn)) Then
            DoEyeWearPrint = True
            lblPrint.Visible = True
            DoEvents
            If (LastOn) Then
                Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
' use a special signature file setup for Rxs
                If (Trim(SigFile) <> "") Then
                    p = InStrPS(SigFile, ".")
                    If (p > 0) Then
                        SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
                    End If
                End If
                Call PrintTemplate(TheFile, TempFile, True, True, 1, ThePrinter, SigFile, TotalRxs, True)
            End If
            lblPrint.Visible = False
        End If
    Else
        lblPrint.Visible = True
        DoEvents
        If (LastOn) Then
            Call frmSystems1.GetResourceSignatureFile(AppointmentId, ActivityId, SigFile)
' use a special signature file setup for Rxs
            If (Trim(SigFile) <> "") Then
                p = InStrPS(SigFile, ".")
                If (p > 0) Then
                    SigFile = Left(SigFile, p - 1) + "-Rx" + Mid(SigFile, p, Len(SigFile) - (p - 1))
                End If
            End If
            If (FM.IsFileThere(TheFile)) Then
                Call PrintTemplate(TheFile, TempFile, True, True, 1, ThePrinter, SigFile, TotalRxs, True)
            End If
        End If
        lblPrint.Visible = False
    End If
End If
End Function

Private Function CreateEyeWear(PatId As Long, ApptId As Long, ActId As Long, ResultFile As String, ODRx As String, OSRx As String, LensType As String, LensCompany As String, ANote As String, CLOn As Boolean, FirstOn As Boolean, LastOn As Boolean) As Boolean
Dim FileNum As Integer, Idx As Integer
Dim i As Integer, q As Integer
Dim u As Integer, w As Integer
Dim RscId As Long, RscId1 As Long
Dim KeyWords(5)
Dim TName As String, ATemp As String
Dim Temp As String, ADate As String
Dim NewFile As String, TempZip As String
Dim RetAct As PracticeActivity
Dim RetAppt As SchedulerAppointment
Dim RetDr As SchedulerResource
Dim RetPrc As PracticeName
Dim RetPat As Patient
ADate = ""
CreateEyeWear = False
lstBox.Clear
KeyWords(1) = "ADD-READING:"
KeyWords(2) = "ADD-INTERMEDIATE:"
KeyWords(3) = "PRISM OF ANGLE 1:"
KeyWords(4) = "PRISM OF ANGLE 2:"
KeyWords(5) = "Vertex dist:"
If (CLOn) Then
    KeyWords(1) = "ADD-READING:"
    KeyWords(2) = "BASE CURVE:"
    KeyWords(3) = "DIAMETER:"
    KeyWords(4) = "PERIPH CURVE:"
    KeyWords(5) = ""
End If
If (PatId > 0) And (ApptId > 0) Then
    If (InStrPS(UCase(ODRx), "ANGLE1") <> 0) Then
        Call ReplaceCharacters(UCase(ODRx), "ANGLE1", "ANGLE 1")
    ElseIf (InStrPS(UCase(ODRx), "ANGLE2") <> 0) Then
        Call ReplaceCharacters(UCase(ODRx), "ANGLE2", "ANGLE 2")
    End If
    If (InStrPS(UCase(OSRx), "ANGLE1") <> 0) Then
        Call ReplaceCharacters(UCase(OSRx), "ANGLE1", "ANGLE 1")
    ElseIf (InStrPS(UCase(OSRx), "ANGLE2") <> 0) Then
        Call ReplaceCharacters(UCase(OSRx), "ANGLE2", "ANGLE 2")
    End If
    Set RetPat = New Patient
    Set RetPrc = New PracticeName
    Set RetDr = New SchedulerResource
    Set RetAct = New PracticeActivity
    Set RetAppt = New SchedulerAppointment
    RetPat.PatientId = PatId
    Call RetPat.RetrievePatient
    
    RscId1 = 0
    If (ActId > 0) Then
        RetAct.ActivityId = ActId
        If (RetAct.RetrieveActivity) Then
            If (RetAct.ActivityCurrResId > 0) Then
                RetDr.ResourceId = RetAct.ActivityCurrResId
                If (RetDr.RetrieveSchedulerResource) Then
                    If (Trim(RetDr.ResourceSignatureFile) <> "") Then
                        RscId1 = RetDr.ResourceId
                    End If
                End If
            End If
        End If
    End If
    
    RetAppt.AppointmentId = ApptId
    If (RetAppt.RetrieveSchedulerAppointment) Then
        If (RscId1 < 1) Then
            If (RetAppt.AppointmentResourceId1 > 0) Then
                RetDr.ResourceId = RetAppt.AppointmentResourceId1
                If (RetDr.RetrieveSchedulerResource) Then
                
                End If
            End If
        End If
    End If
' Load the Merge File
    lstBox.Clear
    If (RetDr.ResourceId > 0) Then
        If (Trim(RetDr.ResourceDescription) <> "") Then
            lstBox.AddItem "01:" + Trim(RetDr.ResourceDescription)
        Else
            lstBox.AddItem "01:  "
        End If
        If (Trim(RetDr.ResourceLic) <> "") Then
            lstBox.AddItem "02:" + Trim(RetDr.ResourceLic)
        Else
            lstBox.AddItem "02:  "
        End If
        If (RetDr.PracticeId > 0) Then
            RetPrc.PracticeId = RetDr.PracticeId
            If (RetPrc.RetrievePracticeName) Then
                lstBox.AddItem "03:" + Trim(RetPrc.PracticeAddress)
                TempZip = ""
                Call DisplayPhone(RetPrc.PracticeZip, TempZip)
                Temp = Trim(RetPrc.PracticeCity) + ", " + RetPrc.PracticeState + " " + TempZip
                lstBox.AddItem "04:" + Temp
                TempZip = ""
                Call DisplayPhone(RetPrc.PracticePhone, TempZip)
                lstBox.AddItem "05:" + TempZip
            Else
                lstBox.AddItem "03:    "
                lstBox.AddItem "04:    "
                lstBox.AddItem "05:    "
            End If
        Else
            lstBox.AddItem "03:    "
            lstBox.AddItem "04:    "
            lstBox.AddItem "05     "
        End If
    Else
        lstBox.AddItem "01:    "
        lstBox.AddItem "02:    "
        lstBox.AddItem "03:    "
        lstBox.AddItem "04:    "
        lstBox.AddItem "05:    "
    End If
    Call FormatTodaysDate(ADate, False)
    lstBox.AddItem "06:" + ADate
    Temp = Trim(Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial) + " " + Trim(RetPat.LastName) + " " + Trim(RetPat.NameRef))
    Call StripCharacters(Temp, Chr(34))
    lstBox.AddItem "07:" + Temp
    lstBox.AddItem "04a:" + Trim(RetPat.Address)
    Call DisplayZip(RetPat.Zip, Temp)
    lstBox.AddItem "04b:" + Trim(RetPat.City) + ", " + Trim(RetPat.State) + " " + Trim(Temp)
' Parse and Place the Od/Os Rx fields
    If (Left(ODRx, 2) = "OD") Then
        ODRx = Trim(Mid(ODRx, 4, Len(ODRx) - 3))
    End If
    q = InStrPS(ODRx, " ")
    If (q > 0) Then
        Temp = Trim(Left(ODRx, q))
        If (Temp = "NE") Then
            Temp = "    "
        End If
        Call ConvertOldZeros(Temp)
        w = InStrPS(q + 1, ODRx, " ")
        If (w > 0) Then
            TName = Mid(ODRx, q + 1, (w - 1) - q)
            If (UCase(TName) = "BALANCE") Then
                Temp = Temp + " " + Trim(TName) + " LENS"
                q = w + 5
            ElseIf (UCase(TName) = "VERTEX") Then
            
            End If
        End If
        lstBox.AddItem "07a:" + Temp
    ElseIf (Trim(ODRx) <> "") Then
        Temp = Trim(ODRx)
        Call ConvertOldZeros(Temp)
        lstBox.AddItem "07a:" + Temp
    Else
        lstBox.AddItem "07a:    "
    End If
    If (UCase(TName) <> "VERTEX") Then
        i = InStrPS(q + 1, ODRx, " ")
        If (i > 0) Then
            Temp = Trim(Mid(ODRx, q + 1, i - q))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            If (UCase(Left(Temp, 3)) <> "ADD") And (UCase(Left(Temp, 3)) <> "PRI") And (UCase(Left(Temp, 3)) <> "BAS") And (UCase(Left(Temp, 3)) <> "DIA") And (UCase(Left(Temp, 1)) <> "X") Then
                Call ConvertOldZeros(Temp)
                If (UCase(Temp) = "VERTEX") Then
                    Temp = "    "
                End If
                lstBox.AddItem "08:" + Temp
            Else
                lstBox.AddItem "08:    "
            End If
        Else
            lstBox.AddItem "08:    "
        End If
        q = InStrPS(ODRx, "X")
        If (q < 1) Then
            q = InStrPS(ODRx, "x")
        End If
        If (q > 0) Then
            i = InStrPS(q, ODRx, " ")
            If (i > 0) Then
                Temp = Trim(Mid(ODRx, q, i - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                Call ConvertOldZeros(Temp)
                If (UCase(Temp) = "X") Then
                    lstBox.AddItem "08a:    "
                Else
                    lstBox.AddItem "08a:" + Temp
                End If
            Else
                Temp = Trim(Mid(ODRx, q, Len(ODRx) - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                Call ConvertOldZeros(Temp)
                If (UCase(Temp) = "X") Then
                    lstBox.AddItem "08a:    "
                Else
                    lstBox.AddItem "08a:" + Temp
                End If
            End If
        Else
            lstBox.AddItem "08a:   "
        End If
    Else
        lstBox.AddItem "08:   "
        lstBox.AddItem "08a:   "
    End If
' OD Data
    Idx = 1
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "16:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "16:" + ATemp
    End If
    
    Idx = 2
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "09:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "09:" + ATemp
    End If
    
    Idx = 3
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "09a:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "09a:" + ATemp
    End If
    
    Idx = 4
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "17:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "17:" + ATemp
    End If
    lstBox.AddItem "09b:    "
    lstBox.AddItem "10:    "

    Idx = 5
    Temp = ODRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "23:   "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "23:" + ATemp
    End If

' OS Data
    If (Left(OSRx, 2) = "OS") Then
        OSRx = Trim(Mid(OSRx, 4, Len(OSRx) - 3))
    End If
    q = InStrPS(OSRx, " ")
    If (q > 0) Then
        Temp = Trim(Left(OSRx, q))
        If (Temp = "NE") Then
            Temp = "    "
        End If
        Call ConvertOldZeros(Temp)
        w = InStrPS(q + 1, OSRx, " ")
        If (w > 0) Then
            TName = Mid(OSRx, q + 1, (w - 1) - q)
            If (UCase(TName) = "BALANCE") Then
                Temp = Temp + " " + Trim(TName) + " LENS"
                q = w + 5
            ElseIf (UCase(TName) = "VERTEX") Then
            
            End If
        End If
        lstBox.AddItem "11:" + Temp
    ElseIf (Trim(OSRx) <> "") Then
        Temp = Trim(OSRx)
        Call ConvertOldZeros(Temp)
        lstBox.AddItem "11:" + Temp
    Else
        lstBox.AddItem "11:    "
    End If
    If (UCase(TName) <> "VERTEX") Then
        i = InStrPS(q + 1, OSRx, " ")
        If (i > 0) Then
            Temp = Trim(Mid(OSRx, q + 1, i - q))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            If (UCase(Left(Temp, 3)) <> "ADD") And (UCase(Left(Temp, 3)) <> "PRI") And (UCase(Left(Temp, 3)) <> "BAS") And (UCase(Left(Temp, 3)) <> "DIA") And (UCase(Left(Temp, 1)) <> "X") Then
                Call ConvertOldZeros(Temp)
                lstBox.AddItem "11a:" + Temp
            Else
                lstBox.AddItem "11a:    "
            End If
        Else
            lstBox.AddItem "11a:    "
        End If
        q = InStrPS(OSRx, "X")
        If (q < 1) Then
            q = InStrPS(OSRx, "x")
        End If
        If (q > 0) Then
            i = InStrPS(q, OSRx, " ")
            If (i > 0) Then
                Temp = Trim(Mid(OSRx, q, i - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                If (UCase(Temp) = "X") Then
                    lstBox.AddItem "11b:    "
                Else
                    lstBox.AddItem "11b:" + Temp
                End If
            Else
                Temp = Trim(Mid(OSRx, q, Len(OSRx) - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                Call ConvertOldZeros(Temp)
                If (UCase(Temp) = "X") Then
                    lstBox.AddItem "11b:    "
                Else
                    lstBox.AddItem "11b:" + Temp
                End If
            End If
        Else
            lstBox.AddItem "11b:   "
        End If
    Else
        lstBox.AddItem "11a:   "
        lstBox.AddItem "11b:   "
    End If
    
    Idx = 1
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "18:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "18:" + ATemp
    End If
    
    Idx = 2
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "12:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "12:" + ATemp
    End If
    
    Idx = 3
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "13:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "13:" + ATemp
    End If
    
    Idx = 4
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "19:    "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "19:" + ATemp
    End If
    
    Idx = 5
    Temp = OSRx
    GoSub GetField
    If (ATemp = "") Then
        lstBox.AddItem "24:   "
    Else
        If (ATemp = "NE") Then
            ATemp = "    "
        End If
        lstBox.AddItem "24:" + ATemp
    End If
    
    lstBox.AddItem "14:    "
    lstBox.AddItem "15:    "
    lstBox.AddItem "18a:    "
    lstBox.AddItem "18b:    "
    lstBox.AddItem "18c:    "
    lstBox.AddItem "18d:    "
    lstBox.AddItem "18e:    "
    
    If (Trim(LensType) <> "") And (UCase(Trim(LensType)) <> "NONE") Then
        lstBox.AddItem "20:" + Trim(LensType)
    Else
        lstBox.AddItem "20:    "
    End If
        
    If (Trim(LensCompany) <> "") And (UCase(Trim(LensCompany)) <> "NONE") Then
        lstBox.AddItem "21:" + Trim(LensCompany)
    Else
        lstBox.AddItem "21:    "
    End If
    
    If (Trim(ANote) <> "") Then
        lstBox.AddItem "22:" + Trim(ANote)
    Else
        lstBox.AddItem "22:   "
    End If
    If (Trim(RetPat.BirthDate) <> "") Then
        ADate = Mid(RetPat.BirthDate, 5, 2) + "/" + Mid(RetPat.BirthDate, 7, 2) + "/" + Left(RetPat.BirthDate, 4)
        lstBox.AddItem "25:" + ADate
    Else
        lstBox.AddItem "25:    "
    End If
' Add the appointment date
    ADate = Mid(RetAppt.AppointmentDate, 5, 2) + "/" + Mid(RetAppt.AppointmentDate, 7, 2) + "/" + Left(RetAppt.AppointmentDate, 4)
    lstBox.AddItem "26:" + ADate
    
    lstBox.AddItem "27:" & PatId
    
    lstBox.AddItem "28:   "
    lstBox.AddItem "29:   "
    lstBox.AddItem "30:   "
    lstBox.AddItem "31:   "
    lstBox.AddItem "32:   "
    lstBox.AddItem "33:   "
    lstBox.AddItem "34:   "
    lstBox.AddItem "35:   "
    lstBox.AddItem "36:   "
    lstBox.AddItem "37:   "
    lstBox.AddItem "38:   "
    lstBox.AddItem "39:   "
    lstBox.AddItem "40:   "
    lstBox.AddItem "41:   "
    lstBox.AddItem "42:   "
    lstBox.AddItem "43:   "
    lstBox.AddItem "44:   "
    lstBox.AddItem "45:   "
    lstBox.AddItem "46:   "
    lstBox.AddItem "47:   "
    lstBox.AddItem "48:   "
    lstBox.AddItem "49:   "
    lstBox.AddItem "50:   "
    lstBox.AddItem "51:   "
    lstBox.AddItem "52:   "
    lstBox.AddItem "53:   "
    lstBox.AddItem "54:   "
    lstBox.AddItem "55:   "
    lstBox.AddItem "56:   "
    lstBox.AddItem "57:   "
    lstBox.AddItem "58:   "
    lstBox.AddItem "59:   "
    lstBox.AddItem "60:   "
    lstBox.AddItem "61:   "
    lstBox.AddItem "62:   "
    lstBox.AddItem "63:   "
    lstBox.AddItem "64:   "
    lstBox.AddItem "65:   "
    lstBox.AddItem "66:   "
    lstBox.AddItem "67:   "
    lstBox.AddItem "68:   "
    lstBox.AddItem "69:   "
    lstBox.AddItem "05a:    "
    lstBox.AddItem "06a:    "
    lstBox.AddItem "18f:    "
    If (FirstOn) Then
        If (FM.IsFileThere(ResultFile)) Then
            FM.Kill ResultFile
        End If
        FM.FileCopy TemplateDirectory + "Letters.txt", ResultFile
        FileNum = FreeFile
        FM.OpenFile ResultFile, FileOpenMode.Binary, FileAccess.ReadWrite, CLng(FileNum)
        FM.CloseFile CLng(FileNum)
    Else
        FileNum = FreeFile
    End If
    FM.OpenFile ResultFile, FileOpenMode.Append, FileAccess.ReadWrite, CLng(FileNum)
    For i = 0 To lstBox.ListCount - 1
        q = InStrPS(lstBox.List(i), ":")
        If (q > 0) Then
            Temp = Mid(lstBox.List(i), q + 1, Len(lstBox.List(i)) - q)
            Call ReplaceCharacters(Temp, vbCrLf, "-&-")
            Print #FileNum, Temp; vbTab;
        End If
    Next i
    Print #FileNum, ""
    FM.CloseFile CLng(FileNum)
    Set RetAct = Nothing
    Set RetAppt = Nothing
    Set RetPat = Nothing
    Set RetPrc = Nothing
    Set RetDr = Nothing
    If (LastOn) Then
        NewFile = Left(ResultFile, Len(ResultFile) - 4) + ".xls"
        If (FM.IsFileThere(NewFile)) Then
            FM.Kill NewFile
        End If
        FM.Name ResultFile, NewFile
        ResultFile = NewFile
    End If
    CreateEyeWear = True
End If
Exit Function
GetField:
    ATemp = ""
    If (Idx > 0) Then
        If (KeyWords(Idx) <> "") Then
            i = InStrPS(UCase(Temp), UCase(KeyWords(Idx)))
            If (i > 0) Then
                GoSub PutEnd
                If (u > 0) Then
                    ATemp = Mid(Temp, i, (u - 1) - (i - 1))
                Else
                    ATemp = Mid(Temp, i, Len(Temp) - (i - 1))
                End If
            End If
        End If
    End If
    If (Trim(ATemp) <> "") Then
        i = InStrPS(ATemp, ":")
        If (i > 0) Then
            ATemp = Trim(Mid(ATemp, i + 1, Len(ATemp) - i))
        End If
    End If
    Call ConvertOldZeros(ATemp)
    Return
PutEnd:
    u = 0
    For w = Idx + 1 To 5
        If (Trim(KeyWords(w)) <> "") Then
            u = InStrPS(UCase(Temp), UCase(KeyWords(w)))
            If (u > 0) Then
                Exit For
            End If
        End If
    Next w
    Return
End Function

Private Function TriggerRx(ALabel As Label, Idx As Integer) As Boolean
Dim j As Integer
Dim IType As Integer
Dim AEye As String
Dim ATemp As String
Dim MyRef As String
Dim ODInvIdNew As Long
Dim OSInvIdNew As Long
Dim ODInvIdDesc As String
Dim OSInvIdDesc As String
Dim InventoryOn As Boolean
If (Trim(ALabel.Caption) = "") Then
    CurrentIndex = Idx
    If (WearType = "PC") Then
        Call LoadCL(WearType)
    End If
Else
    If (WearType = "CL") Then
        InventoryOn = Not (CheckConfigCollection("INVENTORYON") = "F")
        frmEventMsgs.Header = "Action"
        frmEventMsgs.AcceptText = "CL Favorite"
        frmEventMsgs.RejectText = "CL Search"
        frmEventMsgs.CancelText = "Cancel"
        frmEventMsgs.Other0Text = "Print"
        frmEventMsgs.Other1Text = "Remove RX"
        frmEventMsgs.Other2Text = "Remove Lens"
        MyRef = ""
        If (WearType = "CL") Then
            MyRef = "Trial"
            If (Idx = 1) Then
                If (lblTrial1.Visible) Then
                    MyRef = "Change    to RX"
                End If
            ElseIf (Idx = 2) Then
                If (lblTrial2.Visible) Then
                    MyRef = "Change    to RX"
                End If
            ElseIf (Idx = 3) Then
                If (lblTrial3.Visible) Then
                    MyRef = "Change    to RX"
                End If
            ElseIf (Idx = 4) Then
                If (lblTrial4.Visible) Then
                    MyRef = "Change    to RX"
                End If
            ElseIf (Idx = 5) Then
                If (lblTrial5.Visible) Then
                    MyRef = "Change    to RX"
                End If
            ElseIf (Idx = 6) Then
                If (lblTrial6.Visible) Then
                    MyRef = "Change    to RX"
                End If
            ElseIf (Idx = 7) Then
                If (lblTrial7.Visible) Then
                    MyRef = "Change    to RX"
                End If
            ElseIf (Idx = 8) Then
                If (lblTrial8.Visible) Then
                    MyRef = "Change    to RX"
                End If
            End If
        End If
        frmEventMsgs.Other3Text = MyRef
        frmEventMsgs.Other4Text = ""
        If (WearType = "CL") Then
            If (Idx = 1) Then
                If (lblTrial1.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
                End If
            ElseIf (Idx = 2) Then
                If (lblTrial2.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
                End If
            ElseIf (Idx = 3) Then
                If (lblTrial3.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
                End If
            ElseIf (Idx = 4) Then
                If (lblTrial4.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
                End If
            ElseIf (Idx = 5) Then
                If (lblTrial5.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
                End If
            ElseIf (Idx = 6) Then
                If (lblTrial6.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
                End If
            ElseIf (Idx = 7) Then
                If (lblTrial7.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
                End If
            ElseIf (Idx = 8) Then
                If (lblTrial8.Visible) Then
                    frmEventMsgs.Other4Text = "Comment"
                End If
            End If
        End If
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Or (frmEventMsgs.Result = 2) Then
            IType = frmEventMsgs.Result
            frmEventMsgs.Header = "Eye"
            frmEventMsgs.AcceptText = "OD"
            frmEventMsgs.RejectText = "OS"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = "OU"
            frmEventMsgs.Other1Text = "Clear"
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            AEye = ""
            If (frmEventMsgs.Result = 1) Then
                AEye = "OD"
            ElseIf (frmEventMsgs.Result = 2) Then
                AEye = "OS"
            ElseIf (frmEventMsgs.Result = 3) Then
                AEye = "OU"
            ElseIf (frmEventMsgs.Result = 5) Then
                SelectedCLRx(Idx).EWCompany = ""
                SelectedCLRx(Idx).EWType = ""
                SelectedCLRx(Idx).InventoryIdOD = 0
                SelectedCLRx(Idx).InventoryIdOS = 0
                Call LoadWear(False, WearType)
                Exit Function
            End If
            If (IType = 5) Then ' old way no longer in use
                frmCLWear.PatientId = PatientId
                frmCLWear.AppointmentId = AppointmentId
                frmCLWear.CLInventoryId = 0
                frmCLWear.CLPrescription = ""
                frmCLWear.EyeContext = AEye
                frmCLWear.AddReading = ""
                frmCLWear.BaseCurve = ""
                frmCLWear.PeriphCurve = ""
                frmCLWear.Diameter = ""
                frmCLWear.Show 1
                If (frmCLWear.CLInventoryId > 0) And (Trim(frmCLWear.CLPrescription) <> "") Then
                    If (frmCLWear.EyeContext = "OU") Then
                        SelectedCLRx(Idx).EWType = frmCLWear.CLPrescription
                        Mid(SelectedCLRx(Idx).EWType, 2, 1) = "D"
                        SelectedCLRx(Idx).InventoryIdOD = frmCLWear.CLInventoryId
                        SelectedCLRx(Idx).EWCompany = frmCLWear.CLPrescription
                        Mid(SelectedCLRx(Idx).EWCompany, 2, 1) = "S"
                        SelectedCLRx(Idx).InventoryIdOS = frmCLWear.CLInventoryId
                    ElseIf (frmCLWear.EyeContext = "OD") Then
                        SelectedCLRx(Idx).EWType = frmCLWear.CLPrescription
                        SelectedCLRx(Idx).InventoryIdOD = frmCLWear.CLInventoryId
                    ElseIf (frmCLWear.EyeContext = "OS") Then
                        SelectedCLRx(Idx).EWCompany = frmCLWear.CLPrescription
                        SelectedCLRx(Idx).InventoryIdOS = frmCLWear.CLInventoryId
                    End If
                    Call LoadWear(False, WearType)
                End If
            ElseIf (IType = 2) Then
                frmCLSearch.Show 1
                If (frmCLSearch.InventoryId > 0) And (Trim(frmCLSearch.InventoryDescription) <> "") Then
                    If (AEye = "OU") Then
                        SelectedCLRx(Idx).EWType = "OD:" + frmCLSearch.InventoryDescription
                        SelectedCLRx(Idx).InventoryIdOD = frmCLSearch.InventoryId
                        SelectedCLRx(Idx).EWCompany = "OS:" + frmCLSearch.InventoryDescription
                        SelectedCLRx(Idx).InventoryIdOS = frmCLSearch.InventoryId
                    ElseIf (AEye = "OD") Then
                        SelectedCLRx(Idx).EWType = "OD:" + frmCLSearch.InventoryDescription
                        SelectedCLRx(Idx).InventoryIdOD = frmCLSearch.InventoryId
                    ElseIf (AEye = "OS") Then
                        SelectedCLRx(Idx).EWCompany = "OS:" + frmCLSearch.InventoryDescription
                        SelectedCLRx(Idx).InventoryIdOS = frmCLSearch.InventoryId
                    End If
                    Call LoadWear(False, WearType)
                End If
            ElseIf (IType = 1) Then
                If (frmCLFavs.LoadCLFavorites(True)) Then
                    frmCLFavs.Show 1
                    If (frmCLFavs.InventoryId > 0) And (Trim(frmCLFavs.InventoryDescription) <> "") Then
                        Call GetExactInvItem(Idx, AEye, frmCLFavs.InventoryId, ODInvIdNew, ODInvIdDesc, OSInvIdNew, OSInvIdDesc)
                        If (AEye = "OU") Then
                            If (ODInvIdNew < 1) Then
                                If Not (UseFavorite("OD")) Then
                                    Exit Function
                                End If
                            End If
                            If (OSInvIdNew < 1) Then
                                If Not (UseFavorite("OS")) Then
                                    Exit Function
                                End If
                            End If
                        ElseIf (AEye = "OD") Then
                            If (ODInvIdNew < 1) Then
                                If Not (UseFavorite("OD")) Then
                                    Exit Function
                                End If
                            End If
                        ElseIf (AEye = "OS") Then
                            If (OSInvIdNew < 1) Then
                                If Not (UseFavorite("OS")) Then
                                    Exit Function
                                End If
                            End If
                        End If
                        If (AEye = "OU") Then
                            If (ODInvIdNew > 0) Then
                                SelectedCLRx(Idx).EWType = "OD:" + ODInvIdDesc
                                SelectedCLRx(Idx).InventoryIdOD = ODInvIdNew
                            Else
                                SelectedCLRx(Idx).EWType = "OD:" + frmCLFavs.InventoryDescription
                                SelectedCLRx(Idx).InventoryIdOD = frmCLFavs.InventoryId
                            End If
                            If (OSInvIdNew > 0) Then
                                SelectedCLRx(Idx).EWCompany = "OS:" + OSInvIdDesc
                                SelectedCLRx(Idx).InventoryIdOS = OSInvIdNew
                            Else
                                SelectedCLRx(Idx).EWCompany = "OS:" + frmCLFavs.InventoryDescription
                                SelectedCLRx(Idx).InventoryIdOS = frmCLFavs.InventoryId
                            End If
                        ElseIf (AEye = "OD") Then
                            If (ODInvIdNew > 0) Then
                                SelectedCLRx(Idx).EWType = "OD:" + ODInvIdDesc
                                SelectedCLRx(Idx).InventoryIdOD = ODInvIdNew
                            Else
                                SelectedCLRx(Idx).EWType = "OD:" + frmCLFavs.InventoryDescription
                                SelectedCLRx(Idx).InventoryIdOD = frmCLFavs.InventoryId
                            End If
                        ElseIf (AEye = "OS") Then
                            If (OSInvIdNew > 0) Then
                                SelectedCLRx(Idx).EWCompany = "OS:" + OSInvIdDesc
                                SelectedCLRx(Idx).InventoryIdOS = OSInvIdNew
                            Else
                                SelectedCLRx(Idx).EWCompany = "OS:" + frmCLFavs.InventoryDescription
                                SelectedCLRx(Idx).InventoryIdOS = frmCLFavs.InventoryId
                            End If
                        End If
                        Call LoadWear(False, WearType)
                    End If
                End If
            End If
        ElseIf (frmEventMsgs.Result = 3) Then
            Call DoEyeWearPrint(Idx, True, True, 1)
        ElseIf (frmEventMsgs.Result = 5) Then
            ALabel.Caption = ""
            ALabel.Visible = False
            ChangeOn = True
            For j = Idx To MaxSetCL - 1
                SelectedCLRx(j) = SelectedCLRx(j + 1)
            Next j
            SelectedCLRx(MaxSetCL).EWCompany = ""
            SelectedCLRx(MaxSetCL).EWType = ""
            SelectedCLRx(MaxSetCL).ODManifest = ""
            SelectedCLRx(MaxSetCL).OSManifest = ""
            SelectedCLRx(MaxSetCL).RxNote = ""
            CurrRx = CurrRx - 1
            If (CurrRx < 1) Then
                CurrRx = 1
            End If
            CurrRxCnt = CurrRxCnt - 1
            If (CurrRxCnt < 1) Then
                CurrRxCnt = 1
            End If
            Call cmdMore_Click
            Call LoadWear(False, WearType)
        ElseIf (frmEventMsgs.Result = 6) Then
            SelectedCLRx(Idx).EWType = ""
            SelectedCLRx(Idx).InventoryIdOD = 0
            SelectedCLRx(Idx).EWCompany = ""
            SelectedCLRx(Idx).InventoryIdOS = 0
            Call LoadWear(False, WearType)
        ElseIf (frmEventMsgs.Result = 7) Then
            If (frmEventMsgs.Other3Text = "Change    to RX") Then
                Call DoPrescriptions(Idx, True)
            Else
                Call DoPrescriptions(Idx, False)
            End If
        ElseIf (frmEventMsgs.Result = 8) Then
            Call DoDescriptors(Idx)
        End If
    ElseIf (WearType = "PC") Then
        If Not (NewWearOn) Then
            frmEventMsgs.Header = "Action"
            frmEventMsgs.AcceptText = "Print"
            frmEventMsgs.RejectText = "Remove"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                Call DoEyeWearPrint(Idx, True, True, 1)
            ElseIf (frmEventMsgs.Result = 2) Then
                ALabel.Caption = ""
                ALabel.Visible = False
                ChangeOn = True
                For j = Idx To 2
                    SelectedPCRx(j) = SelectedPCRx(j + 1)
                Next j
                SelectedPCRx(3).EWCompany = ""
                SelectedPCRx(3).EWType = ""
                SelectedPCRx(3).ODManifest = ""
                SelectedPCRx(3).OSManifest = ""
                SelectedPCRx(3).RxNote = ""
                If (TotalPCRx < 1) Then
                    Call InitializePCRx
                    Call LoadEyeRx(True)
                Else
                    Call LoadEyeRx(False)
                End If
            End If
        Else
' new editting screens
        End If
    End If
End If
End Function

Private Sub lblNote1_Click()
If (txtNote1.Visible) Then
    txtMagnify.Text = txtNote1.Text
Else
    txtMagnify.Text = txtNote5.Text
End If
txtMagnify.Locked = True
txtMagnify.Visible = True
End Sub

Private Sub lblNote2_Click()
If (txtNote2.Visible) Then
    txtMagnify.Text = txtNote2.Text
Else
    txtMagnify.Text = txtNote6.Text
End If
txtMagnify.Locked = True
txtMagnify.Visible = True
End Sub

Private Sub lblNote3_Click()
If (txtNote3.Visible) Then
    txtMagnify.Text = txtNote3.Text
Else
    txtMagnify.Text = txtNote7.Text
End If
txtMagnify.Locked = True
txtMagnify.Visible = True
End Sub

Private Sub lblNote4_Click()
If (txtNote4.Visible) Then
    txtMagnify.Text = txtNote4.Text
Else
    txtMagnify.Text = txtNote8.Text
End If
txtMagnify.Locked = True
txtMagnify.Visible = True
End Sub

Private Sub lblRx1_Click()
Call TriggerRx(lblRx1, 1)
End Sub

Private Sub lblRx2_Click()
Call TriggerRx(lblRx2, 2)
End Sub

Private Sub lblRx3_Click()
Call TriggerRx(lblRx3, 3)
End Sub

Private Sub lblRx4_Click()
Call TriggerRx(lblRx4, 4)
End Sub

Private Sub lblRx5_Click()
Call TriggerRx(lblRx5, 5)
End Sub

Private Sub lblRx6_Click()
Call TriggerRx(lblRx6, 6)
End Sub

Private Sub lblRx7_Click()
Call TriggerRx(lblRx7, 7)
End Sub

Private Sub lblRx8_Click()
Call TriggerRx(lblRx8, 8)
End Sub

Private Function MakeMrxConvert(AQuestion As String, RTag As String, ATest As Integer, AFile As String, TheFile As String) As Boolean
Dim AForm As Long
Dim ADone As Boolean
Dim TestOn As Boolean
Dim i As Integer, j As Integer
Dim q As Integer, ThePtr As Integer
Dim CTemp As String
Dim OTag As String, ATag As String
Dim ARec As String, Temp As String
Dim ATemp As String, BTemp As String
Dim JTemp As String, TestTime As String
Dim RetCtl As DynamicControls
TestOn = False
OTag = "ContactLens"
ATag = "CE"
AForm = 203
If (RTag = "Glasses") Then
    OTag = "Glasses"
    ATag = "GL"
    AForm = 202
End If
FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(1)
Print #1, "QUESTION=\" + UCase(Trim(AQuestion))
Call FormatTimeNow(TestTime)
Print #1, "TIME=" + TestTime
FM.OpenFile AFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(2)
Line Input #2, ARec
If Not (EOF(2)) Then
    Line Input #2, ARec
    If (ATest = 7) Or (ATest = 8) Then
        Line Input #2, ARec
        If (InStrPS(ARec, "Pair-" + Trim(str(ATest - 6))) <> 0) Then
            TestOn = True
        ElseIf (InStrPS(ARec, "PAIR-" + Trim(str(ATest - 6))) <> 0) Then
            TestOn = True
        End If
    End If
End If
If (ATest = 9) Then
    If (InStrPS(ARec, "ARF-" + Trim(str(ATest - 8))) <> 0) Then
        TestOn = True
    ElseIf (InStrPS(ARec, "Arf-" + Trim(str(ATest - 8))) <> 0) Then
        TestOn = True
    End If
End If
Do Until (EOF(2)) Or (TestOn)
    Line Input #2, ARec
    If (InStrPS(ARec, "MRX-" + Trim(str(ATest))) <> 0) Then
        TestOn = True
    ElseIf (InStrPS(ARec, "RefractCL-" + Trim(str(ATest - 1))) <> 0) Then
        TestOn = True
    ElseIf (InStrPS(ARec, "ARF-" + Trim(str(ATest - 8))) <> 0) Then
        TestOn = True
    ElseIf (InStrPS(ARec, "Pair-" + Trim(str(ATest - 6))) <> 0) Then
        TestOn = True
    End If
Loop
If (TestOn) Then
    ADone = False
    Do Until (EOF(2)) Or (ADone)
        Line Input #2, ARec
        If (ATest < 7) Then
            If (InStrPS(ARec, "MRX") = 0) Or (Left(ARec, 5) = "Recap") Then
                Temp = UCase(ARec)
                Call ReplaceCharacters(Temp, "MANIFEST REFRACTION", UCase(RTag))
                Call ReplaceCharacters(Temp, "MR", ATag)
                GoSub GetRecPtr
                If (ThePtr > 0) Then
                    i = InStrPS(Temp, "=T")
                    If (i > 0) Then
                        j = InStrPS(i, Temp, " ")
                        If (j = 0) Then
                            j = Len(Temp)
                        End If
                        Temp = Left(Temp, i + 1) + Trim(str(ThePtr)) + Mid(Temp, j, Len(Temp) - (j - 1))
                        Print #1, Temp
                    End If
                End If
            Else
                ADone = True
            End If
        ElseIf (ATest = 7) Or (ATest = 8) Then
            If (InStrPS(ARec, "Pair") = 0) Or (Left(ARec, 5) = "Recap") Then
                Temp = UCase(ARec)
                If (RTag <> "ContactLens") Then
                    Call ReplaceCharacters(Temp, "GLASSES", UCase(RTag))
                    Call ReplaceCharacters(Temp, "GL", ATag)
                Else
                    If (Left(Temp, 3) = "*OD") Or (Left(Temp, 3) = "*OS") Then
                        Call ReplaceCharacters(Temp, "CONTACTLENS", UCase(ATag))
                    Else
                        Call ReplaceCharacters(Temp, "CONTACTLENS", UCase(RTag))
                        Call ReplaceCharacters(Temp, "CE", ATag)
                    End If
                End If
                GoSub GetRecPtr
                If (ThePtr > 0) Then
                    i = InStrPS(Temp, "=T")
                    If (i > 0) Then
                        j = InStrPS(i, Temp, " ")
                        If (j = 0) Then
                            j = Len(Temp)
                        End If
                        Temp = Left(Temp, i + 1) + Trim(str(ThePtr)) + Mid(Temp, j, Len(Temp) - (j - 1))
                        Print #1, Temp
                    End If
                End If
            Else
                ADone = True
            End If
        ElseIf (ATest = 9) Then
            If (InStrPS(ARec, "ARF") = 0) Or (Left(ARec, 5) = "Recap") Then
                Temp = UCase(ARec)
                Call ReplaceCharacters(Temp, "AUTOREFRACTION", UCase(RTag))
                Call ReplaceCharacters(Temp, "AUTOREFRACTION", ATag)
                GoSub GetRecPtr
                If (ThePtr > 0) Then
                    i = InStrPS(Temp, "=T")
                    If (i > 0) Then
                        j = InStrPS(i, Temp, " ")
                        If (j = 0) Then
                            j = Len(Temp)
                        End If
                        Temp = Left(Temp, i + 1) + Trim(str(ThePtr)) + Mid(Temp, j, Len(Temp) - (j - 1))
                        Print #1, Temp
                    End If
                End If
            Else
                ADone = True
            End If
        End If
    Loop
End If
FM.CloseFile CLng(1)
FM.CloseFile CLng(2)
Exit Function
GetRecPtr:
    ThePtr = 0
    i = InStrPS(Temp, "-" + UCase(ATag))
    If (i < 1) Then
        i = InStrPS(Temp, "-" + UCase(RTag))
        If (i < 1) Then
            i = InStrPS(Temp, "=T")
            If (i > 0) Then
                Temp = Left(Temp, i - 1) + "-" + RTag + Mid(Temp, i, Len(Temp) - (i - 1))
                Temp = UCase(Temp)
            End If
        End If
    End If
    i = InStrPS(Temp, "=T")
    If (i > 0) Then
        ATemp = Left(Temp, i - 1)
        BTemp = ATemp
        If (Left(ATemp, 1) = "*") Then
            ATemp = Mid(ATemp, 2, Len(ATemp) - 1)
            If (UCase(Left(ATemp, 8)) = "BALANCEL") Then
                BTemp = Left(ATemp, 7) + "-" + Mid(ATemp, 8, Len(ATemp) - 7)
                If (InStrPS(BTemp, "GL") > 0) Then
                    Call ReplaceCharacters(BTemp, "GL", "GLASSES")
                End If
            Else
                BTemp = ATemp
            End If
        End If
        Call StripCharacters(ATemp, " ")
        Call StripCharacters(BTemp, " ")
        CTemp = BTemp
        If (InStrPS(CTemp, "-" + ATag) > 0) Then
            Call ReplaceCharacters(CTemp, "-" + ATag, "-" + UCase(OTag))
        ElseIf (InStrPS(CTemp, "-" + OTag) > 0) Then
            Call ReplaceCharacters(CTemp, "-" + OTag, "-" + UCase(ATag))
        End If
        Set RetCtl = New DynamicControls
        RetCtl.FormId = AForm
        RetCtl.ControlName = ""
        If (RetCtl.FindControl > 0) Then
            q = 1
            Do Until Not (RetCtl.SelectControl(q)) Or (ThePtr > 0)
                JTemp = Trim(RetCtl.ControlName)
                Call StripCharacters(JTemp, " ")
                If (UCase(JTemp) = ATemp) Then
                    ThePtr = RetCtl.ControlId
                    Exit Do
                ElseIf (UCase(JTemp) = BTemp) Then
                    ThePtr = RetCtl.ControlId
                    Exit Do
                ElseIf (UCase(JTemp) = CTemp) Then
                    ThePtr = RetCtl.ControlId
                    Exit Do
                End If
                q = q + 1
            Loop
        End If
        Set RetCtl = Nothing
    End If
    Return
End Function

Public Function PrintEyeWearRx(AType As String, APrinter As String) As Boolean
PrintEyeWearRx = True
If (Trim(AType) <> "") Then
    Call LoadWear(True, AType)
    ThePrinter = APrinter
    If (Trim(ThePrinter) = "") Then
        ThePrinter = "X"
    End If
    Call cmdPrint_Click
End If
End Function

Private Sub lblTrial1_Click()
If (lblTrial1.BackColor = TrialOrderColor) Then
    lblTrial1.BackColor = TrialNormalColor
    lblTrial1.Caption = "Trial, Click to Order"
    If (Trim(SelectedCLRx(1).RxNote) <> "") Then
        Mid(SelectedCLRx(1).RxNote, 1, 1) = "*"
    Else
        SelectedCLRx(1).RxNote = "*"
    End If
Else
    lblTrial1.BackColor = TrialOrderColor
    lblTrial1.Caption = "Order Trial, Click for Trial"
    If (Trim(SelectedCLRx(1).RxNote) <> "") Then
        Mid(SelectedCLRx(1).RxNote, 1, 1) = "~"
    Else
        SelectedCLRx(1).RxNote = "~"
    End If
End If
End Sub

Private Sub lblTrial2_Click()
If (lblTrial2.BackColor = TrialOrderColor) Then
    lblTrial2.BackColor = TrialNormalColor
    lblTrial2.Caption = "Trial, Click to Order"
    If (Trim(SelectedCLRx(2).RxNote) <> "") Then
        Mid(SelectedCLRx(2).RxNote, 1, 1) = "*"
    Else
        SelectedCLRx(2).RxNote = "*"
    End If
Else
    lblTrial2.BackColor = TrialOrderColor
    lblTrial2.Caption = "Order Trial, Click for Trial"
    If (Trim(SelectedCLRx(2).RxNote) <> "") Then
        Mid(SelectedCLRx(2).RxNote, 1, 1) = "~"
    Else
        SelectedCLRx(2).RxNote = "~"
    End If
End If
End Sub

Private Sub lblTrial3_Click()
If (lblTrial3.BackColor = TrialOrderColor) Then
    lblTrial3.BackColor = TrialNormalColor
    lblTrial3.Caption = "Trial, Click to Order"
    If (Trim(SelectedCLRx(3).RxNote) <> "") Then
        Mid(SelectedCLRx(3).RxNote, 1, 1) = "*"
    Else
        SelectedCLRx(3).RxNote = "*"
    End If
Else
    lblTrial3.BackColor = TrialOrderColor
    lblTrial3.Caption = "Order Trial, Click for Trial"
    If (Trim(SelectedCLRx(3).RxNote) <> "") Then
        Mid(SelectedCLRx(3).RxNote, 1, 1) = "~"
    Else
        SelectedCLRx(3).RxNote = "~"
    End If
End If
End Sub

Private Sub lblTrial4_Click()
If (lblTrial4.BackColor = TrialOrderColor) Then
    lblTrial4.BackColor = TrialNormalColor
    lblTrial4.Caption = "Trial, Click to Order"
    If (Trim(SelectedCLRx(4).RxNote) <> "") Then
        Mid(SelectedCLRx(4).RxNote, 1, 1) = "*"
    Else
        SelectedCLRx(4).RxNote = "*"
    End If
Else
    lblTrial4.Caption = "Order Trial, Click for Trial"
    lblTrial4.BackColor = TrialOrderColor
    If (Trim(SelectedCLRx(4).RxNote) <> "") Then
        Mid(SelectedCLRx(4).RxNote, 1, 1) = "~"
    Else
        SelectedCLRx(4).RxNote = "~"
    End If
End If
End Sub

Private Sub lblTrial5_Click()
If (lblTrial5.BackColor = TrialOrderColor) Then
    lblTrial5.BackColor = TrialNormalColor
    lblTrial5.Caption = "Trial, Click to Order"
    If (Trim(SelectedCLRx(5).RxNote) <> "") Then
        Mid(SelectedCLRx(5).RxNote, 1, 1) = "*"
    Else
        SelectedCLRx(5).RxNote = "*"
    End If
Else
    lblTrial5.BackColor = TrialOrderColor
    lblTrial5.Caption = "Order Trial, Click for Trial"
    If (Trim(SelectedCLRx(5).RxNote) <> "") Then
        Mid(SelectedCLRx(5).RxNote, 1, 1) = "~"
    Else
        SelectedCLRx(5).RxNote = "~"
    End If
End If
End Sub

Private Sub lblTrial6_Click()
If (lblTrial6.BackColor = TrialOrderColor) Then
    lblTrial6.BackColor = TrialNormalColor
    lblTrial6.Caption = "Trial, Click to Order"
    If (Trim(SelectedCLRx(6).RxNote) <> "") Then
        Mid(SelectedCLRx(6).RxNote, 1, 1) = "*"
    Else
        SelectedCLRx(6).RxNote = "*"
    End If
Else
    lblTrial6.BackColor = TrialOrderColor
    lblTrial6.Caption = "Order Trial, Click for Trial"
    If (Trim(SelectedCLRx(6).RxNote) <> "") Then
        Mid(SelectedCLRx(6).RxNote, 1, 1) = "~"
    Else
        SelectedCLRx(6).RxNote = "~"
    End If
End If
End Sub

Private Sub lblTrial7_Click()
If (lblTrial7.BackColor = TrialOrderColor) Then
    lblTrial7.BackColor = TrialNormalColor
    lblTrial7.Caption = "Trial, Click to Order"
    If (Trim(SelectedCLRx(7).RxNote) <> "") Then
        Mid(SelectedCLRx(7).RxNote, 1, 1) = "*"
    Else
        SelectedCLRx(7).RxNote = "*"
    End If
Else
    lblTrial7.BackColor = TrialOrderColor
    lblTrial7.Caption = "Order Trial, Click for Trial"
    If (Trim(SelectedCLRx(7).RxNote) <> "") Then
        Mid(SelectedCLRx(7).RxNote, 1, 1) = "~"
    Else
        SelectedCLRx(7).RxNote = "~"
    End If
End If
End Sub

Private Sub lblTrial8_Click()
If (lblTrial8.BackColor = TrialOrderColor) Then
    lblTrial8.BackColor = TrialNormalColor
    lblTrial8.Caption = "Trial, Click to Order"
    If (Trim(SelectedCLRx(8).RxNote) <> "") Then
        Mid(SelectedCLRx(8).RxNote, 1, 1) = "*"
    Else
        SelectedCLRx(8).RxNote = "*"
    End If
Else
    lblTrial8.BackColor = TrialOrderColor
    lblTrial8.Caption = "Order Trial, Click for Trial"
    If (Trim(SelectedCLRx(8).RxNote) <> "") Then
        Mid(SelectedCLRx(8).RxNote, 1, 1) = "~"
    Else
        SelectedCLRx(8).RxNote = "~"
    End If
End If
End Sub

Private Sub lstHx_Click()
If (lstHx.ListIndex = 0) Then
    lstHx.Clear
    lstHx.Visible = False
    Call ControlHistory(True)
End If
End Sub

Private Sub txtMagnify_Click()
txtMagnify.Visible = False
End Sub

Private Sub txtNote1_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii = 13) Then
    Temp = Trim(txtNote1.Text)
    Call StripCharacters(Temp, "*")
    txtNote1.Text = Temp
    If (WearType = "CL") Then
        Temp = Mid(SelectedCLRx(1).RxNote, 1, 1)
        If (Trim(Temp) <> "") Then
            If (InStrPS("*^~", Temp) > 0) Then
                SelectedCLRx(1).RxNote = Temp + Trim(txtNote1.Text)
            Else
                SelectedCLRx(1).RxNote = Trim(txtNote1.Text)
            End If
        Else
            SelectedCLRx(1).RxNote = Trim(txtNote1.Text)
        End If
    Else
        SelectedPCRx(1).RxNote = Trim(txtNote1.Text)
    End If
    lblNote1.BackColor = EnlargeOff
    If (IsNoteBig(txtNote1.Text)) Then
        lblNote1.BackColor = EnlargeOn
    End If
Else
    If (WearType = "CL") Then
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedCLRx(1).RxNote = SelectedCLRx(1).RxNote + Chr(KeyAscii)
        End If
    Else
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedPCRx(1).RxNote = SelectedPCRx(1).RxNote + Chr(KeyAscii)
        End If
    End If
End If
End Sub

Private Sub txtNote1_Validate(Cancel As Boolean)
Call txtNote1_KeyPress(13)
End Sub

Private Sub txtNote2_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii = 13) Then
    Temp = Trim(txtNote2.Text)
    Call StripCharacters(Temp, "*")
    txtNote1.Text = Temp
    If (WearType = "CL") Then
        Temp = Mid(SelectedCLRx(2).RxNote, 1, 1)
        If (Trim(Temp) <> "") Then
            If (InStrPS("*^~", Temp) > 0) Then
                SelectedCLRx(2).RxNote = Temp + Trim(txtNote2.Text)
            Else
                SelectedCLRx(2).RxNote = Trim(txtNote2.Text)
            End If
        Else
            SelectedCLRx(2).RxNote = Trim(txtNote2.Text)
        End If
    Else
        SelectedPCRx(2).RxNote = Trim(txtNote2.Text)
    End If
    lblNote2.BackColor = EnlargeOff
    If (IsNoteBig(txtNote2.Text)) Then
        lblNote2.BackColor = EnlargeOn
    End If
Else
    If (WearType = "CL") Then
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedCLRx(2).RxNote = SelectedCLRx(2).RxNote + Chr(KeyAscii)
        End If
    Else
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedPCRx(2).RxNote = SelectedPCRx(2).RxNote + Chr(KeyAscii)
        End If
    End If
End If
End Sub

Private Sub txtNote2_Validate(Cancel As Boolean)
Call txtNote2_KeyPress(13)
End Sub

Private Sub txtNote3_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii = 13) Then
    Temp = Trim(txtNote3.Text)
    Call StripCharacters(Temp, "*")
    txtNote3.Text = Temp
    If (WearType = "CL") Then
        Temp = Mid(SelectedCLRx(3).RxNote, 1, 1)
        If (Trim(Temp) <> "") Then
            If (InStrPS("*^~", Temp) > 0) Then
                SelectedCLRx(3).RxNote = Temp + Trim(txtNote3.Text)
            Else
                SelectedCLRx(3).RxNote = Trim(txtNote3.Text)
            End If
        Else
            SelectedCLRx(3).RxNote = Trim(txtNote3.Text)
        End If
    Else
        SelectedPCRx(3).RxNote = Trim(txtNote3.Text)
    End If
    lblNote3.BackColor = EnlargeOff
    If (IsNoteBig(txtNote3.Text)) Then
        lblNote3.BackColor = EnlargeOn
    End If
Else
    If (WearType = "CL") Then
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedCLRx(3).RxNote = SelectedCLRx(3).RxNote + Chr(KeyAscii)
        End If
    Else
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedPCRx(3).RxNote = SelectedPCRx(3).RxNote + Chr(KeyAscii)
        End If
    End If
End If
End Sub

Private Sub txtNote3_Validate(Cancel As Boolean)
Call txtNote3_KeyPress(13)
End Sub

Private Sub txtNote4_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii = 13) Then
    Temp = Trim(txtNote4.Text)
    Call StripCharacters(Temp, "*")
    txtNote1.Text = Temp
    If (WearType = "CL") Then
        Temp = Mid(SelectedCLRx(4).RxNote, 1, 1)
        If (Trim(Temp) <> "") Then
            If (InStrPS("*^~", Temp) > 0) Then
                SelectedCLRx(4).RxNote = Temp + Trim(txtNote4.Text)
            Else
                SelectedCLRx(4).RxNote = Trim(txtNote4.Text)
            End If
        Else
            SelectedCLRx(4).RxNote = Trim(txtNote4.Text)
        End If
    lblNote4.BackColor = EnlargeOff
    If (IsNoteBig(txtNote4.Text)) Then
        lblNote4.BackColor = EnlargeOn
    End If
    Else
        SelectedPCRx(4).RxNote = Trim(txtNote4.Text)
    End If
Else
    If (WearType = "CL") Then
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedCLRx(4).RxNote = SelectedCLRx(4).RxNote + Chr(KeyAscii)
        End If
    Else
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedPCRx(4).RxNote = SelectedPCRx(4).RxNote + Chr(KeyAscii)
        End If
    End If
End If
End Sub

Private Sub txtNote4_Validate(Cancel As Boolean)
Call txtNote4_KeyPress(13)
End Sub

Private Sub txtNote5_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii = 13) Then
    Temp = Trim(txtNote5.Text)
    Call StripCharacters(Temp, "*")
    txtNote1.Text = Temp
    If (WearType = "CL") Then
        Temp = Mid(SelectedCLRx(5).RxNote, 1, 1)
        If (Trim(Temp) <> "") Then
            If (InStrPS("*^~", Temp) > 0) Then
                SelectedCLRx(5).RxNote = Temp + Trim(txtNote5.Text)
            Else
                SelectedCLRx(5).RxNote = Trim(txtNote5.Text)
            End If
        Else
            SelectedCLRx(5).RxNote = Trim(txtNote5.Text)
        End If
    Else
        SelectedPCRx(5).RxNote = Trim(txtNote5.Text)
    End If
    lblNote1.BackColor = EnlargeOff
    If (IsNoteBig(txtNote1.Text)) Then
        lblNote1.BackColor = EnlargeOn
    End If
Else
    If (WearType = "CL") Then
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedCLRx(5).RxNote = SelectedCLRx(5).RxNote + Chr(KeyAscii)
        End If
    Else
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedPCRx(6).RxNote = SelectedPCRx(6).RxNote + Chr(KeyAscii)
        End If
    End If
End If
End Sub

Private Sub txtNote5_Validate(Cancel As Boolean)
Call txtNote5_KeyPress(13)
End Sub

Private Sub txtNote6_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii = 13) Then
    Temp = Trim(txtNote6.Text)
    Call StripCharacters(Temp, "*")
    txtNote1.Text = Temp
    If (WearType = "CL") Then
        Temp = Mid(SelectedCLRx(6).RxNote, 1, 1)
        If (Trim(Temp) <> "") Then
            If (InStrPS("*^~", Temp) > 0) Then
                SelectedCLRx(6).RxNote = Temp + Trim(txtNote6.Text)
            Else
                SelectedCLRx(6).RxNote = Trim(txtNote6.Text)
            End If
        Else
            SelectedCLRx(6).RxNote = Trim(txtNote6.Text)
        End If
    Else
        SelectedPCRx(6).RxNote = Trim(txtNote6.Text)
    End If
    lblNote2.BackColor = EnlargeOff
    If (IsNoteBig(txtNote2.Text)) Then
        lblNote2.BackColor = EnlargeOn
    End If
Else
    If (WearType = "CL") Then
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedCLRx(6).RxNote = SelectedCLRx(6).RxNote + Chr(KeyAscii)
        End If
    Else
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedPCRx(6).RxNote = SelectedPCRx(6).RxNote + Chr(KeyAscii)
        End If
    End If
End If
End Sub

Private Sub txtNote6_Validate(Cancel As Boolean)
Call txtNote6_KeyPress(13)
End Sub

Private Sub txtNote7_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii = 13) Then
    Temp = Trim(txtNote7.Text)
    Call StripCharacters(Temp, "*")
    txtNote1.Text = Temp
    If (WearType = "CL") Then
        Temp = Mid(SelectedCLRx(7).RxNote, 1, 1)
        If (Trim(Temp) <> "") Then
            If (InStrPS("*^~", Temp) > 0) Then
                SelectedCLRx(7).RxNote = Temp + Trim(txtNote7.Text)
            Else
                SelectedCLRx(7).RxNote = Trim(txtNote7.Text)
            End If
        Else
            SelectedCLRx(7).RxNote = Trim(txtNote7.Text)
        End If
    Else
        SelectedPCRx(7).RxNote = Trim(txtNote7.Text)
    End If
    lblNote3.BackColor = EnlargeOff
    If (IsNoteBig(txtNote3.Text)) Then
        lblNote3.BackColor = EnlargeOn
    End If
Else
    If (WearType = "CL") Then
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedCLRx(7).RxNote = SelectedCLRx(7).RxNote + Chr(KeyAscii)
        End If
    Else
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedPCRx(7).RxNote = SelectedPCRx(7).RxNote + Chr(KeyAscii)
        End If
    End If
End If
End Sub

Private Sub txtNote7_Validate(Cancel As Boolean)
Call txtNote7_KeyPress(13)
End Sub

Private Sub txtNote8_KeyPress(KeyAscii As Integer)
Dim Temp As String
If (KeyAscii = 13) Then
    Temp = Trim(txtNote8.Text)
    Call StripCharacters(Temp, "*")
    txtNote1.Text = Temp
    If (WearType = "CL") Then
        Temp = Mid(SelectedCLRx(8).RxNote, 1, 1)
        If (Trim(Temp) <> "") Then
            If (InStrPS("*^~", Temp) > 0) Then
                SelectedCLRx(8).RxNote = Temp + Trim(txtNote8.Text)
            Else
                SelectedCLRx(8).RxNote = Trim(txtNote8.Text)
            End If
        Else
            SelectedCLRx(8).RxNote = Trim(txtNote8.Text)
        End If
    Else
        SelectedPCRx(8).RxNote = Trim(txtNote8.Text)
    End If
    lblNote4.BackColor = EnlargeOff
    If (IsNoteBig(txtNote4.Text)) Then
        lblNote4.BackColor = EnlargeOn
    End If
Else
    If (WearType = "CL") Then
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedCLRx(8).RxNote = SelectedCLRx(8).RxNote + Chr(KeyAscii)
        End If
    Else
        If (KeyAscii > 31) And (KeyAscii < 125) Then
            SelectedPCRx(8).RxNote = SelectedPCRx(8).RxNote + Chr(KeyAscii)
        End If
    End If
End If
End Sub

Private Sub txtNote8_Validate(Cancel As Boolean)
Call txtNote8_KeyPress(13)
End Sub

Private Function GetExactInvItem(itm As Integer, AEye As String, InvId As Long, ODInvId As Long, ODInvIdDesc As String, OSInvId As Long, OSInvIdDesc As String) As Boolean
Dim Idx As Integer
Dim u As Integer, q As Integer
Dim i As Integer, w As Integer
Dim MyId As Long
Dim KeyWords(5) As String
Dim Rx As String
Dim ASeries As String, TName As String
Dim Temp As String, ATemp As String
Dim strRef1 As String, strRef2 As String
Dim strRef3 As String, strRef4 As String
Dim strRef5 As String, strAxis As String
Dim strSphere As String, strCyl As String
Dim RetInv As CLInventory
ODInvId = 0
ODInvIdDesc = ""
OSInvId = 0
OSInvIdDesc = ""
GetExactInvItem = False
If (itm > 0) And (InvId > 0) Then
    KeyWords(1) = "ADD-READING:"
    KeyWords(2) = "BASE CURVE:"
    KeyWords(3) = "DIAMETER:"
    KeyWords(4) = "PERIPH CURVE:"
    KeyWords(5) = ""
    If (AEye = "OD") Or (AEye = "OU") Then
        Rx = Trim(SelectedCLRx(itm).ODManifest)
        GoSub GetData
        GoSub GetSeries
        ODInvId = MyId
        ODInvIdDesc = ASeries
    End If
    If (AEye = "OS") Or (AEye = "OU") Then
        If (Trim(SelectedCLRx(itm).ODManifest) <> Trim(SelectedCLRx(itm).OSManifest)) Then
            Rx = Trim(SelectedCLRx(itm).OSManifest)
            GoSub GetData
            GoSub GetSeries
            OSInvId = MyId
            OSInvIdDesc = ASeries
        Else
            OSInvId = ODInvId
            OSInvIdDesc = ODInvIdDesc
        End If
    Else
    End If
    If (ODInvId > 0) Or (OSInvId > 0) Then
        GetExactInvItem = True
    End If
End If
Exit Function
GetData:
    strSphere = ""
    strCyl = ""
    strAxis = ""
    strRef1 = ""
    strRef2 = ""
    strRef3 = ""
    strRef4 = ""
    strRef5 = ""
    If (Left(Rx, 2) = "OD") Or (Left(Rx, 2) = "OS") Then
        Rx = Trim(Mid(Rx, 4, Len(Rx) - 3))
    End If
    q = InStrPS(Rx, " ")
    If (q > 0) Then
        Temp = Trim(Left(Rx, q))
        If (Temp = "NE") Then
            Temp = ""
        End If
        w = InStrPS(q + 1, Rx, " ")
        If (w > 0) Then
            TName = Mid(Rx, q + 1, (w - 1) - q)
            If (UCase(TName) = "BALANCE") Then
                q = w + 5
            ElseIf (UCase(TName) = "VERTEX") Then
            
            End If
        End If
    ElseIf (Trim(Rx) <> "") Then
        Temp = Trim(Rx)
    End If
    strSphere = Temp
    
    If (UCase(TName) <> "VERTEX") Then
        i = InStrPS(q + 1, Rx, " ")
        If (i > 0) Then
            Temp = Trim(Mid(Rx, q + 1, i - q))
            If (Temp = "NE") Then
                Temp = "    "
            End If
            If (UCase(Left(Temp, 3)) <> "ADD") And (UCase(Left(Temp, 3)) <> "PRI") And (UCase(Left(Temp, 3)) <> "BAS") And (UCase(Left(Temp, 3)) <> "DIA") And (UCase(Left(Temp, 1)) <> "X") Then
                If (UCase(Temp) = "VERTEX") Then
                    Temp = ""
                End If
            End If
            strCyl = Temp
        End If
        
        q = InStrPS(Rx, "X")
        If (q < 1) Then
            q = InStrPS(Rx, "x")
        End If
        If (q > 0) Then
            i = InStrPS(q, Rx, " ")
            If (i > 0) Then
                Temp = Trim(Mid(Rx, q, i - (q - 1)))
                If (Temp = "NE") Then
                    Temp = "    "
                End If
                If (UCase(Temp) = "X") Then
                    Temp = "    "
                End If
            Else
                Temp = Trim(Mid(Rx, q, Len(Rx) - (q - 1)))
                If (Temp = "NE") Then
                    Temp = ""
                End If
                If (UCase(Temp) = "X") Then
                    Temp = ""
                End If
            End If
            strAxis = Temp
        End If
    End If
' Rx Data
    Idx = 1
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef1 = ATemp
        If (strRef1 = "NE") Then
            strRef1 = ""
        End If
    End If
    
    Idx = 2
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef2 = ATemp
        If (strRef2 = "NE") Then
            strRef2 = ""
        End If
    End If
    
    Idx = 3
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef3 = ATemp
        If (strRef3 = "NE") Then
            strRef3 = ""
        End If
    End If
    
    Idx = 4
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef4 = ATemp
        If (strRef4 = "NE") Then
            strRef4 = ""
        End If
    End If

    Idx = 5
    Temp = Rx
    GoSub GetField
    If (ATemp <> "") Then
        strRef5 = ATemp
        If (strRef5 = "NE") Then
            strRef5 = ""
        End If
    End If
    Return
GetField:
    ATemp = ""
    If (Idx > 0) Then
        If (KeyWords(Idx) <> "") Then
            i = InStrPS(UCase(Temp), UCase(KeyWords(Idx)))
            If (i > 0) Then
                GoSub PutEnd
                If (u > 0) Then
                    ATemp = Mid(Temp, i, (u - 1) - (i - 1))
                Else
                    ATemp = Mid(Temp, i, Len(Temp) - (i - 1))
                End If
            End If
        End If
    End If
    If (Trim(ATemp) <> "") Then
        i = InStrPS(ATemp, ":")
        If (i > 0) Then
            ATemp = Trim(Mid(ATemp, i + 1, Len(ATemp) - i))
        End If
    End If
    Return
PutEnd:
    u = 0
    For w = Idx + 1 To 5
        If (Trim(KeyWords(w)) <> "") Then
            u = InStrPS(UCase(Temp), UCase(KeyWords(w)))
            If (u > 0) Then
                Exit For
            End If
        End If
    Next w
    Return
GetSeries:
    MyId = 0
    ASeries = ""
    Set RetInv = New CLInventory
    RetInv.InventoryId = InvId
    If (RetInv.RetrieveCLInventory) Then
        ASeries = Trim(RetInv.Series)
    End If
    Set RetInv = Nothing
    If (ASeries <> "") Then
        Set RetInv = New CLInventory
        RetInv.FieldSelect = "InventoryId"
        RetInv.Criteria = "Series = '" & ASeries & "' "
        If (Trim(strSphere) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And SpherePower = '" & strSphere & "' "
        End If
        If (Trim(strCyl) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And CylinderPower = '" & strCyl & "' "
        End If
        If (Trim(strAxis) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And Axis = '" & strAxis & "' "
        End If
        If (Trim(strRef1) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And AddReading = '" & strRef1 & "' "
        End If
        If (Trim(strRef2) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And BaseCurve = '" & strRef2 & "' "
        End If
        If (Trim(strRef3) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And Diameter = '" & strRef3 & "' "
        End If
        If (Trim(strRef4) <> "") Then
            RetInv.Criteria = Trim(RetInv.Criteria) + " And PeriphCurve = '" & strRef4 & "' "
        End If
        If (RetInv.FindCLInventorySingleField) Then
            MyId = Val(Trim(RetInv.SingleFieldValue))
        End If
        Set RetInv = Nothing
    End If
    Return
End Function

Private Function UseFavorite(AEye As String) As Boolean
UseFavorite = False
frmEventMsgs.Header = "Inventory does not match " + AEye + " prescription. Use the selection ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    UseFavorite = True
End If
End Function

Private Function GetInventoryItem(AId As Long, ADisplay As String) As Boolean
Dim RetInv As CLInventory
ADisplay = ""
GetInventoryItem = False
If (AId > 0) Then
    Set RetInv = New CLInventory
    RetInv.InventoryId = AId
    If (RetInv.RetrieveCLInventory) Then
        GetInventoryItem = True
        ADisplay = Trim(RetInv.Series) + " " + Trim(RetInv.Material) + " " + Trim(RetInv.Disposable) + " " + Trim(RetInv.Tint) + " " + Trim(RetInv.Type_)
    End If
    Set RetInv = Nothing
End If
End Function

Private Sub ControlHistory(IType As Boolean)
    lblTrial1.Enabled = IType
    lblTrial2.Enabled = IType
    lblTrial3.Enabled = IType
    lblTrial4.Enabled = IType
    lblTrial5.Enabled = IType
    lblTrial6.Enabled = IType
    lblTrial7.Enabled = IType
    lblTrial8.Enabled = IType
    lblRx1.Enabled = IType
    lblRx2.Enabled = IType
    lblRx3.Enabled = IType
    lblRx4.Enabled = IType
    lblRx5.Enabled = IType
    lblRx6.Enabled = IType
    lblRx7.Enabled = IType
    lblRx8.Enabled = IType
    txtNote1.Enabled = IType
    txtNote2.Enabled = IType
    txtNote3.Enabled = IType
    txtNote4.Enabled = IType
    txtNote5.Enabled = IType
    txtNote6.Enabled = IType
    txtNote7.Enabled = IType
    txtNote8.Enabled = IType
    cmdCurRx1.Enabled = IType
    cmdCurRx2.Enabled = IType
    cmdCurRx3.Enabled = IType
    cmdCurRx4.Enabled = IType
    cmdCurRx5.Enabled = IType
    cmdCurRx6.Enabled = IType
    cmdCurRx7.Enabled = IType
    cmdCurRx8.Enabled = IType
    cmdCurRx9.Enabled = IType
    cmdCLType1.Enabled = IType
    cmdCLType2.Enabled = IType
    cmdCLType3.Enabled = IType
    cmdCLType4.Enabled = IType
    cmdCLType5.Enabled = IType
    cmdCLType6.Enabled = IType
    cmdCLType7.Enabled = IType
    cmdCLType8.Enabled = IType
    cmdNewRx.Enabled = IType
End Sub

Private Sub DisplayHistory()
Dim AId As Long
Dim u As Integer
Dim i As Integer
Dim i1 As Integer, i2 As Integer
Dim MyTemp As String
Dim DisplayText As String
Dim Temp As String, ADate As String
Dim ODText As String, OSText As String
Dim ODTrialText As String, OSTrialText As String
Dim RetCln As PatientClinical
Dim RetAppt As SchedulerAppointment
lstHx.AddItem "Close History"
lstHx.Visible = True
MyTemp = "DISPENSE CL"
If (WearType = "PC") Then
    MyTemp = "DISPENSE SPECTACLE"
End If
Set RetCln = New PatientClinical
RetCln.PatientId = PatientId
RetCln.AppointmentId = 0
RetCln.ClinicalType = "A"
RetCln.Findings = ""
RetCln.Symptom = ""
RetCln.ImageDescriptor = ""
RetCln.ImageInstructions = ""
If (RetCln.FindPatientClinical > 0) Then
    i = 1
    While (RetCln.SelectPatientClinical(i))
        If (RetCln.ClinicalStatus = "A") And (InStrPS(UCase(RetCln.Findings), MyTemp) > 0) Then
            Set RetAppt = New SchedulerAppointment
            RetAppt.AppointmentId = RetCln.AppointmentId
            If (RetAppt.RetrieveSchedulerAppointment) Then
                ADate = RetAppt.AppointmentDate
                ADate = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
            End If
            Set RetAppt = Nothing
            
            ODText = ""
            OSText = ""
            ODTrialText = ""
            OSTrialText = ""
            
            DisplayText = Space(95)
            Mid(DisplayText, 1, 10) = ADate
            i1 = InStrPS(UCase(RetCln.Findings), "-2/")
            i2 = InStrPS(UCase(RetCln.Findings), "-3/")
            If (i1 > 0) And (i2 > 0) Then
                ODText = "OD:" + Mid(RetCln.Findings, i1 + 3, (i2 - 1) - (i1 + 2))
                Mid(DisplayText, 12, Len(ODText)) = ODText
            End If
            lstHx.AddItem DisplayText
            DisplayText = Space(95)
            i1 = InStrPS(UCase(RetCln.Findings), "-3/")
            i2 = InStrPS(UCase(RetCln.Findings), "-4/")
            If (i1 > 0) And (i2 > 0) Then
                OSText = "OS:" + Mid(RetCln.Findings, i1 + 3, (i2 - 1) - (i1 + 2))
                Mid(DisplayText, 12, Len(OSText)) = OSText
            End If
            lstHx.AddItem DisplayText
' Prescription
            i1 = InStrPS(UCase(RetCln.Findings), "-5/")
            i2 = InStrPS(UCase(RetCln.Findings), "-6/")
            If (i1 > 0) And (i2 > 0) Then
                DisplayText = Space(95)
                ODText = Mid(RetCln.Findings, i1 + 3, (i2 - 1) - (i1 + 2))
                AId = Val(Trim(ODText))
                If (AId > 0) Then
                    Call GetInventoryItem(AId, ODText)
                    Mid(DisplayText, 12, Len(ODText) + 3) = "OD:" + ODText
                    lstHx.AddItem DisplayText
                End If
            End If
            i1 = InStrPS(UCase(RetCln.Findings), "-6/")
            i2 = InStrPS(UCase(RetCln.Findings), "-7/")
            If (i1 > 0) And (i2 > 0) Then
                DisplayText = Space(95)
                OSText = Mid(RetCln.Findings, i1 + 3, (i2 - 1) - (i1 + 2))
                AId = Val(Trim(OSText))
                If (AId > 0) Then
                    Call GetInventoryItem(AId, OSText)
                    Mid(DisplayText, 12, Len(OSText) + 3) = "OS:" + OSText
                    lstHx.AddItem DisplayText
                End If
            End If
            
            If (Trim(RetCln.ImageDescriptor) <> "") Then
                ODText = ""
                OSText = ""
                DisplayText = Space(95)
                If (Mid(RetCln.ImageDescriptor, 1, 1) = "*") Then
                    Temp = Mid(Trim(RetCln.ImageDescriptor), 2, Len(Trim(RetCln.ImageDescriptor)) - 1)
                    u = InStrPS(Temp, "-&-")
                    If (u > 0) Then
                        ODText = Left(Temp, u - 1)
                        OSText = Mid(Temp, u + 3, Len(Temp) - (u + 2))
                    Else
                        ODText = Temp
                    End If
                    Mid(DisplayText, 1, 5) = "Trial"
                    If (Trim(ODText) <> "") Then
                        Mid(DisplayText, 12, Len(ODText)) = ODText
                    End If
                    lstHx.AddItem DisplayText
                ElseIf (Mid(RetCln.ImageDescriptor, 1, 1) = "~") Then
                    Temp = Mid(Trim(RetCln.ImageDescriptor), 2, Len(Trim(RetCln.ImageDescriptor)) - 1)
                    u = InStrPS(Temp, "-&-")
                    If (u > 0) Then
                        ODText = Left(Temp, u - 1)
                        OSText = Mid(Temp, u + 3, Len(Temp) - (u + 2))
                    Else
                        ODText = Temp
                    End If
                    Mid(DisplayText, 1, 11) = "OrderTrial"
                    If (Trim(ODText) <> "") Then
                        Mid(DisplayText, 12, Len(ODText)) = ODText
                    End If
                    lstHx.AddItem DisplayText
                Else
                    Temp = Trim(RetCln.ImageDescriptor)
                    u = InStrPS(Temp, "-&-")
                    If (u > 0) Then
                        ODText = Left(Temp, u - 1)
                        OSText = Mid(Temp, u + 3, Len(Temp) - (u + 2))
                    Else
                        ODText = Temp
                    End If
                    If (Trim(ODText) <> "") Then
                        Mid(DisplayText, 12, Len(ODText)) = ODText
                        lstHx.AddItem DisplayText
                    End If
                End If
                If (Trim(OSText) <> "") Then
                    DisplayText = Space(95)
                    Mid(DisplayText, 12, Len(OSText)) = OSText
                    lstHx.AddItem DisplayText
                End If
            End If
            DisplayText = Space(95)
            lstHx.AddItem DisplayText
        End If
        i = i + 1
    Wend
End If
Set RetCln = Nothing
End Sub

Private Function OrderTrial() As Boolean
OrderTrial = False
frmEventMsgs.Header = "Order Trial ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "No"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    OrderTrial = True
End If
End Function

Private Function IsNoteBig(AText As String) As Boolean
Dim i As Integer
Dim j As Integer
IsNoteBig = False
j = InStrPS(AText, vbCrLf)
If (j > 0) Then
    i = j + 2
    j = InStrPS(i, AText, vbCrLf)
    If (j > 0) Then
        IsNoteBig = True
    End If
End If
If Not (IsNoteBig) Then
    If (Len(AText) > 154) Then
        IsNoteBig = True
    End If
End If
End Function
Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
End Function
