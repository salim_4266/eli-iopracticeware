VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{4BD5A3A1-7FFE-11D4-A13A-004005FA6275}#1.0#0"; "ImagXpr6.dll"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmScan 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9255
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12270
   ForeColor       =   &H00785211&
   LinkTopic       =   "Form1"
   Picture         =   "Scan.frx":0000
   ScaleHeight     =   9255
   ScaleWidth      =   12270
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdRRight 
      Height          =   720
      Left            =   8040
      TabIndex        =   23
      Top             =   0
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1270
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7885329
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   0
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRLeft 
      Height          =   705
      Left            =   6480
      TabIndex        =   22
      Top             =   0
      Visible         =   0   'False
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1244
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   7885329
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   0
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":38FB
   End
   Begin VB.TextBox txtScanRef 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      MaxLength       =   32
      TabIndex        =   7
      Top             =   7320
      Visible         =   0   'False
      Width           =   5775
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdImageApp 
      Height          =   975
      Left            =   5160
      TabIndex        =   30
      Top             =   7920
      Visible         =   0   'False
      Width           =   2355
      _Version        =   131072
      _ExtentX        =   4154
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":3B2D
   End
   Begin MSComDlg.CommonDialog msCommonDlg 
      Left            =   2160
      Top             =   8520
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.ListBox lstSelect 
      BackColor       =   &H00F7F5F5&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   5460
      ItemData        =   "Scan.frx":3D65
      Left            =   1320
      List            =   "Scan.frx":3D6C
      TabIndex        =   10
      Top             =   1200
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.ListBox lstTypes 
      BackColor       =   &H00785211&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   5460
      ItemData        =   "Scan.frx":3D7B
      Left            =   1320
      List            =   "Scan.frx":3D82
      TabIndex        =   0
      Top             =   1200
      Visible         =   0   'False
      Width           =   5055
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNextPix 
      Height          =   735
      Left            =   10680
      TabIndex        =   27
      Top             =   5520
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":3D90
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBrowse 
      Height          =   975
      Left            =   6600
      TabIndex        =   9
      Top             =   1320
      Visible         =   0   'False
      Width           =   2355
      _Version        =   131072
      _ExtentX        =   4154
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   16777215
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":3FC7
   End
   Begin VB.ListBox lstFilter 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2460
      ItemData        =   "Scan.frx":41FC
      Left            =   10200
      List            =   "Scan.frx":4206
      TabIndex        =   25
      Top             =   720
      Width           =   1455
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRename 
      Height          =   735
      Left            =   10680
      TabIndex        =   15
      Top             =   4080
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":421D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   975
      Left            =   10680
      TabIndex        =   13
      Top             =   6960
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":4452
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10680
      TabIndex        =   2
      Top             =   7920
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":4686
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAttach 
      Height          =   975
      Left            =   120
      TabIndex        =   3
      Top             =   7920
      Width           =   2355
      _Version        =   131072
      _ExtentX        =   4154
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":48B9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCreate 
      Height          =   975
      Left            =   2640
      TabIndex        =   4
      Top             =   7920
      Width           =   2355
      _Version        =   131072
      _ExtentX        =   4154
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":4AF9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCancel 
      Height          =   975
      Left            =   7680
      TabIndex        =   5
      Top             =   7920
      Width           =   2355
      _Version        =   131072
      _ExtentX        =   4154
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":4D37
   End
   Begin VB.ListBox lstScan 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6360
      ItemData        =   "Scan.frx":4F6C
      Left            =   240
      List            =   "Scan.frx":4F6E
      TabIndex        =   6
      Top             =   720
      Width           =   9975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrint 
      Height          =   975
      Left            =   10680
      TabIndex        =   11
      Top             =   240
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":4F70
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDelete 
      Height          =   735
      Left            =   10680
      TabIndex        =   12
      Top             =   4800
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":51A4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNew 
      Height          =   735
      Left            =   10680
      TabIndex        =   14
      Top             =   3360
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":53D9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdZoomOut 
      Height          =   735
      Left            =   10680
      TabIndex        =   18
      Top             =   1920
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":560F
   End
   Begin IMAGXPR6LibCtl.ImagXpress FxImage1 
      Height          =   7815
      Left            =   360
      TabIndex        =   19
      Top             =   720
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   13785
      ErrStr          =   "MDYC0090GEP-0B3060SXEP"
      ErrCode         =   1426482102
      ErrInfo         =   2129958274
      Persistence     =   -1  'True
      _cx             =   1
      _cy             =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   -2147483633
      ScrollBarLargeChangeH=   10
      ScrollBarSmallChangeH=   1
      OLEDropMode     =   0
      ScrollBarLargeChangeV=   10
      ScrollBarSmallChangeV=   1
      DisplayProgressive=   -1  'True
      SaveTIFByteOrder=   0
      LoadRotated     =   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRotate 
      Height          =   735
      Left            =   10680
      TabIndex        =   21
      Top             =   2640
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":5846
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   9840
      TabIndex        =   26
      Top             =   240
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   0
      BackColor       =   16777215
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      ForeColorSelected=   0
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdZoomIn 
      Height          =   735
      Left            =   10680
      TabIndex        =   17
      Top             =   1200
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":5A7B
   End
   Begin VB.ListBox lstSort 
      Height          =   450
      Left            =   10800
      Sorted          =   -1  'True
      TabIndex        =   20
      Top             =   0
      Visible         =   0   'False
      Width           =   975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrevPix 
      Height          =   735
      Left            =   10680
      TabIndex        =   28
      Top             =   6240
      Visible         =   0   'False
      Width           =   1275
      _Version        =   131072
      _ExtentX        =   2249
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Scan.frx":5CB1
   End
   Begin VB.ListBox lstFilterBack 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2460
      ItemData        =   "Scan.frx":5EE8
      Left            =   10200
      List            =   "Scan.frx":5EF2
      TabIndex        =   29
      Top             =   720
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblScanRef 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Note"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   4170
      TabIndex        =   8
      Top             =   8520
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Label lblRMsg 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   24
      Top             =   360
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label lblPrint 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Printing"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   5010
      TabIndex        =   16
      Top             =   120
      Visible         =   0   'False
      Width           =   840
   End
   Begin VB.Label lblPatName 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   660
   End
End
Attribute VB_Name = "frmScan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Whom As String
Public PatientId As Long
Public AppointmentId As Long
Public SystemReference As String
Public ReplacementFile As String

Private ReloadListOn As Boolean
Private NumPages As Long
Private CurrentPage As Long
Private ZoomOn As Boolean
Private RotateOn As Boolean
Private RotateRate As Single
Private CurrentMagnification As Single
Private SelectedFile As String
Private ScanRenameOn As Boolean
Private OrgLabel As String
Private TodaysDate As String
Private DisplayDate As String
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private ApptDate As String
Private TransId As Long
Private APatId As Long
Private TransRem As String
Private TransRef As String
Private TransDate As String
Private TransType As String
Private ApptType As String

Private ApplClnId As Long
Private ApplApptId As Long
Private ApplDraw As String
Private ApplSymptom As String
Private ApplApptDate As String

Private ApplListTotal As Long
Private ApplListTbl As ADODB.Recordset

Private LegacyScan As Boolean
Private sDocumentCommand As String

Private Sub cmdAttach_Click()
cmdAttach.BackColor = ButtonSelectionColor
cmdAttach.ForeColor = TextSelectionColor
Call LoadAttachableList
End Sub

Private Sub cmdBrowse_Click()
    Dim strTemp As String
    On Error GoTo lcmdBrowse_Click_Error

    strTemp = ""
    strTemp = OpenSingleFile(msCommonDlg, ScanDirectory)
    If (strTemp <> "") Then
        SelectedFile = strTemp
        Call LoadScanType
    Else
        Call cmdCancel_Click
    End If

    Exit Sub

lcmdBrowse_Click_Error:

    LogError "frmScan", "cmdBrowse_Click", Err, Err.Description, strTemp

End Sub

Private Sub cmdCreate_Click()
SelectedFile = ""
cmdCreate.BackColor = ButtonSelectionColor
cmdCreate.ForeColor = TextSelectionColor
Call LoadScanType
End Sub

Private Sub cmdDone_Click()
If (Whom = "D") Then
    ReplacementFile = SelectedFile
End If
RotateOn = False
ZoomOn = False
lblRMsg.Visible = False
FXImage1.Visible = False
FXImage1.Tag = ""
Set FXImage1.picture = Nothing
DoEvents
cmdCancel.Visible = True
cmdAttach.Visible = True
cmdCreate.Visible = True
lstTypes.Visible = False
lstSelect.Visible = False
cmdBrowse.Visible = False
lblScanRef.Visible = False
txtScanRef.Visible = False
txtScanRef.Text = ""
Set FXImage1.picture = Nothing
FXImage1.Visible = False
FXImage1.Tag = ""
SSDateCombo1.Visible = True
lstScan.Visible = True
lstFilter.Visible = True
lstScan.ListIndex = -1
lstTypes.ListIndex = -1
lstSelect.ListIndex = -1
Set ApplListTbl = Nothing
Unload frmScan
End Sub

Private Sub cmdCancel_Click()
RotateOn = False
ZoomOn = False
lstTypes.Visible = False
lstSelect.Visible = False
cmdBrowse.Visible = False
lblScanRef.Visible = False
txtScanRef.Visible = False
txtScanRef.Text = ""
Set FXImage1.picture = Nothing
FXImage1.Visible = False
FXImage1.Tag = ""
SSDateCombo1.Visible = True
lstScan.Visible = True
lstFilter.Visible = True
lstScan.ListIndex = -1
lstTypes.ListIndex = -1
lstSelect.ListIndex = -1
If (ReloadListOn) Then
    Call LoadScan
End If
End Sub

Private Sub cmdExit_Click()
RotateOn = False
ZoomOn = False
lblRMsg.Visible = False
FXImage1.Visible = False
FXImage1.Tag = ""
Set FXImage1.picture = Nothing
DoEvents
cmdCancel.Visible = True
cmdAttach.Visible = True
cmdCreate.Visible = True
lstTypes.Visible = False
lstSelect.Visible = False
cmdBrowse.Visible = False
lblScanRef.Visible = False
txtScanRef.Visible = False
txtScanRef.Text = ""
SSDateCombo1.Visible = True
lstScan.Visible = True
lstFilter.Visible = True
lstScan.ListIndex = -1
lstTypes.ListIndex = -1
lstSelect.ListIndex = -1
cmdRename.Visible = False
cmdPrint.Visible = False
cmdExit.Visible = False
cmdNew.Visible = False
cmdRotate.Visible = False
cmdRLeft.Visible = False
cmdRRight.Visible = False
cmdDelete.Visible = False
cmdZoomIn.Visible = False
cmdZoomOut.Visible = False
cmdNextPix.Visible = False
cmdPrevPix.Visible = False
If (ReloadListOn) Then
    Call LoadScan
End If
End Sub

Private Sub cmdDelete_Click()
    Dim FileName As String
    On Error GoTo lcmdDelete_Click_Error

    FileName = ""
    frmEventMsgs.Header = "Are you sure ?"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = "No"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    DoEvents
    DoEvents
    If (frmEventMsgs.Result = 2) Then
        FXImage1.Visible = False
                FXImage1.FileName = ""
        FXImage1.Tag = ""
        Call GetScanFileName(lstScan.ListIndex, FileName)
        If Not (FM.IsFileThere(FileName)) Then
            FileName = Trim(Mid(lstScan.List(lstScan.ListIndex), 65, Len(lstScan.List(lstScan.ListIndex)) - 64))
            FileName = MyScanDirectory & CStr(PatientId) & "\" & Trim(FileName)
            FM.Kill FileName
            ReloadListOn = True
        End If
        cmdCancel.Visible = True
        cmdAttach.Visible = True
        cmdCreate.Visible = True
        Call cmdCancel_Click
    End If

    Exit Sub

lcmdDelete_Click_Error:

    LogError "frmScan", "cmdDelete_Click", Err, Err.Description, FileName
End Sub

Private Sub cmdImageApp_Click()
Dim oPatient As New Patient
    On Error GoTo lcmdImageApp_Click_Error

    oPatient.PatientId = PatientId
    If oPatient.RetrievePatient Then
        If GetImageAppName = "iViews" Then
            ReplaceCharacters sDocumentCommand, "*LastName", oPatient.LastName
            ReplaceCharacters sDocumentCommand, "*FirstName", oPatient.FirstName
            If Len(oPatient.BirthDate) = 8 Then
                ReplaceCharacters sDocumentCommand, "*DOB", Mid$(oPatient.BirthDate, 5, 4) & Mid$(oPatient.BirthDate, 1, 4)
            Else
                ReplaceCharacters sDocumentCommand, "*DOB", ""
            End If
            Shell sDocumentCommand, vbMaximizedFocus
        Else
            Dim ViewImageApplicationArguments As Variant
            Dim DocumentComWrapper As New comWrapper
            DocumentComWrapper.RethrowExceptions = False
                    
            DocumentComWrapper.Create "System.Collections.Hashtable", emptyArgs
            Set ViewImageApplicationArguments = DocumentComWrapper.Instance
            Dim AddItemArgs() As Variant
            AddItemArgs = Array("patientId", str(PatientId))
            Call DocumentComWrapper.InvokeMethod("Add", AddItemArgs)
            
            Dim arguments() As Variant
            arguments = Array(GetImageAppName, ViewImageApplicationArguments)
            DocumentComWrapper.Create "IO.Practiceware.Presentation.Views.Documents.DocumentViewManager", emptyArgs
            DocumentComWrapper.InvokeMethod "ViewImageApplication", arguments
        End If
    End If

    Exit Sub

lcmdImageApp_Click_Error:
    If Err.Number = 53 Then
        MsgBox "File not found: " & sDocumentCommand
    End If
    LogError "frmScan", "cmdImageApp_Click", Err, Err.Description, , PatientId
End Sub

Private Sub cmdNew_Click()
    Dim FileName As String
    On Error GoTo lcmdNew_Click_Error

    FileName = ""
    FileName = FXImage1.FileName
    FXImage1.Visible = False
    FXImage1.FileName = ""
    Call ScanInFile(FileName)
    If (InStrPS(UCase(FileName), ".BMP") <> 0) Then
        FXImage1.FileName = FileName
    ElseIf (InStrPS(UCase(FileName), ".TIF") <> 0) Then
        FXImage1.FileName = FileName
    ElseIf (InStrPS(UCase(FileName), ".PDF") <> 0) Then
        lstScan.Visible = True
        lstFilter.Visible = True
        SSDateCombo1.Visible = True
    '    Call TriggerPDF(FileName)
        Call cmdExit_Click
        Exit Sub
    Else
        FXImage1.picture = FM.LoadPicture(FileName)
    End If
    FXImage1.Visible = True

    Exit Sub

lcmdNew_Click_Error:

    LogError "frmScan", "cmdNew_Click", Err, Err.Description, FileName
End Sub

Private Sub cmdNextPix_Click()
    Dim FileName As String
    On Error GoTo lcmdNextPix_Click_Error

    FileName = ""
    If (CurrentPage + 1 > NumPages) Then
        CurrentPage = 1
    Else
        CurrentPage = CurrentPage + 1
    End If
    FileName = FXImage1.FileName
    FXImage1.PageNbr = CurrentPage
    FXImage1.FileName = FileName
    lblRMsg.Caption = "Page" + str(CurrentPage) + " of" + str(NumPages)

    Exit Sub

lcmdNextPix_Click_Error:

    LogError "frmScan", "cmdNextPix_Click", Err, Err.Description, FileName
End Sub

Private Sub cmdPrevPix_Click()
    Dim FileName As String
    On Error GoTo lcmdPrevPix_Click_Error

    FileName = ""
    If (CurrentPage - 1 < 1) Then
        CurrentPage = 1
    Else
        CurrentPage = CurrentPage - 1
    End If
    FileName = FXImage1.FileName
    FXImage1.PageNbr = CurrentPage
    FXImage1.FileName = FileName
    lblRMsg.Caption = "Page" + str(CurrentPage) + " of" + str(NumPages)

    Exit Sub

lcmdPrevPix_Click_Error:
    LogError "frmScan", "cmdPrevPix_Click", Err, Err.Description, FileName
End Sub

Private Sub cmdPrint_Click()
    Dim AFile As String

    On Error GoTo lcmdPrint_Click_Error





    AFile = LocalPinPointDirectory & "ScreenShot.jpg"
    If (FM.IsFileThere(AFile)) Then
        FM.Kill AFile
    End If
    Call SaveScreenToFile(AFile)
    Call InsertLog("FrmScan.frm", "Printed Patient  " & CStr(PatientId) & " Document", LoginCatg.PrnDocument, "", "", 0, AppointmentId)

    Call PrintDocument("Image", AFile, "")
    '    Printer.ScaleMode = vbTwips
    '    FxImage1.PrinterTop = 100
    '    FxImage1.PrinterLeft = 100
    '    FxImage1.PrinterWidth = 8 * 1440
    '    If (FxImage1.ImagHeight > FxImage1.ImagWidth) Then
    '        FxImage1.PrinterHeight = 10 * 1440
    '    Else
    '        If ((Int(10 * (FxImage1.ImagHeight / FxImage1.ImagWidth)) - 1) > 0) Then
    '            FxImage1.PrinterHeight = (Int(10 * (FxImage1.ImagHeight / FxImage1.ImagWidth)) - 1) * 1440
    '        Else
    '            FxImage1.PrinterHeight = 10 * 1440
    '        End If
    '    End If
    '    Printer.Print ""
    '    FxImage1.PrinterhDC = Printer.hDC
    '    Printer.EndDoc

    


    DoEvents
    Exit Sub

lcmdPrint_Click_Error:

    LogError "frmScan", "cmdPrint_Click", Err, Err.Description, , PatientId
End Sub

Private Sub cmdRename_Click()
ScanRenameOn = True
lblScanRef.Caption = "Rename Original Scan File - Press Enter to Rename"
lblScanRef.Visible = True
txtScanRef.Text = ""
txtScanRef.Visible = True
txtScanRef.SetFocus
End Sub

Private Sub cmdRLeft_Click()
FXImage1.Rotate -90
FXImage1.ZoomToFit ZOOMFIT_BEST
RotateOn = True
End Sub

Private Sub cmdRotate_Click()
If (cmdRLeft.Visible) Then
    cmdRLeft.Visible = False
    cmdRRight.Visible = False
Else
    cmdRLeft.Visible = True
    cmdRRight.Visible = True
End If
End Sub

Private Sub cmdRRight_Click()
FXImage1.Rotate 90
FXImage1.ZoomToFit ZOOMFIT_BEST
RotateOn = True
End Sub

Private Sub cmdZoomIn_Click()
CurrentMagnification = CurrentMagnification * 1.1
If (CurrentMagnification > 8) Then
    CurrentMagnification = 1
    FXImage1.AutoSize = ISIZE_CropImage
    FXImage1.ScrollBars = SB_Both
End If
FXImage1.Zoom (CurrentMagnification)
ZoomOn = True
DoEvents
End Sub

Private Sub cmdZoomOut_Click()
CurrentMagnification = CurrentMagnification / 1.1
If (CurrentMagnification <= 0.125) Then
    CurrentMagnification = 1
    FXImage1.AutoSize = ISIZE_CropImage
    FXImage1.ScrollBars = SB_Both
End If
Call FXImage1.Zoom(CurrentMagnification)
ZoomOn = True
DoEvents
End Sub

Private Sub Form_Load()
LegacyScan = Not (CheckConfigCollection("LEGACYSCAN") = "F")
    If CheckConfigCollection("ImageApp") = "T" Then
        cmdImageApp.Visible = True
        cmdImageApp.Text = GetImageAppName
    End If
End Sub

Private Sub lstFilter_Click()
If (lstFilter.ListIndex >= 0) Then
    Call LoadScanList(PatientId)
End If
End Sub

Private Sub lstScan_Click()
Dim i As Integer
Dim ClnId As Long
Dim PatId As String
Dim k As Integer
Dim FileName As String
    On Error GoTo llstScan_Click_Error

If (lstScan.ListIndex >= 0) Then
    If (Whom = "D") Then
        SelectedFile = ""
    End If
    If (Mid(lstScan.List(lstScan.ListIndex), 60, 1) = "I") Then
        i = InStrPS(60, lstScan.List(lstScan.ListIndex), "*")
        If (i > 0) Then
            lblRMsg.Visible = False
            lstScan.Visible = False
            lstFilter.Visible = False
            SSDateCombo1.Visible = False
            ClnId = Val(Trim(Mid(lstScan.List(lstScan.ListIndex), 66, i - 65)))
            Call InsertLog("FrmScan.frm", "Viewed Document " & CStr(ClnId) & "", LoginCatg.ViewDocument, "", "", 0, 0)
            If (frmViewDraw.LoadDraw(ClnId, "")) Then
                frmViewDraw.Show 1
                If (Whom = "D") Then
                    SelectedFile = Mid(lstScan.List(lstScan.ListIndex), i + 1, Len(lstScan.List(lstScan.ListIndex)) - i)
                End If
            End If
            lstScan.Visible = True
            lstFilter.Visible = True
            SSDateCombo1.Visible = True
        Else
            lblRMsg.Visible = False
            lstScan.Visible = False
            lstFilter.Visible = False
            SSDateCombo1.Visible = False
            FXImage1.FileName = ""
            Set FXImage1.picture = Nothing
            DoEvents
            FileName = Trim(Mid(lstScan.List(lstScan.ListIndex), 65, Len(lstScan.List(lstScan.ListIndex)) - 64))
            FileName = MyScanDirectory + Trim(FileName)
            If Not (FM.IsFileThere(FileName)) Then
                FileName = Trim(Mid(lstScan.List(lstScan.ListIndex), 65, Len(lstScan.List(lstScan.ListIndex)) - 64))
                FileName = MyScanDirectory & CStr(PatientId) & "\" & Trim(FileName)
                If Not (FM.IsFileThere(FileName)) Then Exit Sub
            End If
                
            If (InStrPS(UCase(FileName), ".PDF") > 0) Then
                frmEventMsgs.Header = "Action ?"
                frmEventMsgs.AcceptText = "View"
                frmEventMsgs.RejectText = ""
                frmEventMsgs.CancelText = "Cancel"
                frmEventMsgs.Other0Text = "Rename"
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = "Delete"
                frmEventMsgs.Show 1
                If (frmEventMsgs.Result = 1) Then
                    lstScan.Visible = True
                    lstFilter.Visible = True
                    SSDateCombo1.Visible = True
                    Call TriggerPDF(FileName)
                    Call InsertLog("FrmScan.frm", "Viewed Document " & CStr(ClnId) & "", LoginCatg.ViewDocument, "", "", 0, 0)
                    Exit Sub
                ElseIf (frmEventMsgs.Result = 3) Then
                    Call cmdRename_Click
                    Exit Sub
                ElseIf (frmEventMsgs.Result = 8) Then
                    frmEventMsgs.Header = "Are you sure ?"
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = "No"
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    If (frmEventMsgs.Result = 2) Then
                        If (KillPDFFile(FileName)) Then
                            lstScan.RemoveItem lstScan.ListIndex
                        End If
                    End If
                    Call cmdCancel_Click
                    Exit Sub
                Else
                    Call cmdCancel_Click
                    Exit Sub
                End If
            End If
            If (Whom = "D") Then
                SelectedFile = FileName
            End If
            If (InStrPS(UCase(FileName), ".DOC") <> 0) Or (InStrPS(UCase(FileName), ".RTF") <> 0) Then
                Call KillDocumentProcessing(FileName, True, False)
                Call TriggerWord(FileName)
                lblRMsg.Visible = True
                lstScan.Visible = True
                lstFilter.Visible = True
                SSDateCombo1.Visible = True
                Exit Sub
            ElseIf (InStrPS(UCase(FileName), ".BMP") <> 0) Or (InStrPS(UCase(FileName), ".TIF") <> 0) Then
                FXImage1.FileName = FileName
            Else
                FXImage1.picture = FM.LoadPicture(FileName)
            End If
            FXImage1.Tag = Trim(FileName)
            FXImage1.AutoSize = ISIZE_CropImage
            FXImage1.ScrollBars = SB_Both
            NumPages = FXImage1.NumPages(FileName)
            CurrentPage = 1
            FXImage1.PageNbr = CurrentPage
            FXImage1.ZoomToFit ZOOMFIT_BEST
            FXImage1.ZOrder 0
            FXImage1.Visible = True
            FXImage1.Refresh
            DoEvents
            cmdPrint.Visible = True
            cmdExit.Visible = True
            cmdRename.Visible = True
            cmdNew.Visible = True
            cmdRotate.Visible = True
            cmdRLeft.Visible = False
            cmdRRight.Visible = False
            cmdDelete.Visible = True
            cmdZoomIn.Visible = True
            cmdZoomOut.Visible = True
            If (NumPages > 1) Then
                cmdNextPix.Visible = True
                cmdPrevPix.Visible = True
                lblRMsg.Caption = "Page" + str(CurrentPage) + " of" + str(NumPages)
                lblRMsg.Visible = True
            Else
                lblRMsg.Visible = False
                cmdNextPix.Visible = False
                cmdPrevPix.Visible = False
            End If
            cmdAttach.Visible = False
            cmdCreate.Visible = False
            cmdCancel.Visible = False
            DoEvents
            DoEvents
        End If
    ElseIf (Mid(lstScan.List(lstScan.ListIndex), 60, 1) = "D") Or _
           (Mid(lstScan.List(lstScan.ListIndex), 60, 1) = "W") Then
        lblRMsg.Visible = False
        lstScan.Visible = False
        lstFilter.Visible = False
        SSDateCombo1.Visible = False
        FileName = Trim(Mid(lstScan.List(lstScan.ListIndex), 65, Len(lstScan.List(lstScan.ListIndex)) - 64))
        If (Mid(lstScan.List(lstScan.ListIndex), 60, 1) = "D") Then
            FileName = DocumentDirectory + Trim(FileName) + ".doc"
        Else
            FileName = DocumentDirectory + Trim(FileName)
        End If
        If (FM.IsFileThere(FileName)) Then
            Call KillDocumentProcessing(FileName, True, False)
            Call TriggerWord(FileName)
        End If
        lstScan.Visible = True
        lstFilter.Visible = True
        SSDateCombo1.Visible = True
		 
	ElseIf (Mid(lstScan.List(lstScan.ListIndex), 60, 1) = "C") Then
        lblRMsg.Visible = False
        lstScan.Visible = False
        lstFilter.Visible = False
        SSDateCombo1.Visible = False
        FileName = Trim(Mid(lstScan.List(lstScan.ListIndex), 65, Len(lstScan.List(lstScan.ListIndex)) - 64))
        k = InStrPS(FileName, "-")
        PatId = Mid(FileName, 1, k - 1)
        FileName = PinPointDirectory + Trim(Str(PatId)) + "\Consent\" + Trim(FileName) + ".pdf"
        
        If (FM.IsFileThere(FileName)) Then
            Call KillDocumentProcessing(FileName, True, False)
            Call TriggerPDF(FileName)
        End If
        lstScan.Visible = True
        lstFilter.Visible = True
        SSDateCombo1.Visible = True
    End If
End If

    Exit Sub

llstScan_Click_Error:

    LogError "frmScan", "lstScan_Click", Err, Err.Description
    RotateOn = False
    ZoomOn = False
    lstTypes.Visible = False
    lstSelect.Visible = False
    cmdBrowse.Visible = False
    lblScanRef.Visible = False
    txtScanRef.Visible = False
    txtScanRef.Text = ""
    Set FXImage1.picture = Nothing
    FXImage1.Visible = False
    FXImage1.Tag = ""
    SSDateCombo1.Visible = True
    lstScan.Visible = True
    lstFilter.Visible = True
    lstScan.ListIndex = -1
    lstTypes.ListIndex = -1
    lstSelect.ListIndex = -1
End Sub

Private Sub lstSelect_Click()
If (lstSelect.ListIndex >= 0) Then
    SelectedFile = Trim(lstSelect.List(lstSelect.ListIndex))
    Call LoadScanType
End If
End Sub

Private Sub lstTypes_Click()
    Dim FileName As String
    On Error GoTo llstTypes_Click_Error

    FileName = ""
    If (lstTypes.ListIndex > 0) Then
        If Not (FormScanFileName(lstTypes.ListIndex, FileName)) Then
            FileName = ""
            frmEventMsgs.Header = "FileName is too long"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (txtScanRef.Visible) Then
                txtScanRef.SetFocus
            Else
                lstTypes.ListIndex = -1
            End If
        End If
        If (Trim(FileName) <> "") Then
            If (Trim(SelectedFile) = "") Then
                If (ScanInFile(FileName)) Then
                    If (FM.IsFileThere(FileName)) Then
                        If LegacyScan Then
                            Call LoadImage(FileName, False)
                        Else
                            lstTypes.Visible = False
                            lstScan.Visible = False
                            lstFilter.Visible = False
                            SSDateCombo1.Visible = False
                            lblScanRef.Visible = False
                            txtScanRef.Visible = False
                            cmdCancel.Visible = False
                            cmdAttach.Visible = True
                            cmdCreate.Visible = True
                            Call cmdCancel_Click
                        End If
                    Else
                        If Not (frmProcessScan.QuitOn) Then
                            frmEventMsgs.Header = "Scan Not Done"
                            frmEventMsgs.AcceptText = ""
                            frmEventMsgs.RejectText = "Ok"
                            frmEventMsgs.CancelText = ""
                            frmEventMsgs.Other0Text = ""
                            frmEventMsgs.Other1Text = ""
                            frmEventMsgs.Other2Text = ""
                            frmEventMsgs.Other3Text = ""
                            frmEventMsgs.Other4Text = ""
                            frmEventMsgs.Show 1
                        End If
                        lstTypes.Visible = False
                        lstScan.Visible = False
                        lstFilter.Visible = False
                        SSDateCombo1.Visible = False
                        lblScanRef.Visible = False
                        txtScanRef.Visible = False
                        cmdCancel.Visible = True
                        cmdAttach.Visible = True
                        cmdCreate.Visible = True
                        Call cmdCancel_Click
                    End If
                Else
                    If Not (frmProcessScan.QuitOn) Then
                        frmEventMsgs.Header = "Scan Not Done"
                        frmEventMsgs.AcceptText = ""
                        frmEventMsgs.RejectText = "Ok"
                        frmEventMsgs.CancelText = ""
                        frmEventMsgs.Other0Text = ""
                        frmEventMsgs.Other1Text = ""
                        frmEventMsgs.Other2Text = ""
                        frmEventMsgs.Other3Text = ""
                        frmEventMsgs.Other4Text = ""
                        frmEventMsgs.Show 1
                    End If
                    lstTypes.Visible = False
                    lstScan.Visible = False
                    lstFilter.Visible = False
                    SSDateCombo1.Visible = False
                    lblScanRef.Visible = False
                    txtScanRef.Visible = False
                    cmdCancel.Visible = True
                    cmdAttach.Visible = True
                    cmdCreate.Visible = True
                    Call cmdCancel_Click
                End If
            Else
                If (InStrPS(SelectedFile, "\") = 0) Then
                    SelectedFile = ScanDirectory + SelectedFile
                End If
                If (FM.IsFileThere(SelectedFile)) Then
                    Call LoadImage(FileName, False, CStr(PatientId))
                End If
            End If
        End If
    ElseIf (lstTypes.ListIndex = 0) Then
        SelectedFile = ""
        lstTypes.Visible = False
        lstScan.ListIndex = -1
        lstScan.Visible = False
        lstFilter.Visible = False
        SSDateCombo1.Visible = False
        lblScanRef.Visible = False
        txtScanRef.Visible = False
        txtScanRef.Text = ""
        cmdCancel.Visible = True
        cmdAttach.Visible = True
        cmdCreate.Visible = True
        Call cmdCancel_Click
    End If

    Exit Sub

llstTypes_Click_Error:

    LogError "frmScan", "lstTypes_Click", Err, Err.Description, FileName, PatientId
End Sub

Private Sub RenameScanFile(OrgFile As String, NewFile As String)
    Dim i As Integer
    Dim NewOrgName As String
    Dim NewFileName As String
    Dim OrgFileDir As String
    Dim FileOperationInProgress As Boolean
    OrgFileDir = FM.GetParentFolder(OrgFile)
    
    On Error GoTo lRenameScanFile_Error

    If (Trim(NewFile) <> "") And (Trim(OrgFile) <> "") Then
        frmEventMsgs.Header = "Are you sure ?"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Yes"
        frmEventMsgs.CancelText = "No"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        DoEvents
        DoEvents
        If (frmEventMsgs.Result = 2) Then
            FXImage1.Visible = False
            FXImage1.FileName = ""
            If (FM.IsFileThere(OrgFile)) Then
                Call StripCharacters(NewFile, " ")
                NewOrgName = FM.GetFileName(OrgFile)
                NewFileName = FM.CombinePath(OrgFileDir, Trim(NewFile))
                i = InStrPS(NewOrgName, "_")
                If (i > 0) Then
                    NewFileName = NewFileName + Mid(NewOrgName, i, Len(NewOrgName) - (i - 1))
                Else
                    NewFileName = NewFileName + Mid(NewOrgName, i, Len(NewOrgName) - i - 1)
                End If
                If (Trim(NewFileName) <> "") And Not (FM.IsFileThere(NewFileName)) Then
                    FXImage1.FileName = ""
                    FileOperationInProgress = True
                    FM.Name OrgFile, NewFileName
                    FileOperationInProgress = False
                    If (InStrPS(UCase(NewFileName), ".BMP") > 0) Then
                        FXImage1.FileName = NewFileName
                        FXImage1.ZoomToFit ZOOMFIT_BEST
                        FXImage1.Visible = True
                    ElseIf (InStrPS(UCase(NewFileName), ".TIF") > 0) Then
                        FXImage1.FileName = NewFileName
                        FXImage1.ZoomToFit ZOOMFIT_BEST
                        FXImage1.Visible = True
                    ElseIf (InStrPS(UCase(NewFileName), ".PDF") = 0) Then
                        FXImage1.picture = FM.LoadPicture(NewFileName)
                        FXImage1.Tag = Trim(NewFileName)
                        FXImage1.ZoomToFit ZOOMFIT_BEST
                        FXImage1.Visible = True
                    ElseIf (InStrPS(UCase(NewFileName), ".DOC") <> 0) Or (InStrPS(UCase(NewFileName), ".RTF") <> 0) Then
                        Call KillDocumentProcessing(NewFileName, True, False)
                        Call TriggerWord(NewFileName)
                        lstTypes.Visible = False
                        lblScanRef.Visible = False
                        txtScanRef.Visible = False
                        Exit Sub
                    End If
                    lblScanRef.Visible = False
                    txtScanRef.Text = ""
                    txtScanRef.Visible = False
                    NewFile = NewFileName
                    OrgFile = NewFileName
                Else
                    FXImage1.FileName = OrgFile
                    FXImage1.Visible = True
                    frmEventMsgs.Header = "Renaming not performed."
                    If (FM.IsFileThere(NewFileName)) Then
                        frmEventMsgs.Header = "File name is in use already."
                    End If
                    frmEventMsgs.AcceptText = ""
                    frmEventMsgs.RejectText = "Ok"
                    frmEventMsgs.CancelText = ""
                    frmEventMsgs.Other0Text = ""
                    frmEventMsgs.Other1Text = ""
                    frmEventMsgs.Other2Text = ""
                    frmEventMsgs.Other3Text = ""
                    frmEventMsgs.Other4Text = ""
                    frmEventMsgs.Show 1
                    lblScanRef.Visible = False
                    txtScanRef.Text = ""
                    txtScanRef.Visible = False
                End If
                ReloadListOn = True
            Else
                FXImage1.FileName = OrgFile
                FXImage1.Visible = True
                frmEventMsgs.Header = "Renaming not performed."
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
                lblScanRef.Visible = False
                txtScanRef.Text = ""
                txtScanRef.Visible = False
            End If
        Else
            lblScanRef.Visible = False
            txtScanRef.Text = ""
            txtScanRef.Visible = False
        End If
    End If

    Exit Sub

lRenameScanFile_Error:
    LogError "frmScan", "RenameScanFile", Err, Err.Description, OrgFile
    If (FileOperationInProgress) Then
        MsgBox "Could not rename selected file. Please, ensure it's not opened in another application. Check event log for more details.", vbExclamation
    End If
End Sub

Private Function LoadScanType() As Boolean
Dim i As Integer
Dim RetCode As PracticeCodes
    On Error GoTo lLoadScanType_Error

LoadScanType = False
lstTypes.Visible = False
lstTypes.Clear
lstTypes.Clear
lstTypes.AddItem "Select reference tag or Exit"
Set RetCode = New PracticeCodes
RetCode.ReferenceType = "ScanType"
If (RetCode.FindCode > 0) Then
    i = 1
    While (RetCode.SelectCode(i))
        lstTypes.AddItem Trim(RetCode.ReferenceCode)
        i = i + 1
    Wend
End If
If (lstTypes.ListCount >= 1) Then
    LoadScanType = True
    lstSelect.Visible = False
    cmdBrowse.Visible = False
    lstTypes.Visible = True
    lblScanRef.Visible = True
    txtScanRef.Visible = True
    txtScanRef.SetFocus
Else
    Call cmdCancel_Click
End If
Set RetCode = Nothing

    Exit Function

lLoadScanType_Error:

    LogError "frmScan", "LoadScanType", Err, Err.Description
End Function

Private Sub txtScanRef_Validate(Cancel As Boolean)
Call txtScanRef_KeyPress(13)
End Sub

Private Sub txtScanRef_KeyPress(KeyAscii As Integer)
'<>3
Dim i As Integer
Dim AName As String
Dim FileName As String
Dim StripChars As String
    On Error GoTo ltxtScanRef_KeyPress_Error

If (KeyAscii = 13) Then
    AName = Trim(txtScanRef.Text)
    StripChars = ".,=`~!@#$%^&*()+}{[]';:\|/?>< "
    For i = 1 To Len(StripChars)
        Call StripCharacters(AName, Mid(StripChars, i, 1))
    Next i
    txtScanRef.Text = AName
    If (ScanRenameOn) And (Trim(AName) <> "") Then
        If (InStrPS(SelectedFile, ".pdf") > 0) Then
            Call RenameScanFile(SelectedFile, AName)
            Call cmdCancel_Click
        ElseIf (Trim(FXImage1.FileName) <> "") Then
            Call RenameScanFile(FXImage1.FileName, AName)
            ScanRenameOn = False
            Call cmdCancel_Click
        ElseIf (Trim(FXImage1.Tag) <> "") Then
            Call RenameScanFile(FXImage1.Tag, AName)
            Call cmdCancel_Click
        Else
            If (lstScan.ListIndex >= 0) Then
                FileName = ScanDirectory + Trim(Mid(lstScan.List(lstScan.ListIndex), 65, Len(lstScan.List(lstScan.ListIndex)) - 64))
                If Not (FM.IsFileThere(FileName)) Then
                    FileName = MyScanDirectory + Trim(Mid(lstScan.List(lstScan.ListIndex), 65, Len(lstScan.List(lstScan.ListIndex)) - 64))
                End If
                If Not (FM.IsFileThere(FileName)) Then
                    FileName = MyScanDirectory + CStr(PatientId) + "\" + Trim(Mid(lstScan.List(lstScan.ListIndex), 65, Len(lstScan.List(lstScan.ListIndex)) - 64))
                End If
                Call RenameScanFile(FileName, AName)
                Call cmdCancel_Click
            End If
        End If
    End If
    ScanRenameOn = False
End If

    Exit Sub

ltxtScanRef_KeyPress_Error:

    LogError "frmScan", "txtScanRef_KeyPress", Err, Err.Description, FileName, PatientId
End Sub

Public Function LoadScan() As Boolean

    Dim RetPat As Patient
    On Error GoTo lLoadScan_Error

    LoadScan = True
    ReloadListOn = False
    RotateOn = False
    ZoomOn = False
    NumPages = 0
    CurrentPage = 1
    If (Whom <> "D") Then
        ReplacementFile = ""
    End If
    CurrentMagnification = 1
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = cmdDone.BackColor
    BaseTextColor = cmdDone.ForeColor
    cmdAttach.BackColor = BaseBackColor
    cmdAttach.ForeColor = BaseTextColor
    cmdCreate.BackColor = BaseBackColor
    cmdCreate.ForeColor = BaseTextColor
    DisplayDate = Trim(SSDateCombo1.DateValue)
    Call FormatTodaysDate(DisplayDate, False)
    RotateRate = Val(CheckConfigCollection("ROTATERATE", "90"))
    ScanRenameOn = False
    If (PatientId > 0) Then
        cmdRename.Visible = False
        cmdPrint.Visible = False
        cmdExit.Visible = False
        cmdNew.Visible = False
        cmdRotate.Visible = False
        cmdRLeft.Visible = False
        cmdRRight.Visible = False
        cmdDelete.Visible = False
        cmdZoomIn.Visible = False
        cmdZoomOut.Visible = False
        cmdNextPix.Visible = False
        cmdPrevPix.Visible = False
        TodaysDate = ""
        Call FormatTodaysDate(TodaysDate, False)
        TodaysDate = Mid(TodaysDate, 7, 4) + Mid(TodaysDate, 1, 2) + Mid(TodaysDate, 4, 2)
        SelectedFile = ""
        Set RetPat = New Patient
        RetPat.PatientId = PatientId
        If (RetPat.RetrievePatient) Then
            lblPatName.Caption = Trim(RetPat.FirstName) + " " + Trim(RetPat.MiddleInitial) + " " + Trim(RetPat.LastName) + " " + Trim(RetPat.NameRef)
            OrgLabel = lblPatName.Caption
            FXImage1.Visible = False
            Call LoadListTypes
            LoadScan = LoadScanList(PatientId)
        End If
        Set RetPat = Nothing
    End If
Exit Function

lLoadScan_Error:

    LogError "frmScan", "LoadScan", Err, Err.Description, str$(PatientId), PatientId
End Function

Private Function LoadScanList(PatId As Long) As Boolean

    Dim k As Integer
    Dim AFilter As String
    On Error GoTo lLoadScanList_Error

    LoadScanList = True
    lblRMsg.Visible = False
    cmdPrint.Visible = False
    cmdExit.Visible = False
    cmdRename.Visible = False
    cmdNew.Visible = False
    cmdRotate.Visible = False
    cmdRLeft.Visible = False
    cmdRRight.Visible = False
    cmdDelete.Visible = False
    cmdZoomIn.Visible = False
    cmdZoomOut.Visible = False
    cmdNextPix.Visible = False
    cmdPrevPix.Visible = False
    lstsort.Clear
    lstScan.Clear
    AFilter = ""
    If (lstFilter.ListIndex > 0) Then
        AFilter = UCase(Trim(lstFilter.List(lstFilter.ListIndex)))
    End If
    Call RetrieveScannedImages(PatId, AFilter, lstsort)
	Call RetrieveConsentFormList(PatId, AFilter, lstSort)
    Call RetrieveDocumentList(PatId, AFilter, lstsort)
    Call AddImagesFromHistory(PatId, AFilter, lstsort)
    If (lstsort.ListCount > 0) Then
        For k = lstsort.ListCount - 1 To 0 Step -1
            If (Mid(lstsort.List(k), 60, 1) = "D") Then
                lstScan.AddItem Mid(lstsort.List(k), 9, Len(lstsort.List(k)) - 8)
            ElseIf (Mid(lstsort.List(k), 60, 1) = "I") Then
                lstScan.AddItem Mid(lstsort.List(k), 9, Len(lstsort.List(k)) - 8)
            Else
                lstScan.AddItem Mid(lstsort.List(k), 9, Len(lstsort.List(k)) - 8)
            End If
        Next k
    Else
        lstScan.Clear
    End If
    Dim j As Integer
    Dim Temp As String
    For k = 0 To lstScan.ListCount - 1
        For j = k + 1 To lstScan.ListCount - 1
             If CDate(Mid(lstScan.List(k), 1, 10)) < CDate(Mid(lstScan.List(j), 1, 10)) Then
                    Temp = lstScan.List(j)
                    lstScan.RemoveItem j
                    lstScan.AddItem lstScan.List(k), j
                    lstScan.RemoveItem k
                    lstScan.AddItem Temp, k
                End If
       Next j
    Next k
    Exit Function

lLoadScanList_Error:

    LogError "frmScan", "LoadScanList", Err, Err.Description, str$(PatId), PatId

End Function

Private Function LoadAttachableList() As Boolean
    Dim MyFile As String
    Dim FileName As String
    On Error GoTo lLoadAttachableList_Error

    MyFile = ""
    LoadAttachableList = True
    lstSelect.Clear
    cmdPrint.Visible = False
    cmdExit.Visible = False
    cmdRename.Visible = False
    cmdNew.Visible = False
    cmdRotate.Visible = False
    cmdRLeft.Visible = False
    cmdRRight.Visible = False
    cmdDelete.Visible = False
    cmdZoomIn.Visible = False
    cmdZoomOut.Visible = False
    cmdNextPix.Visible = False
    cmdPrevPix.Visible = False
    FileName = ScanDirectory + "*.*"
    MyFile = Dir(FileName)
    While (MyFile <> "")
        lstSelect.AddItem MyFile
        MyFile = Dir
    Wend
    lstTypes.Visible = False
    lstSelect.Visible = True
    cmdBrowse.Visible = True
    DoEvents

    Exit Function

lLoadAttachableList_Error:

    LogError "frmScan", "LoadAttachableList", Err, Err.Description, MyFile
End Function

Private Function FormScanFileName(Idx As Integer, TheFile As String) As Boolean
    Dim i As Integer
    Dim Ref As String
    Dim Ext As String
    Dim Appt As String
    Dim ScanRef As String
    Dim LocalDate As String
    Dim ADate As String
    Dim BaseFileName As String
    Dim ScanDirectory As String
    ScanDirectory = MyScanDirectory & PatientId & "\"
    On Error GoTo lFormScanFileName_Error

    If (FM.Dir(ScanDirectory, vbDirectory) = "") Then
        FM.MkDir (ScanDirectory)
    End If

    FormScanFileName = False
    TheFile = ""
    If (Idx > 0) Or (Idx = -1) Then
        i = 0
        Ref = ""
        Appt = ""
        If LegacyScan Then
            Ext = ".jpg"
        Else
            Ext = ".pdf"
        End If
        ScanRef = Trim(txtScanRef.Text)
        ADate = SSDateCombo1.Date
        Call FormatTodaysDate(ADate, False)
        LocalDate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
        If (Trim(SelectedFile) <> "") Then
            Ext = FM.GetFileExtension(SelectedFile)
        End If
        If (AppointmentId > 0) Then
            Appt = Trim(str(AppointmentId))
        End If
        If (Idx = -1) Then
            TheFile = ScanDirectory + Trim(SystemReference) + "-" + Trim(ScanRef) + "_" + Appt + "_" + Trim(str(PatientId)) + "-" + LocalDate + Ext
            BaseFileName = Trim(SystemReference) + "-" + Trim(ScanRef) + "_" + Appt + "_" + Trim(str(PatientId)) + "-" + LocalDate
        Else
            TheFile = ScanDirectory + Trim(lstTypes.List(Idx)) + "-" + Trim(ScanRef) + "_" + Appt + "_" + Trim(str(PatientId)) + "-" + LocalDate + Ext
            BaseFileName = Trim(lstTypes.List(Idx)) + "-" + Trim(ScanRef) + "_" + Appt + "_" + Trim(str(PatientId)) + "-" + LocalDate
        End If
        Call StripCharacters(TheFile, " ")
        Call StripCharacters(BaseFileName, " ")
        If (Len(BaseFileName) <= 128) Then
            FormScanFileName = True
            Do Until Not (FM.IsFileThere(TheFile))
                i = i + 1
                Ref = Trim(str(i))
                If (Idx = -1) Then
                    TheFile = ScanDirectory + Trim(SystemReference) + "-" + Trim(ScanRef) + "_" + Appt + "-" + Trim(str(PatientId)) + "-" + LocalDate + "-" + Ref + Ext
                    BaseFileName = Trim(SystemReference) + "-" + Trim(ScanRef) + "_" + Appt + "_" + Trim(str(PatientId)) + "-" + LocalDate
                Else
                    TheFile = ScanDirectory + Trim(lstTypes.List(Idx)) + "-" + Trim(ScanRef) + "-" + Appt + "_" + Trim(str(PatientId)) + "-" + LocalDate + "-" + Ref + Ext
                    BaseFileName = Trim(lstTypes.List(Idx)) + "-" + Trim(ScanRef) + "_" + Appt + "_" + Trim(str(PatientId)) + "-" + LocalDate
                End If
                Call StripCharacters(TheFile, " ")
                Call StripCharacters(BaseFileName, " ")
                If (Len(BaseFileName) > 128) Then
                    FormScanFileName = False
                    Exit Do
                End If
            Loop
        End If
    End If

    Exit Function

lFormScanFileName_Error:

    LogError "frmScan", "FormScanFileName", Err, Err.Description, TheFile, , AppointmentId
End Function

Private Function GetScanFileName(Idx As Integer, TheFile As String) As Boolean
Dim i As Integer
    On Error GoTo lGetScanFileName_Error

GetScanFileName = False
TheFile = ""
If (Idx >= 0) Then
    TheFile = MyScanDirectory + Trim(Mid(lstScan.List(Idx), 65, Len(lstScan.List(Idx)) - 64))
    If (InStrPS(TheFile, "MyDraw") > 0) Then
        i = InStrPS(TheFile, "*")
        If (i > 0) Then
            TheFile = Mid(TheFile, i + 1, Len(TheFile) - i)
        End If
    ElseIf (InStrPS(TheFile, "NewBase") > 0) Then
        i = InStrPS(TheFile, "*")
        If (i > 0) Then
            TheFile = Mid(TheFile, i + 1, Len(TheFile) - i)
        End If
    End If
    GetScanFileName = True
End If

    Exit Function

lGetScanFileName_Error:

    LogError "frmScan", "GetScanFileName", Err, Err.Description, TheFile
End Function

Private Function ScanInFile(FileName As String) As Boolean
    On Error GoTo lScanInFile_Error

    ScanInFile = False
    If LegacyScan Then
        frmProcessScan.ScanFileName = Trim(FileName)
        frmProcessScan.Show 1
        If Not (frmProcessScan.QuitOn) And (Trim(frmProcessScan.ScanFileName) <> "") Then
            RotateOn = False
            ScanInFile = True
            cmdPrint.Visible = True
            cmdExit.Visible = True
            cmdRename.Visible = True
            cmdNew.Visible = True
            cmdRotate.Visible = True
            cmdRLeft.Visible = False
            cmdRRight.Visible = False
            cmdDelete.Visible = True
            cmdZoomIn.Visible = True
            cmdZoomOut.Visible = True
            If (NumPages > 1) Then
                cmdNextPix.Visible = True
                cmdPrevPix.Visible = True
            Else
                cmdNextPix.Visible = False
                cmdPrevPix.Visible = False
            End If
            lblScanRef.Visible = False
            txtScanRef.Visible = False
            lstTypes.Visible = False
            lstScan.Visible = False
            lstFilter.Visible = False
            SSDateCombo1.Visible = False
            If (Trim(FileName) <> Trim(frmProcessScan.ScanFileName)) Then
                FileName = Trim(frmProcessScan.ScanFileName)
            End If
            ReloadListOn = True
        End If
    Else
        Dim arguments() As Variant
        arguments = Array(Trim(FileName))
        Dim ImagingComWrapper As New comWrapper
        Call ImagingComWrapper.Create(ImagingViewManagerType, emptyArgs)
        ImagingComWrapper.InvokeMethod "ShowScanView", arguments
        ScanInFile = True
        ReloadListOn = True
    End If

    Exit Function

lScanInFile_Error:

    LogError "frmScan", "ScanInFile", Err, Err.Description, FileName
End Function

Private Function RetrieveScannedImages(PatId As Long, AFilter As String, AList As ListBox) As Boolean
    Dim k As Integer, q As Integer
    Dim MyFile As String, ARef As String
    Dim TheRef As String, ADate As String
    Dim TheDate As String, FileName As String
    Dim DisplayText As String
    Dim LocalDate As String
    Dim RetAppt As SchedulerAppointment
    Dim MyScanDirectoryPatient  As String
    On Error GoTo lRetrieveScannedImages_Error

    LocalDate = Mid(DisplayDate, 7, 4) + Mid(DisplayDate, 1, 2) + Mid(DisplayDate, 4, 2)
    
        
    FileName = MyScanDirectory + "*_" + Trim(str(PatId)) + "-*.*"
    MyFile = Dir(FileName)
    If (MyFile = "") Then
        FileName = MyScanDirectory + "*__" + Trim(str(PatId)) + "-*.*"
        MyFile = Dir(FileName)
    End If
    GoSub GetScanFiles
    
    MyScanDirectoryPatient = MyScanDirectory & Trim(str(PatId)) & "\"
    FileName = MyScanDirectoryPatient + "*_" + Trim(str(PatId)) + "-*.*"
    MyFile = Dir(FileName)
    If (MyFile = "") Then
        FileName = MyScanDirectoryPatient + "*__" + Trim(str(PatId)) + "-*.*"
        MyFile = Dir(FileName)
    End If
    GoSub GetScanFiles
    
    
    ' Amd the Collection Letters
    FileName = DocumentDirectory + "Coll*_" + Trim(str(PatId)) + "-*.*"
    MyFile = Dir(FileName)
    While (MyFile <> "")
        ARef = ""
        DisplayText = ""
        k = InStrPS(MyFile, ".")
        For q = k To 1 Step -1
            If (Mid(MyFile, q, 1) = "-") Then
                ARef = Mid(MyFile, q + 1, (k - 1) - q)
                Exit For
            End If
        Next q
        If (Val(ARef) > 0) Then
            Set RetAppt = New SchedulerAppointment
            RetAppt.AppointmentId = Val(ARef)
            If (RetAppt.RetrieveSchedulerAppointment) Then
                TheDate = RetAppt.AppointmentDate
                TheDate = Mid(TheDate, 5, 2) + "/" + Mid(TheDate, 7, 2) + "/" + Left(TheDate, 4)
                k = InStrPS(MyFile, "_")
                TheRef = Left(MyFile, k - 1)
                TheRef = Mid(TheRef, 5, Len(TheRef) - 4)
                DisplayText = TheDate + " Collect   " + TheRef
                DisplayText = DisplayText + Space(64 - Len(DisplayText)) + MyFile
                Mid(DisplayText, 60, 1) = "W"
                AList.AddItem RetAppt.AppointmentDate + DisplayText
            End If
            Set RetAppt = Nothing
        End If
        MyFile = Dir
    Wend

    Exit Function
GetScanFiles:
    
    While (MyFile <> "")
        ARef = ""
        DisplayText = ""
        k = InStrPS(MyFile, ".")
        If (Mid(MyFile, k - 3, 1) = "-") Then
            ARef = Mid(MyFile, k - 2, 2)
            k = k - 3
        End If
        k = InStrPS(MyFile, "_")
        If (k > 0) Then
            q = InStrPS(k, MyFile, "-")
            If (q > 0) Then
                TheDate = Mid(MyFile, q + 1, 8)
            End If
        End If
        ADate = TheDate
        TheDate = Mid(TheDate, 5, 2) + "/" + Mid(TheDate, 7, 2) + "/" + Left(TheDate, 4)
        TheRef = Left(MyFile, k - 1)
        DisplayText = TheDate + " " + TheRef + " " + ARef
        If (Len(DisplayText) > 59) Then
            DisplayText = Left(DisplayText, 59) + Space(5) + MyFile
        Else
            DisplayText = DisplayText + Space(59 - Len(DisplayText)) + Space(5) + MyFile
        End If
        Mid(DisplayText, 60, 1) = "I"
        If (ADate <= LocalDate) Then
            If (Trim(AFilter) = "") Or (Trim(AFilter) = "ANY") Then
                AList.AddItem ADate + DisplayText
            ElseIf (InStrPS(UCase(DisplayText), AFilter) > 0) Then
                AList.AddItem ADate + DisplayText
            End If
        End If
        MyFile = Dir
    Wend
    
    
Return
lRetrieveScannedImages_Error:

    LogError "frmScan", "RetrieveScannedImages", Err, Err.Description, MyFile, PatId
End Function


Private Function RetrieveConsentFormList(PatId As Long, AFilter As String, AList As ListBox) As Boolean
    Dim k As Integer, q As Integer
    Dim MyFile As String, ARef As String
    Dim TheRef As String, ADate As String
    Dim TheDate As String, FileName As String
    Dim DisplayText As String
    Dim LocalDate As String
    On Error GoTo lRetrieveConsentFormList_Error
    LocalDate = Mid(DisplayDate, 7, 4) + Mid(DisplayDate, 1, 2) + Mid(DisplayDate, 4, 2)
    FileName = PinPointDirectory + Trim(Str(PatId)) + "\Consent\"
    MyFile = Dir(FileName)
    
       While (MyFile <> "")
        ARef = ""
        DisplayText = ""
        k = InStrPS(MyFile, ".")
     
        If (k > 0) Then
             TheDate = Mid(MyFile, k - 14, 8)
        End If
        ADate = TheDate
        TheDate = Mid(TheDate, 5, 2) + "/" + Mid(TheDate, 7, 2) + "/" + Left(TheDate, 4)
        TheRef = "Consent"
        MyFile = Mid(MyFile, 1, k - 1)
        DisplayText = TheDate + " " + TheRef + " " + MyFile
        If (Len(DisplayText) > 59) Then
            DisplayText = Left(DisplayText, 59) + Space(5) + MyFile
        Else
            DisplayText = DisplayText + Space(59 - Len(DisplayText)) + Space(5) + MyFile
        End If
        Mid(DisplayText, 60, 1) = "C"
        If (ADate <= LocalDate) Then
            If (Trim(AFilter) = "") Or (Trim(AFilter) = "ANY") Or (AFilter = "CONSENTFORMS") Then
                AList.AddItem ADate + DisplayText
            ElseIf (InStrPS(UCase(DisplayText), AFilter) > 0) Then
                AList.AddItem ADate + DisplayText
            End If
        End If
        MyFile = Dir
    Wend

    Exit Function
lRetrieveConsentFormList_Error:

    LogError "frmScan", "RetrieveConsentFormList", Err, Err.Description, MyFile, PatId
End Function


Private Function AddImagesFromHistory(PatId As Long, AFilter As String, AList As ListBox) As Boolean
    Dim i As Integer, passNumber As Integer
    Dim FoundFile As Boolean
    Dim DrawExt As String
    Dim ADate As String, DName As String
    Dim TheDrawFile As String, TheDiag As String
    Dim TestFile As String, DisplayItem As String
    Dim RetDiag As DiagnosisMasterPrimary
    Dim LocalDate As String
    Dim PathsToCheckParam() As String
    Dim CollectedPathInfos As New Collection, PathsToCheck As New Collection, AddedImages As New Collection
    On Error GoTo lAddImagesFromHistory_Error

    AddImagesFromHistory = False
    
    ' Very images are not filtered out
    If (Not ((AFilter = "") Or (AFilter = "ANY") Or (AFilter = "IMAGES"))) Then
        Exit Function
    End If
    
    LocalDate = Mid(DisplayDate, 7, 4) + Mid(DisplayDate, 1, 2) + Mid(DisplayDate, 4, 2)
    If (PatId > 0) Then
        ApplClnId = 0
        ApplApptId = 0
        ApplApptDate = ""
        ApplDraw = 0
        ApplSymptom = ""
        passNumber = 1
        
lAddImagesFromHistory_SecondPass:
        If (GetImageList(PatId)) Then
            i = 1
            Do Until Not (SelectImageList(i))
                If (Trim(ApplApptDate) = "") Or (Trim(ApplDraw) = "") Then
                    GoTo lAddImagesFromHistory_NextItem
                End If
                ' Not from a future appointment?
                If (ApplApptDate > LocalDate) Then
                    GoTo lAddImagesFromHistory_NextItem
                End If
                
                TestFile = ""
                DName = ""
                ADate = ApplApptDate
                TheDiag = Trim(ApplSymptom)
                
                DrawExt = ".jpg"
                TheDrawFile = MyImageDir + "MyDraw-D" + Trim(TheDiag) + "-A" + Trim(str(ApplApptId)) + "-B" + Trim(ApplDraw) + DrawExt
                
                GoSub lAddImagesFromHistory_IsImagePresent
                If (FoundFile) Then
                    TestFile = TheDrawFile
                Else
                    DrawExt = ".bmp"
                    TheDrawFile = MyImageDir + "MyDraw-D" + Trim(TheDiag) + "-A" + Trim(str(ApplApptId)) + "-B" + Trim(ApplDraw) + DrawExt

                    GoSub lAddImagesFromHistory_IsImagePresent
                    If (FoundFile) Then
                        TestFile = TheDrawFile
                    End If
                End If
                
                If (Trim(TestFile) = "") Then
                    DrawExt = ".jpg"
                    TheDrawFile = MyImageDir + "MyDraw-A" + Trim(str(ApplApptId)) + "-B" + Trim(ApplDraw) + DrawExt

                    GoSub lAddImagesFromHistory_IsImagePresent
                    If (FoundFile) Then
                        TestFile = TheDrawFile
                    Else
                        DrawExt = ".bmp"
                        TheDrawFile = MyImageDir + "MyDraw-A" + Trim(str(ApplApptId)) + "-B" + Trim(ApplDraw) + DrawExt
                
                        GoSub lAddImagesFromHistory_IsImagePresent
                        If (FoundFile) Then
                            TestFile = TheDrawFile
                        End If
                    End If
                End If
                    
                If (TestFile = "") Then
                    If (Left(Trim(ApplDraw), 2) = "I-") Then
                        DrawExt = ".jpg"
                        TheDrawFile = MyImageDir + "MyDraw-D" + Trim(TheDiag) + "-A" + Trim(str(ApplApptId)) + "-B" + Trim(Mid(Trim(ApplDraw), 3, Len(Trim(ApplDraw)) - 2)) + DrawExt

                        GoSub lAddImagesFromHistory_IsImagePresent
                        If (FoundFile) Then
                            TestFile = TheDrawFile
                        Else
                            DrawExt = ".bmp"
                            TheDrawFile = MyImageDir + "MyDraw-D" + Trim(TheDiag) + "-A" + Trim(str(ApplApptId)) + "-B" + Trim(Mid(Trim(ApplDraw), 3, Len(Trim(ApplDraw)) - 2)) + DrawExt
                        
                            GoSub lAddImagesFromHistory_IsImagePresent
                            If (FoundFile) Then
                                TestFile = TheDrawFile
                            End If
                        End If
                        If (Trim(TestFile) = "") Then
                            DrawExt = ".jpg"
                            TheDrawFile = MyImageDir + "MyDraw-A" + Trim(str(ApplApptId)) + "-B" + Trim(Mid(Trim(ApplDraw), 3, Len(Trim(ApplDraw)) - 2)) + DrawExt
                            
                            GoSub lAddImagesFromHistory_IsImagePresent
                            If (FoundFile) Then
                                TestFile = TheDrawFile
                            Else
                                DrawExt = ".bmp"
                                TheDrawFile = MyImageDir + "MyDraw-A" + Trim(str(ApplApptId)) + "-B" + Trim(Mid(Trim(ApplDraw), 3, Len(Trim(ApplDraw)) - 2)) + DrawExt
                            
                                GoSub lAddImagesFromHistory_IsImagePresent
                                If (FoundFile) Then
                                    TestFile = TheDrawFile
                                End If
                            End If
                        End If

                    ElseIf (InStrPS(ApplDraw, "NewBase") = 0) Then
                        DrawExt = ".jpg"
                        TheDrawFile = MyImageDir + "MyDraw-D" + Trim(TheDiag) + "-A" + Trim(str(ApplApptId)) + "-BI-" + Trim(ApplDraw) + DrawExt
                        
                        GoSub lAddImagesFromHistory_IsImagePresent
                        If (FoundFile) Then
                            TestFile = TheDrawFile
                        Else
                            DrawExt = ".bmp"
                            TheDrawFile = MyImageDir + "MyDraw-D" + Trim(TheDiag) + "-A" + Trim(str(ApplApptId)) + "-BI-" + Trim(ApplDraw) + DrawExt
                        
                            GoSub lAddImagesFromHistory_IsImagePresent
                            If (FoundFile) Then
                                TestFile = TheDrawFile
                            End If
                        End If
                        If (Trim(TestFile) = "") Then
                            DrawExt = ".jpg"
                            TheDrawFile = MyImageDir + "MyDraw-A" + Trim(str(ApplApptId)) + "-BI-" + Trim(ApplDraw) + DrawExt
                            
                            GoSub lAddImagesFromHistory_IsImagePresent
                            If (FoundFile) Then
                                TestFile = TheDrawFile
                            Else
                                DrawExt = ".bmp"
                                TheDrawFile = MyImageDir + "MyDraw-A" + Trim(str(ApplApptId)) + "-BI-" + Trim(ApplDraw) + DrawExt
                                
                                GoSub lAddImagesFromHistory_IsImagePresent
                                If (FoundFile) Then
                                    TestFile = TheDrawFile
                                End If
                            End If
                        End If
                    ElseIf (InStrPS(ApplDraw, "NewBase") <> 0) Then
                        If (Trim(TestFile) = "") Then
                            TheDrawFile = MyImageDir + Trim(Mid(ApplDraw, 3, Len(ApplDraw) - 2))
                            
                            GoSub lAddImagesFromHistory_IsImagePresent
                            If (FoundFile) Then
                                TestFile = TheDrawFile
                            End If
                        End If
                    End If
                End If
                
                ' If image found and it's a new image -> use it
                If passNumber <> 1 And Not (CollectionKeyExists(AddedImages, TestFile)) And (Trim(TestFile) <> "") Then
                    Set RetDiag = New DiagnosisMasterPrimary
                    RetDiag.PrimaryId = Val(Trim(ApplSymptom))
                    If (RetDiag.RetrievePrimary) Then
                        DName = Trim(RetDiag.PrimaryLingo)
                    End If
                    Set RetDiag = Nothing
                    DisplayItem = Space(65)
                    Mid(DisplayItem, 1, 10) = Mid(ADate, 5, 2) + "/" + Mid(ADate, 7, 2) + "/" + Left(ADate, 4)
                    Mid(DisplayItem, 12, 9) = GetImageReferenceName(TestFile)
                    Mid(DisplayItem, 22, 35) = Left(DName, 35)
                    Mid(DisplayItem, 60, 1) = "I"
                    DisplayItem = ADate + DisplayItem + Trim(str(ApplClnId)) + "*" + TestFile
                    
                    ' Add item to list
                    AList.AddItem DisplayItem
                    AddedImages.Add DisplayItem, TestFile
                End If

lAddImagesFromHistory_NextItem:
                i = i + 1
            Loop
            
            If (passNumber = 1) Then
                ' Convert collection of paths to string array
                ReDim PathsToCheckParam(PathsToCheck.Count - 1) As String
                For i = 1 To PathsToCheck.Count
                    PathsToCheckParam(i - 1) = PathsToCheck(i)
                Next i
                
                ' Retrieve files information
                Set CollectedPathInfos = FM.GetFileInfos(PathsToCheckParam)
                passNumber = 2
                GoTo lAddImagesFromHistory_SecondPass
            End If
            
            Set ApplListTbl = Nothing
        End If
    End If
    AddImagesFromHistory = True

    Exit Function

lAddImagesFromHistory_IsImagePresent:
    If (passNumber = 1) Then
        If Not (CollectionKeyExists(PathsToCheck, TheDrawFile)) Then
            PathsToCheck.Add TheDrawFile, TheDrawFile
        End If
        FoundFile = False
    Else
        FoundFile = CollectedPathInfos(TheDrawFile)("Exists")
    End If
    Return
    
lAddImagesFromHistory_Error:

    LogError "frmScan", "AddImagesFromHistory", Err, Err.Description, TestFile, PatId, ApplApptId
End Function

Private Function GetImageList(PatId As Long) As Boolean
Dim StrSql As String
    On Error GoTo lGetImageList_Error

GetImageList = False
If (PatId > 0) Then
    StrSql = "usp_DI_DocumentImages " + Trim(str(PatId)) + " "
    Set ApplListTbl = Nothing
    Set ApplListTbl = CreateAdoRecordset
    Call ApplListTbl.Open(StrSql, MyPracticeRepository, adOpenStatic, adLockOptimistic, adCmdText)
    GetImageList = Not (ApplListTbl.EOF)
End If

    Exit Function

lGetImageList_Error:

    LogError "frmScan", "GetImageList", Err, Err.Description, StrSql, PatId
End Function

Private Function SelectImageList(ListItem As Integer) As Boolean
    On Error GoTo lSelectImageList_Error

    If ListItem > 0 Then
        ApplListTbl.Move ListItem - ApplListTbl.AbsolutePosition
        If (ApplListTbl("ClinicalId") <> "") Then
            ApplClnId = ApplListTbl("ClinicalId")
        Else
            ApplClnId = 0
        End If
        If (ApplListTbl("AppointmentId") <> "") Then
            ApplApptId = ApplListTbl("AppointmentId")
        Else
            ApplApptId = 0
        End If
        If (ApplListTbl("DrawFileName") <> "") Then
            ApplDraw = ApplListTbl("DrawFileName")
        Else
            ApplDraw = ""
        End If
        If (ApplListTbl("AppDate") <> "") Then
            ApplApptDate = ApplListTbl("AppDate")
        Else
            ApplApptDate = ""
        End If
        If (ApplListTbl("Symptom") <> "") Then
            ApplSymptom = ApplListTbl("Symptom")
        Else
            ApplSymptom = ""
        End If
        SelectImageList = True
    End If

    Exit Function

lSelectImageList_Error:
    If Not ApplListTbl.EOF Then
        LogError "frmScan", "SelectImageList", Err, Err.Description, "ApptId: " & Trim$(str$(ApplApptId)) & " - ListItem: " & str$(ListItem), , ApplApptId
    End If
End Function

Private Function RetrieveDocumentList(PatId As Long, AFilter As String, AList As ListBox) As Long
    Dim i As Integer
    Dim AFile As String
    Dim DisplayItem As String
    Dim LocalDate As String
    Dim PostIt As Boolean
    On Error GoTo lRetrieveDocumentList_Error

    LocalDate = Mid(DisplayDate, 7, 4) + Mid(DisplayDate, 1, 2) + Mid(DisplayDate, 4, 2)
    RetrieveDocumentList = -1
    If (PatId > 0) Then
        RetrieveDocumentList = GetDocumentList(PatId)
        If (RetrieveDocumentList > 0) Then
            For i = 1 To RetrieveDocumentList
                PostIt = True
                ApptDate = ""
                TransId = 0
                APatId = 0
                TransRem = ""
                TransRef = ""
                TransDate = ""
                ApptType = ""
                TransType = ""
                If (SelectList(i)) Then
                    AFile = "Ltrs-" + TransType + Trim(str(TransId))
                    DisplayItem = Space(65)
                    Mid(DisplayItem, 1, 10) = Mid(ApptDate, 5, 2) + "/" + Mid(ApptDate, 7, 2) + "/" + Left(ApptDate, 4)
                    If (TransType = "S") Then
                        Mid(DisplayItem, 12, 9) = "Consult  "
                    ElseIf (TransType = "Y") Then
                        Mid(DisplayItem, 12, 9) = "Facility "
                    ElseIf (TransType = "X") Then
                        Mid(DisplayItem, 12, 9) = "External "
                    ElseIf (TransType = "F") Then
                        Mid(DisplayItem, 12, 9) = "Referral "
                    Else
                        PostIt = False
                    End If
                    If (PostIt) And (ApptDate <= LocalDate) Then
                        If (AFilter = "") Or (AFilter = "ANY") Or (AFilter = "DOCUMENTS") Then
                            Mid(DisplayItem, 22, 15) = Trim(Left(TransRef, 15))
'                           Mid(DisplayItem, 39, 20) = Trim(Mid(TransRem, 6, 20))
                            Mid(DisplayItem, 39, 20) = AFile
                            Mid(DisplayItem, 60, 1) = "D"
                            DisplayItem = DisplayItem + AFile
                            AList.AddItem TransDate + DisplayItem
                        End If
                    End If
                End If
            Next i
        End If
    End If

    Exit Function

lRetrieveDocumentList_Error:

    LogError "frmScan", "RetrieveDocumentList", Err, Err.Description, str$(PatId), PatId

End Function

Private Function GetDocumentList(PatId As Long) As Long

    On Error GoTo lGetDocumentList_Error
ApplListTotal = 0
GetDocumentList = -1
If (PatId > 0) Then
    Set ApplListTbl = Nothing
    Set ApplListTbl = CreateAdoRecordset
    MyPracticeRepositoryCmd.CommandType = adCmdStoredProc
    MyPracticeRepositoryCmd.CommandText = "PatientDocumentList"
    Set MyParameters = Nothing
    Set MyParameters = MyPracticeRepositoryCmd.CreateParameter("@PatId")
    MyParameters.Type = adInteger
    MyParameters.Direction = adParamInput
    MyParameters.Size = Len(Trim(str(PatId)))
    MyParameters.Value = Trim(str(PatId))
    MyPracticeRepositoryCmd.Parameters()(0) = MyParameters
    Call ApplListTbl.Open(MyPracticeRepositoryCmd, , adOpenStatic, adLockOptimistic, adCmdStoredProc)
    If Not (ApplListTbl.EOF) Then
        ApplListTbl.MoveLast
        ApplListTotal = ApplListTbl.RecordCount
        GetDocumentList = ApplListTotal
    End If
End If
    Exit Function

lGetDocumentList_Error:

    LogError "frmScan", "GetDocumentList", Err, Err.Description, str$(PatId), PatId

End Function

Private Function SelectList(ListItem As Integer) As Boolean
    On Error GoTo lSelectList_Error

SelectList = False
If (ApplListTbl.EOF) Or (ListItem < 1) Or (ListItem > ApplListTotal) Then
    Exit Function
End If
ApplListTbl.MoveFirst
ApplListTbl.Move ListItem - 1
If (ApplListTbl("TransactionType") <> "") Then
    TransType = ApplListTbl("TransactionType")
Else
    TransType = ""
End If
If (ApplListTbl("PatientId") <> "") Then
    APatId = ApplListTbl("PatientId")
Else
    APatId = 0
End If
If (ApplListTbl("TransactionId") <> "") Then
    TransId = ApplListTbl("TransactionId")
Else
    TransId = 0
End If
If (ApplListTbl("TransactionDate") <> "") Then
    TransDate = ApplListTbl("TransactionDate")
Else
    TransDate = ""
End If
If (ApplListTbl("AppointmentType") <> "") Then
    ApptType = ApplListTbl("AppointmentType")
Else
    ApptType = ""
End If
If (ApplListTbl("AppDate") <> "") Then
    ApptDate = ApplListTbl("AppDate")
Else
    ApptDate = ""
End If
If (ApplListTbl("TransactionRef") <> "") Then
    TransRef = ApplListTbl("TransactionRef")
Else
    TransRef = ""
End If
If (ApplListTbl("TransactionRemark") <> "") Then
    TransRem = ApplListTbl("TransactionRemark")
Else
    TransRem = ""
End If
SelectList = True


    Exit Function

lSelectList_Error:

    LogError "frmScan", "SelectList", Err, Err.Description, str$(ListItem), APatId

End Function

Private Function LoadListTypes() As Boolean
    Dim i As Integer
    Dim RetCode As PracticeCodes
    On Error GoTo lLoadListTypes_Error

    lstFilter.Clear
    lstFilterBack.Clear
    lstFilter.AddItem "Any"
    lstFilter.AddItem "Images"
    lstFilter.AddItem "Documents"
	lstFilter.AddItem "ConsentForms"
    lstFilterBack.AddItem "Ref=Any"
    lstFilterBack.AddItem "Ref=Images"
    lstFilterBack.AddItem "Ref=Documents"
	lstFilterBack.AddItem "Ref=ConsentForms"
    Set RetCode = New PracticeCodes
    RetCode.ReferenceType = "ScanType"
    If (RetCode.FindCode > 0) Then
        i = 1
        While (RetCode.SelectCode(i))
            lstFilter.AddItem Trim(RetCode.ReferenceCode)
            If (Trim(RetCode.LetterLanguage) = "") Then
                lstFilterBack.AddItem "Ref=" + Trim(RetCode.ReferenceCode)
            Else
                lstFilterBack.AddItem Trim(RetCode.LetterLanguage)
            End If
            i = i + 1
        Wend
    End If
    Set RetCode = Nothing

    Exit Function

lLoadListTypes_Error:

    LogError "frmScan", "LoadListTypes", Err, Err.Description
End Function

Private Sub SSDateCombo1_Change()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_Click()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_CloseUp()
If (Trim(SSDateCombo1.Date) <> "") Then
    Call SSDateCombo1_KeyPress(13)
End If
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
    DisplayDate = SSDateCombo1.DateValue
    Call LoadScan
End If
End Sub

Private Function GetImageReferenceName(AFile As String) As String
    On Error GoTo lGetImageReferenceName_Error

GetImageReferenceName = "Image"
If (Trim(AFile) <> "") Then
    If (InStrPS(AFile, "-01") <> 0) Then
        GetImageReferenceName = "Adnexa/Lids"
    ElseIf (InStrPS(AFile, "-02") <> 0) Then
        GetImageReferenceName = "Adnexa/Lids everted"
    ElseIf (InStrPS(AFile, "-03") <> 0) Then
        GetImageReferenceName = "Cornea"
    ElseIf (InStrPS(AFile, "-04") <> 0) Then
        GetImageReferenceName = "Iris"
    ElseIf (InStrPS(AFile, "-05") <> 0) Then
        GetImageReferenceName = "Lens"
    ElseIf (InStrPS(AFile, "-07") <> 0) Then
        GetImageReferenceName = "Fundus"
    ElseIf (InStrPS(AFile, "-08") <> 0) Then
        GetImageReferenceName = "Fundus"
    ElseIf (InStrPS(AFile, "-09") <> 0) Then
        GetImageReferenceName = "Adnexa/Lids"
    ElseIf (InStrPS(AFile, "-12") <> 0) Then
        GetImageReferenceName = "Confrontation Field"
    ElseIf (InStrPS(AFile, "-13") <> 0) Then
        GetImageReferenceName = "Optic Nerve"
    ElseIf (InStrPS(AFile, "-14") <> 0) Then
        GetImageReferenceName = "Face/Adnexa"
    End If
End If
If (Len(GetImageReferenceName) < 19) Then
    GetImageReferenceName = GetImageReferenceName + Space(19 - Len(GetImageReferenceName))
End If

    Exit Function

lGetImageReferenceName_Error:

    LogError "frmScan", "GetImageReferenceName", Err, Err.Description, GetImageReferenceName
End Function

Private Function LoadImage(FileName As String, LoadIt As Boolean, Optional SubDir As String) As Boolean
    Dim FileOperationInProgress As Boolean
    On Error GoTo lLoadImage_Error

    LoadImage = False
    If (Trim(FileName) <> "") Then
        If (FM.IsFileThere(SelectedFile)) Then
        
            If Not SubDir = "" Then
                Dim fn(2) As String
                fn(0) = FileName
                fn(2) = FM.GetFileName(fn(0))
                fn(1) = Mid(fn(0), 1, Len(fn(0)) - Len(fn(2)))
                
                If fn(1) = MyScanDirectory Then
                    fn(1) = fn(1) & SubDir & "\"
                    fn(0) = fn(1) & fn(2)
                    FileName = fn(0)
                End If
                
                If (FM.Dir(fn(1), vbDirectory) = "") Then
                    FM.MkDir (fn(1))
                End If
                
            End If
            
            FileOperationInProgress = True
            FM.Compress (SelectedFile)
            FM.Name SelectedFile, FileName
            FileOperationInProgress = False

            If (LoadIt) Then
                ReplacementFile = FileName
                If (InStrPS(UCase(FileName), ".BMP") <> 0) Then
                    FXImage1.FileName = FileName
                    FXImage1.Tag = Trim(FileName)
                ElseIf (InStrPS(UCase(FileName), ".TIF") <> 0) Then
                    FXImage1.FileName = FileName
                    FXImage1.Tag = Trim(FileName)
                ElseIf (InStrPS(UCase(FileName), ".PDF") <> 0) Then
                    SelectedFile = ""
                    lstTypes.Visible = False
                    lstScan.ListIndex = -1
                    lstScan.Visible = False
                    lstFilter.Visible = False
                    SSDateCombo1.Visible = False
                    lblScanRef.Visible = False
                    txtScanRef.Visible = False
                    txtScanRef.Text = ""
                    cmdCancel.Visible = True
                    cmdAttach.Visible = True
                    cmdCreate.Visible = True
                    Call cmdCancel_Click
                    Call TriggerPDF(FileName)
                    Exit Function
                ElseIf (InStrPS(UCase(FileName), ".DOC") <> 0) Or (InStrPS(UCase(FileName), ".RTF") <> 0) Then
                    Call KillDocumentProcessing(FileName, True, False)
                    Call TriggerWord(FileName)
                    lstTypes.Visible = False
                    lblScanRef.Visible = False
                    txtScanRef.Visible = False
                    Exit Function
                Else
                    FXImage1.picture = FM.LoadPicture(FileName)
                    FXImage1.Tag = Trim(FileName)
                End If
                FXImage1.AutoSize = ISIZE_CropImage
                FXImage1.ScrollBars = SB_Both
                CurrentPage = 1
                NumPages = FXImage1.NumPages(FileName)
                FXImage1.PageNbr = CurrentPage
                FXImage1.ZoomToFit ZOOMFIT_BEST
                FXImage1.ZOrder 0
                FXImage1.Visible = True
                cmdPrint.Visible = True
                cmdExit.Visible = True
                cmdRename.Visible = True
                cmdNew.Visible = True
                cmdRotate.Visible = True
                cmdRLeft.Visible = False
                cmdRRight.Visible = False
                cmdDelete.Visible = True
                cmdZoomIn.Visible = True
                cmdZoomOut.Visible = True
                If (NumPages > 1) Then
                    cmdNextPix.Visible = True
                    cmdPrevPix.Visible = True
                Else
                    cmdNextPix.Visible = False
                    cmdPrevPix.Visible = False
                End If
                lstTypes.Visible = False
                lstScan.Visible = False
                lstFilter.Visible = False
                SSDateCombo1.Visible = False
                lblScanRef.Visible = False
                txtScanRef.Visible = False
                cmdCancel.Visible = False
                cmdAttach.Visible = False
                cmdCreate.Visible = False
    ' purge only if pulling from myscan folder
                If (InStrPS(SelectedFile, MyScanDirectory) > 0) Then
                    FM.Kill SelectedFile
                End If
                DoEvents
            Else
                ReloadListOn = True
                cmdCancel_Click
                ReloadListOn = False
            End If
        End If
    End If

    Exit Function

lLoadImage_Error:
    LogError "frmScan", "LoadImage", Err, Err.Description, SelectedFile
    If (FileOperationInProgress) Then
        MsgBox "Could not access selected file. Please, ensure it's not opened in another application. Check event log for more details.", vbExclamation
    End If
End Function

Public Function LoadSelectedFile(AFile As String, ADate As String, AFilter As String) As Boolean
    Dim i As Integer
    Dim MyTrig As Integer
    On Error GoTo lLoadSelectedFile_Error

    MyTrig = -1
    LoadSelectedFile = False
    If (Trim(AFile) <> "") Then
    ' check by filter first
        If (Trim(AFilter) <> "") Then
            For i = 0 To lstFilterBack.ListCount - 1
                If (InStrPS(UCase(lstFilterBack.List(i)), AFilter) <> 0) Then
                    LoadSelectedFile = True
                    MyTrig = i
                    Exit For
                End If
            Next i
        End If
        If (MyTrig < 0) Then
            For i = 0 To lstFilter.ListCount - 1
                If (InStrPS(UCase(lstFilter.List(i)), AFile) <> 0) Then
                    LoadSelectedFile = True
                    MyTrig = i
                    Exit For
                End If
            Next i
        End If
        If (MyTrig < 0) Then
            For i = 0 To lstFilter.ListCount - 1
                If (InStrPS(AFile, UCase(lstFilter.List(i))) <> 0) Then
                    LoadSelectedFile = True
                    MyTrig = i
                    Exit For
                End If
            Next i
        End If
        If (MyTrig >= 0) Then
            lstFilter.ListIndex = MyTrig
            If (lstScan.ListCount > 0) Then
                For i = 0 To lstScan.ListCount - 1
                    If (Left(lstScan.List(i), 10) = ADate) Then
                        lstScan.ListIndex = i
                        Exit For
                    End If
                Next i
            End If
        End If
    End If

    Exit Function

lLoadSelectedFile_Error:

    LogError "frmScan", "LoadSelectedFile", Err, Err.Description, AFile
End Function

Private Function KillPDFFile(AFile As String) As Boolean
On Error GoTo ErrorOut
KillPDFFile = False
If (Trim(AFile) <> "") Then
    If (FM.IsFileThere(AFile)) Then
        FM.Kill AFile
        KillPDFFile = True
    End If
End If
Exit Function
ErrorOut:
    Resume LeaveFast
LeaveFast:
    frmEventMsgs.Header = "Document in use by another user - cannot delete"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End Function

Private Function GetImageAppName() As String
Dim oCode As New PracticeCodes
    On Error GoTo lGetImageAppName_Error

    oCode.ReferenceType = "ImageApp"
    If oCode.FindCode > 0 Then
        oCode.SelectCode 1
        sDocumentCommand = oCode.LetterLanguage & " " & oCode.LetterLanguage1
        GetImageAppName = oCode.ReferenceCode
    End If
    Set oCode = Nothing

    Exit Function

lGetImageAppName_Error:

    LogError "frmScan", "GetImageAppName", Err, Err.Description
    
End Function
Public Function FrmClose()
 Call cmdDone_Click
 Unload Me
End Function
