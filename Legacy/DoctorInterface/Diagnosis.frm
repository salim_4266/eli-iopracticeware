VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "BTN32A20.OCX"
Begin VB.Form frmDiagnosis 
   BackColor       =   &H00A95911&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11880
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   11880
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10080
      TabIndex        =   9
      Top             =   7680
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis1 
      Height          =   990
      Left            =   840
      TabIndex        =   0
      Top             =   1680
      Visible         =   0   'False
      Width           =   5295
      _Version        =   131072
      _ExtentX        =   9340
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":0233
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis2 
      Height          =   990
      Left            =   840
      TabIndex        =   1
      Top             =   3000
      Visible         =   0   'False
      Width           =   5295
      _Version        =   131072
      _ExtentX        =   9340
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":040E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis3 
      Height          =   990
      Left            =   840
      TabIndex        =   2
      Top             =   4320
      Visible         =   0   'False
      Width           =   5295
      _Version        =   131072
      _ExtentX        =   9340
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":05E9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis4 
      Height          =   990
      Left            =   840
      TabIndex        =   3
      Top             =   5640
      Visible         =   0   'False
      Width           =   5295
      _Version        =   131072
      _ExtentX        =   9340
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":07C4
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis6 
      Height          =   990
      Left            =   6360
      TabIndex        =   4
      Top             =   3000
      Visible         =   0   'False
      Width           =   5295
      _Version        =   131072
      _ExtentX        =   9340
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":099F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDiagnosis5 
      Height          =   990
      Left            =   6360
      TabIndex        =   5
      Top             =   1680
      Visible         =   0   'False
      Width           =   5295
      _Version        =   131072
      _ExtentX        =   9340
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":0B7A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdOther 
      Height          =   990
      Left            =   6360
      TabIndex        =   6
      Top             =   4320
      Width           =   5295
      _Version        =   131072
      _ExtentX        =   9340
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":0D55
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMore 
      Height          =   990
      Left            =   6360
      TabIndex        =   7
      Top             =   5640
      Width           =   5295
      _Version        =   131072
      _ExtentX        =   9340
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":0F35
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   975
      Left            =   840
      TabIndex        =   10
      Top             =   7680
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Diagnosis.frx":1114
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00AC897B&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Select Diagnosis"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   540
      Left            =   840
      TabIndex        =   8
      Top             =   360
      Width           =   3330
   End
End
Attribute VB_Name = "frmDiagnosis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ActivityId As Long
Public PatientId As Long
Public AppointmentId As Long
Public ResourceName As String
Public EyeContext As String
Public Diagnosis As String

Private CurrentIndex As Integer
Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub ClearButtons()
cmdDiagnosis1.Text = ""
cmdDiagnosis1.Visible = False
cmdDiagnosis2.Text = ""
cmdDiagnosis2.Visible = False
cmdDiagnosis3.Text = ""
cmdDiagnosis3.Visible = False
cmdDiagnosis4.Text = ""
cmdDiagnosis4.Visible = False
cmdDiagnosis5.Text = ""
cmdDiagnosis5.Visible = False
cmdDiagnosis6.Text = ""
cmdDiagnosis6.Visible = False
End Sub

Private Sub cmdDone_Click()
Unload frmDiagnosis
End Sub

Private Sub cmdMore_Click()
Call LoadDiagnosis
End Sub

Private Sub cmdNotes_Click()
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.ResourceName = ResourceName
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Private Sub cmdOther_Click()
Diagnosis = "-"
Call cmdDone_Click
End Sub

Public Function LoadDiagnosis() As Boolean
On Error GoTo UI_ErrorHandler
Dim k As Integer
Dim p As Integer
Dim TheDiagnosis As String
Dim TotalClinical As Integer
Dim RetrievalClinical As PatientClinical
LoadDiagnosis = False
EyeContext = ""
TheDiagnosis = ""
Call ClearButtons
Set RetrievalClinical = New PatientClinical
RetrievalClinical.PatientId = PatientId
RetrievalClinical.AppointmentId = AppointmentId
RetrievalClinical.ClinicalType = "D"
TotalClinical = RetrievalClinical.FindPatientClinical
If (TotalClinical > 0) Then
    k = CurrentIndex + 1
    If (k > TotalClinical) Then
        k = 1
        CurrentIndex = 0
    End If
    p = 1
    While (RetrievalClinical.SelectPatientClinical(k)) And (p < 7)
        TheDiagnosis = RetrievalClinical.EyeContext + "-" + Trim(RetrievalClinical.Symptom)
        If (p = 1) Then
            cmdDiagnosis1.Text = TheDiagnosis
            cmdDiagnosis1.Visible = True
        ElseIf (p = 2) Then
            cmdDiagnosis2.Text = TheDiagnosis
            cmdDiagnosis2.Visible = True
        ElseIf (p = 3) Then
            cmdDiagnosis3.Text = TheDiagnosis
            cmdDiagnosis3.Visible = True
        ElseIf (p = 4) Then
            cmdDiagnosis4.Text = TheDiagnosis
            cmdDiagnosis4.Visible = True
        ElseIf (p = 5) Then
            cmdDiagnosis5.Text = TheDiagnosis
            cmdDiagnosis5.Visible = True
        ElseIf (p = 6) Then
            cmdDiagnosis6.Text = TheDiagnosis
            cmdDiagnosis6.Visible = True
        End If
        p = p + 1
        k = k + 1
    Wend
End If
CurrentIndex = k
If (CurrentIndex > 0) Then
    LoadDiagnosis = True
End If
If (TotalClinical <= 6) Then
    cmdMore.Visible = False
ElseIf (CurrentIndex >= TotalClinical) Then
    cmdMore.Text = "Starting Over"
Else
    cmdMore.Text = "More"
End If
Set RetrievalClinical = Nothing
Exit Function
UI_ErrorHandler:
    Set RetrievalClinical = Nothing
    Resume LeaveFast
LeaveFast:
End Function

Private Sub cmdDiagnosis1_Click()
Call SelectDiagnosis(cmdDiagnosis1, 1)
If (cmdDiagnosis1.BackColor <> BaseBackColor) Then
    Unload frmDiagnosis
End If
End Sub

Private Sub cmdDiagnosis2_Click()
Call SelectDiagnosis(cmdDiagnosis2, 2)
If (cmdDiagnosis2.BackColor <> BaseBackColor) Then
    Unload frmDiagnosis
End If
End Sub

Private Sub cmdDiagnosis3_Click()
Call SelectDiagnosis(cmdDiagnosis3, 3)
If (cmdDiagnosis3.BackColor <> BaseBackColor) Then
    Unload frmDiagnosis
End If
End Sub

Private Sub cmdDiagnosis4_Click()
Call SelectDiagnosis(cmdDiagnosis4, 4)
If (cmdDiagnosis4.BackColor <> BaseBackColor) Then
    Unload frmDiagnosis
End If
End Sub

Private Sub cmdDiagnosis5_Click()
Call SelectDiagnosis(cmdDiagnosis5, 5)
If (cmdDiagnosis5.BackColor <> BaseBackColor) Then
    Unload frmDiagnosis
End If
End Sub

Private Sub cmdDiagnosis6_Click()
Call SelectDiagnosis(cmdDiagnosis6, 6)
If (cmdDiagnosis6.BackColor <> BaseBackColor) Then
    Unload frmDiagnosis
End If
End Sub

Private Sub TurnOn()
    cmdDiagnosis1.Enabled = True
    cmdDiagnosis2.Enabled = True
    cmdDiagnosis3.Enabled = True
    cmdDiagnosis4.Enabled = True
    cmdDiagnosis5.Enabled = True
    cmdDiagnosis6.Enabled = True
End Sub

Private Sub TurnOff(Ref As Integer)
    If (Ref <> 1) Then
        cmdDiagnosis1.Enabled = False
    End If
    If (Ref <> 2) Then
        cmdDiagnosis2.Enabled = False
    End If
    If (Ref <> 3) Then
        cmdDiagnosis3.Enabled = False
    End If
    If (Ref <> 4) Then
        cmdDiagnosis4.Enabled = False
    End If
    If (Ref <> 5) Then
        cmdDiagnosis5.Enabled = False
    End If
    If (Ref <> 6) Then
        cmdDiagnosis6.Enabled = False
    End If
End Sub

Private Sub SelectDiagnosis(AButton As fpBtn, Ref As Integer)
Dim q As Integer
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    Call TurnOff(Ref)
    q = InStr(AButton.Text, "-")
    If (q = 0) Then
        Diagnosis = AButton.Text
        EyeContext = "OS"
    Else
        Diagnosis = Mid(AButton.Text, q + 1, Len(AButton.Text) - q)
        EyeContext = Left(AButton.Text, q - 1)
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    Call TurnOn
    Diagnosis = ""
    EyeContext = ""
End If
End Sub

Private Sub Form_Load()
CurrentIndex = 0
ButtonSelectionColor = 14745312
TextSelectionColor = 0
BaseBackColor = cmdDiagnosis1.BackColor
BaseTextColor = cmdDiagnosis1.ForeColor
End Sub
