VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmModifiers 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "Modifiers.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   9600
      TabIndex        =   0
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreMods 
      Height          =   1095
      Left            =   3840
      TabIndex        =   1
      Top             =   6480
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":38FC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods4 
      Height          =   1095
      Left            =   240
      TabIndex        =   2
      Top             =   4080
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":3AE5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods3 
      Height          =   1095
      Left            =   240
      TabIndex        =   3
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":3CC5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods2 
      Height          =   1095
      Left            =   240
      TabIndex        =   4
      Top             =   1680
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":3EA5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods12 
      Height          =   1095
      Left            =   7560
      TabIndex        =   5
      Top             =   1680
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":4085
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods13 
      Height          =   1095
      Left            =   7560
      TabIndex        =   6
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":4266
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods14 
      Height          =   1095
      Left            =   7560
      TabIndex        =   7
      Top             =   4080
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":4447
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods15 
      Height          =   1095
      Left            =   7560
      TabIndex        =   8
      Top             =   5280
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":4628
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods6 
      Height          =   1095
      Left            =   3840
      TabIndex        =   9
      Top             =   480
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":4809
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods7 
      Height          =   1095
      Left            =   3840
      TabIndex        =   10
      Top             =   1680
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":49E9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods8 
      Height          =   1095
      Left            =   3840
      TabIndex        =   11
      Top             =   2880
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":4BC9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods9 
      Height          =   1095
      Left            =   3840
      TabIndex        =   12
      Top             =   4080
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":4DA9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods10 
      Height          =   1095
      Left            =   3840
      TabIndex        =   13
      Top             =   5280
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":4F89
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods5 
      Height          =   1095
      Left            =   240
      TabIndex        =   14
      Top             =   5280
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":516A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods1 
      Height          =   1095
      Left            =   240
      TabIndex        =   15
      Top             =   480
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":534A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMods11 
      Height          =   1095
      Left            =   7560
      TabIndex        =   16
      Top             =   480
      Visible         =   0   'False
      Width           =   3495
      _Version        =   131072
      _ExtentX        =   6165
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":552A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   360
      TabIndex        =   18
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Modifiers.frx":570B
   End
   Begin VB.Label lblDiags 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Select Linked Diagnosis"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   240
      TabIndex        =   19
      Top             =   120
      Visible         =   0   'False
      Width           =   3285
   End
   Begin VB.Label lblMods 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      BackStyle       =   0  'Transparent
      Caption         =   "Modifiers"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   240
      TabIndex        =   17
      Top             =   120
      Width           =   1320
   End
End
Attribute VB_Name = "frmModifiers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ModifierOn As Boolean
Public LinkedDiagOn As Boolean
Public Modifiers As String
Public LinkedDiags As String

Private Const MaxLinkedDiags As Integer = 4
Private Const MaxModifierAllowed As Integer = 4
Private CurrentModsIndex As Integer
Private TotalMods As Integer
Private ModifierCnt As Integer

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub ClearMods()
cmdMods1.Visible = False
cmdMods1.BackColor = BaseBackColor
cmdMods1.ForeColor = BaseTextColor
cmdMods2.Visible = False
cmdMods2.BackColor = BaseBackColor
cmdMods2.ForeColor = BaseTextColor
cmdMods3.Visible = False
cmdMods3.BackColor = BaseBackColor
cmdMods3.ForeColor = BaseTextColor
cmdMods4.Visible = False
cmdMods4.BackColor = BaseBackColor
cmdMods4.ForeColor = BaseTextColor
cmdMods5.Visible = False
cmdMods5.BackColor = BaseBackColor
cmdMods5.ForeColor = BaseTextColor
cmdMods6.Visible = False
cmdMods6.BackColor = BaseBackColor
cmdMods6.ForeColor = BaseTextColor
cmdMods7.Visible = False
cmdMods7.BackColor = BaseBackColor
cmdMods7.ForeColor = BaseTextColor
cmdMods8.Visible = False
cmdMods8.BackColor = BaseBackColor
cmdMods8.ForeColor = BaseTextColor
cmdMods9.Visible = False
cmdMods9.BackColor = BaseBackColor
cmdMods9.ForeColor = BaseTextColor
cmdMods10.Visible = False
cmdMods10.BackColor = BaseBackColor
cmdMods10.ForeColor = BaseTextColor
cmdMods11.Visible = False
cmdMods11.BackColor = BaseBackColor
cmdMods11.ForeColor = BaseTextColor
cmdMods12.Visible = False
cmdMods12.BackColor = BaseBackColor
cmdMods12.ForeColor = BaseTextColor
cmdMods13.Visible = False
cmdMods13.BackColor = BaseBackColor
cmdMods13.ForeColor = BaseTextColor
cmdMods14.Visible = False
cmdMods14.BackColor = BaseBackColor
cmdMods14.ForeColor = BaseTextColor
cmdMods15.Visible = False
cmdMods15.BackColor = BaseBackColor
cmdMods15.ForeColor = BaseTextColor
End Sub

Private Sub SetVisibleMods(Display As Boolean)
cmdMods1.Visible = Display
cmdMods2.Visible = Display
cmdMods3.Visible = Display
cmdMods4.Visible = Display
cmdMods5.Visible = Display
cmdMods6.Visible = Display
cmdMods7.Visible = Display
cmdMods8.Visible = Display
cmdMods9.Visible = Display
cmdMods10.Visible = Display
cmdMods11.Visible = Display
cmdMods12.Visible = Display
cmdMods13.Visible = Display
cmdMods14.Visible = Display
cmdMods15.Visible = Display
lblMods.Visible = Display
cmdMoreMods.Visible = Display
End Sub

Private Sub cmdHome_Click()
If (ModifierOn) Then
    Modifiers = ""
ElseIf (LinkedDiagOn) Then
    LinkedDiags = ""
End If
Unload frmModifiers
End Sub

Private Sub cmdMods1_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods1)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods1)
End If
End Sub

Private Sub cmdMods2_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods2)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods2)
End If
End Sub

Private Sub cmdMods3_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods3)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods3)
End If
End Sub

Private Sub cmdMods4_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods4)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods4)
End If
End Sub

Private Sub cmdMods5_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods5)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods5)
End If
End Sub

Private Sub cmdMods6_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods6)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods6)
End If
End Sub

Private Sub cmdMods7_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods7)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods7)
End If
End Sub

Private Sub cmdMods8_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods8)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods8)
End If
End Sub

Private Sub cmdMods9_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods9)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods9)
End If
End Sub

Private Sub cmdMods10_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods10)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods10)
End If
End Sub

Private Sub cmdMods11_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods11)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods11)
End If
End Sub

Private Sub cmdMods12_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods12)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods12)
End If
End Sub

Private Sub cmdMods13_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods13)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods13)
End If
End Sub

Private Sub cmdMods14_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods14)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods14)
End If
End Sub

Private Sub cmdMods15_Click()
If (ModifierOn) Then
    Call TriggerModifier(cmdMods15)
ElseIf (LinkedDiagOn) Then
    Call TriggerDiagnosis(cmdMods15)
End If
End Sub

Private Sub cmdMoreMods_Click()
If (CurrentModsIndex + 1 > TotalMods) Then
    CurrentModsIndex = 1
End If
Call LoadModifiers(False)
End Sub

Private Sub cmdDone_Click()
Unload frmModifiers
End Sub

Public Function LoadLinkedDiags(Init As Boolean) As Boolean
Dim i As Integer
Dim Id As String
Dim Diag As String
Dim AEye As String
Dim ASevA As String
Dim ASevB As String
Dim AHigh As Boolean
Dim TotalDiags As Integer
LoadLinkedDiags = False
If (Init) Then
    CurrentModsIndex = 1
    LinkedDiags = ""
    ModifierCnt = 0
    ModifierOn = False
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = &H9B9626
    BaseTextColor = &HFFFFFF
End If
lblDiags.Visible = True
Call ClearMods
cmdMoreMods.Visible = False
TotalDiags = frmSetProcedures.TotalBilledDiagnosis
For i = 1 To TotalDiags
    If (frmSetProcedures.GetBilledDiagnosis(i, Id, Diag, AEye, ASevA, ASevB, AHigh)) Then
        If (i = 1) Then
            cmdMods1.Text = Diag
            cmdMods1.Visible = True
        ElseIf (i = 2) Then
            cmdMods2.Text = Diag
            cmdMods2.Visible = True
        ElseIf (i = 3) Then
            cmdMods3.Text = Diag
            cmdMods3.Visible = True
        ElseIf (i = 4) Then
            cmdMods4.Text = Diag
            cmdMods4.Visible = True
        ElseIf (i = 5) Then
            cmdMods5.Text = Diag
            cmdMods5.Visible = True
        ElseIf (i = 6) Then
            cmdMods6.Text = Diag
            cmdMods6.Visible = True
        ElseIf (i = 7) Then
            cmdMods7.Text = Diag
            cmdMods7.Visible = True
        ElseIf (i = 8) Then
            cmdMods8.Text = Diag
            cmdMods8.Visible = True
        ElseIf (i = 9) Then
            cmdMods9.Text = Diag
            cmdMods9.Visible = True
        ElseIf (i = 10) Then
            cmdMods10.Text = Diag
            cmdMods10.Visible = True
        End If
    End If
Next i
If (TotalDiags > 0) Then
    LoadLinkedDiags = True
End If
End Function

Private Sub TriggerDiagnosis(AButton As fpBtn)
Dim i As Integer
If (AButton.BackColor = BaseBackColor) Then
    AButton.BackColor = ButtonSelectionColor
    AButton.ForeColor = TextSelectionColor
    If (InStrPS(LinkedDiags, Mid(AButton.Name, Len(AButton.Name), 1)) = 0) Then
        LinkedDiags = LinkedDiags + Mid(AButton.Name, Len(AButton.Name), 1)
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    i = InStrPS(LinkedDiags, Mid(AButton.Name, Len(AButton.Name), 1))
    If (i > 0) Then
        Mid(LinkedDiags, i, 1) = " "
    End If
    Call StripCharacters(LinkedDiags, " ")
End If
End Sub

Public Function LoadModifiers(Init As Boolean) As Boolean
Dim p As Integer
Dim ButtonCnt As Integer
Dim DisplayText As String
Dim Temp1 As String
Dim Temp2 As String
Dim RetCodes As PracticeCodes
Set RetCodes = New PracticeCodes
LoadModifiers = False
If (Init) Then
    CurrentModsIndex = 1
    LinkedDiagOn = False
    ModifierCnt = 0
    Modifiers = Space(MaxModifierAllowed * 2)
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = &H9B9626
    BaseTextColor = &HFFFFFF
End If
Call ClearMods
RetCodes.ReferenceType = "SERVICEMODS"
TotalMods = RetCodes.FindCodeOrderbyAlternate
If (TotalMods > 0) Then
    p = CurrentModsIndex
    ButtonCnt = 1
    While (RetCodes.SelectCode(p)) And (ButtonCnt < 16)
        Temp1 = Left(RetCodes.ReferenceCode, 2)
        Temp2 = Mid(RetCodes.ReferenceCode, 5, Len(RetCodes.ReferenceCode) - 4)
        Call SetButtonMods(ButtonCnt, Trim(Temp1), Trim(Temp2))
        ButtonCnt = ButtonCnt + 1
        p = p + 1
    Wend
    CurrentModsIndex = p
    LoadModifiers = True
    cmdMoreMods.Visible = False
    If (TotalMods > 15) Then
        cmdMoreMods.Visible = True
        If (CurrentModsIndex > 15) And (CurrentModsIndex < TotalMods) Then
            cmdMoreMods.Text = "More Modifiers"
        Else
            cmdMoreMods.Text = "Start Modifier List"
        End If
    End If
    Set RetCodes = Nothing
End If
End Function

Private Sub TriggerModifier(AButton As fpBtn)
Dim i As Integer
If (AButton.BackColor = BaseBackColor) Then
    If (ModifierCnt + 1 <= MaxModifierAllowed) Then
        AButton.BackColor = ButtonSelectionColor
        AButton.ForeColor = TextSelectionColor
        ModifierCnt = ModifierCnt + 1
        Mid(Modifiers, ((ModifierCnt - 1) * 2) + 1, 2) = Trim(AButton.Tag)
    Else
        frmEventMsgs.Header = "Only" + str(MaxModifierAllowed) + " allowed"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
    End If
Else
    AButton.BackColor = BaseBackColor
    AButton.ForeColor = BaseTextColor
    For i = 1 To Len(Modifiers) Step 2
        If (AButton.Tag = Mid(Modifiers, i, 2)) Then
            Mid(Modifiers, i, 2) = "  "
            Exit For
        End If
    Next i
    Call StripCharacters(Modifiers, "  ")
    ModifierCnt = ModifierCnt - 1
    If (ModifierCnt < 1) Then
        ModifierCnt = 1
    End If
End If
End Sub

Private Sub SetButtonMods(Ref As Integer, TheId As String, TheDisplayName As String)
If (Ref = 1) Then
    cmdMods1.Text = TheId + "-" + TheDisplayName
    cmdMods1.Tag = TheId
    cmdMods1.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods1.BackColor = ButtonSelectionColor
        cmdMods1.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 2) Then
    cmdMods2.Text = TheId + "-" + TheDisplayName
    cmdMods2.Tag = TheId
    cmdMods2.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods2.BackColor = ButtonSelectionColor
        cmdMods2.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 3) Then
    cmdMods3.Text = TheId + "-" + TheDisplayName
    cmdMods3.Tag = TheId
    cmdMods3.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods3.BackColor = ButtonSelectionColor
        cmdMods3.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 4) Then
    cmdMods4.Text = TheId + "-" + TheDisplayName
    cmdMods4.Tag = TheId
    cmdMods4.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods4.BackColor = ButtonSelectionColor
        cmdMods4.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 5) Then
    cmdMods5.Text = TheId + "-" + TheDisplayName
    cmdMods5.Tag = TheId
    cmdMods5.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods5.BackColor = ButtonSelectionColor
        cmdMods5.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 6) Then
    cmdMods6.Text = TheId + "-" + TheDisplayName
    cmdMods6.Tag = TheId
    cmdMods6.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods6.BackColor = ButtonSelectionColor
        cmdMods6.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 7) Then
    cmdMods7.Text = TheId + "-" + TheDisplayName
    cmdMods7.Tag = TheId
    cmdMods7.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods7.BackColor = ButtonSelectionColor
        cmdMods7.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 8) Then
    cmdMods8.Text = TheId + "-" + TheDisplayName
    cmdMods8.Tag = TheId
    cmdMods8.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods8.BackColor = ButtonSelectionColor
        cmdMods8.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 9) Then
    cmdMods9.Text = TheId + "-" + TheDisplayName
    cmdMods9.Tag = TheId
    cmdMods9.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods9.BackColor = ButtonSelectionColor
        cmdMods9.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 10) Then
    cmdMods10.Text = TheId + "-" + TheDisplayName
    cmdMods10.Tag = TheId
    cmdMods10.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods10.BackColor = ButtonSelectionColor
        cmdMods10.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 11) Then
    cmdMods11.Text = TheId + "-" + TheDisplayName
    cmdMods11.Tag = TheId
    cmdMods11.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods11.BackColor = ButtonSelectionColor
        cmdMods11.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 12) Then
    cmdMods12.Text = TheId + "-" + TheDisplayName
    cmdMods12.Tag = TheId
    cmdMods12.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods12.BackColor = ButtonSelectionColor
        cmdMods12.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 13) Then
    cmdMods13.Text = TheId + "-" + TheDisplayName
    cmdMods13.Tag = TheId
    cmdMods13.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods13.BackColor = ButtonSelectionColor
        cmdMods13.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 14) Then
    cmdMods14.Text = TheId + "-" + TheDisplayName
    cmdMods14.Tag = TheId
    cmdMods14.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods14.BackColor = ButtonSelectionColor
        cmdMods14.ForeColor = TextSelectionColor
    End If
ElseIf (Ref = 15) Then
    cmdMods15.Text = TheId + "-" + TheDisplayName
    cmdMods15.Tag = TheId
    cmdMods15.Visible = True
    If (IsModifierSet(TheId)) Then
        cmdMods15.BackColor = ButtonSelectionColor
        cmdMods15.ForeColor = TextSelectionColor
    End If
End If
End Sub

Private Function IsModifierSet(TheId As String) As Boolean
Dim i As Integer
IsModifierSet = False
For i = 1 To Len(Modifiers) Step 2
    If (Trim(TheId) <> "") Then
        If (Mid(Modifiers, i, 2) = Trim(TheId)) Then
            IsModifierSet = True
            Exit For
        End If
    End If
Next i
End Function

Public Function FrmClose()
 Call cmdHome_Click
 Unload Me
End Function

