VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmDrugNotes 
   BackColor       =   &H00505050&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   4980
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   9315
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   Picture         =   "DrugNotes.frx":0000
   ScaleHeight     =   4980
   ScaleWidth      =   9315
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstSelect 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4680
      ItemData        =   "DrugNotes.frx":36C9
      Left            =   120
      List            =   "DrugNotes.frx":36CB
      TabIndex        =   5
      Top             =   120
      Visible         =   0   'False
      Width           =   180
   End
   Begin VB.TextBox txtNotes 
      BackColor       =   &H009C8100&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   855
      Index           =   1
      Left            =   120
      MaxLength       =   210
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   2040
      Width           =   7215
   End
   Begin VB.CheckBox chkScript 
      BackColor       =   &H0077742D&
      Caption         =   "Include notes on script"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   615
      Left            =   2040
      TabIndex        =   11
      Top             =   2160
      Width           =   3495
   End
   Begin VB.Frame frAsPrescribed 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   1455
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   8895
      Begin VB.TextBox txtNotes 
         BackColor       =   &H009C8100&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   855
         Index           =   0
         Left            =   1680
         MaxLength       =   140
         MultiLine       =   -1  'True
         TabIndex        =   7
         Top             =   360
         Width           =   5535
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdClear 
         Height          =   975
         Index           =   0
         Left            =   7440
         TabIndex        =   8
         Top             =   360
         Width           =   1515
         _Version        =   131072
         _ExtentX        =   2672
         _ExtentY        =   1720
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   14737632
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "DrugNotes.frx":36CD
      End
      Begin fpBtnAtlLibCtl.fpBtn cmdStdRxNotes 
         Height          =   975
         Left            =   0
         TabIndex        =   9
         Top             =   360
         Width           =   1515
         _Version        =   131072
         _ExtentX        =   2672
         _ExtentY        =   1720
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         GrayAreaColor   =   14737632
         BorderShowDefault=   -1  'True
         ButtonType      =   0
         NoPointerFocus  =   0   'False
         Value           =   0   'False
         GroupID         =   0
         GroupSelect     =   0
         DrawFocusRect   =   2
         DrawFocusRectCell=   -1
         GrayAreaPictureStyle=   0
         Static          =   0   'False
         BackStyle       =   1
         AutoSize        =   0
         AutoSizeOffsetTop=   0
         AutoSizeOffsetBottom=   0
         AutoSizeOffsetLeft=   0
         AutoSizeOffsetRight=   0
         DropShadowOffsetX=   3
         DropShadowOffsetY=   3
         DropShadowType  =   0
         DropShadowColor =   0
         Redraw          =   -1  'True
         ButtonDesigner  =   "DrugNotes.frx":3901
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackColor       =   &H0077742D&
         BackStyle       =   0  'Transparent
         Caption         =   "As prescribed (limit 140 characters)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   0
         Left            =   240
         TabIndex        =   10
         Top             =   0
         Width           =   3315
      End
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   240
      TabIndex        =   0
      Top             =   3600
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14734787
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrugNotes.frx":3B41
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   7560
      TabIndex        =   1
      Top             =   3720
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrugNotes.frx":3D76
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   975
      Index           =   1
      Left            =   7560
      TabIndex        =   3
      Top             =   2040
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DrugNotes.frx":3FA9
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Note to pharmacist (limit 210 characters)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   1
      Left            =   360
      TabIndex        =   4
      Top             =   1680
      Width           =   3810
   End
End
Attribute VB_Name = "frmDrugNotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public NotesSystem As String
Public PatientId As Long
Public AppointmentId As Long
'Public CurrentAction As String
'Public SystemReference As String
'Public EyeContext As String
'Public MaintainOn As Boolean
'Public NoteOn As Boolean
'Public NoteId As Long
'Public SetTo As String

Private TheNoteId(1) As Long
Private NotesDate(1) As Long

Private Sub cmdClear_Click(Index As Integer)
txtNotes(Index).Text = ""
End Sub

Public Function LoadDrugNotes() As Boolean

Dim RetrieveNotes As New PatientNotes
Dim i As Integer
For i = 0 To 1
    TheNoteId(i) = 0
    NotesDate(i) = 0
    txtNotes(i) = ""
Next
'chkScript.Value = 0
chkScript.Value = 1

RetrieveNotes.NotesPatientId = PatientId
RetrieveNotes.NotesSystem = NotesSystem
RetrieveNotes.NotesType = "R"
If (RetrieveNotes.FindNotesbyPatient > 0) Then
    i = 1
    While (RetrieveNotes.SelectNotes(i))
        TheNoteId(0) = RetrieveNotes.NotesId
        NotesDate(0) = RetrieveNotes.NotesDate
        If InStr(1, txtNotes(0).Text, RetrieveNotes.NotesText1) < 1 Then
            txtNotes(0).Text = txtNotes(0).Text & RetrieveNotes.NotesText1
        End If
'        If RetrieveNotes.NotesCommentOn Then
            chkScript.Value = 1
'        Else
'            chkScript.Value = 0
'        End If
        i = i + 1
    Wend
End If

RetrieveNotes.NotesPatientId = PatientId
RetrieveNotes.NotesSystem = NotesSystem
RetrieveNotes.NotesType = "X"
If (RetrieveNotes.FindNotesbyPatient > 0) Then
    i = 1
    While (RetrieveNotes.SelectNotes(i))
        TheNoteId(1) = RetrieveNotes.NotesId
        NotesDate(1) = RetrieveNotes.NotesDate
         If InStr(1, txtNotes(1).Text, RetrieveNotes.NotesText1) < 1 Then
            txtNotes(1).Text = txtNotes(1).Text & RetrieveNotes.NotesText1
        End If
        i = i + 1
    Wend
End If
Set RetrieveNotes = Nothing

End Function


Public Sub cmdDone_Click()

'If Not txtNotes(0) = "" And chkScript.Value = 0 Then
'        'frmEventMsgs.Header = "Include note on script?"
'        frmEventMsgs.Header = "Include as prescribed instructions on script"
'        frmEventMsgs.AcceptText = ""
'        frmEventMsgs.RejectText = "Yes"
'        frmEventMsgs.CancelText = "No"
'        frmEventMsgs.Other0Text = ""
'        frmEventMsgs.Other1Text = ""
'        frmEventMsgs.Other2Text = ""
'        frmEventMsgs.Other3Text = ""
'        frmEventMsgs.Other4Text = ""
'        frmEventMsgs.Show 1
'        If (frmEventMsgs.Result = 2) Then
'            chkScript.Value = 1
'        End If
'End If
'
Dim RetrieveNotes As New PatientNotes
Dim NInd As Integer
Dim UserName As String
Dim RetrieveResource As New SchedulerResource

If UserLogin.iId = 0 Then
    RetrieveResource.ResourceId = 3
Else
    RetrieveResource.ResourceId = UserLogin.iId
End If
If (RetrieveResource.RetrieveSchedulerResource) Then
    UserName = Trim(RetrieveResource.ResourceName)
End If
Set RetrieveResource = Nothing


For NInd = 0 To 1
    RetrieveNotes.NotesId = TheNoteId(NInd)
    
    If RetrieveNotes.RetrieveNotes Then
        If txtNotes(NInd).Text = "" Then
            If RetrieveNotes.NotesId > 0 Then
                Call RetrieveNotes.DeleteNotes
            End If
        Else
            If Not RetrieveNotes.NotesId > 0 Then
                RetrieveNotes.NotesPatientId = PatientId
                RetrieveNotes.NotesAppointmentId = AppointmentId
                If NInd = 0 Then
                    RetrieveNotes.NotesType = "R"
                Else
                    RetrieveNotes.NotesType = "X"
                End If
                RetrieveNotes.NotesUser = UserName
                RetrieveNotes.NotesDate = Format(Now, "yyyymmdd")
                RetrieveNotes.NotesSystem = NotesSystem
                RetrieveNotes.NotesAlertMask = String(32, "0")
                RetrieveNotes.NotesAudioOn = False
                RetrieveNotes.NotesClaimOn = False
            Else
                If RetrieveNotes.NotesAppointmentId <> AppointmentId Then
                    RetrieveNotes.NotesPatientId = PatientId
                    RetrieveNotes.NotesAppointmentId = AppointmentId
                    If NInd = 0 Then
                        RetrieveNotes.NotesType = "R"
                    Else
                        RetrieveNotes.NotesType = "X"
                    End If
                    RetrieveNotes.NotesSystem = NotesSystem
                    If RetrieveNotes.FindNotes < 1 Then
                        RetrieveNotes.NotesPatientId = PatientId
                        RetrieveNotes.NotesAppointmentId = AppointmentId
                        If NInd = 0 Then
                            RetrieveNotes.NotesType = "R"
                        Else
                            RetrieveNotes.NotesType = "X"
                        End If
                        RetrieveNotes.NotesUser = UserName
                        RetrieveNotes.NotesDate = Format(Now, "yyyymmdd")
                        RetrieveNotes.NotesSystem = NotesSystem
                        RetrieveNotes.NotesSystem = NotesSystem
                        RetrieveNotes.NotesAlertMask = String(32, "0")
                        RetrieveNotes.NotesAudioOn = False
                        RetrieveNotes.NotesClaimOn = False
                    End If
                End If
                
            End If
            
            RetrieveNotes.NotesText1 = txtNotes(NInd).Text
            'If NInd = 0 Then
                RetrieveNotes.NotesCommentOn = True 'Not (chkScript.Value = 0)
            'Else
            '    RetrieveNotes.NotesCommentOn = False
            'End If
            
            If Not RetrieveNotes.ApplyNotes Then
                Call PinpointError("frmNotes-Done", "Update Failed")
            End If
        End If
   End If
Next

Set RetrieveNotes = Nothing
Unload Me
End Sub

Private Sub cmdHome_Click()
Unload Me
End Sub

Private Sub cmdStdRxNotes_Click()
Call LoadSelectionList("RXNOTES", lstSelect, True, "")
If (lstSelect.ListCount > 0) Then
    lstSelect.Visible = True
End If
End Sub

Private Sub Form_Load()
lstSelect.Width = 9060
End Sub

Private Sub lstSelect_Click()
Dim NoteFile As String
Dim Rec As String

If lstSelect.ListIndex > 0 Then
    NoteFile = ImpressionNotesDirectory + Trim(lstSelect.Text) + ".txt"
    If Not (FM.IsFileThere(NoteFile)) Then
        txtNotes(0).Text = lstSelect.Text
    Else
        txtNotes(0).Text = ""
        FM.OpenFile NoteFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(101)
        While Not (EOF(101))
            Line Input #101, Rec
            If (Trim(Rec) <> "") Then
                txtNotes(0).Text = Trim(Rec) + vbCrLf + txtNotes(0).Text
            End If
        Wend
        FM.CloseFile CLng(101)
    End If
End If
lstSelect.Visible = False
End Sub

Private Sub txtNotes_Change(Index As Integer)
If Index = 0 Then
    frmRxDetails.moDrugRx.AsPrescribed = Not (Trim(txtNotes(0) = ""))
End If
'If Index = 0 Then
'    If txtNotes(0) = "" Then
'        chkScript.Value = 0
'    End If
'    chkScript.Enabled = Not (txtNotes(0) = "")
'End If
End Sub
Public Function FrmClose()
 Call cmdHome_Click
 Unload Me
End Function

