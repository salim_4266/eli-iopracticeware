VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmDIListFacAdm 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9405
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12360
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "DIListFacAdm.frx":0000
   ScaleHeight     =   9405
   ScaleWidth      =   12360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstTests 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "DIListFacAdm.frx":36C9
      Left            =   3600
      List            =   "DIListFacAdm.frx":36CB
      TabIndex        =   12
      Top             =   120
      Width           =   2655
   End
   Begin VB.ListBox lstPrint 
      Height          =   450
      ItemData        =   "DIListFacAdm.frx":36CD
      Left            =   120
      List            =   "DIListFacAdm.frx":36CF
      Sorted          =   -1  'True
      TabIndex        =   11
      Top             =   8040
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid lstDocs 
      Height          =   5655
      Left            =   120
      TabIndex        =   2
      Top             =   1680
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   9975
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   16250357
      GridColor       =   8388608
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "DIListFacAdm.frx":36D1
      Left            =   6360
      List            =   "DIListFacAdm.frx":36D3
      TabIndex        =   6
      Top             =   120
      Width           =   2055
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "DIListFacAdm.frx":36D5
      Left            =   8520
      List            =   "DIListFacAdm.frx":36D7
      TabIndex        =   4
      Top             =   120
      Width           =   1575
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      ItemData        =   "DIListFacAdm.frx":36D9
      Left            =   10200
      List            =   "DIListFacAdm.frx":36DB
      TabIndex        =   3
      Top             =   120
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDisplay 
      Height          =   1095
      Left            =   5520
      TabIndex        =   0
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DIListFacAdm.frx":36DD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   1110
      Left            =   10320
      TabIndex        =   1
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1958
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DIListFacAdm.frx":38C5
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   720
      TabIndex        =   5
      Top             =   720
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   0
      BackColor       =   16250357
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      ForeColorSelected=   0
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo2 
      Height          =   375
      Left            =   720
      TabIndex        =   9
      Top             =   1200
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   0
      BackColor       =   16250357
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      ForeColorSelected=   0
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAny 
      Height          =   1095
      Left            =   3840
      TabIndex        =   13
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DIListFacAdm.frx":3AA0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdAged 
      Height          =   1095
      Left            =   2160
      TabIndex        =   15
      Top             =   7680
      Width           =   1575
      _Version        =   131072
      _ExtentX        =   2778
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "DIListFacAdm.frx":3C88
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   14
      Top             =   720
      Width           =   450
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   120
      TabIndex        =   10
      Top             =   1200
      Width           =   210
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Surgeries"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   1230
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   120
      TabIndex        =   7
      Top             =   7680
      Visible         =   0   'False
      Width           =   780
   End
End
Attribute VB_Name = "frmDIListFacAdm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public FormOn As Boolean
Private DoAll As Boolean
Private AnyOn As Boolean
Private ZAccessOn As Boolean
Private ResourceId As Long
Private ResourceLocId As Long
Private TheTest As String
Private TheStatus As String
Private CurrentRow As Integer
Private m_SortColumnName As String
Private m_SortColumn As Integer
Private m_SortOrder As SortSettings
Public patientRunsUsed As Long
Dim pat As New Patient



Private Sub cmdAged_Click()
AnyOn = True
ZAccessOn = True
Call LoadFacilityAdminList(True, AnyOn, ZAccessOn)
End Sub

Private Sub cmdAny_Click()
AnyOn = True
ZAccessOn = False
Call LoadFacilityAdminList(True, AnyOn, ZAccessOn)
End Sub

Private Sub cmdDisplay_Click()
AnyOn = False
Call LoadFacilityAdminList(False, AnyOn, ZAccessOn)
End Sub

Private Sub cmdDone_Click()
Unload frmDIListFacAdm
FormOn = False
End Sub

Public Function LoadFacilityAdminList(InitOn As Boolean, AnyOn As Boolean, ZOn As Boolean) As Boolean
Dim p As Integer
Dim i As Integer
Dim m As Integer
Dim Temp As String
Dim ADate As String
Dim CleanOn As Boolean
Dim MyType As Integer
Dim DateNow As String
Dim ApplList As DI_ListFacAdm
FormOn = True
m_SortColumn = 0
LoadFacilityAdminList = False
ADate = SSDateCombo1.DateValue
Call FormatTodaysDate(ADate, False)
ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
DateNow = SSDateCombo2.DateValue
Call FormatTodaysDate(DateNow, False)
DateNow = Mid(DateNow, 7, 4) + Mid(DateNow, 1, 2) + Mid(DateNow, 4, 2)
MyType = 0
If (InitOn) Or (AnyOn) Then
    If (AnyOn) Then
        lstLoc.ListIndex = 0
        lstDr.ListIndex = 0
        lstStatus.ListIndex = 0
        lstTests.ListIndex = 0
        ResourceId = -1
        ResourceLocId = -1
        TheStatus = "0"
        ADate = ""
        DateNow = ""
        MyType = 1
        If (ZOn) Then
            MyType = 2
        End If
    Else
        TheTest = ""
        TheStatus = ""
    End If
    If Not (AnyOn) Then
        Set ApplList = New DI_ListFacAdm
        Set ApplList.ApplList = lstDr
        Call ApplList.ApplLoadStaff(False)
        Set ApplList = Nothing
        ResourceId = -1
        Set ApplList = New DI_ListFacAdm
        Set ApplList.ApplList = lstLoc
        Call ApplList.ApplLoadLocation(False, False)
        Set ApplList = Nothing
        ResourceLocId = -1
        Set ApplList = New DI_ListFacAdm
        Call ApplList.ApplGetCodesbyList("SURGERYSTATUS", False, True, lstStatus)
        lstStatus.List(0) = "All Status"
        Call ApplList.ApplGetCodesbyList("SURGERYTYPE", False, True, lstTests)
        lstTests.List(0) = "All Surgeries"
        Set ApplList = Nothing
    End If
End If
Set ApplList = New DI_ListFacAdm
ApplList.ApplStartDate = ADate
ApplList.ApplEndDate = DateNow
ApplList.ApplResourceId = ResourceId
ApplList.ApplLocId = ResourceLocId
ApplList.ApplConfirmStat = TheStatus
ApplList.ApplOtherFilter = TheTest
Set ApplList.ApplGrid = lstDocs
Call ApplList.ApplRetrieveFacilityAdminsList(MyType)
Set ApplList = Nothing
' Clear any empty cells
m = lstDocs.Rows - 1
For i = 1 To m
    CleanOn = True
    For p = 1 To 10
        If (lstDocs.TextMatrix(i, p) <> "") Then
            CleanOn = False
            Exit For
        End If
    Next p
    If (CleanOn) Then
        If (i > 1) Then
            lstDocs.RemoveItem i
            If (i <> m) Then
                i = i - 1
                m = m - 1
            End If
        End If
    End If
Next i
LoadFacilityAdminList = True
End Function

Private Sub Form_Load()
DoAll = False
ZAccessOn = False
m_SortColumnName = ""
End Sub

Private Sub SortByColumn(ByVal sort_column As Integer)
Dim i As Integer
Dim Temp As String
If (sort_column > 0) Then
    lstDocs.Visible = False
    lstDocs.Refresh
    If (InStrPS(lstDocs.TextMatrix(0, sort_column), "Date") > 0) Then
        For i = 1 To lstDocs.Rows - 1
            Temp = lstDocs.TextMatrix(i, sort_column)
            If (Trim(Temp) <> "") And (InStrPS(Temp, "/") > 0) Then
                Temp = Mid(Temp, 7, 4) + Mid(Temp, 1, 2) + Mid(Temp, 4, 2)
            End If
            lstDocs.TextMatrix(i, sort_column) = Temp
        Next i
    End If
' Sort using the clicked column.
    lstDocs.col = sort_column
    lstDocs.ColSel = sort_column
    lstDocs.Row = 0
    lstDocs.RowSel = 0
' If this is a new sort column, sort ascending.
' Otherwise switch which sort order we use.
    If m_SortColumn <> sort_column Then
        m_SortOrder = flexSortGenericAscending
    ElseIf m_SortOrder = flexSortGenericAscending Then
        m_SortOrder = flexSortGenericDescending
    Else
        m_SortOrder = flexSortGenericAscending
    End If
    lstDocs.Sort = m_SortOrder

' Restore the previous sort column's name.
    If (m_SortColumn <> sort_column) And (m_SortColumn > 0) Then
        If (m_SortColumnName <> "") Then
            lstDocs.TextMatrix(0, m_SortColumn) = m_SortColumnName
        End If
        m_SortColumnName = lstDocs.TextMatrix(0, sort_column)
    ElseIf (Trim(m_SortColumnName) = "") Then
        m_SortColumnName = lstDocs.TextMatrix(0, sort_column)
    End If

' Display the new sort column's name.
    m_SortColumn = sort_column
    If m_SortOrder = flexSortGenericAscending Then
        lstDocs.TextMatrix(0, m_SortColumn) = ">" & m_SortColumnName
    Else
        lstDocs.TextMatrix(0, m_SortColumn) = "<" & m_SortColumnName
    End If
    If (InStrPS(m_SortColumnName, "Date") > 0) Then
        For i = 1 To lstDocs.Rows - 1
            Temp = lstDocs.TextMatrix(i, sort_column)
            If (Trim(Temp) <> "") And (IsNumeric(Temp)) Then
                Temp = Mid(Temp, 5, 2) + "/" + Mid(Temp, 7, 2) + "/" + Left(Temp, 4)
                lstDocs.TextMatrix(i, sort_column) = Temp
            End If
        Next i
    End If
    lstDocs.Visible = True
End If
End Sub

Private Sub lstDocs_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    ' If this is not row 0, do nothing.
    If lstDocs.MouseRow <> 0 Then Exit Sub
    ' Sort by the clicked column.
    SortByColumn lstDocs.MouseCol
End Sub

Private Sub lstDocs_Click()
Dim i As Integer
Dim PatId As Long
CurrentRow = lstDocs.RowSel
i = lstDocs.ColSel
If (CurrentRow > 0) Then
    If (i = 0) Then
        If (Trim(lstDocs.TextMatrix(CurrentRow, 0)) = "") Then
            lstDocs.TextMatrix(CurrentRow, 0) = "Y"
        Else
            lstDocs.TextMatrix(CurrentRow, 0) = ""
        End If
    Else
        If (i = 3) Then
            PatId = Val(lstDocs.TextMatrix(CurrentRow, 3))
            If (PatId > 0) Then
                Dim PatientDemographics As New PatientDemographics
                PatientDemographics.PatientId = PatId
                Call PatientDemographics.DisplayPatientInfoScreen
                Set PatientDemographics = Nothing
            End If
        Else
            lstDocs.TextMatrix(CurrentRow, 0) = "Y"
        End If
    End If
End If
End Sub

Private Sub lstDocs_DblClick()
Dim i As Integer
Dim Cnt As Integer
Dim ActId As Long
Dim PatId As Long, ApptId As Long
Dim Temp As String
Dim MyCheck As Boolean
CurrentRow = lstDocs.RowSel
If (CurrentRow > 0) Then
    PatId = Val(lstDocs.TextMatrix(CurrentRow, 3))
    If (AnyOn) Then
        ApptId = Val(lstDocs.TextMatrix(CurrentRow, 9))
    Else
        ApptId = Val(lstDocs.TextMatrix(CurrentRow, 10))
    End If
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Go To Edit Previous"
    frmEventMsgs.RejectText = "Set Status"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Edit Surgical Request"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
     If Not UserLogin.HasPermission(epEditPreviousVisit) Then
            frmEventMsgs.Header = "Not Permissioned"
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "Ok"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            Exit Sub
            End If
        If (ApptId > 0) Then
            ActId = ApplGetActivityId(ApptId, MyCheck)
            If (ActId > 0) Then
    
           patientRunsUsed = pat.GetPatientAccess(PatId)
           If patientRunsUsed = -1 Then
            frmEventMsgs.Header = " Access Blocked"
                 frmEventMsgs.AcceptText = ""
                 frmEventMsgs.RejectText = "Ok"
                 frmEventMsgs.CancelText = ""
                 frmEventMsgs.Other0Text = ""
                 frmEventMsgs.Other1Text = ""
                 frmEventMsgs.Other2Text = ""
                 frmEventMsgs.Other3Text = ""
                 frmEventMsgs.Other4Text = ""
                 frmEventMsgs.Show 1
                 'Call TurnOn
                 Exit Sub
              Else
            Call SelectSystems(ActId)
                        
             If patientRunsUsed = 1 Then
            Call pat.GetPatientUpdate(PatId, pat.runscmplted)
            End If
            End If
            Else
                frmEventMsgs.Header = "Patient has an active appointment, close it before editing"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            End If
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        Cnt = 0
        For i = 1 To lstDocs.Rows - 1
            If (lstDocs.TextMatrix(i, 0) = "Y") Then
                Cnt = Cnt + 1
            End If
        Next i
        If (Cnt > 1) Then
            Temp = ""
            frmEventMsgs.Header = "Set Status"
            frmEventMsgs.AcceptText = "Last Selected"
            frmEventMsgs.RejectText = "All Selected"
            frmEventMsgs.CancelText = "Cancel"
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
            If (frmEventMsgs.Result = 1) Then
                Call MaintainStatus(CurrentRow, Temp)
                Call LoadFacilityAdminList(False, AnyOn, ZAccessOn)
            ElseIf (frmEventMsgs.Result = 2) Then
                For i = 1 To lstDocs.Rows - 1
                    If (lstDocs.TextMatrix(i, 0) = "Y") Then
                        Call MaintainStatus(i, Temp)
                        lstDocs.TextMatrix(i, 0) = " "
                    End If
                Next i
                Call LoadFacilityAdminList(False, AnyOn, ZAccessOn)
            End If
        Else
            Call MaintainStatus(CurrentRow, Temp)
            Call LoadFacilityAdminList(False, AnyOn, ZAccessOn)
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        Call SelectSurgical(CurrentRow)
        Call LoadFacilityAdminList(False, AnyOn, ZAccessOn)
    End If
End If
End Sub

Private Sub lstTests_Click()
If (lstTests.ListIndex >= 0) Then
    If (TheTest = Trim(lstTests.List(lstTests.ListIndex))) Or (lstTests.ListIndex = 0) Then
        TheTest = ""
    Else
        TheTest = UCase(Trim(lstTests.List(lstTests.ListIndex)))
    End If
End If
End Sub

Private Sub lstLoc_Click()
Dim ApplList As DI_ListFacAdm
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        ResourceLocId = -1
    Else
        Set ApplList = New DI_ListFacAdm
        ResourceLocId = ApplList.ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
        Set ApplList = Nothing
    End If
End If
End Sub

Private Sub lstDr_Click()
Dim ApplList As DI_ListFacAdm
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        ResourceId = -1
    Else
        Set ApplList = New DI_ListFacAdm
        ResourceId = ApplList.ApplGetListResourceId(lstDr.List(lstDr.ListIndex))
        Set ApplList = Nothing
    End If
End If
End Sub

Private Sub lstStatus_Click()
If (lstStatus.ListIndex >= 0) Then
    If (TheStatus = Left(lstStatus.List(lstStatus.ListIndex), 1)) Or (lstStatus.ListIndex = 0) Then
        TheStatus = ""
    Else
        TheStatus = Left(lstStatus.List(lstStatus.ListIndex), 1)
    End If
End If
End Sub

Private Function MaintainStatus(Idx As Integer, IType As String) As Boolean
Dim ClnId As Long
Dim Temp As String
Dim RetCln As PatientClinical
MaintainStatus = False
If (IType = "") Then
    frmSelectDialogue.InsurerSelected = ""
    Call frmSelectDialogue.BuildSelectionDialogue("SURGERYSTATUS")
    frmSelectDialogue.Show 1
    If (frmSelectDialogue.SelectionResult) Then
        IType = Left(frmSelectDialogue.Selection, 1)
    End If
End If
If (Trim(IType) <> "") Then
    ClnId = Val(lstDocs.TextMatrix(Idx, 11))
    If (ClnId > 0) Then
        Set RetCln = New PatientClinical
        RetCln.ClinicalId = ClnId
        If (RetCln.RetrievePatientClinical) Then
            RetCln.ClinicalSurgery = IType
            MaintainStatus = RetCln.ApplyPatientClinical
        End If
        Set RetCln = Nothing
    End If
End If
End Function

Private Function SelectSurgical(Idx As Integer) As Boolean
Dim i As Integer, j As Integer
Dim PatId As Long, ApptId As Long, TransId As Long
Dim ARef As String, ARem As String, AEye As String
Dim ALoc As String, ADate As String, AName As String
SelectSurgical = False
If (Idx > 0) Then
    PatId = Val(lstDocs.TextMatrix(Idx, 3))
    ApptId = Val(lstDocs.TextMatrix(Idx, 9))
    If Not (AnyOn) Then
        ApptId = Val(lstDocs.TextMatrix(Idx, 10))
    End If
    TransId = Val(lstDocs.TextMatrix(Idx, 11))
    ARem = Trim(lstDocs.TextMatrix(Idx, 5))
    AEye = Mid(ARem, Len(ARem) - 2, 2)
    If (AEye <> "OD") And (AEye <> "OS") And (AEye <> "OU") Then
        AEye = ""
    End If
    ARem = Trim(Left(ARem, Len(ARem) - 4))
    ADate = Trim(lstDocs.TextMatrix(Idx, 6))
    ALoc = Trim(lstDocs.TextMatrix(Idx, 8))
    frmSurgical.ClinicalId = TransId
    frmSurgical.PatientId = PatId
    frmSurgical.AppointmentId = ApptId
    globalExam.SetExamIds PatId, ApptId, ApplGetActivityId(ApptId, False)
    frmSurgical.TheProcs = ARem
    frmSurgical.TheLoc = ALoc
    frmSurgical.TheDate = ADate
    frmSurgical.TheEye = AEye
    frmSurgical.Show 1
    SelectSurgical = True
    DoEvents
    DoEvents
End If
End Function

Private Function ApplGetActivityId(ApptId As Long, CheckedOut As Boolean) As Long
Dim PatId As Long
Dim DFile As String
Dim RetAct As PracticeActivity
ApplGetActivityId = 0
CheckedOut = True
If (ApptId > 0) Then
    Set RetAct = New PracticeActivity
    RetAct.ActivityAppointmentId = ApptId
    RetAct.ActivityLocationId = -1
    RetAct.ActivityStatus = ""
    If (RetAct.FindActivity > 0) Then
        If (RetAct.SelectActivity(1)) Then
            PatId = RetAct.ActivityPatientId
            If (RetAct.ActivityStatus = "H") Or (RetAct.ActivityStatus = "D") Then
                DFile = Dir(DoctorInterfaceDirectory + "*" + "_" + Trim(str(ApptId)) + "_" + Trim(str(PatId)) + ".txt")
                If (DFile <> "") Then
                    ApplGetActivityId = 0
                Else
                    ApplGetActivityId = RetAct.ActivityId
                End If
                If (RetAct.ActivityStatus = "H") Then
                    CheckedOut = False
                End If
            End If
        End If
    End If
    Set RetAct = Nothing
End If
End Function

Private Sub SelectSystems(ActId As Long)
If (ActId > 0) Then
    frmSystems1.TheStationId = 999
    globalExam.SetExamIds 0, 0, ActId
    frmSystems1.EditPreviousOn = True
    frmDIListFacAdm.Enabled = False
    DoEvents
    If (frmSystems1.LoadSummaryInfo(True)) Then
        frmSystems1.FormOn = True
        frmSystems1.Show 1
        frmDIListFacAdm.ZOrder 0
    End If
    frmDIListFacAdm.Enabled = True
End If
End Sub
Public Function FrmClose()
Call cmdDone_Click
 Unload Me
End Function
