VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmSetEMProcedure 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   Picture         =   "SetEMProcedure.frx":0000
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstEM 
      Height          =   1035
      Left            =   120
      TabIndex        =   27
      Top             =   1080
      Visible         =   0   'False
      Width           =   975
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure11 
      Height          =   1095
      Left            =   7800
      TabIndex        =   6
      Top             =   480
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdReviewSystems 
      Height          =   975
      Left            =   8400
      TabIndex        =   13
      Top             =   7920
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":38A9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   975
      Left            =   240
      TabIndex        =   14
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":3AE3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNotes 
      Height          =   975
      Left            =   1920
      TabIndex        =   15
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":3D16
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   10080
      TabIndex        =   16
      Top             =   7920
      Width           =   1635
      _Version        =   131072
      _ExtentX        =   2884
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":3F4A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure9 
      Height          =   1095
      Left            =   3960
      TabIndex        =   11
      Top             =   4080
      Visible         =   0   'False
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":417D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure10 
      Height          =   1095
      Left            =   3960
      TabIndex        =   12
      Top             =   5280
      Visible         =   0   'False
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":435C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure12 
      Height          =   1095
      Left            =   7800
      TabIndex        =   7
      Top             =   1680
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":453C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure13 
      Height          =   1095
      Left            =   7800
      TabIndex        =   8
      Top             =   2880
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":471C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure14 
      Height          =   1095
      Left            =   7800
      TabIndex        =   9
      Top             =   4080
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":48FC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure8 
      Height          =   1095
      Left            =   3960
      TabIndex        =   0
      Top             =   2880
      Visible         =   0   'False
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":4ADC
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure5 
      Height          =   1095
      Left            =   240
      TabIndex        =   18
      Top             =   5280
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":4CBB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure15 
      Height          =   1095
      Left            =   7800
      TabIndex        =   10
      Top             =   5280
      Visible         =   0   'False
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":4E9A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure6 
      Height          =   1095
      Left            =   3960
      TabIndex        =   1
      Top             =   480
      Visible         =   0   'False
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":507A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure7 
      Height          =   1095
      Left            =   3960
      TabIndex        =   19
      Top             =   1680
      Visible         =   0   'False
      Width           =   3735
      _Version        =   131072
      _ExtentX        =   6588
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":5259
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   1095
      Left            =   6000
      TabIndex        =   24
      Top             =   6480
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":5438
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   1095
      Left            =   3960
      TabIndex        =   25
      Top             =   6480
      Width           =   1695
      _Version        =   131072
      _ExtentX        =   2990
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":561B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdMoreCPTs 
      Height          =   1095
      Left            =   7800
      TabIndex        =   17
      Top             =   6480
      Width           =   3975
      _Version        =   131072
      _ExtentX        =   7011
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":57FE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdCheckOut 
      Height          =   975
      Left            =   6720
      TabIndex        =   26
      Top             =   7920
      Width           =   1605
      _Version        =   131072
      _ExtentX        =   2831
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":59E2
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure4 
      Height          =   1095
      Left            =   240
      TabIndex        =   5
      Top             =   4080
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":5C22
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure3 
      Height          =   1095
      Left            =   240
      TabIndex        =   4
      Top             =   2880
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":5E01
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure2 
      Height          =   1095
      Left            =   240
      TabIndex        =   3
      Top             =   1680
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":5FE0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdProcedure1 
      Height          =   1095
      Left            =   240
      TabIndex        =   2
      Top             =   480
      Visible         =   0   'False
      Width           =   3615
      _Version        =   131072
      _ExtentX        =   6376
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":61BF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHigh 
      Height          =   975
      Left            =   5160
      TabIndex        =   29
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":639E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSumm 
      Height          =   975
      Left            =   3600
      TabIndex        =   32
      Top             =   7920
      Width           =   1515
      _Version        =   131072
      _ExtentX        =   2672
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   14737632
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetEMProcedure.frx":65D6
   End
   Begin VB.Label lblPostOp 
      Alignment       =   2  'Center
      BackColor       =   &H00F7F5F5&
      Caption         =   "In Post-Op period"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   5040
      TabIndex        =   31
      Top             =   7560
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblAlert 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      Caption         =   "Alert ON"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   5760
      TabIndex        =   30
      Top             =   120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblIns 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1095
      Left            =   240
      TabIndex        =   28
      Top             =   6480
      Width           =   3615
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblRef 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Evaluation and  Management"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   7800
      TabIndex        =   23
      Top             =   120
      Visible         =   0   'False
      Width           =   4035
   End
   Begin VB.Label lblProcs 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "E+M Codes and Ophthalmic Codes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   360
      TabIndex        =   20
      Top             =   120
      Width           =   4815
   End
   Begin VB.Label lblDiags 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      BackStyle       =   0  'Transparent
      Caption         =   "Link Diagnosis"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   360
      TabIndex        =   22
      Top             =   120
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblMods 
      AutoSize        =   -1  'True
      BackColor       =   &H00A95911&
      BackStyle       =   0  'Transparent
      Caption         =   "Modifiers"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   360
      TabIndex        =   21
      Top             =   120
      Visible         =   0   'False
      Width           =   1320
   End
End
Attribute VB_Name = "frmSetEMProcedure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Public AppointmentId As Long
Public NavigationAction As Long
Public InsurerStuff As String
Public DiagMode As Boolean

Private Type ProcedureEM
    Name As String
    Id As Long
End Type

Private ModifierCnt As Integer
Private InsurerId As Long
Private TriggerOn As Boolean
Private BusLogOn As Boolean
Private NewDiagOn As Boolean
Private NewRxOn As Boolean
Private RetainModifiers As Boolean
Private EMBasis As String
Private LinkedDiag As String
Private TotalMods As Integer
Private ProcedureEM As String
Private ProcedureModifier(4) As String
Private ProcedureLink As String
Private ProcedureHighlight As Boolean
Private MaxEMProcedures As Integer
Private CurrentIndex As Integer
Private CurrentCPTIndex As Integer
Private CurrentModsIndex As Integer
Private EMProcs(130) As ProcedureEM

Private ButtonSelectionColor As Long
Private TextSelectionColor As Long
Private BaseBackColor As Long
Private BaseTextColor As Long

Private Sub TurnCPTs(IType As Boolean)
cmdProcedure1.Enabled = IType
cmdProcedure2.Enabled = IType
cmdProcedure3.Enabled = IType
cmdProcedure4.Enabled = IType
cmdProcedure5.Enabled = IType
cmdProcedure6.Enabled = IType
cmdProcedure7.Enabled = IType
cmdProcedure8.Enabled = IType
cmdProcedure9.Enabled = IType
cmdProcedure10.Enabled = IType
cmdProcedure11.Enabled = IType
cmdProcedure12.Enabled = IType
cmdProcedure13.Enabled = IType
cmdProcedure14.Enabled = IType
cmdProcedure15.Enabled = IType
End Sub

Private Sub ClearCPTs()
cmdProcedure1.Text = ""
cmdProcedure1.Visible = False
cmdProcedure1.BackColor = BaseBackColor
cmdProcedure1.ForeColor = BaseTextColor
cmdProcedure2.Text = ""
cmdProcedure2.Visible = False
cmdProcedure2.BackColor = BaseBackColor
cmdProcedure2.ForeColor = BaseTextColor
cmdProcedure3.Text = ""
cmdProcedure3.Visible = False
cmdProcedure3.BackColor = BaseBackColor
cmdProcedure3.ForeColor = BaseTextColor
cmdProcedure4.Text = ""
cmdProcedure4.Visible = False
cmdProcedure4.BackColor = BaseBackColor
cmdProcedure4.ForeColor = BaseTextColor
cmdProcedure5.Text = ""
cmdProcedure5.Visible = False
cmdProcedure5.BackColor = BaseBackColor
cmdProcedure5.ForeColor = BaseTextColor
cmdProcedure6.Text = ""
cmdProcedure6.Visible = False
cmdProcedure6.BackColor = BaseBackColor
cmdProcedure6.ForeColor = BaseTextColor
cmdProcedure7.Text = ""
cmdProcedure7.Visible = False
cmdProcedure7.BackColor = BaseBackColor
cmdProcedure7.ForeColor = BaseTextColor
cmdProcedure8.Text = ""
cmdProcedure8.Visible = False
cmdProcedure8.BackColor = BaseBackColor
cmdProcedure8.ForeColor = BaseTextColor
cmdProcedure9.Text = ""
cmdProcedure9.Visible = False
cmdProcedure9.BackColor = BaseBackColor
cmdProcedure9.ForeColor = BaseTextColor
cmdProcedure10.Text = ""
cmdProcedure10.Visible = False
cmdProcedure10.BackColor = BaseBackColor
cmdProcedure10.ForeColor = BaseTextColor
cmdProcedure11.Text = ""
cmdProcedure11.Visible = False
cmdProcedure11.BackColor = BaseBackColor
cmdProcedure11.ForeColor = BaseTextColor
cmdProcedure12.Text = ""
cmdProcedure12.Visible = False
cmdProcedure12.BackColor = BaseBackColor
cmdProcedure12.ForeColor = BaseTextColor
cmdProcedure13.Text = ""
cmdProcedure13.Visible = False
cmdProcedure13.BackColor = BaseBackColor
cmdProcedure13.ForeColor = BaseTextColor
cmdProcedure14.Text = ""
cmdProcedure14.Visible = False
cmdProcedure14.BackColor = BaseBackColor
cmdProcedure14.ForeColor = BaseTextColor
cmdProcedure15.Text = ""
cmdProcedure15.Visible = False
cmdProcedure15.BackColor = BaseBackColor
cmdProcedure15.ForeColor = BaseTextColor
End Sub

Private Sub SetVisibleCPTs(Display As Boolean)
lblProcs.Visible = Display
cmdProcedure1.Visible = Display
cmdProcedure2.Visible = Display
cmdProcedure3.Visible = Display
cmdProcedure4.Visible = Display
cmdProcedure5.Visible = Display
cmdProcedure6.Visible = Display
cmdProcedure7.Visible = Display
cmdProcedure8.Visible = Display
cmdProcedure9.Visible = Display
cmdProcedure10.Visible = Display
cmdProcedure11.Visible = Display
cmdProcedure12.Visible = Display
cmdProcedure13.Visible = Display
cmdProcedure14.Visible = Display
cmdProcedure15.Visible = Display
End Sub

Private Sub cmdCheckOut_Click()
frmEventMsgs.Header = "Express Check-Out WILL CAUSE WORKFLOW PROBLEMS. Are you sure ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Check-Out"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
    NavigationAction = 90
    Unload frmSetEMProcedure
End If
End Sub

Private Sub cmdHigh_Click()
cmdHigh.Enabled = False
frmHighlights.HighlightLoadOn = frmSystems1.IsHighlightLoadOn
If (frmHighlights.LoadHighlights(PatientId, True, True, False)) Then
    frmHighlights.Show 1
    Call frmSystems1.SetHighlightLoad(frmHighlights.HighlightLoadOn)
End If
cmdHigh.Enabled = True
End Sub

Private Sub cmdNext_Click()
Dim i As Integer
Dim Temp As String
CurrentIndex = CurrentIndex + 1
If (CurrentIndex > MaxEMProcedures) Then
    CurrentIndex = 1
End If
Call LoadEMProcs
Temp = ""
For i = 1 To 4
    Temp = Temp + ProcedureModifier(i)
Next i
lblRef.Caption = Temp + " " + ProcedureLink
lblRef.Visible = True
End Sub

Private Sub cmdPrev_Click()
Dim i As Integer
Dim Temp As String
CurrentIndex = CurrentIndex - 30
If (CurrentIndex < 1) Then
    CurrentIndex = 1
End If
Call LoadEMProcs
Temp = ""
For i = 1 To 4
    Temp = Temp + ProcedureModifier(i)
Next i
lblRef.Caption = Temp + " " + ProcedureLink
lblRef.Visible = True
End Sub

Private Sub cmdProcedure1_Click()
Call TriggerProcedure(cmdProcedure1)
End Sub

Private Sub cmdProcedure2_Click()
Call TriggerProcedure(cmdProcedure2)
End Sub

Private Sub cmdProcedure3_Click()
Call TriggerProcedure(cmdProcedure3)
End Sub

Private Sub cmdProcedure4_Click()
Call TriggerProcedure(cmdProcedure4)
End Sub

Private Sub cmdProcedure5_Click()
Call TriggerProcedure(cmdProcedure5)
End Sub

Private Sub cmdProcedure6_Click()
Call TriggerProcedure(cmdProcedure6)
End Sub

Private Sub cmdProcedure7_Click()
Call TriggerProcedure(cmdProcedure7)
End Sub

Private Sub cmdProcedure8_Click()
Call TriggerProcedure(cmdProcedure8)
End Sub

Private Sub cmdProcedure9_Click()
Call TriggerProcedure(cmdProcedure9)
End Sub

Private Sub cmdProcedure10_Click()
Call TriggerProcedure(cmdProcedure10)
End Sub

Private Sub cmdProcedure11_Click()
Call TriggerProcedure(cmdProcedure11)
End Sub

Private Sub cmdProcedure12_Click()
Call TriggerProcedure(cmdProcedure12)
End Sub

Private Sub cmdProcedure13_Click()
Call TriggerProcedure(cmdProcedure13)
End Sub

Private Sub cmdProcedure14_Click()
Call TriggerProcedure(cmdProcedure14)
End Sub

Private Sub cmdProcedure15_Click()
Call TriggerProcedure(cmdProcedure15)
End Sub

Private Sub cmdMoreCPTs_Click()
Call SetVisibleCPTs(True)
cmdMoreCPTs.Visible = False
cmdNext.Visible = True
cmdPrev.Visible = True
End Sub

Private Sub TriggerProcedure(AButton As fpBtn)
Dim i As Integer
Dim RemoveOn As Boolean
Dim ALevel As Integer, OLevel As Integer
Dim ATemp As String
Dim AMod As String, Temp As String
Dim ETemp As String, OTemp As String
If (AButton.BackColor = BaseBackColor) Then
    i = InStrPS(AButton.Text, Chr(13))
    If (i <> 0) Then
        ProcedureEM = Left(AButton.Text, i - 1)
        ATemp = ProcedureEM
        If Not (RetainModifiers) Then
            Erase ProcedureModifier
            ProcedureLink = ""
        End If
        If (TriggerOn) Then
            If (TriggerPostOpTest(ProcedureEM, AMod)) Then
                ProcedureModifier(1) = AMod
                ProcedureModifier(2) = ""
                ProcedureModifier(3) = ""
                ProcedureModifier(4) = ""
                If (ATemp <> "99024") And (ProcedureEM = "99024") Then
                    Call RetainEMProcedure(PatientId)
                    AButton.BackColor = BaseBackColor
                    AButton.ForeColor = BaseTextColor
                    Call LoadEMProcedure(True, "")
                    Exit Sub
                End If
            End If
        End If
        AButton.BackColor = ButtonSelectionColor
        AButton.ForeColor = TextSelectionColor
        Call TurnCPTs(False)
        AButton.Enabled = True
    End If
ElseIf (TriggerOn) Then
    frmEventMsgs.Header = AButton.Text
    frmEventMsgs.AcceptText = "Change Modifiers"
    frmEventMsgs.RejectText = "De-Select"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Link Diagnosis"
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    If (BusLogOn) Then
        frmEventMsgs.Other1Text = "Level Qualified"
    End If
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 2) Then
        AButton.BackColor = BaseBackColor
        AButton.ForeColor = BaseTextColor
        ProcedureEM = ""
        Erase ProcedureModifier
        ProcedureLink = ""
        Call TurnCPTs(True)
    ElseIf (frmEventMsgs.Result = 1) Then
        frmModifiers.ModifierOn = True
        If (frmModifiers.LoadModifiers(True)) Then
            frmModifiers.Show 1
            If (Trim(frmModifiers.Modifiers) <> "") Then
                For i = 1 To Len(frmModifiers.Modifiers) Step 2
                    ProcedureModifier(Int(i / 2) + 1) = Mid(frmModifiers.Modifiers, i, 2)
                Next i
                Temp = ""
                For i = 1 To 4
                    Temp = Temp + ProcedureModifier(i)
                Next i
                lblRef.Caption = Temp + " " + ProcedureLink
                lblRef.Visible = True
            End If
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        frmModifiers.LinkedDiagOn = True
        If (frmModifiers.LoadLinkedDiags(True)) Then
            frmModifiers.Show 1
            If (Trim(frmModifiers.LinkedDiags) <> "") Then
                ProcedureLink = frmModifiers.LinkedDiags
                Temp = ""
                For i = 1 To 4
                    Temp = Temp + ProcedureModifier(i)
                Next i
                lblRef.Caption = Temp + " " + ProcedureLink
                lblRef.Visible = True
            End If
        End If
    ElseIf (frmEventMsgs.Result = 5) Then
        frmInquiry.Show 1
    End If
End If
End Sub

Private Sub cmdHome_Click()
NavigationAction = 99
Unload frmSetEMProcedure
End Sub

Private Sub cmdReviewSystems_Click()
NavigationAction = 2
CurrentIndex = 0
Unload frmSetEMProcedure
End Sub

Public Sub cmdDone_Click()
NavigationAction = 1
Unload frmSetEMProcedure
End Sub

Private Sub cmdNotes_Click()
If Not UserLogin.HasPermission(epPatientNotes) Then
        frmEventMsgs.Header = "Not Permissioned"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
End If
frmNotes.NoteId = 0
frmNotes.NoteOn = False
frmNotes.EyeContext = ""
frmNotes.SystemReference = ""
frmNotes.PatientId = PatientId
frmNotes.AppointmentId = AppointmentId
frmNotes.MaintainOn = True
frmNotes.SetTo = "C"
If (frmNotes.LoadNotes) Then
    frmNotes.Show 1
End If
End Sub

Private Function LoadEMProcs() As Boolean
Dim j As Integer
Dim i As Integer
Dim ButtonCnt As Integer
Dim TheDisplayName As String
Dim TheTag As String
LoadEMProcs = False
Call ClearCPTs
i = CurrentIndex
ButtonCnt = 1
If (Trim(ProcedureEM) <> "") And (Trim(ProcedureEM) <> "0") Then
    Call TurnCPTs(False)
Else
    Call TurnCPTs(True)
End If
While (i <= MaxEMProcedures) And (ButtonCnt < 16)
    TheDisplayName = EMProcs(i).Name
    TheTag = Trim(str(EMProcs(i).Id))
    Call SetButton(ButtonCnt, TheDisplayName, TheTag)
    ButtonCnt = ButtonCnt + 1
    i = i + 1
Wend
CurrentIndex = i - 1
LoadEMProcs = True
If (MaxEMProcedures > 15) Then
    cmdNext.Visible = True
    cmdPrev.Visible = True
Else
    cmdNext.Visible = False
    cmdPrev.Visible = False
End If
cmdMoreCPTs.Visible = False
End Function

Public Function LoadEMProcedure(Init As Boolean, ABasis As String) As Boolean
Dim RemoveOn As Boolean
Dim ALevel As Integer, OLevel As Integer
Dim ETemp As String, OTemp As String
Dim PolId1 As Long, PolId2 As Long
Dim p As Integer, k As Integer
Dim i As Integer, TempMax As Integer
Dim Found As Boolean
Dim TheName As String, Temp As String
Dim RetCode As PracticeFavorites
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
Dim RetrieveProcedure As DiagnosisMasterProcedures
TriggerOn = True
LoadEMProcedure = False
If (Init) Then
    TriggerOn = False
    NewDiagOn = False
    NewRxOn = False
    MaxEMProcedures = 0
    CurrentIndex = 1
    ButtonSelectionColor = 14745312
    TextSelectionColor = 0
    BaseBackColor = &H9B9626
    BaseTextColor = &HFFFFFF
    ProcedureEM = ""
    ProcedureHighlight = False
    BusLogOn = CheckConfigCollection("BUSLOGON") = "T"
    InsurerId = 0
    If (BusLogOn) Then
        Set RetPatientFinancialService = New PatientFinancialService
        RetPatientFinancialService.InsuredPatientId = PatientId
        Set RS = RetPatientFinancialService.GetPatientPrimaryInsurer
        If (Not RS Is Nothing And RS.RecordCount > 0) Then
            InsurerId = RS("InsurerId")
        End If
        Set RetPatientFinancialService = Nothing
    End If
End If
Set RetrieveProcedure = New DiagnosisMasterProcedures
Set RetCode = New PracticeFavorites
i = 1
RetCode.FavoritesSystem = "?"
RetCode.FavoritesDiagnosis = ""
RetCode.FavoritesRank = 0
If (RetCode.FindFavorites > 0) Then
    While (RetCode.SelectFavorites(i))
        RetrieveProcedure.ProcedureCPT = Trim(RetCode.FavoritesDiagnosis)
        RetrieveProcedure.ProcedureRank = 1
        If (RetrieveProcedure.FindProcedurebyEM) Then
            If (RetrieveProcedure.SelectProcedure(1)) Then
                EMProcs(i).Name = Trim(RetrieveProcedure.ProcedureCPT) + Chr(13) + Chr(10) + Trim(RetrieveProcedure.ProcedureName)
                EMProcs(i).Id = RetrieveProcedure.ProcedureId
            End If
        End If
        i = i + 1
    Wend
End If
MaxEMProcedures = i - 1
p = 0
i = 1
RetrieveProcedure.ProcedureCPT = Chr(1)
RetrieveProcedure.ProcedureRank = 1
TempMax = RetrieveProcedure.FindProcedurebyEM
If (TempMax > 0) And (TempMax <> MaxEMProcedures) Then
    While (RetrieveProcedure.SelectProcedure(i))
        TheName = Trim(RetrieveProcedure.ProcedureCPT) + Chr(13) + Chr(10) + Trim(RetrieveProcedure.ProcedureName)
        Found = False
        For k = 1 To MaxEMProcedures
            If (TheName = EMProcs(k).Name) Then
                Found = True
                Exit For
            End If
        Next k
        If Not (Found) Then
            p = p + 1
            EMProcs(MaxEMProcedures + p).Name = TheName
            EMProcs(MaxEMProcedures + p).Id = RetrieveProcedure.ProcedureId
        End If
        i = i + 1
    Wend
End If
MaxEMProcedures = MaxEMProcedures + p
Set RetCode = Nothing
Set RetrieveProcedure = Nothing
LoadEMProcedure = LoadEMProcs
If (Init) Then
    Call LoadFromFile(False)
    EMBasis = ABasis
    If (frmSystems1.IsPostOpOn) And (Trim(EMBasis) <> "") Then
        EMBasis = "99024"
        ABasis = "99024"
    End If
    CurrentIndex = 1
    LoadEMProcedure = LoadEMProcs
    lblIns.Caption = InsurerStuff
    If (BusLogOn) Then
        If (Trim(ABasis) <> "") Then
            If (ABasis = "99024") Then
                ETemp = "99024"
                OTemp = ""
            Else
                Call frmInquiry.GetAnalyticalLevel(ALevel, OLevel)
                Temp = ""
                If Not (frmSystems1.IsPostOpOn) Then
                    ETemp = Trim(str(Val(EMBasis) + ALevel))
                Else
                    ETemp = "99024"
                End If
                If (OLevel = 0) Then
                    OLevel = 1
                End If
                OTemp = "9200" + Trim(str(OLevel))
                If (Mid(ETemp, 3, 2) = "24") Then
                    OTemp = "9204" + Trim(str(OLevel))
                ElseIf (Mid(ETemp, 3, 2) = "21") Then
                    OTemp = "9201" + Trim(str(OLevel))
                End If
            End If
            ProcedureEM = ETemp
            If (IsOCodeValid(InsurerId, ETemp, OTemp, Temp, RemoveOn)) Then
                If (Trim(Temp) <> "") Then
                    ProcedureEM = Temp
                    If (RemoveOn) Then
' remove Opthmoscopy procedure
                    End If
                End If
            End If
            ProcedureModifier(1) = ""
            ProcedureModifier(2) = ""
            ProcedureModifier(3) = ""
            ProcedureModifier(4) = ""
            ProcedureHighlight = False
            ProcedureLink = ""
            Call RetainEMProcedure(PatientId)
            Call LoadFromFile(False)
            CurrentIndex = 1
            LoadEMProcedure = LoadEMProcs
        End If
    End If
End If
If (IsAlertPresent(PatientId, EAlertType.eatEM)) Then
    lblAlert.Caption = "E and M Alert ON"
    lblAlert.Visible = True
Else
    lblAlert.Caption = "Alert ON"
    lblAlert.Visible = IsAlertPresent(PatientId, 0)
End If
lblPostOp.Visible = False
If (BusLogOn) Then
    If (frmSystems1.IsPostOpOn) Then
        lblPostOp.Visible = True
    End If
End If
TriggerOn = True
End Function

Private Function IsEMPresent(ALabel As String, Cnt As Integer) As Boolean
Dim i As Integer, j As Integer
Dim TestProc As String
Dim Proc As String
IsEMPresent = False
i = InStrPS(ALabel, Chr(13))
If (i <> 0) Then
    Proc = Left(ALabel, i - 1)
Else
    Exit Function
End If
For i = 1 To Cnt
    TestProc = ""
    If (i = 1) Then
        j = InStrPS(cmdProcedure1.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure1.Text, j - 1)
        End If
    ElseIf (i = 2) Then
        j = InStrPS(cmdProcedure2.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure2.Text, j - 1)
        End If
    ElseIf (i = 3) Then
        j = InStrPS(cmdProcedure3.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure3.Text, j - 1)
        End If
    ElseIf (i = 4) Then
        j = InStrPS(cmdProcedure4.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure4.Text, j - 1)
        End If
    ElseIf (i = 5) Then
        j = InStrPS(cmdProcedure5.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure5.Text, j - 1)
        End If
    ElseIf (i = 6) Then
        j = InStrPS(cmdProcedure6.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure6.Text, j - 1)
        End If
    ElseIf (i = 7) Then
        j = InStrPS(cmdProcedure7.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure7.Text, j - 1)
        End If
    ElseIf (i = 8) Then
        j = InStrPS(cmdProcedure8.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure8.Text, j - 1)
        End If
    ElseIf (i = 9) Then
        j = InStrPS(cmdProcedure9.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure9.Text, j - 1)
        End If
    ElseIf (i = 10) Then
        j = InStrPS(cmdProcedure10.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure10.Text, j - 1)
        End If
    ElseIf (i = 11) Then
        j = InStrPS(cmdProcedure11.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure11.Text, j - 1)
        End If
    ElseIf (i = 12) Then
        j = InStrPS(cmdProcedure12.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure12.Text, j - 1)
        End If
    ElseIf (i = 13) Then
        j = InStrPS(cmdProcedure13.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure13.Text, j - 1)
        End If
    ElseIf (i = 14) Then
        j = InStrPS(cmdProcedure14.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure14.Text, j - 1)
        End If
    ElseIf (i = 15) Then
        j = InStrPS(cmdProcedure15.Text, Chr(13))
        If (j <> 0) Then
            TestProc = Left(cmdProcedure15.Text, j - 1)
        End If
    End If
    If (TestProc = Proc) Then
        IsEMPresent = True
        Exit For
    End If
Next i
End Function

Private Sub SetButton(Ref As Integer, ADisplayName As String, ATag As String)
If (Ref = 1) Then
    cmdProcedure1.Text = ADisplayName
    cmdProcedure1.Tag = ATag
    cmdProcedure1.Visible = True
    If (ProcedureEM = Left(cmdProcedure1.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure1)
    End If
ElseIf (Ref = 2) Then
    cmdProcedure2.Text = ADisplayName
    cmdProcedure2.Tag = ATag
    cmdProcedure2.Visible = True
    If (ProcedureEM = Left(cmdProcedure2.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure2)
    End If
ElseIf (Ref = 3) Then
    cmdProcedure3.Text = ADisplayName
    cmdProcedure3.Tag = ATag
    cmdProcedure3.Visible = True
    If (ProcedureEM = Left(cmdProcedure3.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure3)
    End If
ElseIf (Ref = 4) Then
    cmdProcedure4.Text = ADisplayName
    cmdProcedure4.Tag = ATag
    cmdProcedure4.Visible = True
    If (ProcedureEM = Left(cmdProcedure4.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure4)
    End If
ElseIf (Ref = 5) Then
    cmdProcedure5.Text = ADisplayName
    cmdProcedure5.Tag = ATag
    cmdProcedure5.Visible = True
    If (ProcedureEM = Left(cmdProcedure5.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure5)
    End If
ElseIf (Ref = 6) Then
    cmdProcedure6.Text = ADisplayName
    cmdProcedure6.Tag = ATag
    cmdProcedure6.Visible = True
    If (ProcedureEM = Left(cmdProcedure6.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure6)
    End If
ElseIf (Ref = 7) Then
    cmdProcedure7.Text = ADisplayName
    cmdProcedure7.Tag = ATag
    cmdProcedure7.Visible = True
    If (ProcedureEM = Left(cmdProcedure7.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure7)
    End If
ElseIf (Ref = 8) Then
    cmdProcedure8.Text = ADisplayName
    cmdProcedure8.Tag = ATag
    cmdProcedure8.Visible = True
    If (ProcedureEM = Left(cmdProcedure8.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure8)
    End If
ElseIf (Ref = 9) Then
    cmdProcedure9.Text = ADisplayName
    cmdProcedure9.Tag = ATag
    cmdProcedure9.Visible = True
    If (ProcedureEM = Left(cmdProcedure9.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure9)
    End If
ElseIf (Ref = 10) Then
    cmdProcedure10.Text = ADisplayName
    cmdProcedure10.Tag = ATag
    cmdProcedure10.Visible = True
    If (ProcedureEM = Left(cmdProcedure10.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure10)
    End If
ElseIf (Ref = 11) Then
    cmdProcedure11.Text = ADisplayName
    cmdProcedure11.Tag = ATag
    cmdProcedure11.Visible = True
    If (ProcedureEM = Left(cmdProcedure11.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure11)
    End If
ElseIf (Ref = 12) Then
    cmdProcedure12.Text = ADisplayName
    cmdProcedure12.Tag = ATag
    cmdProcedure12.Visible = True
    If (ProcedureEM = Left(cmdProcedure12.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure12)
    End If
ElseIf (Ref = 13) Then
    cmdProcedure13.Text = ADisplayName
    cmdProcedure13.Tag = ATag
    cmdProcedure13.Visible = True
    If (ProcedureEM = Left(cmdProcedure13.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure13)
    End If
ElseIf (Ref = 14) Then
    cmdProcedure14.Text = ADisplayName
    cmdProcedure14.Tag = ATag
    cmdProcedure14.Visible = True
    If (ProcedureEM = Left(cmdProcedure14.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure14)
    End If
ElseIf (Ref = 15) Then
    cmdProcedure15.Text = ADisplayName
    cmdProcedure15.Tag = ATag
    cmdProcedure15.Visible = True
    If (ProcedureEM = Left(cmdProcedure15.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure15)
    End If
End If
End Sub

Public Function GetEMProcedure(AProcedure As String, AMods As String, ALnk As String, AHigh As Boolean) As Boolean
Dim q As Integer
AProcedure = ""
AMods = ""
GetEMProcedure = False
If (Trim(ProcedureEM) <> "") Then
    AProcedure = ProcedureEM
    For q = 1 To 4
        AMods = AMods + Trim(ProcedureModifier(q))
    Next q
    ALnk = ProcedureLink
    AHigh = ProcedureHighlight
    GetEMProcedure = True
End If
End Function

Public Function SetEMProcedureHighlight(IHow As Boolean) As Boolean
SetEMProcedureHighlight = True
ProcedureHighlight = IHow
End Function

Public Function InitializeEMProcedure() As Boolean
InitializeEMProcedure = True
ProcedureEM = ""
Erase ProcedureModifier
ProcedureLink = ""
ProcedureHighlight = False
DiagMode = False
End Function

Public Sub LoadFromFile(ByVal mode As Boolean)
On Error GoTo lLoadEM_Error
Dim i As Integer
Dim Rec As String
Dim Temp As String
Dim TheFile As String
Dim FileNum As Integer
Dim FileNum1 As Integer
Dim AButton As fpBtn
Dim LineCount As Integer
RetainModifiers = False
TheFile = DoctorInterfaceDirectory + "EMProc" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
If (mode) Then TheFile = DoctorInterfaceDirectory + "EMProc_Icd10" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatientId)) + ".txt"
If (FM.IsFileThere(TheFile)) Then
    'Read Number of Lines
    FM.Clear
    LineCount = 0
    FileNum1 = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum1)
    While Not (EOF(FileNum1))
        Line Input #FileNum1, Rec
        LineCount = LineCount + 1
    Wend
    FM.CloseFile CLng(FileNum1), False
'
    FileNum = FreeFile
    FM.OpenFile TheFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadShared, CLng(FileNum)
    If Not (EOF(FileNum)) Then
        Line Input #FileNum, Rec
        ProcedureEM = Trim(Rec)
        Line Input #FileNum, Rec
        Rec = Trim(Rec)
        For i = 1 To Len(Rec) Step 2
            ProcedureModifier(Int(i / 2) + 1) = Mid(Rec, i, 2)
        Next i
        Line Input #FileNum, Rec
        ProcedureLink = Trim(Rec)
        Line Input #FileNum, Rec
        ProcedureHighlight = False
        If (Rec = "T") Then
            ProcedureHighlight = True
        End If
        If (LineCount > 4) Then
            Line Input #FileNum, Rec
            DiagMode = False
            If (Rec = "ICD10") Then DiagMode = True
        End If
    End If
    FM.CloseFile CLng(FileNum), False
    RetainModifiers = True
    If Not RetainModifiers Then
    If (ProcedureEM = Left(cmdProcedure1.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure1)
    ElseIf (ProcedureEM = Left(cmdProcedure2.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure2)
    ElseIf (ProcedureEM = Left(cmdProcedure3.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure3)
    ElseIf (ProcedureEM = Left(cmdProcedure4.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure4)
    ElseIf (ProcedureEM = Left(cmdProcedure5.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure5)
    ElseIf (ProcedureEM = Left(cmdProcedure6.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure6)
    ElseIf (ProcedureEM = Left(cmdProcedure7.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure7)
    ElseIf (ProcedureEM = Left(cmdProcedure8.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure8)
    ElseIf (ProcedureEM = Left(cmdProcedure9.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure9)
    ElseIf (ProcedureEM = Left(cmdProcedure10.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure10)
    ElseIf (ProcedureEM = Left(cmdProcedure11.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure11)
    ElseIf (ProcedureEM = Left(cmdProcedure12.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure12)
    ElseIf (ProcedureEM = Left(cmdProcedure13.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure13)
    ElseIf (ProcedureEM = Left(cmdProcedure14.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure14)
    ElseIf (ProcedureEM = Left(cmdProcedure15.Text, 5)) Then
        Call TriggerProcedure(cmdProcedure15)
    End If
    End If
    If (FM.IsFileThere(TheFile)) Then
        FM.Kill TheFile
    End If
    RetainModifiers = False
    Temp = ""
    For i = 1 To 4
        Temp = Temp + ProcedureModifier(i)
    Next i
    lblRef.Caption = Temp + " " + ProcedureLink
    lblRef.Visible = True
    Exit Sub
lLoadEM_Error:
    FM.CloseFile CLng(FileNum), False
End If
End Sub

Public Function PurgeRetainedEM(PatId As Long, ABack As Boolean) As Boolean
Dim TheFile As String
Dim OtherFile As String
PurgeRetainedEM = False
If (PatId > 0) Then
    Call InitializeEMProcedure
    TheFile = DoctorInterfaceDirectory + "EMProc" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
    OtherFile = DoctorInterfaceDirectory + "Backup\EMProc" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
    If (FM.IsFileThere(TheFile)) Then
        If (ABack) Then
            FM.FileCopy TheFile, OtherFile
        End If
        FM.Kill TheFile
    End If
End If
End Function

Public Function RetainEMProcedure(PatId As Long) As Boolean
Dim RetModifier As String
Dim RetLink As String
Dim RetString As String
Dim RetHigh As Boolean
Dim FileNum As Integer
Dim TheFile As String
RetainEMProcedure = False
If (PatId > 0) Then
    FileNum = FreeFile
    TheFile = DoctorInterfaceDirectory + "EMProc" + "_" + Trim(str(AppointmentId)) + "_" + Trim(str(PatId)) + ".txt"
    FM.OpenFile TheFile, FileOpenMode.Output, FileAccess.WriteShared, CLng(FileNum)
    If (GetEMProcedure(RetString, RetModifier, RetLink, RetHigh)) Then
        Print #FileNum, RetString
        Print #FileNum, RetModifier
        Print #FileNum, RetLink
        If (RetHigh) Then
            Print #FileNum, "T"
        Else
            Print #FileNum, "F"
        End If
    End If
    FM.CloseFile CLng(FileNum)
    RetainEMProcedure = True
End If
End Function

Private Sub cmdSumm_Click()
frmFinalNew.PatientId = PatientId
frmFinalNew.AppointmentId = AppointmentId
frmFinalNew.ActivityId = globalExam.iActivityId
If (frmFinalNew.LoadFinal(True)) Then
    frmFinalNew.Show 1
End If
End Sub

Private Sub lblAlert_Click()
If (frmAlert.LoadAlerts(PatientId, EAlertType.eatEM, True)) Then
    frmAlert.Show 1
    If (IsAlertPresent(PatientId, EAlertType.eatEM)) Then
        lblAlert.Caption = "E and M Alert ON"
        lblAlert.Visible = True
    Else
        lblAlert.Caption = "Alert ON"
        lblAlert.Visible = IsAlertPresent(PatientId, 0)
    End If
End If
End Sub

Private Function TriggerPostOpTest(CPT As String, AMod As String) As Boolean
Dim ModToUse As String
Dim DefaultOn As Boolean
Dim QuestionIt As Boolean
Dim RetMod As PracticeModifierRules
TriggerPostOpTest = True
AMod = ""
QuestionIt = False
If (frmSystems1.IsPostOpOn) Then
    ModToUse = frmSystems1.GetDefaultClassModifier
    DefaultOn = True
    Set RetMod = New PracticeModifierRules
    RetMod.ModifierClassId = frmSystems1.BusinessClassId
    RetMod.ModifierCPT = CPT
    If (RetMod.FindModifierRule > 0) Then
        If (RetMod.SelectModifierRule(1)) Then
            ModToUse = Trim(RetMod.ModifierRule5)
            QuestionIt = True
            DefaultOn = False
        End If
    End If
    Set RetMod = Nothing
    If (QuestionIt) Then
        frmEventMsgs.Header = "Is this service related to the surgery ?"
        frmEventMsgs.AcceptText = "Unrelated Bill Service"
        frmEventMsgs.RejectText = "Related Do Not Bill"
        frmEventMsgs.CancelText = "Related Bill Service"
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        If (frmEventMsgs.Result = 1) Then
            AMod = ModToUse
        ElseIf (frmEventMsgs.Result = 2) Then
' set EM code to 99024
            CPT = "99024"
            AMod = ""
        End If
    End If
End If
End Function

Public Function IsOCodeValid(InsId As Long, ABasis As String, OBasis As String, NewCode As String, ROn As Boolean) As Boolean
NewCode = ""
ROn = False
IsOCodeValid = False
'If (frmSystems1.UseOCodes) Then
    If (Left(ABasis, 4) = "9920") Or (Left(ABasis, 4) = "9921") Or (Left(ABasis, 4) = "9924") Then
        IsOCodeValid = frmInquiry.GetAnalyticalPricing(InsId, ABasis, OBasis, NewCode, ROn)
    End If
'End If
End Function
Public Function FrmClose()
Call cmdDone_Click
  Unload Me
End Function
Public Function ClearEMProcedure()
ProcedureEM = ""
Erase ProcedureModifier
ProcedureLink = ""
End Function
