VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Begin VB.Form frmCLSearch 
   BackColor       =   &H00505050&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9420
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12540
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   Picture         =   "CLSearch.frx":0000
   ScaleHeight     =   9420
   ScaleWidth      =   12540
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   990
      Index           =   1
      Left            =   10800
      TabIndex        =   36
      Top             =   7800
      Visible         =   0   'False
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":36C9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmd 
      Height          =   990
      Index           =   0
      Left            =   9720
      TabIndex        =   37
      Top             =   8280
      Visible         =   0   'False
      Width           =   1095
      _Version        =   131072
      _ExtentX        =   1931
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":38A6
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch3 
      Height          =   1095
      Left            =   4920
      TabIndex        =   0
      Top             =   480
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":3A85
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch2 
      Height          =   1095
      Left            =   2520
      TabIndex        =   1
      Top             =   480
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":3C6C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch1 
      Height          =   1095
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":3E4B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch7 
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   2880
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":402C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch8 
      Height          =   1095
      Left            =   4920
      TabIndex        =   4
      Top             =   1680
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":4210
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch9 
      Height          =   1095
      Left            =   7320
      TabIndex        =   5
      Top             =   1680
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":43F5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch6 
      Height          =   1095
      Left            =   9720
      TabIndex        =   6
      Top             =   480
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":45D8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch5 
      Height          =   1095
      Left            =   7320
      TabIndex        =   7
      Top             =   480
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":47BD
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch4 
      Height          =   1095
      Left            =   120
      TabIndex        =   8
      Top             =   1680
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":49A0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9960
      TabIndex        =   11
      Top             =   7800
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":4B7F
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   12
      Top             =   7800
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":4D5E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch12 
      Height          =   1095
      Left            =   4920
      TabIndex        =   13
      Top             =   2880
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":4F3D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch13 
      Height          =   1095
      Left            =   2520
      TabIndex        =   14
      Top             =   1680
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":511C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch14 
      Height          =   1095
      Left            =   7320
      TabIndex        =   15
      Top             =   2880
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":52FE
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch11 
      Height          =   1095
      Left            =   2520
      TabIndex        =   16
      Top             =   2880
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":54E5
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSearch10 
      Height          =   1095
      Left            =   9720
      TabIndex        =   17
      Top             =   1680
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1931
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":56C8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect2 
      Height          =   735
      Left            =   2520
      TabIndex        =   18
      Top             =   5280
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":58A9
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect3 
      Height          =   735
      Left            =   4920
      TabIndex        =   19
      Top             =   5280
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":5A8A
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect1 
      Height          =   735
      Left            =   120
      TabIndex        =   20
      Top             =   5280
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":5C6B
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect7 
      Height          =   735
      Left            =   2520
      TabIndex        =   21
      Top             =   6120
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":5E4C
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect8 
      Height          =   735
      Left            =   4920
      TabIndex        =   22
      Top             =   6120
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":602D
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect9 
      Height          =   735
      Left            =   7320
      TabIndex        =   23
      Top             =   6120
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":620E
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect6 
      Height          =   735
      Left            =   120
      TabIndex        =   24
      Top             =   6120
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":63EF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect5 
      Height          =   735
      Left            =   9720
      TabIndex        =   25
      Top             =   5280
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":65D0
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect4 
      Height          =   735
      Left            =   7320
      TabIndex        =   26
      Top             =   5280
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":67B1
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect15 
      Height          =   735
      Left            =   9720
      TabIndex        =   27
      Top             =   6960
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":6992
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect12 
      Height          =   735
      Left            =   2520
      TabIndex        =   28
      Top             =   6960
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":6B73
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect13 
      Height          =   735
      Left            =   4920
      TabIndex        =   29
      Top             =   6960
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":6D54
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect14 
      Height          =   735
      Left            =   7320
      TabIndex        =   30
      Top             =   6960
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":6F35
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect11 
      Height          =   735
      Left            =   120
      TabIndex        =   31
      Top             =   6960
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":7116
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdSelect10 
      Height          =   735
      Left            =   9720
      TabIndex        =   32
      Top             =   6120
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":72F7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   735
      Left            =   2520
      TabIndex        =   33
      Top             =   7800
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":74D8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   735
      Left            =   7320
      TabIndex        =   34
      Top             =   7800
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1296
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":76BB
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClear 
      Height          =   990
      Left            =   9720
      TabIndex        =   35
      Top             =   4080
      Width           =   2175
      _Version        =   131072
      _ExtentX        =   3836
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11373126
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "CLSearch.frx":789A
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C19B49&
      BackStyle       =   0  'Transparent
      Caption         =   "Search for CL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label lblSearch 
      BackColor       =   &H00C19B49&
      Caption         =   "Search Selection"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   975
      Left            =   120
      TabIndex        =   9
      Top             =   4080
      Width           =   9375
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmCLSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public InventoryId As Long
Public InventoryDescription As String
Private CurrentCriteria As Integer
Private CurrentSelectButtonIdx As Long
Private TotalSelectButtonIdx As Long
Private SearchCriteria(14) As String
Private SearchCriteriaSelection(14) As String

Private Sub cmd_Click(Index As Integer)

If cmdSearch1.ForeColor = vbWhite _
And cmdSearch4.ForeColor = vbWhite _
And cmdSearch7.ForeColor = vbWhite Then
    InventoryDescription = SearchCriteriaSelection(1)
    If Not InventoryDescription = "" Then InventoryDescription = InventoryDescription & " "
    InventoryDescription = InventoryDescription & SearchCriteriaSelection(4)
    If Not InventoryDescription = "" Then InventoryDescription = InventoryDescription & " "
    InventoryDescription = Trim(InventoryDescription & SearchCriteriaSelection(7))
    frmAssignWearNew.CommArea = Index & vbTab & InventoryId & vbTab & InventoryDescription & vbTab
    Unload frmCLSearch
Else
    frmEventMsgs.Header = "Series, Type and Wear Time must be selected"
    frmEventMsgs.AcceptText = ""
    frmEventMsgs.RejectText = "Ok"
    frmEventMsgs.CancelText = ""
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
End If
End Sub

Private Sub cmdClear_Click()
Dim i As Integer
InventoryId = 0
InventoryDescription = ""
CurrentCriteria = 0
CurrentSelectButtonIdx = 0
TotalSelectButtonIdx = 0
For i = 1 To 14
    SearchCriteriaSelection(i) = ""
Next i
lblSearch.Caption = ""
lblSearch.Tag = ""
Call ClearSelectButtons
End Sub

Private Sub cmdDone_Click()
InventoryId = SetInventoryId
If (InventoryId > 0) Then
    InventoryDescription = Trim(lblSearch.Tag)
Else
    InventoryDescription = ""
End If
Unload frmCLSearch
End Sub

Private Sub cmdHome_Click()
InventoryId = 0
InventoryDescription = ""
Unload frmCLSearch
End Sub

Private Sub cmdNext_Click()
Call LoadSelectionButtons
End Sub

Private Sub cmdPrev_Click()
Dim MyIdx As Integer
CurrentSelectButtonIdx = CurrentSelectButtonIdx - 30
If (CurrentSelectButtonIdx < 1) Then
    MyIdx = Int(TotalSelectButtonIdx / 15) * 15
    If (MyIdx >= TotalSelectButtonIdx) Then
        MyIdx = MyIdx - 15
    End If
    CurrentSelectButtonIdx = MyIdx
End If
Call LoadSelectionButtons
End Sub

Private Sub ClearSelectButtons()
cmdSelect1.Visible = False
cmdSelect1.Text = ""
cmdSelect2.Visible = False
cmdSelect2.Text = ""
cmdSelect3.Visible = False
cmdSelect3.Text = ""
cmdSelect4.Visible = False
cmdSelect4.Text = ""
cmdSelect5.Visible = False
cmdSelect5.Text = ""
cmdSelect6.Visible = False
cmdSelect6.Text = ""
cmdSelect7.Visible = False
cmdSelect7.Text = ""
cmdSelect8.Visible = False
cmdSelect8.Text = ""
cmdSelect9.Visible = False
cmdSelect9.Text = ""
cmdSelect10.Visible = False
cmdSelect10.Text = ""
cmdSelect11.Visible = False
cmdSelect11.Text = ""
cmdSelect12.Visible = False
cmdSelect12.Text = ""
cmdSelect13.Visible = False
cmdSelect13.Text = ""
cmdSelect14.Visible = False
cmdSelect14.Text = ""
cmdSelect15.Visible = False
cmdSelect15.Text = ""
cmdPrev.Visible = False
cmdNext.Visible = False
End Sub

Private Function LoadSelectionButtons() As Boolean
Dim i As Integer
Dim j As Integer
Dim DisplayText As String
Dim CLInv As CLInventory
LoadSelectionButtons = False
Call ClearSelectButtons
DisplayText = ""
For i = 1 To 14
    If (Trim(SearchCriteriaSelection(i)) <> "") Then
        If (Trim(DisplayText) <> "") Then
            DisplayText = DisplayText + "And "
        End If
        DisplayText = DisplayText + "(" + SearchCriteria(i) + " = '" + Trim(SearchCriteriaSelection(i)) + "') "
    End If
Next i
Set CLInv = New CLInventory
CLInv.FieldSelect = SearchCriteria(CurrentCriteria)
CLInv.Criteria = DisplayText
CLInv.OrderBy = SearchCriteria(CurrentCriteria)
TotalSelectButtonIdx = CLInv.FindCLInventorySingleFieldwithTotal
If (TotalSelectButtonIdx > 0) Then
    If (CurrentSelectButtonIdx >= TotalSelectButtonIdx) Or (CurrentSelectButtonIdx < 1) Then
        CurrentSelectButtonIdx = 1
    End If
    j = 1
    i = CurrentSelectButtonIdx
    While (CLInv.SelectCLInventory(i)) And (j < 16)
        If (j = 1) Then
            cmdSelect1.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect1.Visible = True
        ElseIf (j = 2) Then
            cmdSelect2.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect2.Visible = True
        ElseIf (j = 3) Then
            cmdSelect3.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect3.Visible = True
        ElseIf (j = 4) Then
            cmdSelect4.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect4.Visible = True
        ElseIf (j = 5) Then
            cmdSelect5.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect5.Visible = True
        ElseIf (j = 6) Then
            cmdSelect6.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect6.Visible = True
        ElseIf (j = 7) Then
            cmdSelect7.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect7.Visible = True
        ElseIf (j = 8) Then
            cmdSelect8.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect8.Visible = True
        ElseIf (j = 9) Then
            cmdSelect9.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect9.Visible = True
        ElseIf (j = 10) Then
            cmdSelect10.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect10.Visible = True
        ElseIf (j = 11) Then
            cmdSelect11.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect11.Visible = True
        ElseIf (j = 12) Then
            cmdSelect12.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect12.Visible = True
        ElseIf (j = 13) Then
            cmdSelect13.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect13.Visible = True
        ElseIf (j = 14) Then
            cmdSelect14.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect14.Visible = True
        ElseIf (j = 15) Then
            cmdSelect15.Text = Trim(CLInv.SingleFieldValue)
            cmdSelect15.Visible = True
        End If
        i = i + 1
        j = j + 1
    Wend
    CurrentSelectButtonIdx = i
End If
Set CLInv = Nothing
cmdPrev.Visible = False
cmdNext.Visible = False
If (TotalSelectButtonIdx > 15) Then
    cmdPrev.Visible = True
    cmdNext.Visible = True
End If
End Function

Private Sub cmdSearch1_Click()
Call DoSearch(1)
End Sub

Private Sub cmdSearch2_Click()
Call DoSearch(2)
End Sub

Private Sub cmdSearch3_Click()
Call DoSearch(3)
End Sub

Private Sub cmdSearch4_Click()
Call DoSearch(4)
End Sub

Private Sub cmdSearch5_Click()
Call DoSearch(5)
End Sub

Private Sub cmdSearch6_Click()
Call DoSearch(6)
End Sub

Private Sub cmdSearch7_Click()
Call DoSearch(7)
End Sub

Private Sub cmdSearch8_Click()
Call DoSearch(8)
End Sub

Private Sub cmdSearch9_Click()
Call DoSearch(9)
End Sub

Private Sub cmdSearch10_Click()
Call DoSearch(10)
End Sub

Private Sub cmdSearch11_Click()
Call DoSearch(11)
End Sub

Private Sub cmdSearch12_Click()
Call DoSearch(12)
End Sub

Private Sub cmdSearch13_Click()
Call DoSearch(13)
End Sub

Private Sub cmdSearch14_Click()
Call DoSearch(14)
End Sub

Private Sub DoSearch(AId As Integer)
CurrentCriteria = AId
Call LoadSelectionButtons
End Sub

Private Sub cmdSelect1_Click()
Call DoSelect(cmdSelect1.Text)
End Sub

Private Sub cmdSelect2_Click()
Call DoSelect(cmdSelect2.Text)
End Sub

Private Sub cmdSelect3_Click()
Call DoSelect(cmdSelect3.Text)
End Sub

Private Sub cmdSelect4_Click()
Call DoSelect(cmdSelect4.Text)
End Sub

Private Sub cmdSelect5_Click()
Call DoSelect(cmdSelect5.Text)
End Sub

Private Sub cmdSelect6_Click()
Call DoSelect(cmdSelect6.Text)
End Sub

Private Sub cmdSelect7_Click()
Call DoSelect(cmdSelect7.Text)
End Sub

Private Sub cmdSelect8_Click()
Call DoSelect(cmdSelect8.Text)
End Sub

Private Sub cmdSelect9_Click()
Call DoSelect(cmdSelect9.Text)
End Sub

Private Sub cmdSelect10_Click()
Call DoSelect(cmdSelect10.Text)
End Sub

Private Sub cmdSelect11_Click()
Call DoSelect(cmdSelect11.Text)
End Sub

Private Sub cmdSelect12_Click()
Call DoSelect(cmdSelect12.Text)
End Sub

Private Sub cmdSelect13_Click()
Call DoSelect(cmdSelect13.Text)
End Sub

Private Sub cmdSelect14_Click()
Call DoSelect(cmdSelect14.Text)
End Sub

Private Sub cmdSelect15_Click()
Call DoSelect(cmdSelect15.Text)
End Sub

Private Sub DoSelect(AText As String)
SearchCriteriaSelection(CurrentCriteria) = AText
Call DisplaySelections
Call ClearSelectButtons
End Sub

Private Sub DisplaySelections()
Dim i As Integer
Dim DisplayText As String
Dim MyTag As String
MyTag = ""
DisplayText = ""
For i = 1 To 14
    If (Trim(SearchCriteriaSelection(i)) <> "") Then
        If (Trim(DisplayText) <> "") Then
            DisplayText = DisplayText + ", "
        End If
        DisplayText = DisplayText + SearchCriteria(i) + ":" + Trim(SearchCriteriaSelection(i))
        MyTag = MyTag + " " + Trim(SearchCriteriaSelection(i))
    End If
Next i
lblSearch.Caption = DisplayText
lblSearch.Tag = MyTag
End Sub

Private Function SetInventoryId() As Long
Dim i As Integer
Dim DisplayText As String
Dim CLInv As CLInventory
SetInventoryId = 0
DisplayText = ""
For i = 1 To 14
    If (Trim(SearchCriteriaSelection(i)) <> "") Then
        If (Trim(DisplayText) <> "") Then
            DisplayText = DisplayText + "And "
        End If
        DisplayText = DisplayText + "(" + SearchCriteria(i) + " = '" + Trim(SearchCriteriaSelection(i)) + "') "
    End If
Next i
Set CLInv = New CLInventory
CLInv.FieldSelect = "InventoryId"
CLInv.Criteria = DisplayText
CLInv.OrderBy = "InventoryId"
If (CLInv.FindCLInventorySingleField) Then
    SetInventoryId = Val(Trim(CLInv.SingleFieldValue))
End If
Set CLInv = Nothing
End Function

Private Sub Form_Load()
cmd(0).Top = cmd(1).Top
CurrentCriteria = 0
CurrentSelectButtonIdx = 0
TotalSelectButtonIdx = 0
SearchCriteria(1) = "Series"
SearchCriteria(2) = "Tint"
SearchCriteria(3) = "Manufacturer"
SearchCriteria(4) = "Type_"
SearchCriteria(5) = "Material"
SearchCriteria(6) = "Disposable"
SearchCriteria(7) = "WearTime"
SearchCriteria(8) = "BaseCurve"
SearchCriteria(9) = "Diameter"
SearchCriteria(10) = "SpherePower"
SearchCriteria(11) = "CylinderPower"
SearchCriteria(12) = "Axis"
SearchCriteria(13) = "AddReading"
SearchCriteria(14) = "PeriphCurve"
Erase SearchCriteriaSelection
Call ClearSelectButtons
lblSearch.Caption = ""
End Sub


Private Sub lblSearch_Change()

Dim CLInv As CLInventory
Dim DisplayText As String
Dim i As Integer
For i = 1 To 14
    If (Trim(SearchCriteriaSelection(i)) <> "") Then
        If (Trim(DisplayText) <> "") Then
            DisplayText = DisplayText + "And "
        End If
        DisplayText = DisplayText + "(" + SearchCriteria(i) + " = '" + Trim(SearchCriteriaSelection(i)) + "') "
    End If
Next i

If Trim(SearchCriteriaSelection(1)) = "" Then   '"Series"
    Set CLInv = New CLInventory
    CLInv.FieldSelect = SearchCriteria(1)
    CLInv.Criteria = DisplayText
    CLInv.OrderBy = CLInv.FieldSelect
    If CLInv.FindCLInventorySingleFieldwithTotal > 1 Then
        cmdSearch1.ForeColor = vbBlue
    Else
        If CLInv.SelectCLInventory(1) Then
            If Trim(CLInv.SingleFieldValue) = "" Then
                cmdSearch1.ForeColor = vbWhite
            Else
                cmdSearch1.ForeColor = vbBlue
            End If
        End If
    End If
    Set CLInv = Nothing
Else
    cmdSearch1.ForeColor = vbWhite
End If

If Trim(SearchCriteriaSelection(4)) = "" Then   '"Type_"
    Set CLInv = New CLInventory
    CLInv.FieldSelect = SearchCriteria(4)
    CLInv.Criteria = DisplayText
    CLInv.OrderBy = CLInv.FieldSelect
    If CLInv.FindCLInventorySingleFieldwithTotal > 1 Then
        cmdSearch4.ForeColor = vbBlue
    Else
        If CLInv.SelectCLInventory(1) Then
            If Trim(CLInv.SingleFieldValue) = "" Then
                cmdSearch4.ForeColor = vbWhite
            Else
                cmdSearch4.ForeColor = vbBlue
            End If
        End If
    End If
    Set CLInv = Nothing
Else
    cmdSearch4.ForeColor = vbWhite
End If

If Trim(SearchCriteriaSelection(7)) = "" Then   '"WearTime"
    Set CLInv = New CLInventory
    CLInv.FieldSelect = SearchCriteria(7)
    CLInv.Criteria = DisplayText
    CLInv.OrderBy = CLInv.FieldSelect
    If CLInv.FindCLInventorySingleFieldwithTotal > 1 Then
        cmdSearch1.ForeColor = vbBlue
    Else
        If CLInv.SelectCLInventory(1) Then
            If Trim(CLInv.SingleFieldValue) = "" Then
                cmdSearch7.ForeColor = vbWhite
            Else
                cmdSearch7.ForeColor = vbBlue
            End If
        End If
    End If
    Set CLInv = Nothing
Else
    cmdSearch7.ForeColor = vbWhite
End If
End Sub
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
