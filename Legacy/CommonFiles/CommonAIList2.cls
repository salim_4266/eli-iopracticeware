VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CommonAIList2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'This Class contains only routines that are called by routines in CommonAIList -- which contains
'routines called by the various application -- presumably could be made Private routines

Public Function ApplGetUnallocatedBalancebyInvoice(InvoiceId As String) As Single
Dim i As Integer
Dim k As Integer
Dim Temp As String
Dim Paid As Single
Dim BasePaid As Single
Dim Charge As Single
Dim RetRcv As PatientReceivables
Dim RetPay As PatientReceivablePayment
Dim RetTrans As PracticeTransactionJournal
Paid = 0
BasePaid = 0
ApplGetUnallocatedBalancebyInvoice = 0
If (Trim(InvoiceId) <> "") Then
    Set RetRcv = New PatientReceivables
    RetRcv.AppointmentId = 0
    RetRcv.PatientId = 0
    RetRcv.ReceivableInvoice = InvoiceId
    RetRcv.ReceivableInvoiceDate = ""
    RetRcv.InsurerId = 0
    RetRcv.ReceivableStatus = ""
    RetRcv.ReceivableType = ""
    If (RetRcv.FindPatientReceivable > 0) Then
        i = 1
        While (RetRcv.SelectPatientReceivable(i))
            If (i = 1) Then
                Charge = RetRcv.ReceivableAmount
            End If
' get payments
            Set RetPay = New PatientReceivablePayment
            RetPay.ReceivableId = RetRcv.ReceivableId
            BasePaid = BasePaid + RetPay.RetrievePatientReceivableTotalPaid
            Set RetPay = Nothing
' get sent payment requests
            Set RetTrans = New PracticeTransactionJournal
            RetTrans.TransactionJournalType = "R"
            RetTrans.TransactionJournalTypeId = RetRcv.ReceivableId
            RetTrans.TransactionJournalStatus = "-Z"
            RetTrans.TransactionJournalReference = ""
            RetTrans.TransactionJournalAction = ""
            If (RetTrans.FindTransactionJournal > 0) Then
                k = 1
                While (RetTrans.SelectTransactionJournal(k))
                    If (InStr(RetTrans.TransactionJournalRemark, "/Amt/") <> 0) Then
                        Temp = Trim(Mid(RetTrans.TransactionJournalRemark, 6, 20))
                        Paid = Paid + Round(Val(Temp), 2)
                    End If
                    k = k + 1
                Wend
            End If
            Set RetTrans = Nothing
            i = i + 1
        Wend
    End If
    Set RetRcv = Nothing
    ApplGetUnallocatedBalancebyInvoice = Round(Charge - (BasePaid + Paid), 2)
    If (ApplGetUnallocatedBalancebyInvoice < 1) And (ApplGetUnallocatedBalancebyInvoice > -1) Then
        ApplGetUnallocatedBalancebyInvoice = 0
    End If
End If
End Function
