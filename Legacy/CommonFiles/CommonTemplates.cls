VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CommonTemplates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub ResetPatientBillDate(InvId As String, LabelOn As Boolean, ALabel As Label)
Dim i As Long
Dim j As Integer, k As Integer
Dim CurDate As String
Dim RetRec As PatientReceivables
Dim RetRec1 As PatientReceivables
Dim RetTrn As PracticeTransactionJournal
Set RetRec = New PatientReceivables
RetRec.ReceivableInvoice = Chr(1)
If (Trim(InvId) <> "") Then
    RetRec.ReceivableInvoice = InvId
End If
If (RetRec.FindPatientReceivable > 0) Then
    i = 1
    While (RetRec.SelectPatientReceivableAggregate(i))
        CurDate = ""
        If (LabelOn) Then
            ALabel.Caption = "Working on ... " + Trim(RetRec.ReceivableInvoice) + " " + RetRec.ReceivablePatientBillDate
            ALabel.Visible = True
            DoEvents
        End If
        Set RetTrn = New PracticeTransactionJournal
        RetTrn.TransactionJournalType = "R"
        RetTrn.TransactionJournalTypeId = RetRec.ReceivableId
        RetTrn.TransactionJournalStatus = ""
        RetTrn.TransactionJournalReference = "P"
        If (RetTrn.FindTransactionJournal > 0) Then
            j = 1
            While (RetTrn.SelectTransactionJournal(j))
                If (CurDate > RetTrn.TransactionJournalDate) Or (CurDate = "") Then
                    CurDate = RetTrn.TransactionJournalDate
                End If
                j = j + 1
            Wend
        End If
        Set RetTrn = Nothing
        If (CurDate <> "") Then
            Set RetRec1 = New PatientReceivables
            RetRec1.ReceivableInvoice = RetRec.ReceivableInvoice
            If (RetRec1.FindPatientReceivablebyInvoice > 0) Then
                k = 1
                While (RetRec1.SelectPatientReceivable(k))
                    If (Trim(CurDate) <> "") Then
                        RetRec1.ReceivablePatientBillDate = CurDate
                        Call RetRec1.ApplyPatientReceivable
                    End If
                    k = k + 1
                Wend
            End If
            Set RetRec1 = Nothing
        End If
        i = i + 1
    Wend
End If
Set RetRec = Nothing
If (LabelOn) Then
    ALabel.Visible = False
End If
End Sub

Public Sub ResetInsurerDesignation(InvId As String, LabelOn As Boolean, ALabel As Label)
Dim i As Long, TotalRcv As Long
Dim k As Integer
Dim RetRec As PatientReceivables
Set RetRec = New PatientReceivables
RetRec.ReceivableInvoice = Chr(1)
If (Trim(InvId) <> "") Then
    RetRec.ReceivableInvoice = InvId
End If
TotalRcv = RetRec.FindPatientReceivable
If (TotalRcv > 0) Then
    i = 1
    While (RetRec.SelectPatientReceivableAggregate(i))
        If (LabelOn) Then
            ALabel.Caption = "Working on ... " + Trim(RetRec.ReceivableInvoice) + Str(i) + " of " + Str(TotalRcv)
            ALabel.Visible = True
            DoEvents
        End If
        k = 0
        Call ApplGetNextPayerPartyRef(RetRec.PatientId, RetRec.ReceivableInvoice, RetRec.ReceivableInvoiceDate, "", RetRec.InsurerId, k)
        If (k > 0) Then
            RetRec.ReceivableInsurerDesignation = "F"
            If (k = 1) Then
                RetRec.ReceivableInsurerDesignation = "T"
            End If
            Call RetRec.ApplyPatientReceivable
        End If
        i = i + 1
    Wend
End If
Set RetRec = Nothing
If (LabelOn) Then
    ALabel.Visible = False
End If
End Sub

Public Sub ApplGetNextPayerPartyRef(PatId As Long, InvNum As String, SrvDate As String, InsTy As String, InsId As Long, ARef As Integer)
Dim i As Integer
Dim InsIdA As Long, AInsId As Long
Dim i1 As Long, i2 As Long, i3 As Long
Dim i4 As Long, i5 As Long, i6 As Long
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset

ARef = 0
If (Trim(InsTy) = "") Then
    InsTy = "M"
End If
If (PatId > 0) And (Trim(InvNum) <> "") And (InsId > 0) Then
    Call ApplGetAllPatientPayers(PatId, InsTy, SrvDate, i1, i2, i3, i4, i5, i6, False)
    AInsId = 0
    InsIdA = i1
    GoSub ProcGet
    If (InsId = AInsId) Then
        ARef = 1
    Else
        AInsId = 0
        InsIdA = i2
        GoSub ProcGet
        If (InsId = AInsId) Then
            ARef = 2
        Else
            AInsId = 0
            InsIdA = i3
            GoSub ProcGet
            If (InsId = AInsId) Then
                ARef = 3
            Else
                AInsId = 0
                InsIdA = i4
                GoSub ProcGet
                If (InsId = AInsId) Then
                    ARef = 4
                Else
                    AInsId = 0
                    InsIdA = i5
                    GoSub ProcGet
                    If (InsId = AInsId) Then
                        ARef = 5
                    Else
                        AInsId = 0
                        InsIdA = i6
                        GoSub ProcGet
                        If (InsId = AInsId) Then
                            ARef = 6
                        End If
                    End If
                End If
            End If
        End If
    End If
End If
Exit Sub
ProcGet:
    If (InsIdA > 0) Then
        Set RetPatientFinancialService = New PatientFinancialService
        RetPatientFinancialService.PatientInsuranceId = InsIdA
        Set RS = RetPatientFinancialService.RetrievePatientInsurer
        If (Not RS Is Nothing And RS.RecordCount > 0) Then
            AInsId = RS("InsurerId")
        End If
        Set RetPatientFinancialService = Nothing
    End If
    Return
End Sub

Public Sub ApplGetAllPatientPayers(PatId As Long, InsTy As String, AptDate As String, i1 As Long, i2 As Long, i3 As Long, i4 As Long, i5 As Long, i6 As Long, IgnoreType As Boolean)
Dim AUse As Boolean
Dim PolId As Long, SPolId As Long
Dim i As Integer
Dim k As Integer, p As Integer
Dim Stack(24) As String
Dim MyStack(24) As Long
Dim w As Integer
Dim n1 As String, n2 As String
Dim n3 As String, n4 As String, n5 As String
Dim IODateTime As New CIODateTime
Dim RetPat As Patient
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
i1 = 0
i2 = 0
i3 = 0
i4 = 0
i5 = 0
i6 = 0
If (Trim(InsTy) = "") Or (IgnoreType) Then
    InsTy = "M"
End If
Erase Stack
Erase MyStack
If AptDate <> "" Then
    IODateTime.SetDateFromIO AptDate
Else
    IODateTime.MyDateTime = Now
End If
If (PatId > 0) Then
    Set RetPat = New Patient
    RetPat.PatientId = PatId
    If (RetPat.RetrievePatient) Then
        PolId = RetPat.PolicyPatientId
        If (PolId < 1) Then
            PolId = PatId
        End If
        SPolId = RetPat.SecondPolicyPatientId
    End If
    If (IgnoreType) Then
        InsTy = "M"
        GoSub GetInsRecs
        If (i1 > 0) Then
            MyStack(1) = i1
        End If
        If (i2 > 0) Then
            MyStack(2) = i2
        End If
        If (i3 > 0) Then
            MyStack(3) = i3
        End If
        If (i4 > 0) Then
            MyStack(4) = i4
        End If
        If (i5 > 0) Then
            MyStack(5) = i5
        End If
        If (i6 > 0) Then
            MyStack(6) = i6
        End If
        i1 = 0
        i2 = 0
        i3 = 0
        i4 = 0
        i5 = 0
        i6 = 0
        Erase Stack
        InsTy = "V"
        GoSub GetInsRecs
        If (i1 > 0) Then
            MyStack(7) = i1
        End If
        If (i2 > 0) Then
            MyStack(8) = i2
        End If
        If (i3 > 0) Then
            MyStack(9) = i3
        End If
        If (i4 > 0) Then
            MyStack(10) = i4
        End If
        If (i5 > 0) Then
            MyStack(11) = i5
        End If
        If (i6 > 0) Then
            MyStack(12) = i6
        End If
        i1 = 0
        i2 = 0
        i3 = 0
        i4 = 0
        i5 = 0
        i6 = 0
        Erase Stack
        InsTy = "A"
        GoSub GetInsRecs
        If (i1 > 0) Then
            MyStack(13) = i1
        End If
        If (i2 > 0) Then
            MyStack(14) = i2
        End If
        If (i3 > 0) Then
            MyStack(15) = i3
        End If
        If (i4 > 0) Then
            MyStack(16) = i4
        End If
        If (i5 > 0) Then
            MyStack(17) = i5
        End If
        If (i6 > 0) Then
            MyStack(18) = i6
        End If
        i1 = 0
        i2 = 0
        i3 = 0
        i4 = 0
        i5 = 0
        i6 = 0
        Erase Stack
        InsTy = "W"
        GoSub GetInsRecs
        If (i1 > 0) Then
            MyStack(19) = i1
        End If
        If (i2 > 0) Then
            MyStack(20) = i2
        End If
        If (i3 > 0) Then
            MyStack(21) = i3
        End If
        If (i4 > 0) Then
            MyStack(22) = i4
        End If
        If (i5 > 0) Then
            MyStack(23) = i5
        End If
        If (i6 > 0) Then
            MyStack(24) = i6
        End If
        p = 0
        i1 = 0
        i2 = 0
        i3 = 0
        i4 = 0
        i5 = 0
        i6 = 0
        For i = 1 To 24
            If (MyStack(i) > 0) Then
                p = p + 1
                If (p = 1) Then
                    i1 = MyStack(i)
                ElseIf (p = 2) Then
                    i2 = MyStack(i)
                ElseIf (p = 3) Then
                    i3 = MyStack(i)
                ElseIf (p = 4) Then
                    i4 = MyStack(i)
                ElseIf (p = 5) Then
                    i5 = MyStack(i)
                ElseIf (p = 6) Then
                    i6 = MyStack(i)
                    Exit For
                End If
            End If
        Next i
    Else
        GoSub GetInsRecs
    End If
End If
Set IODateTime = Nothing
Exit Sub

GetInsRecs:
    Set RetPatientFinancialService = New PatientFinancialService
    RetPatientFinancialService.InsuredPatientId = PatId
    If (PolId > 0) Then
        RetPatientFinancialService.InsuredPatientId = PolId
    End If
    RetPatientFinancialService.StartDateTime = IODateTime.GetIODate
    RetPatientFinancialService.InsuranceTypeId = InsTy
    Set RS = RetPatientFinancialService.GetPatientInsurerInfo
    Set RetPatientFinancialService = Nothing
    While (Not RS.EOF)
        If (RS("EndDateTime") = "") Or (RS("EndDateTime") >= IODateTime.GetIODate) Then
            Call ApplGetPlanName(RS("InsurerId"), n1, n2, n3, n4, n5, AUse)
            If (PatId = PolId) And Not (AUse) Then
                AUse = True
            End If
            If (RS("OrdinalId") >= 1) And (RS("OrdinalId") <= 3) Then
                If (AUse) Or (PolId = PatId) Then
                    For w = 1 To p
                        If (Val(Stack(w)) = RS("Id")) Then
                            Exit For
                        End If
                    Next w
                    If (w = p + 1) Then
                        p = p + 1
                        Stack(p) = Trim(Str(RS("Id")))
                    End If
                End If
            End If
        End If
        RS.MoveNext
    Wend
    If (PatId <> PolId) And (PolId > 0) Then
        Set RetPatientFinancialService = New PatientFinancialService
        RetPatientFinancialService.InsuredPatientId = PatId
        RetPatientFinancialService.StartDateTime = IODateTime.GetIODate
        RetPatientFinancialService.InsuranceTypeId = InsTy
        Set RS = RetPatientFinancialService.GetPatientInsurerInfo
        Set RetPatientFinancialService = Nothing
        While (Not RS.EOF)
            If (RS("EndDateTime") = "") Or (RS("EndDateTime") >= IODateTime.GetIODate) Then
                    Call ApplGetPlanName(RS("InsurerId"), n1, n2, n3, n4, n5, AUse)
                    If (PatId = SPolId) And Not (AUse) Then
                        AUse = True
                    End If
                    If (RS("OrdinalId") >= 1) And (RS("OrdinalId") <= 3) Then
                        If (AUse) Or (PolId = PatId) Then
                            For w = 1 To p
                                If (Val(Stack(w)) = RS("Id")) Then
                                    Exit For
                                End If
                            Next w
                            If (w = p + 1) Then
                                p = p + 1
                                Stack(p) = Trim(Str(RS("Id")))
                            End If
                        End If
                    End If
                End If
            RS.MoveNext
        Wend
    End If
    If (SPolId > 0) Then
        Set RetPatientFinancialService = New PatientFinancialService
        RetPatientFinancialService.InsuredPatientId = SPolId
        RetPatientFinancialService.StartDateTime = IODateTime.GetIODate
        RetPatientFinancialService.InsuranceTypeId = InsTy
        Set RS = RetPatientFinancialService.GetPatientInsurerInfo
        Set RetPatientFinancialService = Nothing
        While (Not RS.EOF)
            If (RS("EndDateTime") = "") Or (RS("EndDateTime") >= IODateTime.GetIODate) Then
                Call ApplGetPlanName(RS("InsurerId"), n1, n2, n3, n4, n5, AUse)
                If (RS("OrdinalId") >= 1) And (RS("OrdinalId") <= 3) Then
                    If (AUse) Or (SPolId = PatId) Then
                        For w = 1 To p
                            If (Val(Stack(w)) = RS("Id")) Then
                                Exit For
                            End If
                        Next w
                        If (w = p + 1) Then
                            p = p + 1
                            Stack(p) = Trim(Str(RS("Id")))
                        End If
                    End If
                End If
            End If
            RS.MoveNext
        Wend
        Set RetPatientFinancialService = Nothing
    End If
    Set RetPat = Nothing
    i = 0
    For k = 1 To 6
        If (Trim(Stack(k)) <> "") Then
            i = i + 1
            If (i = 1) Then
                i1 = Val(Stack(k))
            ElseIf (i = 2) Then
                i2 = Val(Stack(k))
            ElseIf (i = 3) Then
                i3 = Val(Stack(k))
            ElseIf (i = 4) Then
                i4 = Val(Stack(k))
            ElseIf (i = 5) Then
                i5 = Val(Stack(k))
            ElseIf (i = 6) Then
                i6 = Val(Stack(k))
            End If
        End If
    Next k
    Return
End Sub

Public Sub ApplGetPlanName(RefId As Long, TheInsName As String, TheName As String, TheDName As String, TheNeic As String, TheEType As String, TheUse As Boolean)
On Error GoTo lApplGetPlanName_Error
TheDName = ""
TheInsName = ""
TheName = ""
TheNeic = ""
Dim RetPatientFinancialService As PatientFinancialService
Dim RS As Recordset
Dim RSInsDet As Recordset
TheDName = ""
TheInsName = ""
TheName = ""
TheNeic = ""
Set RetPatientFinancialService = New PatientFinancialService
RetPatientFinancialService.InsurerId = RefId
Set RS = RetPatientFinancialService.RetrieveInsurer
Set RSInsDet = RetPatientFinancialService.GetInsurerDetails
If (Not RS Is Nothing And RS.RecordCount > 0) Then
    TheInsName = Trim(RS("Name"))
    TheName = Trim(RS("Name")) + " " _
            + Trim(Left(RS("PlanName"), 20))
    If (Not RSInsDet And RSInsDet.RecordCount > 0) Then
        TheDName = Trim(RS("Name")) & " " & Trim(Left(RSInsDet("InsurerAddress"), 20))
        TheNeic = Trim(RSInsDet("NEICNumber"))
        TheEType = Trim(RSInsDet("InsurerTFormat"))
    Else
        TheDName = Trim(RS("Name")) & " "
        TheNeic = ""
        TheEType = ""
    End If
    TheUse = False
    If (RS("OutOfPocket") = 0) Then
        TheUse = True
    End If
End If
Set RetPatientFinancialService = Nothing
Exit Sub
lApplGetPlanName_Error:
    LogError "CommonTemplates", "ApplGetPlanName", Err, Err.Description
End Sub

Public Function ApplGetPatientName(RefId As Long, TheName As String, Payer1 As Long, Rel1 As String, Bill1 As Boolean, Payer2 As Long, Rel2 As String, Bill2 As Boolean, IHow As Boolean) As Boolean
Dim RetrievePat As Patient
TheName = ""
Payer1 = 0
Rel1 = ""
Bill1 = False
Payer2 = 0
Rel2 = ""
Bill2 = False
ApplGetPatientName = False
If (RefId > 0) Then
    Set RetrievePat = New Patient
    RetrievePat.PatientId = RefId
    If (RetrievePat.RetrievePatient) Then
        If (IHow) Then
            TheName = Trim(Trim(RetrievePat.LastName) + " " + Trim(RetrievePat.FirstName) + " " + Trim(RetrievePat.MiddleInitial))
        Else
            TheName = Trim(RetrievePat.FirstName) + " " + Trim(RetrievePat.MiddleInitial) + " " _
                    + Trim(RetrievePat.LastName) + " " + Trim(RetrievePat.NameRef)
        End If
        If (RetrievePat.PolicyPatientId > 0) Then
            Payer1 = RetrievePat.PolicyPatientId
            Rel1 = RetrievePat.Relationship
            Bill1 = RetrievePat.BillParty
        Else
            Payer1 = RefId
        End If
        If (RetrievePat.SecondPolicyPatientId > 0) Then
            Payer2 = RetrievePat.SecondPolicyPatientId
            Rel2 = RetrievePat.SecondRelationship
            Bill2 = RetrievePat.SecondBillParty
        End If
        ApplGetPatientName = True
    End If
    Set RetrievePat = Nothing
End If
End Function
