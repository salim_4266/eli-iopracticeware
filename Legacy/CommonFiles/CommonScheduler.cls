VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CommonScheduler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function IsReferralRequiredbyPatient(PatId As Long) As Boolean
    Dim InsId As Long
    Dim APatId As Long
    Dim RetPat As Patient
    Dim RetFin As PatientFinance
    Dim RetrieveInsurer As Insurer
    
    IsReferralRequiredbyPatient = False
    InsId = 0
    APatId = 0
    If (PatId > 0) Then
        Set RetPat = New Patient
        RetPat.PatientID = PatId
        If (RetPat.RetrievePatient) Then
            If (RetPat.PolicyPatientId > 0) Then
                APatId = RetPat.PolicyPatientId
            Else
                APatId = PatId
            End If
        End If
        Set RetPat = Nothing
        If (APatId > 0) Then
            Set RetFin = New PatientFinance
            RetFin.PatientID = APatId
            If (RetFin.RetrievePatientFinancialPrimary) Then
                InsId = RetFin.PrimaryInsurerId
            End If
            Set RetFin = Nothing
        End If
        If (InsId > 0) Then
            Set RetrieveInsurer = New Insurer
            RetrieveInsurer.InsurerId = InsId
            If (RetrieveInsurer.RetrieveInsurer) Then
                IsReferralRequiredbyPatient = RetrieveInsurer.InsurerReferralRequired
            End If
            Set RetrieveInsurer = Nothing
        End If
    End If
End Function

