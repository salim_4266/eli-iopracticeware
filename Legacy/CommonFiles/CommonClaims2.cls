VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CommonClaims2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'This Class contains only routines that are called by routines in CommonClaims -- which contains
'routines called by the various application -- presumably could be made Private routines

Public Function GetServiceCost(SrvCode As String, InsId As Long, DrId As Long, LocId As Long, _
    TheDate As String, TOS As String, POS As String, Fee As Single) As Single
    
    Dim RetRes As SchedulerResource
    Dim RetService As Service
    Dim RetFee As PracticeFeeSchedules
    
    GetServiceCost = 0
    Fee = 0
    TOS = ""
    POS = ""
    Set RetService = New Service
    RetService.Service = SrvCode
    RetService.ServiceType = "P"
    If (RetService.RetrieveService) Then
        TOS = Left(RetService.ServiceTOS, 2)
        If (Trim(TOS) = "") Then
            TOS = "01"
        End If
        POS = Left(RetService.ServicePOS, 2)
        If (Trim(POS) = "") Then
            POS = "11"
        End If
        GetServiceCost = RetService.ServiceFee
        Fee = RetService.ServiceFee
    End If
    Set RetService = Nothing
    If (LocId < 1) Then
        POS = "11"
    Else
        Set RetRes = New SchedulerResource
        RetRes.ResourceId = LocId
        If (RetRes.RetrieveSchedulerResource) Then
            If (Trim(RetRes.ResourcePOS) <> "") Then
                POS = RetRes.ResourcePOS
            End If
        End If
        Set RetRes = Nothing
    End If
    If (DrId > 0) Then
        Set RetFee = New PracticeFeeSchedules
        RetFee.SchService = SrvCode
        RetFee.SchStartDate = TheDate
        RetFee.SchEndDate = ""
        RetFee.SchDoctorId = DrId
        RetFee.SchLocId = LocId
        RetFee.SchPlanId = -1
        If (InsId > 0) Then
            RetFee.SchPlanId = InsId
        End If
        If (RetFee.FindServiceDirect > 0) Then
            If (RetFee.SelectService(1)) Then
                If (RetFee.SchEndDate = "") Or (RetFee.SchEndDate >= TheDate) Then
                    If (RetFee.SchAdjustmentAllowed <= 0) Then
                        GetServiceCost = RetFee.SchFee
                    Else
                        GetServiceCost = RetFee.SchAdjustmentAllowed
                    End If
                    If (LocId > 0) And (LocId < 1000) Then
                        If (RetFee.SchFacFee > 0) Then
                            GetServiceCost = RetFee.SchFacFee
                            If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                                GetServiceCost = RetFee.SchFee
                            End If
                        End If
                    Else
                        If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                            GetServiceCost = RetFee.SchFee
                        End If
                    End If
                    Fee = RetFee.SchAdjustmentAllowed
                End If
            End If
        Else
            Set RetFee = Nothing
            Set RetFee = New PracticeFeeSchedules
            RetFee.SchService = SrvCode
            RetFee.SchStartDate = TheDate
            RetFee.SchEndDate = ""
            RetFee.SchDoctorId = -1
            RetFee.SchLocId = LocId
            RetFee.SchPlanId = -1
            If (InsId > 0) Then
                RetFee.SchPlanId = InsId
            End If
            If (RetFee.FindServiceDirect > 0) Then
                If (RetFee.SelectService(1)) Then
                    If (RetFee.SchEndDate = "") Or (RetFee.SchEndDate >= TheDate) Then
                        If (RetFee.SchAdjustmentAllowed <= 0) Then
                            GetServiceCost = RetFee.SchFee
                        Else
                            GetServiceCost = RetFee.SchAdjustmentAllowed
                        End If
                        If (LocId > 0) And (LocId < 1000) Then
                            If (RetFee.SchFacFee > 0) Then
                                GetServiceCost = RetFee.SchFacFee
                                If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                                    GetServiceCost = RetFee.SchFee
                                End If
                            End If
                        Else
                            If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                                GetServiceCost = RetFee.SchFee
                            End If
                        End If
                        Fee = RetFee.SchAdjustmentAllowed
                    End If
                End If
            Else
                Set RetFee = Nothing
                Set RetFee = New PracticeFeeSchedules
                RetFee.SchService = SrvCode
                RetFee.SchStartDate = TheDate
                RetFee.SchEndDate = ""
                RetFee.SchDoctorId = -1
                RetFee.SchLocId = -1
                RetFee.SchPlanId = -1
                If (InsId > 0) Then
                    RetFee.SchPlanId = InsId
                End If
                If (RetFee.FindServiceDirect > 0) Then
                    If (RetFee.SelectService(1)) Then
                        If (RetFee.SchEndDate = "") Or (RetFee.SchEndDate >= TheDate) Then
                            If (RetFee.SchAdjustmentAllowed <= 0) Then
                                GetServiceCost = RetFee.SchFee
                            Else
                                GetServiceCost = RetFee.SchAdjustmentAllowed
                            End If
                            If (LocId > 0) And (LocId < 1000) Then
                                If (RetFee.SchFacFee > 0) Then
                                    GetServiceCost = RetFee.SchFacFee
                                    If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                                        GetServiceCost = RetFee.SchFee
                                    End If
                                End If
                            Else
                                If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                                    GetServiceCost = RetFee.SchFee
                                End If
                            End If
                            Fee = RetFee.SchAdjustmentAllowed
                        End If
                    End If
                End If
            End If
        End If
        Set RetFee = Nothing
    Else
        Set RetFee = New PracticeFeeSchedules
        RetFee.SchService = SrvCode
        RetFee.SchStartDate = TheDate
        RetFee.SchEndDate = ""
        RetFee.SchDoctorId = -1
        RetFee.SchLocId = LocId
        RetFee.SchPlanId = -1
        If (InsId > 0) Then
            RetFee.SchPlanId = InsId
        End If
        If (RetFee.FindServiceDirect > 0) Then
            If (RetFee.SelectService(1)) Then
                If (RetFee.SchEndDate = "") Or (RetFee.SchEndDate >= TheDate) Then
                    If (RetFee.SchAdjustmentAllowed <= 0) Then
                        GetServiceCost = RetFee.SchFee
                    Else
                        GetServiceCost = RetFee.SchAdjustmentAllowed
                    End If
                    If (LocId > 0) And (LocId < 1000) Then
                        If (RetFee.SchFacFee > 0) Then
                            GetServiceCost = RetFee.SchFacFee
                            If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                                GetServiceCost = RetFee.SchFee
                            End If
                        End If
                    Else
                        If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                            GetServiceCost = RetFee.SchFee
                        End If
                    End If
                    Fee = RetFee.SchAdjustmentAllowed
                End If
            End If
        Else
            Set RetFee = Nothing
            Set RetFee = New PracticeFeeSchedules
            RetFee.SchService = SrvCode
            RetFee.SchStartDate = TheDate
            RetFee.SchEndDate = ""
            RetFee.SchDoctorId = -1
            RetFee.SchLocId = -1
            RetFee.SchPlanId = -1
            If (InsId > 0) Then
                RetFee.SchPlanId = InsId
            End If
            If (RetFee.FindServiceDirect > 0) Then
                If (RetFee.SelectService(1)) Then
                    If (RetFee.SchEndDate = "") Or (RetFee.SchEndDate >= TheDate) Then
                        If (RetFee.SchAdjustmentAllowed <= 0) Then
                            GetServiceCost = RetFee.SchFee
                        Else
                            GetServiceCost = RetFee.SchAdjustmentAllowed
                        End If
                        If (LocId > 0) And (LocId < 1000) Then
                            If (RetFee.SchFacFee > 0) Then
                                GetServiceCost = RetFee.SchFacFee
                                If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                                    GetServiceCost = RetFee.SchFee
                                End If
                            End If
                        Else
                            If (RetFee.SchAdjustmentAllowed <> RetFee.SchFee) Then
                                GetServiceCost = RetFee.SchFee
                            End If
                        End If
                        Fee = RetFee.SchAdjustmentAllowed
                    End If
                End If
            End If
        End If
        Set RetFee = Nothing
    End If
End Function

