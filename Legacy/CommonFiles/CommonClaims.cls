VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CommonClaims"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub SetReceivableCosts(RcvId As Long, RefId As Long, BillDr As Long, VfyFee As Boolean, BillOff As Long, PDate As String)
Dim i As Integer, u As Integer
Dim k As Integer, p As Integer
Dim InvId As String, Temp As String
Dim TransPaid As Single
Dim Paid As Single, IBal As Single
Dim Fees As Single, Bal As Single
Dim Costs As Single, Una As Single
Dim ACost As Single, AFee As Single
Dim ALocId As Long, APlnId As Long
Dim ASDate As String
Dim APOS As String, ATOS As String
Dim RetPay As PatientReceivablePayment
Dim RetSrv As PatientReceivableService
Dim RetRec As PatientReceivables
Dim RetRec1 As PatientReceivables
Dim RetApt As SchedulerAppointment
Dim RetTrans As PracticeTransactionJournal
Dim ComClm2 As CommonClaims2
'Dim ApplList As ApplicationAIList

If (RcvId > 0) Then
    Paid = 0
    Costs = 0
    Fees = 0
    Una = 0
    Bal = 0
    p = 0
    TransPaid = 0
    InvId = ""
'    Set ApplList = New ApplicationAIList
    Set RetRec = New PatientReceivables
    RetRec.ReceivableId = RcvId
    If (RetRec.RetrievePatientReceivable) Then
        InvId = RetRec.ReceivableInvoice
        If (VfyFee) And (RetRec.ReceivableInsurerDesignation = "T") Then
            GoSub VerifyFeeAmounts
        End If
        Set RetSrv = New PatientReceivableService
        RetSrv.Invoice = RetRec.ReceivableInvoice
        Costs = RetSrv.RetrievePatientReceivableTotalCharge
        Fees = RetSrv.RetrievePatientReceivableFeeCharge
        Set RetRec = Nothing
        Set RetRec = New PatientReceivables
        RetRec.ReceivableInvoice = InvId
        If (RetRec.FindPatientReceivable > 0) Then
            i = 1
            While (RetRec.SelectPatientReceivable(i))
                GoSub GetTransCnt
                RetRec.ReceivableAmount = Costs
                RetRec.ReceivableFeeAmount = Fees
                If (Fees = 0) Then
                    RetRec.ReceivableFeeAmount = Costs
                End If
                RetRec.ReceivableRefDr = RefId
                RetRec.ReceivableBillToDr = BillDr
                RetRec.ReceivableBillOffice = BillOff
                If (PDate <> "") Then
                    RetRec.ReceivableLastPayDate = PDate
                End If
                Call RetRec.ApplyPatientReceivable
                Set RetPay = New PatientReceivablePayment
                RetPay.ReceivableId = RetRec.ReceivableId
                RetPay.PaymentPayerId = 0
                RetPay.PaymentPayerType = ""
                Paid = Paid + RetPay.RetrievePatientReceivableTotalPaid
                Set RetPay = Nothing
                i = i + 1
            Wend
            Bal = Costs - Paid
            If (Costs = Paid) Then
                Bal = 0
            End If
            Una = Costs - (Paid + TransPaid)
            Una = (Int((Una + 0.004) * 100)) / 100
            If (Una < 1) And (Una > -1) Then
                Una = 0
            End If
            i = 1
            While (RetRec.SelectPatientReceivable(i))
                RetRec.ReceivableBalance = Bal
                RetRec.ReceivableUnAllocated = Una
                Call RetRec.ApplyPatientReceivable
                i = i + 1
            Wend
        End If
    End If
'    Set ApplList = Nothing
    Set RetSrv = Nothing
    Set RetRec = Nothing
End If
Exit Sub
GetTransCnt:
    Set RetTrans = New PracticeTransactionJournal
    RetTrans.TransactionJournalType = "R"
    RetTrans.TransactionJournalTypeId = RetRec.ReceivableId
    RetTrans.TransactionJournalStatus = "-Z"
    RetTrans.TransactionJournalReference = ""
    RetTrans.TransactionJournalAction = ""
    If (RetTrans.FindTransactionJournal > 0) Then
        k = 1
        While (RetTrans.SelectTransactionJournal(k))
            If (InStr(RetTrans.TransactionJournalRemark, "/Amt/") <> 0) Then
                Temp = Trim(Mid(RetTrans.TransactionJournalRemark, 6, 20))
                TransPaid = TransPaid + Val(Temp)
                TransPaid = (Int((TransPaid + 0.004) * 100)) / 100
            End If
            k = k + 1
        Wend
    End If
    Set RetTrans = Nothing
    Return
VerifyFeeAmounts:
    If (Trim(InvId) <> "") Then
        ALocId = -1
        Set ComClm2 = New CommonClaims2
        Set RetApt = New SchedulerAppointment
        RetApt.AppointmentId = RetRec.AppointmentId
        If (RetApt.RetrieveSchedulerAppointment) Then
            ALocId = RetApt.AppointmentResourceId2
        End If
        Set RetApt = Nothing
        If (ALocId < 1) Then
            ALocId = -1
        End If
        APlnId = RetRec.InsurerId
        Set RetSrv = New PatientReceivableService
        RetSrv.Invoice = InvId
        If (RetSrv.FindPatientReceivableService > 0) Then
            u = 1
            While (RetSrv.SelectPatientReceivableService(u))
                If (RetSrv.ServiceStatus <> "X") Then
                    ASDate = RetSrv.ServiceDate
                    ATOS = RetSrv.ServiceTOS
                    APOS = RetSrv.ServicePOS
                    ACost = ComClm2.GetServiceCost(RetSrv.Service, APlnId, -1, ALocId, ASDate, ATOS, APOS, AFee)
                    If (InStr(RetSrv.ServiceModifier, "50") > 0) And (RetSrv.ServiceQuantity = 1) Then
                        AFee = AFee * 2
                    End If
                    If (AFee <= 0) Then
                        AFee = RetSrv.ServiceCharge
                    End If
                    RetSrv.ServiceFeeCharge = AFee
                    Call RetSrv.ApplyPatientReceivableService
                End If
                u = u + 1
            Wend
        End If
        Set RetSrv = Nothing
    End If
    Return
End Sub
