VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CommonTables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public lstBox As ListBox
Public lstBoxA As ComboBox

Public Function ApplGetPractice(PrcId As Long, RefId As String) As Boolean
Dim i As Integer
Dim RetPrc As PracticeName
PrcId = 0
ApplGetPractice = False
If (Trim(RefId) <> "") Then
    Set RetPrc = New PracticeName
    RetPrc.PracticeType = "P"
    RetPrc.PracticeName = Chr(1)
    If (RetPrc.FindPractice > 0) Then
        i = 1
        Do Until Not (RetPrc.SelectPractice(i))
            If (InStr(RetPrc.PracticeLocationReference, RefId) > 0) Then
                PrcId = Trim(RetPrc.PracticeId)
                ApplGetPractice = True
                Exit Do
            End If
            i = i + 1
        Loop
    End If
    Set RetPrc = Nothing
End If
End Function

Public Function ApplLoadCodes(CodeType As String, AllOn As Boolean, BothFields As Boolean, _
    BoxType As Boolean) As Boolean

Dim k As Integer
Dim DisplayText As String
Dim RetrieveCode As PracticeCodes
ApplLoadCodes = False
Set RetrieveCode = New PracticeCodes
If (BoxType) Then
    lstBox.Clear
    If (AllOn) Then
        lstBox.AddItem " Create a new code"
    End If
Else
    lstBoxA.Clear
End If
RetrieveCode.ReferenceType = Trim(CodeType)
RetrieveCode.ReferenceCode = ""
If (RetrieveCode.FindCode > 0) Then
    k = 1
    Do Until (Not (RetrieveCode.SelectCode(k)))
        DisplayText = Space(512)
        If (BothFields) Then
            Mid(DisplayText, 1, Len(Trim(RetrieveCode.ReferenceCode))) = Trim(RetrieveCode.ReferenceCode)
            Mid(DisplayText, 65, Len(Trim(RetrieveCode.ReferenceAlternateCode))) = Trim(RetrieveCode.ReferenceAlternateCode)
            Mid(DisplayText, 128, Len(Trim(RetrieveCode.LetterLanguage))) = Trim(RetrieveCode.LetterLanguage)
            Mid(DisplayText, 240, 7) = Trim(Str(RetrieveCode.Rank))
            Mid(DisplayText, 248, 1) = " "
            If (RetrieveCode.FollowUp = "F") Then
                Mid(DisplayText, 248, 1) = "F"
            End If
            Mid(DisplayText, 249, Len(Trim(RetrieveCode.TestOrder))) = Trim(RetrieveCode.TestOrder)
            Mid(DisplayText, 254, 1) = "F"
            If (RetrieveCode.Signature = "T") Then
                Mid(DisplayText, 254, 1) = "T"
            End If
            Mid(DisplayText, 255, 1) = "F"
            If (RetrieveCode.Exclusion = "T") Then
                Mid(DisplayText, 255, 1) = "T"
            End If
            Mid(DisplayText, 256, Len(Trim(RetrieveCode.LetterLanguage1))) = Trim(RetrieveCode.LetterLanguage1)
        Else
            Mid(DisplayText, 1, Len(Trim(RetrieveCode.ReferenceAlternateCode))) = Trim(RetrieveCode.ReferenceAlternateCode)
        End If
        If (BoxType) Then
            lstBox.AddItem DisplayText
        Else
            lstBoxA.AddItem DisplayText
        End If
        k = k + 1
    Loop
    ApplLoadCodes = True
End If
Set RetrieveCode = Nothing
End Function

