Attribute VB_Name = "CommonLib"

Option Explicit

Public UserLogin As CUserLogin

Public Type ConfigType
    Name As String
    Value As String
End Type

Public pdf As Object

Public MySystemName As String
Public GblDevice As String
Public LclDevice As String
Public MaxGlobalConfigEntries As Integer
Public PinpointDatabaseOpen As Boolean
Private mMyPracticeRepository As AdoConnection
Private mMyPracticeRepositoryCmd As ADODB.Command
Public MyDynamicForms As ADODB.Connection
Public MyDynamicFormsCmd As ADODB.Command
Public MyDiagnosisMaster As ADODB.Connection
Public MyDiagnosisMasterCmd As ADODB.Command
Public ConfigCollection As Collection

Public sServerSQL As String
Public sDatabase As String
Public sDatabaseCurr As String

Public dfPrinter As String
Public dfPrinterDriver As String
Public dfPrinterOrientation As Integer

Public tmTimer As Integer
' PracticeId either 0 or +1000
Public dbPracticeId As Long
Public dbSoftwareLocation As String
' Actual PracticeId as it appears in PracticeName table.
Public iPracticeID As Long
Public dbTerminal As String
Public dbConnection As String

Public dbDiagnosisMaster As String
Public dbDynamicForms As String
Public PinPointDirectory As String
Public LocalPinPointDirectory As String
Public ImpressionNotesDirectory As String
Public PatientInterfaceDirectory As String
Public DoctorInterfaceDirectory As String
Public ServerDoctorInterfaceDirectory As String
Public RemitDirectory As String
Public FeeDirectory As String
Public ScanDirectory As String
Public MyScanDirectory As String
Public DocumentDirectory As String
Public SchedulerInterfaceDirectory As String
Public SubmissionsDirectory As String
Public SubmissionsAcksDirectory As String
Public ReportsDirectory As String
Public TemplateDirectory As String
Public sClinImportDir As String
Public ImageDir As String
Public MyImageDir As String
Public LocalMyImageDir As String
Public BtnDir As String
Public PrimaryVerifyLog As String
Public SecondVerifyLog As String

Public AdministratorVersionId As String
Public PatientInterfaceVersionId As String
Public DoctorInterfaceVersionId As String
Public ToolsVersionId As String

Private m_ActivePatientId As Long
Private m_ActiveAppointmentId As Long

Public Const EqualsNumberRegex As String = "\s*=\s*[0-9]+"

' For killing processes ONLY
Public Const MAX_MODULE_NAME32 As Integer = 255
Public Const MAX_PATH As Integer = 260
Public Const TH32CS_SNAPHEAPLIST As Long = 1&
Public Const TH32CS_SNAPPROCESS As Long = 2&
Public Const TH32CS_SNAPTHREAD As Long = 4&
Public Const TH32CS_SNAPMODULE As Long = 8&

Public Enum EPermissions
    epCheckIn = 1
    epCheckOut = 2
    epReview = 5
    epPatientClinicalData = 6
    epWaitingForQuestionnaire = 7
    epWaitingRoom = 8
    epExamRoom = 9
    epOfficeMonitor = 10
    epSubmit = 12
    epBookPayables = 14
    epClinicalExchange = 15
    epWritePrescriptions = 21
    epPatientLocator = 22
    epContacts = 23
    epPracticeNotes = 24
    epPatientNotes = 25
    epSetupPractice = 33
    epSetupResources = 34
    epSetupAppointmentTypes = 35
    epSetupConfiguration = 38
    epSetupVendors = 40
    epSetupPlans = 41
    epSetupFavorites = 42
    epSetupNotesActionsTests = 43
    epSetupFeeSchedules = 44
    epSetupCodes = 45
    epSetupReferringDoctors = 46
    epSetupTypeOfService = 47
    epSetupPlaceOfService = 48
    epSetupServices = 49
    epSetupServiceCodes = 50
    epSetupDiagnosis = 51
    epAllowAggregateInsurerChanges = 53
    epBillingNotes = 54
    epSetupOtherFavorites = 55
    epPowerUser = 62
    epEditPreviousVisit = 64
    epSetAlerts = 65
    epeRx = 69
    epCancelAppointmentFromExam = 70
    'Changes Made for Access Control
    epOpenChart = 100
    epExamElements = 101
    epHistoryAndAllergies = 102
    epMeds = 103
    epFindings = 104
    epPlan = 105
    epPastVisits = 106
    epFollowUpAndSurgery = 108
    epExamCheckOut = 109
    epPatientAccess = 110
End Enum

Public Enum LoginCatg
    Login = 1
    LogOut = 2
    AppExit = 3
    AppOpen = 37
    PatientLettersView = 4
    PatientLetterInsert = 5
    PatDel = 6
    RoomInsert = 7
    RoomEdit = 8
    StaffInsert = 9
    StaffEdit = 10
    PatInsert = 11
    PatEdit = 12
    BillGen = 13
    MedicationInsert = 14
    EncounterClose = 15
    EncounterOpen = 16
    MedicationRenew = 17
    PrnDocument = 18
    ViewDocument = 19
    
    'For DI
    ExamElementsInsert = 20
    ApptCreate = 21
    OrderSet = 22
    PatientNotes = 23
    PatientHistory = 24
    EncounterSave = 25
    PrnMedication = 26
    MedicationDiscontinue = 27
    MedicationDel = 28
    MedicationContinue = 29
    
    'For PI
    PatientLogIn = 30
    PatientLogOut = 31
    
    'For AU
    Tools = 32
    ClinicalImport = 33
    MachineConfig = 34
    MediCare = 35
    RxUtility = 36
    BringFindingsForwardCopy = 38
    Query = 39
    
    'Log Error
    Exception = 99
End Enum

Public denyReason(15) As String
Public Const Separator As String = vbNewLine & _
"____________________________________" & _
""

Public ApptCategoryColors(15) As String

Public Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Public Declare Function GetMem4 Lib "msvbvm60" (ByVal pSrc As Long, ByVal pDst As Long) As Long
Public Declare Function ArrPtr Lib "msvbvm60" Alias "VarPtr" (arr() As Any) As Long
Private Declare Function GetCurrentProcessId Lib "kernel32" () As Long

Public Const dbConnectionAccess As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="
Private Const MAX_LOG_SIZE As Long = 10485760         ' = 10 MB
'=========== test only =======================
Public mPatId As Long
'=========== test only =======================

Public Const ImagingViewManagerType As String = "IO.Practiceware.Presentation.Views.Imaging.ImagingViewManager"
Public Const ApplicationContextType As String = "IO.Practiceware.Application.ApplicationContext"
Public Const UserContextType As String = "IO.Practiceware.Application.UserContext"
Public Const DocumentGenerationServiceType As String = "IO.Practiceware.Services.Documents.IDocumentGenerationService"
Public Const DocumentViewManagerType As String = "IO.Practiceware.Presentation.Views.Documents.DocumentViewManager"
Public Const TaskMonitorType As String = "IO.Practiceware.Presentation.Views.Tasks.TaskMonitor"
Public Const TaskViewManagerType As String = "IO.Practiceware.Presentation.Views.Tasks.TaskViewManager"
Public Const ExamViewManagerType As String = "IO.Practiceware.Presentation.Views.Exam.ExamViewManager"
Public Const BatchBuilderViewManagerType As String = "IO.Practiceware.Presentation.Views.BatchBuilder.BatchBuilderViewManager"
Public Const PrinterSetupViewManagerType As String = "IO.Practiceware.Presentation.Views.PrinterSetup.PrinterSetupViewManager"
Public Const ReportViewManagerType As String = "IO.Practiceware.Presentation.Views.Reporting.ReportViewManager"
Public Const AppointmentSearchViewManagerType As String = "IO.Practiceware.Presentation.Views.AppointmentSearch.AppointmentSearchViewManager"
Public Const AppointmentSearchLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.AppointmentSearch.AppointmentSearchLoadArguments"
Public Const MultiCalendarViewManagerType As String = "IO.Practiceware.Presentation.Views.MultiCalendar.MultiCalendarViewManager"
Public Const MultiCalendarLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.MultiCalendar.MultiCalendarLoadArguments"
Public Const ConfigureCategoriesViewManagerType As String = "IO.Practiceware.Presentation.Views.ConfigureCategories.ConfigureCategoriesViewManager"
Public Const ScheduleTemplateBuilderViewManagerType As String = "IO.Practiceware.Presentation.Views.ScheduleTemplateBuilder.ScheduleTemplateBuilderViewManager"
Public Const PatientInfoLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.PatientInfo.PatientInfoLoadArguments"
Public Const PatientInfoViewManagerType As String = "IO.Practiceware.Presentation.Views.PatientInfo.PatientInfoViewManager"
Public Const CancelAppointmentLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.CancelAppointment.CancelAppointmentLoadArguments"
Public Const CancelAppointmentViewManagerType As String = "IO.Practiceware.Presentation.Views.CancelAppointment.CancelAppointmentViewManager"
Public Const CancelAppointmentReturnArgumentsType As String = "IO.Practiceware.Presentation.Views.CancelAppointment.CancelAppointmentReturnArguments"
Public Const GlaucomaMonitorManagerType As String = "IO.Practiceware.Presentation.Views.GlaucomaMonitor.GlaucomaMonitorViewManager"
Public Const SubmitTransactionsLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.SubmitTransactions.SubmitTransactionsLoadArguments"
Public Const SubmitTransactionsViewManagerType As String = "IO.Practiceware.Presentation.Views.SubmitTransactions.SubmitTransactionsViewManager"
Public Const PaperClaimsMessageProducerType As String = "IO.Practiceware.Integration.PaperClaims.PaperClaimsMessageProducer"
Public Const PermissionsViewManagerType As String = "IO.Practiceware.Presentation.Views.Permissions.PermissionsViewManager"
Public Const ManageInsurancePlansLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.InsurancePlansSetup.ManageInsurancePlansLoadArguments"
Public Const InsurancePlansSetupViewManagerType As String = "IO.Practiceware.Presentation.Views.InsurancePlansSetup.InsurancePlansSetupViewManager"
Public Const PermissionLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.Permissions.PermissionLoadArguments"
Public Const LoginViewManagerType As String = "IO.Practiceware.Presentation.Views.Login.LoginViewManager"
Public Const ClinicalDataFilesViewManagerType As String = "IO.Practiceware.Presentation.Views.ClinicalDataFiles.ClinicalDataFilesViewManager"
Public Const PatientSearchViewManagerType As String = "IO.Practiceware.Presentation.Views.PatientSearch.PatientSearchViewManager"
Public Const PatientSearchLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.PatientSearch.PatientSearchLoadArguments"
Public Const HomeScreenViewManagerType As String = "IO.Practiceware.Presentation.Views.Home.HomeScreenViewManager"
Public Const ExternalFeatureIntegrationManagerType As String = "IO.Practiceware.Application.ExternalFeatureIntegrationManager"
Public Const ReferralsViewManagerType As String = "IO.Practiceware.Presentation.Views.Referrals.ReferralsViewManager"
Public Const ReferralLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.Referrals.ReferralLoadArguments"
Public Const PatientInsuranceAuthorizationsLoadArgumentsType As String = " IO.Practiceware.Presentation.Views.PatientInsuranceAuthorization.PatientInsuranceAuthorizationsLoadArguments"
Public Const PatientInsuranceAuthorizationsViewManagerType As String = " IO.Practiceware.Presentation.Views.PatientInsuranceAuthorization.PatientInsuranceAuthorizationsViewManager"
Public Const EditInvoiceViewManagerType As String = "IO.Practiceware.Presentation.Views.Financials.EditInvoice.EditInvoiceViewManager"
Public Const EditInvoiceLoadArgumentsType As String = "IO.Practiceware.Presentation.Views.Financials.EditInvoice.EditInvoiceLoadArguments"
Public Const InsurancePolicyViewManagerType As String = "IO.Practiceware.Presentation.Views.InsurancePolicy.InsurancePolicyViewManager"
Public Const NoteViewManagerType As String = "IO.Practiceware.Presentation.Views.Notes.NoteViewManager"
Public Const NoteViewLoadArgumentsType As String = "IO.Practiceware.Presentation.ViewModels.Notes.NoteLoadArguments"
Public Const AdminUtilitiesViewManagerType As String = "IO.Practiceware.Presentation.Views.Setup.AdminUtilities.AdminUtilitiesViewManager"
Public Const StandardExamViewManagerType As String = "IO.Practiceware.Presentation.Views.StandardExam.StandardExamViewManager"
Public Const StandardExamLoadArgumentsType As String = "IO.Practiceware.Presentation.ViewModels.StandardExam.StandardExamLoadArguments"
Public Const StandardExamOfficeVisitLoadArgumentsType As String = "IO.Practiceware.Presentation.ViewModels.StandardExam.OfficeVisitLoadArguments"
Public Const StandardExamDiagnosesLoadArgumentsType As String = "IO.Practiceware.Presentation.ViewModels.StandardExam.DiagnosesLoadArguments"

Public emptyArgs() As Variant

Public TaskComWrapper As New comWrapper
Public BatchBuilderComWrapper As New comWrapper
Public PatientDemoGraphicsComWrapper As New comWrapper
Public ExternalFeatureIntegrationManagerComWrapper As New comWrapper
Public AdminUtilitiesViewManagerComWrapper As New comWrapper
Public StandardExamViewManagerComWrapper As New comWrapper
Private FileManager As VB6FileManager
Public homeScreenFeatureEventHandler, patientDemoGraphicsEventHandler, batchBuilderEventHandler, openMachineConfigEventHandler, loadPlanFormEventHandler, loadImpressionFormEventHandler, loadChartFormEventHandler, loadHighlightFormEventHandler, GetOfficeVisitEventHandler

Public Function Dir(Optional path As String = "", Optional attr As Integer = 0) As String
    Dir = FM.Dir(path, attr)
End Function

Public Property Get FM() As VB6FileManager
    If FileManager Is Nothing Then
        Set FileManager = New VB6FileManager
    End If
    Set FM = FileManager
End Property

Public Property Get CreateAdoConnection() As AdoConnection
Dim wrapper As New comWrapper
    Set CreateAdoConnection = wrapper.Create("IO.Practiceware.Interop.AdoConnection", emptyArgs).Instance
End Property

Public Property Get CreateAdoCommand() As ADODB.Command
Dim wrapper As New comWrapper
    Set CreateAdoCommand = wrapper.Create("IO.Practiceware.Interop.AdoCommand", emptyArgs).Instance
End Property
    
Public Property Get CreateAdoRecordset() As ADODB.Recordset
Dim wrapper As New comWrapper
    Set CreateAdoRecordset = wrapper.Create("IO.Practiceware.Interop.AdoRecordset", emptyArgs).Instance
End Property

Property Get MyPracticeRepository() As AdoConnection
    Set MyPracticeRepository = mMyPracticeRepository
End Property

Property Get MyPracticeRepositoryCmd() As AdoCommand
    If dbConnection <> "" Then
    Do While mMyPracticeRepository.State = 0
        mMyPracticeRepository.CursorLocation = adUseClient
        mMyPracticeRepository.Open dbConnection
    Loop
    End If
    Set MyPracticeRepositoryCmd = mMyPracticeRepositoryCmd
End Property

Property Get ConnectionString() As String
Dim wrapper As New comWrapper
    ConnectionString = wrapper.InvokeStaticGet("IO.Practiceware.Configuration.ConfigurationManager", "PracticeRepositoryConnectionString")
End Property

Property Get ServerConnectionString() As String
Dim wrapper As New comWrapper
wrapper.Create "Soaf.Data.IAdoService", emptyArgs
    
Dim args() As Variant
args = Array(ConnectionString)

ServerConnectionString = wrapper.InvokeMethod("ResolveConnectionString", args)
End Property

Property Get IdbConnection() As Variant
Dim wrapper As New comWrapper
    
Set IdbConnection = wrapper.InvokeStaticGet("IO.Practiceware.Data.DbConnectionFactory", "PracticeRepository")
End Property
Property Get ActivePatientId() As Long
      ActivePatientId = m_ActivePatientId
End Property

Property Let ActivePatientId(Value As Long)
    On Error GoTo lActivePatientId_Error

    m_ActivePatientId = Value
    'Set PatientID in ApplicationContext
        Dim applicationContext
        Dim arg As Variant

        If (m_ActivePatientId > 0) Then
                arg = Value
        End If
Dim ApplicationContextComWrapper As New comWrapper
        Set applicationContext = ApplicationContextComWrapper.InvokeStaticGet(ApplicationContextType, "Current")
        ApplicationContextComWrapper.SetInstance (applicationContext)
        ApplicationContextComWrapper.InvokeSet "PatientId", arg

    Exit Property

lActivePatientId_Error:

    LogError "CommonLib", "ActivePatientId", Err, Err.Description, , Value
End Property
Property Get ActiveAppointmentId() As Long
      ActiveAppointmentId = m_ActiveAppointmentId
End Property

Property Let ActiveAppointmentId(Value As Long)
    On Error GoTo lActiveAppointmentId_Error

    m_ActiveAppointmentId = Value
    'Set PatientID in ApplicationContext
        Dim applicationContext
Dim ApplicationContextComWrapper As New comWrapper
        Set applicationContext = ApplicationContextComWrapper.InvokeStaticGet(ApplicationContextType, "Current")
        If (m_ActiveAppointmentId > 0) Then
                applicationContext.AppointmentId = m_ActiveAppointmentId
        Else
                applicationContext.AppointmentId = Nothing
        End If

    Exit Property

lActiveAppointmentId_Error:

    LogError "CommonLib", "ActiveAppointmentId", Err, Err.Description, , , Value
End Property
Property Get GetRootDrive() As String
    On Error GoTo lGetRootDrive_Error
    Dim wrapper As New comWrapper
    GetRootDrive = wrapper.InvokeStaticGet("IO.Practiceware.Configuration.ConfigurationManager", "ApplicationDataPath")
    If Not GetRootDrive = "" Then
        GetRootDrive = FM.GetParentFolder(GetRootDrive)
    End If

    Exit Property

lGetRootDrive_Error:
    LogError "CommonLib", "GetRootDrive", Err, Err.Description
End Property
Property Get GetServerDrive() As String
    On Error GoTo lGetServerDrive_Error

    Dim wrapper As New comWrapper
    GetServerDrive = wrapper.InvokeStaticGet("IO.Practiceware.Configuration.ConfigurationManager", "ServerDataPath")
    If Not GetServerDrive = "" Then
        GetServerDrive = FM.GetParentFolder(GetServerDrive)
    End If
    
    Exit Property

lGetServerDrive_Error:
    LogError "CommonLib", "GetServerDrive", Err, Err.Description
End Property
Public Function IsFileThere(sTheFile As String) As Boolean
Dim iMaxLen As Integer
Dim FilePath As String
    On Error GoTo lIsFileThere_Error

    IsFileThere = True
    iMaxLen = 5
    Dim wrapper As New comWrapper
    
    FilePath = wrapper.Evaluate("ConfigurationManager.Configuration.FilePath")
    
    If sTheFile = FilePath Then
        iMaxLen = 1
    End If
    If FM.GetFileLength(sTheFile) < iMaxLen Then
        IsFileThere = False
        FM.Kill sTheFile
    End If

    Exit Function

lIsFileThere_Error:
    IsFileThere = False
End Function

Public Function ArrayExists(ByVal ppArray As Long) As Long
  GetMem4 ppArray, VarPtr(ArrayExists)
End Function

Public Function CollectionKeyExists(ByVal oCol As Collection, ByVal vKey As Variant) As Boolean
    On Error Resume Next
    oCol.Item vKey
    CollectionKeyExists = (Err.Number = 0)
    Err.Clear
End Function

Public Function DoesScratchFileExist(sFileName As String) As Boolean
Dim sFile As String
    On Error GoTo lDoesScratchFileExist_Error
   
    sFile = Dir(sFileName)
    If sFile <> "" Then
        DoesScratchFileExist = True
    End If

    Exit Function

lDoesScratchFileExist_Error:

    LogError "CommonLib", "DoesScratchFileExist", Err, Err.Description
End Function

Public Sub dbPinpointOpen()
    'TaskComWrapper.Create TaskMonitorType, emptyArgs
    
    'Remove Event Handlers
    Call RemoveEventHandlers
    
    ' Setup event handling with singleton View Managers
    Call TaskComWrapper.Create(TaskViewManagerType, emptyArgs)
    Dim taskEventHandler As New PatientTaskEventHandler
    TaskComWrapper.AddEventHandler "RequestingOpenTaskPatient", taskEventHandler
            
    Call BatchBuilderComWrapper.Create(BatchBuilderViewManagerType, emptyArgs)
    Set batchBuilderEventHandler = New PatientBatchEventHandler
    BatchBuilderComWrapper.AddEventHandler "RequestingOpenBatchBuilderPatient", batchBuilderEventHandler
    
    Call PatientDemoGraphicsComWrapper.Create(PatientInfoViewManagerType, emptyArgs)
    Set patientDemoGraphicsEventHandler = New DemoGraphicsEventHandler
    PatientDemoGraphicsComWrapper.AddEventHandler "OpenLegacyPatientScreen", patientDemoGraphicsEventHandler
    
    Call ExternalFeatureIntegrationManagerComWrapper.Create(ExternalFeatureIntegrationManagerType, emptyArgs)
    ' TODO: rename handler itself
    Set homeScreenFeatureEventHandler = New HomeExternalFeatureHandler
    ExternalFeatureIntegrationManagerComWrapper.AddEventHandler "ExternalFeatureExectionRequested", homeScreenFeatureEventHandler

    Call AdminUtilitiesViewManagerComWrapper.Create(AdminUtilitiesViewManagerType, emptyArgs)
    Set openMachineConfigEventHandler = New openMachineConfigHandler
    AdminUtilitiesViewManagerComWrapper.AddEventHandler "OpenMachineConfiguration", openMachineConfigEventHandler
    
    Call StandardExamViewManagerComWrapper.Create(StandardExamViewManagerType, emptyArgs)
    Set loadPlanFormEventHandler = New LoadPlanFormHandler
    Set loadImpressionFormEventHandler = New LoadImpressionFormHandler
    Set loadChartFormEventHandler = New LoadChartFormHandler
    Set loadHighlightFormEventHandler = New LoadHighlightFormHandler
    Set GetOfficeVisitEventHandler = New GetOfficeVisitEventHandler
    StandardExamViewManagerComWrapper.AddEventHandler "LoadPlanFormHandler", loadPlanFormEventHandler
    StandardExamViewManagerComWrapper.AddEventHandler "LoadImpressionFormHandler", loadImpressionFormEventHandler
    StandardExamViewManagerComWrapper.AddEventHandler "LoadChartFormHandler", loadChartFormEventHandler
    StandardExamViewManagerComWrapper.AddEventHandler "LoadHighlightFormHandler", loadHighlightFormEventHandler
    StandardExamViewManagerComWrapper.AddEventHandler "GetOfficeVisitHandler", GetOfficeVisitEventHandler
    

    Dim sConnectString As String
    On Error GoTo ldbPinpointOpen_Error
    
    PinpointDatabaseOpen = False
    Set mMyPracticeRepository = CreateAdoConnection
    mMyPracticeRepository.IsAsyncExecuteEnabled = False
    Set mMyPracticeRepositoryCmd = CreateAdoCommand
    MyPracticeRepositoryCmd.ActiveConnection = MyPracticeRepository
    
    sConnectString = dbConnectionAccess & dbDynamicForms & ";"
    Set MyDynamicForms = New ADODB.Connection
    MyDynamicForms.CursorLocation = adUseClient
    Call MyDynamicForms.Open(sConnectString)
    Set MyDynamicFormsCmd = New ADODB.Command
    MyDynamicFormsCmd.ActiveConnection = MyDynamicForms
    
    sConnectString = dbConnectionAccess & dbDiagnosisMaster & ";"
    Set MyDiagnosisMaster = New ADODB.Connection
    MyDiagnosisMaster.CursorLocation = adUseClient
    Call MyDiagnosisMaster.Open(sConnectString)
    Set MyDiagnosisMasterCmd = New ADODB.Command
    MyDiagnosisMasterCmd.ActiveConnection = MyDiagnosisMaster
    PinpointDatabaseOpen = True
    
    Exit Sub

ldbPinpointOpen_Error:
    
    MsgBox sConnectString & " " & Error(Err)
    End
End Sub

Public Sub InitializeComWrapper()
    On Error GoTo lInitializeComWrapperError
    
    ' Eager initialize ComWrapper
    'Call ComWrapper.Initialize
    Exit Sub
    
lInitializeComWrapperError:
    
    LogError "CommonLib", "InitializeComWrapper", Err, Err.Description
End Sub

Public Sub dbPinpointClose()
    MyPracticeRepository.Close
    MyDynamicForms.Close
    MyDiagnosisMaster.Close
    PinpointDatabaseOpen = False
    Set pdf = Nothing
End Sub

Public Sub RemoveEventHandlers()
    If Not IsEmpty(homeScreenFeatureEventHandler) Then
        ExternalFeatureIntegrationManagerComWrapper.RemoveEventHandler "ExternalFeatureExectionRequested", homeScreenFeatureEventHandler
    End If
    If Not IsEmpty(patientDemoGraphicsEventHandler) Then
        PatientDemoGraphicsComWrapper.RemoveEventHandler "OpenLegacyPatientScreen", patientDemoGraphicsEventHandler
    End If
    If Not IsEmpty(batchBuilderEventHandler) Then
        BatchBuilderComWrapper.RemoveEventHandler "RequestingOpenBatchBuilderPatient", batchBuilderEventHandler
    End If
    If Not IsEmpty(openMachineConfigEventHandler) Then
        AdminUtilitiesViewManagerComWrapper.RemoveEventHandler "OpenMachineConfiguration", openMachineConfigEventHandler
    End If
    If Not IsEmpty(loadPlanFormEventHandler) Then
        StandardExamViewManagerComWrapper.RemoveEventHandler "LoadPlanFormHandler", loadPlanFormEventHandler
    End If
    If Not IsEmpty(loadImpressionFormEventHandler) Then
        StandardExamViewManagerComWrapper.RemoveEventHandler "LoadImpressionFormHandler", loadImpressionFormEventHandler
    End If
    If Not IsEmpty(loadChartFormEventHandler) Then
        StandardExamViewManagerComWrapper.RemoveEventHandler "LoadChartFormHandler", loadChartFormEventHandler
    End If
    If Not IsEmpty(loadHighlightFormEventHandler) Then
        StandardExamViewManagerComWrapper.RemoveEventHandler "LoadHighlightFormHandler", loadHighlightFormEventHandler
    End If
    If Not IsEmpty(GetOfficeVisitEventHandler) Then
        StandardExamViewManagerComWrapper.RemoveEventHandler "GetOfficeVisitHandler", GetOfficeVisitEventHandler
    End If
End Sub

Public Sub StartNewInstanceOfThisApp()
    Dim appPath As String
    Dim fso As New FileSystemObject
    appPath = App.path & "\" & App.EXEName & ".exe"
    ' To prevent errors when running under VB6 debug -> check whether file is present
    If (fso.FileExists(appPath)) Then
        ' Start initializing new app (also pass "restart" command line argument to let it know it's restarting)
        Call Shell(appPath & " restart", vbNormalFocus)
    End If
End Sub

' Closes all sorts of windows associated with current process
Public Sub CloseAllActiveForms()
    Dim comWrapper As New comWrapper
    Call comWrapper.CloseAllActiveForms
End Sub

Public Sub GetSoftwareLocation()
On Error GoTo lGetSoftwareLocation_Error
    
    Dim ServiceLocationShortName As String
    Dim strSQL As String
    Dim RS As Recordset
    
    dbSoftwareLocation = ""
    
    Dim wrapper As New comWrapper
    ServiceLocationShortName = wrapper.Evaluate("UserContext.Current.ServiceLocation.Item2")
    
    strSQL = " Select sl.ShortName from model.ServiceLocations sl " & _
             " WHERE sl.ShortName='" & ServiceLocationShortName & "'"
             
    Set RS = GetRS(strSQL)
    
    If Not RS.EOF Then
        dbSoftwareLocation = RS("ShortName")
        If dbSoftwareLocation = "Office" Then dbSoftwareLocation = ""
    End If
    
Exit Sub

lGetSoftwareLocation_Error:
    LogError "CommonLib", "GetSoftwareLocation", Err, Err.Description
End Sub
Public Sub DisplayDollarAmount(TheText As String, NewText As String)

If IsNumeric(TheText) Then
    TheText = Round(CDbl(TheText), 2)
End If

Dim q As Integer
Dim w As Currency
Dim BuildText As String
If (Trim$(TheText) = "") Then
    NewText = "0.00"
    Exit Sub
End If
    w = val(TheText)
NewText = Space(10)
BuildText = Trim$(Str$(w))
If (Left$(TheText, 1) = "+") Then
    BuildText = "+" + BuildText
End If
If (InStrPS(BuildText, ".") = 0) Then
    BuildText = BuildText + ".00"
End If
q = InStrPS(BuildText, ".")
If (Len(BuildText) > q + 2) Then
    BuildText = Left$(BuildText, q + 2)
ElseIf (Len(BuildText) = q + 1) Then
    BuildText = Left$(BuildText, q + 1) + "0"
End If
If (BuildText = ".00") Then
    BuildText = "0.00"
End If
If (Mid$(BuildText, 1, 1) = ".") Then
    BuildText = "0" + BuildText
End If
If (Len(BuildText) < 10) Then
    NewText = String$(10 - Len(BuildText), 32) + BuildText
Else
    NewText = BuildText
End If
End Sub

Public Sub DisplayZip(TheText As String, NewText As String)
Dim BuildText As String
NewText = TheText
If (Trim$(TheText) <> "") Then
    NewText = ""
    BuildText = TheText
    Call StripCharacters(BuildText, "-")
    If (Len(Trim$(BuildText)) > 5) Then
        NewText = Left$(BuildText, 5) + "-" + Mid$(BuildText, 6, 4)
    Else
        NewText = BuildText
    End If
End If
End Sub

Public Sub EstablishDirectory()
Dim TheRec As String
Dim iFreeFile As Long
On Error Resume Next

AdministratorVersionId = "L A" & App.Major & "." & App.Minor & "." & App.Revision
PatientInterfaceVersionId = "L P" & App.Major & "." & App.Minor & "." & App.Revision
DoctorInterfaceVersionId = "L D" & App.Major & "." & App.Minor & "." & App.Revision
ToolsVersionId = "L T" & App.Major & "." & App.Minor & "." & App.Revision

dfPrinter = Printer.DeviceName
dfPrinterDriver = Printer.DriverName
dfPrinterOrientation = Printer.Orientation

' Initialize pdf object
Set pdf = CreateObject("Tiff2PdfX.Tiff2Pdf")

' Saved for reference only
' dbConnection = "ODBC;DRIVER=SQL Server;SERVER=PINPOINT;UID=station1;PWD=station1;WSID=STATION1;DATABASE=PracticeRepository;TranslationName=Yes"
MySystemName = Space(255)
Call GetComputerName(MySystemName, 255)
MySystemName = Trim$(MySystemName)
MySystemName = Left$(MySystemName, Len(MySystemName) - 1)

dbTerminal = ""
tmTimer = 5
dbConnection = ConnectionString 'Connection String

GblDevice = GetServerDrive 'Server data path root
If GblDevice = "" Then GblDevice = "G:"

LclDevice = GetRootDrive 'Local data path root
If LclDevice = "" Then LclDevice = "C:"

LocalPinPointDirectory = LclDevice + "\Pinpoint\"
PinPointDirectory = GblDevice + "\Pinpoint\"
BtnDir = LocalPinPointDirectory
TemplateDirectory = PinPointDirectory + "Templates\"
DoctorInterfaceDirectory = LocalPinPointDirectory + "DoctorInterface\"
PatientInterfaceDirectory = LocalPinPointDirectory + "PatientInterface\"
FeeDirectory = PinPointDirectory + "FeeUtility\"
RemitDirectory = PinPointDirectory + "Remit\"
ScanDirectory = PinPointDirectory + "Scan\"
MyScanDirectory = PinPointDirectory + "MyScan\"
DocumentDirectory = PinPointDirectory + "Document\"
SchedulerInterfaceDirectory = LocalPinPointDirectory + "SchedulerInterface\"
SubmissionsDirectory = PinPointDirectory + "Submissions\"
SubmissionsAcksDirectory = PinPointDirectory + "SubmissionsAcks\"
ReportsDirectory = PinPointDirectory + "Reports\"
ImpressionNotesDirectory = PinPointDirectory + "Notes\"
sClinImportDir = PinPointDirectory & "ClinImport\"
ServerDoctorInterfaceDirectory = PinPointDirectory & "DoctorInterface\"

MyImageDir = PinPointDirectory + "MyImages\"
LocalMyImageDir = LocalPinPointDirectory + "MyImages\"
ImageDir = LocalPinPointDirectory + "Images\"

' Logs
PrimaryVerifyLog = PinPointDirectory + "PinpointVerifyPrimary.log"
SecondVerifyLog = PinPointDirectory + "PinpointVerifySecond.log"

dbDynamicForms = LocalPinPointDirectory + "DynamicForms.mdb"

dbDiagnosisMaster = LocalPinPointDirectory + "DiagnosisMaster.mdb"
End Sub

Public Sub FormatTimeNow(TheTime As String)
Dim h As Integer, m As Integer
Dim Hr As String, Mn As String, Wh As String
TheTime = ""
h = Hour(Time)
m = Minute(Time)
If (h > 11) Then
    Wh = "PM"
Else
    Wh = "AM"
End If
If (h > 12) Then
    h = h - 12
End If
If (h < 10) Then
    Hr = "0" + Trim$(Str$(h))
Else
    Hr = Str$(h)
End If
If (m < 10) Then
    Mn = "0" + Trim$(Str$(m))
Else
    Mn = Str$(m)
End If
TheTime = Trim$(Hr) + ":" + Trim$(Mn) + " " + Trim$(Wh)
End Sub

Public Sub FormatTodaysDate(NewDate As String, Flag As Boolean)
On Error GoTo ExitFast
Dim MyDate As Date
Dim Temp As String
Dim w As Integer, d As Integer, m As Integer, Y As Integer
Dim dd As String, mm As String, yy As String
Dim MonthofYear(12) As String
Dim DayOfWeek(7) As String
If (NewDate <> "") Then
    Call DisplayDate(NewDate, Temp)
    If (Temp <> "") Then
        MyDate = DateValue(NewDate)
    Else
        MyDate = Date
    End If
Else
    MyDate = Date
End If
NewDate = ""
DayOfWeek(1) = "Sunday"
DayOfWeek(2) = "Monday"
DayOfWeek(3) = "Tuesday"
DayOfWeek(4) = "Wednesday"
DayOfWeek(5) = "Thursday"
DayOfWeek(6) = "Friday"
DayOfWeek(7) = "Saturday"
MonthofYear(1) = "January"
MonthofYear(2) = "February"
MonthofYear(3) = "March"
MonthofYear(4) = "April"
MonthofYear(5) = "May"
MonthofYear(6) = "June"
MonthofYear(7) = "July"
MonthofYear(8) = "August"
MonthofYear(9) = "September"
MonthofYear(10) = "October"
MonthofYear(11) = "November"
MonthofYear(12) = "December"
w = Weekday(MyDate)
d = Day(MyDate)
dd = Trim$(Str$(d))
If (d < 10) Then
    dd = "0" + Trim$(Str$(d))
End If
m = Month(MyDate)
mm = Trim$(Str$(m))
If (m < 10) Then
    mm = "0" + Trim$(Str$(m))
End If
Y = Year(MyDate)
yy = Trim$(Str$(Y))

If (Flag) Then
    NewDate = DayOfWeek(w) + ", " + MonthofYear(m) + " " + dd + ", " + yy
Else
    NewDate = mm + "/" + dd + "/" + yy
End If
Exit Sub
ExitFast:
    NewDate = ""
    Resume LeaveFast
LeaveFast:
End Sub

Public Sub PinpointPRError(ModName As String, AErr As Long)
    On Error Resume Next
    Err.Raise (AErr)
    ' Not a "no current record" error?
    If (Err.Number <> 3021) Then
        LogError ModName, "", AErr, Str$(Err.Number) + " " + Err.Description, "PracticeRepositoryError"
    End If
    Err.Clear
End Sub

Public Sub LogFileError(sModule As String, sMethod As String, lErr As Long, sFileName As String)
    On Error Resume Next
    Err.Raise (lErr)
    LogError sModule, sMethod, lErr, Err.Description, "FileError. FileName: " & sFileName
    Err.Clear
End Sub

''' <summary>
''' Perform InStr over input string (String1) which could start be a full path to file.
''' This is important, since Pinpoint folder was moved from C:\ into user's profile
''' </summary>
Public Function InStrPS(Start As Variant, String1 As Variant, Optional String2 As Variant, Optional Compare As VbCompareMethod = vbBinaryCompare) As Variant
    Dim StartParam As Variant
    Dim String1Param As Variant
    Dim String2Param As Variant
    Dim LclDevicePosition As Integer
    Dim LclDeviceLength As Integer
    
    ' User ommited start? Fix arguments
    If IsMissing(String2) Then
        String2Param = String1
        String1Param = Start
        StartParam = 1
    Else
        String2Param = String2
        String1Param = String1
        StartParam = Start
    End If
    
    ' If input string starts with local root path -> fix search start position
    LclDevicePosition = InStr(String1Param, LclDevice)
    If LclDevicePosition = 1 And InStr(String2Param, ":") = 0 Then
        LclDeviceLength = Len(LclDevice) + 1
        If LclDeviceLength > StartParam And LclDeviceLength < Len(String1Param) Then
            StartParam = LclDeviceLength
        End If
    End If
    
    InStrPS = InStr(StartParam, String1Param, String2Param, Compare)

End Function

Public Sub ReplaceCharacters(TheText As String, ReplaceChar As String, NewChar As String)
Dim k As Integer
Dim t As Integer
Dim ATemp As String
If ((Trim$(TheText) <> "") And (ReplaceChar <> NewChar)) And (Len(NewChar) > 0) Then
    ATemp = TheText
    t = InStrPS(ATemp, ReplaceChar)
    Do Until (t < 1) Or (t > Len(TheText)) Or (t > Len(ATemp))
        ATemp = Replace(ATemp, ReplaceChar, NewChar)
        If (Len(NewChar) <= Len(ReplaceChar)) Then
            Exit Do
        Else
            k = t + Len(NewChar)
        End If
        t = InStrPS(k, ATemp, ReplaceChar)
        If (t = k) Then
            t = 0
        End If
    Loop
    TheText = ATemp
End If
End Sub

Public Sub SetGlobalDefaults()
    tmTimer = val(Trim$(CheckConfigCollection("TIMEOUT")))
End Sub

Public Sub StripCharacters(TheText As String, StripChar As String)
Dim i As Integer
Dim NewText As String
    NewText = ""
    If Trim$(TheText) <> "" And Len(StripChar) > 0 Then
        For i = 1 To Len(TheText)
            If Mid$(TheText, i, 1) <> StripChar Then
                NewText = NewText & Mid$(TheText, i, 1)
            End If
        Next i
        TheText = NewText
    End If
End Sub

Public Sub StripMultiCharacters(OldString, NewString, BasisOn As Boolean, _
Optional allowedChar As String)
Dim i As Integer
Dim Temp As String
Dim AString As String
Dim NString As String
    Temp = """" & "_`)(*&^%$#@!,./<>?:;'[]{}=+\|"
    If BasisOn Then
        Temp = """" & "-`_)(*&^%$#@!,./<>?:;'[]{}=+\| "
    End If
    AString = OldString
    If Len(OldString) > 0 Then
        For i = 1 To Len(Temp)
            If allowedChar = "" Then
                StripCharacters AString, Mid(Temp, i, 1)
            Else
                If InStrPS(Mid(Temp, i, 1), allowedChar) = 0 Then
                    StripCharacters AString, Mid(Temp, i, 1)
                End If
            End If
        Next i
        NString = AString
    End If
    NewString = NString
End Sub

Public Sub UpdateDisplay(TheField As TextBox, UType As String)
Dim NewField As String
NewField = ""
If (UType = "P") Then
    Call DisplayPhone(TheField.Text, NewField)
ElseIf (UType = "S") Then
    Call DisplaySocialSecurity(TheField.Text, NewField)
ElseIf (UType = "Z") Then
    Call DisplayZip(TheField.Text, NewField)
ElseIf (UType = "D") Then
    Call DisplayDate(TheField.Text, NewField)
End If
If (Trim$(NewField) <> "") And (UType = "D") Then
    Call FormatTodaysDate(NewField, False)
    TheField.Text = NewField
Else
    TheField.Text = NewField
End If
End Sub

Public Sub DisplayDate(TheText As String, NewText As String)
Dim yy As Integer
Dim TempText As String
NewText = ""
TempText = TheText
Call StripCharacters(TempText, "/")
Call StripCharacters(TempText, "-")
TempText = Trim$(TempText)
If (Len(TempText) = 4) Then
    If (Left$(TempText, 1) <> "0") Then
        TempText = "0" + Left$(TempText, 1) + "/" + "0" + Mid$(TempText, 2, 1) + "/" + Mid$(TempText, 3, 2)
    Else
        yy = Year(Date)
        TempText = Left$(TempText, 2) + "/" + Mid$(TempText, 3, 2) + "/" + Trim$(Str$(yy))
    End If
ElseIf (Len(TempText) = 5) Then
    TempText = "0" + Left$(TempText, 1) + "/" + Mid$(TempText, 2, 2) + "/" + Mid$(TempText, 4, 2)
ElseIf (Len(TempText) = 6) Then
    TempText = Left$(TempText, 2) + "/" + Mid$(TempText, 3, 2) + "/" + Mid$(TempText, 5, 2)
ElseIf (Len(TempText) = 7) Then
    TempText = "0" + Left$(TempText, 1) + "/" + Mid$(TempText, 2, 2) + "/" + Mid$(TempText, 4, 4)
ElseIf (Len(TempText) = 8) Then
    TempText = Left$(TempText, 2) + "/" + Mid$(TempText, 3, 2) + "/" + Mid$(TempText, 5, 4)
Else
    TempText = ""
End If
NewText = TempText
End Sub

Public Sub DisplayPhone(TheText As String, NewText As String)
Dim ExtText As String
Dim TempText As String
Dim BuildText As String
NewText = ""
ExtText = ""
TempText = ""
If (Trim$(TheText) <> "") Then
    BuildText = Trim$(TheText)
    Call StripCharacters(BuildText, "(")
    Call StripCharacters(BuildText, ")")
    Call StripCharacters(BuildText, "-")
    Call StripCharacters(BuildText, " ")
    If (Len(BuildText) >= 10) Then
        If (Len(BuildText) > 10) Then
            ExtText = Mid$(BuildText, 11, Len(BuildText) - 10)
        End If
        TempText = Left$(BuildText, 10)
        TempText = "(" + Left$(TempText, 3) + ")" + Mid$(TempText, 4, 3) + "-" + Mid$(TempText, 7, 4)
        NewText = TempText + ExtText
    ElseIf (Len(BuildText) > 7) And (Len(BuildText) < 10) Then
        ExtText = Mid$(BuildText, 8, Len(BuildText) - 7)
        TempText = Left$(BuildText, 7)
        TempText = Left$(TempText, 3) + "-" + Mid$(TempText, 4, 4)
        NewText = TempText + ExtText
    ElseIf (Len(BuildText) = 7) Then
        NewText = Left$(BuildText, 3) + "-" + Mid$(BuildText, 4, 4)
    Else
        NewText = BuildText
    End If
End If
End Sub

    'REFACTOR: Use FormatPhoneNumber instead of Display Phone everywhere
Public Function FormatPhoneNumber(ByVal sPhone As String, fDisplay As Boolean) As String
Dim iPos As Long
    If Not Len(sPhone) = 0 Then
        ' Get rid of dashes and invalid chars
        For iPos = Len(sPhone) To 1 Step -1
            If InStrPS("0123456789", Mid$(sPhone, iPos, 1)) = 0 Then
                sPhone = Left$(sPhone, iPos - 1) & Mid$(sPhone, iPos + 1)
            End If
        Next
        ' Te-insert them in the correct position
        If fDisplay Then
            If Len(sPhone) <= 7 Then
                FormatPhoneNumber = Format$(sPhone, "!@@@-@@@@")
            Else
                FormatPhoneNumber = Format$(sPhone, "!(@@@) @@@-@@@@&&&&&&&&&&&&&&&")
            End If
        Else
            FormatPhoneNumber = sPhone
        End If
    End If
End Function

Public Function FormatZip(ByVal sZip As String, fDisplay As Boolean) As String
Dim iPos As Long
    If Not Len(sZip) = 0 Then
        ' Get rid of dashes and invalid chars
        For iPos = Len(sZip) To 1 Step -1
            If InStrPS("0123456789", Mid$(sZip, iPos, 1)) = 0 Then
                sZip = Left$(sZip, iPos - 1) & Mid$(sZip, iPos + 1)
            End If
        Next
        ' Te-insert them in the correct position
        If fDisplay Then
            If Len(sZip) <= 5 Then
                FormatZip = Format$(sZip, "!@@@@@")
            Else
                FormatZip = Format$(sZip, "!@@@@@-@@@@")
            End If
        Else
            FormatZip = sZip
        End If
    End If
End Function

Public Sub DisplaySocialSecurity(TheText As String, NewText As String)
Dim TempText As String
Dim BuildText As String
NewText = ""
TempText = ""
If (Trim$(TheText) <> "") Then
    BuildText = TheText
    Call StripCharacters(BuildText, "-")
    If (Len(BuildText) >= 9) Then
        If (Len(BuildText) > 9) Then
            TempText = Mid$(BuildText, 10, Len(BuildText) - 9)
        End If
        NewText = Left$(BuildText, 3) + "-" + Mid$(BuildText, 4, 2) + "-" + Mid$(BuildText, 6, 4) + " " + TempText
    Else
        NewText = BuildText
    End If
End If
End Sub

Public Function OkDate(TheText As String) As Boolean
On Error GoTo UI_ErrorHandler
Dim i As Integer
Dim Myyear As String
Dim MyDate As Date
Dim NewField As String
OkDate = False
If (Len(TheText) <> 0) Then
    Call DisplayDate(TheText, NewField)
    MyDate = DateValue(NewField)
    Myyear = Year(MyDate)
    If Myyear < 1900 Then
    Exit Function
    End If
    Call FormatTodaysDate(NewField, False)
    TheText = NewField
    OkDate = True
End If
Exit Function
UI_ErrorHandler:
    Resume LeaveFast
LeaveFast:
End Function

Public Function EditableDate(dte As Date) As Boolean
Dim sCloseDate As String
    sCloseDate = CheckConfigCollection("EDITWINDOW")
    EditableDate = Not (DateDiff("d", sCloseDate, dte) < 0)
End Function

Public Sub ConvertMinutesToTime(Mins As Integer, TheTime As String)
Dim Wh As String, Hrs As String, Mns As String
Dim Hr As Integer, Mn As Integer
TheTime = ""
Hr = Int(Mins / 60)
If (Hr > 12) Then
    Wh = "PM"
    Hr = Hr - 12
ElseIf (Hr = 12) Then
    Wh = "PM"
Else
    Wh = "AM"
    If (Hr = 0) Then
        Hr = 12
    End If
End If
Hrs = Trim$(Str$(Hr))
If (Len(Hrs) = 1) Then
    Hrs = "0" + Hrs
End If
If (Wh = "PM") Then
    If (Hr <> 12) Then
        Mn = Mins - ((Hr + 12) * 60)
    Else
        Mn = Mins - (Hr * 60)
    End If
Else
    If (Hr = 12) Then
        Hr = 0
    End If
    Mn = Mins - (Hr * 60)
End If
Mns = Trim$(Str$(Mn))
If (Len(Mns) = 1) Then
    Mns = "0" + Mns
End If
TheTime = Hrs + ":" + Mns + " " + Wh
End Sub

Public Function ConvertTimeToMinutes(TheTime As String) As Long
Dim w As Integer
Dim Wh As String, Hr As String, Mn As String
Call StripCharacters(TheTime, " ")
w = InStrPS(TheTime, ":")
If (w = 0) Then
    ConvertTimeToMinutes = -1
    Exit Function
End If
If (w <= 2) Then
    Hr = Mid$(TheTime, w - 1, 1)
Else
    Hr = Mid$(TheTime, w - 2, 2)
End If
Mn = Mid$(TheTime, w + 1, 2)
Wh = UCase$(Mid$(TheTime, Len(TheTime) - 1, 2))
If (Wh = "PM") Then
    If (Hr <> 12) Then
            ConvertTimeToMinutes = ((val(Hr) + 12) * 60) + val(Mn)
    Else
            ConvertTimeToMinutes = (val(Hr) * 60) + val(Mn)
    End If
Else
    If (Hr = 12) Then
        Hr = 0
    End If
        ConvertTimeToMinutes = (val(Hr) * 60) + val(Mn)
End If
End Function

Sub CheckLogSize(ByVal sPath As String, ByVal sFileName As String)
Dim sArchiveName As String
Dim iNameLength As Integer
    
    On Error GoTo lCheckLogSize_Error

    If FM.GetFileLength(sPath & sFileName) > MAX_LOG_SIZE Then
        iNameLength = Len(sFileName) - 4
        sArchiveName = Left$(sFileName, iNameLength) & Format(Now, "yyyymmdd") & _
            Mid$(sFileName, iNameLength + 1)
        If Not FM.IsFileThere(sPath & sArchiveName) Then Name (sPath & sFileName) As sPath & sArchiveName
    End If

    Exit Sub

lCheckLogSize_Error:

    LogError "CommonLib", "CheckLogSize", Err, Err.Description
    
End Sub

Public Function AutoLogin() As Boolean
Dim iLoc As Integer
Dim sLoginPID As String
    iLoc = InStrPS(1, Command$, "--login ")
    If (iLoc > 0) Then
        sLoginPID = Trim$(Mid$(Command$, iLoc + 7, 5))
        If (Len(sLoginPID) = 4) Then
            AutoLogin = UserLogin.AuthenticateUser(sLoginPID)
        End If
    End If
End Function

Public Sub LogError(ByVal sModule As String, _
                    ByVal sMethod As String, _
                    ByVal iErr As Long, _
                    ByVal sDescription As String, _
                    Optional ByVal sExtraInfo As String = vbNullString, _
                    Optional ByVal PatientId As Long = 0, Optional ByVal AppointmentId As Long = 0)

Dim sErrorMessage As String
Dim sErrorLog     As String
Dim iFileNum      As Integer

    On Error Resume Next
    Err.Raise (iErr)
    
    ' Log exception
    sErrorMessage = "Product: " & Trim$(App.ProductName) & vbTab _
        & "Revision: " & Trim$(App.Revision) & vbTab _
        & "Module: " & sModule & vbTab _
        & "Method: " & sMethod & vbTab _
        & "Number: " & Str$(Err.Number) & vbTab _
        & "Description: " & sDescription & vbTab _
        & "Details: " & sExtraInfo & vbTab
        
    Call InsertLog(sModule, sErrorMessage, LoginCatg.Exception, "", "", PatientId, AppointmentId, 2)
    Err.Clear
    On Error GoTo 0

End Sub
Public Sub ParseConnectionString(sConnectionString As String)
Dim i As Long
Dim j As Long
    On Error GoTo lParseConnectionString_Error
    sServerSQL = vbNullString
    sDatabase = vbNullString
    i = InStrPS(dbConnection, "SERVER=")
    If i > 0 Then
        j = InStrPS(i, dbConnection, ";")
        If j > 0 Then
            sServerSQL = Mid$(dbConnection, i + 7, j - i - 7)
        End If
    End If
    i = InStrPS(dbConnection, "DATABASE=")
    If i > 0 Then
    sDatabaseCurr = Mid$(dbConnection, i + 9)
    j = InStrPS(i, dbConnection, ";")
        If j > 0 Then
            sDatabase = Mid$(dbConnection, i + 9, j - i - 9)
        End If
    End If
    Exit Sub

lParseConnectionString_Error:

    LogError "CommonLib", "ParseConnectionString", Err, Err.Description
End Sub

Public Function myTrim(inp As String, Optional i As Integer = 0, Optional NumOnly As Boolean) As String
Dim b As Integer
Dim out As String
Dim fInp As String
fInp = Trim(inp)

If i = 0 Then
    For b = 1 To Len(inp)
        out = out & Mid(fInp, b, 1)
        If Not Len(out) = Len(Trim(out)) Then Exit For
    Next
Else
    For b = Len(inp) To 1 Step -1
        out = Mid(fInp, b, 1) & out
        If Not Len(out) = Len(Trim(out)) Then Exit For
    Next
End If

out = Trim(out)

If NumOnly Then
    Do Until IsNumeric(out) Or out = ""
        out = Mid(out, 2)
    Loop
End If

myTrim = out
End Function

Public Function myCCur(inp As String) As String
If IsNumeric(inp) Then
    inp = Round(inp, 2)
    myCCur = Format(inp, "######0.00")
Else
    myCCur = "0.00"
End If
End Function

Public Function LoadAccessAds() As String()
Dim iCount As Integer
Dim DisplayText As String
Dim asAds(15) As String
Dim oPracticeCodes As New PracticeCodes
    oPracticeCodes.ReferenceType = "AccessAds"
    If oPracticeCodes.FindCode > 0 Then
        iCount = 1
        Do Until (Not (oPracticeCodes.SelectCode(iCount)))
            DisplayText = Space(512)
            With oPracticeCodes
                If Trim$(.LetterLanguage) <> "" Then ' To support both old and new (top) way of loading AccessAds
                    DisplayText = Trim$(Str$(.Rank)) & " - " & Trim$(.ReferenceCode) & " : " & Trim$(.LetterLanguage)
                    DisplayText = DisplayText & Space(512 - Len(DisplayText))
                Else
                    Mid(DisplayText, 1, Len(Trim(.ReferenceCode))) = Trim(.ReferenceCode)
                    Mid(DisplayText, 65, Len(Trim(.ReferenceAlternateCode))) = Trim(.ReferenceAlternateCode)
                    Mid(DisplayText, 128, Len(Trim(.LetterLanguage))) = Trim(.LetterLanguage)
                    Mid(DisplayText, 240, 7) = Trim(Str(.Rank))
                    Mid(DisplayText, 248, 1) = " "
                    If (.FollowUp = "F") Then
                        Mid(DisplayText, 248, 1) = "F"
                    End If
                    Mid(DisplayText, 249, Len(Trim(.TestOrder))) = Trim(.TestOrder)
                    Mid(DisplayText, 254, 1) = "F"
                    If (.Signature = "T") Then
                        Mid(DisplayText, 254, 1) = "T"
                    End If
                    Mid(DisplayText, 255, 1) = "F"
                    If (.Exclusion = "T") Then
                        Mid(DisplayText, 255, 1) = "T"
                    End If
                    Mid(DisplayText, 256, Len(Trim(.LetterLanguage1))) = Trim(.LetterLanguage1)
                End If
            End With
            asAds(iCount - 1) = DisplayText
            iCount = iCount + 1
        Loop
    End If
    Set oPracticeCodes = Nothing
    LoadAccessAds = asAds
End Function

Public Function recCount(RS As ADODB.Recordset) As Long
Dim fRecCount As Long

With RS
    .MoveFirst
    Do Until .EOF
        fRecCount = fRecCount + 1
        .MoveNext
    Loop
    .MoveFirst
End With

recCount = fRecCount
End Function

Public Function fixNull(txt As Object) As String
fixNull = ""
If Not IsNull(txt) Then fixNull = Trim(txt)

End Function

Public Function LoadConfigCollection() As Boolean
Dim iIndex As Long
Dim InterfaceConfiguration As New PracticeConfigureInterface
    On Error GoTo lLoadConfigCollection_Error

    Set ConfigCollection = New Collection
    With InterfaceConfiguration
        If .FindConfigure > 0 Then
            iIndex = 1
            While .SelectConfigure(iIndex)
                If InCollection(ConfigCollection, .ConfigureInterfaceField) Then
                    LogError "CommonLib", "LoadConfigCollection", Err, Err.Description, .ConfigureInterfaceField & " duplicated in PracticeInterfaceConfig."
                Else
                    ConfigCollection.Add .ConfigureInterfaceValue, .ConfigureInterfaceField
                End If
                iIndex = iIndex + 1
            Wend
        End If
    End With
    Set InterfaceConfiguration = Nothing

    Exit Function

lLoadConfigCollection_Error:

    LogError "CommonLib", "LoadConfigCollection", Err, Err.Description
End Function

Private Function InCollection(col As Collection, sKey As String) As Boolean
Dim FTest As Boolean
    On Error Resume Next
    
    
    FTest = IsObject(col(sKey))
    If Err = 0 Then
        InCollection = True
    Else
        Err.Clear
    End If
End Function

Public Function CheckConfigCollection(sKey As String, Optional sDefaultValue As String = "") As String
    If InCollection(ConfigCollection, sKey) Then
        CheckConfigCollection = ConfigCollection(sKey)
    Else
        CheckConfigCollection = sDefaultValue
    End If
End Function

Public Function GetRS(SQL As String) As ADODB.Recordset
Dim frS As ADODB.Recordset
Set frS = CreateAdoRecordset

frS.ActiveConnection = MyPracticeRepositoryCmd.ActiveConnection
frS.CursorType = adOpenStatic
frS.LockType = adLockOptimistic

frS.Source = SQL
frS.Open
Set GetRS = frS
Set frS = Nothing
End Function

Public Function IsE_Send(DrugName As String, Strength As String) As Integer
Dim SQL As String
Dim rsFdbView As Recordset
SQL = "SELECT MED_REF From FDBView " & _
      " where MED_DESC = '" & DrugName & "' "
If Strength = "" Then
    SQL = SQL & _
    " and MED_STRENGTH_UOM is null "
Else
    SQL = SQL & _
    " and MED_STRENGTH_UOM = '" & Strength & "' "
End If
SQL = SQL & " order by 1"
Set rsFdbView = GetRS(SQL)

If rsFdbView.EOF Then
    IsE_Send = 1
Else
    IsE_Send = rsFdbView(0)
    If IsE_Send = 0 Then IsE_Send = 9
End If

End Function

'=========== test only =======================
Public Sub logPatientId(SPatId As Long, Optional act As String)
    Select Case act
    Case "set"
        mPatId = SPatId
    Case ""
        If mPatId = SPatId Then
        Else
            MsgBox "PatientId mismatch: " & SPatId & vbNewLine & _
            "Please call IO" _
            , vbCritical, "ERROR"
        End If
    End Select
End Sub
'=========== test only =======================

Private Function GetCategoryName(Catgvalue As Integer)
    GetCategoryName = ""
    Select Case Catgvalue
     Case 1
        GetCategoryName = "Log In"
     Case 2
        GetCategoryName = "Log Out"
     Case 3
        GetCategoryName = "Application Exit"
     Case 4
        GetCategoryName = "View PatientLetter"
     Case 5
        GetCategoryName = "Create PatientLetter"
     Case 6
        GetCategoryName = "Patient Info Delete"
     Case 7
        GetCategoryName = "Room Insert"
     Case 8
        GetCategoryName = "Room Edit"
     Case 9
        GetCategoryName = "User Insert"
     Case 10
        GetCategoryName = "User Edit"
     Case 11
        GetCategoryName = "Add a Patient"
     Case 12
        GetCategoryName = "Patient Info Edit"
     Case 13
        GetCategoryName = "Bill Generation"
     Case 14
        GetCategoryName = "Medication Insert"
     Case 15
        GetCategoryName = "Encounter Close"
     Case 16
        GetCategoryName = "Encounter Open"
     Case 17
        GetCategoryName = "Medication Renew"
     Case 18
        GetCategoryName = "Print Document"
     Case 19
        GetCategoryName = "View Document"
     Case 20
        GetCategoryName = "ExamElements Insert"
     Case 21
        GetCategoryName = "Appointment Create"
     Case 22
        GetCategoryName = "Order Entry"
     Case 23
        GetCategoryName = "Patient Notes"
     Case 24
        GetCategoryName = "Patient History"
     Case 25
        GetCategoryName = "Encounter Save"
     Case 26
        GetCategoryName = "Medication Print"
     Case 27
        GetCategoryName = "Medication Discontinue"
     Case 28
        GetCategoryName = "Medication Delete"
     Case 29
        GetCategoryName = "Medication Continue"
     Case 30
        GetCategoryName = "Patient LogIn"
     Case 31
        GetCategoryName = "Patient LogOut"
     Case 32
        GetCategoryName = "Tools"
     Case 33
        GetCategoryName = "Clinical Import"
     Case 34
        GetCategoryName = "Machine Configuration"
     Case 35
        GetCategoryName = "Medicare FeeUtility"
     Case 36
        GetCategoryName = "RxUtility"
     Case 37
        GetCategoryName = "Application Open"
     Case 38
        GetCategoryName = "BringFindingsForwardCopy"
     Case 39
        GetCategoryName = "Query"
     Case 99
        GetCategoryName = "Exception"
    End Select
End Function
Private Function SetLogger(ByVal Source As String)
    Dim logManager As New IO_Practiceware_Interop.logManager
    Dim loggerProperty
    Set SetLogger = logManager.GetLogger(Source)
End Function
Private Function loggerProperty(ByVal Logger, ByVal PropertyName As String, ByVal PropertyValue As String)
     Set loggerProperty = Logger.CreateProperty(PropertyName, PropertyValue)
End Function
Public Sub InsertLog(ByVal Source As String, ByVal Message As String, _
                     Optional ByVal Categories As Integer = 0, _
                     Optional ByVal PropertyName As String, Optional ByVal PropertyValue As String, _
                     Optional ByVal PatientId As Long = 0, Optional ByVal AppointmentId As Long = 0, _
                     Optional ByVal Severity As Integer = 0, Optional ByVal title As String)
   On Error GoTo ExitFast
    Dim Logger
    Set Logger = SetLogger(Source)
    Dim arrCategories() As Variant
    Dim arrloggerProperty() As Variant
    Dim loggerProperty, patientLoggerProperty, AppointmentLoggerProperty
    Dim TempPatientId, TempAppointmentId As Long
    Dim i As Integer
    
    TempPatientId = ActivePatientId
    TempAppointmentId = AppointmentId
    
    ActivePatientId = 0
    ActiveAppointmentId = 0
      
    If Categories > 0 Then
        ReDim arrCategories(0) As Variant
        arrCategories(0) = GetCategoryName(Categories)
    Else
        arrCategories = Array()
    End If
    
    If Not (PropertyName = "" And PropertyValue = "") Then
        Set loggerProperty = Logger.CreateProperty(PropertyName, PropertyValue)
        ReDim Preserve arrloggerProperty(0)
        arrloggerProperty = Array(loggerProperty)
    End If
    
   If PatientId > 0 Then
        Set patientLoggerProperty = Logger.CreateProperty("PatientId", PatientId)
        ReDim Preserve arrloggerProperty(1)
        If IsEmpty(loggerProperty) Then
            arrloggerProperty = Array(patientLoggerProperty)
        Else
            arrloggerProperty = Array(loggerProperty, patientLoggerProperty)
        End If
    End If
    
    If AppointmentId > 0 Then
        Set AppointmentLoggerProperty = Logger.CreateProperty("AppointmentId", AppointmentId)
        If PatientId = 0 Then
            If IsEmpty(loggerProperty) Then
                arrloggerProperty = Array(AppointmentLoggerProperty)
            Else
                arrloggerProperty = Array(loggerProperty, AppointmentLoggerProperty)
            End If
        Else
            ReDim Preserve arrloggerProperty(2)
            If IsEmpty(loggerProperty) Then
                arrloggerProperty = Array(patientLoggerProperty, AppointmentLoggerProperty)
            Else
                arrloggerProperty = Array(loggerProperty, patientLoggerProperty, AppointmentLoggerProperty)
            End If
        End If
    End If
    If ArrayExists(ArrPtr(arrloggerProperty)) = 0 Then
        arrloggerProperty = Array()
    End If
    
    Call Logger.Log(Message, Categories:=arrCategories, title:=title, Source:=Source, Properties:=arrloggerProperty, Severity:=Severity)
    ActivePatientId = TempPatientId
    ActiveAppointmentId = TempAppointmentId
    Exit Sub
ExitFast:
   If Err.Number <> 0 Then
        Err.Clear
    End If
    ActivePatientId = TempPatientId
    ActiveAppointmentId = TempAppointmentId
End Sub
Public Function DisplayInsuranceScreen(PatientId As Long)
On Error GoTo lDisplayInsuranceScreen
    
    Dim InsuranceInfoComWrapper As New comWrapper
Dim loadArguments As Object
Dim arguments() As Variant

DisplayInsuranceScreen = False

InsuranceInfoComWrapper.RethrowExceptions = False

Call InsuranceInfoComWrapper.Create(PatientInfoLoadArgumentsType, emptyArgs)

Set loadArguments = InsuranceInfoComWrapper.Instance

loadArguments.PatientId = PatientId

loadArguments.TabToOpenInt = 1

arguments = Array(loadArguments)
    
Call InsuranceInfoComWrapper.Create(PatientInfoViewManagerType, emptyArgs)

Call InsuranceInfoComWrapper.InvokeMethod("ShowPatientInfo", arguments)

Set InsuranceInfoComWrapper = Nothing

DisplayInsuranceScreen = True

Exit Function

lDisplayInsuranceScreen:
 LogError "CommonLib", "DisplayInsuranceScreen", Err, Err.Description
End Function

Public Function DisplayPatientSearchScreen(Optional ByVal CanCreatePatient As Boolean = True) As Variant
On Error GoTo lDisplayPatientSearchScreen_Error

    Dim PatientSearchComWrapper As New comWrapper
Dim loadArguments As Object
Dim arguments() As Variant


PatientSearchComWrapper.RethrowExceptions = False

Call PatientSearchComWrapper.Create(PatientSearchLoadArgumentsType, emptyArgs)
Set loadArguments = PatientSearchComWrapper.Instance
loadArguments.CanCreatePatient = CanCreatePatient

arguments = Array(loadArguments)
    
Call PatientSearchComWrapper.Create(PatientSearchViewManagerType, emptyArgs)
Set DisplayPatientSearchScreen = PatientSearchComWrapper.InvokeMethod("ShowPatientSearchView", arguments)

If (DisplayPatientSearchScreen.PatientId > 0) Then ActivePatientId = DisplayPatientSearchScreen.PatientId
Set PatientSearchComWrapper = Nothing

Exit Function

lDisplayPatientSearchScreen_Error:
    Set DisplayPatientSearchScreen = Nothing
    LogError "CommonLib", "DisplayPatientSearchScreen", Err, Err.Description
End Function
Public Function ConvertInsuranceTypeToLegacy(ByVal InsuranceTypeId As String) As String
ConvertInsuranceTypeToLegacy = "M"
Select Case InsuranceTypeId
Case "2"
    ConvertInsuranceTypeToLegacy = "V"
Case "3"
    ConvertInsuranceTypeToLegacy = "A"
Case "4"
    ConvertInsuranceTypeToLegacy = "W"
End Select
End Function
Public Function ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode(ByVal ClaimFilingIndicatorCodeId As Long) As String
ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = ""
Select Case CLng(ClaimFilingIndicatorCodeId)

    Case 10
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "P"
    Case 1
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "K"
    Case 12
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "L"
    Case 14
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "F"
    Case 7
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "I"
    Case 19
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "C"
    Case 18
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "N"
    Case 24
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "Z"
    Case 20
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "Y"
    Case 21
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "T"
    Case 11
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "H"
    Case 22
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "V"
    Case 23
        ConvertClaimFilingIndicatorToLegacyInsurerReferenceCode = "W'"
End Select
End Function
Public Sub DisplayInsurancePlanScreen(Optional PlanName As String)
On Error GoTo lDisplayInsurancePlanScreen

Dim InsurancePlanComWrapper As New comWrapper
Dim loadArguments As Object
Dim arguments() As Variant
Call InsurancePlanComWrapper.Create(ManageInsurancePlansLoadArgumentsType, emptyArgs)
Set loadArguments = InsurancePlanComWrapper.Instance

If PlanName <> "" Then loadArguments.Filter = PlanName

arguments = Array(loadArguments)
Call InsurancePlanComWrapper.Create(InsurancePlansSetupViewManagerType, emptyArgs)
Call InsurancePlanComWrapper.InvokeMethod("ManageInsurancePlans", arguments)

Exit Sub
lDisplayInsurancePlanScreen:
    LogError "CommonLib", "DisplayInsurancePlanScreen", Err, Err.Description
End Sub
Public Function DisplayInsurancePlanSearchScreen() As Object
On Error GoTo lDisplayInsurancePlanSearchScreen_Error

Dim InsurancePlanSearchComWrapper As New comWrapper
Set DisplayInsurancePlanSearchScreen = Nothing
Call InsurancePlanSearchComWrapper.Create(InsurancePolicyViewManagerType, emptyArgs)
Set DisplayInsurancePlanSearchScreen = InsurancePlanSearchComWrapper.InvokeMethod("ShowSearchInsurancePlanView", emptyArgs)
Exit Function
lDisplayInsurancePlanSearchScreen_Error:
    LogError "CommonLib", "DisplayInsurancePlanSearchScreen", Err, Err.Description
End Function
Public Sub DisplayNotesScreen(ByVal PatientId As Long)
On Error GoTo lDisplayNotesScreen_Error

Dim NotesComWrapper As New comWrapper
Dim loadArguments As Object
Dim arguments() As Variant

Call NotesComWrapper.Create(NoteViewLoadArgumentsType, emptyArgs)
Set loadArguments = NotesComWrapper.Instance
If PatientId > 0 Then
    loadArguments.EntityId = PatientId
End If
loadArguments.NoteTypeId = 1
arguments = Array(loadArguments)

Call NotesComWrapper.Create(NoteViewManagerType, emptyArgs)
Call NotesComWrapper.InvokeMethod("LaunchManageNotesView", arguments)
Exit Sub
lDisplayNotesScreen_Error:
    LogError "CommonLib", "DisplayNotesScreen", Err, Err.Description
End Sub
Public Sub KillCurrentProcess()
    Call Shell("taskkill /pid  " & CStr(GetCurrentProcessId) & " /f")
End Sub
