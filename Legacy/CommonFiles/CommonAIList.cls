VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CommonAIList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function ApplGetBalancebyInvoice(InvoiceId As String, PatBal As Single, InsBal As Single, AllocateOn As Boolean, ExcludeAdjustments As Boolean) As Single
Dim i As Integer
Dim k As Integer
Dim Temp As String
Dim Paid As Single, AlAmt As Single
Dim Charge As Single, AdjPaid As Single
Dim InsPaid As Single, PatPaid As Single
Dim PatBalance As Single, InsBalance As Single
Dim RetRcv As PatientReceivables
Dim RetPay As PatientReceivablePayment
Dim RetTrans As PracticeTransactionJournal
Paid = 0
AlAmt = 0
AdjPaid = 0
InsBalance = 0
PatBalance = 0
ApplGetBalancebyInvoice = 0
If (Trim(InvoiceId) <> "") Then
    Set RetRcv = New PatientReceivables
    Set RetPay = New PatientReceivablePayment
    Set RetTrans = New PracticeTransactionJournal
    RetRcv.AppointmentId = 0
    RetRcv.PatientId = 0
    RetRcv.ReceivableInvoice = InvoiceId
    RetRcv.ReceivableInvoiceDate = ""
    RetRcv.InsurerId = 0
    RetRcv.ReceivableStatus = ""
    RetRcv.ReceivableType = ""
    If (RetRcv.FindPatientReceivable > 0) Then
        i = 1
        While (RetRcv.SelectPatientReceivable(i))
            Charge = RetRcv.ReceivableAmount
            Paid = Charge - RetRcv.ReceivableBalance
            If (ExcludeAdjustments) Then
                RetPay.ReceivableId = RetRcv.ReceivableId
                AdjPaid = AdjPaid + RetPay.RetrievePatientReceivableAdjustments
            End If
            RetTrans.TransactionJournalType = "R"
            RetTrans.TransactionJournalTypeId = RetRcv.ReceivableId
            RetTrans.TransactionJournalStatus = "-Z"
            RetTrans.TransactionJournalReference = ""
            RetTrans.TransactionJournalAction = ""
            RetTrans.TransactionJournalDate = ""
            If (RetTrans.FindTransactionJournal > 0) Then
                k = 1
                While (RetTrans.SelectTransactionJournal(k))
                    If (InStr(RetTrans.TransactionJournalRemark, "/Amt/") <> 0) Then
                        Temp = Trim(Mid(RetTrans.TransactionJournalRemark, 6, 20))
                        AlAmt = AlAmt + Val(Temp)
                        If (RetTrans.TransactionJournalReference = "P") Then
                            PatBalance = PatBalance + Round(Val(Trim(Temp)), 2)
                        End If
                        If (RetTrans.TransactionJournalReference = "I") Then
                            InsBalance = InsBalance + Round(Val(Trim(Temp)), 2)
                        End If
                    End If
                    k = k + 1
                Wend
            End If
            i = i + 1
        Wend
    End If
    Set RetRcv = Nothing
    Set RetPay = Nothing
    Set RetTrans = Nothing
    If (ExcludeAdjustments) Then
        Paid = Paid - AdjPaid
        If (Paid < 0) Then
            Paid = 0
        End If
    End If
    If (AllocateOn) Then
        ApplGetBalancebyInvoice = Round(Charge - (Paid + AlAmt), 2)
        PatBal = PatBalance
        InsBal = InsBalance
    Else
        ApplGetBalancebyInvoice = Round(Charge - Paid, 2)
        PatBal = Paid
        InsBal = Charge
    End If
    If (ApplGetBalancebyInvoice > -1) And (ApplGetBalancebyInvoice < 0) Then
        ApplGetBalancebyInvoice = 0
    End If
End If
End Function

Public Function ApplGetClosingMonth() As String
On Error GoTo lApplGetClosingMonth_Error

Dim StrSql As String
Dim RS As Recordset
Dim i As Long
Dim Fini As Boolean
Dim RetPrc As PracticeConfigureInterface
ApplGetClosingMonth = ""
Set RetPrc = New PracticeConfigureInterface
RetPrc.ConfigureInterface = "S"
RetPrc.ConfigureInterfaceField = ""
If (RetPrc.FindConfigure > 0) Then
    i = 1
    While (RetPrc.SelectConfigure(i)) And Not (Fini)
        If (RetPrc.ConfigureInterfaceField = "EDITWINDOW") Then
            ApplGetClosingMonth = Trim(RetPrc.ConfigureInterfaceValue)
            If (InStr(ApplGetClosingMonth, "/") = 0) Then
                ApplGetClosingMonth = Mid(ApplGetClosingMonth, 1, 2) + "/" + Mid(ApplGetClosingMonth, 3, 2) + "/" + Mid(ApplGetClosingMonth, 5, 4)
            End If
            Fini = True
        End If
        i = i + 1
    Wend
End If
Set RetPrc = Nothing

'In .net uncomment if required
'StrSql = " Select TOP 1 dbo.ExtractTextValue('<dateTime>',ISNULL(Value,''),'</dateTime>') from model.ApplicationSettings WHERE Name ='CloseDatetime' "

'Set RS = GetRS(StrSql)
'If Not RS.EOF Then
'    If RS.RecordCount > 0 Then ApplGetClosingMonth = RS(0)
'End If
Exit Function

lApplGetClosingMonth_Error:
    ApplGetClosingMonth = ""
End Function

Public Function ApplGetPatientPaidbyInvoice(InvoiceId As String, ExcludeAdjustments As Boolean) As Single
Dim i As Integer
Dim Paid As Single
Dim AdjAmount As Single
Dim RetRcv As PatientReceivables
Dim RetPay As PatientReceivablePayment
Paid = 0
AdjAmount = 0
ApplGetPatientPaidbyInvoice = 0
If (Trim(InvoiceId) <> "") Then
    Set RetRcv = New PatientReceivables
    Set RetPay = New PatientReceivablePayment
    RetRcv.AppointmentId = 0
    RetRcv.PatientId = 0
    RetRcv.ReceivableInvoice = InvoiceId
    RetRcv.ReceivableInvoiceDate = ""
    RetRcv.InsurerId = 0
    RetRcv.ReceivableStatus = ""
    RetRcv.ReceivableType = ""
    If (RetRcv.FindPatientReceivable > 0) Then
        i = 1
        While (RetRcv.SelectPatientReceivable(i))
            RetPay.ReceivableId = RetRcv.ReceivableId
            RetPay.PaymentPayerId = RetRcv.PatientId
            Paid = Paid + RetPay.RetrievePatientReceivableTotalPaidbyPatient
            AdjAmount = AdjAmount + RetPay.RetrievePatientReceivableAdjustments
            If (RetRcv.InsuredId <> RetRcv.PatientId) Then
                RetPay.ReceivableId = RetRcv.ReceivableId
                RetPay.PaymentPayerId = RetRcv.PatientId
                Paid = Paid + RetPay.RetrievePatientReceivableTotalPaidbyPatient
                AdjAmount = AdjAmount + RetPay.RetrievePatientReceivableAdjustments
            End If
            i = i + 1
        Wend
    End If
    Set RetRcv = Nothing
    Set RetPay = Nothing
    If (ExcludeAdjustments) Then
        ApplGetPatientPaidbyInvoice = Round(Paid - AdjAmount, 2)
    Else
        ApplGetPatientPaidbyInvoice = Round(Paid, 2)
    End If
    If (ApplGetPatientPaidbyInvoice > -1) And (ApplGetPatientPaidbyInvoice < 0) Then
        ApplGetPatientPaidbyInvoice = 0
    End If
End If
End Function

Public Function ApplSetClosingMonth(TheDate As String) As Boolean
On Error GoTo lApplSetClosingMonth_Error

Dim StrSql As String
Dim i As Long
Dim Fini As Boolean
Dim RetPrc As PracticeConfigureInterface
ApplSetClosingMonth = False
Set RetPrc = New PracticeConfigureInterface
RetPrc.ConfigureInterface = "S"
RetPrc.ConfigureInterfaceField = ""
If (RetPrc.FindConfigure > 0) Then
    i = 1
    While (RetPrc.SelectConfigure(i)) And Not (Fini)
        If (RetPrc.ConfigureInterfaceField = "EDITWINDOW") Then
            RetPrc.ConfigureInterfaceValue = Trim(TheDate)
            Call RetPrc.ApplyConfigure
            Fini = True
        End If
        i = i + 1
    Wend
End If
Set RetPrc = Nothing

'Update model.ApplicationSetting row used in .Net.
StrSql = "DECLARE @Value NVARCHAR(MAX), @Name NVARCHAR(MAX)" & _
" SET @Name = 'CloseDateTime' " & _
" SET @Value = '<dateTime>" & Format(TheDate, "yyyy-MM-dd") & "T00:00:00Z</dateTime>' " & _
" IF EXISTS (SELECT Id FROM model.ApplicationSettings WHERE Name = @Name) " & _
" BEGIN " & _
" Update Model.ApplicationSettings SET Value = @Value WHERE Name = @Name " & _
" End " & _
" Else BEGIN " & _
" INSERT INTO model.ApplicationSettings (Name, Value) VALUES (@Name, @Value) END "

GetRS (StrSql)
ApplSetClosingMonth = True
Exit Function

lApplSetClosingMonth_Error:
    ApplSetClosingMonth = False
End Function

Public Function ApplSetReceivableBalance(InvId As String, ADate As String, BalTestOn As Boolean) As Boolean
Dim i As Integer
Dim Paid As Single
Dim UnBal As Single
Dim Charge As Single
Dim ALabel As Label
Dim RetRec As PatientReceivables
Dim RetPay As PatientReceivablePayment
Dim ComTemp As CommonTemplates
Dim ComAI2 As CommonAIList2
If (Trim(InvId) <> "") And (Len(InvId) > 9) Then
    Set RetRec = New PatientReceivables
    Set RetPay = New PatientReceivablePayment
    RetRec.ReceivableInvoice = InvId
    If (RetRec.FindPatientReceivable > 0) Then
        Paid = 0
        i = 1
        Set ComAI2 = New CommonAIList2
        Do Until Not (RetRec.SelectPatientReceivable(i))
            Charge = RetRec.ReceivableAmount
            RetPay.ReceivableId = RetRec.ReceivableId
            RetPay.PaymentPayerId = 0
            RetPay.PaymentPayerType = ""
            Paid = Paid + RetPay.RetrievePatientReceivableTotalPaid
            i = i + 1
        Loop
        If (Paid >= Charge) Then
            UnBal = 0
        Else
            UnBal = ComAI2.ApplGetUnallocatedBalancebyInvoice(InvId)
        End If
        i = 1
        While (RetRec.SelectPatientReceivable(i))
            RetRec.ReceivableBalance = Round(RetRec.ReceivableAmount - Paid, 2)
            If (RetRec.ReceivableAmount - Paid < 0.01) And (RetRec.ReceivableAmount - Paid > -0.01) Then
                RetRec.ReceivableBalance = 0
            End If
            If (RetRec.ReceivableBalance = 0) Or (RetRec.ReceivableAmount = 0) Then
                RetRec.ReceivableUnAllocated = 0
            Else
                RetRec.ReceivableUnAllocated = Round(UnBal, 2)
                If (UnBal < 0.01) And (UnBal > -0.01) Then
                    RetRec.ReceivableUnAllocated = 0
                End If
            End If
            If (Trim(ADate) <> "") Then
                If (ADate > RetRec.ReceivableLastPayDate) Then
                    RetRec.ReceivableLastPayDate = ADate
                End If
            End If
            Call RetRec.ApplyPatientReceivable
            i = i + 1
        Wend
    End If
    Set RetRec = Nothing
    Set RetPay = Nothing
    If (BalTestOn) Then
        Set ComTemp = New CommonTemplates
        Call ComTemp.ResetPatientBillDate(InvId, False, ALabel)
        Call ComTemp.ResetInsurerDesignation(InvId, False, ALabel)
        Set ComTemp = Nothing
    End If
End If
End Function

Public Function FormatInvoiceNumber(PatId As Long, ApptId As Long) As String
Dim BaseWidth As Integer
Dim BaseSize1 As Integer
Dim BaseSize2 As Integer
Dim Ref1 As String
Dim Ref2 As String
BaseWidth = 5
BaseSize1 = BaseWidth
BaseSize2 = BaseWidth
Ref1 = Trim(Str(PatId))
If (Len(Ref1) > 5) Then
    BaseSize1 = Len(Ref1)
End If
Ref2 = Trim(Str(ApptId))
If (Len(Ref2) > 5) Then
    BaseSize2 = Len(Ref2)
End If
If (BaseSize1 <> Len(Ref1)) Then
    Ref1 = String(BaseSize1 - Len(Ref1), "0") + Ref1
End If
If (BaseSize2 <> Len(Ref2)) Then
    Ref2 = String(BaseSize2 - Len(Ref2), "0") + Ref2
End If
FormatInvoiceNumber = Ref1 + "-" + Ref2
End Function
