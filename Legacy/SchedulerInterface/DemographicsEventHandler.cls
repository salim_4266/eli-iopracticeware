VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DemoGraphicsEventHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IO_Practiceware_Interop.IEventHandler

Private m_PatientId As Long
Private m_PatientScreenToOpen As String

Property Get PatientId() As Long
      PatientId = m_PatientId
End Property
Property Get PatientScreenToOpen() As String
      PatientScreenToOpen = m_PatientScreenToOpen
End Property


Private Sub IEventHandler_OnEvent(ByVal sender As Variant, ByVal e As Variant)
Dim frx As Form, fCount As Integer
Dim answer As Integer
Dim pat As New Patient
Dim PatRunsUsed As Long

m_PatientScreenToOpen = e.ScreenToOpen
m_PatientId = e.PatientId

If m_PatientId <= 0 Then Exit Sub

If ActivePatientId <> m_PatientId Then ActivePatientId = m_PatientId

If PatientScreenToOpen = "FinancialData" Then
    Call pat.GetPatientUpdate(PatientId, pat.runscmplted)
    Dim PatientDemographics As New PatientDemographics
    PatientDemographics.PatientId = PatientId
    Call PatientDemographics.DisplayPatientFinancialScreen
    Set PatientDemographics = Nothing
    Exit Sub
End If

'Clinical Data
If PatientScreenToOpen = "ClinicalData" Then
    For Each frx In Forms()
        If frx.Name = frmReviewAppts.Name Then
            fCount = 1
        End If
    Next
    If fCount > 0 Then
        answer = MsgBox("The PatientClinical is already open Do you want to save changes before closing?", vbInformation + vbYesNo, "Add Confirm")
        If answer = vbYes Then
             frmReviewAppts.FrmClose
        End If
    End If
    PatRunsUsed = pat.GetPatientAccess(m_PatientId)
    If PatRunsUsed = -1 Then
        frmEventMsgs.Header = " Access Blocked"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
    
    If IsNull(e.Parameter) Then e.Parameter = 0
    
    If (frmReviewAppts.LoadApptsList(m_PatientId, e.Parameter, e.Parameter > 0)) Then
        frmReviewAppts.Show 1
    Else
        frmEventMsgs.Header = "No Appointments"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        frmReviewAppts.FrmClose
    End If
    Exit Sub
End If


'Patient Notes
If PatientScreenToOpen = "Notes" Then
    For Each frx In Forms()
        If frx.Name = frmNotes.Name Then
            fCount = 1
        End If
    Next
    If fCount > 0 Then
        answer = MsgBox("The PatientNotes is already open Do you want to save changes before closing?", vbInformation + vbYesNo, "Add Confirm")
        If answer = vbYes Then
             frmNotes.FrmClose
        End If
    End If
    frmNotes.PatientId = m_PatientId
    frmNotes.NoteId = 0
    frmNotes.AppointmentId = 0
    frmNotes.SystemReference = ""
    frmNotes.CurrentAction = ""
    frmNotes.SetTo = "D"
    frmNotes.MaintainOn = True
    If (frmNotes.LoadNotes) Then
        DoEvents
        frmNotes.Show 1
    End If
    Exit Sub
End If
    
    
'Patient Documents
If PatientScreenToOpen = "Documents" Then
    For Each frx In Forms()
        If frx.Name = frmScan.Name Then
            fCount = 1
        End If
    Next
    If fCount > 0 Then
        answer = MsgBox("The PatientDocuments is already open Do you want to save changes before closing?", vbInformation + vbYesNo, "Add Confirm")
        If answer = vbYes Then
             frmScan.FrmClose
        End If
    End If
    frmScan.PatientId = m_PatientId
    frmScan.AppointmentId = 0
    If (frmScan.LoadScan) Then
        frmScan.Show 1
    End If
    Exit Sub
End If

'Patient Alerts
If PatientScreenToOpen = "Alerts" Then
        If (frmAlert.LoadAlerts(m_PatientId, e.Parameter, True)) Then
                frmAlert.Show 1
        Else
        frmEventMsgs.Header = "There are no active alerts for this patient."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        End If
        
    Exit Sub
End If

End Sub
