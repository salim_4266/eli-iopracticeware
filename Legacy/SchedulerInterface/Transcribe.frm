VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmTranscribe 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstPrint 
      Height          =   450
      ItemData        =   "Transcribe.frx":0000
      Left            =   1680
      List            =   "Transcribe.frx":0002
      TabIndex        =   8
      Top             =   480
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtDragon 
      Height          =   4935
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   960
      Width           =   11535
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   975
      Left            =   9840
      TabIndex        =   0
      Top             =   7680
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Transcribe.frx":0004
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdTrans 
      Height          =   975
      Left            =   7320
      TabIndex        =   3
      Top             =   7680
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Transcribe.frx":01E3
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdRetTrans 
      Height          =   975
      Left            =   2640
      TabIndex        =   5
      Top             =   7680
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Transcribe.frx":03C8
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdLetter 
      Height          =   975
      Left            =   5040
      TabIndex        =   6
      Top             =   7680
      Width           =   1935
      _Version        =   131072
      _ExtentX        =   3413
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   12632256
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "Transcribe.frx":05B9
   End
   Begin VB.Label lblPrint 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Label1"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   240
      TabIndex        =   7
      Top             =   240
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label lblAudio 
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Label1"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   240
      TabIndex        =   4
      Top             =   720
      Width           =   480
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H0077742D&
      Caption         =   "Transcription"
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   4200
      TabIndex        =   1
      Top             =   240
      Width           =   2775
   End
End
Attribute VB_Name = "frmTranscribe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public NoteId As Long
Public PatientId As Long
Public AppointmentId As Long
Public PatName As String
Private WavFileName As String
Private bSetScroll  As Boolean

Private Sub cmdDone_Click()
Dim i As Integer
Dim CurLoc As Integer
Dim NoteRec As Long
Dim ATemp(4) As String
Dim RetNote As PatientNotes
Dim RetNewNote As PatientNotes
If (NoteId > 0) Then
    Erase ATemp
    CurLoc = 1
    If (Trim(txtDragon.Text) <> "") Then
' write the first set to the current note record
        For i = 1 To 4
            If (((i - 1) * 255) + CurLoc < Len(txtDragon.Text)) Then
                ATemp(i) = Mid(txtDragon.Text, ((i - 1) * 255) + CurLoc, 255)
            End If
        Next i
        If (NoteId > 0) And (Trim(ATemp(1)) <> "") Then
            NoteRec = NoteId
            GoSub WriteIt
        End If
' write any additional segments to new note records
        CurLoc = CurLoc + 1020
        While (Len(txtDragon.Text) > CurLoc)
            Erase ATemp
            For i = 1 To 4
                If (((i - 1) * 255) + CurLoc < Len(txtDragon.Text)) Then
                    ATemp(i) = Mid(txtDragon.Text, ((i - 1) * 255) + CurLoc, 255)
                End If
            Next i
            If (NoteId > 0) And (Trim(ATemp(1)) <> "") Then
                GoSub WriteNew
            End If
            CurLoc = CurLoc + 1020
        Wend
    End If
End If
Unload frmTranscribe
Exit Sub
WriteNew:
    Set RetNote = New PatientNotes
    RetNote.NotesId = NoteId
    If (RetNote.RetrieveNotes) Then
        Set RetNewNote = New PatientNotes
        RetNewNote.NotesId = 0
        If (RetNewNote.RetrieveNotes) Then
            RetNewNote.NotesAlertMask = RetNote.NotesAlertMask
            RetNewNote.NotesAppointmentId = RetNote.NotesAppointmentId
            RetNewNote.NotesAudioOn = RetNote.NotesAudioOn
            RetNewNote.NotesCategory = RetNote.NotesCategory
            RetNewNote.NotesClaimOn = RetNote.NotesClaimOn
            RetNewNote.NotesCommentOn = RetNote.NotesCommentOn
            RetNewNote.NotesDate = RetNote.NotesDate
            RetNewNote.NotesEye = RetNote.NotesEye
            RetNewNote.NotesHighlight = RetNote.NotesHighlight
            RetNewNote.NotesILPNRef = RetNote.NotesILPNRef
            RetNewNote.NotesOffDate = RetNote.NotesOffDate
            RetNewNote.NotesPatientId = RetNote.NotesPatientId
            RetNewNote.NotesSystem = RetNote.NotesSystem
            RetNewNote.NotesType = RetNote.NotesType
            RetNewNote.NotesUser = RetNote.NotesUser
            If (Trim(ATemp(1)) <> "") Then
                RetNewNote.NotesText1 = ATemp(1)
            End If
            If (Trim(ATemp(2)) <> "") Then
                RetNewNote.NotesText2 = ATemp(2)
            End If
            If (Trim(ATemp(3)) <> "") Then
                RetNewNote.NotesText3 = ATemp(3)
            End If
            If (Trim(ATemp(4)) <> "") Then
                RetNewNote.NotesText4 = ATemp(4)
            End If
            Call RetNewNote.ApplyNotes
        End If
        Set RetNewNote = Nothing
    End If
    Set RetNote = Nothing
    Return
WriteIt:
    Set RetNote = New PatientNotes
    RetNote.NotesId = NoteRec
    If (RetNote.RetrieveNotes) Then
        If (Trim(ATemp(1)) <> "") Then
            RetNote.NotesText1 = ATemp(1)
        End If
        If (Trim(ATemp(2)) <> "") Then
            RetNote.NotesText2 = ATemp(2)
        End If
        If (Trim(ATemp(3)) <> "") Then
            RetNote.NotesText3 = ATemp(3)
        End If
        If (Trim(ATemp(4)) <> "") Then
            RetNote.NotesText4 = ATemp(4)
        End If
        RetNote.NotesAudioOn = False
        Call RetNote.ApplyNotes
    End If
    Set RetNote = Nothing
    Return
End Sub

Private Sub cmdLetter_Click()
Dim i As Integer
Dim PostIt As Boolean
Dim ATemp(10) As String
Dim TransId As Long
Dim Temp As String
Dim KTemp As String
Dim RetNote As PatientNotes
Dim RetTrn As PracticeTransactionJournal
Dim ApplLetters As Letters
Dim ApplTemp As ApplicationTemplates
Erase ATemp
PostIt = False
If (Trim(txtDragon.Text) <> "") Then
    If (NoteId > 0) Then
        For i = 1 To Len(txtDragon.Text) Step 256
            ATemp((i / 255) + 1) = Mid(txtDragon.Text, i, 255)
        Next i
        If (Trim(ATemp(1)) <> "") Then
            Set RetNote = New PatientNotes
            RetNote.NotesId = NoteId
            If (RetNote.RetrieveNotes) Then
                If (Trim(ATemp(1)) <> "") Then
                    RetNote.NotesText1 = ATemp(1)
                End If
                If (Trim(ATemp(2)) <> "") Then
                    RetNote.NotesText2 = ATemp(2)
                End If
                If (Trim(ATemp(3)) <> "") Then
                    RetNote.NotesText3 = ATemp(3)
                End If
                If (Trim(ATemp(4)) <> "") Then
                    RetNote.NotesText4 = ATemp(4)
                End If
                RetNote.NotesAudioOn = False
                Call RetNote.ApplyNotes
            End If
            Set RetNote = Nothing
        End If
    End If
    PostIt = True
End If
If (PostIt) Then
    Set RetTrn = New PracticeTransactionJournal
    RetTrn.TransactionJournalTypeId = AppointmentId
    RetTrn.TransactionJournalType = "S"
    RetTrn.TransactionJournalStatus = "-Z"
    If (RetTrn.FindTransactionJournal > 0) Then
        i = 1
        Do Until Not (RetTrn.SelectTransactionJournal(i))
            TransId = RetTrn.TransactionJournalId
            Set ApplLetters = New Letters
            Temp = ApplLetters.ApplLettersFile(TransId)
            If (Trim(Temp) <> "") Then
                KTemp = ""
                If (KTemp = "[") Then
                    KTemp = "a"
                ElseIf (KTemp = "]") Then
                    KTemp = "d"
                End If
                If Not (FM.IsFileThere(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")) Then
                    lblPrint.Caption = "File Creation In Progress"
                    lblPrint.Visible = True
                    DoEvents
                    Set ApplTemp.lstBox = lstPrint
                    Call ApplTemp.PrintTransaction(TransId, 2, KTemp, True, True, Temp, False, 0, "S", "", 0)
                End If
                lblPrint.Visible = False
                DoEvents
                Call TriggerWord(DocumentDirectory + "Ltrs-" + KTemp + Trim(Str(TransId)) + ".doc")
                Set ApplLetters = Nothing
                Exit Do
            End If
            Set ApplLetters = Nothing
            i = i + 1
        Loop
    End If
    Set RetTrn = Nothing
End If
End Sub
Private Sub cmdRetTrans_Click()
Dim p As String
Dim ATemp As String
Dim AFile As String
AFile = SchedulerInterfaceDirectory + "Audio-" + Trim(Str(NoteId)) + ".txt"
If (FM.IsFileThere(AFile)) Then
    ATemp = ""
    FM.OpenFile AFile, FileOpenMode.InputFileOpenMode, FileAccess.ReadWrite, CLng(101)
    While (Not (EOF(101)))
        p = Input(1, 101)
        If (Asc(p) >= 32) Then
            ATemp = ATemp + p
        End If
    Wend
    FM.CloseFile CLng(101)
    txtDragon.Text = Trim(ATemp)
End If
End Sub

Private Sub cmdTrans_Click()
Dim p As String
Dim ATemp As String
Dim AFile As String
If (Trim(WavFileName) <> "") Then
    AFile = SchedulerInterfaceDirectory + "Audio-" + Trim(Str(NoteId)) + ".txt"
    FM.OpenFile AFile, FileOpenMode.Output, FileAccess.ReadWrite, CLng(101)
    Print #101, ""
    Print #101, ""
    Print #101, WavFileName
    Print #101, ""
    FM.CloseFile CLng(101)
    Call TriggerWord(AFile)
End If
End Sub

Private Sub Form_Load()
WavFileName = DocumentDirectory + "Audio-" + Trim(Str(NoteId)) + ".wav"
lblAudio.Caption = "Transcribe " + WavFileName + " for " + PatName
End Sub
Public Sub FrmClose()
Unload Me
End Sub

