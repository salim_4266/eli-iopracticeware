VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "Btn32a20.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmPendingClaims 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H0077742D&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Check1 
      BackColor       =   &H0077742D&
      Caption         =   "Show all"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000A&
      Height          =   375
      Left            =   4920
      TabIndex        =   20
      Top             =   480
      Visible         =   0   'False
      Width           =   1695
   End
   Begin MSFlexGridLib.MSFlexGrid lstClaims1 
      Height          =   6135
      Left            =   840
      TabIndex        =   9
      Top             =   1200
      Visible         =   0   'False
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   10821
      _Version        =   393216
      Cols            =   6
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      ForeColorSel    =   0
      BackColorBkg    =   7828525
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox lstReason 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "PendingClaims.frx":0000
      Left            =   6960
      List            =   "PendingClaims.frx":0002
      TabIndex        =   16
      Top             =   240
      Width           =   1575
   End
   Begin VB.ListBox lstDisb 
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4860
      ItemData        =   "PendingClaims.frx":0004
      Left            =   960
      List            =   "PendingClaims.frx":0006
      TabIndex        =   15
      Top             =   2520
      Visible         =   0   'False
      Width           =   9015
   End
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "PendingClaims.frx":0008
      Left            =   8640
      List            =   "PendingClaims.frx":000A
      TabIndex        =   6
      Top             =   240
      Width           =   1575
   End
   Begin VB.ListBox lstLoc 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      ItemData        =   "PendingClaims.frx":000C
      Left            =   10320
      List            =   "PendingClaims.frx":000E
      TabIndex        =   5
      Top             =   240
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdHome 
      Height          =   990
      Left            =   120
      TabIndex        =   0
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingClaims.frx":0010
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBill 
      Height          =   975
      Left            =   7200
      TabIndex        =   1
      Top             =   7680
      Width           =   1500
      _Version        =   131072
      _ExtentX        =   2646
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingClaims.frx":01EF
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   10440
      TabIndex        =   2
      Top             =   7680
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingClaims.frx":03CE
   End
   Begin MSFlexGridLib.MSFlexGrid lstClaims 
      Height          =   6135
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   10821
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      BackColorFixed  =   7104808
      ForeColorFixed  =   16777215
      BackColorSel    =   0
      ForeColorSel    =   16777215
      BackColorBkg    =   7828525
      GridColor       =   8388608
      AllowBigSelection=   0   'False
      FocusRect       =   2
      HighLight       =   0
      ScrollBars      =   2
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
      Height          =   375
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDisplayResults 
      Height          =   975
      Left            =   3120
      TabIndex        =   11
      Top             =   7680
      Width           =   1500
      _Version        =   131072
      _ExtentX        =   2646
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingClaims.frx":05AD
   End
   Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo2 
      Height          =   375
      Left            =   2760
      TabIndex        =   12
      Top             =   480
      Width           =   1815
      _Version        =   65537
      _ExtentX        =   3201
      _ExtentY        =   661
      _StockProps     =   93
      ForeColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1999/1/1"
      MaxDate         =   "2050/12/31"
      EditMode        =   0
      ShowCentury     =   -1  'True
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdBatch 
      Height          =   975
      Left            =   7200
      TabIndex        =   14
      Top             =   7680
      Width           =   1500
      _Version        =   131072
      _ExtentX        =   2646
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingClaims.frx":0797
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdClose 
      Height          =   975
      Left            =   5160
      TabIndex        =   18
      Top             =   7680
      Width           =   1500
      _Version        =   131072
      _ExtentX        =   2646
      _ExtentY        =   1720
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "PendingClaims.frx":0977
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H0077742D&
      Caption         =   "Select Insurer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   960
      Width           =   6735
   End
   Begin VB.Label lblReason 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Reason"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   6960
      TabIndex        =   17
      Top             =   0
      Width           =   660
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   2160
      TabIndex        =   13
      Top             =   480
      Width           =   345
   End
   Begin VB.Label lblDr 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   8640
      TabIndex        =   8
      Top             =   0
      Width           =   570
   End
   Begin VB.Label lblLoc 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Location"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   10320
      TabIndex        =   7
      Top             =   0
      Width           =   735
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Pending Claims"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   1995
   End
End
Attribute VB_Name = "frmPendingClaims"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public PatientId As Long
Private ProcessType As String
Private StartDate As String
Private EndDate As String
Private ReasonCode As String
Private CurrentRow As Long
Private CurrentCol As Long
Private ResourceId As Long
Private LocationId As Long
Private InsurerNameA As String

Private Sub Check1_Click()
'fishman future functions
End Sub

Private Sub cmdBatch_Click()
Dim i As Integer
frmEventMsgs.Header = "Batch"
frmEventMsgs.AcceptText = "View"
frmEventMsgs.RejectText = "Apply"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = "Clear"
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    lstDisb.Visible = True
ElseIf (frmEventMsgs.Result = 2) Then
    lstDisb.Visible = True
    DoEvents
    Call ApplyOptions(0)
    lstDisb.Clear
    lstDisb.Visible = False
    lstDisb.AddItem "Close Batch List"
    Call LoadPendingClaims(True, PatientId, ProcessType, False)
ElseIf (frmEventMsgs.Result = 3) Then
    lstDisb.Visible = True
    lstDisb.Clear
    lstDisb.Visible = False
    lstDisb.AddItem "Close Batch List"
    For i = 1 To lstClaims.Rows - 1
        lstClaims.TextMatrix(i, 0) = ""
    Next i
End If
End Sub

Private Sub cmdBill_Click()
frmEventMsgs.Header = "Bill Payer ?"
frmEventMsgs.AcceptText = ""
frmEventMsgs.RejectText = "Yes"
frmEventMsgs.CancelText = "Cancel"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 2) Then
'    Call BillOptions
    Dim r As Integer, i As Integer
    Dim srvs As Collection
    Dim currInv As String
    For r = 1 To lstClaims.Rows - 1
        If lstClaims.TextMatrix(r, 0) = "Y" Then
            Set srvs = Nothing
            Set srvs = New Collection
            i = r
            currInv = lstClaims1.TextMatrix(i, 1)
            Do While lstClaims1.TextMatrix(i, 1) = currInv
                srvs.Add lstClaims.TextMatrix(i, 11), lstClaims1.TextMatrix(i, 0)
                i = i + 1
            Loop
            
            Dim PatientDemographics As New PatientDemographics
            PatientDemographics.PatientId = val(Trim(lstClaims.TextMatrix(r, 2)))
            Call PatientDemographics.DisplayPatientFinancialScreen
            Set PatientDemographics = Nothing
            'We are using New .Net Screen for displaying PendingClaims..But Same FORM is being to display Followups.
'           'We wont get into this event as cmdBill is always visible False for Followups
'            With frmBillServices
'                .HiddenMode = True
'                If .LoadServices(lstClaims1.TextMatrix(r, 1), lstClaims1.TextMatrix(r, 2)) Then
'                    For i = 1 To .lstSrvs1.Rows - 1
'                        '.lstSrvs1.TextMatrix(i, 4) = srvs(.lstSrvs2.TextMatrix(i, 0))
'                        .lstSrvs1.TextMatrix(i, 5) = 1
'                        .lstSrvs1.TextMatrix(i, 6) = "B"
'                    Next
'                    '.Show 1
'                    'Call .BatchOut_5(frmPaymentsTotal.lstSrvs2.TextMatrix(frmPaymentsTotal.lstSrvs1.Row, 3))
'                End If
'            End With
        End If
        Unload frmBillServices
    Next
    
    Dim memR As Integer, memTR As Integer
    memR = lstClaims.Row
    memTR = lstClaims.TopRow
    Call LoadPendingClaims(True, PatientId, ProcessType, False)
    If Not memR < lstClaims.Rows Then memR = lstClaims.Rows - 1
    If Not memTR < lstClaims.Rows Then memTR = lstClaims.Rows - 1
    lstClaims.Row = memR
    lstClaims.TopRow = memTR
End If
End Sub

Private Sub cmdClose_Click()
Dim y1 As Single, y2 As Single
Dim ApplList As ApplicationAIList
Dim NonZero As Boolean
Dim r As Integer
If (CurrentRow > 0) Then
    If (val(lstClaims1.TextMatrix(CurrentRow, 1)) >= 0) Then
        'If (Val(lstClaims.TextMatrix(CurrentRow, 11)) <= 0) Then
        '------------------------------------------------------------
        ' check all services
        NonZero = False
        r = CurrentRow
        Do
            If (val(lstClaims.TextMatrix(r, 11)) > 0) Then
                NonZero = True
                Exit Do
            End If
            
            r = r + 1
            If r = lstClaims.Rows Then Exit Do
            If Not lstClaims.TextMatrix(r, 1) = "" Then Exit Do
        Loop
        '------------------------------------------------------------
        If Not NonZero Then
            Set ApplList = New ApplicationAIList
            Call ApplList.ApplSetReceivableStatus(val(lstClaims1.TextMatrix(CurrentRow, 1)), "S")
            Set ApplList = Nothing
            Call LoadPendingClaims(True, PatientId, ProcessType, False)
        Else
            Set ApplList = New ApplicationAIList
            If (ApplList.ApplGetBalancebyReceivable(val(lstClaims1.TextMatrix(CurrentRow, 1)), y1, y2) <= 0) Then
                Call ApplList.ApplSetReceivableStatus(val(lstClaims1.TextMatrix(CurrentRow, 1)), "S")
                Set ApplList = Nothing
                Call LoadPendingClaims(True, PatientId, ProcessType, False)
            Else
                Set ApplList = Nothing
                frmEventMsgs.Header = "Balance present, cannot close"
                frmEventMsgs.AcceptText = ""
                frmEventMsgs.RejectText = "Ok"
                frmEventMsgs.CancelText = ""
                frmEventMsgs.Other0Text = ""
                frmEventMsgs.Other1Text = ""
                frmEventMsgs.Other2Text = ""
                frmEventMsgs.Other3Text = ""
                frmEventMsgs.Other4Text = ""
                frmEventMsgs.Show 1
            End If
        End If
    End If
End If
End Sub

Private Sub cmdDisplayResults_Click()
Call LoadPendingClaims(True, PatientId, ProcessType, False)
End Sub

Private Sub cmdHome_Click()
MyPracticeRepositoryCmd.CommandType = adCmdText
Unload frmPendingClaims
End Sub

Private Sub cmdDone_Click()
If (lstDisb.ListCount > 1) Then
    frmEventMsgs.Header = "Items Batched."
    frmEventMsgs.AcceptText = "Apply"
    frmEventMsgs.RejectText = "Exit"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        lstDisb.Visible = True
        DoEvents
        Call ApplyOptions(0)
        lstDisb.Clear
        lstDisb.Visible = False
        MyPracticeRepositoryCmd.CommandType = adCmdText
        Unload frmPendingClaims
    ElseIf (frmEventMsgs.Result = 2) Then
        MyPracticeRepositoryCmd.CommandType = adCmdText
        Unload frmPendingClaims
    End If
Else
    MyPracticeRepositoryCmd.CommandType = adCmdText
    Unload frmPendingClaims
End If
End Sub

Private Sub Form_Load()
StartDate = ""
EndDate = ""
End Sub

Public Function LoadPendingClaims(InitOn As Boolean, PatId As Long, PType As String, MyInit As Boolean) As Boolean
Dim Temp As String
Dim TheDate As String
Dim ApplList As ApplicationAIList
LoadPendingClaims = False
PatientId = PatId
ProcessType = PType
If (InitOn) Then
    Set ApplList = New ApplicationAIList
    Set ApplList.ApplList = lstDr
    Call ApplList.ApplLoadStaff(False)
    Set ApplList = Nothing
    If (MyInit) Then
        ResourceId = -1
    End If
    Set ApplList = New ApplicationAIList
    Set ApplList.ApplList = lstLoc
    Call ApplList.ApplLoadLocation(False, True)
    Set ApplList = Nothing
    If (MyInit) Then
        LocationId = -1
    End If
    Set ApplList = New ApplicationAIList
    Set ApplList.ApplList = lstReason
    Call ApplList.ApplLoadReasons
    Set ApplList = Nothing
    If (MyInit) Then
        ReasonCode = ""
    End If
    Label3.Visible = True
    If (MyInit) Then
        InsurerNameA = ""
    End If
End If
If (ProcessType = "@") Then
    Label1.Caption = "Pending Appeals"
ElseIf (ProcessType = "D") Then
    Label1.Caption = "Pending Denials"
End If
CurrentRow = 0
CurrentCol = 0
lblDr.Visible = True
lstDr.Visible = True
lstClaims.Clear
If (Trim(StartDate) <> "") And (Trim(EndDate) <> "") Then
    TheDate = StartDate
    Call FormatTodaysDate(TheDate, False)
    StartDate = TheDate
    TheDate = EndDate
    Call FormatTodaysDate(TheDate, False)
    EndDate = TheDate
Else
    TheDate = ""
    Call FormatTodaysDate(TheDate, False)
    StartDate = TheDate
    EndDate = TheDate
End If
Set ApplList = New ApplicationAIList
ApplList.ApplDate = Mid(TheDate, 7, 4) + Mid(TheDate, 1, 2) + Mid(TheDate, 4, 2)
ApplList.ApplStartDate = Mid(StartDate, 7, 4) + Mid(StartDate, 1, 2) + Mid(StartDate, 4, 2)
ApplList.ApplEndDate = Mid(EndDate, 7, 4) + Mid(EndDate, 1, 2) + Mid(EndDate, 4, 2)
ApplList.ApplPatId = 0
ApplList.ApplReceivableType = "I"
If (PatientId > 0) Then
    ApplList.ApplPatId = PatientId
    ApplList.ApplReceivableType = ""
End If
ApplList.ApplResourceId = ResourceId
ApplList.ApplLocId = LocationId
ApplList.ApplPayStatus = PType
If (Trim(ProcessType) <> "") Then
    ApplList.ApplReceivableType = ""
End If
ApplList.ApplInsurerId = 0
Set ApplList.ApplGrid = lstClaims
Set ApplList.ApplGrid1 = lstClaims1
lstDisb.Visible = False
If (lstDisb.ListCount = 0) Then
    lstDisb.AddItem "Close Batch List"
End If
If (ProcessType = "") Then
    LoadPendingClaims = ApplList.ApplRetrieveClaimsbyLineItem(0, "")
    cmdBill.Visible = True
    cmdBatch.Visible = False
    lstReason.Visible = False
    lblReason.Visible = False
Else
    LoadPendingClaims = ApplList.ApplRetrieveClaimsbyLineItem(1, ReasonCode)
    cmdBill.Visible = False
    cmdBatch.Visible = True
End If
Call FilterInsurers(InsurerNameA)
LoadPendingClaims = True
Set ApplList = Nothing
End Function

Private Sub Label3_Click()
Dim AUse As Boolean
Dim Temp1 As String
Dim Temp2 As String
Dim Temp3 As String
Dim Temp4 As String
Dim TheText As String
Dim ApplTemp As ApplicationTemplates
frmEventMsgs.Header = "Select Insurer ?"
frmEventMsgs.AcceptText = "Select"
frmEventMsgs.RejectText = "Any"
frmEventMsgs.CancelText = "Quit"
frmEventMsgs.Other0Text = ""
frmEventMsgs.Other1Text = ""
frmEventMsgs.Other2Text = ""
frmEventMsgs.Other3Text = ""
frmEventMsgs.Other4Text = ""
frmEventMsgs.Show 1
If (frmEventMsgs.Result = 1) Then
    Call frmSelectDialogue.BuildSelectionDialogue("INSURERONLY")
    frmSelectDialogue.Show 1
    If (frmSelectDialogue.SelectionResult) Then
        If (frmSelectDialogue.Selection <> "Create Insurer") Then
            InsurerNameA = Trim(Left(frmSelectDialogue.Selection, 55))
            Label3.Caption = InsurerNameA
            DoEvents
            Call LoadPendingClaims(False, PatientId, ProcessType, True)
        End If
    End If
ElseIf (frmEventMsgs.Result = 2) Then
    InsurerNameA = ""
    Label3.Caption = "Select Insurer"
    DoEvents
    Call LoadPendingClaims(False, PatientId, ProcessType, True)
End If
End Sub

Private Sub lstDisb_Click()
If (lstDisb.ListIndex > 0) Then
    Call ApplyOptions(lstDisb.ListIndex)
ElseIf (lstDisb.ListIndex = 0) Then
    lstDisb.Visible = False
End If
End Sub

Private Sub lstDr_Click()
Dim ApplList As ApplicationAIList
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        ResourceId = 0
    Else
        Set ApplList = New ApplicationAIList
        ResourceId = ApplList.ApplGetListResourceId(lstDr.List(lstDr.ListIndex))
        Set ApplList = Nothing
    End If
    Call LoadPendingClaims(False, PatientId, ProcessType, True)
End If
End Sub

Private Sub lstLoc_Click()
Dim ApplList As ApplicationAIList
If (lstLoc.ListIndex >= 0) Then
    If (lstLoc.ListIndex = 0) Then
        LocationId = -1
    Else
        Set ApplList = New ApplicationAIList
        LocationId = ApplList.ApplGetListResourceId(lstLoc.List(lstLoc.ListIndex))
        Set ApplList = Nothing
    End If
    Call LoadPendingClaims(False, PatientId, ProcessType, True)
End If
End Sub


Private Sub lstClaims_Click()
Dim u As Integer, z As Integer
Dim a1 As String, InvDt As String
Dim r1 As String, r2 As String
Dim b1 As Boolean, b2 As Boolean
Dim PatId As Long, InsdId As Long
Dim p1 As Long, p2 As Long
Dim InsId As Long, RcvId As Long
Dim TheDate As String, ATemp As String
Dim ApplTemp As ApplicationTemplates
Dim ApplList As New ApplicationAIList
Dim r As Integer
Dim NonZero As Boolean
Dim memR As Integer, memTR As Integer
Dim PatientDemographics As PatientDemographics

lstClaims.AllowBigSelection = False
CurrentRow = lstClaims.Row
If (lstClaims.col > 0) Then
    If (IsBillPresent) Then
        frmEventMsgs.Header = "Please Submit Bill Items First."
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        Exit Sub
    End If
End If
If (CurrentRow > 0) And (Trim(lstClaims.TextMatrix(CurrentRow, 2)) <> "") Then
    CurrentCol = lstClaims.col
    If (CurrentCol = 0) Then
        'check open ballance
        PatId = lstClaims.TextMatrix(CurrentRow, 2)
        Set ApplList = New ApplicationAIList
        If ApplList.ApplIsServicesChargeZero(lstClaims1.TextMatrix(CurrentRow, 3), 0, 1) Then
            lstClaims.TextMatrix(CurrentRow, 0) = " "
            frmEventMsgs.InfoMessage "Service(s) has $0 charge"
            frmEventMsgs.Show 1
            Exit Sub
        End If
        Set ApplList = New ApplicationAIList
        If ApplList.anyPositiveServices(lstClaims1.TextMatrix(CurrentRow, 3)) Then
        '========================================
            If IsDate(lstClaims.TextMatrix(CurrentRow, 1)) Then
                If (ProcessType <> "") Then
                    Call SelectOptions
                Else
                    PatId = val(Trim(lstClaims.TextMatrix(CurrentRow, 2)))
                    'If (Val(lstClaims.TextMatrix(CurrentRow, 11)) <> 0) Then
                    '------------------------------------------------------------
                    ' check all services
                    NonZero = False
                    r = CurrentRow
                    Do
                        If (val(lstClaims.TextMatrix(r, 11)) <> 0) Then
                            NonZero = True
                            Exit Do
                        End If
                        
                        r = r + 1
                        If r = lstClaims.Rows Then Exit Do
                        If Not lstClaims.TextMatrix(r, 1) = "" Then Exit Do
                    Loop
                    '------------------------------------------------------------
                    If NonZero Then
                        If (Trim(lstClaims.TextMatrix(CurrentRow, 0)) = "Y") Then
                            lstClaims.TextMatrix(CurrentRow, 0) = ""
                        Else
                            InsId = val(Trim(lstClaims1.TextMatrix(CurrentRow, 4)))
                            Set ApplTemp = New ApplicationTemplates
                            ATemp = ApplTemp.ApplVerifyPatient(PatId, InsId)
                            If (ATemp <> "") Then
                                frmEventMsgs.Header = ATemp
                                frmEventMsgs.AcceptText = ""
                                frmEventMsgs.RejectText = "OK"
                                frmEventMsgs.CancelText = ""
                                frmEventMsgs.Other0Text = ""
                                frmEventMsgs.Other1Text = ""
                                frmEventMsgs.Other2Text = ""
                                frmEventMsgs.Other3Text = ""
                                frmEventMsgs.Other4Text = ""
                                frmEventMsgs.Show 1
                                lstClaims.TextMatrix(CurrentRow, 0) = " "
                            Else
                                RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(lstClaims1.TextMatrix(CurrentRow, 3), InsId, InvDt)
                                If Not (ApplTemp.ApplIsUPinValid(RcvId)) Then
                                    frmEventMsgs.Header = "Upin is invalid."
                                    frmEventMsgs.AcceptText = ""
                                    frmEventMsgs.RejectText = "OK"
                                    frmEventMsgs.CancelText = ""
                                    frmEventMsgs.Other0Text = ""
                                    frmEventMsgs.Other1Text = ""
                                    frmEventMsgs.Other2Text = ""
                                    frmEventMsgs.Other3Text = ""
                                    frmEventMsgs.Other4Text = ""
                                    frmEventMsgs.Show 1
                                    lstClaims.TextMatrix(CurrentRow, 0) = " "
                                Else
                                    lstClaims.TextMatrix(CurrentRow, 0) = "Y"
                                End If
                            End If
                            Set ApplTemp = Nothing
                        End If
                    Else
                        z = CurrentRow + 1
                        If (val(lstClaims.TextMatrix(z, 11)) <> 0) And (Trim(lstClaims.TextMatrix(z, 0)) = "") Then
                            If (Trim(lstClaims.TextMatrix(CurrentRow, 0)) = "Y") Then
                                lstClaims.TextMatrix(CurrentRow, 0) = ""
                            Else
                                InsId = val(Trim(lstClaims1.TextMatrix(CurrentRow, 4)))
                                Set ApplTemp = New ApplicationTemplates
                                ATemp = ApplTemp.ApplVerifyPatient(PatId, InsId)
                                If (ATemp <> "") Then
                                    frmEventMsgs.Header = ATemp
                                    frmEventMsgs.AcceptText = ""
                                    frmEventMsgs.RejectText = "OK"
                                    frmEventMsgs.CancelText = ""
                                    frmEventMsgs.Other0Text = ""
                                    frmEventMsgs.Other1Text = ""
                                    frmEventMsgs.Other2Text = ""
                                    frmEventMsgs.Other3Text = ""
                                    frmEventMsgs.Other4Text = ""
                                    frmEventMsgs.Show 1
                                    lstClaims.TextMatrix(CurrentRow, 0) = " "
                                Else
                                    RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(lstClaims1.TextMatrix(CurrentRow, 3), InsId, InvDt)
                                    If Not (ApplTemp.ApplIsUPinValid(RcvId)) Then
                                        frmEventMsgs.Header = "Upin is invalid."
                                        frmEventMsgs.AcceptText = ""
                                        frmEventMsgs.RejectText = "OK"
                                        frmEventMsgs.CancelText = ""
                                        frmEventMsgs.Other0Text = ""
                                        frmEventMsgs.Other1Text = ""
                                        frmEventMsgs.Other2Text = ""
                                        frmEventMsgs.Other3Text = ""
                                        frmEventMsgs.Other4Text = ""
                                        frmEventMsgs.Show 1
                                        lstClaims.TextMatrix(CurrentRow, 0) = " "
                                    Else
                                        lstClaims.TextMatrix(CurrentRow, 0) = "Y"
                                    End If
                                End If
                                Set ApplTemp = Nothing
                            End If
                        End If
                    End If
                    If (lstClaims.TextMatrix(CurrentRow, 0) = "Y") Then
                        If Not (IsClaimBillable(val(lstClaims1.TextMatrix(CurrentRow, 1)), ATemp)) Then
                            frmEventMsgs.Header = ATemp
                            frmEventMsgs.AcceptText = ""
                            frmEventMsgs.RejectText = "Ok"
                            frmEventMsgs.CancelText = ""
                            frmEventMsgs.Other0Text = ""
                            frmEventMsgs.Other1Text = ""
                            frmEventMsgs.Other2Text = ""
                            frmEventMsgs.Other3Text = ""
                            frmEventMsgs.Other4Text = ""
                            frmEventMsgs.Show 1
                            lstClaims.TextMatrix(CurrentRow, 0) = " "
                        End If
                    End If
                End If
            End If
        '========================================
        Else
            frmEventMsgs.Header = "No balance to bill. Close claim."
            frmEventMsgs.AcceptText = ""
            frmEventMsgs.RejectText = "OK"
            frmEventMsgs.CancelText = ""
            frmEventMsgs.Other0Text = ""
            frmEventMsgs.Other1Text = ""
            frmEventMsgs.Other2Text = ""
            frmEventMsgs.Other3Text = ""
            frmEventMsgs.Other4Text = ""
            frmEventMsgs.Show 1
        End If
        Set ApplList = Nothing

    ElseIf (CurrentCol = 1) Then
        GoSub ShowfrmPaymentsTotal
    ElseIf (CurrentCol = 2) Then
        PatId = val(Trim(lstClaims.TextMatrix(CurrentRow, 2)))
        If (PatId > 0) Then
            Set PatientDemographics = New PatientDemographics
            PatientDemographics.PatientId = PatId
            Call PatientDemographics.DisplayPatientInfoScreen
            
            memR = lstClaims.Row
            memTR = lstClaims.TopRow
            Call LoadPendingClaims(True, PatientId, ProcessType, False)
            If Not memR < lstClaims.Rows Then memR = lstClaims.Rows - 1
            If Not memTR < lstClaims.Rows Then memTR = lstClaims.Rows - 1
            lstClaims.Row = memR
            lstClaims.TopRow = memTR
        End If
    ElseIf (CurrentCol = 3) Then
        InsdId = val(Trim(lstClaims.TextMatrix(CurrentRow, 3)))
        If (InsdId > 0) And (InsdId <> lstClaims.TextMatrix(CurrentRow, 2)) Then
            If (frmNewInsured.PatientLoadDisplay(InsdId, True, "")) Then
                frmNewInsured.Show 1
                Call LoadPendingClaims(True, PatientId, ProcessType, False)
            End If
        End If
    ElseIf (CurrentCol = 6) Then
        PatId = val(Trim(lstClaims.TextMatrix(CurrentRow, 2)))
        If (PatId > 0) Then
            DisplayInsuranceScreen (PatId)
        End If
    Else
        GoSub ShowfrmPaymentsTotal
    End If
End If
Exit Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
ShowfrmPaymentsTotal:
PatId = val(Trim(lstClaims.TextMatrix(CurrentRow, 2)))
InsId = val(Trim(lstClaims1.TextMatrix(CurrentRow, 5)))
Set ApplTemp = New ApplicationTemplates
RcvId = ApplTemp.ApplGetReceivableIdbyInvoice(lstClaims1.TextMatrix(CurrentRow, 3), InsId, InvDt)
Set PatientDemographics = New PatientDemographics
PatientDemographics.PatientId = PatId
Call PatientDemographics.DisplayInvoiceEditAndBillScreen(RcvId)
Set PatientDemographics = Nothing
Set ApplTemp = Nothing
    
    
    
    memR = lstClaims.Row
    memTR = lstClaims.TopRow
    Call LoadPendingClaims(True, PatientId, ProcessType, False)
    If Not memR < lstClaims.Rows Then memR = lstClaims.Rows - 1
    If Not memTR < lstClaims.Rows Then memTR = lstClaims.Rows - 1
    lstClaims.Row = memR
    lstClaims.TopRow = memTR
Return
End Sub

Private Sub lstReason_Click()
Dim i As Integer
Dim q As Integer
Dim CanDoIt As Boolean
CanDoIt = True
If (lstReason.ListIndex >= 0) Then
    For i = 1 To lstClaims.Rows - 1
        If (lstClaims.TextMatrix(i, 0) = "*") Then
            CanDoIt = False
        End If
    Next i
    If (CanDoIt) Then
        If (lstReason.ListIndex = 0) Then
            ReasonCode = ""
        Else
            q = InStrPS(lstReason.List(lstReason.ListIndex), "-")
            If (q > 0) Then
                ReasonCode = Trim(Left(lstReason.List(lstReason.ListIndex), q - 1))
            Else
                ReasonCode = Trim(lstReason.List(lstReason.ListIndex))
            End If
        End If
        Call LoadPendingClaims(False, PatientId, ProcessType, True)
    End If
End If
End Sub

Private Sub SSDateCombo1_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo1.Date) <> "") Then
        EndDate = SSDateCombo1.DateValue
    Else
        ATemp = EndDate
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        EndDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        SSDateCombo1.Text = EndDate
    End If
    Call FormatTodaysDate(EndDate, False)
    If (EndDate = "") Then
        SSDateCombo1.Text = ""
    End If
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        EndDate = EndDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(EndDate) > 0) Then
        EndDate = Left(EndDate, Len(StartDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(EndDate) < 1) Then
        EndDate = ""
    End If
End If
End Sub

Private Sub SSDateCombo1_Validate(Cancel As Boolean)
Call SSDateCombo1_KeyPress(13)
End Sub

Private Sub SSDateCombo2_KeyPress(KeyAscii As Integer)
Dim ATemp As String
If (KeyAscii = 13) Then
    If (Trim(SSDateCombo2.Date) <> "") Then
        StartDate = SSDateCombo2.DateValue
    Else
        ATemp = StartDate
        ATemp = Left(ATemp, 4) + "20" + Mid(ATemp, 5, 2)
        StartDate = Mid(ATemp, 1, 2) + "/" + Mid(ATemp, 3, 2) + "/" + Mid(ATemp, 5, 4)
        SSDateCombo1.Text = StartDate
    End If
    Call FormatTodaysDate(StartDate, False)
    If (StartDate = "") Then
        SSDateCombo1.Text = ""
    End If
Else
    If (KeyAscii > 47) And (KeyAscii < 58) Then
        StartDate = StartDate + Chr(KeyAscii)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(StartDate) > 0) Then
        StartDate = Left(StartDate, Len(StartDate) - 1)
    ElseIf (KeyAscii = vbKeyDelete) And (Len(StartDate) < 1) Then
        StartDate = ""
    End If
End If
End Sub

Private Sub SSDateCombo2_Validate(Cancel As Boolean)
Call SSDateCombo2_KeyPress(13)
End Sub

Private Function SelectOptions() As Boolean
Dim u As Integer
Dim DupOn As Boolean
Dim Temp As String
Dim DisplayText As String
SelectOptions = False
If (ProcessType <> "") Then
    SelectOptions = True
    frmEventMsgs.Header = "Action ?"
    frmEventMsgs.AcceptText = "Appeal"
    If (ProcessType <> "D") Then
        frmEventMsgs.AcceptText = "Denial"
    End If
    frmEventMsgs.RejectText = "Adjustment"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = "Close Denial"
    If (ProcessType <> "D") Then
        frmEventMsgs.Other0Text = "Close Appeal"
    End If
    frmEventMsgs.Other1Text = "Bill Party"
    frmEventMsgs.Other2Text = "Bill Patient"
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        DisplayText = Space(75)
        Mid(DisplayText, 1, 7) = "Appeal "
        If (ProcessType <> "D") Then
            Mid(DisplayText, 1, 7) = "Denial "
        End If
        Mid(DisplayText, 9, 7) = lstClaims.TextMatrix(CurrentRow, 8)
        Mid(DisplayText, 17, 10) = lstClaims.TextMatrix(CurrentRow, 1)
        Mid(DisplayText, 28, 10) = Trim(Str(CurrentRow))
        DisplayText = DisplayText + Trim(lstClaims1.TextMatrix(CurrentRow, 1))
        GoSub VerifyDup
        If Not (DupOn) Then
            lstDisb.AddItem DisplayText
            lstClaims.TextMatrix(CurrentRow, 0) = "*"
            frmSelectDialogue.InsurerSelected = ""
            If (ProcessType <> "D") Then
                Call frmSelectDialogue.BuildSelectionDialogue("DENIALLETTERS")
            Else
                Call frmSelectDialogue.BuildSelectionDialogue("APPEALLETTERS")
            End If
            frmSelectDialogue.Show 1
            If (frmSelectDialogue.SelectionResult) Then
                Temp = Trim(frmSelectDialogue.Selection)
                DisplayText = Space(75)
                If (ProcessType <> "D") Then
                    Mid(DisplayText, 1, 7) = "DnlLtr "
                Else
                    Mid(DisplayText, 1, 7) = "AplLtr "
                End If
                Mid(DisplayText, 9, 7) = lstClaims.TextMatrix(CurrentRow, 8)
                Mid(DisplayText, 17, 10) = lstClaims.TextMatrix(CurrentRow, 1)
                Mid(DisplayText, 28, 10) = Trim(Str(CurrentRow))
                Mid(DisplayText, 38, Len(Temp)) = Temp
                DisplayText = DisplayText + Trim(lstClaims1.TextMatrix(CurrentRow, 1))
                GoSub VerifyDup
                If Not (DupOn) Then
                    lstDisb.AddItem DisplayText
                    lstClaims.TextMatrix(CurrentRow, 0) = "*"
                End If
            End If
        End If
    ElseIf (frmEventMsgs.Result = 2) Then
        frmSelectDialogue.InsurerSelected = ""
        Call frmSelectDialogue.BuildSelectionDialogue("ADJUSTMENTSONLY")
        frmSelectDialogue.Show 1
        If (frmSelectDialogue.SelectionResult) Then
            Temp = Mid(frmSelectDialogue.Selection, 40, 1)
            DisplayText = Space(75)
            Mid(DisplayText, 1, 7) = "Adjust "
            Mid(DisplayText, 9, 7) = lstClaims.TextMatrix(CurrentRow, 8)
            Mid(DisplayText, 17, 10) = lstClaims.TextMatrix(CurrentRow, 1)
            Mid(DisplayText, 28, 10) = Trim(Str(CurrentRow))
            Mid(DisplayText, 38, Len(Temp)) = Temp
            DisplayText = DisplayText + Trim(lstClaims1.TextMatrix(CurrentRow, 1))
            GoSub VerifyDup
            If Not (DupOn) Then
                lstDisb.AddItem DisplayText
                lstClaims.TextMatrix(CurrentRow, 0) = "*"
            End If
        End If
    ElseIf (frmEventMsgs.Result = 3) Then
        DisplayText = Space(75)
        Mid(DisplayText, 1, 7) = "Cls Dny"
        If (ProcessType <> "D") Then
            Mid(DisplayText, 1, 7) = "Cls Apl"
        End If
        Mid(DisplayText, 9, 7) = lstClaims.TextMatrix(CurrentRow, 8)
        Mid(DisplayText, 17, 10) = lstClaims.TextMatrix(CurrentRow, 1)
        Mid(DisplayText, 28, 10) = Trim(Str(CurrentRow))
        DisplayText = DisplayText + Trim(lstClaims1.TextMatrix(CurrentRow, 1))
        GoSub VerifyDup
        If Not (DupOn) Then
            lstDisb.AddItem DisplayText
            lstClaims.TextMatrix(CurrentRow, 0) = "*"
            If (ProcessType = "D") Then
                For u = 0 To lstDisb.ListCount - 1
                    If (Left(lstDisb.List(u), 5) = "Party") Then
                        If (val(Trim(Mid(lstDisb.List(u), 28, 10))) = CurrentRow) Then
                            lstDisb.RemoveItem u
                            u = u - 1
                            Exit For
                        End If
                    End If
                Next u
            End If
        End If
    ElseIf (frmEventMsgs.Result = 5) Then
        DisplayText = Space(75)
        Mid(DisplayText, 1, 7) = "Party  "
        Mid(DisplayText, 9, 7) = lstClaims.TextMatrix(CurrentRow, 8)
        Mid(DisplayText, 17, 10) = lstClaims.TextMatrix(CurrentRow, 1)
        Mid(DisplayText, 28, 10) = Trim(Str(CurrentRow))
        DisplayText = DisplayText + Trim(lstClaims1.TextMatrix(CurrentRow, 1))
        GoSub VerifyDup
        If Not (DupOn) Then
            lstDisb.AddItem DisplayText
            lstClaims.TextMatrix(CurrentRow, 0) = "*"
            DisplayText = Space(75)
            Mid(DisplayText, 1, 7) = "Cls Dny"
            If (ProcessType <> "D") Then
                Mid(DisplayText, 1, 7) = "Cls Apl"
            End If
            Mid(DisplayText, 9, 7) = lstClaims.TextMatrix(CurrentRow, 8)
            Mid(DisplayText, 17, 10) = lstClaims.TextMatrix(CurrentRow, 1)
            Mid(DisplayText, 28, 10) = Trim(Str(CurrentRow))
            DisplayText = DisplayText + Trim(lstClaims1.TextMatrix(CurrentRow, 1))
        End If
    ElseIf (frmEventMsgs.Result = 6) Then
        DisplayText = Space(75)
        Mid(DisplayText, 1, 7) = "Patient"
        Mid(DisplayText, 9, 7) = lstClaims.TextMatrix(CurrentRow, 8)
        Mid(DisplayText, 17, 10) = lstClaims.TextMatrix(CurrentRow, 1)
        Mid(DisplayText, 28, 10) = Trim(Str(CurrentRow))
        DisplayText = DisplayText + Trim(lstClaims1.TextMatrix(CurrentRow, 1))
        GoSub VerifyDup
        If Not (DupOn) Then
            lstDisb.AddItem DisplayText
            lstClaims.TextMatrix(CurrentRow, 0) = "*"
        End If
    End If
End If
Exit Function
VerifyDup:
    DupOn = False
    For u = 0 To lstDisb.ListCount - 1
        If (Trim(lstDisb.List(u)) = Trim(DisplayText)) Then
            DupOn = True
        End If
    Next u
    Return
End Function

Private Function BillOptions() As Boolean
Dim ReloadOn As Boolean, DoAll As Boolean
Dim k As Integer
Dim i As Integer, j As Integer
Dim TheSrvs(24) As Integer
Dim SrvId As Long, RcvId As Long
Dim InsId As Long, p1 As Long
Dim InvDt As String
Dim InvId As String
Dim ApplTemp As ApplicationTemplates
Dim ApplTemp1 As ApplicationTemplates
Dim ApplList As ApplicationAIList
BillOptions = False
If (lstClaims.Rows > 1) Then
    ReloadOn = False
    For j = 1 To 10
        TheSrvs(j) = 0
    Next j
    j = 0
    DoAll = True
    Set ApplTemp = New ApplicationTemplates
    For i = 1 To lstClaims.Rows - 1
        If (Trim(lstClaims.TextMatrix(i, 0)) <> "") Then
            If (j > 0) Then
                GoSub PostIt
                For j = 1 To 24
                    TheSrvs(j) = 0
                Next j
                j = 0
            End If
            If (Trim(lstClaims.TextMatrix(i, 0)) = "Y") Then
                j = j + 1
                TheSrvs(j) = i
            Else
                j = j + 1
                TheSrvs(j) = 0
                DoAll = False
            End If
        End If
    Next i
    If (j > 0) Then
        GoSub PostIt
    End If
    Set ApplTemp = Nothing
    If (ReloadOn) Then
        Call LoadPendingClaims(True, PatientId, ProcessType, False)
    End If
End If
Exit Function
IsSecondary:
    Return
PostIt:
    If (DoAll) Then
        If (Trim(ProcessType) = "") Then
            InsId = val(Trim(lstClaims1.TextMatrix(TheSrvs(1), 4)))
        Else
            InsId = val(Trim(lstClaims1.TextMatrix(TheSrvs(1), 5)))
        End If
        If (InsId > 0) Then
            Set ApplTemp1 = New ApplicationTemplates
            RcvId = ApplTemp1.ApplGetReceivableIdbyInvoice(lstClaims1.TextMatrix(TheSrvs(1), 3), InsId, InvDt)
            Set ApplTemp1 = Nothing
            If (RcvId > 0) Then
                ReloadOn = True
                If Not (ApplTemp.ApplIsInsurerPrimary(RcvId)) Then
                    If Not (ApplTemp.ApplIsInsurerPlanPaper(InsId)) Then
                        If (ApplTemp.ApplIsElectronic(RcvId)) Then
                            If (ApplTemp.ApplIsElectronicValid(RcvId)) Then
                                Call ApplTemp.BatchTransaction(RcvId, "I", "", "B", True, 0, False, False, False)
                            Else
                                Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, 0, False, False, False)
                            End If
                        Else
                            Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, 0, False, False, False)
                        End If
                    Else
                        Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, 0, False, False, False)
                    End If
                Else
                    If (ApplTemp.ApplIsElectronic(RcvId)) Then
                        If (ApplTemp.ApplIsElectronicValid(RcvId)) Then
                            Call ApplTemp.BatchTransaction(RcvId, "I", "", "B", True, 0, False, False, False)
                        Else
                            Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, 0, False, False, False)
                        End If
                    Else
                        Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, 0, False, False, False)
                    End If
                    Call ApplTemp.ApplProcessAutoCross(RcvId)
                End If
                InvId = lstClaims1.TextMatrix(TheSrvs(1), 3)
                Set ApplList = New ApplicationAIList
                Call ApplList.ApplSetReceivableBalance(InvId, "", True)
                Set ApplList = Nothing
            End If
        Else
            Set ApplTemp1 = New ApplicationTemplates
            RcvId = ApplTemp1.ApplGetReceivableIdbyInvoice(lstClaims1.TextMatrix(TheSrvs(1), 3), InsId, InvDt)
            Set ApplTemp1 = Nothing
            If (RcvId > 0) Then
                ReloadOn = True
                Call ApplTemp.BatchTransaction(RcvId, "P", "", "P", True, 0, False, False, False)
            End If
        End If
    Else
        For k = 1 To j
            If (TheSrvs(k) > 0) Then
                SrvId = val(lstClaims1.TextMatrix(TheSrvs(k), 0))
                If (Trim(ProcessType) = "") Then
                    InsId = val(Trim(lstClaims1.TextMatrix(TheSrvs(k), 4)))
                Else
                    InsId = val(Trim(lstClaims1.TextMatrix(TheSrvs(k), 5)))
                End If
                If (InsId > 0) Then
                    Set ApplTemp1 = New ApplicationTemplates
                    RcvId = ApplTemp1.ApplGetReceivableIdbyInvoice(lstClaims1.TextMatrix(TheSrvs(1), 3), InsId, InvDt)
                    Set ApplTemp1 = Nothing
                    If (RcvId > 0) Then
                        ReloadOn = True
                        If Not (ApplTemp.ApplIsInsurerPrimary(RcvId)) Then
                            If Not (ApplTemp.ApplIsInsurerPlanPaper(InsId)) Then
                                If (ApplTemp.ApplIsElectronic(RcvId)) Then
                                    If (ApplTemp.ApplIsElectronicValid(RcvId)) Then
                                        Call ApplTemp.BatchTransaction(RcvId, "I", "", "B", True, 0, False, False, False)
                                    Else
                                        Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, 0, False, False, False)
                                    End If
                                Else
                                    Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, 0, False, False, False)
                                End If
                            Else
                                Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, 0, False, False, False)
                            End If
                        Else
                            If (ApplTemp.ApplIsElectronic(RcvId)) Then
                                If (ApplTemp.ApplIsElectronicValid(RcvId)) Then
                                    Call ApplTemp.BatchTransaction(RcvId, "I", "", "B", True, SrvId, False, False, False)
                                Else
                                    Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, SrvId, False, False, False)
                                End If
                            Else
                                Call ApplTemp.BatchTransaction(RcvId, "I", "", "P", True, SrvId, False, False, False)
                            End If
                            Call ApplTemp.ApplProcessAutoCross(RcvId)
                        End If
                        InvId = lstClaims1.TextMatrix(TheSrvs(1), 3)
                        Set ApplList = New ApplicationAIList
                        Call ApplList.ApplSetReceivableBalance(InvId, "", True)
                        Set ApplList = Nothing
                    End If
                Else
                    Set ApplTemp1 = New ApplicationTemplates
                    RcvId = ApplTemp1.ApplGetReceivableIdbyInvoice(lstClaims1.TextMatrix(TheSrvs(1), 3), InsId, InvDt)
                    Set ApplTemp1 = Nothing
                    If (RcvId > 0) Then
                        ReloadOn = True
                        Call ApplTemp.BatchTransaction(RcvId, "P", "", "P", True, SrvId, False, False, False)
                        Call ApplList.ApplSetReceivableBalance(InvId, "", True)
                    End If
                End If
            End If
        Next k
    End If
    Return
End Function

Private Function ApplyOptions(TheItem As Integer) As Boolean
Dim i As Integer
Dim RecId As Long
Dim PostIt As Boolean
ApplyOptions = False
If (TheItem > 0) Then
    frmEventMsgs.Header = "Item Action?"
    frmEventMsgs.AcceptText = "Apply Item"
    frmEventMsgs.RejectText = "Clear Item"
    frmEventMsgs.CancelText = "Cancel"
    frmEventMsgs.Other0Text = ""
    frmEventMsgs.Other1Text = ""
    frmEventMsgs.Other2Text = ""
    frmEventMsgs.Other3Text = ""
    frmEventMsgs.Other4Text = ""
    frmEventMsgs.Show 1
    If (frmEventMsgs.Result = 1) Then
        ApplyOptions = ApplyItem(TheItem)
        If (ApplyOptions) Then
            lstDisb.RemoveItem TheItem
        End If
        Call LoadPendingClaims(True, PatientId, ProcessType, False)
    ElseIf (frmEventMsgs.Result = 2) Then
        PostIt = True
        RecId = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
        For i = 1 To lstDisb.ListCount - 1
            If (i <> TheItem) Then
                If (RecId = val(Trim(Mid(lstDisb.List(i), 28, 10)))) Then
                    PostIt = False
                    Exit For
                End If
            End If
        Next i
        If (PostIt) Then
            lstClaims.TextMatrix(RecId, 0) = ""
        End If
        lstDisb.RemoveItem TheItem
        ApplyOptions = True
    End If
Else
    For i = 1 To lstDisb.ListCount - 1
        Call ApplyItem(i)
    Next i
    ApplyOptions = True
End If
End Function

Private Function ApplyItem(TheItem) As Boolean
Dim ARow As Integer
Dim ApptId As Long
Dim PayId As Long, InsId As Long
Dim SrvId As Long, RcvId As Long
Dim ItemId As Long, PatId As Long
Dim AChrg As String, ARsn As String
Dim RType As String, ARef As String
Dim InvId As String, IType As String
Dim ADate As String, Srv As String
Dim ApplList As ApplicationAIList
Dim ApplTemp As ApplicationTemplates
ApplyItem = False
If (Left(lstDisb.List(TheItem), 7) = "Appeal ") Then
    ARow = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
    IType = "@"
    RType = "&"
    GoSub PostPayment
    ApplyItem = True
ElseIf (Left(lstDisb.List(TheItem), 7) = "Denial ") Then
    ARow = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
    IType = "D"
    RType = "%"
    GoSub PostPayment
    ApplyItem = True
ElseIf (Left(lstDisb.List(TheItem), 7) = "Cls Dny") Then
    ARow = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
    RType = "&"
    GoSub PostClose
    ApplyItem = True
ElseIf (Left(lstDisb.List(TheItem), 7) = "Cls Apl") Then
    ARow = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
    RType = "%"
    GoSub PostClose
    ApplyItem = True
ElseIf (Left(lstDisb.List(TheItem), 7) = "Adjust ") Then
    ARow = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
    IType = Trim(Mid(lstDisb.List(TheItem), 38, 10))
    RType = "%"
    GoSub PostAdjust
    ApplyItem = True
ElseIf (Left(lstDisb.List(TheItem), 7) = "Patient") Then
    ARow = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
    IType = "P"
    GoSub PostBill
    ApplyItem = True
ElseIf (Left(lstDisb.List(TheItem), 7) = "Party  ") Then
    ARow = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
    IType = "I"
    GoSub PostBill
    ApplyItem = True
ElseIf (Left(lstDisb.List(TheItem), 7) = "AplLtr ") Then
    ARow = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
    ARef = Trim(Mid(lstDisb.List(TheItem), 38, 36))
    RType = "["
    GoSub PostTrans
    ApplyItem = True
ElseIf (Left(lstDisb.List(TheItem), 7) = "DnlLtr ") Then
    ARow = val(Trim(Mid(lstDisb.List(TheItem), 28, 10)))
    ARef = Trim(Mid(lstDisb.List(TheItem), 38, 36))
    RType = "]"
    GoSub PostTrans
    ApplyItem = True
End If
Exit Function
PostTrans:
    ADate = ""
    Call FormatTodaysDate(ADate, False)
    ARef = "/Ltr/" + Trim(ARef)
    ApptId = val(Trim(lstClaims1.TextMatrix(ARow, 2)))
    If (ApptId > 0) Then
        Set ApplList = New ApplicationAIList
        Call ApplList.ApplSetNewTransaction(ApptId, RType, ADate, "", ARef, 0)
        Set ApplList = Nothing
    End If
    Return
PostPayment:
    PayId = val(Trim(lstClaims1.TextMatrix(ARow, 0)))
    InvId = Trim(lstClaims1.TextMatrix(ARow, 3))
    ItemId = val(Trim(lstClaims1.TextMatrix(ARow, 4)))
    RcvId = val(Trim(lstClaims1.TextMatrix(ARow, 1)))
    ADate = ""
    Call FormatTodaysDate(ADate, False)
    AChrg = Trim(lstClaims.TextMatrix(ARow, 11))
    PatId = val(Trim(lstClaims.TextMatrix(ARow, 2)))
    Srv = Trim(lstClaims.TextMatrix(ARow, 7))
    If (Trim(ProcessType) = "") Then
        InsId = val(Trim(lstClaims1.TextMatrix(ARow, 4)))
    Else
        InsId = val(Trim(lstClaims1.TextMatrix(ARow, 5)))
    End If
    ARsn = Trim(lstClaims.TextMatrix(ARow, 13))
    If (RcvId > 0) Then
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatId, 0, InvId, "I", InsId, AChrg, ADate, IType, "", "", "", "", Srv, ItemId, False, UserLogin.iId, False, ARsn, 0, "", "", _
        -1)
        Call ApplTemp.ApplChangePaymentStatus(PayId, RType)
        Set ApplTemp = Nothing
    End If
    Return
PostClose:
    PayId = val(Trim(lstClaims1.TextMatrix(ARow, 0)))
    If (PayId > 0) Then
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplChangePaymentStatus(PayId, RType)
        Set ApplTemp = Nothing
    End If
    Return
PostAdjust:
    PayId = val(Trim(lstClaims1.TextMatrix(ARow, 0)))
    InvId = Trim(lstClaims1.TextMatrix(ARow, 3))
    ItemId = val(Trim(lstClaims1.TextMatrix(ARow, 4)))
    RcvId = val(Trim(lstClaims1.TextMatrix(ARow, 1)))
    AChrg = Trim(lstClaims.TextMatrix(ARow, 11))
    ADate = ""
    Call FormatTodaysDate(ADate, False)
    PatId = val(Trim(lstClaims.TextMatrix(ARow, 2)))
    Srv = Trim(lstClaims.TextMatrix(ARow, 7))
    If (Trim(ProcessType) = "") Then
        InsId = val(Trim(lstClaims1.TextMatrix(ARow, 4)))
    Else
        InsId = val(Trim(lstClaims1.TextMatrix(ARow, 5)))
    End If
    ARsn = Trim(lstClaims.TextMatrix(ARow, 13))
    If (RcvId > 0) Then
        Set ApplTemp = New ApplicationTemplates
        Call ApplTemp.ApplPostPaymentActivity(RcvId, PatId, 0, InvId, "I", InsId, AChrg, ADate, IType, "", "", "", "", Srv, ItemId, False, UserLogin.iId, False, ARsn, 0, "", "", _
        -1)
        Call ApplTemp.ApplChangePaymentStatus(PayId, RType)
        Set ApplTemp = Nothing
    End If
    Return
PostBill:
    SrvId = val(lstClaims1.TextMatrix(ARow, 0))
    If (Trim(ProcessType) = "") Then
        InsId = val(Trim(lstClaims1.TextMatrix(ARow, 4)))
    Else
        InsId = val(Trim(lstClaims1.TextMatrix(ARow, 5)))
    End If
    RcvId = val(Trim(lstClaims1.TextMatrix(ARow, 1)))
    AChrg = Trim(lstClaims.TextMatrix(ARow, 11))
    If (RcvId > 0) Then
        Set ApplTemp = New ApplicationTemplates
        If (IType = "P") Then
            Call ApplTemp.BatchTransaction(RcvId, "P", AChrg, "P", True, SrvId, False, False, False)
        Else
            If (ApplTemp.ApplIsElectronic(RcvId)) Then
                Call ApplTemp.BatchTransaction(RcvId, "I", AChrg, "B", True, SrvId, False, False, False)
            Else
                Call ApplTemp.BatchTransaction(RcvId, "I", AChrg, "P", True, SrvId, False, False, False)
            End If
        End If
        Set ApplTemp = Nothing
    End If
    Return
End Function

Private Function IsBillPresent() As Boolean
Dim i As Integer
IsBillPresent = False
For i = 1 To lstClaims.Rows - 1
    If (lstClaims.TextMatrix(i, 0) = "Y") Then
        IsBillPresent = True
    End If
Next i
End Function

Public Function IsClaimBillable(RcvId As Long, ATemp As String, Optional ShortMode As Boolean) As Boolean
Dim Temp As String
Dim a1 As String
Dim a2 As String
Dim InvId As String
Dim IDate As String
Dim IPlan As Long
Dim InsId As Long
Dim ApptId As Long
Dim NPIOn As Boolean
Dim NPIAll As Boolean
Dim GTemp As String
Dim ApplTemp As ApplicationTemplates
IsClaimBillable = False
If (RcvId > 0) Then
    IsClaimBillable = True
    NPIOn = CheckConfigCollection("NPIONLY") = "T"
    NPIAll = CheckConfigCollection("NPIALL") = "T"
    Set ApplTemp = New ApplicationTemplates
' Get The Invoice
    Call ApplTemp.ApplGetInvoice(RcvId, InvId, IDate, IPlan, ApptId)
    If CheckConfigCollection("REQUIREBILLDR") = "T" Then
        If Not (ApplTemp.ApplIsDoctorBillable(RcvId)) Then
            IsClaimBillable = False
            ATemp = "Billing Doctor Required"
        End If
    End If
    If CheckConfigCollection("REQUIREREFERRALTOBILL") = "T" Then
        If (ApplTemp.ApplIsReceivableReferralRequired(RcvId)) Then
            IsClaimBillable = False
            ATemp = "Referring Doctor Required"
        End If
    End If
    If CheckConfigCollection("REQUIREREFDRUPIN") = "T" Then
        If Not (NPIOn) And Not (NPIAll) Then
            If Not (ApplTemp.ApplIsRefDrUPinOn(RcvId)) Then
                IsClaimBillable = False
                ATemp = "Referring Doctor UPIN Required"
            End If
            If Not (ApplTemp.ApplIsRefDrAffOn(RcvId)) Then
                IsClaimBillable = False
                ATemp = "Referring Doctor PIN Required"
            End If
        End If
    End If
    If (NPIOn) Or (NPIAll) Then
        If Not (ApplTemp.ApplIsRefDrNPIOn(RcvId)) Then
            IsClaimBillable = False
            ATemp = "Referring Doctor requires an NPI"
        End If
    End If
    If Not (ApplTemp.ApplIsRefDrNameOn(RcvId)) Then
        IsClaimBillable = False
        ATemp = "Referring Doctor requires a First And Last Name"
    End If
    If (ApplTemp.ApplIsServiceReferralRequired(RcvId)) Then
        IsClaimBillable = False
        ATemp = "Consults with Diagnostic Tests Need Ref Dr"
    End If
    
    Temp = ApplTemp.ApplPatientLevelVerification(RcvId)
    If (Trim(Temp) <> "") Then
        IsClaimBillable = False
        ATemp = Temp
    End If
    
    Temp = ApplTemp.ApplDiagnosisLevelVerification(RcvId)
    If (Trim(Temp) <> "") Then
        IsClaimBillable = False
        ATemp = Temp
    End If
' Applies only to Medicaids
    If (ApplTemp.ApplIsInsurerMedicaidNJ(IPlan)) Then
        If Not (ApplTemp.ApplIsRefDrUniqueForServices(RcvId)) Then
            IsClaimBillable = False
            ATemp = "Referring Dr must be assigned and not the rendering physician"
        ElseIf Not (ApplTemp.ApplIsRefDrAffPresent(RcvId)) And (Not (NPIOn)) Then
            IsClaimBillable = False
            ATemp = "Referring Dr must be assigned an affiliation number"
        End If
    End If
' Verify the UPin
    If (NPIOn) Or (NPIAll) Then
        If Not (ApplTemp.ApplIsNPIValid(RcvId)) Then
            IsClaimBillable = False
            ATemp = "NPI is not valid"
        End If
    Else
        If Not (ApplTemp.ApplIsUPinValid(RcvId)) Then
            IsClaimBillable = False
            ATemp = "Upin is not valid"
        End If
    End If
    If (ApplTemp.ApplIsElectronic(RcvId)) Then
        If Not (ApplTemp.ApplIsElectronicValid(RcvId)) Then
            IsClaimBillable = False
            ATemp = "Insurer Requires NEIC Number"
        End If
    End If
    
    If Not ShortMode Then
        If (IsClaimBillable) Then
            IsClaimBillable = ApplTemp.ApplIsPlanValid(RcvId)
            If Not (IsClaimBillable) Then
                ATemp = "Incorrect Insurer, please set correct insurer."
            End If
        End If
    End If
    
    Set ApplTemp = Nothing
End If
End Function

Private Function FilterInsurers(InsName As String) As Boolean
Dim i As Integer
Dim u As Integer
If (Trim(InsName) <> "") And (lstClaims.Rows > 1) Then
    u = lstClaims.Rows - 1
    For i = 1 To u
        If (i <= u) Then
            If (Trim(lstClaims.TextMatrix(i, 6)) <> "") Then
                If (UCase(Left(lstClaims.TextMatrix(i, 6), Len(InsName))) <> UCase(InsName)) Then
                    lstClaims.RemoveItem i
                    lstClaims1.RemoveItem i
                    i = i - 1
                    u = lstClaims.Rows - 1
                End If
            End If
            If (i >= u) Then
                Exit For
            End If
        End If
    Next i
End If
End Function
Public Sub FrmClose()
Call cmdHome_Click
Unload Me
End Sub
