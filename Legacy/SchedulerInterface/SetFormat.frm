VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmSetFormat 
   BackColor       =   &H00808000&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   8385
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00808000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   8385
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstFormat 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Left            =   5400
      TabIndex        =   4
      Top             =   1200
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.ListBox lstFields 
      BeginProperty Font 
         Name            =   "Lucida Sans"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5235
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Width           =   4215
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdExit 
      Height          =   990
      Left            =   120
      TabIndex        =   1
      Top             =   7080
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetFormat.frx":0000
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   990
      Left            =   9960
      TabIndex        =   2
      Top             =   7200
      Width           =   1815
      _Version        =   131072
      _ExtentX        =   3201
      _ExtentY        =   1746
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "SetFormat.frx":01DF
   End
   Begin VB.Label lblFormat 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Format Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   5400
      TabIndex        =   6
      Top             =   840
      Width           =   1320
   End
   Begin VB.Label lblFields 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Merge Fields"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   5
      Top             =   840
      Width           =   1380
   End
   Begin VB.Label lblName 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Print Display Format Definition for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   3840
   End
End
Attribute VB_Name = "frmSetFormat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public TheFormat As String
Private CurrentFormat As String

Private Sub cmdDone_Click()
TheFormat = CurrentFormat
Unload frmSetFormat
End Sub

Private Sub cmdExit_Click()
TheFormat = ""
Unload frmSetFormat
End Sub

Public Function LoadFormat(ACode As String, AFormat, FormType As Boolean) As Boolean
Dim i As Integer
Dim RetQues As DynamicClass
LoadFormat = True
lblName.Caption = "Print Display Definition for " + Trim(Left(ACode, 32))
If (Trim(AFormat) = "") Then
    CurrentFormat = String(255, "0")
Else
    CurrentFormat = AFormat
End If
lblFormat.Visible = False
lstFormat.Clear
lstFormat.AddItem "No Format"
lstFormat.AddItem "Text SemiColon Space"
lstFormat.AddItem "Text CrLf"
lstFormat.AddItem "Text Comma Space"
lstFormat.AddItem "Eye Results CrLf"
lstFormat.AddItem "Eye Results SemiColon"
lstFormat.AddItem "Eye Results Comma"
lstFormat.AddItem "Combined City State Zip"
lstFormat.AddItem "TestName Eye Results CrLf"
lstFormat.AddItem "TestName Eye Results SemiColon"
lstFormat.AddItem "TestName Eye Results Comma "
lstFormat.AddItem "Results Comma"
lstFormat.AddItem "Results <in the> Eye Comma"
lstFormat.AddItem "Finding Results SemiColon"
lstFormat.AddItem "Finding Results Comma"
lstFormat.AddItem "Finding Results CrLf"
lstFormat.AddItem "Test Special Setting"

lstFields.Clear
If (FormType) Then
    lstFields.AddItem "Todays Date"
    lstFields.AddItem "Patient Name"
    lstFields.AddItem "Surgery Order Date"
    lstFields.AddItem "Patient Address"
    lstFields.AddItem "Patient Suite"
    lstFields.AddItem "Patient City"
    lstFields.AddItem "Patient State"
    lstFields.AddItem "Patient Zip"
    lstFields.AddItem "Patient City, State, Zip"
    lstFields.AddItem "Patient First Name"
    lstFields.AddItem "Patient MI"
    lstFields.AddItem "Patient Last Name"
    lstFields.AddItem "Patient Number"
    lstFields.AddItem "Patient Age"
    lstFields.AddItem "Patient Home Phone"
    lstFields.AddItem "Patient Business Phone"
    lstFields.AddItem "Patient Cell Phone"
    lstFields.AddItem "Patient Emergency Contact"
    lstFields.AddItem "Patient Emergency Phone"
    lstFields.AddItem "Patient Emergency Relationship"
    lstFields.AddItem "Patient Birth Date"
    lstFields.AddItem "Patient SSN"
    lstFields.AddItem "Patient Language"
    lstFields.AddItem "Patient Sex"
    lstFields.AddItem "Insured Name"
    lstFields.AddItem "Insurer 1"
    lstFields.AddItem "Insurer 1 Address"
    lstFields.AddItem "Insurer 1 City, State, Zip"
    lstFields.AddItem "Insurer 1 Policy"
    lstFields.AddItem "Insurer 1 Group"
    lstFields.AddItem "Insurer 2"
    lstFields.AddItem "Insurer 2 Address"
    lstFields.AddItem "Insurer 2 City, State, Zip"
    lstFields.AddItem "Insurer Policy 2"
    lstFields.AddItem "Insurer Group 2"
    lstFields.AddItem "Doctor Name"
    lstFields.AddItem "PreCert Number"
    lstFields.AddItem "Surgery Location"
    lstFields.AddItem "Surgery Loc Address"
    lstFields.AddItem "Surgery Loc Suite"
    lstFields.AddItem "Diagnosis 1"
    lstFields.AddItem "Diagnosis 2"
    lstFields.AddItem "Diagnosis 3"
    lstFields.AddItem "Diagnosis 4"
    lstFields.AddItem "HPI"
    lstFields.AddItem "Chief Complaints"
    lstFields.AddItem "Combo Complaints"
    lstFields.AddItem "OHX"
    lstFields.AddItem "PMHX"
    lstFields.AddItem "FMX"
    lstFields.AddItem "Allergies"
    lstFields.AddItem "Social History"
    lstFields.AddItem "Prescribed Meds"
    lstFields.AddItem "Other Meds"
    lstFields.AddItem "Surgical Eye"
    lstFields.AddItem "Surgery Name"
    lstFields.AddItem "Surgery Case Type"
    lstFields.AddItem "Surgery Loc City"
    lstFields.AddItem "Surgery Loc State"
    lstFields.AddItem "Surgery Loc Zip"
    lstFields.AddItem "MRX"
    lstFields.AddItem "A Scan"
    lstFields.AddItem "Autokeratometry"
    lstFields.AddItem "Keratometry"
    lstFields.AddItem "IOL Master"
    lstFields.AddItem "VSC"
    lstFields.AddItem "VCC"
    lstFields.AddItem "IOP"
    lstFields.AddItem "General Medical Observations"
    lstFields.AddItem "Topography"
    lstFields.AddItem "Surgeon"
    lstFields.AddItem "Assistant Surgeon"
    lstFields.AddItem "Other Assistant"
    lstFields.AddItem "Patient Orders"
    lstFields.AddItem "IOL"
    lstFields.AddItem "IOL Power"
    lstFields.AddItem "IOL Backup"
    lstFields.AddItem "IOL Backup Power"
    lstFields.AddItem "Transportation"
    lstFields.AddItem "OR Anestesia"
    lstFields.AddItem "OR Instruments"
    lstFields.AddItem "OR Medical Clearance"
    lstFields.AddItem "OR Pre-Surgical Meds"
    lstFields.AddItem "OR Medical Supplies"
    lstFields.AddItem "LRI: Incisions"
    lstFields.AddItem "LRI: Axis"
    lstFields.AddItem "LRI: Arc"
    lstFields.AddItem "Surgery Notes"
    lstFields.AddItem "Surgery Date"
    lstFields.AddItem "PostOP Date"
    lstFields.AddItem "Past Tests 1"
    lstFields.AddItem "Past Tests 2"
    lstFields.AddItem "Past Tests 3"
    lstFields.AddItem "Past Tests 4"
    lstFields.AddItem "Past Tests 5"
    lstFields.AddItem "Surgery Phone"
    lstFields.AddItem "Referring Doctor"
    lstFields.AddItem "Surgery Time"
    lstFields.AddItem "Surgery PostOp Time"
    lstFields.AddItem "Previous Same Surgery 1"
    lstFields.AddItem "Previous Same Surgery 2"
    lstFields.AddItem "Surgery Aim"
    lstFields.AddItem "RX with Surgical Issues"
    lstFields.AddItem "RX Ocular"
Else
    lstFields.AddItem "PCP City/State/Zip"
    lstFields.AddItem "Bill Dr City/State/Zip"
    lstFields.AddItem "Clinical Plan - Actions"
    lstFields.AddItem "Clinical Findings"
    lstFields.AddItem "Clinical Tests"
    lstFields.AddItem "OHX"
    lstFields.AddItem "PMHX"
    lstFields.AddItem "FMX"
    lstFields.AddItem "Allergies"
    lstFields.AddItem "Medications"
    lstFields.AddItem "Clinical Impressions"
    lstFields.AddItem "HPI"
    lstFields.AddItem "Patient City/State/Zip"
    lstFields.AddItem "Primary Insurer City/State/Zip"
    lstFields.AddItem "Second Insurer City/State/Zip"
    lstFields.AddItem "Practice City/State/Zip"
    lstFields.AddItem "Diagnosis 1"
    lstFields.AddItem "Diagnosis 2"
    lstFields.AddItem "Diagnosis 3"
    lstFields.AddItem "Diagnosis 4"
    lstFields.AddItem "Impr Link Plan Notes"
    lstFields.AddItem "Social History"
    Set RetQues = New DynamicClass
    RetQues.QuestionParty = "T"
    If (RetQues.FindQuestionbyFormatMask > 0) Then
        i = 1
        While (RetQues.SelectClassForm(i))
            If (RetQues.FormatMaskId > 0) Then
                lstFields.AddItem Trim(RetQues.Question)
            End If
            i = i + 1
        Wend
    End If
    Set RetQues = Nothing
End If
End Function

Private Sub lstFields_Click()
Dim u As Integer
Dim n As Integer
If (lstFields.ListIndex >= 0) Then
    n = lstFields.ListIndex + 1
    If (Len(CurrentFormat) >= n) Then
        If (IsNumeric(Mid(CurrentFormat, n, 1))) Then
            lstFormat.ListIndex = val(Mid(CurrentFormat, n, 1))
        Else
            u = Asc(Mid(CurrentFormat, n, 1)) - 55
            lstFormat.ListIndex = u
        End If
        lstFormat.Visible = True
    End If
End If
End Sub

Private Sub lstFormat_Click()
If (lstFormat.ListIndex >= 0) Then
    If (lstFormat.ListIndex < 10) Then
        Mid(CurrentFormat, lstFields.ListIndex + 1, 1) = Trim(Str(lstFormat.ListIndex))
    Else
        Mid(CurrentFormat, lstFields.ListIndex + 1, 1) = Chr(55 + lstFormat.ListIndex)
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdExit_Click
Unload Me
End Sub
