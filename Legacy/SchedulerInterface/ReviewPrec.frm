VERSION 5.00
Object = "{FD2FB1F1-D4FC-11CE-A335-A8D5ECAE5B02}#2.0#0"; "btn32a20.ocx"
Begin VB.Form frmReviewPrec 
   BackColor       =   &H0077742D&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   9000
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12000
   ForeColor       =   &H00A95911&
   LinkTopic       =   "Form1"
   ScaleHeight     =   9000
   ScaleWidth      =   12000
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.ListBox lstDr 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      ItemData        =   "ReviewPrec.frx":0000
      Left            =   8160
      List            =   "ReviewPrec.frx":0002
      TabIndex        =   3
      Top             =   240
      Width           =   1575
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdDone 
      Height          =   870
      Left            =   10200
      TabIndex        =   0
      Top             =   7560
      Width           =   1455
      _Version        =   131072
      _ExtentX        =   2566
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewPrec.frx":0004
   End
   Begin VB.ListBox lstAppts 
      BackColor       =   &H006C6928&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   6000
      ItemData        =   "ReviewPrec.frx":01E3
      Left            =   360
      List            =   "ReviewPrec.frx":01E5
      TabIndex        =   1
      Top             =   1440
      Width           =   11295
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdNext 
      Height          =   870
      Left            =   1680
      TabIndex        =   5
      Top             =   7560
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1535
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewPrec.frx":01E7
   End
   Begin fpBtnAtlLibCtl.fpBtn cmdPrev 
      Height          =   855
      Left            =   360
      TabIndex        =   6
      Top             =   7560
      Width           =   1215
      _Version        =   131072
      _ExtentX        =   2143
      _ExtentY        =   1508
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      GrayAreaColor   =   11098385
      BorderShowDefault=   -1  'True
      ButtonType      =   0
      NoPointerFocus  =   0   'False
      Value           =   0   'False
      GroupID         =   0
      GroupSelect     =   0
      DrawFocusRect   =   2
      DrawFocusRectCell=   -1
      GrayAreaPictureStyle=   0
      Static          =   0   'False
      BackStyle       =   1
      AutoSize        =   0
      AutoSizeOffsetTop=   0
      AutoSizeOffsetBottom=   0
      AutoSizeOffsetLeft=   0
      AutoSizeOffsetRight=   0
      DropShadowOffsetX=   3
      DropShadowOffsetY=   3
      DropShadowType  =   0
      DropShadowColor =   0
      Redraw          =   -1  'True
      ButtonDesigner  =   "ReviewPrec.frx":03C6
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Doctor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   8160
      TabIndex        =   4
      Top             =   0
      Width           =   570
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H0077742D&
      Caption         =   "Review Appointments for"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   360
      TabIndex        =   2
      Top             =   240
      Width           =   3180
   End
End
Attribute VB_Name = "frmReviewPrec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Const MaxCount As Integer = 25
Private CurrentIndex As Integer
Private TotalFound As Long
Private ResourceId As Long

Private Sub cmdDone_Click()
Unload frmReviewPrec
End Sub

Private Sub cmdNext_Click()
If (CurrentIndex > TotalFound) Then
    CurrentIndex = 1
End If
Call LoadApptsbyPreCert(False)
End Sub

Private Sub cmdPrev_Click()
If (CurrentIndex > TotalFound) Then
    CurrentIndex = TotalFound
End If
CurrentIndex = CurrentIndex - (lstAppts.ListCount + MaxCount)
If (CurrentIndex <= 1) Then
    CurrentIndex = 1
End If
Call LoadApptsbyPreCert(False)
End Sub

Public Function LoadApptsbyPreCert(Init As Boolean) As Boolean
Dim ADate As String
Dim TheDate As String
Dim EndDate As String
Dim DisplayText As String
Dim ApplList As ApplicationAIList
LoadApptsbyPreCert = False
Label1.Caption = "Review PreCerts"
lstAppts.Clear
If (Init) Then
    CurrentIndex = 1
    Set ApplList = New ApplicationAIList
    Set ApplList.ApplList = lstDr
    Call ApplList.ApplLoadStaff(False)
    Set ApplList = Nothing
End If
Call FormatTodaysDate(ADate, False)
ADate = Mid(ADate, 7, 4) + Mid(ADate, 1, 2) + Mid(ADate, 4, 2)
Label2.Visible = True
lstDr.Visible = True
Set ApplList = New ApplicationAIList
ApplList.ApplDate = ""
ApplList.ApplStartAt = CurrentIndex
ApplList.ApplReturnCount = MaxCount
ApplList.ApplResourceId = ResourceId
Set ApplList.ApplList = lstAppts
TotalFound = ApplList.ApplRetrievePreCertsList
CurrentIndex = ApplList.ApplReturnCount
Set ApplList = Nothing
LoadApptsbyPreCert = True
End Function

Private Sub Form_Load()
If UserLogin.HasPermission(epPowerUser) Then
    frmReviewPrec.BorderStyle = 1
    frmReviewPrec.ClipControls = True
    frmReviewPrec.Caption = Mid(frmReviewPrec.Name, 4, Len(frmReviewPrec.Name) - 3)
    frmReviewPrec.AutoRedraw = True
    frmReviewPrec.Refresh
End If
End Sub

Private Sub lstAppts_Click()
Dim ApptId As Long
Dim PatId As Long
Dim PreId As Long
Dim OrgPreId As Long
Dim ApplList As ApplicationAIList
If (lstAppts.ListIndex >= 0) Then
    Set ApplList = New ApplicationAIList
    ApptId = ApplList.ApplGetAppointmentId(lstAppts.List(lstAppts.ListIndex))
    PatId = ApplList.ApplGetPatientId(lstAppts.List(lstAppts.ListIndex))
    PreId = ApplList.ApplGetPreCertId(lstAppts.List(lstAppts.ListIndex))
    OrgPreId = PreId
    If (PatId > 0) Then
        Dim PreCertId As Long
        Dim ReturnArguments As Object
        Dim PatientDemographics As New PatientDemographics
        Set ReturnArguments = PatientDemographics.DisplayPreAuthorizationScreen(ApptId, OrgPreId)
        Set PatientDemographics = Nothing
        If Not ReturnArguments Is Nothing Then
            PreId = ReturnArguments.AuthorizationId
        End If
        If (PreId <> OrgPreId) Then
            Call ApplList.ApplPostPreCert(ApptId, PreId, UserLogin.iId)
        End If
        Call LoadApptsbyPreCert(False)
    End If
    Set ApplList = Nothing
End If
End Sub

Private Sub lstDr_Click()
Dim ApplList As ApplicationAIList
If (lstDr.ListIndex >= 0) Then
    If (lstDr.ListIndex = 0) Then
        ResourceId = 0
    Else
        Set ApplList = New ApplicationAIList
        ResourceId = ApplList.ApplGetListResourceId(lstDr.List(lstDr.ListIndex))
        Set ApplList = Nothing
    End If
    Call LoadApptsbyPreCert(True)
    If (lstAppts.ListCount < 1) Then
        frmEventMsgs.Header = "Nothing Outstanding"
        frmEventMsgs.AcceptText = ""
        frmEventMsgs.RejectText = "Ok"
        frmEventMsgs.CancelText = ""
        frmEventMsgs.Other0Text = ""
        frmEventMsgs.Other1Text = ""
        frmEventMsgs.Other2Text = ""
        frmEventMsgs.Other3Text = ""
        frmEventMsgs.Other4Text = ""
        frmEventMsgs.Show 1
        ResourceId = 0
        Call LoadApptsbyPreCert(True)
        lstDr.ListIndex = -1
    End If
End If
End Sub
Public Sub FrmClose()
Call cmdDone_Click
Unload Me
End Sub

